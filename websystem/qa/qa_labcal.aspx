﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="qa_labcal.aspx.cs" Inherits="websystem_qa_qa_labcal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:HyperLink ID="SETFOCUS" runat="server"></asp:HyperLink>
    <div class="col-md-12" role="main">
        <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    </div>

    <!--tab menu-->

    <nav class="navbar navbar-default" id="divBtnHead" runat="server" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-bar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="sub-navbar">
                    <a class="navbar-brand" href="#"><b>Menu</b></a>
                </div>
            </div>

            <!--Collect the nav links, forms, and other content for toggling-->
            <div class="collapse navbar-collapse" id="menu-bar">
                <ul class="nav navbar-nav" id="uiNav" runat="server">
                    <li id="li0" runat="server">
                        <asp:LinkButton ID="lbtab1" runat="server" CommandName="cmdtab2" OnCommand="navCommand" CommandArgument="tab1"> แจ้งขอบริการสอบเทียบ</asp:LinkButton>
                    </li>
                    <li id="li1" runat="server">
                        <asp:LinkButton ID="lbtab2" runat="server" CommandName="cmdtab2" OnCommand="navCommand" CommandArgument="tab2"> รายการสอบเทียบ</asp:LinkButton>
                    </li>
                    <li id="li2" runat="server">
                        <asp:LinkButton ID="lbtab3" runat="server" Visible="false" CommandName="cmdtab3" OnCommand="navCommand" CommandArgument="tab3"> ทะเบียนเครื่องมือ</asp:LinkButton>
                    </li>
                    <%--  <li id="li3" runat="server">
                        <asp:LinkButton ID="lbtab4" runat="server" CommandName="cmdtab4" OnCommand="navCommand" CommandArgument="tab4"> การจัดการเครื่องมือ</asp:LinkButton>
                    </li>--%>
                    <li id="li4" runat="server" class="dropdown">
                        <asp:LinkButton ID="lbtab5" runat="server" CommandName="cmdtab5" OnCommand="navCommand" class="dropdown-toggle" data-toggle="dropdown"
                            role="button" aria-haspopup="true" aria-expanded="false" CommandArgument="tab5"> Master Data <span class="caret"></span></asp:LinkButton>

                        <ul class="dropdown-menu">
                            <li>
                                <asp:LinkButton ID="LinkButton54" runat="server" data-original-title="ผลการสอบเทียบ" data-toggle="tooltip" OnCommand="btnCommand"
                                    CommandArgument="3" CommandName="cmdView"> ผลการสอบเทียบ</asp:LinkButton>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li>
                                <asp:LinkButton ID="ss" runat="server" data-original-title="หน่วยวัด" data-toggle="tooltip" OnCommand="btnCommand"
                                    CommandArgument="4" CommandName="cmdView"> หน่วยวัด</asp:LinkButton>
                            </li>
                        </ul>
                    </li>
                </ul>
                <%--  <ul class="nav navbar-nav navbar-right" runat="server">
                    <li id="li5" runat="server">
                        <a href='<%=ResolveUrl("~/print-formcal") %>' target="_blank"><i class="fa fa-print" aria-hidden="true"></i>Calibration Request Form</a>

                    </li>
                </ul>--%>
            </div>
        </div>
    </nav>
    <!--tab menu-->

    <!--search-->



    <!--multiview-->
    <asp:MultiView ID="mvLabCal" runat="server">

        <asp:View ID="tab1" runat="server">
            <%--  1--%>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">รายละเอียดผู้ใช้งาน</h3>
                </div>
                <!--user info-->
                <div class="panel-body">
                    <div class="form-horizontal" role="form">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">รหัสพนักงาน</label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="tbEmpCode" runat="server" CssClass="form-control" Text='6000xxxx' Enabled="false" />
                            </div>
                            <%-- <label class="col-sm-2 control-label">ชื่อ - นามสกุล</label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="tbEmpName" runat="server" CssClass="form-control" Text='ทดสอบ ทดสอบ' Enabled="false" />
                            </div>--%>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">ชื่อ - นามสกุล</label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="tbEmpName" runat="server" CssClass="form-control" Text='ทดสอบ ทดสอบ' Enabled="false" />
                            </div>
                            <%--  <label class="col-sm-2 control-label">องค์กร</label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="tbEmpOrg" runat="server" CssClass="form-control" Text='องค์กรทดสอบ' Enabled="false" />
                            </div>--%>
                            <label class="col-sm-2 control-label">ฝ่าย</label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="tbEmpDept" runat="server" CssClass="form-control" Text='ทดสอบ ทดสอบ' Enabled="false" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">แผนก</label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="tbEmpSec" runat="server" CssClass="form-control" Text='ทดสอบ ทดสอบ' Enabled="false" />
                            </div>
                            <label class="col-sm-2 control-label">ตำแหน่ง</label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="tbEmpPos" runat="server" CssClass="form-control" Text='ทดสอบ ทดสอบ' Enabled="false" />
                            </div>
                        </div>
                    </div>
                </div>
                <!--user info-->
            </div>

            <asp:FormView ID="fvDocDetail2" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <%--    <asp:HiddenField ID="hfNodeIDX" runat="server" Value="1" />
                    <asp:HiddenField ID="hfActorIDX" runat="server" Value="1" />--%>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">รายละเอียดรายการ</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <%-- <label class="col-sm-2 control-label">Document No</label>
                                    <div class="col-sm-4 control-label textleft">
                                        CAL0000001
                                    </div>--%>
                                    <label class="col-sm-2 control-label">ผู้ทำรายการ :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        ทดสอบ ทดสอบ
                                    </div>
                                    <label class="col-sm-2 control-label">ฝ่าย :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        ทดสอบ ทดสอบ
                                    </div>
                                </div>
                                <%--  <div class="form-group">
                                    <label class="col-sm-2 control-label">องค์กร :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        ทดสอบ ทดสอบ
                                    </div>
                                    <label class="col-sm-2 control-label">ฝ่าย :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        ทดสอบ ทดสอบ
                                    </div>
                                </div>--%>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">แผนก :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        ทดสอบ ทดสอบ
                                    </div>
                                    <label class="col-sm-2 control-label">ตำแหน่ง :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        ทดสอบ ทดสอบ
                                    </div>
                                </div>
                                <div class="form-group">

                                    <label class="col-sm-2 control-label">แผนกผู้ถือครอง :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        QA LAB
                                    </div>
                                    <label class="col-sm-2 control-label">ประเภท :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        เครื่องมือใหม่
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="lbDocCancel" CssClass="btn btn-default" runat="server" data-original-title="Back" data-toggle="tooltip" OnCommand="btnCommand" CommandArgument="0" CommandName="cmdDocCancel"><i class="fa fa-reply" aria-hidden="true"></i> Back</asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:FormView ID="fvDetails5" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">รายละเอียดรายการ</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <%--  <label class="col-sm-2 control-label">Document No</label>
                                    <div class="col-sm-4 control-label textleft">
                                        CAL0000003
                                    </div>--%>
                                    <label class="col-sm-2 control-label">ผู้ทำรายการ :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        ทดสอบ ทดสอบ
                                    </div>
                                    <label class="col-sm-2 control-label">ฝ่าย :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        ทดสอบ ทดสอบ
                                    </div>
                                </div>
                                <%-- <div class="form-group">
                                    <label class="col-sm-2 control-label">องค์กร</label>
                                    <div class="col-sm-4 control-label textleft">
                                        ทดสอบ ทดสอบ
                                    </div>
                                    <label class="col-sm-2 control-label">ฝ่าย :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        ทดสอบ ทดสอบ
                                    </div>
                                </div>--%>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">แผนก :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        ทดสอบ ทดสอบ
                                    </div>
                                    <label class="col-sm-2 control-label">ตำแหน่ง :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        ทดสอบ ทดสอบ
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">แผนกผู้ถือครอง :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        QA LAB
                                    </div>
                                    <label class="col-sm-2 control-label">ประเภท :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        เครื่องมือเก่า
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="lbDocCancel" CssClass="btn btn-default" runat="server" data-original-title="Back" data-toggle="tooltip" OnCommand="btnCommand" CommandArgument="0" CommandName="cmdDocCancel"><i class="fa fa-reply" aria-hidden="true"></i> Back</asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:FormView ID="fvDocSelectLocationTest" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <asp:HiddenField ID="hfNodeIDX" runat="server" Value="3" />
                    <asp:HiddenField ID="hfActorIDX" runat="server" Value="2" />
                    <asp:HiddenField ID="hfDocisionIDX" runat="server" Value="1" />
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">เลือกสถานที่สอบเทียบ</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <%--   <label class="col-sm-2 control-label">Sample Code</label>
                                    <div class="col-sm-4 control-label textleft">
                                        CAL0000001R03
                                    </div>--%>


                                    <label class="col-sm-2 control-label">Create Date :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        06/07/2017
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ID No. :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        LAB 1003 E
                                    </div>
                                    <label class="col-sm-2 control-label">ชื่อเครื่องมือ :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        เครื่องชั่ง 1.5 Kg
                                    </div>


                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Serial No. :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        10092883337
                                    </div>
                                    <label class="col-sm-2 control-label">ช่วงการใช้งาน :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        1-300 g
                                    </div>
                                    <%--  <label class="col-sm-2 control-label">Certificate :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        ยังไม่มี cerfiticate
                                    </div>--%>
                                </div>

                                <hr />

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ประเภทการสอบเทียบ :</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="ddlselecttype" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="selectedindexchange" runat="server">
                                            <%-- <asp:ListItem> -- เลือกประเภทการสอบเทียบ --</asp:ListItem>--%>
                                            <asp:ListItem Value="1">สอบเทียบภายใน</asp:ListItem>
                                            <asp:ListItem Value="2">สอบเทียบภายนอก</asp:ListItem>

                                        </asp:DropDownList>
                                    </div>
                                    <%--    </div>
                                <div class="form-group">--%>
                                    <asp:Panel ID="location_select" runat="server" Visible="true">

                                        <label class="col-sm-2 control-label">สถานที่สอบเทียบ :</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddllocation" CssClass="form-control" runat="server">
                                                <%--<asp:ListItem> -- เลือกสถานที่ --</asp:ListItem>--%>
                                                <asp:ListItem Value="1">โรจนะ</asp:ListItem>
                                                <asp:ListItem Value="2">นพวงศ์</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                    </asp:Panel>


                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="lbDocSave11" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="btnInsert" CommandArgument="2,3,0,3,4"></asp:LinkButton>
                                        <asp:LinkButton ID="lbDocCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="Cancel" OnCommand="btnCommand" CommandArgument="0" CommandName="cmdDocCancel"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:FormView ID="fvReciveLab" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>

                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">ฟอร์มการรับงาน</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ID No. :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        LAB 1001 E
                                    </div>
                                    <label class="col-sm-2 control-label">Received Date :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        07/07/2017
                                    </div>

                                </div>

                                <div class="form-group">

                                    <label class="col-sm-2 control-label">ชื่อเครื่องมือ :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        เครื่องชั่ง 1.5 Kg
                                    </div>
                                    <label class="col-sm-2 control-label">ช่วงเวลาใช้งาน :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        1-300 g
                                    </div>

                                </div>
                                <div class="form-group">

                                    <label class="col-sm-2 control-label">Serial No. :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        10092883707
                                    </div>
                                    <label class="col-sm-2 control-label">Certificate :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        ยังไม่มี cerfiticate
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">สถานที่สอบเทียบ :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        โรจนะ
                                    </div>
                                    <label class="col-sm-2 control-label">ประเภทการสอบเทียบ :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        สอบเทียบภายใน
                                    </div>

                                </div>

                                <hr />

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">การรับงาน :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        <asp:DropDownList ID="ddlrecieve" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="selectedindexchange" runat="server">
                                            <%--<asp:ListItem Value="0" Text="-- เลือกสถานะการรับงาน --"></asp:ListItem>--%>
                                            <asp:ListItem Value="1" Text="รับงาน"></asp:ListItem>
                                            <asp:ListItem Value="2" Text="ไม่รับงาน"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>


                                </div>

                                <asp:Panel ID="inputComment" runat="server" Visible="false">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">หมายเหตุ :</label>
                                        <div class="col-sm-3 control-label textleft">
                                            <asp:TextBox ID="txtcomment" CssClass="form-control" TextMode="MultiLine" Rows="3" runat="server">
                                           
                                            </asp:TextBox>
                                        </div>

                                    </div>
                                </asp:Panel>


                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="lbDocSave33" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="btnInsert" CommandArgument="3,4,0,3,5"></asp:LinkButton>
                                        <asp:LinkButton ID="lbDocSavenot" CssClass="btn btn-success" runat="server" Visible="false" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="btnInsert" CommandArgument="3,4,9,2,3"></asp:LinkButton>
                                        <asp:LinkButton ID="lbDocCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="Cancel" OnCommand="btnCommand" CommandArgument="0" CommandName="cmdDocCancel"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>


            <asp:FormView ID="ExternalLabRecive" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>

                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">ฟอร์มการรับงาน</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <%--  <label class="col-sm-2 control-label">Sample Code</label>
                                    <div class="col-sm-4 control-label textleft">
                                        CAL0000001R03
                                    </div>--%>
                                    <label class="col-sm-2 control-label">Received Date :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        07/07/2017
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Certificate :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        ยังไม่มี cerfiticate
                                    </div>
                                    <label class="col-sm-2 control-label">ชื่อเครื่องมือ :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        เครื่องชั่ง 1.5 Kg
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ประเภทเครื่องมือ :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        Digital
                                    </div>
                                    <label class="col-sm-2 control-label">ช่วงเวลาใช้งาน :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        1-300 g
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ID No. :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        LAB 1001 E
                                    </div>
                                    <label class="col-sm-2 control-label">Serial No. :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        10092883337
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">สถานที่ตรวจ :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        ทดสอบ1
                                    </div>
                                    <label class="col-sm-2 control-label">ประเภทการสอบเทียบ :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        สอบเทียบภายนอก
                                    </div>
                                </div>
                                <hr />
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">การรับงาน :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        <asp:DropDownList ID="ddlrecieve_1" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="selectedindexchange" runat="server">
                                            <%--<asp:ListItem Value="0" Text="-- เลือกสถานะการรับงาน --"></asp:ListItem>--%>
                                            <asp:ListItem Value="1" Text="รับงาน"></asp:ListItem>
                                            <asp:ListItem Value="2" Text="ไม่รับงาน"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>


                                </div>

                                <asp:Panel ID="inputComment_1" runat="server" Visible="false">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">หมายเหตุ :</label>
                                        <div class="col-sm-3 control-label textleft">
                                            <asp:TextBox ID="txtcomment_1" CssClass="form-control" TextMode="MultiLine" Rows="3" runat="server">
                                           
                                            </asp:TextBox>
                                        </div>

                                    </div>
                                </asp:Panel>

                                <%--   <div class="form-group">

                                    <div class="col-lg-offset-2 col-sm-10">
                                        <asp:RadioButton ID="locationtest_machine1" Width="25%" runat="server" Checked="true" AutoPostBack="true" OnCheckedChanged="RadioButton_CheckedChanged" Text=" รับงาน " GroupName="machine" ValidationGroup="Saveinsertdetail" />

                                        <asp:RadioButton ID="locationtest_machine2" Width="25%" runat="server" AutoPostBack="true" OnCheckedChanged="RadioButton_CheckedChanged" Text=" ไม่รับงาน" GroupName="machine" ValidationGroup="Saveinsertdetail" />

                                    </div>
                                </div>--%>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="btnSave2" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="btnInsert" CommandArgument="2,3,3,5,9"></asp:LinkButton>
                                        <asp:LinkButton ID="notsave2" CssClass="btn btn-success" Visible="false" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="btnInsert" CommandArgument="5,9,8,2,3"></asp:LinkButton>
                                        <asp:LinkButton ID="lbDocCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="Cancel" OnCommand="btnCommand" CommandArgument="0" CommandName="cmdDocCancel"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:FormView ID="fvDocSelectLocationTest2" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">เลือกสถานที่สอบเทียบ</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <%--   <label class="col-sm-2 control-label">Sample Code</label>
                                    <div class="col-sm-4 control-label textleft">
                                        CAL0000001R03
                                    </div>--%>


                                    <label class="col-sm-2 control-label">Create Date :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        06/07/2017
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ID No. :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        LAB 1003 E
                                    </div>
                                    <label class="col-sm-2 control-label">ชื่อเครื่องมือ :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        เครื่องชั่ง 1.5 Kg
                                    </div>


                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Serial No. :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        10092883337
                                    </div>
                                    <label class="col-sm-2 control-label">ช่วงการใช้งาน :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        1-300 g
                                    </div>
                                    <%--  <label class="col-sm-2 control-label">Certificate :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        ยังไม่มี cerfiticate
                                    </div>--%>
                                </div>

                                <hr />

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ประเภทการสอบเทียบ :</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="ddlselecttype1" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="selectedindexchange" runat="server">
                                            <%--<asp:ListItem> -- เลือกประเภทการสอบเทียบ --</asp:ListItem>--%>
                                            <asp:ListItem Value="2">สอบเทียบภายนอก</asp:ListItem>
                                            <asp:ListItem Value="1">สอบเทียบภายใน</asp:ListItem>


                                        </asp:DropDownList>
                                    </div>

                               <%-- </div>
                                <div class="form-group">--%>
                                    <label class="col-sm-2 control-label">สถานที่สอบเทียบ :</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="ddllocation1" CssClass="form-control" runat="server">
                                            <%--<asp:ListItem> -- เลือกสถานที่ --</asp:ListItem>--%>
                                            <asp:ListItem Value="1">ทดสอบ1</asp:ListItem>
                                            <asp:ListItem Value="2">ทดสอบ2</asp:ListItem>

                                        </asp:DropDownList>
                                    </div>


                                </div>
                                <%--   <div class="form-group">

                                    <div class="col-lg-offset-2 col-sm-10">
                                        <asp:RadioButton ID="locationtest_machine1" Width="25%" runat="server" AutoPostBack="true" OnCheckedChanged="RadioButton_CheckedChanged" Text=" สอบเทียบภายใน " GroupName="machine" ValidationGroup="Saveinsertdetail" />

                                        <asp:RadioButton ID="locationtest_machine2" Width="25%" runat="server" Checked="true" AutoPostBack="true" OnCheckedChanged="RadioButton_CheckedChanged" Text=" สอบเทียบภายนอก" GroupName="machine" ValidationGroup="Saveinsertdetail" />

                                    </div>
                                </div>--%>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="save1" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="btnInsert" CommandArgument="2,3,2,5,9"></asp:LinkButton>
                                        <asp:LinkButton ID="lbDocCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="Cancel" OnCommand="btnCommand" CommandArgument="0" CommandName="cmdDocCancel"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:FormView ID="fvDocinsertResultLabCal" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">กรอกผลการสอบเทียบเครื่องมือ</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <%-- <label class="col-sm-2 control-label">Sample Code</label>
                                    <div class="col-sm-4 control-label textleft">
                                        CAL0000001R03
                                    </div>--%>
                                    <label class="col-sm-2 control-label">Received Date :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        07/07/2017
                                    </div>
                                    <label class="col-sm-2 control-label">ID No. :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        LAB 1001 E
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ชื่อเครื่องมือ :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        เครื่องชั่ง 1.5 Kg
                                    </div>

                                    <label class="col-sm-2 control-label">Serial No. :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        10092883707
                                    </div>
                                </div>
                                <div class="form-group">

                                    <label class="col-sm-2 control-label">ช่วงการใช้งาน :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        1-300 g
                                    </div>
                                    <label class="col-sm-2 control-label">Certificate</label>
                                    <div class="col-sm-4 control-label textleft">
                                        ยังไม่มี cerfiticate
                                    </div>
                                </div>
                                <hr />
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-8">
                                        <label class="control-label">1.ทดสอบความคลาดเคลื่อนของเครื่องชั่ง</label>
                                    </div>
                                </div>
                                <div class="form-group">

                                    <label class="col-sm-2 control-label">ผลการตรวจ :</label>
                                    <div class="col-sm-10">
                                        <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                                            <tbody>

                                                <tr class="info" style="font-size: Small; text-align: center; height: 40px;">
                                                    <th scope="col" rowspan="2" style="width: 10%; text-align: center; font-size: 11px;">พิกัดมวล
                                                        <br />
                                                        (Nominal Value)
                                                        <br />
                                                        (g)</th>
                                                    <th scope="col" colspan="4" style="width: 15%; text-align: center; font-size: 11px;">ค่าที่อ่านได้ของเครื่องชั่ง (UUC) (g)</th>
                                                    <th scope="col" rowspan="2" style="width: 15%; text-align: center; font-size: 11px;">Conventional Mass
                                                        <br />
                                                        (STD)
                                                        <br />
                                                        (g)</th>
                                                    <th scope="col" rowspan="2" style="width: 10%; text-align: center; font-size: 11px;">Error 
                                                        <br />
                                                        (UUC-STD)
                                                        <br />
                                                        (g)</th>
                                                    <th scope="col" rowspan="2" style="width: 10%; text-align: center; font-size: 11px;">ค่าความไม่แน่นอน 
                                                        <br />
                                                        (Uncertainty of)
                                                        <br />
                                                        Standard(g)</th>
                                                    <th scope="col" colspan="2" style="width: 12%; text-align: center; font-size: 11px;">Verification
                                                    </th>
                                                    <th scope="col" rowspan="2" style="width: 15%; text-align: center; font-size: 11px;">/(ผ่าน), x(ไม่ผ่าน) 
                                                    </th>
                                                </tr>
                                                <tr class="info" style="font-size: Small; text-align: center; height: 40px;">
                                                    <th scope="col" style="width: 5%; text-align: center; font-size: 11px;">1</th>
                                                    <th scope="col" style="width: 5%; text-align: center; font-size: 11px;">2</th>
                                                    <th scope="col" style="width: 5%; text-align: center; font-size: 11px;">3</th>
                                                    <th scope="col" style="width: 5%; text-align: center; font-size: 11px;">เฉลี่ย</th>
                                                    <th scope="col" style="width: 5%; text-align: center; font-size: 11px;">Error+U</th>
                                                    <th scope="col" style="width: 5%; text-align: center; font-size: 11px;">Error-U</th>

                                                </tr>
                                                <tr style="font-size: Small; text-align: center; height: 40px;">
                                                    <td>
                                                        <asp:TextBox ID="TextBox34" Width="100px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox25" Width="30px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox26" Width="30px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox27" Width="30px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox28" Width="30px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox29" Width="100px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox30" Width="100px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox31" Width="100px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox32" Width="50px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox33" Width="50px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:RadioButton ID="RadioButton1" runat="server" AutoPostBack="true" Font-Size="10px" Text=" ผ่าน" GroupName="machine12" ValidationGroup="Saveinsertdetail" />
                                                        <asp:RadioButton ID="RadioButton2" runat="server" AutoPostBack="true" Font-Size="10px" Text="ไม่ผ่าน" GroupName="machine12" ValidationGroup="Saveinsertdetail" />
                                                    </td>
                                                </tr>
                                                <tr style="font-size: Small; text-align: center; height: 40px;">
                                                    <td>
                                                        <asp:TextBox ID="TextBox35" Width="100px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox36" Width="30px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox37" Width="30px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox38" Width="30px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox39" Width="30px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox40" Width="100px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox41" Width="100px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox42" Width="100px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox43" Width="50px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox44" Width="50px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:RadioButton ID="pass" runat="server" AutoPostBack="true" Font-Size="10px" Text=" ผ่าน" GroupName="machine13" ValidationGroup="Saveinsertdetail" />
                                                        <asp:RadioButton ID="notpass" runat="server" AutoPostBack="true" Font-Size="10px" Text="ไม่ผ่าน" GroupName="machine13" ValidationGroup="Saveinsertdetail" />
                                                    </td>
                                                </tr>
                                                <tr style="font-size: Small; text-align: center; height: 40px;">
                                                    <td>
                                                        <asp:TextBox ID="TextBox45" Width="100px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox46" Width="30px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox47" Width="30px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox48" Width="30px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox49" Width="30px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox50" Width="100px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox51" Width="100px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox52" Width="100px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox53" Width="50px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox54" Width="50px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:RadioButton ID="RadioButton3" runat="server" AutoPostBack="true" Font-Size="10px" Text=" ผ่าน" GroupName="machine14" ValidationGroup="Saveinsertdetail" />
                                                        <asp:RadioButton ID="RadioButton4" runat="server" AutoPostBack="true" Font-Size="10px" Text="ไม่ผ่าน" GroupName="machine14" ValidationGroup="Saveinsertdetail" />
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>




                                <div class="form-group">
                                    <label class="col-sm-2 control-label">สภาพเครื่องมือ :</label>
                                    <div class="col-sm-5">
                                        <asp:DropDownList ID="ddlresult" CssClass="form-control" runat="server" OnSelectedIndexChanged="selectedindexchange">
                                            <asp:ListItem Text="ปกติ" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="ไม่ปกติ" Value="2"></asp:ListItem>
                                        </asp:DropDownList>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">วิธีการตรวจสอบ :</label>
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="TextBox14" runat="server" CssClass="form-control" Text='ตรวจสอบด้วยวิธี...' Enabled="true" />
                                    </div>

                                </div>
                                <div class="form-group">
                                    <%-- <label class="col-sm-2 control-label">ผลการตรวจสอบ :</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="TextBox2" runat="server" TextMode="MultiLine" Rows="4" CssClass="form-control" Text='ผ่านการตรวจสอบ ปกติ' Enabled="true" />
                                    </div>--%>

                                    <label class="col-sm-2 control-label">ความคิดเห็นเพิ่มเติม :</label>
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="TextBox10" runat="server" TextMode="MultiLine" Rows="3" CssClass="form-control" Text='ผ่านการตรวจสอบ ปกตินะคะ' Enabled="true" />
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">แนบไฟล์ certificate :</label>
                                    <div class="col-sm-10">
                                        <asp:FileUpload ID="FileUpload1" runat="server" />
                                    </div>


                                </div>


                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="lbDocSave" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="btnInsert" CommandArgument="3,5,1,4,6"></asp:LinkButton>
                                        <asp:LinkButton ID="lbDocCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="Cancel" OnCommand="btnCommand" CommandArgument="0" CommandName="cmdDocCancel"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:FormView ID="fvDocinsertResultLabCalMachineOld" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">กรอกผลการสอบเทียบเครื่องมือ</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <%--   <label class="col-sm-2 control-label">Sample Code</label>
                                    <div class="col-sm-4 control-label textleft">
                                        CAL0000001R03
                                    </div>--%>
                                    <label class="col-sm-2 control-label">Received Date :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        07/07/2017
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Certificate :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        ยังไม่มี cerfiticate
                                    </div>
                                    <label class="col-sm-2 control-label">ชื่อเครื่องมือ :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        เครื่องชั่ง 1.5 Kg
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ID No. :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        LAB 1001 E
                                    </div>
                                    <label class="col-sm-2 control-label">Serial No. :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        10092883337
                                    </div>
                                </div>
                                <div class="form-group">

                                    <label class="col-sm-2 control-label">ประเภทเครื่องมือ :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        Digital
                                    </div>
                                    <label class="col-sm-2 control-label">ช่วงการใช้งาน :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        1-300 g
                                    </div>
                                </div>
                                <hr />

                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-8">
                                        <label class="control-label">1.ทดสอบความคลาดเคลื่อนของเครื่องชั่ง</label>
                                    </div>
                                </div>
                                <div class="form-group">

                                    <label class="col-sm-2 control-label">ผลการตรวจ :</label>
                                    <div class="col-sm-10">
                                        <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                                            <tbody>

                                                <tr class="info" style="font-size: Small; text-align: center; height: 40px;">
                                                    <th scope="col" rowspan="2" style="width: 10%; text-align: center; font-size: 11px;">พิกัดมวล
                                                        <br />
                                                        (Nominal Value)
                                                        <br />
                                                        (g)</th>
                                                    <th scope="col" colspan="4" style="width: 15%; text-align: center; font-size: 11px;">ค่าที่อ่านได้ของเครื่องชั่ง (UUC) (g)</th>
                                                    <th scope="col" rowspan="2" style="width: 15%; text-align: center; font-size: 11px;">Conventional Mass
                                                        <br />
                                                        (STD)
                                                        <br />
                                                        (g)</th>
                                                    <th scope="col" rowspan="2" style="width: 10%; text-align: center; font-size: 11px;">Error 
                                                        <br />
                                                        (UUC-STD)
                                                        <br />
                                                        (g)</th>
                                                    <th scope="col" rowspan="2" style="width: 10%; text-align: center; font-size: 11px;">ค่าความไม่แน่นอน 
                                                        <br />
                                                        (Uncertainty of)
                                                        <br />
                                                        Standard(g)</th>
                                                    <th scope="col" colspan="2" style="width: 12%; text-align: center; font-size: 11px;">Verification
                                                    </th>
                                                    <th scope="col" rowspan="2" style="width: 15%; text-align: center; font-size: 11px;">/(ผ่าน), x(ไม่ผ่าน) 
                                                    </th>
                                                </tr>
                                                <tr class="info" style="font-size: Small; text-align: center; height: 40px;">
                                                    <th scope="col" style="width: 5%; text-align: center; font-size: 11px;">1</th>
                                                    <th scope="col" style="width: 5%; text-align: center; font-size: 11px;">2</th>
                                                    <th scope="col" style="width: 5%; text-align: center; font-size: 11px;">3</th>
                                                    <th scope="col" style="width: 5%; text-align: center; font-size: 11px;">เฉลี่ย</th>
                                                    <th scope="col" style="width: 5%; text-align: center; font-size: 11px;">Error+U</th>
                                                    <th scope="col" style="width: 5%; text-align: center; font-size: 11px;">Error-U</th>

                                                </tr>
                                                <tr style="font-size: Small; text-align: center; height: 40px;">
                                                    <td>
                                                        <asp:TextBox ID="TextBox34" Width="100px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox25" Width="30px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox26" Width="30px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox27" Width="30px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox28" Width="30px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox29" Width="100px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox30" Width="100px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox31" Width="100px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox32" Width="50px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox33" Width="50px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:RadioButton ID="RadioButton1" runat="server" AutoPostBack="true" Font-Size="10px" Text=" ผ่าน" GroupName="machine19" ValidationGroup="Saveinsertdetail" />
                                                        <asp:RadioButton ID="RadioButton2" runat="server" AutoPostBack="true" Font-Size="10px" Text="ไม่ผ่าน" GroupName="machine19" ValidationGroup="Saveinsertdetail" />
                                                    </td>
                                                </tr>
                                                <tr style="font-size: Small; text-align: center; height: 40px;">
                                                    <td>
                                                        <asp:TextBox ID="TextBox35" Width="100px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox36" Width="30px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox37" Width="30px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox38" Width="30px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox39" Width="30px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox40" Width="100px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox41" Width="100px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox42" Width="100px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox43" Width="50px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox44" Width="50px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:RadioButton ID="pass" runat="server" AutoPostBack="true" Font-Size="10px" Text=" ผ่าน" GroupName="machine10" ValidationGroup="Saveinsertdetail" />
                                                        <asp:RadioButton ID="notpass" runat="server" AutoPostBack="true" Font-Size="10px" Text="ไม่ผ่าน" GroupName="machine10" ValidationGroup="Saveinsertdetail" />
                                                    </td>
                                                </tr>
                                                <tr style="font-size: Small; text-align: center; height: 40px;">
                                                    <td>
                                                        <asp:TextBox ID="TextBox45" Width="100px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox46" Width="30px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox47" Width="30px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox48" Width="30px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox49" Width="30px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox50" Width="100px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox51" Width="100px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox52" Width="100px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox53" Width="50px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox54" Width="50px" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:RadioButton ID="RadioButton3" runat="server" AutoPostBack="true" Font-Size="10px" Text=" ผ่าน" GroupName="machine15" ValidationGroup="Saveinsertdetail" />
                                                        <asp:RadioButton ID="RadioButton4" runat="server" AutoPostBack="true" Font-Size="10px" Text="ไม่ผ่าน" GroupName="machine15" ValidationGroup="Saveinsertdetail" />
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-sm-2 control-label">สภาพเครื่องมือ :</label>
                                    <div class="col-sm-5">
                                        <asp:DropDownList ID="ddlresult" CssClass="form-control" runat="server" OnSelectedIndexChanged="selectedindexchange">
                                            <asp:ListItem Text="ปกติ" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="ไม่ปกติ" Value="2"></asp:ListItem>
                                        </asp:DropDownList>
                                        <%-- <asp:TextBox ID="txtmachine" runat="server" CssClass="form-control" Text='ปกติ' Enabled="true" />--%>
                                    </div>
                                    </div>
                                    <div class="form-group">
                                    <%--   <div class="col-sm-1"></div>--%>
                                    <label class="col-sm-2 control-label">วิธีการตรวจสอบ :</label>
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="TextBox14" runat="server" CssClass="form-control" Text='ตรวจสอบด้วยวิธี...' Enabled="true" />
                                    </div>
                                    <%--  <div class="col-sm-1"></div>--%>
                                </div>
                                <div class="form-group">
                                    <%--  <label class="col-sm-2 control-label">ผลการตรวจสอบ :</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="TextBox2" runat="server" TextMode="MultiLine" Rows="4" CssClass="form-control" Text='ผ่านการตรวจสอบ ปกติ' Enabled="true" />
                                    </div>--%>
                                    <%--   <div class="col-sm-1"></div>--%>
                                    <label class="col-sm-2 control-label">ความคิดเห็นเพิ่มเติม :</label>
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="TextBox10" runat="server" TextMode="MultiLine" Rows="3" CssClass="form-control" Text='ผ่านการตรวจสอบ ปกตินะคะ' Enabled="true" />
                                    </div>
                                    <%--  <div class="col-sm-1"></div>--%>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">แนบไฟล์ certificate :</label>
                                    <div class="col-sm-10">
                                        <asp:FileUpload ID="FileUpload1" runat="server" />
                                        <p style="color: red;">** แนบเอกสารอ้างอิงการตัดชำรุดเครื่องมือ ขนาดไฟล์ ไม่เกิน 5 MB</p>
                                    </div>


                                </div>




                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="lbDocSave" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="btnInsert" CommandArgument="2,10,2,1,7"></asp:LinkButton>
                                        <asp:LinkButton ID="lbDocCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="Cancel" OnCommand="btnCommand" CommandArgument="0" CommandName="cmdDocCancel"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:FormView ID="fvDetailsResultLabCal" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">ผลการสอบเทียบเครื่องมือ</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <%--  <label class="col-sm-2 control-label">Sample Code</label>
                                    <div class="col-sm-4 control-label textleft">
                                        CAL0000001R03
                                    </div>--%>
                                    <label class="col-sm-2 control-label">Received Date :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        07/07/2017
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Certificate :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        ยังไม่มี cerfiticate
                                    </div>
                                    <label class="col-sm-2 control-label">ชื่อเครื่องมือ :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        เครื่องชั่ง 1.5 Kg
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ID No. :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        LAB 1001 E
                                    </div>
                                    <label class="col-sm-2 control-label">ประเภทเครื่องมือ :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        Digital
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ช่วงเวลาใช้งาน :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        1-300 g
                                    </div>
                                    <label class="col-sm-2 control-label">Serial No. :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        10092883707
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ประเภทการสอบเทียบ :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        สอบเทียบภายใน
                                    </div>
                                    <label class="col-sm-2 control-label">สถานที่สอบเทียบ :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        โรจนะ
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-8">
                                        <label class="control-label">1.ทดสอบความคลาดเคลื่อนของเครื่องชั่ง</label>
                                    </div>
                                </div>
                                <div class="form-group">

                                    <label class="col-sm-2 control-label">ผลการตรวจ :</label>
                                    <div class="col-sm-10">
                                        <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                                            <tbody>

                                                <tr class="info" style="font-size: Small; text-align: center; height: 40px;">
                                                    <th scope="col" rowspan="2" style="width: 10%; text-align: center; font-size: 11px;">พิกัดมวล
                                                        <br />
                                                        (Nominal Value)
                                                        <br />
                                                        (g)</th>
                                                    <th scope="col" colspan="4" style="width: 15%; text-align: center; font-size: 11px;">ค่าที่อ่านได้ของเครื่องชั่ง (UUC) (g)</th>
                                                    <th scope="col" rowspan="2" style="width: 15%; text-align: center; font-size: 11px;">Conventional Mass
                                                        <br />
                                                        (STD)
                                                        <br />
                                                        (g)</th>
                                                    <th scope="col" rowspan="2" style="width: 10%; text-align: center; font-size: 11px;">Error 
                                                        <br />
                                                        (UUC-STD)
                                                        <br />
                                                        (g)</th>
                                                    <th scope="col" rowspan="2" style="width: 10%; text-align: center; font-size: 11px;">ค่าความไม่แน่นอน 
                                                        <br />
                                                        (Uncertainty of)
                                                        <br />
                                                        Standard(g)</th>
                                                    <th scope="col" colspan="2" style="width: 15%; text-align: center; font-size: 11px;">Verification
                                                    </th>
                                                    <th scope="col" rowspan="2" style="width: 13%; text-align: center; font-size: 11px;">/(ผ่าน), x(ไม่ผ่าน) 
                                                    </th>
                                                </tr>
                                                <tr class="info" style="font-size: Small; text-align: center; height: 40px;">
                                                    <th scope="col" style="width: 5%; text-align: center; font-size: 11px;">1</th>
                                                    <th scope="col" style="width: 5%; text-align: center; font-size: 11px;">2</th>
                                                    <th scope="col" style="width: 5%; text-align: center; font-size: 11px;">3</th>
                                                    <th scope="col" style="width: 5%; text-align: center; font-size: 11px;">เฉลี่ย</th>
                                                    <th scope="col" style="width: 5%; text-align: center; font-size: 11px;">Error+U</th>
                                                    <th scope="col" style="width: 6%; text-align: center; font-size: 11px;">Error-U</th>

                                                </tr>
                                                <tr style="font-size: Small; text-align: center; height: 40px;">
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>ผ่าน
                                                    </td>
                                                </tr>
                                                <tr style="font-size: Small; text-align: center; height: 40px;">
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>ผ่าน
                                                    </td>
                                                </tr>
                                                <tr style="font-size: Small; text-align: center; height: 40px;">
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>ผ่าน
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">สภาพเครื่องมือ :</label>
                                    <div class="col-sm-10 control-label textleft">
                                        ปกติ
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">วิธีการตรวจสอบ :</label>
                                    <div class="col-sm-10 control-label textleft">
                                        ตรวจสอบด้วยวิธี XXXXXXXXXXX
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ความคิดเห็นเพิ่มเติม :</label>
                                    <div class="col-sm-10 control-label textleft">
                                        -
                                    </div>

                                </div>
                                <hr />
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">หมายเหตุ/ความคิดเห็น :</label>
                                    <div class="col-sm-10 control-label textleft">
                                        <asp:TextBox ID="TextBox1s1" runat="server" TextMode="MultiLine" Rows="3" CssClass="form-control" Enabled="true" />
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">การอนุมัติ :</label>
                                    <div class="col-sm-5 control-label textleft">
                                        <asp:DropDownList ID="ddlapprove" runat="server" AutoPostBack="true" OnSelectedIndexChanged="selectedindexchange" CssClass="form-control" Enabled="true">
                                            <%--<asp:ListItem>-- เลือกสถานะการอนุมัติ --</asp:ListItem>--%>
                                            <asp:ListItem Value="1">อนุมัติ</asp:ListItem>
                                            <asp:ListItem Value="2">ไม่อนุมัติ</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="btnsave_1" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="btnInsert" CommandArgument="4,6,1,2,7"></asp:LinkButton>
                                        <asp:LinkButton ID="btnnotsave" CssClass="btn btn-success" Visible="false" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="btnInsert" CommandArgument="4,6,9,3,5"></asp:LinkButton>
                                        <asp:LinkButton ID="lbDocCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="Cancel" OnCommand="btnCommand" CommandArgument="0" CommandName="cmdDocCancel"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:FormView ID="fvDetails3" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">รายละเอียดเครื่องมือ</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <%--  <label class="col-sm-2 control-label">Sample Code</label>
                                    <div class="col-sm-4 control-label textleft">
                                        CAL0000001R01
                                    </div>--%>
                                    <label class="col-sm-2 control-label">Received Date :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        07/07/2017
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ID No. :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        LAB 1001 E
                                    </div>

                                    <label class="col-sm-2 control-label">ชื่อเครื่องมือ :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        เครื่องชั่ง 1.5 Kg
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ช่วงการใช้งาน :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        1-300 g
                                    </div>
                                    <label class="col-sm-2 control-label">Serial No. :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        10092883337
                                    </div>
                                </div>
                                <hr />
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">แนบไฟล์ :</label>
                                    <asp:FileUpload ID="FileUpload3" runat="server" />
                                    <p style="color: red;">** แนบเอกสารอ้างอิงการตัดชำรุดเครื่องมือ ขนาดไฟล์ ไม่เกิน 5 MB</p>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="lbDocSave" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="btnInsert" CommandArgument="2,2,3,2,11"></asp:LinkButton>
                                        <asp:LinkButton ID="lbDocCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="Cancel" OnCommand="btnCommand" CommandArgument="0" CommandName="cmdDocCancel"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:FormView ID="fvDetails4" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>

                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">ผลการสอบเทียบเครื่องมือ</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <%-- <label class="col-sm-2 control-label">Sample Code</label>
                                    <div class="col-sm-4 control-label textleft">
                                        CAL0000001R03
                                    </div>--%>
                                    <label class="col-sm-2 control-label">Received Date :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        07/07/2017
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Certificate :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        cerfiticate.pdf
                                    </div>
                                    <label class="col-sm-2 control-label">ชื่อเครื่องมือ :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        Machine03
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ID No. :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        ID0003
                                    </div>
                                    <label class="col-sm-2 control-label">Serial No. :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        Serial0003
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">สภาพเครื่องมือ :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        ปกติ
                                    </div>
                                    <label class="col-sm-2 control-label">วิธีการตรวจสอบ :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        ตรวจสอบด้วยวิธี XXXXXXXXXXX
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ผลการตรวจสอบ :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        ผ่านการตรวจสอบ ปกติ
                                    </div>
                                    <label class="col-sm-2 control-label">สถานที่สอบเทียบ :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        RJN
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </InsertItemTemplate>
            </asp:FormView>

            <asp:FormView ID="fvDetailsCalComplete" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>

                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">ผลการสอบเทียบเครื่องมือ</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <%--   <label class="col-sm-2 control-label">Sample Code</label>
                                    <div class="col-sm-4 control-label textleft">
                                        CAL0000001R03
                                    </div>--%>
                                    <label class="col-sm-2 control-label">Received Date :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        07/07/2017
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Certificate :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        cerfiticate.pdf
                                    </div>
                                    <label class="col-sm-2 control-label">ชื่อเครื่องมือ :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        Machine03
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ID No. :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        ID0003
                                    </div>
                                    <label class="col-sm-2 control-label">Serial No. :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        Serial0003
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">สภาพเครื่องมือ :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        ปกติ
                                    </div>
                                    <label class="col-sm-2 control-label">วิธีการตรวจสอบ :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        ตรวจสอบด้วยวิธี XXXXXXXXXXX
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ผลการตรวจสอบ :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        ผ่านการตรวจสอบ ปกติ
                                    </div>
                                    <label class="col-sm-2 control-label">สถานที่สอบเทียบ :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        External Lab
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </InsertItemTemplate>
            </asp:FormView>

            <!--sample list old-->
            <div class="panel panel-success" id="samplelistold" runat="server">
                <div class="panel-heading">
                    <h3 class="panel-title">รายการเครื่องมือที่สอบเทียบ</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <a href='<%=ResolveUrl("~/print-sample-cal") %>' id="btn" class="btn btn-sm btn-default" target="_blank"><i class="fa fa-print" aria-hidden="true"></i>Print</a>
                    </div>
                    <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                        <tbody>
                            <tr class="info" style="font-size: Small; height: 40px;">
                                <th scope="col" style="width: 2%;">No.</th>
                                <th scope="col" style="width: 13%;">Machine Name</th>
                                <th scope="col" style="width: 10%;">ID No.</th>
                                <th scope="col" style="width: 10%;">Serial No.</th>
                                <th scope="col" style="width: 10%;">Last Cal Date</th>
                                <th scope="col" style="width: 10%;">Received Date</th>
                                <th scope="col" style="width: 10%;">Tested Date</th>
                                <th scope="col" style="width: 10%;">Lab</th>
                                <th scope="col" style="width: 10%;">Status</th>
                                <th scope="col" style="width: 5%;">Action</th>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>1.</td>
                                <td>เครื่องชั่ง 1.5 Kg</td>
                                <td>LAB 1001 E</td>
                                <td>10092883337</td>
                                <td>06/05/2017</td>
                                <td>-</td>
                                <td>-</td>
                                <%--<td>NPW</td>--%>
                                <td>-</td>

                                <td>Waiting QA </td>
                                <td>
                                    <asp:LinkButton ID="LinkButton27" CssClass="btn-sm btn-default" runat="server" data-original-title="Sample Result" data-toggle="tooltip" OnCommand="btnCommand" CommandName="btnInsert" CommandArgument="2,3,1,5,9"><i class="fa fa-sign-in" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!--sample list-->

            <!--sample list old External Lab-->
            <div class="panel panel-success" id="samplelistoldExternalLab" runat="server" visible="false">
                <div class="panel-heading">
                    <h3 class="panel-title">รายการเครื่องมือที่สอบเทียบ</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <a href='<%=ResolveUrl("~/print-sample-cal") %>' id="btn" class="btn btn-sm btn-default" target="_blank"><i class="fa fa-print" aria-hidden="true"></i>Print</a>
                    </div>
                    <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                        <tbody>
                            <tr class="info" style="font-size: Small; height: 40px;">
                                <th scope="col" style="width: 2%;">No.</th>
                                <th scope="col" style="width: 13%;">Machine Name</th>
                                <th scope="col" style="width: 10%;">ID No.</th>
                                <th scope="col" style="width: 10%;">Serial No.</th>
                                <th scope="col" style="width: 10%;">Last Cal Date</th>
                                <th scope="col" style="width: 10%;">Received Date</th>
                                <th scope="col" style="width: 10%;">Tested Date</th>
                                <th scope="col" style="width: 10%;">Lab</th>
                                <th scope="col" style="width: 10%;">Status</th>
                                <th scope="col" style="width: 5%;">Action</th>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>1.</td>
                                <td>เครื่องชั่ง 1.5 Kg</td>
                                <td>LAB 1001 E</td>
                                <td>10092883337</td>
                                <td>06/06/2017</td>
                                <td>07/07/2017</td>
                                <td>-</td>
                                <%--<td>NPW</td>--%>
                                <td>-</td>

                                <td>Waiting External Lab Recive</td>
                                <td>
                                    <asp:LinkButton ID="LinkButton28" CssClass="btn-sm btn-default" runat="server" data-original-title="Sample Result" data-toggle="tooltip" OnCommand="btnCommand" CommandName="btnInsert" CommandArgument="2,3,0,5,9"><i class="fa fa-sign-in" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!--sample list-->

            <!--sample list old External Lab Recive-->
            <div class="panel panel-success" id="samplelistoldExternalLabRecive" runat="server" visible="false">
                <div class="panel-heading">
                    <h3 class="panel-title">รายการเครื่องมือที่สอบเทียบ</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <a href='<%=ResolveUrl("~/print-sample-cal") %>' id="btn" class="btn btn-sm btn-default" target="_blank"><i class="fa fa-print" aria-hidden="true"></i>Print</a>
                    </div>
                    <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                        <tbody>
                            <tr class="info" style="font-size: Small; height: 40px;">
                                <th scope="col" style="width: 2%;">No.</th>
                                <th scope="col" style="width: 13%;">Machine Name</th>
                                <th scope="col" style="width: 10%;">ID No.</th>
                                <th scope="col" style="width: 10%;">Serial No.</th>
                                <th scope="col" style="width: 10%;">Last Cal Date</th>
                                <th scope="col" style="width: 10%;">Received Date</th>
                                <th scope="col" style="width: 10%;">Tested Date</th>
                                <th scope="col" style="width: 10%;">Lab</th>
                                <th scope="col" style="width: 10%;">Status</th>
                                <th scope="col" style="width: 5%;">Action</th>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>1.</td>
                                <td>เครื่องชั่ง 1.5 Kg</td>
                                <td>LAB 1001 E</td>
                                <td>10092883337</td>
                                <td>06/06/2017</td>
                                <td>07/07/2017</td>
                                <td>-</td>
                                <%--<td>NPW</td>--%>
                                <td>External Lab</td>

                                <td>Ongoing</td>
                                <td>
                                    <asp:LinkButton ID="LinkButton36" CssClass="btn-sm btn-default" runat="server" data-original-title="Sample Result" data-toggle="tooltip" OnCommand="btnCommand" CommandName="btnInsert" CommandArgument="5,9,1,2,10"><i class="fa fa-pencil" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
            <!--sample list-->


            <!--sample list old External Lab-->
            <div class="panel panel-success" id="samplelistoldExternalLabComplete" runat="server" visible="false">
                <div class="panel-heading">
                    <h3 class="panel-title">รายการเครื่องมือที่สอบเทียบ</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <a href='<%=ResolveUrl("~/print-sample-cal") %>' id="btn" class="btn btn-sm btn-default" target="_blank"><i class="fa fa-print" aria-hidden="true"></i>Print</a>
                    </div>
                    <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                        <tbody>
                            <tr class="info" style="font-size: Small; height: 40px;">
                                <th scope="col" style="width: 2%;">No.</th>
                                <th scope="col" style="width: 13%;">Machine Name</th>
                                <th scope="col" style="width: 10%;">ID No.</th>
                                <th scope="col" style="width: 10%;">Serial No.</th>
                                <th scope="col" style="width: 10%;">Last Cal Date</th>
                                <th scope="col" style="width: 10%;">Received Date</th>
                                <th scope="col" style="width: 10%;">Tested Date</th>
                                <th scope="col" style="width: 10%;">Lab</th>
                                <th scope="col" style="width: 10%;">Status</th>
                                <th scope="col" style="width: 5%;">Action</th>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>1</td>
                                <td>เครื่องชั่ง 1.5 Kg</td>
                                <td>LAB 1001 E</td>
                                <td>10092883337</td>
                                <td>06/06/2017</td>
                                <td>07/07/2017</td>
                                <td>07/07/2017</td>
                                <%--<td>NPW</td>--%>
                                <td>External Lab</td>

                                <td>Completed</td>
                                <td>
                                    <asp:LinkButton ID="LinkButton29" CssClass="btn-sm btn-default" runat="server" data-original-title="Sample Result" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdViewDoc1"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="form-group">
                        <div class="pull-right">
                            <div class="col-sm-12">

                                <asp:LinkButton ID="btnsaveold" CssClass="btn btn-success" Visible="false" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="btnInsert" CommandArgument="2,7,4,1,8"></asp:LinkButton>
                                <asp:LinkButton ID="btncancelold" CssClass="btn btn-danger" Visible="false" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="Cancel" OnCommand="btnCommand" CommandArgument="0" CommandName="cmdDocCancel"></asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--sample list-->

            <!--sample Result Cal Machine new-->
            <div class="panel panel-success" id="resultLabCalMachinenew" runat="server" visible="false">
                <div class="panel-heading">
                    <h3 class="panel-title">รายการเครื่องมือที่สอบเทียบ</h3>
                </div>
                <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                        <tbody>
                            <tr class="info" style="font-size: Small; height: 40px;">
                                <th scope="col" style="width: 2%;">No.</th>
                                <th scope="col" style="width: 18%;">Machine Name</th>
                                <th scope="col" style="width: 10%;">ID No.</th>
                                <th scope="col" style="width: 10%;">Serial No.</th>
                                <th scope="col" style="width: 10%;">Received Date</th>
                                <th scope="col" style="width: 10%;">Tested Date</th>
                                <th scope="col" style="width: 8%;">Lab</th>
                                <th scope="col" style="width: 5%;">certificate</th>
                                <th scope="col" style="width: 15%;">Status</th>
                                <th scope="col" style="width: 5%;">Action</th>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>1.</td>
                                <td>Machine01</td>
                                <td>ID0001</td>
                                <td>Serial0001</td>
                                <td>-</td>
                                <td>07/07/2017</td>
                                <%--<td>NPW</td>--%>
                                <td>-</td>
                                <td>
                                    <i class="fa fa-check" aria-hidden="true"></i>

                                </td>
                                <td>Completed</td>
                                <td>
                                    <asp:LinkButton ID="LinkButton24" CssClass="btn-sm btn-default" runat="server" data-original-title="Sample Result" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdViewDoc" CommandArgument="7"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>2.</td>
                                <td>Machine02</td>
                                <td>ID0002</td>
                                <td>Serial0002</td>
                                <td>07/07/2017</td>
                                <td>10/07/2017</td>
                                <%-- <td>External Lab</td>--%>

                                <td>NPW</td>
                                <td>
                                    <i class="fa fa-check" aria-hidden="true"></i>

                                </td>
                                <td>Completed</td>
                                <td>
                                    <asp:LinkButton ID="LinkButton25" CssClass="btn-sm btn-default" runat="server" data-original-title="View" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdViewDoc" CommandArgument="7"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>3.</td>
                                <td>Machine03</td>
                                <td>ID0003</td>
                                <td>Serial0003</td>

                                <td>-</td>
                                <td>10/07/2017</td>
                                <td>RJN</td>
                                <td>
                                    <i class="fa fa-check" aria-hidden="true"></i>

                                </td>
                                <td>Completed</td>
                                <td>
                                    <asp:LinkButton ID="LinkButton26" CssClass="btn-sm btn-default" runat="server" data-original-title="Receive" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdViewDoc" CommandArgument="7"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!--sample Waiting Supervisor-->


            <!-- document log detail preview -->
            <div class="panel panel-default" id="logCalExtanalMachineOld" runat="server" visible="false">
                <div class="panel-heading">
                    <h3 class="panel-title">รายละเอียดการดำเนินการ</h3>
                </div>
                <div class="panel-body">
                    <div class="form-horizontal" role="form">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">ดำเนินการโดย</label>
                            <div class="col-sm-4 control-label textleft">
                                ผู้ขอบริการสอบเทียบเครื่องมือ
                            </div>
                            <label class="col-sm-2 control-label">ผลการดำเนินการ</label>
                            <div class="col-sm-4 control-label textleft">
                                ดำเนินการ
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">โดย</label>
                            <div class="col-sm-4 control-label textleft">
                                ทดสอบ ทดสอบ
                            </div>
                            <label class="col-sm-2 control-label">วันที่</label>
                            <div class="col-sm-4 control-label textleft">
                                07/07/2017
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Comment</label>
                            <div class="col-sm-4 control-label textleft">
                                -
                            </div>
                            <%-- <label class="col-sm-2 control-label">ตำแหน่ง</label>
                                    <div class="col-sm-4 control-label textleft">
                                        ทดสอบ ทดสอบ
                                    </div>--%>
                        </div>
                        <hr />

                        <div class="form-group">
                            <label class="col-sm-2 control-label">ดำเนินการโดย</label>
                            <div class="col-sm-4 control-label textleft">
                                ผู้รับบริการ เจ้าหน้าที่หน่วยงานสอบเทียบ (นอก)
                            </div>
                            <label class="col-sm-2 control-label">ผลการดำเนินการ</label>
                            <div class="col-sm-4 control-label textleft">
                                สอบเทียบเครื่องมือ
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">โดย</label>
                            <div class="col-sm-4 control-label textleft">
                                จนท. ทดสอบ ทดสอบ (Lab นอก)
                            </div>
                            <label class="col-sm-2 control-label">วันที่</label>
                            <div class="col-sm-4 control-label textleft">
                                10/07/2017
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Comment</label>
                            <div class="col-sm-4 control-label textleft">
                                -
                            </div>

                        </div>
                        <hr />

                        <div class="form-group">
                            <label class="col-sm-2 control-label">ดำเนินการโดย</label>
                            <div class="col-sm-4 control-label textleft">
                                ผู้ทำการบันทึกผลสอบเทียบ
                            </div>
                            <label class="col-sm-2 control-label">ผลการดำเนินการ</label>
                            <div class="col-sm-4 control-label textleft">
                                บันทึกผลตรวจสอบเทียบ
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">โดย</label>
                            <div class="col-sm-4 control-label textleft">
                                ศิริรักษ์ บุญเรืองจักร
                            </div>
                            <label class="col-sm-2 control-label">วันที่</label>
                            <div class="col-sm-4 control-label textleft">
                                10/07/2017
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Comment</label>
                            <div class="col-sm-4 control-label textleft">
                                -
                            </div>

                        </div>
                        <hr />
                    </div>

                </div>
            </div>
            <!--document log detail preview-->


            <!-- document log detail preview -->
            <div class="panel panel-default" id="logListPreview" runat="server">
                <div class="panel-heading">
                    <h3 class="panel-title">รายละเอียดการดำเนินการ</h3>
                </div>
                <div class="panel-body">
                    <div class="form-horizontal" role="form">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">ดำเนินการโดย</label>
                            <div class="col-sm-4 control-label textleft">
                                ผู้ขอบริการสอบเทียบเครื่องมือ
                            </div>
                            <label class="col-sm-2 control-label">ผลการดำเนินการ</label>
                            <div class="col-sm-4 control-label textleft">
                                ดำเนินการ
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">โดย</label>
                            <div class="col-sm-4 control-label textleft">
                                ทดสอบ ทดสอบ
                            </div>
                            <label class="col-sm-2 control-label">วันที่</label>
                            <div class="col-sm-4 control-label textleft">
                                07/07/2017
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Comment</label>
                            <div class="col-sm-4 control-label textleft">
                                -
                            </div>
                            <%-- <label class="col-sm-2 control-label">ตำแหน่ง</label>
                                    <div class="col-sm-4 control-label textleft">
                                        ทดสอบ ทดสอบ
                                    </div>--%>
                        </div>
                        <hr />

                        <div class="form-group">
                            <label class="col-sm-2 control-label">ดำเนินการโดย</label>
                            <div class="col-sm-4 control-label textleft">
                                ผู้รับบริการ เจ้าหน้าที่หน่วยงานสอบเทียบ
                            </div>
                            <label class="col-sm-2 control-label">ผลการดำเนินการ</label>
                            <div class="col-sm-4 control-label textleft">
                                สอบเทียบเครื่องมือ
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">โดย</label>
                            <div class="col-sm-4 control-label textleft">
                                ศิริรักษ์ บุญเรืองจักร
                            </div>
                            <label class="col-sm-2 control-label">วันที่</label>
                            <div class="col-sm-4 control-label textleft">
                                10/07/2017
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Comment</label>
                            <div class="col-sm-4 control-label textleft">
                                -
                            </div>
                            <%-- <label class="col-sm-2 control-label">ตำแหน่ง</label>
                                    <div class="col-sm-4 control-label textleft">
                                        ทดสอบ ทดสอบ
                                    </div>--%>
                        </div>
                        <hr />

                        <div class="form-group">
                            <label class="col-sm-2 control-label">ดำเนินการโดย</label>
                            <div class="col-sm-4 control-label textleft">
                                Supervisor
                            </div>
                            <label class="col-sm-2 control-label">ผลการดำเนินการ</label>
                            <div class="col-sm-4 control-label textleft">
                                อนุมัติรายการสอบเทียบเครื่องมือ
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">โดย</label>
                            <div class="col-sm-4 control-label textleft">
                                Supervisor test
                            </div>
                            <label class="col-sm-2 control-label">วันที่</label>
                            <div class="col-sm-4 control-label textleft">
                                10/07/2017
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Comment</label>
                            <div class="col-sm-4 control-label textleft">
                                -
                            </div>
                            <%-- <label class="col-sm-2 control-label">ตำแหน่ง</label>
                                    <div class="col-sm-4 control-label textleft">
                                        ทดสอบ ทดสอบ
                                    </div>--%>
                        </div>
                        <hr />
                    </div>

                </div>
            </div>
            <!--document log detail preview-->

            <!--sample list-->
            <div class="panel panel-success" id="sampleList" runat="server">
                <div class="panel-heading">
                    <h3 class="panel-title">รายการเครื่องมือที่สอบเทียบ</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <a href='<%=ResolveUrl("~/print-sample-cal") %>' id="btn" class="btn btn-sm btn-default" target="_blank"><i class="fa fa-print" aria-hidden="true"></i>Print</a>
                    </div>
                    <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                        <tbody>
                            <tr class="info" style="font-size: Small; height: 40px;">
                                <th scope="col" style="width: 2%;">No.</th>
                                <th scope="col" style="width: 18%;">Machine Name</th>
                                <th scope="col" style="width: 10%;">ID No.</th>
                                <th scope="col" style="width: 10%;">Serial No.</th>
                                <th scope="col" style="width: 10%;">Received Date</th>
                                <th scope="col" style="width: 10%;">Tested Date</th>
                                <th scope="col" style="width: 8%;">Lab</th>
                                <th scope="col" style="width: 5%;">certificate</th>
                                <th scope="col" style="width: 15%;">Status</th>
                                <th scope="col" style="width: 5%;">Action</th>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>1.</td>
                                <td>เครื่องชั่ง 1.5 Kg</td>
                                <td>LAB 1001 E</td>
                                <td>10092883337</td>
                                <td>-</td>
                                <td>-</td>
                                <%--<td>NPW</td>--%>
                                <td>-</td>
                                <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                <td>Ongoing</td>
                                <td>
                                    <asp:LinkButton ID="lbSampleResult" CssClass="btn-sm btn-default" runat="server" data-original-title="Sample Result" data-toggle="tooltip" OnCommand="btnCommand" CommandName="btnInsert" CommandArgument="2,2,2,2,11"><i class="fa fa-pencil" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>2.</td>
                                <td>เครื่องชั่ง 1.5 Kg</td>
                                <td>LAB 1002 E</td>
                                <td>10092883337</td>
                                <td>07/07/2017</td>
                                <td>10/07/2017</td>
                                <%-- <td>External Lab</td>--%>

                                <td>NPW</td>
                                <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                <td>Completed</td>
                                <td>
                                    <asp:LinkButton ID="lbSampleView" CssClass="btn-sm btn-default" runat="server" data-original-title="View" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEvent" CommandArgument=""><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>3.</td>
                                <td>เครื่องชั่ง 1.5 Kg</td>
                                <td>LAB 1003 E</td>
                                <td>10092883337</td>

                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                                <td>Waiting QA</td>
                                <td>
                                    <asp:LinkButton ID="LinkButton7" CssClass="btn-sm btn-default" runat="server" data-original-title="Receive" data-toggle="tooltip" OnCommand="btnCommand" CommandName="btnInsert"
                                        CommandArgument="2,2,1,2,3"><i class="fa fa-sign-in" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!--sample list-->

            <!--sample Waiting Recieve-->
            <div class="panel panel-success" id="sampleWaitingRecieve" runat="server" visible="false">
                <div class="panel-heading">
                    <h3 class="panel-title">รายการเครื่องมือที่สอบเทียบ</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <a href='<%=ResolveUrl("~/print-sample-cal") %>' id="btn1" class="btn btn-sm btn-default" target="_blank"><i class="fa fa-print" aria-hidden="true"></i>Print</a>
                    </div>
                    <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                        <tbody>
                            <tr class="info" style="font-size: Small; height: 40px;">
                                <th scope="col" style="width: 2%;">No.</th>
                                <th scope="col" style="width: 18%;">Machine Name</th>
                                <th scope="col" style="width: 10%;">ID No.</th>
                                <th scope="col" style="width: 10%;">Serial No.</th>
                                <th scope="col" style="width: 10%;">Received Date</th>
                                <th scope="col" style="width: 10%;">Tested Date</th>
                                <th scope="col" style="width: 8%;">Lab</th>
                                <th scope="col" style="width: 5%;">certificate</th>
                                <th scope="col" style="width: 15%;">Status</th>
                                <th scope="col" style="width: 5%;">Action</th>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>1.</td>
                                <td>เครื่องชั่ง 1.5 Kg</td>
                                <td>LAB 1001 E</td>
                                <td>10092883337</td>
                                <td>07/07/2017</td>
                                <td>07/07/2017</td>
                                <%--<td>NPW</td>--%>
                                <td></td>
                                <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                <td>Ongoing</td>
                                <td>
                                    <asp:LinkButton ID="LinkButton5" CssClass="btn-sm btn-default" runat="server" data-original-title="Sample Result" data-toggle="tooltip" OnCommand="btnCommand" CommandName="btnInsert" CommandArgument="2,2,2,2,11"><i class="fa fa-pencil" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>2.</td>
                                <td>เครื่องชั่ง 1.5 Kg</td>
                                <td>LAB 1002 E</td>
                                <td>10092883337</td>
                                <td>07/07/2017</td>
                                <td>10/07/2017</td>
                                <%-- <td>External Lab</td>--%>

                                <td>NPW</td>
                                <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                <td>Completed</td>
                                <td>
                                    <asp:LinkButton ID="LinkButton6" CssClass="btn-sm btn-default" runat="server" data-original-title="View" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEvent" CommandArgument=""><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>3.</td>
                                <td>เครื่องชั่ง 1.5 Kg</td>
                                <td>LAB 1003 E</td>
                                <td>10092883337</td>

                                <td>07/07/2017</td>
                                <td>10/07/2017</td>
                                <td>RJN</td>
                                <td></td>
                                <td>Waiting Lab Receive</td>
                                <td>
                                    <asp:LinkButton ID="LinkButton8" CssClass="btn-sm btn-default" runat="server" data-original-title="Receive" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEvent" OnClientClick="return confirm('คุณต้องการรับรายการนี้ใช่หรือไม่ ?')" CommandArgument="4"><i class="fa fa-sign-in" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!--sample Waiting Recieve-->

            <!--sample Recieve-->
            <div class="panel panel-success" id="sampleRecieve" runat="server" visible="false">
                <div class="panel-heading">
                    <h3 class="panel-title">รายการเครื่องมือที่สอบเทียบ</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <a href='<%=ResolveUrl("~/print-sample-cal") %>' id="btn1" class="btn btn-sm btn-default" target="_blank"><i class="fa fa-print" aria-hidden="true"></i>Print</a>
                    </div>
                    <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                        <tbody>
                            <tr class="info" style="font-size: Small; height: 40px;">
                                <th scope="col" style="width: 2%;">No.</th>
                                <th scope="col" style="width: 18%;">Machine Name</th>
                                <th scope="col" style="width: 10%;">ID No.</th>
                                <th scope="col" style="width: 10%;">Serial No.</th>
                                <th scope="col" style="width: 10%;">Received Date</th>
                                <th scope="col" style="width: 10%;">Tested Date</th>
                                <th scope="col" style="width: 8%;">Lab</th>
                                <th scope="col" style="width: 5%;">certificate</th>
                                <th scope="col" style="width: 15%;">Status</th>
                                <th scope="col" style="width: 5%;">Action</th>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>1.</td>
                                <td>เครื่องชั่ง 1.5 Kg</td>
                                <td>LAB 1001 E</td>
                                <td>10092883337</td>
                                <td>-</td>
                                <td>-</td>
                                <%--<td>NPW</td>--%>
                                <td>-</td>
                                <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                <td>Ongoing</td>
                                <td>
                                    <asp:LinkButton ID="LinkButton9" CssClass="btn-sm btn-default" runat="server" data-original-title="Sample Result" data-toggle="tooltip" OnCommand="btnCommand" CommandName="btnInsert" CommandArgument="2,2,2,2,11"><i class="fa fa-pencil" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>2.</td>
                                <td>เครื่องชั่ง 1.5 Kg</td>
                                <td>LAB 1002 E</td>
                                <td>10092883337</td>
                                <td>07/07/2017</td>
                                <td>10/07/2017</td>
                                <%-- <td>External Lab</td>--%>

                                <td>NPW</td>
                                <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                <td>Completed</td>
                                <td>
                                    <asp:LinkButton ID="LinkButton10" CssClass="btn-sm btn-default" runat="server" data-original-title="View" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEvent" CommandArgument=""><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>3.</td>
                                <td>เครื่องชั่ง 1.5 Kg</td>
                                <td>LAB 1003 E</td>
                                <td>10092883337</td>

                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                                <td>Waiting Internal Lab</td>
                                <td>
                                    <asp:LinkButton ID="LinkButton11" CssClass="btn-sm btn-default" runat="server" data-original-title="Receive" data-toggle="tooltip" OnCommand="btnCommand" CommandName="btnInsert" CommandArgument="3,4,1,3,5"><i class="fa fa-sign-in" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!--sample Recieve-->


            <!--sample LAB Recieve-->
            <div class="panel panel-success" id="sampleLABRecieve" runat="server" visible="false">
                <div class="panel-heading">
                    <h3 class="panel-title">รายการเครื่องมือที่สอบเทียบ</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <a href='<%=ResolveUrl("~/print-sample-cal") %>' id="btn" class="btn btn-sm btn-default" target="_blank"><i class="fa fa-print" aria-hidden="true"></i>Print</a>
                    </div>
                    <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                        <tbody>
                            <tr class="info" style="font-size: Small; height: 40px;">
                                <th scope="col" style="width: 2%;">No.</th>
                                <th scope="col" style="width: 18%;">Machine Name</th>
                                <th scope="col" style="width: 10%;">ID No.</th>
                                <th scope="col" style="width: 10%;">Serial No.</th>
                                <th scope="col" style="width: 10%;">Received Date</th>
                                <th scope="col" style="width: 10%;">Tested Date</th>
                                <th scope="col" style="width: 8%;">Lab</th>
                                <th scope="col" style="width: 5%;">certificate</th>
                                <th scope="col" style="width: 15%;">Status</th>
                                <th scope="col" style="width: 5%;">Action</th>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>1.</td>
                                <td>เครื่องชั่ง 1.5 Kg</td>
                                <td>LAB 1001 E</td>
                                <td>10092883337</td>
                                <td>-</td>
                                <td>-</td>
                                <%--<td>NPW</td>--%>
                                <td>-</td>
                                <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                <td>Ongoing</td>
                                <td>
                                    <asp:LinkButton ID="LinkButton30" CssClass="btn-sm btn-default" runat="server" data-original-title="Sample Result" data-toggle="tooltip" OnCommand="btnCommand" CommandName="btnInsert" CommandArgument="2,2,2,2,11"><i class="fa fa-pencil" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>2.</td>
                                <td>เครื่องชั่ง 1.5 Kg</td>
                                <td>LAB 1002 E</td>
                                <td>10092883337</td>
                                <td>07/07/2017</td>
                                <td>10/07/2017</td>
                                <%-- <td>External Lab</td>--%>

                                <td>NPW</td>
                                <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                <td>Completed</td>
                                <td>
                                    <asp:LinkButton ID="LinkButton34" CssClass="btn-sm btn-default" runat="server" data-original-title="View" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEvent" CommandArgument=""><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>3.</td>
                                <td>เครื่องชั่ง 1.5 Kg</td>
                                <td>LAB 1003 E</td>
                                <td>10092883337</td>

                                <td>07/07/2017</td>
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                                <td>Ongoing</td>
                                <td>
                                    <asp:LinkButton ID="LinkButton35" CssClass="btn-sm btn-default" runat="server" data-original-title="Receive" data-toggle="tooltip" OnCommand="btnCommand" CommandName="btnInsert" CommandArgument="3,5,0,4,6"><i class="fa fa-pencil" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!--sample Recieve-->


            <!--sample Waiting Supervisor-->
            <div class="panel panel-success" id="sampleWaitingSupervisor" runat="server" visible="false">
                <div class="panel-heading">
                    <h3 class="panel-title">รายการเครื่องมือที่สอบเทียบ</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <a href='<%=ResolveUrl("~/print-sample-cal") %>' id="btn" class="btn btn-sm btn-default" target="_blank"><i class="fa fa-print" aria-hidden="true"></i>Print</a>
                    </div>
                    <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                        <tbody>
                            <tr class="info" style="font-size: Small; height: 40px;">
                                <th scope="col" style="width: 2%;">No.</th>
                                <th scope="col" style="width: 18%;">Machine Name</th>
                                <th scope="col" style="width: 10%;">ID No.</th>
                                <th scope="col" style="width: 10%;">Serial No.</th>
                                <th scope="col" style="width: 10%;">Received Date</th>
                                <th scope="col" style="width: 10%;">Tested Date</th>
                                <th scope="col" style="width: 8%;">Lab</th>
                                <th scope="col" style="width: 5%;">certificate</th>
                                <th scope="col" style="width: 15%;">Status</th>
                                <th scope="col" style="width: 5%;">Action</th>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>1.</td>
                                <td>เครื่องชั่ง 1.5 Kg</td>
                                <td>LAB 1001 E</td>
                                <td>10092883337</td>
                                <td>-</td>
                                <td>-</td>
                                <%--<td>NPW</td>--%>
                                <td>-</td>
                                <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                <td>Ongoing</td>
                                <td>
                                    <asp:LinkButton ID="LinkButton12" CssClass="btn-sm btn-default" runat="server" data-original-title="Sample Result" data-toggle="tooltip" OnCommand="btnCommand" CommandName="btnInsert" CommandArgument="2,2,2,2,11"><i class="fa fa-pencil" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>2.</td>
                                <td>เครื่องชั่ง 1.5 Kg</td>
                                <td>LAB 1002 E</td>
                                <td>10092883337</td>
                                <td>07/07/2017</td>
                                <td>10/07/2017</td>
                                <%-- <td>External Lab</td>--%>

                                <td>NPW</td>
                                <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                <td>Completed</td>
                                <td>
                                    <asp:LinkButton ID="LinkButton13" CssClass="btn-sm btn-default" runat="server" data-original-title="View" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEvent" CommandArgument=""><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>3.</td>
                                <td>เครื่องชั่ง 1.5 Kg</td>
                                <td>LAB 1003 E</td>
                                <td>10092883337</td>

                                <td>07/07/2017</td>
                                <td>10/07/2017</td>
                                <td>RJN</td>
                                <td>-</td>
                                <td>Waiting Supervisor</td>
                                <td>
                                    <asp:LinkButton ID="LinkButton14" CssClass="btn-sm btn-default" runat="server" data-original-title="Receive" data-toggle="tooltip" OnCommand="btnCommand" CommandName="btnInsert" CommandArgument="4,6,0,2,7"><i class="fa fa-user" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!--sample Waiting Supervisor-->

            <!--sample Result Accept-->
            <div class="panel panel-success" id="sampleResultAccept" runat="server" visible="false">
                <div class="panel-heading">
                    <h3 class="panel-title">รายการเครื่องมือที่สอบเทียบ</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <a href='<%=ResolveUrl("~/print-sample-cal") %>' id="btn" class="btn btn-sm btn-default" target="_blank"><i class="fa fa-print" aria-hidden="true"></i>Print</a>
                    </div>
                    <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                        <tbody>
                            <tr class="info" style="font-size: Small; height: 40px;">
                                <th scope="col" style="width: 2%;">No.</th>
                                <th scope="col" style="width: 18%;">Machine Name</th>
                                <th scope="col" style="width: 10%;">ID No.</th>
                                <th scope="col" style="width: 10%;">Serial No.</th>
                                <th scope="col" style="width: 10%;">Received Date</th>
                                <th scope="col" style="width: 10%;">Tested Date</th>
                                <th scope="col" style="width: 8%;">Lab</th>
                                <th scope="col" style="width: 5%;">certificate</th>
                                <th scope="col" style="width: 15%;">Status</th>
                                <th scope="col" style="width: 5%;">Action</th>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>1.</td>
                                <td>เครื่องชั่ง 1.5 Kg</td>
                                <td>LAB 1001 E</td>
                                <td>10092883337</td>
                                <td>-</td>
                                <td>-</td>
                                <%--<td>NPW</td>--%>
                                <td>-</td>
                                <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                <td>Ongoing</td>
                                <td>
                                    <asp:LinkButton ID="LinkButton15" CssClass="btn-sm btn-default" runat="server" data-original-title="Sample Result" data-toggle="tooltip" OnCommand="btnCommand" CommandName="btnInsert" CommandArgument="2,2,2,2,11"><i class="fa fa-pencil" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>2.</td>
                                <td>เครื่องชั่ง 1.5 Kg</td>
                                <td>LAB 1002 E</td>
                                <td>10092883737</td>
                                <td>07/07/2017</td>
                                <td>10/07/2017</td>
                                <%-- <td>External Lab</td>--%>

                                <td>NPW</td>
                                <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                <td>Completed</td>
                                <td>
                                    <asp:LinkButton ID="LinkButton16" CssClass="btn-sm btn-default" runat="server" data-original-title="View" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEvent" CommandArgument=""><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>3.</td>
                                <td>เครื่องชั่ง 1.5 Kg</td>
                                <td>LAB 1003 E</td>
                                <td>10092883707</td>

                                <td>07/07/2017</td>
                                <td>10/07/2017</td>
                                <td>RJN</td>
                                <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                <td>Completed</td>
                                <td>
                                    <asp:LinkButton ID="LinkButton17" CssClass="btn-sm btn-default" runat="server" data-original-title="Receive" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEvent"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!--sample Waiting Supervisor-->

            <!--sample Result cerfiticate-->
            <div class="panel panel-success" id="sampleResultcerfiticate" runat="server" visible="false">
                <div class="panel-heading">
                    <h3 class="panel-title">รายการเครื่องมือที่สอบเทียบ</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <a href='<%=ResolveUrl("~/print-sample-cal") %>' id="btn" class="btn btn-sm btn-default" target="_blank"><i class="fa fa-print" aria-hidden="true"></i>Print</a>
                    </div>
                    <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                        <tbody>
                            <tr class="info" style="font-size: Small; height: 40px;">
                                <th scope="col" style="width: 2%;">No.</th>
                                <th scope="col" style="width: 18%;">Machine Name</th>
                                <th scope="col" style="width: 10%;">ID No.</th>
                                <th scope="col" style="width: 10%;">Serial No.</th>
                                <th scope="col" style="width: 10%;">Received Date</th>
                                <th scope="col" style="width: 10%;">Tested Date</th>
                                <th scope="col" style="width: 8%;">Lab</th>
                                <th scope="col" style="width: 5%;">certificate</th>
                                <th scope="col" style="width: 15%;">Status</th>
                                <th scope="col" style="width: 5%;">Action</th>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>1.</td>
                                <td>เครื่องชั่ง 1.5 Kg</td>
                                <td>LAB 1001 E</td>
                                <td>10092883337</td>
                                <td>07/07/2017</td>
                                <td>07/07/2017</td>
                                <%--<td>NPW</td>--%>
                                <td>-</td>
                                <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                <td>Completed</td>
                                <td>
                                    <asp:LinkButton ID="LinkButton18" CssClass="btn-sm btn-default" runat="server" data-original-title="Sample Result" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdViewDoc" CommandArgument="7"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>2.</td>
                                <td>เครื่องชั่ง 1.5 Kg</td>
                                <td>LAB 1002 E</td>
                                <td>10092883337</td>
                                <td>07/07/2017</td>
                                <td>10/07/2017</td>
                                <%-- <td>External Lab</td>--%>

                                <td>NPW</td>
                                <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                <td>Completed</td>
                                <td>
                                    <asp:LinkButton ID="LinkButton19" CssClass="btn-sm btn-default" runat="server" data-original-title="View" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdViewDoc" CommandArgument="7"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>3.</td>
                                <td>เครื่องชั่ง 1.5 Kg</td>
                                <td>LAB 1003 E</td>
                                <td>10092883337</td>

                                <td>07/07/2017</td>
                                <td>10/07/2017</td>
                                <td>RJN</td>
                                <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                <td>Completed</td>
                                <td>
                                    <asp:LinkButton ID="LinkButton20" CssClass="btn-sm btn-default" runat="server" data-original-title="Receive" data-toggle="tooltip" OnCommand="btnCommand" CommandName="btnInsert" CommandArgument=""><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="form-group">
                        <div class="pull-right">
                            <div class="col-sm-12">

                                <asp:LinkButton ID="lbDocSave1" CssClass="btn btn-success" Visible="false" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="btnInsert" CommandArgument="2,7,1,1,8"></asp:LinkButton>
                                <asp:LinkButton ID="btncancelnew" CssClass="btn btn-danger" Visible="false" runat="server" data-original-title="cancel" data-toggle="tooltip" Text="Cancel" OnCommand="btnCommand" CommandArgument="0" CommandName="cmdDocCancel"></asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!--sample Waiting Supervisor-->
            <!--document log-->
            <div class="panel panel-default" id="logList" runat="server">
                <div class="panel-heading">
                    <h3 class="panel-title">รายละเอียดการดำเนินการ</h3>
                </div>
                <div class="panel-body">
                </div>
            </div>
            <!--document log-->

            <!--document info-->
            <asp:FormView ID="fvInsertLabCal" runat="server" DefaultMode="Insert" Width="100%">
                <InsertItemTemplate>
                    <asp:HiddenField ID="hfNodeIDX" runat="server" Value="1" />
                    <asp:HiddenField ID="hfActorIDX" runat="server" Value="1" />
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">รายการเครื่องมือ</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">

                                <div class="form-group">

                                    <div class="col-lg-offset-2 col-sm-10">
                                        <asp:RadioButton ID="machine_1" Width="25%" runat="server" AutoPostBack="true"
                                            OnCheckedChanged="RadioButton_CheckedChanged" CssClass="small" Text=" เครื่องมือเก่า (Machine Old)" GroupName="machine"
                                            ValidationGroup="Saveinsertdetail" />

                                        <asp:RadioButton ID="machine_2" Width="25%" CssClass="small" runat="server" AutoPostBack="true"
                                            OnCheckedChanged="RadioButton_CheckedChanged" Text=" เครื่องมือใหม่ (Machine New)" GroupName="machine"
                                            ValidationGroup="Saveinsertdetail" />
                                    </div>
                                </div>

                                <asp:Panel ID="machine_old" runat="server" Visible="false">
                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Search ID No.</b>
                                            <p class="list-group-item-text">ค้นหาจากรหัสเครื่องมือ</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="searchingToolID" runat="server" placeholder="รหัสเครื่อง.. (ID No.)" Visible="true"
                                                OnTextChanged="txtchange" CssClass="form-control" AutoPostBack="true" />
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="pull-left">
                                                <asp:LinkButton ID="btnSearchIndex1" CssClass="btn btn-primary" runat="server" ValidationGroup="SearchIndex" OnCommand="btnCommand" Font-Size="Small" data-toggle="tooltip" title="Search"><span class="glyphicon glyphicon-search"></span></asp:LinkButton>


                                          <%--      <asp:LinkButton ID="LinkButton117" CssClass="btn btn-default" runat="server" OnCommand="btnCommand" data-toggle="tooltip" Font-Size="Small" title="Cancel"><i class="fa fa-refresh"></i></asp:LinkButton>--%>
                                            </div>

                                            </label>
                                        </div>

                                        <%--<label class="col-sm-2 control-label">รหัสเครื่องมือ</label>--%>
                                        <%-- <div class="col-sm-2 control-label textleft small">
                                            <table id="PanelBody_YrChkBoxColumns" cellspacing="10" style="width: 100%;">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <input id="PanelBody_YrChkBoxColumns_0" type="checkbox" name="ctl00$PanelBody$YrChkBoxColumns$0" value="รหัสพนักงาน"><label for="PanelBody_YrChkBoxColumns_0">LAB 1001 E</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input id="PanelBody_YrChkBoxColumns_1" type="checkbox" name="ctl00$PanelBody$YrChkBoxColumns$1" value="ชื่อพนักงาน"><label for="PanelBody_YrChkBoxColumns_1">LAB 1002 E</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input id="PanelBody_YrChkBoxColumns_0" type="checkbox" name="ctl00$PanelBody$YrChkBoxColumns$0" value="รหัสพนักงาน"><label for="PanelBody_YrChkBoxColumns_0">LAB 1003 E</label></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>--%>
                                    </div>
                                    <hr />
                                    <asp:Panel ID="divShowWarning" runat="server" Visible="false" class="alert alert-warning small" role="alert">

                                        <strong><i class="glyphicon glyphicon-info-sign"></i>&nbsp;</strong>
                                        ไม่พบข้อมูลเครื่องมือ 
                
                                    </asp:Panel>
                                    <asp:Panel ID="panellist_ToolID" runat="server" Visible="false">
                                        <div class="form-group">
                                            <div class="col-sm-2 text_right small">
                                                <b>ID No.</b>
                                                <p class="list-group-item-text">รหัสเครื่องมือ</p>
                                            </div>
                                            <div class="col-sm-2 control-label textleft small">
                                                <table id="PanelBody_YrChkBoxColumns" cellspacing="10" style="width: 100%;">
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <asp:CheckBox ID="chktoolID" runat="server" Font-Bold="true" Text="LAB 1001 E" AutoPostBack="true" OnCheckedChanged="CheckboxChanged" RepeatDirection="Vertical" />
                                                                <%--  <input id="checkIDtool" type="checkbox" name="ctl00$PanelBody$YrChkBoxColumns$0" value="รหัสพนักงาน"><label for="PanelBody_YrChkBoxColumns_0">LAB 1001 E</label>--%></td>
                                                        </tr>
                                                        <%--  <tr>
                                                        <td>
                                                            <input id="PanelBody_YrChkBoxColumns_1" type="checkbox" name="ctl00$PanelBody$YrChkBoxColumns$1" value="ชื่อพนักงาน"><label for="PanelBody_YrChkBoxColumns_1">LAB 1002 E</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input id="PanelBody_YrChkBoxColumns_0" type="checkbox" name="ctl00$PanelBody$YrChkBoxColumns$0" value="รหัสพนักงาน"><label for="PanelBody_YrChkBoxColumns_0">LAB 1003 E</label></td>
                                                    </tr>--%>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                    <asp:Panel ID="paneldetailTool" runat="server" Visible="false">
                                        <div class="form-group">
                                            <div class="col-sm-2 text_right small">
                                                <b>ID No.</b>
                                                <p class="list-group-item-text">รหัสเครื่องมือ</p>
                                            </div>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="DropDownList2" runat="server" CssClass="form-control" Text="LAB 1001 E" Enabled="false">
                                                  
                                                </asp:TextBox>
                                            </div>
                                            <div class="col-sm-2 text_right small">
                                                <b>Machine Type</b>
                                                <p class="list-group-item-text">ประเภทเครื่องมือ</p>
                                            </div>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="DropDownList1" runat="server" Text="Digital" CssClass="form-control" Enabled="false">
                                                 
                                                </asp:TextBox>
                                            </div>


                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-2 text_right small">
                                                <b>Machine Name</b>
                                                <p class="list-group-item-text">ชื่อเครื่องมือ/อุปกรณ์</p>
                                            </div>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="ddlDocType" runat="server" Text="เครื่องชั่ง 1.5 Kg" CssClass="form-control" Enabled="false">
                                                  
                                                </asp:TextBox>
                                            </div>
                                            <div class="col-sm-2 text_right small">
                                                <b>Brand</b>
                                                <p class="list-group-item-text">ยี่ห้อ/รุ่น</p>
                                            </div>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="DropDownList4" runat="server" Text="3 S/SU/NEW SU" CssClass="form-control" Enabled="false">
                                                 
                                                </asp:TextBox>
                                            </div>
                                            <%-- <label class="col-sm-2 control-label">รายละเอียดเครื่องมือ</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txt_insert" runat="server" TextMode="MultiLine" Rows="1" placeholder="กรอกรายละเอียดเครื่องมือ.." CssClass="form-control" />
                                        </div>--%>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-2 text_right small">
                                                <b>Serial No.</b>
                                                <p class="list-group-item-text">หมายเลขเครื่อง </p>
                                            </div>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="TextBox22" runat="server" CssClass="form-control" Text="1006120542" Enabled="false"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-2 text_right small">
                                                <b>Section Holder</b>
                                                <p class="list-group-item-text">แผนกผู้ถือครอง</p>
                                            </div>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="DropDownList5" runat="server" CssClass="form-control" Text="QA Lab" Enabled="false"></asp:TextBox>
                                            </div>


                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-2 text_right small">
                                                <b>Cal Date</b>
                                                <p class="list-group-item-text">วันที่สอบเทียบล่าสุด </p>
                                            </div>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="TextBox23" runat="server" CssClass="form-control" Text="06/07/2016" Enabled="false"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-2 text_right small">
                                                <b>Due date</b>
                                                <p class="list-group-item-text">วันที่สอบเทียบครั้งถัดไป</p>
                                            </div>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="TextBox24" runat="server" CssClass="form-control" Text="06/08/2017" Enabled="false"></asp:TextBox>
                                            </div>


                                        </div>


                                        <div class="form-group">

                                            <div class="col-lg-offset-2 col-sm-10">
                                                <asp:LinkButton ID="btnInsertDetail" CssClass="btn btn-default" runat="server"
                                                    CommandName="btnInsertDetail" OnCommand="btnCommand" ValidationGroup="Saveinsertdetail"
                                                    title="เพิ่มรายการ"><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่ม</asp:LinkButton>
                                            </div>

                                        </div>
                                    </asp:Panel>
                                </asp:Panel>

                                <asp:Panel ID="machine_new" runat="server" Visible="false">

                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Machine Name</b>
                                            <p class="list-group-item-text">ชื่อเครื่องมือ/อุปกรณ์</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txtinput_machine_name" runat="server" placeholder="กรอกชื่อเครื่องมือ.." Visible="false" CssClass="form-control" />
                                            <asp:DropDownList ID="ddl_machine_name" runat="server" CssClass="form-control">
                                                <asp:ListItem>เครื่องชั่ง 1.5 Kg</asp:ListItem>
                                                <asp:ListItem>เครื่องชั่ง 2.0 Kg</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:CheckBox ID="chk_other_name" runat="server" Font-Bold="true" CssClass="small" Text="อื่นๆ (Other)" AutoPostBack="true" OnCheckedChanged="CheckboxChanged" RepeatDirection="Vertical" />
                                        </div>
                                        <div class="col-sm-2 text_right small">
                                            <b>Brand</b>
                                            <p class="list-group-item-text">ยี่ห้อ/รุ่น</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txtinput_machine_brand" runat="server" placeholder="กรอกชื่อยี่ห้อ.." Visible="false" CssClass="form-control" />
                                            <asp:DropDownList ID="ddl_machine_brand" runat="server" CssClass="form-control">
                                                <asp:ListItem>3 S/SU/NEW SU</asp:ListItem>
                                                <asp:ListItem>3 S/SU/NEW SA</asp:ListItem>
                                            </asp:DropDownList>
                                            <%--   <asp:CheckBox ID="chk_other_brand" runat="server" Font-Bold="true" Text="อื่นๆ" AutoPostBack="true" RepeatDirection="Vertical" />--%>
                                        </div>
                                    </div>
                                    <%-- <div class="form-group">--%>


                                    <%--</div>--%>
                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Machine Type</b>
                                            <p class="list-group-item-text">ประเภทเครื่องมือ</p>
                                        </div>

                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlmachineType" runat="server" CssClass="form-control" Enabled="true">
                                                <asp:ListItem Text="Digital" Value="1"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <%--  </div>
                                    <div class="form-group">--%>
                                        <div class="col-sm-2 text_right small">
                                            <b>Serial No.</b>
                                            <p class="list-group-item-text">หมายเลขเครื่อง</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="TextBox12" runat="server" placeholder="กรอกหมายเลขเครื่อง.." CssClass="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Range of use</b>
                                            <p class="list-group-item-text">ช่วงการใช้งาน</p>
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:TextBox ID="TextBox9" runat="server" placeholder="เริ่มต้น.." CssClass="form-control" />
                                        </div>

                                        <div class="col-sm-1">
                                            <asp:TextBox ID="TextBox11" runat="server" placeholder="สิ้นสุด.." CssClass="form-control" />
                                        </div>
                                        <div class="col-sm-2">
                                            <asp:DropDownList ID="TextBox13" runat="server" CssClass="form-control">
                                                <asp:ListItem>mg</asp:ListItem>
                                                <asp:ListItem>g</asp:ListItem>
                                                <asp:ListItem>kg</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-2 text_right small">
                                            <b>Machine Detail</b>
                                            <p class="list-group-item-text">รายละเอียดเครื่อง</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="TextBox15" runat="server" TextMode="MultiLine" Rows="1" placeholder="กรอกรายละเอียดเครื่อง.." CssClass="form-control" />
                                        </div>
                                    </div>

                                    <div class="form-group">

                                        <div class="col-lg-offset-2 col-sm-10">
                                            <asp:RadioButton ID="certificate_1" Width="25%" runat="server" CssClass="small" AutoPostBack="true" Text=" มีใบรับรอง (Has a certificate)" GroupName="machine1" ValidationGroup="Saveinsertdetail" />

                                            <asp:RadioButton ID="certificate_2" Width="25%" runat="server" AutoPostBack="true" CssClass="small" Text="ไม่มีใบรับรอง (Has not certificate)" GroupName="machine1" ValidationGroup="Saveinsertdetail" />
                                        </div>
                                    </div>

                                    <asp:Panel ID="uploadfile" runat="server" Visible="false">
                                        <div class="form-group">

                                            <div class="col-lg-offset-2 col-sm-10">
                                                <asp:FileUpload ID="uploadecertificate" runat="server" />
                                            </div>
                                        </div>
                                    </asp:Panel>

                                    <div class="form-group">

                                        <div class="col-lg-offset-2 col-sm-10">
                                            <asp:LinkButton ID="btninsertmachine" CssClass="btn btn-default" runat="server"
                                                CommandName="btninsertmachine" OnCommand="btnCommand" ValidationGroup="Saveinsertdetail"
                                                title="เพิ่มรายการ"><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่ม</asp:LinkButton>
                                        </div>

                                    </div>

                                </asp:Panel>


                                <asp:Panel ID="div_showdetail_machine_new" runat="server" Visible="false">
                                    <%--  <div class="form-group">

                                        <div class="pull-left">
                                            <div class="col-sm-12">
                                                <a href='<%=ResolveUrl("~/print-formcal") %>' class="btn btn-default" target="_blank"><i class="fa fa-print" aria-hidden="true"></i> print</a>
                                            </div>
                                        </div>
                                    </div>--%>
                                    <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" id="machinenew" style="border-collapse: collapse;">
                                        <tbody>
                                            <tr class="info" style="font-size: Small; height: 40px;">
                                                <th scope="col" style="width: 10%;">ชื่อเครื่องมือ/อุปกรณ์
                                                    <br />
                                                    (Machine Name)</th>
                                                <th scope="col" style="width: 10%;">ประเภทเครื่องมือ
                                                    <br />
                                                    (Machine Type)</th>
                                                <th scope="col" style="width: 10%;">หมายเลขเครื่อง
                                                    <br />
                                                    (Serial No.)</th>
                                                <th scope="col" style="width: 10%;">ยี่ห้อ/รุ่น
                                                    <br />
                                                    (Brand) </th>
                                                <th scope="col" style="width: 10%;">ช่วงการใช้งาน
                                                    <br />
                                                    (Range of use)</th>
                                                <th scope="col" style="width: 10%;">รายละเอียดเครื่อง<br />
                                                    (Machine Detail)</th>
                                                <th scope="col" style="width: 2%;">ใบรับรอง
                                                    <br />
                                                    (Certificate)</th>
                                                <th scope="col" style="width: 8%;">การจัดการ
                                                    <br />
                                                    (Manage)</th>
                                            </tr>
                                            <tr style="font-size: Small;">
                                                <td>เครื่องชั่ง 1.5 Kg</td>
                                                <td>Digital</td>
                                                <td>10096578883</td>
                                                <td>3 S/SU/NEW SU</td>
                                                <td>1-300 g</td>
                                                <td>รายละเอียด</td>
                                                <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                                <td><i class="fa fa-trash" data-original-title="delete" data-toggle="tooltip" aria-hidden="true"></i></td>
                                            </tr>
                                            <tr style="font-size: Small;">
                                                <td>เครื่องชั่ง 1.5 Kg</td>
                                                <td>Digital</td>
                                                <td>10099878883</td>
                                                <td>SUPER-SS/3S</td>
                                                <td>1-300 g</td>
                                                <td>รายละเอียด</td>
                                                <td>-</td>
                                                <td><i class="fa fa-trash" data-original-title="delete" data-toggle="tooltip" aria-hidden="true"></i></td>
                                            </tr>
                                            <tr style="font-size: Small;">
                                                <td>เครื่องชั่ง 1.5 Kg</td>
                                                <td>Digital</td>
                                                <td>10064578883</td>
                                                <td>3 S/SU/NEW SU</td>
                                                <td>1-300 g</td>
                                                <td>รายละเอียด</td>
                                                <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                                <td><i class="fa fa-trash" data-original-title="delete" data-toggle="tooltip" aria-hidden="true"></i></td>
                                            </tr>

                                        </tbody>
                                    </table>

                                    <div class="pull-right">

                                        <asp:LinkButton ID="LinkButton1" CssClass="btn btn-success" Font-Size="Small" runat="server" CommandName="btnInsert" data-original-title="บันทึก" data-toggle="tooltip" CommandArgument="1,1,1,2,2" OnCommand="btnCommand" Text="บันทึก" ValidationGroup="Saveinsert">
                                        </asp:LinkButton>

                                        <asp:LinkButton ID="LinkButton2" CssClass="btn btn-danger" Font-Size="Small" runat="server" data-original-title="ยกเลิก" data-toggle="tooltip" OnCommand="btnCommand" Text="ยกเลิก" CommandName="btnCancel"></asp:LinkButton>
                                        <a href='<%=ResolveUrl("~/print-formcal") %>' class="btn btn-default" target="_blank"><i class="fa fa-print" aria-hidden="true"></i>Print</a>
                                    </div>


                                </asp:Panel>

                                <asp:Panel ID="div_showdetail_machine_old" runat="server" Visible="false">

                                    <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" id="ContentMain_gvDoingList" style="border-collapse: collapse;">
                                        <tbody>
                                            <tr class="info" style="font-size: Small; height: 40px;">
                                                <th scope="col" style="width: 10%;">รหัสเครื่องมือ
                                                    <br />
                                                    (ID No.)</th>
                                                <th scope="col" style="width: 13%;">ชื่อเครื่องมือ/อุปกรณ์
                                                    <br />
                                                    (Machine Name)</th>
                                                <th scope="col" style="width: 11%;">ประเภทเครื่องมือ
                                                    <br />
                                                    (Machine Type)</th>
                                                <th scope="col" style="width: 10%;">ยี่ห้อ/รุ่น
                                                    <br />
                                                    (Brand)</th>
                                                <th scope="col" style="width: 10%;">หมายเลขเครื่อง
                                                    <br />
                                                    (Serial No.)</th>
                                                <th scope="col" style="width: 8%;">ผู้ถือครอง
                                                    <br />
                                                    (Holder)</th>
                                                <th scope="col" style="width: 12%;">วันที่สอบเทียบล่าสุด
                                                    <br />
                                                    (Cal Date)</th>
                                                <th scope="col" style="width: 14%;">วันที่สอบเทียบครั้งถัดไป
                                                    <br />
                                                    (Due date)</th>
                                                <th scope="col" style="width: 7%;">การจัดการ<br />
                                                    (Manage)</th>

                                            </tr>
                                            <tr style="font-size: Small;">
                                                <td>LAB 1001 E</td>
                                                <td>เครื่องชั่ง 1.5 Kg</td>
                                                <td>Digital</td>
                                                <td>3 S/SU/NEW SU</td>
                                                <td>1006120542</td>
                                                <td>QA Lab</td>
                                                <td>06/07/2016</td>
                                                <td>06/08/2017</td>
                                                <td><i class="fa fa-trash" data-original-title="delete" data-toggle="tooltip" aria-hidden="true"></i></td>
                                            </tr>
                                            <tr style="font-size: Small;">
                                                <td>LAB 1002 E</td>
                                                <td>เครื่องชั่ง 1.5 Kg</td>
                                                <td>Digital</td>
                                                <td>3 S/SU/NEW SU</td>
                                                <td>1096120542</td>
                                                <td>QA Lab</td>
                                                <td>06/07/2016</td>
                                                <td>06/08/2017</td>
                                                <td><i class="fa fa-trash" data-original-title="delete" data-toggle="tooltip" aria-hidden="true"></i></td>
                                            </tr>
                                            <tr style="font-size: Small;">
                                                <td>LAB 1003 E</td>
                                                <td>เครื่องชั่ง 1.5 Kg</td>
                                                <td>Digital</td>
                                                <td>3 S/SU/NEW SU</td>
                                                <td>1096120342</td>
                                                <td>QA Lab</td>
                                                <td>06/07/2016</td>
                                                <td>06/08/2017</td>
                                                <td><i class="fa fa-trash" data-original-title="delete" data-toggle="tooltip" aria-hidden="true"></i></td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <div class="form-group">

                                        <div class="pull-right">
                                            <div class="col-sm-12">

                                                <asp:LinkButton ID="btnInsert" CssClass="btn btn-success" Font-Size="Small" runat="server" CommandName="btnInsert" data-original-title="บันทึก" data-toggle="tooltip" OnCommand="btnCommand" CommandArgument="1,1,1,2,3" Text="บันทึก" ValidationGroup="Saveinsert">

                                                </asp:LinkButton>

                                                <asp:LinkButton ID="btnCancel" CssClass="btn btn-danger" Font-Size="Small" runat="server" data-original-title="ยกเลิก" data-toggle="tooltip" OnCommand="btnCommand" Text="ยกเลิก" CommandName="btnCancel"></asp:LinkButton>
                                                <a href='<%=ResolveUrl("~/print-formcal") %>' class="btn btn-default" target="_blank"><i class="fa fa-print" aria-hidden="true"></i>Print</a>
                                            </div>
                                        </div>
                                    </div>

                                </asp:Panel>

                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>
            <!--document info-->

            <!--document info-->




        </asp:View>

        <asp:View ID="tab2" runat="server">

            <div class="form-horizontal" role="form">

                <div class="col-sm-12">
                    <div class="form-group">
                        <div class="input-group col-sm-3 pull-right">
                            <asp:TextBox ID="txtsearch" runat="server" placeholder="Document No." CssClass="form-control" />
                            <div class="input-group-btn">
                                <asp:LinkButton ID="LinkButton50" CssClass="btn btn-primary" runat="server" data-original-title="Search.." data-toggle="tooltip" OnCommand="btnCommand" CommandArgument="1" CommandName="cmdSearch"><i class="glyphicon glyphicon-search"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton119" CssClass="btn btn-default" runat="server" data-original-title="Reset.." data-toggle="tooltip" OnCommand="btnCommand" CommandArgument="1" CommandName="cmdReset"><i class="fa fa-refresh" aria-hidden="true"></i></asp:LinkButton>
                            </div>
                        </div>

                        <label class="col-sm-9 control-label"></label>

                        <%--       <div class="input-group col-sm-3 pull-right">
                            <asp:TextBox ID="txtsearch" runat="server" placeholder="Document No. or Sample Code" CssClass="form-control input-sm" />
                            <div class="input-group-btn btn-group-sm">
                                <asp:LinkButton ID="btnSearch" CssClass="btn btn-primary" Font-Size="Small" runat="server" data-original-title="Search.." data-toggle="tooltip" OnCommand="btnCommand" CommandArgument="1" CommandName="cmdSearch"><i class="glyphicon glyphicon-search"></i></asp:LinkButton>
                                <asp:LinkButton ID="btnreset" CssClass="btn btn-default" Font-Size="Small" runat="server" data-original-title="Reset.." data-toggle="tooltip" OnCommand="btnCommand" CommandArgument="1" CommandName="cmdReset"><i class="fa fa-refresh" aria-hidden="true"></i></asp:LinkButton>
                            </div>
                        </div>--%>
                    </div>
                </div>
            </div>

            <asp:Panel ID="details_search_doc" runat="server" Visible="false">
                <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" id="table1" style="border-collapse: collapse; width: 100%">
                    <tbody>
                        <tr class="info" style="font-size: Small; height: 40px;">
                            <th scope="col" style="width: 20%;">รหัสเอกสาร</th>
                            <th scope="col" style="width: 20%;">วันที่สร้างรายการ</th>
                            <th scope="col" style="width: 20%;">เครื่องมือประเภท</th>
                            <th scope="col" style="width: 20%;">status</th>
                            <th scope="col" style="width: 20%;">Action</th>

                        </tr>
                        <tr style="font-size: Small;">
                            <td>
                                <span id="ContentMain_gvDoingList_lblDocCode_0">CAL0000001</span>
                            </td>
                            <td>
                                <span id="ContentMain_gvDoingList_lblDocCode_0">06/07/2017</span>
                            </td>
                            <td>
                                <span id="ContentMain_gvDoingList_lblCreateDate_0">เครื่องใหม่</span>
                            </td>

                            <td>
                                <span id="ContentMain_gvDoingList_lblCreateDate_0">Waiting</span>
                            </td>
                            <td>
                                <asp:LinkButton ID="LinkButton44" CssClass="btn-sm btn-default" runat="server" data-original-title="Receive" data-toggle="tooltip" OnCommand="btnCommand" CommandName="btnInsert" CommandArgument='2,2,0,2,3'><i class="fa fa-sign-in" aria-hidden="true"></i></asp:LinkButton>
                            </td>

                        </tr>
                    </tbody>
                </table>
            </asp:Panel>

            <asp:Panel ID="details_search_sam" runat="server" Visible="false">
                <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                    <tbody>
                        <tr class="info" style="font-size: Small; height: 40px;">
                            <th scope="col" style="width: 10%;">Sample Code</th>
                            <th scope="col" style="width: 10%;">Machine Name</th>
                            <th scope="col" style="width: 10%;">ID No.</th>
                            <th scope="col" style="width: 10%;">Serial No.</th>
                            <th scope="col" style="width: 10%;">Received Date</th>
                            <th scope="col" style="width: 10%;">Tested Date</th>
                            <th scope="col" style="width: 10%;">Lab</th>
                            <th scope="col" style="width: 5%;">certificate</th>
                            <th scope="col" style="width: 10%;">Status</th>
                            <th scope="col" style="width: 5%;">Action</th>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>CAL0000001R03</td>
                            <td>Machine03</td>
                            <td>ID0003</td>
                            <td>Serial0003</td>

                            <td></td>
                            <td>-</td>
                            <td></td>
                            <td></td>
                            <td>Waiting QA</td>
                            <td>
                                <asp:LinkButton ID="LinkButton47" CssClass="btn-sm btn-default" runat="server" data-original-title="Receive" data-toggle="tooltip" OnCommand="btnCommand" CommandName="btnInsert"
                                    CommandArgument="2,2,1,2,3"><i class="fa fa-sign-in" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>

            <asp:Panel ID="genaralLabCal" runat="server">

                <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" id="table1" style="border-collapse: collapse; width: 100%">
                    <tbody>
                        <tr class="info" style="font-size: Small; height: 40px;">
                            <th scope="col" style="width: 20%;">รหัสเอกสาร</th>
                            <th scope="col" style="width: 20%;">วันที่สร้างรายการ</th>
                            <th scope="col" style="width: 20%;">เครื่องมือประเภท</th>
                            <th scope="col" style="width: 20%;">สถานะ</th>
                            <th scope="col" style="width: 20%;">การจัดการ</th>

                        </tr>
                        <tr style="font-size: Small;">
                            <td>
                                <span id="ContentMain_gvDoingList_lblDocCode_0">CAL0000001</span>
                            </td>
                            <td>
                                <span id="ContentMain_gvDoingList_lblDocCode_0">06/07/2017</span>
                            </td>
                            <td>
                                <span id="ContentMain_gvDoingList_lblCreateDate_0">เครื่องใหม่</span>
                            </td>
                            <%--  <td>
                                         <span id="ContentMain_gvDoingList_lblCreateDate_0">xxxxxxx</span>
                                     </td>--%>
                            <td>
                                <span id="ContentMain_gvDoingList_lblCreateDate_0">Waiting</span>
                            </td>
                            <%--  <td>
                                         <span id="ContentMain_gvDoingList_lblCreateDate_0">06/07/2017</span>
                                     </td>--%>
                            <td>
                                <asp:LinkButton ID="LinkButton3" CssClass="btn-sm btn-default" runat="server" data-original-title="Receive" data-toggle="tooltip" OnCommand="btnCommand" CommandName="btnInsert" CommandArgument='2,2,0,2,3'><i class="fa fa-sign-in" aria-hidden="true"></i></asp:LinkButton>
                            </td>

                        </tr>
                        <tr style="font-size: Small;">
                            <td>
                                <span id="ContentMain_gvDoingList_lblDocCode_0">CAL0000002</span>
                            </td>
                            <td>
                                <span id="ContentMain_gvDoingList_lblDocCode_0">06/07/2017</span>
                            </td>
                            <td>
                                <span id="ContentMain_gvDoingList_lblCreateDate_0">เครื่องใหม่</span>
                            </td>

                            <td>
                                <span id="ContentMain_gvDoingList_lblCreateDate_0">Completed</span>
                            </td>

                            <td>
                                <asp:LinkButton ID="lbSampleReceive" CssClass="btn-sm btn-default" runat="server" data-original-title="Receive" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEvent"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                            </td>

                        </tr>
                        <tr style="font-size: Small;">
                            <td>
                                <span id="ContentMain_gvDoingList_lblDocCode_0">CAL0000003</span>
                            </td>
                            <td>
                                <span id="ContentMain_gvDoingList_lblDocCode_0">06/07/2017</span>
                            </td>
                            <td>
                                <span id="ContentMain_gvDoingList_lblCreateDate_0">เครื่องเก่า</span>
                            </td>
                            <%--  <td>
                                         <span id="ContentMain_gvDoingList_lblCreateDate_0">xxxxxxx</span>
                                     </td>--%>
                            <%-- <td>
                                         <span id="ContentMain_gvDoingList_lblCreateDate_0"></span>
                                     </td>--%>
                            <td>
                                <span id="ContentMain_gvDoingList_lblCreateDate_0">Waiting</span>
                            </td>
                            <td>
                                <asp:LinkButton ID="LinkButton4" CssClass="btn-sm btn-default" runat="server" data-original-title="Receive" data-toggle="tooltip" OnCommand="btnCommand" CommandName="btnInsert" CommandArgument='2,2,4,5,9'><i class="fa fa-sign-in" aria-hidden="true"></i></asp:LinkButton>
                            </td>

                        </tr>


                    </tbody>
                </table>


            </asp:Panel>

            <asp:Panel ID="genaralLabCal_machine_newCompelte" runat="server" Visible="false">


                <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" id="ContentMain_gvDoingList" style="border-collapse: collapse;">
                    <tbody>
                        <tr class="info" style="font-size: Small; height: 40px;">
                            <th scope="col" style="width: 10%;">รหัสเอกสาร</th>
                            <th scope="col" style="width: 10%;">วันที่สร้างรายการ</th>
                            <th scope="col" style="width: 10%;">เครื่องมือประเภท</th>
                            <%-- <th scope="col" style="width: 12%;">หมายเลขเครื่อง(Serial No.)</th>--%>
                            <%--  <th scope="col" style="width: 10%;">ผู้ถือครอง</th>--%>
                            <th scope="col" style="width: 10%;">status</th>
                            <th scope="col" style="width: 10%;">Action</th>
                            <%--<th scope="col" style="width: 33%;">รายการ</th>
                        <th scope="col" style="width: 12%;">ฝ่ายที่ถูกแจ้ง</th>
                        <th scope="col" style="width: 12%;">ผู้แจ้งปัญหา</th>
                        <th scope="col" style="width: 15%;">สถานะเอกสาร</th> 06/07/2017
                        <th scope="col" style="width: 8%;">รายละเอียด</th>--%>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>
                                <span id="ContentMain_gvDoingList_lblDocCode_0">CAL0000001</span>
                            </td>
                            <td>
                                <span id="ContentMain_gvDoingList_lblDocCode_0">06/07/2017</span>
                            </td>
                            <td>
                                <span id="ContentMain_gvDoingList_lblCreateDate_0">เครื่องใหม่</span>
                            </td>
                            <%--  <td>
                                         <span id="ContentMain_gvDoingList_lblCreateDate_0">xxxxxxx</span>
                                     </td>--%>
                            <td>
                                <span id="ContentMain_gvDoingList_lblCreateDate_0">Completed</span>
                            </td>
                            <%--  <td>
                                         <span id="ContentMain_gvDoingList_lblCreateDate_0">06/07/2017</span>
                                     </td>--%>
                            <td>
                                <asp:LinkButton ID="LinkButton21" CssClass="btn-sm btn-default" runat="server" data-original-title="Receive" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdViewDoc" CommandArgument="8"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                            </td>

                        </tr>
                        <tr style="font-size: Small;">
                            <td>
                                <span id="ContentMain_gvDoingList_lblDocCode_0">CAL0000002</span>
                            </td>
                            <td>
                                <span id="ContentMain_gvDoingList_lblDocCode_0">06/07/2017</span>
                            </td>
                            <td>
                                <span id="ContentMain_gvDoingList_lblCreateDate_0">เครื่องใหม่</span>
                            </td>

                            <td>
                                <span id="ContentMain_gvDoingList_lblCreateDate_0">Completed</span>
                            </td>

                            <td>
                                <asp:LinkButton ID="LinkButton22" CssClass="btn-sm btn-default" runat="server" data-original-title="Receive" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEvent"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                            </td>

                        </tr>
                        <tr style="font-size: Small;">
                            <td>
                                <span id="ContentMain_gvDoingList_lblDocCode_0">CAL0000003</span>
                            </td>
                            <td>
                                <span id="ContentMain_gvDoingList_lblDocCode_0">06/07/2017</span>
                            </td>
                            <td>
                                <span id="ContentMain_gvDoingList_lblCreateDate_0">เครื่องเก่า</span>
                            </td>
                            <%--  <td>
                                         <span id="ContentMain_gvDoingList_lblCreateDate_0">xxxxxxx</span>
                                     </td>--%>
                            <%-- <td>
                                         <span id="ContentMain_gvDoingList_lblCreateDate_0"></span>
                                     </td>--%>
                            <td>
                                <span id="ContentMain_gvDoingList_lblCreateDate_0">Waiting</span>
                            </td>
                            <td>
                                <asp:LinkButton ID="LinkButton23" CssClass="btn-sm btn-default" runat="server" data-original-title="Receive" data-toggle="tooltip" OnCommand="btnCommand" CommandName="btnInsert" CommandArgument='2,2,4,5,9'><i class="fa fa-sign-in" aria-hidden="true"></i></asp:LinkButton>
                            </td>

                        </tr>


                    </tbody>
                </table>


            </asp:Panel>

            <asp:Panel ID="genaralLabCal_machine_oleCompelte" runat="server" Visible="false">


                <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" id="ContentMain_gvDoingList" style="border-collapse: collapse;">
                    <tbody>
                        <tr class="info" style="font-size: Small; height: 40px;">
                            <th scope="col" style="width: 10%;">รหัสเอกสาร</th>
                            <th scope="col" style="width: 10%;">วันที่สร้างรายการ</th>
                            <th scope="col" style="width: 10%;">เครื่องมือประเภท</th>
                            <%-- <th scope="col" style="width: 12%;">หมายเลขเครื่อง(Serial No.)</th>--%>
                            <%--  <th scope="col" style="width: 10%;">ผู้ถือครอง</th>--%>
                            <th scope="col" style="width: 10%;">status</th>
                            <th scope="col" style="width: 10%;">Action</th>
                            <%--<th scope="col" style="width: 33%;">รายการ</th>
                        <th scope="col" style="width: 12%;">ฝ่ายที่ถูกแจ้ง</th>
                        <th scope="col" style="width: 12%;">ผู้แจ้งปัญหา</th>
                        <th scope="col" style="width: 15%;">สถานะเอกสาร</th> 06/07/2017
                        <th scope="col" style="width: 8%;">รายละเอียด</th>--%>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>
                                <span id="ContentMain_gvDoingList_lblDocCode_0">CAL0000001</span>
                            </td>
                            <td>
                                <span id="ContentMain_gvDoingList_lblDocCode_0">06/07/2017</span>
                            </td>
                            <td>
                                <span id="ContentMain_gvDoingList_lblCreateDate_0">เครื่องใหม่</span>
                            </td>
                            <%--  <td>
                                         <span id="ContentMain_gvDoingList_lblCreateDate_0">xxxxxxx</span>
                                     </td>--%>
                            <td>
                                <span id="ContentMain_gvDoingList_lblCreateDate_0">Completed</span>
                            </td>
                            <%--  <td>
                                         <span id="ContentMain_gvDoingList_lblCreateDate_0">06/07/2017</span>
                                     </td>--%>
                            <td>
                                <asp:LinkButton ID="LinkButton31" CssClass="btn-sm btn-default" runat="server" data-original-title="Receive" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdViewDoc" CommandArgument="8"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                            </td>

                        </tr>
                        <tr style="font-size: Small;">
                            <td>
                                <span id="ContentMain_gvDoingList_lblDocCode_0">CAL0000002</span>
                            </td>
                            <td>
                                <span id="ContentMain_gvDoingList_lblDocCode_0">06/07/2017</span>
                            </td>
                            <td>
                                <span id="ContentMain_gvDoingList_lblCreateDate_0">เครื่องใหม่</span>
                            </td>

                            <td>
                                <span id="ContentMain_gvDoingList_lblCreateDate_0">Completed</span>
                            </td>

                            <td>
                                <asp:LinkButton ID="LinkButton32" CssClass="btn-sm btn-default" runat="server" data-original-title="Receive" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEvent"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                            </td>

                        </tr>
                        <tr style="font-size: Small;">
                            <td>
                                <span id="ContentMain_gvDoingList_lblDocCode_0">CAL0000003</span>
                            </td>
                            <td>
                                <span id="ContentMain_gvDoingList_lblDocCode_0">06/07/2017</span>
                            </td>
                            <td>
                                <span id="ContentMain_gvDoingList_lblCreateDate_0">เครื่องเก่า</span>
                            </td>
                            <%--  <td>
                                         <span id="ContentMain_gvDoingList_lblCreateDate_0">xxxxxxx</span>
                                     </td>--%>
                            <%-- <td>
                                         <span id="ContentMain_gvDoingList_lblCreateDate_0"></span>
                                     </td>--%>
                            <td>
                                <span id="ContentMain_gvDoingList_lblCreateDate_0">Completed</span>
                            </td>
                            <td>
                                <asp:LinkButton ID="LinkButton33" CssClass="btn-sm btn-default" runat="server" data-original-title="Receive" data-toggle="tooltip" OnCommand="btnCommand" CommandName="btnInsert" CommandArgument="1,1,3,2,3"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                            </td>

                        </tr>


                    </tbody>
                </table>


            </asp:Panel>
            <%-- </div>--%>
        </asp:View>

        <asp:View ID="tab3" runat="server">

            <%--            <div class="form-horizontal" role="form">--%>
            <asp:Panel ID="tool" runat="server" Visible="true">

                <div class="form-group">
                    <asp:LinkButton ID="LinkButton46" CssClass="btn btn-sm btn-default" runat="server" data-original-title="user" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdView" CommandArgument="0"><i class="fa fa-users" aria-hidden="true"></i> user</asp:LinkButton>
                    <asp:LinkButton ID="LinkButton48" CssClass="btn btn-sm btn-default" runat="server" data-original-title="admin" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdView" CommandArgument="1"><i class="fa fa-user" aria-hidden="true"></i> admin</asp:LinkButton>
                    <asp:LinkButton ID="btnsearchshow" CssClass="btn btn-sm btn-info" runat="server" data-original-title="search" data-toggle="tooltip" OnCommand="btnCommand" CommandName="showsearch"><i class="fa fa-search" aria-hidden="true"></i> ค้นหา</asp:LinkButton>
                    <asp:LinkButton ID="hiddensearch" Visible="false" CssClass="btn btn-sm btn-default" runat="server" data-original-title="search" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdReset" CommandArgument="3"><i class="fa fa-times" aria-hidden="true"></i> ยกเลิกค้นหา</asp:LinkButton>
                </div>
            </asp:Panel>
            <%--   </div>--%>
            <!--*** START Search Index ***-->
            <div id="TabSearch" runat="server" visible="false">

                <div class="panel panel-info">
                    <div class="panel-heading f-bold">ค้นหารายการ</div>
                    <div class="panel-body">
                        <div class="form-horizontal" role="form">

                            <%-------------- Document No, Sample Code --------------%>
                            <div class="form-group">
                                <asp:Label ID="lborg_idxsearch" CssClass="col-sm-2 control-label" runat="server" Text="ชื่อเครื่องมือ : " />
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txt_machine_search" AutoPostBack="true" placeholder="ชื่อเครื่องมือ..." runat="server" CssClass="form-control">
                                    </asp:TextBox>

                                </div>
                                <div class="col-sm-1"></div>
                                <asp:Label ID="lbrdept_idxsearch" CssClass="col-sm-2 control-label" runat="server" Text="รหัสเครื่องมือ : " />
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txt_sample_codesearch" AutoPostBack="true" placeholder="รหัสเครื่องมือ ..." runat="server" CssClass="form-control">
                                              
                                    </asp:TextBox>

                                </div>

                            </div>
                            <%-------------- Document No, Sample Code --------------%>

                            <%-------------- Mat., เลขตู้  --------------%>
                            <div class="form-group">
                                <asp:Label ID="Label1" CssClass="col-sm-2 control-label" runat="server" Text="หมายเลขเครื่อง : " />
                                <div class="col-sm-3">
                                    <asp:TextBox ID="TextBox1" AutoPostBack="true" placeholder="หมายเลขเครื่อง ..." runat="server" CssClass="form-control">
                                    </asp:TextBox>

                                </div>
                                <div class="col-sm-1"></div>
                                <asp:Label ID="Label2" CssClass="col-sm-2 control-label" runat="server" Text="ผู้ถือครอง : " />
                                <div class="col-sm-3">
                                    <asp:TextBox ID="TextBox2" AutoPostBack="true" placeholder="ผู้ถือครอง ..." runat="server" CssClass="form-control">
                                              
                                    </asp:TextBox>

                                </div>

                            </div>

                            <div class="form-group">
                                <asp:Label ID="Label3" CssClass="col-sm-2 control-label" runat="server" Text="Cal Date : " />
                                <div class="col-sm-3">
                                    <asp:TextBox ID="TextBox19" AutoPostBack="true" placeholder="Cal Date ..." runat="server" CssClass="form-control">
                                    </asp:TextBox>

                                </div>
                                <div class="col-sm-1"></div>
                                <asp:Label ID="Label4" CssClass="col-sm-2 control-label" runat="server" Text="Due Date : " />
                                <div class="col-sm-3">
                                    <asp:TextBox ID="TextBox21" AutoPostBack="true" placeholder="Due Date ..." runat="server" CssClass="form-control">
                                              
                                    </asp:TextBox>

                                </div>

                            </div>
                            <%-------------- Mat., เลขตู้ --------------%>

                            <%-------------- btnsearch --------------%>
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10">
                                <div class="form-group">
                                    <%--<div class="col-sm-12"></div>--%>
                                    <div class="pull-left">
                                        <asp:LinkButton ID="btnSearchIndex1" CssClass="btn btn-primary" runat="server" ValidationGroup="SearchIndex" OnCommand="btnCommand" CommandArgument="0" CommandName="cmdSearch" data-toggle="tooltip" title="Search"><span class="glyphicon glyphicon-search"></span></asp:LinkButton>


                                        <asp:LinkButton ID="btnCancelIndex_First" CssClass="btn btn-default" runat="server" OnCommand="btnCommand" CommandName="cmdReset" CommandArgument="0" data-toggle="tooltip" title="refresh"><i class="fa fa-refresh"></i></asp:LinkButton>
                                    </div>

                                </div>
                            </div>
                            <%-------------- btnsearch --------------%>
                        </div>
                    </div>

                </div>
            </div>


            <asp:Panel ID="detailsUser" runat="server" Visible="false">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">รายละเอียดผู้ใช้งาน</h3>
                    </div>
                    <!--user info-->
                    <div class="panel-body">
                        <div class="form-horizontal" role="form">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">รหัสพนักงาน</label>
                                <div class="col-sm-4">
                                    <asp:TextBox ID="TextBox3" runat="server" CssClass="form-control" Text='6000xxxx' Enabled="false" />
                                </div>
                                <label class="col-sm-2 control-label">ชื่อ - นามสกุล</label>
                                <div class="col-sm-4">
                                    <asp:TextBox ID="TextBox4" runat="server" CssClass="form-control" Text='ทดสอบ ทดสอบ' Enabled="false" />
                                </div>
                            </div>
                            <div class="form-group">
                                <%-- <label class="col-sm-2 control-label">ชื่อ - นามสกุล</label>
                                <div class="col-sm-4">
                                    <asp:TextBox ID="TextBox4" runat="server" CssClass="form-control" Text='ทดสอบ ทดสอบ' Enabled="false" />
                                </div>--%>
                                <label class="col-sm-2 control-label">องค์กร</label>
                                <div class="col-sm-4">
                                    <asp:TextBox ID="TextBox5" runat="server" CssClass="form-control" Text='องค์กรทดสอบ' Enabled="false" />
                                </div>
                                <label class="col-sm-2 control-label">ฝ่าย</label>
                                <div class="col-sm-4">
                                    <asp:TextBox ID="TextBox6" runat="server" CssClass="form-control" Text='ทดสอบ ทดสอบ' Enabled="false" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">แผนก</label>
                                <div class="col-sm-4">
                                    <asp:TextBox ID="TextBox7" runat="server" CssClass="form-control" Text='ทดสอบ ทดสอบ' Enabled="false" />
                                </div>
                                <label class="col-sm-2 control-label">ตำแหน่ง</label>
                                <div class="col-sm-4">
                                    <asp:TextBox ID="TextBox8" runat="server" CssClass="form-control" Text='ทดสอบ ทดสอบ' Enabled="false" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--user info-->
                </div>

            </asp:Panel>


            <asp:Panel ID="genaral_machine_userview" runat="server" Visible="false">
                <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                    <tbody>
                        <tr class="info" style="font-size: Small; height: 40px;">
                            <th scope="col" style="width: 2%;">ลำดับ</th>
                            <th scope="col" style="width: 10%;">ประเภทเครื่องมือ</th>
                            <th scope="col" style="width: 12%;">ชื่อเครื่องมือ</th>
                            <th scope="col" style="width: 10%;">รหัสเครื่อง</th>
                            <th scope="col" style="width: 15%;">หมายเลขเครื่อง</th>
                            <th scope="col" style="width: 10%;">ผู้ถือครอง</th>
                            <th scope="col" style="width: 10%;">สถานะอุปกรณ์</th>
                            <th scope="col" style="width: 14%;">สถานะดำเนินการอื่นๆ</th>
                            <th scope="col" style="width: 15%;">การจัดการ</th>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>1</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1001 E</td>
                            <td>10092883707</td>
                            <td>QA LAB</td>
                            <td><i class="fa fa-circle" style="color: lime; font-size: 10px;" aria-hidden="true"></i>&nbsp;Online</td>
                            <td>None</td>
                            <td>

                                <asp:LinkButton ID="LinkButton45" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton43" CssClass="btn-sm btn-default" runat="server" data-original-title="transfer machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,1,0,2,2"><i class="glyphicon glyphicon-transfer" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton52" CssClass="btn-sm btn-default" runat="server" data-original-title="cut off machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,7,0,2,8"><i class="fa fa-cut" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                        </tr>

                    </tbody>
                </table>
            </asp:Panel>

            <asp:Panel ID="genaral_machinelist" runat="server">


                <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                    <tbody>
                        <tr class="info" style="font-size: Small; height: 40px;">
                            <th scope="col" style="width: 2%;">ลำดับ</th>
                            <th scope="col" style="width: 10%;">ประเภทเครื่องมือ</th>
                            <th scope="col" style="width: 12%;">ชื่อเครื่องมือ</th>
                            <th scope="col" style="width: 10%;">รหัสเครื่อง</th>
                            <th scope="col" style="width: 15%;">หมายเลขเครื่อง</th>
                            <th scope="col" style="width: 10%;">ผู้ถือครอง</th>
                            <th scope="col" style="width: 10%;">สถานะอุปกรณ์</th>
                            <th scope="col" style="width: 14%;">สถานะดำเนินการอื่นๆ</th>
                            <th scope="col" style="width: 15%;">การจัดการ</th>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>1</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1001 E</td>
                            <td>10092883707</td>
                            <td>QA LAB</td>
                            <td><i class="fa fa-circle" style="color: lime; font-size: 10px;" aria-hidden="true"></i>&nbsp;Online</td>
                            <td>None</td>
                            <td>
                                <asp:LinkButton ID="LinkButton132" CssClass="btn-sm btn-default" runat="server" data-original-title="edit" data-toggle="tooltip"><i class="fa fa-edit" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton134" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton37" CssClass="btn-sm btn-default" runat="server" data-original-title="transfer machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,1,0,2,2"><i class="glyphicon glyphicon-transfer" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton40" CssClass="btn-sm btn-default" runat="server" data-original-title="cut off machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,7,0,2,8"><i class="fa fa-cut" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>2</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1002 E</td>
                            <td>10092883797</td>
                            <td>QA PL4</td>
                            <td><i class="fa fa-circle" style="color: lime; font-size: 10px;" aria-hidden="true"></i>&nbsp;Online</td>
                            <td>None</td>
                            <td>
                                <asp:LinkButton ID="LinkButton131" CssClass="btn-sm btn-default" runat="server" data-original-title="edit" data-toggle="tooltip"><i class="fa fa-edit" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton174" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton38" CssClass="btn-sm btn-default" runat="server" data-original-title="transfer machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,1,0,2,2"><i class="glyphicon glyphicon-transfer" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton41" CssClass="btn-sm btn-default" runat="server" data-original-title="cut off machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,7,0,2,8"><i class="fa fa-cut" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>3</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1003 E</td>
                            <td>10092883787</td>
                            <td>PD 1 SC</td>
                            <td><i class="fa fa-circle" style="color: lime; font-size: 10px;" aria-hidden="true"></i>&nbsp;Online</td>
                            <td>None</td>
                            <td>
                                <asp:LinkButton ID="LinkButton130" CssClass="btn-sm btn-default" runat="server" data-original-title="edit" data-toggle="tooltip"><i class="fa fa-edit" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton175" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton39" CssClass="btn-sm btn-default" runat="server" data-original-title="transfer machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,1,0,2,2"> <i class="glyphicon glyphicon-transfer" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton42" CssClass="btn-sm btn-default" runat="server" data-original-title="cut off machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,7,0,2,8"><i class="fa fa-cut" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>4</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1004 E</td>
                            <td>10092883777</td>
                            <td>QA IN</td>
                            <td><i class="fa fa-circle" style="color: red; font-size: 10px;" aria-hidden="true"></i>&nbsp;Offline</td>

                            <td>cut off machine</td>
                            <td>
                                <asp:LinkButton ID="LinkButton177" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <%-- <asp:LinkButton ID="LinkButton43" CssClass="btn-sm btn-default" runat="server" data-original-title="cut off machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,7,0,2,8"><i class="fa fa-cut" aria-hidden="true"></i></asp:LinkButton>--%>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>

            <asp:Panel ID="Waiting_Head_User" runat="server">


                <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                    <tbody>
                        <tr class="info" style="font-size: Small; height: 40px;">
                            <th scope="col" style="width: 2%;">ลำดับ</th>
                            <th scope="col" style="width: 10%;">ประเภทเครื่องมือ</th>
                            <th scope="col" style="width: 12%;">ชื่อเครื่องมือ</th>
                            <th scope="col" style="width: 10%;">รหัสเครื่อง</th>
                            <th scope="col" style="width: 15%;">หมายเลขเครื่อง</th>
                            <th scope="col" style="width: 10%;">ผู้ถือครอง</th>
                            <th scope="col" style="width: 10%;">สถานะอุปกรณ์</th>
                            <th scope="col" style="width: 14%;">สถานะดำเนินการอื่นๆ</th>
                            <th scope="col" style="width: 15%;">การจัดการ</th>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>1</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1001 E</td>
                            <td>10092883707</td>
                            <td>QA LAB</td>
                            <td><i class="fa fa-circle" style="color: lime; font-size: 10px;" aria-hidden="true"></i>&nbsp;Online</td>
                            <td>Waiting Head User (transfer)</td>
                            <td>
                                <asp:LinkButton ID="LinkButton129" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton55" CssClass="btn-sm btn-default" runat="server" data-original-title="Head user" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="2,2,0,3,3"><i class="fa fa-user" aria-hidden="true"></i></asp:LinkButton>
                                <%-- <asp:LinkButton ID="LinkButton56" CssClass="btn-sm btn-default" runat="server" data-original-title="remove machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="5"><i class="fa fa-cut" aria-hidden="true"></i></asp:LinkButton>--%>
                            </td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>2</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1002 E</td>
                            <td>10092883797</td>
                            <td>QA PL4</td>
                            <td><i class="fa fa-circle" style="color: lime; font-size: 10px;" aria-hidden="true"></i>&nbsp;Online</td>
                            <td>None</td>
                            <td>
                                <asp:LinkButton ID="LinkButton133" CssClass="btn-sm btn-default" runat="server" data-original-title="edit" data-toggle="tooltip"><i class="fa fa-edit" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton128" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton57" CssClass="btn-sm btn-default" runat="server" data-original-title="transfer machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,1,0,2,2"><i class="glyphicon glyphicon-transfer" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton58" CssClass="btn-sm btn-default" runat="server" data-original-title="cut off machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,7,0,2,8"><i class="fa fa-cut" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>3</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1003 E</td>
                            <td>10092883787</td>
                            <td>PD 1 SC</td>
                            <td><i class="fa fa-circle" style="color: lime; font-size: 10px;" aria-hidden="true"></i>&nbsp;Online</td>
                            <td>None</td>
                            <td>
                                <asp:LinkButton ID="LinkButton49" CssClass="btn-sm btn-default" runat="server" data-original-title="edit" data-toggle="tooltip"><i class="fa fa-edit" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton176" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton59" CssClass="btn-sm btn-default" runat="server" data-original-title="transfer machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,1,0,2,2"><i class="glyphicon glyphicon-transfer" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton60" CssClass="btn-sm btn-default" runat="server" data-original-title="cut off machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,7,0,2,8"><i class="fa fa-cut" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>4</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1004 E</td>
                            <td>10092883777</td>
                            <td>QA IN</td>
                            <td><i class="fa fa-circle" style="color: red; font-size: 10px;" aria-hidden="true"></i>&nbsp;Offline</td>

                            <td>cut off machine</td>
                            <td>
                                <asp:LinkButton ID="LinkButton61" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <%-- <asp:LinkButton ID="LinkButton62" CssClass="btn-sm btn-default" runat="server" data-original-title="cut machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="5"><i class="fa fa-cut" aria-hidden="true"></i></asp:LinkButton>--%>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>

            <asp:Panel ID="Waiting_Qa_tranfer" runat="server" Visible="false">


                <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                    <tbody>
                        <tr class="info" style="font-size: Small; height: 40px;">
                            <th scope="col" style="width: 2%;">ลำดับ</th>
                            <th scope="col" style="width: 10%;">ประเภทเครื่องมือ</th>
                            <th scope="col" style="width: 12%;">ชื่อเครื่องมือ</th>
                            <th scope="col" style="width: 10%;">รหัสเครื่อง</th>
                            <th scope="col" style="width: 15%;">หมายเลขเครื่อง</th>
                            <th scope="col" style="width: 10%;">ผู้ถือครอง</th>
                            <th scope="col" style="width: 10%;">สถานะอุปกรณ์</th>
                            <th scope="col" style="width: 14%;">สถานะดำเนินการอื่นๆ</th>
                            <th scope="col" style="width: 15%;">การจัดการ</th>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>1</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1001 E</td>
                            <td>10092883707</td>
                            <td>QA LAB</td>
                            <td><i class="fa fa-circle" style="color: lime; font-size: 10px;" aria-hidden="true"></i>&nbsp;Online</td>
                            <td>Waiting QA (transfer)</td>
                            <td>
                                <asp:LinkButton ID="LinkButton135" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton63" CssClass="btn-sm btn-default" runat="server" data-original-title="remove machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="3,3,0,4,4"><i class="fa fa-user" aria-hidden="true"></i></asp:LinkButton>
                                <%-- <asp:LinkButton ID="LinkButton64" CssClass="btn-sm btn-default" runat="server" data-original-title="remove machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="5"><i class="fa fa-cut" aria-hidden="true"></i></asp:LinkButton>--%>
                            </td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>2</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1002 E</td>
                            <td>10092883797</td>
                            <td>QA PL4</td>
                            <td><i class="fa fa-circle" style="color: lime; font-size: 10px;" aria-hidden="true"></i>&nbsp;Online</td>
                            <td>None</td>
                            <td>
                                <asp:LinkButton ID="LinkButton136" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton62" CssClass="btn-sm btn-default" runat="server" data-original-title="transfer machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,1,0,2,2"><i class="glyphicon glyphicon-transfer" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton65" CssClass="btn-sm btn-default" runat="server" data-original-title="cut off machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,7,0,2,8"><i class="fa fa-cut" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>3</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1003 E</td>
                            <td>10092883787</td>
                            <td>PD 1 SC</td>

                            <td><i class="fa fa-circle" style="color: lime; font-size: 10px;" aria-hidden="true"></i>&nbsp;Online</td>
                            <td>None</td>
                            <td>
                                <asp:LinkButton ID="LinkButton137" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton66" CssClass="btn-sm btn-default" runat="server" data-original-title="transfer machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,1,0,2,2"><i class="glyphicon glyphicon-transfer" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton67" CssClass="btn-sm btn-default" runat="server" data-original-title="cut off machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,7,0,2,8"><i class="fa fa-cut" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>4</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1004 E</td>
                            <td>10092883777</td>
                            <td>QA IN</td>
                            <td><i class="fa fa-circle" style="color: red; font-size: 10px;" aria-hidden="true"></i>&nbsp;Offline</td>

                            <td>cut off machine</td>
                            <td>
                                <asp:LinkButton ID="LinkButton69" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <%-- <asp:LinkButton ID="LinkButton62" CssClass="btn-sm btn-default" runat="server" data-original-title="cut machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="5"><i class="fa fa-cut" aria-hidden="true"></i></asp:LinkButton>--%>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>

            <asp:Panel ID="Waiting_Head_Qa_tranfer" runat="server" Visible="false">


                <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                    <tbody>
                        <tr class="info" style="font-size: Small; height: 40px;">
                            <th scope="col" style="width: 2%;">ลำดับ</th>
                            <th scope="col" style="width: 10%;">ประเภทเครื่องมือ</th>
                            <th scope="col" style="width: 12%;">ชื่อเครื่องมือ</th>
                            <th scope="col" style="width: 10%;">รหัสเครื่อง</th>
                            <th scope="col" style="width: 15%;">หมายเลขเครื่อง</th>
                            <th scope="col" style="width: 10%;">ผู้ถือครอง</th>
                            <th scope="col" style="width: 10%;">สถานะอุปกรณ์</th>
                            <th scope="col" style="width: 14%;">สถานะดำเนินการอื่นๆ</th>
                            <th scope="col" style="width: 15%;">การจัดการ</th>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>1</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1001 E</td>
                            <td>10092883707</td>
                            <td>QA LAB</td>
                            <td><i class="fa fa-circle" style="color: lime; font-size: 10px;" aria-hidden="true"></i>&nbsp;Online</td>
                            <td>Waiting Head QA (transfer)</td>
                            <td>
                                <asp:LinkButton ID="LinkButton138" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton71" CssClass="btn-sm btn-default" runat="server" data-original-title="remove machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="4,4,0,5,5"><i class="fa fa-user" aria-hidden="true"></i></asp:LinkButton>
                                <%-- <asp:LinkButton ID="LinkButton72" CssClass="btn-sm btn-default" runat="server" data-original-title="remove machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="5"><i class="fa fa-cut" aria-hidden="true"></i></asp:LinkButton>--%>
                            </td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>2</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1002 E</td>
                            <td>10092883797</td>
                            <td>QA PL4</td>
                            <td><i class="fa fa-circle" style="color: lime; font-size: 10px;" aria-hidden="true"></i>&nbsp;Online</td>
                            <td>None</td>
                            <td>
                                <asp:LinkButton ID="LinkButton147" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton68" CssClass="btn-sm btn-default" runat="server" data-original-title="transfer machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,1,0,2,2"><i class="glyphicon glyphicon-transfer" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton73" CssClass="btn-sm btn-default" runat="server" data-original-title="cut off machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,7,0,2,8"><i class="fa fa-cut" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>3</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1003 E</td>
                            <td>10092883787</td>
                            <td>PD 1 SC</td>
                            <td><i class="fa fa-circle" style="color: lime; font-size: 10px;" aria-hidden="true"></i>&nbsp;Online</td>
                            <td>None</td>
                            <td>
                                <asp:LinkButton ID="LinkButton148" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton74" CssClass="btn-sm btn-default" runat="server" data-original-title="transfer machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,1,0,2,2"><i class="glyphicon glyphicon-transfer" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton75" CssClass="btn-sm btn-default" runat="server" data-original-title="cut off machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,7,0,2,8"><i class="fa fa-cut" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>4</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1004 E</td>
                            <td>10092883777</td>
                            <td>QA IN</td>
                            <td><i class="fa fa-circle" style="color: red; font-size: 10px;" aria-hidden="true"></i>&nbsp;Offline</td>
                            <td>cut off machine</td>
                            <td>
                                <asp:LinkButton ID="LinkButton70" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <%-- <asp:LinkButton ID="LinkButton62" CssClass="btn-sm btn-default" runat="server" data-original-title="cut machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="5"><i class="fa fa-cut" aria-hidden="true"></i></asp:LinkButton>--%>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>

            <asp:Panel ID="Waiting_Receive_Machine" runat="server" Visible="false">


                <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                    <tbody>
                        <tr class="info" style="font-size: Small; height: 40px;">
                            <th scope="col" style="width: 2%;">ลำดับ</th>
                            <th scope="col" style="width: 10%;">ประเภทเครื่องมือ</th>
                            <th scope="col" style="width: 12%;">ชื่อเครื่องมือ</th>
                            <th scope="col" style="width: 10%;">รหัสเครื่อง</th>
                            <th scope="col" style="width: 15%;">หมายเลขเครื่อง</th>
                            <th scope="col" style="width: 10%;">ผู้ถือครอง</th>
                            <th scope="col" style="width: 10%;">สถานะอุปกรณ์</th>
                            <th scope="col" style="width: 14%;">สถานะดำเนินการอื่นๆ</th>
                            <th scope="col" style="width: 15%;">การจัดการ</th>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>1</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1001 E</td>
                            <td>10092883707</td>
                            <td>QA LAB</td>
                            <td><i class="fa fa-circle" style="color: lime; font-size: 10px;" aria-hidden="true"></i>&nbsp;Online</td>
                            <td>Waiting Receive Machine (transfer)</td>
                            <td>
                                <asp:LinkButton ID="LinkButton149" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton79" CssClass="btn-sm btn-default" runat="server" data-original-title="remove machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="5,5,0,5,6"><i class="fa fa-user" aria-hidden="true"></i></asp:LinkButton>
                                <%-- <asp:LinkButton ID="LinkButton80" CssClass="btn-sm btn-default" runat="server" data-original-title="remove machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="5"><i class="fa fa-cut" aria-hidden="true"></i></asp:LinkButton>--%>
                            </td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>2</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1002 E</td>
                            <td>10092883797</td>
                            <td>QA PL4</td>
                            <td><i class="fa fa-circle" style="color: lime; font-size: 10px;" aria-hidden="true"></i>&nbsp;Online</td>
                            <td>None</td>
                            <td>
                                <asp:LinkButton ID="LinkButton150" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton81" CssClass="btn-sm btn-default" runat="server" data-original-title="transfer machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,1,0,2,2"><i class="glyphicon glyphicon-transfer" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton82" CssClass="btn-sm btn-default" runat="server" data-original-title="cut off machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,7,0,2,8"><i class="fa fa-cut" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>3</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1003 E</td>
                            <td>10092883787</td>
                            <td>PD 1 SC</td>
                            <td><i class="fa fa-circle" style="color: lime; font-size: 10px;" aria-hidden="true"></i>&nbsp;Online</td>
                            <td>None</td>
                            <td>
                                <asp:LinkButton ID="LinkButton151" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton83" CssClass="btn-sm btn-default" runat="server" data-original-title="transfer machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,1,0,2,2"><i class="glyphicon glyphicon-transfer" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton84" CssClass="btn-sm btn-default" runat="server" data-original-title="cut off machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,7,0,2,8"><i class="fa fa-cut" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>4</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1004 E</td>
                            <td>10092883777</td>
                            <td>QA IN</td>
                            <td><i class="fa fa-circle" style="color: red; font-size: 10px;" aria-hidden="true"></i>&nbsp;Offline</td>
                            <td>cut off machine</td>
                            <td>
                                <asp:LinkButton ID="LinkButton76" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <%-- <asp:LinkButton ID="LinkButton62" CssClass="btn-sm btn-default" runat="server" data-original-title="cut machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="5"><i class="fa fa-cut" aria-hidden="true"></i></asp:LinkButton>--%>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>

            <asp:Panel ID="Receive_Machine_Tranfer_Complete" runat="server" Visible="false">


                <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                    <tbody>
                        <tr class="info" style="font-size: Small; height: 40px;">
                            <th scope="col" style="width: 2%;">ลำดับ</th>
                            <th scope="col" style="width: 10%;">ประเภทเครื่องมือ</th>
                            <th scope="col" style="width: 12%;">ชื่อเครื่องมือ</th>
                            <th scope="col" style="width: 10%;">รหัสเครื่อง</th>
                            <th scope="col" style="width: 15%;">หมายเลขเครื่อง</th>
                            <th scope="col" style="width: 10%;">ผู้ถือครอง</th>
                            <th scope="col" style="width: 10%;">สถานะอุปกรณ์</th>
                            <th scope="col" style="width: 14%;">สถานะดำเนินการอื่นๆ</th>
                            <th scope="col" style="width: 15%;">การจัดการ</th>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>1</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1001 E</td>
                            <td>10092883707</td>
                            <td>ทดสอบ ทดสอบ</td>
                            <td><i class="fa fa-circle" style="color: lime; font-size: 10px;" aria-hidden="true"></i>&nbsp;Online</td>
                            <td>transfer completed</td>
                            <td>
                                <asp:LinkButton ID="LinkButton152" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton87" CssClass="btn-sm btn-default" runat="server" data-original-title="transfer machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,1,0,2,2"><i class="glyphicon glyphicon-transfer" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton88" CssClass="btn-sm btn-default" runat="server" data-original-title="cut off machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,7,0,2,8"><i class="fa fa-cut" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>2</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1002 E</td>
                            <td>10092883797</td>
                            <td>QA PL4</td>
                            <td><i class="fa fa-circle" style="color: lime; font-size: 10px;" aria-hidden="true"></i>&nbsp;Online</td>
                            <td>None</td>
                            <td>
                                <asp:LinkButton ID="LinkButton153" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton89" CssClass="btn-sm btn-default" runat="server" data-original-title="transfer machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,1,0,2,2"><i class="glyphicon glyphicon-transfer" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton90" CssClass="btn-sm btn-default" runat="server" data-original-title="cut off machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,7,0,2,8"><i class="fa fa-cut" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>3</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1003 E</td>
                            <td>10092883787</td>
                            <td>PD 1 SC</td>
                            <td><i class="fa fa-circle" style="color: lime; font-size: 10px;" aria-hidden="true"></i>&nbsp;Online</td>
                            <td>None</td>
                            <td>
                                <asp:LinkButton ID="LinkButton154" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton91" CssClass="btn-sm btn-default" runat="server" data-original-title="transfer machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,1,0,2,2"><i class="glyphicon glyphicon-transfer" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton92" CssClass="btn-sm btn-default" runat="server" data-original-title="cut off machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,7,0,2,8"><i class="fa fa-cut" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>4</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1004 E</td>
                            <td>10092883777</td>
                            <td>QA IN</td>
                            <td><i class="fa fa-circle" style="color: red; font-size: 10px;" aria-hidden="true"></i>&nbsp;Offline</td>
                            <td>cut off machine</td>
                            <td>
                                <asp:LinkButton ID="LinkButton77" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <%-- <asp:LinkButton ID="LinkButton62" CssClass="btn-sm btn-default" runat="server" data-original-title="cut machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="5"><i class="fa fa-cut" aria-hidden="true"></i></asp:LinkButton>--%>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>

            <asp:Panel ID="Machine_Tranfer_NotComplete" runat="server" Visible="false">


                <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                    <tbody>
                        <tr class="info" style="font-size: Small; height: 40px;">
                            <th scope="col" style="width: 2%;">ลำดับ</th>
                            <th scope="col" style="width: 10%;">ประเภทเครื่องมือ</th>
                            <th scope="col" style="width: 12%;">ชื่อเครื่องมือ</th>
                            <th scope="col" style="width: 10%;">รหัสเครื่อง</th>
                            <th scope="col" style="width: 15%;">หมายเลขเครื่อง</th>
                            <th scope="col" style="width: 10%;">ผู้ถือครอง</th>
                            <th scope="col" style="width: 10%;">สถานะอุปกรณ์</th>
                            <th scope="col" style="width: 14%;">สถานะดำเนินการอื่นๆ</th>
                            <th scope="col" style="width: 15%;">การจัดการ</th>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>1</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1001 E</td>
                            <td>10092883707</td>
                            <td>QA LAB</td>
                            <td><i class="fa fa-circle" style="color: lime; font-size: 10px;" aria-hidden="true"></i>&nbsp;Online</td>
                            <td>Cancelled tranfer</td>
                            <td>
                                <asp:LinkButton ID="LinkButton155" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton95" CssClass="btn-sm btn-default" runat="server" data-original-title="transfer machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,1,0,2,2"><i class="glyphicon glyphicon-transfer" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton96" CssClass="btn-sm btn-default" runat="server" data-original-title="cut off machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,7,0,2,8"><i class="fa fa-cut" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>2</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1002 E</td>
                            <td>10092883797</td>
                            <td>QA PL4</td>
                            <td><i class="fa fa-circle" style="color: lime; font-size: 10px;" aria-hidden="true"></i>&nbsp;Online</td>
                            <td>None</td>
                            <td>
                                <asp:LinkButton ID="LinkButton156" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton97" CssClass="btn-sm btn-default" runat="server" data-original-title="transfer machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,1,0,2,2"><i class="glyphicon glyphicon-transfer" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton98" CssClass="btn-sm btn-default" runat="server" data-original-title="cut off machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,7,0,2,8"><i class="fa fa-cut" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>3</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1003 E</td>
                            <td>10092883787</td>
                            <td>PD 1 SC</td>
                            <td><i class="fa fa-circle" style="color: lime; font-size: 10px;" aria-hidden="true"></i>&nbsp;Online</td>
                            <td>None</td>
                            <td>
                                <asp:LinkButton ID="LinkButton157" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton99" CssClass="btn-sm btn-default" runat="server" data-original-title="transfer machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,1,0,2,2"><i class="glyphicon glyphicon-transfer" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton100" CssClass="btn-sm btn-default" runat="server" data-original-title="cut off machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,7,0,2,8"><i class="fa fa-cut" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>4</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1004 E</td>
                            <td>10092883777</td>
                            <td>QA IN</td>
                            <td><i class="fa fa-circle" style="color: red; font-size: 10px;" aria-hidden="true"></i>&nbsp;Offline</td>
                            <td>cut off machine</td>
                            <td>
                                <asp:LinkButton ID="LinkButton85" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <%-- <asp:LinkButton ID="LinkButton62" CssClass="btn-sm btn-default" runat="server" data-original-title="cut machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="5"><i class="fa fa-cut" aria-hidden="true"></i></asp:LinkButton>--%>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>

            <asp:Panel ID="showSearch" runat="server" Visible="false">

                <div class="form-group">
                    <a href='<%=ResolveUrl("~/print-tool") %>' id="btnprint" class="btn btn-sm btn-default" target="_blank"><i class="fa fa-print" aria-hidden="true"></i>Print</a>
                </div>
                <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                    <tbody>
                        <tr class="info" style="font-size: Small; height: 40px;">
                            <th scope="col" style="width: 2%;">ลำดับ</th>
                            <th scope="col" style="width: 10%;">ประเภทเครื่องมือ</th>
                            <th scope="col" style="width: 12%;">ชื่อเครื่องมือ</th>
                            <th scope="col" style="width: 10%;">รหัสเครื่อง</th>
                            <th scope="col" style="width: 15%;">หมายเลขเครื่อง</th>
                            <th scope="col" style="width: 10%;">ผู้ถือครอง</th>
                            <th scope="col" style="width: 10%;">สถานะอุปกรณ์</th>
                            <th scope="col" style="width: 14%;">สถานะดำเนินการอื่นๆ</th>
                            <th scope="col" style="width: 15%;">การจัดการ</th>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>1</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1001 E</td>
                            <td>10092883707</td>
                            <td>QA LAB</td>
                            <td><i class="fa fa-circle" style="color: lime; font-size: 10px;" aria-hidden="true"></i>&nbsp;Online</td>
                            <td>None</td>
                            <td>
                                <asp:LinkButton ID="LinkButton158" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton51" CssClass="btn-sm btn-default" runat="server" data-original-title="transfer machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,1,0,2,2"><i class="glyphicon glyphicon-transfer" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton53" CssClass="btn-sm btn-default" runat="server" data-original-title="cut off machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,7,0,2,8"><i class="fa fa-cut" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                        </tr>

                    </tbody>
                </table>
            </asp:Panel>

            <asp:Panel ID="Waiting_Head_User_CutOff" runat="server" Visible="false">


                <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                    <tbody>
                        <tr class="info" style="font-size: Small; height: 40px;">
                            <th scope="col" style="width: 2%;">ลำดับ</th>
                            <th scope="col" style="width: 10%;">ประเภทเครื่องมือ</th>
                            <th scope="col" style="width: 12%;">ชื่อเครื่องมือ</th>
                            <th scope="col" style="width: 10%;">รหัสเครื่อง</th>
                            <th scope="col" style="width: 15%;">หมายเลขเครื่อง</th>
                            <th scope="col" style="width: 10%;">ผู้ถือครอง</th>
                            <th scope="col" style="width: 10%;">สถานะอุปกรณ์</th>
                            <th scope="col" style="width: 14%;">สถานะดำเนินการอื่นๆ</th>
                            <th scope="col" style="width: 15%;">การจัดการ</th>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>1</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1001 E</td>
                            <td>10092883707</td>
                            <td>QA LAB</td>
                            <td><i class="fa fa-circle" style="color: lime; font-size: 10px;" aria-hidden="true"></i>&nbsp;Online</td>
                            <td>None</td>
                            <td>
                                <asp:LinkButton ID="LinkButton159" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton103" CssClass="btn-sm btn-default" runat="server" data-original-title="transfer machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,1,0,2,2"><i class="glyphicon glyphicon-transfer" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton104" CssClass="btn-sm btn-default" runat="server" data-original-title="cut off machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,7,0,2,8"><i class="fa fa-cut" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>2</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1002 E</td>
                            <td>10092883797</td>
                            <td>QA PL4</td>
                            <td><i class="fa fa-circle" style="color: lime; font-size: 10px;" aria-hidden="true"></i>&nbsp;Online</td>
                            <td>Waiting Head User (cut off)</td>
                            <td>
                                <asp:LinkButton ID="LinkButton160" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <%--  <asp:LinkButton ID="LinkButton105" CssClass="btn-sm btn-default" runat="server" data-original-title="remove machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="2,2,0,3,3"><i class="fa fa-pencil" aria-hidden="true"></i></asp:LinkButton>--%>
                                <asp:LinkButton ID="LinkButton106" CssClass="btn-sm btn-default" runat="server" data-original-title="cut machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="2,8,0,3,9"><i class="fa fa-user" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>3</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1003 E</td>
                            <td>10092883787</td>
                            <td>PD 1 SC</td>
                            <td><i class="fa fa-circle" style="color: lime; font-size: 10px;" aria-hidden="true"></i>&nbsp;Online</td>
                            <td>None</td>
                            <td>
                                <asp:LinkButton ID="LinkButton161" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton107" CssClass="btn-sm btn-default" runat="server" data-original-title="transfer machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,1,0,2,2"><i class="glyphicon glyphicon-transfer" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton108" CssClass="btn-sm btn-default" runat="server" data-original-title="cut off machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,7,0,2,8"><i class="fa fa-cut" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>4</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1004 E</td>
                            <td>10092883777</td>
                            <td>QA IN</td>
                            <td><i class="fa fa-circle" style="color: red; font-size: 10px;" aria-hidden="true"></i>&nbsp;Offline</td>
                            <td>cut off machine</td>
                            <td>
                                <asp:LinkButton ID="LinkButton86" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <%-- <asp:LinkButton ID="LinkButton62" CssClass="btn-sm btn-default" runat="server" data-original-title="cut machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="5"><i class="fa fa-cut" aria-hidden="true"></i></asp:LinkButton>--%>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>

            <asp:Panel ID="Waiting_QA_cutoff" runat="server" Visible="false">


                <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                    <tbody>
                        <tr class="info" style="font-size: Small; height: 40px;">
                            <th scope="col" style="width: 2%;">ลำดับ</th>
                            <th scope="col" style="width: 10%;">ประเภทเครื่องมือ</th>
                            <th scope="col" style="width: 12%;">ชื่อเครื่องมือ</th>
                            <th scope="col" style="width: 10%;">รหัสเครื่อง</th>
                            <th scope="col" style="width: 15%;">หมายเลขเครื่อง</th>
                            <th scope="col" style="width: 10%;">ผู้ถือครอง</th>
                            <th scope="col" style="width: 10%;">สถานะอุปกรณ์</th>
                            <th scope="col" style="width: 14%;">สถานะดำเนินการอื่นๆ</th>
                            <th scope="col" style="width: 15%;">การจัดการ</th>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>1</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1001 E</td>
                            <td>10092883707</td>
                            <td>QA LAB</td>
                            <td><i class="fa fa-circle" style="color: lime; font-size: 10px;" aria-hidden="true"></i>&nbsp;Online</td>
                            <td>None</td>
                            <td>
                                <asp:LinkButton ID="LinkButton162" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton56" CssClass="btn-sm btn-default" runat="server" data-original-title="transfer machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,1,0,2,2"><i class="glyphicon glyphicon-transfer" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton105" CssClass="btn-sm btn-default" runat="server" data-original-title="cut off machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,7,0,2,8"><i class="fa fa-cut" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>2</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1002 E</td>
                            <td>10092883797</td>
                            <td>QA PL4</td>
                            <td><i class="fa fa-circle" style="color: lime; font-size: 10px;" aria-hidden="true"></i>&nbsp;Online</td>
                            <td>Waiting QA (cut off)</td>
                            <td>
                                <asp:LinkButton ID="LinkButton163" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <%--  <asp:LinkButton ID="LinkButton105" CssClass="btn-sm btn-default" runat="server" data-original-title="remove machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="2,2,0,3,3"><i class="fa fa-pencil" aria-hidden="true"></i></asp:LinkButton>--%>
                                <asp:LinkButton ID="LinkButton111" CssClass="btn-sm btn-default" runat="server" data-original-title="cut machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="2,8,0,3,10"><i class="fa fa-user" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>3</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1003 E</td>
                            <td>10092883787</td>
                            <td>PD 1 SC</td>
                            <td><i class="fa fa-circle" style="color: lime; font-size: 10px;" aria-hidden="true"></i>&nbsp;Online</td>
                            <td>None</td>
                            <td>
                                <asp:LinkButton ID="LinkButton164" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton109" CssClass="btn-sm btn-default" runat="server" data-original-title="transfer machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,1,0,2,2"><i class="glyphicon glyphicon-transfer" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton110" CssClass="btn-sm btn-default" runat="server" data-original-title="cut off machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,7,0,2,8"><i class="fa fa-cut" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>4</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1004 E</td>
                            <td>10092883777</td>
                            <td>QA IN</td>
                            <td><i class="fa fa-circle" style="color: red; font-size: 10px;" aria-hidden="true"></i>&nbsp;Offline</td>
                            <td>cut off machine</td>
                            <td>
                                <asp:LinkButton ID="LinkButton93" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <%-- <asp:LinkButton ID="LinkButton62" CssClass="btn-sm btn-default" runat="server" data-original-title="cut machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="5"><i class="fa fa-cut" aria-hidden="true"></i></asp:LinkButton>--%>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>

            <asp:Panel ID="Waiting_HeadQA_cutoff" runat="server" Visible="false">


                <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                    <tbody>
                        <tr class="info" style="font-size: Small; height: 40px;">
                            <th scope="col" style="width: 2%;">ลำดับ</th>
                            <th scope="col" style="width: 10%;">ประเภทเครื่องมือ</th>
                            <th scope="col" style="width: 12%;">ชื่อเครื่องมือ</th>
                            <th scope="col" style="width: 10%;">รหัสเครื่อง</th>
                            <th scope="col" style="width: 15%;">หมายเลขเครื่อง</th>
                            <th scope="col" style="width: 10%;">ผู้ถือครอง</th>
                            <th scope="col" style="width: 10%;">สถานะอุปกรณ์</th>
                            <th scope="col" style="width: 14%;">สถานะดำเนินการอื่นๆ</th>
                            <th scope="col" style="width: 15%;">การจัดการ</th>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>1</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1001 E</td>
                            <td>10092883707</td>
                            <td>QA LAB</td>
                            <td><i class="fa fa-circle" style="color: lime; font-size: 10px;" aria-hidden="true"></i>&nbsp;Online</td>
                            <td>None</td>
                            <td>
                                <asp:LinkButton ID="LinkButton165" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton78" CssClass="btn-sm btn-default" runat="server" data-original-title="transfer machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,1,0,2,2"><i class="glyphicon glyphicon-transfer" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton112" CssClass="btn-sm btn-default" runat="server" data-original-title="cut off machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,7,0,2,8"><i class="fa fa-cut" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>2</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1002 E</td>
                            <td>10092883797</td>
                            <td>QA PL4</td>
                            <td><i class="fa fa-circle" style="color: lime; font-size: 10px;" aria-hidden="true"></i>&nbsp;Online</td>
                            <td>Waiting Head QA (cut off)</td>
                            <td>
                                <asp:LinkButton ID="LinkButton166" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <%--  <asp:LinkButton ID="LinkButton105" CssClass="btn-sm btn-default" runat="server" data-original-title="remove machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="2,2,0,3,3"><i class="fa fa-pencil" aria-hidden="true"></i></asp:LinkButton>--%>
                                <asp:LinkButton ID="LinkButton118" CssClass="btn-sm btn-default" runat="server" data-original-title="cut machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="4,10,0,4,11"><i class="fa fa-user" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>3</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1003 E</td>
                            <td>10092883787</td>
                            <td>PD 1 SC</td>
                            <td><i class="fa fa-circle" style="color: lime; font-size: 10px;" aria-hidden="true"></i>&nbsp;Online</td>
                            <td>None</td>
                            <td>
                                <asp:LinkButton ID="LinkButton167" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton113" CssClass="btn-sm btn-default" runat="server" data-original-title="transfer machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,1,0,2,2"><i class="glyphicon glyphicon-transfer" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton114" CssClass="btn-sm btn-default" runat="server" data-original-title="cut off machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,7,0,2,8"><i class="fa fa-cut" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>4</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1004 E</td>
                            <td>10092883777</td>
                            <td>QA IN</td>
                            <td><i class="fa fa-circle" style="color: red; font-size: 10px;" aria-hidden="true"></i>&nbsp;Offline</td>
                            <td>cut off machine</td>
                            <td>
                                <asp:LinkButton ID="LinkButton94" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <%-- <asp:LinkButton ID="LinkButton62" CssClass="btn-sm btn-default" runat="server" data-original-title="cut machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="5"><i class="fa fa-cut" aria-hidden="true"></i></asp:LinkButton>--%>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>

            <asp:Panel ID="cutoff_complete" runat="server" Visible="false">


                <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                    <tbody>
                        <tr class="info" style="font-size: Small; height: 40px;">
                            <th scope="col" style="width: 2%;">ลำดับ</th>
                            <th scope="col" style="width: 10%;">ประเภทเครื่องมือ</th>
                            <th scope="col" style="width: 12%;">ชื่อเครื่องมือ</th>
                            <th scope="col" style="width: 10%;">รหัสเครื่อง</th>
                            <th scope="col" style="width: 15%;">หมายเลขเครื่อง</th>
                            <th scope="col" style="width: 10%;">ผู้ถือครอง</th>
                            <th scope="col" style="width: 10%;">สถานะอุปกรณ์</th>
                            <th scope="col" style="width: 14%;">สถานะดำเนินการอื่นๆ</th>
                            <th scope="col" style="width: 15%;">การจัดการ</th>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>1</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1001 E</td>
                            <td>10092883707</td>
                            <td>QA LAB</td>
                            <td><i class="fa fa-circle" style="color: lime; font-size: 10px;" aria-hidden="true"></i>&nbsp;Online</td>
                            <td>None</td>
                            <td>
                                <asp:LinkButton ID="LinkButton169" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton115" CssClass="btn-sm btn-default" runat="server" data-original-title="transfer machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,1,0,2,2"><i class="glyphicon glyphicon-transfer" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton116" CssClass="btn-sm btn-default" runat="server" data-original-title="cut off machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,7,0,2,8"><i class="fa fa-cut" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>2</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1002 E</td>
                            <td>10092883797</td>
                            <td>QA PL4</td>
                            <td><i class="fa fa-circle" style="color: red; font-size: 10px;" aria-hidden="true"></i>&nbsp;Offline</td>
                            <td>cut off completed</td>
                            <td>
                                <asp:LinkButton ID="LinkButton168" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <%--<asp:LinkButton ID="LinkButton117" CssClass="btn-sm btn-default" runat="server" data-original-title="transfer machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,1,0,2,2"><i class="glyphicon glyphicon-file" aria-hidden="true"></i></asp:LinkButton>--%>

                            </td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>3</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1003 E</td>
                            <td>10092883787</td>
                            <td>PD 1 SC</td>
                            <td><i class="fa fa-circle" style="color: lime; font-size: 10px;" aria-hidden="true"></i>&nbsp;Online</td>
                            <td>None</td>
                            <td>
                                <asp:LinkButton ID="LinkButton170" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton120" CssClass="btn-sm btn-default" runat="server" data-original-title="transfer machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,1,0,2,2"><i class="glyphicon glyphicon-transfer" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton121" CssClass="btn-sm btn-default" runat="server" data-original-title="cut off machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,7,0,2,8"><i class="fa fa-cut" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>4</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1004 E</td>
                            <td>10092883777</td>
                            <td>QA IN</td>
                            <td><i class="fa fa-circle" style="color: red; font-size: 10px;" aria-hidden="true"></i>&nbsp;Offline</td>
                            <td>cut off machine</td>
                            <td>
                                <asp:LinkButton ID="LinkButton101" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <%-- <asp:LinkButton ID="LinkButton62" CssClass="btn-sm btn-default" runat="server" data-original-title="cut machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="5"><i class="fa fa-cut" aria-hidden="true"></i></asp:LinkButton>--%>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>

            <asp:Panel ID="cutoff_Notcomplete" runat="server" Visible="false">


                <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                    <tbody>
                        <tr class="info" style="font-size: Small; height: 40px;">
                            <th scope="col" style="width: 2%;">ลำดับ</th>
                            <th scope="col" style="width: 10%;">ประเภทเครื่องมือ</th>
                            <th scope="col" style="width: 12%;">ชื่อเครื่องมือ</th>
                            <th scope="col" style="width: 10%;">รหัสเครื่อง</th>
                            <th scope="col" style="width: 15%;">หมายเลขเครื่อง</th>
                            <th scope="col" style="width: 10%;">ผู้ถือครอง</th>
                            <th scope="col" style="width: 10%;">สถานะอุปกรณ์</th>
                            <th scope="col" style="width: 14%;">สถานะดำเนินการอื่นๆ</th>
                            <th scope="col" style="width: 15%;">การจัดการ</th>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>1</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1001 E</td>
                            <td>10092883707</td>
                            <td>QA LAB</td>
                            <td><i class="fa fa-circle" style="color: lime; font-size: 10px;" aria-hidden="true"></i>&nbsp;Online</td>
                            <td>None</td>
                            <td>
                                <asp:LinkButton ID="LinkButton171" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton122" CssClass="btn-sm btn-default" runat="server" data-original-title="transfer machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,1,0,2,2"><i class="glyphicon glyphicon-transfer" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton123" CssClass="btn-sm btn-default" runat="server" data-original-title="cut off machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,7,0,2,8"><i class="fa fa-cut" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>2</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1002 E</td>
                            <td>10092883797</td>
                            <td>QA PL4</td>
                            <td><i class="fa fa-circle" style="color: lime; font-size: 10px;" aria-hidden="true"></i>&nbsp;Online</td>
                            <td>Cancelled Cut off</td>
                            <td>
                                <asp:LinkButton ID="LinkButton172" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton124" CssClass="btn-sm btn-default" runat="server" data-original-title="transfer machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,1,0,2,2"><i class="glyphicon glyphicon-transfer" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton125" CssClass="btn-sm btn-default" runat="server" data-original-title="cut off machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,7,0,2,8"><i class="fa fa-cut" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>3</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1003 E</td>
                            <td>10092883787</td>
                            <td>PD 1 SC</td>
                            <td><i class="fa fa-circle" style="color: lime; font-size: 10px;" aria-hidden="true"></i>&nbsp;Online</td>
                            <td>None</td>
                            <td>
                                <asp:LinkButton ID="LinkButton173" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton126" CssClass="btn-sm btn-default" runat="server" data-original-title="transfer machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,1,0,2,2"><i class="glyphicon glyphicon-transfer" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton127" CssClass="btn-sm btn-default" runat="server" data-original-title="cut off machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,7,0,2,8"><i class="fa fa-cut" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>4</td>
                            <td>Digital</td>
                            <td>เครื่องชั่ง 1.5 Kg</td>
                            <td>LAB 1004 E</td>
                            <td>10092883777</td>
                            <td>QA IN</td>
                            <td><i class="fa fa-circle" style="color: red; font-size: 10px;" aria-hidden="true"></i>&nbsp;Offline</td>
                            <td>cut off machine</td>
                            <td>
                                <asp:LinkButton ID="LinkButton102" CssClass="btn-sm btn-default" runat="server" data-original-title="view" data-toggle="tooltip"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <%-- <asp:LinkButton ID="LinkButton62" CssClass="btn-sm btn-default" runat="server" data-original-title="cut machine" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="5"><i class="fa fa-cut" aria-hidden="true"></i></asp:LinkButton>--%>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>

            <asp:FormView ID="fvDetailsMachine" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">รายละเอียดเครื่องมือ</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <%-- <label class="col-sm-2 control-label">Sample Code</label>
                                    <div class="col-sm-4 control-label textleft">
                                        CAL0000001R03
                                    </div>--%>
                                    <label class="col-sm-2 control-label">Test Date</label>
                                    <div class="col-sm-4 control-label textleft">
                                        07/07/2017
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">certificate</label>
                                    <div class="col-sm-4 control-label textleft">
                                        cerfiticate.pdf
                                    </div>
                                    <label class="col-sm-2 control-label">ชื่อเครื่องมือ</label>
                                    <div class="col-sm-4 control-label textleft">
                                        Machine03
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ID No.</label>
                                    <div class="col-sm-4 control-label textleft">
                                        ID0003
                                    </div>
                                    <label class="col-sm-2 control-label">ประเภทเครื่องมือ</label>
                                    <div class="col-sm-4 control-label textleft">
                                        Digital
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ช่วงเวลาใช้งาน</label>
                                    <div class="col-sm-4 control-label textleft">
                                        1-300 g
                                    </div>
                                    <label class="col-sm-2 control-label">Serial No.</label>
                                    <div class="col-sm-4 control-label textleft">
                                        Serial0003
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Holders</label>
                                    <div class="col-sm-4 control-label textleft">
                                        QA LAB
                                    </div>
                                    <label class="col-sm-2 control-label">Status Machine</label>
                                    <div class="col-sm-4 control-label textleft">
                                        Online
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="lbDocCancel" CssClass="btn btn-default" runat="server" data-original-title="Back" data-toggle="tooltip" OnCommand="btnCommand" CommandArgument="1" CommandName="cmdDocCancel"><i class="fa fa-reply" aria-hidden="true"></i> Back</asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:FormView ID="fvDetails_MachineBeforeremove" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">รายละเอียดเครื่องมือ (โอนย้าย)</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">


                                <div class="form-group">

                                    <label class="col-sm-2 control-label">ชื่อเครื่องมือ</label>
                                    <div class="col-sm-4 control-label textleft">
                                        <asp:TextBox ID="TextBox12" runat="server" Text="เครื่องชั่ง 1.5 Kg" CssClass="form-control" Enabled="false" />
                                    </div>
                                    <label class="col-sm-2 control-label">ID No.</label>
                                    <div class="col-sm-4 control-label textleft">
                                        <asp:TextBox ID="TextBox1" runat="server" Text="LAB 1001 E" CssClass="form-control" Enabled="false" />
                                    </div>
                                </div>
                                <div class="form-group">

                                    <label class="col-sm-2 control-label">ประเภทเครื่องมือ</label>
                                    <div class="col-sm-4 control-label textleft">
                                        <asp:TextBox ID="TextBox16" runat="server" Text="Digital" CssClass="form-control" Enabled="false" />
                                    </div>
                                    <label class="col-sm-2 control-label">ช่วงเวลาใช้งาน</label>
                                    <div class="col-sm-4 control-label textleft">
                                        <asp:TextBox ID="TextBox17" runat="server" Text=" 1-300 g" CssClass="form-control" Enabled="false" />

                                    </div>
                                </div>
                                <div class="form-group">

                                    <label class="col-sm-2 control-label">Serial No.</label>
                                    <div class="col-sm-4 control-label textleft">
                                        <asp:TextBox ID="TextBox18" runat="server" Text=" 10092883707" CssClass="form-control" Enabled="false" />
                                    </div>
                                    <label class="col-sm-2 control-label">แผนกเดิม</label>
                                    <div class="col-sm-4 control-label textleft">
                                        <asp:TextBox ID="TextBox20" runat="server" Text=" QA LAB" CssClass="form-control" Enabled="false" />
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="lbDocCancel" CssClass="btn btn-default" runat="server" data-original-title="Back" data-toggle="tooltip" OnCommand="btnCommand" CommandArgument="1" CommandName="cmdDocCancel"><i class="fa fa-reply" aria-hidden="true"></i> Back</asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:FormView ID="fvActionRemove" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">การโอนย้ายเครื่องมือ</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">


                                <div class="form-group">

                                    <label class="col-sm-2 control-label">สถานะการโอนย้าย</label>
                                    <div class="col-sm-4 control-label textleft">
                                        <asp:DropDownList ID="ddltranfermachine" runat="server" CssClass="form-control" Enabled="true">

                                            <asp:ListItem>โอนย้ายเครื่องมือ</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ฝ่ายที่ต้องการโอนย้าย</label>
                                    <div class="col-sm-4 control-label textleft">
                                        <asp:DropDownList ID="ddltranfer1" runat="server" CssClass="form-control" Enabled="true">
                                            <asp:ListItem>ทดสอบ ทดสอบ</asp:ListItem>
                                            <asp:ListItem>ทดสอบ1 ทดสอบ1</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <label class="col-sm-2 control-label">แผนกที่ต้องการโอนย้าย</label>
                                    <div class="col-sm-4 control-label textleft">
                                        <asp:DropDownList ID="ddltranfer" runat="server" CssClass="form-control" Enabled="true">
                                            <asp:ListItem>ทดสอบ ทดสอบ</asp:ListItem>
                                            <asp:ListItem>ทดสอบ1 ทดสอบ1</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">

                                    <label class="col-sm-2 control-label">หมายเหตุ</label>
                                    <div class="col-sm-10 control-label textleft">
                                        <asp:TextBox ID="txtcoment_tranfer" runat="server" TextMode="MultiLine" placeholder="กรอกหมายเหตุ.." Rows="3" CssClass="form-control" Enabled="true" />
                                    </div>

                                </div>
                                <asp:Panel ID="formapprove" runat="server" Visible="false">
                                    <div class="form-group">

                                        <label class="col-sm-2 control-label">สถานะการอนุมัติ</label>
                                        <div class="col-sm-4 control-label textleft">
                                            <asp:DropDownList ID="ddlapprove_headuser" AutoPostBack="true" OnSelectedIndexChanged="selectedindexchange" runat="server" CssClass="form-control" Enabled="true">
                                                <%--  <asp:ListItem>-- เลือกแผนกที่ถือครอง -- </asp:ListItem>--%>
                                                <asp:ListItem Value="1">อนุมัติ</asp:ListItem>
                                                <asp:ListItem Value="2">ไม่อนุมัติ</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="btnsave_user" CssClass="btn btn-success" Visible="true" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,1,1,2,2"></asp:LinkButton>
                                        <asp:LinkButton ID="btnsave_headuser" CssClass="btn btn-success" Visible="false" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="2,2,1,3,3"></asp:LinkButton>
                                        <asp:LinkButton ID="btnsave_headuser_not" CssClass="btn btn-success" Visible="false" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="2,2,9,2,2"></asp:LinkButton>
                                        <asp:LinkButton ID="btncancelnew" CssClass="btn btn-danger" Visible="true" runat="server" data-original-title="cancel" data-toggle="tooltip" Text="Cancel" OnCommand="btnCommand" CommandArgument="0" CommandName="cmdDocCancel"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:FormView ID="fvremove_qaAction" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">การโอนย้ายเครื่องมือ</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">

                                <div class="form-group">

                                    <label class="col-sm-2 control-label">สถานะการโอนย้าย</label>
                                    <div class="col-sm-4 control-label textleft">
                                        <asp:DropDownList ID="ddltranfermachine" runat="server" CssClass="form-control" Enabled="false">

                                            <asp:ListItem>โอนย้ายเครื่องมือ</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ฝ่ายที่ต้องการโอนย้าย</label>
                                    <div class="col-sm-4 control-label textleft">
                                        <asp:DropDownList ID="DropDownList3d" runat="server" CssClass="form-control" Enabled="false">
                                            <asp:ListItem>ทดสอบ ทดสอบ</asp:ListItem>
                                            <asp:ListItem>ทดสอบ3 ทดสอบ1</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <label class="col-sm-2 control-label">แผนกที่ต้องการโอนย้าย</label>
                                    <div class="col-sm-4 control-label textleft">
                                        <asp:DropDownList ID="ddltranfer" runat="server" CssClass="form-control" Enabled="false">
                                            <asp:ListItem>ทดสอบ ทดสอบ</asp:ListItem>
                                            <asp:ListItem>ทดสอบ1 ทดสอบ1</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">

                                    <label class="col-sm-2 control-label">หมายเหตุ</label>
                                    <div class="col-sm-10 control-label textleft">
                                        <asp:TextBox ID="txtcoment_tranfer" Text="test tranfer machine" runat="server" TextMode="MultiLine" placeholder="กรอกหมายเหตุ.." Rows="3" CssClass="form-control" Enabled="false" />
                                    </div>

                                </div>
                                <asp:Panel ID="formapprove" runat="server" Visible="true">
                                    <div class="form-group">

                                        <label class="col-sm-2 control-label">สถานะการอนุมัติ</label>
                                        <div class="col-sm-4 control-label textleft">
                                            <asp:DropDownList ID="ddlapprove_Qa" runat="server" AutoPostBack="true" OnSelectedIndexChanged="selectedindexchange" CssClass="form-control" Enabled="true">
                                                <%--  <asp:ListItem>-- เลือกแผนกที่ถือครอง -- </asp:ListItem>--%>
                                                <asp:ListItem Value="1">อนุมัติ</asp:ListItem>
                                                <asp:ListItem Value="2">ไม่อนุมัติ</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="btnsave_Qa_not" CssClass="btn btn-success" Visible="false" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="3,3,9,3,3"></asp:LinkButton>
                                        <asp:LinkButton ID="btnsave_Qa" CssClass="btn btn-success" Visible="true" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="3,3,1,4,4"></asp:LinkButton>
                                        <asp:LinkButton ID="btncancelnew" CssClass="btn btn-danger" Visible="true" runat="server" data-original-title="cancel" data-toggle="tooltip" Text="Cancel" OnCommand="btnCommand" CommandArgument="0" CommandName="cmdDocCancel"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:FormView ID="fvremove_Head_qaAction" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">การโอนย้ายเครื่องมือ</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">



                                <div class="form-group">

                                    <label class="col-sm-2 control-label">สถานะการโอนย้าย</label>
                                    <div class="col-sm-4 control-label textleft">
                                        <asp:DropDownList ID="ddltranfermachine" runat="server" CssClass="form-control" Enabled="false">

                                            <asp:ListItem>โอนย้ายเครื่องมือ</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ฝ่ายที่ต้องการโอนย้าย</label>
                                    <div class="col-sm-4 control-label textleft">
                                        <asp:DropDownList ID="DropDownList3l" runat="server" CssClass="form-control" Enabled="false">
                                            <asp:ListItem>ทดสอบ ทดสอบ</asp:ListItem>
                                            <asp:ListItem>ทดสอบ1 ทดสอบ1</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <label class="col-sm-2 control-label">แผนกที่ต้องการโอนย้าย</label>
                                    <div class="col-sm-4 control-label textleft">
                                        <asp:DropDownList ID="ddltranfer" runat="server" CssClass="form-control" Enabled="false">
                                            <asp:ListItem>ทดสอบ ทดสอบ</asp:ListItem>
                                            <asp:ListItem>ทดสอบ1 ทดสอบ1</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">

                                    <label class="col-sm-2 control-label">หมายเหตุ</label>
                                    <div class="col-sm-10 control-label textleft">
                                        <asp:TextBox ID="txtcoment_tranfer" Text="test tranfer machine" runat="server" TextMode="MultiLine" placeholder="กรอกหมายเหตุ.." Rows="3" CssClass="form-control" Enabled="false" />
                                    </div>

                                </div>
                                <asp:Panel ID="formapprove" runat="server" Visible="true">
                                    <div class="form-group">

                                        <label class="col-sm-2 control-label">สถานะการอนุมัติ</label>
                                        <div class="col-sm-4 control-label textleft">
                                            <asp:DropDownList ID="ddlapprove_head_Qa" runat="server" AutoPostBack="true" OnSelectedIndexChanged="selectedindexchange" CssClass="form-control" Enabled="true">
                                                <%--  <asp:ListItem>-- เลือกแผนกที่ถือครอง -- </asp:ListItem>--%>
                                                <asp:ListItem Value="1">อนุมัติ</asp:ListItem>
                                                <asp:ListItem Value="2">ไม่อนุมัติ</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="btnsave_HeadQa_not" CssClass="btn btn-success" Visible="false" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="4,4,9,4,4"></asp:LinkButton>
                                        <asp:LinkButton ID="btnsave_HeadQa" CssClass="btn btn-success" Visible="true" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="4,4,1,5,5"></asp:LinkButton>
                                        <asp:LinkButton ID="btncancelnew" CssClass="btn btn-danger" Visible="true" runat="server" data-original-title="cancel" data-toggle="tooltip" Text="Cancel" OnCommand="btnCommand" CommandArgument="0" CommandName="cmdDocCancel"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:FormView ID="fvremove_Recevive" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">การโอนย้ายเครื่องมือ (รับเครื่อง)</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">



                                <div class="form-group">

                                    <label class="col-sm-2 control-label">สถานะการโอนย้าย</label>
                                    <div class="col-sm-4 control-label textleft">
                                        <asp:DropDownList ID="ddltranfermachine" runat="server" CssClass="form-control" Enabled="false">

                                            <asp:ListItem>โอนย้ายเครื่องมือ</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ฝ่ายที่ต้องการโอนย้าย</label>
                                    <div class="col-sm-4 control-label textleft">
                                        <asp:DropDownList ID="DropDownList3la" runat="server" CssClass="form-control" Enabled="false">
                                            <asp:ListItem>ทดสอบ ทดสอบ</asp:ListItem>
                                            <asp:ListItem>ทดสอบ1 ทดสอบ1</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <label class="col-sm-2 control-label">แผนกที่ต้องการโอนย้าย</label>
                                    <div class="col-sm-4 control-label textleft">
                                        <asp:DropDownList ID="ddltranfer" runat="server" CssClass="form-control" Enabled="false">
                                            <asp:ListItem>ทดสอบ ทดสอบ</asp:ListItem>
                                            <asp:ListItem>ทดสอบ1 ทดสอบ1</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">

                                    <label class="col-sm-2 control-label">หมายเหตุ</label>
                                    <div class="col-sm-10 control-label textleft">
                                        <asp:TextBox ID="txtcoment_tranfer" Text="test tranfer machine" runat="server" TextMode="MultiLine" placeholder="กรอกหมายเหตุ.." Rows="3" CssClass="form-control" Enabled="false" />
                                    </div>

                                </div>
                                <asp:Panel ID="formapprove" runat="server" Visible="true">
                                    <div class="form-group">

                                        <label class="col-sm-2 control-label">สถานะการอนุมัติ</label>
                                        <div class="col-sm-4 control-label textleft">
                                            <asp:DropDownList ID="ddlapprove_resive" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="selectedindexchange" Enabled="true">
                                                <%--  <asp:ListItem>-- เลือกแผนกที่ถือครอง -- </asp:ListItem>--%>
                                                <asp:ListItem Value="1">รับเครื่อง</asp:ListItem>
                                                <asp:ListItem Value="2">ไม่รับเครื่อง</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="btnsave_reciceve_not" CssClass="btn btn-success" Visible="false" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="5,5,9,5,5"></asp:LinkButton>
                                        <asp:LinkButton ID="btnsave_reciceve" CssClass="btn btn-success" Visible="true" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="5,5,1,5,6"></asp:LinkButton>
                                        <asp:LinkButton ID="btncancelnew" CssClass="btn btn-danger" Visible="true" runat="server" data-original-title="cancel" data-toggle="tooltip" Text="Cancel" OnCommand="btnCommand" CommandArgument="0" CommandName="cmdDocCancel"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:FormView ID="fvMachine_beforeCutoff" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">รายละเอียดเครื่องมือ</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">


                                <div class="form-group">

                                    <label class="col-sm-2 control-label">ชื่อเครื่องมือ</label>
                                    <div class="col-sm-4 control-label textleft">
                                        <asp:TextBox ID="TextBox12" runat="server" Text="เครื่องชั่ง 1.5 Kg" CssClass="form-control" Enabled="false" />
                                    </div>
                                    <label class="col-sm-2 control-label">ID No.</label>
                                    <div class="col-sm-4 control-label textleft">
                                        <asp:TextBox ID="TextBox1" runat="server" Text="LAB 1001 E" CssClass="form-control" Enabled="false" />
                                    </div>
                                </div>
                                <div class="form-group">

                                    <label class="col-sm-2 control-label">ประเภทเครื่องมือ</label>
                                    <div class="col-sm-4 control-label textleft">
                                        <asp:TextBox ID="TextBox16" runat="server" Text="Digital" CssClass="form-control" Enabled="false" />
                                    </div>
                                    <label class="col-sm-2 control-label">ช่วงเวลาใช้งาน</label>
                                    <div class="col-sm-4 control-label textleft">
                                        <asp:TextBox ID="TextBox17" runat="server" Text=" 1-300 g" CssClass="form-control" Enabled="false" />

                                    </div>
                                </div>
                                <div class="form-group">

                                    <label class="col-sm-2 control-label">Serial No.</label>
                                    <div class="col-sm-4 control-label textleft">
                                        <asp:TextBox ID="TextBox18" runat="server" Text=" 10092883707" CssClass="form-control" Enabled="false" />
                                    </div>
                                    <label class="col-sm-2 control-label">ผู้ถือครอง</label>
                                    <div class="col-sm-4 control-label textleft">
                                        <asp:TextBox ID="TextBox20" runat="server" Text=" ทดสอบ ทดสอบ" CssClass="form-control" Enabled="false" />
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="lbDocCancel" CssClass="btn btn-default" runat="server" data-original-title="Back" data-toggle="tooltip" OnCommand="btnCommand" CommandArgument="1" CommandName="cmdDocCancel"><i class="fa fa-reply" aria-hidden="true"></i> Back</asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>
            <!-- Cut off -->
            <asp:FormView ID="fvcutoff_userAction" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">การตัดชำรุดเครื่องมือ</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">


                                <div class="form-group">

                                    <label class="col-sm-2 control-label">สถานะการตัดชำรุด :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        <asp:DropDownList ID="ddlcutoff" runat="server" CssClass="form-control" Enabled="true">
                                            <%--   <asp:ListItem>-- เลือกสถานะการโอนย้าย -- </asp:ListItem>--%>
                                            <asp:ListItem>ตัดชำรุด</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                </div>
                                <div class="form-group">

                                    <label class="col-sm-2 control-label">แนบไฟล์ :</label>
                                    <div class="col-sm-10 control-label textleft">
                                        <asp:FileUpload ID="FileUpload2" runat="server" />
                                        <p style="color: red;">** แนบเอกสารอ้างอิงการตัดชำรุดเครื่องมือ ขนาดไฟล์ ไม่เกิน 5 MB</p>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="btnsave_user_cutoff" CssClass="btn btn-success" Visible="true" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="1,7,1,2,8"></asp:LinkButton>
                                        <asp:LinkButton ID="btncancelnew" CssClass="btn btn-danger" Visible="true" runat="server" data-original-title="cancel" data-toggle="tooltip" Text="Cancel" OnCommand="btnCommand" CommandArgument="0" CommandName="cmdDocCancel"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:FormView ID="fvcutoff_HeaduserAction" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">การตัดชำรุดเครื่องมือ</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">


                                <div class="form-group">

                                    <label class="col-sm-2 control-label">สถานะการการตัดชำรุด :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        <asp:DropDownList ID="ddltranfermachine" runat="server" CssClass="form-control" Enabled="false">
                                            <%--   <asp:ListItem>-- เลือกสถานะการโอนย้าย -- </asp:ListItem>--%>
                                            <asp:ListItem>ตัดชำรุด</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ไฟล์เอกสารแนบ :</label>
                                    <div class="col-sm-8">
                                        <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                                            <tbody>
                                                <tr class="info" style="font-size: Small; height: 40px;">

                                                    <th scope="col" colspan="2" style="width: 100%;"><i class="fa fa-file-text-o" aria-hidden="true"></i>&nbsp;เอกสารอ้างอิงการตัดชำรุดเครื่องมือ</th>


                                                </tr>
                                                <tr>
                                                    <td>เอกสารอ้างอิงการตัดชำรุดเครื่องมือ.pdf</td>
                                                    <td><i class="fa fa-download" data-original-title="Save" data-toggle="tooltip" title="download" aria-hidden="true"></i></td>
                                                </tr>
                                                <tr>
                                                    <td>เอกสารอ้างอิงการตัดชำรุดเครื่องมือ2.pdf</td>
                                                    <td><i class="fa fa-download" data-original-title="Save" data-toggle="tooltip" title="download" aria-hidden="true"></i></td>
                                                </tr>
                                            </tbody>
                                        </table>

                                        <%--<asp:HyperLink ID="HyperLink1" runat="server"><i class="glyphicon glyphicon-file"></i>ไฟล์เอกสารอ้างอิงการตัดชำรุด</asp:HyperLink>--%>
                                    </div>
                                </div>

                                <div class="form-group">

                                    <label class="col-sm-2 control-label">สถานะการอนุมัติ :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        <asp:DropDownList ID="ddlapprove_cutoff" runat="server" AutoPostBack="true" OnSelectedIndexChanged="selectedindexchange" CssClass="form-control" Enabled="true">
                                            <%--   <asp:ListItem>-- เลือกสถานะการโอนย้าย -- </asp:ListItem>--%>
                                            <asp:ListItem Value="1">อนุมัติ</asp:ListItem>
                                            <asp:ListItem Value="2">ไม่อนุมัติ</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="btnsave_user_cutoff" CssClass="btn btn-success" Visible="true" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="2,8,1,3,9"></asp:LinkButton>
                                        <asp:LinkButton ID="btnsave_user_notcutoff" CssClass="btn btn-success" Visible="false" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="2,8,9,3,8"></asp:LinkButton>
                                        <asp:LinkButton ID="btncancelnew" CssClass="btn btn-danger" Visible="true" runat="server" data-original-title="cancel" data-toggle="tooltip" Text="Cancel" OnCommand="btnCommand" CommandArgument="0" CommandName="cmdDocCancel"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:FormView ID="fvcutoff_QaAction" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">การตัดชำรุดเครื่องมือ</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">


                                <div class="form-group">

                                    <label class="col-sm-2 control-label">สถานะการการตัดชำรุด :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        <asp:DropDownList ID="ddltranfermachine" runat="server" CssClass="form-control" Enabled="false">
                                            <%--   <asp:ListItem>-- เลือกสถานะการโอนย้าย -- </asp:ListItem>--%>
                                            <asp:ListItem>ตัดชำรุด</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ไฟล์เอกสารแนบ :</label>
                                    <div class="col-sm-8">
                                        <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                                            <tbody>
                                                <tr class="info" style="font-size: Small; height: 40px;">

                                                    <th scope="col" colspan="2" style="width: 100%;"><i class="fa fa-file-text-o" aria-hidden="true"></i>&nbsp;เอกสารอ้างอิงการตัดชำรุดเครื่องมือ</th>


                                                </tr>
                                                <tr>
                                                    <td>เอกสารอ้างอิงการตัดชำรุดเครื่องมือ.pdf</td>
                                                    <td><i class="fa fa-download" data-original-title="Save" data-toggle="tooltip" title="download" aria-hidden="true"></i></td>
                                                </tr>
                                                <tr>
                                                    <td>เอกสารอ้างอิงการตัดชำรุดเครื่องมือ2.pdf</td>
                                                    <td><i class="fa fa-download" data-original-title="Save" data-toggle="tooltip" title="download" aria-hidden="true"></i></td>
                                                </tr>
                                            </tbody>
                                        </table>

                                        <%--<asp:HyperLink ID="HyperLink1" runat="server"><i class="glyphicon glyphicon-file"></i>ไฟล์เอกสารอ้างอิงการตัดชำรุด</asp:HyperLink>--%>
                                    </div>
                                </div>
                                <div class="form-group">

                                    <label class="col-sm-2 control-label">สถานะการอนุมัติ :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        <asp:DropDownList ID="ddlapprove_Qacutoff" runat="server" AutoPostBack="true" OnSelectedIndexChanged="selectedindexchange" CssClass="form-control" Enabled="true">
                                            <%--   <asp:ListItem>-- เลือกสถานะการโอนย้าย -- </asp:ListItem>--%>
                                            <asp:ListItem Value="1">อนุมัติ</asp:ListItem>
                                            <asp:ListItem Value="2">ไม่อนุมัติ</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="btnsave_Qa_cutoff" CssClass="btn btn-success" Visible="true" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="3,9,1,4,10"></asp:LinkButton>
                                        <asp:LinkButton ID="btnsave_Qa_notcutoff" CssClass="btn btn-success" Visible="false" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="3,9,9,4,9"></asp:LinkButton>
                                        <asp:LinkButton ID="btncancelnew" CssClass="btn btn-danger" Visible="true" runat="server" data-original-title="cancel" data-toggle="tooltip" Text="Cancel" OnCommand="btnCommand" CommandArgument="0" CommandName="cmdDocCancel"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:FormView ID="fvcutoff_HeadQaAction" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">การตัดชำรุดเครื่องมือ</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">


                                <div class="form-group">

                                    <label class="col-sm-2 control-label">สถานะการการตัดชำรุด :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        <asp:DropDownList ID="ddltranfermachine" runat="server" CssClass="form-control" Enabled="false">
                                            <%--   <asp:ListItem>-- เลือกสถานะการโอนย้าย -- </asp:ListItem>--%>
                                            <asp:ListItem>ตัดชำรุด</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ไฟล์เอกสารแนบ :</label>
                                    <div class="col-sm-8">
                                        <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                                            <tbody>
                                                <tr class="info" style="font-size: Small; height: 40px;">

                                                    <th scope="col" colspan="2" style="width: 100%;"><i class="fa fa-file-text-o" aria-hidden="true"></i>&nbsp;เอกสารอ้างอิงการตัดชำรุดเครื่องมือ</th>


                                                </tr>
                                                <tr>
                                                    <td>เอกสารอ้างอิงการตัดชำรุดเครื่องมือ.pdf</td>
                                                    <td><i class="fa fa-download" data-original-title="Save" data-toggle="tooltip" title="download" aria-hidden="true"></i></td>
                                                </tr>
                                                <tr>
                                                    <td>เอกสารอ้างอิงการตัดชำรุดเครื่องมือ2.pdf</td>
                                                    <td><i class="fa fa-download" data-original-title="Save" data-toggle="tooltip" title="download" aria-hidden="true"></i></td>
                                                </tr>
                                            </tbody>
                                        </table>

                                        <%--<asp:HyperLink ID="HyperLink1" runat="server"><i class="glyphicon glyphicon-file"></i>ไฟล์เอกสารอ้างอิงการตัดชำรุด</asp:HyperLink>--%>
                                    </div>
                                </div>
                                <div class="form-group">

                                    <label class="col-sm-2 control-label">สถานะการอนุมัติ :</label>
                                    <div class="col-sm-4 control-label textleft">
                                        <asp:DropDownList ID="ddlapprove_HeadQacutoff" runat="server" OnSelectedIndexChanged="selectedindexchange" AutoPostBack="true" CssClass="form-control" Enabled="true">
                                            <%--   <asp:ListItem>-- เลือกสถานะการโอนย้าย -- </asp:ListItem>--%>
                                            <asp:ListItem Value="1">อนุมัติ</asp:ListItem>
                                            <asp:ListItem Value="2">ไม่อนุมัติ</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="btnsave_HeadQa_cutoff" CssClass="btn btn-success" Visible="true" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="4,10,1,4,11"></asp:LinkButton>
                                        <asp:LinkButton ID="btnsave_HeadQa_notcutoff" CssClass="btn btn-success" Visible="false" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="cmdAction" CommandArgument="4,10,9,4,10"></asp:LinkButton>
                                        <asp:LinkButton ID="btncancelnew" CssClass="btn btn-danger" Visible="true" runat="server" data-original-title="cancel" data-toggle="tooltip" Text="Cancel" OnCommand="btnCommand" CommandArgument="0" CommandName="cmdDocCancel"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>
        </asp:View>

        <asp:View ID="tab4" runat="server">
        </asp:View>

        <asp:View ID="tab5" runat="server">
            <asp:Panel ID="master_result_test" runat="server" Visible="false">

                <div class="form-horizontal" role="form">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <%--<div class="col-sm-12"></div>--%>
                            <div class="pull-left">
                                <asp:LinkButton ID="btnInsertmasterlab" CssClass="btn btn-primary" runat="server" Font-Size="Small" ValidationGroup="Insert" OnCommand="btnCommand" CommandName="btnInsertmasterlab" data-toggle="tooltip" title="Insert"><span class="glyphicon glyphicon-plus"></span> เพิ่มข้อมูล</asp:LinkButton>
                            </div>

                        </div>
                    </div>
                </div>

                <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                    <tbody>
                        <tr class="info" style="font-size: Small; height: 30px;">
                            <th scope="col" style="width: 1%;">#</th>
                            <th scope="col" style="width: 10%;">ผลการสอบเทียบ</th>
                            <th scope="col" style="width: 10%;">สถานะ</th>
                            <th scope="col" style="width: 10%;">การจัดการ</th>

                        </tr>
                        <tr style="font-size: Small;">
                            <td>1.</td>
                            <td>ปกติ</td>
                            <td>online</td>
                            <td>
                                <asp:LinkButton ID="LinkButton64" CssClass="btn-sm btn-default" runat="server" data-original-title="Edit" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEvent" CommandArgument=""><i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></asp:LinkButton>

                                <asp:LinkButton ID="LinkButton72" CssClass="btn-sm btn-default" runat="server" data-original-title="Delete" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEvent" CommandArgument=""><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></asp:LinkButton>
                            </td>

                        </tr>
                        <tr style="font-size: Small;">
                            <td>2.</td>
                            <td>ไม่ปกติ</td>
                            <td>online</td>
                            <td>
                                <asp:LinkButton ID="LinkButton80" CssClass="btn-sm btn-default" runat="server" data-original-title="Edit" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEvent" CommandArgument=""><i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></asp:LinkButton>

                                <asp:LinkButton ID="LinkButton139" CssClass="btn-sm btn-default" runat="server" data-original-title="Delete" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEvent" CommandArgument=""><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></asp:LinkButton>
                            </td>

                        </tr>
                    </tbody>
                </table>
            </asp:Panel>

            <asp:Panel ID="master_unit" runat="server" Visible="false">

                <div class="form-horizontal" role="form">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <%--<div class="col-sm-12"></div>--%>
                            <div class="pull-left">
                                <asp:LinkButton ID="LinkButton146" CssClass="btn btn-primary" runat="server" Font-Size="Small" ValidationGroup="Insert" OnCommand="btnCommand" CommandName="btnInsertmasterlab" data-toggle="tooltip" title="Insert"><span class="glyphicon glyphicon-plus"></span> เพิ่มข้อมูล</asp:LinkButton>
                            </div>

                        </div>
                    </div>
                </div>


                <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                    <tbody>
                        <tr class="info" style="font-size: Small; height: 30px;">
                            <th scope="col" style="width: 1%;">#</th>
                            <th scope="col" style="width: 10%;">หน่วย</th>
                            <th scope="col" style="width: 10%;">หน่วยย่อ</th>
                            <th scope="col" style="width: 10%;">สถานะ</th>
                            <th scope="col" style="width: 10%;">การจัดการ</th>

                        </tr>
                        <tr style="font-size: Small;">
                            <td>1.</td>
                            <td>มิลลิกรัม</td>
                            <td>mg</td>
                            <td>online</td>
                            <td>
                                <asp:LinkButton ID="LinkButton140" CssClass="btn-sm btn-default" runat="server" data-original-title="Edit" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEvent" CommandArgument=""><i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></asp:LinkButton>

                                <asp:LinkButton ID="LinkButton141" CssClass="btn-sm btn-default" runat="server" data-original-title="Delete" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEvent" CommandArgument=""><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></asp:LinkButton>
                            </td>

                        </tr>
                        <tr style="font-size: Small;">
                            <td>2.</td>
                            <td>กรัม</td>
                            <td>g</td>
                            <td>online</td>
                            <td>
                                <asp:LinkButton ID="LinkButton142" CssClass="btn-sm btn-default" runat="server" data-original-title="Edit" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEvent" CommandArgument=""><i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></asp:LinkButton>

                                <asp:LinkButton ID="LinkButton143" CssClass="btn-sm btn-default" runat="server" data-original-title="Delete" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEvent" CommandArgument=""><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></asp:LinkButton>
                            </td>

                        </tr>
                        <tr style="font-size: Small;">
                            <td>3.</td>
                            <td>กิโลกรัม</td>
                            <td>kg</td>
                            <td>online</td>
                            <td>
                                <asp:LinkButton ID="LinkButton144" CssClass="btn-sm btn-default" runat="server" data-original-title="Edit" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEvent" CommandArgument=""><i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></asp:LinkButton>

                                <asp:LinkButton ID="LinkButton145" CssClass="btn-sm btn-default" runat="server" data-original-title="Delete" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEvent" CommandArgument=""><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></asp:LinkButton>
                            </td>

                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
        </asp:View>

    </asp:MultiView>
    <!--multiview-->


</asp:Content>

