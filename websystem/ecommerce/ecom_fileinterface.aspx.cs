using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System.Diagnostics;
using System.Data.OleDb;
using System.Text;

public partial class websystem_ecommerce_ecom_fileinterface : System.Web.UI.Page
{
    #region initial function/data
    function_tool _funcTool = new function_tool();

    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _local_json = String.Empty;
    string _localJson = String.Empty;
    #endregion initial function/data

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetfileinterface = _serviceUrl + ConfigurationManager.AppSettings["urlGetfileinterface"];
    static string _urlGetfileinterface_m0 = _serviceUrl + ConfigurationManager.AppSettings["urlGetfileinterface_m0"];
    static string _urlSetfileinterface_insert = _serviceUrl + ConfigurationManager.AppSettings["urlSetfileinterface_insert"];
    static string _urlGetfileinterface_u0 = _serviceUrl + ConfigurationManager.AppSettings["urlGetfileinterface_u0"];
    static string _urlGetfileinterface_u1_detail = _serviceUrl + ConfigurationManager.AppSettings["urlGetfileinterface_u1_detail"];
    static string _urlDelfileinterface_u1 = _serviceUrl + ConfigurationManager.AppSettings["urlDelfileinterface_u1"];

    static string _urlGetsaleunit_m0 = _serviceUrl + ConfigurationManager.AppSettings["urlGetsaleunit_m0"];
    static string _urlGetpromotion_m0 = _serviceUrl + ConfigurationManager.AppSettings["urlGetpromotion_m0"];

    static string _urlGetBOM_m0_m1 = _serviceUrl + ConfigurationManager.AppSettings["urlGetBOM_m0_m1"];


    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = _funcTool.convertToInt(Session["emp_idx"].ToString());
        //litDebug.Text = String.Format("{0:F2}", (Convert.ToDouble(100 * 7) / 107));

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            initPage();
        }

        setTrigger();
    }
    protected void initPage()
    {
        clearSession();
        clearViewState();

        setActiveTab("view_ifif", 0);
        //li0.Attributes.Add("class", "active");
        LinkButton btnsavedatafile2 = (LinkButton)FvAddImport.FindControl("btnsavedatafile");

        btnsavedatafile2.Visible = false;
        set_default_import();
        hlSetTotop.Focus();
        // new_data_temp();

        //data_ecom_fileinterface data_interface_m0_saleunit = new data_ecom_fileinterface();

        //data_interface_m0_saleunit.ecom_m0_saleunit_list = new ecom_m0_fif_saleunit[0];
        //data_interface_m0_saleunit = callServicePost_func(_urlGetsaleunit_m0, data_interface_m0_saleunit);
        //ViewState["data_interface_m0_saleunit"] = data_interface_m0_saleunit.ecom_m0_saleunit_list;

        //data_ecom_fileinterface data_interface_m0_pro = new data_ecom_fileinterface();

        //data_interface_m0_pro.ecom_m0_promotion_list = new ecom_m0_fif_promotion[0];
        //data_interface_m0_pro = callServicePost_func(_urlGetpromotion_m0, data_interface_m0_pro);
        ////litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_interface_m0_pro));

        //ViewState["data_interface_m0_pro"] = data_interface_m0_pro.ecom_m0_promotion_list;

        data_ecom_fileinterface data_interface = new data_ecom_fileinterface();
        data_interface.ecom_m0_m1_bom_list = new ecom_m0_m1_bom[0];
        data_interface = callServicePost_func(_urlGetBOM_m0_m1, data_interface);
        ViewState["data_BOM"] = data_interface.ecom_m0_m1_bom_list;
    }
    #region event navCommand
    protected void navCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();

        string cmdArg = e.CommandArgument.ToString();

        setActiveTab(cmdArg, 0);
        // litDebug.Text = cmdArg;

    }
    #endregion event navCommand

    protected void setActiveTab(string activeTab, int doc_idx)
    {
        setActiveView(activeTab, doc_idx);
        // setActiveTabBar(activeTab);
        li0.Attributes.Add("class", "");
        li1.Attributes.Add("class", "");
        li2.Attributes.Add("class", "");


        switch (activeTab)
        {
            case "view_ifif":
                li0.Attributes.Add("class", "active");
                break;
            case "view_tnkso":

                li1.Attributes.Add("class", "active");
                break;
            case "view_listfile":

                li2.Attributes.Add("class", "active");

                break;
        }
    }

    protected void setActiveView(string activeTab, int doc_idx)
    {
        mvSystem.SetActiveView((View)mvSystem.FindControl(activeTab));

        FormView FvAddImport = (FormView)view_ifif.FindControl("FvAddImport");
        LinkButton btnsavedatafile2 = (LinkButton)FvAddImport.FindControl("btnsavedatafile");

        //btnsavedatafile2.Visible = false;
        div_detail.Visible = false;
        GV_ecom_u0.Visible = false;
        div_detail.Visible = false;
        switch (activeTab)
        {
            case "view_ifif":
                break;
            case "view_tnkso":
                break;
            case "view_listfile":
                GV_ecom_u0.Visible = true;
                get_GV_ecom_u0();
                break;
        }
    }

    protected void get_GV_ecom_u0()
    {
        data_ecom_fileinterface data_interface = new data_ecom_fileinterface();
        data_interface = callServicePost_func(_urlGetfileinterface_u0, data_interface);
        Select_data_u0(data_interface.ecom_u0_data_detail);
        ViewState["GV_ecom_u0"] = data_interface.ecom_u0_data_detail;
    }
    protected void setdropdown()
    {
        FormView FvAddImport = (FormView)view_ifif.FindControl("FvAddImport");
        DropDownList m0_channel = (DropDownList)FvAddImport.FindControl("m0_channel");


        data_ecom_fileinterface data_m0_channel = new data_ecom_fileinterface();
        ecom_m0_data ecom_m0_data_detail = new ecom_m0_data();
        data_m0_channel.ecom_m0_data_list = new ecom_m0_data[1];


        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0_channel)) +" <br> ";
        data_m0_channel = callServicePost_func(_urlGetfileinterface_m0, data_m0_channel);

        //data_ecom_fileinterface data_m0_channel = new data_ecom_fileinterface();

        //data_m0_channel.ecom_m0_data_list = new ecom_m0_data[2];

        //for (int i = 0; i < 2; i++)
        //{
        //    ecom_m0_data m0_data_detail = new ecom_m0_data();
        //    m0_data_detail.channel_idx = i.ToString();
        //    m0_data_detail.channel_name = "TKN Online." + i.ToString();
        //    m0_data_detail.cemp_idx = i.ToString();
        //    data_m0_channel.ecom_m0_data_list[i] = m0_data_detail;
        //}

        ViewState["ecom_m0_data"] = data_m0_channel.ecom_m0_data_list;
        setDdlData(m0_channel, data_m0_channel.ecom_m0_data_list, "channel_name", "channel_idx");
        m0_channel.Items.Insert(0, new ListItem("- เลือกช่องทางขาย -", ""));

    }
    protected void set_default_import()
    {
        setdropdown();
        setGridData(GvExcel_Show, null);
        ViewState["ReportListImport"] = null;
        GvExcel_Show.Visible = false;
    }

    #region event command

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        FormView FvAddImport = (FormView)view_ifif.FindControl("FvAddImport");

        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        lbDocCancelExcel.Visible = false;

        switch (cmdName)
        {

            #region cmdSearch         
            case "cmdSearch":
                string _dateStart = tbDateStart.Text.Trim();
                string _dateEnd = tbDateEnd.Text.Trim();
                if (!String.IsNullOrEmpty(_dateStart))
                {
                    _dateStart = tbDateStart.Text.Trim() + " 00:00:00";
                }
                else
                {
                    _dateStart = "0";
                }
                if (!String.IsNullOrEmpty(_dateEnd))
                {
                    _dateEnd = tbDateEnd.Text.Trim() + " 23:59:59";
                }
                else
                {
                    _dateEnd = "0";
                }
                data_ecom_fileinterface data_fileinterface = new data_ecom_fileinterface();
                ecom_fileinterface_data fileinterface_detail = new ecom_fileinterface_data();
                data_fileinterface.ecom_fileinterface_list = new ecom_fileinterface_data[1];

                fileinterface_detail.date_start = _dateStart;
                fileinterface_detail.date_end = _dateEnd;
                data_fileinterface.ecom_fileinterface_list[0] = fileinterface_detail;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_fileinterface)) +" <br> ";
                data_fileinterface = callServicePost_func(_urlGetfileinterface, data_fileinterface);
                //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_fileinterface));



                DataTable tableReport2 = new DataTable();
                data_ecom_fileinterface ReportListExport = new data_ecom_fileinterface();


                int _count_row2 = 0;
                ecom_fileinterface_data[] _item_Result3 = (ecom_fileinterface_data[])data_fileinterface.ecom_fileinterface_list;
                ReportListExport.ecom_fileinterface_list = new ecom_fileinterface_data[_item_Result3.Count()];
                #region tableReport2
                tableReport2.Columns.Add("หมายเลขคำสั่งซื้อ", typeof(String));
                tableReport2.Columns.Add("สถานะการสั่งซื้อ", typeof(String));
                tableReport2.Columns.Add("สถานะการคืนเงินหรือคืนสินค้า", typeof(String));
                tableReport2.Columns.Add("ชื่อผู้ใช้(ผู้ซื้อ)", typeof(String));
                tableReport2.Columns.Add("วันที่ทำการสั่งซื้อ", typeof(String));
                tableReport2.Columns.Add("เวลาการชำระสินค้า", typeof(String));
                tableReport2.Columns.Add("ช่องทางการชำระเงิน", typeof(String));
                tableReport2.Columns.Add("ตัวเลือกการจัดส่ง", typeof(String));
                tableReport2.Columns.Add("วิธีการจัดส่ง", typeof(String));
                tableReport2.Columns.Add("หมายเลขติดตามพัสดุ", typeof(String));
                tableReport2.Columns.Add("วันที่คาดว่าจะทำการจัดส่งสินค้า", typeof(String));
                tableReport2.Columns.Add("เวลาส่งสินค้า", typeof(String));
                tableReport2.Columns.Add("เลขอ้างอิง Parent SKU", typeof(String));
                tableReport2.Columns.Add("ชื่อสินค้า", typeof(String));
                tableReport2.Columns.Add("เลขอ้างอิง SKU (SKU Reference No.)", typeof(String));
                tableReport2.Columns.Add("Unit", typeof(String));
                tableReport2.Columns.Add("ชื่อตัวเลือก", typeof(String));
                tableReport2.Columns.Add("ราคาตั้งต้น", typeof(String));
                tableReport2.Columns.Add("ราคาขาย ราคาสุทธิ + vat - ส่วนลด", typeof(String));
                tableReport2.Columns.Add("จำนวน", typeof(String));
                tableReport2.Columns.Add("ราคาขายสุทธิ", typeof(String));
                tableReport2.Columns.Add("ส่วนลดจาก Shopee", typeof(String));
                tableReport2.Columns.Add("โค้ดส่วนลดชำระโดยผู้ขาย", typeof(String));
                tableReport2.Columns.Add("โค้ด Coins Cashback", typeof(String));
                tableReport2.Columns.Add("โค้ดส่วนลดชำระโดย Shopee", typeof(String));
                tableReport2.Columns.Add("โค้ดส่วนลด", typeof(String));
                tableReport2.Columns.Add("เข้าร่วมแคมเปญ bundle deal หรือไม่", typeof(String));
                tableReport2.Columns.Add("ส่วนลด bundle deal ชำระโดยผู้ขาย", typeof(String));
                tableReport2.Columns.Add("ส่วนลด bundle deal ชำระโดย Shopee", typeof(String));
                tableReport2.Columns.Add("ส่วนลดจากการใช้เหรียญ", typeof(String));
                tableReport2.Columns.Add("ส่วนลดทั้งหมดจากบัตรเครดิต", typeof(String));
                tableReport2.Columns.Add("ค่าคอมมิชชั่น", typeof(String));
                tableReport2.Columns.Add("ค่าธุรกรรมการชำระเงิน", typeof(String));
                tableReport2.Columns.Add("ต้นทุนขาย", typeof(String));
                tableReport2.Columns.Add("ค่าจัดส่งที่ชำระโดยผู้ซื้อ", typeof(String));
                tableReport2.Columns.Add("ค่าบริการ", typeof(String));
                tableReport2.Columns.Add("จำนวนเงินทั้งหมด", typeof(String));
                tableReport2.Columns.Add("ค่าจัดส่งโดยประมาณ", typeof(String));
                tableReport2.Columns.Add("ชื่อผู้รับ", typeof(String));
                tableReport2.Columns.Add("หมายเลขโทรศัพท์", typeof(String));
                tableReport2.Columns.Add("หมายเหตุจากผู้ซื้อ", typeof(String));
                tableReport2.Columns.Add("ที่อยู่ในการจัดส่ง", typeof(String));
                tableReport2.Columns.Add("ประเทศ", typeof(String));
                tableReport2.Columns.Add("จังหวัด", typeof(String));
                tableReport2.Columns.Add("เขต/อำเภอ", typeof(String));
                tableReport2.Columns.Add("รหัสไปรษณีย์", typeof(String));
                tableReport2.Columns.Add("ประเภทคำสั่งซื้อ", typeof(String));
                tableReport2.Columns.Add("เวลาที่ทำการสั่งซื้อสำเร็จ", typeof(String));
                tableReport2.Columns.Add("DiscountAmount", typeof(String));
                tableReport2.Columns.Add(" ", typeof(String));
                tableReport2.Columns.Add("PromotionID", typeof(String));
                #endregion tableReport2
                if (_item_Result3 != null)
                {

                    foreach (var temp_row in _item_Result3)
                    {

                        DataRow add_row = tableReport2.NewRow();
                        add_row[0] = temp_row.reference;//หมายเลขคำสั่งซื้อ
                        add_row[1] = temp_row.id_order_state;
                        add_row[2] = "";
                        add_row[3] = temp_row.cname;
                        add_row[4] = temp_row.date_add;
                        add_row[5] = "";
                        add_row[6] = "";
                        add_row[7] = "";
                        add_row[8] = "";
                        add_row[9] = "";
                        add_row[10] = "";
                        add_row[11] = temp_row.delivery_date;
                        add_row[12] = temp_row.tmaterial;
                        add_row[13] = temp_row.product_name;
                        add_row[14] = temp_row.freeitem.ToString();//ทดลอง สถานะของแถม
                        add_row[15] = "";
                        add_row[16] = "";
                        add_row[17] = "";
                        add_row[18] = temp_row.price_unit;
                        add_row[19] = temp_row.qty;
                        add_row[20] = temp_row.price_unit * temp_row.qty;
                        add_row[21] = "";
                        add_row[22] = "";
                        add_row[23] = "";
                        add_row[24] = "";
                        add_row[25] = "";
                        add_row[26] = "";
                        add_row[27] = "";
                        add_row[28] = "";
                        add_row[29] = "";
                        add_row[30] = "";
                        add_row[31] = "";
                        add_row[32] = "";
                        add_row[33] = "";
                        add_row[34] = "";
                        add_row[35] = "";
                        add_row[36] = "";
                        add_row[37] = "";
                        add_row[38] = "";
                        add_row[39] = temp_row.phone;
                        add_row[40] = "";
                        add_row[41] = temp_row.address;//ที่อยู่ในการจัดส่ง
                        add_row[42] = temp_row.county_name;
                        add_row[43] = temp_row.city;
                        add_row[44] = "";
                        add_row[45] = temp_row.postcode;
                        add_row[46] = "";
                        add_row[47] = "";
                        add_row[48] = "";
                        add_row[49] = "";
                        add_row[50] = "";
                        //add_row[5] = String.Format("{0:N}", temp_row["total_paid"]);

                        ReportListExport.ecom_fileinterface_list[_count_row2] = temp_row;
                        tableReport2.Rows.InsertAt(add_row, _count_row2++);

                    }
                }

                setGridData(Gv_select_interface, tableReport2);
                ViewState["ReportListExport_gv"] = tableReport2;

                ViewState["ReportListExport"] = ReportListExport.ecom_fileinterface_list;

                break;
            #endregion cmdSearch
            #region cmdReset
            case "cmdReset":
                tbDateStart.Text = String.Empty;
                tbDateEnd.Text = String.Empty;
                //_funcTool.setGvData(gvReport, null);
                break;
            #endregion cmdReset
            #region cmdExport
            case "cmdExport":
                DataTable _tempTable = new DataTable();
                DataTable tableReport = new DataTable();
                int _count_row = 0;
                int _temp_sum = 0;



                ecom_fileinterface_data[] _item_Result2 = (ecom_fileinterface_data[])ViewState["ReportListExport"];

                if (_item_Result2 == null)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่พบข้อมูลที่ต้องการ Export ค่ะ');", true);
                }
                else
                {
                    //_tempTable = (DataTable)ViewState["ReportListExport"];

                    //if (_tempTable == null)
                    //{
                    //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่พบข้อมูลที่ต้องการ Export ค่ะ');", true);
                    //    return;
                    //}
                    tableReport.Columns.Add("หมายเลขคำสั่งซื้อ", typeof(String));
                    tableReport.Columns.Add("สถานะการสั่งซื้อ", typeof(String));
                    tableReport.Columns.Add("สถานะการคืนเงินหรือคืนสินค้า", typeof(String));
                    tableReport.Columns.Add("ชื่อผู้ใช้(ผู้ซื้อ)", typeof(String));
                    tableReport.Columns.Add("วันที่ทำการสั่งซื้อ", typeof(String));
                    tableReport.Columns.Add("เวลาการชำระสินค้า", typeof(String));
                    tableReport.Columns.Add("ช่องทางการชำระเงิน", typeof(String));
                    tableReport.Columns.Add("ตัวเลือกการจัดส่ง", typeof(String));
                    tableReport.Columns.Add("วิธีการจัดส่ง", typeof(String));
                    tableReport.Columns.Add("หมายเลขติดตามพัสดุ", typeof(String));
                    tableReport.Columns.Add("วันที่คาดว่าจะทำการจัดส่งสินค้า", typeof(String));
                    tableReport.Columns.Add("เวลาส่งสินค้า", typeof(String));
                    tableReport.Columns.Add("เลขอ้างอิง Parent SKU", typeof(String));
                    tableReport.Columns.Add("ชื่อสินค้า", typeof(String));
                    tableReport.Columns.Add("เลขอ้างอิง SKU (SKU Reference No.)", typeof(String));
                    tableReport.Columns.Add("Unit", typeof(String));
                    tableReport.Columns.Add("ชื่อตัวเลือก", typeof(String));
                    tableReport.Columns.Add("ราคาตั้งต้น", typeof(String));
                    tableReport.Columns.Add("ราคาขาย ราคาสุทธิ + vat - ส่วนลด", typeof(String));
                    tableReport.Columns.Add("จำนวน", typeof(String));
                    tableReport.Columns.Add("ราคาขายสุทธิ", typeof(String));
                    tableReport.Columns.Add("ส่วนลดจาก Shopee", typeof(String));
                    tableReport.Columns.Add("โค้ดส่วนลดชำระโดยผู้ขาย", typeof(String));
                    tableReport.Columns.Add("โค้ด Coins Cashback", typeof(String));
                    tableReport.Columns.Add("โค้ดส่วนลดชำระโดย Shopee", typeof(String));
                    tableReport.Columns.Add("โค้ดส่วนลด", typeof(String));
                    tableReport.Columns.Add("เข้าร่วมแคมเปญ bundle deal หรือไม่", typeof(String));
                    tableReport.Columns.Add("ส่วนลด bundle deal ชำระโดยผู้ขาย", typeof(String));
                    tableReport.Columns.Add("ส่วนลด bundle deal ชำระโดย Shopee", typeof(String));
                    tableReport.Columns.Add("ส่วนลดจากการใช้เหรียญ", typeof(String));
                    tableReport.Columns.Add("ส่วนลดทั้งหมดจากบัตรเครดิต", typeof(String));
                    tableReport.Columns.Add("ค่าคอมมิชชั่น", typeof(String));
                    tableReport.Columns.Add("ค่าธุรกรรมการชำระเงิน", typeof(String));
                    tableReport.Columns.Add("ต้นทุนขาย", typeof(String));
                    tableReport.Columns.Add("ค่าจัดส่งที่ชำระโดยผู้ซื้อ", typeof(String));
                    tableReport.Columns.Add("ค่าบริการ", typeof(String));
                    tableReport.Columns.Add("จำนวนเงินทั้งหมด", typeof(String));
                    tableReport.Columns.Add("ค่าจัดส่งโดยประมาณ", typeof(String));
                    tableReport.Columns.Add("ชื่อผู้รับ", typeof(String));
                    tableReport.Columns.Add("หมายเลขโทรศัพท์", typeof(String));
                    tableReport.Columns.Add("หมายเหตุจากผู้ซื้อ", typeof(String));
                    tableReport.Columns.Add("ที่อยู่ในการจัดส่ง", typeof(String));
                    tableReport.Columns.Add("ประเทศ", typeof(String));
                    tableReport.Columns.Add("จังหวัด", typeof(String));
                    tableReport.Columns.Add("เขต/อำเภอ", typeof(String));
                    tableReport.Columns.Add("รหัสไปรษณีย์", typeof(String));
                    tableReport.Columns.Add("ประเภทคำสั่งซื้อ", typeof(String));
                    tableReport.Columns.Add("เวลาที่ทำการสั่งซื้อสำเร็จ", typeof(String));
                    tableReport.Columns.Add("บันทึก", typeof(String));
                    tableReport.Columns.Add("เพิ่ม column หน่วยของสินค้า", typeof(String));
                    tableReport.Columns.Add("Plant", typeof(String));


                    foreach (var temp_row in _item_Result2)
                    {


                        DataRow add_row = tableReport.NewRow();

                        add_row[0] = temp_row.reference;//หมายเลขคำสั่งซื้อ
                        add_row[1] = temp_row.id_order_state;

                        add_row[2] = "";
                        add_row[3] = temp_row.cname;
                        add_row[4] = temp_row.date_add;
                        add_row[5] = "";
                        add_row[6] = "";
                        add_row[7] = "";
                        add_row[8] = "";
                        add_row[9] = "";
                        add_row[10] = "";
                        add_row[11] = temp_row.delivery_date;

                        add_row[12] = temp_row.tmaterial;
                        add_row[13] = temp_row.product_name;
                        add_row[14] = "";
                        add_row[15] = "";
                        add_row[16] = "";
                        add_row[17] = "";
                        add_row[18] = temp_row.price_unit;
                        add_row[19] = temp_row.qty;
                        add_row[20] = temp_row.price_unit * temp_row.qty;
                        add_row[21] = "";
                        add_row[22] = "";
                        add_row[23] = "";
                        add_row[24] = "";
                        add_row[25] = "";
                        add_row[26] = "";
                        add_row[27] = "";
                        add_row[28] = "";
                        add_row[29] = "";
                        add_row[30] = "";
                        add_row[31] = "";
                        add_row[32] = "";
                        add_row[33] = "";
                        add_row[34] = "";
                        add_row[35] = "";
                        add_row[36] = "";
                        add_row[37] = "";
                        add_row[38] = "";
                        add_row[39] = temp_row.phone;
                        add_row[40] = "";
                        add_row[41] = temp_row.address;//ที่อยู่ในการจัดส่ง
                        add_row[42] = temp_row.county_name;
                        add_row[43] = temp_row.city;
                        add_row[44] = "";
                        add_row[45] = temp_row.postcode;
                        add_row[46] = "";
                        add_row[47] = "";
                        add_row[48] = "";
                        add_row[49] = "";
                        add_row[50] = "";


                        //add_row[5] = String.Format("{0:N}", temp_row["total_paid"]);

                        tableReport.Rows.InsertAt(add_row, _count_row++);
                    }

                    WriteExcelWithNPOI(tableReport, "xls", "report");
                }
                break;
            #endregion cmdExport
            #region btnImport
            case "btnImport":

             
                FileUpload FileUpload1 = (FileUpload)FvAddImport.FindControl("upload");
                DropDownList m0_channel = (DropDownList)FvAddImport.FindControl("m0_channel");
                LinkButton btnsavedatafile = (LinkButton)FvAddImport.FindControl("btnsavedatafile");


                ecom_m0_data data_m0_temp = new ecom_m0_data();

                var data_u0_temp = new ecom_u0_data[1];
                data_u0_temp[0] = new ecom_u0_data();
                ecom_m0_data[] _templist_default = (ecom_m0_data[])ViewState["ecom_m0_data"];

                ecom_m0_m1_bom[] _tempe_BOM = (ecom_m0_m1_bom[])ViewState["data_BOM"];

                /////////////////  ข้อมูล ของ m0  /////////////////////////
                var _ViewState_m0 = (from m0_viewstate in _templist_default
                                     where m0_viewstate.channel_idx == m0_channel.SelectedValue.ToString()
                                     select new
                                     {
                                         m0_viewstate.channel_idx,
                                         m0_viewstate.channel_name,
                                         m0_viewstate.cemp_idx,
                                         m0_viewstate.m0_order_type,
                                         m0_viewstate.m0_sale_org,
                                         m0_viewstate.m0_channel,
                                         m0_viewstate.m0_division,
                                         m0_viewstate.m0_sales_office,
                                         m0_viewstate.m0_sales_group,
                                         m0_viewstate.m0_customer_no,
                                         m0_viewstate.channel_status
                                     }).ToList();

                ////////////////////////////////////////////////////////
                /////
                data_u0_temp[0].user_emp_idx = _emp_idx;


                if (_ViewState_m0.Count() > 0)
                {
                    data_m0_temp.channel_idx = _ViewState_m0[0].channel_idx;
                    data_m0_temp.channel_name = _ViewState_m0[0].channel_name;
                    data_m0_temp.cemp_idx = _ViewState_m0[0].cemp_idx;
                    data_m0_temp.m0_order_type = _ViewState_m0[0].m0_order_type;
                    data_m0_temp.m0_sale_org = _ViewState_m0[0].m0_sale_org;
                    data_m0_temp.m0_channel = _ViewState_m0[0].m0_channel;
                    data_m0_temp.m0_division = _ViewState_m0[0].m0_division;
                    data_m0_temp.m0_sales_office = _ViewState_m0[0].m0_sales_office;
                    data_m0_temp.m0_sales_group = _ViewState_m0[0].m0_sales_group;
                    data_m0_temp.m0_customer_no = _ViewState_m0[0].m0_customer_no;
                    data_m0_temp.channel_status = _ViewState_m0[0].channel_status;


                    data_u0_temp[0].channel_idx = int.Parse(_ViewState_m0[0].channel_idx);



                }
                DataTable dt = new DataTable();
                if (upload.HasFile)
                {
                    string datetimeNow = DateTime.Now.ToString("yyyyMMdd_HHmmss_ffff");
                    string FileName = Path.GetFileName(upload.PostedFile.FileName);
                    string extension = Path.GetExtension(upload.PostedFile.FileName);
                    
                    string newFileName = datetimeNow + extension.ToLower();
                    data_u0_temp[0].file_name_import = FileName;
                    data_u0_temp[0].file_name_real = newFileName;
                    string folderPath = ConfigurationManager.AppSettings["path_fileinterface_import"];
                    string filePath = Server.MapPath(folderPath + newFileName);

                    if (extension.ToLower() == ".xls")
                    {

                        FileUpload1.SaveAs(filePath);
                        string conStr = String.Empty;

                        conStr = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                        conStr = String.Format(conStr, filePath, "Yes");
                        OleDbConnection connExcel = new OleDbConnection(conStr);
                        OleDbCommand cmdExcel = new OleDbCommand();
                        OleDbDataAdapter oda = new OleDbDataAdapter();

                        cmdExcel.Connection = connExcel;
                        connExcel.Open();
                        DataTable dtExcelSchema;
                        dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                        string SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                        connExcel.Close();
                        connExcel.Open();
                        cmdExcel.CommandText = "SELECT * From [" + SheetName + "]";
                        oda.SelectCommand = cmdExcel;
                        oda.Fill(dt);
                        connExcel.Close();
                    }
                    else
                    {
                        _funcTool.showAlert(this, "Import ได้เฉพาะไฟล์ Excel (นามสกุลไฟล์ .xls) เท่านั้น");
                    }
                }
                ViewState["data_u0_temp"] = data_u0_temp;
                ////data_ecom_fileinterface data_fileinterface_temp = new data_ecom_fileinterface();
                ////ecom_temptosap_data ecom_temptosap_detail = new ecom_temptosap_data();
                ////data_fileinterface_temp.ecom_temptosap_list = new ecom_temptosap_data[1];
                data_ecom_fileinterface data_fileinterface_temp = new data_ecom_fileinterface();
                DataTable _tempTable_ = new DataTable();
                //_tempTable_.Columns.Add("DiscountAmount", typeof(String));
                _tempTable_.Columns.Add("BOM", typeof(String));
                _tempTable_.Columns.Add("PO_number", typeof(String));
                _tempTable_.Columns.Add("Item", typeof(String));
                _tempTable_.Columns.Add("Name", typeof(String));
                _tempTable_.Columns.Add("Delivery_Date", typeof(String));
                _tempTable_.Columns.Add("Material", typeof(String));
                _tempTable_.Columns.Add("Price_Unit", typeof(float));
                _tempTable_.Columns.Add("Quantity", typeof(String));
                _tempTable_.Columns.Add("Tel", typeof(String));
                _tempTable_.Columns.Add("Address", typeof(String));
                _tempTable_.Columns.Add("Honse_number", typeof(String));
                _tempTable_.Columns.Add("Tax_number", typeof(String));
                _tempTable_.Columns.Add("usu", typeof(String));

                _tempTable_.Columns.Add("Country", typeof(String));
                _tempTable_.Columns.Add("City", typeof(String));
                _tempTable_.Columns.Add("District", typeof(String));
                _tempTable_.Columns.Add("Currency", typeof(String));
                _tempTable_.Columns.Add("Postal_Code", typeof(String));
                _tempTable_.Columns.Add("Plant", typeof(String));
                _tempTable_.Columns.Add("Division", typeof(String));
                _tempTable_.Columns.Add("Sale_Org", typeof(String));
                _tempTable_.Columns.Add("Sales_group", typeof(String));
                _tempTable_.Columns.Add("Sales_office", typeof(String));
                _tempTable_.Columns.Add("Channel", typeof(String));
                _tempTable_.Columns.Add("Order_type", typeof(String));
                _tempTable_.Columns.Add("Customer_No", typeof(String));
                _tempTable_.Columns.Add("VATAmount", typeof(float));
                _tempTable_.Columns.Add("NetPriceAmount", typeof(float));

                _tempTable_.Columns.Add("DiscountType", typeof(String));
                _tempTable_.Columns.Add("PromotionID", typeof(String));
                _tempTable_.Columns.Add("PromotionDesc", typeof(String));
                _tempTable_.Columns.Add("DiscountAmount", typeof(int));
                _tempTable_.Columns.Add("Item_cate", typeof(String));
                _tempTable_.Columns.Add("Discount_1", typeof(decimal));
                //var ds_Add_Sample = (DataSet)ViewState["ReportListImport"];
                //var u0_fileimport = new ecom_temptosap_data[dt.Rows.Count];
                string items_temp = string.Empty;
                string tag_error = string.Empty;
                int items_cnt = 0;
                int n_row = 0;




                for (var i = 0; i <= dt.Rows.Count - 1; i++)
                {
                    int iitemp = i;
                    string txt_bom = dt.Rows[i][13].ToString().Trim();
                    if (txt_bom == string.Empty) {
                        continue;
                    }
                    //litDebug.Text += "<br>" + oDate;
                    if (dt.Rows[i][13].ToString().Trim() != String.Empty)
                    {
                        string cDate = dt.Rows[i][4].ToString().Trim();
                        if (dt.Rows[i][4].ToString().Trim() == "-")
                        {
                           
                            tag_error += (i + 2) + ",";
                            continue;
                        }
                        else if (dt.Rows[i][4].ToString().Trim() == string.Empty)
                        {
                            iitemp = i - 1;
                            bool ck_wloop = true;
                            while (dt.Rows[iitemp][4].ToString().Trim() == string.Empty && iitemp >= 0 && ck_wloop)
                            {
                                iitemp = iitemp - 1;
                                if (dt.Rows[iitemp][4].ToString().Trim() != string.Empty && iitemp >= 0)
                                {
                                    ck_wloop = false;
                                  
                                }

                            }
                            if (dt.Rows[iitemp][4].ToString().Trim() != string.Empty && iitemp >= 0)
                            {
                           
                                cDate = dt.Rows[iitemp][4].ToString().Trim();
                            }
                            if (cDate == string.Empty)
                            {
                                litDebug.Text += txt_bom;
                                tag_error += (i + 2) + ",";
                                continue;
                            }
                        }

                        string oDate = Convert.ToDateTime(cDate).ToString("yyyyMMdd");

                        //string oDate = Convert.ToDateTime(dt.Rows[i][4].ToString().Trim()).ToString("yyyyMMdd");
                        var bom_data = (from bom_master in _tempe_BOM
                                        where bom_master.bom_name.ToString().Trim() == txt_bom
                                        && _funcTool.convertToInt(Convert.ToDateTime(bom_master.start_date.ToString().Trim()).ToString("yyyyMMdd")) <= _funcTool.convertToInt(oDate)
                                         && _funcTool.convertToInt(Convert.ToDateTime(bom_master.end_date.ToString().Trim()).ToString("yyyyMMdd")) >= _funcTool.convertToInt(oDate)
                                        select bom_master).ToList();



                        if (bom_data.Count > 0)
                        {
                            //litDebug.Text+= _funcTool.convertToDecimal(dt.Rows[i][48].ToString().Trim()).ToString()+"<br>";

                            foreach (ecom_m0_m1_bom detail in bom_data)
                            {

                                ecom_temptosap_data u0_fileimport = new ecom_temptosap_data();
                                if (items_temp == dt.Rows[iitemp][0].ToString().Trim() || dt.Rows[iitemp][0].ToString().Trim() == string.Empty)
                                {
                                    items_cnt++;
                                }
                                else
                                {
                                    items_cnt = 1;
                                    items_temp = dt.Rows[iitemp][0].ToString().Trim();
                                }
                                DataRow add_row = _tempTable_.NewRow();
                                int qty_buy = _funcTool.convertToInt(dt.Rows[i][19].ToString().Trim());
                                int total_qty = _funcTool.convertToInt(detail.amount.ToString()) * qty_buy;
                                if (items_cnt == 1 && dt.Rows[i][48].ToString().Trim() != string.Empty)
                                {
                                    add_row["Discount_1"] = _funcTool.convertToDecimal(dt.Rows[i][48].ToString().Trim());
                                }
                              
                                add_row["DiscountAmount"] = _funcTool.convertToInt(detail.discount.ToString());
                                add_row["PO_number"] = dt.Rows[iitemp][0].ToString().Trim();
                                add_row["Item"] = string.Format("{0:00}", items_cnt);
                                add_row["Name"] = dt.Rows[iitemp][3].ToString().Trim();
                                add_row["Delivery_Date"] = dt.Rows[iitemp][11].ToString().Trim();
                                add_row["Material"] = detail.material;
                                add_row["Price_Unit"] = float.Parse(String.Format("{0:F2}", _funcTool.convertToDecimal(detail.price.ToString())));
                                add_row["Quantity"] = total_qty;
                                add_row["Tel"] = dt.Rows[iitemp][39].ToString().Trim();
                                add_row["Address"] = dt.Rows[iitemp][41].ToString().Trim();
                                add_row["Country"] = dt.Rows[iitemp][42].ToString().Trim();
                                add_row["usu"] = detail.unit_name.ToString();
                                //add_row["Honse_number"] = txt_bom;
                                add_row["City"] = dt.Rows[iitemp][43].ToString().Trim();
                                add_row["District"] = dt.Rows[iitemp][44].ToString().Trim();
                                add_row["Currency"] = "THB";
                                add_row["Postal_Code"] = dt.Rows[iitemp][45].ToString().Trim();
                                add_row["Plant"] = "1300";
                                add_row["Division"] = data_m0_temp.m0_division;
                                add_row["Sale_Org"] = data_m0_temp.m0_sale_org;
                                add_row["Sales_group"] = data_m0_temp.m0_sales_group;
                                add_row["Sales_office"] = data_m0_temp.m0_sales_office;
                                add_row["Channel"] = data_m0_temp.m0_channel;
                                add_row["Order_type"] = data_m0_temp.m0_order_type;
                                add_row["Customer_No"] = data_m0_temp.m0_customer_no;
                                add_row["BOM"] = txt_bom;
                                add_row["DiscountType"] = String.Empty;
                                add_row["PromotionID"] = detail.SAP_code.ToString();
                                add_row["PromotionDesc"] = detail.PromotionDesc.ToString();
                                //  add_row["DiscountAmount"] = _funcTool.convertToInt(detail.amount.ToString());
                                add_row["Item_cate"] = "TAN";

                                add_row["VATAmount"] = float.Parse(String.Format("{0:F2}", _funcTool.convertToDecimal(detail.VAT.ToString()) * _funcTool.convertToDecimal(detail.amount.ToString()))) * qty_buy;
                                add_row["NetPriceAmount"] = float.Parse(String.Format("{0:F2}", (_funcTool.convertToDecimal(detail.price.ToString()) - _funcTool.convertToDecimal(detail.discount.ToString())) * _funcTool.convertToDecimal(detail.amount.ToString()))) * qty_buy;


                                switch (detail.type_promotion.ToString())
                                {
                                    case "1":
                                        add_row["DiscountType"] = "ZDC1";
                                        add_row["Item_cate"] = "TANN";
                                        break;
                                    case "2":
                                        add_row["DiscountType"] = "ZDC2";
                                        add_row["Item_cate"] = "TANN";
                                        break;
                                    case "3":
                                        add_row["DiscountType"] = "ZLSDEN04";
                                        add_row["Item_cate"] = "TANN";
                                        add_row["VATAmount"] = float.Parse(String.Format("{0:F2}", (0)));
                                        add_row["NetPriceAmount"] = float.Parse(String.Format("{0:F2}", (0)));
                                        break;
                                    default:
                                        add_row["DiscountType"] = "";
                                        break;
                                }

                                _tempTable_.Rows.InsertAt(add_row, n_row++);
                            }
                            //ค่าขนส่ง
                            decimal delivery_p = _funcTool.convertToDecimal(dt.Rows[i][37].ToString().Trim());
                            if (dt.Rows[i][37].ToString().Trim() != string.Empty && delivery_p != 0)
                            {
                                DataRow add_row = _tempTable_.NewRow();
                                add_row["BOM"] = txt_bom;
                                add_row["DiscountAmount"] = 0;
                                add_row["PO_number"] = dt.Rows[iitemp][0].ToString().Trim();
                                add_row["Item"] = string.Format("{0:00}", items_cnt + 1);
                                add_row["Name"] = dt.Rows[iitemp][3].ToString().Trim();
                                add_row["Delivery_Date"] = dt.Rows[iitemp][11].ToString().Trim();
                                add_row["Material"] = "8300032";
                                add_row["Price_Unit"] = float.Parse(String.Format("{0:F2}", delivery_p));
                                add_row["Quantity"] = 1;
                                add_row["Tel"] = dt.Rows[iitemp][39].ToString().Trim();
                                add_row["Address"] = dt.Rows[iitemp][41].ToString().Trim();
                                add_row["Country"] = dt.Rows[iitemp][42].ToString().Trim();
                                add_row["usu"] = "";
                                add_row["City"] = dt.Rows[iitemp][43].ToString().Trim();
                                add_row["District"] = dt.Rows[iitemp][44].ToString().Trim();
                                add_row["Currency"] = "THB";
                                add_row["Postal_Code"] = dt.Rows[iitemp][45].ToString().Trim();
                                add_row["Plant"] = "1300";
                                add_row["Division"] = data_m0_temp.m0_division;
                                add_row["Sale_Org"] = data_m0_temp.m0_sale_org;
                                add_row["Sales_group"] = data_m0_temp.m0_sales_group;
                                add_row["Sales_office"] = data_m0_temp.m0_sales_office;
                                add_row["Channel"] = data_m0_temp.m0_channel;
                                add_row["Order_type"] = data_m0_temp.m0_order_type;
                                add_row["Customer_No"] = data_m0_temp.m0_customer_no;
                                add_row["DiscountType"] = String.Empty;
                                add_row["PromotionID"] = "";
                                add_row["PromotionDesc"] = "";
                                //  add_row["DiscountAmount"] = _funcTool.convertToInt(detail.amount.ToString());
                                add_row["Item_cate"] = "ZTAW";

                                add_row["VATAmount"] = float.Parse(String.Format("{0:F2}", (delivery_p * 7) / 107));
                                add_row["NetPriceAmount"] = delivery_p;
                                _tempTable_.Rows.InsertAt(add_row, n_row++);
                            }
                        }
                        else
                        {
                        
                            tag_error += (i + 2) + ",";
                        }
                    }


                    //}
                }


                GridView GvExcel_Shows = (GridView)FvAddImport.FindControl("GvExcel_Show");

                setGridData(GvExcel_Show, _tempTable_);
                ViewState["ReportListImport"] = _tempTable_;
                if (_tempTable_.Rows.Count > 0)
                {
                    btnsavedatafile.Visible = true;
                    lbDocCancelExcel.Visible = true;
                    GvExcel_Show.Visible = true;
                }
                if (tag_error != string.Empty)
                {
                    if (_tempTable_.Rows.Count == 0)
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่สามารถ บันทึกได้ กรุณาตรวจสอบ BOM');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert(`มีบางข้อมูลผิดพลาด`);", true);
                    }
                    //litDebug.Text = tag_error.ToString().Remove(tag_error.Length - 1, 1) ;

                }
                break;
            #endregion btnImport
            #region btntoSAP
            case "btntoSAP":
                //string datetimeNows = DateTime.Now.ToString("yyyyMMdd_HHmmss_ffff");
                //string folderPath_save = ConfigurationManager.AppSettings["path_fileinterface_import"];
                //string FilePath = Server.MapPath(folderPath_save + "material_transaction" + datetimeNows + ".txt");
                //ecom_temptosap_data[] _item_Result4 = (ecom_temptosap_data[])ViewState["ReportListImport"];
                //string txt = string.Empty;
                //foreach (TableCell cell in GvExcel_Show.HeaderRow.Cells)
                //{
                //    //Add the Header row for Text file.
                //    txt += cell.Text + "\t\t";
                //}
                ////Add new line.
                //txt += "\r\n";

                //foreach (var temp_rows in _item_Result4)
                //{


                //    txt += temp_rows.Order_type + "\t\t";
                //    txt += temp_rows.Sale_Org + "\t\t";
                //    txt += temp_rows.Channel + "\t\t";
                //    txt += temp_rows.Division + "\t\t";
                //    txt += temp_rows.Sales_office + "\t\t";
                //    txt += temp_rows.Sales_group + "\t\t";
                //    txt += temp_rows.Customer_No + "\t\t";
                //    txt += temp_rows.Name + "\t\t";
                //    txt += temp_rows.Address + "\t\t";
                //    txt += temp_rows.Honse_number + "\t\t";
                //    txt += temp_rows.Postal_Code + "\t\t";
                //    txt += temp_rows.District + "\t\t";
                //    txt += temp_rows.City + "\t\t";
                //    txt += temp_rows.Country + "\t\t";
                //    txt += temp_rows.Tel + "\t\t";
                //    txt += temp_rows.Tax_number + "\t\t";
                //    txt += temp_rows.PO_number + "\t\t";
                //    txt += temp_rows.Delivery_Date + "\t\t";
                //    txt += temp_rows.Item + "\t\t";
                //    txt += temp_rows.Material + "\t\t";
                //    txt += temp_rows.Quantity + "\t\t";
                //    txt += temp_rows.usu + "\t\t";
                //    txt += temp_rows.Plant + "\t\t";
                //    txt += temp_rows.Price_Unit + "\t\t";
                //    txt += temp_rows.Currency + "\t\t";
                //    txt += temp_rows.DiscountType + "\t\t";
                //    txt += temp_rows.DiscountAmount + "\t\t";
                //    txt += temp_rows.PromotionID + "\t\t";
                //    txt += temp_rows.PromotionDesc + "\t\t";
                //    txt += temp_rows.NetPriceAmount + "\t\t";
                //    txt += temp_rows.VATAmount + "\t\t";
                //    txt += temp_rows.Item_cate + "\t\t";

                //    txt += "\r\n";
                //}


                //string FileContent = txt;
                //File.WriteAllText(FilePath, FileContent);
                break;
            #endregion btntoSAP
            #region btn_saveToSAP
            case "btn_saveToSAP":

                data_ecom_fileinterface box_data_u1 = new data_ecom_fileinterface();
                ecom_u1_data data_u1_temp_detail_ = new ecom_u1_data();
                box_data_u1.ecom_u1_data_list = new ecom_u1_data[1];

                data_u1_temp_detail_.u0_fileinterface_idx = cmdArg;
                data_u1_temp_detail_.export_sap_status = 1;
                data_u1_temp_detail_.export_sap_emp_idx = _emp_idx;
                box_data_u1.ecom_u1_data_list[0] = data_u1_temp_detail_;

                box_data_u1 = callServicePost_func(_urlGetfileinterface_u1_detail, box_data_u1);
                // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(box_data_u1));

                ecom_u1_data[] _item_Result4 = (ecom_u1_data[])box_data_u1.ecom_u1_data_list;
                string txt = string.Empty;
                txt += "Order Type\tSale Org.\tChannel\tDivision\tSales office\tSales group\tCustomer No.\tName\tAddress\tHonse number\tPostal Code\tDistrict\tCity\t";
                txt += "Country\tTel\tTax\tnumber\tPO number\tDelivery Date\tDiscount_1\tItem\tMaterial\tQuantity\tUn (Sale Unit)\tPlant\tPrice/Unit\tCurrency\tDiscountType\tDiscountAmount\t";
                txt += "PromotionID\tPromotionDesc\tNetPriceAmount\tVATAmount\tItem cate";
                //foreach (TableCell cell in GvExcel_Show.HeaderRow.Cells)
                //{
                //    //Add the Header row for Text file.
                //    txt += cell.Text + "\t\t";
                //}
                ////Add new line.
                txt += "\r\n";

                foreach (var temp_rows in _item_Result4)
                {
                    txt += temp_rows.u1_order_type + "\t";
                    txt += temp_rows.u1_sale_org + "\t";
                    txt += temp_rows.u1_channel + "\t";
                    txt += temp_rows.u1_division + "\t";
                    txt += temp_rows.u1_sales_office + "\t";
                    txt += temp_rows.u1_sales_group + "\t";
                    txt += temp_rows.u1_customer_no + "\t";
                    txt += temp_rows.u1_name + "\t";
                    txt += temp_rows.u1_address + "\t";
                    txt += temp_rows.u1_honse_number + "\t";
                    txt += temp_rows.u1_postal_code + "\t";
                    txt += temp_rows.u1_district + "\t";
                    txt += temp_rows.u1_city + "\t";
                    txt += temp_rows.u1_country + "\t";
                    txt += temp_rows.u1_tel + "\t";
                    txt += temp_rows.u1_tax_number + "\t";
                    txt += temp_rows.u1_po_number + "\t";
                    txt += temp_rows.u1_delivery_date + "\t";
                    txt += temp_rows.u1_discount_1 + "\t";
                    txt += temp_rows.u1_item + "\t";
                    txt += temp_rows.u1_material + "\t";
                    txt += temp_rows.u1_quantity + "\t";
                    txt += temp_rows.u1_un_sale_unit + "\t";
                    txt += temp_rows.u1_plant + "\t";
                    txt += temp_rows.u1_price_unit + "\t";
                    txt += temp_rows.u1_currency + "\t";
                    txt += temp_rows.u1_discounttype + "\t";
                    txt += temp_rows.u1_discountAmount + "\t";
                    txt += temp_rows.u1_promotionID + "\t";
                    txt += temp_rows.u1_promotionDesc + "\t";
                    txt += temp_rows.u1_netPriceAmount + "\t";
                    txt += temp_rows.u1_VATamount + "\t";
                    txt += temp_rows.u1_item_cate + "\t";

                    txt += "\r\n";
                }

                string datetimeNows = DateTime.Now.ToString("yyyyMMdd_HHmmss_ffff");
                string folderPath_save = ConfigurationManager.AppSettings["path_fileinterface_import"];
                string FilePath = Server.MapPath(folderPath_save + "fileinterface_" + datetimeNows + ".txt");
                string FileContent = txt;
                File.WriteAllText(FilePath, FileContent);

                get_GV_ecom_u0();
                break;
            #endregion btn_saveToSAP
            #region btnfiletobase
            case "btnfiletobase":

                data_ecom_fileinterface data_import_u0_u1 = new data_ecom_fileinterface();
                ecom_u0_data[] data_u0_detail = (ecom_u0_data[])ViewState["data_u0_temp"];
                //ecom_temptosap_data[] data_u1_detail = (ecom_temptosap_data[])ViewState["ReportListImport"];
                DataTable ReportListImport = (DataTable)ViewState["ReportListImport"];

                if (ReportListImport.Rows.Count > 0)
                {
                    ecom_temptosap_data[] data_u1_detail = new ecom_temptosap_data[ReportListImport.Rows.Count];

                    int i = 0;
                    foreach (DataRow detail in ReportListImport.Rows)
                    {
                        data_u1_detail[i] = new ecom_temptosap_data();

                        data_u1_detail[i].DiscountAmount = _funcTool.convertToInt(detail["DiscountAmount"].ToString());
                        data_u1_detail[i].PO_number = detail["PO_number"].ToString();
                        data_u1_detail[i].Item = detail["Item"].ToString();
                        data_u1_detail[i].Name = detail["Name"].ToString();
                        data_u1_detail[i].Delivery_Date = detail["Delivery_Date"].ToString();
                        data_u1_detail[i].Material = detail["Material"].ToString();
                        data_u1_detail[i].Price_Unit = float.Parse(detail["Price_Unit"].ToString());
                        data_u1_detail[i].Quantity = detail["Quantity"].ToString();
                        data_u1_detail[i].Tel = detail["Tel"].ToString();
                        data_u1_detail[i].Address = detail["Address"].ToString();
                        data_u1_detail[i].Country = detail["Country"].ToString();
                        data_u1_detail[i].usu = detail["usu"].ToString();

                        data_u1_detail[i].City = detail["City"].ToString();
                        data_u1_detail[i].District = detail["District"].ToString();
                        data_u1_detail[i].Currency = detail["Currency"].ToString();
                        data_u1_detail[i].Postal_Code = detail["Postal_Code"].ToString();
                        data_u1_detail[i].Plant = detail["Plant"].ToString();
                        data_u1_detail[i].Division = detail["Division"].ToString();
                        data_u1_detail[i].Sale_Org = detail["Sale_Org"].ToString();
                        data_u1_detail[i].Sales_group = detail["Sales_group"].ToString();
                        data_u1_detail[i].Sales_office = detail["Sales_office"].ToString();
                        data_u1_detail[i].Channel = detail["Channel"].ToString();
                        data_u1_detail[i].Order_type = detail["Order_type"].ToString();
                        data_u1_detail[i].Customer_No = detail["Customer_No"].ToString();

                        data_u1_detail[i].DiscountType = detail["DiscountType"].ToString();
                        data_u1_detail[i].PromotionID = detail["PromotionID"].ToString();
                        data_u1_detail[i].PromotionDesc = detail["PromotionDesc"].ToString();

                        data_u1_detail[i].Item_cate = detail["Item_cate"].ToString();
                        data_u1_detail[i].Discount_1 = _funcTool.convertToDecimal(detail["Discount_1"].ToString());

                        data_u1_detail[i].VATAmount = float.Parse(detail["VATAmount"].ToString());
                        data_u1_detail[i].NetPriceAmount = float.Parse(detail["NetPriceAmount"].ToString());
                        i++;
                    }
                    data_import_u0_u1.ecom_temptosap_list = data_u1_detail;
                    data_import_u0_u1.ecom_u0_data_list = data_u0_detail;
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_import_u0_u1));

                    data_fileinterface = callServicePost_func(_urlSetfileinterface_insert, data_import_u0_u1);
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_import_u0_u1));

                    if (data_fileinterface.return_code.ToString() == "0" || data_fileinterface.return_code == 0)
                    {
                        LinkButton btnsavedatafile2 = (LinkButton)FvAddImport.FindControl("btnsavedatafile");

                        btnsavedatafile2.Visible = false;
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('บันทึกข้อมุลสำเร็จ');", true);
                        data_ecom_fileinterface u0_fileimport_empty = new data_ecom_fileinterface();
                        ecom_temptosap_data u0_fileimport_empty_list = new ecom_temptosap_data();
                        u0_fileimport_empty.ecom_temptosap_list = new ecom_temptosap_data[0];
                        //ecom_temptosap_data u0_fileimport_empty = new ecom_temptosap_data();
                        setGridData(GvExcel_Show, u0_fileimport_empty.ecom_temptosap_list);
                        GvExcel_Show.Visible = false;
                        ViewState["ReportListImport"] = null;
                        setActiveTab("view_listfile", 0);

                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('!!! บันทึกข้อมูลไม่สำเร็จ');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('!!! บันทึกข้อมูลไม่สำเร็จ');", true);
                }

                break;
            #endregion btnfiletobase
            #region btn_view_temp
            case "btn_view_temp":

                data_ecom_fileinterface data_u1_temp = new data_ecom_fileinterface();
                ecom_u1_data data_u1_temp_detail = new ecom_u1_data();
                data_u1_temp.ecom_u1_data_list = new ecom_u1_data[1];

                data_u1_temp_detail.u0_fileinterface_idx = cmdArg;
                data_u1_temp.ecom_u1_data_list[0] = data_u1_temp_detail;

                data_u1_temp = callServicePost_func(_urlGetfileinterface_u1_detail, data_u1_temp);

                // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u1_temp));
                div_detail.Visible = true;
                GV_ecom_u0.Visible = false;
                setGridData(Gv_data_interface_from_base, data_u1_temp.ecom_u1_data_list);
                ViewState["Gv_data_interface_from_base"] = data_u1_temp.ecom_u1_data_list;
                break;
            #endregion btn_view_temp
            #region btn_backToGv
            case "btn_backToGv":
                div_detail.Visible = false;
                GV_ecom_u0.Visible = true;
                break;
            #endregion btn_backToGv
            #region btnTodelete_temp
            case "btnTodelete_temp":

                data_ecom_fileinterface data_u0_del = new data_ecom_fileinterface();
                ecom_u0_data data_u0_del_detail = new ecom_u0_data();
                data_u0_del.ecom_u0_data_list = new ecom_u0_data[1];

                data_u0_del_detail.u0_fileinterface_idx = int.Parse(cmdArg);
                data_u0_del.ecom_u0_data_list[0] = data_u0_del_detail;

                data_u1_temp = callServicePost_func(_urlDelfileinterface_u1, data_u0_del);

                get_GV_ecom_u0();
                break;
            #endregion btnTodelete_temp
            #region cmdDocCancel
            case "cmdDocCancel":
                FormView FvAddImport_d = (FormView)view_ifif.FindControl("FvAddImport");
                setFormData(FvAddImport_d, FormViewMode.Insert, null);
                FormView FvAddImport_c = (FormView)view_ifif.FindControl("FvAddImport");
                LinkButton btnsavedatafile_c = (LinkButton)FvAddImport_c.FindControl("btnsavedatafile");
                btnsavedatafile_c.Visible = false;
                set_default_import();
                break;
                #endregion cmdDocCancel
        }
    }
    #endregion event command
    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvExcel_Show":
                GvExcel_Show.PageIndex = e.NewPageIndex;
                GvExcel_Show.DataBind();
                setGridData(GvExcel_Show, ViewState["ReportListImport"]);
                break;

            case "Gv_select_interface":
                Gv_select_interface.PageIndex = e.NewPageIndex;
                Gv_select_interface.DataBind();
                setGridData(Gv_select_interface, ViewState["ReportListExport_gv"]);
                break;

            case "GV_ecom_u0":

                GV_ecom_u0.PageIndex = e.NewPageIndex;
                GV_ecom_u0.DataBind();
                setGridData(GV_ecom_u0, ViewState["GV_ecom_u0"]);
                break;
            case "Gv_data_interface_from_base":
                Gv_data_interface_from_base.PageIndex = e.NewPageIndex;
                Gv_data_interface_from_base.DataBind();
                setGridData(Gv_data_interface_from_base, ViewState["Gv_data_interface_from_base"]);
                break;

        }
    }

    protected void Select_Place(Object data)
    {

        setGridData(Gv_select_interface, data);

    }

    protected void Select_data_u0(Object data)
    {

        setGridData(GV_ecom_u0, data);

    }
    protected void new_data_temp()
    {

        //var ds_sample_data = new DataSet();
        //ds_sample_data.Tables.Add("NewTempData");


        //ds_sample_data.Tables[0].Columns.Add("DiscountAmount", typeof(String));
        //ds_sample_data.Tables[0].Columns.Add("PO_number", typeof(String));
        //ds_sample_data.Tables[0].Columns.Add("Item", typeof(String));
        //ds_sample_data.Tables[0].Columns.Add("Name", typeof(String));
        //ds_sample_data.Tables[0].Columns.Add("Delivery_Date", typeof(String));
        //ds_sample_data.Tables[0].Columns.Add("Material", typeof(String));
        //ds_sample_data.Tables[0].Columns.Add("Price_Unit", typeof(float));
        //ds_sample_data.Tables[0].Columns.Add("Quantity", typeof(String));
        //ds_sample_data.Tables[0].Columns.Add("Tel", typeof(String));
        //ds_sample_data.Tables[0].Columns.Add("Address", typeof(String));
        //ds_sample_data.Tables[0].Columns.Add("Country", typeof(String));
        //ds_sample_data.Tables[0].Columns.Add("City", typeof(String));
        //ds_sample_data.Tables[0].Columns.Add("District", typeof(String));
        //ds_sample_data.Tables[0].Columns.Add("Currency", typeof(String));
        //ds_sample_data.Tables[0].Columns.Add("Postal_Code", typeof(String));
        //ds_sample_data.Tables[0].Columns.Add("Plant", typeof(String));
        //ds_sample_data.Tables[0].Columns.Add("Division", typeof(String));
        //ds_sample_data.Tables[0].Columns.Add("Sale_Org", typeof(String));
        //ds_sample_data.Tables[0].Columns.Add("Sales_group", typeof(String));
        //ds_sample_data.Tables[0].Columns.Add("Sales_office", typeof(String));
        //ds_sample_data.Tables[0].Columns.Add("Channel", typeof(String));
        //ds_sample_data.Tables[0].Columns.Add("Order_type", typeof(String));
        //ds_sample_data.Tables[0].Columns.Add("Customer_No", typeof(String));
        //ds_sample_data.Tables[0].Columns.Add("VATAmount", typeof(float));
        //ds_sample_data.Tables[0].Columns.Add("NetPriceAmount", typeof(float));
        //ViewState["ReportListImport"] = ds_sample_data;
    }


    #region setGridData
    private void setGridData(GridView gvName, Object obj)
    {
        //ViewState["ReportListExport"] = obj;
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion
    #region reuse


    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        //ViewState["ReportListExport"] = null;
    }

    protected void setTrigger()
    {
        // trigger
        linkBtnTrigger(lbSearch);
        linkBtnTrigger(lbReset);
        linkBtnTrigger(lbExport);
    }



    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    protected void WriteExcelWithNPOI(DataTable dt, String extension, String fileName)
    {
        IWorkbook workbook;
        if (extension == "xlsx")
        {
            workbook = new XSSFWorkbook();
        }
        else if (extension == "xls")
        {
            workbook = new HSSFWorkbook();
        }
        else
        {
            throw new Exception("This format is not supported");
        }
        ISheet sheet1 = workbook.CreateSheet("Sheet 1");
        IRow row1 = sheet1.CreateRow(0);
        for (int j = 0; j < dt.Columns.Count; j++)
        {
            ICell cell = row1.CreateCell(j);
            String columnName = dt.Columns[j].ToString();
            cell.SetCellValue(columnName);
        }
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            IRow row = sheet1.CreateRow(i + 1);
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                ICell cell = row.CreateCell(j);
                String columnName = dt.Columns[j].ToString();
                cell.SetCellValue(dt.Rows[i][columnName].ToString());
            }
        }
        using (var exportData = new MemoryStream())
        {
            Response.Clear();
            workbook.Write(exportData);
            if (extension == "xlsx")
            {
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xlsx"));
                Response.BinaryWrite(exportData.ToArray());
            }
            else if (extension == "xls")
            {
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xls"));
                Response.BinaryWrite(exportData.GetBuffer());
            }
            Response.End();
        }
    }
    #endregion reuse


    protected data_ecom_fileinterface callServicePost_func(string _cmdUrl, data_ecom_fileinterface _data_post, int test = 0)
    {
        _localJson = _funcTool.convertObjectToJson(_data_post);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        if (test == 1)
        {

            litDebug.Text += "<br>" + _localJson;
        }
        else
        {
            _data_post = (data_ecom_fileinterface)_funcTool.convertJsonToObject(typeof(data_ecom_fileinterface), _localJson);



        }
        return _data_post;
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            default:
                break;

                //        case "Gv_select_place":
                //            if (e.Row.RowType == DataControlRowType.DataRow)
                //            {
                //                Label lbpplace_status = (Label)e.Row.Cells[3].FindControl("lbpplace_status");
                //                Label place_statusOnline = (Label)e.Row.Cells[3].FindControl("place_statusOnline");
                //                Label place_statusOffline = (Label)e.Row.Cells[3].FindControl("place_statusOffline");
                //                ViewState["_place_status"] = lbpplace_status.Text;
                //                if (ViewState["_place_status"].ToString() == "1")
                //                {
                //                    place_statusOnline.Visible = true;
                //                }
                //                else if (ViewState["_place_status"].ToString() == "0")
                //                {
                //                    place_statusOffline.Visible = true;
                //                }
                //            }
                //            if (e.Row.RowState.ToString().Contains("Edit"))
                //            {
                //                GridView editGrid = sender as GridView;
                //                int colSpan = editGrid.Columns.Count;
                //                for (int i = 1; i < colSpan; i++)
                //                {
                //                    e.Row.Cells[i].Visible = false;
                //                    e.Row.Cells[i].Controls.Clear();
                //                }

                //                e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                //                e.Row.Cells[0].CssClass = "";
                //                //btn_addplace.Visible = true;
                //                //setFormData(Fv_Insert_Result, FormViewMode.ReadOnly, null);

                //                //MultiView1.Visible = true;
                //                //Gv_select_unit.Visible = false;
                //                //btn_addplace.Visible = true;
                //                //FvInsert.Visible = false;

                //            }



                //            break;
                //        case "GV_ecom_u0":

                //            break;

        }
    }
    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }
}

