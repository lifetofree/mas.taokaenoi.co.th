﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_drc_m0_manage_setname : System.Web.UI.Page
{
    #region Connect
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();
    data_drc _data_drc = new data_drc();

    data_qa_cims _data_qa_cims = new data_qa_cims();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];


    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    //-- employee --//


    //
    static string _urlGetM0SetnameDRC = _serviceUrl + ConfigurationManager.AppSettings["urlGetM0SetnameDRC"];
    static string _urlSetM0SetnameDRC = _serviceUrl + ConfigurationManager.AppSettings["urlSetM0SetnameDRC"];
    static string _urlSetDelM0SetnameDRC = _serviceUrl + ConfigurationManager.AppSettings["urlSetDelM0SetnameDRC"];
    static string _urlSetUpdateRootM0SetName = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateRootM0SetName"];

    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    bool _flag_qmr = false;
    #endregion

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {

            initPage();
            //MvMaster_lab.SetActiveView(view_Genaral);
            //Select_lab();
        }

    }
    #endregion

    #region Page_Init
    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
        //check permission
        //foreach (int item in rdept_qmr)
        //{
        //    if (_dataEmployee.employee_list[0].rdept_idx == item)
        //    {
        //        _flag_qmr = true;
        //        break;
        //    }
        //}
    }
    #endregion

    #region getEmployeeProfile
    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }
    #endregion

    #region initPage
    protected void initPage()
    {

        clearSession();
        clearViewState();
        setActiveView("view_Genaral", 0);
        //setFormData(Fv_Insert_Result, FormViewMode.ReadOnly, null);
        SelectSetnane();
    }
    #endregion

    #region Select Bind Data
    protected void SelectSetnane()
    {

        data_drc _data_drc = new data_drc();
        SetNameDetail m0_setname_insert = new SetNameDetail();

        _data_drc.BoxM0SetNameList = new SetNameDetail[1];
        _data_drc.BoxM0SetNameList[0] = m0_setname_insert;

        _data_drc = callServicePostDRC(_urlGetM0SetnameDRC, _data_drc);
        setGridData(GvSetNameDRC, _data_drc.BoxM0SetNameList);

        //test_lab.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_drc));
       
    }

    protected void SelectRootSetnane()
    {

        data_drc _data_drc_root = new data_drc();
        SetNameDetail m0_root_setname = new SetNameDetail();
        _data_drc_root.BoxM0SetNameList = new SetNameDetail[1];

        m0_root_setname.setroot_idx = int.Parse(ViewState["vs_setname_sidx"].ToString());

        _data_drc_root.BoxM0SetNameList[0] = m0_root_setname;

        _data_drc_root = callServicePostDRC(_urlGetM0SetnameDRC, _data_drc_root);
        setGridData(GvRootSetName, _data_drc_root.BoxM0SetNameList);

        //test_lab.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_drc));

    }
    #endregion

    #region setDdlData
    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }
    #endregion

    #region clear
    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        // ViewState["listData"] = null;
    }

    #endregion

    #region setGridData
    private void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region setformdata
    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }
    #endregion

    #region callService
    protected data_qa_cims callServicePostMasterQACIMS(string _cmdUrl, data_qa_cims _data_qa_cims)
    {
        _localJson = _funcTool.convertObjectToJson(_data_qa_cims);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_qa_cims = (data_qa_cims)_funcTool.convertJsonToObject(typeof(data_qa_cims), _localJson);


        return _data_qa_cims;
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_drc callServicePostDRC(string _cmdUrl, data_drc _data_drc)
    {
        _localJson = _funcTool.convertObjectToJson(_data_drc);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_drc = (data_drc)_funcTool.convertJsonToObject(typeof(data_drc), _localJson);


        return _data_drc;
    }

    #endregion

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();
        switch (cmdName)
        {

            case "cmdAddSetname":
                switch (cmdArg)
                {
                    case "0":
                        GvSetNameDRC.EditIndex = -1;
                        // btn_addlab.Visible = false;
                        setFormData(FvInsertSetname, FormViewMode.Insert, null);
                        FvInsertSetname.Visible = true;

                        SelectSetnane();
                        break;
                    case "1":
                        GvRootSetName.EditIndex = -1;

                        btnAddRootSet.Visible = false;
                        setFormData(fvform_Insert_Root, FormViewMode.Insert, null);
                        //getM1Set_labName();
                        //FormView fvDocDetail_insert_Root_qa = (FormView)view_Genaral.FindControl("fvform_Insert_Root");
                        //getM1equipment_nameList((DropDownList)fvDocDetail_insert_Root_qa.FindControl("DD_EquipmentName"), "0");
                        fvform_Insert_Root.Visible = true;
                        TextBox txt_sidx_root = (TextBox)fvform_Insert_Root.FindControl("txt_sidx_root");
                        TextBox txt_sidx_root_list = (TextBox)fvform_Insert_Root.FindControl("txt_sidx_root_list");

                        txt_sidx_root.Text = ViewState["vs_setname_sidx"].ToString();
                        txt_sidx_root_list.Text = ViewState["vs_setname"].ToString();



                        SelectRootSetnane();


                        break;
                }
                //btn_addlab.Visible = false;
                //setFormData(Fv_Insert_Result, FormViewMode.Insert, null);
                //Fv_Insert_Result.Visible = true;

                break;
            case "cmdCancel":
                switch (cmdArg)
                {
                    case "0":
                        GvSetNameDRC.EditIndex = -1;
                        btn_addsetname.Visible = true;
                        setActiveView("view_Genaral", 0);
                        FvInsertSetname.Visible = false;
                        setFormData(FvInsertSetname, FormViewMode.Insert, null);

                        SETFOCUS.Focus();
                        break;
                    case "1":
                        GvRootSetName.EditIndex = -1;

                        btnAddRootSet.Visible = true;
                        setActiveView("view_CreateRoot", 0);
                        fvform_Insert_Root.Visible = false;
                        setFormData(fvform_Insert_Root, FormViewMode.Insert, null);

                        SETFOCUS.Focus();
                        break;
                }

                break;

            case "cmdback":
                //ViewState["m0_lab_idx"] = 0;
                btn_addsetname.Visible = true;
                setActiveView("view_Genaral", 0);
                setFormData(FvInsertSetname, FormViewMode.Insert, null);
                SETFOCUS.Focus();
                break;


            case "Lbtn_cancel_lab":
                btn_addsetname.Visible = true;
                setFormData(FvInsertSetname, FormViewMode.ReadOnly, null);
                FvInsertSetname.Visible = false;
                break;

            case "cmdSave":
                switch (cmdArg)
                {
                    case "0": //insert setname
                      
                        TextBox txtset_name = (TextBox)FvInsertSetname.FindControl("txtset_name");
                        DropDownList ddl_status_setname = (DropDownList)FvInsertSetname.FindControl("ddl_status_setname");

                        data_drc _data_drc = new data_drc();
                        SetNameDetail m0_setname_insert = new SetNameDetail();
                        _data_drc.BoxM0SetNameList = new SetNameDetail[1];

                        m0_setname_insert.sidx = 0;
                        m0_setname_insert.set_name = txtset_name.Text;
                        m0_setname_insert.set_status = int.Parse(ddl_status_setname.SelectedValue);
                        m0_setname_insert.cempidx = _emp_idx;

                        _data_drc.BoxM0SetNameList[0] = m0_setname_insert;

                        _data_drc = callServicePostDRC(_urlSetM0SetnameDRC, _data_drc);
                        //test_lab.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(lab_bIN));

                        setActiveView("view_Genaral", 0);
                       
                        btn_addsetname.Visible = true;
                        SelectSetnane();

                        SETFOCUS.Focus();
                        break;

                    case "1":

                        TextBox txt_sidx_root = (TextBox)fvform_Insert_Root.FindControl("txt_sidx_root");
                        TextBox txt_statusname_set = (TextBox)fvform_Insert_Root.FindControl("txt_statusname_set");
                        DropDownList ddlRootSetStatus_set = (DropDownList)fvform_Insert_Root.FindControl("ddlRootSetStatus");



                        data_drc _data_drc_root = new data_drc();
                        SetNameDetail m0_setname_root = new SetNameDetail();
                        _data_drc_root.BoxM0SetNameList = new SetNameDetail[1];

                        m0_setname_root.sidx = 0;
                        m0_setname_root.setroot_idx = int.Parse(txt_sidx_root.Text);
                        m0_setname_root.set_name = txt_statusname_set.Text;
                        m0_setname_root.set_status = int.Parse(ddlRootSetStatus_set.SelectedValue);
                        m0_setname_root.cempidx = _emp_idx;

                        _data_drc_root.BoxM0SetNameList[0] = m0_setname_root;

                        _data_drc_root = callServicePostDRC(_urlSetM0SetnameDRC, _data_drc_root);

                        setActiveView("view_CreateRoot", 0);
                        setFormData(fvform_Insert_Root, FormViewMode.ReadOnly, null);
                        //fvform_Insert_Root.Visible = true;
                        btnAddRootSet.Visible = true;

                        SelectRootSetnane();
                        SETFOCUS.Focus();
                        break;


                }
                break;
            case "cmdDeleteSetname":

                int setname_idx = int.Parse(cmdArg);

                data_drc _data_drc_setname_del = new data_drc();
                SetNameDetail m0_setname_del = new SetNameDetail();
                _data_drc_setname_del.BoxM0SetNameList = new SetNameDetail[1];

                m0_setname_del.sidx = setname_idx;
                m0_setname_del.cempidx = _emp_idx;

                _data_drc_setname_del.BoxM0SetNameList[0] = m0_setname_del;

                _data_drc_setname_del = callServicePostDRC(_urlSetDelM0SetnameDRC, _data_drc_setname_del);

                FvInsertSetname.Visible = false;
                btn_addsetname.Visible = true;
                SelectSetnane();
                SETFOCUS.Focus();

                break;

            case "btnTodeleteRoot":
                int sidx_root_del = int.Parse(cmdArg);

                data_drc _data_drc_root_setname_del = new data_drc();
                SetNameDetail m0_setname_del_root = new SetNameDetail();
                _data_drc_root_setname_del.BoxM0SetNameList = new SetNameDetail[1];

                m0_setname_del_root.sidx = sidx_root_del;
                m0_setname_del_root.cempidx = _emp_idx;

                _data_drc_root_setname_del.BoxM0SetNameList[0] = m0_setname_del_root;

                _data_drc_root_setname_del = callServicePostDRC(_urlSetDelM0SetnameDRC, _data_drc_root_setname_del);

                fvform_Insert_Root.Visible = false;
                btnAddRootSet.Visible = true;
                SelectRootSetnane();
                SETFOCUS.Focus();
                break;

            case "cmdViewSetname":

                GvRootSetName.EditIndex = -1;
                btnAddRootSet.Visible = true;

                string[] arg2 = new string[2];
                arg2 = e.CommandArgument.ToString().Split(';');
                int setname_sidx = int.Parse(arg2[0]);
                string setname_ = arg2[1];


                //int setname_sidx = int.Parse(cmdArg);

                ViewState["vs_setname_sidx"] = setname_sidx;
                ViewState["vs_setname"] = setname_;

                //test_lab.Text = ViewState["m0_lab_idx"].ToString();
                setActiveView("view_CreateRoot", setname_sidx);
                setFormData(fvform_Insert_Root, FormViewMode.ReadOnly, null);

                //TextBox txt_sidx_root = (TextBox)fvform_Insert_Root.FindControl("txt_sidx_root");
                //TextBox txt_sidx_root_list = (TextBox)fvform_Insert_Root.FindControl("txt_sidx_root_list");

                //txt_sidx_root.Text = ViewState["vs_setname_sidx"].ToString();
                //txt_sidx_root_list.Text = ViewState["vs_setname"].ToString();


                SelectRootSetnane();
                SETFOCUS.Focus();
                break;
        }

    }
    #endregion

    #region Masterbound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvSetNameDRC":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbl_set_status = (Label)e.Row.FindControl("lbl_set_status");
                    Label lbl_set_statusOpen = (Label)e.Row.FindControl("lbl_set_statusOpen");
                    Label lbl_set_statusClose = (Label)e.Row.FindControl("lbl_set_statusClose");

                    ViewState["vs_setname_status"] = lbl_set_status.Text;


                    if (ViewState["vs_setname_status"].ToString() == "1")
                    {
                        lbl_set_statusOpen.Visible = true;
                    }
                    else if (ViewState["vs_setname_status"].ToString() == "0")
                    {
                        lbl_set_statusClose.Visible = true;
                    }

                }


                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
  
                    btn_addsetname.Visible = true;
                    FvInsertSetname.Visible = false;

                    //setFormData(Fv_Insert_Result, FormViewMode.ReadOnly, null);
                }
                break;

            case "GvRootSetName":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbl_set_status = (Label)e.Row.FindControl("lbl_set_status");
                    Label lbl_set_status_Online = (Label)e.Row.FindControl("lbl_set_status_Online");
                    Label lbl_set_status_Offline = (Label)e.Row.FindControl("lbl_set_status_Offline");



                    ViewState["_lbtestDetail_status_root"] = lbl_set_status.Text;


                    if (ViewState["_lbtestDetail_status_root"].ToString() == "1")
                    {
                        lbl_set_status_Online.Visible = true;
                    }
                    else if (ViewState["_lbtestDetail_status_root"].ToString() == "0")
                    {
                        lbl_set_status_Offline.Visible = true;
                    }

                }

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    //FormView fvDocDetail_insert_qa = (FormView)view_Genaral.FindControl("Fv_Insert_Result");

                    //TextBox txt_equipmentname = (TextBox)e.Row.Cells[0].FindControl("txt_equipmentname");
                    //getM1equipment_nameList((DropDownList)e.Row.Cells[0].FindControl("DD_equipmentname"), txt_equipmentname.Text);


                    TextBox txt_Setname_idx_edit = (TextBox)e.Row.FindControl("txt_Setname_idx_edit");
                    TextBox txt_Setname_edit = (TextBox)e.Row.FindControl("txt_Setname_edit");

                    txt_Setname_idx_edit.Text = ViewState["vs_setname_sidx"].ToString();
                    txt_Setname_edit.Text = ViewState["vs_setname"].ToString();


                    btnAddRootSet.Visible = true;
                    fvform_Insert_Root.Visible = false;
                    //setFormData(fvform_Insert_Root, FormViewMode.ReadOnly, null);
                }

                break;


        }
    }
    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvSetNameDRC":
                GvSetNameDRC.EditIndex = e.NewEditIndex;
                SelectSetnane();
                break;
            case "GvRootSetName":
                GvRootSetName.EditIndex = e.NewEditIndex;
                SelectRootSetnane();
                break;
        }
    }
    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvSetNameDRC":

                var txt_sidx_edit = (TextBox)GvSetNameDRC.Rows[e.RowIndex].FindControl("txt_sidx_edit");
                var txt_set_name_edit = (TextBox)GvSetNameDRC.Rows[e.RowIndex].FindControl("txt_set_name_edit");
                var dd_set_status_edit = (DropDownList)GvSetNameDRC.Rows[e.RowIndex].FindControl("dd_set_status_edit");


                GvSetNameDRC.EditIndex = -1;

                data_drc _data_drc = new data_drc();
                SetNameDetail m0_setname_insert = new SetNameDetail();
                _data_drc.BoxM0SetNameList = new SetNameDetail[1];

                m0_setname_insert.sidx = int.Parse(txt_sidx_edit.Text);
                m0_setname_insert.set_name = txt_set_name_edit.Text;
                m0_setname_insert.set_status = int.Parse(dd_set_status_edit.SelectedValue);
                m0_setname_insert.cempidx = _emp_idx;

                _data_drc.BoxM0SetNameList[0] = m0_setname_insert;

                _data_drc = callServicePostDRC(_urlSetM0SetnameDRC, _data_drc);


                SelectSetnane();

                break;

            case "GvRootSetName":

                var lblUIDX_edit = (Label)GvRootSetName.Rows[e.RowIndex].FindControl("lblUIDX_edit");
                var txt_Setname_idx_edit = (TextBox)GvRootSetName.Rows[e.RowIndex].FindControl("txt_Setname_idx_edit");
                var txt_set_name_edit_root = (TextBox)GvRootSetName.Rows[e.RowIndex].FindControl("txt_set_name_edit");
                var ddlset_status_edit_root = (DropDownList)GvRootSetName.Rows[e.RowIndex].FindControl("ddlset_status_edit");

                GvRootSetName.EditIndex = -1;

                data_drc _data_drc_edit_root = new data_drc();
                SetNameDetail m0_setname_edit_root = new SetNameDetail();
                _data_drc_edit_root.BoxM0SetNameList = new SetNameDetail[1];

                m0_setname_edit_root.sidx = int.Parse(lblUIDX_edit.Text);
                m0_setname_edit_root.set_name = txt_set_name_edit_root.Text;
                m0_setname_edit_root.set_status = int.Parse(ddlset_status_edit_root.SelectedValue);
                m0_setname_edit_root.cempidx = _emp_idx;

                _data_drc_edit_root.BoxM0SetNameList[0] = m0_setname_edit_root;

               // test_lab.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_drc_edit_root));
                _data_drc_edit_root = callServicePostDRC(_urlSetUpdateRootM0SetName, _data_drc_edit_root);

                SelectRootSetnane();
                break;

        }
    }
    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvSetNameDRC":
                GvSetNameDRC.EditIndex = -1;
                SelectSetnane();
                btn_addsetname.Visible = true;
                break;
            case "GvRootSetName":
                GvRootSetName.EditIndex = -1;
                SelectRootSetnane();
                btnAddRootSet.Visible = true;
                break;
        }
    }
    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvSetNameDRC":
                GvSetNameDRC.PageIndex = e.NewPageIndex;
                GvSetNameDRC.DataBind();
                SelectSetnane();
                break;

            case "GvRootSetName":
                GvRootSetName.PageIndex = e.NewPageIndex;
                GvRootSetName.DataBind();
                SelectRootSetnane();
                break;
        }
    }
    #endregion

    #region setActiveView

    protected void setActiveView(string activeTab, int uidx)
    {
        MvMaster_lab.SetActiveView((View)MvMaster_lab.FindControl(activeTab));
        FvInsertSetname.Visible = false;
        //fvform_Insert_Root.Visible = false;

        switch (activeTab)
        {
            case "view_Genaral":

                break;

            case "view_CreateRoot":


               // getM1Set_labName();
               // Select_labM1();
                break;
        }
    }

    #endregion
}