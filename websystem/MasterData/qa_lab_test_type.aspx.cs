using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_qa_lab_test_type : System.Web.UI.Page
{

 #region Connect
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();
    data_qa _data_qa = new data_qa();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];

    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    //-- employee --//


  
    static string _urlQaSettype = _serviceUrl + ConfigurationManager.AppSettings["urlQaSettype"];
    static string _urlQagettype = _serviceUrl + ConfigurationManager.AppSettings["urlQagettype"];
    static string _urlQaDeletetype = _serviceUrl + ConfigurationManager.AppSettings["urlQaDeletetype"];

    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    bool _flag_qmr = false;
    #endregion

    #region Page_Init
    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
        //check permission
        //foreach (int item in rdept_qmr)
        //{
        //    if (_dataEmployee.employee_list[0].rdept_idx == item)
        //    {
        //        _flag_qmr = true;
        //        break;
        //    }
        //}
    }
    #endregion

    #region getEmployeeProfile
    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }
    #endregion

    #region Insert&Select&Update
    protected void Insert_Test_type()
    {
        FormView fvDocDetail_insert_qa = (FormView)view1_test_type.FindControl("Fv_Insert_Type");
        TextBox tex_name_typeTH = (TextBox)fvDocDetail_insert_qa.FindControl("txttype_nameTH");
        TextBox tex_name_typeEN = (TextBox)fvDocDetail_insert_qa.FindControl("txttype_nameEN");
        TextBox tex_symbol_type = (TextBox)fvDocDetail_insert_qa.FindControl("txt_symbol");
        DropDownList dropD_status_type = (DropDownList)fvDocDetail_insert_qa.FindControl("ddTypeadd");

        data_qa TType_bIN = new data_qa();
        qa_m0_test_type TType_sIN = new qa_m0_test_type();
        TType_bIN.qa_m0_test_type_list = new qa_m0_test_type[1];

        TType_sIN.test_type_name_th = tex_name_typeTH.Text;
        TType_sIN.test_type_name_en = tex_name_typeEN.Text;
        TType_sIN.test_type_symbol = tex_symbol_type.Text;
        TType_sIN.test_type_status = int.Parse(dropD_status_type.SelectedValue);
        TType_sIN.cemp_idx = _emp_idx;
        TType_bIN.qa_m0_test_type_list[0] = TType_sIN;

        //test_type.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(TType_bIN));
        TType_bIN = callServicePostMasterQA(_urlQaSettype, TType_bIN);
        if (TType_bIN.return_code == 101)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);

        }
        else
        {
            tex_name_typeTH.Text = String.Empty;
            tex_name_typeEN.Text = String.Empty;
            tex_symbol_type.Text = String.Empty;

        }
    }
    protected void Select_Test_type()
    {
        data_qa TType_bSE = new data_qa();
        qa_m0_test_type TType_sSE = new qa_m0_test_type();

        TType_bSE.qa_m0_test_type_list = new qa_m0_test_type[1];
        TType_bSE.qa_m0_test_type_list[0] = TType_sSE;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(TType_bSE));
        TType_bSE = callServicePostMasterQA(_urlQagettype, TType_bSE);
        setGridData(Gv_select_Type, TType_bSE.qa_m0_test_type_list);
        Gv_select_Type.DataSource = TType_bSE.qa_m0_test_type_list;
        Gv_select_Type.DataBind();
    }
    #endregion


    #region page load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {    
            MvMaster_test_type.SetActiveView(view1_test_type);
            Select_Test_type();
        }

      
    }
    #endregion

    #region setformdata
    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    #endregion

    #region setGridData
    private void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region callService
    protected data_qa callServicePostMasterQA(string _cmdUrl, data_qa _data_qa)
    {
        _localJson = _funcTool.convertObjectToJson(_data_qa);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_qa = (data_qa)_funcTool.convertJsonToObject(typeof(data_qa), _localJson);
        return _data_qa;

    }
    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    //protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    //{
    //    // convert to json
    //    _localJson = _funcTool.convertObjectToJson(_dataEmployee);
    //    //litDebug.Text = _localJson;

    //    // call services
    //    _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

    //    // convert json to object
    //    _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

    //    return _dataEmployee;
    //}
    #endregion

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();
        switch (cmdName)
        {
            case "cmdAddtype":
                Gv_select_Type.EditIndex = -1;
                Select_Test_type();
                btn_add_type.Visible = false;
                setFormData(Fv_Insert_Type, FormViewMode.Insert, null);
                Fv_Insert_Type.Visible = true;
                

                break;
            case "Lbtn_cancel_type":
                btn_add_type.Visible = true;
                //setFormData(Fv_Insert_Type, FormViewMode.ReadOnly, null);
                Fv_Insert_Type.Visible = false;
                SETFOCUS.Focus();
                break;
            case "Lbtn_submit_type":
                Insert_Test_type();
              //setFormData(Fv_Insert_Type, FormViewMode.ReadOnly, null);
                Fv_Insert_Type.Visible = false;
                btn_add_type.Visible = true;
                Select_Test_type();
                SETFOCUS.Focus();
                break;
            case "btnTodelete_type":

                int a = int.Parse(cmdArg);
                data_qa TType_bDE = new data_qa();
                qa_m0_test_type TType_sDE = new qa_m0_test_type();

                TType_bDE.qa_m0_test_type_list = new qa_m0_test_type[1];

                TType_sDE.test_type_idx = int.Parse(a.ToString());

                TType_bDE.qa_m0_test_type_list[0] = TType_sDE;
                //test_type.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(TType_bDE));
                TType_bDE = callServicePostMasterQA(_urlQaDeletetype, TType_bDE);

                
                if (TType_bDE.return_code == 0)
                {

                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินการสำเร็จ');", true);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินการไม่สำเร็จ');", true);
                }
                Fv_Insert_Type.Visible = false;
                btn_add_type.Visible = true;
                Select_Test_type();
                break;
        }
    }
    #endregion

    #region Masterbound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "Gv_select_Type":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbtype_status = (Label)e.Row.Cells[3].FindControl("lbtype_status");
                    Label type_statusOpen = (Label)e.Row.Cells[3].FindControl("type_statusOpen");
                    Label type_statusClose = (Label)e.Row.Cells[3].FindControl("type_statusClose");

                    ViewState["_type_status"] = lbtype_status.Text;


                    if (ViewState["_type_status"].ToString() == "1")
                    {
                        type_statusOpen.Visible = true;
                    }
                    else if (ViewState["_type_status"].ToString() == "0")
                    {
                        type_statusClose.Visible = true;
                    }

                }


                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    btn_add_type.Visible = true;
                    setFormData(Fv_Insert_Type, FormViewMode.ReadOnly, null);
                }



                break;


        }
    }

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "Gv_select_Type":
                Gv_select_Type.EditIndex = e.NewEditIndex;
                Select_Test_type();
                break;

        }
    }

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "Gv_select_Type":
                var id_type = (TextBox)Gv_select_Type.Rows[e.RowIndex].FindControl("ID_type");
                var name_th_type = (TextBox)Gv_select_Type.Rows[e.RowIndex].FindControl("Name_typeTH");
                var name_en_type = (TextBox)Gv_select_Type.Rows[e.RowIndex].FindControl("Name_typeEN");
                var symbol_type = (TextBox)Gv_select_Type.Rows[e.RowIndex].FindControl("symbol_type");
                var status_type = (DropDownList)Gv_select_Type.Rows[e.RowIndex].FindControl("ddEdit_type");

                Gv_select_Type.EditIndex = -1;

                data_qa TType_pUP = new data_qa();

                qa_m0_test_type TType_sUP = new qa_m0_test_type();

                TType_pUP.qa_m0_test_type_list = new qa_m0_test_type[1];

                TType_sUP.test_type_idx = int.Parse(id_type.Text);
                TType_sUP.test_type_status = int.Parse(status_type.SelectedValue);
                TType_sUP.test_type_name_th = name_th_type.Text;
                TType_sUP.test_type_name_en = name_en_type.Text;
                TType_sUP.test_type_symbol = symbol_type.Text;

                TType_pUP.qa_m0_test_type_list[0] = TType_sUP;
              //test_type.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(TType_pUP));
               TType_pUP = callServicePostMasterQA(_urlQaSettype, TType_pUP);
                Select_Test_type();

                if (TType_pUP.return_code == 0)
                {

                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินการสำเร็จ');", true);

                }

                else

                {


                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินการไม่สำเร็จ');", true);


                }

                break;

        }
    }
    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "Gv_select_Type":
                Gv_select_Type.EditIndex = -1;
                Select_Test_type();
                btn_add_type.Visible = true;
                SETFOCUS.Focus();
                break;

        }
    }

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "Gv_select_Type":
                Gv_select_Type.PageIndex = e.NewPageIndex;
                Gv_select_Type.DataBind();
                Select_Test_type();
                break;

        }
    }
    #endregion

}