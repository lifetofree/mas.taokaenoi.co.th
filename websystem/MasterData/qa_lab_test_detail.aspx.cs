﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class websystem_MasterData_qa_lab_test_detail : System.Web.UI.Page
{

    #region initial function/data
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();
    data_qa _data_qa = new data_qa();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];

    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    //-- employee --//

    //-- master data setname --//
    static string _urlQagettype = _serviceUrl + ConfigurationManager.AppSettings["urlQagettype"];
    static string _urlQaGetTestDetail = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetTestDetail"];
    static string _urlQaSetM0TestDetail = _serviceUrl + ConfigurationManager.AppSettings["urlQaSetM0TestDetail"];
    static string _urlQaDelM0TestDetail = _serviceUrl + ConfigurationManager.AppSettings["urlQaDelM0TestDetail"];
    

    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    bool _flag_qmr = false;

    // rdept permission
    // int[] rdept_qmr = { 20 }; //QMR:26
    #endregion initial function/data


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            initPage();
        }

    }
    #region event command
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {
            case "cmdAddTestDetail":
                switch (cmdArg)
                {
                    case "0":
                        gvMaster.EditIndex = -1;
                        selectM0TestDeatial();
                        fvformInsert.Visible = true;
                        setFormData(fvformInsert, FormViewMode.Insert, null);
                        getM0LabTypeList((DropDownList)fvformInsert.FindControl("ddlLabType"), "0");

                        break;

                }

                break;

            case "cmdSave":
                switch (cmdArg)

                {
                    case "0":

                        TextBox txtTestDetailName = (TextBox)fvformInsert.FindControl("txtTestDetailName");
                        TextBox txtActionTime = (TextBox)fvformInsert.FindControl("txtActionTime");
                        DropDownList ddlTestDetailStatus = (DropDownList)fvformInsert.FindControl("ddlTestDetailStatus");
                        DropDownList ddlLabType = (DropDownList)fvformInsert.FindControl("ddlLabType");

                        _data_qa.qa_m0_test_detail_list = new qa_m0_test_detail[1];
                        qa_m0_test_detail _m0test = new qa_m0_test_detail();
                        _m0test.cemp_idx = _emp_idx;
                        _m0test.test_detail_name = txtTestDetailName.Text;
                        _m0test.test_detail_status = int.Parse(ddlTestDetailStatus.SelectedValue);
                        _m0test.test_type_idx = int.Parse(ddlLabType.SelectedValue);
                        _m0test.test_detail_time = int.Parse(txtActionTime.Text);

                        _data_qa.qa_m0_test_detail_list[0] = _m0test;

                        _data_qa = callServicePostMasterQA(_urlQaSetM0TestDetail, _data_qa);

                        
                        if (_data_qa.return_code == 101)
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);

                        }
                        else
                        {
                            txtTestDetailName.Text = String.Empty;
                            //tex_code_place.Text = String.Empty;
                        }

                        //setFormData(fvformInsert, FormViewMode.ReadOnly, null);
                        setActiveView("pageGenaral", 0);
                        selectM0TestDeatial();
                       // gvMaster.DataBind();
                        break;


                }

                break;

            case "cmdCancel":
                switch (cmdArg)
                {
                    case "0":
                        setActiveView("pageGenaral", 0);
                        setFormData(fvformInsert, FormViewMode.Insert, null);
                        selectM0TestDeatial();
                        SETFOCUS.Focus();
                        break;
                }
          
                break;

            case "cmdDelete":
                string[] cmdArgDelete = cmdArg.Split(',');
                int test_idx = int.TryParse(cmdArgDelete[0].ToString(), out _default_int) ? int.Parse(cmdArgDelete[0].ToString()) : _default_int;
                int condition = int.TryParse(cmdArgDelete[1].ToString(), out _default_int) ? int.Parse(cmdArgDelete[1].ToString()) : _default_int;

                _data_qa.qa_m0_test_detail_list = new qa_m0_test_detail[1];
                qa_m0_test_detail _deltest = new qa_m0_test_detail();
                _deltest.test_detail_idx = test_idx;
                _data_qa.qa_m0_test_detail_list[0] = _deltest;
                _data_qa = callServicePostMasterQA(_urlQaDelM0TestDetail, _data_qa);
                if (_data_qa.return_code == 0)
                {
                  //  litDebug.Text = "success";
                }
                else
                {
                  //  litDebug.Text = "not success";
                }
                switch (condition)
                {
                    // select test detail level 1 (DELETE)
                    case 0:
                        selectM0TestDeatial();
                        break;

                }

                break;

        }

    }

    #endregion

    #region dropdown list
    protected void getM0LabTypeList(DropDownList ddlName, string _test_type_idx)
    {
        data_qa _dataqa = new data_qa();
        _dataqa.qa_m0_test_type_list = new qa_m0_test_type[1];
        qa_m0_test_type _m0test = new qa_m0_test_type();
        //_m0lab.condition = 1;
        _dataqa.qa_m0_test_type_list[0] = _m0test;

        _dataqa = callServicePostMasterQA(_urlQagettype, _dataqa);
        setDdlData(ddlName, _dataqa.qa_m0_test_type_list, "test_type_name_th", "test_type_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกประเภทการปฏิบัติการ ---", "0"));
        ddlName.SelectedValue = _test_type_idx;
    }
    protected void getM0PlaceList(DropDownList ddlName, string _m0_place_idx)
    {
      
        _data_qa.qa_m0_place_list = new qa_m0_place_detail[1];
        qa_m0_place_detail _m0place = new qa_m0_place_detail();
        _m0place.condition = 1;
        _data_qa.qa_m0_place_list[0] = _m0place;

       // _data_qa = callServicePostMasterQA(_urlQaGetplace, _data_qa);
        setDdlData(ddlName, _data_qa.qa_m0_place_list, "place_name", "place_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกสถานที่ตรวจ ---", "0"));
        ddlName.SelectedValue = _m0_place_idx;
    }

    #endregion

    #region select 
    protected void selectM0TestDeatial()
    {
        _data_qa.qa_m0_test_detail_list = new qa_m0_test_detail[1];
        qa_m0_test_detail _selecttest = new qa_m0_test_detail();
        _selecttest.condition = 3;
        _data_qa.qa_m0_test_detail_list[0] = _selecttest;
        _data_qa = callServicePostMasterQA(_urlQaGetTestDetail, _data_qa);

        setGridData(gvMaster, _data_qa.qa_m0_test_detail_list);

    }

    #endregion

    #region reuse
    protected void initPage()
    {

        //clearSession();
        //clearViewState();

        setActiveView("pageGenaral", 0);
      //  setFormData(fvformInsert, FormViewMode.ReadOnly, null);
        selectM0TestDeatial();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }



    #region grid view
    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var gvName = (GridView)sender;

        switch (gvName.ID)
        {
            case "gvMaster":
                gvMaster.PageIndex = e.NewPageIndex;
                gvMaster.DataBind();
                selectM0TestDeatial();

                break;

        }
    }
    protected void gvRowEditing(object sender, GridViewEditEventArgs e)
    {
        var gvName = (GridView)sender;

        switch (gvName.ID)
        {
            case "gvMaster":
                gvMaster.EditIndex = e.NewEditIndex;
                selectM0TestDeatial();
                break;

          
        }
    }
    protected void gvRowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var gvName = (GridView)sender;

        switch (gvName.ID)
        {
            case "gvMaster":

                var _updateTestDetailIDX = (TextBox)gvMaster.Rows[e.RowIndex].FindControl("_updateTestDetailIDX");
                var _update_testDetail= (TextBox)gvMaster.Rows[e.RowIndex].FindControl("tbupdateTestDeatil");
                var _update_test_time = (TextBox)gvMaster.Rows[e.RowIndex].FindControl("txtupdateActionTime");
                var _updateTestDeatil = (DropDownList)gvMaster.Rows[e.RowIndex].FindControl("ddlupdateTestDeatil");
                var _updateStatus = (DropDownList)gvMaster.Rows[e.RowIndex].FindControl("ddlupdate_test_status");

                gvMaster.EditIndex = -1;

                _data_qa.qa_m0_test_detail_list = new qa_m0_test_detail[1];
                qa_m0_test_detail _uptest = new qa_m0_test_detail();
                _uptest.test_detail_idx = int.Parse(_updateTestDetailIDX.Text);
                _uptest.test_detail_name = _update_testDetail.Text;
                _uptest.test_detail_time = int.Parse(_update_test_time.Text);
                _uptest.test_type_idx = int.Parse(_updateTestDeatil.SelectedValue);
                _uptest.test_detail_status = int.Parse(_updateStatus.SelectedValue);
                _data_qa.qa_m0_test_detail_list[0] = _uptest;

                _data_qa = callServicePostMasterQA(_urlQaSetM0TestDetail, _data_qa);
                //if (_data_qa.return_code == 102)
                //{
                //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถแก้ไขข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);
                //}

                //else
                //{

                //}

                selectM0TestDeatial();
                SETFOCUS.Focus();
                break;

        }
    }
    protected void gvRowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var gvName = (GridView)sender;

        switch (gvName.ID)
        {
            case "gvMaster":

                gvMaster.EditIndex = -1;
                selectM0TestDeatial();
                SETFOCUS.Focus();
                break;


        }
    }
    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var gvName = (GridView)sender;
        switch (gvName.ID)
        {
         
            case "gvMaster":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label _status = (Label)e.Row.Cells[4].FindControl("lbstatus");
                    Label status_online = (Label)e.Row.Cells[4].FindControl("status_online");
                    Label status_offline = (Label)e.Row.Cells[4].FindControl("status_offline");

                    ViewState["status"] = _status.Text;

                    if (ViewState["status"].ToString() == "0")
                    {
                        status_offline.Visible = true;
                    }
                    else if (ViewState["status"].ToString() == "1")
                    {
                        status_online.Visible = true;
                    }
                }

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    TextBox txttestTypeIDX = (TextBox)e.Row.Cells[0].FindControl("txttestTypeIDX");
                    getM0LabTypeList((DropDownList)e.Row.Cells[0].FindControl("ddlupdateTestDeatil"), txttestTypeIDX.Text);
                    fvformInsert.Visible = false;
                    //setFormData(fvformInsert, FormViewMode.ReadOnly, null);

                }

                break;
        }

    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion
    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void setActiveView(string activeTab, int uidx)
    {

        mvMaster.SetActiveView((View)mvMaster.FindControl(activeTab));

        setFormData(fvformInsert, FormViewMode.Insert, null);
        fvformInsert.Visible = false;

        switch (activeTab)
        {
            
            case "pageGenaral":
                //setFormData(fvformInsert, FormViewMode.Insert, null);

                break;
        }

    }



    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }

    protected data_qa callServicePostMasterQA(string _cmdUrl, data_qa _data_qa)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_qa);
        // litDebug.Text = _localJson;

        // call services
        // _localJson = _funcTool.callServicePost(_cmdUrl + _localJson);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_qa = (data_qa)_funcTool.convertJsonToObject(typeof(data_qa), _localJson);

        return _data_qa;
    }
    #endregion

}