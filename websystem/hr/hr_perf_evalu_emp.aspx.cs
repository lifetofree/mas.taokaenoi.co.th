﻿using DotNet.Highcharts;
using DotNet.Highcharts.Helpers;
using DotNet.Highcharts.Options;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_hr_perf_evalu_emp : System.Web.UI.Page
{
    //2018-12-28 08:52:39.950
    #region Connect

    service_execute serviceexcute = new service_execute();
    service_mail servicemail = new service_mail();
    function_tool _funcTool = new function_tool();
    data_employee _dtEmployee = new data_employee();
    data_hr_evaluation _data_hr_evaluation = new data_hr_evaluation();
    function_dmu _func_dmu = new function_dmu();

    string _localJson = String.Empty;
    int _tempInt = 0;
    string _mail_subject = "";
    string _mail_body = "";
    string _link = "";
    int emp_idx = 0;
    int _u0idx = 0;

    // set object
    FormView _FormView;

    GridView
        _GridView
        ;

    TextBox _txtremark,
        _txt,
         _txt_hidden_u0idx
        ;

    DropDownList
        _ddl

        ;
    Panel
        _Panel_body_approve,
        _Panel_approve,
        _Panel_ImportAdd
        ;
    Label
        _lb_title_approve,
        _ddlu0_status
        ;
    Repeater
        _Repeater
        ;
    LinkButton
        _lbtn,
        _btnsave,
        _btncancel,
        _btncalculator,
        _btnConfirm,
        _btnExportInput
        ;
    CheckBox
        _CheckBox
        ;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];

    // start it asset 
    // static string _urlGetits_lookup = _serviceUrl + ConfigurationManager.AppSettings["urlGetits_lookup"];
    //its_u_hr_evaluation
    static string _urlGetits_u_hr_evaluation = _serviceUrl + ConfigurationManager.AppSettings["urlGetits_u_hr_evaluation"];
    static string _urlSetInsits_u_hr_evaluation = _serviceUrl + ConfigurationManager.AppSettings["urlSetInsits_u_hr_evaluation"];
    static string _urlSetUpdits_u_hr_evaluation = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdits_u_hr_evaluation"];
    static string _urlDelits_u_hr_evaluation = _serviceUrl + ConfigurationManager.AppSettings["urlDelits_u_hr_evaluation"];

    //static string _urlGetits_masterit = _serviceUrl + ConfigurationManager.AppSettings["urlGetits_masterit"];
    //static string _urlsendEmailits_u_hr_evaluation = _serviceUrl + ConfigurationManager.AppSettings["urlsendEmailits_u_hr_evaluation"];

    static string _urlsendEmailhr_evaluation = _serviceUrl + ConfigurationManager.AppSettings["urlsendEmailhr_evaluation"];

    // end it asset

    static string _urlGetddlDeptSW = _serviceUrl + ConfigurationManager.AppSettings["urlGetddlDeptSW"];
    static string _urlGetddlOrganizationSW = _serviceUrl + ConfigurationManager.AppSettings["urlGetddlOrganizationSW"];
    static string _urlGetlocation = _serviceUrl + ConfigurationManager.AppSettings["urlGetlocation"];

    #endregion Connect

    private void Page_Init(object sender, EventArgs e)
    {
        ClearApplicationCache();
    }
    public void ClearApplicationCache()
    {
        List<string> keys = new List<string>();
        // retrieve application Cache enumerator
        IDictionaryEnumerator enumerator = Cache.GetEnumerator();
        // copy all keys that currently exist in Cache
        while (enumerator.MoveNext())
        {
            keys.Add(enumerator.Key.ToString());
        }

        // delete every key from cache
        for (int i = 0; i < keys.Count; i++)
        {
            Cache.Remove(keys[i]);
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        emp_idx = int.Parse(Session["emp_idx"].ToString());
        if (!Page.IsPostBack)
        {
            ViewState["EmpIDX"] = int.Parse(Session["emp_idx"].ToString());

            ViewState["mode"] = "p";
            select_empIdx_present();
            setActiveTab("p");
            if (_func_dmu.zStringToInt(ViewState["status_u0idx"].ToString()) == 0)
            {
                ShowDataIndex();
            }
            else
            {
                _u0idx = _func_dmu.zStringToInt(ViewState["status_u0idx"].ToString());
                setActiveTab("insert");
                ViewState["mode"] = "update";
                MvMaster.SetActiveView(ViewMain);

                FvInsert.ChangeMode(FormViewMode.Insert);
                FvInsert.DataBind();
                FvInsertH.ChangeMode(FormViewMode.Edit);
                FvInsertH.DataBind();
                VMain_Showdata(_u0idx);
            }
            SETFOCUS.Focus();
        }
        setBtnTrigger();
    }

    private void setBtnTrigger()
    {
        linkBtnTrigger(_divMenuBtnToIndex);
        linkBtnTrigger(_divMenuBtnToInsert);
        linkBtnTrigger(_divMenuBtnToGarp);
        linkBtnTrigger(btnSearchGraph);
        linkBtnTrigger(btnexport_report);
        GridViewTrigger(GvIndexlist);
        try
        {
            LinkButton btnExportInput = (LinkButton)FvInsertH.FindControl("btnExportInput");
            LinkButton btncalculator = (LinkButton)FvInsertH.FindControl("btncalculator");
            LinkButton btnSave = (LinkButton)FvInsertH.FindControl("btnSave");
            LinkButton btnConfirm = (LinkButton)FvInsertH.FindControl("btnConfirm");
            linkBtnTrigger(btnExportInput);
            linkBtnTrigger(btncalculator);
            linkBtnTrigger(btnSave);
            linkBtnTrigger(btnConfirm);
        }
        catch { }
    }

    protected void GridViewTrigger(GridView gridview)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = gridview.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var gridViewName = (GridView)sender;

        if (gridViewName.ID == "GvIndexlist")
        {
            gridViewName.PageIndex = e.NewPageIndex;
            gridViewName.DataBind();
            _func_dmu.zSetGridData(gridViewName, ViewState["V_GvIndexlist"]);
            SETFOCUS.Focus();
        }
    }

    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var gridViewName = (GridView)sender;

        if (gridViewName.ID == "gvItemsu1PerfEvaluEmpDaily")
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lb_u0_emp_idx = (Label)e.Row.FindControl("lb_u0_emp_idx");
                TextBox txt_ownership_qty = (TextBox)e.Row.FindControl("txt_ownership_qty");
                TextBox txt_improvement_continuously_qty = (TextBox)e.Row.FindControl("txt_improvement_continuously_qty");
                TextBox txt_commitment_qty = (TextBox)e.Row.FindControl("txt_commitment_qty");
                Label txt_warning_notice_qty = (Label)e.Row.FindControl("txt_warning_notice_qty");
                Label txt_work_break_qty = (Label)e.Row.FindControl("txt_work_break_qty");
                TextBox txt_labor_cost = (TextBox)e.Row.FindControl("txt_labor_cost");
                TextBox txt_labor_cost_new = (TextBox)e.Row.FindControl("txt_labor_cost_new");
                TextBox txt_bonus_day = (TextBox)e.Row.FindControl("txt_labor_cost_new");
                Label lb_description = (Label)e.Row.FindControl("lb_description");
                Label lb_u0_status = (Label)e.Row.FindControl("lb_u0_status");

                Boolean _BMode = false;
                if (
                    (
                    (_func_dmu.zStringToInt(lb_u0_emp_idx.Text) == emp_idx)
                    ||
                    (ViewState["mode"].ToString() == "insert")
                    )
                    &&
                    (_func_dmu.zStringToInt(lb_u0_status.Text) != 2)
                    )
                {
                    _BMode = true;
                }
                else
                {
                    _BMode = false;
                }
                txt_ownership_qty.Enabled = _BMode;
                txt_improvement_continuously_qty.Enabled = _BMode;
                txt_commitment_qty.Enabled = _BMode;
                // txt_warning_notice_qty.Enabled = _BMode;
                // txt_work_break_qty.Enabled = _BMode;
                txt_labor_cost.Enabled = _BMode;
                txt_labor_cost_new.Enabled = _BMode;
                txt_bonus_day.Enabled = _BMode;


            }
        }
        else if (gridViewName.ID == "GvIndexlist")
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton btnDelete = (LinkButton)e.Row.FindControl("btnDelete");
                LinkButton btnunconfirm = (LinkButton)e.Row.FindControl("btnunconfirm");
                Label lb_u0_emp_idx = (Label)e.Row.FindControl("lb_u0_emp_idx");
                Label lb_u0_status = (Label)e.Row.FindControl("lb_u0_status");
                if (
                    (_func_dmu.zStringToInt(lb_u0_emp_idx.Text) == emp_idx)
                    &&
                    (_func_dmu.zStringToInt(lb_u0_status.Text) == 1)
                    )
                {
                    btnDelete.Visible = true;
                }
                else
                {
                    btnDelete.Visible = false;
                }

                if ((ViewState["admin_idx"].ToString() == "1") && (lb_u0_status.Text == "2"))
                {
                    btnunconfirm.Visible = true;
                }
                else
                {
                    btnunconfirm.Visible = false;
                }

            }
        }
    }

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {
            case "_divMenuBtnToIndex":
                setActiveTab("p");
                ViewState["mode"] = "p";
                MvMaster.SetActiveView(ViewIndex);
                ShowDataIndex();
                break;
            case "_divMenuLiToInsert":
                setActiveTab("insert");
                ViewState["mode"] = "insert";
                MvMaster.SetActiveView(ViewMain);
                FvInsert.ChangeMode(FormViewMode.Insert);
                FvInsert.DataBind();
                FvInsertH.ChangeMode(FormViewMode.Insert);
                FvInsertH.DataBind();
                VMain_Showdata(0);
                break;
            case "btncalculator":
                if (setCheckError(2) == false)
                {
                    // _u0idx = _func_dmu.zStringToInt(cmdArg);
                    // VMain_Showdata(_u0idx);
                    setCalculator();
                }
                break;
            case "btnSave":
                if (setCheckError(2) == false)
                {
                    setCalculator();
                    setObject_Main();
                    _u0idx = _func_dmu.zStringToInt(_txt_hidden_u0idx.Text);
                    _u0idx = zSave(_u0idx, 1);
                    // Page.Response.Redirect(Page.Request.Url.ToString(), true);
                    if (_u0idx > 0)
                    {
                        setActiveTab("insert");
                        ViewState["mode"] = "update";
                        MvMaster.SetActiveView(ViewMain);
                        FvInsert.ChangeMode(FormViewMode.Insert);
                        FvInsert.DataBind();
                        FvInsertH.ChangeMode(FormViewMode.Edit);
                        FvInsertH.DataBind();
                        VMain_Showdata(_u0idx);
                    }
                }
                break;
            case "btnCancel":
                // Page.Response.Redirect(Page.Request.Url.ToString(), true);
                setActiveTab("p");
                ViewState["mode"] = "p";
                MvMaster.SetActiveView(ViewIndex);
                ShowDataIndex();
                SETFOCUS.Focus();
                break;
            case "btnmanage_its":
                _u0idx = _func_dmu.zStringToInt(cmdArg);
                setActiveTab("insert");
                ViewState["mode"] = "update";
                MvMaster.SetActiveView(ViewMain);
                FvInsert.ChangeMode(FormViewMode.Insert);
                FvInsert.DataBind();
                FvInsertH.ChangeMode(FormViewMode.Edit);
                FvInsertH.DataBind();
                VMain_Showdata(_u0idx);
                break;
            case "btnDelete":
                _u0idx = _func_dmu.zStringToInt(cmdArg);
                zDelete(_u0idx);
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btnexport":
                _u0idx = _func_dmu.zStringToInt(cmdArg);
                zExportExcel(_u0idx);
                break;
            case "_divMenuBtnToGarp":
                _u0idx = _func_dmu.zStringToInt(cmdArg);
                setActiveTab("report");
                ViewState["mode"] = "report";
                MvMaster.SetActiveView(View_report);
                Update_PanelGraph.Visible = false;
                Update_Panelreport.Visible = false;
                CreateDsits_u1_PerfEvaluEmpDaily();
                select_hr_year(ddlyear);
                ddlTypeReport.SelectedIndex = 0;
                break;
            case "cmdSearchReportGraph":
                if (_func_dmu.zStringToInt(ddlTypeReport.SelectedValue) == 1)
                {
                    ReportGraph();
                }
                else
                {
                    getCountBookingReportGraph();
                }
                break;
            case "btnConfirm":
                if (setCheckError(1) == false)
                {
                    setCalculator();
                    setObject_Main();
                    _u0idx = _func_dmu.zStringToInt(_txt_hidden_u0idx.Text);
                    zSave(_u0idx, 2);
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);

                }
                break;
            case "btnexport_report":
                zExportExcel_report();
                break;
            case "btnunconfirm":
                _u0idx = _func_dmu.zStringToInt(cmdArg);
                zUnconfirm(_u0idx);
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btnImport":
                _u0idx = _func_dmu.zStringToInt(cmdArg);
                VMain_Showdata(_u0idx);
                ImportData();
                break;
            case "btnExportInput":
                _u0idx = _func_dmu.zStringToInt(cmdArg);
                VMain_Showdata(_u0idx);
                ExportGridToExcel(GvExportExcel_2, "template_แบบประเมินผลการปฏิบัติงานระดับพนักงานรายวัน", "template_แบบประเมินผลการปฏิบัติงานระดับพนักงานรายวัน");
                break;
        }
    }

    #region setActiveTab
    protected void setActiveTab(string activeTab)
    {
        _divMenuLiToIndex.Attributes.Remove("class");
        _divMenuLiToDivInsert.Attributes.Remove("class");
        _divMenuLiToDivGarp.Attributes.Remove("class");

        if (activeTab == "p")
        {
            _divMenuLiToIndex.Attributes.Add("class", "active");
        }
        else if (activeTab == "insert")
        {
            _divMenuLiToDivInsert.Attributes.Add("class", "active");
        }
        else if (activeTab == "report")
        {
            _divMenuLiToDivGarp.Attributes.Add("class", "active");
        }

    }
    #endregion setActiveTab

    #region Select
    protected void select_empIdx_present()
    {

        _dtEmployee = callServiceEmpProfile(_urlGetMyProfile + ViewState["EmpIDX"].ToString());

        if (_dtEmployee.employee_list != null)
        {
            ViewState["rdept_name"] = _dtEmployee.employee_list[0].dept_name_th;
            ViewState["rdept_idx"] = _dtEmployee.employee_list[0].rdept_idx;
            ViewState["FullName"] = _dtEmployee.employee_list[0].emp_name_th;
            ViewState["Org_name"] = _dtEmployee.employee_list[0].org_name_th;
            ViewState["Org_idx"] = _dtEmployee.employee_list[0].org_idx;

            ViewState["EmpCode"] = _dtEmployee.employee_list[0].emp_code;
            ViewState["Positname"] = _dtEmployee.employee_list[0].pos_name_th;
            ViewState["Pos_idx"] = _dtEmployee.employee_list[0].rpos_idx;
            ViewState["Email"] = _dtEmployee.employee_list[0].emp_email;
            ViewState["Tel"] = _dtEmployee.employee_list[0].emp_mobile_no;
            ViewState["Secname"] = _dtEmployee.employee_list[0].sec_name_th;
            ViewState["Sec_idx"] = _dtEmployee.employee_list[0].rsec_idx;
            ViewState["CostIDX"] = _dtEmployee.employee_list[0].costcenter_idx;

            ViewState["rsec_idx"] = _dtEmployee.employee_list[0].rsec_idx;
            ViewState["rdept_idx"] = _dtEmployee.employee_list[0].rdept_idx;
            ViewState["rpos_idx"] = _dtEmployee.employee_list[0].rpos_idx;
            ViewState["org_idx"] = _dtEmployee.employee_list[0].org_idx;
            ViewState["jobgrade_level"] = _dtEmployee.employee_list[0].jobgrade_level;
            ViewState["locidx"] = _dtEmployee.employee_list[0].LocIDX;

            if (emp_idx == 33455)
            {
                ViewState["Pos_idx"] = 5900;
                ViewState["Sec_idx"] = 431;

                ViewState["rsec_idx"] = 431;
                ViewState["rdept_idx"] = 13;
                ViewState["rpos_idx"] = 5900;
                ViewState["jobgrade_level"] = 9;
            }

        }
        else
        {
            ViewState["rdept_name"] = "";
            ViewState["rdept_idx"] = "";
            ViewState["FullName"] = "";
            ViewState["Org_name"] = "";
            ViewState["Org_idx"] = "";

            ViewState["EmpCode"] = "";
            ViewState["Positname"] = "";
            ViewState["Pos_idx"] = "";
            ViewState["Email"] = "";
            ViewState["Tel"] = "";
            ViewState["Secname"] = "";
            ViewState["Sec_idx"] = "";
            ViewState["CostIDX"] = "";

            ViewState["rsec_idx"] = "";
            ViewState["rdept_idx"] = "";
            ViewState["rpos_idx"] = "";
            ViewState["org_idx"] = "";
            ViewState["jobgrade_level"] = "";
            ViewState["locidx"] = "";
        }

        ViewState["emp_idx"] = ViewState["EmpIDX"].ToString();


        _data_hr_evaluation.hr_perf_evalu_emp_action = new hr_perf_evalu_emp[1];
        hr_perf_evalu_emp select_obj = new hr_perf_evalu_emp();
        select_obj.emp_idx = _func_dmu.zStringToInt(ViewState["emp_idx"].ToString());
        select_obj.rpos_idx = _func_dmu.zStringToInt(ViewState["rpos_idx"].ToString());
        select_obj.rdept_idx = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());
        select_obj.operation_status_id = "hr_setpermission_admin_dept_m0";
        _data_hr_evaluation.hr_perf_evalu_emp_action[0] = select_obj;
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_hr_evaluation));
        _data_hr_evaluation = callServicePostPerfEvaluEmp(_urlGetits_u_hr_evaluation, _data_hr_evaluation);
        if (_data_hr_evaluation.hr_perf_evalu_emp_action != null)
        {
            foreach (var item in _data_hr_evaluation.hr_perf_evalu_emp_action)
            {
                ViewState["admin_idx"] = item.admin_idx;
            }
        }
        else
        {
            ViewState["admin_idx"] = 0;
        }



        _data_hr_evaluation.hr_perf_evalu_emp_action = new hr_perf_evalu_emp[1];
        select_obj = new hr_perf_evalu_emp();
        select_obj.emp_idx = _func_dmu.zStringToInt(ViewState["emp_idx"].ToString());
        select_obj.operation_status_id = "list_dataconfirm";
        _data_hr_evaluation.hr_perf_evalu_emp_action[0] = select_obj;
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_hr_evaluation));
        _data_hr_evaluation = callServicePostPerfEvaluEmp(_urlGetits_u_hr_evaluation, _data_hr_evaluation);
        if (_data_hr_evaluation.hr_perf_evalu_emp_action != null)
        {
            ViewState["status_u0idx"] = _data_hr_evaluation.hr_perf_evalu_emp_action[0].u0idx;
        }
        else
        {
            ViewState["status_u0idx"] = 0;
        }


    }
    #endregion Select

    protected void select_hr_year(DropDownList ddlName)
    {
        ddlName.Items.Clear();
        data_hr_evaluation datahrevaluation = new data_hr_evaluation();
        datahrevaluation.hr_perf_evalu_emp_action = new hr_perf_evalu_emp[1];
        hr_perf_evalu_emp select_obj = new hr_perf_evalu_emp();
        select_obj.operation_status_id = "list_th_year";
        datahrevaluation.hr_perf_evalu_emp_action[0] = select_obj;
        datahrevaluation = callServicePostPerfEvaluEmp(_urlGetits_u_hr_evaluation, datahrevaluation);
        ddlName.DataSource = datahrevaluation.hr_perf_evalu_emp_action;
        ddlName.DataTextField = "zyear";
        ddlName.DataValueField = "zyear";
        ddlName.DataBind();
        ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("ปี.....", "0"));

    }

    protected data_employee callServiceEmpProfile(string _cmdUrl)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }

    protected void ShowDataIndex()
    {
        _data_hr_evaluation.hr_perf_evalu_emp_action = new hr_perf_evalu_emp[1];
        hr_perf_evalu_emp select_obj = new hr_perf_evalu_emp();
        select_obj.emp_idx = emp_idx;
        select_obj.admin_idx = _func_dmu.zStringToInt(ViewState["admin_idx"].ToString());
        //select_obj.rpos_idx = _func_dmu.zStringToInt(ViewState["rpos_idx"].ToString());
        //select_obj.rdept_idx = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());
        select_obj.operation_status_id = "list_index";
        _data_hr_evaluation.hr_perf_evalu_emp_action[0] = select_obj;
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_hr_evaluation));
        _data_hr_evaluation = callServicePostPerfEvaluEmp(_urlGetits_u_hr_evaluation, _data_hr_evaluation);

        ViewState["V_GvIndexlist"] = _data_hr_evaluation.hr_perf_evalu_emp_action;

        _func_dmu.zSetGridData(GvIndexlist, ViewState["V_GvIndexlist"]);
    }

    protected data_hr_evaluation callServicePostPerfEvaluEmp(string _cmdUrl, data_hr_evaluation _dt)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dt);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dt = (data_hr_evaluation)_funcTool.convertJsonToObject(typeof(data_hr_evaluation), _localJson);

        return _dt;
    }

    public string getformatfloat(string Str, int i)
    {
        return _func_dmu.zFormatfloat(Str, i);
    }

    private void setObject_Main()
    {
        _GridView = (GridView)FvInsert.FindControl("gvItemsu1PerfEvaluEmpDaily");
        _txtremark = (TextBox)FvInsertH.FindControl("txtremark");
        _ddlu0_status = (Label)FvInsertH.FindControl("ddlu0_status");
        _txtremark = (TextBox)FvInsertH.FindControl("txtremark");
        _txt_hidden_u0idx = (TextBox)FvInsertH.FindControl("txt_hidden_u0idx");
        _btnsave = (LinkButton)FvInsertH.FindControl("btnSave");
        _btncalculator = (LinkButton)FvInsertH.FindControl("btncalculator");
        _btnExportInput = (LinkButton)FvInsertH.FindControl("btnExportInput");
        _btnConfirm = (LinkButton)FvInsertH.FindControl("btnConfirm");
        _Panel_ImportAdd = (Panel)FvInsertH.FindControl("Panel_ImportAdd");

    }

    private void setUserMain(int _emp_idx)
    {
        _dtEmployee = callServiceEmpProfile(_urlGetMyProfile + _emp_idx.ToString());
        //litDebug.Text = _dtEmployee.employee_list[0].emp_name_th;
        FvDetailUser_Main.DataSource = _dtEmployee.employee_list;
        FvDetailUser_Main.DataBind();
    }

    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        var FvName = (FormView)sender;
        if (FvName.ID == "FvInsertH")
        {
            if (
                       (FvName.CurrentMode == FormViewMode.Insert) ||
                       (FvName.CurrentMode == FormViewMode.Edit)
                       )
            {
                LinkButton btnExportInput = (LinkButton)FvInsertH.FindControl("btnExportInput");
                LinkButton btncalculator = (LinkButton)FvInsertH.FindControl("btncalculator");
                LinkButton btnSave = (LinkButton)FvInsertH.FindControl("btnSave");
                LinkButton btnConfirm = (LinkButton)FvInsertH.FindControl("btnConfirm");
                LinkButton btnCancel = (LinkButton)FvInsertH.FindControl("btnCancel");
                LinkButton btnImport = (LinkButton)FvInsertH.FindControl("btnImport");
                if (btnExportInput != null)
                {
                    linkBtnTrigger(btnExportInput);
                }
                if (btncalculator != null)
                {
                    linkBtnTrigger(btncalculator);
                }
                if (btnSave != null)
                {
                    linkBtnTrigger(btnSave);
                }
                if (btnConfirm != null)
                {
                    linkBtnTrigger(btnConfirm);
                }
                if (btnCancel != null)
                {
                    linkBtnTrigger(btnCancel);
                }
                if (btnImport != null)
                {
                    linkBtnTrigger(btnImport);
                }

            }

        }
    }

    protected void CreateDsits_u1_PerfEvaluEmpDaily()
    {
        string sDs = "dshr_u1_perf_evalu_employee_daily";
        string sVs = "vshr_u1_perf_evalu_employee_daily";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);

        ds.Tables[sDs].Columns.Add("id", typeof(String));

        ds.Tables[sDs].Columns.Add("doccode", typeof(String));
        ds.Tables[sDs].Columns.Add("zdocdate", typeof(String));
        ds.Tables[sDs].Columns.Add("docdate", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_status", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_emp_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_org_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_rdept_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_rpos_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_rsec_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("remark", typeof(String));
        ds.Tables[sDs].Columns.Add("CEmpIDX", typeof(String));
        ds.Tables[sDs].Columns.Add("CreateDate", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_emp_name_th", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_pos_name_th", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_sec_name_th", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_org_name_th", typeof(String));
        ds.Tables[sDs].Columns.Add("u1idx", typeof(String));
        ds.Tables[sDs].Columns.Add("u0idx", typeof(String));
        ds.Tables[sDs].Columns.Add("emp_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("work_experience_qty", typeof(String));
        ds.Tables[sDs].Columns.Add("ownership_qty", typeof(String));
        ds.Tables[sDs].Columns.Add("improvement_continuously_qty", typeof(String));
        ds.Tables[sDs].Columns.Add("commitment_qty", typeof(String));
        ds.Tables[sDs].Columns.Add("total_score", typeof(String));
        ds.Tables[sDs].Columns.Add("missing_qty", typeof(String));
        ds.Tables[sDs].Columns.Add("sick_Leave_qty", typeof(String));
        ds.Tables[sDs].Columns.Add("call_back_qty", typeof(String));
        ds.Tables[sDs].Columns.Add("warning_notice_qty", typeof(String));
        ds.Tables[sDs].Columns.Add("work_break_qty", typeof(String));
        ds.Tables[sDs].Columns.Add("total_deduction", typeof(String));
        ds.Tables[sDs].Columns.Add("deduction_qty", typeof(String));
        ds.Tables[sDs].Columns.Add("score_balance", typeof(String));
        ds.Tables[sDs].Columns.Add("score_assessment_qty", typeof(String));
        ds.Tables[sDs].Columns.Add("grade", typeof(String));
        ds.Tables[sDs].Columns.Add("labor_cost", typeof(String));
        ds.Tables[sDs].Columns.Add("bonus", typeof(String));
        ds.Tables[sDs].Columns.Add("labor_cost_new", typeof(String));
        ds.Tables[sDs].Columns.Add("bonus_day", typeof(String));
        ds.Tables[sDs].Columns.Add("UEmpIDX", typeof(String));
        ds.Tables[sDs].Columns.Add("UpdateDate", typeof(String));
        ds.Tables[sDs].Columns.Add("u1_status", typeof(String));
        ds.Tables[sDs].Columns.Add("emp_code", typeof(String));
        ds.Tables[sDs].Columns.Add("rpos_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("pos_name_th", typeof(String));
        ds.Tables[sDs].Columns.Add("rsec_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("sec_name_th", typeof(String));
        ds.Tables[sDs].Columns.Add("rdept_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("dept_name_th", typeof(String));
        ds.Tables[sDs].Columns.Add("org_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("org_name_th", typeof(String));
        ds.Tables[sDs].Columns.Add("emp_name_th", typeof(String));
        ds.Tables[sDs].Columns.Add("costcenter_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("costcenter_no", typeof(String));
        ds.Tables[sDs].Columns.Add("costcenter_name", typeof(String));
        ds.Tables[sDs].Columns.Add("emp_probation_date", typeof(String));
        //ds.Tables[sDs].Columns.Add("tax_mounth", typeof(String));
        ds.Tables[sDs].Columns.Add("supervisor1_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("supervisor2_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("grade_criteria", typeof(String));
        ds.Tables[sDs].Columns.Add("description", typeof(String));
        ds.Tables[sDs].Columns.Add("work_experience", typeof(String));
        ds.Tables[sDs].Columns.Add("admonish_speech", typeof(String));

        ds.Tables[sDs].Columns.Add("total_score_before", typeof(String));
        ds.Tables[sDs].Columns.Add("total_deduction_before", typeof(String));
        ds.Tables[sDs].Columns.Add("score_balance_before", typeof(String));
        ds.Tables[sDs].Columns.Add("score_assessment_qty_before", typeof(String));

        ViewState[sVs] = ds;

    }

    protected void VMain_Showdata(int id)
    {


        _data_hr_evaluation.hr_perf_evalu_emp_action = new hr_perf_evalu_emp[1];
        hr_perf_evalu_emp select_obj = new hr_perf_evalu_emp();
        select_obj.u0idx = id;
        select_obj.operation_status_id = "list_th";
        _data_hr_evaluation.hr_perf_evalu_emp_action[0] = select_obj;
        _data_hr_evaluation = callServicePostPerfEvaluEmp(_urlGetits_u_hr_evaluation, _data_hr_evaluation);
        FvInsertH.DataSource = _data_hr_evaluation.hr_perf_evalu_emp_action;
        FvInsertH.DataBind();
        Label lb_u0_status = (Label)FvInsertH.FindControl("lb_u0_status");
        Label ddlu0_status = (Label)FvInsertH.FindControl("ddlu0_status");
        lb_u0_status.Text = getStatus(_func_dmu.zStringToInt(ddlu0_status.Text));

        int _emp_idx = 0;
        if (_data_hr_evaluation.hr_perf_evalu_emp_action != null)
        {
            _emp_idx = _data_hr_evaluation.hr_perf_evalu_emp_action[0].u0_emp_idx;
        }
        else
        {
            _emp_idx = emp_idx;
        }

        FV_docno.DataSource = _data_hr_evaluation.hr_perf_evalu_emp_action;
        FV_docno.DataBind();

        setUserMain(_emp_idx);

        setObject_Main();
        _txtremark.Enabled = true;
        _ddlu0_status.Enabled = true;
        _btnsave.Visible = true;
        _btncalculator.Visible = true;
        _btnExportInput.Visible = true;
        _btnConfirm.Visible = true;
        _Panel_ImportAdd.Visible = true;
        if (id == 0)
        {
            _data_hr_evaluation.hr_perf_evalu_emp_action = new hr_perf_evalu_emp[1];
            select_obj = new hr_perf_evalu_emp();
            select_obj.emp_idx = emp_idx;
            select_obj.operation_status_id = "list_employee_daily";
            _data_hr_evaluation.hr_perf_evalu_emp_action[0] = select_obj;
            // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_hr_evaluation));
            _data_hr_evaluation = callServicePostPerfEvaluEmp(_urlGetits_u_hr_evaluation, _data_hr_evaluation);
        }
        else
        {
            if (_data_hr_evaluation.hr_perf_evalu_emp_action != null)
            {
                foreach (var item in _data_hr_evaluation.hr_perf_evalu_emp_action)
                {
                    if ((item.u0_emp_idx == emp_idx) && (_func_dmu.zStringToInt(ddlu0_status.Text) != 2))
                    {
                        // 
                    }
                    else
                    {
                        _txtremark.Enabled = false;
                        _ddlu0_status.Enabled = false;
                        _btnsave.Visible = false;
                        _btncalculator.Visible = false;
                        _btnExportInput.Visible = false;
                        _btnConfirm.Visible = false;
                        _Panel_ImportAdd.Visible = false;
                    }
                }
            }
            _data_hr_evaluation.hr_perf_evalu_emp_action = new hr_perf_evalu_emp[1];
            select_obj = new hr_perf_evalu_emp();
            select_obj.u0idx = id;
            select_obj.operation_status_id = "list_td";
            _data_hr_evaluation.hr_perf_evalu_emp_action[0] = select_obj;
            // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_hr_evaluation));
            _data_hr_evaluation = callServicePostPerfEvaluEmp(_urlGetits_u_hr_evaluation, _data_hr_evaluation);
        }


        CreateDsits_u1_PerfEvaluEmpDaily();
        //collum = 21 k.นุช
        //collum = 22 k.แก้ม
        //_GridView.Columns[21].HeaderText = "เกรด";
        // _GridView.Columns[22].HeaderText = "เกรดสุทธิ";
        //_GridView.Columns[20].Visible = true;
        //_GridView.Columns[21].Visible = true;
        //litDebug.Text = ViewState["admin_idx"].ToString();
        // if (_func_dmu.zStringToInt(ViewState["admin_idx"].ToString()) == 0)
        // {
        //     _GridView.Columns[22].Visible = true;
        //     _GridView.Columns[23].Visible = false;
        // }
        // else if (_func_dmu.zStringToInt(ViewState["admin_idx"].ToString()) == 1) //  k.นุช
        // {
        //     _GridView.Columns[22].Visible = true;
        //     _GridView.Columns[23].Visible = false;
        // }
        // else if (_func_dmu.zStringToInt(ViewState["admin_idx"].ToString()) == 2) //  Dev.
        // {
        //     _GridView.Columns[22].Visible = true;
        //     // _GridView.Columns[21].HeaderText = "เกรด";
        //     _GridView.Columns[23].Visible = true;
        //     // _GridView.Columns[22].HeaderText = "เกรดสุทธิ";
        // }
        // else if (_func_dmu.zStringToInt(ViewState["admin_idx"].ToString()) == 3) //  k.แก้ม
        // {
        //     _GridView.Columns[22].Visible = true;
        //     _GridView.Columns[23].Visible = true;
        // }
        // else
        // {
        //     _GridView.Columns[22].Visible = true;
        //     _GridView.Columns[23].Visible = false;
        // }

        DataSet dsContacts = (DataSet)ViewState["vshr_u1_perf_evalu_employee_daily"];
        if (_data_hr_evaluation.hr_perf_evalu_emp_action != null)
        {
            int ic = 0;
            foreach (var vdr in _data_hr_evaluation.hr_perf_evalu_emp_action)
            {
                ic++;

                DataRow drContacts = dsContacts.Tables["dshr_u1_perf_evalu_employee_daily"].NewRow();

                drContacts["doccode"] = vdr.doccode;
                drContacts["zdocdate"] = vdr.zdocdate;
                drContacts["docdate"] = vdr.docdate;
                drContacts["u0_status"] = vdr.u0_status;

                drContacts["u0_emp_idx"] = vdr.u0_emp_idx;
                drContacts["u0_org_idx"] = vdr.u0_org_idx;
                drContacts["u0_rdept_idx"] = vdr.u0_rdept_idx;
                drContacts["u0_rpos_idx"] = vdr.u0_rpos_idx;
                drContacts["u0_rsec_idx"] = vdr.u0_rsec_idx;
                drContacts["remark"] = vdr.remark;
                drContacts["CEmpIDX"] = vdr.CEmpIDX;
                drContacts["CreateDate"] = vdr.CreateDate;
                drContacts["u0_emp_name_th"] = vdr.u0_emp_name_th;
                drContacts["u0_pos_name_th"] = vdr.u0_pos_name_th;
                drContacts["u0_sec_name_th"] = vdr.u0_sec_name_th;
                drContacts["u0_org_name_th"] = vdr.u0_org_name_th;
                drContacts["u1idx"] = vdr.u1idx;
                drContacts["u0idx"] = vdr.u0idx;
                drContacts["emp_idx"] = vdr.emp_idx;
                drContacts["work_experience_qty"] = vdr.work_experience;

                drContacts["ownership_qty"] = vdr.ownership_qty;
                drContacts["improvement_continuously_qty"] = vdr.improvement_continuously_qty;
                drContacts["commitment_qty"] = vdr.commitment_qty;

                drContacts["total_score"] = vdr.total_score;
                drContacts["missing_qty"] = vdr.missing_qty;
                drContacts["sick_Leave_qty"] = vdr.sick_Leave_qty;
                drContacts["call_back_qty"] = vdr.call_back_qty;
                drContacts["warning_notice_qty"] = vdr.warning_notice_qty;
                drContacts["work_break_qty"] = vdr.work_break_qty;
                drContacts["total_deduction"] = vdr.total_deduction;
                drContacts["deduction_qty"] = vdr.deduction_qty;
                drContacts["score_balance"] = vdr.score_balance;
                drContacts["score_assessment_qty"] = vdr.score_assessment_qty;
                drContacts["grade"] = vdr.grade;
                drContacts["labor_cost"] = vdr.labor_cost;
                drContacts["bonus"] = vdr.bonus;
                drContacts["labor_cost_new"] = vdr.bonus_day;
                drContacts["bonus_day"] = vdr.bonus_day;
                drContacts["UEmpIDX"] = vdr.UEmpIDX;
                drContacts["UpdateDate"] = vdr.UpdateDate;
                drContacts["u1_status"] = vdr.u1_status;
                drContacts["emp_code"] = vdr.emp_code;
                drContacts["rpos_idx"] = vdr.rpos_idx;
                drContacts["pos_name_th"] = vdr.pos_name_th;
                drContacts["rsec_idx"] = vdr.rsec_idx;
                drContacts["sec_name_th"] = vdr.sec_name_th;
                drContacts["rdept_idx"] = vdr.rdept_idx;
                drContacts["dept_name_th"] = vdr.dept_name_th;
                drContacts["org_idx"] = vdr.org_idx;
                drContacts["org_name_th"] = vdr.org_name_th;
                drContacts["emp_name_th"] = vdr.emp_name_th;

                drContacts["costcenter_idx"] = vdr.costcenter_idx;
                drContacts["costcenter_no"] = vdr.costcenter_no;
                drContacts["costcenter_name"] = vdr.costcenter_name;
                drContacts["emp_probation_date"] = vdr.emp_probation_date;
                drContacts["supervisor1_idx"] = vdr.supervisor1_idx;
                drContacts["supervisor2_idx"] = vdr.supervisor2_idx;
                drContacts["grade_criteria"] = vdr.grade_criteria;
                drContacts["description"] = vdr.description;
                drContacts["admonish_speech"] = vdr.admonish_speech;

                drContacts["total_score_before"] = vdr.total_score_before;
                drContacts["total_deduction_before"] = vdr.total_deduction_before;
                drContacts["score_balance_before"] = vdr.score_balance_before;
                drContacts["score_assessment_qty_before"] = vdr.score_assessment_qty_before;

                if (id == 0)
                {
                    drContacts["total_deduction"] = getformatfloat(zConductCalculat(Convert.ToInt32(vdr.missing_qty)
                                                                     , Convert.ToInt32(vdr.call_back_qty)
                                                                     , 0
                                                                     , 0
                                                                    ).ToString(), 4);
                }

                dsContacts.Tables["dshr_u1_perf_evalu_employee_daily"].Rows.Add(drContacts);
            }
        }


        ViewState["vshr_u1_perf_evalu_employee_daily"] = dsContacts;
        _func_dmu.zSetGridData(_GridView, dsContacts.Tables["dshr_u1_perf_evalu_employee_daily"]);
        _func_dmu.zSetGridData(GvExportExcel_2, dsContacts.Tables["dshr_u1_perf_evalu_employee_daily"]);
    }

    private void setCalculator()
    {
        setObject_Main();
        foreach (GridViewRow gridrow in _GridView.Rows)
        {
            TextBox txt_ownership_qty = (TextBox)gridrow.FindControl("txt_ownership_qty");
            TextBox txt_improvement_continuously_qty = (TextBox)gridrow.FindControl("txt_improvement_continuously_qty");
            TextBox txt_commitment_qty = (TextBox)gridrow.FindControl("txt_commitment_qty");
            Label txt_total_score = (Label)gridrow.FindControl("txt_total_score");
            Label txt_missing_qty = (Label)gridrow.FindControl("txt_missing_qty");
            Label txt_call_back_qty = (Label)gridrow.FindControl("txt_call_back_qty");
            Label txt_warning_notice_qty = (Label)gridrow.FindControl("txt_warning_notice_qty");
            Label txt_total_deduction = (Label)gridrow.FindControl("txt_total_deduction");
            Label txt_score_balance = (Label)gridrow.FindControl("txt_score_balance");
            Label txt_score_assessment_qty = (Label)gridrow.FindControl("txt_score_assessment_qty");
            Label txt_grade = (Label)gridrow.FindControl("txt_grade");
            Label lb_grade_criteria = (Label)gridrow.FindControl("lb_grade_criteria");
            Label txt_work_break_qty = (Label)gridrow.FindControl("txt_work_break_qty");
            Label txt_sick_Leave_qty = (Label)gridrow.FindControl("txt_sick_Leave_qty");
            Label lb_description = (Label)gridrow.FindControl("lb_description");
            Label lb_admonish_speech = (Label)gridrow.FindControl("lb_admonish_speech");

            Label lb_total_score_before = (Label)gridrow.FindControl("lb_total_score_before");
            //Label lb_total_deduction_before = (Label)gridrow.FindControl("lb_total_deduction_before");
            Label lb_score_balance_before = (Label)gridrow.FindControl("lb_score_balance_before");
            Label lb_score_assessment_qty_before = (Label)gridrow.FindControl("lb_score_assessment_qty_before");

            decimal itotal = 0;
            itotal = _func_dmu.zStringToInt(txt_ownership_qty.Text) +
                    _func_dmu.zStringToInt(txt_improvement_continuously_qty.Text) +
                    _func_dmu.zStringToInt(txt_commitment_qty.Text);
            decimal dtotal = 0;
            decimal dc1 = 3;
            dtotal = (itotal * 4) / dc1;

            lb_total_score_before.Text = getformatfloat(dtotal.ToString(), 4);

            //dtotal = Math.Round(dtotal, 2);

            txt_total_score.Text = getformatfloat(dtotal.ToString(), 4);

            // *********************************** //
            decimal itotal1 = 0;
            itotal = zConductCalculat(_func_dmu.zStringToDecimal(txt_missing_qty.Text)
                                     , _func_dmu.zStringToDecimal(txt_call_back_qty.Text)
                                     , _func_dmu.zStringToDecimal(txt_warning_notice_qty.Text)
                                     , _func_dmu.zStringToDecimal(txt_work_break_qty.Text));
            itotal1 = itotal;

            txt_total_deduction.Text = getformatfloat(itotal.ToString(), 4);

            //จำนวนเต็ม
            //สูตรคำนวนหา 80 %
            itotal = 80 - itotal;//Convert.ToInt32(100 - itotal);
            // itotal = (itotal * 8) / 10;

            if (itotal < 0)
            {
                itotal = decimal.Parse("0", CultureInfo.InvariantCulture);
            }

            txt_score_balance.Text = getformatfloat(itotal.ToString(), 4);
            itotal = itotal + dtotal; //+ Convert.ToInt32(dtotal);
            txt_score_assessment_qty.Text = getformatfloat(itotal.ToString(), 4);

            //หารเอาเศษ
            //สูตรคำนวนหา 80 %
            decimal dtotal1 = 0;
            itotal = 80 - itotal1;//Convert.ToInt32(100 - itotal1);
            dc1 = 10;
            dtotal1 = (itotal * 8) / dc1;

            if (dtotal1 < 0)
            {
                dtotal1 = decimal.Parse("0", CultureInfo.InvariantCulture);
            }

            lb_score_balance_before.Text = getformatfloat(dtotal1.ToString(), 4);
            dtotal1 = _func_dmu.zStringToDecimal(lb_total_score_before.Text) + _func_dmu.zStringToDecimal(lb_score_balance_before.Text);
            lb_score_assessment_qty_before.Text = getformatfloat(dtotal1.ToString(), 4);

            // if (
            //      (_func_dmu.zStringToInt(txt_ownership_qty.Text) > 0) &&
            //      (_func_dmu.zStringToInt(txt_improvement_continuously_qty.Text) > 0) &&
            //      (_func_dmu.zStringToInt(txt_commitment_qty.Text) > 0)
            //     )
            // {
            //     txt_grade.Text = zGradeCalculat(itotal);
            // }
            // else
            // {
            //     txt_grade.Text = "";
            // }

            // grade k.แก้ม
            //     lb_grade_criteria.Text = zGradeCriteriaCalculat(_func_dmu.zStringToInt(txt_ownership_qty.Text)
            //         , _func_dmu.zStringToInt(txt_improvement_continuously_qty.Text)
            //         , _func_dmu.zStringToInt(txt_commitment_qty.Text)
            // , _func_dmu.zStringToInt(txt_total_deduction.Text)
            // , _func_dmu.zStringToInt(txt_score_balance.Text)
            // , _func_dmu.zStringToInt(txt_score_assessment_qty.Text)
            // , _func_dmu.zStringToInt(txt_missing_qty.Text)
            // , _func_dmu.zStringToInt(txt_sick_Leave_qty.Text)
            // , _func_dmu.zStringToInt(txt_call_back_qty.Text)
            // , _func_dmu.zStringToInt(txt_warning_notice_qty.Text)
            // , _func_dmu.zStringToInt(txt_work_break_qty.Text)
            // , txt_grade.Text
            // );

            string _sadmonish_speech = "", _swork_break_qty = "";
            //if (
            //    (_func_dmu.zStringToInt(txt_ownership_qty.Text) > 0) &&
            //    (_func_dmu.zStringToInt(txt_improvement_continuously_qty.Text) > 0) &&
            //    (_func_dmu.zStringToInt(txt_commitment_qty.Text) > 0)
            //   )
            //{
            if (Convert.ToInt32(_func_dmu.zStringToDecimal(lb_admonish_speech.Text)) > 0)
            {
                _sadmonish_speech = "*ตักเตือนด้วยวาจา";
            }

            if (Convert.ToInt32(_func_dmu.zStringToDecimal(txt_warning_notice_qty.Text)) > 3) //ใบเตือนเกินสามครั้งไม่ปรับเงินเดือน
            {
                _swork_break_qty = "*ไม่พิจารณาปรับเงินเดือน";
            }
            else if (Convert.ToInt32(_func_dmu.zStringToDecimal(txt_work_break_qty.Text)) > 0)
            {
                _swork_break_qty = "*ไม่พิจารณาปรับเงินเดือน";
            }
            //}

            lb_description.Text = _sadmonish_speech + " " + _swork_break_qty;

        }
        _GridView.SelectedIndex = 0;

    }

    private Boolean setCheckError(int _iError)
    {
        setObject_Main();
        Boolean _Boolean = false;

        if (_GridView.Rows.Count == 0)
        {
            showMsAlert("ไม่มีข้อมูลพนักงาน !");
            _GridView.Focus();
            return true;
        }
        else
        {

            foreach (GridViewRow gridrow in _GridView.Rows)
            {
                TextBox txt_ownership_qty = (TextBox)gridrow.FindControl("txt_ownership_qty");
                TextBox txt_improvement_continuously_qty = (TextBox)gridrow.FindControl("txt_improvement_continuously_qty");
                TextBox txt_commitment_qty = (TextBox)gridrow.FindControl("txt_commitment_qty");
                Label txt_total_score = (Label)gridrow.FindControl("txt_total_score");
                Label txt_warning_notice_qty = (Label)gridrow.FindControl("txt_warning_notice_qty");
                Label txt_work_break_qty = (Label)gridrow.FindControl("txt_work_break_qty");
                Label lb_emp_code = (Label)gridrow.FindControl("lb_emp_code");
                Label lb_emp_name_th = (Label)gridrow.FindControl("lb_emp_name_th");

                string _sEmp_Name = "";
                _sEmp_Name = lb_emp_code.Text + " : " + lb_emp_name_th.Text;
                string _sMs1 = "เฉพาะจำนวนเต็มเท่านั้น !";
                string _sMs2 = "ตั้งแต่ 1 - 5 เท่านั้น !";
                string _sMs2_cal = "ตั้งแต่ 0 - 5 เท่านั้น !";

                int _bl = 0;
                _bl = TKNCoreValue(txt_ownership_qty.Text, _iError);
                if (_bl == 1)
                {
                    showMsAlert("กรุณากรอกคะแนน การทำงานเป็นทีม เฉพาะจำนวนเต็มเท่านั้น !");
                    txt_ownership_qty.Focus();
                    return true;
                }
                else if (_bl == 2)
                {
                    if (_iError == 1) // 1 save
                    {
                        showMsAlert("กรุณากรอกคะแนน การทำงานเป็นทีม " + _sMs2);
                    }
                    else // 2 cal
                    {
                        showMsAlert("กรุณากรอกคะแนน การทำงานเป็นทีม " + _sMs2_cal);
                    }

                    txt_ownership_qty.Focus();
                    return true;
                }
                _bl = TKNCoreValue(txt_improvement_continuously_qty.Text, _iError);
                if (_bl == 1)
                {
                    showMsAlert("กรุณากรอกคะแนน มุ่งมั่นสู่ความเป็นเสิศ เฉพาะจำนวนเต็มเท่านั้น !");
                    txt_improvement_continuously_qty.Focus();
                    return true;
                }
                else if (_bl == 2)
                {

                    if (_iError == 1) // 1 save
                    {
                        showMsAlert("กรุณากรอกคะแนน มุ่งมั่นสู่ความเป็นเสิศ " + _sMs2);
                    }
                    else // 2 cal
                    {
                        showMsAlert("กรุณากรอกคะแนน มุ่งมั่นสู่ความเป็นเสิศ " + _sMs2_cal);
                    }
                    txt_improvement_continuously_qty.Focus();
                    return true;
                }
                _bl = TKNCoreValue(txt_commitment_qty.Text, _iError);
                if (_bl == 1)
                {
                    showMsAlert("กรุณากรอกคะแนน สร้างสรรค์สิ่งใหม่ เฉพาะจำนวนเต็มเท่านั้น !");
                    txt_commitment_qty.Focus();
                    return true;
                }
                else if (_bl == 2)
                {

                    if (_iError == 1) // 1 save
                    {
                        showMsAlert("กรุณากรอกคะแนน สร้างสรรค์สิ่งใหม่ " + _sMs2);
                    }
                    else // 2 cal
                    {
                        showMsAlert("กรุณากรอกคะแนน สร้างสรรค์สิ่งใหม่ " + _sMs2_cal);
                    }
                    txt_commitment_qty.Focus();
                    return true;
                }

            }

        }
        _GridView.SelectedIndex = 0;

        return _Boolean;
    }

    private int TKNCoreValue(string _qty, int _iError)
    {
        int ierror = 0;

        ierror = 0;
        try
        {
            _func_dmu.zStringToInt(_qty);
        }
        catch
        {
            ierror = 1;
        }
        if (ierror == 1)
        {

        }
        else
        {
            if (_iError == 1)
            {
                if (_func_dmu.zStringToInt(_qty) > 5)
                {
                    ierror = 2;

                }
                else if (_func_dmu.zStringToInt(_qty) <= 0)
                {
                    ierror = 2;

                }
            }
            else
            {
                if (_func_dmu.zStringToInt(_qty) > 5)
                {
                    ierror = 2;

                }
                else if (_func_dmu.zStringToInt(_qty) < 0)
                {
                    ierror = 2;

                }
            }

        }

        return ierror;

    }

    public void showMsAlert(string Str)
    {
        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('" + Str + "');", true);
    }


    private string zGradeCriteriaCalculat(int _ownership, int _improvement_continuously, int _commitment
        , int _total_deduction, int _score_balance, int _score_assessment
        , int _missing, int _sick_Leave, int _call_back, int _warning_notice, int _work_break
        , string _grade
        )
    {
        string _rtGrade = "";
        // // 3 ช่องกรอกไม่ครบไม่ออก เกรด
        // if (
        //     (_ownership > 0) &&
        //     (_improvement_continuously > 0) &&
        //     (_commitment > 0)
        //     )
        // {
        //     //พนักงานที่จะได้เกรด A  คะแนนทุกช่องจะต้องไม่ต่ำกว่า 9  และถูกหักคะแนน ไม่เกิน 5 คะแนน
        //     if ((_ownership >= 9) &&
        //         (_improvement_continuously >= 9) &&
        //         (_commitment >= 9) &&
        //         (_total_deduction <= 5))
        //     {
        //         _rtGrade = "A";
        //     }
        //     //พนักงานที่จะได้เกรด B  คะแนนทุกช่องจะต้องไม่ต่ำกว่า 8  และถูกหักคะแนน ไม่เกิน 20 คะแนน
        //     else if ((_ownership >= 8) &&
        //         (_improvement_continuously >= 8) &&
        //         (_commitment >= 8) &&
        //         (_total_deduction <= 20))
        //     {
        //         _rtGrade = "B";
        //     }
        //     //พนักงานที่มีคะแนนประเมินในแต่ละช่องต่ำกว่า 7 คะแนน
        //     else if ((_ownership < 7) ||
        //         (_improvement_continuously < 7) ||
        //         (_commitment < 7))
        //     {
        //         _rtGrade = "C";
        //     }
        //     //พนักงานที่ลาป่วยเกิน 10 วัน
        //     else if (_sick_Leave > 10)
        //     {
        //         _rtGrade = "C";
        //     }
        //     //พนักงานที่มาสายเกิน 100 นาที
        //     else if (_call_back > 100)
        //     {
        //         _rtGrade = "C";
        //     }
        //     //พนักงานที่มีคะแนนประเมินในแต่ละช่องต่ำกว่า 6 คะแนน
        //     else if ((_ownership < 6) ||
        //         (_improvement_continuously < 6) ||
        //         (_commitment < 6))
        //     {
        //         _rtGrade = "D";
        //     }
        //     //พนักงานที่มียอดหักรวมเกิน 50 คะแนนขึ้นไป  
        //     else if ((_score_assessment >= 95) && (_score_assessment < 50))
        //     {
        //         _rtGrade = "D";
        //     }
        //     else if (_score_assessment > 95)
        //     {
        //         _rtGrade = "E";
        //     }
        //     else
        //     {
        //         _rtGrade = _grade;
        //     }
        // }

        return _rtGrade;
    }

    private int zSave(int id, int _iu0_status)
    {
        setObject_Main();
        int itemObj = 0;

        hr_perf_evalu_emp[] objcourse1 = new hr_perf_evalu_emp[_GridView.Rows.Count];

        foreach (GridViewRow gridrow in _GridView.Rows)
        {

            Label lb_emp_idx = (Label)gridrow.FindControl("lb_emp_idx");
            Label lb_work_experience_qty = (Label)gridrow.FindControl("lb_work_experience_qty");

            TextBox txt_ownership_qty = (TextBox)gridrow.FindControl("txt_ownership_qty");
            TextBox txt_improvement_continuously_qty = (TextBox)gridrow.FindControl("txt_improvement_continuously_qty");
            TextBox txt_commitment_qty = (TextBox)gridrow.FindControl("txt_commitment_qty");
            Label txt_total_score = (Label)gridrow.FindControl("txt_total_score");

            Label txt_missing_qty = (Label)gridrow.FindControl("txt_missing_qty");
            Label txt_sick_Leave_qty = (Label)gridrow.FindControl("txt_sick_Leave_qty");
            Label txt_call_back_qty = (Label)gridrow.FindControl("txt_call_back_qty");
            Label txt_warning_notice_qty = (Label)gridrow.FindControl("txt_warning_notice_qty");
            Label txt_work_break_qty = (Label)gridrow.FindControl("txt_work_break_qty");
            Label txt_total_deduction = (Label)gridrow.FindControl("txt_total_deduction");
            Label txt_deduction_qty = (Label)gridrow.FindControl("txt_deduction_qty");
            Label txt_score_balance = (Label)gridrow.FindControl("txt_score_balance");
            Label txt_score_assessment_qty = (Label)gridrow.FindControl("txt_score_assessment_qty");
            Label txt_grade = (Label)gridrow.FindControl("txt_grade");
            TextBox txt_labor_cost = (TextBox)gridrow.FindControl("txt_labor_cost");

            Label txt_bonus = (Label)gridrow.FindControl("txt_bonus");
            TextBox txt_labor_cost_new = (TextBox)gridrow.FindControl("txt_labor_cost_new");
            TextBox txt_bonus_day = (TextBox)gridrow.FindControl("txt_bonus_day");

            Label lb_u0idx = (Label)gridrow.FindControl("lb_u0idx");
            Label lb_u1idx = (Label)gridrow.FindControl("lb_u1idx");

            Label lb_supervisor1_idx = (Label)gridrow.FindControl("lb_supervisor1_idx");
            Label lb_supervisor2_idx = (Label)gridrow.FindControl("lb_supervisor2_idx");

            Label lb_grade_criteria = (Label)gridrow.FindControl("lb_grade_criteria");
            Label lb_description = (Label)gridrow.FindControl("lb_description");
            Label lb_admonish_speech = (Label)gridrow.FindControl("lb_admonish_speech");

            Label lb_total_score_before = (Label)gridrow.FindControl("lb_total_score_before");
            Label lb_score_balance_before = (Label)gridrow.FindControl("lb_score_balance_before");
            Label lb_score_assessment_qty_before = (Label)gridrow.FindControl("lb_score_assessment_qty_before");

            objcourse1[itemObj] = new hr_perf_evalu_emp();
            objcourse1[itemObj].u0idx = _func_dmu.zStringToInt(lb_u0idx.Text);
            objcourse1[itemObj].u1idx = _func_dmu.zStringToInt(lb_u1idx.Text);

            objcourse1[itemObj].supervisor1_idx = _func_dmu.zStringToInt(lb_supervisor1_idx.Text);
            objcourse1[itemObj].supervisor2_idx = _func_dmu.zStringToInt(lb_supervisor2_idx.Text);

            objcourse1[itemObj].admonish_speech = _func_dmu.zStringToInt(lb_admonish_speech.Text);

            objcourse1[itemObj].emp_idx = _func_dmu.zStringToInt(lb_emp_idx.Text);
            objcourse1[itemObj].work_experience = lb_work_experience_qty.Text;
            objcourse1[itemObj].ownership_qty = _func_dmu.zStringToInt(txt_ownership_qty.Text);
            objcourse1[itemObj].improvement_continuously_qty = _func_dmu.zStringToInt(txt_improvement_continuously_qty.Text);
            objcourse1[itemObj].commitment_qty = _func_dmu.zStringToInt(txt_commitment_qty.Text);
            objcourse1[itemObj].total_score = _func_dmu.zStringToDecimal(txt_total_score.Text);

            objcourse1[itemObj].missing_qty = _func_dmu.zStringToInt(txt_missing_qty.Text);
            objcourse1[itemObj].sick_Leave_qty = _func_dmu.zStringToInt(txt_sick_Leave_qty.Text);
            objcourse1[itemObj].call_back_qty = _func_dmu.zStringToInt(txt_call_back_qty.Text);
            objcourse1[itemObj].warning_notice_qty = _func_dmu.zStringToInt(txt_warning_notice_qty.Text);
            objcourse1[itemObj].work_break_qty = _func_dmu.zStringToInt(txt_work_break_qty.Text);
            objcourse1[itemObj].total_deduction = _func_dmu.zStringToDecimal(txt_total_deduction.Text);

            objcourse1[itemObj].deduction_qty = _func_dmu.zStringToDecimal(txt_deduction_qty.Text);
            objcourse1[itemObj].score_balance = _func_dmu.zStringToDecimal(txt_score_balance.Text);
            objcourse1[itemObj].score_assessment_qty = _func_dmu.zStringToDecimal(txt_score_assessment_qty.Text);

            objcourse1[itemObj].grade = txt_grade.Text;
            objcourse1[itemObj].grade_criteria = lb_grade_criteria.Text;

            objcourse1[itemObj].labor_cost = _func_dmu.zStringToDecimal(txt_labor_cost.Text);
            objcourse1[itemObj].bonus = _func_dmu.zStringToDecimal(txt_bonus.Text);
            objcourse1[itemObj].labor_cost_new = _func_dmu.zStringToDecimal(txt_labor_cost_new.Text);
            objcourse1[itemObj].bonus_day = _func_dmu.zStringToDecimal(txt_bonus_day.Text);
            objcourse1[itemObj].CEmpIDX = emp_idx;
            objcourse1[itemObj].UEmpIDX = emp_idx;

            objcourse1[itemObj].u0_emp_idx = emp_idx;
            objcourse1[itemObj].u0_org_idx = _func_dmu.zStringToInt(ViewState["org_idx"].ToString());
            objcourse1[itemObj].u0_rdept_idx = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());
            objcourse1[itemObj].u0_rpos_idx = _func_dmu.zStringToInt(ViewState["rpos_idx"].ToString());
            objcourse1[itemObj].u0_rsec_idx = _func_dmu.zStringToInt(ViewState["rsec_idx"].ToString());
            objcourse1[itemObj].remark = _txtremark.Text;
            objcourse1[itemObj].u0_status = _iu0_status; //_func_dmu.zStringToInt(_ddlu0_status.Text);
            objcourse1[itemObj].description = lb_description.Text;

            objcourse1[itemObj].total_score_before = _func_dmu.zStringToDecimal(lb_total_score_before.Text);
            objcourse1[itemObj].score_balance_before = _func_dmu.zStringToDecimal(lb_score_balance_before.Text);
            objcourse1[itemObj].score_assessment_qty_before = _func_dmu.zStringToDecimal(lb_score_assessment_qty_before.Text);

            objcourse1[itemObj].operation_status_id = "perf_evalu_employee_daily";

            itemObj++;
        }
        _data_hr_evaluation.hr_perf_evalu_emp_action = objcourse1;
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_hr_evaluation));

        if (id == 0)
        {
            _data_hr_evaluation = callServicePostPerfEvaluEmp(_urlSetInsits_u_hr_evaluation, _data_hr_evaluation);
            id = _data_hr_evaluation.return_code;
        }
        else
        {
            callServicePostPerfEvaluEmp(_urlSetUpdits_u_hr_evaluation, _data_hr_evaluation);
        }

        if (_iu0_status == 2) //
        {
            _data_hr_evaluation.hr_perf_evalu_emp_action = new hr_perf_evalu_emp[1];
            hr_perf_evalu_emp select_obj = new hr_perf_evalu_emp();
            select_obj.u0idx = id;
            select_obj.operation_status_id = "daily";
            _data_hr_evaluation.hr_perf_evalu_emp_action[0] = select_obj;
            callServicePostPerfEvaluEmp(_urlsendEmailhr_evaluation, _data_hr_evaluation);
        }

        return id;
    }

    private decimal zConductCalculat(decimal _missing, decimal _call_back, decimal _warning_notice, decimal _work_break)
    {
        decimal total = 0;
        decimal _decimal1 = 30;
        _decimal1 = _call_back / _decimal1;
        // _decimal1 = Math.Ceiling(_decimal1);
        _call_back = _decimal1; //Convert.ToInt32(_decimal1);

        _missing = _missing * 5; // ขาดงานครั้งละ 5 คะแนน

        _warning_notice = _warning_notice * 10; // ใบเตือน ครั้งละ 10 คะแนน

        _work_break = _work_break * 10; // พักงาน ครั้งละ 10 คะแนน

        total = _missing + _call_back + _warning_notice + _work_break;

        return total;
    }

    private void zDelete(int id)
    {
        _data_hr_evaluation.hr_perf_evalu_emp_action = new hr_perf_evalu_emp[1];
        hr_perf_evalu_emp select_obj = new hr_perf_evalu_emp();
        select_obj.u0idx = id;
        select_obj.operation_status_id = "perf_evalu_employee_daily";
        _data_hr_evaluation.hr_perf_evalu_emp_action[0] = select_obj;
        callServicePostPerfEvaluEmp(_urlDelits_u_hr_evaluation, _data_hr_evaluation);
    }

    protected string getStatus(int status)
    {
        if (status == 2)
        {
            // return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
            return "จบการดำเนินการและส่งข้อมูลให้ HR";
        }
        {

            // return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
            return "ดำเนินการสร้าง / แก้ไข";
        }
    }

    private string zGradeCalculat(int total)
    {
        string _Grade = "";
        // A Outstanding ผลการปฏิบัติงานโดดเด่นอย่างชัดเจน 96 - 100 %
        if ((total >= 96) && (total <= 100))
        {
            _Grade = "A";
        }
        //B Exceeds Expectations ผลการปฏิบัติงานเกินกว่าความคาดหวัง 81-95%
        else if ((total >= 81) && (total <= 95))
        {
            _Grade = "B";
        }
        //C Meet Expectations ผลการปฏิบัติงานเป็นไปตามความคาดหวัง 61-80%
        else if ((total >= 61) && (total <= 80))
        {
            _Grade = "C";
        }
        //D Improvement Needed ผลการปฏิบัติงานยังต้องปรับปรุง 50-60% (ไม่พิจารณาปรับเงินเดือน) 
        else if ((total >= 50) && (total <= 60))
        {
            _Grade = "D";
        }
        else
        {
            _Grade = "E";
        }

        return _Grade;
    }

    private void zExportExcel(int id)
    {

        _data_hr_evaluation.hr_perf_evalu_emp_action = new hr_perf_evalu_emp[1];
        hr_perf_evalu_emp select_obj = new hr_perf_evalu_emp();
        select_obj.u0idx = id;
        select_obj.admin_idx = _func_dmu.zStringToInt(ViewState["admin_idx"].ToString());
        select_obj.zstatus = "exportdata";
        select_obj.operation_status_id = "list_td_export";
        _data_hr_evaluation.hr_perf_evalu_emp_action[0] = select_obj;
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_hr_evaluation));
        _data_hr_evaluation = callServicePostPerfEvaluEmp(_urlGetits_u_hr_evaluation, _data_hr_evaluation);

        int ic = 0;
        if (_func_dmu.zStringToInt(ViewState["admin_idx"].ToString()) == 0)
        {
            ic = 1;
        }
        else if (_func_dmu.zStringToInt(ViewState["admin_idx"].ToString()) == 1) //  k.นุช
        {
            ic = 1;
        }
        else if (_func_dmu.zStringToInt(ViewState["admin_idx"].ToString()) == 2) //  Dev.
        {
            ic = 2;
        }
        else if (_func_dmu.zStringToInt(ViewState["admin_idx"].ToString()) == 3) //  k.แก้ม
        {
            ic = 2;
        }
        else
        {
            ic = 1;
        }
        if (ic == 1)
        {
            _func_dmu.zSetGridData(GvExportExcel_1, _data_hr_evaluation.hr_perf_evalu_emp_action);
            ExportGridToExcel(GvExportExcel_1, "แบบประเมินผลการปฏิบัติงานระดับพนักงานรายวัน", "แบบประเมินผลการปฏิบัติงานระดับพนักงานรายวัน");
        }
        else
        {
            _func_dmu.zSetGridData(GvExportExcel, _data_hr_evaluation.hr_perf_evalu_emp_action);
            ExportGridToExcel(GvExportExcel, "แบบประเมินผลการปฏิบัติงานระดับพนักงานรายวัน", "แบบประเมินผลการปฏิบัติงานระดับพนักงานรายวัน");
        }
    }

    private void ExportGridToExcel(GridView _gv, string sfilename = "ExportToExcel", string ATitle = "Export To Excel")
    {
        if (_gv.Rows.Count == 0)
        {
            return;
        }
        //_gv.AllowPaging = false;
        //_gv.DataBind();
        var totalCols = _gv.Rows[0].Cells.Count;
        var totalRows = _gv.Rows.Count;
        var headerRow = _gv.HeaderRow;

        DataTable tableMaterialLog = new DataTable();

        for (var i = 1; i <= totalCols; i++)
        {
            tableMaterialLog.Columns.Add(headerRow.Cells[i - 1].Text, typeof(String));

        }
        for (var j = 1; j <= totalRows; j++)
        {
            DataRow addMaterialLogRow = tableMaterialLog.NewRow();
            for (var i = 1; i <= totalCols; i++)
            {
                string sdata = ((Label)_gv.Rows[j - 1].Cells[i - 1].Controls[1]).Text;
                addMaterialLogRow[i - 1] = sdata;

            }
            tableMaterialLog.Rows.Add(addMaterialLogRow);
        }
        GridView gv = new GridView();
        gv.DataSource = tableMaterialLog;
        gv.DataBind();
        WriteExcelWithNPOI(tableMaterialLog, "xlsx", sfilename, ATitle);

    }

    public void WriteExcelWithNPOI(DataTable dt, String extension, String fileName, string ATitle)
    {
        IWorkbook workbook;
        if (extension == "xlsx")
        {
            workbook = new XSSFWorkbook();
        }
        else if (extension == "xls")
        {
            workbook = new HSSFWorkbook();
        }
        else
        {
            throw new Exception("This format is not supported");
        }
        ISheet sheet1 = workbook.CreateSheet("Sheet 1");

        IRow row1 = sheet1.CreateRow(0);

        ICell cellTitle = row1.CreateCell(0);
        String columnNameTitle = ATitle;
        cellTitle.SetCellValue(columnNameTitle);


        row1 = sheet1.CreateRow(1);
        cellTitle = row1.CreateCell(0);
        columnNameTitle = "Print Date : " + DateTime.Now.ToString("dd/MM/yyyy", CultureInfo.InstalledUICulture);
        cellTitle.SetCellValue(columnNameTitle);

        row1 = sheet1.CreateRow(3);

        for (int j = 0; j < dt.Columns.Count; j++)
        {
            ICell cell = row1.CreateCell(j);
            String columnName = dt.Columns[j].ToString();
            cell.SetCellValue(columnName);
        }
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            IRow row = sheet1.CreateRow(i + 4);
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                ICell cell = row.CreateCell(j);
                String columnName = dt.Columns[j].ToString();
                cell.SetCellValue(dt.Rows[i][columnName].ToString());
            }
        }

        using (var exportData = new MemoryStream())
        {
            Response.Clear();
            workbook.Write(exportData);
            if (extension == "xlsx")
            {
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xlsx"));
                Response.BinaryWrite(exportData.ToArray());
            }
            else if (extension == "xls")
            {
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xls"));
                Response.BinaryWrite(exportData.GetBuffer());
            }
            Response.End();
        }
    }

    private int getGrade(int count_grade, int count_grade_criteria)
    {
        int _grade = 0;
        if (_func_dmu.zStringToInt(ViewState["admin_idx"].ToString()) == 0)
        {
            _grade = count_grade;
        }
        else if (_func_dmu.zStringToInt(ViewState["admin_idx"].ToString()) == 1) //  k.นุช
        {
            _grade = count_grade;
        }
        else if (_func_dmu.zStringToInt(ViewState["admin_idx"].ToString()) == 2) //  Dev.
        {
            if (ddlgrade.SelectedValue == "1")
            {
                _grade = count_grade;
            }
            else
            {
                _grade = count_grade_criteria;
            }
        }
        else if (_func_dmu.zStringToInt(ViewState["admin_idx"].ToString()) == 3) //  k.แก้ม
        {
            if (ddlgrade.SelectedValue == "1")
            {
                _grade = count_grade;
            }
            else
            {
                _grade = count_grade_criteria;
            }
        }
        else
        {
            _grade = count_grade;
        }

        return _grade;
    }

    public void getCountBookingReportGraph()
    {
        _data_hr_evaluation.hr_perf_evalu_emp_action = new hr_perf_evalu_emp[1];
        hr_perf_evalu_emp select_obj = new hr_perf_evalu_emp();
        select_obj.emp_idx = emp_idx;
        select_obj.admin_idx = _func_dmu.zStringToInt(ViewState["admin_idx"].ToString());
        //select_obj.rpos_idx = _func_dmu.zStringToInt(ViewState["rpos_idx"].ToString());
        //select_obj.rdept_idx = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());
        select_obj.zyear = _func_dmu.zStringToInt(ddlyear.SelectedValue);
        select_obj.operation_status_id = "garp_grade";
        _data_hr_evaluation.hr_perf_evalu_emp_action[0] = select_obj;
        //litDebug.Text = ViewState["admin_idx"].ToString();
        _data_hr_evaluation = callServicePostPerfEvaluEmp(_urlGetits_u_hr_evaluation, _data_hr_evaluation);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_hr_evaluation));
        ViewState["Vs_ReportCountGarp"] = _data_hr_evaluation.hr_perf_evalu_emp_action;
        if (_data_hr_evaluation.hr_perf_evalu_emp_action != null)
        {

            //litDebug.Text = "3333";
            int count = _data_hr_evaluation.hr_perf_evalu_emp_action.Length;
            string[] Dept_name = new string[count];
            object[] Count_booking = new object[count];
            int i = 0;
            int iTotal = 0;
            foreach (var data in _data_hr_evaluation.hr_perf_evalu_emp_action)
            {
                int g = getGrade(data.count_grade, data.count_grade_criteria);
                Dept_name[i] = data.grade_code;
                Count_booking[i] = g.ToString();
                iTotal += g;

                i++;
            }
            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "จำนวนพนักงาน" } } });
            chart.SetXAxis(new XAxis { Categories = Dept_name });
            chart.SetSeries(
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "จำนวนพนักงาน",
                    Data = new Data(Count_booking)
                }
            );
            Update_PanelGraph.Visible = true;
            litReportChart.Text = chart.ToHtmlString();

            // -- -- //

            // -- pie chart --//
            var caseclose = new List<object>();

            foreach (var data in _data_hr_evaluation.hr_perf_evalu_emp_action)
            {
                int g = getGrade(data.count_grade, data.count_grade_criteria);
                caseclose.Add(new object[] {

                    data.grade_code.ToString(), g.ToString() });
            }

            Highcharts chart1 = new Highcharts("chart1");
            chart1.SetTitle(new DotNet.Highcharts.Options.Title { Text = "จำนวนพนักงาน / " + _func_dmu.zGetTextDropDownList(ddlgrade) });
            chart1.InitChart(new DotNet.Highcharts.Options.Chart { DefaultSeriesType = DotNet.Highcharts.Enums.ChartTypes.Pie });

            chart1.SetPlotOptions(new PlotOptions
            {
                Pie = new PlotOptionsPie
                {
                    ShowInLegend = true,
                    AllowPointSelect = true,
                    DataLabels = new PlotOptionsPieDataLabels
                    {
                        Enabled = true,
                        // Formatter = "function() { return '" + this. + ' : '" + lv1count + "';}" //  this.percentage.toFixed(2)+' % '; }"
                        //Color = ColorTranslator.FromHtml("#000000"),
                        //ConnectorColor = ColorTranslator.FromHtml("#000000"),
                        Formatter = "function() { return this.point.name +' : '+  this.y.toFixed(0)+' '; }"
                    }
                }
            });

            chart1.SetTooltip(new Tooltip
            {
                Enabled = true,
                Formatter = "function() { return this.point.name +' : '+  this.percentage.toFixed(2)+' % '; }"

            });
            chart1.SetSeries(new DotNet.Highcharts.Options.Series[]
                 {
                        new DotNet.Highcharts.Options.Series()
                        {
                            Data = new Data (caseclose.ToArray())
                        }
                 });


            litReportChart1.Text = chart1.ToHtmlString();
            if (iTotal == 0)
            {
                //  Update_PanelGraph.Visible = false;
            }
            // -- pie chart --//
        }
        else
        {
            Update_PanelGraph.Visible = false;

        }
    }

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlName = (DropDownList)sender;

        switch (ddlName.ID)
        {

            case "ddlTypeReport":

                Update_PanelGraph.Visible = false;
                Update_Panelreport.Visible = false;
                btnexport_report.Visible = false;
                pln_grade.Visible = false;
                ddlgrade.SelectedValue = "1";
                if (ddlName.SelectedValue == "1")
                {
                    CreateDsits_u1_PerfEvaluEmpDaily();
                    btnexport_report.Visible = true;
                }
                else if (ddlName.SelectedValue == "2")
                {

                    int ic = 0;
                    if (_func_dmu.zStringToInt(ViewState["admin_idx"].ToString()) == 0)
                    {
                        ic = 1;
                    }
                    else if (_func_dmu.zStringToInt(ViewState["admin_idx"].ToString()) == 1) //  k.นุช
                    {
                        ic = 1;
                    }
                    else if (_func_dmu.zStringToInt(ViewState["admin_idx"].ToString()) == 2) //  Dev.
                    {
                        ic = 2;
                    }
                    else if (_func_dmu.zStringToInt(ViewState["admin_idx"].ToString()) == 3) //  k.แก้ม
                    {
                        ic = 2;
                    }
                    else
                    {
                        ic = 1;
                    }
                    if (ic == 1)
                    {

                    }
                    else
                    {
                        pln_grade.Visible = true;
                    }
                }

                break;
        }
    }

    public void ReportGraph()
    {
        _data_hr_evaluation.hr_perf_evalu_emp_action = new hr_perf_evalu_emp[1];
        hr_perf_evalu_emp select_obj = new hr_perf_evalu_emp();
        select_obj.emp_idx = emp_idx;
        select_obj.admin_idx = _func_dmu.zStringToInt(ViewState["admin_idx"].ToString());
        select_obj.zyear = _func_dmu.zStringToInt(ddlyear.SelectedValue);
        select_obj.operation_status_id = "garp_report";
        _data_hr_evaluation.hr_perf_evalu_emp_action[0] = select_obj;
        _data_hr_evaluation = callServicePostPerfEvaluEmp(_urlGetits_u_hr_evaluation, _data_hr_evaluation);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_hr_evaluation));

        // if (_func_dmu.zStringToInt(ViewState["admin_idx"].ToString()) == 0)
        // {
        //     GVreport.Columns[22].Visible = true;
        //     GVreport.Columns[23].Visible = false;
        // }
        // else if (_func_dmu.zStringToInt(ViewState["admin_idx"].ToString()) == 1) //  k.นุช
        // {
        //     GVreport.Columns[22].Visible = true;
        //     GVreport.Columns[23].Visible = false;
        // }
        // else if (_func_dmu.zStringToInt(ViewState["admin_idx"].ToString()) == 2) //  Dev.
        // {
        //     GVreport.Columns[22].Visible = true;
        //     // _GridView.Columns[21].HeaderText = "เกรด";
        //     GVreport.Columns[23].Visible = true;
        //     // _GridView.Columns[22].HeaderText = "เกรดสุทธิ";
        // }
        // else if (_func_dmu.zStringToInt(ViewState["admin_idx"].ToString()) == 3) //  k.แก้ม
        // {
        //     GVreport.Columns[22].Visible = true;
        //     GVreport.Columns[23].Visible = true;
        // }
        // else
        // {
        //     GVreport.Columns[22].Visible = true;
        //     GVreport.Columns[23].Visible = false;
        // }

        CreateDsits_u1_PerfEvaluEmpDaily();
        DataSet dsContacts = (DataSet)ViewState["vshr_u1_perf_evalu_employee_daily"];
        if (_data_hr_evaluation.hr_perf_evalu_emp_action != null)
        {
            int ic = 0;
            foreach (var vdr in _data_hr_evaluation.hr_perf_evalu_emp_action)
            {
                ic++;

                DataRow drContacts = dsContacts.Tables["dshr_u1_perf_evalu_employee_daily"].NewRow();

                drContacts["doccode"] = vdr.doccode;
                drContacts["zdocdate"] = vdr.zdocdate;
                drContacts["docdate"] = vdr.docdate;
                drContacts["u0_status"] = vdr.u0_status;

                drContacts["u0_emp_idx"] = vdr.u0_emp_idx;
                drContacts["u0_org_idx"] = vdr.u0_org_idx;
                drContacts["u0_rdept_idx"] = vdr.u0_rdept_idx;
                drContacts["u0_rpos_idx"] = vdr.u0_rpos_idx;
                drContacts["u0_rsec_idx"] = vdr.u0_rsec_idx;
                drContacts["remark"] = vdr.remark;
                drContacts["CEmpIDX"] = vdr.CEmpIDX;
                drContacts["CreateDate"] = vdr.CreateDate;
                drContacts["u0_emp_name_th"] = vdr.u0_emp_name_th;
                drContacts["u0_pos_name_th"] = vdr.u0_pos_name_th;
                drContacts["u0_sec_name_th"] = vdr.u0_sec_name_th;
                drContacts["u0_org_name_th"] = vdr.u0_org_name_th;
                drContacts["u1idx"] = vdr.u1idx;
                drContacts["u0idx"] = vdr.u0idx;
                drContacts["emp_idx"] = vdr.emp_idx;
                drContacts["work_experience"] = vdr.work_experience;

                drContacts["ownership_qty"] = vdr.ownership_qty;
                drContacts["improvement_continuously_qty"] = vdr.improvement_continuously_qty;
                drContacts["commitment_qty"] = vdr.commitment_qty;

                drContacts["total_score"] = vdr.total_score;
                drContacts["missing_qty"] = vdr.missing_qty;
                drContacts["sick_Leave_qty"] = vdr.sick_Leave_qty;
                drContacts["call_back_qty"] = vdr.call_back_qty;
                drContacts["warning_notice_qty"] = vdr.warning_notice_qty;
                drContacts["work_break_qty"] = vdr.work_break_qty;
                drContacts["total_deduction"] = vdr.total_deduction;
                drContacts["deduction_qty"] = vdr.deduction_qty;
                drContacts["score_balance"] = vdr.score_balance;
                drContacts["score_assessment_qty"] = vdr.score_assessment_qty;
                drContacts["grade"] = vdr.grade;
                drContacts["labor_cost"] = vdr.labor_cost;
                drContacts["bonus"] = vdr.bonus;
                drContacts["labor_cost_new"] = vdr.bonus_day;
                drContacts["bonus_day"] = vdr.bonus_day;
                drContacts["UEmpIDX"] = vdr.UEmpIDX;
                drContacts["UpdateDate"] = vdr.UpdateDate;
                drContacts["u1_status"] = vdr.u1_status;
                drContacts["emp_code"] = vdr.emp_code;
                drContacts["rpos_idx"] = vdr.rpos_idx;
                drContacts["pos_name_th"] = vdr.pos_name_th;
                drContacts["rsec_idx"] = vdr.rsec_idx;
                drContacts["sec_name_th"] = vdr.sec_name_th;
                drContacts["rdept_idx"] = vdr.rdept_idx;
                drContacts["dept_name_th"] = vdr.dept_name_th;
                drContacts["org_idx"] = vdr.org_idx;
                drContacts["org_name_th"] = vdr.org_name_th;
                drContacts["emp_name_th"] = vdr.emp_name_th;

                drContacts["costcenter_idx"] = vdr.costcenter_idx;
                drContacts["costcenter_no"] = vdr.costcenter_no;
                drContacts["costcenter_name"] = vdr.costcenter_name;
                drContacts["emp_probation_date"] = vdr.emp_probation_date;
                drContacts["supervisor1_idx"] = vdr.supervisor1_idx;
                drContacts["supervisor2_idx"] = vdr.supervisor2_idx;
                drContacts["grade_criteria"] = vdr.grade_criteria;
                drContacts["description"] = vdr.description;
                drContacts["admonish_speech"] = vdr.admonish_speech;

                drContacts["total_score_before"] = vdr.total_score_before;
                drContacts["total_deduction_before"] = vdr.total_deduction_before;
                drContacts["score_balance_before"] = vdr.score_balance_before;
                drContacts["score_assessment_qty_before"] = vdr.score_assessment_qty_before;

                dsContacts.Tables["dshr_u1_perf_evalu_employee_daily"].Rows.Add(drContacts);
            }
        }


        ViewState["vshr_u1_perf_evalu_employee_daily"] = dsContacts;
        _func_dmu.zSetGridData(GVreport, dsContacts.Tables["dshr_u1_perf_evalu_employee_daily"]);

        Update_PanelGraph.Visible = false;
        Update_Panelreport.Visible = true;
    }

    private void zExportExcel_report()
    {
        int ic = 0;
        if (_func_dmu.zStringToInt(ViewState["admin_idx"].ToString()) == 0)
        {
            ic = 1;
        }
        else if (_func_dmu.zStringToInt(ViewState["admin_idx"].ToString()) == 1) //  k.นุช
        {
            ic = 1;
        }
        else if (_func_dmu.zStringToInt(ViewState["admin_idx"].ToString()) == 2) //  Dev.
        {
            ic = 2;
        }
        else if (_func_dmu.zStringToInt(ViewState["admin_idx"].ToString()) == 3) //  k.แก้ม
        {
            ic = 2;
        }
        else
        {
            ic = 1;
        }
        if (ic == 1)
        {
            _func_dmu.zSetGridData(GvExportExcel_1, ViewState["vshr_u1_perf_evalu_employee_daily"]);
            ExportGridToExcel(GvExportExcel_1, "แบบประเมินผลการปฏิบัติงานระดับพนักงานรายวัน", "แบบประเมินผลการปฏิบัติงานระดับพนักงานรายวัน");
        }
        else
        {
            _func_dmu.zSetGridData(GvExportExcel, ViewState["vshr_u1_perf_evalu_employee_daily"]);
            ExportGridToExcel(GvExportExcel, "แบบประเมินผลการปฏิบัติงานระดับพนักงานรายวัน", "แบบประเมินผลการปฏิบัติงานระดับพนักงานรายวัน");
        }
    }

    protected void grvMergeHeader_RowCreated(object sender, GridViewRowEventArgs e)
    {
        var gridViewName = (GridView)sender;
        if (e.Row.RowType == DataControlRowType.Header)
        {
            //GridView HeaderGrid = (GridView)sender;
            //GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
            //TableCell HeaderCell = new TableCell();
            //HeaderCell.Text = "Department";
            //HeaderCell.ColumnSpan = 5;
            //HeaderGridRow.Cells.Add(HeaderCell);

            //HeaderCell = new TableCell();
            //HeaderCell.Text = "Employee";
            //HeaderCell.ColumnSpan = 2;
            //HeaderGridRow.Cells.Add(HeaderCell);

            //gridViewName.Controls[0].Controls.AddAt(0, HeaderGridRow);
        }
    }

    private void zUnconfirm(int id)
    {
        if (_func_dmu.zStringToInt(ViewState["admin_idx"].ToString()) == 1)
        {
            _data_hr_evaluation.hr_perf_evalu_emp_action = new hr_perf_evalu_emp[1];
            hr_perf_evalu_emp select_obj = new hr_perf_evalu_emp();
            select_obj.u0idx = id;
            select_obj.operation_status_id = "unconfirm";
            _data_hr_evaluation.hr_perf_evalu_emp_action[0] = select_obj;
            callServicePostPerfEvaluEmp(_urlSetUpdits_u_hr_evaluation, _data_hr_evaluation);

            _data_hr_evaluation.hr_perf_evalu_emp_action = new hr_perf_evalu_emp[1];
            select_obj = new hr_perf_evalu_emp();
            select_obj.u0idx = id;
            select_obj.operation_status_id = "daily_unconfirm";
            _data_hr_evaluation.hr_perf_evalu_emp_action[0] = select_obj;
            callServicePostPerfEvaluEmp(_urlsendEmailhr_evaluation, _data_hr_evaluation);
        }
    }

    public void ImportData()
    {
        FileUpload uploadio = (FileUpload)FvInsertH.FindControl("upload");
        if (uploadio.HasFile)
        {
            string datetimeNow = DateTime.Now.ToString("yyyyMMdd_HHmmss_ffff");
            string FileName = Path.GetFileName(uploadio.PostedFile.FileName);
            string extension = Path.GetExtension(uploadio.PostedFile.FileName);
            string newFileName = datetimeNow + extension.ToLower();
            string folderPath = ConfigurationManager.AppSettings["path_flie_elearning"];
            string filePath = Server.MapPath(folderPath + newFileName);
            if (extension.ToLower() == ".xlsx")//extension.ToLower() == ".xls" || 
            {
                uploadio.SaveAs(filePath);
                GridView gvItemsu1PerfEvaluEmpDaily = (GridView)FvInsert.FindControl("gvItemsu1PerfEvaluEmpDaily");
                // filePath = Server.MapPath(filePath);

                string conStr = String.Empty;
                if (extension.ToLower() == ".xls")
                {
                    conStr = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                }
                else
                {
                    conStr = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                }

                conStr = String.Format(conStr, filePath, "Yes");
                OleDbConnection connExcel = new OleDbConnection(conStr);
                OleDbCommand cmdExcel = new OleDbCommand();
                OleDbDataAdapter oda = new OleDbDataAdapter();
                DataTable dt = new DataTable();

                cmdExcel.Connection = connExcel;
                connExcel.Open();
                DataTable dtExcelSchema;
                dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                string SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                connExcel.Close();
                connExcel.Open();
                cmdExcel.CommandText = "SELECT * From [" + SheetName + "]";
                oda.SelectCommand = cmdExcel;
                oda.Fill(dt);
                connExcel.Close();

                // setObject_Main();
                int itemObj = 0;
                var count_alert_u1 = dt.Rows.Count;
                for (int i = 0; i <= dt.Rows.Count - 1; i++)
                {
                    if (dt.Rows[i][1].ToString().Trim() != String.Empty)
                    {
                        string sEmp_Code = "";
                        int iEmp_Idx = 0;
                        sEmp_Code = dt.Rows[i][1].ToString().Trim();
                        //if (i == 2)
                        //{
                        //    litDebug.Text = dt.Rows[i][0].ToString().Trim() + "-" + dt.Rows[i][38].ToString().Trim();
                        //}
                        //*****************************
                        GridView gvList = (GridView)FvInsert.FindControl("gvItemsu1PerfEvaluEmpDaily");
                        foreach (GridViewRow gridrow in gvList.Rows)
                        {

                            Label lb_emp_idx = (Label)gridrow.FindControl("lb_emp_idx");
                            Label lb_emp_code = (Label)gridrow.FindControl("lb_emp_code");

                            if (lb_emp_code.Text.Trim() == sEmp_Code)
                            {
                                TextBox txt_ownership_qty = (TextBox)gridrow.FindControl("txt_ownership_qty");
                                TextBox txt_improvement_continuously_qty = (TextBox)gridrow.FindControl("txt_improvement_continuously_qty");
                                TextBox txt_commitment_qty = (TextBox)gridrow.FindControl("txt_commitment_qty");

                                txt_ownership_qty.Text = TKNCoreValue_import(dt.Rows[i][10].ToString().Trim());
                                txt_improvement_continuously_qty.Text = TKNCoreValue_import(dt.Rows[i][11].ToString().Trim());
                                txt_commitment_qty.Text = TKNCoreValue_import(dt.Rows[i][12].ToString().Trim());

                                itemObj++;
                                break;

                            }
                        }
                        //*****************************
                    }
                }
                File.Delete(filePath);

                if (itemObj > 0)
                {
                    if (setCheckError(2) == false)
                    {
                        setCalculator();
                        showMsAlert("Import ข้อมูลเสร็จแล้ว");
                    }
                }
                else
                {
                    showMsAlert("ไฟล์ข้อมูลไม่ถูกต้อง !");
                }
            }
            else
            {
                showMsAlert("Import ได้เฉพาะไฟล์ Excel (นามสกุลไฟล์ .xlsx) เท่านั้น");//.xls หรือ 
            }
        }
        else
        {
            showMsAlert("กรุณาเลือกไฟล์ก่อนค่ะ !");
        }
    }

    private string TKNCoreValue_import(string _qty)
    {
        int ierror = 0, qty_return = 0;

        ierror = 0;
        try
        {
            qty_return = _func_dmu.zStringToInt(_qty);
        }
        catch
        {
            ierror = 1;
        }
        if (ierror == 1)
        {
            qty_return = 0;
        }
        else
        {
            if (_func_dmu.zStringToInt(_qty) > 5)
            {
                ierror = 2;
                qty_return = 0;
            }
            else if (_func_dmu.zStringToInt(_qty) < 0)
            {
                ierror = 2;
                qty_return = 0;
            }
        }

        return qty_return.ToString();
    }
}