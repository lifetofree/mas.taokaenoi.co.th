﻿using System;
using System.Xml.Serialization;

/// <summary>
/// Summary description for data_roombooking
/// </summary>
/// 
[Serializable]
[XmlRoot("data_ecom_channel")]
public class data_ecom_channel
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    // --- master data ---//
    [XmlElement("ecom_m0_channel_list")]
    public ecom_m0_channel[] ecom_m0_channel_list { get; set; }

}

[Serializable]
public class ecom_m0_channel
{
    [XmlElement("channel_idx")]
    public int channel_idx { get; set; }

    [XmlElement("channel_name")]
    public string channel_name { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("m0_order_type")]
    public string m0_order_type { get; set; }

    [XmlElement("m0_sale_org")]
    public string m0_sale_org { get; set; }

    [XmlElement("m0_channel")]
    public string m0_channel { get; set; }

    [XmlElement("m0_division")]
    public string m0_division { get; set; }

    [XmlElement("m0_sales_office")]
    public string m0_sales_office { get; set; }

    [XmlElement("m0_sales_group")]
    public string m0_sales_group { get; set; }

    [XmlElement("m0_customer_no")]
    public string m0_customer_no { get; set; }

    [XmlElement("channel_status")]
    public int channel_status { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("emp_idx_update")]
    public int emp_idx_update { get; set; }
    
}


