﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="cims.aspx.cs" Inherits="websystem_qa_cims" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <!--tab menu-->
    <asp:HyperLink ID="setOntop" runat="server"></asp:HyperLink>
    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default" id="divBtnHead" runat="server" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-bar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="sub-navbar">
                        <a class="navbar-brand" href="#"><b>Menu</b></a>
                    </div>
                </div>

                <!--Collect the nav links, forms, and other content for toggling-->
                <div class="collapse navbar-collapse" id="menu-bar">
                    <ul class="nav navbar-nav" id="uiNav" runat="server">
                        <li id="li0" runat="server">
                            <asp:LinkButton ID="lbCreate" runat="server" CommandName="cmdCreate" OnCommand="navCommand" CommandArgument="docCreate"> แจ้งขอบริการสอบเทียบ</asp:LinkButton>
                        </li>
                        <li id="li1" runat="server">
                            <asp:LinkButton ID="lbDetail" runat="server" CommandName="cmdDetailList" OnCommand="navCommand" CommandArgument="docDetailList"> รายการสอบเทียบ</asp:LinkButton>
                        </li>
                        <li id="li2" runat="server">
                            <asp:LinkButton ID="lbdocLab" runat="server" CommandName="cmdLabList" OnCommand="navCommand" CommandArgument="docLab">Lab ปฏิบัติการ</asp:LinkButton>
                        </li>
                        <li id="li3" runat="server">
                            <asp:LinkButton ID="lbdocRecordCalibration" runat="server" CommandName="cmdRecordCalibration" OnCommand="navCommand" CommandArgument="docRecordCalibration">บันทึกผลการสอบเทียบ</asp:LinkButton>
                        </li>
                        <li id="li4" runat="server">
                            <asp:LinkButton ID="lbdocSupervisor" runat="server" CommandName="cmdRecordCalibration" OnCommand="navCommand" CommandArgument="docSupervisor">Supervisor</asp:LinkButton>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
    <!--tab menu-->
    <div class="col-md-12">
        <asp:Literal ID="litDebug" runat="server"></asp:Literal>
        <asp:Literal ID="Literal1" runat="server"></asp:Literal>
        <asp:Literal ID="Testing" runat="server"></asp:Literal>
        <asp:TextBox ID="setActive" Visible="true" runat="server" CssClass="form-control"></asp:TextBox>
    </div>

    <!--multiview-->
    <asp:MultiView ID="mvSystem" runat="server">
        <asp:View ID="docCreate" runat="server">
            <div id="divCreate" runat="server" class="col-md-12">
                <!--รายละเอียดผู้ใช้งาน-->
                <asp:FormView ID="fvEmpDetail" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <ItemTemplate>
                        <asp:HiddenField ID="hfEmpOrgIDX" runat="server" Value='<%# Eval("org_idx") %>' />
                        <asp:HiddenField ID="hfEmpRdeptIDX" runat="server" Value='<%# Eval("rdept_idx") %>' />
                        <asp:HiddenField ID="hfEmpRsecIDX" runat="server" Value='<%# Eval("rsec_idx") %>' />
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายละเอียดข้อมูลผู้ใช้งาน</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">รหัสพนักงาน :</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpCode" runat="server" CssClass="form-control" Text='<%# Eval("emp_code") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">ชื่อ - นามสกุล :</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpName" runat="server" CssClass="form-control" Text='<%# Eval("emp_name_th") %>' Enabled="false" />
                                        </div>
                                        <%-- <label class="col-sm-2 control-label">องค์กร</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpOrg" runat="server" CssClass="form-control" Text='<%# Eval("org_name_th") %>' Enabled="false" />
                                        </div>--%>
                                        <label class="col-sm-2 control-label">ฝ่าย :</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpDept" runat="server" CssClass="form-control" Text='<%# Eval("dept_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">แผนก :</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpSec" runat="server" CssClass="form-control" Text='<%# Eval("sec_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ตำแหน่ง :</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpPos" runat="server" CssClass="form-control" Text='<%# Eval("pos_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </ItemTemplate>

                </asp:FormView>

                <asp:FormView ID="fvDetailsDocument" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <ItemTemplate>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายละเอียดรายการ</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">ผู้ทำรายการ :</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbDetailsEmpCreate" runat="server" CssClass="form-control" Text='<%# Eval("emp_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ฝ่าย :</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbDetailsDeptCreate" runat="server" CssClass="form-control" Text='<%# Eval("dept_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">แผนก :</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbDetailsSecCreate" runat="server" CssClass="form-control" Text='<%# Eval("sec_name_th") %>' Enabled="false" />
                                        </div>

                                        <label class="col-sm-2 control-label">ตำแหน่ง :</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbDetailsPosCreate" runat="server" CssClass="form-control" Text='<%# Eval("pos_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">แผนกผู้ถือครอง :</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbDetailsSectionHolder" runat="server" CssClass="form-control" Text='<%# Eval("sec_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ประเภท :</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbDetailsEquipmentType" runat="server" CssClass="form-control" Text='<%# Eval("equipment_type_name") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbtypeEquipment" runat="server" Visible="false" CssClass="form-control" Text='<%# Eval("equipment_type") %>' Enabled="false" />
                                            <asp:TextBox ID="tbplaceidx" runat="server" Visible="false" CssClass="form-control" Text='<%# Eval("place_idx") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-10">
                                            <asp:LinkButton ID="lbDocCancelDoc" CssClass="btn btn-default" runat="server" data-original-title="Back" data-toggle="tooltip" OnCommand="btnCommand" CommandArgument="0" CommandName="cmdDocCancel"><i class="fa fa-reply" aria-hidden="true"></i> Back</asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </ItemTemplate>

                </asp:FormView>
                <!-- บันทึกใบ certificate -->
                <asp:TextBox ID="tbNodeDecisionIDX" runat="server" CssClass="form-control" Visible="false" Enabled="false"></asp:TextBox>
                <asp:FormView ID="fvRecordCertificate" runat="server" Width="100%">
                    <ItemTemplate>
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title">บันทึกใบ Certificate</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <asp:UpdatePanel ID="updatepanelFileCertificate" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="form-group">
                                                <div class="col-sm-2 text_right small">
                                                    <b>ID No.</b>
                                                    <p class="list-group-item-text">รหัสเครื่องมือ</p>
                                                </div>
                                                <div class="col-sm-4">
                                                    <asp:TextBox ID="tbIDCodeEquipmentView" runat="server" CssClass="form-control" Text='<%# Eval("device_id_no") %>' Enabled="false"></asp:TextBox>
                                                    <asp:TextBox ID="tbM0NodeIDXCer" runat="server" CssClass="form-control" Text='<%# Eval("m0_node_idx") %>' Enabled="false"></asp:TextBox>
                                                    <asp:TextBox ID="tbM0ActorIDXCer" runat="server" CssClass="form-control" Text='<%# Eval("m0_actor_idx") %>' Enabled="false"></asp:TextBox>
                                                    <asp:TextBox ID="tbU1CalIDXCer" runat="server" CssClass="form-control" Text='<%# Eval("u1_cal_idx") %>' Enabled="false"></asp:TextBox>
                                                    <asp:TextBox ID="tbU0CalIDXCer" runat="server" CssClass="form-control" Text='<%# Eval("u0_cal_idx") %>' Enabled="false"></asp:TextBox>
                                                </div>
                                                <div class="col-sm-2 text_right small">
                                                    <b>Machine Type</b>
                                                    <p class="list-group-item-text">ประเภทเครื่องมือ</p>
                                                </div>
                                                <div class="col-sm-4">
                                                    <asp:TextBox ID="tbEquipmentTypeView" runat="server" Text='<%# Eval("equipment_type_name") %>' CssClass="form-control" Enabled="false"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-2 text_right small">
                                                    <b>Machine Name</b>
                                                    <p class="list-group-item-text">ชื่อเครื่องมือ/อุปกรณ์</p>
                                                </div>
                                                <div class="col-sm-4">
                                                    <asp:TextBox ID="tbEquipmentNameView" runat="server" Text='<%# Eval("equipment_name") %>' CssClass="form-control" Enabled="false"></asp:TextBox>
                                                </div>
                                                <div class="col-sm-2 text_right small">
                                                    <b>Brand</b>
                                                    <p class="list-group-item-text">ยี่ห้อ/รุ่น</p>
                                                </div>
                                                <div class="col-sm-4">
                                                    <asp:TextBox ID="tbEquipmentBrandNameView" runat="server" Text='<%# Eval("brand_name") %>' CssClass="form-control" Enabled="false"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-2 text_right small">
                                                    <b>Serial No.</b>
                                                    <p class="list-group-item-text">หมายเลขเครื่อง </p>
                                                </div>
                                                <div class="col-sm-4">
                                                    <asp:TextBox ID="tbSerailNumberView" runat="server" CssClass="form-control" Text='<%# Eval("device_serial") %>' Enabled="false"></asp:TextBox>
                                                </div>
                                                <div class="col-sm-2 text_right small">
                                                    <b>Section Holder</b>
                                                    <p class="list-group-item-text">แผนกผู้ถือครอง</p>
                                                </div>
                                                <div class="col-sm-4">
                                                    <asp:TextBox ID="tbSectionHolderView" runat="server" CssClass="form-control" Text='<%# Eval("device_rsec") %>' Enabled="false"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-2 text_right small">
                                                    <b>Upload File</b>
                                                    <p class="list-group-item-text">แนบไฟล์ </p>
                                                </div>
                                                <div class="col-sm-10">
                                                    <asp:FileUpload ID="UploadCertificate" runat="server" /><br />
                                                    <p style="color: red">**แนบไฟล์ Certificate ขนาดไม่เกิน 5 MB. (นามสกุล .jpg|.png|.pdf) </p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-2"></div>
                                                <div class="col-sm-10">

                                                    <asp:LinkButton ID="lnkbtnSaveCertificate" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                        Text="บันทึก" OnCommand="btnCommand" ValidationGroup="SelectTypegroup" CommandName="cmdSaveCertificate"></asp:LinkButton>
                                                    <asp:LinkButton ID="lnkbtnCancelCertificate" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                        Text="ยกเลิก" OnCommand="btnCommand" CommandArgument="0" CommandName="cmdDocCancel"></asp:LinkButton>

                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="lnkbtnSaveCertificate" />
                                            <asp:PostBackTrigger ControlID="lnkbtnCancelCertificate" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:FormView>


                <!--รายการเครื่องมือสอบเทียบ"  -->
                <asp:FormView ID="fvCalDocumentList" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <ItemTemplate>
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายการเครื่องมือที่สอบเทียบ</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <asp:GridView ID="gvCalibrationList"
                                        runat="server"
                                        AutoGenerateColumns="false" HeaderStyle-CssClass="info"
                                        OnRowDeleting="gvRowDeleted"
                                        OnRowDataBound="gvRowDataBound"
                                        RowStyle-Wrap="true"
                                        CssClass="table table-striped table-bordered table-responsive">
                                        <HeaderStyle CssClass="info" Height="30px" Font-Size="Small" />
                                        <RowStyle Font-Size="Small" />
                                        <EmptyDataTemplate>
                                            <div style="text-align: center">ไม่พบข้อมูล</div>
                                        </EmptyDataTemplate>
                                        <Columns>
                                            <asp:TemplateField HeaderText="No." HeaderStyle-CssClass="text-left" HeaderStyle-Width="1%" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblU1DocCal" Visible="false" runat="server" Text='<%# Eval("u1_cal_idx")%>'></asp:Label>
                                                    <asp:Label ID="lblm0NodeIDX" Visible="false" runat="server" Text='<%# Eval("m0_node_idx")%>'></asp:Label>
                                                    <asp:Label ID="lblm0DeviveIDX" Visible="false" runat="server" Text='<%# Eval("m0_device_idx")%>'></asp:Label>
                                                    <%# (Container.DataItemIndex +1) %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Machine Name" HeaderStyle-Width="9%" HeaderStyle-CssClass="text-left" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblequipment_name" runat="server" Text='<%# Eval("equipment_name")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ID No." HeaderStyle-Width="8%" HeaderStyle-CssClass="text-left" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbldevice_id_no" runat="server" Text='<%# Eval("device_id_no")%>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Serial No." HeaderStyle-Width="10%" HeaderStyle-CssClass="text-left" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbldevice_serial" runat="server" Text='<%# Eval("device_serial")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Last Cal Date" HeaderStyle-Width="5%" HeaderStyle-CssClass="text-left" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLastCalDate" runat="server" Text='<%# Eval("received_calibration_date")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Received Date" HeaderStyle-Width="5%" HeaderStyle-CssClass="text-left" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblreceived_calibration_date" runat="server" Text='<%# Eval("received_calibration_date")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Cal Date" HeaderStyle-Width="5%" HeaderStyle-CssClass="text-left" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Lab" HeaderStyle-Width="15%" HeaderStyle-CssClass="text-left" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>

                                                    <asp:Panel ID="pnlChooseLocationCal" runat="server" Visible="true" Width="100%">
                                                        <div class="panel-body">
                                                            <div class="form-horizontal" role="form">
                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <asp:DropDownList ID="ddlChooseLabType" runat="server" CssClass="form-control"
                                                                            OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" />
                                                                        <asp:RequiredFieldValidator ID="RequiredddlChooseLabType" runat="server" InitialValue="0"
                                                                            ControlToValidate="ddlChooseLabType" Display="None" SetFocusOnError="true"
                                                                            ErrorMessage="*กรุณาเลือกประเภทการสอบเทียบ" ValidationGroup="SelectTypegroup" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorddlChooseLabType" runat="Server"
                                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredddlChooseLabType" Width="250" />
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">

                                                                    <div class="col-sm-12">
                                                                        <asp:DropDownList ID="ddlChooseLocation" Visible="true" Enabled="true" CssClass="form-control" runat="server" />
                                                                        <asp:RequiredFieldValidator ID="RequiredddlChooseLocation" runat="server" InitialValue="0"
                                                                            ControlToValidate="ddlChooseLocation" Display="None" SetFocusOnError="true"
                                                                            ErrorMessage="*กรุณาเลือกประเภทแลปสอบเทียบ" ValidationGroup="SelectTypegroup" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorddlChooseLocation" runat="Server"
                                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredddlChooseLocation" Width="250" />
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">

                                                                    <div class="col-sm-12">
                                                                        <asp:TextBox ID="txtCommentBeforeExtanalLab" Visible="false" CssClass="col-sm-12 form-control"
                                                                            placeholder="กรอกความคิดเห็น ..." runat="server" />
                                                                        <asp:TextBox ID="tet" Visible="false" CssClass="col-sm-12 form-control"
                                                                            placeholder="กรอกความคิดเห็น ..." runat="server" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </asp:Panel>


                                                    <asp:Label ID="lblM0Lab" runat="server" Visible="false" Text='<%# Eval("m0_lab_idx")%>'></asp:Label>
                                                    <asp:Label ID="lblM0LabName" runat="server" Text='<%# Eval("lab_name")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="certificate" HeaderStyle-Width="5%" HeaderStyle-CssClass="text-left" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblcertificate" runat="server" Visible="true" Text='<%# Eval("certificate")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Status" HeaderStyle-Width="10%" HeaderStyle-CssClass="text-left" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblstaidx" runat="server" Visible="false" Text='<%# Eval("staidx")%>'></asp:Label>
                                                    <asp:Label ID="lblstaidxName" runat="server" Text='<%# Eval("status_name")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Action" HeaderStyle-Width="5%" HeaderStyle-CssClass="text-left" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:UpdatePanel ID="updatepanelView" runat="server">
                                                        <ContentTemplate>
                                                            <asp:LinkButton ID="btnSaveCertificate" Visible="false" CssClass="btn-sm btn-success" runat="server" data-original-title="Record Certificate" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdView"
                                                                CommandArgument='<%# Eval("u0_cal_idx") + "," + "2" + "," + Eval("equipment_type") + "," + Eval("certificate") + "," +  Eval("u1_cal_idx") + "," +  Eval("staidx") + "," +  Eval("place_idx") + "," +  Eval("m0_node_idx") %>'><i class="fa fa-pencil" aria-hidden="true"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnViewDocCal" Visible="false" CssClass="btn-sm btn-info" runat="server" data-original-title="View" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdView"
                                                                CommandArgument='<%# Eval("u0_cal_idx") + "," + "2" + "," + Eval("equipment_type") + "," + Eval("certificate") + "," +  Eval("u1_cal_idx") + "," +  Eval("staidx") + "," +  Eval("place_idx") + "," +  Eval("m0_node_idx") %>'><i class="fa fa-file-text-o"></i></asp:LinkButton>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="btnSaveCertificate" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    <asp:Panel ID="pnlAction" runat="server" CssClass="pull-right" Visible="true">
                                        <asp:LinkButton ID="lnkbtnSave" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip"
                                            Text="บันทึก" OnCommand="btnCommand" ValidationGroup="SelectTypegroup" CommandName="cmdDocSave"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkbtnCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                            Text="ยกเลิก" OnCommand="btnCommand" CommandArgument="0" CommandName="cmdDocCancel"></asp:LinkButton>
                                    </asp:Panel>

                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:FormView>

            </div>

            <!--ผลรายการเครื่องมือสอบเทียบ"  -->
            <div id="divresult" class="col-md-12" runat="server">
                <asp:FormView ID="fvCalibrationResult" runat="server" Width="100%">
                    <ItemTemplate>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">ผลการสอบเทียบเครื่องมือ</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <asp:Label ID="lblM0NodeIdxView" runat="server" Visible="false" Text='<%# Eval("m0_node_idx")%>'></asp:Label>
                                    <asp:Label ID="lblM0ActorIdxView" runat="server" Visible="false" Text='<%# Eval("m0_actor_idx")%>'></asp:Label>
                                    <asp:Label ID="lblU1DocIdxView" runat="server" Visible="false" Text='<%# Eval("u1_cal_idx")%>'></asp:Label>

                                    <div style="text-align: center">
                                        <asp:Label ID="lbNameHeaderView" CssClass="text_center" Font-Bold="true" Font-Size="12" runat="server"></asp:Label>
                                        <asp:Label ID="lbNameFormView" CssClass="text_center" Font-Bold="true" Font-Size="12" runat="server"></asp:Label>

                                    </div>
                                    <br />
                                    <hr />
                                    <br />
                                    <asp:Label ID="lbnodeidxview" CssClass="text_center" Font-Bold="true" Font-Size="12" runat="server"></asp:Label>

                                    <!-- Repeater topics result salt -->
                                    <asp:Repeater ID="rptTopicsSaltView" runat="server">
                                        <ItemTemplate>
                                            <div class="form-group">
                                                <div class="col-sm-2">
                                                    <label class="control-label">วันที่ทวนสอบ</label>
                                                </div>
                                                <div class="col-sm-2">

                                                    <asp:TextBox ID="txtresult_salt_view_val1" runat="server" CssClass="form-control" Enabled="false" Text='<%# Eval("u1_val1")%>'></asp:TextBox>
                                                </div>
                                                <div class="col-sm-8"></div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-2">
                                                    <label class="control-label">Salt meter (UUC) รหัส</label>
                                                </div>
                                                <div class="col-sm-2">
                                                    <asp:TextBox ID="txtresult_salt_view_val2" runat="server" CssClass="form-control" Enabled="false" Text='<%# Eval("u1_val2")%>'></asp:TextBox>
                                                </div>
                                                <div class="col-sm-2">
                                                    <label class="control-label">Temperature room</label>
                                                </div>
                                                <div class="col-sm-2">
                                                    <asp:TextBox ID="txtresult_salt_view_val3" runat="server" CssClass="form-control" Enabled="false" Text='<%# Eval("u1_val3") %>'></asp:TextBox>
                                                </div>
                                                <div class="col-sm-2">
                                                    <label class="control-label">Relative Humidity</label>
                                                </div>
                                                <div class="col-sm-2">
                                                    <asp:TextBox ID="txtresult_salt_view_val4" runat="server" CssClass="form-control" Enabled="false" Text='<%# Eval("u1_val4") %>'></asp:TextBox>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>

                                    <!-- GridView result salt -->
                                    <asp:GridView ID="GvSaltView" runat="server" ShowFooter="false"
                                        OnRowDataBound="gvRowDataBound"
                                        HeaderStyle-CssClass="info text_center" Font-Size="8"
                                        AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-responsive"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <HeaderStyle CssClass="info" Width="100%" Height="30px" Font-Size="Small" />
                                        <Columns>
                                            <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                <HeaderTemplate>
                                                    <p>สารละลายความเข้มข้นโซเดียมคลอไรด์</p>
                                                    <p>Reference NaCl Solution (%) (STD)</p>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <asp:TextBox ID="tb_result_salt_val1_view" runat="server" Text='<%# Eval("val1") %>'
                                                                CssClass="form-control" placeholder="" Enabled="false" Visible="true"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderText="1">
                                                <ItemTemplate>
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <asp:TextBox ID="tb_result_salt_val2_view" runat="server" Text='<%# Eval("val2") %>'
                                                                CssClass="form-control" placeholder="" Enabled="false" Visible="true"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderText="2">

                                                <ItemTemplate>
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <asp:TextBox ID="tb_result_salt_val3_view" runat="server" Text='<%# Eval("val3") %>'
                                                                CssClass="form-control" placeholder="" Enabled="false" Visible="true"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderText="3">
                                                <ItemTemplate>
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <asp:TextBox ID="tb_result_salt_val4" runat="server" Text='<%# Eval("val4") %>'
                                                                CssClass="form-control" placeholder="" Enabled="false" Visible="true"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderText="avg">
                                                <ItemTemplate>
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <asp:TextBox ID="tb_result_salt_val5_view" runat="server" Text='<%# Eval("val5") %>'
                                                                CssClass="form-control" placeholder="" Enabled="false" Visible="true"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderText="error">
                                                <ItemTemplate>
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <asp:TextBox ID="tb_result_salt_val6_view" runat="server" Text='<%# Eval("val6") %>'
                                                                CssClass="form-control" placeholder="" Enabled="false" Visible="true"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderText=" ผ่าน/ไม่ผ่าน">
                                                <ItemTemplate>
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <asp:TextBox ID="tb_result_salt_val7_view" runat="server" Text='<%# Eval("val7") %>'
                                                                CssClass="form-control" placeholder="" Enabled="false" Visible="false"></asp:TextBox>
                                                            <asp:RadioButtonList ID="rd_result_salt_val7_view" Enabled="false" runat="server" AutoPostBack="true" RepeatDirection="Horizontal" Width="100%">
                                                                <asp:ListItem Value="1">ผ่าน</asp:ListItem>
                                                                <asp:ListItem Value="0">ไม่ผ่าน</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderText="หมายเหตุ">
                                                <ItemTemplate>
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <asp:TextBox ID="tb_result_salt_val8_view" runat="server" Text='<%# Eval("val8") %>'
                                                                CssClass="form-control" placeholder="" Enabled="false" Visible="true"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                                <%--   <FooterTemplate>
                                            <asp:LinkButton ID="btnInsertRow" CssClass="btn btn-sm btn-success" Text="เพิ่มแถวใหม่" CommandName="InsertRowSalt" data-toggle="tooltip" title="เพิ่มแถวใหม่" runat="server"></asp:LinkButton>
                                        </FooterTemplate>--%>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>

                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:FormView>
            </div>
            <div id="pnActionSupervisorApprove" class="col-md-12" runat="server">

                <asp:FormView ID="fvSupervisorApprove" runat="server" Width="100%">
                    <ItemTemplate>
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title">การดำเนินการอนุมัติเครื่องมือที่สอบเทียบ</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:RadioButtonList ID="rd_supervisor_approve" CellPadding="3" Font-Size="10" CellSpacing="2" Width="15%" AutoPostBack="true" RepeatColumns="2" RepeatLayout="Table" runat="server" RepeatDirection="Horizontal"></asp:RadioButtonList>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txtComment" Visible="true" Rows="3" TextMode="MultiLine" CssClass="form-control" placeholder="กรอกความคิดเห็น / หมายเหตุ ..." runat="server" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:LinkButton ID="btnSupervisorSave" CssClass="btn btn-success" Font-Size="Small" runat="server" CommandName="cmdApproveSupervisor" data-original-title="บันทึก" data-toggle="tooltip" OnCommand="btnCommand" Text="บันทึก" ValidationGroup="Saveinsert1"></asp:LinkButton>
                                            <asp:LinkButton ID="btnSupervisorCancel" CssClass="btn btn-danger" Font-Size="Small" runat="server" CommandName="cmdCancelSupervisor" data-original-title="ยกเลิก" data-toggle="tooltip" OnCommand="btnCommand" Text="ยกเลิก" ValidationGroup="Saveinsert1"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:FormView>
            </div>

            <!--รายการเครื่องมือ OnTextChanged="txtchange"  -->
            <div id="divSelectTypeEquipment" runat="server" class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">รายการเครื่องมือ</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal" role="form">
                            <div class="form-group">
                                <div class="col-sm-2 text_right small">
                                    <b>Place</b>
                                    <p class="list-group-item-text">เลือกสถานที่</p>
                                </div>
                                <div class="col-sm-4">
                                    <asp:DropDownList ID="ddlPlaceCreate" CssClass="form-control" runat="server"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequireddlPlaceCreate" runat="server" InitialValue="0"
                                        ControlToValidate="ddlPlaceCreate" Display="None" SetFocusOnError="true"
                                        ErrorMessage="*กรุณาเลือกสถานที่" ValidationGroup="SelectSearchToolID" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorddlPlaceCreate" runat="Server"
                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequireddlPlaceCreate" Width="250" />
                                </div>
                                <div class="col-sm-6"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label"></label>
                                <div class="col-sm-10">
                                    <asp:RadioButtonList ID="rblTypeEquipment" CellPadding="3" Font-Size="10" CellSpacing="2" Width="50%" AutoPostBack="true" RepeatColumns="2" RepeatLayout="Table"
                                        RepeatDirection="Vertical" OnSelectedIndexChanged="radioCheckChange" runat="server">
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                            <asp:Panel ID="pnEquipmentOld" runat="server" Visible="true">
                                <div class="form-group">
                                    <div class="col-sm-2 text_right small">
                                        <b>Search ID No.</b>
                                        <p class="list-group-item-text">ค้นหาจากรหัสเครื่องมือ</p>
                                    </div>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="tbsearchingToolID" runat="server" placeholder="รหัสเครื่อง.. (ID No.)" Visible="true" CssClass="form-control" />

                                        <asp:RequiredFieldValidator ID="RequiredtbsearchingToolID" ValidationGroup="SelectSearchToolID" runat="server" Display="None"
                                            ControlToValidate="tbsearchingToolID" Font-Size="11"
                                            ErrorMessage="*กรุณากรอกรหัสเครื่องที่ต้องการค้นหา!!!" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender105555" runat="Server" PopupPosition="BottomLeft" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredtbsearchingToolID" Width="250" />

                                    </div>
                                    <div class="col-sm-2">
                                        <div class="pull-left">
                                            <asp:LinkButton ID="btnSearchToolID" CssClass="btn btn-primary" runat="server" ValidationGroup="SelectSearchToolID"
                                                OnCommand="btnCommand" CommandName="cmdSearchTool" Font-Size="Small" data-toggle="tooltip" title="Search"><span class="glyphicon glyphicon-search"></span></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                                <hr />
                            </asp:Panel>

                            <asp:Panel ID="pnEquipmentNew" runat="server" Visible="false">
                                <hr />

                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Organization</b>
                                            <p class="list-group-item-text">องค์กรที่ถือครอง</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlOrg_holder" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" ValidationGroup="saveFormRegis" />
                                            <%-- <asp:RequiredFieldValidator ID="RequiredddlOrg_holder" ValidationGroup="saveFormRegis" runat="server" Display="None"
                                                ControlToValidate="ddlOrg_holder" Font-Size="11" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกองค์กร"
                                                ValidationExpression="*กรุณาเลือกองค์กร" InitialValue="-1" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredddlOrg_holderl" Width="160" PopupPosition="BottomRight" />--%>
                                        </div>
                                        <div class="col-sm-2 text_right small">
                                            <b>Department</b>
                                            <p class="list-group-item-text">ฝ่ายที่ถือครอง</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlDept_holder" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" ValidationGroup="saveFormRegis" />
                                            <%--<asp:RequiredFieldValidator ID="RequiredddlDept_excel" ValidationGroup="saveFormRegis" runat="server" Display="None"
                                                ControlToValidate="ddlDept_excel" Font-Size="11"
                                                ErrorMessage="ฝ่าย"
                                                ValidationExpression="*กรุณาเลือกฝ่าย" InitialValue="-1" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredddlDept_excel" Width="160" PopupPosition="BottomRight" />--%>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Section</b>
                                            <p class="list-group-item-text">แผนกที่ถือครอง</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlSec_holder" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" ValidationGroup="saveFormRegis" />
                                            <%-- <asp:RequiredFieldValidator ID="RequiredddlSec_excel" ValidationGroup="saveFormRegis" runat="server" Display="None"
                                                ControlToValidate="ddlSec_excel" Font-Size="11"
                                                ErrorMessage="*กรุณาเลือกแผนก"
                                                ValidationExpression="*กรุณาเลือกแผนก" InitialValue="-1" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredddlSec_excel" Width="160" PopupPosition="BottomRight" />--%>
                                        </div>
                                        <%--<div class="col-sm-2 text_right small">
                                            <b>Status</b>
                                            <p class="list-group-item-text">สถานะ</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlFormStatus" runat="server" CssClass="form-control" Enabled="true">
                                                <asp:ListItem Text=" Online " Value="1"></asp:ListItem>
                                                <asp:ListItem Text=" Offline " Value="0"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>--%>
                                    </div>


                                    <hr />

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-10">
                                            <asp:RadioButtonList ID="rd_certificate" CellPadding="3" CellSpacing="2" Width="54%" AutoPostBack="true" RepeatColumns="2" Font-Size="10" RepeatLayout="Table"
                                                RepeatDirection="Vertical" OnSelectedIndexChanged="radioCheckChange" runat="server">
                                                <asp:ListItem Value="4" Selected="True">มีใบรับรอง</asp:ListItem>
                                                <asp:ListItem Value="3">ไม่มีใบรับรอง</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>
                                        <%--  <div class="col-sm-6"></div>--%>
                                    </div>

                                    <div class="form-group">
                                        <%--<div class="col-sm-2 text_right small">
                                            <b>Machine Location</b>
                                            <p class="list-group-item-text">สถานที่ของเครื่องมือ</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlPlaceMachine" ValidationGroup="saveFormRegis" runat="server" CssClass="form-control" Enabled="true"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="ReddlPlaceMachine" runat="server" InitialValue="0"
                                                ControlToValidate="ddlPlaceMachine" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกสถานที่ของเครื่องมือ" ValidationGroup="saveFormRegis" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorddlPlaceMachine" runat="Server" PopupPosition="BottomRight"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReddlPlaceMachine" Width="200" />
                                        </div>--%>
                                        <div class="col-sm-2 text_right small">
                                            <b>Calibration Type</b>
                                            <p class="list-group-item-text">ประเภทการสอบเทียบ</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:RequiredFieldValidator ID="Redllcaltype" runat="server" InitialValue="0"
                                                ControlToValidate="dllcaltype" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกประเภทการสอบเทียบ" ValidationGroup="saveFormRegis" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="Valdllcaltype" runat="Server" PopupPosition="BottomRight"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Redllcaltype" Width="200" />
                                            <asp:DropDownList ID="dllcaltype" runat="server" CssClass="form-control" Enabled="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Machine Name</b>
                                            <p class="list-group-item-text">ชื่อเครื่องมือ/อุปกรณ์</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txtinput_machine_name" runat="server" placeholder="กรอกชื่อเครื่องมือ.." Visible="false" CssClass="form-control" />
                                            <asp:DropDownList ID="ddl_machine_name" runat="server" CssClass="form-control"></asp:DropDownList>
                                            <asp:CheckBox ID="chk_other_name_equipment" runat="server" Font-Bold="true" CssClass="small" Text="อื่นๆ (Other)" AutoPostBack="true" OnCheckedChanged="checkindexchange" RepeatDirection="Vertical" />
                                            <asp:RequiredFieldValidator ID="Requireddl_machine_name" runat="server" InitialValue="0"
                                                ControlToValidate="ddl_machine_name" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกชื่อเครื่องมือ/อุปกรณ์" ValidationGroup="addInformation" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="Validatorddl_machine_name" runat="Server" PopupPosition="TopLeft"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requireddl_machine_name" Width="250" />
                                        </div>
                                        <div class="col-sm-2 text_right small">
                                            <b>Brand</b>
                                            <p class="list-group-item-text">ยี่ห้อ/รุ่น</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txtinput_machine_brand" runat="server" placeholder="กรอกชื่อยี่ห้อ.." Visible="false" CssClass="form-control" />
                                            <asp:DropDownList ID="ddl_machine_brand" runat="server" CssClass="form-control"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="Requiredddl_machine_brand" runat="server" InitialValue="0"
                                                ControlToValidate="ddl_machine_brand" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกยี่ห้อ/รุ่น" ValidationGroup="addInformation" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="Validatorddl_machine_brand" runat="Server" PopupPosition="TopLeft"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredddl_machine_brand" Width="250" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>ID No.</b>
                                            <p class="list-group-item-text">รหัสเครื่องมือ</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbIDNo" runat="server" placeholder="กรอกรหัสเครื่อง.." CssClass="form-control" />
                                            <asp:RequiredFieldValidator ID="RequiredtbIDNo" runat="server"
                                                ControlToValidate="tbIDNo" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกรหัสเครื่องมือ" ValidationGroup="addInformation" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatortbIDNo" runat="Server" PopupPosition="TopLeft"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredtbIDNo" Width="250" />
                                        </div>

                                        <div class="col-sm-2 text_right small">
                                            <b>Serial No.</b>
                                            <p class="list-group-item-text">หมายเลขเครื่อง</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbserailnumber" runat="server" placeholder="กรอกหมายเลขเครื่อง.." CssClass="form-control" />
                                        </div>

                                    </div>

                                    <div class="form-group">

                                        <%--<div class="col-sm-2 text_right small">
                                            <b>Calibration Type</b>
                                            <p class="list-group-item-text">ประเภทการสอบเทียบ</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlCalibrationType" runat="server" CssClass="form-control"></asp:DropDownList>
                                        </div>--%>
                                        <div class="col-sm-2 text_right small">
                                            <b>Machine Type</b>
                                            <p class="list-group-item-text">ประเภทเครื่องมือ</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlmachineType" runat="server" CssClass="form-control" Enabled="true"></asp:DropDownList>
                                        </div>

                                        <div class="col-sm-2 text_right small">
                                            <b>Machine Detail</b>
                                            <p class="list-group-item-text">รายละเอียดเครื่อง</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbdetails_equipment" runat="server" TextMode="MultiLine" Rows="1" placeholder="กรอกรายละเอียดเครื่อง.." CssClass="form-control" />
                                        </div>


                                    </div>

                                    <div class="form-group" id="div_datecalibrate" runat="server" visible="false">
                                        <div class="col-sm-2 text_right small">
                                            <b>Calibration Date</b>
                                            <p class="list-group-item-text">วันที่สอบเทียบครั้งล่าสุด</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class='input-group date'>
                                                <asp:HiddenField ID="HiddenCalDate" runat="server" />
                                                <asp:TextBox ID="txt_cal_date" placeholder="เลือกวันที่สอบเทียบครั้งล่าสุด..." runat="server" CssClass="form-control datetimepicker-filter-perm-from cursor-pointer">
                                                </asp:TextBox>
                                                <span class="input-group-addon show-filter-perm-from-onclick">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                                <asp:RequiredFieldValidator ID="Reqtxt_cal_date" runat="server"
                                                    ControlToValidate="txt_cal_date" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกวันที่สอบเทียบครั้งล่าสุด" ValidationGroup="saveFormRegis" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="Valtxt_cal_date" runat="Server" PopupPosition="BottomRight"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Reqtxt_cal_date" Width="200" />
                                            </div>
                                        </div>
                                        <div class="col-sm-2 text_right small">
                                            <b>Due Date</b>
                                            <p class="list-group-item-text">วันที่สอบเทียบครั้งถัดไป</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class='input-group date'>
                                                <asp:HiddenField ID="HiddenDueDate" runat="server" />
                                                <asp:TextBox ID="txt_due_date" placeholder="เลือกวันที่สอบเทียบครั้งถัดไป..." runat="server" CssClass="form-control datetimepicker-filter-perm-to cursor-pointer">
                                                </asp:TextBox>
                                                <span class="input-group-addon show-filter-perm-to-onclick">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                                <asp:RequiredFieldValidator ID="Reqtxt_due_date" runat="server"
                                                    ControlToValidate="txt_due_date" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกวันที่สอบเทียบครั้งถัดไป" ValidationGroup="saveFormRegis" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="Valtxt_due_date" runat="Server" PopupPosition="BottomRight"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Reqtxt_due_date" Width="200" />
                                            </div>
                                        </div>
                                    </div>
                                    <hr />

                                    <%--ความถี่ในการสอบเทียบ--%>
                                    <div class="form-group">                                      
                                        <div class="col-sm-2 text_right small">
                                            <b>Calibration Frequency</b>
                                            <p class="list-group-item-text">ความถี่ในการสอบเทียบ</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlCalFrequency" runat="server" CssClass="form-control" Enabled="true" AutoPostBack="true" ValidationGroup="saveFormRegis"></asp:DropDownList>

                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldddlCalFrequency" ValidationGroup="saveFormRegis" runat="server" Display="None"
                                                ControlToValidate="ddlCalFrequency" Font-Size="11"
                                                ErrorMessage="*กรุณาเลือกความถี่ในการสอบเทียบ"
                                                ValidationExpression="*กรุณาเลือกความถี่ในการสอบเทียบ" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldddlCalFrequency" Width="160" PopupPosition="BottomRight" />--%>
                                        </div>

                                    </div>
                                    <%--ความถี่ในการสอบเทียบ--%>

                                    <%--ค่าอ่านละเอียด--%>
                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Resolution</b>
                                            <p class="list-group-item-text">ค่าอ่านละเอียด</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlResolution" runat="server" CssClass="form-control" Enabled="true"></asp:DropDownList>



                                        </div>
                                        <div class="col-sm-1">
                                            <asp:LinkButton ID="btnAddResolution" CssClass="btn btn-default" data-toggle="tooltip" runat="server" CommandName="btnAddResolution" OnCommand="btnCommand" title="เพิ่มค่าอ่านละเอียด"><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่ม</asp:LinkButton>
                                        </div>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-4 small">
                                            <asp:GridView ID="gvResolution"
                                                runat="server" HeaderStyle-Font-Size="8.5"
                                                AutoGenerateColumns="false"
                                                RowStyle-VerticalAlign="NotSet" HeaderStyle-CssClass="info"
                                                OnRowDeleting="gvRowDeleted"
                                                OnRowDataBound="gvRowDataBound"
                                                RowStyle-Wrap="true"
                                                CssClass="table table-striped table-bordered table-responsive"
                                                HeaderStyle-Height="10px"
                                                ShowFooter="False"
                                                BorderStyle="None">
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">ยังไม่มีข้อมูลค่าอ่านละเอียด</div>
                                                </EmptyDataTemplate>
                                                <Columns>
                                                    <asp:TemplateField HeaderText="ค่าอ่านละเอียด" HeaderStyle-Width="85%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblResolution" runat="server" Text='<%# Eval("Resolution")%>'></asp:Label>
                                                            <asp:Label ID="lblResolutionIDX" runat="server" Visible="false" Text='<%# Eval("ResolutionIDX")%>'></asp:Label>
                                                            <asp:Label ID="lblIDResolution" runat="server" Visible="false" Text='<%# Eval("IDNUMBERResolution")%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate />

                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="ลบ" HeaderStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnDeleteResolution" data-toggle="tooltip" runat="server" title="delete" Text="" CssClass="btn-danger btn-sm small" OnCommand="btnCommand" CommandName="Delete" OnClientClick="return confirm('Do you want delete this item?')"><span class="fa fa-times"></span></asp:LinkButton>
                                                        </ItemTemplate>
                                                        <EditItemTemplate />

                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>

                                        <div class="col-sm-4 small">
                                            <asp:GridView ID="gvResolutionInsert" Visible="false"
                                                runat="server" RowStyle-Font-Size="9" RowStyle-Height="2"
                                                HeaderStyle-Font-Size="Small"
                                                AutoGenerateColumns="false" RowStyle-VerticalAlign="NotSet" HeaderStyle-CssClass="info"
                                                OnRowDeleting="gvRowDeleted"
                                                OnRowDataBound="gvRowDataBound"
                                                RowStyle-Wrap="true"
                                                CssClass="table table-striped table-bordered table-responsive"
                                                HeaderStyle-Height="20px"
                                                ShowHeaderWhenEmpty="True"
                                                ShowFooter="False"
                                                BorderStyle="None"
                                                CellSpacing="2">
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">ยังไม่มีข้อมูลค่าอ่านละเอียด</div>
                                                </EmptyDataTemplate>
                                                <Columns>
                                                    <asp:TemplateField HeaderText="ค่าอ่านละเอียด" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblResolution1" runat="server" Text='<%# Eval("Resolution_insert")%>'></asp:Label>
                                                            <asp:Label ID="lblResolutionIDX1" runat="server" Text='<%# Eval("ResolutionIDX_insert")%>'></asp:Label>
                                                            <asp:Label ID="lblIDResolution1" runat="server" Text='<%# Eval("IDNUMBERResolution_insert")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ลบ" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <small>
                                                                <asp:LinkButton ID="btnResolution1" runat="server" title="delete" Text="" CssClass="btn-danger btn-sm small" OnCommand="btnCommand" CommandName="Delete" OnClientClick="return confirm('Do you want delete this item?')"><span class="fa fa-times"></span></asp:LinkButton>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>

                                    </div>
                                    <%--ค่าอ่านละเอียด--%>

                                    <%--ช่วงการใช้งาน--%>
                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Range of use</b>
                                            <p class="list-group-item-text">ช่วงการใช้งาน</p>
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:TextBox ID="txtRangestart" runat="server" placeholder="เริ่มต้น.." CssClass="form-control" />
                                            <asp:RequiredFieldValidator ID="RetxtROUstart" runat="server"
                                                ControlToValidate="txtRangeend" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกค่าเริ่มต้นของช่วงการใช้งาน" ValidationGroup="saveFormRegis" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValtxtROUstart" runat="Server" PopupPosition="BottomRight"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RetxtROUstart" Width="200" />
                                        </div>

                                        <div class="col-sm-1">
                                            <asp:TextBox ID="txtRangeend" runat="server" placeholder="สิ้นสุด.." CssClass="form-control" />
                                            <asp:RequiredFieldValidator ID="RetxtROUend" runat="server"
                                                ControlToValidate="txtRangeend" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกค่าสิ้นสุดของช่วงการใช้งาน" ValidationGroup="saveFormRegis" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValtxtROUend" runat="Server" PopupPosition="BottomRight"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RetxtROUend" Width="200" />
                                        </div>
                                        <div class="col-sm-2">
                                            <asp:RequiredFieldValidator ID="ReddlUnitROU" runat="server" InitialValue="0"
                                                ControlToValidate="ddlUnitRange" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกหน่วยวัด" ValidationGroup="saveFormRegis" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValddlUnitROU" runat="Server" PopupPosition="BottomRight"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReddlUnitROU" Width="180" />
                                            <asp:DropDownList ID="ddlUnitRange" runat="server" CssClass="form-control" Enabled="true">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:LinkButton ID="btnAddRangeOfUse" CssClass="btn btn-default" data-toggle="tooltip" runat="server" CommandName="btnAddRangeOfUse"
                                                OnCommand="btnCommand" title="เพิ่มช่วงการใช้งาน"><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่ม</asp:LinkButton>
                                        </div>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-4 small">
                                            <asp:GridView ID="gvRange"
                                                runat="server" HeaderStyle-Font-Size="8.5"
                                                AutoGenerateColumns="false"
                                                RowStyle-VerticalAlign="NotSet" HeaderStyle-CssClass="info"
                                                OnRowDeleting="gvRowDeleted"
                                                OnRowDataBound="gvRowDataBound"
                                                RowStyle-Wrap="true"
                                                CssClass="table table-striped table-bordered table-responsive"
                                                HeaderStyle-Height="10px"
                                                ShowFooter="False"
                                                BorderStyle="None">
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">ยังไม่มีข้อมูลช่วงการใช้งาน</div>
                                                </EmptyDataTemplate>
                                                <Columns>
                                                    <asp:TemplateField HeaderText="ช่วงการใช้งานเริ่มต้น" HeaderStyle-Width="28%" HeaderStyle-CssClass="text-center" Visible="true" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblRangeStart" runat="server" Text='<%# Eval("RangeStart")%>'></asp:Label>
                                                            <asp:Label ID="lblIDRangeStart" runat="server" Text='<%# Eval("IDNUMBERRange")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="ช่วงการใช้งานสูงสุด" HeaderStyle-Width="28%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblRangeEnd" runat="server" Text='<%# Eval("RangeEnd")%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="หน่วยวัด" HeaderStyle-Width="29%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblRangeUnit" runat="server" Text='<%# Eval("RangeUnit")%>'></asp:Label>
                                                            <asp:Label ID="lblRangeUnitIDX" Visible="false" runat="server" Text='<%# Eval("RangeUnitIDX")%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate />

                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ลบ" HeaderStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnDeleteRange" data-toggle="tooltip" runat="server" title="delete" Text="" CssClass="btn-danger btn-sm small" OnCommand="btnCommand" CommandName="Delete" OnClientClick="return confirm('Do you want delete this item?')"><span class="fa fa-times"></span></asp:LinkButton>
                                                        </ItemTemplate>
                                                        <EditItemTemplate />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>

                                        <div class="col-sm-4 small">
                                            <asp:GridView ID="gvRangeInsert" Visible="false"
                                                runat="server" RowStyle-Font-Size="9" RowStyle-Height="2"
                                                HeaderStyle-Font-Size="Small"
                                                AutoGenerateColumns="false" RowStyle-VerticalAlign="NotSet" HeaderStyle-CssClass="info"
                                                OnRowDeleting="gvRowDeleted"
                                                OnRowDataBound="gvRowDataBound"
                                                RowStyle-Wrap="true"
                                                CssClass="table table-striped table-bordered table-responsive"
                                                HeaderStyle-Height="20px"
                                                ShowHeaderWhenEmpty="True"
                                                ShowFooter="False"
                                                BorderStyle="None"
                                                CellSpacing="2">
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">ยังไม่มีข้อมูลช่วงการใช้งาน</div>
                                                </EmptyDataTemplate>
                                                <Columns>
                                                    <asp:TemplateField HeaderText="ช่วงการใช้งานเริ่มต้น" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblRangeStart1" runat="server" Text='<%# Eval("RangeStart_insert")%>'></asp:Label>
                                                            <asp:Label ID="lblIDRangeStart1" runat="server" Text='<%# Eval("IDNUMBERRange_insert")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ช่วงการใช้งานสูงสุด" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblRangeEnd1" runat="server" Text='<%# Eval("RangeEnd_insert")%>'></asp:Label>
                                                            <%--<asp:Label ID="lblIDAcceptance1" runat="server" Text='<%# Eval("IDNUMBERMeasuringEnd_insert")%>'></asp:Label>--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="หน่วย" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblRangeUnitIDX1" Visible="false" runat="server" Text='<%# Eval("RangeUnitIDX_insert")%>'></asp:Label>
                                                            <asp:Label ID="lblRangeUnit1" runat="server" Text='<%# Eval("RangeUnit_insert")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="ลบ" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <small>
                                                                <asp:LinkButton ID="btnRange1" runat="server" title="delete" Text="" CssClass="btn-danger btn-sm small" OnCommand="btnCommand" CommandName="Delete" OnClientClick="return confirm('Do you want delete this item?')"><span class="fa fa-times"></span></asp:LinkButton>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                    <%--ช่วงการใช้งาน--%>

                                    <%--ช่วงการวัด/พิสัย--%>
                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Measuring Range</b>
                                            <p class="list-group-item-text">ช่วงการวัด/พิสัย</p>
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:TextBox ID="txtMeasuringstart" runat="server" placeholder="เริ่มต้น.." CssClass="form-control" />
                                            <asp:RequiredFieldValidator ID="RetxtMRstart" runat="server"
                                                ControlToValidate="txtMeasuringstart" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกค่าเริ่มต้นของช่วงการวัด/พิสัย" ValidationGroup="saveFormRegis" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValtxtMRstart" runat="Server" PopupPosition="BottomRight"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RetxtMRstart" Width="200" />
                                        </div>

                                        <div class="col-sm-1">
                                            <asp:TextBox ID="txtMeasuringend" runat="server" placeholder="สิ้นสุด.." CssClass="form-control" />
                                            <asp:RequiredFieldValidator ID="RetxtMRend" runat="server"
                                                ControlToValidate="txtMeasuringend" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกค่าสิ้นสุดของช่วงการวัด/พิสัย" ValidationGroup="saveFormRegis" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValtxtMRend" runat="Server" PopupPosition="BottomRight"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RetxtMRend" Width="200" />
                                        </div>

                                        <div class="col-sm-2">
                                            <asp:RequiredFieldValidator ID="ReddlUnitMeasuring" runat="server" InitialValue="0"
                                                ControlToValidate="ddlUnitMeasuring" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกหน่วยวัด" ValidationGroup="saveFormRegis" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValddlUnitMR" runat="Server" PopupPosition="BottomRight"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReddlUnitMeasuring" Width="180" />
                                            <asp:DropDownList ID="ddlUnitMeasuring" runat="server" CssClass="form-control" Enabled="true">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:LinkButton ID="btnAddMeasuring" CssClass="btn btn-default" data-toggle="tooltip" runat="server" CommandName="btnAddMeasuring" OnCommand="btnCommand" title="เพิ่มช่วงการวัด/พิสัย"><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่ม</asp:LinkButton>
                                        </div>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-4 small">
                                            <asp:GridView ID="gvMeasuring"
                                                runat="server" HeaderStyle-Font-Size="8.5"
                                                AutoGenerateColumns="false"
                                                RowStyle-VerticalAlign="NotSet" HeaderStyle-CssClass="info"
                                                OnRowDeleting="gvRowDeleted"
                                                OnRowDataBound="gvRowDataBound"
                                                RowStyle-Wrap="true"
                                                CssClass="table table-striped table-bordered table-responsive"
                                                HeaderStyle-Height="10px"
                                                ShowFooter="False"
                                                BorderStyle="None">
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">ยังไม่มีข้อมูลช่วงการวัด/พิสัย</div>
                                                </EmptyDataTemplate>
                                                <Columns>
                                                    <asp:TemplateField HeaderText="ช่วงการวัดเริ่มต้น" HeaderStyle-Width="28%" HeaderStyle-CssClass="text-center" Visible="true" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblMeasuringStart" runat="server" Text='<%# Eval("MeasuringStart")%>'></asp:Label>
                                                            <asp:Label ID="lblIDNUMBERMeasuring" runat="server" Text='<%# Eval("IDNUMBERMeasuring")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="ช่วงการวัดสูงสุด" HeaderStyle-Width="28%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblMeasuringEnd" runat="server" Text='<%# Eval("MeasuringEnd")%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="หน่วยวัด" HeaderStyle-Width="29%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblMeasuringUnit" runat="server" Text='<%# Eval("MeasuringUnit")%>'></asp:Label>
                                                            <asp:Label ID="lblMeasuringUnitIDX" Visible="false" runat="server" Text='<%# Eval("MeasuringUnitIDX")%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ลบ" HeaderStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnDeleteMeasuring" data-toggle="tooltip" runat="server" title="delete" Text="" CssClass="btn-danger btn-sm small" OnCommand="btnCommand" CommandName="Delete" OnClientClick="return confirm('Do you want delete this item?')"><span class="fa fa-times"></span></asp:LinkButton>
                                                        </ItemTemplate>
                                                        <EditItemTemplate />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>

                                        <div class="col-sm-4 small">
                                            <asp:GridView ID="gvMeasuringInsert" Visible="false"
                                                runat="server" RowStyle-Font-Size="9" RowStyle-Height="2"
                                                HeaderStyle-Font-Size="Small"
                                                AutoGenerateColumns="false" RowStyle-VerticalAlign="NotSet" HeaderStyle-CssClass="info"
                                                OnRowDeleting="gvRowDeleted"
                                                OnRowDataBound="gvRowDataBound"
                                                RowStyle-Wrap="true"
                                                CssClass="table table-striped table-bordered table-responsive"
                                                HeaderStyle-Height="20px"
                                                ShowHeaderWhenEmpty="True"
                                                ShowFooter="False"
                                                BorderStyle="None"
                                                CellSpacing="2">
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">ยังไม่มีข้อมูลช่วงการวัด/พิสัย</div>
                                                </EmptyDataTemplate>
                                                <Columns>
                                                    <asp:TemplateField HeaderText="ช่วงการวัดเริ่มต้น" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblMeasuringStart1" runat="server" Text='<%# Eval("MeasuringStart_insert")%>'></asp:Label>
                                                            <asp:Label ID="lblIDMeasuringStart1" runat="server" Text='<%# Eval("IDNUMBERMeasuring_insert")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ช่วงการวัดสิ้นสุด" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblMeasuringEnd1" runat="server" Text='<%# Eval("MeasuringEnd_insert")%>'></asp:Label>
                                                            <%--<asp:Label ID="lblIDAcceptance1" runat="server" Text='<%# Eval("IDNUMBERMeasuringEnd_insert")%>'></asp:Label>--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="หน่วย" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblAcceptanceUnitIDX1" Visible="false" runat="server" Text='<%# Eval("MeasuringUnitIDX_insert")%>'></asp:Label>
                                                            <asp:Label ID="lblAcceptanceUnit1" runat="server" Text='<%# Eval("MeasuringUnit_insert")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="ลบ" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <small>
                                                                <asp:LinkButton ID="btnMeasuring1" runat="server" title="delete" Text="" CssClass="btn-danger btn-sm small" OnCommand="btnCommand" CommandName="Delete" OnClientClick="return confirm('Do you want delete this item?')"><span class="fa fa-times"></span></asp:LinkButton>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                    <%--ช่วงการวัด/พิสัย--%>

                                    <%--เกณฑ์การยอมรับ--%>
                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Acceptance Criteria</b>
                                            <p class="list-group-item-text">เกณฑ์การยอมรับ</p>
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:TextBox ID="txtdefaultvalueAC" runat="server" Text="+-" Style="text-align: center" Enabled="false" CssClass="form-control" />
                                        </div>

                                        <div class="col-sm-1">
                                            <asp:TextBox ID="txtAcceptance" runat="server" placeholder="ใส่ค่า.." CssClass="form-control" />
                                            <asp:RequiredFieldValidator ID="RettxtAcceptance" runat="server"
                                                ControlToValidate="txtAcceptance" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกค่าเกณฑ์การยอมรับ" ValidationGroup="saveFormRegis" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValtxtAcceptance" runat="Server" PopupPosition="BottomRight"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RettxtAcceptance" Width="200" />
                                        </div>
                                        <div class="col-sm-2">
                                            <%-- <asp:RequiredFieldValidator ID="ReddlUnitAC" runat="server" InitialValue="0"
                                                ControlToValidate="ddlUnitAC" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกหน่วยวัด" ValidationGroup="saveFormRegis" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValddlUnitAC" runat="Server" PopupPosition="BottomRight"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReddlUnitAC" Width="180" />--%>
                                            <asp:DropDownList ID="ddlUnitAcceptance" runat="server" CssClass="form-control" Enabled="true">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:LinkButton ID="btnAddAcceptance" CssClass="btn btn-default" data-toggle="tooltip" runat="server" CommandName="btnAddAcceptance" OnCommand="btnCommand" title="เพิ่มค่าเกณฑ์การยอมรับ"><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่ม</asp:LinkButton>
                                        </div>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-4 small">
                                            <asp:GridView ID="gvAcceptance"
                                                runat="server" HeaderStyle-Font-Size="8.5"
                                                AutoGenerateColumns="false"
                                                RowStyle-VerticalAlign="NotSet" HeaderStyle-CssClass="info"
                                                OnRowDeleting="gvRowDeleted"
                                                OnRowDataBound="gvRowDataBound"
                                                RowStyle-Wrap="true"
                                                CssClass="table table-striped table-bordered table-responsive"
                                                HeaderStyle-Height="10px"
                                                ShowFooter="False"
                                                BorderStyle="None">
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">ยังไม่มีข้อมูลเกณฑ์การยอมรับ</div>
                                                </EmptyDataTemplate>
                                                <Columns>
                                                    <asp:TemplateField HeaderText="เกณฑ์การยอมรับ" HeaderStyle-Width="45%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblAcceptance" runat="server" Text='<%# "+-" + " " + Eval("Acceptance")%>'></asp:Label>
                                                            <asp:Label ID="lblIDNumberAcceptance" runat="server" Text='<%# Eval("IDNUMBERAcceptance")%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="หน่วยวัด" HeaderStyle-Width="40%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblAcceptanceUnit" runat="server" Text='<%# Eval("AcceptanceUnit")%>'></asp:Label>
                                                            <asp:Label ID="lblAcceptanceUnitIDX" Visible="false" runat="server" Text='<%# Eval("AcceptanceUnitIDX")%>'></asp:Label>

                                                        </ItemTemplate>
                                                        <EditItemTemplate />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ลบ" HeaderStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <%--  <asp:LinkButton ID="btnDeleteAcceptance" data-toggle="tooltip" runat="server" title="delete" Text="" CssClass="btn-danger btn-sm small" OnCommand="btnCommand" CommandName="Delete" OnClientClick="return confirm('Do you want delete this item?')"><span class="fa fa-times"></span></asp:LinkButton>--%>
                                                            <asp:LinkButton ID="btnDeleteAcceptance" runat="server" title="delete" Text="" CssClass="btn-danger btn-sm small" OnCommand="btnCommand" CommandName="DeleteAcceptance" CommandArgument='<%# Eval("IDNUMBERAcceptance") + "," + "0" + "," + Eval("Acceptance") %>' OnClientClick="return confirm('Do you want delete this item?')"><span class="fa fa-times"></span></asp:LinkButton>
                                                        </ItemTemplate>
                                                        <EditItemTemplate />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>

                                        <div class="col-sm-4 small">
                                            <asp:GridView ID="gvAcceptanceInsert" Visible="false"
                                                runat="server" RowStyle-Font-Size="9" RowStyle-Height="2"
                                                HeaderStyle-Font-Size="Small"
                                                AutoGenerateColumns="false" RowStyle-VerticalAlign="NotSet" HeaderStyle-CssClass="info"
                                                OnRowDeleting="gvRowDeleted"
                                                OnRowDataBound="gvRowDataBound"
                                                RowStyle-Wrap="true"
                                                CssClass="table table-striped table-bordered table-responsive"
                                                HeaderStyle-Height="20px"
                                                ShowHeaderWhenEmpty="True"
                                                ShowFooter="False"
                                                BorderStyle="None"
                                                CellSpacing="2">
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                </EmptyDataTemplate>
                                                <Columns>
                                                    <asp:TemplateField HeaderText="เกณฑ์การยอมรับ" HeaderStyle-CssClass="text-left" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblAcceptance1" runat="server" Text='<%# Eval("Acceptance_insert")%>'></asp:Label>
                                                            <asp:Label ID="lblIDAcceptance1" runat="server" Text='<%# Eval("IDNUMBERAcceptance_insert")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="หน่วย" HeaderStyle-CssClass="text-left" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblAcceptanceUnitIDX1" Visible="false" runat="server" Text='<%# Eval("AcceptanceUnitIDX_insert")%>'></asp:Label>
                                                            <asp:Label ID="lblAcceptanceUnit1" runat="server" Text='<%# Eval("AcceptanceUnit_insert")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-left" ControlStyle-Font-Size="XX-Small" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <small>
                                                                <asp:LinkButton ID="btnDeleteAcceptance1" runat="server" title="delete" Text="" CssClass="btn-danger btn-sm small" OnCommand="btnCommand" CommandName="Delete" OnClientClick="return confirm('Do you want delete this item?')"><span class="fa fa-times"></span></asp:LinkButton>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                    <%--เกณฑ์การยอมรับ--%>

                                    <%--จุดสอบเทียบ--%>
                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Calibration Point</b>
                                            <p class="list-group-item-text">จุดสอบเทียบ</p>
                                        </div>
                                        <div class="col-sm-2">
                                            <asp:TextBox ID="tbcalibration_point" runat="server" placeholder="กรอกค่าจุดสอบเทียบ.." Enabled="true" CssClass="form-control" />
                                        </div>
                                        <div class="col-sm-2">
                                            <asp:DropDownList ID="ddl_calibration_point_unit" runat="server" CssClass="form-control"></asp:DropDownList>
                                        </div>
                                        <div class="col-sm-2">
                                            <asp:LinkButton ID="btnAddCalibrationPoint" CssClass="btn btn-default" data-toggle="tooltip" runat="server" CommandName="cmdAddCalibrationPoint" OnCommand="btnCommand" title="เพิ่มค่าจุดสอบเทียบ"><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่ม</asp:LinkButton>
                                            <%--<asp:LinkButton ID="btnAddCalibrationPoint" CssClass="btn btn-primary" Font-Size="Small" runat="server" CommandName="cmdAddCalibrationPoint" data-original-title="เพิ่มค่าจุดสอบเทียบ" data-toggle="tooltip" OnCommand="btnCommand" Text="เพิ่มจุดสอบเทียบ" ValidationGroup="Saveinsert"></asp:LinkButton>--%>
                                        </div>
                                        <%--<div class="col-sm-4"></div>--%>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-4 small">
                                            <asp:GridView ID="gvCalibrationPoint"
                                                runat="server" HeaderStyle-Font-Size="8.5"
                                                AutoGenerateColumns="false"
                                                RowStyle-VerticalAlign="NotSet" HeaderStyle-CssClass="info"
                                                OnRowDeleting="gvRowDeleted"
                                                OnRowDataBound="gvRowDataBound"
                                                RowStyle-Wrap="true"
                                                CssClass="table table-striped table-bordered table-responsive"
                                                HeaderStyle-Height="10px"
                                                ShowFooter="False"
                                                BorderStyle="None">
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">ยังไม่มีข้อมูลจุดสอบเทียบ</div>
                                                </EmptyDataTemplate>
                                                <Columns>
                                                    <asp:TemplateField HeaderText="ค่าจุดสอบเทียบ" HeaderStyle-Width="45%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCalibrationPoint" runat="server" Text='<%# Eval("CalibrationPoint")%>'></asp:Label>
                                                            <asp:Label ID="lblIDNumber" runat="server" Text='<%# Eval("IDNUMBER")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="หน่วย" HeaderStyle-Width="40%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCalibrationPointUnitIDX" Visible="false" runat="server" Text='<%# Eval("CalibrationPointUnitIDX")%>'></asp:Label>
                                                            <asp:Label ID="lblCalibrationPointUnit" runat="server" Text='<%# Eval("CalibrationPointUnit")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="ลบ" HeaderStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <small>
                                                                <asp:LinkButton ID="btnDeleteCalibrationPoint" runat="server" title="delete" Text="" CssClass="btn-danger btn-sm small" OnCommand="btnCommand" CommandName="DeletePoint" CommandArgument='<%# Eval("IDNUMBER") + "," + "0" + "," + Eval("CalibrationPoint") %>' OnClientClick="return confirm('Do you want delete this item?')"><span class="fa fa-times"></span></asp:LinkButton>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>

                                        <div class="col-sm-4 small">
                                            <asp:GridView ID="gvCalPointInsert" Visible="false"
                                                runat="server" RowStyle-Font-Size="9" RowStyle-Height="2" HeaderStyle-Font-Size="Small"
                                                AutoGenerateColumns="false" RowStyle-VerticalAlign="NotSet" HeaderStyle-CssClass="info"
                                                OnRowDeleting="gvRowDeleted"
                                                OnRowDataBound="gvRowDataBound"
                                                RowStyle-Wrap="true"
                                                CssClass="table table-striped table-bordered table-responsive"
                                                HeaderStyle-Height="20px"
                                                ShowHeaderWhenEmpty="True"
                                                ShowFooter="False"
                                                BorderStyle="None"
                                                CellSpacing="2">
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">ยังไม่มีข้อมูลจุดสอบเทียบ</div>
                                                </EmptyDataTemplate>
                                                <Columns>
                                                    <asp:TemplateField HeaderText="ค่าจุดสอบเทียบ" HeaderStyle-CssClass="text-left" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCalibrationPoint1" runat="server" Text='<%# Eval("CalibrationPoint_insert")%>'></asp:Label>
                                                            <asp:Label ID="lblIDNumber1" runat="server" Text='<%# Eval("IDNUMBER_insert")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="หน่วย" HeaderStyle-CssClass="text-left" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCalibrationPointUnitIDX1" Visible="false" runat="server" Text='<%# Eval("CalibrationPointUnitIDX_insert")%>'></asp:Label>
                                                            <asp:Label ID="lblCalibrationPointUnit1" runat="server" Text='<%# Eval("CalibrationPointUnit_insert")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-left" ControlStyle-Font-Size="XX-Small" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <small>
                                                                <asp:LinkButton ID="btnDeleteCalibrationPoint1" runat="server" title="delete" Text="" CssClass="btn-danger btn-sm small" OnCommand="btnCommand" CommandName="Delete" OnClientClick="return confirm('Do you want delete this item?')"><span class="fa fa-times"></span></asp:LinkButton>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>

                                    <%--<div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>cal. point</b>
                                        </div>
                                        <div class="col-sm-4">
                                        </div>
                                        <div class="col-sm-4">
                                        </div>
                                    </div>--%>
                                    <%--จุดสอบเทียบ--%>


                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small"></div>
                                        <div class="col-sm-10">
                                            <asp:LinkButton ID="btnInsertDetail" Visible="true" CssClass="btn btn-default pull-left" runat="server"
                                                CommandName="cmdInserEquipmentNew" OnCommand="btnCommand" ValidationGroup="Saveinsertdetail"
                                                title="เพิ่มเครื่องมือ">เพิ่มเครื่องมือ</asp:LinkButton>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:GridView ID="gvListEquipmentCalNew"
                                                runat="server" RowStyle-Font-Size="9" RowStyle-Height="2" HeaderStyle-Font-Size="Small"
                                                AutoGenerateColumns="false" RowStyle-VerticalAlign="NotSet" HeaderStyle-CssClass="info"
                                                OnRowDeleting="gvRowDeleted"
                                                OnRowDataBound="gvRowDataBound"
                                                RowStyle-Wrap="true"
                                                CssClass="table table-striped table-bordered table-responsive"
                                                HeaderStyle-Height="25px"
                                                ShowHeaderWhenEmpty="True"
                                                ShowFooter="False"
                                                BorderStyle="None"
                                                CellSpacing="2">
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                </EmptyDataTemplate>
                                                <Columns>
                                                    <asp:TemplateField HeaderText="รหัสเครื่องมือ" HeaderStyle-CssClass="text-left" ControlStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblIDNo" runat="server" Text='<%# Eval("IDCodeNumber_New")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="ชื่อเครื่องมือ/อุปกรณ์" HeaderStyle-CssClass="text-left" ControlStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblEquipmentNameNew" runat="server" Text='<%# Eval("EquipmentName_New")%>'></asp:Label>
                                                            <asp:Label ID="lblEquipmentNameIDX" runat="server" Visible="false" Text='<%# Eval("M0DeviceIDX_New")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="ประเภทเครื่องมือ" HeaderStyle-CssClass="text-left" ControlStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblEquipmentTypeIDXNew" Visible="false" runat="server" Text='<%# Eval("EquipmentTypeIDX_New")%>'></asp:Label>
                                                            <asp:Label ID="lblEquipmentTypeNew" runat="server" Text='<%# Eval("EquipmentType_New")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="ยี่ห้อ/รุ่น" HeaderStyle-CssClass="text-left" ControlStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <small>
                                                                <asp:Label ID="lblM0BrandIDXNew" Visible="false" runat="server" Text='<%# Eval("M0BrandIDX_New")%>'></asp:Label>
                                                                <asp:Label ID="lblBrandNameNew" runat="server" Text='<%# Eval("BrandName_New")%>'></asp:Label>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="หมายเลขเครื่อง" HeaderStyle-CssClass="text-left" ControlStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <small>
                                                                <asp:Label ID="lblSerialNumberNew" runat="server" Text='<%# Eval("SerialNumber_New")%>'></asp:Label>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="ช่วงการใช้งาน" HeaderStyle-CssClass="text-left" ControlStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <small>
                                                                <asp:Label ID="lblRangeOfUseMin" Visible="false" runat="server" Text='<%# Eval("RangeOfUseMin")%>'></asp:Label>
                                                                <asp:Label ID="lblRangeOfUseMax" Visible="false" runat="server" Text='<%# Eval("RangeOfUseMax")%>'></asp:Label>
                                                                <asp:Label ID="lblRangeOfUse" runat="server" Text='<%# Eval("RangeOfUseMin") + "-" + Eval("RangeOfUseMax") + "  " + Eval("RangeOfUseUnitName") %>'></asp:Label>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="ช่วงการวัด" HeaderStyle-CssClass="text-left" ControlStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <small>
                                                                <asp:Label ID="lblMeasuringRangeMin" Visible="false" runat="server" Text='<%# Eval("Measuring_Range_Min")%>'></asp:Label>
                                                                <asp:Label ID="lblMeasuringRangeMax" Visible="false" runat="server" Text='<%# Eval("Measuring_Range_Max")%>'></asp:Label>
                                                                <asp:Label ID="lblMeasuringRange" runat="server" Text='<%# Eval("Measuring_Range_Min") + "-" + Eval("Measuring_Range_Max") + "  " + Eval("Measuring_Range_UnitName") %>'></asp:Label>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="ค่าอ่านละเอียด" HeaderStyle-CssClass="text-left" ControlStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <small>
                                                                <asp:Label ID="lblResolution" Visible="false" runat="server" Text='<%# Eval("Resolution")%>'></asp:Label>
                                                                <asp:Label ID="lblResolutionShow" runat="server" Text='<%# Eval("Resolution") + " " + Eval("Resolution_UnitName") %>'></asp:Label>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="เกณฑ์การยอมรับ" HeaderStyle-CssClass="text-left" ControlStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <small>
                                                                <asp:Label ID="lblAcceptanceCriteria" Visible="false" runat="server" Text='<%# Eval("Acceptance_Criteria")%>'></asp:Label>
                                                                <asp:Label ID="lblAcceptanceCriteriaName" runat="server" Text='<%# Eval("Acceptance_Criteria") + "  " + Eval("Acceptance_Criteria_UnitName") %>'></asp:Label>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="ความถี่ในการสอบเทียบ" HeaderStyle-CssClass="text-left" ControlStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <small>
                                                                <asp:Label ID="lblCalibrationFrequency" Visible="false" runat="server" Text='<%# Eval("Calibration_Frequency")%>'></asp:Label>
                                                                <asp:Label ID="lblCalibrationFrequencyName" runat="server" Text='<%# Eval("Calibration_Frequency") + "  " +  "เดือน/ครั้ง" %>'></asp:Label>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="จุดสอบเทียบ" HeaderStyle-CssClass="text-left" ControlStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <small>
                                                                <asp:Label ID="lblCalibrationPointList" runat="server" Text='<%# Eval("CalibrationPointList") %>'></asp:Label>
                                                                <asp:Label ID="lblCalibrationPointListIDX" runat="server" Text='<%# Eval("CalibrationPointList_IDX")%>'></asp:Label>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="ใบรับรอง" HeaderStyle-CssClass="text-left" ControlStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <small>
                                                                <asp:Label ID="lblCerticicate" runat="server" Visible="true" Text='<%# Eval("Certificate_insert") %>'></asp:Label>
                                                                <asp:Label ID="lblHasCerticicate" Visible="true" runat="server">
                                                                <i class="fa fa-check-circle" style="color:darkgreen;" aria-hidden="true"></i>
                                                                </asp:Label>
                                                                <asp:Label ID="lblNotHasCerticicate" Visible="false" runat="server">
                                                                <i class="fa fa-circle" style="color: red; font-size: 10px;" aria-hidden="true"></i>
                                                                </asp:Label>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-left" ControlStyle-Font-Size="XX-Small" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <small>
                                                                <asp:LinkButton ID="btnDeleteEquipmentNew" runat="server" title="delete" Text="" CssClass="btn-danger btn-sm small" OnCommand="btnCommand" CommandName="DeletePoint" CommandArgument='<%# Eval("IDCodeNumber_New") + "," + "1" + "," + Eval("M0DeviceIDX_New") %>' OnClientClick="return confirm('Do you want delete this item?')"><span class="fa fa-times"></span></asp:LinkButton>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>

                            <div class="alert alert-warning" id="divAlertSearchEquipmentOld" runat="server" visible="false">
                                <button type="button" class="close" data-dismiss="alert">x</button>
                                <strong>ไม่พบรายการเครื่องมือที่ค้นหา ! กรุณาทำการกรอกรหัสเครื่องมือใหม่อีกครั้ง </strong>
                            </div>
                            <!--รายการเครื่องมือเก่าที่ค้นหา-->
                            <asp:FormView ID="fvShowDetailsSearchTool" runat="server" Width="100%">
                                <ItemTemplate>
                                    <div class="form-horizontal" role="form">
                                        <%--  <div class="form-group">
                                            <div class="col-sm-2 text_right small"></div>
                                            <div class="col-sm-10 text_left">
                                                 <b>รายละเอียดเครื่องมือ</b>
                                            </div>
                                         </div>--%>
                                        <div class="form-group">
                                            <asp:TextBox ID="tbM0DeviceIDX" runat="server" Visible="false" CssClass="form-control" Text='<%# Eval("m0_device_idx") %>' Enabled="false"></asp:TextBox>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-2 text_right small">
                                                <b>ID No.</b>
                                                <p class="list-group-item-text">รหัสเครื่องมือ</p>
                                            </div>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbIDCodeEquipment" runat="server" CssClass="form-control" Text='<%# Eval("device_id_no") %>' Enabled="false"></asp:TextBox>

                                            </div>
                                            <div class="col-sm-2 text_right small">
                                                <b>Machine Type</b>
                                                <p class="list-group-item-text">ประเภทเครื่องมือ</p>
                                            </div>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbEquipmentType" runat="server" Text='<%# Eval("equipment_type_name") %>' CssClass="form-control" Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-2 text_right small">
                                                <b>Machine Name</b>
                                                <p class="list-group-item-text">ชื่อเครื่องมือ/อุปกรณ์</p>
                                            </div>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbEquipmentName" runat="server" Text='<%# Eval("equipment_name") %>' CssClass="form-control" Enabled="false"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-2 text_right small">
                                                <b>Brand</b>
                                                <p class="list-group-item-text">ยี่ห้อ/รุ่น</p>
                                            </div>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbEquipmentBrandName" runat="server" Text='<%# Eval("brand_name") %>' CssClass="form-control" Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-2 text_right small">
                                                <b>Serial No.</b>
                                                <p class="list-group-item-text">หมายเลขเครื่อง </p>
                                            </div>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbSerailNumber" runat="server" CssClass="form-control" Text='<%# Eval("device_serial") %>' Enabled="false"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-2 text_right small">
                                                <b>Section Holder</b>
                                                <p class="list-group-item-text">แผนกผู้ถือครอง</p>
                                            </div>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbSectionHolder" runat="server" CssClass="form-control" Text='<%# Eval("device_rsec") %>' Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-2 text_right small">
                                                <b>Cal Date</b>
                                                <p class="list-group-item-text">วันที่สอบเทียบล่าสุด </p>
                                            </div>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbDeviceCalDate" runat="server" CssClass="form-control" Text='<%# Eval("device_cal_date") %>' Enabled="false"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-2 text_right small">
                                                <b>Due date</b>
                                                <p class="list-group-item-text">วันที่สอบเทียบครั้งถัดไป</p>
                                            </div>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbDeviceDueDate" runat="server" CssClass="form-control" Text='<%# Eval("device_due_date") %>' Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-offset-2 col-sm-10">
                                                <asp:LinkButton ID="btnInsertDetail" CssClass="btn btn-default" runat="server"
                                                    CommandName="cmdInsertDetail" OnCommand="btnCommand" ValidationGroup="Saveinsertdetail"
                                                    title="เพิ่มรายการ"><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่ม</asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:FormView>

                            <asp:GridView ID="gvListEquipmentCalOld"
                                runat="server" RowStyle-Font-Size="11" HeaderStyle-Font-Size="Small"
                                AutoGenerateColumns="false" RowStyle-VerticalAlign="NotSet" HeaderStyle-CssClass="info"
                                OnRowDeleting="gvRowDeleted"
                                OnRowDataBound="gvRowDataBound"
                                RowStyle-Wrap="true"
                                CssClass="table table-striped table-bordered table-responsive"
                                HeaderStyle-Height="25px"
                                ShowHeaderWhenEmpty="True"
                                ShowFooter="False"
                                BorderStyle="None"
                                CellSpacing="2">
                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="รหัสเครื่องมือ" HeaderStyle-CssClass="text-left" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblIDCodeNumber" runat="server" Text='<%# Eval("IDCodeNumber")%>'></asp:Label>
                                                <asp:Label ID="lblM0DeviceIdx" runat="server" Visible="false" Text='<%# Eval("M0DeviceIDX")%>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ชื่อเครื่องมือ/อุปกรณ์" HeaderStyle-CssClass="text-left" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblEquipmentName" runat="server" Text='<%# Eval("EquipmentName")%>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ประเภทเครื่องมือ" HeaderStyle-CssClass="text-left" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblEquipmentType" runat="server" Text='<%# Eval("EquipmentType")%>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ยี่ห้อ/รุ่น" HeaderStyle-CssClass="text-left" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblBrandname" runat="server" Text='<%# Eval("BrandName")%>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="หมายเลขเครื่อง" HeaderStyle-CssClass="text-left" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblSerialNumber" runat="server" Text='<%# Eval("SerialNumber")%>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ผู้ถือครอง" HeaderStyle-CssClass="text-left" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblSectionHolder" runat="server" Text='<%# Eval("Holder")%>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="วันที่สอบเทียบล่าสุด" HeaderStyle-CssClass="text-left" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblcalDate" runat="server" Text='<%# Eval("calDate")%>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="วันที่สอบเทียบครั้งถัดไป" HeaderStyle-CssClass="text-left" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblDueDate" runat="server" Text='<%# Eval("DueDate")%>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-left" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <small>
                                                <asp:LinkButton ID="btnDeleteEquipmentOld" runat="server" title="delete" Text="" Font-Size="1px" CssClass="btn btn-danger btn-sm" OnCommand="btnCommand" CommandName="Delete" OnClientClick="return confirm('Do you want delete this item?')"><span class="fa fa-times"></span></asp:LinkButton>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>

                            <div id="divActionSaveCreateDocument" runat="server" visible="false" class="pull-right">
                                <asp:LinkButton ID="btnSaveDocCal" CssClass="btn btn-success" Font-Size="Small" runat="server" CommandName="cmdSaveDocCal" data-original-title="บันทึก" data-toggle="tooltip" OnCommand="btnCommand" Text="บันทึก" ValidationGroup="Saveinsert"></asp:LinkButton>
                                <asp:LinkButton ID="btnCancelDocCal" CssClass="btn btn-danger" Font-Size="Small" runat="server" CommandArgument="0" CommandName="cmdDocCancel" data-original-title="ยกเลิก" data-toggle="tooltip" OnCommand="btnCommand" Text="ยกเลิก" ValidationGroup="Saveinsert"></asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:View>

        <asp:View ID="docDetailList" runat="server">
            <div id="divDocumentCalibration" runat="server" class="col-md-12">

                <asp:GridView ID="gvCalibrationDocument"
                    runat="server"
                    AutoGenerateColumns="false" HeaderStyle-CssClass="info"
                    OnRowDeleting="gvRowDeleted"
                    OnRowDataBound="gvRowDataBound"
                    RowStyle-Wrap="true"
                    CssClass="table table-striped table-bordered table-responsive"
                    OnPageIndexChanging="gvPageIndexChanging"
                    AllowPaging="True"
                    PageSize="10">
                    <HeaderStyle CssClass="info" Height="30px" Font-Size="Small" />
                    <RowStyle Font-Size="Small" />
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่พบข้อมูล</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-left" ControlStyle-Width="1%" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblU0DocCal" Visible="false" runat="server" Text='<%# Eval("u0_cal_idx")%>'></asp:Label>
                                <%# (Container.DataItemIndex +1) %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="วันที่สร้างรายการ" HeaderStyle-CssClass="text-left" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblCreateDateDocument" runat="server" Text='<%# Eval("create_date")%>'></asp:Label>
                                <%-- <asp:Label ID="lblPlaceDocIDX" Visible="false" runat="server" Text='<%# Eval("place_idx")%>'></asp:Label>--%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="เครื่องมือประเภท" HeaderStyle-CssClass="text-left" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblequipmentType" runat="server" Text='<%# Eval("equipment_type_name")%>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="สถานะ" HeaderStyle-CssClass="text-left" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblstaidxDoc" runat="server" Visible="false" Text='<%# Eval("staidx")%>'></asp:Label>
                                <asp:Label ID="lblstatus_name" runat="server" Text='<%# Eval("status_name")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-left" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnViewDocCal" CssClass="btn-sm btn-info" runat="server" CommandName="cmdView" OnCommand="btnCommand"
                                    CommandArgument='<%# Eval("u0_cal_idx") + "," + "1" + "," + Eval("equipment_type") + "," + "0" + "," + Eval("u1_cal_idx") + "," +  Eval("staidx") + "," +  Eval("place_idx") + "," +  Eval("m0_node_idx") %>' data-toggle="tooltip" title="View"><i class="fa fa-file-text-o"></i></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </asp:View>

        <asp:View ID="docLab" runat="server">

            <div id="divCalibrationInLab" runat="server" class="col-md-12">
                <asp:GridView ID="gvCalibrationInLab"
                    runat="server"
                    AutoGenerateColumns="false" HeaderStyle-CssClass="info"
                    OnRowDeleting="gvRowDeleted"
                    OnRowDataBound="gvRowDataBound"
                    RowStyle-Wrap="true" Font-Size="Small"
                    CssClass="table table-striped table-bordered table-responsive">
                    <HeaderStyle CssClass="info" Height="30px" Font-Size="Small" />
                    <RowStyle Font-Size="Small" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่พบข้อมูล</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="No." HeaderStyle-CssClass="text-left" HeaderStyle-Width="1%" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblU1DocCalInLab" Visible="false" runat="server" Text='<%# Eval("u1_cal_idx")%>'></asp:Label>
                                <asp:Label ID="lblU0DocCalInLab" Visible="false" runat="server" Text='<%# Eval("u0_cal_idx")%>'></asp:Label>
                                <asp:Label ID="lblm0NodeIDXInLab" Visible="false" runat="server" Text='<%# Eval("m0_node_idx")%>'></asp:Label>
                                <asp:Label ID="lblm0DeviveIDXInLab" Visible="false" runat="server" Text='<%# Eval("m0_device_idx")%>'></asp:Label>
                                <%# (Container.DataItemIndex +1) %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Machine Name" HeaderStyle-Width="15%" HeaderStyle-CssClass="text-left" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblequipment_nameInLab" runat="server" Text='<%# Eval("equipment_name")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ID No." HeaderStyle-Width="10%" HeaderStyle-CssClass="text-left" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lbldevice_id_noInLab" runat="server" Text='<%# Eval("device_id_no")%>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Serial No." HeaderStyle-Width="10%" HeaderStyle-CssClass="text-left" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lbldevice_serialInLab" runat="server" Text='<%# Eval("device_serial")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Type" HeaderStyle-Width="10%" HeaderStyle-CssClass="text-left" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblTypeEquipmentInlab" runat="server" Text='<%# Eval("equipment_type_name")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lab" HeaderStyle-Width="7%" HeaderStyle-CssClass="text-left" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblM0LabInLab" runat="server" Visible="false" Text='<%# Eval("m0_lab_idx")%>'></asp:Label>
                                <asp:Label ID="lblM0LabNameInLab" runat="server" Text='<%# Eval("lab_name")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status" HeaderStyle-Width="10%" HeaderStyle-CssClass="text-left" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblstaidxInLab" runat="server" Visible="false" Text='<%# Eval("staidx")%>'></asp:Label>
                                <asp:Label ID="lblstaidxInLabname" runat="server" Text='<%# Eval("status_name")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="การดำเนินการ" HeaderStyle-Width="15%" HeaderStyle-CssClass="text-left" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Panel ID="divActionCalibrationInLab" runat="server" Visible="false" class="col-md-12">
                                    <asp:LinkButton ID="lnkbtnSaveInLab" CssClass="btn btn-success" runat="server" data-original-title="รับงาน" data-toggle="tooltip"
                                        CommandArgument='<%# Eval("u0_cal_idx")+ "," + Eval("u1_cal_idx") + "," + "8" + "," + Eval("m0_node_idx") %>'
                                        Text="รับงาน" OnCommand="btnCommand" ValidationGroup="SelectTypegroup" CommandName="cmdDocSaveInLab"></asp:LinkButton>
                                    <asp:LinkButton ID="lnkbtnRejectInLab" CssClass="btn btn-danger" runat="server" data-original-title="ไม่รับงาน" data-toggle="tooltip"
                                        CommandArgument='<%# Eval("u0_cal_idx")+ "," + Eval("u1_cal_idx") + "," + "7" + "," + Eval("m0_node_idx") %>'
                                        Text="ไม่รับงาน" OnCommand="btnCommand" ValidationGroup="SelectTypegroup" CommandName="cmdDocSaveInLab"></asp:LinkButton>
                                </asp:Panel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Action" HeaderStyle-Width="5%" HeaderStyle-CssClass="text-left" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnViewDocCalInLab" Visible="true" CssClass="btn btn-sm btn-info" runat="server" data-original-title="View" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdView"
                                    CommandArgument='<%# Eval("u0_cal_idx") + "," + "2" + "," + Eval("equipment_type") + "," + Eval("certificate") + "," +  Eval("u1_cal_idx") + "," +  Eval("staidx") + "," +  Eval("place_idx") + "," +  Eval("m0_node_idx") %>'><i class="fa fa-file-text-o"></i></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>



            </div>

        </asp:View>

        <asp:View ID="docRecordCalibration" runat="server">
            <div id="divRecordCal" runat="server" class="col-md-12">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">บันทึกผลการสอบเทียบ</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal" role="form">
                            <asp:FormView ID="fvRecordCalResults" runat="server" Width="100%">
                                <InsertItemTemplate>
                                    <contenttemplate>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">รหัสเครื่องมือ :</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="txtSearchEquipmentIDNo" runat="server" MaxLength="10"
                                                        CssClass="form-control" placeholder="ex.MIS 99999" Enabled="true"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredtxtSearchEquipmentIDNo"
                                                        runat="server" ValidationGroup="SearchResult"
                                                        Display="None"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="txtSearchEquipmentIDNo"
                                                        Font-Size="13px" ForeColor="Red"
                                                        ErrorMessage="*กรุณากรอกรหัสเครื่องมือ" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatortxtSearchEquipmentIDNo" runat="Server" HighlightCssClass="validatortxtSearchEquipmentIDNo"
                                                        TargetControlID="RequiredtxtSearchEquipmentIDNo" Width="220" />
                                            </div>
                                            <label class="col-sm-2 control-label">เลือกสถานที่ :</label>
                                            <div class="col-sm-4">
                                                <asp:DropDownList ID="ddlSearchPlaceSaveResult" CssClass="form-control" runat="server"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="requireddlSearchPlaceSaveResult"
                                                        runat="server" ValidationGroup="SearchResult"
                                                        Display="None"
                                                        SetFocusOnError="true" InitialValue="0"
                                                        ControlToValidate="ddlSearchPlaceSaveResult"
                                                        Font-Size="13px" ForeColor="Red"
                                                        ErrorMessage="*กรุณาเลือกสถานที่" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="toolkitddlSearchPlaceSaveResult" runat="Server" PopupPosition="BottomLeft" HighlightCssClass="validatorddlSearchPlaceSaveResult"
                                                        TargetControlID="requireddlSearchPlaceSaveResult" Width="220" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"></label>
                                            <div class="col-sm-10">
                                                <div class="pull-left">
                                                        <asp:LinkButton ID="btnSearchResult" CssClass="btn btn-primary" runat="server" ValidationGroup="SearchResult"
                                                            OnCommand="btnCommand" Font-Size="Small" CommandName="btnSearchResult" data-toggle="tooltip" title="Search"><span class="glyphicon glyphicon-search"></span></asp:LinkButton>
                                                        <asp:LinkButton ID="btnCancelResult" CssClass="btn btn-default" runat="server" OnCommand="btnCommand"
                                                            CommandName="cmdCancelResult" data-toggle="tooltip" Font-Size="Small" title="Cancel"><i class="fa fa-times"></i></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                    </contenttemplate>
                                </InsertItemTemplate>
                            </asp:FormView>

                            <table class="table" style="width: 100%">
                                <asp:FormView ID="fvPassValue" Visible="false" runat="server">
                                    <ItemTemplate>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">รหัสเครื่องมือ</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="txtEquipmentIDNoResult" runat="server" Text='<%# Eval("device_id_no") %>'
                                                    CssClass="form-control" placeholder="" Enabled="false"></asp:TextBox>
                                                <asp:TextBox ID="txtu1DocCal" runat="server" Text='<%# Eval("u1_cal_idx") %>'
                                                    CssClass="form-control" placeholder="" Enabled="false"></asp:TextBox>
                                                <asp:TextBox ID="txtu0DocCal" runat="server" Text='<%# Eval("u0_cal_idx") %>'
                                                    CssClass="form-control" placeholder="" Enabled="false"></asp:TextBox>
                                                <asp:TextBox ID="txtM0Deviceidx" runat="server" Text='<%# Eval("m0_device_idx") %>'
                                                    CssClass="form-control" placeholder="" Enabled="false"></asp:TextBox>
                                                <asp:TextBox ID="txtM0Actoridx" runat="server" Text='<%# Eval("m0_actor_idx") %>'
                                                    CssClass="form-control" placeholder="" Enabled="false"></asp:TextBox>
                                                <asp:TextBox ID="txtM0Labidx" runat="server" Text='<%# Eval("m0_lab_idx") %>'
                                                    CssClass="form-control" placeholder="" Enabled="false"></asp:TextBox>
                                                <asp:TextBox ID="txtM0Nodeidx" runat="server" Text='<%# Eval("m0_node_idx") %>'
                                                    CssClass="form-control" placeholder="" Enabled="false"></asp:TextBox>
                                            </div>
                                            <label class="col-sm-2 control-label">ชื่อเครื่องมือ/อุปกรณ์</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="txtEquipmentNameNoResult" runat="server" Text='<%# Eval("equipment_name") %>'
                                                    CssClass="form-control" placeholder="" Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">หมายเลขเครื่อง</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="txtEquipmentSerialNoResult" runat="server" Text='<%# Eval("device_serial") %>'
                                                    CssClass="form-control" placeholder="" Enabled="false"></asp:TextBox>
                                            </div>
                                            <label class="col-sm-2 control-label">ยี่ห้อ/รุ่น</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="txtBrandNameNoResult" runat="server" Text='<%# Eval("brand_name") %>'
                                                    CssClass="form-control" placeholder="" Enabled="false"></asp:TextBox>
                                                <asp:TextBox ID="txtEquipmentIDXNoResult" runat="server" Text='<%# Eval("equipment_idx") %>'
                                                    CssClass="form-control" placeholder="" Enabled="false" Visible="false"></asp:TextBox>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:FormView>
                            </table>

                            <hr />

                            <div class="alert alert-warning" id="divAlertNotfindEquipment" runat="server" visible="false">
                                <button type="button" class="close" data-dismiss="alert">x</button>
                                <strong>ไม่พบรายการเครื่องมือที่ค้นหา ! กรุณาทำการกรอกรหัสเครื่องมือใหม่อีกครั้งค่ะ </strong>
                            </div>


                            <!-- header form -->
                            <asp:Label ID="lbHeader" CssClass="text_center" Font-Bold="true" Font-Size="12" runat="server"></asp:Label>
                            <asp:Label ID="lbHeaderFormName" CssClass="text_center" Font-Bold="true" Font-Size="12" runat="server"></asp:Label>
                            <br />
                            <!-- topics salt-->
                            <asp:Panel ID="pnValueTopicsSalt" runat="server">
                                <div class="form-group">
                                    <div class="col-sm-2">
                                        <asp:Label ID="lb_u1_val1_idx" Visible="false" CssClass="control-label" runat="server"></asp:Label>
                                        <asp:Label ID="u1_val1_salt" CssClass="control-label" runat="server"></asp:Label>
                                    </div>
                                    <div class="col-sm-2">
                                        <asp:TextBox ID="tb_u1_val1_salt" runat="server" CssClass="form-control cal-date-datepicker" placeholder="Cal. Date" Enabled="true" Visible="true"></asp:TextBox>
                                    </div>
                                    <div class="col-sm-8"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-2">
                                        <asp:Label ID="lb_u1_val2_idx" Visible="false" CssClass="control-label" runat="server"></asp:Label>
                                        <asp:Label ID="u1_val2_salt" CssClass="control-label" runat="server"></asp:Label>
                                    </div>
                                    <div class="col-sm-2">
                                        <asp:TextBox ID="tb_u1_val2_salt" runat="server" CssClass="form-control" placeholder="input..." Enabled="true" Visible="true"></asp:TextBox>
                                    </div>
                                    <div class="col-sm-2">
                                        <asp:Label ID="u1_val3_salt" CssClass="control-label" runat="server"></asp:Label>
                                    </div>
                                    <div class="col-sm-2">
                                        <asp:TextBox ID="tb_u1_val3_salt" runat="server" CssClass="form-control" placeholder="input..." Enabled="true" Visible="true"></asp:TextBox>
                                    </div>
                                    <div class="col-sm-2">
                                        <asp:Label ID="u1_val4_salt" CssClass="control-label font_text" runat="server"></asp:Label>
                                    </div>
                                    <div class="col-sm-2">
                                        <asp:TextBox ID="tb_u1_val4_salt" runat="server" CssClass="form-control" placeholder="input..." Enabled="true" Visible="true"></asp:TextBox>
                                    </div>
                                </div>
                            </asp:Panel>
                            <br />


                            <!-- gv salt -->
                            <asp:Label ID="lbnameGridview" CssClass="control-label" Visible="false" runat="server"></asp:Label>
                            <asp:Label ID="lbDecisionRecordResult" CssClass="control-label" Visible="false" runat="server"></asp:Label>

                            <asp:GridView ID="GvSalt" runat="server"
                                ShowFooter="true"
                                OnRowDataBound="gvRowDataBound"
                                OnRowCommand="gvOnrowCommand"
                                HeaderStyle-CssClass="info" Font-Size="8"
                                AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-responsive"
                                HeaderStyle-HorizontalAlign="Center">
                                <HeaderStyle CssClass="info" Width="100%" Height="25px" Font-Size="Small" />
                                <Columns>
                                    <asp:BoundField DataField="RowNumber" HeaderText="Row Number" />
                                    <asp:TemplateField HeaderStyle-Font-Size="8" HeaderText="สารละลาย">
                                        <ItemTemplate>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="tb_result_salt_val1" runat="server"
                                                        CssClass="form-control" placeholder="" Enabled="true" Visible="true"></asp:TextBox>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-Font-Size="8" HeaderText="1">
                                        <ItemTemplate>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="tb_result_salt_val2" runat="server"
                                                        CssClass="form-control" placeholder="" Enabled="true" Visible="true"></asp:TextBox>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-Font-Size="8" HeaderText="2">

                                        <ItemTemplate>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="tb_result_salt_val3" runat="server"
                                                        CssClass="form-control" placeholder="" Enabled="true" Visible="true"></asp:TextBox>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-Font-Size="8" HeaderText="3">
                                        <ItemTemplate>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="tb_result_salt_val4" runat="server"
                                                        CssClass="form-control" placeholder="" Enabled="true" Visible="true"></asp:TextBox>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-Font-Size="8" HeaderText="avg">
                                        <ItemTemplate>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="tb_result_salt_val5" runat="server"
                                                        CssClass="form-control" placeholder="" Enabled="true" Visible="true"></asp:TextBox>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-Font-Size="8" HeaderText="error">
                                        <ItemTemplate>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="tb_result_salt_val6" runat="server"
                                                        CssClass="form-control" placeholder="" Enabled="true" Visible="true"></asp:TextBox>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-Font-Size="8" HeaderStyle-Width="10%" HeaderText=" ผ่าน/ไม่ผ่าน">
                                        <ItemTemplate>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <asp:RadioButtonList ID="rd_result_salt_val7" runat="server" AutoPostBack="true" RepeatDirection="Horizontal" Width="100%">
                                                        <asp:ListItem Value="1">ผ่าน</asp:ListItem>
                                                        <asp:ListItem Value="0">ไม่ผ่าน</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-Font-Size="8" HeaderText="หมายเหตุ">
                                        <ItemTemplate>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="tb_result_salt_val8" runat="server"
                                                        CssClass="form-control" placeholder="" Enabled="true" Visible="true"></asp:TextBox>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                        <FooterStyle HorizontalAlign="Right" />
                                        <FooterTemplate>
                                            <%--<asp:Button ID="ButtonAdd" runat="server" Text="Add New Row" onclick="ButtonAdd_Click" />--%>
                                            <asp:LinkButton ID="btn_InsertRowGvSalt" OnCommand="btnCommand" CssClass="btn btn-sm btn-success" runat="server" Text="เพิ่มแถว" data-toggle="tooltip" title="เพิ่มข้อมูล" CommandName="cmdInsertRowGvSalt" />
                                        </FooterTemplate>
                                        <%--   <FooterTemplate>
                                            <asp:LinkButton ID="btnInsertRow" CssClass="btn btn-sm btn-success" Text="เพิ่มแถวใหม่" CommandName="InsertRowSalt" data-toggle="tooltip" title="เพิ่มแถวใหม่" runat="server"></asp:LinkButton>
                                        </FooterTemplate>--%>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>

                            <!-- topics salt-->
                            <asp:Panel ID="pnActionRecordResult" Visible="false" runat="server">
                                <div class="form-group pull-right">
                                    <div class="col-sm-12">
                                        <asp:LinkButton ID="btnSaveResult" CssClass="btn btn-sm btn-success" Text="บันทึก" OnCommand="btnCommand" CommandName="cmdSaveResult" data-toggle="tooltip" title="บันทึก" runat="server"></asp:LinkButton>
                                        <asp:LinkButton ID="btnCancelSaveResult" OnCommand="btnCommand" CommandName="cmdCancelResult" CssClass="btn btn-sm btn-danger" Text="ยกเลิก" data-toggle="tooltip" title="ยกเลิก" runat="server"></asp:LinkButton>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                    </div>
                </div>
            </div>
        </asp:View>

        <asp:View ID="docSupervisor" runat="server">
            <div id="divdocSupervisor" runat="server" class="col-md-12">

                <asp:GridView ID="GvSupervisor"
                    runat="server"
                    AutoGenerateColumns="false" HeaderStyle-CssClass="info"
                    OnRowDeleting="gvRowDeleted"
                    OnRowDataBound="gvRowDataBound"
                    RowStyle-Wrap="true" Font-Size="Small"
                    CssClass="table table-striped table-bordered table-responsive">
                    <HeaderStyle CssClass="info" Height="30px" Font-Size="Small" />
                    <RowStyle Font-Size="Small" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่พบข้อมูล</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="No." HeaderStyle-CssClass="text-left" HeaderStyle-Width="1%" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblU1DocCalSupervisor" Visible="false" runat="server" Text='<%# Eval("u1_cal_idx")%>'></asp:Label>
                                <asp:Label ID="lblU0DocCalSupervisor" Visible="false" runat="server" Text='<%# Eval("u0_cal_idx")%>'></asp:Label>
                                <asp:Label ID="lblm0NodeIDXSupervisor" Visible="false" runat="server" Text='<%# Eval("m0_node_idx")%>'></asp:Label>
                                <asp:Label ID="lblm0DeviveIDXSupervisor" Visible="false" runat="server" Text='<%# Eval("m0_device_idx")%>'></asp:Label>
                                <%# (Container.DataItemIndex +1) %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Machine Name" HeaderStyle-Width="15%" HeaderStyle-CssClass="text-left" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblequipment_nameSupervisor" runat="server" Text='<%# Eval("equipment_name")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ID No." HeaderStyle-Width="10%" HeaderStyle-CssClass="text-left" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lbldevice_id_noSupervisor" runat="server" Text='<%# Eval("device_id_no")%>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Serial No." HeaderStyle-Width="10%" HeaderStyle-CssClass="text-left" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lbldevice_serialSupervisor" runat="server" Text='<%# Eval("device_serial")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Type" HeaderStyle-Width="10%" HeaderStyle-CssClass="text-left" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblTypeEquipmentSupervisor" runat="server" Text='<%# Eval("equipment_type_name")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lab" HeaderStyle-Width="7%" HeaderStyle-CssClass="text-left" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblM0LabInLab" runat="server" Visible="false" Text='<%# Eval("m0_lab_idx")%>'></asp:Label>
                                <asp:Label ID="lblM0LabNameSupervisor" runat="server" Text='<%# Eval("lab_name")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Cal. Date" HeaderStyle-Width="7%" HeaderStyle-CssClass="text-left" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status" HeaderStyle-Width="10%" HeaderStyle-CssClass="text-left" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblstaidxSupervisor" runat="server" Text='<%# Eval("staidx")%>'></asp:Label>
                                <asp:Label ID="lblstaidxSupervisorName" runat="server" Text='<%# Eval("status_name")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Action" HeaderStyle-Width="5%" HeaderStyle-CssClass="text-left" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnViewDocCalSupervisor" Visible="true" CssClass="btn btn-sm btn-info" runat="server" data-original-title="View" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdView"
                                    CommandArgument='<%# Eval("u0_cal_idx") + "," + "2" + "," + Eval("equipment_type") + "," + Eval("certificate") + "," +  Eval("u1_cal_idx") + "," +  Eval("staidx") + "," +  Eval("place_idx") + "," +  Eval("m0_node_idx") %>'><i class="fa fa-file-text-o"></i></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>

            </div>
        </asp:View>
    </asp:MultiView>


    <script>
        $(function () {
            $('.cal-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });
        });


        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.cal-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY',
                    ignoreReadonly: true
                });
            });
        });

    </script>

    <script>
        $(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });
        });


        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY',
                    ignoreReadonly: true
                });
            });

            /* START Filter Datetimepicker Permission Permanent */
            $('.datetimepicker-filter-perm-from').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });
            $('.datetimepicker-filter-perm-from').on('dp.change', function (e) {
                var dateTo = $('.datetimepicker-filter-perm-to').data("DateTimePicker").date();
                if (e.date > dateTo) {
                    $('.datetimepicker-filter-perm-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
           <%-- $('#<%= HiddenCalDate.ClientID %>').val($('.datetimepicker-filter-perm-from').val());--%>
            });
            $('.show-filter-perm-from-onclick').click(function () {
                $('.datetimepicker-filter-perm-from').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-perm-to').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });
            $('.show-filter-perm-to-onclick').click(function () {
                $('.datetimepicker-filter-perm-to').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-perm-to').on('dp.change', function (e) {
                var dateFrom = $('.datetimepicker-filter-perm-from').data("DateTimePicker").date();
                if (e.date < dateFrom) {
                    $('.datetimepicker-filter-perm-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                 <%--$('#<%= txtFilterIndexPermPermToHidden.ClientID %>').val($('.datetimepicker-filter-perm-to').val());--%>
            });

            /* END Filter Datetimepicker Permission Permanent */
        });


        /* START Filter Datetimepicker Permission Permanent */
        $('.datetimepicker-filter-perm-from').datetimepicker({
            format: 'DD/MM/YYYY',
            ignoreReadonly: true
        });
        $('.datetimepicker-filter-perm-from').on('dp.change', function (e) {
            var dateTo = $('.datetimepicker-filter-perm-to').data("DateTimePicker").date();
            if (e.date > dateTo) {
                $('.datetimepicker-filter-perm-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
            }
           <%-- $('#<%= HiddenCalDate.ClientID %>').val($('.datetimepicker-filter-perm-from').val());--%>
        });
        $('.show-filter-perm-from-onclick').click(function () {
            $('.datetimepicker-filter-perm-from').data("DateTimePicker").show();
        });
        $('.datetimepicker-filter-perm-to').datetimepicker({
            format: 'DD/MM/YYYY',
            ignoreReadonly: true
        });
        $('.show-filter-perm-to-onclick').click(function () {
            $('.datetimepicker-filter-perm-to').data("DateTimePicker").show();
        });
        $('.datetimepicker-filter-perm-to').on('dp.change', function (e) {
            var dateFrom = $('.datetimepicker-filter-perm-from').data("DateTimePicker").date();
            if (e.date < dateFrom) {
                $('.datetimepicker-filter-perm-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
            }
             <%--$('#<%= txtFilterIndexPermPermToHidden.ClientID %>').val($('.datetimepicker-filter-perm-to').val());--%>
        });

            /* END Filter Datetimepicker Permission Permanent */


    </script>
</asp:Content>

