﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_chr")]
public class data_chr
{
    [XmlElement("ReturnCode")]
    public string ReturnCode { get; set; }

    [XmlElement("ReturnMsg")]
    public string ReturnMsg { get; set; }


    [XmlElement("ReturnIDX")]
    public string ReturnIDX { get; set; }

    [XmlElement("ReturnCount")]
    public string ReturnCount { get; set; }

    [XmlElement("ReturnCount2")]
    public string ReturnCount2 { get; set; }

    [XmlElement("ReturnCount3")]
    public string ReturnCount3 { get; set; }

    [XmlElement("ReturnCount4")]
    public string ReturnCount4 { get; set; }

    [XmlElement("ReturnCount5")]
    public string ReturnCount5 { get; set; }

    [XmlElement("ReturnCount6")]
    public string ReturnCount6 { get; set; }

    [XmlElement("ReturnCount7")]
    public string ReturnCount7 { get; set; }

    [XmlElement("ReturnCount8")]
    public string ReturnCount8 { get; set; }

    [XmlElement("ReturnCount9")]
    public string ReturnCount9 { get; set; }

    [XmlElement("ReturnCount10")]
    public string ReturnCount10 { get; set; }

    [XmlElement("ReturnName")]
    public string ReturnName { get; set; }

    [XmlElement("ReturnTemp")]
    public string ReturnTemp { get; set; }

    [XmlElement("ReturnEmail")]
    public string ReturnEmail { get; set; }

    [XmlElement("ReturnActor")]
    public string ReturnActor { get; set; }

    [XmlElement("ReturnU1IDX")]
    public string ReturnU1IDX { get; set; }



    [XmlElement("BoxCHRList")]
    public CHRList[] BoxCHRList { get; set; }

    [XmlElement("Export_BoxCHR")]
    public ExportCHRList[] Export_BoxCHR { get; set; }

    [XmlElement("BindData_U0Document")]
    public BindData[] BindData_U0Document { get; set; }

    [XmlElement("BoxMaster_SystemList")]
    public MasterSystemList[] BoxMaster_SystemList { get; set; }

    [XmlElement("BoxMaster_TypenameList")]
    public MasterTypenameList[] BoxMaster_TypenameList { get; set; }

    [XmlElement("BoxMaster_TypeClose1List")]
    public MasterTypeClose1List[] BoxMaster_TypeClose1List { get; set; }

    [XmlElement("BoxMaster_TypeClose2List")]
    public MasterTypeClose2List[] BoxMaster_TypeClose2List { get; set; }

    [XmlElement("BoxMaster_UserSAPList")]
    public MasterUserSAPList[] BoxMaster_UserSAPList { get; set; }

    [XmlElement("BoxMaster_ISOList")]
    public MasterISOList[] BoxMaster_ISOList { get; set; }

    [XmlElement("BoxMaster_AssignList")]
    public MasterAssignList[] BoxMaster_AssignList { get; set; }


}

[Serializable]
public class CHRList
{
    [XmlElement("DC_Year")]
    public string DC_Year { get; set; }

    [XmlElement("DC_Mount")]
    public string DC_Mount { get; set; }

    [XmlElement("TC1IDX")]
    public int TC1IDX { get; set; }

    [XmlElement("TC2IDX")]
    public int TC2IDX { get; set; }

    [XmlElement("MS1IDX")]
    public int MS1IDX { get; set; }

    [XmlElement("AEmpIDX")]
    public int AEmpIDX { get; set; }

    [XmlElement("AEmpIDX1")]
    public int AEmpIDX1 { get; set; }

    [XmlElement("AEmpIDX2")]
    public int AEmpIDX2 { get; set; }


    [XmlElement("IFSearch")]
    public int IFSearch { get; set; }

    [XmlElement("IFSearchbetween")]
    public int IFSearchbetween { get; set; }

    [XmlElement("DategetJob")]
    public string DategetJob { get; set; }

    [XmlElement("DateCloseJob")]
    public string DateCloseJob { get; set; }

    [XmlElement("StaIDX")]
    public int StaIDX { get; set; }

    [XmlElement("OrgIDX")]
    public int OrgIDX { get; set; }

    [XmlElement("RDeptIDX")]
    public int RDeptIDX { get; set; }

    [XmlElement("SysIDX")]
    public int SysIDX { get; set; }

    [XmlElement("MTIDX")]
    public int MTIDX { get; set; }

    [XmlElement("Typename")]
    public string Typename { get; set; }

    [XmlElement("TypeClose1_name")]
    public string TypeClose1_name { get; set; }

    [XmlElement("TypeClose2_name")]
    public string TypeClose2_name { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("AdminIDX")]
    public int AdminIDX { get; set; }

    [XmlElement("countmodule1")]
    public int countmodule1 { get; set; }

    [XmlElement("countmodule2")]
    public int countmodule2 { get; set; }

    [XmlElement("Sysidx")]
    public int Sysidx { get; set; }

    [XmlElement("noidx")]
    public int noidx { get; set; }

    [XmlElement("System_name")]
    public string System_name { get; set; }


    [XmlElement("node_desc")]
    public string node_desc { get; set; }

    [XmlElement("u0idx")]
    public int u0idx { get; set; }

    [XmlElement("doc_code")]
    public string doc_code { get; set; }

    [XmlElement("datecreate")]
    public string datecreate { get; set; }

    [XmlElement("dategetjob1")]
    public string dategetjob1 { get; set; }

    [XmlElement("dateendjob")]
    public string dateendjob { get; set; }

    [XmlElement("EmpName")]
    public string EmpName { get; set; }

    [XmlElement("node_name")]
    public string node_name { get; set; }

    [XmlElement("DeptNameTH")]
    public string DeptNameTH { get; set; }

    [XmlElement("SecNameTH")]
    public string SecNameTH { get; set; }

    [XmlElement("Usersap")]
    public string Usersap { get; set; }

    [XmlElement("Approve1")]
    public string Approve1 { get; set; }

    [XmlElement("Approve2")]
    public string Approve2 { get; set; }

    [XmlElement("Approve3")]
    public string Approve3 { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }

    [XmlElement("Name_Code1")]
    public string Name_Code1 { get; set; }

    [XmlElement("Name_Code11")]
    public string Name_Code11 { get; set; }


    [XmlElement("close2")]
    public string close2 { get; set; }


    [XmlElement("close3")]
    public string close3 { get; set; }

    [XmlElement("AdminMain")]
    public string AdminMain { get; set; }

    [XmlElement("AdminAdd")]
    public string AdminAdd { get; set; }


    [XmlElement("OrgNameTH")]
    public string OrgNameTH { get; set; }

    [XmlElement("old_sys")]
    public string old_sys { get; set; }

    [XmlElement("Topic")]
    public string Topic { get; set; }

    [XmlElement("new_sys")]
    public string new_sys { get; set; }

    [XmlElement("mandays")]
    public string mandays { get; set; }

    [XmlElement("Downtime")]
    public string Downtime { get; set; }

    [XmlElement("AdminName")]
    public string AdminName { get; set; }

    [XmlElement("Namecode")]
    public string Namecode { get; set; }

    [XmlElement("Name")]
    public string Name { get; set; }

    [XmlElement("mandays_1")]
    public string mandays_1 { get; set; }

    [XmlElement("Tcode1")]
    public string Tcode1 { get; set; }

    [XmlElement("Tcode2")]
    public string Tcode2 { get; set; }

    [XmlElement("monthdowntime")]
    public string monthdowntime { get; set; }

    [XmlElement("sumtime")]
    public string sumtime { get; set; }

    

}

[Serializable]
public class BindData
{

    [XmlElement("u0idx")]
    public int u0idx { get; set; }

    [XmlElement("u0idx_add")]
    public int u0idx_add { get; set; }


    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("unidx")]
    public int unidx { get; set; }


    [XmlElement("Sysidx")]
    public int Sysidx { get; set; }

    [XmlElement("md0idx")]
    public int md0idx { get; set; }


    [XmlElement("org_idx")]
    public int org_idx { get; set; }


    [XmlElement("tidx")]
    public int tidx { get; set; }

    [XmlElement("mtidx")]
    public int mtidx { get; set; }

    [XmlElement("CEmpidx")]
    public int CEmpidx { get; set; }

    [XmlElement("doc_decision")]
    public int doc_decision { get; set; }

    [XmlElement("SysSapIDX")]
    public int SysSapIDX { get; set; }

    [XmlElement("System_name")]
    public string System_name { get; set; }

    [XmlElement("NEmpidx1")]
    public string NEmpidx1 { get; set; }

    [XmlElement("NEmpidx2")]
    public string NEmpidx2 { get; set; }

    [XmlElement("NEmpidx3")]
    public string NEmpidx3 { get; set; }

    [XmlElement("NEmpidxAbout")]
    public string NEmpidxAbout { get; set; }

    [XmlElement("topic")]
    public string topic { get; set; }

    [XmlElement("Usersap")]
    public string Usersap { get; set; }

    [XmlElement("CostNo")]
    public string CostNo { get; set; }

    [XmlElement("Topic")]
    public string Topic { get; set; }

    [XmlElement("TypeJobname")]
    public string TypeJobname { get; set; }

    [XmlElement("PosNameTH")]
    public string PosNameTH { get; set; }

    [XmlElement("old_sys")]
    public string old_sys { get; set; }


    [XmlElement("Typename")]
    public string Typename { get; set; }

    [XmlElement("new_sys")]
    public string new_sys { get; set; }


    [XmlElement("rpos_idx")]
    public int rpos_idx { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("noidx")]
    public int noidx { get; set; }

    [XmlElement("acidx")]
    public int acidx { get; set; }

    [XmlElement("doc_staidx")]
    public int doc_staidx { get; set; }


    [XmlElement("EmpName")]
    public string EmpName { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("dategetjob")]
    public string dategetjob { get; set; }

    [XmlElement("dateendjob")]
    public string dateendjob { get; set; }

    [XmlElement("datecreate")]
    public string datecreate { get; set; }

    [XmlElement("datecreatestart")]
    public string datecreatestart { get; set; }

    [XmlElement("AEmpidx")]
    public int AEmpidx { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }

    [XmlElement("commentapp1")]
    public string commentapp1 { get; set; }

    [XmlElement("commentapp2")]
    public string commentapp2 { get; set; }

    [XmlElement("commentapp3")]
    public string commentapp3 { get; set; }

    [XmlElement("commentabout")]
    public string commentabout { get; set; }


    [XmlElement("Email")]
    public string Email { get; set; }


    [XmlElement("MobileNo")]
    public string MobileNo { get; set; }

    [XmlElement("OrgNameTH")]
    public string OrgNameTH { get; set; }

    [XmlElement("DeptNameTH")]
    public string DeptNameTH { get; set; }

    [XmlElement("SecNameTH")]
    public string SecNameTH { get; set; }


    [XmlElement("user_lock")]
    public int user_lock { get; set; }

    [XmlElement("locks")]
    public int locks { get; set; }

    [XmlElement("doc_code")]
    public string doc_code { get; set; }


    [XmlElement("node_name")]
    public string node_name { get; set; }


    [XmlElement("actor_des")]
    public string actor_des { get; set; }


    [XmlElement("StatusDoc")]
    public string StatusDoc { get; set; }

    [XmlElement("noidx_add")]
    public int noidx_add { get; set; }

    [XmlElement("acidx_add")]
    public int acidx_add { get; set; }

    [XmlElement("unidx_add")]
    public int unidx_add { get; set; }

    [XmlElement("approve_status_add")]
    public int approve_status_add { get; set; }

    [XmlElement("tidx_add")]
    public int tidx_add { get; set; }

    [XmlElement("Add_IDX_add")]
    public int Add_IDX_add { get; set; }

    [XmlElement("UIDX")]
    public int UIDX { get; set; }

    [XmlElement("ccidx_add")]
    public int ccidx_add { get; set; }

    [XmlElement("comment_add")]
    public string comment_add { get; set; }

    [XmlElement("Tcode1")]
    public string Tcode1 { get; set; }

    [XmlElement("Tcode2")]
    public string Tcode2 { get; set; }



}

[Serializable]
public class ExportCHRList
{
    [XmlElement("รหัสเอกสาร")]
    public string รหัสเอกสาร { get; set; }

    [XmlElement("ระบบ")]
    public string ระบบ { get; set; }

    [XmlElement("เงื่อนไขในการแจ้ง")]
    public string เงื่อนไขในการแจ้ง { get; set; }

    [XmlElement("องค์กรผู้สร้าง")]
    public string องค์กรผู้สร้าง { get; set; }

    [XmlElement("ฝ่ายผู้สร้าง")]
    public string ฝ่ายผู้สร้าง { get; set; }

    [XmlElement("แผนกผู้สร้าง")]
    public string แผนกผู้สร้าง { get; set; }

    [XmlElement("ชื่อผู้สร้าง")]
    public string ชื่อผู้สร้าง { get; set; }

    [XmlElement("Usersap")]
    public string Usersap { get; set; }

    [XmlElement("ผู้อนุมัติลำดับที่1")]
    public string ผู้อนุมัติลำดับที่1 { get; set; }

    [XmlElement("ผู้อนุมัติลำดับที่2")]
    public string ผู้อนุมัติลำดับที่2 { get; set; }


    [XmlElement("ผู้อนุมัติลำดับที่3")]
    public string ผู้อนุมัติลำดับที่3 { get; set; }

    [XmlElement("ระบบเดิม")]
    public string ระบบเดิม { get; set; }

    [XmlElement("ระบบใหม่")]
    public string ระบบใหม่ { get; set; }


    [XmlElement("comment")]
    public string comment { get; set; }

    [XmlElement("ขั้นตอนดำเนินการ")]
    public string ขั้นตอนดำเนินการ { get; set; }

    [XmlElement("วันที่แจ้ง")]
    public string วันที่แจ้ง { get; set; }

    [XmlElement("วันที่รับงาน")]
    public string วันที่รับงาน { get; set; }


    [XmlElement("วันที่ปิดงาน")]
    public string วันที่ปิดงาน { get; set; }


    [XmlElement("Downtime")]
    public string Downtime { get; set; }



    [XmlElement("ปิดงานLV1_SAPหลัก")]
    public string ปิดงานLV1_SAPหลัก { get; set; }

    [XmlElement("ปิดงานLV2_SAPหลัก")]
    public string ปิดงานLV2_SAPหลัก { get; set; }

    [XmlElement("ปิดงานLV3_SAPหลัก")]
    public string ปิดงานLV3_SAPหลัก { get; set; }

    [XmlElement("mandays_SAPหลัก")]
    public string mandays_SAPหลัก { get; set; }

    [XmlElement("ปิดงานLV1_SAPรอง")]
    public string ปิดงานLV1_SAPรอง { get; set; }

    [XmlElement("ปิดงานLV2_SAPรอง")]
    public string ปิดงานLV2_SAPรอง { get; set; }

    [XmlElement("ปิดงานLV3_SAPรอง")]
    public string ปิดงานLV3_SAPรอง { get; set; }

    [XmlElement("mandays_SAPรอง")]
    public string mandays_SAPรอง { get; set; }


    [XmlElement("ผู้รับงาน")]
    public string ผู้รับงาน { get; set; }


    [XmlElement("เจ้าหน้าที่SAPหลัก")]
    public string เจ้าหน้าที่SAPหลัก { get; set; }


    [XmlElement("เจ้าหน้าที่SAPรอง")]
    public string เจ้าหน้าที่SAPรอง { get; set; }

    [XmlElement("Tcode1")]
    public string Tcode1 { get; set; }

    [XmlElement("Tcode2")]
    public string Tcode2 { get; set; }


}

[Serializable]
public class MasterSystemList
{
    [XmlElement("System_name")]
    public string System_name { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("Sysidx")]
    public int Sysidx { get; set; }

    [XmlElement("Sys_Status")]
    public int Sys_Status { get; set; }

    [XmlElement("System_status")]
    public string System_status { get; set; }


}

[Serializable]
public class MasterTypenameList
{
    [XmlElement("Typename")]
    public string Typename { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("MTIDX")]
    public int MTIDX { get; set; }

    [XmlElement("Type_Status")]
    public int Type_Status { get; set; }

    [XmlElement("TypeStatus")]
    public string TypeStatus { get; set; }

}

[Serializable]
public class MasterTypeClose1List
{
    [XmlElement("TypeClose1_name")]
    public string TypeClose1_name { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("TC1IDX")]
    public int TC1IDX { get; set; }

    [XmlElement("TypeClose1_Status")]
    public int TypeClose1_Status { get; set; }

    [XmlElement("TypeClose1Status")]
    public string TypeClose1Status { get; set; }

}

[Serializable]
public class MasterTypeClose2List
{
    [XmlElement("TypeClose2_name")]
    public string TypeClose2_name { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("TC2IDX")]
    public int TC2IDX { get; set; }

    [XmlElement("TypeClose2_Status")]
    public int TypeClose2_Status { get; set; }

    [XmlElement("TypeClose2Status")]
    public string TypeClose2Status { get; set; }

}

[Serializable]
public class MasterUserSAPList
{
    [XmlElement("Sysidx")]
    public int Sysidx { get; set; }

    [XmlElement("UserIDX")]
    public int UserIDX { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("U_Status")]
    public int U_Status { get; set; }

    [XmlElement("UserSAPStatus")]
    public string UserSAPStatus { get; set; }

    [XmlElement("Usersap")]
    public string Usersap { get; set; }

    [XmlElement("CostIDX")]
    public int CostIDX { get; set; }

    [XmlElement("OrgIDX")]
    public int OrgIDX { get; set; }

    [XmlElement("RDeptIDX")]
    public int RDeptIDX { get; set; }

    [XmlElement("System_name")]
    public string System_name { get; set; }

    [XmlElement("OrgNameTH")]
    public string OrgNameTH { get; set; }

    [XmlElement("DeptNameTH")]
    public string DeptNameTH { get; set; }

    [XmlElement("CostNo")]
    public string CostNo { get; set; }


}

[Serializable]
public class MasterISOList
{
    [XmlElement("ISOIDX")]
    public int ISOIDX { get; set; }

    [XmlElement("Sysidx")]
    public int Sysidx { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }


    [XmlElement("Doccode")]
    public string Doccode { get; set; }

    [XmlElement("Docname")]
    public string Docname { get; set; }

    [XmlElement("ISO_Status")]
    public int ISO_Status { get; set; }

    [XmlElement("OrgIDX")]
    public int OrgIDX { get; set; }

    [XmlElement("System_name")]
    public string System_name { get; set; }

    [XmlElement("OrgNameTH")]
    public string OrgNameTH { get; set; }

    [XmlElement("VersionIso")]
    public string VersionIso { get; set; }

    [XmlElement("Result")]
    public string Result { get; set; }

    [XmlElement("ISO_StatusDetail")]
    public string ISO_StatusDetail { get; set; }

    [XmlElement("FullNameTH")]
    public string FullNameTH { get; set; }


    

}

[Serializable]
public class MasterAssignList
{
    [XmlElement("AssignIDX")]
    public int AssignIDX { get; set; }

    [XmlElement("Assign_Status")]
    public int Assign_Status { get; set; }

    [XmlElement("Sysidx")]
    public int Sysidx { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("OrgIDX")]
    public int OrgIDX { get; set; }

    [XmlElement("RDeptIDX")]
    public int RDeptIDX { get; set; }

    [XmlElement("EmpIDX")]
    public int EmpIDX { get; set; }

    [XmlElement("RSecIDX")]
    public int RSecIDX { get; set; }

    [XmlElement("System_name")]
    public string System_name { get; set; }

    [XmlElement("OrgNameTH")]
    public string OrgNameTH { get; set; }

    [XmlElement("DeptNameTH")]
    public string DeptNameTH { get; set; }

    [XmlElement("SecNameTH")]
    public string SecNameTH { get; set; }

    [XmlElement("FullNameTH")]
    public string FullNameTH { get; set; }

    [XmlElement("Assign_StatusDetail")]
    public string Assign_StatusDetail { get; set; }

    [XmlElement("FileName")]
    public string FileName { get; set; }

}



