using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_fingerscan_master_machine : System.Web.UI.Page
{
    #region initial function/data
    function_tool _funcTool = new function_tool();
    data_times _data_times = new data_times();
    data_employee _data_employee = new data_employee();
    List<finger_machine_detail> _data_machine_selected = new List<finger_machine_detail>();
    List<tm_group_detail_m2> _data_position_selected = new List<tm_group_detail_m2>();

    int _masterType; //1:machine; 2:place; 3:location; 4:zone;
    int _emp_idx = 0;
    int _temp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _local_json = String.Empty;

    static int[] _permission = {172, 24047};

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlTmSetMachine = _serviceUrl + ConfigurationManager.AppSettings["urlTmSetMachine"];
    static string _urlTmGetMachine = _serviceUrl + ConfigurationManager.AppSettings["urlTmGetMachine"];
    static string _urlTmSetPlace = _serviceUrl + ConfigurationManager.AppSettings["urlTmSetPlace"];
    static string _urlTmGetPlace = _serviceUrl + ConfigurationManager.AppSettings["urlTmGetPlace"];
    static string _urlTmSetLocation = _serviceUrl + ConfigurationManager.AppSettings["urlTmSetLocation"];
    static string _urlTmGetLocation = _serviceUrl + ConfigurationManager.AppSettings["urlTmGetLocation"];
    static string _urlTmSetZone = _serviceUrl + ConfigurationManager.AppSettings["urlTmSetZone"];
    static string _urlTmGetZone = _serviceUrl + ConfigurationManager.AppSettings["urlTmGetZone"];
    static string _urlTmSetGroup = _serviceUrl + ConfigurationManager.AppSettings["urlTmSetGroup"];
    static string _urlTmGetGroup = _serviceUrl + ConfigurationManager.AppSettings["urlTmGetGroup"];
    static string _urlTmGetPlant = _serviceUrl + ConfigurationManager.AppSettings["urlTmGetPlant"];

    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    static string _urlGetPositionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetPositionList"];
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        _emp_idx = _funcTool.convertToInt(Session["emp_idx"].ToString());

        if(!IsPostBack)
        {
            _masterType = 1;
            rblMasterType2.SelectedValue = _masterType.ToString();
            initPage();
        }
    }

    #region event command
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        switch(cmdName)
        {
            case "cmdCreate":
                _masterType = _funcTool.convertToInt(rblMasterType2.SelectedValue);
                setActiveView("viewCreate", _masterType);

                div_heading.Attributes.Add("class", "panel panel-info");
                litHeadingTitle.Text = "เพิ่มข้อมูล";
                rblMasterType.Enabled = false;
                break;
            case "cmdEdit":
                _masterType = _funcTool.convertToInt(rblMasterType2.SelectedValue);
                setActiveView("viewCreate", _masterType);

                div_heading.Attributes.Add("class", "panel panel-warning");
                litHeadingTitle.Text = "แก้ไขข้อมูล";
                rblMasterType.Enabled = false;
                switch(_masterType)
                {
                    case 1:
                        search_machine_detail _edit_machine = new search_machine_detail();
                        _edit_machine.s_m0_idx = cmdArg;
                        _data_times.search_machine_list = new search_machine_detail[1];
                        _data_times.search_machine_list[0] = _edit_machine;
                        _data_times = getMachineList(_data_times);
                        setFormData(fvDetail1, FormViewMode.Edit, _data_times.finger_machine_list);
                        break;
                    case 2:
                        search_place_detail _edit_place = new search_place_detail();
                        _edit_place.s_m0_idx = cmdArg;
                        _data_times.search_place_list = new search_place_detail[1];
                        _data_times.search_place_list[0] = _edit_place;
                        _data_times = getPlaceList(_data_times);
                        setFormData(fvDetail2, FormViewMode.Edit, _data_times.tm_place_list);
                        break;
                    case 3:
                        search_location_detail _edit_location = new search_location_detail();
                        _edit_location.s_m0_idx = cmdArg;
                        _data_times.search_location_list = new search_location_detail[1];
                        _data_times.search_location_list[0] = _edit_location;
                        _data_times = getLocationList(_data_times);
                        setFormData(fvDetail3, FormViewMode.Edit, _data_times.tm_location_list);
                        break;
                    case 4:
                        search_zone_detail _edit_zone = new search_zone_detail();
                        _edit_zone.s_m0_idx = cmdArg;
                        _data_times.search_zone_list = new search_zone_detail[1];
                        _data_times.search_zone_list[0] = _edit_zone;
                        _data_times = getZoneList(_data_times);
                        setFormData(fvDetail4, FormViewMode.Edit, _data_times.tm_zone_list);
                        break;
                    case 5:
                        search_group_detail _edit_group = new search_group_detail();
                        _edit_group.s_m0_idx = cmdArg;
                        _data_times.search_group_list = new search_group_detail[1];
                        _data_times.search_group_list[0] = _edit_group;
                        _data_times = getGroupList(_data_times);
                        setFormData(fvDetail5, FormViewMode.Edit, _data_times.tm_group_list);

                        // set command argument
                        // lbSave.CommandArgument = _data_times.tm_group_list[0].m0_idx.ToString();
                        
                        // get machine list selected
                        if(_data_times.finger_machine_list != null)
                        {
                            ViewState["FingerMachineGroupSelected"] = (List<finger_machine_detail>)_data_times.finger_machine_list.ToList();
                        }
                        else
                        {
                            ViewState["FingerMachineGroupSelected"] = null;
                        }
                        setGridData(gvMachineListSelected, ViewState["FingerMachineGroupSelected"]);

                        // get position list selected
                        if(_data_times.tm_group_list_m2 != null)
                        {
                            ViewState["PositionListSelected"] = (List<tm_group_detail_m2>)_data_times.tm_group_list_m2.ToList();
                        }
                        else
                        {
                            ViewState["PositionListSelected"] = null;
                        }
                        setGridData(gvPositionListSelected, ViewState["PositionListSelected"]);
                        break;
                }
                break;
            case "cmdSave":
                _masterType = _funcTool.convertToInt(rblMasterType2.SelectedValue);
                switch(_masterType)
                {
                    case 1:
                        TextBox tbIpHost = (TextBox)fvDetail1.FindControl("tbIpHost");
                        TextBox tbMachineName = (TextBox)fvDetail1.FindControl("tbMachineName");
                        DropDownList ddlMachineZone = (DropDownList)fvDetail1.FindControl("ddlMachineZone");
                        DropDownList ddlMachinePlace = (DropDownList)fvDetail1.FindControl("ddlMachinePlace");
                        DropDownList ddlMachineLoc = (DropDownList)fvDetail1.FindControl("ddlMachineLoc");
                        DropDownList ddlMachineSec = (DropDownList)fvDetail1.FindControl("ddlMachineSec");
                        RadioButtonList rblAttendanceStatus = (RadioButtonList)fvDetail1.FindControl("rblAttendanceStatus");
                        RadioButtonList rblMachineStatus = (RadioButtonList)fvDetail1.FindControl("rblStatus");

                        finger_machine_detail _save_machine = new finger_machine_detail();
                        _save_machine.m0_idx = _funcTool.convertToInt(cmdArg);
                        _save_machine.ip_host = tbIpHost.Text.Trim();
                        _save_machine.machine_name = tbMachineName.Text.Trim();
                        _save_machine.zone_idx = _funcTool.convertToInt(ddlMachineZone.SelectedValue);
                        _save_machine.place_idx = _funcTool.convertToInt(ddlMachinePlace.SelectedValue);
                        _save_machine.loc_idx = _funcTool.convertToInt(ddlMachineLoc.SelectedValue);
                        _save_machine.rsec_idx = _funcTool.convertToInt(ddlMachineSec.SelectedValue);
                        _save_machine.flag_attendance = _funcTool.convertToInt(rblAttendanceStatus.SelectedValue);
                        _save_machine.machine_status = _funcTool.convertToInt(rblMachineStatus.SelectedValue);
                        _data_times.finger_machine_list = new finger_machine_detail[1];
                        _data_times.finger_machine_list[0] = _save_machine;

                        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_times));

                        _data_times = setMachineList(_data_times);
                        if(_data_times.return_code != 0)
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('error code " + _data_times.return_code + ": " + _data_times.return_msg + "')", true);
                        }
                        break;
                    case 2:
                        TextBox tbPlaceName = (TextBox)fvDetail2.FindControl("tbPlaceName");
                        RadioButtonList rblPlaceStatus = (RadioButtonList)fvDetail2.FindControl("rblStatus");

                        tm_place_detail _save_place = new tm_place_detail();
                        _save_place.m0_idx = _funcTool.convertToInt(cmdArg);
                        _save_place.place_name = tbPlaceName.Text.Trim();
                        _save_place.place_status = _funcTool.convertToInt(rblPlaceStatus.SelectedValue);
                        _data_times.tm_place_list = new tm_place_detail[1];
                        _data_times.tm_place_list[0] = _save_place;

                        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_times));

                        _data_times = setPlaceList(_data_times);
                        if(_data_times.return_code != 0)
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('error code " + _data_times.return_code + ": " + _data_times.return_msg + "')", true);
                        }
                        break;
                    case 3:
                        TextBox tbLocationName = (TextBox)fvDetail3.FindControl("tbLocationName");
                        RadioButtonList rblLocationStatus = (RadioButtonList)fvDetail3.FindControl("rblStatus");

                        tm_location_detail _save_location = new tm_location_detail();
                        _save_location.m0_idx = _funcTool.convertToInt(cmdArg);
                        _save_location.location_name = tbLocationName.Text.Trim();
                        _save_location.location_status = _funcTool.convertToInt(rblLocationStatus.SelectedValue);
                        _data_times.tm_location_list = new tm_location_detail[1];
                        _data_times.tm_location_list[0] = _save_location;

                        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_times));

                        _data_times = setLocationList(_data_times);
                        if(_data_times.return_code != 0)
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('error code " + _data_times.return_code + ": " + _data_times.return_msg + "')", true);
                        }
                        break;
                    case 4:
                        TextBox tbZoneName = (TextBox)fvDetail4.FindControl("tbZoneName");
                        RadioButtonList rblZoneStatus = (RadioButtonList)fvDetail4.FindControl("rblStatus");

                        tm_zone_detail _save_zone = new tm_zone_detail();
                        _save_zone.m0_idx = _funcTool.convertToInt(cmdArg);
                        _save_zone.zone_name = tbZoneName.Text.Trim();
                        _save_zone.zone_status = _funcTool.convertToInt(rblZoneStatus.SelectedValue);
                        _data_times.tm_zone_list = new tm_zone_detail[1];
                        _data_times.tm_zone_list[0] = _save_zone;

                        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_times));

                        _data_times = setZoneList(_data_times);
                        if(_data_times.return_code != 0)
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('error code " + _data_times.return_code + ": " + _data_times.return_msg + "')", true);
                        }
                        break;
                    case 5:
                        TextBox tbGroupName = (TextBox)fvDetail5.FindControl("tbGroupName");
                        RadioButtonList rblGroupStatus = (RadioButtonList)fvDetail5.FindControl("rblStatus");
                        DropDownList ddlPlantList = (DropDownList)fvDetail5.FindControl("ddlPlantList");
                        HiddenField hfM0Idx = (HiddenField)fvDetail5.FindControl("hfM0Idx");

                        tm_group_detail _save_group = new tm_group_detail();
                        _save_group.m0_idx = _funcTool.convertToInt(hfM0Idx.Value);
                        _save_group.group_name = tbGroupName.Text.Trim();
                        _save_group.group_status = _funcTool.convertToInt(rblGroupStatus.SelectedValue);
                        _save_group.plant_idx = _funcTool.convertToInt(ddlPlantList.SelectedValue);
                        _data_times.tm_group_list = new tm_group_detail[1];
                        _data_times.tm_group_list[0] = _save_group;

                        // save machine list selected
                        if(ViewState["FingerMachineGroupSelected"] != null)
                        {
                            _data_machine_selected = (List<finger_machine_detail>)ViewState["FingerMachineGroupSelected"];
                            _data_times.finger_machine_list = new finger_machine_detail[_data_machine_selected.Count];
                        
                            int i = 0;
                            foreach(finger_machine_detail _item in _data_machine_selected)
                            {
                                finger_machine_detail _machine_list = new finger_machine_detail();
                                _machine_list.m0_idx = _item.m0_idx;

                                _data_times.finger_machine_list[i] = _machine_list;
                                i++;
                            }
                        }

                        // save position list selected
                        if(ViewState["PositionListSelected"] != null)
                        {
                            _data_position_selected = (List<tm_group_detail_m2>)ViewState["PositionListSelected"];
                            _data_times.tm_group_list_m2 = new tm_group_detail_m2[_data_position_selected.Count];
                        
                            int j = 0;
                            foreach(tm_group_detail_m2 _item in _data_position_selected)
                            {
                                tm_group_detail_m2 _position_list = new tm_group_detail_m2();
                                _position_list.rpos_idx = _item.rpos_idx;
                                _position_list.flag_priority = _item.flag_priority;

                                _data_times.tm_group_list_m2[j] = _position_list;
                                j++;
                            }
                        }

                        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_times));

                        _data_times = setGroupList(_data_times);

                        if(_data_times.return_code != 0)
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('error code " + _data_times.return_code + ": " + _data_times.return_msg + "')", true);
                        }
                        clearViewState();
                        break;
                }
                setActiveView("viewList", _masterType);
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "scrollToTop", "window.scrollTo(0, 0)", true);
                break;
            case "cmdCancel":
                _masterType = _funcTool.convertToInt(rblMasterType2.SelectedValue);
                setActiveView("viewList", _masterType);
                clearViewState();
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "scrollToTop", "window.scrollTo(0, 0)", true);
                break;
            case "cmdReset":
                _masterType = _funcTool.convertToInt(rblMasterType2.SelectedValue);
                switch(_masterType)
                {
                    case 1:
                        setFormData(fvDetail1, FormViewMode.Insert, null);
                        break;
                    case 2:
                        setFormData(fvDetail2, FormViewMode.Insert, null);
                        break;
                    case 3:
                        setFormData(fvDetail3, FormViewMode.Insert, null);
                        break;
                    case 4:
                        setFormData(fvDetail4, FormViewMode.Insert, null);
                        break;
                    case 5:
                        setFormData(fvDetail5, FormViewMode.Insert, null);
                        // clear machine data - 2 side
                        ViewState["FingerMachineGroupSelected"] = null;
                        setGridData(gvMachineListSelected, ViewState["FingerMachineGroupSelected"]);
                        setGridData(gvMachineList, ViewState["FingerMachineGroupList"]);
                        gvMachineList.PageIndex = 0;
                        // clear position data - 2 side
                        ViewState["PositionListSelected"] = null;
                        ViewState["PositionList"] = null;
                        setGridData(gvPositionListSelected, ViewState["PositionListSelected"]);
                        setGridData(gvPositionList, ViewState["PositionList"]);
                        // clear dropdownlist
                        getOrganizationList(ddlOrg);
                        ddlDept.Items.Clear();
                        ddlDept.Items.Insert(0, new ListItem("--- ฝ่าย ---", "0"));
                        ddlSec.Items.Clear();
                        ddlSec.Items.Insert(0, new ListItem("--- แผนก ---", "0"));
                        break;
                }
                break;
            case "cmdDelete":
                _masterType = _funcTool.convertToInt(rblMasterType2.SelectedValue);
                switch(_masterType)
                {
                    case 1:
                        finger_machine_detail _delete_machine = new finger_machine_detail();
                        _delete_machine.m0_idx = _funcTool.convertToInt(cmdArg);
                        _delete_machine.machine_status = 9;
                        _data_times.finger_machine_list = new finger_machine_detail[1];
                        _data_times.finger_machine_list[0] = _delete_machine;

                        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_times));

                        _data_times = setMachineList(_data_times);
                        if(_data_times.return_code != 0)
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('error code " + _data_times.return_code + ": " + _data_times.return_msg + "')", true);
                        }
                        break;
                    case 2:
                        tm_place_detail _delete_place = new tm_place_detail();
                        _delete_place.m0_idx = _funcTool.convertToInt(cmdArg);
                        _delete_place.place_status = 9;
                        _data_times.tm_place_list = new tm_place_detail[1];
                        _data_times.tm_place_list[0] = _delete_place;

                        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_times));

                        _data_times = setPlaceList(_data_times);
                        if(_data_times.return_code != 0)
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('error code " + _data_times.return_code + ": " + _data_times.return_msg + "')", true);
                        }
                        break;
                    case 3:
                        tm_location_detail _delete_location = new tm_location_detail();
                        _delete_location.m0_idx = _funcTool.convertToInt(cmdArg);
                        _delete_location.location_status = 9;
                        _data_times.tm_location_list = new tm_location_detail[1];
                        _data_times.tm_location_list[0] = _delete_location;

                        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_times));

                        _data_times = setLocationList(_data_times);
                        if(_data_times.return_code != 0)
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('error code " + _data_times.return_code + ": " + _data_times.return_msg + "')", true);
                        }
                        break;
                    case 4:
                        tm_zone_detail _delete_zone = new tm_zone_detail();
                        _delete_zone.m0_idx = _funcTool.convertToInt(cmdArg);
                        _delete_zone.zone_status = 9;
                        _data_times.tm_zone_list = new tm_zone_detail[1];
                        _data_times.tm_zone_list[0] = _delete_zone;

                        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_times));

                        _data_times = setZoneList(_data_times);
                        if(_data_times.return_code != 0)
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('error code " + _data_times.return_code + ": " + _data_times.return_msg + "')", true);
                        }
                        break;
                    case 5:
                        tm_group_detail _delete_group = new tm_group_detail();
                        _delete_group.m0_idx = _funcTool.convertToInt(cmdArg);
                        _delete_group.group_status = 9;
                        _data_times.tm_group_list = new tm_group_detail[1];
                        _data_times.tm_group_list[0] = _delete_group;

                        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_times));

                        _data_times = setGroupList(_data_times);

                        if(_data_times.return_code != 0)
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('error code " + _data_times.return_code + ": " + _data_times.return_msg + "')", true);
                        }
                        break;
                }
                setActiveView("viewList", _masterType);
                break;
            case "cmdItemSearch":
                _masterType = _funcTool.convertToInt(rblMasterType2.SelectedValue);
                switch(_masterType)
                {
                    case 1:
                        search_machine_detail _search_item_machine = new search_machine_detail();
                        // check permission
                        foreach(int _pass in _permission)
                        {
                            if(_emp_idx == _pass)
                            {
                                _search_item_machine.s_ip_host = tbItemSearch.Text.Trim();
                                continue;
                            }
                        }
                        _search_item_machine.s_machine_name = tbItemSearch.Text.Trim();
                        _data_times.search_machine_list = new search_machine_detail[1];
                        _data_times.search_machine_list[0] = _search_item_machine;
                        _data_times = getMachineList(_data_times);
                        setGridData(gvList1, _data_times.finger_machine_list);
                        break;
                    case 2:
                        search_place_detail _search_item_place = new search_place_detail();
                        _search_item_place.s_place_name = tbItemSearch.Text.Trim();
                        _data_times.search_place_list = new search_place_detail[1];
                        _data_times.search_place_list[0] = _search_item_place;
                        _data_times = getPlaceList(_data_times);
                        setGridData(gvList2, _data_times.tm_place_list);
                        break;
                    case 3:
                        search_location_detail _search_item_location = new search_location_detail();
                        _search_item_location.s_location_name = tbItemSearch.Text.Trim();
                        _data_times.search_location_list = new search_location_detail[1];
                        _data_times.search_location_list[0] = _search_item_location;
                        _data_times = getLocationList(_data_times);
                        setGridData(gvList3, _data_times.tm_location_list);
                        break;
                    case 4:
                        search_zone_detail _search_item_zone = new search_zone_detail();
                        _search_item_zone.s_zone_name = tbItemSearch.Text.Trim();
                        _data_times.search_zone_list = new search_zone_detail[1];
                        _data_times.search_zone_list[0] = _search_item_zone;
                        _data_times = getZoneList(_data_times);
                        setGridData(gvList4, _data_times.tm_zone_list);
                        break;
                    case 5:
                        search_group_detail _search_item_group = new search_group_detail();
                        _search_item_group.s_group_name = tbItemSearch.Text.Trim();
                        _data_times.search_group_list = new search_group_detail[1];
                        _data_times.search_group_list[0] = _search_item_group;
                        _data_times = getGroupList(_data_times);
                        setGridData(gvList5, _data_times.tm_group_list);
                        break;
                }
                break;
            case "cmdItemReset":
                tbItemSearch.Text = String.Empty;

                _masterType = _funcTool.convertToInt(rblMasterType2.SelectedValue);
                setActiveView("viewList", _masterType);
                break;
            case "cmdGroupManage":
                _masterType = _funcTool.convertToInt(rblMasterType2.SelectedValue);
                setActiveView("viewGroupManage", _masterType);
                break;
            case "cmdMachineSearch":
                search_machine_detail _search_machine = new search_machine_detail();
                _search_machine.s_zone_idx = ddlZone.SelectedValue;
                _search_machine.s_place_idx = ddlPlace.SelectedValue;
                _search_machine.s_loc_idx = ddlLoc.SelectedValue;
                _search_machine.s_machine_status = "1";
                _data_times.search_machine_list = new search_machine_detail[1];
                _data_times.search_machine_list[0] = _search_machine;
                _data_times = getMachineList(_data_times);
                ViewState["FingerMachineGroupList"] = _data_times.finger_machine_list;
                gvMachineList.PageIndex = 0;
                setGridData(gvMachineList, ViewState["FingerMachineGroupList"]);
                break;
            case "cmdMachineReset":
                _data_times = new data_times();
                search_machine_detail _machine_reset = new search_machine_detail();
                _machine_reset.s_machine_status = "1";
                _data_times.search_machine_list = new search_machine_detail[1];
                _data_times.search_machine_list[0] = _machine_reset;
                _data_times = getMachineList(_data_times);
                ViewState["FingerMachineGroupList"] = _data_times.finger_machine_list;
                setGridData(gvMachineList, ViewState["FingerMachineGroupList"]);

                getZoneDdl(ddlZone);
                getPlaceDdl(ddlPlace);
                getLocationDdl(ddlLoc);
                break;
            case "cmdPositionReset":
                ViewState["PositionList"] = null;
                setGridData(gvPositionList, ViewState["PositionList"]);

                // clear dropdownlist
                getOrganizationList(ddlOrg);
                ddlDept.Items.Clear();
                ddlDept.Items.Insert(0, new ListItem("--- ฝ่าย ---", "0"));
                ddlSec.Items.Clear();
                ddlSec.Items.Insert(0, new ListItem("--- แผนก ---", "0"));
                break;
            case "cmdActionDelete":
                if(ViewState["FingerMachineGroupSelected"] != null)
                {
                    _temp_idx = _funcTool.convertToInt(cmdArg);
                    _data_machine_selected = (List<finger_machine_detail>)ViewState["FingerMachineGroupSelected"];
                    _data_machine_selected.RemoveAll(l => l.m0_idx == _temp_idx);
                }
                ViewState["FingerMachineGroupSelected"] = _data_machine_selected;
                setGridData(gvMachineListSelected, ViewState["FingerMachineGroupSelected"]);
                setGridData(gvMachineList, ViewState["FingerMachineGroupList"]);
                break;
            case "cmdActionDeletePosition":
                if(ViewState["PositionListSelected"] != null)
                {
                    _temp_idx = _funcTool.convertToInt(cmdArg);
                    _data_position_selected = (List<tm_group_detail_m2>)ViewState["PositionListSelected"];
                    _data_position_selected.RemoveAll(l => l.rpos_idx == _temp_idx);
                }
                ViewState["PositionListSelected"] = _data_position_selected;
                setGridData(gvPositionListSelected, ViewState["PositionListSelected"]);
                setGridData(gvPositionList, ViewState["PositionList"]);
                break;
        }
    }

    protected void rblSelectedIndexChanged(object sender, EventArgs e)
    {
        RadioButtonList rblName = (RadioButtonList)sender;
        _masterType = _funcTool.convertToInt(rblName.SelectedValue);

        switch (rblName.ID)
        {
            case "rblMasterType2":
                setActiveView("viewList", _masterType);
                break;
        }
    }

    protected void cbCheckedChanged(object sender, EventArgs e)
    {
        var cb = (CheckBox)sender;
        switch (cb.ID)
        {
            case "cbMachineSelected":
                string[] _val_in2 = cb.Text.Split('|');
                _temp_idx = _funcTool.convertToInt(_val_in2[0]);
                if(ViewState["FingerMachineGroupSelected"] != null)
                {
                    _data_machine_selected = (List<finger_machine_detail>)ViewState["FingerMachineGroupSelected"];
                    if(cb.Checked)
                    {
                        var check_selected = _data_machine_selected.Where(d => 
                        d.m0_idx == _temp_idx).FirstOrDefault();
                        if(check_selected == null)
                        {
                            finger_machine_detail _machine_list = new finger_machine_detail();
                            _machine_list.m0_idx = _temp_idx;
                            _machine_list.ip_host = _val_in2[1];
                            _machine_list.zone_name = _val_in2[2];
                            _machine_list.place_name = _val_in2[3];
                            _machine_list.loc_name = _val_in2[4];
                            _data_machine_selected.Add(_machine_list);
                        }
                    }
                    else
                    {
                        _data_machine_selected.RemoveAll(l => l.m0_idx == _temp_idx);
                    }
                }
                else
                {
                    if(cb.Checked)
                    { 
                        finger_machine_detail _machine_list = new finger_machine_detail();
                        _machine_list.m0_idx = _temp_idx;
                        _machine_list.ip_host = _val_in2[1];
                        _machine_list.zone_name = _val_in2[2];
                        _machine_list.place_name = _val_in2[3];
                        _machine_list.loc_name = _val_in2[4];
                        _data_machine_selected.Add(_machine_list);
                    }
                }

                ViewState["FingerMachineGroupSelected"] = _data_machine_selected;
                setGridData(gvMachineListSelected, ViewState["FingerMachineGroupSelected"]);
                break;
            case "cbPositionSelected":
                string[] _val_in3 = cb.Text.Split('|');
                _temp_idx = _funcTool.convertToInt(_val_in3[0]);
                if(ViewState["PositionListSelected"] != null)
                {
                    _data_position_selected = (List<tm_group_detail_m2>)ViewState["PositionListSelected"];
                    if(cb.Checked)
                    {
                        var check_selected = _data_position_selected.Where(d => 
                        d.rpos_idx == _temp_idx).FirstOrDefault();
                        if(check_selected == null)
                        {
                            tm_group_detail_m2 _position_list = new tm_group_detail_m2();
                            _position_list.rpos_idx = _temp_idx;
                            _position_list.pos_name_th = _val_in3[1];
                            _position_list.sec_name_th = _val_in3[2];
                            _position_list.dept_name_th = _val_in3[3];
                            _data_position_selected.Add(_position_list);
                        }
                    }
                    else
                    {
                        _data_position_selected.RemoveAll(l => l.rpos_idx == _temp_idx);
                    }
                }
                else
                {
                    if(cb.Checked)
                    { 
                        tm_group_detail_m2 _position_list = new tm_group_detail_m2();
                        _position_list.rpos_idx = _temp_idx;
                        _position_list.pos_name_th = _val_in3[1];
                        _position_list.sec_name_th = _val_in3[2];
                        _position_list.dept_name_th = _val_in3[3];
                        _data_position_selected.Add(_position_list);
                    }
                }

                ViewState["PositionListSelected"] = _data_position_selected;
                setGridData(gvPositionListSelected, ViewState["PositionListSelected"]);
                break;
        }
    }

    protected void rbCheckedChanged(object sender, EventArgs e)
    {
        var rb = (RadioButton)sender;
        switch (rb.ID)
        {
            case "rbFlagPriority":
                string val_in = rb.Text;
                _temp_idx = _funcTool.convertToInt(val_in);
                if(ViewState["PositionListSelected"] != null)
                {
                    _data_position_selected = (List<tm_group_detail_m2>)ViewState["PositionListSelected"];
                    foreach(tm_group_detail_m2 _item in _data_position_selected)
                    {
                        if(rb.Checked && _item.rpos_idx == _temp_idx)
                        {
                            _item.flag_priority = 1;
                        }
                        else
                        {
                            _item.flag_priority = 0;
                        }
                    }

                    ViewState["PositionListSelected"] = _data_position_selected;
                    setGridData(gvPositionListSelected, ViewState["PositionListSelected"]);
                }
                break;
        }
    }
    #endregion event command

    #region RowDatabound
    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var gvName = (GridView)sender;

        switch (gvName.ID)
        {
            case "gvList1":
                gvName.Columns[1].Visible = false;

                // check permission
                foreach(int _pass in _permission)
                {
                    if(_emp_idx == _pass)
                    {
                        gvName.Columns[1].Visible = true;
                        continue;
                    }
                }
                break;
            case "gvMachineList":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox cbMachineSelected = (CheckBox)e.Row.FindControl("cbMachineSelected");
                    HiddenField hfM0Idx = (HiddenField)e.Row.FindControl("hfM0Idx");
                    _temp_idx = _funcTool.convertToInt(hfM0Idx.Value);
                    if(ViewState["FingerMachineGroupSelected"] != null)
                    {
                        _data_machine_selected = (List<finger_machine_detail>)ViewState["FingerMachineGroupSelected"];
                        var check_selected = _data_machine_selected.Where(d => 
                        d.m0_idx == _temp_idx).FirstOrDefault();
                        if(check_selected != null)
                        {
                            cbMachineSelected.Checked = true;
                        }
                    }
                }

                gvName.Columns[1].Visible = false;

                // check permission
                foreach(int _pass in _permission)
                {
                    if(_emp_idx == _pass)
                    {
                        gvName.Columns[1].Visible = true;
                        continue;
                    }
                }
                break;
            case "gvMachineListSelected":
                gvName.Columns[1].Visible = false;

                // check permission
                foreach(int _pass in _permission)
                {
                    if(_emp_idx == _pass)
                    {
                        gvName.Columns[1].Visible = true;
                        continue;
                    }
                }
                break;
            case "gvPositionList":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox cbPositionSelected = (CheckBox)e.Row.FindControl("cbPositionSelected");
                    HiddenField hfRposIdx = (HiddenField)e.Row.FindControl("hfRposIdx");
                    _temp_idx = _funcTool.convertToInt(hfRposIdx.Value);
                    if(ViewState["PositionListSelected"] != null)
                    {
                        _data_position_selected = (List<tm_group_detail_m2>)ViewState["PositionListSelected"];
                        var check_selected = _data_position_selected.Where(d => 
                        d.rpos_idx == _temp_idx).FirstOrDefault();
                        if(check_selected != null)
                        {
                            cbPositionSelected.Checked = true;
                        }
                    }
                }
                break;
            case "gvPositionListSelected":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    RadioButton rbFlagPriority = (RadioButton)e.Row.FindControl("rbFlagPriority");
                    HiddenField hfFlagPriority = (HiddenField)e.Row.FindControl("hfFlagPriority");
                    LinkButton lbDelete = (LinkButton)e.Row.FindControl("lbDelete");
                    _temp_idx = _funcTool.convertToInt(hfFlagPriority.Value);
                    if(ViewState["PositionListSelected"] != null)
                    {
                        if(hfFlagPriority.Value == "1")
                        {
                            rbFlagPriority.Checked = true;
                            lbDelete.Visible = false;
                        }
                    }
                }
                break;
        }
    }
    #endregion RowDatabound

    #region databound
    protected void fvDataBound(object sender, EventArgs e)
    {
        var fvName = (FormView)sender;
        switch (fvName.ID)
        {
            case "fvDetail1":
                // check formview mode
                if (fvName.CurrentMode == FormViewMode.Edit)
                {
                    HiddenField hfZoneIdx = (HiddenField)fvDetail1.FindControl("hfZoneIdx");
                    HiddenField hfPlaceIdx = (HiddenField)fvDetail1.FindControl("hfPlaceIdx");
                    HiddenField hfLocIdx = (HiddenField)fvDetail1.FindControl("hfLocIdx");
                    HiddenField hfOrgIdx = (HiddenField)fvDetail1.FindControl("hfOrgIdx");
                    HiddenField hfRdeptIdx = (HiddenField)fvDetail1.FindControl("hfRdeptIdx");
                    HiddenField hfRsecIdx = (HiddenField)fvDetail1.FindControl("hfRsecIdx");
                    DropDownList ddlMachineZone = (DropDownList)fvDetail1.FindControl("ddlMachineZone");
                    DropDownList ddlMachinePlace = (DropDownList)fvDetail1.FindControl("ddlMachinePlace");
                    DropDownList ddlMachineLoc = (DropDownList)fvDetail1.FindControl("ddlMachineLoc");
                    DropDownList ddlMachineOrg = (DropDownList)fvDetail1.FindControl("ddlMachineOrg");
                    DropDownList ddlMachineDept = (DropDownList)fvDetail1.FindControl("ddlMachineDept");
                    DropDownList ddlMachineSec = (DropDownList)fvDetail1.FindControl("ddlMachineSec");

                    if(hfZoneIdx.Value != "0") { ddlMachineZone.SelectedValue = hfZoneIdx.Value; }
                    if(hfPlaceIdx.Value != "0") { ddlMachinePlace.SelectedValue = hfPlaceIdx.Value; }
                    if(hfLocIdx.Value != "0") { ddlMachineLoc.SelectedValue = hfLocIdx.Value; }

                    getOrganizationList(ddlMachineOrg);
                    getDepartmentList(ddlMachineDept, _funcTool.convertToInt(hfOrgIdx.Value));
                    getSectionList(ddlMachineSec, _funcTool.convertToInt(hfOrgIdx.Value), _funcTool.convertToInt(hfRdeptIdx.Value));

                    if(hfOrgIdx.Value != "0") { ddlMachineOrg.SelectedValue = hfOrgIdx.Value; }
                    if(hfRdeptIdx.Value != "0") { ddlMachineDept.SelectedValue = hfRdeptIdx.Value; }
                    if(hfRsecIdx.Value != "0") { ddlMachineSec.SelectedValue = hfRsecIdx.Value; }
                }
                break;
            case "fvDetail5":
                // check formview mode
                if (fvName.CurrentMode == FormViewMode.Edit)
                {
                    HiddenField hfPlantIdx = (HiddenField)fvDetail5.FindControl("hfPlantIdx");
                    DropDownList ddlPlantList = (DropDownList)fvDetail5.FindControl("ddlPlantList");

                    if(hfPlantIdx.Value != "0") { ddlPlantList.SelectedValue = hfPlantIdx.Value; }
                }
                break;
        }
    }
    #endregion databound

    #region paging
    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView gvName = (GridView)sender;
        gvName.PageIndex = e.NewPageIndex;

        switch(gvName.ID)
        {
            case "gvList1":
                if(ViewState["FingerMachineList"] != null)
                {
                    setGridData(gvList1, ViewState["FingerMachineList"]);
                }
                else
                {
                    search_machine_detail _search_machine = new search_machine_detail();
                    _search_machine.s_m0_idx = "0";
                    _data_times.search_machine_list = new search_machine_detail[1];
                    _data_times.search_machine_list[0] = _search_machine;
                    _data_times = getMachineList(_data_times);
                    ViewState["FingerMachineList"] = _data_times.finger_machine_list;
                    setGridData(gvList1, ViewState["FingerMachineList"]);
                }
                break;
            case "gvList2":
                if(ViewState["TmPlaceList"] != null)
                {
                    setGridData(gvList2, ViewState["TmPlaceList"]);
                }
                else
                {
                    search_place_detail _search_place = new search_place_detail();
                    _search_place.s_m0_idx = "0";
                    _data_times.search_place_list = new search_place_detail[1];
                    _data_times.search_place_list[0] = _search_place;
                    _data_times = getPlaceList(_data_times);
                    ViewState["TmPlaceList"] = _data_times.tm_place_list;
                    setGridData(gvList2, ViewState["TmPlaceList"]);
                }
                break;
            case "gvList3":
                if(ViewState["TmLocationList"] != null)
                {
                    setGridData(gvList3, ViewState["TmLocationList"]);
                }
                else
                {
                    search_location_detail _search_location = new search_location_detail();
                    _search_location.s_m0_idx = "0";
                    _data_times.search_location_list = new search_location_detail[1];
                    _data_times.search_location_list[0] = _search_location;
                    _data_times = getLocationList(_data_times);
                    ViewState["TmLocationList"] = _data_times.tm_location_list;
                    setGridData(gvList3, ViewState["TmLocationList"]);
                }
                break;
            case "gvList4":
                if(ViewState["TmZoneList"] != null)
                {
                    setGridData(gvList4, ViewState["TmZoneList"]);
                }
                else
                {
                    search_zone_detail _search_zone = new search_zone_detail();
                    _search_zone.s_m0_idx = "0";
                    _data_times.search_zone_list = new search_zone_detail[1];
                    _data_times.search_zone_list[0] = _search_zone;
                    _data_times = getZoneList(_data_times);
                    ViewState["TmZoneList"] = _data_times.tm_zone_list;
                    setGridData(gvList4, ViewState["TmZoneList"]);
                }
                break;
            case "gvList5":
                if(ViewState["TmGroupList"] != null)
                {
                    setGridData(gvList5, ViewState["TmGroupList"]);
                }
                else
                {
                    search_group_detail _search_group = new search_group_detail();
                    _search_group.s_m0_idx = "0";
                    _data_times.search_group_list = new search_group_detail[1];
                    _data_times.search_group_list[0] = _search_group;
                    _data_times = getGroupList(_data_times);
                    ViewState["TmGroupList"] = _data_times.tm_group_list;
                    setGridData(gvList5, ViewState["TmGroupList"]);
                }
                break;
            case "gvMachineList":
                if(ViewState["FingerMachineGroupList"] != null)
                {
                    setGridData(gvMachineList, ViewState["FingerMachineGroupList"]);
                }
                else
                {
                    _data_times = new data_times();
                    _data_times = getMachineList(_data_times);
                    setGridData(gvMachineList, _data_times.finger_machine_list);
                }
                break;
        }
    }
    #endregion paging

    #region drorpdown list
    protected void getOrganizationList(DropDownList ddlName)
    {
        _data_employee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _data_employee.organization_list[0] = _orgList;

        _data_employee = callServicePostEmployee(_urlGetOrganizationList, _data_employee);
        setDdlData(ddlName, _data_employee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("--- องค์กร ---", "0"));
    }

    protected void getDepartmentList(DropDownList ddlName, int _org_idx)
    {
        _data_employee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _data_employee.department_list[0] = _deptList;

        _data_employee = callServicePostEmployee(_urlGetDepartmentList, _data_employee);
        setDdlData(ddlName, _data_employee.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("--- ฝ่าย ---", "0"));
    }

    protected void getSectionList(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {
        _data_employee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _data_employee.section_list[0] = _secList;

        _data_employee = callServicePostEmployee(_urlGetSectionList, _data_employee);
        setDdlData(ddlName, _data_employee.section_list, "sec_name_th", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("--- แผนก ---", "0"));
    }

    protected data_employee getPositionList(int _org_idx, int _rdept_idx, int _rsec_idx)
    {
        _data_employee.position_list = new position_details[1];
        position_details _posList = new position_details();
        _posList.org_idx = _org_idx;
        _posList.rdept_idx = _rdept_idx;
        _posList.rsec_idx = _rsec_idx;
        _data_employee.position_list[0] = _posList;

        _data_employee = callServicePostEmployee(_urlGetPositionList, _data_employee);

        return _data_employee;
    }

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlName = (DropDownList)sender;

        DropDownList ddlMachineOrg = (DropDownList)fvDetail1.FindControl("ddlMachineOrg");
        DropDownList ddlMachineDept = (DropDownList)fvDetail1.FindControl("ddlMachineDept");
        DropDownList ddlMachineSec = (DropDownList)fvDetail1.FindControl("ddlMachineSec");

        switch (ddlName.ID)
        {
            case "ddlMachineOrg":
                getDepartmentList(ddlMachineDept, _funcTool.convertToInt(ddlMachineOrg.SelectedItem.Value));
                ddlMachineSec.Items.Clear();
                ddlMachineSec.Items.Insert(0, new ListItem("--- แผนก ---", "0"));
                break;
            case "ddlMachineDept":
                getSectionList(ddlMachineSec, _funcTool.convertToInt(ddlMachineOrg.SelectedItem.Value), _funcTool.convertToInt(ddlMachineDept.SelectedItem.Value));
                break;
            case "ddlOrg":
                getDepartmentList(ddlDept, _funcTool.convertToInt(ddlOrg.SelectedItem.Value));
                ddlSec.Items.Clear();
                ddlSec.Items.Insert(0, new ListItem("--- แผนก ---", "0"));
                break;
            case "ddlDept":
                getSectionList(ddlSec, _funcTool.convertToInt(ddlOrg.SelectedItem.Value), _funcTool.convertToInt(ddlDept.SelectedItem.Value));
                break;
            case "ddlSec":
                _data_employee = getPositionList(_funcTool.convertToInt(ddlOrg.SelectedItem.Value), _funcTool.convertToInt(ddlDept.SelectedItem.Value), _funcTool.convertToInt(ddlSec.SelectedItem.Value));
                ViewState["PositionList"] = _data_employee.position_list;
                setGridData(gvPositionList, ViewState["PositionList"]);
                break;
        }
    }
    #endregion dropdown list

    #region reuse
    protected void initPage()
    {
        clearSession();
        clearViewState();

        setActiveView("viewList", _masterType);
    }

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        ViewState["FingerMachineList"] = null;
        ViewState["TmPlaceList"] = null;
        ViewState["TmLocationList"] = null;
        ViewState["TmZoneList"] = null;
        ViewState["TmGroupList"] = null;
        ViewState["FingerMachineGroupList"] = null;
        ViewState["FingerMachineGroupSelected"] = null;
        ViewState["PositionList"] = null;
        ViewState["PositionListSelected"] = null;
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();

        switch(fvName.ID)
        {
            case "fvDetail1":
                if(fvMode == FormViewMode.Insert || fvMode == FormViewMode.Edit)
                {
                    DropDownList ddlMachineZone = (DropDownList)fvDetail1.FindControl("ddlMachineZone");
                    DropDownList ddlMachinePlace = (DropDownList)fvDetail1.FindControl("ddlMachinePlace");
                    DropDownList ddlMachineLoc = (DropDownList)fvDetail1.FindControl("ddlMachineLoc");
                    DropDownList ddlMachineOrg = (DropDownList)fvDetail1.FindControl("ddlMachineOrg");
                    DropDownList ddlMachineDept = (DropDownList)fvDetail1.FindControl("ddlMachineDept");
                    DropDownList ddlMachineSec = (DropDownList)fvDetail1.FindControl("ddlMachineSec");
                    // ddl zone
                    search_zone_detail _zone_ddl = new search_zone_detail();
                    _zone_ddl.s_m0_idx = "0";
                    _zone_ddl.s_zone_status = "1";
                    _data_times.search_zone_list = new search_zone_detail[1];
                    _data_times.search_zone_list[0] = _zone_ddl;
                    _data_times = getZoneList(_data_times);
                    setDdlData(ddlMachineZone, _data_times.tm_zone_list, "zone_name", "m0_idx");
                    ddlMachineZone.Items.Insert(0, new ListItem("--- Zone ---", "0"));
                    
                    // ddl place 
                    search_place_detail _place_ddl = new search_place_detail();
                    _place_ddl.s_m0_idx = "0";
                    _place_ddl.s_place_status = "1";
                    _data_times.search_place_list = new search_place_detail[1];
                    _data_times.search_place_list[0] = _place_ddl;
                    _data_times = getPlaceList(_data_times);
                    setDdlData(ddlMachinePlace, _data_times.tm_place_list, "place_name", "m0_idx");
                    ddlMachinePlace.Items.Insert(0, new ListItem("--- สถานที่ตั้ง ---", "0"));

                    // ddl location
                    search_location_detail _localtion_ddl = new search_location_detail();
                    _localtion_ddl.s_m0_idx = "0";
                    _localtion_ddl.s_location_status = "1";
                    _data_times.search_location_list = new search_location_detail[1];
                    _data_times.search_location_list[0] = _localtion_ddl;
                    _data_times = getLocationList(_data_times);
                    setDdlData(ddlMachineLoc, _data_times.tm_location_list, "location_name", "m0_idx");
                    ddlMachineLoc.Items.Insert(0, new ListItem("--- ตำแหน่งที่ตั้ง ---", "0"));

                    // ddl org
                    getOrganizationList(ddlMachineOrg);

                    // check permission
                    Panel pnIpHost = (Panel)fvDetail1.FindControl("pnIpHost");
                    foreach(int _pass in _permission)
                    {
                        if(_emp_idx == _pass)
                        {
                            pnIpHost.Visible = true;
                            continue;
                        }
                    }
                }
                break;
            case "fvDetail5":
                if(fvMode == FormViewMode.Insert || fvMode == FormViewMode.Edit)
                {
                    DropDownList ddlPlantList = (DropDownList)fvDetail5.FindControl("ddlPlantList");

                    // ddl plant
                    search_tm_plant_detail _temp = new search_tm_plant_detail();
                    _temp.s_flag_visitor = "1";
                    _data_times.search_tm_plant_list = new search_tm_plant_detail[1];
                    _data_times.search_tm_plant_list[0] = _temp;
                    _data_times = getPlantList(_data_times);
                    setDdlData(ddlPlantList, _data_times.tm_plant_list, "plant_name", "plant_idx");
                    ddlPlantList.Items.Insert(0, new ListItem("--- สถานที่ ---", "0"));
                }
                break;
            
        }
    }

    protected void setRepeaterData(Repeater rptName, Object obj)
    {
        rptName.DataSource = obj;
        rptName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setActiveView(string activeTab, int masterType)
    {
        mvSystem.SetActiveView((View)mvSystem.FindControl(activeTab));

        switch(activeTab)
        {
            case "viewList":
                setGridData(gvList1, null);
                setGridData(gvList2, null);
                setGridData(gvList3, null);
                setGridData(gvList4, null);
                setGridData(gvList5, null);

                gvList1.Visible = false;
                gvList2.Visible = false;
                gvList3.Visible = false;
                gvList4.Visible = false;
                gvList5.Visible = false;

                switch(masterType)
                {
                    case 1:
                        gvList1.Visible = true;
                        search_machine_detail _search_machine = new search_machine_detail();
                        _search_machine.s_m0_idx = "0";
                        _data_times.search_machine_list = new search_machine_detail[1];
                        _data_times.search_machine_list[0] = _search_machine;
                        _data_times = getMachineList(_data_times);
                        ViewState["FingerMachineList"] = _data_times.finger_machine_list;
                        setGridData(gvList1, ViewState["FingerMachineList"]);
                        break;
                    case 2:
                        gvList2.Visible = true;
                        search_place_detail _search_place = new search_place_detail();
                        _search_place.s_m0_idx = "0";
                        _data_times.search_place_list = new search_place_detail[1];
                        _data_times.search_place_list[0] = _search_place;
                        _data_times = getPlaceList(_data_times);
                        ViewState["TmPlaceList"] = _data_times.tm_place_list;
                        setGridData(gvList2, ViewState["TmPlaceList"]);
                        break;
                    case 3:
                        gvList3.Visible = true;
                        search_location_detail _search_location = new search_location_detail();
                        _search_location.s_m0_idx = "0";
                        _data_times.search_location_list = new search_location_detail[1];
                        _data_times.search_location_list[0] = _search_location;
                        _data_times = getLocationList(_data_times);
                        ViewState["TmLocationList"] = _data_times.tm_location_list;
                        setGridData(gvList3, ViewState["TmLocationList"]);
                        break;
                    case 4:
                        gvList4.Visible = true;
                        search_zone_detail _search_zone = new search_zone_detail();
                        _search_zone.s_m0_idx = "0";
                        _data_times.search_zone_list = new search_zone_detail[1];
                        _data_times.search_zone_list[0] = _search_zone;
                        _data_times = getZoneList(_data_times);
                        ViewState["TmZoneList"] = _data_times.tm_zone_list;
                        setGridData(gvList4, ViewState["TmZoneList"]);
                        break;
                    case 5:
                        gvList5.Visible = true;
                        search_group_detail _search_group = new search_group_detail();
                        _search_group.s_m0_idx = "0";
                        _data_times.search_group_list = new search_group_detail[1];
                        _data_times.search_group_list[0] = _search_group;
                        _data_times = getGroupList(_data_times);
                        ViewState["TmGroupList"] = _data_times.tm_group_list;
                        setGridData(gvList5, ViewState["TmGroupList"]);
                        break;
                }
                break;
            case "viewCreate":
                rblMasterType.SelectedValue = rblMasterType2.SelectedValue;
                setFormData(fvDetail1, FormViewMode.ReadOnly, null);
                setFormData(fvDetail2, FormViewMode.ReadOnly, null);
                setFormData(fvDetail3, FormViewMode.ReadOnly, null);
                setFormData(fvDetail4, FormViewMode.ReadOnly, null);
                setFormData(fvDetail5, FormViewMode.ReadOnly, null);
                div_manage_machine.Visible = false;

                // litDebug.Text = masterType.ToString();
                switch(masterType)
                {
                    case 1:
                        setFormData(fvDetail1, FormViewMode.Insert, null);
                        break;
                    case 2:
                        setFormData(fvDetail2, FormViewMode.Insert, null);
                        break;
                    case 3:
                        setFormData(fvDetail3, FormViewMode.Insert, null);
                        break;
                    case 4:
                        setFormData(fvDetail4, FormViewMode.Insert, null);
                        break;
                    case 5:
                        setFormData(fvDetail5, FormViewMode.Insert, null);
                        div_manage_machine.Visible = true;

                        setGridData(gvMachineListSelected, ViewState["FingerMachineGroupSelected"]);

                        _data_times = new data_times();
                        search_machine_detail machine_transfer = new search_machine_detail();
                        machine_transfer.s_machine_status = "1";
                        _data_times.search_machine_list = new search_machine_detail[1];
                        _data_times.search_machine_list[0] = machine_transfer;
                        _data_times = getMachineList(_data_times);
                        ViewState["FingerMachineGroupList"] = _data_times.finger_machine_list;
                        setGridData(gvMachineList, ViewState["FingerMachineGroupList"]);

                        getZoneDdl(ddlZone);
                        getPlaceDdl(ddlPlace);
                        getLocationDdl(ddlLoc);
                    
                        setGridData(gvPositionListSelected, ViewState["PositionListSelected"]);

                        getOrganizationList(ddlOrg);
                        setGridData(gvPositionList, ViewState["PositionList"]);
                        break;
                }
                break;
        }
    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    protected void getPlaceDdl(DropDownList ddlName)
    {
        _data_times = callServicePostTimes(_urlTmGetPlace, _data_times);
        setDdlData(ddlName, _data_times.tm_place_list, "place_name", "m0_idx");
        ddlName.Items.Insert(0, new ListItem("--- สถานที่ตั้ง ---", "0"));
    }

    protected void getLocationDdl(DropDownList ddlName)
    {
        _data_times = new data_times();
        _data_times = callServicePostTimes(_urlTmGetLocation, _data_times);
        setDdlData(ddlName, _data_times.tm_location_list, "location_name", "m0_idx");
        ddlName.Items.Insert(0, new ListItem("--- ตำแหน่งที่ตั้ง ---", "0"));
    }

    protected void getZoneDdl(DropDownList ddlName)
    {
        _data_times = new data_times();
        _data_times = callServicePostTimes(_urlTmGetZone, _data_times);
        setDdlData(ddlName, _data_times.tm_zone_list, "zone_name", "m0_idx");
        ddlName.Items.Insert(0, new ListItem("--- Zone ---", "0"));
    }

    protected void getPlantDdl(DropDownList ddlName)
    {
        _data_times = new data_times();
        _data_times = callServicePostTimes(_urlTmGetPlant, _data_times);
        setDdlData(ddlName, _data_times.tm_plant_list, "plant_name", "plant_idx");
        ddlName.Items.Insert(0, new ListItem("--- สถานที่ ---", "0"));
    }

    private data_times setMachineList(data_times _data_times)
    {
        return callServicePostTimes(_urlTmSetMachine, _data_times);
    }

    private data_times getMachineList(data_times _data_times)
    {
        return callServicePostTimes(_urlTmGetMachine, _data_times);
    }

    private data_times setPlaceList(data_times _data_times)
    {
        return callServicePostTimes(_urlTmSetPlace, _data_times);
    }

    private data_times getPlaceList(data_times _data_times)
    {
        return callServicePostTimes(_urlTmGetPlace, _data_times);
    }

    private data_times setLocationList(data_times _data_times)
    {
        return callServicePostTimes(_urlTmSetLocation, _data_times);
    }

    private data_times getLocationList(data_times _data_times)
    {
        return callServicePostTimes(_urlTmGetLocation, _data_times);
    }

    private data_times setZoneList(data_times _data_times)
    {
        return callServicePostTimes(_urlTmSetZone, _data_times);
    }

    private data_times getZoneList(data_times _data_times)
    {
        return callServicePostTimes(_urlTmGetZone, _data_times);
    }

    private data_times setGroupList(data_times _data_times)
    {
        return callServicePostTimes(_urlTmSetGroup, _data_times);
    }

    private data_times getGroupList(data_times _data_times)
    {
        return callServicePostTimes(_urlTmGetGroup, _data_times);
    }

    private data_times getPlantList(data_times _data_times)
    {
        return callServicePostTimes(_urlTmGetPlant, _data_times);
    }

    private data_times callServicePostTimes(string _cmdUrl, data_times _data_times)
    {
        // convert to json
        _local_json = _funcTool.convertObjectToJson(_data_times);
        // litDebug.Text = _local_json;

        // call services
        _local_json = _funcTool.callServicePost(_cmdUrl, _local_json);

        // convert json to object
        _data_times = (data_times)_funcTool.convertJsonToObject(typeof(data_times), _local_json);

        return _data_times;
    }

    private data_employee callServicePostEmployee(string _cmdUrl, data_employee _data_employee)
    {
        // convert to json
        _local_json = _funcTool.convertObjectToJson(_data_employee);
        //litDebug.Text = _local_json;

        // call services
        _local_json = _funcTool.callServicePost(_cmdUrl, _local_json);

        // convert json to object
        _data_employee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _local_json);

        return _data_employee;
    }

    public string getStatusIcon(Int32 inStatus)
    {
        return (inStatus == 1 ? "<i class=\"fas fa-check text-success\"></i>" : "<i class=\"fas fa-times text-danger\"></i>");
    }
    #endregion reuse
}