﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

#region data imxls
[Serializable]
[XmlRoot("data_imxls")]
public class data_imxls
{
   [XmlElement("return_code")]
   public int return_code { get; set; }
   [XmlElement("return_msg")]
   public string return_msg { get; set; }
   [XmlElement("return_id_added")]
   public string return_id_added { get; set; }
   [XmlElement("return_inserted_u0_id")]
   public string return_inserted_u0_id { get; set; }
   [XmlElement("return_empcode_stack")]
   public string return_empcode_stack { get; set; }
   [XmlElement("imxls_myimxls_action")]
   public imxls[] imxls_myimxls_action { get; set; }
   [XmlElement("imxls_distribute_action")]
   public imxlsdistribute[] imxls_distribute_action { get; set; }
   [XmlElement("imxls_empcode_stack_action")]
   public imxlsempcodestack[] imxls_empcode_stack_action { get; set; }

}
#endregion data imxls

#region imxls
[Serializable]
public class imxls
{
   [XmlElement("count_row")]
   public int count_row { get; set; }
   [XmlElement("u1_imxls_idx")]
   public int u1_imxls_idx { get; set; }
   [XmlElement("u0_imxls_idx_ref")]
   public int u0_imxls_idx_ref { get; set; }
   [XmlElement("empcode_new")]
   public string empcode_new { get; set; }
   [XmlElement("prefix")]
   public string prefix { get; set; }
   [XmlElement("firstname_th")]
   public string firstname_th { get; set; }
   [XmlElement("lastname_th")]
   public string lastname_th { get; set; }
   [XmlElement("firstname_en")]
   public string firstname_en { get; set; }
   [XmlElement("lastname_en")]
   public string lastname_en { get; set; }
   [XmlElement("emp_in")]
   public string emp_in { get; set; }
   [XmlElement("emp_in_1")]
   public string emp_in_1 { get; set; }
   [XmlElement("emp_in_2")]
   public string emp_in_2 { get; set; }
   [XmlElement("position")]
   public string position { get; set; }
   [XmlElement("section")]
   public string section { get; set; }
   [XmlElement("department")]
   public string department { get; set; }
   [XmlElement("cost_center")]
   public string cost_center { get; set; }
   [XmlElement("sex")]
   public string sex { get; set; }
   [XmlElement("birthday")]
   public string birthday { get; set; }
   [XmlElement("passport")]
   public string passport { get; set; }
   [XmlElement("passport_issued_at")]
   public string passport_issued_at { get; set; }
   [XmlElement("issued_date")]
   public string issued_date { get; set; }
   [XmlElement("passport_exp_date")]
   public string passport_exp_date { get; set; }
   [XmlElement("work_permit_no")]
   public string work_permit_no { get; set; }
   [XmlElement("work_permit_issued_at")]
   public string work_permit_issued_at { get; set; }
   [XmlElement("work_permit_issued_date")]
   public string work_permit_issued_date { get; set; }
   [XmlElement("work_permit_exp_date")]
   public string work_permit_exp_date { get; set; }
   [XmlElement("social_id")]
   public string social_id { get; set; }
   [XmlElement("account_no")]
   public string account_no { get; set; }
   [XmlElement("nationality")]
   public string nationality { get; set; }
   [XmlElement("race")]
   public string race { get; set; }
   [XmlElement("religion")]
   public string religion { get; set; }

   [XmlElement("idx_added")]
   public string idx_added { get; set; }

   [XmlElement("u0_imxls_idx")]
   public int u0_imxls_idx { get; set; }
   [XmlElement("imxls_filename")]
   public string imxls_filename { get; set; }
   [XmlElement("imxls_rows")]
   public int imxls_rows { get; set; }
   [XmlElement("imxls_created_at")]
   public string imxls_created_at { get; set; }
   [XmlElement("imxls_created_by")]
   public int imxls_created_by { get; set; }

   [XmlElement("inserted_u0_id")]
   public string inserted_u0_id { get; set; }

   [XmlElement("imxls_created_at_date")]
   public string imxls_created_at_date { get; set; }
   [XmlElement("imxls_created_at_time")]
   public string imxls_created_at_time { get; set; }

   [XmlElement("EmpIDX")]
   public int EmpIDX { get; set; }
   [XmlElement("RSecIDX")]
   public int RSecIDX { get; set; }
}
#endregion imxls

#region imxlsdistribute
[Serializable]
public class imxlsdistribute
{
   [XmlElement("idx_added")]
   public string idx_added { get; set; }
}
#endregion imxlsdistribute

#region imxlsempcodestack
[Serializable]
public class imxlsempcodestack
{
   [XmlElement("StackIDX")]
   public int StackIDX { get; set; }
   [XmlElement("available_stack_code")]
   public string available_stack_code { get; set; }
   [XmlElement("StackStatus")]
   public int StackStatus { get; set; }
}
#endregion imxlsempcodestack
