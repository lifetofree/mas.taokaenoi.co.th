using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Xml.Serialization;

/// <summary>
/// Summary description for DataMaster
/// </summary>
[Serializable]
[XmlRoot("data_hny")]
public class data_hny
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    [XmlElement("prize_list")]
    public detail_prize[] prize_list { get; set; }
    [XmlElement("prize_emp_list")]
    public detail_prize_emp[] prize_emp_list { get; set; }
}

[Serializable]
public class detail_prize
{
    [XmlElement("midx")]
    public int midx { get; set; }
    [XmlElement("prize_name")]
    public string prize_name { get; set; }
    [XmlElement("prize_type")]
    public int prize_type { get; set; }
    [XmlElement("prize_status")]
    public int prize_status { get; set; }

    [XmlElement("prize_remain")]
    public int prize_remain { get; set; }
    [XmlElement("prize_reward")]
    public int prize_reward { get; set; }
}

[Serializable]
public class detail_prize_emp
{
    [XmlElement("emp_code")]
    public string emp_code { get; set; }
    [XmlElement("emp_name")]
    public string emp_name { get; set; }
    [XmlElement("emp_prize_idx")]
    public int emp_prize_idx { get; set; }
    [XmlElement("emp_prize_status")]
    public int emp_prize_status { get; set; }

    [XmlElement("emp_pos")]
    public string emp_pos { get; set; }
    [XmlElement("emp_sec")]
    public string emp_sec { get; set; }

    [XmlElement("prize_name")]
    public string prize_name { get; set; }

    [XmlElement("emp_prize_rand")]
    public int emp_prize_rand { get; set; }

    [XmlElement("emp_type")]
    public int emp_type { get; set; }
    [XmlElement("emp_search")]
    public string emp_search { get; set; }
}