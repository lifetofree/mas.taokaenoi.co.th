﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="law_trademark_recordsheet.aspx.cs" Inherits="websystem_laws_law_trademark_recordsheet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:HyperLink ID="setOntop" runat="server"></asp:HyperLink>

    <!--tab menu-->
    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default" id="divBtnHead" runat="server" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-bar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="sub-navbar">
                        <a class="navbar-brand" href="#"><b>Menu</b></a>
                    </div>
                </div>

                <!--Collect the nav links, forms, and other content for toggling-->
                <div class="collapse navbar-collapse" id="menu-bar">
                    <ul class="nav navbar-nav" id="uiNav" runat="server">
                        <li id="li0" runat="server">
                            <asp:LinkButton ID="lbRecordSheet" runat="server" CommandName="cmdRecordSheet" OnCommand="navCommand" CommandArgument="docRecordSheet"> RecordSheet</asp:LinkButton>
                        </li>
                        <%-- <li id="li1" runat="server">
                            <asp:LinkButton ID="lbCreate" runat="server" CommandName="cmdCreate" OnCommand="navCommand" CommandArgument="docCreate"> Craete Document</asp:LinkButton>
                        </li>--%>
                    </ul>

                    <%-- <ul class="nav navbar-nav navbar-right" runat="server">

                        <li id="_divMenuLiToDocument" runat="server">
                            <asp:HyperLink ID="_divMenuBtnToDocument" runat="server"
                                NavigateUrl="https://docs.google.com/document/d/1JhPRLQm2gVCwARkpJhTF8Qrfv8zR03-qz1ZVzIGoqJo/edit?usp=sharing" Target="_blank" Text="คู่มือการใช้งาน" />
                        </li>
                    </ul>--%>
                </div>
                <!--Collect the nav links, forms, and other content for toggling-->

            </div>
        </nav>
    </div>
    <!--tab menu-->

    <div class="col-md-12">
        <asp:Literal ID="litDebug" runat="server"></asp:Literal>
        <asp:Literal ID="litDebug1" runat="server"></asp:Literal>
    </div>

    <!--multiview-->
    <asp:MultiView ID="mvSystem" runat="server">

        <!--View Detail-->
        <asp:View ID="docRecordSheet" runat="server">

            <div class="col-md-12" id="div_record" runat="server">

                <div class="col-md-12" id="div_fileshow" runat="server" style="color: transparent;">
                    <asp:FileUpload ID="FileShowUpload" CssClass="btn btn-warning hidden" ClientIDMode="Static" runat="server" accept="jpg|png|pdf" />
                </div>

                <!-- Form view Search Registration Detail -->
                <asp:FormView ID="FvSearchRecordSheet" runat="server" DefaultMode="ReadOnly" Width="100%" Visible="false">
                    <InsertItemTemplate>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Search Detail</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Document Type</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlDocumentType" runat="server" CssClass="form-control">
                                                <%--<asp:ListItem Value="0" Text="ไม่เร่งด่วน" />
                                                <asp:ListItem Value="1" Text="เร่งด่วน" />--%>
                                            </asp:DropDownList>

                                        </div>

                                        <label class="col-sm-2 control-label">Status</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control">
                                                <%--<asp:ListItem Value="0" Text="ไม่เร่งด่วน" />
                                                <asp:ListItem Value="1" Text="เร่งด่วน" />--%>
                                            </asp:DropDownList>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </InsertItemTemplate>
                </asp:FormView>
                <!-- Form view Search Registration Detail -->
                <%-- <div id="Div1" class="media" style="overflow-x: scroll; width: 100%" visible="false" runat="server">--%>
                <div id="gvRecordSheet_scroll" style="overflow-x: auto; width: 100%" runat="server">
                    <asp:GridView ID="gvRecordSheet" runat="server"
                        AutoGenerateColumns="False"
                        CssClass="table table-bordered table-striped table-responsive col-md-12 footable"
                        HeaderStyle-CssClass="success"
                        OnPageIndexChanging="gvPageIndexChanging"
                        OnRowDataBound="gvRowDataBound"
                        AllowPaging="True"
                        PageSize="10">
                        <RowStyle Font-Size="Small" />
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">--- No Data ---</div>
                        </EmptyDataTemplate>
                        <Columns>

                            <asp:TemplateField HeaderText="RecordSheet No." Visible="false" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                <ItemTemplate>
                                    <asp:Label ID="lblu0_doc_idx_record" runat="server" Text='<%#Eval("u0_doc_idx") %>'></asp:Label>
                                    <asp:Label ID="lbl_u1_doc_idx_record" runat="server" Text='<%#Eval("u1_doc_idx") %>'></asp:Label>

                                    <asp:Label ID="lbl_doc_code_record" runat="server" Text='<%#Eval("doc_code") %>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Create Date" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                <ItemTemplate>

                                    <asp:Label ID="lblcreate_date_record" runat="server" Text='<%#Eval("create_date") %>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Country" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                <ItemTemplate>

                                    <asp:Label ID="lblcountry_name_en_record" runat="server" Text='<%#Eval("country_name_en") %>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Document Detail" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="20%">
                                <ItemTemplate>
                                    <p>
                                        <b>Document Request No :</b>
                                        <asp:Label ID="lbl_docrequest_code_record" runat="server" Text='<%# Eval("docrequest_code") %>' />
                                    </p>
                                    <p>
                                        <b>Document Type :</b>
                                        <asp:Label ID="lbltype_name_en_record" runat="server" Text='<%# Eval("type_name_en") %>' />
                                    </p>
                                    <p>
                                        <b>Sub Document Type :</b>
                                        <asp:Label ID="lblsubtype_name_en_record" runat="server" Text='<%# Eval("subtype_name_en") %>' />
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Document Status" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%">
                                <ItemTemplate>
                                    <asp:Label ID="lblcurrent_status_wait" runat="server" Text='<%# Eval("current_status") %>'></asp:Label>
                                    <%--<asp:Label ID="lblDocStatus" Visible="false" runat="server" Text='<%# Eval("status_name") %>'></asp:Label>--%>
                                    <asp:Label ID="lblstaidx_wait" runat="server" Visible="false" Text='<%# Eval("staidx") %>'></asp:Label>
                                    <asp:Label ID="lblnoidx_wait" runat="server" Visible="false" Text='<%# Eval("noidx") %>'></asp:Label>
                                    <asp:Label ID="lblacidx_wait" runat="server" Visible="false" Text='<%# Eval("acidx") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Action" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%">
                                <ItemTemplate>

                                    <asp:UpdatePanel ID="Panel_btnRecordSheet" runat="server">
                                        <ContentTemplate>
                                            <asp:LinkButton ID="btnEditRecordSheet" Visible="false" CssClass="btn btn-warning btn-sm" runat="server" CommandName="cmdEditRecordSheet" OnCommand="btnCommand" CommandArgument='<%# Eval("u0_doc_idx")+ ";" + Eval("u1_doc_idx") + ";" + Eval("noidx") + ";" + Eval("acidx") + ";" + Eval("staidx") + ";" + Eval("cemp_idx") + ";" + Eval("type_idx") %>' data-toggle="tooltip" title="Edit Record"><i class="fa fa-edit"></i></asp:LinkButton>

                                            <asp:LinkButton ID="btnSaveRecordSheet" CssClass="btn btn-success btn-sm" runat="server" CommandName="cmdSaveRecordSheet" OnCommand="btnCommand" CommandArgument='<%# Eval("u0_doc_idx")+ ";" + Eval("u1_doc_idx") + ";" + Eval("noidx") + ";" + Eval("acidx") + ";" + Eval("staidx") + ";" + Eval("cemp_idx") + ";" + Eval("type_idx") %>' data-toggle="tooltip" title="Save Record"><i class="fa fa-save"></i></asp:LinkButton>

                                            <asp:LinkButton ID="btnViewRecordSheet" CssClass="btn btn-info btn-sm" runat="server" CommandName="cmdViewRecordSheet" OnCommand="btnCommand" CommandArgument='<%# Eval("u0_doc_idx")+ ";" + Eval("u1_doc_idx") + ";" + Eval("noidx") + ";" + Eval("acidx") + ";" + Eval("staidx") + ";" + Eval("cemp_idx") + ";" + Eval("type_idx") %>' data-toggle="tooltip" title="View"><i class="fa fa-edit"></i></asp:LinkButton>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnSaveRecordSheet" />
                                            <asp:PostBackTrigger ControlID="btnViewRecordSheet" />
                                        </Triggers>
                                    </asp:UpdatePanel>

                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>
                </div>

                <!-- Back To Detail Record Sheet -->
                <asp:UpdatePanel ID="Update_BackToDetail" runat="server">
                    <ContentTemplate>
                        <div class="form-group">
                            <asp:LinkButton ID="btnBackToDetail" CssClass="btn btn-default" data-toggle="tooltip" title="Back" runat="server"
                                CommandName="cmdBackToDetail" OnCommand="btnCommand"><i class="fa fa-reply" aria-hidden="true"></i> Back</asp:LinkButton>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- Back To Detail Record Sheet -->

                <asp:UpdatePanel ID="Panel_HeaderRecordSheet" runat="server">
                    <ContentTemplate>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <center><h3 class="panel-title" style="font-size:18px;">Record Sheet</h3></center>
                            </div>
                        </div>

                    </ContentTemplate>
                </asp:UpdatePanel>

                <!-- Form view Registration Detail -->
                <div id="div_RegistrationDetail" runat="server" style="overflow-x: auto; width: 100%">
                    <asp:FormView ID="fvRegistrationDetail" runat="server" DefaultMode="ReadOnly" Width="100%" OnDataBound="FvDetail_DataBound">
                        <EditItemTemplate>
                            <asp:HiddenField ID="hfU0IDX" runat="server" Value='<%# Eval("u0_doc_idx") %>' />
                            <asp:HiddenField ID="hfU1IDX" runat="server" Value='<%# Eval("u1_doc_idx") %>' />
                            <asp:HiddenField ID="hfACIDX" runat="server" Value='<%# Eval("acidx") %>' />
                            <asp:HiddenField ID="hfNOIDX" runat="server" Value='<%# Eval("noidx") %>' />
                            <asp:HiddenField ID="hfSTAIDX" runat="server" Value='<%# Eval("staidx") %>' />
                            <asp:HiddenField ID="hftype_idx" runat="server" Value='<%# Eval("type_idx") %>' />
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Registration Details</h3>
                                </div>
                                <div class="panel-body">

                                    <div class="form-horizontal" role="form">

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Document Request Code</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbdocrequest_code_detail" runat="server" CssClass="form-control" Text='<%# Eval("docrequest_code") %>' Enabled="false" />
                                            </div>
                                            <label class="col-sm-2 control-label">Create Date</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbcreate_date_u0_detail" runat="server" CssClass="form-control" Text='<%# Eval("create_date_u0") %>' Enabled="false" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Document Type</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbtype_idx_viewdetail" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("type_idx") %>' Enabled="true" />
                                                <asp:TextBox ID="tbdocrequest_code_viewdetail" runat="server" CssClass="form-control" Text='<%# Eval("type_name_en") %>' Enabled="false" />
                                            </div>
                                            <label class="col-sm-2 control-label">Sub Document Type</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbcreate_date_viewdetail" runat="server" CssClass="form-control" Text='<%# Eval("subtype_name_en") %>' Enabled="false" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Owner</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbowner_idx" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("owner_idx") %>' Enabled="true" />
                                                <asp:DropDownList ID="ddlowner_idx" AutoPostBack="true" Enabled="true" runat="server" ValidationGroup="SaveRecordsheet" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>
                                            <label class="col-sm-2 control-label">Owner Address</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbowner_address_en" runat="server" TextMode="MultiLine" Rows="2" CssClass="form-control" Text='<%# Eval("owner_address_en") %>' Enabled="false" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Incorporated in</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbincorporated_in_idx" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("incorporated_in_idx") %>' Enabled="true" />
                                                <asp:DropDownList ID="ddlincorporated_in_idx" Enabled="true" AutoPostBack="true" runat="server" ValidationGroup="SaveRecordsheet" CssClass="form-control"></asp:DropDownList>
                                            </div>
                                            <%--<label class="col-sm-2 control-label">Owner Address</label>--%>
                                            <div class="col-sm-6">
                                                <%--<asp:TextBox ID="TextBox2" runat="server" CssClass="form-control" Text='<%# Eval("subtype_name_en") %>' Enabled="false" />--%>
                                            </div>
                                        </div>

                                        <hr />
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Address Of Record</h3>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Country</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbcountry_name_en" runat="server" CssClass="form-control" Text='<%# Eval("country_name_en") %>' Enabled="false" />
                                            </div>
                                            <label class="col-sm-2 control-label"></label>
                                            <div class="col-sm-4">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Supervisors</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbemp_name_en_head" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("emp_name_en_head") %>' />
                                            </div>
                                            <label class="col-sm-2 control-label">Supervisors</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbemp_name_en_mgemp" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("emp_name_en_mgemp") %>' />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Application No</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="txt_application_no" runat="server" CssClass="form-control" placeholder="Application No ..." Enabled="true" Text='<%# Eval("application_no") %>' />
                                            </div>
                                            <label class="col-sm-2 control-label">Application Date</label>
                                            <div class="col-sm-4">
                                                <div class='input-group date'>
                                                    <asp:TextBox ID="txt_application_date" runat="server" placeholder="Application Date ..." CssClass="form-control from-date-datepicker cursor-pointer" Enabled="true" Text='<%# Eval("application_date") %>' />
                                                    <span class="input-group-addon show-from-onclick">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                                <%--<asp:TextBox ID="TextBox2" runat="server" CssClass="form-control" Text='<%# Eval("subtype_name_en") %>' Enabled="false" />--%>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Registration No</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="txt_registration_no" runat="server" Enabled="true" CssClass="form-control" Text='<%# Eval("registration_no") %>' />
                                            </div>
                                            <label class="col-sm-2 control-label">Registration Date</label>
                                            <div class="col-sm-4">
                                                <div class='input-group date'>
                                                    <asp:TextBox ID="txt_registration_date" runat="server" Enabled="true" CssClass="form-control from-date-datepicker cursor-pointer" Text='<%# Eval("registration_date") %>' />
                                                    <span class="input-group-addon show-from-onclick">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Registration Status</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="txt_tradmark_status" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("tradmark_status") %>' Enabled="false" />
                                                <asp:DropDownList ID="ddl_tradmark_status" Enabled="true" AutoPostBack="true" runat="server" ValidationGroup="SaveRecordsheet" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>
                                            <label class="col-sm-2 control-label">Registration Sub Status</label>
                                            <%--SelectedValue='<%# Eval("trademark_substatus_idx") %>' --%>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="txt_tradmark_substatus" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("tradmark_substatus") %>' Enabled="false" />
                                                <asp:DropDownList ID="ddl_tradmark_substatus" AutoPostBack="true" Enabled="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="form-group">

                                            <label class="col-sm-2 control-label">Type Of Registrtion</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="txt_type_of_registration_idx" Visible="false" Enabled="false" runat="server" CssClass="form-control" Text='<%# Eval("type_of_registration_idx") %>' />
                                                <asp:DropDownList ID="ddlTypeOfRegistrtion" Enabled="true" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>
                                            <label class="col-sm-6 control-label"></label>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">File Reference</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="txt_file_reference" Enabled="true" runat="server" placeholder="File Reference ..." CssClass="form-control" Text='<%# Eval("file_reference") %>' />
                                            </div>
                                            <label class="col-sm-2 control-label">Record Reference</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="txt_recode_referense" Enabled="true" runat="server" placeholder="Record Reference ..." CssClass="form-control" Text='<%# Eval("recode_referense") %>' />
                                            </div>
                                        </div>

                                        <hr />
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Renewal Detail</label>
                                            <div class="col-sm-10">

                                                <asp:GridView ID="gvDetailRenewal" runat="server"
                                                    AutoGenerateColumns="False"
                                                    CssClass="table table-bordered table-striped table-responsive col-md-12 footable"
                                                    HeaderStyle-CssClass="success"
                                                    OnPageIndexChanging="gvPageIndexChanging"
                                                    OnRowDataBound="gvRowDataBound"
                                                    OnRowUpdating="gvRowUpdating"
                                                    OnRowEditing="gvRowEditing"
                                                    OnRowCancelingEdit="gvRowCancelingEdit"
                                                    AllowPaging="True"
                                                    PageSize="10">
                                                    <RowStyle Font-Size="Small" />
                                                    <PagerStyle CssClass="pageCustom" />
                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">--- No Data ---</div>
                                                    </EmptyDataTemplate>

                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Next Renewal Due" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                            <ItemTemplate>
                                                                <%--  <asp:Label ID="lblu0_doc_idx_record" runat="server" Text='<%#Eval("u0_doc_idx") %>'></asp:Label>
                                                            <asp:Label ID="lbl_u1_doc_idx_record" runat="server" Text='<%#Eval("u1_doc_idx") %>'></asp:Label>--%>
                                                                <asp:Label ID="lbl_u1_doc_idx" runat="server" Visible="false" Text='<%#Eval("u1_doc_idx") %>'></asp:Label>
                                                                <asp:Label ID="lblrenewal_idx" runat="server" Visible="false" Text='<%#Eval("renewal_idx") %>'></asp:Label>
                                                                <asp:Label ID="lbl_NextRenewalDue_date" runat="server" Text='<%#Eval("NextRenewalDue_date") %>'></asp:Label>

                                                            </ItemTemplate>

                                                            <EditItemTemplate>

                                                                <div class="panel-body">
                                                                    <div class="form-horizontal" role="form">

                                                                        <div class="form-group">
                                                                            <label class="col-sm-2 control-label">Next Renewal Due</label>
                                                                            <div class="col-sm-4">
                                                                                <div class='input-group date'>
                                                                                    <asp:Label ID="lblrenewal_idx" runat="server" Visible="false" Text='<%#Eval("renewal_idx") %>'></asp:Label>
                                                                                    <asp:TextBox ID="tb_Next_Renewal_Due_edit" Text='<%# Eval("NextRenewalDue_date") %>' runat="server" placeholder="Next Renewal Due ..." CssClass="form-control from-date-datepicker cursor-pointer" />

                                                                                    <span class="input-group-addon show-from-onclick">
                                                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                                                    </span>

                                                                                    <asp:RequiredFieldValidator ID="Re_tb_Next_Renewal_Due_edit"
                                                                                        runat="server"
                                                                                        ControlToValidate="tb_Next_Renewal_Due_edit" Display="None" SetFocusOnError="true"
                                                                                        ErrorMessage="*Next Renewal Due"
                                                                                        ValidationGroup="UpdateRenewal" />
                                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1233" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_tb_Next_Renewal_Due_edit" Width="200" PopupPosition="BottomLeft" />
                                                                                </div>
                                                                            </div>
                                                                            <label class="col-sm-2 control-label">Selected</label>
                                                                            <div class="col-sm-4 control-label textleft">
                                                                                <asp:Label ID="lbl_Selected_value" runat="server" Visible="false" Text='<%#Eval("Selected_value") %>'></asp:Label>
                                                                                <asp:RadioButtonList ID="rdoRenewal_edit" Font-Bold="true" Font-Size="Small" runat="server" CssClass="">
                                                                                    <asp:ListItem Text=" ALL REGISTERED GOODS & SERVICES" Value="2" />
                                                                                    <asp:ListItem Text=" SOME REGISTERED GOODS & SERVICES" Value="1" />
                                                                                    <asp:ListItem Text=" No Selected" Value="0" />
                                                                                </asp:RadioButtonList>

                                                                                <asp:RequiredFieldValidator ID="Req_rdoRenewal_edit"
                                                                                    runat="server"
                                                                                    ControlToValidate="rdoRenewal_edit" Display="None" SetFocusOnError="true"
                                                                                    ErrorMessage="*Please Check Selected"
                                                                                    ValidationGroup="UpdateRenewal" />
                                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender101" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_rdoRenewal_edit" Width="200" PopupPosition="BottomLeft" />
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label class="col-sm-2 control-label">Renewal Status</label>
                                                                            <div class="col-sm-4">
                                                                                <asp:Label ID="lbl_Renewal_Status_edit" runat="server" Visible="false" Text='<%#Eval("Renewal_Status") %>'></asp:Label>
                                                                                <asp:DropDownList ID="ddlRenewalStatus_edit" AutoPostBack="true" placeholder="Trademark Status ..." runat="server" ValidationGroup="UpdateRenewal" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                                </asp:DropDownList>

                                                                                <asp:RequiredFieldValidator ID="Req_ddlRenewalStatus_edit"
                                                                                    runat="server" InitialValue="0"
                                                                                    ControlToValidate="ddlRenewalStatus_edit" Display="None" SetFocusOnError="true"
                                                                                    ErrorMessage="*Select Renewal Status"
                                                                                    ValidationGroup="UpdateRenewal" />
                                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1011" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlRenewalStatus_edit" Width="200" PopupPosition="BottomLeft" />
                                                                            </div>
                                                                            <label class="col-sm-2 control-label">Renewal SubStatus</label>
                                                                            <div class="col-sm-4">
                                                                                <asp:Label ID="lbl_Renewal_SubStatus_edit" runat="server" Visible="false" Text='<%#Eval("Renewal_SubStatus") %>'></asp:Label>
                                                                                <asp:DropDownList ID="ddlRenewalSubStatus_edit" AutoPostBack="true" runat="server" placeholder="Trademark Sub Status ..." ValidationGroup="UpdateRenewal" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                                </asp:DropDownList>

                                                                                <asp:RequiredFieldValidator ID="Req_ddlRenewalSubStatus_edit"
                                                                                    runat="server" InitialValue="0"
                                                                                    ControlToValidate="ddlRenewalSubStatus_edit" Display="None" SetFocusOnError="true"
                                                                                    ErrorMessage="*Select Renewal SubStatus"
                                                                                    ValidationGroup="UpdateRenewal" />
                                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlRenewalSubStatus_edit" Width="200" PopupPosition="BottomLeft" />
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <div class="col-sm-2"></div>
                                                                            <div class="col-sm-10">
                                                                                <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" ValidationGroup="UpdateRenewal" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" CommandName="Update"></asp:LinkButton>
                                                                                <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                                                    Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </EditItemTemplate>

                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Selected" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                            <ItemTemplate>

                                                                <asp:Label ID="lblSelected_name" runat="server" Text='<%#Eval("Selected_name") %>'></asp:Label>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Renewal Status" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                            <ItemTemplate>

                                                                <asp:Label ID="lbltrademark_status_en" runat="server" Text='<%#Eval("trademark_status_en") %>'></asp:Label>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Renewal SubStatus" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                            <ItemTemplate>


                                                                <asp:Label ID="lbl_trademark_substatus_en" runat="server" Text='<%#Eval("trademark_substatus_en") %>'></asp:Label>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Manage" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                            <ItemTemplate>

                                                                <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                                                    data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                                                            <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>

                                                                <asp:LinkButton ID="btnDeleteRenewal" CssClass="text-trash" runat="server" CommandName="CmdDeleteRenewal"
                                                                    data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('Delete data?')"
                                                                    CommandArgument='<%#Eval("renewal_idx") %>' title="delete"><i class="glyphicon glyphicon-trash"></i>

                                                                </asp:LinkButton>


                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                    </Columns>
                                                </asp:GridView>

                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"></label>
                                            <div class="col-sm-4 control-label textleft">

                                                <asp:CheckBox ID="chkAddRenewalDate" runat="server" Font-Bold="false" Text=" Add Renewal" OnCheckedChanged="chkSelectedIndexChanged" AutoPostBack="true" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right" />

                                            </div>
                                        </div>

                                        <asp:UpdatePanel ID="Panel_AddRenewalDate" runat="server" Visible="false">
                                            <ContentTemplate>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Next Renewal Due</label>
                                                    <div class="col-sm-4">
                                                        <div class='input-group date'>
                                                            <asp:TextBox ID="tb_Next_Renewal_Due" runat="server" placeholder="Next Renewal Due ..." CssClass="form-control from-date-datepicker cursor-pointer" />

                                                            <span class="input-group-addon show-from-onclick">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>

                                                            <asp:RequiredFieldValidator ID="Re_tb_Next_Renewal_Due"
                                                                runat="server"
                                                                ControlToValidate="tb_Next_Renewal_Due" Display="None" SetFocusOnError="true"
                                                                ErrorMessage="*Next Renewal Due"
                                                                ValidationGroup="addRenewalList" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1233" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_tb_Next_Renewal_Due" Width="200" PopupPosition="BottomLeft" />

                                                        </div>
                                                    </div>
                                                    <label class="col-sm-2 control-label">Selected</label>
                                                    <div class="col-sm-4 control-label textleft">

                                                        <asp:RadioButtonList ID="rdoRenewal" Font-Bold="true" Font-Size="Small" runat="server" CssClass="">
                                                            <asp:ListItem Text=" ALL REGISTERED GOODS & SERVICES" Value="2" />
                                                            <asp:ListItem Text=" SOME REGISTERED GOODS & SERVICES" Value="1" />
                                                            <asp:ListItem Text=" No Selected" Value="0" />
                                                        </asp:RadioButtonList>

                                                        <asp:RequiredFieldValidator ID="Req_rdoRenewal"
                                                            runat="server"
                                                            ControlToValidate="rdoRenewal" Display="None" SetFocusOnError="true"
                                                            ErrorMessage="*Please Check Selected"
                                                            ValidationGroup="addRenewalList" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1033" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_rdoRenewal" Width="200" PopupPosition="BottomLeft" />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Renewal Status</label>
                                                    <div class="col-sm-4">

                                                        <asp:DropDownList ID="ddlRenewalStatus" AutoPostBack="true" placeholder="Trademark Status ..." runat="server" ValidationGroup="addRenewalList" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                        </asp:DropDownList>

                                                        <asp:RequiredFieldValidator ID="Req_ddlRenewalStatus"
                                                            runat="server" InitialValue="0"
                                                            ControlToValidate="ddlRenewalStatus" Display="None" SetFocusOnError="true"
                                                            ErrorMessage="*Select Renewal Status"
                                                            ValidationGroup="addRenewalList" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlRenewalStatus" Width="200" PopupPosition="BottomLeft" />


                                                    </div>
                                                    <label class="col-sm-2 control-label">Renewal SubStatus</label>
                                                    <div class="col-sm-4">

                                                        <asp:DropDownList ID="ddlRenewalSubStatus" AutoPostBack="true" runat="server" placeholder="Trademark Sub Status ..." ValidationGroup="addRenewalList" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                        </asp:DropDownList>

                                                        <asp:RequiredFieldValidator ID="Req_ddlRenewalSubStatus"
                                                            runat="server" InitialValue="0"
                                                            ControlToValidate="ddlRenewalSubStatus" Display="None" SetFocusOnError="true"
                                                            ErrorMessage="*Select Renewal SubStatus"
                                                            ValidationGroup="addRenewalList" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlRenewalSubStatus" Width="200" PopupPosition="BottomLeft" />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label"></label>
                                                    <div class="col-sm-2">
                                                        <asp:LinkButton ID="btnAddRenewalTolist" runat="server" CssClass="btn btn-primary" Text="Add Renewal" OnCommand="btnCommand"
                                                            CommandName="cmdAddRenewalList" ValidationGroup="addRenewalList" />
                                                    </div>
                                                    <label class="col-sm-8 control-label"></label>

                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label"></label>
                                                    <div class="col-sm-10">

                                                        <asp:GridView ID="gvRenewalList"
                                                            runat="server"
                                                            CssClass="table table-bordered table-striped table-responsive col-md-12"
                                                            HeaderStyle-CssClass="info"
                                                            OnRowCommand="onRowCommand"
                                                            AutoGenerateColumns="false">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center"
                                                                    HeaderStyle-CssClass="text-center"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller">
                                                                    <ItemTemplate>
                                                                        <%# (Container.DataItemIndex + 1) %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:BoundField DataField="drNextRenewalDueText" HeaderText="Next Renewal Due" ItemStyle-CssClass="left"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                                <asp:BoundField DataField="drSelectedText" HeaderText="Selected" ItemStyle-CssClass="left"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                                <asp:BoundField DataField="drRenewalStatusText" HeaderText="Renewal Status" ItemStyle-CssClass="left"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                                <asp:BoundField DataField="drRenewalSubStatusText" HeaderText="Renewal SubStatus" ItemStyle-CssClass="left"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                                <asp:TemplateField ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="btnRemoveRenewal" runat="server"
                                                                            CssClass="btn btn-danger btn-xs"
                                                                            OnClientClick="return confirm('Delete data Renewal?')"
                                                                            CommandName="cmdRemoveRenewal"><i class="fa fa-times"></i>
                                                                        </asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>

                                                    </div>
                                                </div>

                                            </ContentTemplate>
                                        </asp:UpdatePanel>

                                    </div>

                                </div>
                            </div>
                        </EditItemTemplate>
                        <InsertItemTemplate>

                            <div class="panel panel-default">

                                <asp:HiddenField ID="hfU0IDX" runat="server" Value='<%# Eval("u0_doc_idx") %>' />
                                <asp:HiddenField ID="hfU1IDX" runat="server" Value='<%# Eval("u1_doc_idx") %>' />
                                <asp:HiddenField ID="hfACIDX" runat="server" Value='<%# Eval("acidx") %>' />
                                <asp:HiddenField ID="hfNOIDX" runat="server" Value='<%# Eval("noidx") %>' />
                                <asp:HiddenField ID="hfSTAIDX" runat="server" Value='<%# Eval("staidx") %>' />
                                <asp:HiddenField ID="hftype_idx" runat="server" Value='<%# Eval("type_idx") %>' />

                                <div class="panel-heading">
                                    <h3 class="panel-title">Registration Details</h3>
                                </div>
                                <div class="panel-body">

                                    <div class="form-horizontal" role="form">

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Document Request Code</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbdocrequest_code_detail" runat="server" CssClass="form-control" Enabled="false" />
                                            </div>
                                            <label class="col-sm-2 control-label">Create Date</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbcreate_date_u0_detail" runat="server" CssClass="form-control" Enabled="false" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Document Type</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbtype_name_en" runat="server" CssClass="form-control" Enabled="false" />
                                            </div>
                                            <label class="col-sm-2 control-label">Sub Document Type</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbsubtype_name_en" runat="server" CssClass="form-control" Enabled="false" />
                                            </div>
                                        </div>

                                        <asp:UpdatePanel ID="Panel_test" runat="server">
                                            <ContentTemplate>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Owner</label>
                                                    <div class="col-sm-4">
                                                        <%--<asp:TextBox ID="tbowner_idx" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("owner_idx") %>' Enabled="false" />--%>
                                                        <asp:DropDownList ID="ddlowner_idx" AutoPostBack="true" runat="server" ValidationGroup="SaveCreateRecordSheet" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                        </asp:DropDownList>

                                                        <asp:RequiredFieldValidator ID="Req_ddlowner_idx"
                                                            runat="server" ControlToValidate="ddlowner_idx" InitialValue="0"
                                                            Display="None" SetFocusOnError="true"
                                                            ErrorMessage="*Select Owner"
                                                            ValidationGroup="SaveCreateRecordSheet" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlowner_idx" Width="200" PopupPosition="BottomLeft" />

                                                    </div>
                                                    <label class="col-sm-2 control-label">Owner Address</label>
                                                    <div class="col-sm-4">
                                                        <asp:TextBox ID="tbowner_address_en" runat="server" placeholder="Owner Address ..." TextMode="MultiLine" Rows="2" CssClass="form-control" Enabled="false" />
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Incorporated in</label>
                                                    <div class="col-sm-4">
                                                        <asp:TextBox ID="tbincorporated_in_idx" Visible="false" runat="server" CssClass="form-control" Enabled="false" />
                                                        <asp:DropDownList ID="ddlincorporated_in_idx" AutoPostBack="true" runat="server" ValidationGroup="SaveCreateRecordSheet" CssClass="form-control"></asp:DropDownList>

                                                        <asp:RequiredFieldValidator ID="Req_ddlincorporated_in_idx"
                                                            runat="server" ControlToValidate="ddlincorporated_in_idx" InitialValue="0"
                                                            Display="None" SetFocusOnError="true"
                                                            ErrorMessage="*Select Incorporated in"
                                                            ValidationGroup="SaveCreateRecordSheet" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlincorporated_in_idx" Width="200" PopupPosition="BottomLeft" />

                                                    </div>
                                                    <%--<label class="col-sm-2 control-label">Owner Address</label>--%>
                                                    <div class="col-sm-6">
                                                        <%--<asp:TextBox ID="TextBox2" runat="server" CssClass="form-control" Text='<%# Eval("subtype_name_en") %>' Enabled="false" />--%>
                                                    </div>
                                                </div>

                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="ddlowner_idx" EventName="SelectedIndexChanged" />
                                                <asp:AsyncPostBackTrigger ControlID="ddlincorporated_in_idx" EventName="SelectedIndexChanged" />
                                                <%--<asp:PostBackTrigger ControlID="btnSaveTrademarkRecordSheet" />--%>
                                            </Triggers>
                                        </asp:UpdatePanel>

                                        <hr />
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Address Of Record</h3>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Country</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbcountry_name_en" runat="server" CssClass="form-control" Enabled="false" />


                                            </div>
                                            <label class="col-sm-2 control-label"></label>
                                            <div class="col-sm-4">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Supervisors</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbemp_name_en_head" runat="server" Enabled="false" CssClass="form-control" />
                                            </div>
                                            <label class="col-sm-2 control-label">Supervisors</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbemp_name_en_mgemp" runat="server" Enabled="false" CssClass="form-control" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Application No</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="txt_application_no" runat="server" placeholder="Application No ..." CssClass="form-control" />
                                            </div>
                                            <label class="col-sm-2 control-label">Application Date</label>
                                            <div class="col-sm-4">
                                                <div class='input-group date'>
                                                    <asp:TextBox ID="txt_application_date" runat="server" placeholder="Application Date ..." CssClass="form-control from-date-datepicker cursor-pointer" />
                                                    <span class="input-group-addon show-from-onclick">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                    <%--<asp:TextBox ID="TextBox2" runat="server" CssClass="form-control" Text='<%# Eval("subtype_name_en") %>' Enabled="false" />--%>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Registration No</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="txt_registration_no" runat="server" placeholder="Registration No ..." CssClass="form-control" />
                                            </div>
                                            <label class="col-sm-2 control-label">Registration Date</label>
                                            <div class="col-sm-4">
                                                <div class='input-group date'>
                                                    <asp:TextBox ID="txt_registration_date" runat="server" placeholder="Registration Date ..." CssClass="form-control from-date-datepicker cursor-pointer" />

                                                    <span class="input-group-addon show-from-onclick">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Registration Status</label>
                                            <div class="col-sm-4">

                                                <asp:DropDownList ID="ddlTrademarkStatus" AutoPostBack="true" placeholder="Trademark Status ..." runat="server" ValidationGroup="SaveRecordsheet" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>
                                            <label class="col-sm-2 control-label">Registration SubStatus</label>
                                            <div class="col-sm-4">

                                                <asp:DropDownList ID="ddlTrademarkSubStatus" AutoPostBack="true" runat="server" placeholder="Trademark Sub Status ..." ValidationGroup="SaveRecordsheet" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <%-- <label class="col-sm-2 control-label">Next Renewal Due</label>
                                        <div class="col-sm-4">
                                            <div class='input-group date'>
                                                <asp:TextBox ID="TextBox1" runat="server" placeholder="Next Renewal Due ..." CssClass="form-control from-date-datepicker cursor-pointer" />

                                                <span class="input-group-addon show-from-onclick">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>--%>
                                            <label class="col-sm-2 control-label">Type Of Registrtion</label>
                                            <div class="col-sm-4">
                                                <asp:DropDownList ID="ddlTypeOfRegistrtion" AutoPostBack="true" runat="server" ValidationGroup="SaveRecordsheet" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                </asp:DropDownList>

                                            </div>
                                            <label class="col-sm-6 control-label"></label>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">File Reference</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tb_File_Reference" runat="server" placeholder="File Reference ..." CssClass="form-control" />
                                            </div>
                                            <label class="col-sm-2 control-label">Record Reference</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tb_Record_Reference" runat="server" placeholder="Record Reference ..." CssClass="form-control" />
                                            </div>
                                        </div>

                                        <hr />
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Next Renewal Due</label>
                                            <div class="col-sm-4">
                                                <div class='input-group date'>
                                                    <asp:TextBox ID="tb_Next_Renewal_Due" runat="server" ValidationGroup="addRenewalList" placeholder="Next Renewal Due ..." CssClass="form-control from-date-datepicker-top cursor-pointer" />

                                                    <span class="input-group-addon show-from-top-onclick">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>

                                                    <asp:RequiredFieldValidator ID="Req_tb_Next_Renewal_Due"
                                                        runat="server" ControlToValidate="tb_Next_Renewal_Due"
                                                        Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*Next Renewal Due"
                                                        ValidationGroup="addRenewalList" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_tb_Next_Renewal_Due" Width="200" PopupPosition="BottomLeft" />
                                                </div>
                                            </div>
                                            <label class="col-sm-2 control-label">Selected</label>
                                            <div class="col-sm-4 control-label textleft">

                                                <asp:RadioButtonList ID="rdoRenewal" Font-Bold="true" Font-Size="Small" runat="server" CssClass="">
                                                    <asp:ListItem Text=" ALL REGISTERED GOODS & SERVICES" Value="2" />
                                                    <asp:ListItem Text=" SOME REGISTERED GOODS & SERVICES" Value="1" />
                                                    <asp:ListItem Text=" No Selected" Value="0" />
                                                </asp:RadioButtonList>

                                                <asp:RequiredFieldValidator ID="Req_rdoRenewal"
                                                    runat="server" ControlToValidate="rdoRenewal"
                                                    Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*Select Check Selected"
                                                    ValidationGroup="addRenewalList" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender14" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_rdoRenewal" Width="200" PopupPosition="BottomLeft" />

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Renewal Status</label>
                                            <div class="col-sm-4">
                                                <asp:DropDownList ID="ddlRenewalStatus" AutoPostBack="true" placeholder="Renewal Status ..." runat="server" ValidationGroup="addRenewalList" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="Req_ddlRenewalStatus"
                                                    runat="server" ControlToValidate="ddlRenewalStatus" InitialValue="0"
                                                    Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*Select Renewal Status"
                                                    ValidationGroup="addRenewalList" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlRenewalStatus" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                            <label class="col-sm-2 control-label">Renewal SubStatus</label>
                                            <div class="col-sm-4">

                                                <asp:DropDownList ID="ddlRenewalSubStatus" AutoPostBack="true" runat="server" placeholder="Renewal SubStatus ..." ValidationGroup="addRenewalList" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                </asp:DropDownList>

                                                <asp:RequiredFieldValidator ID="Req_ddlRenewalSubStatus"
                                                    runat="server" ControlToValidate="ddlRenewalSubStatus" InitialValue="0"
                                                    Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*Select Renewal SubStatus"
                                                    ValidationGroup="addRenewalList" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender13" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlRenewalSubStatus" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"></label>
                                            <div class="col-sm-2">
                                                <asp:LinkButton ID="btnAddRenewalTolist" runat="server" CssClass="btn btn-primary" Text="Add Renewal" OnCommand="btnCommand"
                                                    CommandName="cmdAddRenewalList" ValidationGroup="addRenewalList" />
                                            </div>
                                            <label class="col-sm-8 control-label"></label>

                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"></label>
                                            <div class="col-sm-10">

                                                <asp:GridView ID="gvRenewalList"
                                                    runat="server"
                                                    CssClass="table table-bordered table-striped table-responsive col-md-12"
                                                    HeaderStyle-CssClass="info"
                                                    OnRowCommand="onRowCommand"
                                                    AutoGenerateColumns="false">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center"
                                                            HeaderStyle-CssClass="text-center"
                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller">
                                                            <ItemTemplate>
                                                                <%# (Container.DataItemIndex + 1) %>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:BoundField DataField="drNextRenewalDueText" HeaderText="Next Renewal Due" ItemStyle-CssClass="left"
                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                        <asp:BoundField DataField="drSelectedText" HeaderText="Selected" ItemStyle-CssClass="left"
                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                        <asp:BoundField DataField="drRenewalStatusText" HeaderText="Renewal Status" ItemStyle-CssClass="left"
                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                        <asp:BoundField DataField="drRenewalSubStatusText" HeaderText="Renewal SubStatus" ItemStyle-CssClass="left"
                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                        <asp:TemplateField ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="btnRemoveRenewal" runat="server"
                                                                    CssClass="btn btn-danger btn-xs"
                                                                    OnClientClick="return confirm('Delete data Renewal?')"
                                                                    CommandName="cmdRemoveRenewal"><i class="fa fa-times"></i>
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>

                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>

                        </InsertItemTemplate>

                    </asp:FormView>
                </div>
                <!-- Form view Registration Detail -->

                <!-- Form view Publication Detail -->
                <%--<div id="div_Publication" runat="server" style="overflow: auto; height: auto; min-width:inherit" class="media">--%>
                <asp:UpdatePanel ID="UpPanel_Publication" runat="server">
                    <ContentTemplate>
                        <div id="div_Publication" runat="server" style="overflow-x: auto; width: 100%">
                            <asp:FormView ID="fvPublicationDetail" runat="server" DefaultMode="ReadOnly" Width="100%" OnDataBound="FvDetail_DataBound">

                                <EditItemTemplate>
                                    <div class="panel panel-default">

                                        <asp:HiddenField ID="hfU1IDX" runat="server" Value='<%# Eval("u1_doc_idx") %>' />

                                        <div class="panel-heading">
                                            <h3 class="panel-title">Publication Details</h3>
                                        </div>

                                        <div class="panel-body">

                                            <div class="form-horizontal" role="form">

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Publication Date</label>
                                                    <div class="col-sm-4">

                                                        <asp:TextBox ID="txt_u1_doc_idx" runat="server" Visible="false" Enabled="false" Text='<%# Eval("u1_doc_idx") %>' CssClass="form-control" />
                                                        <asp:TextBox ID="txt_publication_idx" runat="server" Visible="false" Enabled="false" Text='<%# Eval("publication_idx") %>' CssClass="form-control" />

                                                        <div class='input-group date'>

                                                            <asp:TextBox ID="tbPublication_Date" runat="server" Enabled="true" Text='<%# Eval("Publication_Date") %>' placeholder="Publication Date ..." CssClass="form-control from-date-datepicker cursor-pointer" />
                                                            <span class="input-group-addon show-from-onclick">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <label class="col-sm-2 control-label">Notice of Allowance/Grant date</label>
                                                    <div class="col-sm-4">
                                                        <div class='input-group date'>
                                                            <asp:TextBox ID="tbGrant_date" runat="server" Enabled="true" Text='<%# Eval("NoticeofAllowance_Date") %>' placeholder="Notice of Allowance/Grant date ..." CssClass="form-control from-date-datepicker cursor-pointer" />
                                                            <span class="input-group-addon show-from-onclick">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Journal Volume</label>
                                                    <div class="col-sm-4">
                                                        <asp:TextBox ID="tbJournal_Volume" runat="server" Enabled="true" Text='<%# Eval("Journal_Volume") %>' placeholder="Journal Volume ..." CssClass="form-control" />
                                                    </div>
                                                    <label class="col-sm-2 control-label">Pub. Reg. Date</label>
                                                    <div class="col-sm-4">
                                                        <div class='input-group date'>
                                                            <asp:TextBox ID="tb_Pub_Date" runat="server" Enabled="true" Text='<%# Eval("PubRegDate_Date") %>' placeholder="Pub. Reg. Date ..." CssClass="form-control from-date-datepicker cursor-pointer" />
                                                            <span class="input-group-addon show-from-onclick">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Registry Reference</label>
                                                    <div class="col-sm-4">
                                                        <asp:TextBox ID="tb_Registry_Reference" runat="server" Enabled="true" Text='<%# Eval("Registry_Reference") %>' placeholder="Registry Reference ..." CssClass="form-control" />
                                                    </div>
                                                    <label class="col-sm-2 control-label">Final Office Action Date</label>
                                                    <div class="col-sm-4">
                                                        <div class='input-group date'>
                                                            <asp:TextBox ID="tbFinal_Office_Date" runat="server" Enabled="true" Text='<%# Eval("FinalOfficeAction_Date") %>' placeholder="Final Office Action Date ..." CssClass="form-control from-date-datepicker cursor-pointer" />
                                                            <span class="input-group-addon show-from-onclick">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Date Declaration Filed</label>
                                                    <div class="col-sm-4">
                                                        <div class='input-group date'>
                                                            <asp:TextBox ID="tb_Date_DeclarationFiled" runat="server" Enabled="true" Text='<%# Eval("Date_DeclarationFiled") %>' placeholder="Date Declaration Filed ..." CssClass="form-control from-date-datepicker cursor-pointer" />
                                                            <span class="input-group-addon show-from-onclick">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <label class="col-sm-6 control-label"></label>

                                                </div>

                                                <hr />

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Deadline Detail</label>
                                                    <div class="col-sm-10">
                                                        <asp:GridView ID="gvDetailDeadline" runat="server"
                                                            AutoGenerateColumns="False"
                                                            CssClass="table table-bordered table-striped table-responsive col-md-12 footable"
                                                            HeaderStyle-CssClass="success"
                                                            OnPageIndexChanging="gvPageIndexChanging"
                                                            OnRowDataBound="gvRowDataBound"
                                                            OnRowUpdating="gvRowUpdating"
                                                            OnRowEditing="gvRowEditing"
                                                            OnRowCancelingEdit="gvRowCancelingEdit"
                                                            AllowPaging="True"
                                                            PageSize="10">
                                                            <RowStyle Font-Size="Small" />
                                                            <PagerStyle CssClass="pageCustom" />
                                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                                            <EmptyDataTemplate>
                                                                <div style="text-align: center">--- No Data ---</div>
                                                            </EmptyDataTemplate>

                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Deadline Date" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                                    <ItemTemplate>
                                                                        <%--  <asp:Label ID="lblu0_doc_idx_record" runat="server" Text='<%#Eval("u0_doc_idx") %>'></asp:Label>
                                                            <asp:Label ID="lbl_u1_doc_idx_record" runat="server" Text='<%#Eval("u1_doc_idx") %>'></asp:Label>--%>
                                                                        <asp:Label ID="lbl_condition" runat="server" Visible="false"></asp:Label>
                                                                        <asp:Label ID="lbl_u1_doc_idx" runat="server" Visible="false" Text='<%#Eval("u1_doc_idx") %>'></asp:Label>
                                                                        <asp:Label ID="lbldeadline_idx" runat="server" Visible="false" Text='<%#Eval("deadline_idx") %>'></asp:Label>
                                                                        <asp:Label ID="lbl_deadline_date" runat="server" Text='<%#Eval("deadline_date") %>'></asp:Label>

                                                                    </ItemTemplate>

                                                                    <EditItemTemplate>

                                                                        <div class="panel-body">
                                                                            <div class="form-horizontal" role="form">

                                                                                <div class="form-group">
                                                                                    <label class="col-sm-2 control-label">Deadline Date</label>
                                                                                    <div class="col-sm-4">
                                                                                        <asp:Label ID="lbl_u1_doc_idx" runat="server" Visible="false" Text='<%#Eval("u1_doc_idx") %>'></asp:Label>
                                                                                        <asp:Label ID="lbl_deadline_idx" runat="server" Visible="false" Text='<%#Eval("deadline_idx") %>'></asp:Label>
                                                                                        <div class='input-group date'>

                                                                                            <asp:TextBox ID="tb_Office_Action_Date_edit" runat="server" Text='<%#Eval("deadline_date") %>' placeholder="Deadline Date ..." CssClass="form-control from-date-datepicker cursor-pointer" />
                                                                                            <span class="input-group-addon show-from-onclick">
                                                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                                                            </span>
                                                                                        </div>
                                                                                    </div>

                                                                                    <label class="col-sm-2 control-label">Selected</label>
                                                                                    <div class="col-sm-4 control-label textleft">
                                                                                        <asp:Label ID="lbl_status" runat="server" Visible="false" Text='<%#Eval("status") %>'></asp:Label>
                                                                                        <asp:RadioButtonList ID="rdoDeadline_edit" Font-Bold="true" Font-Size="Small" runat="server" CssClass="">
                                                                                            <asp:ListItem Text=" Extensible" Value="1" />
                                                                                            <asp:ListItem Text=" Non-extensible" Value="0" />
                                                                                        </asp:RadioButtonList>

                                                                                    </div>

                                                                                </div>


                                                                                <div class="form-group">
                                                                                    <div id="ActionList_scroll" style="overflow-x: auto; width: 100%; height: 150px" runat="server">
                                                                                        <label class="col-sm-2 control-label">Select ActionList</label>
                                                                                        <div class="col-sm-10 control-label textleft">
                                                                                            <%-- <asp:CheckBoxList ID="chkActionList" runat="server" Enabled="true" CellPadding="5" CellSpacing="5" RepeatColumns="2" Width="100%" Font-Bold="false" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right" Style="overflow: auto;" CssClass="word-wrap" />--%>
                                                                                            <asp:Label ID="lbl_action_detail_idx" runat="server" Visible="false" Text='<%#Eval("action_detail_idx_value") %>'></asp:Label>
                                                                                            <asp:CheckBoxList ID="chkActionList_edit" runat="server" ValidationGroup="Create_Actor1" RepeatColumns="2" Width="100%" Font-Bold="false" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right" />

                                                                                            <%-- <asp:CheckBoxList ID="chkActionList" runat="server" Enabled="true" CellPadding="5" CellSpacing="5" RepeatColumns="1" Width="100%" Font-Bold="false" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right" Style="overflow: auto;" CssClass="word-wrap" />--%>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group">

                                                                                    <label class="col-sm-2 control-label">Select ActionList</label>
                                                                                    <div class="col-sm-10">
                                                                                        <asp:GridView ID="gvActionsDetailEdit" runat="server"
                                                                                            AutoGenerateColumns="False"
                                                                                            CssClass="table table-bordered table-striped table-responsive col-md-12 footable"
                                                                                            HeaderStyle-CssClass="default"
                                                                                            OnPageIndexChanging="gvPageIndexChanging"
                                                                                            OnRowDataBound="gvRowDataBound"
                                                                                            AllowPaging="false"
                                                                                            PageSize="10">
                                                                                            <RowStyle Font-Size="Small" />
                                                                                            <PagerStyle CssClass="pageCustom" />
                                                                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                                                                            <EmptyDataTemplate>
                                                                                                <div style="text-align: center">--- No Data ---</div>
                                                                                            </EmptyDataTemplate>
                                                                                            <Columns>

                                                                                                <asp:TemplateField HeaderText="#" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%">
                                                                                                    <ItemTemplate>
                                                                                                        <%# (Container.DataItemIndex + 1) %>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>

                                                                                                <asp:TemplateField HeaderText="Actions" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="35%">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="lbl_actiondetail_idx_edit" runat="server" Visible="false" Text='<%# Eval("actiondetail_idx") %>' />
                                                                                                        <asp:TextBox ID="tbActionsDetail_edit" TextMode="MultiLine" Text='<%# Eval("Actions_Detail") %>' placeholder="Actions ..." Rows="1" runat="server" CssClass="form-control" />

                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>

                                                                                                <asp:TemplateField HeaderText="Owner" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="35%">
                                                                                                    <ItemTemplate>

                                                                                                        <asp:TextBox ID="tbOwnerDetail_edit" Text='<%# Eval("Owner_Detail") %>' TextMode="MultiLine" placeholder="Owner ..." Rows="1" runat="server" CssClass="form-control" />

                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>

                                                                                                <asp:TemplateField HeaderText="Agent" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="25%">
                                                                                                    <ItemTemplate>

                                                                                                        <asp:Label ID="lbl_action_idx_edit" runat="server" Visible="false" Text='<%# Eval("action_idx") %>' />
                                                                                                        <asp:TextBox ID="tb_action_name_en_edit" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("action_name_en") %>' />


                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>



                                                                                            </Columns>
                                                                                        </asp:GridView>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group">
                                                                                    <div class="col-sm-2"></div>
                                                                                    <div class="col-sm-10">
                                                                                        <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" ValidationGroup="Edit" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                                                            Text="Save" CommandName="Update"></asp:LinkButton>
                                                                                        <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                                                            Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </EditItemTemplate>

                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Selected" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                                    <ItemTemplate>

                                                                        <asp:Label ID="lbldeadline_statusname" runat="server" Text='<%#Eval("deadline_statusname") %>'></asp:Label>

                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Select ActionList" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="20%">
                                                                    <ItemTemplate>

                                                                        <asp:Label ID="lblaction_detail_idx" Visible="false" runat="server" Text='<%#Eval("action_detail_idx_value") %>'></asp:Label>
                                                                        <asp:Label ID="lbl_action_detail_name" runat="server" Text='<%#Eval("action_detail_name") %>'></asp:Label>

                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Action Detail" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="40%">
                                                                    <ItemTemplate>
                                                                        <div id="div_gvActionsDetail" runat="server" style="overflow-x: auto; width: 100%; height: 150px">
                                                                            <asp:GridView ID="gvActionsDetail" runat="server"
                                                                                AutoGenerateColumns="False"
                                                                                CssClass="table table-bordered table-striped table-responsive col-md-12 footable"
                                                                                HeaderStyle-CssClass="default"
                                                                                OnPageIndexChanging="gvPageIndexChanging"
                                                                                OnRowDataBound="gvRowDataBound"
                                                                                AllowPaging="false"
                                                                                PageSize="10">
                                                                                <RowStyle Font-Size="Small" />
                                                                                <PagerStyle CssClass="pageCustom" />
                                                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                                                                <EmptyDataTemplate>
                                                                                    <div style="text-align: center">--- No Data ---</div>
                                                                                </EmptyDataTemplate>
                                                                                <Columns>

                                                                                    <asp:TemplateField HeaderText="#" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%">
                                                                                        <ItemTemplate>
                                                                                            <%# (Container.DataItemIndex + 1) %>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField HeaderText="Actions" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="35%">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lbl_actiondetail_idx" runat="server" Visible="false" Text='<%# Eval("actiondetail_idx") %>' />
                                                                                            <asp:TextBox ID="tbActionsDetail" TextMode="MultiLine" Text='<%# Eval("Actions_Detail") %>' placeholder="Actions ..." Rows="1" runat="server" CssClass="form-control" Enabled="false" />

                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField HeaderText="Owner" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="35%">
                                                                                        <ItemTemplate>

                                                                                            <asp:TextBox ID="tbOwnerDetail" Text='<%# Eval("Owner_Detail") %>' TextMode="MultiLine" placeholder="Owner ..." Rows="1" runat="server" CssClass="form-control" Enabled="false" />

                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField HeaderText="Agent" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="25%">
                                                                                        <ItemTemplate>

                                                                                            <asp:Label ID="lbl_action_idx" runat="server" Visible="false" Text='<%# Eval("action_idx") %>' />
                                                                                            <asp:TextBox ID="tb_action_name_en" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("action_name_en") %>' />


                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>



                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </div>

                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Manage" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                                    <ItemTemplate>

                                                                        <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                                                            data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                                                            <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>

                                                                        <asp:LinkButton ID="btnTodelete" CssClass="text-trash" runat="server" CommandName="CmdDeletedeadline"
                                                                            data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('Delete data?')"
                                                                            CommandArgument='<%#Eval("deadline_idx") %>' title="delete"><i class="glyphicon glyphicon-trash"></i>

                                                                        </asp:LinkButton>


                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label"></label>
                                                    <div class="col-sm-4 control-label textleft">

                                                        <asp:CheckBox ID="chkAddDeadline" runat="server" Font-Bold="false" Text=" Add Deadline" OnCheckedChanged="chkSelectedIndexChanged" AutoPostBack="true" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right" />

                                                    </div>
                                                </div>

                                                <asp:UpdatePanel ID="Panel_AddDeadline" runat="server" Visible="false">
                                                    <ContentTemplate>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">Deadline Date</label>
                                                            <div class="col-sm-4">
                                                                <div class='input-group date'>
                                                                    <asp:TextBox ID="tb_Office_Action_Date" runat="server" placeholder="Deadline Date ..." CssClass="form-control from-date-datepicker cursor-pointer" />
                                                                    <span class="input-group-addon show-from-onclick">
                                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                                    </span>
                                                                </div>
                                                            </div>

                                                            <label class="col-sm-2 control-label">Selected</label>
                                                            <div class="col-sm-4 control-label textleft">

                                                                <asp:RadioButtonList ID="rdoDeadline" Font-Bold="true" Font-Size="Small" runat="server" CssClass="">
                                                                    <asp:ListItem Text=" Extensible" Value="1" />
                                                                    <asp:ListItem Text=" Non-extensible" Value="0" />
                                                                </asp:RadioButtonList>

                                                            </div>

                                                        </div>


                                                        <div class="form-group">
                                                            <div id="ActionList_scroll" style="overflow-x: auto; width: 100%; height: 150px" runat="server">
                                                                <label class="col-sm-2 control-label">Select ActionList</label>
                                                                <div class="col-sm-10 control-label textleft">
                                                                    <%-- <asp:CheckBoxList ID="chkActionList" runat="server" Enabled="true" CellPadding="5" CellSpacing="5" RepeatColumns="2" Width="100%" Font-Bold="false" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right" Style="overflow: auto;" CssClass="word-wrap" />--%>

                                                                    <asp:CheckBoxList ID="chkActionList" runat="server" ValidationGroup="Create_Actor1" CssClass="media" RepeatColumns="2" Width="100%" Font-Bold="false" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right" />

                                                                    <%-- <asp:CheckBoxList ID="chkActionList" runat="server" Enabled="true" CellPadding="5" CellSpacing="5" RepeatColumns="1" Width="100%" Font-Bold="false" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right" Style="overflow: auto;" CssClass="word-wrap" />--%>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label"></label>
                                                            <div class="col-sm-2">
                                                                <asp:LinkButton ID="btnAddDeadlineTolist" runat="server" CssClass="btn btn-primary" Text="Add Deadline" OnCommand="btnCommand"
                                                                    CommandName="cmdAddDeadlineList" ValidationGroup="addDeadlineList" />
                                                            </div>
                                                            <label class="col-sm-8 control-label"></label>

                                                        </div>

                                                        <div class="form-group">

                                                            <div class="col-sm-12">

                                                                <asp:GridView ID="gvDeadlineInsert" runat="server" Visible="false"
                                                                    AutoGenerateColumns="False"
                                                                    CssClass="table table-bordered table-striped table-responsive col-md-12 footable"
                                                                    HeaderStyle-CssClass="info"
                                                                    OnPageIndexChanging="gvPageIndexChanging"
                                                                    OnRowDataBound="gvRowDataBound"
                                                                    OnRowCommand="onRowCommand"
                                                                    AllowPaging="false"
                                                                    PageSize="10">
                                                                    <RowStyle Font-Size="Small" />
                                                                    <PagerStyle CssClass="pageCustom" />
                                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                                                    <EmptyDataTemplate>
                                                                        <div style="text-align: center">--- No Data ---</div>
                                                                    </EmptyDataTemplate>
                                                                    <Columns>

                                                                        <asp:TemplateField HeaderText="#" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%">
                                                                            <ItemTemplate>
                                                                                <%# (Container.DataItemIndex + 1) %>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Deadline Date" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lbl_drOfficeActionDateText" runat="server" Text='<%# Eval("drOfficeActionDateText")%>'></asp:Label>
                                                                                <%--<asp:TextBox ID="tbActionsDetail" TextMode="MultiLine" placeholder="Actions ..." Rows="2" runat="server" CssClass="form-control" />--%>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Selected" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lbl_drSelectedDeadlineIdx" Visible="false" runat="server" Text='<%# Eval("drSelectedDeadlineIdx")%>'></asp:Label>
                                                                                <asp:Label ID="lbl_drSelectedDeadlineText" runat="server" Text='<%# Eval("drSelectedDeadlineText")%>'></asp:Label>
                                                                                <%--<asp:TextBox ID="tbOwnerDetail" TextMode="MultiLine" placeholder="Owner ..." Rows="2" runat="server" CssClass="form-control" />--%>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Select ActionList" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="25%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lbl_drSelectedActionListDeadlineText" runat="server" Text='<%# Eval("drSelectedActionListDeadlineText")%>'></asp:Label>
                                                                                <asp:Label ID="lbl_drSelectedActionListDeadlineIdx" Visible="false" runat="server" Text='<%# Eval("drSelectedActionListDeadlineIdx")%>'></asp:Label>
                                                                                <%--<asp:Label ID="lblaction_idx" runat="server" Visible="false" Text='<%# Eval("action_idx") %>' />
                                                            <asp:TextBox ID="tb_action_name_en" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("action_name_en") %>' />--%>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Action Detail" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="40%">
                                                                            <ItemTemplate>


                                                                                <asp:GridView ID="gvActionsDetail" runat="server"
                                                                                    AutoGenerateColumns="False"
                                                                                    CssClass="table table-bordered table-striped table-responsive col-md-12 footable"
                                                                                    HeaderStyle-CssClass="default"
                                                                                    OnPageIndexChanging="gvPageIndexChanging"
                                                                                    OnRowDataBound="gvRowDataBound"
                                                                                    AllowPaging="false"
                                                                                    PageSize="10">
                                                                                    <RowStyle Font-Size="Small" />
                                                                                    <PagerStyle CssClass="pageCustom" />
                                                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                                                                    <EmptyDataTemplate>
                                                                                        <div style="text-align: center">--- No Data ---</div>
                                                                                    </EmptyDataTemplate>
                                                                                    <Columns>

                                                                                        <asp:TemplateField HeaderText="#" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%">
                                                                                            <ItemTemplate>
                                                                                                <%# (Container.DataItemIndex + 1) %>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>

                                                                                        <asp:TemplateField HeaderText="Actions" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="35%">
                                                                                            <ItemTemplate>

                                                                                                <asp:TextBox ID="tbActionsDetail" TextMode="MultiLine" placeholder="Actions ..." Rows="1" runat="server" CssClass="form-control" />

                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>

                                                                                        <asp:TemplateField HeaderText="Owner" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="35%">
                                                                                            <ItemTemplate>

                                                                                                <asp:TextBox ID="tbOwnerDetail" TextMode="MultiLine" placeholder="Owner ..." Rows="1" runat="server" CssClass="form-control" />

                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>

                                                                                        <asp:TemplateField HeaderText="Agent" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="25%">
                                                                                            <ItemTemplate>

                                                                                                <asp:Label ID="lblaction_idx" runat="server" Visible="false" Text='<%# Eval("action_idx") %>' />
                                                                                                <asp:TextBox ID="tb_action_name_en" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("action_name_en") %>' />


                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>

                                                                                    </Columns>
                                                                                </asp:GridView>

                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="btnRemoveDeadline" runat="server"
                                                                                    CssClass="btn btn-danger btn-xs"
                                                                                    OnClientClick="return confirm('Delete data Deadline?')"
                                                                                    CommandName="cmdRemoveDeadline"><i class="fa fa-times"></i>
                                                                                </asp:LinkButton>
                                                                                <%--<asp:Label ID="lblaction_idx" runat="server" Visible="false" Text='<%# Eval("action_idx") %>' />
                                                            <asp:TextBox ID="tb_action_name_en" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("action_name_en") %>' />--%>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                    </Columns>
                                                                </asp:GridView>

                                                            </div>

                                                        </div>

                                                    </ContentTemplate>
                                                </asp:UpdatePanel>

                                            </div>

                                        </div>

                                    </div>
                                </EditItemTemplate>

                                <InsertItemTemplate>
                                    <div class="panel panel-default">
                                        <asp:HiddenField ID="hfU0IDX" runat="server" Value='<%# Eval("u0_doc_idx") %>' />
                                        <asp:HiddenField ID="hfU1IDX" runat="server" Value='<%# Eval("u1_doc_idx") %>' />
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Publication Details</h3>
                                        </div>
                                        <div class="panel-body">

                                            <div class="form-horizontal" role="form">

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Publication Date</label>
                                                    <div class="col-sm-4">
                                                        <div class='input-group date'>
                                                            <asp:TextBox ID="tbPublication_Date" runat="server" placeholder="Publication Date ..." CssClass="form-control from-date-datepicker cursor-pointer" />
                                                            <span class="input-group-addon show-from-onclick">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>

                                                        </div>
                                                    </div>

                                                    <label class="col-sm-2 control-label">Notice of Allowance/Grant date</label>
                                                    <div class="col-sm-4">
                                                        <div class='input-group date'>
                                                            <asp:TextBox ID="tbGrant_date" runat="server" placeholder="Notice of Allowance/Grant date ..." CssClass="form-control from-date-datepicker cursor-pointer" />
                                                            <span class="input-group-addon show-from-onclick">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Journal Volume</label>
                                                    <div class="col-sm-4">
                                                        <asp:TextBox ID="tbJournal_Volume" runat="server" placeholder="Journal Volume ..." CssClass="form-control" />
                                                    </div>

                                                    <label class="col-sm-2 control-label">Pub. Reg. Date</label>
                                                    <div class="col-sm-4">
                                                        <div class='input-group date'>
                                                            <asp:TextBox ID="tb_Pub_Date" runat="server" placeholder="Pub. Reg. Date ..." CssClass="form-control from-date-datepicker cursor-pointer" />
                                                            <span class="input-group-addon show-from-onclick">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Registry Reference</label>
                                                    <div class="col-sm-4">
                                                        <asp:TextBox ID="tb_Registry_Reference" runat="server" placeholder="Registry Reference ..." CssClass="form-control" />
                                                    </div>
                                                    <label class="col-sm-2 control-label">Final Office Action Date</label>
                                                    <div class="col-sm-4">
                                                        <div class='input-group date'>
                                                            <asp:TextBox ID="tbFinal_Office_Date" runat="server" placeholder="Final Office Action Date ..." CssClass="form-control from-date-datepicker cursor-pointer" />
                                                            <span class="input-group-addon show-from-onclick">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Date Declaration Filed</label>
                                                    <div class="col-sm-4">
                                                        <div class='input-group date'>
                                                            <asp:TextBox ID="tb_Date_Declaration_Filed" runat="server" placeholder="Date Declaration Filed ..." CssClass="form-control from-date-datepicker cursor-pointer" />
                                                            <span class="input-group-addon show-from-onclick">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <label class="col-sm-4 control-label"></label>
                                                </div>

                                                <hr />
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Deadline Date</label>
                                                    <div class="col-sm-4">
                                                        <div class='input-group date'>
                                                            <asp:TextBox ID="tb_Office_Action_Date" runat="server" ValidationGroup="addDeadlineList" placeholder="Deadline Date ..." CssClass="form-control from-date-datepicker cursor-pointer" />
                                                            <span class="input-group-addon show-from-onclick">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>

                                                            <asp:RequiredFieldValidator ID="Req_tb_Office_Action_Date"
                                                                runat="server" ControlToValidate="tb_Office_Action_Date"
                                                                Display="None" SetFocusOnError="true"
                                                                ErrorMessage="*Select Deadline Date"
                                                                ValidationGroup="addDeadlineList" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_tb_Office_Action_Date" Width="200" PopupPosition="BottomLeft" />
                                                        </div>
                                                    </div>

                                                    <label class="col-sm-2 control-label">Selected</label>
                                                    <div class="col-sm-4 control-label textleft">

                                                        <asp:RadioButtonList ID="rdoDeadline" Font-Bold="true" Font-Size="Small" runat="server" CssClass="">
                                                            <asp:ListItem Text=" Extensible" Value="1" />
                                                            <asp:ListItem Text=" Non-extensible" Value="0" />
                                                        </asp:RadioButtonList>

                                                        <asp:RequiredFieldValidator ID="Req_rdoDeadline"
                                                            runat="server" ControlToValidate="rdoDeadline"
                                                            Display="None" SetFocusOnError="true"
                                                            ErrorMessage="*Please Check Selected"
                                                            ValidationGroup="addDeadlineList" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender15" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_rdoDeadline" Width="200" PopupPosition="BottomLeft" />

                                                    </div>

                                                </div>

                                                <div class="form-group">
                                                    <div id="ActionList_scroll" style="overflow-x: auto;" runat="server">
                                                        <label class="col-sm-2 control-label">Select ActionList</label>
                                                        <div class="col-sm-10 control-label textleft">
                                                            <asp:CheckBoxList ID="chkActionList" runat="server" ValidationGroup="Create_Actor1" RepeatColumns="2" Width="100%" Font-Bold="false" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right" />


                                                        </div>
                                                    </div>




                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label"></label>
                                                    <div class="col-sm-2">
                                                        <asp:LinkButton ID="btnAddDeadlineTolist" runat="server" CssClass="btn btn-primary" Text="Add Deadline" OnCommand="btnCommand"
                                                            CommandName="cmdAddDeadlineList" ValidationGroup="addDeadlineList" />
                                                    </div>
                                                    <label class="col-sm-8 control-label"></label>

                                                </div>

                                                <div class="form-group">

                                                    <div class="col-sm-12">

                                                        <asp:GridView ID="gvDeadlineInsert" runat="server" Visible="false"
                                                            AutoGenerateColumns="False"
                                                            CssClass="table table-bordered table-striped table-responsive col-md-12 footable"
                                                            HeaderStyle-CssClass="info"
                                                            OnPageIndexChanging="gvPageIndexChanging"
                                                            OnRowDataBound="gvRowDataBound"
                                                            OnRowCommand="onRowCommand"
                                                            AllowPaging="false"
                                                            PageSize="10">
                                                            <RowStyle Font-Size="Small" />
                                                            <PagerStyle CssClass="pageCustom" />
                                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                                            <EmptyDataTemplate>
                                                                <div style="text-align: center">--- No Data ---</div>
                                                            </EmptyDataTemplate>
                                                            <Columns>

                                                                <asp:TemplateField HeaderText="#" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%">
                                                                    <ItemTemplate>
                                                                        <%# (Container.DataItemIndex + 1) %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Deadline Date" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_drOfficeActionDateText" runat="server" Text='<%# Eval("drOfficeActionDateText")%>'></asp:Label>
                                                                        <%--<asp:TextBox ID="tbActionsDetail" TextMode="MultiLine" placeholder="Actions ..." Rows="2" runat="server" CssClass="form-control" />--%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Selected" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_drSelectedDeadlineIdx" Visible="false" runat="server" Text='<%# Eval("drSelectedDeadlineIdx")%>'></asp:Label>
                                                                        <asp:Label ID="lbl_drSelectedDeadlineText" runat="server" Text='<%# Eval("drSelectedDeadlineText")%>'></asp:Label>
                                                                        <%--<asp:TextBox ID="tbOwnerDetail" TextMode="MultiLine" placeholder="Owner ..." Rows="2" runat="server" CssClass="form-control" />--%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Select ActionList" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="25%">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_drSelectedActionListDeadlineText" runat="server" Text='<%# Eval("drSelectedActionListDeadlineText")%>'></asp:Label>
                                                                        <asp:Label ID="lbl_drSelectedActionListDeadlineIdx" Visible="false" runat="server" Text='<%# Eval("drSelectedActionListDeadlineIdx")%>'></asp:Label>
                                                                        <%--<asp:Label ID="lblaction_idx" runat="server" Visible="false" Text='<%# Eval("action_idx") %>' />
                                                            <asp:TextBox ID="tb_action_name_en" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("action_name_en") %>' />--%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Action Detail" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="40%">
                                                                    <ItemTemplate>

                                                                        <asp:GridView ID="gvActionsDetail" runat="server"
                                                                            AutoGenerateColumns="False"
                                                                            CssClass="table table-bordered table-striped table-responsive col-md-12 footable"
                                                                            HeaderStyle-CssClass="default"
                                                                            OnPageIndexChanging="gvPageIndexChanging"
                                                                            OnRowDataBound="gvRowDataBound"
                                                                            AllowPaging="false"
                                                                            PageSize="10">
                                                                            <RowStyle Font-Size="Small" />
                                                                            <PagerStyle CssClass="pageCustom" />
                                                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                                                            <EmptyDataTemplate>
                                                                                <div style="text-align: center">--- No Data ---</div>
                                                                            </EmptyDataTemplate>
                                                                            <Columns>

                                                                                <asp:TemplateField HeaderText="#" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%">
                                                                                    <ItemTemplate>
                                                                                        <%# (Container.DataItemIndex + 1) %>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Actions" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="35%">
                                                                                    <ItemTemplate>

                                                                                        <asp:TextBox ID="tbActionsDetail" TextMode="MultiLine" placeholder="Actions ..." Rows="1" runat="server" CssClass="form-control" />

                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Owner" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="35%">
                                                                                    <ItemTemplate>

                                                                                        <asp:TextBox ID="tbOwnerDetail" TextMode="MultiLine" placeholder="Owner ..." Rows="1" runat="server" CssClass="form-control" />

                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Agent" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="25%">
                                                                                    <ItemTemplate>

                                                                                        <asp:Label ID="lblaction_idx" runat="server" Visible="false" Text='<%# Eval("action_idx") %>' />
                                                                                        <asp:TextBox ID="tb_action_name_en" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("action_name_en") %>' />


                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                            </Columns>
                                                                        </asp:GridView>


                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="btnRemoveDeadline" runat="server"
                                                                            CssClass="btn btn-danger btn-xs"
                                                                            OnClientClick="return confirm('Delete data Deadline?')"
                                                                            CommandName="cmdRemoveDeadline"><i class="fa fa-times"></i>
                                                                        </asp:LinkButton>
                                                                        <%--<asp:Label ID="lblaction_idx" runat="server" Visible="false" Text='<%# Eval("action_idx") %>' />
                                                            <asp:TextBox ID="tb_action_name_en" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("action_name_en") %>' />--%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                            </Columns>
                                                        </asp:GridView>

                                                    </div>

                                                </div>



                                            </div>

                                        </div>
                                    </div>
                                </InsertItemTemplate>

                            </asp:FormView>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- Form view Publication Detail -->

                <!-- Form view Assignment Detail -->
                <div id="div_Assignment" runat="server" style="overflow-x: auto; width: 100%">
                    <asp:FormView ID="fvAssignment" runat="server" DefaultMode="ReadOnly" Width="100%" OnDataBound="FvDetail_DataBound">
                        <EditItemTemplate>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Assignment</h3>
                                </div>

                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Assignment Detail</label>
                                            <div class="col-sm-10">

                                                <asp:GridView ID="gvDetailAssignment" runat="server"
                                                    AutoGenerateColumns="False"
                                                    CssClass="table table-bordered table-striped table-responsive col-md-12 footable"
                                                    HeaderStyle-CssClass="success"
                                                    OnPageIndexChanging="gvPageIndexChanging"
                                                    OnRowDataBound="gvRowDataBound"
                                                    OnRowUpdating="gvRowUpdating"
                                                    OnRowEditing="gvRowEditing"
                                                    OnRowCancelingEdit="gvRowCancelingEdit"
                                                    AllowPaging="True"
                                                    PageSize="10">
                                                    <RowStyle Font-Size="Small" />
                                                    <PagerStyle CssClass="pageCustom" />
                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">--- No Data ---</div>
                                                    </EmptyDataTemplate>

                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Step" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                            <ItemTemplate>

                                                                <asp:Label ID="lbl_assignment_idx" Visible="false" runat="server" Text='<%#Eval("assignment_idx") %>'></asp:Label>
                                                                <asp:Label ID="lblstep_idx" Visible="false" runat="server" Text='<%#Eval("step_idx") %>'></asp:Label>
                                                                <asp:Label ID="lbl_step_name" runat="server" Text='<%#Eval("step_name") %>'></asp:Label>

                                                            </ItemTemplate>
                                                            <EditItemTemplate>

                                                                <div class="panel-body">
                                                                    <div class="form-horizontal" role="form">
                                                                        <div class="form-group">
                                                                            <label class="col-sm-2 control-label">Step</label>
                                                                            <div class="col-sm-4 control-label textleft">
                                                                                <asp:Label ID="lbl_assignment_idx" Visible="false" runat="server" Text='<%#Eval("assignment_idx") %>'></asp:Label>
                                                                                <asp:Label ID="lbl_step_idx" runat="server" Visible="false" Text='<%#Eval("step_idx") %>'></asp:Label>
                                                                                <asp:RadioButtonList ID="rdoStepAssignment_edit" Font-Bold="true" Font-Size="Small" runat="server" CssClass="" AutoPostBack="true"
                                                                                    OnSelectedIndexChanged="rdoSelectedIndexChanged">
                                                                                    <asp:ListItem Text=" 1 step" Value="0" />
                                                                                    <asp:ListItem Text=" 2 step" Value="1" />
                                                                                    <asp:ListItem Text=" > 2 step" Value="2" />
                                                                                </asp:RadioButtonList>

                                                                                <asp:TextBox ID="txt_step_remark_edit" runat="server" placeholder="Detail Step ..." Visible="false" CssClass="form-control" />

                                                                            </div>
                                                                            <label class="col-sm-2 control-label">Selected</label>
                                                                            <div class="col-sm-4 control-label textleft">
                                                                                <asp:Label ID="lbl_selected_idx" runat="server" Visible="false" Text='<%#Eval("selected_idx") %>'></asp:Label>
                                                                                <asp:RadioButtonList ID="rdoAssignment_edit" Font-Bold="true" Font-Size="Small" runat="server" CssClass="">
                                                                                    <asp:ListItem Text=" During registration" Value="1" />
                                                                                    <asp:ListItem Text=" After registration" Value="0" />
                                                                                </asp:RadioButtonList>
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label class="col-sm-2 control-label">Status</label>
                                                                            <div class="col-sm-4">
                                                                                <asp:Label ID="lbl_staus" runat="server" Visible="false" Text='<%#Eval("status") %>'></asp:Label>
                                                                                <asp:DropDownList ID="ddlStatusAssignment_edit" AutoPostBack="true" runat="server" ValidationGroup="SaveRecordsheet" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                            <label class="col-sm-2 control-label">Sub Status</label>
                                                                            <div class="col-sm-4">
                                                                                <asp:Label ID="lbl_substatus" runat="server" Visible="false" Text='<%#Eval("substatus") %>'></asp:Label>
                                                                                <asp:DropDownList ID="ddlSubStatusAssignment_edit" runat="server" ValidationGroup="SaveRecordsheet" CssClass="form-control">
                                                                                </asp:DropDownList>
                                                                            </div>

                                                                        </div>

                                                                        <div class="form-group">
                                                                            <div class="col-sm-2"></div>
                                                                            <div class="col-sm-10">
                                                                                <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" ValidationGroup="Edit" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                                                    Text="Save" CommandName="Update"></asp:LinkButton>
                                                                                <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                                                    Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </EditItemTemplate>


                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Selected" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                            <ItemTemplate>

                                                                <asp:Label ID="lblselected_name" runat="server" Text='<%#Eval("selected_name") %>'></asp:Label>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Status" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                            <ItemTemplate>

                                                                <asp:Label ID="lbltrademark_status_en" runat="server" Text='<%#Eval("trademark_status_en") %>'></asp:Label>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Sub Status" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                            <ItemTemplate>

                                                                <asp:Label ID="lbl_trademark_substatus_en" runat="server" Text='<%#Eval("trademark_substatus_en") %>'></asp:Label>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Manage" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                            <ItemTemplate>

                                                                <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                                                    data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                                                            <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>

                                                                <asp:LinkButton ID="btnDeleteAssignment" CssClass="text-trash" runat="server" CommandName="CmdDeleteAssignment"
                                                                    data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')"
                                                                    CommandArgument='<%#Eval("assignment_idx") %>' title="delete">
                                                                    <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>



                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                    </Columns>
                                                </asp:GridView>

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"></label>
                                            <div class="col-sm-4 control-label textleft">

                                                <asp:CheckBox ID="chkAddAssignment" runat="server" Font-Bold="false" Text=" Add Assignment" OnCheckedChanged="chkSelectedIndexChanged" AutoPostBack="true" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right" />

                                            </div>
                                        </div>

                                        <asp:UpdatePanel ID="Panel_AddAssignment" runat="server" Visible="false">
                                            <ContentTemplate>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Step</label>
                                                    <div class="col-sm-4 control-label textleft">
                                                        <asp:RadioButtonList ID="rdoStepAssignment" Font-Bold="true" Font-Size="Small" runat="server" CssClass="" AutoPostBack="true"
                                                            OnSelectedIndexChanged="rdoSelectedIndexChanged">
                                                            <asp:ListItem Text=" 1 step" Value="0" />
                                                            <asp:ListItem Text=" 2 step" Value="1" />
                                                            <asp:ListItem Text=" > 2 step" Value="2" />
                                                        </asp:RadioButtonList>

                                                        <asp:TextBox ID="txt_step_remark" runat="server" placeholder="Detail Step ..." Visible="false" CssClass="form-control" />

                                                    </div>
                                                    <label class="col-sm-2 control-label">Selected</label>
                                                    <div class="col-sm-4 control-label textleft">
                                                        <asp:RadioButtonList ID="rdoAssignment" Font-Bold="true" Font-Size="Small" runat="server" CssClass="">
                                                            <asp:ListItem Text=" During registration" Value="1" />
                                                            <asp:ListItem Text=" After registration" Value="0" />
                                                        </asp:RadioButtonList>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Status</label>
                                                    <div class="col-sm-4">
                                                        <asp:DropDownList ID="ddlStatusAssignment" AutoPostBack="true" runat="server" ValidationGroup="SaveRecordsheet" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </div>
                                                    <label class="col-sm-2 control-label">Sub Status</label>
                                                    <div class="col-sm-4">
                                                        <asp:DropDownList ID="ddlSubStatusAssignment" runat="server" ValidationGroup="SaveRecordsheet" CssClass="form-control">
                                                        </asp:DropDownList>
                                                    </div>

                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label"></label>
                                                    <div class="col-sm-2">
                                                        <asp:LinkButton ID="btnAddAssignmentTolist" runat="server" CssClass="btn btn-primary" Text="Add Assignment" OnCommand="btnCommand"
                                                            CommandName="cmdAddAssignmentList" ValidationGroup="addPrioritiesList" />
                                                    </div>
                                                    <label class="col-sm-8 control-label"></label>

                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label"></label>
                                                    <div class="col-sm-10">

                                                        <asp:GridView ID="gvAssignmentList"
                                                            runat="server"
                                                            CssClass="table table-bordered table-striped table-responsive col-md-12"
                                                            HeaderStyle-CssClass="info"
                                                            OnRowCommand="onRowCommand"
                                                            AutoGenerateColumns="false">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center"
                                                                    HeaderStyle-CssClass="text-center"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller">
                                                                    <ItemTemplate>
                                                                        <%# (Container.DataItemIndex + 1) %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Step" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller">
                                                                    <ItemTemplate>
                                                                        <p>
                                                                            <b>Step :</b>
                                                                            <asp:Label ID="lbl_drStepAssignmentText" runat="server" Text='<%# Eval("drStepAssignmentText")%>'></asp:Label>
                                                                        </p>
                                                                        <p>
                                                                            <b>Step Detail:</b>
                                                                            <asp:Label ID="lbl_drStepRemarkText" runat="server" Text='<%# Eval("drStepRemarkText")%>'></asp:Label>
                                                                        </p>


                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:BoundField DataField="drSelectedAssignmentText" HeaderText="Selected" ItemStyle-CssClass="text-left"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                                <asp:BoundField DataField="drStatusAssignmentText" HeaderText="Status" ItemStyle-CssClass="text-left"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                                <asp:BoundField DataField="drSubStatusAssignmentText" HeaderText="Sub Status" ItemStyle-CssClass="text-left"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                                <asp:TemplateField ItemStyle-HorizontalAlign="center"
                                                                    HeaderStyle-CssClass="text-center">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="btnRemoveAssignment" runat="server"
                                                                            CssClass="btn btn-danger btn-xs"
                                                                            OnClientClick="return confirm('Delete data Assignment?')"
                                                                            CommandName="cmdRemoveAssignment"><i class="fa fa-times"></i>
                                                                        </asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>

                                                    </div>
                                                </div>

                                            </ContentTemplate>
                                        </asp:UpdatePanel>

                                    </div>
                                </div>

                            </div>
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <div class="panel panel-default">
                                <asp:HiddenField ID="hfU0IDX" runat="server" Value='<%# Eval("u0_doc_idx") %>' />
                                <asp:HiddenField ID="hfU1IDX" runat="server" Value='<%# Eval("u1_doc_idx") %>' />

                                <div class="panel-heading">
                                    <h3 class="panel-title">Assignment</h3>
                                </div>

                                <div class="panel-body">

                                    <div class="form-horizontal" role="form">

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Step</label>
                                            <div class="col-sm-4 control-label textleft">
                                                <asp:RadioButtonList ID="rdoStepAssignment" Font-Bold="true" Font-Size="Small" runat="server" CssClass="" AutoPostBack="true"
                                                    OnSelectedIndexChanged="rdoSelectedIndexChanged">
                                                    <asp:ListItem Text=" 1 step" Value="0" />
                                                    <asp:ListItem Text=" 2 step" Value="1" />
                                                    <asp:ListItem Text=" > 2 step" Value="2" />
                                                </asp:RadioButtonList>

                                                <asp:RequiredFieldValidator ID="Req_rdoStepAssignment"
                                                    runat="server" ControlToValidate="rdoStepAssignment"
                                                    Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*Please Selected Step"
                                                    ValidationGroup="addAssignmentList" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender15" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_rdoStepAssignment" Width="200" PopupPosition="BottomLeft" />

                                                <asp:TextBox ID="txt_step_remark" runat="server" placeholder="Detail Step ..." Visible="false" CssClass="form-control" />

                                                <%--<asp:RequiredFieldValidator ID="Req_txt_step_remark"
                                                    runat="server" ControlToValidate="txt_step_remark"
                                                    Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*Please Enter Data"
                                                    ValidationGroup="addAssignmentList" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender16" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_txt_step_remark" Width="200" PopupPosition="BottomLeft" />--%>
                                            </div>
                                            <label class="col-sm-2 control-label">Selected</label>
                                            <div class="col-sm-4 control-label textleft">
                                                <asp:RadioButtonList ID="rdoAssignment" Font-Bold="true" Font-Size="Small" runat="server" CssClass="">
                                                    <asp:ListItem Text=" During registration" Value="1" />
                                                    <asp:ListItem Text=" After registration" Value="0" />
                                                </asp:RadioButtonList>

                                                <asp:RequiredFieldValidator ID="Req_rdoAssignment"
                                                    runat="server" ControlToValidate="rdoAssignment"
                                                    Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*Please Check Selected"
                                                    ValidationGroup="addAssignmentList" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender17" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_rdoAssignment" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Status</label>
                                            <div class="col-sm-4">
                                                <asp:DropDownList ID="ddlStatusAssignment" AutoPostBack="true" runat="server" ValidationGroup="SaveRecordsheet" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                </asp:DropDownList>

                                                <asp:RequiredFieldValidator ID="Req_ddlStatusAssignment"
                                                    runat="server" ControlToValidate="ddlStatusAssignment" InitialValue="0"
                                                    Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*Please Selected Status"
                                                    ValidationGroup="addAssignmentList" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender18" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlStatusAssignment" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                            <label class="col-sm-2 control-label">Sub Status</label>
                                            <div class="col-sm-4">
                                                <asp:DropDownList ID="ddlSubStatusAssignment" runat="server" ValidationGroup="SaveRecordsheet" CssClass="form-control">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="Req_ddlSubStatusAssignment"
                                                    runat="server" ControlToValidate="ddlSubStatusAssignment" InitialValue="0"
                                                    Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*Please Selected Sub Status"
                                                    ValidationGroup="addAssignmentList" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender19" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlSubStatusAssignment" Width="200" PopupPosition="BottomLeft" />
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"></label>
                                            <div class="col-sm-2">
                                                <asp:LinkButton ID="btnAddAssignmentTolist" runat="server" CssClass="btn btn-primary" Text="Add Assignment" OnCommand="btnCommand"
                                                    CommandName="cmdAddAssignmentList" ValidationGroup="addAssignmentList" />
                                            </div>
                                            <label class="col-sm-8 control-label"></label>

                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"></label>
                                            <div class="col-sm-10">

                                                <asp:GridView ID="gvAssignmentList"
                                                    runat="server"
                                                    CssClass="table table-bordered table-striped table-responsive col-md-12"
                                                    HeaderStyle-CssClass="info"
                                                    OnRowCommand="onRowCommand"
                                                    AutoGenerateColumns="false">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center"
                                                            HeaderStyle-CssClass="text-center"
                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller">
                                                            <ItemTemplate>
                                                                <%# (Container.DataItemIndex + 1) %>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Step" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller">
                                                            <ItemTemplate>
                                                                <p>
                                                                    <b>Step :</b>
                                                                    <asp:Label ID="lbl_drStepAssignmentText" runat="server" Text='<%# Eval("drStepAssignmentText")%>'></asp:Label>
                                                                </p>
                                                                <p>
                                                                    <b>Step Detail:</b>
                                                                    <asp:Label ID="lbl_drStepRemarkText" runat="server" Text='<%# Eval("drStepRemarkText")%>'></asp:Label>
                                                                </p>


                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:BoundField DataField="drSelectedAssignmentText" HeaderText="Selected" ItemStyle-CssClass="text-left"
                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                        <asp:BoundField DataField="drStatusAssignmentText" HeaderText="Status" ItemStyle-CssClass="text-left"
                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                        <asp:BoundField DataField="drSubStatusAssignmentText" HeaderText="Sub Status" ItemStyle-CssClass="text-left"
                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                        <asp:TemplateField ItemStyle-HorizontalAlign="center"
                                                            HeaderStyle-CssClass="text-center">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="btnRemoveAssignment" runat="server"
                                                                    CssClass="btn btn-danger btn-xs"
                                                                    OnClientClick="return confirm('Delete data Assignment?')"
                                                                    CommandName="cmdRemoveAssignment"><i class="fa fa-times"></i>
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>

                                            </div>
                                        </div>


                                    </div>
                                </div>


                            </div>
                        </InsertItemTemplate>

                    </asp:FormView>
                </div>
                <!-- Form view Assignment Detail -->

                <!-- Form view ChangeofName Detail -->
                <div id="div_ChangeofName" runat="server" style="overflow-x: auto; width: 100%">
                    <asp:FormView ID="fvChangeofName" runat="server" CssClass="col-md-12" DefaultMode="ReadOnly" Width="100%" OnDataBound="FvDetail_DataBound">

                        <EditItemTemplate>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Change of Name/Address</h3>
                                </div>

                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Change of Name/Address Detail</label>
                                            <div class="col-sm-10">

                                                <asp:GridView ID="gvDetailChangeofName" runat="server"
                                                    AutoGenerateColumns="False"
                                                    CssClass="table table-bordered table-striped table-responsive col-md-12 footable"
                                                    HeaderStyle-CssClass="success"
                                                    OnPageIndexChanging="gvPageIndexChanging"
                                                    OnRowDataBound="gvRowDataBound"
                                                    OnRowUpdating="gvRowUpdating"
                                                    OnRowEditing="gvRowEditing"
                                                    OnRowCancelingEdit="gvRowCancelingEdit"
                                                    AllowPaging="True"
                                                    PageSize="10">
                                                    <RowStyle Font-Size="Small" />
                                                    <PagerStyle CssClass="pageCustom" />
                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">--- No Data ---</div>
                                                    </EmptyDataTemplate>

                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Selected" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                            <ItemTemplate>
                                                                <%--  <asp:Label ID="lblu0_doc_idx_record" runat="server" Text='<%#Eval("u0_doc_idx") %>'></asp:Label>
                                                            <asp:Label ID="lbl_u1_doc_idx_record" runat="server" Text='<%#Eval("u1_doc_idx") %>'></asp:Label>--%>

                                                                <asp:Label ID="lblchangeofname_idx" Visible="false" runat="server" Text='<%#Eval("changeofname_idx") %>'></asp:Label>
                                                                <asp:Label ID="lbl_selected_name" runat="server" Text='<%#Eval("selected_name") %>'></asp:Label>

                                                            </ItemTemplate>
                                                            <EditItemTemplate>

                                                                <div class="panel-body">
                                                                    <div class="form-horizontal" role="form">

                                                                        <div class="form-group">
                                                                            <label class="col-sm-2 control-label">Selected</label>
                                                                            <div class="col-sm-4 control-label textleft">
                                                                                <asp:Label ID="lblchangeofname_idx" Visible="false" runat="server" Text='<%#Eval("changeofname_idx") %>'></asp:Label>
                                                                                <asp:Label ID="lbl_selected_idx" Visible="false" runat="server" Text='<%#Eval("selected_idx") %>'></asp:Label>
                                                                                <asp:RadioButtonList ID="rdoChangeofName_edit" Font-Bold="true" Font-Size="Small" runat="server" CssClass="">
                                                                                    <asp:ListItem Text=" Name" Value="0" />
                                                                                    <asp:ListItem Text=" Address" Value="1" />
                                                                                    <asp:ListItem Text=" Name and Address" Value="2" />

                                                                                </asp:RadioButtonList>

                                                                                <asp:RequiredFieldValidator ID="Re_rdoChangeofName_edit"
                                                                                    runat="server"
                                                                                    ControlToValidate="rdoChangeofName_edit" Display="None" SetFocusOnError="true"
                                                                                    ErrorMessage="*Please Selected"
                                                                                    ValidationGroup="Updatechangeofname" />
                                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1233" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_rdoChangeofName_edit" Width="200" PopupPosition="BottomLeft" />

                                                                            </div>

                                                                            <label class="col-sm-6 control-label"></label>

                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label class="col-sm-2 control-label">Status</label>
                                                                            <div class="col-sm-4">
                                                                                <asp:Label ID="lbl_status" runat="server" Visible="false" Text='<%#Eval("status") %>'></asp:Label>
                                                                                <asp:DropDownList ID="ddlStatusChangeofName_edit" AutoPostBack="true" runat="server" ValidationGroup="Updatechangeofname" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                                </asp:DropDownList>
                                                                                <asp:RequiredFieldValidator ID="Req_ddlStatusChangeofName_edit"
                                                                                    runat="server" InitialValue="0"
                                                                                    ControlToValidate="ddlStatusChangeofName_edit" Display="None" SetFocusOnError="true"
                                                                                    ErrorMessage="*Please Selected"
                                                                                    ValidationGroup="Updatechangeofname" />
                                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender34" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlStatusChangeofName_edit" Width="200" PopupPosition="BottomLeft" />

                                                                            </div>
                                                                            <label class="col-sm-2 control-label">Sub Status</label>
                                                                            <div class="col-sm-4">
                                                                                <asp:Label ID="lbl_substatus" Visible="false" runat="server" Text='<%#Eval("substatus") %>'></asp:Label>
                                                                                <asp:DropDownList ID="ddlSubStatusChangeofName_edit" runat="server" ValidationGroup="Updatechangeofname" CssClass="form-control">
                                                                                </asp:DropDownList>
                                                                                <asp:RequiredFieldValidator ID="Req_ddlSubStatusChangeofName_edit"
                                                                                    runat="server" InitialValue="0"
                                                                                    ControlToValidate="ddlSubStatusChangeofName_edit" Display="None" SetFocusOnError="true"
                                                                                    ErrorMessage="*Please Selected"
                                                                                    ValidationGroup="Updatechangeofname" />
                                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender35" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlSubStatusChangeofName_edit" Width="200" PopupPosition="BottomLeft" />
                                                                            </div>

                                                                        </div>

                                                                        <div class="form-group">
                                                                            <div class="col-sm-2"></div>
                                                                            <div class="col-sm-10">
                                                                                <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" ValidationGroup="Updatechangeofname" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                                                    Text="Save" CommandName="Update"></asp:LinkButton>
                                                                                <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                                                    Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </EditItemTemplate>

                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Status" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                            <ItemTemplate>

                                                                <asp:Label ID="lbltrademark_status_en" runat="server" Text='<%#Eval("trademark_status_en") %>'></asp:Label>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Sub Status" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                            <ItemTemplate>

                                                                <asp:Label ID="lbltrademark_substatus_en" runat="server" Text='<%#Eval("trademark_substatus_en") %>'></asp:Label>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Manage" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                            <ItemTemplate>

                                                                <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                                                    data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                                                            <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>

                                                                <asp:LinkButton ID="btnDeleteChangeofname" CssClass="text-trash" runat="server" CommandName="CmdDeleteChangeofname"
                                                                    data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')"
                                                                    CommandArgument='<%#Eval("changeofname_idx") %>' title="delete">
                                                                    <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>



                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                    </Columns>
                                                </asp:GridView>

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"></label>
                                            <div class="col-sm-4 control-label textleft">

                                                <asp:CheckBox ID="chkAddChangeofName" runat="server" Font-Bold="false" Text=" Add ChangeofName" OnCheckedChanged="chkSelectedIndexChanged" AutoPostBack="true" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right" />

                                            </div>
                                        </div>

                                        <asp:UpdatePanel ID="Panel_AddChangeofName" runat="server" Visible="false">
                                            <ContentTemplate>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Selected</label>
                                                    <div class="col-sm-4 control-label textleft">
                                                        <asp:RadioButtonList ID="rdoChangeofName" Font-Bold="true" Font-Size="Small" runat="server" CssClass="">
                                                            <asp:ListItem Text=" Name" Value="0" />
                                                            <asp:ListItem Text=" Address" Value="1" />
                                                            <asp:ListItem Text=" Name and Address" Value="2" />

                                                        </asp:RadioButtonList>
                                                    </div>

                                                    <label class="col-sm-6 control-label"></label>

                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Status</label>
                                                    <div class="col-sm-4">
                                                        <asp:DropDownList ID="ddlStatusChangeofName" AutoPostBack="true" runat="server" ValidationGroup="SaveRecordsheet" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </div>
                                                    <label class="col-sm-2 control-label">Sub Status</label>
                                                    <div class="col-sm-4">
                                                        <asp:DropDownList ID="ddlSubStatusChangeofName" runat="server" ValidationGroup="SaveRecordsheet" CssClass="form-control">
                                                        </asp:DropDownList>
                                                    </div>

                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label"></label>
                                                    <div class="col-sm-2">
                                                        <asp:LinkButton ID="btnAddChangeofNameTolist" runat="server" CssClass="btn btn-primary" Text="Add ChangeofName" OnCommand="btnCommand"
                                                            CommandName="cmdAddChangeofNameList" ValidationGroup="addPrioritiesList" />
                                                    </div>
                                                    <label class="col-sm-8 control-label"></label>

                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label"></label>
                                                    <div class="col-sm-10">

                                                        <asp:GridView ID="gvChangeofNameList"
                                                            runat="server"
                                                            CssClass="table table-bordered table-striped table-responsive col-md-12"
                                                            HeaderStyle-CssClass="info"
                                                            OnRowCommand="onRowCommand"
                                                            AutoGenerateColumns="false">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center"
                                                                    HeaderStyle-CssClass="text-center"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller">
                                                                    <ItemTemplate>
                                                                        <%# (Container.DataItemIndex + 1) %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:BoundField DataField="drSelectedChangeofNameText" HeaderText="Selected" ItemStyle-CssClass="text-left"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                                <asp:BoundField DataField="drStatusChangeofNameText" HeaderText="Status" ItemStyle-CssClass="text-left"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                                <asp:BoundField DataField="drSubStatusChangeofNameText" HeaderText="Sub Status" ItemStyle-CssClass="text-left"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                                <asp:TemplateField ItemStyle-HorizontalAlign="center"
                                                                    HeaderStyle-CssClass="text-center">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="btnRemoveChangeofName" runat="server"
                                                                            CssClass="btn btn-danger btn-xs"
                                                                            OnClientClick="return confirm('Delete data Change of Name/Address?')"
                                                                            CommandName="cmdRemoveChangeofName"><i class="fa fa-times"></i>
                                                                        </asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>

                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>

                                    </div>
                                </div>

                            </div>
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <div class="panel panel-default">
                                <asp:HiddenField ID="hfU0IDX" runat="server" Value='<%# Eval("u0_doc_idx") %>' />
                                <asp:HiddenField ID="hfU1IDX" runat="server" Value='<%# Eval("u1_doc_idx") %>' />

                                <div class="panel-heading">
                                    <h3 class="panel-title">Change of Name/Address</h3>
                                </div>

                                <div class="panel-body">

                                    <div class="form-horizontal" role="form">

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Selected</label>
                                            <div class="col-sm-4 control-label textleft">
                                                <asp:RadioButtonList ID="rdoChangeofName" Font-Bold="true" Font-Size="Small" runat="server" CssClass="">
                                                    <asp:ListItem Text=" Name" Value="0" />
                                                    <asp:ListItem Text=" Address" Value="1" />
                                                    <asp:ListItem Text=" Name and Address" Value="2" />

                                                </asp:RadioButtonList>

                                                <asp:RequiredFieldValidator ID="Req_rdoChangeofName"
                                                    runat="server" ControlToValidate="rdoChangeofName"
                                                    Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*Please Selected"
                                                    ValidationGroup="addChangeofNameList" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender15" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_rdoChangeofName" Width="200" PopupPosition="BottomLeft" />
                                            </div>

                                            <label class="col-sm-6 control-label"></label>

                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Status</label>
                                            <div class="col-sm-4">
                                                <asp:DropDownList ID="ddlStatusChangeofName" AutoPostBack="true" runat="server" ValidationGroup="addChangeofNameList" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                </asp:DropDownList>

                                                <asp:RequiredFieldValidator ID="Req_ddlStatusChangeofName"
                                                    runat="server" ControlToValidate="ddlStatusChangeofName" InitialValue="0"
                                                    Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*Please Selected Status"
                                                    ValidationGroup="addChangeofNameList" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender16" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlStatusChangeofName" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                            <label class="col-sm-2 control-label">Sub Status</label>
                                            <div class="col-sm-4">
                                                <asp:DropDownList ID="ddlSubStatusChangeofName" runat="server" ValidationGroup="addChangeofNameList" CssClass="form-control">
                                                </asp:DropDownList>

                                                <asp:RequiredFieldValidator ID="Req_ddlSubStatusChangeofName"
                                                    runat="server" ControlToValidate="ddlSubStatusChangeofName" InitialValue="0"
                                                    Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*Please Selected Status"
                                                    ValidationGroup="addChangeofNameList" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender20" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlSubStatusChangeofName" Width="200" PopupPosition="BottomLeft" />
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"></label>
                                            <div class="col-sm-2">
                                                <asp:LinkButton ID="btnAddChangeofNameTolist" runat="server" CssClass="btn btn-primary" Text="Add ChangeofName" OnCommand="btnCommand"
                                                    CommandName="cmdAddChangeofNameList" ValidationGroup="addChangeofNameList" />
                                            </div>
                                            <label class="col-sm-8 control-label"></label>

                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"></label>
                                            <div class="col-sm-10">

                                                <asp:GridView ID="gvChangeofNameList"
                                                    runat="server"
                                                    CssClass="table table-bordered table-striped table-responsive col-md-12"
                                                    HeaderStyle-CssClass="info"
                                                    OnRowCommand="onRowCommand"
                                                    AutoGenerateColumns="false">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center"
                                                            HeaderStyle-CssClass="text-center"
                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller">
                                                            <ItemTemplate>
                                                                <%# (Container.DataItemIndex + 1) %>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:BoundField DataField="drSelectedChangeofNameText" HeaderText="Selected" ItemStyle-CssClass="text-left"
                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                        <asp:BoundField DataField="drStatusChangeofNameText" HeaderText="Status" ItemStyle-CssClass="text-left"
                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                        <asp:BoundField DataField="drSubStatusChangeofNameText" HeaderText="Sub Status" ItemStyle-CssClass="text-left"
                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                        <asp:TemplateField ItemStyle-HorizontalAlign="center"
                                                            HeaderStyle-CssClass="text-center">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="btnRemoveChangeofName" runat="server"
                                                                    CssClass="btn btn-danger btn-xs"
                                                                    OnClientClick="return confirm('Delete data Change of Name/Address?')"
                                                                    CommandName="cmdRemoveChangeofName"><i class="fa fa-times"></i>
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>

                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </InsertItemTemplate>

                    </asp:FormView>
                </div>
                <!-- Form view ChangeofName Detail -->

                <!-- Form view Dependent Registration Detail -->
                <div id="div_DependentDetail" runat="server" style="overflow-x: auto; width: 100%">
                    <asp:FormView ID="fvDependentDetail" runat="server" DefaultMode="ReadOnly" Width="100%" OnDataBound="FvDetail_DataBound">
                        <EditItemTemplate>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Dependent Registration</h3>
                                </div>
                                <div class="panel-body">

                                    <div class="form-horizontal" role="form">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Dependent Registration</label>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lbl_dependent_idx_edit" Visible="false" Text='<%# Eval("dependent_idx") %>' runat="server" />
                                                <asp:TextBox ID="tbdependent_registration" placeholder="Dependent Registration ..." Text='<%# Eval("Dependent_Registration") %>' Enabled="true" TextMode="MultiLine" Rows="2" runat="server" CssClass="form-control" />
                                            </div>
                                            <label class="col-sm-4 control-label"></label>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </EditItemTemplate>
                        <InsertItemTemplate>

                            <asp:HiddenField ID="hfU0IDX" runat="server" Value='<%# Eval("u0_doc_idx") %>' />
                            <asp:HiddenField ID="hfU1IDX" runat="server" Value='<%# Eval("u1_doc_idx") %>' />
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Dependent Registration</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Dependent Registration</label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="tbdependent_registration" placeholder="Dependent Registration ..." TextMode="MultiLine" Rows="2" runat="server" CssClass="form-control" />
                                            </div>
                                            <label class="col-sm-4 control-label"></label>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </InsertItemTemplate>

                    </asp:FormView>
                </div>
                <!-- Form view Dependent Registration Detail -->

                <!-- Form view Priorities Detail -->
                <%--<div id="div_PrioritiesDetail" runat="server" style="overflow-x: auto; width: 100%">--%>
                <asp:FormView ID="fvPrioritiesDetail" runat="server" DefaultMode="ReadOnly" Width="100%" OnDataBound="FvDetail_DataBound">
                    <EditItemTemplate>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Priorities</h3>
                            </div>

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Priorities Detail</label>
                                        <div class="col-sm-10">

                                            <asp:GridView ID="gvDetailPriorities" runat="server"
                                                AutoGenerateColumns="False"
                                                CssClass="table table-bordered table-striped table-responsive col-md-12 footable"
                                                HeaderStyle-CssClass="success"
                                                OnPageIndexChanging="gvPageIndexChanging"
                                                OnRowDataBound="gvRowDataBound"
                                                OnRowUpdating="gvRowUpdating"
                                                OnRowEditing="gvRowEditing"
                                                OnRowCancelingEdit="gvRowCancelingEdit"
                                                AllowPaging="True"
                                                PageSize="10">
                                                <RowStyle Font-Size="Small" />
                                                <PagerStyle CssClass="pageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">--- No Data ---</div>
                                                </EmptyDataTemplate>

                                                <Columns>
                                                    <asp:TemplateField HeaderText="Type of priority" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                        <ItemTemplate>
                                                            <%--  <asp:Label ID="lblu0_doc_idx_record" runat="server" Text='<%#Eval("u0_doc_idx") %>'></asp:Label>
                                                            <asp:Label ID="lbl_u1_doc_idx_record" runat="server" Text='<%#Eval("u1_doc_idx") %>'></asp:Label>--%>
                                                            <asp:Label ID="lbl_u1_doc_idx" runat="server" Visible="false" Text='<%#Eval("u1_doc_idx") %>'></asp:Label>
                                                            <asp:Label ID="lbltypeofpriority_idx" runat="server" Visible="false" Text='<%#Eval("typeofpriority_idx") %>'></asp:Label>
                                                            <asp:Label ID="lbl_Type_of_priority" runat="server" Text='<%#Eval("Type_of_priority") %>'></asp:Label>

                                                        </ItemTemplate>

                                                        <EditItemTemplate>

                                                            <div class="panel-body">
                                                                <div class="form-horizontal" role="form">

                                                                    <div class="form-group">
                                                                        <label class="col-sm-2 control-label">Type of priority</label>
                                                                        <div class="col-sm-4">
                                                                            <asp:Label ID="lbltypeofpriority_idx" runat="server" Visible="false" Text='<%#Eval("typeofpriority_idx") %>'></asp:Label>
                                                                            <asp:TextBox ID="tbType_of_priority_edit" runat="server" Text='<%#Eval("Type_of_priority") %>' placeholder="Type of priority ..." CssClass="form-control" />
                                                                            <asp:RequiredFieldValidator ID="Re_tbType_of_priority_edit"
                                                                                runat="server"
                                                                                ControlToValidate="tbType_of_priority_edit" Display="None" SetFocusOnError="true"
                                                                                ErrorMessage="*Type of priority"
                                                                                ValidationGroup="Updatepriority" />
                                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1233" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_tbType_of_priority_edit" Width="200" PopupPosition="BottomLeft" />

                                                                        </div>
                                                                        <label class="col-sm-2 control-label">Priority Date</label>
                                                                        <div class="col-sm-4">
                                                                            <div class='input-group date'>
                                                                                <asp:TextBox ID="tb_Priority_Date_edit" runat="server" Text='<%#Eval("Priority_Date") %>' placeholder="Priority Date ..." CssClass="form-control from-date-datepicker cursor-pointer" />
                                                                                <span class="input-group-addon show-from-onclick">
                                                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                                                </span>

                                                                                <asp:RequiredFieldValidator ID="Req_tb_Priority_Date_edit"
                                                                                    runat="server"
                                                                                    ControlToValidate="tb_Priority_Date_edit" Display="None" SetFocusOnError="true"
                                                                                    ErrorMessage="*Priority Date"
                                                                                    ValidationGroup="Updatepriority" />
                                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender36" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_tb_Priority_Date_edit" Width="200" PopupPosition="BottomLeft" />
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="col-sm-2 control-label">Priority No</label>
                                                                        <div class="col-sm-4">
                                                                            <asp:TextBox ID="tb_Priority_No_edit" runat="server" Text='<%#Eval("Priority_No") %>' placeholder="Priority No ..." CssClass="form-control" />
                                                                            <asp:RequiredFieldValidator ID="Req_tb_Priority_No_edit"
                                                                                runat="server"
                                                                                ControlToValidate="tb_Priority_No_edit" Display="None" SetFocusOnError="true"
                                                                                ErrorMessage="*Priority No"
                                                                                ValidationGroup="Updatepriority" />
                                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender37" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_tb_Priority_No_edit" Width="200" PopupPosition="BottomLeft" />

                                                                        </div>
                                                                        <label class="col-sm-2 control-label">Country</label>
                                                                        <div class="col-sm-4">
                                                                            <asp:Label ID="lbl_country_idx" runat="server" Visible="false" Text='<%#Eval("country_idx") %>'></asp:Label>
                                                                            <asp:DropDownList ID="ddlCountry_priority_edit" AutoPostBack="true" runat="server" ValidationGroup="SaveRecordsheet" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                            </asp:DropDownList>
                                                                            <asp:RequiredFieldValidator ID="Req_ddlCountry_priority_edit"
                                                                                runat="server" InitialValue="0"
                                                                                ControlToValidate="ddlCountry_priority_edit" Display="None" SetFocusOnError="true"
                                                                                ErrorMessage="*Country"
                                                                                ValidationGroup="Updatepriority" />
                                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender38" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlCountry_priority_edit" Width="200" PopupPosition="BottomLeft" />

                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="col-sm-2 control-label">Status</label>
                                                                        <div class="col-sm-4">
                                                                            <asp:Label ID="lbl_status_idx" Visible="false" runat="server" Text='<%#Eval("status_idx") %>'></asp:Label>
                                                                            <asp:DropDownList ID="ddlStatus_priority_edit" AutoPostBack="true" runat="server" ValidationGroup="SaveRecordsheet" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                            </asp:DropDownList>
                                                                            <asp:RequiredFieldValidator ID="Req_ddlStatus_priority_edit"
                                                                                runat="server" InitialValue="0"
                                                                                ControlToValidate="ddlStatus_priority_edit" Display="None" SetFocusOnError="true"
                                                                                ErrorMessage="*Status"
                                                                                ValidationGroup="Updatepriority" />
                                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender39" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlStatus_priority_edit" Width="200" PopupPosition="BottomLeft" />

                                                                        </div>
                                                                        <label class="col-sm-6 control-label"></label>

                                                                    </div>

                                                                    <div class="form-group">
                                                                        <div class="col-sm-2"></div>
                                                                        <div class="col-sm-10">
                                                                            <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" ValidationGroup="Updatepriority" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                                                Text="Save" CommandName="Update"></asp:LinkButton>
                                                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                                                Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </EditItemTemplate>

                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Priority Date" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                        <ItemTemplate>

                                                            <asp:Label ID="lblPriority_Date" runat="server" Text='<%#Eval("Priority_Date") %>'></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Priority No" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                        <ItemTemplate>

                                                            <asp:Label ID="lblPriority_No" runat="server" Text='<%#Eval("Priority_No") %>'></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Country" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                        <ItemTemplate>

                                                            <asp:Label ID="lblcountry_idx" Visible="false" runat="server" Text='<%#Eval("country_idx") %>'></asp:Label>
                                                            <asp:Label ID="lbl_country_name_en" runat="server" Text='<%#Eval("country_name_en") %>'></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Status" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                        <ItemTemplate>

                                                            <asp:Label ID="lblstatus_idx" Visible="false" runat="server" Text='<%#Eval("status_idx") %>'></asp:Label>
                                                            <asp:Label ID="lbl_status_name_en" runat="server" Text='<%#Eval("status_name_en") %>'></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Manage" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                        <ItemTemplate>

                                                            <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                                                data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                                                            <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>

                                                            <asp:LinkButton ID="btnDeleteTypeofpriority" CssClass="text-trash" runat="server" CommandName="CmdDeleteTypeofpriority"
                                                                data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')"
                                                                CommandArgument='<%#Eval("typeofpriority_idx") %>' title="delete">
                                                                    <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>



                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-4 control-label textleft">

                                            <asp:CheckBox ID="chkAddPriorities" runat="server" Font-Bold="false" Text=" Add Priorities" OnCheckedChanged="chkSelectedIndexChanged" AutoPostBack="true" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right" />

                                        </div>
                                        <label class="col-sm-6 control-label"></label>
                                    </div>

                                    <asp:UpdatePanel ID="Panel_AddPriorities" runat="server" Visible="false">
                                        <ContentTemplate>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Type of priority</label>
                                                <div class="col-sm-4">
                                                    <asp:TextBox ID="tbType_of_priority" runat="server" placeholder="Type of priority ..." CssClass="form-control" />
                                                </div>
                                                <label class="col-sm-2 control-label">Priority Date</label>
                                                <div class="col-sm-4">
                                                    <div class='input-group date'>
                                                        <asp:TextBox ID="tb_Priority_Date" runat="server" placeholder="Priority Date ..." CssClass="form-control from-date-datepicker cursor-pointer" />
                                                        <span class="input-group-addon show-from-onclick">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Priority No</label>
                                                <div class="col-sm-4">
                                                    <asp:TextBox ID="tb_Priority_No" runat="server" placeholder="Priority No ..." CssClass="form-control" />
                                                </div>
                                                <label class="col-sm-2 control-label">Country</label>
                                                <div class="col-sm-4">

                                                    <asp:DropDownList ID="ddlCountry_priority" AutoPostBack="true" runat="server" ValidationGroup="SaveRecordsheet" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Status</label>
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddlStatus_priority" AutoPostBack="true" runat="server" ValidationGroup="SaveRecordsheet" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </div>
                                                <label class="col-sm-6 control-label"></label>

                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label"></label>
                                                <div class="col-sm-2">
                                                    <asp:LinkButton ID="btnAddPrioritiesTolist" runat="server" CssClass="btn btn-primary" Text="Add Priorities" OnCommand="btnCommand"
                                                        CommandName="cmdAddPrioritiesList" ValidationGroup="addPrioritiesList" />
                                                </div>
                                                <label class="col-sm-8 control-label"></label>

                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label"></label>
                                                <div class="col-sm-10">

                                                    <asp:GridView ID="gvPrioritiesList"
                                                        runat="server"
                                                        CssClass="table table-bordered table-striped table-responsive col-md-12"
                                                        HeaderStyle-CssClass="info"
                                                        OnRowCommand="onRowCommand"
                                                        AutoGenerateColumns="false">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center"
                                                                HeaderStyle-CssClass="text-center"
                                                                HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller">
                                                                <ItemTemplate>
                                                                    <%# (Container.DataItemIndex + 1) %>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:BoundField DataField="drTypeofpriorityText" HeaderText="Type of priority" ItemStyle-CssClass="text-left"
                                                                HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                            <asp:BoundField DataField="drPriorityDateText" HeaderText="Priority Date" ItemStyle-CssClass="text-left"
                                                                HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                            <asp:BoundField DataField="drPriorityNoText" HeaderText="Priority No" ItemStyle-CssClass="text-left"
                                                                HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                            <asp:BoundField DataField="drCountrypriorityText" HeaderText="Country" ItemStyle-CssClass="text-left"
                                                                HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                            <asp:BoundField DataField="drStatuspriorityText" HeaderText="Status" ItemStyle-CssClass="text-left"
                                                                HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                            <asp:TemplateField ItemStyle-HorizontalAlign="center"
                                                                HeaderStyle-CssClass="text-center">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="btnRemovePriorities" runat="server"
                                                                        CssClass="btn btn-danger btn-xs"
                                                                        OnClientClick="return confirm('Delete data Priorities?')"
                                                                        CommandName="cmdRemovePriorities"><i class="fa fa-times"></i>
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>

                                                </div>
                                            </div>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                </div>
                            </div>

                        </div>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <div class="panel panel-default">
                            <asp:HiddenField ID="hfU0IDX" runat="server" Value='<%# Eval("u0_doc_idx") %>' />
                            <asp:HiddenField ID="hfU1IDX" runat="server" Value='<%# Eval("u1_doc_idx") %>' />

                            <div class="panel-heading">
                                <h3 class="panel-title">Priorities</h3>
                            </div>

                            <div class="panel-body">

                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Type of priority</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbType_of_priority" runat="server" placeholder="Type of priority ..." CssClass="form-control" />
                                            <asp:RequiredFieldValidator ID="Req_tbType_of_priority"
                                                runat="server" ControlToValidate="tbType_of_priority"
                                                Display="None" SetFocusOnError="true"
                                                ErrorMessage="*Type of priority"
                                                ValidationGroup="addPrioritiesList" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender24" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_tbType_of_priority" Width="200" PopupPosition="BottomLeft" />
                                        </div>
                                        <label class="col-sm-2 control-label">Priority Date</label>
                                        <div class="col-sm-4">
                                            <div class='input-group date'>
                                                <asp:TextBox ID="tb_Priority_Date" runat="server" placeholder="Priority Date ..." CssClass="form-control from-date-datepicker cursor-pointer" />
                                                <span class="input-group-addon show-from-onclick">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>

                                                <asp:RequiredFieldValidator ID="Req_tb_Priority_Date"
                                                    runat="server" ControlToValidate="tb_Priority_Date"
                                                    Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*Priority Date"
                                                    ValidationGroup="addPrioritiesList" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender15" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_tb_Priority_Date" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Priority No</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tb_Priority_No" runat="server" placeholder="Priority No ..." CssClass="form-control" />
                                            <asp:RequiredFieldValidator ID="Req_tb_Priority_No"
                                                runat="server" ControlToValidate="tb_Priority_No"
                                                Display="None" SetFocusOnError="true"
                                                ErrorMessage="*Priority No"
                                                ValidationGroup="addPrioritiesList" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender23" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_tb_Priority_No" Width="200" PopupPosition="BottomLeft" />
                                        </div>
                                        <label class="col-sm-2 control-label">Country</label>
                                        <div class="col-sm-4">

                                            <asp:DropDownList ID="ddlCountry_priority" AutoPostBack="true" runat="server" ValidationGroup="SaveRecordsheet" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="Req_ddlCountry_priority"
                                                runat="server" ControlToValidate="ddlCountry_priority" InitialValue="0"
                                                Display="None" SetFocusOnError="true"
                                                ErrorMessage="*Please Selected Country"
                                                ValidationGroup="addPrioritiesList" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender21" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlCountry_priority" Width="200" PopupPosition="BottomLeft" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Status</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlStatus_priority" AutoPostBack="true" runat="server" ValidationGroup="SaveRecordsheet" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="Req_ddlStatus_priority"
                                                runat="server" ControlToValidate="ddlStatus_priority" InitialValue="0"
                                                Display="None" SetFocusOnError="true"
                                                ErrorMessage="*Please Selected Status"
                                                ValidationGroup="addPrioritiesList" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender22" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlStatus_priority" Width="200" PopupPosition="BottomLeft" />
                                        </div>
                                        <label class="col-sm-6 control-label"></label>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-2">
                                            <asp:LinkButton ID="btnAddPrioritiesTolist" runat="server" CssClass="btn btn-primary" Text="Add Priorities" OnCommand="btnCommand"
                                                CommandName="cmdAddPrioritiesList" ValidationGroup="addPrioritiesList" />
                                        </div>
                                        <label class="col-sm-8 control-label"></label>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-10">

                                            <asp:GridView ID="gvPrioritiesList"
                                                runat="server"
                                                CssClass="table table-bordered table-striped table-responsive col-md-12"
                                                HeaderStyle-CssClass="info"
                                                OnRowCommand="onRowCommand"
                                                AutoGenerateColumns="false">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center"
                                                        HeaderStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller">
                                                        <ItemTemplate>
                                                            <%# (Container.DataItemIndex + 1) %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:BoundField DataField="drTypeofpriorityText" HeaderText="Type of priority" ItemStyle-CssClass="text-left"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                    <asp:BoundField DataField="drPriorityDateText" HeaderText="Priority Date" ItemStyle-CssClass="text-left"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                    <asp:BoundField DataField="drPriorityNoText" HeaderText="Priority No" ItemStyle-CssClass="text-left"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                    <asp:BoundField DataField="drCountrypriorityText" HeaderText="Country" ItemStyle-CssClass="text-left"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                    <asp:BoundField DataField="drStatuspriorityText" HeaderText="Status" ItemStyle-CssClass="text-left"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                    <asp:TemplateField ItemStyle-HorizontalAlign="center"
                                                        HeaderStyle-CssClass="text-center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnRemovePriorities" runat="server"
                                                                CssClass="btn btn-danger btn-xs"
                                                                OnClientClick="return confirm('Delete data Priorities?')"
                                                                CommandName="cmdRemovePriorities"><i class="fa fa-times"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>

                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </InsertItemTemplate>

                </asp:FormView>
                <%-- </div>--%>
                <!-- Form view Priorities Detail -->

                <!-- Form view Client Detail-->
                <div id="div_ClientDetail" runat="server" style="overflow-x: auto; width: 100%">
                    <asp:FormView ID="fvClientDetail" runat="server" DefaultMode="ReadOnly" Width="100%" OnDataBound="FvDetail_DataBound">
                        <EditItemTemplate>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Client Details</h3>
                                </div>

                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Client Details</label>
                                            <div class="col-sm-10">

                                                <asp:GridView ID="gvDetailClient" runat="server"
                                                    AutoGenerateColumns="False"
                                                    CssClass="table table-bordered table-striped table-responsive col-md-12 footable"
                                                    HeaderStyle-CssClass="success"
                                                    OnPageIndexChanging="gvPageIndexChanging"
                                                    OnRowDataBound="gvRowDataBound"
                                                    OnRowUpdating="gvRowUpdating"
                                                    OnRowEditing="gvRowEditing"
                                                    OnRowCancelingEdit="gvRowCancelingEdit"
                                                    AllowPaging="True"
                                                    PageSize="10">
                                                    <RowStyle Font-Size="Small" />
                                                    <PagerStyle CssClass="pageCustom" />
                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">--- No Data ---</div>
                                                    </EmptyDataTemplate>

                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Name/ Department" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                            <ItemTemplate>
                                                                <%--  <asp:Label ID="lblu0_doc_idx_record" runat="server" Text='<%#Eval("u0_doc_idx") %>'></asp:Label>
                                                            <asp:Label ID="lbl_u1_doc_idx_record" runat="server" Text='<%#Eval("u1_doc_idx") %>'></asp:Label>--%>

                                                                <asp:Label ID="lblClient_Detail" runat="server" Text='<%#Eval("Client_Detail") %>'></asp:Label>

                                                            </ItemTemplate>

                                                            <EditItemTemplate>

                                                                <div class="panel-body">
                                                                    <div class="form-horizontal" role="form">

                                                                        <div class="form-group">
                                                                            <label class="col-sm-2 control-label">Name/ Department</label>
                                                                            <div class="col-sm-4">
                                                                                <asp:Label ID="lblclient_idx" Visible="false" runat="server" Text='<%#Eval("client_idx") %>'></asp:Label>
                                                                                <asp:TextBox ID="tbClientDetail_edit" Text='<%#Eval("Client_Detail") %>' runat="server" placeholder="Details ..." CssClass="form-control" />
                                                                                <asp:RequiredFieldValidator ID="Re_tbClientDetail_edit"
                                                                                    runat="server"
                                                                                    ControlToValidate="tbClientDetail_edit" Display="None" SetFocusOnError="true"
                                                                                    ErrorMessage="*Details"
                                                                                    ValidationGroup="UpdateClient" />
                                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtende1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_tbClientDetail_edit" Width="200" PopupPosition="BottomLeft" />

                                                                            </div>
                                                                            <label class="col-sm-2 control-label">Contact person</label>
                                                                            <div class="col-sm-4">
                                                                                <asp:TextBox ID="tbClientContact_edit" Text='<%#Eval("Client_Contact") %>' runat="server" placeholder="Contact ..." CssClass="form-control" />
                                                                                <asp:RequiredFieldValidator ID="Req_tbClientContact_edit"
                                                                                    runat="server"
                                                                                    ControlToValidate="tbClientContact_edit" Display="None" SetFocusOnError="true"
                                                                                    ErrorMessage="*Contact"
                                                                                    ValidationGroup="UpdateClient" />
                                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender40" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_tbClientContact_edit" Width="200" PopupPosition="BottomLeft" />
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label class="col-sm-2 control-label">Reference</label>
                                                                            <div class="col-sm-4">
                                                                                <asp:TextBox ID="tbClientReference_edit" Text='<%#Eval("Client_Reference") %>' runat="server" placeholder="Reference ..." CssClass="form-control" />
                                                                                <asp:RequiredFieldValidator ID="Req_tbClientReference_edit"
                                                                                    runat="server"
                                                                                    ControlToValidate="tbClientReference_edit" Display="None" SetFocusOnError="true"
                                                                                    ErrorMessage="*Reference"
                                                                                    ValidationGroup="UpdateClient" />
                                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender41" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_tbClientReference_edit" Width="200" PopupPosition="BottomLeft" />
                                                                            </div>
                                                                            <label class="col-sm-2 control-label" id="clientedit" runat="server" visible="false">Status</label>
                                                                            <div class="col-sm-4" id="ddlclientedit" runat="server" visible="false">
                                                                                <asp:Label ID="lbl_status_idx" Visible="false" runat="server" Text='<%#Eval("status_idx") %>'></asp:Label>
                                                                                <asp:DropDownList ID="ddlClientStatus_edit" AutoPostBack="true" runat="server" ValidationGroup="UpdateClient" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged"></asp:DropDownList>

                                                                                <asp:RequiredFieldValidator ID="Req_ddlClientStatus_edit"
                                                                                    runat="server" InitialValue="0"
                                                                                    ControlToValidate="ddlClientStatus_edit" Display="None" SetFocusOnError="true"
                                                                                    ErrorMessage="*Reference"
                                                                                    ValidationGroup="UpdateClient" />
                                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender42" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlClientStatus_edit" Width="200" PopupPosition="BottomLeft" />
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <div class="col-sm-2"></div>
                                                                            <div class="col-sm-10">
                                                                                <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" ValidationGroup="UpdateClient" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                                                    Text="Save" CommandName="Update"></asp:LinkButton>
                                                                                <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                                                    Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </EditItemTemplate>

                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Contact person" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                            <ItemTemplate>

                                                                <asp:Label ID="lblClient_Contact" runat="server" Text='<%#Eval("Client_Contact") %>'></asp:Label>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Reference" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                            <ItemTemplate>

                                                                <asp:Label ID="lblClient_Reference" runat="server" Text='<%#Eval("Client_Reference") %>'></asp:Label>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Status" Visible="false" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                            <ItemTemplate>

                                                                <asp:Label ID="lblclientstatus_idx" Visible="false" runat="server" Text='<%#Eval("status_idx") %>'></asp:Label>
                                                                <asp:Label ID="lblclient_status_name_en" runat="server" Text='<%#Eval("status_name_en") %>'></asp:Label>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Manage" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                            <ItemTemplate>

                                                                <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                                                    data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                                                            <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>

                                                                <asp:LinkButton ID="btnDeleteClient" CssClass="text-trash" runat="server" CommandName="CmdDeleteClient"
                                                                    data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')"
                                                                    CommandArgument='<%#Eval("client_idx") %>' title="delete">
                                                                    <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>



                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                    </Columns>
                                                </asp:GridView>

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"></label>
                                            <div class="col-sm-4 control-label textleft">

                                                <asp:CheckBox ID="chkAddClient" runat="server" Font-Bold="false" Text=" Add Client" OnCheckedChanged="chkSelectedIndexChanged" AutoPostBack="true" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right" />

                                            </div>
                                            <label class="col-sm-6 control-label"></label>
                                        </div>

                                        <asp:UpdatePanel ID="Panel_AddClient" runat="server" Visible="false">
                                            <ContentTemplate>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Name/ Department</label>
                                                    <div class="col-sm-4">
                                                        <asp:TextBox ID="tbClientDetail" runat="server" placeholder="Name/ Department ..." CssClass="form-control" />
                                                    </div>
                                                    <label class="col-sm-2 control-label">Contact person</label>
                                                    <div class="col-sm-4">
                                                        <asp:TextBox ID="tbClientContact" runat="server" placeholder="Contact person ..." CssClass="form-control" />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Reference</label>
                                                    <div class="col-sm-4">
                                                        <asp:TextBox ID="tbClientReference" runat="server" placeholder="Reference ..." CssClass="form-control" />
                                                    </div>
                                                    <label class="col-sm-2 control-label" id="div_clientstatus" runat="server" visible="false">Status</label>
                                                    <div class="col-sm-4" id="div_ddlclientstatus" runat="server" visible="false">
                                                        <asp:DropDownList ID="ddlClientStatus" AutoPostBack="true" runat="server" ValidationGroup="SaveRecordsheet" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged"></asp:DropDownList>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label"></label>
                                                    <div class="col-sm-2">
                                                        <asp:LinkButton ID="btnAddClientTolist" runat="server" CssClass="btn btn-primary" Text="Add Client" OnCommand="btnCommand"
                                                            CommandName="cmdAddClientList" ValidationGroup="addClientList" />
                                                    </div>
                                                    <label class="col-sm-8 control-label"></label>

                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label"></label>
                                                    <div class="col-sm-10">

                                                        <asp:GridView ID="gvClientList"
                                                            runat="server"
                                                            CssClass="table table-bordered table-striped table-responsive col-md-12 footable"
                                                            HeaderStyle-CssClass="info"
                                                            OnRowCommand="onRowCommand"
                                                            AutoGenerateColumns="false">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center"
                                                                    HeaderStyle-CssClass="text-center"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller">
                                                                    <ItemTemplate>
                                                                        <%# (Container.DataItemIndex + 1) %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:BoundField DataField="drClientDetailText" HeaderText="Details" ItemStyle-CssClass="text-left"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                                <asp:BoundField DataField="drClientContactText" HeaderText="Contact" ItemStyle-CssClass="text-left"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                                <asp:BoundField DataField="drClientReferenceText" HeaderText="Reference" ItemStyle-CssClass="text-left"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                                <asp:BoundField DataField="drClientStatusText" Visible="false" HeaderText="Status" ItemStyle-CssClass="text-left"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                                <asp:TemplateField ItemStyle-HorizontalAlign="center"
                                                                    HeaderStyle-CssClass="text-center">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="btnRemoveClient" runat="server"
                                                                            CssClass="btn btn-danger btn-xs"
                                                                            OnClientClick="return confirm('Delete data Client?')"
                                                                            CommandName="cmdRemoveClient"><i class="fa fa-times"></i>
                                                                        </asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>


                                                    </div>
                                                </div>

                                            </ContentTemplate>
                                        </asp:UpdatePanel>

                                    </div>
                                </div>

                            </div>
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <asp:HiddenField ID="hfU0IDX" runat="server" Value='<%# Eval("u0_doc_idx") %>' />
                            <asp:HiddenField ID="hfU1IDX" runat="server" Value='<%# Eval("u1_doc_idx") %>' />

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Client Details</h3>
                                </div>

                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Name/ Department</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbClientDetail" runat="server" placeholder="Name/ Department ..." CssClass="form-control" />

                                                <asp:RequiredFieldValidator ID="Req_tbClientDetail"
                                                    runat="server" ControlToValidate="tbClientDetail"
                                                    Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*Details"
                                                    ValidationGroup="addClientList" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender24" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_tbClientDetail" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                            <label class="col-sm-2 control-label">Contact person</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbClientContact" runat="server" placeholder="Contact person ..." CssClass="form-control" />

                                                <asp:RequiredFieldValidator ID="Req_tbClientContact"
                                                    runat="server" ControlToValidate="tbClientContact"
                                                    Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*Contact"
                                                    ValidationGroup="addClientList" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender25" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_tbClientContact" Width="200" PopupPosition="BottomLeft" />

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Reference</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbClientReference" runat="server" placeholder="Reference ..." CssClass="form-control" />

                                                <asp:RequiredFieldValidator ID="Req_tbClientReference"
                                                    runat="server" ControlToValidate="tbClientReference"
                                                    Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*Reference"
                                                    ValidationGroup="addClientList" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender26" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_tbClientReference" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                            <label class="col-sm-2 control-label" id="div_clientstatus" runat="server" visible="false">Status</label>
                                            <div class="col-sm-4" id="div_ddlclientstatus" runat="server" visible="false">
                                                <asp:DropDownList ID="ddlClientStatus" AutoPostBack="true" runat="server" ValidationGroup="addClientList" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged"></asp:DropDownList>

                                                <asp:RequiredFieldValidator ID="Req_ddlClientStatus"
                                                    runat="server" ControlToValidate="ddlClientStatus" InitialValue="0"
                                                    Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*Selected Status"
                                                    ValidationGroup="addClientList" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender27" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlClientStatus" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"></label>
                                            <div class="col-sm-2">
                                                <asp:LinkButton ID="btnAddClientTolist" runat="server" CssClass="btn btn-primary" Text="Add Client" OnCommand="btnCommand"
                                                    CommandName="cmdAddClientList" ValidationGroup="addClientList" />
                                            </div>
                                            <label class="col-sm-8 control-label"></label>

                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"></label>
                                            <div class="col-sm-10">

                                                <asp:GridView ID="gvClientList"
                                                    runat="server"
                                                    CssClass="table table-bordered table-striped table-responsive col-md-12 footable"
                                                    HeaderStyle-CssClass="info"
                                                    OnRowCommand="onRowCommand"
                                                    AutoGenerateColumns="false">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center"
                                                            HeaderStyle-CssClass="text-center"
                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller">
                                                            <ItemTemplate>
                                                                <%# (Container.DataItemIndex + 1) %>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:BoundField DataField="drClientDetailText" HeaderText="Details" ItemStyle-CssClass="text-left"
                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                        <asp:BoundField DataField="drClientContactText" HeaderText="Contact" ItemStyle-CssClass="text-left"
                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                        <asp:BoundField DataField="drClientReferenceText" HeaderText="Reference" ItemStyle-CssClass="text-left"
                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                        <asp:BoundField DataField="drClientStatusText" HeaderText="Status" ItemStyle-CssClass="text-left"
                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" Visible="false" />

                                                        <asp:TemplateField ItemStyle-HorizontalAlign="center"
                                                            HeaderStyle-CssClass="text-center">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="btnRemoveClient" runat="server"
                                                                    CssClass="btn btn-danger btn-xs"
                                                                    OnClientClick="return confirm('Delete data Client?')"
                                                                    CommandName="cmdRemoveClient"><i class="fa fa-times"></i>
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>


                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </InsertItemTemplate>

                    </asp:FormView>
                </div>
                <!-- Form view Client Detail-->

                <!-- Form view Agent Detail -->
                <div id="div_AgentDetail" runat="server" style="overflow-x: auto; width: 100%">
                    <asp:FormView ID="fvAgentDetail" runat="server" DefaultMode="ReadOnly" Width="100%" OnDataBound="FvDetail_DataBound">
                        <EditItemTemplate>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Agent Details</h3>
                                </div>

                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Agent Details</label>
                                            <div class="col-sm-10">

                                                <asp:GridView ID="gvDetailAgent" runat="server"
                                                    AutoGenerateColumns="False"
                                                    CssClass="table table-bordered table-striped table-responsive col-md-12 footable"
                                                    HeaderStyle-CssClass="success"
                                                    OnPageIndexChanging="gvPageIndexChanging"
                                                    OnRowDataBound="gvRowDataBound"
                                                    OnRowUpdating="gvRowUpdating"
                                                    OnRowEditing="gvRowEditing"
                                                    OnRowCancelingEdit="gvRowCancelingEdit"
                                                    AllowPaging="True"
                                                    PageSize="10">
                                                    <RowStyle Font-Size="Small" />
                                                    <PagerStyle CssClass="pageCustom" />
                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">--- No Data ---</div>
                                                    </EmptyDataTemplate>

                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Name" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                            <ItemTemplate>
                                                                <%--  <asp:Label ID="lblu0_doc_idx_record" runat="server" Text='<%#Eval("u0_doc_idx") %>'></asp:Label>
                                                            <asp:Label ID="lbl_u1_doc_idx_record" runat="server" Text='<%#Eval("u1_doc_idx") %>'></asp:Label>--%>

                                                                <asp:Label ID="lblAgent_Detail" runat="server" Text='<%#Eval("Agent_Detail") %>'></asp:Label>

                                                            </ItemTemplate>
                                                            <EditItemTemplate>

                                                                <div class="panel-body">
                                                                    <div class="form-horizontal" role="form">

                                                                        <div class="form-group">
                                                                            <label class="col-sm-2 control-label">Name</label>
                                                                            <div class="col-sm-4">
                                                                                <asp:Label ID="lblagent_idx" Visible="false" runat="server" Text='<%#Eval("agent_idx") %>'></asp:Label>
                                                                                <asp:TextBox ID="tbAgentDetail_edit" Text='<%#Eval("Agent_Detail") %>' runat="server" placeholder="Details ..." CssClass="form-control" />

                                                                                <asp:RequiredFieldValidator ID="Re_tbAgentDetail_edit"
                                                                                    runat="server"
                                                                                    ControlToValidate="tbAgentDetail_edit" Display="None" SetFocusOnError="true"
                                                                                    ErrorMessage="*Details"
                                                                                    ValidationGroup="UpdateAgent" />
                                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtende1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_tbAgentDetail_edit" Width="200" PopupPosition="BottomLeft" />
                                                                            </div>
                                                                            <label class="col-sm-2 control-label">Contact person</label>
                                                                            <div class="col-sm-4">
                                                                                <asp:TextBox ID="tbAgentContact_edit" Text='<%#Eval("Agent_Contact") %>' runat="server" placeholder="Contact ..." CssClass="form-control" />
                                                                                <asp:RequiredFieldValidator ID="Req_tbAgentContact_edit"
                                                                                    runat="server"
                                                                                    ControlToValidate="tbAgentContact_edit" Display="None" SetFocusOnError="true"
                                                                                    ErrorMessage="*Contact"
                                                                                    ValidationGroup="UpdateAgent" />
                                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender43" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_tbAgentContact_edit" Width="200" PopupPosition="BottomLeft" />
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label class="col-sm-2 control-label">Reference</label>
                                                                            <div class="col-sm-4">
                                                                                <asp:TextBox ID="tbAgentReference_edit" Text='<%#Eval("Agent_Reference") %>' runat="server" placeholder="Reference ..." CssClass="form-control" />
                                                                                <asp:RequiredFieldValidator ID="Req_tbAgentReference_edit"
                                                                                    runat="server"
                                                                                    ControlToValidate="tbAgentReference_edit" Display="None" SetFocusOnError="true"
                                                                                    ErrorMessage="*Reference"
                                                                                    ValidationGroup="UpdateAgent" />
                                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender44" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_tbAgentReference_edit" Width="200" PopupPosition="BottomLeft" />
                                                                            </div>
                                                                            <label class="col-sm-2 control-label">Status</label>
                                                                            <div class="col-sm-4">
                                                                                <asp:Label ID="lbl_status_idx" Visible="false" runat="server" Text='<%#Eval("status_idx") %>'></asp:Label>
                                                                                <asp:DropDownList ID="ddlAgentStatus_edit" AutoPostBack="true" runat="server" ValidationGroup="UpdateAgent" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged"></asp:DropDownList>
                                                                                <asp:RequiredFieldValidator ID="Req_ddlAgentStatus_edit"
                                                                                    runat="server"
                                                                                    ControlToValidate="ddlAgentStatus_edit" Display="None" SetFocusOnError="true"
                                                                                    ErrorMessage="*Reference" InitialValue="0"
                                                                                    ValidationGroup="UpdateAgent" />
                                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender45" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlAgentStatus_edit" Width="200" PopupPosition="BottomLeft" />
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <div class="col-sm-2"></div>
                                                                            <div class="col-sm-10">
                                                                                <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" ValidationGroup="UpdateAgent" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                                                    Text="Save" CommandName="Update"></asp:LinkButton>
                                                                                <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                                                    Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </EditItemTemplate>


                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Contact person" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                            <ItemTemplate>

                                                                <asp:Label ID="lblAgent_Contact" runat="server" Text='<%#Eval("Agent_Contact") %>'></asp:Label>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Reference" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                            <ItemTemplate>

                                                                <asp:Label ID="lblAgent_Reference" runat="server" Text='<%#Eval("Agent_Reference") %>'></asp:Label>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Status" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                            <ItemTemplate>

                                                                <asp:Label ID="lblagentstatus_idx" Visible="false" runat="server" Text='<%#Eval("status_idx") %>'></asp:Label>
                                                                <asp:Label ID="lblagent_status_name_en" runat="server" Text='<%#Eval("status_name_en") %>'></asp:Label>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Manage" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                            <ItemTemplate>

                                                                <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                                                    data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                                                            <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>

                                                                <asp:LinkButton ID="btnDeleteAgent" CssClass="text-trash" runat="server" CommandName="CmdDeleteAgent"
                                                                    data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')"
                                                                    CommandArgument='<%#Eval("agent_idx") %>' title="delete">
                                                                    <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>



                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                    </Columns>
                                                </asp:GridView>

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"></label>
                                            <div class="col-sm-4 control-label textleft">

                                                <asp:CheckBox ID="chkAddAgent" runat="server" Font-Bold="false" Text=" Add Agent" OnCheckedChanged="chkSelectedIndexChanged" AutoPostBack="true" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right" />

                                            </div>
                                            <label class="col-sm-6 control-label"></label>
                                        </div>

                                        <asp:UpdatePanel ID="Panel_AddAgent" runat="server" Visible="false">
                                            <ContentTemplate>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Name</label>
                                                    <div class="col-sm-4">
                                                        <asp:TextBox ID="tbAgentDetail" runat="server" placeholder="Name ..." CssClass="form-control" />
                                                    </div>
                                                    <label class="col-sm-2 control-label">Contact Person</label>
                                                    <div class="col-sm-4">
                                                        <asp:TextBox ID="tbAgentContact" runat="server" placeholder="Contact Person ..." CssClass="form-control" />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Reference</label>
                                                    <div class="col-sm-4">
                                                        <asp:TextBox ID="tbAgentReference" runat="server" placeholder="Reference ..." CssClass="form-control" />
                                                    </div>
                                                    <label class="col-sm-2 control-label">Status</label>
                                                    <div class="col-sm-4">
                                                        <asp:DropDownList ID="ddlAgentStatus" AutoPostBack="true" runat="server" ValidationGroup="SaveRecordsheet" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged"></asp:DropDownList>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label"></label>
                                                    <div class="col-sm-2">
                                                        <asp:LinkButton ID="btnAddAgentTolist" runat="server" CssClass="btn btn-primary" Text="Add Agent" OnCommand="btnCommand"
                                                            CommandName="cmdAddAgentList" ValidationGroup="addAgentList" />
                                                    </div>
                                                    <label class="col-sm-8 control-label"></label>

                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label"></label>
                                                    <div class="col-sm-10">
                                                        <asp:GridView ID="gvAgentList"
                                                            runat="server"
                                                            CssClass="table table-bordered table-striped table-responsive col-md-12 footable"
                                                            HeaderStyle-CssClass="info"
                                                            OnRowCommand="onRowCommand"
                                                            AutoGenerateColumns="false">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center"
                                                                    HeaderStyle-CssClass="text-center"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller">
                                                                    <ItemTemplate>
                                                                        <%# (Container.DataItemIndex + 1) %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:BoundField DataField="drAgentDetailText" HeaderText="Name" ItemStyle-CssClass="text-left"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                                <asp:BoundField DataField="drAgentContactText" HeaderText="Contact person" ItemStyle-CssClass="text-left"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                                <asp:BoundField DataField="drAgentReferenceText" HeaderText="Reference" ItemStyle-CssClass="text-left"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                                <asp:BoundField DataField="drAgentStatusText" HeaderText="Status" ItemStyle-CssClass="text-left"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                                <asp:TemplateField ItemStyle-HorizontalAlign="center"
                                                                    HeaderStyle-CssClass="text-center">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="btnRemoveAgent" runat="server"
                                                                            CssClass="btn btn-danger btn-xs"
                                                                            OnClientClick="return confirm('Delete data Agent?')"
                                                                            CommandName="cmdRemoveAgent"><i class="fa fa-times"></i>
                                                                        </asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>

                                            </ContentTemplate>
                                        </asp:UpdatePanel>

                                    </div>
                                </div>

                            </div>
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <asp:HiddenField ID="hfU0IDX" runat="server" Value='<%# Eval("u0_doc_idx") %>' />
                            <asp:HiddenField ID="hfU1IDX" runat="server" Value='<%# Eval("u1_doc_idx") %>' />

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Agent Details</h3>
                                </div>

                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Name</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbAgentDetail" runat="server" placeholder="Name ..." CssClass="form-control" />

                                                <asp:RequiredFieldValidator ID="Req_tbAgentDetail"
                                                    runat="server" ControlToValidate="tbAgentDetail"
                                                    Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*Details"
                                                    ValidationGroup="addAgentList" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender24" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_tbAgentDetail" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                            <label class="col-sm-2 control-label">Contact Person</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbAgentContact" runat="server" placeholder="Contact Person ..." CssClass="form-control" />
                                                <asp:RequiredFieldValidator ID="Req_tbAgentContact"
                                                    runat="server" ControlToValidate="tbAgentContact"
                                                    Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*Contact"
                                                    ValidationGroup="addAgentList" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender28" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_tbAgentContact" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Reference</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbAgentReference" runat="server" placeholder="Reference ..." CssClass="form-control" />

                                                <asp:RequiredFieldValidator ID="Req_tbAgentReference"
                                                    runat="server" ControlToValidate="tbAgentReference"
                                                    Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*Reference"
                                                    ValidationGroup="addAgentList" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender29" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_tbAgentReference" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                            <label class="col-sm-2 control-label">Status</label>
                                            <div class="col-sm-4">
                                                <asp:DropDownList ID="ddlAgentStatus" AutoPostBack="true" runat="server" ValidationGroup="SaveRecordsheet" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged"></asp:DropDownList>

                                                <asp:RequiredFieldValidator ID="Req_ddlAgentStatus" InitialValue="0"
                                                    runat="server" ControlToValidate="ddlAgentStatus"
                                                    Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*Selected Status"
                                                    ValidationGroup="addAgentList" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender30" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlAgentStatus" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"></label>
                                            <div class="col-sm-2">
                                                <asp:LinkButton ID="btnAddAgentTolist" runat="server" CssClass="btn btn-primary" Text="Add Agent" OnCommand="btnCommand"
                                                    CommandName="cmdAddAgentList" ValidationGroup="addAgentList" />
                                            </div>
                                            <label class="col-sm-8 control-label"></label>

                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"></label>
                                            <div class="col-sm-10">
                                                <asp:GridView ID="gvAgentList"
                                                    runat="server"
                                                    CssClass="table table-bordered table-striped table-responsive col-md-12 footable"
                                                    HeaderStyle-CssClass="info"
                                                    OnRowCommand="onRowCommand"
                                                    AutoGenerateColumns="false">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center"
                                                            HeaderStyle-CssClass="text-center"
                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller">
                                                            <ItemTemplate>
                                                                <%# (Container.DataItemIndex + 1) %>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:BoundField DataField="drAgentDetailText" HeaderText="Details" ItemStyle-CssClass="text-left"
                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                        <asp:BoundField DataField="drAgentContactText" HeaderText="Contact" ItemStyle-CssClass="text-left"
                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                        <asp:BoundField DataField="drAgentReferenceText" HeaderText="Reference" ItemStyle-CssClass="text-left"
                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                        <asp:BoundField DataField="drAgentStatusText" HeaderText="Status" ItemStyle-CssClass="text-left"
                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                        <asp:TemplateField ItemStyle-HorizontalAlign="center"
                                                            HeaderStyle-CssClass="text-center">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="btnRemoveAgent" runat="server"
                                                                    CssClass="btn btn-danger btn-xs"
                                                                    OnClientClick="return confirm('Delete data Agent?')"
                                                                    CommandName="cmdRemoveAgent"><i class="fa fa-times"></i>
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>


                                    </div>
                                </div>

                            </div>
                        </InsertItemTemplate>

                    </asp:FormView>
                </div>
                <!-- Form view Agent Detail -->

                <!-- Form view Goods Detail -->
                <div id="div_GoodsDetail" runat="server" style="overflow-x: auto; width: 100%">
                    <asp:FormView ID="fvGoodsDetail" runat="server" DefaultMode="ReadOnly" Width="100%" OnDataBound="FvDetail_DataBound">
                        <EditItemTemplate>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Goods</h3>
                                </div>

                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Good Details</label>
                                            <div class="col-sm-10">

                                                <asp:GridView ID="gvDetailGood" runat="server"
                                                    AutoGenerateColumns="False"
                                                    CssClass="table table-bordered table-striped table-responsive col-md-12 footable"
                                                    HeaderStyle-CssClass="success"
                                                    OnPageIndexChanging="gvPageIndexChanging"
                                                    OnRowDataBound="gvRowDataBound"
                                                    OnRowUpdating="gvRowUpdating"
                                                    OnRowEditing="gvRowEditing"
                                                    OnRowCancelingEdit="gvRowCancelingEdit"
                                                    AllowPaging="True"
                                                    PageSize="10">
                                                    <RowStyle Font-Size="Small" />
                                                    <PagerStyle CssClass="pageCustom" />
                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">--- No Data ---</div>
                                                    </EmptyDataTemplate>

                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Int.Class" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                            <ItemTemplate>
                                                                <%--  <asp:Label ID="lblu0_doc_idx_record" runat="server" Text='<%#Eval("u0_doc_idx") %>'></asp:Label>
                                                            <asp:Label ID="lbl_u1_doc_idx_record" runat="server" Text='<%#Eval("u1_doc_idx") %>'></asp:Label>--%>

                                                                <asp:Label ID="lblIntClass_Detail" runat="server" Text='<%#Eval("IntClass") %>'></asp:Label>

                                                            </ItemTemplate>
                                                            <EditItemTemplate>

                                                                <div class="panel-body">
                                                                    <div class="form-horizontal" role="form">

                                                                        <div class="form-group">
                                                                            <label class="col-sm-2 control-label">Int. Class</label>
                                                                            <div class="col-sm-4">
                                                                                <asp:Label ID="lblgoods_idx" Visible="false" runat="server" Text='<%#Eval("goods_idx") %>'></asp:Label>
                                                                                <asp:TextBox ID="tbGoods_IntClass_edit" Text='<%#Eval("IntClass") %>' runat="server" placeholder="Int. Class ..." CssClass="form-control" />
                                                                                <asp:RequiredFieldValidator ID="Re_tbGoods_IntClass_edit"
                                                                                    runat="server"
                                                                                    ControlToValidate="tbGoods_IntClass_edit" Display="None" SetFocusOnError="true"
                                                                                    ErrorMessage="*Int. Class"
                                                                                    ValidationGroup="UpdateGoods" />
                                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtende1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_tbGoods_IntClass_edit" Width="200" PopupPosition="BottomLeft" />
                                                                            </div>
                                                                            <label class="col-sm-2 control-label">Goods</label>
                                                                            <div class="col-sm-4">
                                                                                <asp:TextBox ID="tbGoodsDetail_edit" Text='<%#Eval("Goods_Detail") %>' runat="server" placeholder="Goods ..." CssClass="form-control" />
                                                                                <asp:RequiredFieldValidator ID="Req_tbGoodsDetail_edit"
                                                                                    runat="server"
                                                                                    ControlToValidate="tbGoodsDetail_edit" Display="None" SetFocusOnError="true"
                                                                                    ErrorMessage="*Goods"
                                                                                    ValidationGroup="UpdateGoods" />
                                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender46" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_tbGoodsDetail_edit" Width="200" PopupPosition="BottomLeft" />
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label class="col-sm-2 control-label">Local language</label>
                                                                            <div class="col-sm-4">
                                                                                <asp:Label ID="lbl_language_idx" Visible="false" runat="server" Text='<%#Eval("language_idx") %>'></asp:Label>
                                                                                <asp:DropDownList ID="ddlLocal_language_edit" AutoPostBack="true" runat="server" ValidationGroup="UpdateGoods" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged"></asp:DropDownList>
                                                                                <asp:RequiredFieldValidator ID="Req_ddlLocal_language_edit"
                                                                                    runat="server" InitialValue="0"
                                                                                    ControlToValidate="ddlLocal_language_edit" Display="None" SetFocusOnError="true"
                                                                                    ErrorMessage="*Local language"
                                                                                    ValidationGroup="UpdateGoods" />
                                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender47" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlLocal_language_edit" Width="200" PopupPosition="BottomLeft" />
                                                                            </div>
                                                                            <label class="col-sm-6 control-label"></label>

                                                                        </div>

                                                                        <div class="form-group">
                                                                            <div class="col-sm-2"></div>
                                                                            <div class="col-sm-10">
                                                                                <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" ValidationGroup="Edit" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                                                    Text="Save" CommandName="Update"></asp:LinkButton>
                                                                                <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                                                    Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </EditItemTemplate>

                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Goods" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                            <ItemTemplate>

                                                                <asp:Label ID="lblGoods_Detail" runat="server" Text='<%#Eval("Goods_Detail") %>'></asp:Label>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Local language" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                            <ItemTemplate>

                                                                <asp:Label ID="lbllanguage_idx" Visible="false" runat="server" Text='<%#Eval("language_idx") %>'></asp:Label>
                                                                <asp:Label ID="lbl_language_en" runat="server" Text='<%#Eval("language_en") %>'></asp:Label>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Manage" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                            <ItemTemplate>

                                                                <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                                                    data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                                                            <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>

                                                                <asp:LinkButton ID="btnDeleteGoods" CssClass="text-trash" runat="server" CommandName="CmdDeleteGoods"
                                                                    data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')"
                                                                    CommandArgument='<%#Eval("goods_idx") %>' title="delete">
                                                                    <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>



                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                    </Columns>
                                                </asp:GridView>

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"></label>
                                            <div class="col-sm-4 control-label textleft">

                                                <asp:CheckBox ID="chkAddGood" runat="server" Font-Bold="false" Text=" Add Good" OnCheckedChanged="chkSelectedIndexChanged" AutoPostBack="true" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right" />

                                            </div>
                                            <label class="col-sm-6 control-label"></label>
                                        </div>

                                        <asp:UpdatePanel ID="Panel_AddGood" runat="server" Visible="false">
                                            <ContentTemplate>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Int. Class</label>
                                                    <div class="col-sm-4">
                                                        <asp:TextBox ID="tbGoods_IntClass" runat="server" placeholder="Int. Class ..." CssClass="form-control" />
                                                    </div>
                                                    <label class="col-sm-2 control-label">Goods</label>
                                                    <div class="col-sm-4">
                                                        <asp:TextBox ID="tbGoodsDetail" runat="server" placeholder="Goods ..." CssClass="form-control" />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Local language</label>
                                                    <div class="col-sm-4">
                                                        <asp:DropDownList ID="ddlLocal_language" AutoPostBack="true" runat="server" ValidationGroup="SaveRecordsheet" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged"></asp:DropDownList>
                                                    </div>
                                                    <label class="col-sm-6 control-label"></label>
                                                    <%--<div class="col-sm-4">
                                                           
                                                        </div>--%>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label"></label>
                                                    <div class="col-sm-2">
                                                        <asp:LinkButton ID="btnAddGoodsTolist" runat="server" CssClass="btn btn-primary" Text="Add Goods" OnCommand="btnCommand"
                                                            CommandName="cmdAddGoodsList" ValidationGroup="addGoodsList" />
                                                    </div>
                                                    <label class="col-sm-8 control-label"></label>

                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label"></label>
                                                    <div class="col-sm-10">

                                                        <asp:GridView ID="gvGoodsList"
                                                            runat="server"
                                                            CssClass="table table-bordered table-striped table-responsive"
                                                            HeaderStyle-CssClass="info"
                                                            OnRowCommand="onRowCommand"
                                                            AutoGenerateColumns="false">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center"
                                                                    HeaderStyle-CssClass="text-center"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller">
                                                                    <ItemTemplate>
                                                                        <%# (Container.DataItemIndex + 1) %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:BoundField DataField="drGoodsIntClassText" HeaderText="Details" ItemStyle-CssClass="text-left"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                                <asp:BoundField DataField="drGoodsDetailText" HeaderText="Contact" ItemStyle-CssClass="text-left"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                                <asp:BoundField DataField="drLocallanguageText" HeaderText="Reference" ItemStyle-CssClass="text-left"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                                <asp:TemplateField ItemStyle-HorizontalAlign="center"
                                                                    HeaderStyle-CssClass="text-center">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="btnRemoveGoods" runat="server"
                                                                            CssClass="btn btn-danger btn-xs"
                                                                            OnClientClick="return confirm('Delete data Goods?')"
                                                                            CommandName="cmdRemoveGoods"><i class="fa fa-times"></i>
                                                                        </asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>


                                                    </div>
                                                </div>

                                            </ContentTemplate>
                                        </asp:UpdatePanel>

                                    </div>
                                </div>

                            </div>
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <asp:HiddenField ID="hfU0IDX" runat="server" Value='<%# Eval("u0_doc_idx") %>' />
                            <asp:HiddenField ID="hfU1IDX" runat="server" Value='<%# Eval("u1_doc_idx") %>' />

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Goods</h3>
                                </div>

                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Int. Class</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbGoods_IntClass" runat="server" placeholder="Int. Class ..." CssClass="form-control" />

                                                <asp:RequiredFieldValidator ID="Req_tbGoods_IntClass"
                                                    runat="server" ControlToValidate="tbGoods_IntClass"
                                                    Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*Int. Class"
                                                    ValidationGroup="addGoodsList" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender24" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_tbGoods_IntClass" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                            <label class="col-sm-2 control-label">Goods</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbGoodsDetail" runat="server" placeholder="Goods ..." CssClass="form-control" />

                                                <asp:RequiredFieldValidator ID="Req_tbGoodsDetail"
                                                    runat="server" ControlToValidate="tbGoodsDetail"
                                                    Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*Goods"
                                                    ValidationGroup="addGoodsList" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender31" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_tbGoodsDetail" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Local language</label>
                                            <div class="col-sm-4">
                                                <asp:DropDownList ID="ddlLocal_language" AutoPostBack="true" runat="server" ValidationGroup="addGoodsList" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged"></asp:DropDownList>

                                                <asp:RequiredFieldValidator ID="Req_ddlLocal_language"
                                                    runat="server" ControlToValidate="ddlLocal_language" InitialValue="0"
                                                    Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*Selected Local language"
                                                    ValidationGroup="addGoodsList" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender32" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlLocal_language" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                            <label class="col-sm-6 control-label"></label>
                                            <%--<div class="col-sm-4">
                                                           
                                                        </div>--%>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"></label>
                                            <div class="col-sm-2">
                                                <asp:LinkButton ID="btnAddGoodsTolist" runat="server" CssClass="btn btn-primary" Text="Add Goods" OnCommand="btnCommand"
                                                    CommandName="cmdAddGoodsList" ValidationGroup="addGoodsList" />
                                            </div>
                                            <label class="col-sm-8 control-label"></label>

                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"></label>
                                            <div class="col-sm-10">

                                                <asp:GridView ID="gvGoodsList"
                                                    runat="server"
                                                    CssClass="table table-bordered table-striped table-responsive"
                                                    HeaderStyle-CssClass="info"
                                                    OnRowCommand="onRowCommand"
                                                    AutoGenerateColumns="false">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center"
                                                            HeaderStyle-CssClass="text-center"
                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller">
                                                            <ItemTemplate>
                                                                <%# (Container.DataItemIndex + 1) %>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:BoundField DataField="drGoodsIntClassText" HeaderText="Details" ItemStyle-CssClass="text-left"
                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                        <asp:BoundField DataField="drGoodsDetailText" HeaderText="Contact" ItemStyle-CssClass="text-left"
                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                        <asp:BoundField DataField="drLocallanguageText" HeaderText="Reference" ItemStyle-CssClass="text-left"
                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                        <asp:TemplateField ItemStyle-HorizontalAlign="center"
                                                            HeaderStyle-CssClass="text-center">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="btnRemoveGoods" runat="server"
                                                                    CssClass="btn btn-danger btn-xs"
                                                                    OnClientClick="return confirm('Delete data Goods?')"
                                                                    CommandName="cmdRemoveGoods"><i class="fa fa-times"></i>
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>


                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </InsertItemTemplate>

                    </asp:FormView>
                </div>
                <!-- Form view Goods Detail -->

                <!-- Form view Additional Details -->
                <asp:FormView ID="fvAdditionalDetail" runat="server" DefaultMode="ReadOnly" Width="100%" OnDataBound="FvDetail_DataBound">
                    <EditItemTemplate>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Additional Details</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Additional Details</label>
                                        <div class="col-sm-6">
                                            <asp:Label ID="lbl_additional_idx_edit" Visible="false" Text='<%# Eval("additional_idx") %>' runat="server" />
                                            <asp:TextBox ID="tbAdditionalDetail" Text='<%# Eval("Additional_Details") %>' TextMode="MultiLine" placeholder="Additional Details ..." Rows="2" runat="server" CssClass="form-control" Enabled="true" />
                                        </div>
                                        <label class="col-sm-4 control-label"></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </EditItemTemplate>
                    <InsertItemTemplate>

                        <asp:HiddenField ID="hfU0IDX" runat="server" Value='<%# Eval("u0_doc_idx") %>' />
                        <asp:HiddenField ID="hfU1IDX" runat="server" Value='<%# Eval("u1_doc_idx") %>' />
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Additional Details</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Additional Details</label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="tbAdditionalDetail" TextMode="MultiLine" placeholder="Additional Details ..." Rows="2" runat="server" CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-4 control-label"></label>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </InsertItemTemplate>

                </asp:FormView>
                <!-- Form view Additional Details -->

                <!-- Form view Tademark Profile Details -->
                <asp:FormView ID="fvTademarkProfile" runat="server" DefaultMode="ReadOnly" Width="100%" OnDataBound="FvDetail_DataBound">
                    <EditItemTemplate>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Tademark Profile/ Design/ Invention/ Process</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Name Tademark Profile</label>
                                        <div class="col-sm-4">
                                            <asp:Label ID="lbl_trademark_profile_idx_edit" Visible="false" Text='<%# Eval("trademark_profile_idx") %>' runat="server" />
                                            <asp:TextBox ID="tbName_trademark" Enabled="true" runat="server" Text='<%# Eval("Name") %>' placeholder="Name Tademark Profile ..." CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-2 control-label">Doc (Local Language)</label>
                                        <div class="col-sm-4">
                                            <asp:Label ID="lbl_doc_language_idx" runat="server" Visible="false" Text='<%# Eval("doc_language_idx") %>' placeholder="Name Tademark Profile ..." CssClass="form-control" />
                                            <asp:DropDownList ID="ddlLocal_language_trademark" Enabled="true" AutoPostBack="true" runat="server" ValidationGroup="SaveRecordsheet" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged"></asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Profile Owner</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbProfileOwner" Enabled="true" Text='<%# Eval("Profile_Owner") %>' placeholder="Profile Owner ..." runat="server" CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-2 control-label">Profile Owner (Local Language)</label>
                                        <div class="col-sm-4">
                                            <asp:Label ID="lbl_owner_language_idx" Visible="false" runat="server" Text='<%# Eval("owner_language_idx") %>' placeholder="Name Tademark Profile ..." CssClass="form-control" />
                                            <asp:DropDownList ID="ddlProfileOwnerLanguage" Enabled="true" AutoPostBack="true" runat="server" ValidationGroup="SaveRecordsheet" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged"></asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Profile Client</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbProfileClient" Enabled="true" Text='<%# Eval("Profile_Client") %>' runat="server" placeholder="Profile Client ..." CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-2 control-label">Profile Client (Local Language)</label>
                                        <div class="col-sm-4">
                                            <asp:Label ID="lbl_client_language_idx" Visible="false" runat="server" Text='<%# Eval("client_language_idx") %>' placeholder="Name Tademark Profile ..." CssClass="form-control" />
                                            <asp:DropDownList ID="ddlProfileClientLanguage" Enabled="true" AutoPostBack="true" runat="server" ValidationGroup="SaveRecordsheet" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged"></asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Translation</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbTranslation" Enabled="true" Text='<%# Eval("Translation") %>' runat="server" placeholder="Translation ..." TextMode="MultiLine" Rows="2" CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-2 control-label">Transliteration</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbTransliteration" Enabled="true" Text='<%# Eval("Transliteration") %>' runat="server" placeholder="Transliteration ..." TextMode="MultiLine" Rows="2" CssClass="form-control" />
                                        </div>
                                    </div>

                                    <asp:UpdatePanel ID="Panel_ViewUploadFileTrademark" runat="server">
                                        <ContentTemplate>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Edit Local Filename</label>
                                                <div class="col-sm-6">
                                                    <asp:FileUpload ID="UploadFileTrademark_Edit" CssClass="control-label UploadFileTrademark_Edit multi max-2 accept-png|jpg|pdf maxsize-1024 with-preview" runat="server" />
                                                </div>
                                                <label class="col-sm-4"></label>
                                            </div>

                                        </ContentTemplate>
                                        <Triggers>
                                            <%-- <asp:PostBackTrigger ControlID="lbDocSaveLawOfficerEditFileMemo" />--%>
                                        </Triggers>
                                    </asp:UpdatePanel>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Detail File</label>
                                        <div class="col-sm-10">
                                            <asp:GridView ID="gvFileTrademarkDetail"
                                                runat="server"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered table-responsive col-md-12"
                                                HeaderStyle-CssClass="default"
                                                AllowPaging="true"
                                                OnRowDataBound="gvRowDataBound"
                                                OnPageIndexChanging="gvPageIndexChanging"
                                                PageSize="10">
                                                <PagerStyle CssClass="pageCustom" HorizontalAlign="Left" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">No result</div>
                                                </EmptyDataTemplate>
                                                <Columns>

                                                    <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <small>
                                                                <%# (Container.DataItemIndex + 1) %>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="File Attach">
                                                        <ItemTemplate>
                                                            <div class="col-lg-10">
                                                                <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />
                                                            </div>
                                                            <div class="col-lg-2">
                                                                <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-default" data-original-title="Download" Target="_blank">
                                                                    <i class="fa fa-download"></i></asp:HyperLink>
                                                                <asp:HiddenField runat="server" ID="hidFile" Value='<%# Eval("Download") %>' />
                                                            </div>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>
                                        </div>

                                    </div>


                                </div>
                            </div>
                        </div>
                    </EditItemTemplate>
                    <InsertItemTemplate>

                        <asp:HiddenField ID="hfU0IDX" runat="server" Value='<%# Eval("u0_doc_idx") %>' />
                        <asp:HiddenField ID="hfU1IDX" runat="server" Value='<%# Eval("u1_doc_idx") %>' />
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Tademark Profile/ Design/ Invention/ Process</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <asp:UpdatePanel ID="Panel_TademarkProfile" runat="server">
                                        <ContentTemplate>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label"></label>
                                                <div class="col-sm-4 control-label textleft">
                                                    <asp:CheckBox ID="chkTrademark" runat="server" Font-Bold="false" Text=" Selected Trademark Profile" OnCheckedChanged="chkSelectedIndexChanged" AutoPostBack="true" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right" />
                                                </div>

                                                <asp:UpdatePanel ID="Panel_SearchTrademark" runat="server" Visible="false">
                                                    <ContentTemplate>
                                                        <label class="col-sm-2 control-label"></label>
                                                        <div class="col-sm-4">
                                                            <asp:DropDownList ID="ddlSearchTrademark" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged"></asp:DropDownList>

                                                            <asp:RequiredFieldValidator ID="Req_ddlSearchTrademark"
                                                                runat="server" ControlToValidate="ddlSearchTrademark" InitialValue="0"
                                                                Display="None" SetFocusOnError="true"
                                                                ErrorMessage="*Selected Trademark Profile"
                                                                ValidationGroup="SaveCreateRecordSheet" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender33" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlSearchTrademark" Width="200" PopupPosition="BottomLeft" />
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>

                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Name Tademark Profile</label>
                                                <div class="col-sm-4">
                                                    <asp:TextBox ID="tbName_trademark" runat="server" Text='<%# Eval("Name") %>' ValidationGroup="SaveCreateRecordSheet" placeholder="Name Tademark Profile ..." CssClass="form-control" />

                                                    <asp:RequiredFieldValidator ID="Req_tbName_trademark"
                                                        runat="server" ControlToValidate="tbName_trademark"
                                                        Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*Name Tademark Profile"
                                                        ValidationGroup="SaveCreateRecordSheet" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6s" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_tbName_trademark" Width="200" PopupPosition="BottomLeft" />
                                                </div>
                                                <label class="col-sm-2 control-label">Doc (Local Language)</label>
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddlLocal_language_trademark" AutoPostBack="true" runat="server" ValidationGroup="SaveCreateRecordSheet" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged"></asp:DropDownList>

                                                    <asp:RequiredFieldValidator ID="Req_ddlLocal_language_trademark"
                                                        runat="server" ControlToValidate="ddlLocal_language_trademark" InitialValue="0"
                                                        Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*Select Doc (Local Language)"
                                                        ValidationGroup="SaveCreateRecordSheet" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlLocal_language_trademark" Width="200" PopupPosition="BottomLeft" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Profile Owner</label>
                                                <div class="col-sm-4">
                                                    <asp:TextBox ID="tbProfileOwner" placeholder="Profile Owner ..." ValidationGroup="SaveCreateRecordSheet" runat="server" CssClass="form-control" />

                                                    <asp:RequiredFieldValidator ID="Req_tbProfileOwner"
                                                        runat="server" ControlToValidate="tbProfileOwner"
                                                        Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*Select Profile Owner"
                                                        ValidationGroup="SaveCreateRecordSheet" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_tbProfileOwner" Width="200" PopupPosition="BottomLeft" />
                                                </div>
                                                <label class="col-sm-2 control-label">Profile Owner (Local Language)</label>
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddlProfileOwnerLanguage" AutoPostBack="true" runat="server" ValidationGroup="SaveCreateRecordSheet" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged"></asp:DropDownList>

                                                    <asp:RequiredFieldValidator ID="Req_ddlProfileOwnerLanguage"
                                                        runat="server" ControlToValidate="ddlProfileOwnerLanguage" InitialValue="0"
                                                        Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*Select Profile Owner (Local Language)"
                                                        ValidationGroup="SaveCreateRecordSheet" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlProfileOwnerLanguage" Width="200" PopupPosition="BottomLeft" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Profile Client</label>
                                                <div class="col-sm-4">
                                                    <asp:TextBox ID="tbProfileClient" runat="server" placeholder="Profile Client ..." ValidationGroup="SaveCreateRecordSheet" CssClass="form-control" />

                                                    <asp:RequiredFieldValidator ID="Req_tbProfileClient"
                                                        runat="server" ControlToValidate="tbProfileClient"
                                                        Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*Profile Client"
                                                        ValidationGroup="SaveCreateRecordSheet" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_tbProfileClient" Width="200" PopupPosition="BottomLeft" />
                                                </div>
                                                <label class="col-sm-2 control-label">Profile Client (Local Language)</label>
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddlProfileClientLanguage" AutoPostBack="true" runat="server" ValidationGroup="SaveCreateRecordSheet" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged"></asp:DropDownList>

                                                    <asp:RequiredFieldValidator ID="Req_ddlProfileClientLanguage"
                                                        runat="server" ControlToValidate="ddlProfileClientLanguage" InitialValue="0"
                                                        Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*Select Profile Client (Local Language)"
                                                        ValidationGroup="SaveCreateRecordSheet" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlProfileClientLanguage" Width="200" PopupPosition="BottomLeft" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Translation</label>
                                                <div class="col-sm-4">
                                                    <asp:TextBox ID="tbTranslation" runat="server" placeholder="Translation ..." TextMode="MultiLine" Rows="2" CssClass="form-control" />

                                                    <asp:RequiredFieldValidator ID="Req_tbTranslation"
                                                        runat="server" ControlToValidate="tbTranslation"
                                                        Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*Translation"
                                                        ValidationGroup="SaveCreateRecordSheet" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_tbTranslation" Width="200" PopupPosition="BottomLeft" />
                                                </div>
                                                <label class="col-sm-2 control-label">Transliteration</label>
                                                <div class="col-sm-4">
                                                    <asp:TextBox ID="tbTransliteration" runat="server" placeholder="Transliteration ..." TextMode="MultiLine" Rows="2" CssClass="form-control" />

                                                    <asp:RequiredFieldValidator ID="Req_tbTransliteration"
                                                        runat="server" ControlToValidate="tbTransliteration"
                                                        Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*Transliteration"
                                                        ValidationGroup="SaveCreateRecordSheet" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_tbTransliteration" Width="200" PopupPosition="BottomLeft" />
                                                </div>
                                            </div>

                                            <asp:UpdatePanel ID="Panel_UploadFileTrademark" runat="server">
                                                <ContentTemplate>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Local Filename</label>
                                                        <div class="col-sm-6">
                                                            <asp:FileUpload ID="UploadFileTrademark" CssClass="control-label UploadFileTrademark multi max-2 accept-png|jpg|pdf maxsize-1024 with-preview" ValidationGroup="SaveCreateRecordSheet" runat="server" />

                                                            <asp:RequiredFieldValidator ID="Req_UploadFileTrademark"
                                                                runat="server" ControlToValidate="UploadFileTrademark"
                                                                Display="None" SetFocusOnError="true"
                                                                ErrorMessage="*Attach File Local Filename"
                                                                ValidationGroup="SaveCreateRecordSheet" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender48" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_UploadFileTrademark" Width="200" PopupPosition="BottomLeft" />


                                                        </div>
                                                        <label class="col-sm-4"></label>
                                                    </div>

                                                </ContentTemplate>
                                                <Triggers>
                                                    <%-- <asp:PostBackTrigger ControlID="lbDocSaveLawOfficerEditFileMemo" />--%>
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ddlSearchTrademark" EventName="SelectedIndexChanged" />
                                            <asp:AsyncPostBackTrigger ControlID="ddlLocal_language_trademark" EventName="SelectedIndexChanged" />
                                            <asp:AsyncPostBackTrigger ControlID="ddlProfileOwnerLanguage" EventName="SelectedIndexChanged" />
                                            <asp:AsyncPostBackTrigger ControlID="ddlProfileClientLanguage" EventName="SelectedIndexChanged" />

                                            <%--<asp:PostBackTrigger ControlID="btnSaveTrademarkRecordSheet" />--%>
                                        </Triggers>
                                    </asp:UpdatePanel>

                                </div>
                            </div>
                        </div>
                    </InsertItemTemplate>
                </asp:FormView>
                <!-- Form view Tademark Profile Details -->

                <!-- gvActions Details -->
                <div id="div_Actions" runat="server" style="overflow: auto">
                    <asp:GridView ID="gvActions" runat="server"
                        AutoGenerateColumns="False"
                        CssClass="table table-bordered table-striped table-responsive col-md-12 footable"
                        HeaderStyle-CssClass="default"
                        OnPageIndexChanging="gvPageIndexChanging"
                        OnRowDataBound="gvRowDataBound"
                        AllowPaging="True"
                        PageSize="10">
                        <RowStyle Font-Size="Small" />
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">--- No Data ---</div>
                        </EmptyDataTemplate>
                        <Columns>

                            <asp:TemplateField HeaderText="#" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                <ItemTemplate>
                                    <%# (Container.DataItemIndex + 1) %>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Actions" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="40%">
                                <ItemTemplate>

                                    <asp:TextBox ID="tbActionsDetail" TextMode="MultiLine" placeholder="Actions ..." Rows="2" runat="server" CssClass="form-control" />

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Owner" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="40%">
                                <ItemTemplate>

                                    <asp:TextBox ID="tbOwnerDetail" TextMode="MultiLine" placeholder="Owner ..." Rows="2" runat="server" CssClass="form-control" />

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Agent" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                <ItemTemplate>

                                    <asp:Label ID="lblaction_idx" runat="server" Visible="false" Text='<%# Eval("action_idx") %>' />
                                    <asp:TextBox ID="tb_action_name_en" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("action_name_en") %>' />


                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>
                </div>
                <!-- gvActions Details -->

                <!-- Form view Background of the invention -->
                <asp:FormView ID="fvBackground" runat="server" DefaultMode="ReadOnly" Width="100%" OnDataBound="FvDetail_DataBound">
                    <EditItemTemplate>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Background of the invention/ design</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Background of the invention/ design</label>
                                        <div class="col-sm-6">
                                            <asp:Label ID="lbl_background_idx_edit" Visible="false" Text='<%# Eval("background_idx") %>' runat="server" />
                                            <asp:TextBox ID="tbBackgroundDetail" placeholder="Background of the invention/ design ..." TextMode="MultiLine" Text='<%# Eval("background_detail") %>' Rows="2" runat="server" CssClass="form-control" Enabled="true" />
                                        </div>
                                        <label class="col-sm-4 control-label"></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </EditItemTemplate>
                    <InsertItemTemplate>

                        <asp:HiddenField ID="hfU0IDX" runat="server" Value='<%# Eval("u0_doc_idx") %>' />
                        <asp:HiddenField ID="hfU1IDX" runat="server" Value='<%# Eval("u1_doc_idx") %>' />
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Background of the invention/ design</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Background of the invention/ design</label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="tbBackgroundDetail" TextMode="MultiLine" placeholder="Background of the invention/ design ..." Rows="2" runat="server" CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-4 control-label"></label>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </InsertItemTemplate>

                </asp:FormView>
                <!-- Form view Background of the invention -->

                <!-- Form view Summary of the invention -->
                <asp:FormView ID="fvSummary" runat="server" DefaultMode="ReadOnly" Width="100%" OnDataBound="FvDetail_DataBound">
                    <EditItemTemplate>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Summary of the invention/ design</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Summary of the invention/ design</label>
                                        <div class="col-sm-6">
                                            <asp:Label ID="lbl_summary_idx_edit" Visible="false" Text='<%# Eval("summary_idx") %>' runat="server" />
                                            <asp:TextBox ID="tbSummaryDetail" placeholder="Summary of the invention/ design ..." Text='<%# Eval("summary_detail") %>' TextMode="MultiLine" Rows="2" runat="server" CssClass="form-control" Enabled="true" />
                                        </div>
                                        <label class="col-sm-4 control-label"></label>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </EditItemTemplate>
                    <InsertItemTemplate>

                        <asp:HiddenField ID="hfU0IDX" runat="server" Value='<%# Eval("u0_doc_idx") %>' />
                        <asp:HiddenField ID="hfU1IDX" runat="server" Value='<%# Eval("u1_doc_idx") %>' />
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Summary of the invention/ design</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Summary of the invention/ design</label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="tbSummaryDetail" TextMode="MultiLine" placeholder="Summary of the invention/ design ..." Rows="2" runat="server" CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-4 control-label"></label>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </InsertItemTemplate>

                </asp:FormView>
                <!-- Form view Summary of the invention -->

                <!-- Form view Claims -->
                <asp:FormView ID="fvClaims" runat="server" DefaultMode="ReadOnly" Width="100%" OnDataBound="FvDetail_DataBound">
                    <EditItemTemplate>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Claims</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Claims</label>
                                        <div class="col-sm-6">
                                            <asp:Label ID="lbl_claims_idx_edit" Visible="false" Text='<%# Eval("claims_idx") %>' runat="server" />
                                            <asp:TextBox ID="tbClaimsDetail" placeholder="Claims ..." TextMode="MultiLine" Text='<%# Eval("claims_detail") %>' Rows="2" runat="server" CssClass="form-control" Enabled="true" />
                                        </div>
                                        <label class="col-sm-4 control-label"></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </EditItemTemplate>
                    <InsertItemTemplate>

                        <asp:HiddenField ID="hfU0IDX" runat="server" Value='<%# Eval("u0_doc_idx") %>' />
                        <asp:HiddenField ID="hfU1IDX" runat="server" Value='<%# Eval("u1_doc_idx") %>' />
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Claims</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Claims</label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="tbClaimsDetail" TextMode="MultiLine" placeholder="Claims ..." Rows="2" runat="server" CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-4 control-label"></label>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </InsertItemTemplate>
                </asp:FormView>
                <!-- Form view Claims -->

                <!-- Form view Abstract -->
                <asp:FormView ID="fvAbstract" runat="server" DefaultMode="ReadOnly" Width="100%" OnDataBound="FvDetail_DataBound">
                    <EditItemTemplate>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Additional</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Abstract Details</label>
                                        <div class="col-sm-6">
                                            <asp:Label ID="lbl_abstract_idx_edit" Visible="false" Text='<%# Eval("abstract_idx") %>' runat="server" />
                                            <asp:TextBox ID="tbAbstractDetail" placeholder="Abstract Details ..." Text='<%# Eval("abstract_detail") %>' TextMode="MultiLine" Rows="2" runat="server" CssClass="form-control" Enabled="false" />
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </EditItemTemplate>
                    <InsertItemTemplate>

                        <asp:HiddenField ID="hfU0IDX" runat="server" Value='<%# Eval("u0_doc_idx") %>' />
                        <asp:HiddenField ID="hfU1IDX" runat="server" Value='<%# Eval("u1_doc_idx") %>' />
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Abstract</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Abstract Details</label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="tbAbstractDetail" TextMode="MultiLine" placeholder="Abstract ..." Rows="2" runat="server" CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-4 control-label"></label>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </InsertItemTemplate>
                </asp:FormView>
                <!-- Form view Abstract -->

                <!-- Form view Typeofwork -->
                <asp:FormView ID="fvTypeofwork" runat="server" DefaultMode="ReadOnly" Width="100%" OnDataBound="FvDetail_DataBound">
                    <EditItemTemplate>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Type of work</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Type of work</label>
                                        <div class="col-sm-4">
                                            <asp:Label ID="lbl_typework_idx_edit" runat="server" Visible="false" Text='<%# Eval("typework_idx") %>' placeholder="Type of work ..." CssClass="form-control" />
                                            <asp:RadioButtonList ID="rdoTypeofwork" Font-Bold="true" Font-Size="Small" runat="server" CssClass="">
                                            </asp:RadioButtonList>
                                        </div>
                                        <label class="col-sm-6 control-label"></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </EditItemTemplate>
                    <InsertItemTemplate>

                        <asp:HiddenField ID="hfU0IDX" runat="server" Value='<%# Eval("u0_doc_idx") %>' />
                        <asp:HiddenField ID="hfU1IDX" runat="server" Value='<%# Eval("u1_doc_idx") %>' />
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Type of work</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Type of work</label>

                                        <div class="col-sm-4 control-label textleft">

                                            <asp:RadioButtonList ID="rdoTypeofwork" Font-Bold="true" Font-Size="Small" runat="server" CssClass="">
                                            </asp:RadioButtonList>

                                            <asp:RequiredFieldValidator ID="Req_rdoTypeofwork"
                                                runat="server" ControlToValidate="rdoTypeofwork"
                                                Display="None" SetFocusOnError="true"
                                                ErrorMessage="*Selected Type of work"
                                                ValidationGroup="SaveCreateRecordSheet" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_rdoTypeofwork" Width="200" PopupPosition="BottomLeft" />

                                        </div>

                                        <label class="col-sm-6 control-label"></label>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </InsertItemTemplate>

                </asp:FormView>
                <!-- Form view Typeofwork -->

                <!-- Form view Description of invention -->
                <asp:FormView ID="fvDescription" runat="server" DefaultMode="ReadOnly" Width="100%" OnDataBound="FvDetail_DataBound">
                    <EditItemTemplate>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Descriptions of invention/ design</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Descriptions Details/ design</label>
                                        <div class="col-sm-6">
                                            <asp:Label ID="lbl_description_idx_edit" Visible="false" Text='<%# Eval("description_idx") %>' runat="server" />
                                            <asp:TextBox ID="tbDescriptionDetail" TextMode="MultiLine" Text='<%# Eval("description_detail") %>' placeholder="Description of invention/ design ..." Rows="2" runat="server" CssClass="form-control" Enabled="true" />
                                        </div>
                                        <label class="col-sm-4 control-label"></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </EditItemTemplate>
                    <InsertItemTemplate>

                        <asp:HiddenField ID="hfU0IDX" runat="server" Value='<%# Eval("u0_doc_idx") %>' />
                        <asp:HiddenField ID="hfU1IDX" runat="server" Value='<%# Eval("u1_doc_idx") %>' />
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Descriptions of invention/ design</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Descriptions Details/ design</label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="tbDescriptionDetail" TextMode="MultiLine" placeholder="Description of invention/ design ..." Rows="2" runat="server" CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-4 control-label"></label>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </InsertItemTemplate>
                </asp:FormView>
                <!-- Form view Description of invention -->

                <asp:UpdatePanel ID="Panel_SaveRecordSheet" runat="server" Visible="false" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="form-group">
                            <div class="pull-right">
                                <asp:LinkButton ID="btnSaveTrademarkRecordSheet" CssClass="btn btn-success" data-toggle="tooltip" title="Save Record" runat="server"
                                    CommandName="cmdSaveTrademarkRecordSheet" ValidationGroup="SaveCreateRecordSheet" OnCommand="btnCommand"><i class="fa fa-save" aria-hidden="true"></i> Save RecordSheet</asp:LinkButton>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnSaveTrademarkRecordSheet" />
                    </Triggers>
                </asp:UpdatePanel>

                <asp:UpdatePanel ID="Panel_UpdateRecordSheet" runat="server" Visible="false" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="form-group">
                            <div class="pull-right">
                                <asp:LinkButton ID="btnUpdateTrademarkRecordSheet" CssClass="btn btn-success" data-toggle="tooltip" title="Update Record" runat="server"
                                    CommandName="cmdUpdateTrademarkRecordSheet" ValidationGroup="SaveCreateRecordSheet" OnCommand="btnCommand"><i class="fa fa-save" aria-hidden="true"></i> Update RecordSheet</asp:LinkButton>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnUpdateTrademarkRecordSheet" />
                    </Triggers>
                </asp:UpdatePanel>

                <!-- Log Detail Record Status -->
                <div id="div_LogRecordSheet" runat="server" visible="false">
                    <%--<div class="panel-heading">Log Detail</div>--%>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">Record Status</h3>
                        </div>
                        <div class="panel-body">
                            <div id="divLogRecordSheet" runat="server" style="overflow: auto; height: 150px">
                                <table class="table table-bordered table-striped table-responsive col-md-12">
                                    <asp:Repeater ID="rptLogRecordSheet" runat="server">
                                        <HeaderTemplate>
                                            <tr>
                                                <th>date / time</th>
                                                <th>operator</th>
                                                <th>process</th>
                                                <%--<th>ผลการดำเนินการ</th>--%>
                                                <th>comment</th>
                                            </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td data-th="date / time"><%# Eval("create_date") %> <%# Eval("time_create") %></td>
                                                <td data-th="operator"><%# Eval("emp_name_en") %> ( <%# Eval("current_artor") %> )</td>
                                                <td data-th="process"><%# Eval("current_decision") %></td>
                                                <td data-th="comment"><%# Eval("comment") %></td>
                                                <%--<td data-th="ผลการดำเนินการ"><%# Eval("current_status_name") %></td>--%>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </table>
                            </div>
                        </div>
                        <%-- <div class="m-t-10 m-b-10"></div>--%>
                    </div>
                </div>
                <!-- Log Detail Record Status -->

            </div>

        </asp:View>
        <!--View Detail-->


    </asp:MultiView>
    <!--multiview-->

    <!--script-->
    <script src='<%=ResolveUrl("~/Scripts/jquery.MultiFileNew.js")%>'></script>

    <script>
        $(function () {
            $('.UploadFile').MultiFile({

            });

        });
        $(function () {
            $('.UploadFileMemo').MultiFile({

            });

        });

        $(function () {
            $('.UploadFileTrademark').MultiFile({

            });

        });

        $(function () {
            $('.UploadFileTrademark_Edit').MultiFile({

            });

        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.UploadFile').MultiFile({

                });

            });

            $(function () {
                $('.UploadFileMemo').MultiFile({

                });

            });

            $(function () {
                $('.UploadFileTrademark').MultiFile({

                });

            });

            $(function () {
                $('.UploadFileTrademark_Edit').MultiFile({

                });

            });


        });

        //})
    </script>

    <script type="text/javascript">
        function confirmWithOutValidated(message, validateGroup) {
            if (validateGroup != '') {
                var validated = Page_ClientValidate(validateGroup);
                if (validated) {
                    return confirm(message);
                }
            } else {
                return confirm(message);
            }
        }


        $(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true,
                widgetPositioning: {
                    horizontal: 'left',
                    vertical: 'bottom'
                }
            });

            $('.show-from-onclick').click(function () {

                $('.from-date-datepicker').data("DateTimePicker").show;

            });

            $('.from-date-datepicker-top').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true,
                widgetPositioning: {
                    horizontal: 'left',
                    vertical: 'top'
                }
            });
            $('.show-from-top-onclick').click(function () {

                $('.from-date-datepicker-top').data("DateTimePicker").show;

            });

        });

        //});

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY',
                    ignoreReadonly: true,
                    widgetPositioning: {
                        horizontal: 'left',
                        vertical: 'bottom'
                    }
                });
                $('.show-from-onclick').click(function () {
                    $('.from-date-datepicker').data("DateTimePicker").show;
                });

                $('.from-date-datepicker-top').datetimepicker({
                    format: 'DD/MM/YYYY',
                    ignoreReadonly: true,
                    widgetPositioning: {
                        horizontal: 'left',
                        vertical: 'top'
                    }
                });

                $('.show-from-top-onclick').click(function () {

                    $('.from-date-datepicker-top').data("DateTimePicker").show;
                });
            });
        });

    </script>

    <!--script-->

</asp:Content>
