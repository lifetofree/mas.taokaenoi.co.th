﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_qa_qmr_problem : System.Web.UI.Page
{

    #region Connect
    data_employee _dtEmployee = new data_employee();
    function_tool _funcTool = new function_tool();
    data_qmr_problem _dtproblem = new data_qmr_problem();

    string _localJson = String.Empty;
    public string checkfile;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetMaster = _serviceUrl + ConfigurationManager.AppSettings["urlSelectMaster_Problem"];
    static string _urlInsertMaster = _serviceUrl + ConfigurationManager.AppSettings["urlInsertMaster_Problem"];
    static string _urlDeleteMaster = _serviceUrl + ConfigurationManager.AppSettings["urlDeleteMaster_Problem"];
    static string _urlInsertSystem = _serviceUrl + ConfigurationManager.AppSettings["urlInsertSystem_Problem"];
    static string _urlGetSystem = _serviceUrl + ConfigurationManager.AppSettings["urlSelectSystem_Problem"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    int checkcomment = 1;
    int checkcount;
    DateTime datemodify;
    DateTime dateexp;
    DateTime dateproblem;
    DateTime dateget;

    #endregion

    #region Pageload
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ViewState["EmpIDX"] = int.Parse(Session["emp_idx"].ToString());

            select_empIdx_present();
            ViewState["check_onlyrepot"] = "-";
            select_admin(int.Parse(ViewState["Sec_idx"].ToString()), int.Parse(ViewState["EmpIDX"].ToString()));
            ViewState["check_search"] = "0";
            ViewState["check_gvmaster_risk"] = "1";
            
            SetDefaultpage(1);

        }

        linkBtnTrigger(_divMenuBtnToDivIndex);
        linkBtnTrigger(_divMenuBtnToDivAdd);
        linkBtnTrigger(_divMenuBtnToDivApprove);
        linkBtnTrigger(_divMenuBtnToDivMasterSupplier);
        linkBtnTrigger(_divMenuBtnToDivMasterMat);
        linkBtnTrigger(_divMenuBtnToDivReport);
        //linkBtnTrigger(_divMenuBtnToDivManual);

    }
    #endregion

    #region Insert && Update && Delete Master
    protected void insertmaster_product(string typeproduct, int status, int cempidx, string m0_tpidx, int m0_groupidx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxm0_ProblemDocument = new M0_ProblemDocument[1];
        M0_ProblemDocument _insert = new M0_ProblemDocument();

        _insert.typeproduct_name = typeproduct;
        _insert.status = status;
        _insert.m0_tpidx = _funcTool.convertToInt(m0_tpidx);
        _insert.m0_group_tpidx = m0_groupidx;
        _dtproblem.Boxm0_ProblemDocument[0] = _insert;


        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 1;
        _select.CEmpIDX = cempidx;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;



        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlInsertMaster, _dtproblem);


        if (_dtproblem.ReturnCode.ToString() == "1")
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว!!!');", true);

        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('บันทึกข้อมูลเสร็จสมบูรณ์');", true);

        }
    }

    protected void deletemaster_product(int status, int cempidx, int m0_tpidx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxm0_ProblemDocument = new M0_ProblemDocument[1];
        M0_ProblemDocument _insert = new M0_ProblemDocument();

        _insert.status = status;
        _insert.m0_tpidx = m0_tpidx;
        _dtproblem.Boxm0_ProblemDocument[0] = _insert;


        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 1;
        _select.CEmpIDX = cempidx;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlDeleteMaster, _dtproblem);


    }

    protected void insertmaster_unit(string unit, int status, int cempidx, string m0_unidx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxm0_ProblemDocument = new M0_ProblemDocument[1];
        M0_ProblemDocument _insert = new M0_ProblemDocument();

        _insert.unit_name = unit;
        _insert.status = status;
        _insert.m0_unidx = _funcTool.convertToInt(m0_unidx);
        _dtproblem.Boxm0_ProblemDocument[0] = _insert;


        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 2;
        _select.CEmpIDX = cempidx;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;



        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlInsertMaster, _dtproblem);


        if (_dtproblem.ReturnCode.ToString() == "1")
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว!!!');", true);

        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('บันทึกข้อมูลเสร็จสมบูรณ์');", true);

        }
    }

    protected void deletemaster_unit(int status, int cempidx, int m0_unidx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxm0_ProblemDocument = new M0_ProblemDocument[1];
        M0_ProblemDocument _insert = new M0_ProblemDocument();

        _insert.status = status;
        _insert.m0_unidx = m0_unidx;
        _dtproblem.Boxm0_ProblemDocument[0] = _insert;


        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 2;
        _select.CEmpIDX = cempidx;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlDeleteMaster, _dtproblem);


    }

    protected void insertmaster_place(int LocIDX, int BuildIDX, string production, int status, int cempidx, string m0_plidx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxm0_ProblemDocument = new M0_ProblemDocument[1];
        M0_ProblemDocument _insert = new M0_ProblemDocument();

        _insert.LocIDX = LocIDX;
        _insert.BuildingIDX = BuildIDX;
        _insert.production_name = production;

        _insert.status = status;
        _insert.m0_plidx = _funcTool.convertToInt(m0_plidx);
        _dtproblem.Boxm0_ProblemDocument[0] = _insert;


        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 3;
        _select.CEmpIDX = cempidx;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;



        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlInsertMaster, _dtproblem);


        if (_dtproblem.ReturnCode.ToString() == "1")
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว!!!');", true);

        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('บันทึกข้อมูลเสร็จสมบูรณ์');", true);

        }
    }

    protected void deletemaster_place(int status, int cempidx, int m0_plidx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxm0_ProblemDocument = new M0_ProblemDocument[1];
        M0_ProblemDocument _insert = new M0_ProblemDocument();

        _insert.status = status;
        _insert.m0_plidx = m0_plidx;
        _dtproblem.Boxm0_ProblemDocument[0] = _insert;


        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 3;
        _select.CEmpIDX = cempidx;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlDeleteMaster, _dtproblem);


    }

    protected void insertmaster_problem(int m0_tpidx, string problem_name, int status, int cempidx, string m0_pridx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxm0_ProblemDocument = new M0_ProblemDocument[1];
        M0_ProblemDocument _insert = new M0_ProblemDocument();

        _insert.m0_tpidx = m0_tpidx;
        _insert.problem_name = problem_name;
        _insert.status = status;
        _insert.m0_pridx = _funcTool.convertToInt(m0_pridx); ;
        _dtproblem.Boxm0_ProblemDocument[0] = _insert;


        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 4;
        _select.CEmpIDX = cempidx;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;



        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlInsertMaster, _dtproblem);


        if (_dtproblem.ReturnCode.ToString() == "1")
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว!!!');", true);

        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('บันทึกข้อมูลเสร็จสมบูรณ์');", true);

        }
    }

    protected void deletemaster_problem(int status, int cempidx, int m0_pridx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxm0_ProblemDocument = new M0_ProblemDocument[1];
        M0_ProblemDocument _insert = new M0_ProblemDocument();

        _insert.status = status;
        _insert.m0_pridx = m0_pridx;
        _dtproblem.Boxm0_ProblemDocument[0] = _insert;


        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 4;
        _select.CEmpIDX = cempidx;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlDeleteMaster, _dtproblem);


    }

    protected void insertmaster_supplier(int m0_supidx, string sup_name, string email, int status, int cempidx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxm0_ProblemDocument = new M0_ProblemDocument[1];
        M0_ProblemDocument _insert = new M0_ProblemDocument();

        _insert.m0_supidx = m0_supidx;
        _insert.sup_name = sup_name;
        _insert.email = email;
        _insert.status = status;
        _dtproblem.Boxm0_ProblemDocument[0] = _insert;


        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 5;
        _select.CEmpIDX = cempidx;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;



        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlInsertMaster, _dtproblem);


        if (_dtproblem.ReturnCode.ToString() == "1")
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว!!!');", true);

        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('บันทึกข้อมูลเสร็จสมบูรณ์');", true);

        }
    }

    protected void ImportFileSuppliername(string filePath, string Extension, string isHDR, string fileName, int cempidx, int condition)
    {
        string conStr = String.Empty;
        conStr = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
        conStr = String.Format(conStr, filePath, "Yes");
        OleDbConnection connExcel = new OleDbConnection(conStr);
        OleDbCommand cmdExcel = new OleDbCommand();
        OleDbDataAdapter oda = new OleDbDataAdapter();
        DataTable dt = new DataTable();

        cmdExcel.Connection = connExcel;
        connExcel.Open();
        DataTable dtExcelSchema;
        dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
        string SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
        connExcel.Close();
        connExcel.Open();
        cmdExcel.CommandText = "SELECT * From [" + SheetName + "]";
        oda.SelectCommand = cmdExcel;
        oda.Fill(dt);
        connExcel.Close();

        _dtproblem = new data_qmr_problem();

        string idxCreated = String.Empty;
        for (var i = 0; i <= dt.Rows.Count - 1;)
        {
            if (dt.Rows[i][0].ToString().Trim() != "")
            {
                M0_ProblemDocument import = new M0_ProblemDocument();
                _dtproblem.Boxm0_ProblemDocument = new M0_ProblemDocument[1];

                import.sup_name = dt.Rows[i][0].ToString().Trim();
                import.email = dt.Rows[i][1].ToString().Trim();
                import.status = 1;
                _dtproblem.Boxm0_ProblemDocument[0] = import;

                _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
                U0_ProblemDocument _select = new U0_ProblemDocument();

                _select.condition = condition;
                _select.CEmpIDX = cempidx;

                _dtproblem.Boxu0_ProblemDocument[0] = _select;

                //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

                _dtproblem = callServicePostQMR_Problem(_urlInsertMaster, _dtproblem);
            }
            i++;
        }
    }

    protected void deletemaster_supplier(int status, int cempidx, int m0_supidx, int condition)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxm0_ProblemDocument = new M0_ProblemDocument[1];
        M0_ProblemDocument _insert = new M0_ProblemDocument();

        _insert.status = status;
        _insert.m0_supidx = m0_supidx;
        _dtproblem.Boxm0_ProblemDocument[0] = _insert;


        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = condition;
        _select.CEmpIDX = cempidx;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlDeleteMaster, _dtproblem);

        if (_dtproblem.ReturnCode.ToString() == "0")
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินการเสร็จเรียบร้อย !!');", true);

        }
    }

    protected void insertmaster_mat(int matidx, int mat_no, string matname, int status, int cempidx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxm0_ProblemDocument = new M0_ProblemDocument[1];
        M0_ProblemDocument _insert = new M0_ProblemDocument();

        _insert.matidx = matidx;
        _insert.mat_no = mat_no;
        _insert.matname = matname;
        _insert.status = status;
        _dtproblem.Boxm0_ProblemDocument[0] = _insert;


        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 7;
        _select.CEmpIDX = cempidx;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlInsertMaster, _dtproblem);


        if (_dtproblem.ReturnCode.ToString() == "1")
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว!!!');", true);

        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('บันทึกข้อมูลเสร็จสมบูรณ์');", true);

        }
    }

    protected void deletemaster_mat(int status, int cempidx, int matidx, int condition)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxm0_ProblemDocument = new M0_ProblemDocument[1];
        M0_ProblemDocument _insert = new M0_ProblemDocument();

        _insert.status = status;
        _insert.matidx = matidx;
        _dtproblem.Boxm0_ProblemDocument[0] = _insert;


        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = condition;
        _select.CEmpIDX = cempidx;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlDeleteMaster, _dtproblem);

        if (_dtproblem.ReturnCode.ToString() == "0")
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินการเสร็จเรียบร้อย !!');", true);

        }
    }

    protected void ImportFileMatNumber(string filePath, string Extension, string isHDR, string fileName, int cempidx, int condition)
    {
        string conStr = String.Empty;
        conStr = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
        conStr = String.Format(conStr, filePath, "Yes");
        OleDbConnection connExcel = new OleDbConnection(conStr);
        OleDbCommand cmdExcel = new OleDbCommand();
        OleDbDataAdapter oda = new OleDbDataAdapter();
        DataTable dt = new DataTable();

        cmdExcel.Connection = connExcel;
        connExcel.Open();
        DataTable dtExcelSchema;
        dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
        string SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
        connExcel.Close();
        connExcel.Open();
        cmdExcel.CommandText = "SELECT * From [" + SheetName + "]";
        oda.SelectCommand = cmdExcel;
        oda.Fill(dt);
        connExcel.Close();

        _dtproblem = new data_qmr_problem();

        string idxCreated = String.Empty;
        for (var i = 0; i <= dt.Rows.Count - 1;)
        {
            if (dt.Rows[i][0].ToString().Trim() != "")
            {
                M0_ProblemDocument import = new M0_ProblemDocument();
                _dtproblem.Boxm0_ProblemDocument = new M0_ProblemDocument[1];

                import.mat_no = int.Parse(dt.Rows[i][0].ToString().Trim());
                import.matname = dt.Rows[i][1].ToString().Trim();

                _dtproblem.Boxm0_ProblemDocument[0] = import;

                _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
                U0_ProblemDocument _select = new U0_ProblemDocument();

                _select.condition = condition;
                _select.CEmpIDX = cempidx;

                _dtproblem.Boxu0_ProblemDocument[0] = _select;
                _dtproblem = callServicePostQMR_Problem(_urlInsertSystem, _dtproblem);
            }
            i++;
        }
    }

    protected void insertmaster_material_risk(string material_risk, int status, int cempidx, string m0_mridx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxm0_ProblemDocument = new M0_ProblemDocument[1];
        M0_ProblemDocument _insert = new M0_ProblemDocument();

        _insert.material_risk = material_risk;
        _insert.status = status;
        _insert.m0_mridx = _funcTool.convertToInt(m0_mridx);
        _dtproblem.Boxm0_ProblemDocument[0] = _insert;


        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 8;
        _select.CEmpIDX = cempidx;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;



        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlInsertMaster, _dtproblem);


        if (_dtproblem.ReturnCode.ToString() == "1")
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว!!!');", true);

        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('บันทึกข้อมูลเสร็จสมบูรณ์');", true);

        }
    }

    protected void deletemaster_matterial_risk(int status, int cempidx, int m0_mridx, int condition)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxm0_ProblemDocument = new M0_ProblemDocument[1];
        M0_ProblemDocument _insert = new M0_ProblemDocument();

        _insert.status = status;
        _insert.m0_mridx = m0_mridx;
        _dtproblem.Boxm0_ProblemDocument[0] = _insert;


        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = condition;
        _select.CEmpIDX = cempidx;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlDeleteMaster, _dtproblem);

        if (_dtproblem.ReturnCode.ToString() == "0")
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินการเสร็จเรียบร้อย !!');", true);

        }
    }

    protected void insertmaster_group(string group_name, int status, int cempidx, string m0_group_tpidx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxm0_ProblemDocument = new M0_ProblemDocument[1];
        M0_ProblemDocument _insert = new M0_ProblemDocument();

        _insert.group_name = group_name;
        _insert.status = status;
        _insert.m0_group_tpidx = _funcTool.convertToInt(m0_group_tpidx);
        _dtproblem.Boxm0_ProblemDocument[0] = _insert;


        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 9;
        _select.CEmpIDX = cempidx;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;



        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlInsertMaster, _dtproblem);


        if (_dtproblem.ReturnCode.ToString() == "1")
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว!!!');", true);

        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('บันทึกข้อมูลเสร็จสมบูรณ์');", true);

        }
    }

    protected void deletemaster_group(int status, int cempidx, int m0_group_tpidx, int condition)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxm0_ProblemDocument = new M0_ProblemDocument[1];
        M0_ProblemDocument _insert = new M0_ProblemDocument();

        _insert.status = status;
        _insert.m0_group_tpidx = m0_group_tpidx;
        _dtproblem.Boxm0_ProblemDocument[0] = _insert;


        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = condition;
        _select.CEmpIDX = cempidx;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlDeleteMaster, _dtproblem);

        if (_dtproblem.ReturnCode.ToString() == "0")
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินการเสร็จเรียบร้อย !!');", true);

        }
    }

    protected void insertmaster_product_m1(string typeproduct_name_m1, int status, int cempidx, string m1_tpidx, int m0_tpidx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxm0_ProblemDocument = new M0_ProblemDocument[1];
        M0_ProblemDocument _insert = new M0_ProblemDocument();

        _insert.typeproduct_name_m1 = typeproduct_name_m1;
        _insert.status = status;
        _insert.m1_tpidx = _funcTool.convertToInt(m1_tpidx);
        _insert.m0_tpidx = m0_tpidx;

        _dtproblem.Boxm0_ProblemDocument[0] = _insert;


        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 10;
        _select.CEmpIDX = cempidx;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;



        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlInsertMaster, _dtproblem);


        if (_dtproblem.ReturnCode.ToString() == "1")
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว!!!');", true);

        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('บันทึกข้อมูลเสร็จสมบูรณ์');", true);

        }
    }

    protected void deletemaster_product_m1(int status, int cempidx, int m1_tpidx, int condition)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxm0_ProblemDocument = new M0_ProblemDocument[1];
        M0_ProblemDocument _insert = new M0_ProblemDocument();

        _insert.status = status;
        _insert.m1_tpidx = m1_tpidx;
        _dtproblem.Boxm0_ProblemDocument[0] = _insert;


        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = condition;
        _select.CEmpIDX = cempidx;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlDeleteMaster, _dtproblem);

        if (_dtproblem.ReturnCode.ToString() == "0")
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินการเสร็จเรียบร้อย !!');", true);

        }
    }

    protected void insertmaster_product_m2(string typeproduct_name_m2, int status, int cempidx, string m2_tpidx, int m1_tpidx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxm0_ProblemDocument = new M0_ProblemDocument[1];
        M0_ProblemDocument _insert = new M0_ProblemDocument();

        _insert.typeproduct_name_m2 = typeproduct_name_m2;
        _insert.status = status;
        _insert.m2_tpidx = _funcTool.convertToInt(m2_tpidx);
        _insert.m1_tpidx = m1_tpidx;

        _dtproblem.Boxm0_ProblemDocument[0] = _insert;


        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 11;
        _select.CEmpIDX = cempidx;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;



        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlInsertMaster, _dtproblem);


        if (_dtproblem.ReturnCode.ToString() == "1")
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว!!!');", true);

        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('บันทึกข้อมูลเสร็จสมบูรณ์');", true);

        }
    }

    protected void deletemaster_product_m2(int status, int cempidx, int m2_tpidx, int condition)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxm0_ProblemDocument = new M0_ProblemDocument[1];
        M0_ProblemDocument _insert = new M0_ProblemDocument();

        _insert.status = status;
        _insert.m2_tpidx = m2_tpidx;
        _dtproblem.Boxm0_ProblemDocument[0] = _insert;


        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = condition;
        _select.CEmpIDX = cempidx;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlDeleteMaster, _dtproblem);

        if (_dtproblem.ReturnCode.ToString() == "0")
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินการเสร็จเรียบร้อย !!');", true);

        }
    }

    #endregion

    #region Insert & Update System
    protected void Insert_System()
    {
        var ddllocation_problem = (DropDownList)FvinsertProblem.FindControl("ddllocation_problem");
        var ddlproduction_problem = (DropDownList)FvinsertProblem.FindControl("ddlproduction_problem");
        var ddlgroupproduct_problem = (DropDownList)FvinsertProblem.FindControl("ddlgroupproduct_problem");
        var ddltypeproduct_problem = (DropDownList)FvinsertProblem.FindControl("ddltypeproduct_problem");
        var ddltypeproduct_problem_m1 = (DropDownList)FvinsertProblem.FindControl("ddltypeproduct_problem_m1");
        var ddltypeproduct_problem_m2 = (DropDownList)FvinsertProblem.FindControl("ddltypeproduct_problem_m2");
        var ddlmat_no = (DropDownList)FvinsertProblem.FindControl("ddlmat_no");
        var txtmat_name = (TextBox)FvinsertProblem.FindControl("txtmat_name");
        var txtbatch_name = (TextBox)FvinsertProblem.FindControl("txtbatch_name");
        var ddl_sup = (DropDownList)FvinsertProblem.FindControl("ddl_sup");
        var txtgetdate = (TextBox)FvinsertProblem.FindControl("txtgetdate");
        var txtqtyget = (TextBox)FvinsertProblem.FindControl("txtqtyget");
        var ddlunit_get = (DropDownList)FvinsertProblem.FindControl("ddlunit_get");
        var txtqty_problem = (TextBox)FvinsertProblem.FindControl("txtqty_problem");
        var ddlunit_problem = (DropDownList)FvinsertProblem.FindControl("ddlunit_problem");
        var txtqty_random = (TextBox)FvinsertProblem.FindControl("txtqty_random");
        var ddlunit_random = (DropDownList)FvinsertProblem.FindControl("ddlunit_random");
        var ddltype_problem = (DropDownList)FvinsertProblem.FindControl("ddltype_problem");
        var txtremark = (TextBox)FvinsertProblem.FindControl("txtremark");
        var txtdatemodify = (TextBox)FvinsertProblem.FindControl("txtdatemodify");
        var txtdateexp = (TextBox)FvinsertProblem.FindControl("txtdateexp");
        var txtdateproblem = (TextBox)FvinsertProblem.FindControl("txtdateproblem");
        var lblcount_problem = (Label)FvinsertProblem.FindControl("lblcount_problem");

        int count_sup = 1;
        CultureInfo provider = CultureInfo.InvariantCulture;

        datemodify = DateTime.ParseExact(txtdatemodify.Text, "dd/MM/yyyy", provider);

        if (txtdateexp.Text != String.Empty && txtdateexp.Text != null & txtdateexp.Text != "")
        {
            dateexp = DateTime.ParseExact(txtdateexp.Text, "dd/MM/yyyy", provider);
        }

        dateproblem = DateTime.ParseExact(txtdateproblem.Text, "dd/MM/yyyy", provider);
        dateget = DateTime.ParseExact(txtgetdate.Text, "dd/MM/yyyy", provider);


        count_sup += int.Parse(lblcount_problem.Text);
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _insert = new U0_ProblemDocument();

        _insert.LocIDX_ref = int.Parse(ddllocation_problem.SelectedValue);
        _insert.m0_plidx = int.Parse(ddlproduction_problem.SelectedValue);
        _insert.m0_group_tpidx = int.Parse(ddlgroupproduct_problem.SelectedValue);
        _insert.m0_tpidx = int.Parse(ddltypeproduct_problem.SelectedValue);
        _insert.m1_tpidx = int.Parse(ddltypeproduct_problem_m1.SelectedValue);
        _insert.m2_tpidx = int.Parse(ddltypeproduct_problem_m2.SelectedValue);
        _insert.matidx = int.Parse(ddlmat_no.SelectedValue);
        _insert.matname = txtmat_name.Text;
        _insert.batchnumber = txtbatch_name.Text;
        _insert.m0_supidx = int.Parse(ddl_sup.SelectedValue);
        _insert.dategetproduct = txtgetdate.Text;
        _insert.qty_get = txtqtyget.Text;
        _insert.unit_get = int.Parse(ddlunit_get.SelectedValue);
        _insert.qty_problem = txtqty_problem.Text;
        _insert.unit_problem = int.Parse(ddlunit_problem.SelectedValue);
        _insert.qty_random = txtqty_random.Text;
        _insert.unit_random = int.Parse(ddlunit_random.SelectedValue);
        _insert.m0_pridx = int.Parse(ddltype_problem.SelectedValue);
        _insert.detail_remark = txtremark.Text;
        _insert.datemodify = txtdatemodify.Text;
        _insert.dateexp = txtdateexp.Text;
        _insert.dateproblem = txtdateproblem.Text;
        _insert.unidx = 1;
        _insert.acidx = 1;
        _insert.staidx = 1;
        _insert.orgidx = int.Parse(ViewState["Org_idx"].ToString());
        _insert.rdeptidx = int.Parse(ViewState["rdept_idx"].ToString());
        _insert.rsecidx = int.Parse(ViewState["Sec_idx"].ToString());
        _insert.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        _insert.count_sup = count_sup;
        _insert.condition = 1;

        _dtproblem.Boxu0_ProblemDocument[0] = _insert;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));


        //if (datemodify <= dateproblem && datemodify <= dateget) //datemodify < dateexp && 
        //{
        _dtproblem = callServicePostQMR_Problem(_urlInsertSystem, _dtproblem);

        ViewState["ReturnCode_DocCode"] = _dtproblem.ReturnMsg;

        string getPathfile = ConfigurationManager.AppSettings["path_file_qmr_problem"];
        string fileName_upload = ViewState["ReturnCode_DocCode"].ToString();
        string filePath_upload = Server.MapPath(getPathfile + fileName_upload);
        int x = 0;
        if (UploadFileProblem.HasFile)
        {
            if (!Directory.Exists(filePath_upload))
            {
                Directory.CreateDirectory(filePath_upload);
            }
            string extension = Path.GetExtension(UploadFileProblem.FileName);

            foreach (HttpPostedFile uploadedFile in UploadFileProblem.PostedFiles)
            {
                if (uploadedFile.FileName != "" && uploadedFile.FileName != String.Empty)
                {
                    uploadedFile.SaveAs(Server.MapPath(getPathfile + ViewState["ReturnCode_DocCode"].ToString()) + "\\" + fileName_upload + x + extension);
                }
                x++;
            }

        }
        //}
        //else if (datemodify >= dateexp)
        //{
        //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาตรวจสอบวันที่ผลิตและวันที่หมดอายุอีกครั้ง!!!');", true);
        //    txtdateexp.Focus();
        //}
        //else if (datemodify > dateproblem)
        //{
        //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาตรวจสอบวันที่ผลิตและวันที่พบปัญหาอีกครั้ง!!!');", true);
        //    txtdateproblem.Focus();
        //}
        //else if (datemodify > dateget)
        //{
        //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาตรวจสอบวันที่ผลิตและวันที่รับเข้าอายุอีกครั้ง!!!');", true);
        //    txtgetdate.Focus();
        //}
    }

    protected void Update_System(int unidx, int acidx, int staidx, int u0_docidx, string comment, int empidx, int condition, int m0_tpidx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _update = new U0_ProblemDocument();

        _update.unidx = unidx;
        _update.acidx = acidx;
        _update.staidx = staidx;
        _update.u0_docidx = u0_docidx;
        _update.comment = comment;
        _update.m0_tpidx = m0_tpidx;
        _update.CEmpIDX = empidx;
        _update.condition = condition;


        _dtproblem.Boxu0_ProblemDocument[0] = _update;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));
        _dtproblem = callServicePostQMR_Problem(_urlInsertSystem, _dtproblem);


    }

    protected void Edit_Detail_Problem()
    {
        var ddlproduction_problemedit = (DropDownList)FvdetailProblem.FindControl("ddlproduction_problemedit");
        var ddlgroupproduct_problemedit = (DropDownList)FvdetailProblem.FindControl("ddlgroupproduct_problemedit");
        var ddltypeproduct_problemedit = (DropDownList)FvdetailProblem.FindControl("ddltypeproduct_problemedit");
        var ddltypeproduct_problemedit_m1 = (DropDownList)FvdetailProblem.FindControl("ddltypeproduct_problemedit_m1");
        var ddltypeproduct_problemedit_m2 = (DropDownList)FvdetailProblem.FindControl("ddltypeproduct_problemedit_m2");
        var ddlmat_noedit = (DropDownList)FvdetailProblem.FindControl("ddlmat_noedit");
        var txtmat_name_edit = (TextBox)FvdetailProblem.FindControl("txtmat_name_edit");
        var txtbatch_name_edit = (TextBox)FvdetailProblem.FindControl("txtbatch_name_edit");
        var ddl_sup_edit = (DropDownList)FvdetailProblem.FindControl("ddl_sup_edit");
        var txtgetdate_edit = (TextBox)FvdetailProblem.FindControl("txtgetdate_edit");
        var txtqtyget_edit = (TextBox)FvdetailProblem.FindControl("txtqtyget_edit");
        var ddlunit_getedit = (DropDownList)FvdetailProblem.FindControl("ddlunit_getedit");
        var txtqty_problemedit = (TextBox)FvdetailProblem.FindControl("txtqty_problemedit");
        var ddlunit_problemedit = (DropDownList)FvdetailProblem.FindControl("ddlunit_problemedit");
        var txtqty_randomedit = (TextBox)FvdetailProblem.FindControl("txtqty_randomedit");
        var ddlunit_randomedit = (DropDownList)FvdetailProblem.FindControl("ddlunit_randomedit");
        var ddltype_problemedit = (DropDownList)FvdetailProblem.FindControl("ddltype_problemedit");
        var txtremark_edit = (TextBox)FvdetailProblem.FindControl("txtremark_edit");
        var txtdatemodify_edit = (TextBox)FvdetailProblem.FindControl("txtdatemodify_edit");
        var txtdateexp_edit = (TextBox)FvdetailProblem.FindControl("txtdateexp_edit");
        var txtdateproblem_edit = (TextBox)FvdetailProblem.FindControl("txtdateproblem_edit");

        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _update = new U0_ProblemDocument();

        _update.m0_plidx = int.Parse(ddlproduction_problemedit.SelectedValue);
        _update.m0_group_tpidx = int.Parse(ddlgroupproduct_problemedit.SelectedValue);
        _update.m0_tpidx = int.Parse(ddltypeproduct_problemedit.SelectedValue);
        _update.m1_tpidx = int.Parse(ddltypeproduct_problemedit_m1.SelectedValue);
        _update.m2_tpidx = int.Parse(ddltypeproduct_problemedit_m2.SelectedValue);
        _update.m0_pridx = int.Parse(ddltype_problemedit.SelectedValue);
        _update.u1_docidx = int.Parse(ViewState["u1_docidx"].ToString());
        _update.m0_supidx = int.Parse(ddl_sup_edit.SelectedValue);
        _update.matidx = int.Parse(ddlmat_noedit.SelectedValue);
        _update.matname = txtmat_name_edit.Text;
        _update.batchnumber = txtbatch_name_edit.Text;
        _update.datemodify = txtdatemodify_edit.Text;
        _update.dateexp = txtdateexp_edit.Text;
        _update.dateproblem = txtdateproblem_edit.Text;
        _update.dategetproduct = txtgetdate_edit.Text;
        _update.qty_get = txtqtyget_edit.Text;
        _update.unit_get = int.Parse(ddlunit_getedit.SelectedValue);
        _update.qty_problem = txtqty_problemedit.Text;
        _update.unit_problem = int.Parse(ddlunit_problemedit.SelectedValue);
        _update.qty_random = txtqty_randomedit.Text;
        _update.unit_random = int.Parse(ddlunit_randomedit.SelectedValue);
        _update.detail_remark = txtremark_edit.Text;
        _update.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        _update.condition = 3;

        _dtproblem.Boxu0_ProblemDocument[0] = _update;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlInsertSystem, _dtproblem);
    }

    protected void Exchange_System()
    {
        //var ddllocation_exchange = (DropDownList)FvQA_Comment.FindControl("ddllocation_exchange");
        var lblm0_plidx_show = (Label)FvdetailProblem.FindControl("lblm0_plidx_show");
        var lblm0_m0_group_tpidx_show = (Label)FvdetailProblem.FindControl("lblm0_m0_group_tpidx_show");
        var lblm0_tpidx_show = (Label)FvdetailProblem.FindControl("lblm0_tpidx_show");
        var lblm1_tpidx_show = (Label)FvdetailProblem.FindControl("lblm1_tpidx_show");
        var lblm2_tpidx_show = (Label)FvdetailProblem.FindControl("lblm2_tpidx_show");
        var lblmatidx_show = (Label)FvdetailProblem.FindControl("lblmatidx_show");
        var lblmast_name_show = (Label)FvdetailProblem.FindControl("lblmast_name_show");
        var lblbatchnumber = (Label)FvdetailProblem.FindControl("lblbatchnumber");
        var lblm0_supidx_show = (Label)FvdetailProblem.FindControl("lblm0_supidx_show");
        var lbldategetproduct = (Label)FvdetailProblem.FindControl("lbldategetproduct");
        var lbldatemodify = (Label)FvdetailProblem.FindControl("lbldatemodify");
        var lbldateexp = (Label)FvdetailProblem.FindControl("lbldateexp");
        var lbldateproblem = (Label)FvdetailProblem.FindControl("lbldateproblem");
        var lblqty_get = (Label)FvdetailProblem.FindControl("lblqty_get");
        var lblunit_show = (Label)FvdetailProblem.FindControl("lblunit_show");
        var lblqty_problem = (Label)FvdetailProblem.FindControl("lblqty_problem");
        var lblunit_problem_show = (Label)FvdetailProblem.FindControl("lblunit_problem_show");
        var lblqty_random = (Label)FvdetailProblem.FindControl("lblqty_random");
        var lblunit_random_show = (Label)FvdetailProblem.FindControl("lblunit_random_show");
        var lblm0_pridx_show = (Label)FvdetailProblem.FindControl("lblm0_pridx_show");
        var lbldetail_remark = (Label)FvdetailProblem.FindControl("lbldetail_remark");
        var lbllocidx_show = (Label)FvdetailProblem.FindControl("lbllocidx_show");
        var lblcount_sup = (Label)FvdetailProblem.FindControl("lblcount_sup");

        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _insert = new U0_ProblemDocument();

        _insert.LocIDX_ref = int.Parse(lbllocidx_show.Text);
        _insert.m0_plidx = int.Parse(lblm0_plidx_show.Text);
        _insert.m0_group_tpidx = int.Parse(lblm0_m0_group_tpidx_show.Text);
        _insert.m0_tpidx = int.Parse(lblm0_tpidx_show.Text);
        _insert.m1_tpidx = int.Parse(lblm1_tpidx_show.Text);
        _insert.m2_tpidx = int.Parse(lblm2_tpidx_show.Text);
        _insert.matidx = int.Parse(lblmatidx_show.Text);
        _insert.matname = lblmast_name_show.Text;
        _insert.batchnumber = lblbatchnumber.Text;
        _insert.m0_supidx = int.Parse(lblm0_supidx_show.Text);
        _insert.dategetproduct = lbldategetproduct.Text;
        _insert.datemodify = lbldatemodify.Text;
        _insert.dateexp = lbldateexp.Text;
        _insert.dateproblem = lbldateproblem.Text;
        _insert.qty_get = lblqty_get.Text;
        _insert.unit_get = int.Parse(lblunit_show.Text);
        _insert.qty_problem = lblqty_problem.Text;
        _insert.unit_problem = int.Parse(lblunit_problem_show.Text);
        _insert.qty_random = lblqty_random.Text;
        _insert.unit_random = int.Parse(lblunit_random_show.Text);
        _insert.m0_pridx = int.Parse(lblm0_pridx_show.Text);
        _insert.detail_remark = lbldetail_remark.Text;
        _insert.unidx = 4;
        _insert.acidx = 4;
        _insert.staidx = 2;
        _insert.orgidx = int.Parse(ViewState["Org_idx_create"].ToString());
        _insert.rdeptidx = int.Parse(ViewState["rdept_idx_create"].ToString());
        _insert.rsecidx = int.Parse(ViewState["Sec_idx_create"].ToString());
        _insert.CEmpIDX = int.Parse(ViewState["CEmpIDX_u0"].ToString());
        _insert.condition = 1;
        _insert.u0_docidx = int.Parse(ViewState["u0_docidx"].ToString());
        _insert.count_sup = int.Parse(lblcount_sup.Text);

        _dtproblem.Boxu0_ProblemDocument[0] = _insert;
        // txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlInsertSystem, _dtproblem);

        ViewState["ReturnCode_DocCode"] = _dtproblem.ReturnMsg;


        string getPath = ConfigurationSettings.AppSettings["path_file_qmr_problem"];
        string filePath = Server.MapPath(getPath + ViewState["doc_code"].ToString());
        DirectoryInfo dir = new DirectoryInfo(filePath);

        SearchDirectories(dir, ViewState["doc_code"].ToString());

        string dirfiles = dir.ToString();

        if (checkfile != "11")
        {
            string fileName_upload = ViewState["ReturnCode_DocCode"].ToString();
            string filePath_upload = Server.MapPath(getPath + fileName_upload);
            FileInfo[] files = dir.GetFiles();

            int x = 0;

            if (!Directory.Exists(filePath_upload))
            {
                Directory.CreateDirectory(filePath_upload);
            }

            foreach (FileInfo file_ in files)
            {
                string extension = Path.GetExtension(file_.Name);
                file_.CopyTo(Server.MapPath(getPath + ViewState["ReturnCode_DocCode"].ToString()) + "\\" + fileName_upload + x + extension);

                x++;
            }

        }
    }

    protected void UpdateQAComment_System()
    {
        var ddl_approve_qa = (DropDownList)FvQA_Comment.FindControl("ddl_approve_qa");
        var lblu1_docidx = (Label)FvdetailProblem.FindControl("lblu1_docidx");
        var rdoproblem = (RadioButtonList)FvQA_Comment.FindControl("rdoproblem");
        var rdorollback = (RadioButtonList)FvQA_Comment.FindControl("rdorollback");
        var txtqtyrollback = (TextBox)FvQA_Comment.FindControl("txtqtyrollback");
        var ddlunit_rollback = (DropDownList)FvQA_Comment.FindControl("ddlunit_rollback");
        var txtremark_rollback = (TextBox)FvQA_Comment.FindControl("txtremark_rollback");
        var txtpono = (TextBox)FvQA_Comment.FindControl("txtpono");
        var txttvno = (TextBox)FvQA_Comment.FindControl("txttvno");
        //var chkmat_risk = (CheckBoxList)FvQA_Comment.FindControl("chkmat_risk");
        var txtper_seaweed = (TextBox)FvQA_Comment.FindControl("txtper_seaweed");
        var txtcode = (TextBox)FvQA_Comment.FindControl("txtcode");
        var ddllocation_exchange = (DropDownList)FvQA_Comment.FindControl("ddllocation_exchange");
        var txtremark_exchange = (TextBox)FvQA_Comment.FindControl("txtremark_exchange");
        var Gv_Material_risk = (GridView)FvQA_Comment.FindControl("Gv_Material_risk");

        checkcount = 0;

        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _update = new U0_ProblemDocument();

        _update.unidx = int.Parse(ViewState["m0_node"].ToString());
        _update.acidx = int.Parse(ViewState["m0_actor"].ToString());
        _update.staidx = int.Parse(ddl_approve_qa.SelectedValue);
        _update.u1_docidx = int.Parse(lblu1_docidx.Text);
        _update.u0_docidx = int.Parse(ViewState["u0_docidx"].ToString());
        _update.m0_pbidx = int.Parse(rdoproblem.SelectedValue);
        _update.m0_rbidx = int.Parse(rdorollback.SelectedValue);
        _update.qty_rollback = txtqtyrollback.Text;
        _update.LocIDX_ref = int.Parse(ddllocation_exchange.SelectedValue);
        _update.m0_tpidx = int.Parse(ViewState["m0_tpidx_check_seaweed"].ToString());
        _update.unit_rollback = int.Parse(ddlunit_rollback.SelectedValue);
        _update.detail_rollback = txtremark_rollback.Text;
        _update.po_no = txtpono.Text;
        _update.LocIDX_List = int.Parse(ViewState["LocIDX_List"].ToString());
        _update.tv_no = txttvno.Text;
        _update.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        _update.condition = 2;
        _update.comment = txtremark_exchange.Text;

        if (ViewState["m0_tpidx_check_seaweed"].ToString() == "1")
        {
            _update.code_seaweed = txtcode.Text;
            _update.percent_seaweed = txtper_seaweed.Text;
        }
        else
        {
            _update.code_seaweed = "0";
            _update.percent_seaweed = "";
        }


        _dtproblem.Boxu0_ProblemDocument[0] = _update;


        if (ViewState["m0_tpidx_check_seaweed"].ToString() == "1" || ViewState["LocIDX_List"].ToString() == "14")
        {
            var u3_doc_data = new U3_ProblemDocument[Gv_Material_risk.Rows.Count];
            int i = 0;

            foreach (GridViewRow row_quest in Gv_Material_risk.Rows)
            {
                CheckBox chkmatrisk = (CheckBox)row_quest.Cells[0].FindControl("chkmatrisk");
                Label lbm0_mridx = (Label)row_quest.Cells[1].FindControl("lbm0_mridx");
                TextBox txtcomment = (TextBox)row_quest.Cells[2].FindControl("txtcomment");
                u3_doc_data[i] = new U3_ProblemDocument();

                if (chkmatrisk.Checked)
                {

                    u3_doc_data[i].m0_mridx = int.Parse(lbm0_mridx.Text);

                    if (txtcomment.Text != null && txtcomment.Text != String.Empty && txtcomment.Text != "")
                    {
                        u3_doc_data[i].comment_risk = txtcomment.Text;
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณากรอกข้อมูล Material Risk ให้ครบ!!!');", true);
                        checkcomment = 0;
                        return;
                    }

                    _dtproblem.Boxu3_ProblemDocument = u3_doc_data;
                    checkcount = checkcount + 1;
                    i++;
                }
            }

        }
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));


        if (checkcount != 0 && (ViewState["m0_tpidx_check_seaweed"].ToString() == "1" || ViewState["LocIDX_List"].ToString() == "14") || ViewState["m0_tpidx_check_seaweed"].ToString() != "1")
        {
            
            string getPath = ConfigurationSettings.AppSettings["path_file_qmr_problem"];
            string filePath = Server.MapPath(getPath + ViewState["doc_code"].ToString());
            DirectoryInfo dir = new DirectoryInfo(filePath);
            HttpFileCollection hfc = Request.Files;
            int ocount = 0;

            SearchDirectories(dir, ViewState["doc_code"].ToString());

            if (checkfile != "11")
            {
                ocount = dir.GetFiles().Length;
            }
            int x = ocount;

            if (UploadFileProblem_QA.HasFile)
            {
                if (!Directory.Exists(filePath))
                {
                    Directory.CreateDirectory(filePath);
                }
                string extension = Path.GetExtension(UploadFileProblem_QA.FileName);

                foreach (HttpPostedFile uploadedFile in UploadFileProblem_QA.PostedFiles)
                {
                    if (uploadedFile.FileName != "" && uploadedFile.FileName != String.Empty)
                    {
                        uploadedFile.SaveAs(Server.MapPath(getPath + ViewState["doc_code"].ToString()) + "\\" + "QA_" + ViewState["doc_code"].ToString() + x + extension);
                    }
                    x++;
                }
                //  UploadFileProblem_QA.SaveAs(Server.MapPath(getPath + ViewState["doc_code"].ToString()) + "\\" + "QA_Admin_" + ViewState["doc_code"].ToString() + extension);
            }

            _dtproblem = callServicePostQMR_Problem(_urlInsertSystem, _dtproblem);

        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาเลือกข้อมูล Material Risk!!!');", true);
            return;
        }

    }

    protected void UpdateQACommentEdit_System()
    {
        var ddl_approve_qa_edit = (DropDownList)FvQA_Comment.FindControl("ddl_approve_qa_edit");
        var lblu1_docidx = (Label)FvdetailProblem.FindControl("lblu1_docidx");
        var rdoproblem_edit = (RadioButtonList)FvQA_Comment.FindControl("rdoproblem_edit");
        var rdorollback_edit = (RadioButtonList)FvQA_Comment.FindControl("rdorollback_edit");
        var txtqtyrollback_edit = (TextBox)FvQA_Comment.FindControl("txtqtyrollback_edit");
        var ddlunit_rollback_edit = (DropDownList)FvQA_Comment.FindControl("ddlunit_rollback_edit");
        var txtremark_rollback_edit = (TextBox)FvQA_Comment.FindControl("txtremark_rollback_edit");
        var UploadFileProblem_QA_edit = (FileUpload)FvQA_Comment.FindControl("UploadFileProblem_QA_edit");
        var txtpono_edit = (TextBox)FvQA_Comment.FindControl("txtpono_edit");
        var txttvno_edit = (TextBox)FvQA_Comment.FindControl("txttvno_edit");
        var Gvedit_Material_risk = (GridView)FvQA_Comment.FindControl("Gvedit_Material_risk");
        var txtcode_edit = (TextBox)FvQA_Comment.FindControl("txtcode_edit");
        var txtper_seaweed_edit = (TextBox)FvQA_Comment.FindControl("txtper_seaweed_edit");

        checkcount = 0;

        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _update = new U0_ProblemDocument();

        _update.unidx = int.Parse(ViewState["m0_node"].ToString());
        _update.acidx = int.Parse(ViewState["m0_actor"].ToString());
        _update.staidx = int.Parse(ddl_approve_qa_edit.SelectedValue);
        _update.u1_docidx = int.Parse(lblu1_docidx.Text);
        _update.u0_docidx = int.Parse(ViewState["u0_docidx"].ToString());
        _update.m0_pbidx = int.Parse(rdoproblem_edit.SelectedValue);
        _update.m0_rbidx = int.Parse(rdorollback_edit.SelectedValue);
        _update.qty_rollback = txtqtyrollback_edit.Text;
        _update.unit_rollback = int.Parse(ddlunit_rollback_edit.SelectedValue);
        _update.detail_rollback = txtremark_rollback_edit.Text;
        _update.po_no = txtpono_edit.Text;
        _update.tv_no = txttvno_edit.Text;
        _update.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        _update.condition = 2;
        _update.LocIDX_List = int.Parse(ViewState["LocIDX_List"].ToString());
        _update.m0_tpidx = int.Parse(ViewState["m0_tpidx_check_seaweed"].ToString());

        _dtproblem.Boxu0_ProblemDocument[0] = _update;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        if (ViewState["m0_tpidx_check_seaweed"].ToString() == "1")
        {
            _update.code_seaweed = txtcode_edit.Text;
            _update.percent_seaweed = txtper_seaweed_edit.Text;
        }
        else
        {
            _update.code_seaweed = "0";
            _update.percent_seaweed = "";
        }


        if (ViewState["m0_tpidx_check_seaweed"].ToString() == "1" || ViewState["LocIDX_List"].ToString() == "14")
        {
            var u3_doc_data = new U3_ProblemDocument[Gvedit_Material_risk.Rows.Count];
            int i = 0;

            foreach (GridViewRow row_quest in Gvedit_Material_risk.Rows)
            {
                CheckBox chkmatrisk_edit = (CheckBox)row_quest.Cells[0].FindControl("chkmatrisk_edit");
                Label lbm0_mridx_edit = (Label)row_quest.Cells[1].FindControl("lbm0_mridx_edit");
                TextBox txtmatrisk_edit = (TextBox)row_quest.Cells[2].FindControl("txtmatrisk_edit");
                u3_doc_data[i] = new U3_ProblemDocument();

                if (chkmatrisk_edit.Checked)
                {
                    u3_doc_data[i].m0_mridx = int.Parse(lbm0_mridx_edit.Text);

                    if (txtmatrisk_edit.Text != null && txtmatrisk_edit.Text != String.Empty && txtmatrisk_edit.Text != "")
                    {
                        u3_doc_data[i].comment_risk = txtmatrisk_edit.Text;
                        checkcomment = 1;
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณากรอกข้อมูล Material Risk ให้ครบ!!!');", true);
                        checkcomment = 0;
                        return;
                    }

                    _dtproblem.Boxu3_ProblemDocument = u3_doc_data;
                    checkcount = checkcount + 1;
                    i++;
                }


            }

        }

        //if (checkcount != 0)
        if (checkcount != 0 && (ViewState["m0_tpidx_check_seaweed"].ToString() == "1" || ViewState["LocIDX_List"].ToString() == "14") || ViewState["m0_tpidx_check_seaweed"].ToString() != "1" && ViewState["LocIDX_List"].ToString() != "14")
        {

          
            string getPath = ConfigurationSettings.AppSettings["path_file_qmr_problem"];
            string filePath = Server.MapPath(getPath + ViewState["doc_code"].ToString());
            DirectoryInfo dir = new DirectoryInfo(filePath);
            HttpFileCollection hfc = Request.Files;
            int ocount = 0;

            SearchDirectories(dir, ViewState["doc_code"].ToString());

            if (checkfile != "11")
            {
                ocount = dir.GetFiles().Length;
            }
            int x = ocount;

            if (UploadFileProblem_QA_edit.HasFile)
            {
                if (!Directory.Exists(filePath))
                {
                    Directory.CreateDirectory(filePath);
                }
                string extension = Path.GetExtension(UploadFileProblem_QA_edit.FileName);

                foreach (HttpPostedFile uploadedFile in UploadFileProblem_QA_edit.PostedFiles)
                {
                    if (uploadedFile.FileName != "" && uploadedFile.FileName != String.Empty)
                    {
                        uploadedFile.SaveAs(Server.MapPath(getPath + ViewState["doc_code"].ToString()) + "\\" + "QA_" + ViewState["doc_code"].ToString() + x + extension);
                    }
                    x++;
                }
            }

            _dtproblem = callServicePostQMR_Problem(_urlInsertSystem, _dtproblem);

        }

        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาเลือกข้อมูล Material Risk!!!');", true);
            return;
        }

    }

    #endregion

    #region Select Master
    protected void select_empIdx_present()
    {

        _dtEmployee = callServiceEmpProfile(_urlGetMyProfile + ViewState["EmpIDX"].ToString());

        ViewState["rdept_name"] = _dtEmployee.employee_list[0].dept_name_th;
        ViewState["rdept_idx"] = _dtEmployee.employee_list[0].rdept_idx;
        ViewState["FullName"] = _dtEmployee.employee_list[0].emp_name_th;
        ViewState["Org_name"] = _dtEmployee.employee_list[0].org_name_th;
        ViewState["Org_idx"] = _dtEmployee.employee_list[0].org_idx;
        ViewState["EmpCode"] = _dtEmployee.employee_list[0].emp_code;
        ViewState["Positname"] = _dtEmployee.employee_list[0].pos_name_th;
        ViewState["Pos_idx"] = _dtEmployee.employee_list[0].rpos_idx;
        ViewState["Email"] = _dtEmployee.employee_list[0].emp_email;
        ViewState["Tel"] = _dtEmployee.employee_list[0].emp_mobile_no;
        ViewState["Secname"] = _dtEmployee.employee_list[0].sec_name_th;
        ViewState["Sec_idx"] = _dtEmployee.employee_list[0].rsec_idx;
        ViewState["CostIDX"] = _dtEmployee.employee_list[0].costcenter_idx;
        ViewState["JobGradeIDX"] = _dtEmployee.employee_list[0].jobgrade_idx;
        ViewState["EmpType"] = _dtEmployee.employee_list[0].emp_type_idx;

    }

    protected void selectmaster_product(GridView gvName, string typeproduct, int m0_groupidx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 1;
        _select.m0_group_tpidx = m0_groupidx;
        _select.typeproduct_name = typeproduct;
        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

        _dtproblem = callServicePostQMR_Problem(_urlGetMaster, _dtproblem);
        setGridData(gvName, _dtproblem.Boxm0_ProblemDocument);

    }

    protected void selectmaster_unit(GridView gvName, string unit_name)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 2;
        _select.unit_name = unit_name;
        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

        _dtproblem = callServicePostQMR_Problem(_urlGetMaster, _dtproblem);
        setGridData(gvName, _dtproblem.Boxm0_ProblemDocument);

    }

    protected void selectlocation(DropDownList ddlName)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 3;
        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

        _dtproblem = callServicePostQMR_Problem(_urlGetMaster, _dtproblem);

        setDdlData(ddlName, _dtproblem.Boxm0_ProblemDocument, "LocName", "LocIDX");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกสถานที่...", "0"));

    }

    protected void selectbuilding(DropDownList ddlName, int locidx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 4;
        _select.LocIDX = locidx;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlGetMaster, _dtproblem);

        setDdlData(ddlName, _dtproblem.Boxm0_ProblemDocument, "BuildingName", "BuildingIDX");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกอาคาร...", "0"));

    }

    protected void selectmaster_place(GridView gvName, int LocIDX, int BuilingIDX, string production_name)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 5;
        _select.LocIDX = LocIDX;
        _select.BuildingIDX = BuilingIDX;
        _select.production_name = production_name;
        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

        _dtproblem = callServicePostQMR_Problem(_urlGetMaster, _dtproblem);
        setGridData(gvName, _dtproblem.Boxm0_ProblemDocument);

    }

    protected void selectmaster_problem(GridView gvName, int m0_tpidx, string problem_name, int m0_group)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 7;
        _select.m0_tpidx = m0_tpidx;
        _select.m0_group_tpidx = m0_group;
        _select.problem_name = problem_name;
        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

        _dtproblem = callServicePostQMR_Problem(_urlGetMaster, _dtproblem);
        setGridData(gvName, _dtproblem.Boxm0_ProblemDocument);

    }

    protected void selectmaster_supplier(GridView gvName, string sup_name, string email_sup)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 14;
        _select.sup_name = sup_name;
        _select.email_sup = email_sup;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

        _dtproblem = callServicePostQMR_Problem(_urlGetMaster, _dtproblem);
        setGridData(gvName, _dtproblem.Boxm0_ProblemDocument);

    }

    protected void selectmaster_mat(GridView gvName, string matno, string matname)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 16;
        _select.mat_no_search = matno;
        _select.matname = matname;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlGetMaster, _dtproblem);
        setGridData(gvName, _dtproblem.Boxm0_ProblemDocument);

    }

    protected void selectmaster_materialrisk(GridView gvName, string material_risk)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 17;
        _select.material_risk = material_risk;
        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlGetMaster, _dtproblem);

        setGridData(gvName, _dtproblem.Boxm0_ProblemDocument);
    }

    protected void selectmaster_group(GridView gvName, string group_name)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 22;
        _select.group_name = group_name;
        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlGetMaster, _dtproblem);

        setGridData(gvName, _dtproblem.Boxm0_ProblemDocument);
    }

    protected void selectmaster_product_m1(GridView gvName, string typeproduct_name_m1, int m0_tpidx, int m0_group_tpidx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 23;
        _select.typeproduct_name_m1 = typeproduct_name_m1;
        _select.m0_group_tpidx = m0_group_tpidx;
        _select.m0_tpidx = m0_tpidx;
        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlGetMaster, _dtproblem);

        setGridData(gvName, _dtproblem.Boxm0_ProblemDocument);
    }

    protected void selectmaster_product_m2(GridView gvName, string typeproduct_name_m2, int m1_tpidx, int m0_tpidx, int m0_group_tpidx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 24;
        _select.typeproduct_name_m2 = typeproduct_name_m2;
        _select.m0_group_tpidx = m0_group_tpidx;
        _select.m0_tpidx = m0_tpidx;
        _select.m1_tpidx = m1_tpidx;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlGetMaster, _dtproblem);

        setGridData(gvName, _dtproblem.Boxm0_ProblemDocument);
    }

    protected void select_approve_master(int empidx, int rsecidx, int jobidx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 26;
        _select.CEmpIDX = empidx;
        _select.rsecidx = rsecidx;
        _select.JobGradeIDX = jobidx;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlGetMaster, _dtproblem);

        ViewState["Approve_Master"] = _dtproblem.ReturnCode.ToString();
    }
    #endregion

    #region Select System

    protected void selectgroupproduct(DropDownList ddlName)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 19;
        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

        _dtproblem = callServicePostQMR_Problem(_urlGetMaster, _dtproblem);

        setDdlData(ddlName, _dtproblem.Boxm0_ProblemDocument, "group_name", "m0_group_tpidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกกลุ่มวัตถุดิบ...", "0"));

    }

    protected void selecttypeproduct(DropDownList ddlName, int groupidx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 6;
        _select.m0_group_tpidx = groupidx;
        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

        _dtproblem = callServicePostQMR_Problem(_urlGetMaster, _dtproblem);

        setDdlData(ddlName, _dtproblem.Boxm0_ProblemDocument, "typeproduct_name", "m0_tpidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกประเภทวัตถุดิบ...", "0"));

    }

    protected void selecttypeproduct_m1(DropDownList ddlName, int m0_tpidx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 20;
        _select.m0_tpidx = m0_tpidx;
        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

        _dtproblem = callServicePostQMR_Problem(_urlGetMaster, _dtproblem);

        setDdlData(ddlName, _dtproblem.Boxm0_ProblemDocument, "typeproduct_name_m1", "m1_tpidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกโครงสร้างวัตถุดิบ...", "0"));

    }

    protected void selecttypeproduct_m2(DropDownList ddlName, int m1_tpidx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 21;
        _select.m1_tpidx = m1_tpidx;
        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

        _dtproblem = callServicePostQMR_Problem(_urlGetMaster, _dtproblem);

        setDdlData(ddlName, _dtproblem.Boxm0_ProblemDocument, "typeproduct_name_m2", "m2_tpidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกโครงสร้างย่อยวัตถุดิบ...", "0"));

    }

    protected void selectplace(DropDownList ddlName, int locidx, int building)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 8;
        _select.LocIDX = locidx;
        _select.BuildingIDX = building;
        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

        _dtproblem = callServicePostQMR_Problem(_urlGetMaster, _dtproblem);
        setDdlData(ddlName, _dtproblem.Boxm0_ProblemDocument, "production_name", "m0_plidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกไลน์ผลิต...", "0"));

    }

    protected void selecttypeproblem(DropDownList ddlName, int m0_tpidx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 9;
        _select.m0_tpidx = m0_tpidx;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlGetMaster, _dtproblem);

        setDdlData(ddlName, _dtproblem.Boxm0_ProblemDocument, "problem_name", "m0_pridx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกประเภทปัญหา...", "0"));

    }

    protected void selectunit(DropDownList ddlName)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 10;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlGetMaster, _dtproblem);

        setDdlData(ddlName, _dtproblem.Boxm0_ProblemDocument, "unit_name", "m0_unidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกหน่วย...", "0"));

    }

    protected void selectmat(DropDownList ddlName)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 11;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlGetMaster, _dtproblem);

        setDdlData(ddlName, _dtproblem.Boxm0_ProblemDocument, "mat_no", "matidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกเลข Mat...", "0"));

    }

    protected void selectmatname(TextBox txtname, int matidx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 11;
        _select.matidx = matidx;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlGetMaster, _dtproblem);

        txtname.Text = _dtproblem.Boxm0_ProblemDocument[0].matname.ToString();
        txtname.Enabled = false;
    }

    protected void selectsupplier(DropDownList ddlName)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 12;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlGetMaster, _dtproblem);

        setDdlData(ddlName, _dtproblem.Boxm0_ProblemDocument, "sup_name", "m0_supidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือก Supplier...", "0"));

    }

    protected void selectcount_problemwithsupplier(Label lblname, Label lblyear, int m0_pridx, int m0_supidx, int matidx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 13;
        _select.m0_pridx = m0_pridx;
        _select.m0_supidx = m0_supidx;
        _select.matidx = matidx;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlGetMaster, _dtproblem);

        if (_dtproblem.ReturnMsg.ToString() != "" && _dtproblem.ReturnMsg.ToString() != null) // All 
        {
            lblyear.Text = _dtproblem.ReturnMsg.ToString();
        }
        else
        {
            lblyear.Text = "0";
        }
        if (_dtproblem.ReturnCode.ToString() != "" && _dtproblem.ReturnCode.ToString() != null) // Only Year
        {
            lblname.Text = _dtproblem.ReturnCode.ToString();
        }
        else
        {
            lblname.Text = "0";
        }
    }

    protected void selectindexlist(GridView gvName, int rsecidx, int empidx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 1;
        _select.rsecidx = rsecidx;
        _select.CEmpIDX = empidx;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlGetSystem, _dtproblem);
        setGridData(gvName, _dtproblem.Boxu0_ProblemDocument);

    }

    protected void selectindexlist_searchuser(GridView gvName, int rsecidx, int empidx, int ifsearch, string datestart, string dateend, int m0_tpidx, int m0_pridx, int m0_supidx, string rtcode, int m0_group_tpidx, int m1_tpidx, int m2_tpidx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        if (rtcode == "0")
        {
            _select.condition = 18;
        }
        else
        {
            _select.condition = 17;
        }

        _select.rsecidx = rsecidx;
        _select.CEmpIDX = empidx;
        _select.ifsearch = ifsearch;
        _select.datecreate = datestart;
        _select.dateend = dateend;
        _select.m0_tpidx = m0_tpidx;
        _select.m0_pridx = m0_pridx;
        _select.m0_supidx = m0_supidx;
        _select.m0_group_tpidx = m0_group_tpidx;
        _select.m1_tpidx = m1_tpidx;
        _select.m2_tpidx = m2_tpidx;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlGetSystem, _dtproblem);
        setGridData(gvName, _dtproblem.Boxu0_ProblemDocument);

    }

    protected void select_admin(int rsecidx, int empidx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 2;
        _select.rsecidx = rsecidx;
        _select.CEmpIDX = empidx;
        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlGetSystem, _dtproblem);
        ViewState["return_admin"] = _dtproblem.ReturnCode.ToString();
        if (ViewState["return_admin"].ToString() == "0")
        {
            ViewState["check_onlyrepot"] = _dtproblem.Boxu0_ProblemDocument[0].adminidx.ToString();
        }
    }

    protected void select_empIdx_create()
    {
        _dtEmployee = callServiceEmpProfile(_urlGetMyProfile + ViewState["CEmpIDX_u0"].ToString());

        ViewState["rdept_name_create"] = _dtEmployee.employee_list[0].dept_name_th;
        ViewState["rdept_idx_create"] = _dtEmployee.employee_list[0].rdept_idx;
        ViewState["FullName_create"] = _dtEmployee.employee_list[0].emp_name_th;
        ViewState["Org_name_create"] = _dtEmployee.employee_list[0].org_name_th;
        ViewState["Org_idx_create"] = _dtEmployee.employee_list[0].org_idx;
        ViewState["EmpCode_create"] = _dtEmployee.employee_list[0].emp_code;
        ViewState["Positname_create"] = _dtEmployee.employee_list[0].pos_name_th;
        ViewState["Pos_idx_create"] = _dtEmployee.employee_list[0].rpos_idx;
        ViewState["Email_create"] = _dtEmployee.employee_list[0].emp_email;
        ViewState["Tel_create"] = _dtEmployee.employee_list[0].emp_mobile_no;
        ViewState["Secname_create"] = _dtEmployee.employee_list[0].sec_name_th;
        ViewState["Sec_idx_create"] = _dtEmployee.employee_list[0].rsec_idx;
        ViewState["CostIDX_create"] = _dtEmployee.employee_list[0].costcenter_idx;
        ViewState["JobGradeIDX_create"] = _dtEmployee.employee_list[0].jobgrade_idx;
    }

    protected void select_detail_list(FormView fvName, int u0_docidx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 3;
        _select.u0_docidx = u0_docidx;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlGetSystem, _dtproblem);
        setFormViewData(fvName, _dtproblem.Boxu0_ProblemDocument);

    }

    protected void select_approve_list(GridView gvName, int jobgradeidx, int rsecidx, int cempidx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 4;
        _select.JobGradeIDX = jobgradeidx;
        _select.rsecidx = rsecidx;
        _select.CEmpIDX = cempidx;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlGetSystem, _dtproblem);
        setGridData(gvName, _dtproblem.Boxu0_ProblemDocument);

    }

    protected void select_sum_approve(int jobgradeidx, int rsecidx, int cempidx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 4;
        _select.JobGradeIDX = jobgradeidx;
        _select.rsecidx = rsecidx;
        _select.CEmpIDX = cempidx;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlGetSystem, _dtproblem);
        nav_approve.Text = "<span class='badge progress-bar-danger' >" + _dtproblem.ReturnMsg.ToString() + "</span>";

    }

    protected void selectstatus(DropDownList ddlName, int unidx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 5;
        _select.unidx = unidx;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlGetSystem, _dtproblem);

        setDdlData(ddlName, _dtproblem.Boxm0_ProblemDocument, "status_name", "staidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกสถานะอนุมัติ...", "0"));

    }

    protected void selectactor(DropDownList ddlName)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 15;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlGetMaster, _dtproblem);

        setDdlData(ddlName, _dtproblem.Boxm0_ProblemDocument, "actor_name", "acidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกสถานะอนุมัติ...", "0"));

    }

    protected void select_log_list(Repeater rpName, int u0_docidx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 6;
        _select.u0_docidx = u0_docidx;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlGetSystem, _dtproblem);
        setRepeatData(rpName, _dtproblem.Boxm0_ProblemDocument);

    }

    protected void select_problemlist(RadioButtonList rdoName)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 7;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlGetSystem, _dtproblem);
        setRdoData(rdoName, _dtproblem.Boxm0_ProblemDocument, "type_problem_name", "m0_pbidx");

    }

    protected void select_problemlist_report(DropDownList ddlName)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 7;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlGetSystem, _dtproblem);
        setDdlData(ddlName, _dtproblem.Boxm0_ProblemDocument, "type_problem_name", "m0_pbidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกระดับปัญหา...", "0"));

    }

    protected void select_rollback(RadioButtonList rdoName)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 8;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlGetSystem, _dtproblem);
        setRdoData(rdoName, _dtproblem.Boxm0_ProblemDocument, "rollback_name", "m0_rbidx");

    }

    protected void select_adminqa_comment(FormView fvName, int u1_docidx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 9;
        _select.u1_docidx = u1_docidx;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlGetSystem, _dtproblem);
        setFormViewData(fvName, _dtproblem.Boxu0_ProblemDocument);

    }

    protected void getDepartmentList(DropDownList ddlName, int _org_idx)
    {
        _dtEmployee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dtEmployee.department_list[0] = _deptList;

        _dtEmployee = callServicePostEmp(_urlGetDepartmentList, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกฝ่าย...", "0"));
    }

    protected void getSectionList(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {
        _dtEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _dtEmployee.section_list[0] = _secList;

        _dtEmployee = callServicePostEmp(_urlGetSectionList, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.section_list, "sec_name_th", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกแผนก...", "0"));
    }

    protected void selectreportlist(GridView gvName)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 12;
        _select.rsecidx = int.Parse(ddlrsecidx_search.SelectedValue);
        _select.rdeptidx = int.Parse(ddlrdeptidx_search.SelectedValue);
        _select.doc_code = txtDocCode.Text;
        _select.empcode = txtEmpCOde_search.Text;
        _select.LocIDX = int.Parse(ddlLocate_search.SelectedValue);
        _select.acidx = int.Parse(ddlactor_search.SelectedValue);
        _select.m0_supidx = int.Parse(ddlsupplier_search.SelectedValue);
        _select.m0_tpidx = int.Parse(ddltypeproduct_search.SelectedValue);
        _select.m0_pridx = int.Parse(ddlproblem_search.SelectedValue);
        _select.m0_pbidx = int.Parse(ddltypeproblem_search.SelectedValue);
        _select.ifsearch = int.Parse(ddlSearchDate.SelectedValue);
        _select.datecreate = AddStartdate.Text;
        _select.dateend = AddEndDate.Text;
        _select.BuildingIDX = int.Parse(ddlBuilding_search.SelectedValue);
        _select.m0_plidx = int.Parse(ddlProduction_name_search.SelectedValue);
        _select.staidx = int.Parse(ddlstatus_search.SelectedValue);
        _select.matidx = int.Parse(ddlmat_search.SelectedValue);

        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlGetSystem, _dtproblem);
        setGridData(gvName, _dtproblem.Boxu0_ProblemDocument);

    }

    protected void selectmaterial_show(GridView gvName, int u2_docidx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 13;
        _select.u2_docidx = u2_docidx;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlGetSystem, _dtproblem);

        ViewState["check_gvmaster_risk"] = _dtproblem.ReturnCode.ToString();
        setGridData(gvName, _dtproblem.Boxu3_ProblemDocument);
    }

    protected void select_materialrisk(GridView gvName)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 18;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlGetMaster, _dtproblem);

        setGridData(gvName, _dtproblem.Boxm0_ProblemDocument);
    }

    protected void selectmaterial_comment_edit(TextBox txtName, int u2_docidx, int m0_mridx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 14;
        _select.u2_docidx = u2_docidx;
        _select.m0_mridx = m0_mridx;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlGetSystem, _dtproblem);
        if (_dtproblem.ReturnCode.ToString() == "0")
        {
            txtName.Text = _dtproblem.Boxu3_ProblemDocument[0].comment_risk.ToString();
        }
    }

    protected void select_supplier_comment(FormView fvName, int u0_docidx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 15;
        _select.u0_docidx = u0_docidx;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlGetSystem, _dtproblem);
        setFormViewData(fvName, _dtproblem.Boxu0_ProblemDocument);

    }


    #endregion

    #region CallService

    protected data_employee callServiceEmpProfile(string _cmdUrl)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }

    protected data_employee callServicePostEmp(string _cmdUrl, data_employee _dtEmployee)
    {
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);


        return _dtEmployee;
    }

    protected data_qmr_problem callServicePostQMR_Problem(string _cmdUrl, data_qmr_problem _dtproblem)
    {
        _localJson = _funcTool.convertObjectToJson(_dtproblem);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        _dtproblem = (data_qmr_problem)_funcTool.convertJsonToObject(typeof(data_qmr_problem), _localJson);


        return _dtproblem;
    }
    #endregion

    #region reuse
    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        ddlName.Items.Clear();
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setRdoData(RadioButtonList rdoName, Object obj, string _data_text, string _data_value)
    {
        rdoName.Items.Clear();
        rdoName.DataSource = obj;
        rdoName.DataTextField = _data_text;
        rdoName.DataValueField = _data_value;
        rdoName.DataBind();
    }

    protected void setChkData(CheckBoxList chkName, Object obj, string _data_text, string _data_value)
    {
        chkName.Items.Clear();
        chkName.DataSource = obj;
        chkName.DataTextField = _data_text;
        chkName.DataValueField = _data_value;
        chkName.DataBind();
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setRepeatData(Repeater rpName, Object obj)
    {
        rpName.DataSource = obj;
        rpName.DataBind();
    }

    protected void setFormViewData(FormView fvName, Object obj)
    {
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger1 = new PostBackTrigger();
        trigger1.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger1);
    }

    protected string getStatus(int status)
    {
        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='fa fa-check-circle'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='fa fa-times-circle'></i></span>";
        }
    }

    protected void GenerateddlYear(DropDownList ddlName)
    {

        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Clear();

        var currentYear = DateTime.Today.Year;
        for (int i = 5; i >= 0; i--)
        {
            ddlName.Items.Add((currentYear - i).ToString());
        }

        ddlName.Items.FindByValue(Convert.ToString(currentYear)).Selected = true;
    }

    #endregion

    #region Directories_File URL


    protected string MapURL(string path)
    {

        string appPath = Server.MapPath("~/").ToLower();
        return string.Format("/{0}", path.ToLower().Replace(appPath, "").Replace(@"\", "/"));
    }

    public void SearchDirectories(DirectoryInfo dir, String target)
    {

        string dirfiles = dir.ToString();

        DataSet ds1 = new DataSet();
        DataTable dt1 = new DataTable();
        DataRow dr1 = dt1.NewRow();
        DataColumn dc1 = new DataColumn("FileName", typeof(string));
        dc1 = new DataColumn("Download", typeof(string));
        dt1.Columns.Add("FileName");
        dt1.Columns.Add("Download");

        try
        {
            FileInfo[] files = dir.GetFiles();
            int i = 0;
            foreach (FileInfo file in files)
            {
                if (file.Name.ToLower().IndexOf(target.ToLower()) > -1)
                {
                    string[] f = Directory.GetFiles(dirfiles, file.Name, SearchOption.AllDirectories);
                    dt1.Rows.Add(file.Name);
                    dt1.Rows[i][1] = f[0];
                    i++;
                }
            }

            GridView gvFile = (GridView)FvdetailProblem.FindControl("gvFile");
            if (dt1.Rows.Count > 0)
            {

                ds1.Tables.Add(dt1);
                // gvFileLo1.Visible = true;
                gvFile.DataSource = ds1.Tables[0]; //กรณี ที่กำหนดให้ดึง DataTable ใน DataSet 
                gvFile.DataBind();
                ds1.Dispose();
            }
            else
            {

                gvFile.DataSource = null;
                gvFile.DataBind();

            }
            checkfile = "0";
        }
        catch
        {
            checkfile = "11";
        }
    }

    #endregion

    #region SetDefault
    protected void SetDefaultpage(int page)
    {

        switch (page)
        {

            case 1:
                _divMenuLiToViewIndex.Attributes.Add("class", "bg-color-active");
                _divMenuLiToViewAdd.Attributes.Remove("class");
                _divMenuLiToViewApprove.Attributes.Remove("class");
                _divMenuLiToViewMaster.Attributes.Remove("class");
                _divMenuLiToViewMaster_GroupProduct.Attributes.Remove("class");
                _divMenuLiToViewMaster_Product.Attributes.Remove("class");
                _divMenuBtnToDivMasterProduct_M1.Attributes.Remove("class");
                _divMenuBtnToDivMasterProduct_M2.Attributes.Remove("class");
                _divMenuLiToViewMaster_Unit.Attributes.Remove("class");
                _divMenuLiToViewMaster_Place.Attributes.Remove("class");
                _divMenuLiToViewMaster_Problem.Attributes.Remove("class");
                _divMenuLiToViewMaster_Supplier.Attributes.Remove("class");
                _divMenuLiToViewMaster_Mat.Attributes.Remove("class");
                _divMenuLiToViewMaster_MatRisk.Attributes.Remove("class");
                _divMenuLiToViewReport.Attributes.Remove("class");

                MvMaster.SetActiveView(ViewIndex);
                setOntop.Focus();
                selectgroupproduct(ddlgroupproduct_List);

                selectsupplier(ddlsupplier_List);
                ddlSearchDate_List.SelectedValue = "0";
                AddStartdate_List.Text = String.Empty;
                AddEndDate_List.Text = String.Empty;
                ddltypeproduct_List.SelectedValue = "0";
                ddlproblem_List.SelectedValue = "0";
                ddlsupplier_List.SelectedValue = "0";
                ddlgroupproduct_List.SelectedValue = "0";
                ddltypeproduct_list_m1.SelectedValue = "0";
                ddltypeproduct_list_m2.SelectedValue = "0";

                selectindexlist(GvIndex, int.Parse(ViewState["Sec_idx"].ToString()), int.Parse(ViewState["EmpIDX"].ToString()));
                select_sum_approve(int.Parse(ViewState["JobGradeIDX"].ToString()), int.Parse(ViewState["Sec_idx"].ToString()), int.Parse(ViewState["EmpIDX"].ToString()));
               
                if (ViewState["return_admin"].ToString() == "0" && ViewState["check_onlyrepot"].ToString() != "4")
                {
                    _divMenuLiToViewMaster.Visible = true;
                    _divMenuLiToViewReport.Visible = true;
                }
                else if(ViewState["return_admin"].ToString() == "0" && ViewState["check_onlyrepot"].ToString() == "4")
                {
                    _divMenuLiToViewMaster.Visible = false;
                    _divMenuLiToViewReport.Visible = true;
                }
                else
                {
                    _divMenuLiToViewMaster.Visible = false;
                    _divMenuLiToViewReport.Visible = false;
                }
                break;
            case 2:
                _divMenuLiToViewIndex.Attributes.Remove("class");
                _divMenuLiToViewAdd.Attributes.Add("class", "bg-color-active");
                _divMenuLiToViewApprove.Attributes.Remove("class");
                _divMenuLiToViewMaster.Attributes.Remove("class");
                _divMenuLiToViewMaster_GroupProduct.Attributes.Remove("class");
                _divMenuLiToViewMaster_Product.Attributes.Remove("class");
                _divMenuBtnToDivMasterProduct_M1.Attributes.Remove("class");
                _divMenuBtnToDivMasterProduct_M2.Attributes.Remove("class");
                _divMenuLiToViewMaster_Unit.Attributes.Remove("class");
                _divMenuLiToViewMaster_Place.Attributes.Remove("class");
                _divMenuLiToViewMaster_Problem.Attributes.Remove("class");
                _divMenuLiToViewMaster_Supplier.Attributes.Remove("class");
                _divMenuLiToViewMaster_Mat.Attributes.Remove("class");
                _divMenuLiToViewMaster_MatRisk.Attributes.Remove("class");
                _divMenuLiToViewReport.Attributes.Remove("class");

                MvMaster.SetActiveView(ViewInsert);
                setOntop.Focus();

                FvDetailUser.ChangeMode(FormViewMode.Insert);
                FvDetailUser.DataBind();

                FvinsertProblem.ChangeMode(FormViewMode.Insert);
                FvinsertProblem.DataBind();
                break;

            case 3:
                _divMenuLiToViewIndex.Attributes.Remove("class");
                _divMenuLiToViewAdd.Attributes.Remove("class");
                _divMenuLiToViewApprove.Attributes.Add("class", "bg-color-active");
                _divMenuLiToViewMaster.Attributes.Remove("class");
                _divMenuLiToViewMaster_GroupProduct.Attributes.Remove("class");
                _divMenuLiToViewMaster_Product.Attributes.Remove("class");
                _divMenuBtnToDivMasterProduct_M1.Attributes.Remove("class");
                _divMenuBtnToDivMasterProduct_M2.Attributes.Remove("class");
                _divMenuLiToViewMaster_Unit.Attributes.Remove("class");
                _divMenuLiToViewMaster_Place.Attributes.Remove("class");
                _divMenuLiToViewMaster_Problem.Attributes.Remove("class");
                _divMenuLiToViewMaster_Supplier.Attributes.Remove("class");
                _divMenuLiToViewMaster_Mat.Attributes.Remove("class");
                _divMenuLiToViewMaster_MatRisk.Attributes.Remove("class");
                _divMenuLiToViewReport.Attributes.Remove("class");

                MvMaster.SetActiveView(ViewApprove);
                setOntop.Focus();

                select_approve_list(GvApprove, int.Parse(ViewState["JobGradeIDX"].ToString()), int.Parse(ViewState["Sec_idx"].ToString()), int.Parse(ViewState["EmpIDX"].ToString()));
                select_sum_approve(int.Parse(ViewState["JobGradeIDX"].ToString()), int.Parse(ViewState["Sec_idx"].ToString()), int.Parse(ViewState["EmpIDX"].ToString()));

                break;

            case 4:
                _divMenuLiToViewIndex.Attributes.Remove("class");
                _divMenuLiToViewAdd.Attributes.Remove("class");
                _divMenuLiToViewApprove.Attributes.Remove("class");
                _divMenuLiToViewMaster.Attributes.Add("class", "bg-color-active");
                _divMenuLiToViewMaster_GroupProduct.Attributes.Remove("class");
                _divMenuLiToViewMaster_Product.Attributes.Add("class", "bg-color-active");
                _divMenuBtnToDivMasterProduct_M1.Attributes.Remove("class");
                _divMenuBtnToDivMasterProduct_M2.Attributes.Remove("class");
                _divMenuLiToViewMaster_Unit.Attributes.Remove("class");
                _divMenuLiToViewMaster_Place.Attributes.Remove("class");
                _divMenuLiToViewMaster_Problem.Attributes.Remove("class");
                _divMenuLiToViewMaster_Supplier.Attributes.Remove("class");
                _divMenuLiToViewMaster_Mat.Attributes.Remove("class");
                _divMenuLiToViewMaster_MatRisk.Attributes.Remove("class");
                _divMenuLiToViewReport.Attributes.Remove("class");


                MvMaster.SetActiveView(ViewMaster_Product);
                setOntop.Focus();
                btnshow_product.Visible = true;
                Panel_Add.Visible = false;
                txttypeproduct.Text = String.Empty;
                txtmaster_searchproduct.Text = String.Empty;
                selectgroupproduct(ddlmaster_searchgroup);
                ddlmaster_searchgroup.SelectedValue = "0";
                selectmaster_product(GvMaster_Product, txtmaster_searchproduct.Text, int.Parse(ddlmaster_searchgroup.SelectedValue));
                mergeCell(GvMaster_Product);
                div_searchmaster_typeproduct.Visible = true;

                break;

            case 5:
                _divMenuLiToViewIndex.Attributes.Remove("class");
                _divMenuLiToViewAdd.Attributes.Remove("class");
                _divMenuLiToViewApprove.Attributes.Remove("class");
                _divMenuLiToViewMaster.Attributes.Remove("class");
                _divMenuLiToViewMaster_GroupProduct.Attributes.Remove("class");
                _divMenuLiToViewMaster_Product.Attributes.Remove("class");
                _divMenuBtnToDivMasterProduct_M1.Attributes.Remove("class");
                _divMenuBtnToDivMasterProduct_M2.Attributes.Remove("class");
                _divMenuLiToViewMaster_Unit.Attributes.Remove("class");
                _divMenuLiToViewMaster_Place.Attributes.Remove("class");
                _divMenuLiToViewMaster_Problem.Attributes.Remove("class");
                _divMenuLiToViewMaster_Supplier.Attributes.Remove("class");
                _divMenuLiToViewMaster_Mat.Attributes.Remove("class");
                _divMenuLiToViewMaster_MatRisk.Attributes.Remove("class");
                _divMenuLiToViewReport.Attributes.Add("class", "bg-color-active");

                MvMaster.SetActiveView(ViewReport);
                setOntop.Focus();

                getDepartmentList(ddlrdeptidx_search, 1);
                selectlocation(ddlLocate_search);
                selectgroupproduct(ddlgroupproduct_search);
                selectmat(ddlmat_search);
                select_problemlist_report(ddltypeproblem_search);
                ddlgroupproduct_search.SelectedValue = "0";
                ddlmat_search.SelectedValue = "0";
                //selecttypeproduct(ddltypeproduct_search, 0);
                selectsupplier(ddlsupplier_search);
                ddlSearchDate.SelectedValue = "0";
                AddStartdate.Text = String.Empty;
                AddEndDate.Text = String.Empty;
                AddEndDate.Enabled = false;
                ddlrdeptidx_search.SelectedValue = "0";
                ddlrsecidx_search.SelectedValue = "0";
                ddlLocate_search.SelectedValue = "0";
                ddltypeproduct_search.SelectedValue = "0";
                ddltypeproduct_search_m1.SelectedValue = "0";
                ddltypeproduct_search_m2.SelectedValue = "0";
                ddlproblem_search.SelectedValue = "0";
                ddlsupplier_search.SelectedValue = "0";
                ddltypeproblem_search.SelectedValue = "0";
                txtDocCode.Text = String.Empty;
                txtEmpCOde_search.Text = String.Empty;
                selectactor(ddlactor_search);
                ddlactor_search.SelectedValue = "0";
                GvReport.Visible = false;

                ArrayList deviceStatus = new ArrayList() { "รหัสรายการ", "ข้อมูลสถานที่", "ข้อมูลผู้แจ้ง", "ข้อมูลแจ้งปัญหา", "ข้อมูลSupplier", "รายละเอียดสินค้า", "ประเมินระดับปัญหา", "สถานะรายการ", "รายละเอียด" };

                YrChkBoxColumns.DataSource = deviceStatus;
                YrChkBoxColumns.DataBind();

                foreach (ListItem li in YrChkBoxColumns.Items)
                {
                    li.Selected = true;
                }
                break;

            case 6:
                _divMenuLiToViewIndex.Attributes.Remove("class");
                _divMenuLiToViewAdd.Attributes.Remove("class");
                _divMenuLiToViewApprove.Attributes.Remove("class");
                _divMenuLiToViewMaster.Attributes.Add("class", "bg-color-active");
                _divMenuLiToViewMaster_GroupProduct.Attributes.Remove("class");
                _divMenuLiToViewMaster_Product.Attributes.Remove("class");
                _divMenuBtnToDivMasterProduct_M1.Attributes.Remove("class");
                _divMenuBtnToDivMasterProduct_M2.Attributes.Remove("class");
                _divMenuLiToViewMaster_Unit.Attributes.Add("class", "bg-color-active");
                _divMenuLiToViewMaster_Place.Attributes.Remove("class");
                _divMenuLiToViewMaster_Problem.Attributes.Remove("class");
                _divMenuLiToViewMaster_Supplier.Attributes.Remove("class");
                _divMenuLiToViewMaster_Mat.Attributes.Remove("class");
                _divMenuLiToViewMaster_MatRisk.Attributes.Remove("class");
                _divMenuLiToViewReport.Attributes.Remove("class");

                MvMaster.SetActiveView(ViewMaster_Unit);
                setOntop.Focus();

                btnshow_unit.Visible = true;
                Panel_AddMasterUnit.Visible = false;
                txtunit.Text = String.Empty;
                div_searchmaster_unit.Visible = true;
                txtmaster_searchunit.Text = String.Empty;
                selectmaster_unit(GvMaster_Unit, txtmaster_searchunit.Text);
                break;

            case 7:
                _divMenuLiToViewIndex.Attributes.Remove("class");
                _divMenuLiToViewAdd.Attributes.Remove("class");
                _divMenuLiToViewApprove.Attributes.Remove("class");
                _divMenuLiToViewMaster.Attributes.Add("class", "bg-color-active");
                _divMenuLiToViewMaster_GroupProduct.Attributes.Remove("class");
                _divMenuLiToViewMaster_Product.Attributes.Remove("class");
                _divMenuBtnToDivMasterProduct_M1.Attributes.Remove("class");
                _divMenuBtnToDivMasterProduct_M2.Attributes.Remove("class");
                _divMenuLiToViewMaster_Unit.Attributes.Remove("class");
                _divMenuLiToViewMaster_Place.Attributes.Add("class", "bg-color-active");
                _divMenuLiToViewMaster_Problem.Attributes.Remove("class");
                _divMenuLiToViewMaster_Supplier.Attributes.Remove("class");
                _divMenuLiToViewMaster_Mat.Attributes.Remove("class");
                _divMenuLiToViewMaster_MatRisk.Attributes.Remove("class");
                _divMenuLiToViewReport.Attributes.Remove("class");

                MvMaster.SetActiveView(ViewMaster_Place);
                setOntop.Focus();
                selectlocation(ddllocation);

                btnshow_place.Visible = true;
                Panel_AddMasterPlace.Visible = false;
                div_searchmaster_place.Visible = true;
                selectlocation(ddlmaster_searchlocation);
                ddlmaster_searchlocation.SelectedValue = "0";
                ddlmaster_searchlocation.SelectedItem.Text = "ค้นหาสถานที่...";
                ddlmaster_searchbuilding.SelectedValue = "0";
                txtmaster_searchproduction.Text = String.Empty;
                selectmaster_place(GvMaster_Place, int.Parse(ddlmaster_searchlocation.SelectedValue), int.Parse(ddlmaster_searchbuilding.SelectedValue), txtmaster_searchproduction.Text);
                mergeCell(GvMaster_Place);
                break;

            case 8:
                _divMenuLiToViewIndex.Attributes.Remove("class");
                _divMenuLiToViewAdd.Attributes.Remove("class");
                _divMenuLiToViewApprove.Attributes.Remove("class");
                _divMenuLiToViewMaster.Attributes.Add("class", "bg-color-active");
                _divMenuLiToViewMaster_GroupProduct.Attributes.Remove("class");
                _divMenuLiToViewMaster_Product.Attributes.Remove("class");
                _divMenuBtnToDivMasterProduct_M1.Attributes.Remove("class");
                _divMenuBtnToDivMasterProduct_M2.Attributes.Remove("class");
                _divMenuLiToViewMaster_Unit.Attributes.Remove("class");
                _divMenuLiToViewMaster_Place.Attributes.Remove("class");
                _divMenuLiToViewMaster_Problem.Attributes.Add("class", "bg-color-active");
                _divMenuLiToViewMaster_Supplier.Attributes.Remove("class");
                _divMenuLiToViewMaster_Mat.Attributes.Remove("class");
                _divMenuLiToViewMaster_MatRisk.Attributes.Remove("class");
                _divMenuLiToViewReport.Attributes.Remove("class");

                MvMaster.SetActiveView(ViewMaster_Problem);
                setOntop.Focus();
                btnshow_problem.Visible = true;
                Panel_AddMasterProblem.Visible = false;
                selectgroupproduct(ddltype_group);
                selectgroupproduct(ddlmaster_searchgroupproblem);
                ddlmaster_searchgroupproblem.SelectedValue = "0";
                ddltype_group.SelectedValue = "0";
                ddltype_product.SelectedValue = "0";
                selecttypeproduct(ddlmaster_searchproblem, int.Parse(ddltype_group.SelectedValue));
                selectmaster_problem(GvMaster_Problem, int.Parse(ddlmaster_searchproblem.SelectedValue), txtmaster_searchtopicproblem.Text, int.Parse(ddlmaster_searchgroupproblem.SelectedValue));
                mergeCell(GvMaster_Problem);
                txttypeproblem.Text = String.Empty;
                div_searchmaster_problem.Visible = true;
                ddlmaster_searchproblem.SelectedValue = "0";
                txtmaster_searchtopicproblem.Text = String.Empty;

                Div_Log_Problem.Visible = false;
                div_index_masterproblem.Visible = true;
                break;

            case 9:
                _divMenuLiToViewIndex.Attributes.Add("class", "bg-color-active");
                _divMenuLiToViewMaster_GroupProduct.Attributes.Remove("class");
                _divMenuLiToViewAdd.Attributes.Remove("class");
                _divMenuLiToViewApprove.Attributes.Remove("class");
                _divMenuLiToViewMaster.Attributes.Remove("class");
                _divMenuLiToViewMaster_Product.Attributes.Remove("class");
                _divMenuBtnToDivMasterProduct_M1.Attributes.Remove("class");
                _divMenuBtnToDivMasterProduct_M2.Attributes.Remove("class");
                _divMenuLiToViewMaster_Unit.Attributes.Remove("class");
                _divMenuLiToViewMaster_Place.Attributes.Remove("class");
                _divMenuLiToViewMaster_Problem.Attributes.Remove("class");
                _divMenuLiToViewMaster_Supplier.Attributes.Remove("class");
                _divMenuLiToViewMaster_Mat.Attributes.Remove("class");
                _divMenuLiToViewMaster_MatRisk.Attributes.Remove("class");
                _divMenuLiToViewReport.Attributes.Remove("class");
                MvMaster.SetActiveView(ViewDetail);
                select_empIdx_create();
                Fvdetailusercreate.ChangeMode(FormViewMode.Insert);
                Fvdetailusercreate.DataBind();

                FvdetailProblem.ChangeMode(FormViewMode.ReadOnly);
                select_detail_list(FvdetailProblem, int.Parse(ViewState["u0_docidx"].ToString()));

                CheckBox chkmat_show = (CheckBox)FvdetailProblem.FindControl("chkmat_show");
                Label lblmat_no = (Label)FvdetailProblem.FindControl("lblmat_no");
                Control divmatshow_no = (Control)FvdetailProblem.FindControl("divmatshow_no");
                Label lbLocIDX_List = (Label)FvdetailProblem.FindControl("lbLocIDX_List");


                ViewState["LocIDX_List"] = lbLocIDX_List.Text;

                if (lblmat_no.Text == null || lblmat_no.Text == "0" || lblmat_no.Text == String.Empty)
                {
                    chkmat_show.Checked = false;
                    divmatshow_no.Visible = false;
                }
                else
                {
                    chkmat_show.Checked = true;
                    divmatshow_no.Visible = true;
                }
                chkmat_show.Enabled = false;

                if (ViewState["History_Detail"].ToString() == "3")
                {
                    btnback_report.Visible = true;
                    btnback.Visible = false;
                }
                else
                {
                    btnback_report.Visible = false;
                    btnback.Visible = true;
                }

                select_log_list(rpLog, int.Parse(ViewState["u0_docidx"].ToString()));
                try
                {

                    string getPathLotus = ConfigurationSettings.AppSettings["path_file_qmr_problem"];

                    string filePathLotus = Server.MapPath(getPathLotus + ViewState["doc_code"].ToString());
                    DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);

                    SearchDirectories(myDirLotus, ViewState["doc_code"].ToString());
                }
                catch
                {

                }

                break;

            case 10:
                _divMenuLiToViewIndex.Attributes.Remove("class");
                _divMenuLiToViewAdd.Attributes.Remove("class");
                _divMenuLiToViewApprove.Attributes.Remove("class");
                _divMenuLiToViewMaster.Attributes.Add("class", "bg-color-active");
                _divMenuLiToViewMaster_GroupProduct.Attributes.Remove("class");
                _divMenuLiToViewMaster_Product.Attributes.Remove("class");
                _divMenuBtnToDivMasterProduct_M1.Attributes.Remove("class");
                _divMenuBtnToDivMasterProduct_M2.Attributes.Remove("class");
                _divMenuLiToViewMaster_Unit.Attributes.Remove("class");
                _divMenuLiToViewMaster_Place.Attributes.Remove("class");
                _divMenuLiToViewMaster_Problem.Attributes.Remove("class");
                _divMenuLiToViewMaster_Supplier.Attributes.Add("class", "bg-color-active");
                _divMenuLiToViewMaster_Mat.Attributes.Remove("class");
                _divMenuLiToViewMaster_MatRisk.Attributes.Remove("class");
                _divMenuLiToViewReport.Attributes.Remove("class");

                MvMaster.SetActiveView(ViewMaster_Supplier);
                setOntop.Focus();
                btnshow_supplier.Visible = true;
                linkBtnTrigger(btnshow_supplier);
                ddlchoose_addsup.SelectedValue = "0";
                div_addimportsup.Visible = false;
                div_addmanualsup.Visible = false;
                Panel_AddMasterSupplier.Visible = false;
                txtmaster_searchsupplier.Text = String.Empty;
                txtmaster_searchemail.Text = String.Empty;
                div_searchmaster_supplier.Visible = true;
                selectmaster_supplier(GvMaster_Supplier, txtmaster_searchsupplier.Text, txtmaster_searchemail.Text);

                break;

            case 11:
                _divMenuLiToViewIndex.Attributes.Remove("class");
                _divMenuLiToViewAdd.Attributes.Remove("class");
                _divMenuLiToViewApprove.Attributes.Remove("class");
                _divMenuLiToViewMaster.Attributes.Add("class", "bg-color-active");
                _divMenuLiToViewMaster_GroupProduct.Attributes.Remove("class");
                _divMenuLiToViewMaster_Product.Attributes.Remove("class");
                _divMenuBtnToDivMasterProduct_M1.Attributes.Remove("class");
                _divMenuBtnToDivMasterProduct_M2.Attributes.Remove("class");
                _divMenuLiToViewMaster_Unit.Attributes.Remove("class");
                _divMenuLiToViewMaster_Place.Attributes.Remove("class");
                _divMenuLiToViewMaster_Problem.Attributes.Remove("class");
                _divMenuLiToViewMaster_Supplier.Attributes.Remove("class");
                _divMenuLiToViewMaster_Mat.Attributes.Add("class", "bg-color-active");
                _divMenuLiToViewMaster_MatRisk.Attributes.Remove("class");
                _divMenuLiToViewReport.Attributes.Remove("class");

                MvMaster.SetActiveView(ViewMaster_Mat);
                setOntop.Focus();
                btnshow_mat.Visible = true;
                linkBtnTrigger(btnshow_mat);

                Panel_AddMasterMaterial.Visible = false;
                txtmatno.Text = String.Empty;
                txtmatname.Text = String.Empty;
                selectmaster_mat(GvMaster_Mat, txtmaster_searchmatno.Text, txtmaster_searchmatname.Text);
                div_searchmaster_mat.Visible = true;
                ddltype_addmat.SelectedValue = "0";
                div_addmanual.Visible = false;
                div_addimport.Visible = false;
                ddlstatus_mastermat.SelectedValue = "1";
                break;
            case 12:
                _divMenuLiToViewIndex.Attributes.Remove("class");
                _divMenuLiToViewAdd.Attributes.Remove("class");
                _divMenuLiToViewApprove.Attributes.Remove("class");
                _divMenuLiToViewMaster.Attributes.Add("class", "bg-color-active");
                _divMenuLiToViewMaster_GroupProduct.Attributes.Remove("class");
                _divMenuLiToViewMaster_Product.Attributes.Remove("class");
                _divMenuBtnToDivMasterProduct_M1.Attributes.Remove("class");
                _divMenuBtnToDivMasterProduct_M2.Attributes.Remove("class");
                _divMenuLiToViewMaster_Unit.Attributes.Remove("class");
                _divMenuLiToViewMaster_Place.Attributes.Remove("class");
                _divMenuLiToViewMaster_Problem.Attributes.Remove("class");
                _divMenuLiToViewMaster_Supplier.Attributes.Remove("class");
                _divMenuLiToViewMaster_Mat.Attributes.Remove("class");
                _divMenuLiToViewMaster_MatRisk.Attributes.Add("class", "bg-color-active");
                _divMenuLiToViewReport.Attributes.Remove("class");

                MvMaster.SetActiveView(ViewMaster_Material_Risk);
                setOntop.Focus();
                btnshow_matrisk.Visible = true;
                linkBtnTrigger(btnshow_matrisk);

                Panel_AddMasterMaterialRisk.Visible = false;
                txtmaster_searchmatrisk.Text = String.Empty;
                selectmaster_materialrisk(GvMaster_MatRisk, txtmaster_searchmatrisk.Text);
                div_searchmaster_matrisk.Visible = true;
                txtmat_risk.Text = String.Empty;
                ddlstatus_mastermatrisk.SelectedValue = "1";
                div_index_mastermatrisk.Visible = true;
                Div_Log_Matrisk.Visible = false;

                break;

            case 13:
                _divMenuLiToViewIndex.Attributes.Remove("class");
                _divMenuLiToViewAdd.Attributes.Remove("class");
                _divMenuLiToViewApprove.Attributes.Remove("class");
                _divMenuLiToViewMaster.Attributes.Add("class", "bg-color-active");
                _divMenuLiToViewMaster_GroupProduct.Attributes.Add("class", "bg-color-active");
                _divMenuLiToViewMaster_Product.Attributes.Remove("class");
                _divMenuBtnToDivMasterProduct_M1.Attributes.Remove("class");
                _divMenuBtnToDivMasterProduct_M2.Attributes.Remove("class");
                _divMenuLiToViewMaster_Unit.Attributes.Remove("class");
                _divMenuLiToViewMaster_Place.Attributes.Remove("class");
                _divMenuLiToViewMaster_Problem.Attributes.Remove("class");
                _divMenuLiToViewMaster_Supplier.Attributes.Remove("class");
                _divMenuLiToViewMaster_Mat.Attributes.Remove("class");
                _divMenuLiToViewMaster_MatRisk.Attributes.Remove("class");
                _divMenuLiToViewReport.Attributes.Remove("class");

                MvMaster.SetActiveView(ViewMaster_GroupProduct);
                setOntop.Focus();
                txtmaster_searchgroup.Text = String.Empty;
                btnshow_groupproduct.Visible = true;
                Panel_AddMasterGroup.Visible = false;
                div_searchmaster_groupproduct.Visible = true;
                div_index_mastergroup.Visible = true;
                Div_Log_Group.Visible = false;
                selectmaster_group(GvMaster_Group, txtmaster_searchgroup.Text);

                break;

            case 14:
                _divMenuLiToViewIndex.Attributes.Remove("class");
                _divMenuLiToViewAdd.Attributes.Remove("class");
                _divMenuLiToViewApprove.Attributes.Remove("class");
                _divMenuLiToViewMaster.Attributes.Add("class", "bg-color-active");
                _divMenuLiToViewMaster_GroupProduct.Attributes.Remove("class");
                _divMenuLiToViewMaster_Product.Attributes.Remove("class");
                _divMenuBtnToDivMasterProduct_M1.Attributes.Add("class", "bg-color-active");
                _divMenuBtnToDivMasterProduct_M2.Attributes.Remove("class");
                _divMenuLiToViewMaster_Unit.Attributes.Remove("class");
                _divMenuLiToViewMaster_Place.Attributes.Remove("class");
                _divMenuLiToViewMaster_Problem.Attributes.Remove("class");
                _divMenuLiToViewMaster_Supplier.Attributes.Remove("class");
                _divMenuLiToViewMaster_Mat.Attributes.Remove("class");
                _divMenuLiToViewMaster_MatRisk.Attributes.Remove("class");
                _divMenuLiToViewReport.Attributes.Remove("class");

                MvMaster.SetActiveView(ViewMaster_Product_M1);
                setOntop.Focus();
                btnshow_product_m1.Visible = true;
                Panel_AddMasterProduct_M1.Visible = false;
                div_searchmaster_product_m1.Visible = true;
                selectgroupproduct(ddlsearchgroup_masterm1);
                ddlsearchgroup_masterm1.SelectedValue = "0";
                ddlsearchproduct_masterm1.SelectedValue = "0";
                txtsearchproduct_masterm1.Text = String.Empty;
                selectmaster_product_m1(GvMaster_Product_M1, txtsearchproduct_masterm1.Text, int.Parse(ddlsearchproduct_masterm1.SelectedValue), int.Parse(ddlsearchgroup_masterm1.SelectedValue));
                mergeCell(GvMaster_Product_M1);

                break;

            case 15:
                _divMenuLiToViewIndex.Attributes.Remove("class");
                _divMenuLiToViewAdd.Attributes.Remove("class");
                _divMenuLiToViewApprove.Attributes.Remove("class");
                _divMenuLiToViewMaster.Attributes.Add("class", "bg-color-active");
                _divMenuLiToViewMaster_GroupProduct.Attributes.Remove("class");
                _divMenuLiToViewMaster_Product.Attributes.Remove("class");
                _divMenuBtnToDivMasterProduct_M1.Attributes.Remove("class");
                _divMenuBtnToDivMasterProduct_M2.Attributes.Add("class", "bg-color-active");
                _divMenuLiToViewMaster_Unit.Attributes.Remove("class");
                _divMenuLiToViewMaster_Place.Attributes.Remove("class");
                _divMenuLiToViewMaster_Problem.Attributes.Remove("class");
                _divMenuLiToViewMaster_Supplier.Attributes.Remove("class");
                _divMenuLiToViewMaster_Mat.Attributes.Remove("class");
                _divMenuLiToViewMaster_MatRisk.Attributes.Remove("class");
                _divMenuLiToViewReport.Attributes.Remove("class");

                MvMaster.SetActiveView(ViewMaster_Product_M2);
                setOntop.Focus();
                btnshow_product_m2.Visible = true;
                selectgroupproduct(ddlsearchgroup_masterm2);
                ddlsearchgroup_masterm2.SelectedValue = "0";
                ddlsearchproduct_masterm2.SelectedValue = "0";
                ddlsearchproduct_masterm1_m2.SelectedValue = "0";
                txtsearchproduct_masterm2.Text = String.Empty;
                Panel_AddMasterProduct_M2.Visible = false;
                div_searchmaster_product_m2.Visible = true;

                selectmaster_product_m2(GvMaster_Product_M2, txtsearchproduct_masterm2.Text, int.Parse(ddlsearchproduct_masterm1_m2.SelectedValue), int.Parse(ddlsearchproduct_masterm2.SelectedValue), int.Parse(ddlsearchgroup_masterm2.SelectedValue));
                mergeCell(GvMaster_Product_M2);

                break;
        }
    }
    #endregion

    #region Gridview
    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster_Group":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster_Group.EditIndex != e.Row.RowIndex)
                    {
                        var lblstatus_edit = (Label)e.Row.FindControl("lblstatus_edit");
                        var lblstatus_name = (Label)e.Row.FindControl("lblstatus_name");
                        var History_Group = (LinkButton)e.Row.FindControl("History_Group");
                        var Delete = (LinkButton)e.Row.FindControl("Delete");

                        if (lblstatus_edit.Text == "2")
                        {
                            lblstatus_name.Text = "แจ้งมีการแก้ไขข้อมูล";
                            lblstatus_name.Visible = true;
                            History_Group.Visible = true;
                        }
                        else
                        {
                            lblstatus_name.Visible = false;
                            History_Group.Visible = false;
                        }

                        if (int.Parse(ViewState["JobGradeIDX"].ToString()) > 7)
                        {
                            Delete.Visible = true;
                        }
                        else
                        {
                            Delete.Visible = false;
                        }
                    }
                }
                break;

            case "GvMaster_Product":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster_Product.EditIndex == e.Row.RowIndex)
                    {
                        var txtm0_group_tpidx = (TextBox)e.Row.FindControl("txtm0_group_tpidx");
                        var ddlgroupproduct_insertmasterproduct_edit = (DropDownList)e.Row.FindControl("ddlgroupproduct_insertmasterproduct_edit");


                        selectgroupproduct(ddlgroupproduct_insertmasterproduct_edit);
                        ddlgroupproduct_insertmasterproduct_edit.SelectedValue = txtm0_group_tpidx.Text;

                    }
                }

                break;

            case "GvMaster_Product_M1":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster_Product_M1.EditIndex == e.Row.RowIndex)
                    {
                        var txtm0_group_tpidx = (TextBox)e.Row.FindControl("txtm0_group_tpidx");
                        var ddlgroupproduct_insertmasterproduct_edit = (DropDownList)e.Row.FindControl("ddlgroupproduct_insertmasterproduct_edit");

                        var txtm0_tpidx = (TextBox)e.Row.FindControl("txtm0_tpidx");
                        var ddlproduct_insertmasterproductm1_edit = (DropDownList)e.Row.FindControl("ddlproduct_insertmasterproductm1_edit");


                        selectgroupproduct(ddlgroupproduct_insertmasterproduct_edit);
                        ddlgroupproduct_insertmasterproduct_edit.SelectedValue = txtm0_group_tpidx.Text;

                        selecttypeproduct(ddlproduct_insertmasterproductm1_edit, int.Parse(txtm0_group_tpidx.Text));
                        ddlproduct_insertmasterproductm1_edit.SelectedValue = txtm0_tpidx.Text;

                    }
                }

                break;

            case "GvMaster_Product_M2":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster_Product_M2.EditIndex == e.Row.RowIndex)
                    {
                        var txtm0_group_tpidx = (TextBox)e.Row.FindControl("txtm0_group_tpidx");
                        var ddlgroupproduct_insertmasterproduct_editm2 = (DropDownList)e.Row.FindControl("ddlgroupproduct_insertmasterproduct_editm2");

                        var txtm0_tpidx = (TextBox)e.Row.FindControl("txtm0_tpidx");
                        var ddlproduct_insertmasterproduct_editm2 = (DropDownList)e.Row.FindControl("ddlproduct_insertmasterproduct_editm2");

                        var txtm1_tpidx = (TextBox)e.Row.FindControl("txtm1_tpidx");
                        var ddlproduct_insertmasterproductm1_editm2 = (DropDownList)e.Row.FindControl("ddlproduct_insertmasterproductm1_editm2");


                        selectgroupproduct(ddlgroupproduct_insertmasterproduct_editm2);
                        ddlgroupproduct_insertmasterproduct_editm2.SelectedValue = txtm0_group_tpidx.Text;

                        selecttypeproduct(ddlproduct_insertmasterproduct_editm2, int.Parse(txtm0_group_tpidx.Text));
                        ddlproduct_insertmasterproduct_editm2.SelectedValue = txtm0_tpidx.Text;

                        selecttypeproduct_m1(ddlproduct_insertmasterproductm1_editm2, int.Parse(txtm0_tpidx.Text));
                        ddlproduct_insertmasterproductm1_editm2.SelectedValue = txtm1_tpidx.Text;
                    }
                }

                break;

            case "GvMaster_Unit":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                break;

            case "GvMaster_Place":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster_Place.EditIndex == e.Row.RowIndex)
                    {
                        var txtLocIDX_edit = (TextBox)e.Row.FindControl("txtLocIDX_edit");
                        var ddlLocIDX_edit = (DropDownList)e.Row.FindControl("ddlLocIDX_edit");
                        var txtBuildingIDX_edit = (TextBox)e.Row.FindControl("txtBuildingIDX_edit");
                        var ddlBuildingIDX_edit = (DropDownList)e.Row.FindControl("ddlBuildingIDX_edit");

                        selectlocation(ddlLocIDX_edit);
                        ddlLocIDX_edit.SelectedValue = txtLocIDX_edit.Text;

                        selectbuilding(ddlBuildingIDX_edit, int.Parse(ddlLocIDX_edit.SelectedValue));
                        ddlBuildingIDX_edit.SelectedValue = txtBuildingIDX_edit.Text;
                    }

                }
                break;

            case "GvMaster_Problem":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster_Problem.EditIndex == e.Row.RowIndex)
                    {
                        var txtm0_tpidx_edit = (TextBox)e.Row.FindControl("txtm0_tpidx_edit");
                        var ddlm0_tpidx_edit = (DropDownList)e.Row.FindControl("ddlm0_tpidx_edit");

                        var txtm0_group_tpidx_edit = (TextBox)e.Row.FindControl("txtm0_group_tpidx_edit");
                        var ddlm0_group_tpidx_edit = (DropDownList)e.Row.FindControl("ddlm0_group_tpidx_edit");

                        selectgroupproduct(ddlm0_group_tpidx_edit);
                        ddlm0_group_tpidx_edit.SelectedValue = txtm0_group_tpidx_edit.Text;

                        selecttypeproduct(ddlm0_tpidx_edit, int.Parse(txtm0_group_tpidx_edit.Text));
                        ddlm0_tpidx_edit.SelectedValue = txtm0_tpidx_edit.Text;
                    }
                    else if (GvMaster_Problem.EditIndex != e.Row.RowIndex)
                    {
                        var lblstatus_edit = (Label)e.Row.FindControl("lblstatus_edit");
                        var lblstatus_name = (Label)e.Row.FindControl("lblstatus_name");
                        var History_problem = (LinkButton)e.Row.FindControl("History_problem");
                        var Delete = (LinkButton)e.Row.FindControl("Delete");

                        if (lblstatus_edit.Text == "2")
                        {
                            lblstatus_name.Text = "แจ้งมีการแก้ไขข้อมูล";
                            lblstatus_name.Visible = true;
                            History_problem.Visible = true;
                        }
                        else
                        {
                            lblstatus_name.Visible = false;
                            History_problem.Visible = false;
                        }

                        if (int.Parse(ViewState["JobGradeIDX"].ToString()) > 7)
                        {
                            Delete.Visible = true;
                        }
                        else
                        {
                            Delete.Visible = false;
                        }
                    }
                }
                break;

            case "GvMaster_Supplier":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                break;

            case "GvMaster_Mat":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                break;

            case "GvMaster_MatRisk":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster_MatRisk.EditIndex != e.Row.RowIndex)
                    {
                        var lblstatus_edit = (Label)e.Row.FindControl("lblstatus_edit");
                        var lblstatus_name = (Label)e.Row.FindControl("lblstatus_name");
                        var History_Material_Risk = (LinkButton)e.Row.FindControl("History_Material_Risk");
                        var Delete = (LinkButton)e.Row.FindControl("Delete");

                        if (lblstatus_edit.Text == "2")
                        {
                            lblstatus_name.Text = "แจ้งมีการแก้ไขข้อมูล";
                            lblstatus_name.Visible = true;
                            History_Material_Risk.Visible = true;
                        }
                        else
                        {
                            lblstatus_name.Visible = false;
                            History_Material_Risk.Visible = false;
                        }


                        if (int.Parse(ViewState["JobGradeIDX"].ToString()) > 7)
                        {
                            Delete.Visible = true;
                        }
                        else
                        {
                            Delete.Visible = false;
                        }
                    }
                }
                break;

            case "GvIndex":
            case "GvApprove":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblunidx = (Label)e.Row.FindControl("lblunidx");
                    Label lblstaidx = (Label)e.Row.FindControl("lblstaidx");
                    Label lblStatusDoc = (Label)e.Row.FindControl("lblStatusDoc");
                    LinkButton btnviewdetail = (LinkButton)e.Row.FindControl("btnviewdetail");


                    switch (int.Parse(lblunidx.Text))
                    {
                        case 1:
                            lblStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff8c1a");
                            break;
                        case 2:

                            lblStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#cc66ff");
                            break;
                        case 3:

                            lblStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#996600");
                            break;
                        case 4:
                            lblStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#F08080");

                            break;
                        case 6:
                            lblStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#4F77B9");

                            break;
                        case 7:
                            lblStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#803DA9");

                            break;
                        case 9:
                            if (lblstaidx.Text != "3" && lblstaidx.Text != "6" && lblstaidx.Text != "8")
                            {
                                lblStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC00");
                            }
                            else if (lblstaidx.Text == "6")
                            {
                                lblStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#737373");
                            }
                            else
                            {
                                lblStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff0000");
                            }
                            break;
                    }

                    linkBtnTrigger(btnviewdetail);
                }
                break;

            case "GvReport":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblunidx = (Label)e.Row.FindControl("lblunidx");
                    Label lblstaidx = (Label)e.Row.FindControl("lblstaidx");
                    Label lblStatusDoc = (Label)e.Row.FindControl("lblStatusDoc");
                    LinkButton btnviewdetail = (LinkButton)e.Row.FindControl("btnviewdetail");


                    switch (int.Parse(lblunidx.Text))
                    {
                        case 1:
                            lblStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff8c1a");
                            break;
                        case 2:

                            lblStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#cc66ff");
                            break;
                        case 3:

                            lblStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#996600");
                            break;
                        case 4:
                            lblStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#F08080");

                            break;
                        case 6:
                            lblStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#4F77B9");

                            break;
                        case 7:
                            lblStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#803DA9");

                            break;
                        case 9:
                            if (lblstaidx.Text != "3" && lblstaidx.Text != "6" && lblstaidx.Text != "8")
                            {
                                lblStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC00");
                            }
                            else if (lblstaidx.Text == "6")
                            {
                                lblStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#737373");
                            }
                            else
                            {
                                lblStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff0000");
                            }
                            break;
                    }

                    if (ViewState["Columns_Check_Search"].ToString() != "0")
                    {
                        string To = ViewState["Columns_Check_Search"].ToString();
                        string[] ToId = To.Split(',');
                        int _i = 0;
                        foreach (string ToColumn in ToId)
                        {
                            if (ToColumn != String.Empty)
                            {
                                if (ToColumn != "0")
                                {
                                    GvReport.Columns[_i].Visible = true;
                                }
                                else
                                {
                                    GvReport.Columns[_i].Visible = false;
                                }
                            }
                            _i++;
                        }
                    }
                    linkBtnTrigger(btnviewdetail);
                }
                break;

            case "gvFile":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HyperLink btnDL11 = (HyperLink)e.Row.Cells[0].FindControl("btnDL11");
                    HiddenField hidFile11 = (HiddenField)e.Row.Cells[0].FindControl("hidFile11");
                    // Display the company name in italics.
                    string LinkHost11 = string.Format("http://{0}", Request.Url.Host);

                    btnDL11.NavigateUrl = LinkHost11 + MapURL(hidFile11.Value);
                }

                break;

            case "Gvedit_Material_risk":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    var chkmatrisk_edit = ((CheckBox)e.Row.Cells[0].FindControl("chkmatrisk_edit"));
                    var lbm0_mridx_edit = ((Label)e.Row.Cells[1].FindControl("lbm0_mridx_edit"));
                    var txtmatrisk_edit = ((TextBox)e.Row.Cells[2].FindControl("txtmatrisk_edit"));

                    _dtproblem = new data_qmr_problem();

                    _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
                    U0_ProblemDocument _select = new U0_ProblemDocument();

                    _select.condition = 13;
                    _select.u2_docidx = int.Parse(ViewState["u2_docidx"].ToString());

                    _dtproblem.Boxu0_ProblemDocument[0] = _select;

                    //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));
                    _dtproblem = callServicePostQMR_Problem(_urlGetSystem, _dtproblem);

                    if (_dtproblem.ReturnCode.ToString() == "0")
                    {
                        ViewState["m0_mridx_comma"] = _dtproblem.Boxu3_ProblemDocument[0].m0_mridx_comma.ToString();

                        string[] Tovpn = ViewState["m0_mridx_comma"].ToString().Split(',');
                        foreach (string To_check in Tovpn)
                        {
                            if (int.Parse(To_check) == int.Parse(lbm0_mridx_edit.Text))
                            {
                                chkmatrisk_edit.Checked = true;
                                txtmatrisk_edit.Visible = true;

                                selectmaterial_comment_edit(txtmatrisk_edit, int.Parse(ViewState["u2_docidx"].ToString()), int.Parse(To_check));
                                break;
                            }
                            else
                            {
                                txtmatrisk_edit.Visible = false;
                            }
                        }
                    }
                }
                break;
        }

    }

    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster_Group":
                GvMaster_Group.PageIndex = e.NewPageIndex;
                selectmaster_group(GvMaster_Group, txtmaster_searchgroup.Text);
                break;

            case "GvMaster_Product":

                GvMaster_Product.PageIndex = e.NewPageIndex;
                selectmaster_product(GvMaster_Product, txtmaster_searchproduct.Text, int.Parse(ddlmaster_searchgroup.SelectedValue));
                mergeCell(GvMaster_Product);
                break;
            case "GvMaster_Product_M1":
                GvMaster_Product_M1.PageIndex = e.NewPageIndex;
                selectmaster_product_m1(GvMaster_Product_M1, txtsearchproduct_masterm1.Text, int.Parse(ddlsearchproduct_masterm1.SelectedValue), int.Parse(ddlsearchgroup_masterm1.SelectedValue));
                mergeCell(GvMaster_Product_M1);

                break;

            case "GvMaster_Product_M2":
                GvMaster_Product_M2.PageIndex = e.NewPageIndex;
                selectmaster_product_m2(GvMaster_Product_M2, txtsearchproduct_masterm2.Text, int.Parse(ddlsearchproduct_masterm1_m2.SelectedValue), int.Parse(ddlsearchproduct_masterm2.SelectedValue), int.Parse(ddlsearchgroup_masterm2.SelectedValue));

                mergeCell(GvMaster_Product_M2);

                break;

            case "GvMaster_Unit":

                GvMaster_Unit.PageIndex = e.NewPageIndex;
                selectmaster_unit(GvMaster_Unit, txtmaster_searchunit.Text);

                break;

            case "GvMaster_Place":
                GvMaster_Place.PageIndex = e.NewPageIndex;
                selectmaster_place(GvMaster_Place, int.Parse(ddlmaster_searchlocation.SelectedValue), int.Parse(ddlmaster_searchbuilding.SelectedValue), txtmaster_searchproduction.Text);
                mergeCell(GvMaster_Place);
                break;

            case "GvMaster_Problem":
                GvMaster_Problem.PageIndex = e.NewPageIndex;
                selectmaster_problem(GvMaster_Problem, int.Parse(ddlmaster_searchproblem.SelectedValue), txtmaster_searchtopicproblem.Text, int.Parse(ddlmaster_searchgroupproblem.SelectedValue));
                mergeCell(GvMaster_Problem);
                break;

            case "GvMaster_Supplier":
                GvMaster_Supplier.PageIndex = e.NewPageIndex;
                selectmaster_supplier(GvMaster_Supplier, txtmaster_searchsupplier.Text, txtmaster_searchemail.Text);
                break;

            case "GvMaster_Mat":
                GvMaster_Mat.PageIndex = e.NewPageIndex;
                selectmaster_mat(GvMaster_Mat, txtmaster_searchmatno.Text, txtmaster_searchmatname.Text);
                break;

            case "GvMaster_MatRisk":
                GvMaster_MatRisk.PageIndex = e.NewPageIndex;
                selectmaster_materialrisk(GvMaster_MatRisk, txtmaster_searchmatrisk.Text);
                break;

            case "GvIndex":
                GvIndex.PageIndex = e.NewPageIndex;
                //  selectindexlist(GvIndex, int.Parse(ViewState["Sec_idx"].ToString()), int.Parse(ViewState["EmpIDX"].ToString()));
                if (ViewState["check_search"].ToString() == "0")
                {
                    selectindexlist(GvIndex, int.Parse(ViewState["Sec_idx"].ToString()), int.Parse(ViewState["EmpIDX"].ToString()));
                }
                else
                {
                    selectindexlist_searchuser(GvIndex, int.Parse(ViewState["Sec_idx"].ToString()), int.Parse(ViewState["EmpIDX"].ToString()), int.Parse(ddlSearchDate_List.SelectedValue), AddStartdate_List.Text, AddEndDate_List.Text, int.Parse(ddltypeproduct_List.SelectedValue), int.Parse(ddlproblem_List.SelectedValue), int.Parse(ddlsupplier_List.SelectedValue), ViewState["return_admin"].ToString(), int.Parse(ddlgroupproduct_List.SelectedValue), int.Parse(ddltypeproduct_list_m1.SelectedValue), int.Parse(ddltypeproduct_list_m2.SelectedValue));
                }
                break;

            case "GvApprove":
                GvApprove.PageIndex = e.NewPageIndex;
                select_approve_list(GvApprove, int.Parse(ViewState["JobGradeIDX"].ToString()), int.Parse(ViewState["Sec_idx"].ToString()), int.Parse(ViewState["EmpIDX"].ToString()));

                break;

            case "GvReport":
                GvReport.PageIndex = e.NewPageIndex;
                selectreportlist(GvReport);
                break;
        }
    }

    #endregion

    #region GvRowEditing

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster_Group":
                GvMaster_Group.EditIndex = e.NewEditIndex;
                selectmaster_group(GvMaster_Group, txtmaster_searchgroup.Text);
                btnshow_groupproduct.Visible = false;
                div_searchmaster_groupproduct.Visible = false;

                break;

            case "GvMaster_Product":

                GvMaster_Product.EditIndex = e.NewEditIndex;
                selectmaster_product(GvMaster_Product, txtmaster_searchproduct.Text, int.Parse(ddlmaster_searchgroup.SelectedValue));
                div_searchmaster_typeproduct.Visible = false;
                btnshow_product.Visible = false;
                break;

            case "GvMaster_Product_M1":
                GvMaster_Product_M1.EditIndex = e.NewEditIndex;
                selectmaster_product_m1(GvMaster_Product_M1, txtsearchproduct_masterm1.Text, int.Parse(ddlsearchproduct_masterm1.SelectedValue), int.Parse(ddlsearchgroup_masterm1.SelectedValue));
                div_searchmaster_product_m1.Visible = false;
                btnshow_product_m1.Visible = false;
                break;

            case "GvMaster_Product_M2":
                GvMaster_Product_M2.EditIndex = e.NewEditIndex;
                selectmaster_product_m2(GvMaster_Product_M2, txtsearchproduct_masterm2.Text, int.Parse(ddlsearchproduct_masterm1_m2.SelectedValue), int.Parse(ddlsearchproduct_masterm2.SelectedValue), int.Parse(ddlsearchgroup_masterm2.SelectedValue));

                div_searchmaster_product_m2.Visible = false;
                btnshow_product_m2.Visible = false;
                break;

            case "GvMaster_Unit":
                GvMaster_Unit.EditIndex = e.NewEditIndex;
                selectmaster_unit(GvMaster_Unit, txtmaster_searchunit.Text);
                btnshow_unit.Visible = false;
                div_searchmaster_unit.Visible = false;
                break;

            case "GvMaster_Place":
                GvMaster_Place.EditIndex = e.NewEditIndex;
                selectmaster_place(GvMaster_Place, int.Parse(ddlmaster_searchlocation.SelectedValue), int.Parse(ddlmaster_searchbuilding.SelectedValue), txtmaster_searchproduction.Text);
                div_searchmaster_place.Visible = false;
                btnshow_place.Visible = false;
                break;

            case "GvMaster_Problem":
                GvMaster_Problem.EditIndex = e.NewEditIndex;
                selectmaster_problem(GvMaster_Problem, int.Parse(ddlmaster_searchproblem.SelectedValue), txtmaster_searchtopicproblem.Text, int.Parse(ddlmaster_searchgroupproblem.SelectedValue));
                btnshow_problem.Visible = false;
                div_searchmaster_problem.Visible = false;
                break;

            case "GvMaster_Supplier":
                GvMaster_Supplier.EditIndex = e.NewEditIndex;
                selectmaster_supplier(GvMaster_Supplier, txtmaster_searchsupplier.Text, txtmaster_searchemail.Text);
                div_searchmaster_supplier.Visible = false;
                btnshow_supplier.Visible = false;
                break;

            case "GvMaster_Mat":
                GvMaster_Mat.EditIndex = e.NewEditIndex;
                selectmaster_mat(GvMaster_Mat, txtmaster_searchmatno.Text, txtmaster_searchmatname.Text);
                div_searchmaster_mat.Visible = false;
                btnshow_mat.Visible = false;
                break;

            case "GvMaster_MatRisk":
                GvMaster_MatRisk.EditIndex = e.NewEditIndex;
                selectmaster_materialrisk(GvMaster_MatRisk, txtmaster_searchmatrisk.Text);
                div_searchmaster_matrisk.Visible = false;
                btnshow_matrisk.Visible = false;
                break;
        }
    }

    #endregion

    #region GvRowCancelingEdit

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster_Group":
                GvMaster_Group.EditIndex = -1;
                selectmaster_group(GvMaster_Group, txtmaster_searchgroup.Text);
                btnshow_groupproduct.Visible = true;
                div_searchmaster_groupproduct.Visible = true;
                break;
            case "GvMaster_Product":
                GvMaster_Product.EditIndex = -1;
                selectmaster_product(GvMaster_Product, txtmaster_searchproduct.Text, int.Parse(ddlmaster_searchgroup.SelectedValue));
                btnshow_product.Visible = true;
                div_searchmaster_typeproduct.Visible = true;
                mergeCell(GvMaster_Product);
                break;
            case "GvMaster_Product_M1":
                GvMaster_Product_M1.EditIndex = -1;
                selectmaster_product_m1(GvMaster_Product_M1, txtsearchproduct_masterm1.Text, int.Parse(ddlsearchproduct_masterm1.SelectedValue), int.Parse(ddlsearchgroup_masterm1.SelectedValue));
                mergeCell(GvMaster_Product_M1);
                div_searchmaster_product_m1.Visible = true;
                btnshow_product_m1.Visible = true;
                break;

            case "GvMaster_Product_M2":
                GvMaster_Product_M2.EditIndex = -1;
                selectmaster_product_m2(GvMaster_Product_M2, txtsearchproduct_masterm2.Text, int.Parse(ddlsearchproduct_masterm1_m2.SelectedValue), int.Parse(ddlsearchproduct_masterm2.SelectedValue), int.Parse(ddlsearchgroup_masterm2.SelectedValue));
                mergeCell(GvMaster_Product_M2);
                div_searchmaster_product_m2.Visible = true;
                btnshow_product_m2.Visible = true;
                break;

            case "GvMaster_Unit":
                GvMaster_Unit.EditIndex = -1;
                selectmaster_unit(GvMaster_Unit, txtmaster_searchunit.Text);
                btnshow_unit.Visible = true;
                div_searchmaster_unit.Visible = true;

                break;

            case "GvMaster_Place":
                GvMaster_Place.EditIndex = -1;
                selectmaster_place(GvMaster_Place, int.Parse(ddlmaster_searchlocation.SelectedValue), int.Parse(ddlmaster_searchbuilding.SelectedValue), txtmaster_searchproduction.Text);
                div_searchmaster_place.Visible = true;
                mergeCell(GvMaster_Place);
                btnshow_place.Visible = true;
                break;

            case "GvMaster_Problem":
                GvMaster_Problem.EditIndex = -1;
                selectmaster_problem(GvMaster_Problem, int.Parse(ddlmaster_searchproblem.SelectedValue), txtmaster_searchtopicproblem.Text, int.Parse(ddlmaster_searchgroupproblem.SelectedValue));
                mergeCell(GvMaster_Problem);
                btnshow_problem.Visible = true;
                div_searchmaster_problem.Visible = true;
                break;

            case "GvMaster_Supplier":
                GvMaster_Supplier.EditIndex = -1;
                selectmaster_supplier(GvMaster_Supplier, txtmaster_searchsupplier.Text, txtmaster_searchemail.Text);
                div_searchmaster_supplier.Visible = true;
                btnshow_supplier.Visible = true;
                break;

            case "GvMaster_Mat":
                GvMaster_Mat.EditIndex = -1;
                selectmaster_mat(GvMaster_Mat, txtmaster_searchmatno.Text, txtmaster_searchmatname.Text);
                div_searchmaster_mat.Visible = true;
                btnshow_mat.Visible = true;
                break;

            case "GvMaster_MatRisk":
                GvMaster_MatRisk.EditIndex = -1;
                selectmaster_materialrisk(GvMaster_MatRisk, txtmaster_searchmatrisk.Text);
                div_searchmaster_matrisk.Visible = true;
                btnshow_matrisk.Visible = true;
                break;
        }
    }

    #endregion

    #region GvRowUpdating

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster_Group":
                string m0_group_tpidx = GvMaster_Group.DataKeys[e.RowIndex].Values[0].ToString();
                var txtgroupproduct_edit = (TextBox)GvMaster_Group.Rows[e.RowIndex].FindControl("txtgroupproduct_edit");
                var ddStatusUpdate_mastergroup = (DropDownList)GvMaster_Group.Rows[e.RowIndex].FindControl("ddStatusUpdate_mastergroup");

                GvMaster_Group.EditIndex = -1;
                insertmaster_group(txtgroupproduct_edit.Text, int.Parse(ddStatusUpdate_mastergroup.SelectedValue), int.Parse(ViewState["EmpIDX"].ToString()), m0_group_tpidx);
                selectmaster_group(GvMaster_Group, txtmaster_searchgroup.Text);
                btnshow_groupproduct.Visible = true;
                div_searchmaster_groupproduct.Visible = true;
                break;

            case "GvMaster_Product":

                string m0_tpidx = GvMaster_Product.DataKeys[e.RowIndex].Values[0].ToString();
                var ddlgroupproduct_insertmasterproduct_edit = (DropDownList)GvMaster_Product.Rows[e.RowIndex].FindControl("ddlgroupproduct_insertmasterproduct_edit");
                var txttypeproduct_edit = (TextBox)GvMaster_Product.Rows[e.RowIndex].FindControl("txttypeproduct_edit");
                var StatusUpdate = (DropDownList)GvMaster_Product.Rows[e.RowIndex].FindControl("ddStatusUpdate");

                GvMaster_Product.EditIndex = -1;

                insertmaster_product(txttypeproduct_edit.Text, int.Parse(StatusUpdate.SelectedValue), int.Parse(ViewState["EmpIDX"].ToString()), m0_tpidx, int.Parse(ddlgroupproduct_insertmasterproduct_edit.SelectedValue));
                selectmaster_product(GvMaster_Product, txtmaster_searchproduct.Text, int.Parse(ddlmaster_searchgroup.SelectedValue));
                div_searchmaster_typeproduct.Visible = true;
                btnshow_product.Visible = true;
                mergeCell(GvMaster_Product);

                break;

            case "GvMaster_Product_M1":

                string m1_tpidx = GvMaster_Product_M1.DataKeys[e.RowIndex].Values[0].ToString();
                // var ddlgroupproduct_insertmasterproduct_edit_m1 = (DropDownList)GvMaster_Product.Rows[e.RowIndex].FindControl("ddlgroupproduct_insertmasterproduct_edit");
                var ddlproduct_insertmasterproductm1_edit = (DropDownList)GvMaster_Product_M1.Rows[e.RowIndex].FindControl("ddlproduct_insertmasterproductm1_edit");
                var txttypeproductm1_edit = (TextBox)GvMaster_Product_M1.Rows[e.RowIndex].FindControl("txttypeproductm1_edit");
                var ddStatusUpdate_masterproduct_m1 = (DropDownList)GvMaster_Product_M1.Rows[e.RowIndex].FindControl("ddStatusUpdate_masterproduct_m1");

                GvMaster_Product_M1.EditIndex = -1;
                insertmaster_product_m1(txttypeproductm1_edit.Text, int.Parse(ddStatusUpdate_masterproduct_m1.SelectedValue), int.Parse(ViewState["EmpIDX"].ToString()), m1_tpidx, int.Parse(ddlproduct_insertmasterproductm1_edit.SelectedValue));
                selectmaster_product_m1(GvMaster_Product_M1, txtsearchproduct_masterm1.Text, int.Parse(ddlsearchproduct_masterm1.SelectedValue), int.Parse(ddlsearchgroup_masterm1.SelectedValue));
                div_searchmaster_product_m1.Visible = true;
                btnshow_product_m1.Visible = true;
                mergeCell(GvMaster_Product_M1);
                break;

            case "GvMaster_Product_M2":
                string m2_tpidx = GvMaster_Product_M2.DataKeys[e.RowIndex].Values[0].ToString();
                var ddlproduct_insertmasterproductm1_editm2 = (DropDownList)GvMaster_Product_M2.Rows[e.RowIndex].FindControl("ddlproduct_insertmasterproductm1_editm2");
                var txttypeproductm2_edit = (TextBox)GvMaster_Product_M2.Rows[e.RowIndex].FindControl("txttypeproductm2_edit");
                var ddStatusUpdate_masterproduct_m2 = (DropDownList)GvMaster_Product_M2.Rows[e.RowIndex].FindControl("ddStatusUpdate_masterproduct_m2");

                GvMaster_Product_M2.EditIndex = -1;
                insertmaster_product_m2(txttypeproductm2_edit.Text, int.Parse(ddStatusUpdate_masterproduct_m2.SelectedValue), int.Parse(ViewState["EmpIDX"].ToString()), m2_tpidx, int.Parse(ddlproduct_insertmasterproductm1_editm2.SelectedValue));


                selectmaster_product_m2(GvMaster_Product_M2, txtsearchproduct_masterm2.Text, int.Parse(ddlsearchproduct_masterm1_m2.SelectedValue), int.Parse(ddlsearchproduct_masterm2.SelectedValue), int.Parse(ddlsearchgroup_masterm2.SelectedValue));
                div_searchmaster_product_m2.Visible = true;
                btnshow_product_m2.Visible = true;
                mergeCell(GvMaster_Product_M2);
                break;

            case "GvMaster_Unit":

                string m0_unidx = GvMaster_Unit.DataKeys[e.RowIndex].Values[0].ToString();
                var txtunit_name_edit = (TextBox)GvMaster_Unit.Rows[e.RowIndex].FindControl("txtunit_name_edit");
                var ddStatusUpdate = (DropDownList)GvMaster_Unit.Rows[e.RowIndex].FindControl("ddStatusUpdate");

                GvMaster_Unit.EditIndex = -1;
                insertmaster_unit(txtunit_name_edit.Text, int.Parse(ddStatusUpdate.SelectedValue), int.Parse(ViewState["EmpIDX"].ToString()), m0_unidx);
                selectmaster_unit(GvMaster_Unit, txtmaster_searchunit.Text);
                div_searchmaster_unit.Visible = true;
                btnshow_unit.Visible = true;
                break;

            case "GvMaster_Place":
                string m0_plidx = GvMaster_Place.DataKeys[e.RowIndex].Values[0].ToString();
                var ddlLocIDX_edit = (DropDownList)GvMaster_Place.Rows[e.RowIndex].FindControl("ddlLocIDX_edit");
                var ddlBuildingIDX_edit = (DropDownList)GvMaster_Place.Rows[e.RowIndex].FindControl("ddlBuildingIDX_edit");

                var txtproduction_name_edit = (TextBox)GvMaster_Place.Rows[e.RowIndex].FindControl("txtproduction_name_edit");
                var ddStatusUpdate_masterplace = (DropDownList)GvMaster_Place.Rows[e.RowIndex].FindControl("ddStatusUpdate_masterplace");

                GvMaster_Place.EditIndex = -1;
                insertmaster_place(int.Parse(ddlLocIDX_edit.SelectedValue), int.Parse(ddlBuildingIDX_edit.SelectedValue), txtproduction_name_edit.Text, int.Parse(ddStatusUpdate_masterplace.SelectedValue), int.Parse(ViewState["EmpIDX"].ToString()), m0_plidx);
                selectmaster_place(GvMaster_Place, int.Parse(ddlmaster_searchlocation.SelectedValue), int.Parse(ddlmaster_searchbuilding.SelectedValue), txtmaster_searchproduction.Text);
                div_searchmaster_place.Visible = true;
                btnshow_place.Visible = true;
                mergeCell(GvMaster_Place);
                break;


            case "GvMaster_Problem":

                string m0_pridx = GvMaster_Problem.DataKeys[e.RowIndex].Values[0].ToString();
                var ddlm0_tpidx_edit = (DropDownList)GvMaster_Problem.Rows[e.RowIndex].FindControl("ddlm0_tpidx_edit");
                var txtproblem_name_edit = (TextBox)GvMaster_Problem.Rows[e.RowIndex].FindControl("txtproblem_name_edit");
                var ddStatusUpdate_masterproblem = (DropDownList)GvMaster_Problem.Rows[e.RowIndex].FindControl("ddStatusUpdate_masterproblem");

                GvMaster_Problem.EditIndex = -1;
                insertmaster_problem(int.Parse(ddlm0_tpidx_edit.SelectedValue), txtproblem_name_edit.Text, int.Parse(ddStatusUpdate_masterproblem.SelectedValue), int.Parse(ViewState["EmpIDX"].ToString()), m0_pridx);
                selectmaster_problem(GvMaster_Problem, int.Parse(ddlmaster_searchproblem.SelectedValue), txtmaster_searchtopicproblem.Text, int.Parse(ddlmaster_searchgroupproblem.SelectedValue));
                mergeCell(GvMaster_Problem);
                div_searchmaster_problem.Visible = true;
                btnshow_problem.Visible = true;
                break;

            case "GvMaster_Supplier":
                string m0_supidx = GvMaster_Supplier.DataKeys[e.RowIndex].Values[0].ToString();
                var txtsupname_edit = (TextBox)GvMaster_Supplier.Rows[e.RowIndex].FindControl("txtsupname_edit");
                var txtemail_edit = (TextBox)GvMaster_Supplier.Rows[e.RowIndex].FindControl("txtemail_edit");
                var ddStatusUpdate_mastersupplier = (DropDownList)GvMaster_Supplier.Rows[e.RowIndex].FindControl("ddStatusUpdate_mastersupplier");

                GvMaster_Supplier.EditIndex = -1;
                insertmaster_supplier(int.Parse(m0_supidx), txtsupname_edit.Text, txtemail_edit.Text, int.Parse(ddStatusUpdate_mastersupplier.SelectedValue),
                 int.Parse(ViewState["EmpIDX"].ToString()));
                selectmaster_supplier(GvMaster_Supplier, txtmaster_searchsupplier.Text, txtmaster_searchemail.Text);
                div_searchmaster_supplier.Visible = true;
                btnshow_supplier.Visible = true;
                break;

            case "GvMaster_Mat":
                string matidx = GvMaster_Mat.DataKeys[e.RowIndex].Values[0].ToString();
                var txtmatno_edit = (TextBox)GvMaster_Mat.Rows[e.RowIndex].FindControl("txtmatno_edit");
                var txtmatname_edit = (TextBox)GvMaster_Mat.Rows[e.RowIndex].FindControl("txtmatname_edit");
                var ddStatusUpdate_mastermat = (DropDownList)GvMaster_Mat.Rows[e.RowIndex].FindControl("ddStatusUpdate_mastermat");

                GvMaster_Mat.EditIndex = -1;
                insertmaster_mat(int.Parse(matidx), int.Parse(txtmatno_edit.Text), txtmatname_edit.Text, int.Parse(ddStatusUpdate_mastermat.SelectedValue),
                 int.Parse(ViewState["EmpIDX"].ToString()));
                selectmaster_mat(GvMaster_Mat, txtmaster_searchmatno.Text, txtmaster_searchmatname.Text);
                div_searchmaster_mat.Visible = true;
                btnshow_mat.Visible = true;
                break;

            case "GvMaster_MatRisk":
                string m0_mridx = GvMaster_MatRisk.DataKeys[e.RowIndex].Values[0].ToString();
                var txtmaterial_risk_edit = (TextBox)GvMaster_MatRisk.Rows[e.RowIndex].FindControl("txtmaterial_risk_edit");
                var ddStatusUpdate_matrisk = (DropDownList)GvMaster_MatRisk.Rows[e.RowIndex].FindControl("ddStatusUpdate_matrisk");


                GvMaster_MatRisk.EditIndex = -1;
                insertmaster_material_risk(txtmaterial_risk_edit.Text, int.Parse(ddStatusUpdate_matrisk.SelectedValue), int.Parse(ViewState["EmpIDX"].ToString()), m0_mridx);
                selectmaster_materialrisk(GvMaster_MatRisk, txtmaster_searchmatrisk.Text);
                div_searchmaster_matrisk.Visible = true;
                btnshow_matrisk.Visible = true;

                break;
        }
    }

    #endregion

    #region mergeCell

    protected void mergeCell(GridView GvName)
    {
        switch (GvName.ID)
        {

            case "GvMaster_Product":
                for (int rowIndex = GvName.Rows.Count - 2; rowIndex >= 0; rowIndex--)
                {
                    GridViewRow currentRow = GvName.Rows[rowIndex];
                    GridViewRow previousRow = GvName.Rows[rowIndex + 1];

                    if (((Label)currentRow.Cells[0].FindControl("lblm0_group_tpidx")).Text == ((Label)previousRow.Cells[0].FindControl("lblm0_group_tpidx")).Text)
                    {
                        if (previousRow.Cells[0].RowSpan < 2)
                        {
                            currentRow.Cells[0].RowSpan = 2;

                        }
                        else
                        {
                            currentRow.Cells[0].RowSpan = previousRow.Cells[0].RowSpan + 1;

                        }
                        previousRow.Cells[0].Visible = false;
                    }
                }

                break;

            case "GvMaster_Product_M1":
                for (int rowIndex = GvName.Rows.Count - 2; rowIndex >= 0; rowIndex--)
                {
                    GridViewRow currentRow = GvName.Rows[rowIndex];
                    GridViewRow previousRow = GvName.Rows[rowIndex + 1];

                    if (((Label)currentRow.Cells[0].FindControl("lblm0_group_tpidx")).Text == ((Label)previousRow.Cells[0].FindControl("lblm0_group_tpidx")).Text)
                    {
                        if (previousRow.Cells[0].RowSpan < 2)
                        {
                            currentRow.Cells[0].RowSpan = 2;

                        }
                        else
                        {
                            currentRow.Cells[0].RowSpan = previousRow.Cells[0].RowSpan + 1;

                        }
                        previousRow.Cells[0].Visible = false;
                    }

                    if (((Label)currentRow.Cells[1].FindControl("lblm0_tpidx")).Text == ((Label)previousRow.Cells[1].FindControl("lblm0_tpidx")).Text)
                    {
                        if (previousRow.Cells[1].RowSpan < 2)
                        {
                            currentRow.Cells[1].RowSpan = 2;

                        }
                        else
                        {
                            currentRow.Cells[1].RowSpan = previousRow.Cells[1].RowSpan + 1;

                        }
                        previousRow.Cells[1].Visible = false;
                    }
                }

                break;

            case "GvMaster_Product_M2":
                for (int rowIndex = GvName.Rows.Count - 2; rowIndex >= 0; rowIndex--)
                {
                    GridViewRow currentRow = GvName.Rows[rowIndex];
                    GridViewRow previousRow = GvName.Rows[rowIndex + 1];

                    if (((Label)currentRow.Cells[0].FindControl("lblm0_group_tpidx")).Text == ((Label)previousRow.Cells[0].FindControl("lblm0_group_tpidx")).Text)
                    {
                        if (previousRow.Cells[0].RowSpan < 2)
                        {
                            currentRow.Cells[0].RowSpan = 2;

                        }
                        else
                        {
                            currentRow.Cells[0].RowSpan = previousRow.Cells[0].RowSpan + 1;

                        }
                        previousRow.Cells[0].Visible = false;
                    }

                    if (((Label)currentRow.Cells[1].FindControl("lblm0_tpidx")).Text == ((Label)previousRow.Cells[1].FindControl("lblm0_tpidx")).Text)
                    {
                        if (previousRow.Cells[1].RowSpan < 2)
                        {
                            currentRow.Cells[1].RowSpan = 2;

                        }
                        else
                        {
                            currentRow.Cells[1].RowSpan = previousRow.Cells[1].RowSpan + 1;

                        }
                        previousRow.Cells[1].Visible = false;
                    }

                    if (((Label)currentRow.Cells[2].FindControl("lblm1_tpidx")).Text == ((Label)previousRow.Cells[2].FindControl("lblm1_tpidx")).Text)
                    {
                        if (previousRow.Cells[2].RowSpan < 2)
                        {
                            currentRow.Cells[2].RowSpan = 2;

                        }
                        else
                        {
                            currentRow.Cells[2].RowSpan = previousRow.Cells[2].RowSpan + 1;

                        }
                        previousRow.Cells[2].Visible = false;
                    }
                }

                break;
            case "GvMaster_Place":

                for (int rowIndex = GvName.Rows.Count - 2; rowIndex >= 0; rowIndex--)
                {
                    GridViewRow currentRow = GvName.Rows[rowIndex];
                    GridViewRow previousRow = GvName.Rows[rowIndex + 1];

                    if (((Label)currentRow.Cells[0].FindControl("lblLocIDX")).Text == ((Label)previousRow.Cells[0].FindControl("lblLocIDX")).Text)
                    {
                        if (previousRow.Cells[0].RowSpan < 2)
                        {
                            currentRow.Cells[0].RowSpan = 2;

                        }
                        else
                        {
                            currentRow.Cells[0].RowSpan = previousRow.Cells[0].RowSpan + 1;

                        }
                        previousRow.Cells[0].Visible = false;
                    }

                    if (((Label)currentRow.Cells[1].FindControl("lblBuildingIDX")).Text == ((Label)previousRow.Cells[1].FindControl("lblBuildingIDX")).Text)
                    {
                        if (previousRow.Cells[1].RowSpan < 2)
                        {
                            currentRow.Cells[1].RowSpan = 2;

                        }
                        else
                        {
                            currentRow.Cells[1].RowSpan = previousRow.Cells[1].RowSpan + 1;

                        }
                        previousRow.Cells[1].Visible = false;
                    }

                }

                break;

            case "GvMaster_Problem":
                for (int rowIndex = GvName.Rows.Count - 2; rowIndex >= 0; rowIndex--)
                {
                    GridViewRow currentRow = GvName.Rows[rowIndex];
                    GridViewRow previousRow = GvName.Rows[rowIndex + 1];

                    if (((Label)currentRow.Cells[0].FindControl("lblm0_group_tpidx")).Text == ((Label)previousRow.Cells[0].FindControl("lblm0_group_tpidx")).Text)
                    {
                        if (previousRow.Cells[0].RowSpan < 2)
                        {
                            currentRow.Cells[0].RowSpan = 2;

                        }
                        else
                        {
                            currentRow.Cells[0].RowSpan = previousRow.Cells[0].RowSpan + 1;

                        }
                        previousRow.Cells[0].Visible = false;
                    }


                    if (((Label)currentRow.Cells[1].FindControl("lblm0_tpidx")).Text == ((Label)previousRow.Cells[1].FindControl("lblm0_tpidx")).Text)
                    {
                        if (previousRow.Cells[1].RowSpan < 2)
                        {
                            currentRow.Cells[1].RowSpan = 2;

                        }
                        else
                        {
                            currentRow.Cells[1].RowSpan = previousRow.Cells[1].RowSpan + 1;

                        }
                        previousRow.Cells[1].Visible = false;
                    }
                }
                break;

        }
    }

    #endregion

    #endregion

    #region FormView

    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        var FvName = (FormView)sender;

        switch (FvName.ID)
        {

            case "FvDetailUser":
                FormView FvDetailUser = (FormView)ViewInsert.FindControl("FvDetailUser");

                if (FvDetailUser.CurrentMode == FormViewMode.Insert)
                {
                    var txtempcode = ((TextBox)FvDetailUser.FindControl("txtempcode"));
                    var txtrequesname = ((TextBox)FvDetailUser.FindControl("txtrequesname"));
                    var txtrequesdept = ((TextBox)FvDetailUser.FindControl("txtrequesdept"));
                    var txtsec = ((TextBox)FvDetailUser.FindControl("txtsec"));
                    var txtpos = ((TextBox)FvDetailUser.FindControl("txtpos"));
                    var txtemail = ((TextBox)FvDetailUser.FindControl("txtemail"));
                    var txttel = ((TextBox)FvDetailUser.FindControl("txttel"));
                    var txtorg = ((TextBox)FvDetailUser.FindControl("txtorg"));

                    txtempcode.Text = ViewState["EmpCode"].ToString();
                    txtrequesname.Text = ViewState["FullName"].ToString();
                    txtorg.Text = ViewState["Org_name"].ToString();
                    txtrequesdept.Text = ViewState["rdept_name"].ToString();
                    txtsec.Text = ViewState["Secname"].ToString();
                    txtpos.Text = ViewState["Positname"].ToString();
                    txttel.Text = ViewState["Tel"].ToString();
                    txtemail.Text = ViewState["Email"].ToString();
                }
                break;

            case "FvinsertProblem":
                FormView FvinsertProblem = (FormView)ViewInsert.FindControl("FvinsertProblem");

                if (FvinsertProblem.CurrentMode == FormViewMode.Insert)
                {
                    var ddllocation_problem = ((DropDownList)FvinsertProblem.FindControl("ddllocation_problem"));
                    var ddlgroupproduct_problem = ((DropDownList)FvinsertProblem.FindControl("ddlgroupproduct_problem"));

                    // var ddltypeproduct_problem = ((DropDownList)FvinsertProblem.FindControl("ddltypeproduct_problem"));
                    var ddlunit_get = ((DropDownList)FvinsertProblem.FindControl("ddlunit_get"));
                    var ddlunit_problem = ((DropDownList)FvinsertProblem.FindControl("ddlunit_problem"));
                    var ddlunit_random = ((DropDownList)FvinsertProblem.FindControl("ddlunit_random"));
                    var ddlmat_no = ((DropDownList)FvinsertProblem.FindControl("ddlmat_no"));
                    var ddl_sup = ((DropDownList)FvinsertProblem.FindControl("ddl_sup"));

                    selectlocation(ddllocation_problem);
                    selectgroupproduct(ddlgroupproduct_problem);
                    // selecttypeproduct(ddltypeproduct_problem);
                    selectunit(ddlunit_get);
                    selectunit(ddlunit_problem);
                    selectunit(ddlunit_random);
                    selectmat(ddlmat_no);
                    selectsupplier(ddl_sup);
                }
                break;

            case "Fvdetailusercreate":
                FormView Fvdetailusercreate = (FormView)ViewDetail.FindControl("Fvdetailusercreate");

                if (Fvdetailusercreate.CurrentMode == FormViewMode.Insert)
                {
                    var txtempcode = ((TextBox)Fvdetailusercreate.FindControl("txtempcode"));
                    var txtrequesname = ((TextBox)Fvdetailusercreate.FindControl("txtrequesname"));
                    var txtrequesdept = ((TextBox)Fvdetailusercreate.FindControl("txtrequesdept"));
                    var txtsec = ((TextBox)Fvdetailusercreate.FindControl("txtsec"));
                    var txtsecidx = ((TextBox)Fvdetailusercreate.FindControl("txtsecidx"));
                    var txtpos = ((TextBox)Fvdetailusercreate.FindControl("txtpos"));
                    var txtemail = ((TextBox)Fvdetailusercreate.FindControl("txtemail"));
                    var txttel = ((TextBox)Fvdetailusercreate.FindControl("txttel"));
                    var txtorg = ((TextBox)Fvdetailusercreate.FindControl("txtorg"));
                    var txtorgidx = ((TextBox)Fvdetailusercreate.FindControl("txtorgidx"));

                    txtempcode.Text = ViewState["EmpCode_create"].ToString();
                    txtrequesname.Text = ViewState["FullName_create"].ToString();
                    txtorg.Text = ViewState["Org_name_create"].ToString();
                    txtrequesdept.Text = ViewState["rdept_name_create"].ToString();
                    txtsec.Text = ViewState["Secname_create"].ToString();
                    txtpos.Text = ViewState["Positname_create"].ToString();
                    txttel.Text = ViewState["Tel_create"].ToString();
                    txtemail.Text = ViewState["Email_create"].ToString();
                    txtorgidx.Text = ViewState["Org_idx_create"].ToString();
                    txtsecidx.Text = ViewState["Sec_idx_create"].ToString();
                }
                break;

            case "FvdetailProblem":
                FormView FvdetailProblem = (FormView)ViewDetail.FindControl("FvdetailProblem");

                if (FvdetailProblem.CurrentMode == FormViewMode.Edit)
                {
                    var lbllocidx_edit = ((Label)FvdetailProblem.FindControl("lbllocidx_edit"));
                    var ddllocation_problemedit = ((DropDownList)FvdetailProblem.FindControl("ddllocation_problemedit"));
                    var lblBuildingIDX_edit = ((Label)FvdetailProblem.FindControl("lblBuildingIDX_edit"));
                    var ddlbuilding_problemedit = ((DropDownList)FvdetailProblem.FindControl("ddlbuilding_problemedit"));
                    var lblm0_plidx_edit = ((Label)FvdetailProblem.FindControl("lblm0_plidx_edit"));
                    var ddlproduction_problemedit = ((DropDownList)FvdetailProblem.FindControl("ddlproduction_problemedit"));

                    var lblm0_m0_group_tpidx_edit = ((Label)FvdetailProblem.FindControl("lblm0_m0_group_tpidx_edit"));
                    var ddlgroupproduct_problemedit = ((DropDownList)FvdetailProblem.FindControl("ddlgroupproduct_problemedit"));

                    var lblm0_tpidx_edit = ((Label)FvdetailProblem.FindControl("lblm0_tpidx_edit"));
                    var ddltypeproduct_problemedit = ((DropDownList)FvdetailProblem.FindControl("ddltypeproduct_problemedit"));

                    var lblm1_tpidx_edit = ((Label)FvdetailProblem.FindControl("lblm1_tpidx_edit"));
                    var ddltypeproduct_problemedit_m1 = ((DropDownList)FvdetailProblem.FindControl("ddltypeproduct_problemedit_m1"));

                    var lblm2_tpidx_edit = ((Label)FvdetailProblem.FindControl("lblm2_tpidx_edit"));
                    var ddltypeproduct_problemedit_m2 = ((DropDownList)FvdetailProblem.FindControl("ddltypeproduct_problemedit_m2"));

                    var lblmatidx_edit = ((Label)FvdetailProblem.FindControl("lblmatidx_edit"));
                    var chkmat_edit = ((CheckBox)FvdetailProblem.FindControl("chkmat_edit"));
                    var divmat_no_edit = ((Control)FvdetailProblem.FindControl("divmat_no_edit"));
                    var txtmat_name_edit = ((TextBox)FvdetailProblem.FindControl("txtmat_name_edit"));
                    var ddlmat_noedit = ((DropDownList)FvdetailProblem.FindControl("ddlmat_noedit"));
                    var lblm0_supidx_edit = ((Label)FvdetailProblem.FindControl("lblm0_supidx_edit"));
                    var ddl_sup_edit = ((DropDownList)FvdetailProblem.FindControl("ddl_sup_edit"));
                    var lblunit_edit = ((Label)FvdetailProblem.FindControl("lblunit_edit"));
                    var ddlunit_getedit = ((DropDownList)FvdetailProblem.FindControl("ddlunit_getedit"));
                    var lblunit_problemedit = ((Label)FvdetailProblem.FindControl("lblunit_problemedit"));
                    var ddlunit_problemedit = ((DropDownList)FvdetailProblem.FindControl("ddlunit_problemedit"));
                    var lblunit_randomedit = ((Label)FvdetailProblem.FindControl("lblunit_randomedit"));
                    var ddlunit_randomedit = ((DropDownList)FvdetailProblem.FindControl("ddlunit_randomedit"));
                    var lblm0_pridxedit = ((Label)FvdetailProblem.FindControl("lblm0_pridxedit"));
                    var ddltype_problemedit = ((DropDownList)FvdetailProblem.FindControl("ddltype_problemedit"));


                    selectlocation(ddllocation_problemedit);
                    ddllocation_problemedit.SelectedValue = lbllocidx_edit.Text;
                    selectbuilding(ddlbuilding_problemedit, int.Parse(lbllocidx_edit.Text));
                    ddlbuilding_problemedit.SelectedValue = lblBuildingIDX_edit.Text;
                    selectplace(ddlproduction_problemedit, int.Parse(lbllocidx_edit.Text), int.Parse(lblBuildingIDX_edit.Text));
                    ddlproduction_problemedit.SelectedValue = lblm0_plidx_edit.Text;

                    selectgroupproduct(ddlgroupproduct_problemedit);
                    ddlgroupproduct_problemedit.SelectedValue = lblm0_m0_group_tpidx_edit.Text;

                    selecttypeproduct(ddltypeproduct_problemedit, int.Parse(lblm0_m0_group_tpidx_edit.Text));
                    ddltypeproduct_problemedit.SelectedValue = lblm0_tpidx_edit.Text;

                    selecttypeproduct_m1(ddltypeproduct_problemedit_m1, int.Parse(lblm0_tpidx_edit.Text));
                    ddltypeproduct_problemedit_m1.SelectedValue = lblm1_tpidx_edit.Text;

                    selecttypeproduct_m2(ddltypeproduct_problemedit_m2, int.Parse(lblm1_tpidx_edit.Text));
                    ddltypeproduct_problemedit_m2.SelectedValue = lblm2_tpidx_edit.Text;


                    selectmat(ddlmat_noedit);

                    if (lblmatidx_edit.Text != "" && lblmatidx_edit.Text != "0" && lblmatidx_edit.Text != String.Empty)
                    {
                        chkmat_edit.Checked = true;
                        divmat_no_edit.Visible = true;
                        ddlmat_noedit.SelectedValue = lblmatidx_edit.Text;

                    }
                    else
                    {
                        chkmat_edit.Checked = false;
                        divmat_no_edit.Visible = false;
                        ddlmat_noedit.SelectedValue = "0";
                    }


                    selectsupplier(ddl_sup_edit);
                    ddl_sup_edit.SelectedValue = lblm0_supidx_edit.Text;
                    selectunit(ddlunit_getedit);
                    ddlunit_getedit.SelectedValue = lblunit_edit.Text;
                    selectunit(ddlunit_problemedit);
                    ddlunit_problemedit.SelectedValue = lblunit_problemedit.Text;
                    selectunit(ddlunit_randomedit);
                    ddlunit_randomedit.SelectedValue = lblunit_randomedit.Text;
                    selecttypeproblem(ddltype_problemedit, int.Parse(lblm0_tpidx_edit.Text));
                    ddltype_problemedit.SelectedValue = lblm0_pridxedit.Text;
                }

                if (FvdetailProblem.CurrentMode == FormViewMode.ReadOnly)
                {
                    var lbllocidx_show = ((Label)FvdetailProblem.FindControl("lbllocidx_show"));
                    var lblm0_tpidx_show = ((Label)FvdetailProblem.FindControl("lblm0_tpidx_show"));
                    var lblLocIDX_original = ((Label)FvdetailProblem.FindControl("lblLocIDX_original"));

                    ViewState["LocRef_Exchange"] = lbllocidx_show.Text;
                    ViewState["Loc_Originale"] = lblLocIDX_original.Text;
                    ViewState["m0_tpidx_check_seaweed"] = lblm0_tpidx_show.Text;
                }
                break;

            case "FvQA_Comment":
                FormView FvQA_Comment = (FormView)ViewDetail.FindControl("FvQA_Comment");

                if (FvQA_Comment.CurrentMode == FormViewMode.Insert)
                {
                    var rdoproblem = ((RadioButtonList)FvQA_Comment.FindControl("rdoproblem"));
                    var ddllocation_exchange = ((DropDownList)FvQA_Comment.FindControl("ddllocation_exchange"));
                    var rdorollback = ((RadioButtonList)FvQA_Comment.FindControl("rdorollback"));
                    var ddlunit_rollback = ((DropDownList)FvQA_Comment.FindControl("ddlunit_rollback"));
                    var ddl_approve_qa = ((DropDownList)FvQA_Comment.FindControl("ddl_approve_qa"));
                    var div_seaweed = ((Control)FvQA_Comment.FindControl("div_seaweed"));
                    var Gv_Material_risk = ((GridView)FvQA_Comment.FindControl("Gv_Material_risk"));
                    var div_gvmaterial_risk = ((Control)FvQA_Comment.FindControl("div_gvmaterial_risk"));

                    int b = 0;

                    select_problemlist(rdoproblem);
                    selectlocation(ddllocation_exchange);
                    ddllocation_exchange.SelectedValue = ViewState["LocRef_Exchange"].ToString();
                    select_rollback(rdorollback);
                    selectunit(ddlunit_rollback);
                    selectstatus(ddl_approve_qa, int.Parse(ViewState["m0_node"].ToString()));

                    if (ViewState["m0_tpidx_check_seaweed"].ToString() == "1")
                    {
                        select_materialrisk(Gv_Material_risk);
                        div_seaweed.Visible = true;
                        ddllocation_exchange.Enabled = false;
                    }
                    else
                    {
                        div_seaweed.Visible = false;
                        ddllocation_exchange.Visible = true;
                    }

                    if (ViewState["LocIDX_List"].ToString() == "14")
                    {
                        select_materialrisk(Gv_Material_risk);
                        div_gvmaterial_risk.Visible = true;
                    }
                    else
                    {
                        div_gvmaterial_risk.Visible = false;
                    }
                }

                if (FvQA_Comment.CurrentMode == FormViewMode.ReadOnly)
                {
                    var lblu2_docidx = ((Label)FvQA_Comment.FindControl("lblu2_docidx"));
                    var divshow_seaweed = ((Control)FvQA_Comment.FindControl("divshow_seaweed"));
                    var Gvshow_Material_risk = ((GridView)FvQA_Comment.FindControl("Gvshow_Material_risk"));
                    //var chkmat_risk_show = ((CheckBoxList)FvQA_Comment.FindControl("chkmat_risk_show"));
                    var divshow_gvmaterial_risk = ((Control)FvQA_Comment.FindControl("divshow_gvmaterial_risk"));

                    if (ViewState["m0_tpidx_check_seaweed"].ToString() == "1")
                    {
                        divshow_seaweed.Visible = true;
                    }
                    else
                    {
                        divshow_seaweed.Visible = false;
                    }

                    selectmaterial_show(Gvshow_Material_risk, int.Parse(lblu2_docidx.Text));

                    if (ViewState["check_gvmaster_risk"].ToString() == "1")
                    {
                        divshow_gvmaterial_risk.Visible = false;
                    }
                    else
                    {
                        divshow_gvmaterial_risk.Visible = true;
                    }
                    //if (ViewState["LocIDX_List"].ToString() == "14")
                    //{
                    //    divshow_gvmaterial_risk.Visible = true;
                    //    selectmaterial_show(Gvshow_Material_risk, int.Parse(lblu2_docidx.Text));
                    //}
                    //else
                    //{
                    //    divshow_gvmaterial_risk.Visible = false;
                    //}

                }

                if (FvQA_Comment.CurrentMode == FormViewMode.Edit)
                {
                    var lblu2_docidx_edit = ((Label)FvQA_Comment.FindControl("lblu2_docidx_edit"));
                    var lblpbidx_edit = ((Label)FvQA_Comment.FindControl("lblpbidx_edit"));
                    var rdoproblem_edit = ((RadioButtonList)FvQA_Comment.FindControl("rdoproblem_edit"));
                    var lblm0_rbidx_edit = ((Label)FvQA_Comment.FindControl("lblm0_rbidx_edit"));
                    var rdorollback_edit = ((RadioButtonList)FvQA_Comment.FindControl("rdorollback_edit"));
                    var lblunit_rollback = ((Label)FvQA_Comment.FindControl("lblunit_rollback"));
                    var ddlunit_rollback_edit = ((DropDownList)FvQA_Comment.FindControl("ddlunit_rollback_edit"));
                    var ddl_approve_qa_edit = ((DropDownList)FvQA_Comment.FindControl("ddl_approve_qa_edit"));
                    var btnapprove_qa_edit = ((LinkButton)FvQA_Comment.FindControl("btnapprove_qa_edit"));
                    var div_seaweed_edit = ((Control)FvQA_Comment.FindControl("div_seaweed_edit"));
                    var Gvedit_Material_risk = ((GridView)FvQA_Comment.FindControl("Gvedit_Material_risk"));
                    var divedit_gvmaterial_risk = ((Control)FvQA_Comment.FindControl("divedit_gvmaterial_risk"));

                    select_problemlist(rdoproblem_edit);
                    rdoproblem_edit.SelectedValue = lblpbidx_edit.Text;
                    select_rollback(rdorollback_edit);
                    rdorollback_edit.SelectedValue = lblm0_rbidx_edit.Text;
                    selectunit(ddlunit_rollback_edit);
                    ddlunit_rollback_edit.SelectedValue = lblunit_rollback.Text;
                    selectstatus(ddl_approve_qa_edit, int.Parse(ViewState["m0_node"].ToString()));

                    linkBtnTrigger(btnapprove_qa_edit);

                    ViewState["u2_docidx"] = lblu2_docidx_edit.Text;

                    if (ViewState["m0_tpidx_check_seaweed"].ToString() == "1")
                    {
                        div_seaweed_edit.Visible = true;
                        select_materialrisk(Gvedit_Material_risk);
                    }
                    else
                    {
                        div_seaweed_edit.Visible = false;
                    }

                    if (ViewState["LocIDX_List"].ToString() == "14")
                    {
                        divedit_gvmaterial_risk.Visible = true;
                        select_materialrisk(Gvedit_Material_risk);
                    }
                    else
                    {
                        divedit_gvmaterial_risk.Visible = false;
                    }

                }
                break;

            case "fvapprove_group":
                if (fvapprove_group.CurrentMode == FormViewMode.Edit)
                {
                    var txtgroup_rsecidx = ((TextBox)fvapprove_group.FindControl("txtgroup_rsecidx"));
                    var txtgroup_job = ((TextBox)fvapprove_group.FindControl("txtgroup_job"));
                    var btnapprove_group = ((LinkButton)fvapprove_group.FindControl("btnapprove_group"));
                    var btnunapprove_group = ((LinkButton)fvapprove_group.FindControl("btnunapprove_group"));

                    select_approve_master(int.Parse(ViewState["EmpIDX"].ToString()), int.Parse(txtgroup_rsecidx.Text), int.Parse(txtgroup_job.Text));

                    if (ViewState["Approve_Master"].ToString() == "1")
                    {
                        btnapprove_group.Visible = false;
                        btnunapprove_group.Visible = false;
                    }
                    else
                    {
                        btnapprove_group.Visible = true;
                        btnunapprove_group.Visible = true;
                    }

                }
                break;

            case "fvapprove_matrisk":
                if (fvapprove_matrisk.CurrentMode == FormViewMode.Edit)
                {
                    var txtmatrisk_rsecidx = ((TextBox)fvapprove_matrisk.FindControl("txtmatrisk_rsecidx"));
                    var txtmatrisk_job = ((TextBox)fvapprove_matrisk.FindControl("txtmatrisk_job"));
                    var btnapprove_matrisk = ((LinkButton)fvapprove_matrisk.FindControl("btnapprove_matrisk"));
                    var btnunapprove_matrisk = ((LinkButton)fvapprove_matrisk.FindControl("btnunapprove_matrisk"));

                    select_approve_master(int.Parse(ViewState["EmpIDX"].ToString()), int.Parse(txtmatrisk_rsecidx.Text), int.Parse(txtmatrisk_job.Text));

                    if (ViewState["Approve_Master"].ToString() == "1")
                    {
                        btnapprove_matrisk.Visible = false;
                        btnunapprove_matrisk.Visible = false;
                    }
                    else
                    {
                        btnapprove_matrisk.Visible = true;
                        btnunapprove_matrisk.Visible = true;
                    }

                }
                break;

            case "fvapprove_problem":
                if (fvapprove_problem.CurrentMode == FormViewMode.Edit)
                {
                    var txtproblem_rsecidx = ((TextBox)fvapprove_problem.FindControl("txtproblem_rsecidx"));
                    var txtproblem_job = ((TextBox)fvapprove_problem.FindControl("txtproblem_job"));
                    var btnapprove_problem = ((LinkButton)fvapprove_problem.FindControl("btnapprove_problem"));
                    var btnunapprove_problem = ((LinkButton)fvapprove_problem.FindControl("btnunapprove_problem"));

                    select_approve_master(int.Parse(ViewState["EmpIDX"].ToString()), int.Parse(txtproblem_rsecidx.Text), int.Parse(txtproblem_job.Text));

                    if (ViewState["Approve_Master"].ToString() == "1")
                    {
                        btnapprove_problem.Visible = false;
                        btnunapprove_problem.Visible = false;
                    }
                    else
                    {
                        btnapprove_problem.Visible = true;
                        btnunapprove_problem.Visible = true;
                    }

                }
                break;
        }
    }
    #endregion

    #region SelectedIndexChanged
    protected void SelectedIndexChanged(object sender, EventArgs e)
    {
        if (sender is DropDownList)
        {
            DropDownList ddlName = (DropDownList)sender;

            DropDownList ddllocation_problem = ((DropDownList)FvinsertProblem.FindControl("ddllocation_problem"));
            DropDownList ddlbuilding_problem = ((DropDownList)FvinsertProblem.FindControl("ddlbuilding_problem"));
            DropDownList ddlproduction_problem = ((DropDownList)FvinsertProblem.FindControl("ddlproduction_problem"));
            DropDownList ddltype_problem = ((DropDownList)FvinsertProblem.FindControl("ddltype_problem"));
            DropDownList ddltypeproduct_problem = ((DropDownList)FvinsertProblem.FindControl("ddltypeproduct_problem"));

            DropDownList ddlgroupproduct_problem = ((DropDownList)FvinsertProblem.FindControl("ddlgroupproduct_problem"));
            DropDownList ddltypeproduct_problem_m1 = ((DropDownList)FvinsertProblem.FindControl("ddltypeproduct_problem_m1"));
            DropDownList ddltypeproduct_problem_m2 = ((DropDownList)FvinsertProblem.FindControl("ddltypeproduct_problem_m2"));

            DropDownList ddlmat_no = ((DropDownList)FvinsertProblem.FindControl("ddlmat_no"));
            DropDownList ddl_sup = ((DropDownList)FvinsertProblem.FindControl("ddl_sup"));
            Label lblcount_problem = ((Label)FvinsertProblem.FindControl("lblcount_problem"));
            Label lblcount_problem_all = ((Label)FvinsertProblem.FindControl("lblcount_problem_all"));
            TextBox txtmat_name = ((TextBox)FvinsertProblem.FindControl("txtmat_name"));
            DropDownList ddlunit_get = ((DropDownList)FvinsertProblem.FindControl("ddlunit_get"));
            DropDownList ddlunit_problem = ((DropDownList)FvinsertProblem.FindControl("ddlunit_problem"));
            DropDownList ddlunit_random = ((DropDownList)FvinsertProblem.FindControl("ddlunit_random"));


            DropDownList ddllocation_problemedit = ((DropDownList)FvdetailProblem.FindControl("ddllocation_problemedit"));
            DropDownList ddlbuilding_problemedit = ((DropDownList)FvdetailProblem.FindControl("ddlbuilding_problemedit"));
            DropDownList ddlproduction_problemedit = ((DropDownList)FvdetailProblem.FindControl("ddlproduction_problemedit"));
            DropDownList ddlmat_noedit = ((DropDownList)FvdetailProblem.FindControl("ddlmat_noedit"));
            TextBox txtmat_name_edit = ((TextBox)FvdetailProblem.FindControl("txtmat_name_edit"));
            DropDownList ddlunit_getedit = ((DropDownList)FvdetailProblem.FindControl("ddlunit_getedit"));
            DropDownList ddlunit_problemedit = ((DropDownList)FvdetailProblem.FindControl("ddlunit_problemedit"));
            DropDownList ddlunit_randomedit = ((DropDownList)FvdetailProblem.FindControl("ddlunit_randomedit"));
            DropDownList ddltype_problemedit = ((DropDownList)FvdetailProblem.FindControl("ddltype_problemedit"));

            DropDownList ddlgroupproduct_problemedit = ((DropDownList)FvdetailProblem.FindControl("ddlgroupproduct_problemedit"));
            DropDownList ddltypeproduct_problemedit = ((DropDownList)FvdetailProblem.FindControl("ddltypeproduct_problemedit"));
            DropDownList ddltypeproduct_problemedit_m1 = ((DropDownList)FvdetailProblem.FindControl("ddltypeproduct_problemedit_m1"));
            DropDownList ddltypeproduct_problemedit_m2 = ((DropDownList)FvdetailProblem.FindControl("ddltypeproduct_problemedit_m2"));

            DropDownList ddllocation_exchange = ((DropDownList)FvQA_Comment.FindControl("ddllocation_exchange"));
            Panel panel_accept = ((Panel)FvQA_Comment.FindControl("panel_accept"));
            Panel panel_change = ((Panel)FvQA_Comment.FindControl("panel_change"));
            Control div_approveqa = ((Control)FvQA_Comment.FindControl("div_approveqa"));
            Control div_reason_exchange = ((Control)FvQA_Comment.FindControl("div_reason_exchange"));
            TextBox txtqtyget = ((TextBox)FvinsertProblem.FindControl("txtqtyget"));
            TextBox txtqty_problem = ((TextBox)FvinsertProblem.FindControl("txtqty_problem"));
            TextBox txtqty_random = ((TextBox)FvinsertProblem.FindControl("txtqty_random"));

            Label lblqty_get = ((Label)FvdetailProblem.FindControl("lblqty_get"));
            TextBox txtqtyrollback = ((TextBox)FvQA_Comment.FindControl("txtqtyrollback"));
            Label lblunit_problem_show = ((Label)FvdetailProblem.FindControl("lblunit_problem_show"));

            switch (ddlName.ID)
            {

                case "ddllocation":
                    selectbuilding(ddlbuilding, int.Parse(ddllocation.SelectedValue));

                    break;

                case "ddllocation_problem":
                    selectbuilding(ddlbuilding_problem, int.Parse(ddllocation_problem.SelectedValue));

                    break;


                case "ddlbuilding_problem":
                    selectplace(ddlproduction_problem, int.Parse(ddllocation_problem.SelectedValue), int.Parse(ddlbuilding_problem.SelectedValue));

                    break;

                case "ddlmat_no":
                    selectmatname(txtmat_name, int.Parse(ddlmat_no.SelectedValue));
                    break;

                case "ddlgroupproduct_problem":
                    selecttypeproduct(ddltypeproduct_problem, int.Parse(ddlgroupproduct_problem.SelectedValue));
                    break;

                case "ddltypeproduct_problem":
                    selecttypeproduct_m1(ddltypeproduct_problem_m1, int.Parse(ddltypeproduct_problem.SelectedValue));
                    selecttypeproblem(ddltype_problem, int.Parse(ddltypeproduct_problem.SelectedValue));
                    break;

                case "ddltypeproduct_problem_m1":
                    selecttypeproduct_m2(ddltypeproduct_problem_m2, int.Parse(ddltypeproduct_problem_m1.SelectedValue));
                    break;

                case "ddltype_problem":
                    if (ddltype_problem.SelectedValue == "0")
                    {
                        lblcount_problem.Text = "0";
                        lblcount_problem_all.Text = "0";
                    }
                    else
                    {
                        selectcount_problemwithsupplier(lblcount_problem, lblcount_problem_all, int.Parse(ddltype_problem.SelectedValue), int.Parse(ddl_sup.SelectedValue), int.Parse(ddlmat_no.SelectedValue));
                    }
                    break;

                case "ddlunit_get":
                    ddlunit_problem.SelectedValue = ddlunit_get.SelectedValue;
                    ddlunit_random.SelectedValue = ddlunit_get.SelectedValue;
                    break;

                case "ddllocation_problemedit":
                    selectbuilding(ddlbuilding_problemedit, int.Parse(ddllocation_problemedit.SelectedValue));

                    break;

                case "ddlbuilding_problemedit":
                    selectplace(ddlproduction_problemedit, int.Parse(ddllocation_problemedit.SelectedValue), int.Parse(ddlbuilding_problemedit.SelectedValue));

                    break;

                case "ddlmat_noedit":
                    selectmatname(txtmat_name_edit, int.Parse(ddlmat_noedit.SelectedValue));
                    break;

                case "ddlunit_getedit":
                    ddlunit_problemedit.SelectedValue = ddlunit_getedit.SelectedValue;
                    ddlunit_randomedit.SelectedValue = ddlunit_getedit.SelectedValue;
                    break;

                case "ddlgroupproduct_problemedit":
                    selecttypeproduct(ddltypeproduct_problemedit, int.Parse(ddlgroupproduct_problemedit.SelectedValue));
                    break;

                case "ddltypeproduct_problemedit":
                    selecttypeproduct_m1(ddltypeproduct_problemedit_m1, int.Parse(ddltypeproduct_problemedit.SelectedValue));
                    selecttypeproblem(ddltype_problemedit, int.Parse(ddltypeproduct_problemedit.SelectedValue));
                    break;

                case "ddltypeproduct_problemedit_m1":
                    selecttypeproduct_m2(ddltypeproduct_problemedit_m2, int.Parse(ddltypeproduct_problemedit_m1.SelectedValue));
                    break;

                case "ddl_approve_qa":
                    if (ddl_approve_qa.SelectedValue == "7")
                    {
                        // div_checklist.Visible = true;
                        div_approveqa.Visible = false;
                        panel_accept.Visible = true;
                    }
                    else if (ddl_approve_qa.SelectedValue == "8")
                    {
                        //  div_checklist.Visible = false;
                        panel_accept.Visible = false;
                        // panel_change.Visible = false;
                        div_approveqa.Visible = true;
                        //  rdochange.ClearSelection();
                    }
                    break;

                case "ddlSearchDate":

                    if (int.Parse(ddlSearchDate.SelectedValue) == 3)
                    {
                        AddEndDate.Enabled = true;
                    }
                    else
                    {
                        AddEndDate.Enabled = false;
                        AddEndDate.Text = string.Empty;
                    }

                    break;

                case "ddlrdeptidx_search":
                    getSectionList(ddlrsecidx_search, 1, int.Parse(ddlrdeptidx_search.SelectedValue));
                    break;

                case "ddlgroupproduct_search":
                    selecttypeproduct(ddltypeproduct_search, int.Parse(ddlgroupproduct_search.SelectedValue));
                    break;

                case "ddltypeproduct_search":
                    selecttypeproduct_m1(ddltypeproduct_search_m1, int.Parse(ddltypeproduct_search.SelectedValue));
                    selecttypeproblem(ddlproblem_search, int.Parse(ddltypeproduct_search.SelectedValue));
                    break;
                case "ddltypeproduct_search_m1":
                    selecttypeproduct_m2(ddltypeproduct_search_m2, int.Parse(ddltypeproduct_search_m1.SelectedValue));
                    break;

                case "ddlLocate_search":
                    selectbuilding(ddlBuilding_search, int.Parse(ddlLocate_search.SelectedValue));

                    break;

                case "ddlBuilding_search":
                    selectplace(ddlProduction_name_search, int.Parse(ddlLocate_search.SelectedValue), int.Parse(ddlBuilding_search.SelectedValue));

                    break;

                case "ddltype_addmat":

                    if (ddltype_addmat.SelectedValue == "1")
                    {
                        div_addmanual.Visible = true;
                        div_addimport.Visible = false;
                    }
                    else if (ddltype_addmat.SelectedValue == "2")
                    {
                        div_addmanual.Visible = false;
                        div_addimport.Visible = true;
                    }
                    else
                    {
                        div_addmanual.Visible = false;
                        div_addimport.Visible = false;
                    }

                    break;

                case "ddllocation_exchange":
                    if (ddllocation_exchange.SelectedValue != ViewState["LocRef_Exchange"].ToString())
                    {
                        div_reason_exchange.Visible = true;
                    }
                    else
                    {
                        div_reason_exchange.Visible = false;
                    }
                    break;

                case "ddlactor_search":
                    selectstatus(ddlstatus_search, int.Parse(ddlactor_search.SelectedValue));

                    if (ddlactor_search.SelectedValue != "1")
                    {
                        ddlstatus_search.Items.Insert(1, new ListItem("ดำเนินการ", "1"));
                    }

                    break;

                case "ddlmaster_searchlocation":
                    selectbuilding(ddlmaster_searchbuilding, int.Parse(ddlmaster_searchlocation.SelectedValue));

                    break;

                case "ddlSearchDate_List":

                    if (int.Parse(ddlSearchDate_List.SelectedValue) == 3)
                    {
                        AddEndDate_List.Enabled = true;
                    }
                    else
                    {
                        AddEndDate_List.Enabled = false;
                        AddEndDate_List.Text = string.Empty;
                    }

                    break;

                case "ddltypeproduct_List":
                    selecttypeproduct_m1(ddltypeproduct_list_m1, int.Parse(ddltypeproduct_List.SelectedValue));
                    selecttypeproblem(ddlproblem_List, int.Parse(ddltypeproduct_List.SelectedValue));
                    break;

                case "ddlchoose_addsup":
                    if (ddlchoose_addsup.SelectedValue == "1")
                    {
                        div_addmanualsup.Visible = true;
                        div_addimportsup.Visible = false;
                    }
                    else if (ddlchoose_addsup.SelectedValue == "2")
                    {
                        div_addmanualsup.Visible = false;
                        div_addimportsup.Visible = true;

                    }
                    else
                    {
                        div_addmanualsup.Visible = false;
                        div_addimportsup.Visible = false;
                    }
                    btnshow_supplier.Visible = false;
                    break;

                case "ddlunit_problem":
                    decimal qty_get = Convert.ToDecimal(txtqtyget.Text);
                    decimal qty_problem = Convert.ToDecimal(txtqty_problem.Text);

                    if (ddlunit_get.SelectedValue == ddlunit_problem.SelectedValue && qty_get < qty_problem)
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาตรวจสอบจำนวนที่พบปัญหาอีกครั้ง!!!');", true);
                        ddlunit_problem.SelectedValue = "0";
                    }
                    break;

                case "ddlunit_random":
                    decimal qty_get1 = Convert.ToDecimal(txtqtyget.Text);
                    decimal qty_random = Convert.ToDecimal(txtqty_random.Text);

                    if (ddlunit_get.SelectedValue == ddlunit_random.SelectedValue && qty_get1 < qty_random)
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาตรวจสอบจำนวนสุ่มอีกครั้ง!!!');", true);
                        ddlunit_random.SelectedValue = "0";
                    }
                    break;

                case "ddlunit_rollback":
                    decimal qty_get_edit2 = Convert.ToDecimal(lblqty_get.Text);
                    decimal qty_rollback = Convert.ToDecimal(txtqtyrollback.Text);

                    if (qty_get_edit2 < qty_rollback && lblunit_problem_show.Text == ddlunit_rollback.SelectedValue)
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาตรวจสอบจำนวนส่งคืนอีกครั้ง!!!');", true);
                        ddlunit_rollback.SelectedValue = "0";
                    }
                    break;


                case "ddlgroupproduct_List":
                    selecttypeproduct(ddltypeproduct_List, int.Parse(ddlgroupproduct_List.SelectedValue));

                    break;

                case "ddltypeproduct_list_m1":
                    selecttypeproduct_m2(ddltypeproduct_list_m2, int.Parse(ddltypeproduct_list_m1.SelectedValue));

                    break;

                case "ddladdgroup_masterm1":
                    selecttypeproduct(ddladdproduct_master, int.Parse(ddladdgroup_masterm1.SelectedValue));

                    break;

                case "ddlsearchgroup_masterm1":
                    selecttypeproduct(ddlsearchproduct_masterm1, int.Parse(ddlsearchgroup_masterm1.SelectedValue));

                    break;

                case "ddlgroupproduct_insertmasterproduct_edit":
                    var ddNameProduct = (DropDownList)sender;
                    var row = (GridViewRow)ddNameProduct.NamingContainer;
                    var ddlgroupproduct_insertmasterproduct_edit = (DropDownList)row.FindControl("ddlgroupproduct_insertmasterproduct_edit");
                    var ddlproduct_insertmasterproductm1_edit = (DropDownList)row.FindControl("ddlproduct_insertmasterproductm1_edit");

                    selecttypeproduct(ddlproduct_insertmasterproductm1_edit, int.Parse(ddlgroupproduct_insertmasterproduct_edit.SelectedValue));
                    break;


                case "ddladdgroup_masterm2":
                    selecttypeproduct(ddladdproduct_master_m2, int.Parse(ddladdgroup_masterm2.SelectedValue));
                    break;

                case "ddladdproduct_master_m2":
                    selecttypeproduct_m1(ddladdproduct_masterm1_m2, int.Parse(ddladdproduct_master_m2.SelectedValue));
                    break;

                case "ddlsearchgroup_masterm2":
                    selecttypeproduct(ddlsearchproduct_masterm2, int.Parse(ddlsearchgroup_masterm2.SelectedValue));
                    break;

                case "ddlsearchproduct_masterm2":
                    selecttypeproduct_m1(ddlsearchproduct_masterm1_m2, int.Parse(ddlsearchproduct_masterm2.SelectedValue));
                    break;

                case "ddlgroupproduct_insertmasterproduct_editm2":
                    var ddNameProduct_groupm2 = (DropDownList)sender;
                    var row_groupm2 = (GridViewRow)ddNameProduct_groupm2.NamingContainer;
                    var ddlgroupproduct_insertmasterproduct_editm2 = (DropDownList)row_groupm2.FindControl("ddlgroupproduct_insertmasterproduct_editm2");
                    var ddlproduct_insertmasterproduct_editm2 = (DropDownList)row_groupm2.FindControl("ddlproduct_insertmasterproduct_editm2");

                    selecttypeproduct(ddlproduct_insertmasterproduct_editm2, int.Parse(ddlgroupproduct_insertmasterproduct_editm2.SelectedValue));

                    break;

                case "ddlproduct_insertmasterproduct_editm2":
                    var ddNameProduct_typem2 = (DropDownList)sender;
                    var row_typem2 = (GridViewRow)ddNameProduct_typem2.NamingContainer;
                    var ddlproduct_insertmasterproduct_editm2_ = (DropDownList)row_typem2.FindControl("ddlproduct_insertmasterproduct_editm2");
                    var ddlproduct_insertmasterproductm1_editm2 = (DropDownList)row_typem2.FindControl("ddlproduct_insertmasterproductm1_editm2");

                    selecttypeproduct_m1(ddlproduct_insertmasterproductm1_editm2, int.Parse(ddlproduct_insertmasterproduct_editm2_.SelectedValue));

                    break;

                case "ddltype_group":
                    selecttypeproduct(ddltype_product, int.Parse(ddltype_group.SelectedValue));
                    break;

                case "ddlmaster_searchgroupproblem":
                    selecttypeproduct(ddlmaster_searchproblem, int.Parse(ddlmaster_searchgroupproblem.SelectedValue));

                    break;

                case "ddlm0_group_tpidx_edit":
                    var ddNameProduct_group_problem = (DropDownList)sender;
                    var row_group_problem = (GridViewRow)ddNameProduct_group_problem.NamingContainer;
                    var ddlm0_group_tpidx_edit = (DropDownList)row_group_problem.FindControl("ddlm0_group_tpidx_edit");
                    var ddlm0_tpidx_edit = (DropDownList)row_group_problem.FindControl("ddlm0_tpidx_edit");

                    selecttypeproduct(ddlm0_tpidx_edit, int.Parse(ddlm0_group_tpidx_edit.SelectedValue));

                    break;

            }
        }
        else if (sender is CheckBox)
        {
            CheckBox chkName = (CheckBox)sender;

            CheckBox chkmat = ((CheckBox)FvinsertProblem.FindControl("chkmat"));
            Control divmat_no = ((Control)FvinsertProblem.FindControl("divmat_no"));
            TextBox txtmat_name = ((TextBox)FvinsertProblem.FindControl("txtmat_name"));
            DropDownList ddlmat_no = ((DropDownList)FvinsertProblem.FindControl("ddlmat_no"));

            CheckBox chkmat_edit = ((CheckBox)FvdetailProblem.FindControl("chkmat_edit"));
            Control divmat_no_edit = ((Control)FvdetailProblem.FindControl("divmat_no_edit"));
            TextBox txtmat_name_edit = ((TextBox)FvdetailProblem.FindControl("txtmat_name_edit"));
            DropDownList ddlmat_noedit = ((DropDownList)FvdetailProblem.FindControl("ddlmat_noedit"));


            switch (chkName.ID)
            {
                case "chkmat":
                    if (chkmat.Checked)
                    {
                        //txt.Text = "1";
                        divmat_no.Visible = true;
                    }
                    else
                    {
                        //txt.Text = "2";
                        divmat_no.Visible = false;
                        txtmat_name.Enabled = true;
                        ddlmat_no.SelectedValue = "0";
                        txtmat_name.Text = String.Empty;
                    }
                    break;

                case "chkmat_edit":
                    if (chkmat_edit.Checked)
                    {
                        //txt.Text = "1";
                        divmat_no_edit.Visible = true;
                    }
                    else
                    {
                        //txt.Text = "2";
                        divmat_no_edit.Visible = false;
                        txtmat_name_edit.Enabled = true;
                        ddlmat_noedit.SelectedValue = "0";
                        txtmat_name_edit.Text = String.Empty;
                    }
                    break;

                case "chkmatrisk_edit":
                    setchk_changed();
                    break;

            }
        }
        else if (sender is TextBox)
        {
            TextBox txtqtyget = ((TextBox)FvinsertProblem.FindControl("txtqtyget"));
            TextBox txtqty_problem = ((TextBox)FvinsertProblem.FindControl("txtqty_problem"));
            TextBox txtqty_random = ((TextBox)FvinsertProblem.FindControl("txtqty_random"));
            DropDownList ddlunit_get = ((DropDownList)FvinsertProblem.FindControl("ddlunit_get"));
            DropDownList ddlunit_problem = ((DropDownList)FvinsertProblem.FindControl("ddlunit_problem"));
            DropDownList ddlunit_random = ((DropDownList)FvinsertProblem.FindControl("ddlunit_random"));


            TextBox txtqtyget_edit = ((TextBox)FvdetailProblem.FindControl("txtqtyget_edit"));
            TextBox txtqty_problemedit = ((TextBox)FvdetailProblem.FindControl("txtqty_problemedit"));
            TextBox txtqty_randomedit = ((TextBox)FvdetailProblem.FindControl("txtqty_randomedit"));
            DropDownList ddlunit_getedit = ((DropDownList)FvdetailProblem.FindControl("ddlunit_getedit"));
            DropDownList ddlunit_problemedit = ((DropDownList)FvdetailProblem.FindControl("ddlunit_problemedit"));
            DropDownList ddlunit_randomedit = ((DropDownList)FvdetailProblem.FindControl("ddlunit_randomedit"));


            TextBox txtName = (TextBox)sender;

            switch (txtName.ID)
            {
                case "txtqty_problem":
                    decimal qty_get = Convert.ToDecimal(txtqtyget.Text);
                    decimal qty_problem = Convert.ToDecimal(txtqty_problem.Text);

                    if (qty_get < qty_problem && ddlunit_get.SelectedValue == ddlunit_problem.SelectedValue)
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาตรวจสอบจำนวนที่พบปัญหาอีกครั้ง!!!');", true);
                        txtqty_problem.Text = String.Empty;
                        ddlunit_problem.SelectedValue = "0";
                    }

                    break;

                case "txtqty_random":
                    decimal qty_get1 = Convert.ToDecimal(txtqtyget.Text);
                    decimal qty_random = Convert.ToDecimal(txtqty_random.Text);

                    if (qty_get1 < qty_random && ddlunit_get.SelectedValue == ddlunit_random.SelectedValue)
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาตรวจสอบจำนวนสุ่มอีกครั้ง!!!');", true);
                        txtqty_random.Text = String.Empty;
                        ddlunit_random.SelectedValue = "0";
                    }

                    break;

                case "txtqty_problemedit":
                    decimal qty_get_edit = Convert.ToDecimal(txtqtyget_edit.Text);
                    decimal qty_problem_edit = Convert.ToDecimal(txtqty_problemedit.Text);

                    if (qty_get_edit < qty_problem_edit && ddlunit_getedit.SelectedValue == ddlunit_problemedit.SelectedValue)
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาตรวจสอบจำนวนที่พบปัญหาอีกครั้ง!!!');", true);
                        txtqty_problemedit.Text = String.Empty;
                        ddlunit_problemedit.SelectedValue = "0";
                    }
                    break;

                case "txtqty_randomedit":
                    decimal qty_get_edit1 = Convert.ToDecimal(txtqtyget_edit.Text);
                    decimal qty_random_edit = Convert.ToDecimal(txtqty_randomedit.Text);

                    if (qty_get_edit1 < qty_random_edit && ddlunit_getedit.SelectedValue == ddlunit_randomedit.SelectedValue)
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาตรวจสอบจำนวนสุ่มอีกครั้ง!!!');", true);
                        txtqty_randomedit.Text = String.Empty;
                        ddlunit_randomedit.SelectedValue = "0";
                    }

                    break;


            }

        }

    }

    protected void setchk_changed()
    {
        GridView Gvedit_Material_risk = ((GridView)FvQA_Comment.FindControl("Gvedit_Material_risk"));

        foreach (GridViewRow row in Gvedit_Material_risk.Rows)
        {
            CheckBox chkmatrisk_edit = (CheckBox)row.Cells[0].FindControl("chkmatrisk_edit");
            Label lbm0_mridx_edit = (Label)row.Cells[1].FindControl("lbm0_mridx_edit");
            TextBox txtmatrisk_edit = (TextBox)row.Cells[2].FindControl("txtmatrisk_edit");


            if (chkmatrisk_edit.Checked == true)
            {
                txtmatrisk_edit.Visible = true;
            }
            else
            {
                txtmatrisk_edit.Visible = false;
                txtmatrisk_edit.Text = string.Empty;
            }

        }

    }
    #endregion

    #region bind Actor

    protected void setFormDataActor()
    {

        HiddenField hfM0NodeIDX = (HiddenField)FvdetailProblem.FindControl("hfM0NodeIDX");
        HiddenField hfM0ActoreIDX = (HiddenField)FvdetailProblem.FindControl("hfM0ActoreIDX");
        HiddenField hfM0StatusIDX = (HiddenField)FvdetailProblem.FindControl("hfM0StatusIDX");
        Control div_btnedit = (Control)FvdetailProblem.FindControl("div_btnedit");
        Control qa_iqi_comment = (Control)ViewDetail.FindControl("qa_iqi_comment");
        Label lblu1_docidx = (Label)FvdetailProblem.FindControl("lblu1_docidx");

        ViewState["m0_node"] = hfM0NodeIDX.Value;
        ViewState["m0_actor"] = hfM0ActoreIDX.Value;
        ViewState["m0_status"] = hfM0StatusIDX.Value;

        int m0_actor = int.Parse(hfM0ActoreIDX.Value);

        //txt.Text = ViewState["m0_node"].ToString() + "," + ViewState["m0_actor"].ToString() + "," + ViewState["m0_status"].ToString() + "," + ViewState["History_Detail"].ToString();

        switch (m0_actor)
        {

            case 1: // ผู้สร้าง

                if (ViewState["m0_node"].ToString() == "1" && ViewState["CEmpIDX_u0"].ToString() == ViewState["EmpIDX"].ToString() && ViewState["History_Detail"].ToString() != "3")
                {
                    div_approve.Visible = true;
                    div_btnedit.Visible = true;
                    btnback.Visible = false;
                }
                else
                {
                    div_approve.Visible = false;
                    div_btnedit.Visible = false;
                    if (ViewState["History_Detail"].ToString() == "1")
                    {
                        btnback.Visible = true;
                    }
                    else
                    {
                        btnback.Visible = false;
                    }

                }
                qa_iqi_comment.Visible = false;
                supplier_complete.Visible = false;
                break;

            case 2: // ผู้มีสิทธิ์อนุมัติ
                if (ViewState["History_Detail"].ToString() == "2" && ViewState["m0_node"].ToString() == "2")
                {
                    div_approve.Visible = true;
                    btnback.Visible = false;
                }
                else
                {
                    div_approve.Visible = false;
                    if (ViewState["History_Detail"].ToString() == "1")
                    {
                        btnback.Visible = true;
                    }
                    else
                    {
                        btnback.Visible = false;
                    }

                }
                qa_iqi_comment.Visible = false;
                supplier_complete.Visible = false;
                break;

            case 3: // เจ้าหน้าที่ QA IQI
                if (ViewState["History_Detail"].ToString() == "2" && ViewState["m0_node"].ToString() == "3")
                {
                    div_btnedit.Visible = true;
                    qa_iqi_comment.Visible = true;
                    FvQA_Comment.ChangeMode(FormViewMode.Insert);
                    FvQA_Comment.DataBind();
                    btnback.Visible = false;
                    div_approve.Visible = false;
                    supplier_complete.Visible = false;
                }
                else if (ViewState["History_Detail"].ToString() == "2" && ViewState["m0_node"].ToString() == "7")
                {
                    div_btnedit.Visible = true;
                    qa_iqi_comment.Visible = true;
                    FvQA_Comment.ChangeMode(FormViewMode.Edit);
                    select_adminqa_comment(FvQA_Comment, int.Parse(lblu1_docidx.Text));
                    btnback.Visible = false;
                    div_approve.Visible = false;
                    supplier_complete.Visible = false;
                }
                else if (ViewState["History_Detail"].ToString() == "2" && ViewState["m0_node"].ToString() == "8")
                {
                    div_btnedit.Visible = false;
                    qa_iqi_comment.Visible = true;
                    FvQA_Comment.ChangeMode(FormViewMode.ReadOnly);
                    select_adminqa_comment(FvQA_Comment, int.Parse(lblu1_docidx.Text));
                    btnback.Visible = false;
                    supplier_complete.Visible = true;
                    FvSupplier_Comment.ChangeMode(FormViewMode.ReadOnly);
                    select_supplier_comment(FvSupplier_Comment, int.Parse(ViewState["u0_docidx"].ToString()));
                    div_approve.Visible = true;
                    supplier_complete.Visible = true;
                }
                else if (ViewState["History_Detail"].ToString() != "2" && ViewState["m0_node"].ToString() == "7")
                {
                    div_btnedit.Visible = false;
                    qa_iqi_comment.Visible = true;
                    FvQA_Comment.ChangeMode(FormViewMode.ReadOnly);
                    select_adminqa_comment(FvQA_Comment, int.Parse(lblu1_docidx.Text));

                    if (ViewState["History_Detail"].ToString() == "1")
                    {
                        btnback.Visible = true;
                    }
                    else
                    {
                        btnback.Visible = false;
                    }
                    div_approve.Visible = false;
                    supplier_complete.Visible = false;
                }
                else if (ViewState["History_Detail"].ToString() != "2" && ViewState["m0_node"].ToString() == "8")
                {
                    div_btnedit.Visible = false;
                    qa_iqi_comment.Visible = true;
                    FvQA_Comment.ChangeMode(FormViewMode.ReadOnly);
                    select_adminqa_comment(FvQA_Comment, int.Parse(lblu1_docidx.Text));
                    supplier_complete.Visible = true;
                    FvSupplier_Comment.ChangeMode(FormViewMode.ReadOnly);
                    select_supplier_comment(FvSupplier_Comment, int.Parse(ViewState["u0_docidx"].ToString()));
                    div_approve.Visible = false;
                    supplier_complete.Visible = true;
                    if (ViewState["History_Detail"].ToString() == "1")
                    {
                        btnback.Visible = true;
                    }
                    else
                    {
                        btnback.Visible = false;
                    }
                }
                else if (ViewState["m0_node"].ToString() == "9" && ViewState["m0_status"].ToString() == "7")
                {
                    qa_iqi_comment.Visible = true;
                    FvQA_Comment.ChangeMode(FormViewMode.ReadOnly);
                    select_adminqa_comment(FvQA_Comment, int.Parse(lblu1_docidx.Text));
                    supplier_complete.Visible = true;
                    FvSupplier_Comment.ChangeMode(FormViewMode.ReadOnly);
                    select_supplier_comment(FvSupplier_Comment, int.Parse(ViewState["u0_docidx"].ToString()));
                    div_approve.Visible = false;
                    supplier_complete.Visible = true;

                    if (ViewState["History_Detail"].ToString() == "1")
                    {
                        btnback.Visible = true;
                    }
                    else
                    {
                        btnback.Visible = false;
                    }
                }
                else
                {
                    div_btnedit.Visible = false;
                    qa_iqi_comment.Visible = false;
                    if (ViewState["History_Detail"].ToString() == "1")
                    {
                        btnback.Visible = true;
                    }
                    else
                    {
                        btnback.Visible = false;
                    }

                    if(ViewState["m0_node"].ToString() == "9" && ViewState["m0_status"].ToString() == "8" || ViewState["m0_node"].ToString() == "3" && ViewState["History_Detail"].ToString() == "1")
                    {
                        supplier_complete.Visible = false;
                    }
                    else
                    {
                        supplier_complete.Visible = true;
                    }


                }

                break;
            case 4: // หัวหน้า QA IQI
                if (ViewState["History_Detail"].ToString() == "2" && ViewState["m0_node"].ToString() == "4")
                {
                    div_approve.Visible = true;
                    btnback.Visible = false;
                }
                else
                {
                    div_approve.Visible = false;
                    if (ViewState["History_Detail"].ToString() == "1")
                    {
                        btnback.Visible = true;
                    }
                    else
                    {
                        btnback.Visible = false;
                    }
                }
                qa_iqi_comment.Visible = true;
                FvQA_Comment.ChangeMode(FormViewMode.ReadOnly);
                select_adminqa_comment(FvQA_Comment, int.Parse(lblu1_docidx.Text));
                supplier_complete.Visible = false;
                break;

            case 5: // เจ้าหน้าที่จัดซื้อ

                break;

            case 6: // Supplier
                div_btnedit.Visible = false;
                qa_iqi_comment.Visible = true;
                FvQA_Comment.ChangeMode(FormViewMode.ReadOnly);
                select_adminqa_comment(FvQA_Comment, int.Parse(lblu1_docidx.Text));
                div_approve.Visible = false;

                if (ViewState["m0_node"].ToString() == "9")
                {
                    supplier_complete.Visible = true;
                    FvSupplier_Comment.ChangeMode(FormViewMode.ReadOnly);
                    select_supplier_comment(FvSupplier_Comment, int.Parse(ViewState["u0_docidx"].ToString()));
                }
                else
                {
                    supplier_complete.Visible = false;
                }
                break;
        }

    }
    #endregion

    #region data excel
    protected void WriteExcelWithNPOI(DataTable dt, String extension, String fileName, int _set_value)
    {
        IWorkbook workbook;


        if (extension == "xlsx")
        {
            workbook = new XSSFWorkbook();
        }
        else if (extension == "xls")
        {
            workbook = new HSSFWorkbook();
        }
        else
        {
            throw new Exception("This format is not supported");
        }

        ISheet sheet1 = workbook.CreateSheet("Sheet 1");
        IRow row1 = sheet1.CreateRow(0);

        ICellStyle testeStyle = workbook.CreateCellStyle();
        testeStyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.Medium;
        testeStyle.FillForegroundColor = IndexedColors.BrightGreen.Index;
        testeStyle.FillPattern = FillPattern.SolidForeground;

        for (int j = 0; j < dt.Columns.Count; j++)
        {
            ICell cell = row1.CreateCell(j);
            String columnName = dt.Columns[j].ToString();
            cell.SetCellValue(columnName);
        }

        //set merge cell
        int max_row = dt.Rows.Count - 1;

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            IRow row = sheet1.CreateRow(i + 1);

            for (int j = 0; j < dt.Columns.Count; j++)
            {
                ICell cell = row.CreateCell(j);
                String columnName = dt.Columns[j].ToString();
                cell.SetCellValue(dt.Rows[i][columnName].ToString());

            }

        }

        using (var exportData = new MemoryStream())
        {
            Response.Clear();
            workbook.Write(exportData);

            if (extension == "xlsx")
            {
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xlsx"));

                Response.BinaryWrite(exportData.ToArray());
            }
            else if (extension == "xls")
            {
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xls"));
                Response.BinaryWrite(exportData.GetBuffer());
            }
            Response.End();
        }


    }
    #endregion data excel

    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        var ddllocation_problem = (DropDownList)FvinsertProblem.FindControl("ddllocation_problem");
        var ddlproduction_problem = (DropDownList)FvinsertProblem.FindControl("ddlproduction_problem");
        var ddltypeproduct_problem = (DropDownList)FvinsertProblem.FindControl("ddltypeproduct_problem");
        var ddlmat_no = (DropDownList)FvinsertProblem.FindControl("ddlmat_no");
        var txtmat_name = (TextBox)FvinsertProblem.FindControl("txtmat_name");
        var txtbatch_name = (TextBox)FvinsertProblem.FindControl("txtbatch_name");
        var ddl_sup = (DropDownList)FvinsertProblem.FindControl("ddl_sup");
        var txtgetdate = (TextBox)FvinsertProblem.FindControl("txtgetdate");
        var txtqtyget = (TextBox)FvinsertProblem.FindControl("txtqtyget");
        var ddlunit_get = (DropDownList)FvinsertProblem.FindControl("ddlunit_get");
        var txtqty_problem = (TextBox)FvinsertProblem.FindControl("txtqty_problem");
        var ddlunit_problem = (DropDownList)FvinsertProblem.FindControl("ddlunit_problem");
        var txtqty_random = (TextBox)FvinsertProblem.FindControl("txtqty_random");
        var ddlunit_random = (DropDownList)FvinsertProblem.FindControl("ddlunit_random");
        var ddltype_problem = (DropDownList)FvinsertProblem.FindControl("ddltype_problem");
        var txtremark = (TextBox)FvinsertProblem.FindControl("txtremark");
        var chkmat_edit = (CheckBox)FvdetailProblem.FindControl("chkmat_edit");
        var txtremark_exchange = (TextBox)FvQA_Comment.FindControl("txtremark_exchange");
        //var rdochange = (RadioButtonList)FvQA_Comment.FindControl("rdochange");
        var txtremark_approveqa = (TextBox)FvQA_Comment.FindControl("txtremark_approveqa");


        switch (cmdName)
        {
            case "_divMenuBtnToDivIndex":
                SetDefaultpage(1);
                break;

            case "_divMenuBtnToDivAdd":

                SetDefaultpage(2);
                break;

            case "_divMenuBtnToDivApprove":
                SetDefaultpage(3);

                break;

            case "_divMenuBtnToDivMasterGroupProduct":
                SetDefaultpage(13);

                break;

            case "_divMenuBtnToDivMasterProduct":
                SetDefaultpage(4);
                break;

            case "_divMenuBtnToDivMasterProduct_M1":
                SetDefaultpage(14);

                break;

            case "_divMenuBtnToDivMasterProduct_M2":
                SetDefaultpage(15);

                break;

            case "_divMenuBtnToDivMasterUnit":
                SetDefaultpage(6);
                break;

            case "_divMenuBtnToDivMasterPlace":
                SetDefaultpage(7);
                break;


            case "_divMenuBtnToDivMasterProblem":
            case "btnCancelMaster_Problem":
                SetDefaultpage(8);
                break;


            case "_divMenuBtnToDivReport":
                SetDefaultpage(5);

                break;

            case "_divMenuBtnToDivMasterSupplier":
                SetDefaultpage(10);
                break;

            case "_divMenuBtnToDivMasterMat":
                SetDefaultpage(11);
                break;

            case "_divMenuBtnToDivMasterMatRisk":
                SetDefaultpage(12);
                break;

            case "CmdAddMaster_GroupProduct":
                Panel_AddMasterGroup.Visible = true;
                btnshow_groupproduct.Visible = false;
                txtgroupproduct.Text = String.Empty;
                ddStatusadd_mastergroup.SelectedValue = "1";
                div_searchmaster_groupproduct.Visible = false;
                selectgroupproduct(ddlgroupproduct_insertmasterproduct);
                ddlgroupproduct_insertmasterproduct.SelectedValue = "0";
                break;

            case "CmdInsertMaster_Group":
                insertmaster_group(txtgroupproduct.Text, int.Parse(ddStatusadd_mastergroup.SelectedValue), int.Parse(ViewState["EmpIDX"].ToString()), "0");
                SetDefaultpage(13);

                break;

            case "CmdDelMaster_group":
                int m0_group_tpidx_master = int.Parse(cmdArg);
                deletemaster_group(9, int.Parse(ViewState["EmpIDX"].ToString()), m0_group_tpidx_master, 9);
                SetDefaultpage(13);
                break;

            case "btnCancelMaster_Group":
                Panel_AddMasterGroup.Visible = false;
                btnshow_groupproduct.Visible = true;
                div_searchmaster_groupproduct.Visible = true;
                break;

            case "CmdSearchMaster_Group":
                selectmaster_group(GvMaster_Group, txtmaster_searchgroup.Text);

                break;

            case "btnRefreshMaster_Group":
                txtmaster_searchgroup.Text = String.Empty;
                selectmaster_group(GvMaster_Group, txtmaster_searchgroup.Text);
                break;

            case "CmdAddMaster_Product":
                Panel_Add.Visible = true;
                btnshow_product.Visible = false;
                txttypeproduct.Text = String.Empty;
                ddStatusadd.SelectedValue = "1";
                div_searchmaster_typeproduct.Visible = false;
                selectgroupproduct(ddlgroupproduct_insertmasterproduct);
                ddlgroupproduct_insertmasterproduct.SelectedValue = "0";
                break;

            case "btnCancelMaster_Product":
                Panel_Add.Visible = false;
                btnshow_product.Visible = true;
                div_searchmaster_typeproduct.Visible = true;
                break;

            case "CmdInsertMaster_Product":
                insertmaster_product(txttypeproduct.Text, int.Parse(ddStatusadd.SelectedValue), int.Parse(ViewState["EmpIDX"].ToString()), "0", int.Parse(ddlgroupproduct_insertmasterproduct.SelectedValue));
                SetDefaultpage(4);

                break;

            case "CmdDelMaster_product":
                int m0_tpidx_master = int.Parse(cmdArg);
                deletemaster_product(9, int.Parse(ViewState["EmpIDX"].ToString()), m0_tpidx_master);
                SetDefaultpage(4);
                break;


            case "CmdAddMaster_Product_M1":
                Panel_AddMasterProduct_M1.Visible = true;
                btnshow_product_m1.Visible = false;
                selectgroupproduct(ddladdgroup_masterm1);
                ddladdgroup_masterm1.SelectedValue = "0";
                ddladdproduct_master.SelectedValue = "0";
                txtaddmaster_product_m1.Text = string.Empty;
                ddlstatusinsert_masterproductm1.SelectedValue = "1";
                div_searchmaster_product_m1.Visible = false;
                break;

            case "CmdInsertMaster_Product_M1":
                insertmaster_product_m1(txtaddmaster_product_m1.Text, int.Parse(ddlstatusinsert_masterproductm1.SelectedValue), int.Parse(ViewState["EmpIDX"].ToString()), "0", int.Parse(ddladdproduct_master.SelectedValue));
                SetDefaultpage(14);
                break;

            case "btnCancelMaster_Product_M1":
                Panel_AddMasterProduct_M1.Visible = false;
                btnshow_product_m1.Visible = true;
                div_searchmaster_product_m1.Visible = true;
                break;

            case "CmdSearchMaster_Product_M1":
                selectmaster_product_m1(GvMaster_Product_M1, txtsearchproduct_masterm1.Text, int.Parse(ddlsearchproduct_masterm1.SelectedValue), int.Parse(ddlsearchgroup_masterm1.SelectedValue));
                mergeCell(GvMaster_Product_M1);

                break;
            case "btnRefreshMaster_Product_M1":
                selectgroupproduct(ddlsearchgroup_masterm1);
                ddlsearchgroup_masterm1.SelectedValue = "0";
                ddlsearchproduct_masterm1.SelectedValue = "0";
                txtsearchproduct_masterm1.Text = String.Empty;
                selectmaster_product_m1(GvMaster_Product_M1, txtsearchproduct_masterm1.Text, int.Parse(ddlsearchproduct_masterm1.SelectedValue), int.Parse(ddlsearchgroup_masterm1.SelectedValue));
                mergeCell(GvMaster_Product_M1);

                break;

            case "CmdDelMaster_product_m1":
                int m1_tpidx_master = int.Parse(cmdArg);
                deletemaster_product_m1(9, int.Parse(ViewState["EmpIDX"].ToString()), m1_tpidx_master, 10);
                SetDefaultpage(14);
                break;


            case "CmdAddMaster_Product_M2":
                Panel_AddMasterProduct_M2.Visible = true;
                btnshow_product_m2.Visible = false;
                selectgroupproduct(ddladdgroup_masterm2);
                ddladdgroup_masterm2.SelectedValue = "0";
                ddladdproduct_master_m2.SelectedValue = "0";
                ddladdproduct_masterm1_m2.SelectedValue = "0";
                txtaddmaster_product_m2.Text = string.Empty;
                ddlstatusinsert_masterproductm2.SelectedValue = "1";
                div_searchmaster_product_m2.Visible = false;
                break;

            case "CmdInsertMaster_Product_M2":
                insertmaster_product_m2(txtaddmaster_product_m2.Text, int.Parse(ddlstatusinsert_masterproductm2.SelectedValue), int.Parse(ViewState["EmpIDX"].ToString()), "0", int.Parse(ddladdproduct_masterm1_m2.SelectedValue));
                SetDefaultpage(15);
                break;

            case "btnCancelMaster_Product_M2":
                Panel_AddMasterProduct_M2.Visible = false;
                btnshow_product_m2.Visible = true;
                div_searchmaster_product_m2.Visible = true;
                break;

            case "CmdSearchMaster_Product_M2":
                selectmaster_product_m2(GvMaster_Product_M2, txtsearchproduct_masterm2.Text, int.Parse(ddlsearchproduct_masterm1_m2.SelectedValue), int.Parse(ddlsearchproduct_masterm2.SelectedValue), int.Parse(ddlsearchgroup_masterm2.SelectedValue));
                mergeCell(GvMaster_Product_M2);
                break;

            case "btnRefreshMaster_Product_M2":
                selectgroupproduct(ddlsearchgroup_masterm2);
                ddlsearchgroup_masterm2.SelectedValue = "0";
                ddlsearchproduct_masterm2.SelectedValue = "0";
                ddlsearchproduct_masterm1_m2.SelectedValue = "0";
                txtsearchproduct_masterm2.Text = String.Empty;

                selectmaster_product_m2(GvMaster_Product_M2, txtsearchproduct_masterm2.Text, int.Parse(ddlsearchproduct_masterm1_m2.SelectedValue), int.Parse(ddlsearchproduct_masterm2.SelectedValue), int.Parse(ddlsearchgroup_masterm2.SelectedValue));
                mergeCell(GvMaster_Product_M2);
                break;

            case "CmdDelMaster_product_m2":
                int m2_tpidx_master = int.Parse(cmdArg);
                deletemaster_product_m2(9, int.Parse(ViewState["EmpIDX"].ToString()), m2_tpidx_master, 11);
                SetDefaultpage(15);
                break;

            case "CmdAddMaster_Unit":
                Panel_AddMasterUnit.Visible = true;
                btnshow_unit.Visible = false;
                txtunit.Text = String.Empty;
                ddStatusadd_masterunit.SelectedValue = "1";
                div_searchmaster_unit.Visible = false;
                break;


            case "btnCancelMaster_Unit":
                Panel_AddMasterUnit.Visible = false;
                btnshow_unit.Visible = true;
                div_searchmaster_unit.Visible = true;
                break;

            case "CmdInsertMaster_Unit":
                insertmaster_unit(txtunit.Text, int.Parse(ddStatusadd_masterunit.SelectedValue), int.Parse(ViewState["EmpIDX"].ToString()), "0");
                SetDefaultpage(6);
                break;

            case "CmdDelMaster_unit":
                int m0_unidx_master = int.Parse(cmdArg);
                deletemaster_unit(9, int.Parse(ViewState["EmpIDX"].ToString()), m0_unidx_master);
                SetDefaultpage(6);
                break;

            case "CmdAddMaster_Place":
                Panel_AddMasterPlace.Visible = true;
                btnshow_place.Visible = false;
                txtplace.Text = String.Empty;
                ddStatusadd_masterplace.SelectedValue = "1";
                ddlbuilding.SelectedValue = "0";
                ddllocation.SelectedValue = "0";
                div_searchmaster_place.Visible = false;
                break;

            case "btnCancelMaster_Place":
                Panel_AddMasterPlace.Visible = false;
                btnshow_place.Visible = true;
                div_searchmaster_place.Visible = true;

                break;

            case "CmdInsertMaster_Place":
                insertmaster_place(int.Parse(ddllocation.SelectedValue), int.Parse(ddlbuilding.SelectedValue), txtplace.Text, int.Parse(ddStatusadd_masterplace.SelectedValue),
                    int.Parse(ViewState["EmpIDX"].ToString()), "0");
                SetDefaultpage(7);

                break;
            case "CmdDelMaster_place":
                int m0_plidx_master = int.Parse(cmdArg);
                deletemaster_place(9, int.Parse(ViewState["EmpIDX"].ToString()), m0_plidx_master);
                SetDefaultpage(7);
                break;

            case "CmdAddMaster_Problem":
                Panel_AddMasterProblem.Visible = true;
                btnshow_problem.Visible = false;
                txtplace.Text = String.Empty;
                ddStatusadd_masterplace.SelectedValue = "1";
                div_searchmaster_problem.Visible = false;
                break;

            case "CmdInsertMaster_Problem":
                insertmaster_problem(int.Parse(ddltype_product.SelectedValue), txttypeproblem.Text, int.Parse(ddlstatus_mastertypeproblem.SelectedValue),
                  int.Parse(ViewState["EmpIDX"].ToString()), "0");
                SetDefaultpage(8);
                break;

            case "CmdDelMaster_problem":
                int m0_pridx_master = int.Parse(cmdArg);
                deletemaster_problem(9, int.Parse(ViewState["EmpIDX"].ToString()), m0_pridx_master);
                SetDefaultpage(8);
                break;

            case "CmdAddMaster_Supplier":
                Panel_AddMasterSupplier.Visible = true;
                btnshow_supplier.Visible = false;
                txtsupname.Text = String.Empty;
                txtemail_sup.Text = String.Empty;
                ddlstatus_sup.SelectedValue = "1";
                div_searchmaster_supplier.Visible = false;

                break;

            case "CmdInsertMaster_Supplier":
                insertmaster_supplier(0, txtsupname.Text, txtemail_sup.Text, int.Parse(ddlstatus_mastertypeproblem.SelectedValue),
                  int.Parse(ViewState["EmpIDX"].ToString()));
                SetDefaultpage(10);
                break;

            case "btnCancelMaster_Supplier":
                Panel_AddMasterSupplier.Visible = false;
                btnshow_supplier.Visible = true;
                div_searchmaster_supplier.Visible = true;
                ddlchoose_addsup.SelectedValue = "0";
                div_addimportsup.Visible = false;
                div_addmanualsup.Visible = false;

                break;

            case "CmdImportMaster_Sup":
                if (UploadFileSup.HasFile)
                {
                    string datetimeNow = DateTime.Now.ToString("yyyyMMdd_HHmmss_ffff_import");
                    string FileName = Path.GetFileName(UploadFileSup.PostedFile.FileName);
                    string extension = Path.GetExtension(UploadFileSup.PostedFile.FileName);
                    string newFileName = datetimeNow + extension.ToLower();
                    string folderPath = ConfigurationManager.AppSettings["path_supplier_import"];
                    string filePath = Server.MapPath(folderPath + newFileName);

                    if (extension.ToLower() == ".xls")
                    {
                        UploadFileSup.SaveAs(filePath);
                        ImportFileSuppliername(filePath, extension, "Yes", FileName, int.Parse(ViewState["EmpIDX"].ToString()), 5);
                        File.Delete(filePath);
                        SetDefaultpage(10);
                    }
                    else
                    {
                        _funcTool.showAlert(this, "Import ได้เฉพาะไฟล์ Excel (นามสกุลไฟล์ .xls) เท่านั้น");
                    }
                }
                break;

            case "CmdDelMaster_Supplier":
                int m0_supidx_master = int.Parse(cmdArg);
                deletemaster_supplier(9, int.Parse(ViewState["EmpIDX"].ToString()), m0_supidx_master, 5);
                selectmaster_supplier(GvMaster_Supplier, txtmaster_searchsupplier.Text, txtmaster_searchemail.Text);
                div_searchmaster_supplier.Visible = true;
                break;

            case "btnreset_pass":
                int m0_supidx_resetpass = int.Parse(cmdArg);
                deletemaster_supplier(1, int.Parse(ViewState["EmpIDX"].ToString()), m0_supidx_resetpass, 6);
                selectmaster_supplier(GvMaster_Supplier, txtmaster_searchsupplier.Text, txtmaster_searchemail.Text);

                break;

            case "CmdAddMaster_Mat":
                Panel_AddMasterMaterial.Visible = true;
                btnshow_mat.Visible = false;
                linkBtnTrigger(btnAdddata_mat);
                div_searchmaster_mat.Visible = false;
                div_addimport.Visible = false;
                div_addmanual.Visible = false;
                ddltype_addmat.SelectedValue = "0";
                txtmatno.Text = String.Empty;
                txtmatname.Text = String.Empty;
                ddlstatus_mastermat.SelectedValue = "1";
                break;

            case "btnCancelMaster_Mat":
                Panel_AddMasterMaterial.Visible = false;
                btnshow_mat.Visible = true;
                div_searchmaster_mat.Visible = true;

                break;

            case "CmdInsertMaster_Mat":
                insertmaster_mat(0, int.Parse(txtmatno.Text), txtmatname.Text, int.Parse(ddlstatus_mastermat.SelectedValue), int.Parse(ViewState["EmpIDX"].ToString()));
                SetDefaultpage(11);
                break;

            case "CmdImportMaster_Mat":
                if (UploadFileMat.HasFile)
                {
                    string datetimeNow = DateTime.Now.ToString("yyyyMMdd_HHmmss_ffff_import");
                    string FileName = Path.GetFileName(UploadFileMat.PostedFile.FileName);
                    string extension = Path.GetExtension(UploadFileMat.PostedFile.FileName);
                    string newFileName = datetimeNow + extension.ToLower();
                    string folderPath = ConfigurationManager.AppSettings["path_matnumber_import"];
                    string filePath = Server.MapPath(folderPath + newFileName);
                    if (extension.ToLower() == ".xls" || extension.ToLower() == ".xlsx" || extension.ToLower() == ".csv" || extension.ToLower() == ".ods")
                    {
                        UploadFileMat.SaveAs(filePath);
                        ImportFileMatNumber(filePath, extension, "Yes", FileName, int.Parse(ViewState["EmpIDX"].ToString()), 4);
                        File.Delete(filePath);
                        SetDefaultpage(11);
                    }
                    else
                    {
                        _funcTool.showAlert(this, "Import ได้เฉพาะไฟล์ Excel (นามสกุลไฟล์ .xls หรือ .xlsx) เท่านั้น");
                    }
                }
                break;

            case "CmdDelMaster_Mat":
                int matidx_master = int.Parse(cmdArg);
                deletemaster_mat(9, int.Parse(ViewState["EmpIDX"].ToString()), matidx_master, 7);
                selectmaster_mat(GvMaster_Mat, txtmaster_searchmatno.Text, txtmaster_searchmatname.Text);
                div_searchmaster_mat.Visible = true;
                break;

            case "CmdAddMaster_Risk":
                Panel_AddMasterMaterialRisk.Visible = true;
                btnshow_matrisk.Visible = false;
                div_searchmaster_matrisk.Visible = false;

                break;

            case "btnCancelMaster_Risk":
                Panel_AddMasterMaterialRisk.Visible = false;
                btnshow_matrisk.Visible = true;
                txtmat_risk.Text = String.Empty;
                ddlstatus_mastermatrisk.SelectedValue = "1";
                div_searchmaster_matrisk.Visible = true;

                break;

            case "CmdInsertMaster_Risk":
                insertmaster_material_risk(txtmat_risk.Text, int.Parse(ddlstatus_mastermatrisk.SelectedValue), int.Parse(ViewState["EmpIDX"].ToString()), "0");
                SetDefaultpage(12);
                break;

            case "CmdDelMaster_Risk":
                int m0_mridx_master = int.Parse(cmdArg);
                deletemaster_matterial_risk(9, int.Parse(ViewState["EmpIDX"].ToString()), m0_mridx_master, 8);
                selectmaster_materialrisk(GvMaster_MatRisk, txtmaster_searchmatrisk.Text);
                div_searchmaster_matrisk.Visible = true;
                break;

            case "CmdInsert":
                Insert_System();
                //if (datemodify < dateexp && datemodify <= dateproblem && datemodify <= dateget)
                //{
                SetDefaultpage(1);
                //}
                //else
                //{
                //    break;
                //}
                break;

            case "BtnBack":
                MvMaster.SetActiveView(ViewIndex);
                setOntop.Focus();

                break;

            case "CmdDetail":
                string[] arg1_detail = new string[4];
                arg1_detail = e.CommandArgument.ToString().Split(';');

                ViewState["u0_docidx"] = arg1_detail[0];
                ViewState["doc_code"] = arg1_detail[1];
                ViewState["CEmpIDX_u0"] = arg1_detail[2];
                ViewState["History_Detail"] = arg1_detail[3];

                //txt.Text = ViewState["u0_docidx"].ToString() + "," + ViewState["doc_code"].ToString() + "," + ViewState["CEmpIDX_u0"].ToString() + "," + ViewState["History_Detail"].ToString();

                SetDefaultpage(9);
                setFormDataActor();
                selectstatus(ddl_approve, int.Parse(ViewState["m0_node"].ToString()));
                txtremark_approve.Text = String.Empty;
                break;

            case "CmdUpdateApprove":

                Update_System(int.Parse(ViewState["m0_node"].ToString()), int.Parse(ViewState["m0_actor"].ToString()), int.Parse(ddl_approve.SelectedValue), int.Parse(ViewState["u0_docidx"].ToString()), txtremark_approve.Text, int.Parse(ViewState["EmpIDX"].ToString()), 2, int.Parse(ViewState["m0_tpidx_check_seaweed"].ToString()));

                if (ViewState["m0_node"].ToString() == "4" && ViewState["m0_actor"].ToString() == "4" && int.Parse(ddl_approve.SelectedValue) == 2 && ViewState["LocRef_Exchange"].ToString() != ViewState["Loc_Originale"].ToString() && ViewState["m0_tpidx_check_seaweed"].ToString() != "1")
                {
                    Exchange_System();
                }

                SetDefaultpage(3);
                break;

            case "CmdUpdateApprove_Qa":
                Update_System(int.Parse(ViewState["m0_node"].ToString()), int.Parse(ViewState["m0_actor"].ToString()), int.Parse(ddl_approve_qa.SelectedValue), int.Parse(ViewState["u0_docidx"].ToString()), txtremark_approveqa.Text, int.Parse(ViewState["EmpIDX"].ToString()), 2, int.Parse(ViewState["m0_tpidx_check_seaweed"].ToString()));
                SetDefaultpage(3);
                break;

            case "CmdEdit_Problem":
                string arg_edit = e.CommandArgument.ToString();

                ViewState["u1_docidx"] = arg_edit;

                FvdetailProblem.ChangeMode(FormViewMode.Edit);
                select_detail_list(FvdetailProblem, int.Parse(ViewState["u0_docidx"].ToString()));
                div_approve.Visible = false;
                qa_iqi_comment.Visible = false;
                break;

            case "CmdUpdate_Problem":
                Edit_Detail_Problem();
                SetDefaultpage(9);
                setFormDataActor();
                break;

            case "BtnBack_Problem":
                SetDefaultpage(9);
                setFormDataActor();
                break;

            case "btnback_editqa":
                SetDefaultpage(3);
                break;

            case "btnCancel_exchange":
                SetDefaultpage(3);
                break;

            case "CmdApprove_QA":
                UpdateQAComment_System();
                if (checkcomment == 1 && checkcount != 0 && (ViewState["m0_tpidx_check_seaweed"].ToString() == "1" || ViewState["LocIDX_List"].ToString() == "14") || ViewState["m0_tpidx_check_seaweed"].ToString() != "1" && ViewState["LocIDX_List"].ToString() != "14")
                {
                    SetDefaultpage(3);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณากรอกข้อมูล Material Risk ให้ครบ!!!');", true);

                }
                break;

            case "CmdApprove_QAEdit":
                UpdateQACommentEdit_System();
                if (checkcomment == 1 && checkcount != 0 && (ViewState["m0_tpidx_check_seaweed"].ToString() == "1" || ViewState["LocIDX_List"].ToString() == "14") || ViewState["m0_tpidx_check_seaweed"].ToString() != "1" && ViewState["LocIDX_List"].ToString() != "14")
                {
                    SetDefaultpage(3);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณากรอกข้อมูล Material Risk ให้ครบ!!!');", true);

                }
                break;

            case "btnCancel":
                if (ViewState["History_Detail"].ToString() == "2")
                {
                    SetDefaultpage(3);
                }
                else
                {
                    MvMaster.SetActiveView(ViewIndex);
                    setOntop.Focus();
                }
                break;

            case "btnsearch_report":
                ViewState["Columns_Check_Search"] = null;
                List<String> YrColumnsList = new List<string>();
                foreach (ListItem item1 in YrChkBoxColumns.Items)
                {
                    if (item1.Selected)
                    {
                        YrColumnsList.Add(item1.Value);
                    }
                    else
                    {
                        YrColumnsList.Add("0");
                    }
                }
                String YrColumnsList1 = String.Join(",", YrColumnsList.ToArray());
                ViewState["Columns_Check_Search"] = YrColumnsList1;
                selectreportlist(GvReport);
                GvReport.Visible = true;
                break;

            case "btnexport":
                int count_ = 0;
                int no = 1;

                _dtproblem = new data_qmr_problem();

                _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
                U0_ProblemDocument _select = new U0_ProblemDocument();

                _select.condition = 12;
                _select.rsecidx = int.Parse(ddlrsecidx_search.SelectedValue);
                _select.rdeptidx = int.Parse(ddlrdeptidx_search.SelectedValue);
                _select.doc_code = txtDocCode.Text;
                _select.empcode = txtEmpCOde_search.Text;
                _select.LocIDX = int.Parse(ddlLocate_search.SelectedValue);
                _select.acidx = int.Parse(ddlactor_search.SelectedValue);
                _select.staidx = int.Parse(ddlstatus_search.SelectedValue);
                _select.BuildingIDX = int.Parse(ddlBuilding_search.SelectedValue);
                _select.m0_plidx = int.Parse(ddlProduction_name_search.SelectedValue);
                _select.m0_pbidx = int.Parse(ddltypeproblem_search.SelectedValue);
                _select.m0_supidx = int.Parse(ddlsupplier_search.SelectedValue);
                _select.m0_group_tpidx = int.Parse(ddlgroupproduct_search.SelectedValue);
                _select.m0_tpidx = int.Parse(ddltypeproduct_search.SelectedValue);
                _select.m1_tpidx = int.Parse(ddltypeproduct_search_m1.SelectedValue);
                _select.m2_tpidx = int.Parse(ddltypeproduct_search_m2.SelectedValue);
                _select.m0_pridx = int.Parse(ddlproblem_search.SelectedValue);
                _select.ifsearch = int.Parse(ddlSearchDate.SelectedValue);
                _select.datecreate = AddStartdate.Text;
                _select.dateend = AddEndDate.Text;

                _dtproblem.Boxu0_ProblemDocument[0] = _select;
                //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

                _dtproblem = callServicePostQMR_Problem(_urlGetSystem, _dtproblem);

                if (_dtproblem.ReturnCode.ToString() != "1")
                {
                    DataTable tableFormList = new DataTable();

                    tableFormList.Columns.Add("ลำดับ", typeof(String));
                    tableFormList.Columns.Add("เดือน", typeof(String));
                    tableFormList.Columns.Add("วันที่พบ", typeof(String));
                    tableFormList.Columns.Add("รหัสรายการ", typeof(String));
                    tableFormList.Columns.Add("Mat", typeof(String));
                    tableFormList.Columns.Add("ชื่อ", typeof(String));
                    tableFormList.Columns.Add("Batch", typeof(String));
                    tableFormList.Columns.Add("ประเภท", typeof(String));
                    tableFormList.Columns.Add("Supplier", typeof(String));
                    tableFormList.Columns.Add("รายละเอียดปัญหา", typeof(String));
                    tableFormList.Columns.Add("รับเข้า", typeof(String));

                    tableFormList.Columns.Add("จำนวนสุ่ม / ผลิตเบิก", typeof(String));
                    tableFormList.Columns.Add("จำนวนปัญหา", typeof(String));
                    tableFormList.Columns.Add("ระดับ", typeof(String));
                    tableFormList.Columns.Add("สถานที่พบ", typeof(String));
                    tableFormList.Columns.Add("Action-คืน/แก้ไข", typeof(String));
                    tableFormList.Columns.Add("วันส่งจัดซื้อ", typeof(String));
                    tableFormList.Columns.Add("ตอบกลับ", typeof(String));
                    tableFormList.Columns.Add("โรงงาน", typeof(String));
                    tableFormList.Columns.Add("MFD/EXP", typeof(String));

                    foreach (var row_report in _dtproblem.Boxu0_ProblemDocument)
                    {
                        DataRow addFormListRow = tableFormList.NewRow();

                        addFormListRow[0] = no.ToString();
                        addFormListRow[1] = row_report.month_create.ToString();
                        addFormListRow[2] = row_report.dateproblem.ToString();
                        addFormListRow[3] = row_report.doc_code.ToString();
                        addFormListRow[4] = row_report.mat_no.ToString();
                        addFormListRow[5] = row_report.matname.ToString();
                        addFormListRow[6] = row_report.batchnumber.ToString();
                        addFormListRow[7] = row_report.typeproduct_name.ToString();
                        addFormListRow[8] = row_report.sup_name.ToString();
                        addFormListRow[9] = row_report.detail_remark.ToString();
                        addFormListRow[10] = row_report.qty_get.ToString() + " " + row_report.unit_name.ToString();


                        addFormListRow[11] = row_report.qty_random.ToString() + " " + row_report.unit_name_random.ToString();
                        addFormListRow[12] = row_report.qty_problem.ToString() + " " + row_report.unit_name_problem.ToString();
                        addFormListRow[13] = row_report.type_problem_name.ToString();
                        addFormListRow[14] = row_report.production_name.ToString();
                        addFormListRow[15] = row_report.rollback_name.ToString();
                        addFormListRow[16] = row_report.send_pur.ToString();
                        addFormListRow[17] = row_report.comment_sup.ToString();
                        addFormListRow[18] = row_report.LocName.ToString();
                        addFormListRow[19] = row_report.datemodify.ToString() + " " + row_report.dateexp.ToString();

                        no += 1;
                        tableFormList.Rows.InsertAt(addFormListRow, count_++);
                    }



                    /*
                    tableFormList.Columns.Add("ฝ่าย", typeof(String));
                    tableFormList.Columns.Add("แผนก", typeof(String));
                    tableFormList.Columns.Add("ชื่อผู้แจ้งปัญหา", typeof(String));
                    tableFormList.Columns.Add("วันที่แจ้งปัญหา", typeof(String));
                    tableFormList.Columns.Add("รหัสเอกสาร", typeof(String));
                    tableFormList.Columns.Add("สถานที่", typeof(String));
                    tableFormList.Columns.Add("อาคาร", typeof(String));
                    tableFormList.Columns.Add("ไลน์ผลิต", typeof(String));
                    tableFormList.Columns.Add("กลุ่มวัตถุดิบ", typeof(String));
                    tableFormList.Columns.Add("ประเภทวัตถุดิบ", typeof(String));
                    tableFormList.Columns.Add("โครงสร้างวัตถุดิบ", typeof(String));


                    tableFormList.Columns.Add("โครงสร้างย่อยวัตถุดิบ", typeof(String));
                    tableFormList.Columns.Add("Mat No", typeof(String));
                    tableFormList.Columns.Add("Mat Name", typeof(String));
                    tableFormList.Columns.Add("Batch", typeof(String));
                    tableFormList.Columns.Add("Supplier", typeof(String));
                    tableFormList.Columns.Add("ประเภทปัญหา", typeof(String));
                    tableFormList.Columns.Add("วันที่ผลิต", typeof(String));
                    tableFormList.Columns.Add("วันที่หมดอายุ", typeof(String));
                    tableFormList.Columns.Add("วันที่พบปัญหา", typeof(String));
                    tableFormList.Columns.Add("วันที่รับเข้า", typeof(String));


                    tableFormList.Columns.Add("จำนวนรับเข้า", typeof(String));
                    tableFormList.Columns.Add("หน่วยรับเข้า", typeof(String));
                    tableFormList.Columns.Add("จำนวนพบปัญหา", typeof(String));
                    tableFormList.Columns.Add("หน่วยพบปัญหา", typeof(String));
                    tableFormList.Columns.Add("จำนวนสุ่ม", typeof(String));
                    tableFormList.Columns.Add("หน่วยสุ่ม", typeof(String));
                    tableFormList.Columns.Add("รายละเอียดเพิ่มเติม", typeof(String));
                    tableFormList.Columns.Add("ผู้ดูแลปัญหา", typeof(String));
                    tableFormList.Columns.Add("PO Number", typeof(String));
                    tableFormList.Columns.Add("TV Number", typeof(String));


                    tableFormList.Columns.Add("ระดับปัญหา", typeof(String));
                    tableFormList.Columns.Add("ประเภทส่งคืน", typeof(String));
                    tableFormList.Columns.Add("จำนวนส่งคืน", typeof(String));
                    tableFormList.Columns.Add("หน่วยส่งคืน", typeof(String));
                    tableFormList.Columns.Add("ความคิดเห็นส่งคืน", typeof(String));
                    tableFormList.Columns.Add("สถานะรายการ", typeof(String));


                    foreach (var row_report in _dtproblem.Boxu0_ProblemDocument)
                    {
                        DataRow addFormListRow = tableFormList.NewRow();

                        //   addFormListRow[0] = row_report.org_name_th.ToString();
                        addFormListRow[0] = row_report.dept_name_th.ToString();
                        addFormListRow[1] = row_report.sec_name_th.ToString();
                        addFormListRow[2] = row_report.emp_name_th.ToString();
                        addFormListRow[3] = row_report.create_date.ToString();
                        addFormListRow[4] = row_report.doc_code.ToString();
                        addFormListRow[5] = row_report.LocName.ToString();
                        addFormListRow[6] = row_report.BuildingName.ToString();
                        addFormListRow[7] = row_report.production_name.ToString();
                        addFormListRow[8] = row_report.group_name.ToString();
                        addFormListRow[9] = row_report.typeproduct_name.ToString();
                        addFormListRow[10] = row_report.typeproduct_name_m1.ToString();


                        addFormListRow[11] = row_report.typeproduct_name_m2.ToString();
                        addFormListRow[12] = row_report.mat_no.ToString();
                        addFormListRow[13] = row_report.matname.ToString();
                        addFormListRow[14] = row_report.batchnumber.ToString();
                        addFormListRow[15] = row_report.sup_name.ToString();
                        addFormListRow[16] = row_report.problem_name.ToString();
                        addFormListRow[17] = row_report.datemodify.ToString();
                        addFormListRow[18] = row_report.dateexp.ToString();
                        addFormListRow[19] = row_report.dateproblem.ToString();
                        addFormListRow[20] = row_report.dategetproduct.ToString();


                        addFormListRow[21] = row_report.qty_get.ToString();
                        addFormListRow[22] = row_report.unit_name.ToString();
                        addFormListRow[23] = row_report.qty_problem.ToString();
                        addFormListRow[24] = row_report.unit_name_problem.ToString();
                        addFormListRow[25] = row_report.qty_random.ToString();
                        addFormListRow[26] = row_report.unit_name_random.ToString();
                        addFormListRow[27] = row_report.detail_remark.ToString();
                        addFormListRow[28] = row_report.admin_name.ToString();
                        addFormListRow[29] = row_report.po_no.ToString();
                        addFormListRow[30] = row_report.tv_no.ToString();


                        addFormListRow[31] = row_report.type_problem_name.ToString();
                        addFormListRow[32] = row_report.rollback_name.ToString();
                        addFormListRow[33] = row_report.qty_rollback.ToString();
                        addFormListRow[34] = row_report.unit_name_rollback.ToString();
                        addFormListRow[35] = row_report.detail_rollback.ToString();
                        addFormListRow[36] = row_report.StatusDoc.ToString();

                        tableFormList.Rows.InsertAt(addFormListRow, count_++);
                    }

                    */

                    WriteExcelWithNPOI(tableFormList, "xls", "report-problem", 0);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- ไม่พบข้อมูล ---');", true);
                    break;
                }
                break;

            case "btnrefresh_report":

                SetDefaultpage(5);
                break;

            case "BtnBack_Report":
                MvMaster.SetActiveView(ViewReport);
                _divMenuLiToViewIndex.Attributes.Remove("class");
                _divMenuLiToViewReport.Attributes.Add("class", "active");

                break;

            case "btnsearch_list":
                ViewState["check_search"] = "1";
                selectindexlist_searchuser(GvIndex, int.Parse(ViewState["Sec_idx"].ToString()), int.Parse(ViewState["EmpIDX"].ToString()), int.Parse(ddlSearchDate_List.SelectedValue), AddStartdate_List.Text, AddEndDate_List.Text, int.Parse(ddltypeproduct_List.SelectedValue), int.Parse(ddlproblem_List.SelectedValue), int.Parse(ddlsupplier_List.SelectedValue), ViewState["return_admin"].ToString(), int.Parse(ddlgroupproduct_List.SelectedValue), int.Parse(ddltypeproduct_list_m1.SelectedValue), int.Parse(ddltypeproduct_list_m2.SelectedValue));

                break;

            case "btnrefresh_list":
                ViewState["check_search"] = "0";
                ddlSearchDate_List.SelectedValue = "0";
                AddStartdate_List.Text = String.Empty;
                AddEndDate_List.Text = String.Empty;
                ddlgroupproduct_List.SelectedValue = "0";
                ddltypeproduct_List.SelectedValue = "0";
                ddltypeproduct_list_m1.SelectedValue = "0";
                ddltypeproduct_list_m2.SelectedValue = "0";
                ddlproblem_List.SelectedValue = "0";
                ddlsupplier_List.SelectedValue = "0";

                // selectindexlist(GvIndex, int.Parse(ViewState["Sec_idx"].ToString()), int.Parse(ViewState["EmpIDX"].ToString()));
                selectindexlist(GvIndex, int.Parse(ViewState["Sec_idx"].ToString()), int.Parse(ViewState["EmpIDX"].ToString()));


                break;

            case "CmdSearchMaster_Product":
                selectmaster_product(GvMaster_Product, txtmaster_searchproduct.Text, int.Parse(ddlmaster_searchgroup.SelectedValue));
                mergeCell(GvMaster_Product);

                break;

            case "btnRefreshMaster_Product":
                txtmaster_searchproduct.Text = String.Empty;
                ddlmaster_searchgroup.SelectedValue = "0";
                selectmaster_product(GvMaster_Product, txtmaster_searchproduct.Text, int.Parse(ddlmaster_searchgroup.SelectedValue));
                mergeCell(GvMaster_Product);
                break;

            case "CmdSearchMaster_Unit":
                selectmaster_unit(GvMaster_Unit, txtmaster_searchunit.Text);
                break;

            case "btnRefreshMaster_Unit":
                txtmaster_searchunit.Text = String.Empty;
                selectmaster_unit(GvMaster_Unit, txtmaster_searchunit.Text);

                break;

            case "CmdSearchMaster_Production":
                selectmaster_place(GvMaster_Place, int.Parse(ddlmaster_searchlocation.SelectedValue), int.Parse(ddlmaster_searchbuilding.SelectedValue), txtmaster_searchproduction.Text);
                mergeCell(GvMaster_Place);
                break;

            case "btnRefreshMaster_Production":
                ddlmaster_searchlocation.SelectedValue = "0";
                ddlmaster_searchbuilding.SelectedValue = "0";
                txtmaster_searchproduction.Text = String.Empty;
                selectmaster_place(GvMaster_Place, int.Parse(ddlmaster_searchlocation.SelectedValue), int.Parse(ddlmaster_searchbuilding.SelectedValue), txtmaster_searchproduction.Text);
                mergeCell(GvMaster_Place);

                break;

            case "CmdSearchMaster_Problem":
                selectmaster_problem(GvMaster_Problem, int.Parse(ddlmaster_searchproblem.SelectedValue), txtmaster_searchtopicproblem.Text, int.Parse(ddlmaster_searchgroupproblem.SelectedValue));
                mergeCell(GvMaster_Problem);

                break;

            case "btnRefreshMaster_Problem":
                ddlmaster_searchgroupproblem.SelectedValue = "0";
                ddlmaster_searchproblem.SelectedValue = "0";
                txtmaster_searchtopicproblem.Text = String.Empty;
                div_searchmaster_problem.Visible = true;
                selectmaster_problem(GvMaster_Problem, int.Parse(ddlmaster_searchproblem.SelectedValue), txtmaster_searchtopicproblem.Text, int.Parse(ddlmaster_searchgroupproblem.SelectedValue));
                mergeCell(GvMaster_Problem);

                break;

            case "CmdSearchMaster_Supplier":
                selectmaster_supplier(GvMaster_Supplier, txtmaster_searchsupplier.Text, txtmaster_searchemail.Text);

                break;
            case "btnRefreshMaster_Supplier":
                txtmaster_searchsupplier.Text = String.Empty;
                txtmaster_searchemail.Text = String.Empty;
                selectmaster_supplier(GvMaster_Supplier, txtmaster_searchsupplier.Text, txtmaster_searchemail.Text);
                div_searchmaster_supplier.Visible = true;

                break;

            case "CmdSearchMaster_Mat":
                selectmaster_mat(GvMaster_Mat, txtmaster_searchmatno.Text, txtmaster_searchmatname.Text);
                break;

            case "btnRefreshMaster_Mat":
                txtmaster_searchmatno.Text = String.Empty;
                txtmaster_searchmatname.Text = String.Empty;
                div_searchmaster_mat.Visible = true;
                selectmaster_mat(GvMaster_Mat, txtmaster_searchmatno.Text, txtmaster_searchmatname.Text);

                break;

            case "CmdSearchMaster_MatRisk":
                selectmaster_materialrisk(GvMaster_MatRisk, txtmaster_searchmatrisk.Text);

                break;

            case "btnRefreshMaster_MatRisk":
                div_searchmaster_matrisk.Visible = true;
                txtmaster_searchmatrisk.Text = String.Empty;
                selectmaster_materialrisk(GvMaster_MatRisk, txtmaster_searchmatrisk.Text);

                break;

            case "CmdSeeversion_group":

                string m0_group_tpidx = e.CommandArgument.ToString();
                ViewState["mastergroup_approve"] = m0_group_tpidx;
                fvapprove_group.ChangeMode(FormViewMode.Edit);

                _dtproblem = new data_qmr_problem();

                _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
                U0_ProblemDocument _select_group_history = new U0_ProblemDocument();

                _select_group_history.condition = 25;
                _select_group_history.m0_group_tpidx = int.Parse(m0_group_tpidx);
                _dtproblem.Boxu0_ProblemDocument[0] = _select_group_history;
                //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

                _dtproblem = callServicePostQMR_Problem(_urlGetMaster, _dtproblem);

                setFormViewData(fvapprove_group, _dtproblem.Boxm0_ProblemDocument);

                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                Div_Log_Group.Visible = true;
                div_index_mastergroup.Visible = false;

                break;

            case "btnbackapprove_group":
                SetDefaultpage(13);

                break;

            case "btnapprove_group":
                string approve_master_group = e.CommandArgument.ToString();
                TextBox txtgroup_old = ((TextBox)fvapprove_group.FindControl("txtgroup_old"));
                TextBox txtgroup_new = ((TextBox)fvapprove_group.FindControl("txtgroup_new"));
                TextBox txtgroup_statusold = ((TextBox)fvapprove_group.FindControl("txtgroup_statusold"));
                TextBox txtgroup_statusnew = ((TextBox)fvapprove_group.FindControl("txtgroup_statusnew"));

                _dtproblem = new data_qmr_problem();

                _dtproblem.Boxm0_ProblemDocument = new M0_ProblemDocument[1];
                M0_ProblemDocument _insertmaster = new M0_ProblemDocument();

                if (approve_master_group == "1")
                {
                    _insertmaster.group_name = txtgroup_new.Text;
                    _insertmaster.status = int.Parse(txtgroup_statusnew.Text);
                }
                else if (approve_master_group == "2")
                {
                    _insertmaster.group_name = txtgroup_old.Text;
                    _insertmaster.status = int.Parse(txtgroup_statusold.Text);
                }
                _insertmaster.m0_group_tpidx = int.Parse(ViewState["mastergroup_approve"].ToString());

                _insertmaster.status_approve = int.Parse(approve_master_group);
                _dtproblem.Boxm0_ProblemDocument[0] = _insertmaster;


                _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
                U0_ProblemDocument _insertsytem = new U0_ProblemDocument();

                _insertsytem.condition = 12;
                _insertsytem.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

                _dtproblem.Boxu0_ProblemDocument[0] = _insertsytem;

                //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

                _dtproblem = callServicePostQMR_Problem(_urlInsertMaster, _dtproblem);


                SetDefaultpage(13);

                break;

            case "CmdSeeversion_matrisk":
                string m0_mridx_approve = e.CommandArgument.ToString();
                ViewState["mastermatrisk_approve"] = m0_mridx_approve;
                fvapprove_matrisk.ChangeMode(FormViewMode.Edit);

                _dtproblem = new data_qmr_problem();

                _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
                U0_ProblemDocument _select_matrisk_history = new U0_ProblemDocument();

                _select_matrisk_history.condition = 27;
                _select_matrisk_history.m0_mridx = int.Parse(m0_mridx_approve);
                _dtproblem.Boxu0_ProblemDocument[0] = _select_matrisk_history;
                //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

                _dtproblem = callServicePostQMR_Problem(_urlGetMaster, _dtproblem);

                setFormViewData(fvapprove_matrisk, _dtproblem.Boxm0_ProblemDocument);

                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                Div_Log_Matrisk.Visible = true;
                div_index_mastermatrisk.Visible = false;
                break;

            case "btnapprove_matrisk":
                string approve_master_matrisk = e.CommandArgument.ToString();
                TextBox txtmatrisk_old = ((TextBox)fvapprove_matrisk.FindControl("txtmatrisk_old"));
                TextBox txtmatrisk_new = ((TextBox)fvapprove_matrisk.FindControl("txtmatrisk_new"));
                TextBox txtmatrisk_statusold = ((TextBox)fvapprove_matrisk.FindControl("txtmatrisk_statusold"));
                TextBox txtmatrisk_statusnew = ((TextBox)fvapprove_matrisk.FindControl("txtmatrisk_statusnew"));

                _dtproblem = new data_qmr_problem();

                _dtproblem.Boxm0_ProblemDocument = new M0_ProblemDocument[1];
                M0_ProblemDocument _insertmaster_matrisk = new M0_ProblemDocument();

                if (approve_master_matrisk == "1")
                {
                    _insertmaster_matrisk.material_risk = txtmatrisk_new.Text;
                    _insertmaster_matrisk.status = int.Parse(txtmatrisk_statusnew.Text);
                }
                else if (approve_master_matrisk == "2")
                {
                    _insertmaster_matrisk.material_risk = txtmatrisk_old.Text;
                    _insertmaster_matrisk.status = int.Parse(txtmatrisk_statusold.Text);
                }
                _insertmaster_matrisk.m0_mridx = int.Parse(ViewState["mastermatrisk_approve"].ToString());

                _insertmaster_matrisk.status_approve = int.Parse(approve_master_matrisk);
                _dtproblem.Boxm0_ProblemDocument[0] = _insertmaster_matrisk;


                _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
                U0_ProblemDocument _insertsytem_matrisk = new U0_ProblemDocument();

                _insertsytem_matrisk.condition = 13;
                _insertsytem_matrisk.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

                _dtproblem.Boxu0_ProblemDocument[0] = _insertsytem_matrisk;

                //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

                _dtproblem = callServicePostQMR_Problem(_urlInsertMaster, _dtproblem);


                SetDefaultpage(12);
                break;

            case "btnbackapprove_matrisk":
                SetDefaultpage(12);
                break;

            case "CmdSeeversion_problem":
                string m0_pridx_approve = e.CommandArgument.ToString();
                ViewState["masterproblem_approve"] = m0_pridx_approve;
                fvapprove_problem.ChangeMode(FormViewMode.Edit);

                _dtproblem = new data_qmr_problem();

                _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
                U0_ProblemDocument _select_problem_history = new U0_ProblemDocument();

                _select_problem_history.condition = 28;
                _select_problem_history.m0_pridx = int.Parse(m0_pridx_approve);
                _dtproblem.Boxu0_ProblemDocument[0] = _select_problem_history;
                //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

                _dtproblem = callServicePostQMR_Problem(_urlGetMaster, _dtproblem);
                setFormViewData(fvapprove_problem, _dtproblem.Boxm0_ProblemDocument);

                Div_Log_Problem.Visible = true;
                div_index_masterproblem.Visible = false;
                break;

            case "btnbackapprove_problem":
                SetDefaultpage(8);
                break;

            case "btnapprove_problem":
                string approve_master_problem = e.CommandArgument.ToString();

                TextBox txtproblem_m0_tpidx = ((TextBox)fvapprove_problem.FindControl("txtproblem_m0_tpidx"));
                TextBox txtproblem_m0_tpidx_new = ((TextBox)fvapprove_problem.FindControl("txtproblem_m0_tpidx_new"));
                TextBox txtproblem_name_old = ((TextBox)fvapprove_problem.FindControl("txtproblem_name_old"));
                TextBox txtproblem_name_new = ((TextBox)fvapprove_problem.FindControl("txtproblem_name_new"));
                TextBox txtproblem_statusold = ((TextBox)fvapprove_problem.FindControl("txtproblem_statusold"));
                TextBox txtproblem_statusnew = ((TextBox)fvapprove_problem.FindControl("txtproblem_statusnew"));

                _dtproblem = new data_qmr_problem();

                _dtproblem.Boxm0_ProblemDocument = new M0_ProblemDocument[1];
                M0_ProblemDocument _insertmaster_problem = new M0_ProblemDocument();

                if (approve_master_problem == "1")
                {
                    _insertmaster_problem.m0_tpidx = int.Parse(txtproblem_m0_tpidx_new.Text);
                    _insertmaster_problem.problem_name = txtproblem_name_new.Text;
                    _insertmaster_problem.status = int.Parse(txtproblem_statusnew.Text);
                }
                else if (approve_master_problem == "2")
                {
                    _insertmaster_problem.m0_tpidx = int.Parse(txtproblem_m0_tpidx.Text);
                    _insertmaster_problem.problem_name = txtproblem_name_old.Text;
                    _insertmaster_problem.status = int.Parse(txtproblem_statusold.Text);
                }

                _insertmaster_problem.m0_pridx = int.Parse(ViewState["masterproblem_approve"].ToString());
                _insertmaster_problem.status_approve = int.Parse(approve_master_problem);
                _dtproblem.Boxm0_ProblemDocument[0] = _insertmaster_problem;


                _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
                U0_ProblemDocument _insertsytem_problem = new U0_ProblemDocument();

                _insertsytem_problem.condition = 14;
                _insertsytem_problem.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

                _dtproblem.Boxu0_ProblemDocument[0] = _insertsytem_problem;

                //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

                _dtproblem = callServicePostQMR_Problem(_urlInsertMaster, _dtproblem);


                SetDefaultpage(8);
                break;
        }
    }

    #endregion
}