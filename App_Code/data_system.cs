﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Serialization;

/// <summary>
/// Summary description for data_system
/// </summary>
/// 
[Serializable]
[XmlRoot("data_system")]
public class data_system
{
    public int return_code { get; set; }
    public string return_msg { get; set; }

    [XmlElement("menusystem_list")]
    public menusystem_detail[] menusystem_list { get; set; }

    [XmlElement("empmenusystem_list")]
    public empmenusystem_detail[] empmenusystem_list { get; set; }


}

[Serializable]
public class menusystem_detail
{
    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("menu_idx")]
    public int menu_idx { get; set; }

    [XmlElement("menu_name_th")]
    public string menu_name_th { get; set; }

    [XmlElement("menu_name_thlist")]
    public string menu_name_thlist { get; set; }

    [XmlElement("menu_name_thtest")]
    public string menu_name_thtest { get; set; }

    [XmlElement("menu_name_en")]
    public string menu_name_en { get; set; }

    [XmlElement("menu_url")]
    public string menu_url { get; set; }

    [XmlElement("menu_relation")]
    public int menu_relation { get; set; }

    [XmlElement("menu_order")]
    public int menu_order { get; set; }

    [XmlElement("menu_icon")]
    public string menu_icon { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("menu_status")]
    public int menu_status { get; set; }

    [XmlElement("menu_level")]
    public int menu_level { get; set; }

    [XmlElement("system_idx")]
    public int system_idx { get; set; }

    [XmlElement("system_name_th")]
    public string system_name_th { get; set; }

    [XmlElement("MenuIDX1")]
    public int MenuIDX1 { get; set; }


}

[Serializable]
public class empmenusystem_detail
{
    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }


    // employee organization profile
    [XmlElement("org_idx")]
    public int org_idx { get; set; }
    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }
    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }
    [XmlElement("rpos_idx")]
    public int rpos_idx { get; set; }

    [XmlElement("org_name_en")]
    public string org_name_en { get; set; }
    [XmlElement("dept_name_en")]
    public string dept_name_en { get; set; }
    [XmlElement("sec_name_en")]
    public string sec_name_en { get; set; }
    [XmlElement("pos_name_en")]
    public string pos_name_en { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }
    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }
    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }
    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }

   


}




