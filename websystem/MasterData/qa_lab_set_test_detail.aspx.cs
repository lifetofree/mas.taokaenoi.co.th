﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class websystem_MasterData_qa_lab_set_test_detail : System.Web.UI.Page
{
    #region initial function/data
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();
    data_qa _data_qa = new data_qa();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];

    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    //-- employee --//

    //-- master data setname & form detail --//
    static string _urlQaGetTestDetail = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetTestDetail"];
    static string _urlQaGetR0SetTestDetail = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetSetTestDetail"];
    static string _urlQaSetR0SetTestDetail = _serviceUrl + ConfigurationManager.AppSettings["urlQaSetR0SetTestDetail"];
    static string _urlQaDelSetTestDetail = _serviceUrl + ConfigurationManager.AppSettings["urlQaDelSetTestDetail"];


    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    bool _flag_qmr = false;

    // rdept permission
    // int[] rdept_qmr = { 20 }; //QMR:26
    #endregion initial function/data


    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
        //check permission
        ////foreach (int item in rdept_qmr)
        ////{
        ////    if (_dataEmployee.employee_list[0].rdept_idx == item)
        ////    {
        ////        _flag_qmr = true;
        ////        break;
        ////    }
        ////}
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            initPage();
            selectR0SetTestDetail();
        }
        linkBtnTrigger(btnCreateForm);
        createSetTestList();
    }



    #region event command


    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();


        switch (cmdName)
        {

            case "cmdCreateTest":
                switch (cmdArg)
                {
                    case "0":
                        setFormData(fvformInsert, FormViewMode.Insert, null);
                        getM0TestDetailList((DropDownList)fvformInsert.FindControl("ddlTestType"), "0");
                        createSetTestList();
                        btnCreateForm.Visible = false;
                        btnCancel.Visible = true;
                        break;

                }

                break;

            case "cmdCancel":
                switch (cmdArg)
                {
                    case "0":
                        setActiveView("pageGenaral", 0);
                        GridView gvCreateSetTest = (GridView)fvformInsert.FindControl("gvCreateSetTest");
                        setFormData(fvformInsert, FormViewMode.ReadOnly, null);
                        ViewState["setTestList"] = null;
                        setGridData(gvCreateSetTest, ViewState["setTestList"]);
                        btnCreateForm.Visible = true;
                        btnCancel.Visible = false;
                        SETFOCUS.Focus();

                        break;

                }

                break;

            case "cmdSave":
                switch (cmdArg)
                {
                    case "0":
                        TextBox tbSetTestName = (TextBox)fvformInsert.FindControl("tbSetTestName");
                       
                        DropDownList ddlFormStatus = (DropDownList)fvformInsert.FindControl("ddlFormStatus");
                        DropDownList ddlTestType = (DropDownList)fvformInsert.FindControl("ddlTestType");
                        Panel panelActionTime1 = (Panel)fvformInsert.FindControl("panelActionTime");
                        TextBox txtActionTime = (TextBox)fvformInsert.FindControl("txtActionTime");


                        _data_qa.qa_r0_set_test_detail_list = new qa_r0_set_test_detail[1];
                        qa_r0_set_test_detail _r0form = new qa_r0_set_test_detail();
                        _r0form.cemp_idx = _emp_idx;
                        _r0form.set_test_detail_name = tbSetTestName.Text;
                        _r0form.test_detail_time = int.Parse(txtActionTime.Text);

                        //_r0form.r0_form_create_status = int.Parse(ddlFormStatus.SelectedValue);

                        if (ViewState["setTestList"] != null)
                        {
                            var _datasetCreateForm = (DataSet)ViewState["setTestList"];
                            var _addFormList = new qa_r0_set_test_dataset_detail[_datasetCreateForm.Tables[0].Rows.Count];
                            int i = 0;

                            foreach (DataRow datarowForm in _datasetCreateForm.Tables[0].Rows)
                            {
                                _addFormList[i] = new qa_r0_set_test_dataset_detail();
                                _addFormList[i].test_detail_idx = int.Parse(datarowForm["m0TestType"].ToString());
                                _addFormList[i].cemp_idx = _emp_idx;
                                _addFormList[i].test_detail_time = int.Parse(txtActionTime.Text);
                                _addFormList[i].test_detail_name = (datarowForm["nameTestDetail"].ToString());

                                i++;


                            }
                            if (_datasetCreateForm.Tables[0].Rows.Count == 0)

                            {
                                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาเพิ่มข้อมูลก่อนทำการบันทึกผลค่ะ !!!');", true);
                            }

                            else
                            {

                                _data_qa.qa_r0_set_test_dataset_list = _addFormList;
                                _data_qa.qa_r0_set_test_detail_list[0] = _r0form;

                                //  litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_qa));
                                _data_qa = callServicePostMasterQA(_urlQaSetR0SetTestDetail, _data_qa);
                                if (_data_qa.return_code != 0)
                                {

                                }
                                else
                                {

                                }
                                setFormData(fvformInsert, FormViewMode.Insert, null);
                                GridView gvCreateSetTest = (GridView)fvformInsert.FindControl("gvCreateSetTest");
                                getM0TestDetailList((DropDownList)fvformInsert.FindControl("ddlTestType"), "0");
                                ViewState["setTestList"] = null;
                                setGridData(gvCreateSetTest, ViewState["setTestList"]);
                                tbSetTestName.Text = String.Empty;
                                txtActionTime.Text = String.Empty;

                            }
                        }
                        else
                        {

                          
                        }
                       
                        selectR0SetTestDetail();
                        break;

                }

                break;

            case "cmdDelete":
                int _formIDX = int.Parse(cmdArg);
                _data_qa.qa_r0_set_test_detail_list = new qa_r0_set_test_detail[1];
                qa_r0_set_test_detail _delform = new qa_r0_set_test_detail();
                _delform.set_test_detail_idx = _formIDX;
                _data_qa.qa_r0_set_test_detail_list[0] = _delform;
                _data_qa = callServicePostMasterQA(_urlQaDelSetTestDetail, _data_qa);

                if (_data_qa.return_code == 0)
                {
                    // litDebug.Text = "success";
                }
                else
                {
                    // litDebug.Text = "not success";
                }

                selectR0SetTestDetail();

                break;

            case "cmdAddSetTest":

                DropDownList _testType = (DropDownList)fvformInsert.FindControl("ddlTestType");
                GridView _gvCreateSetTest = (GridView)fvformInsert.FindControl("gvCreateSetTest");
                Panel panelFormButton = (Panel)fvformInsert.FindControl("panelFormButton");
                Panel panelActionTime = (Panel)fvformInsert.FindControl("panelActionTime");
                
                var _dataset_setTest = (DataSet)ViewState["setTestList"];
                var _datarow_setTest = _dataset_setTest.Tables[0].NewRow();

                //ค่าที่เก็บที่จะเอามาเช็ค
                ViewState["m0SetTest"] = (_testType.SelectedValue);
                int _numrow = _dataset_setTest.Tables[0].Rows.Count; //จำนวนแถว
                if (_numrow > 0)
                {
                    foreach (DataRow check in _dataset_setTest.Tables[0].Rows)
                    {
                        ViewState["_check_testDetail"] = check["m0TestType"];
                        //กรณิประเภทการตรวจตรงกัน
                        if (ViewState["m0SetTest"].ToString() == ViewState["_check_testDetail"].ToString())
                        {

                            ViewState["CheckDataset"] = "2";
                            break;

                        }
                        else
                        {
                            //กรณิประเภทการตรวจไม่ตรงกัน
                            ViewState["CheckDataset"] = "1";
                        }

                    }
                    //ถ้าข้อมูลไม่ซ้ำ จะเข้าการทำงานนี้
                    if (ViewState["CheckDataset"].ToString() == "1")
                    {
                        _datarow_setTest["m0TestType"] = (_testType.SelectedValue);
                        _datarow_setTest["nameTestDetail"] = (_testType.SelectedItem);
                        //_datarow_setTest["m0FormDetailIDX"] = (_topicForm.SelectedValue);
   

                        _dataset_setTest.Tables[0].Rows.Add(_datarow_setTest);
                        ViewState["setTestList"] = _dataset_setTest;

                        _gvCreateSetTest.DataSource = _dataset_setTest.Tables[0];
                        _gvCreateSetTest.DataBind();

                        panelFormButton.Visible = true;
                    }
                    else if (ViewState["CheckDataset"].ToString() == "2")
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ขออภัยค่ะ ท่านมีการเพิ่มข้อมูลหัวข้อนี้แล้วค่ะ');", true);

                    }
                }
                else
                {
                    _datarow_setTest["m0TestType"] = (_testType.SelectedValue);
                    _datarow_setTest["nameTestDetail"] = (_testType.SelectedItem);
                    //_datarow_setTest["m0FormDetailIDX"] = (_topicForm.SelectedValue);
                  

                    _dataset_setTest.Tables[0].Rows.Add(_datarow_setTest);
                    ViewState["setTestList"] = _dataset_setTest;

                    _gvCreateSetTest.DataSource = _dataset_setTest.Tables[0];
                    _gvCreateSetTest.DataBind();
                    panelFormButton.Visible = true;
                    panelActionTime.Visible = true;
                    panelActionTime.Focus();
                }


                break;

        }
    }
    #endregion event command

    #region data set
    protected void createSetTestList()
    {
        var _createSetTestList = new DataSet();
        _createSetTestList.Tables.Add("datacreateSetTestList");
        _createSetTestList.Tables[0].Columns.Add("setTestIDX", typeof(int));
        _createSetTestList.Tables[0].Columns.Add("m0TestType", typeof(int));
        _createSetTestList.Tables[0].Columns.Add("nameTestDetail", typeof(String));


        if (ViewState["setTestList"] == null)
        {
            ViewState["setTestList"] = _createSetTestList;
        }
    }

    #endregion

    #region dropdown list
    protected void getM0TestDetailList(DropDownList ddlName, string _test_detail_idx)
    {
        data_qa _dataqa = new data_qa();
        _dataqa.qa_m0_test_detail_list = new qa_m0_test_detail[1];
        qa_m0_test_detail _m0test = new qa_m0_test_detail();
        _m0test.condition = 1;
        _dataqa.qa_m0_test_detail_list[0] = _m0test;

        _dataqa = callServicePostMasterQA(_urlQaGetTestDetail, _dataqa);
        setDdlData(ddlName, _dataqa.qa_m0_test_detail_list, "test_detail_name", "test_detail_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกประเภทการตรวจ ---", "0"));
        ddlName.SelectedValue = _test_detail_idx;
    }

    #endregion

    #region grid view
    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var gvName = (GridView)sender;

        switch (gvName.ID)
        {
            case "gvMaster":
                gvMaster.PageIndex = e.NewPageIndex;
                gvMaster.DataBind();
                selectR0SetTestDetail();
                SETFOCUS.Focus();
                break;

        }

    }
    protected void gvRowDeleted(object sender, GridViewDeleteEventArgs e)
    {
        var gvName = (GridView)sender;

        switch (gvName.ID)
        {
            case "gvCreateSetTest":

                GridView gvCreateSetTest = (GridView)fvformInsert.FindControl("gvCreateSetTest");
                var DeleteFormList = (DataSet)ViewState["setTestList"];
                var drDriving = DeleteFormList.Tables[0].Rows;

                drDriving.RemoveAt(e.RowIndex);

                ViewState["setTestList"] = DeleteFormList;
                gvCreateSetTest.EditIndex = -1;

                setGridData(gvCreateSetTest, ViewState["setTestList"]);

                break;
        }

    }
    protected void gvRowEditing(object sender, GridViewEditEventArgs e)
    {

    }
    protected void gvRowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }
    protected void gvRowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {

    }
    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var gvName = (GridView)sender;
        switch (gvName.ID)
        {
            case "gvCreateFormList":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                }

                break;

            case "gvMaster":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    Label lbstatus = (Label)e.Row.FindControl("lbstatus");
                    Label Formidx = (Label)e.Row.FindControl("lbFormidx");
                    Label status_offline = (Label)e.Row.FindControl("status_offline");
                    Label status_online = (Label)e.Row.FindControl("status_online");

                    ViewState["status_subForm"] = lbstatus.Text;

                    if (ViewState["status_subForm"].ToString() == "0")
                    {
                        status_offline.Visible = true;
                    }
                    else if (ViewState["status_subForm"].ToString() == "1")
                    {
                        status_online.Visible = true;
                    }

                    GridView gvTopic = (GridView)e.Row.Cells[2].FindControl("gvTopic");
                    Label lblSetTestIDX = (Label)e.Row.FindControl("lblSetTestIDX");
                   
                    data_qa _dataTopic = new data_qa();
                    _dataTopic.qa_m0_test_detail_list = new qa_m0_test_detail[1];
                    qa_m0_test_detail _topic = new qa_m0_test_detail();
                    //_topic.condition = 0;
                    _topic.test_detail_idx = int.Parse(Formidx.Text);
                    _dataTopic.qa_m0_test_detail_list[0] = _topic;

                    _dataTopic = callServicePostMasterQA(_urlQaGetTestDetail, _dataTopic);
                    if (_dataTopic.return_code == 0)
                    {
                        setGridData(gvTopic, _dataTopic.qa_m0_test_detail_list);
                    }

                }


                break;
        }

    }
    #endregion

    #region select 
    protected void selectR0SetTestDetail()
    {
        _data_qa.qa_r0_set_test_detail_list = new qa_r0_set_test_detail[1];
        qa_r0_set_test_detail _selectSet = new qa_r0_set_test_detail();
       // _selectSet.condition = 1;
        _data_qa.qa_r0_set_test_detail_list[0] = _selectSet;
        _data_qa = callServicePostMasterQA(_urlQaGetR0SetTestDetail, _data_qa);

        setGridData(gvMaster, _data_qa.qa_r0_set_test_detail_list);

    }

    #endregion

    #region reuse
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void initPage()
    {

        //clearSession();
        //clearViewState();

        setActiveView("pageGenaral", 0);
        setFormData(fvformInsert, FormViewMode.ReadOnly, null);
        //  selectFormDetail();
    }

    protected void setActiveView(string activeTab, int uidx)
    {
        mvMaster.SetActiveView((View)mvMaster.FindControl(activeTab));

        switch (activeTab)
        {
            case "pageGenaral":

                break;

        }

    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }

    protected data_qa callServicePostMasterQA(string _cmdUrl, data_qa _data_qa)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_qa);
        // litDebug.Text = _localJson;

        // call services
        // _localJson = _funcTool.callServicePost(_cmdUrl + _localJson);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_qa = (data_qa)_funcTool.convertJsonToObject(typeof(data_qa), _localJson);

        return _data_qa;
    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger1 = new PostBackTrigger();
        trigger1.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger1);
    }

    #endregion

}