﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_mis_m0_tel : System.Web.UI.Page
{
    #region Connect

    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();
    data_employee _dtEmployee = new data_employee();


    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";
    string _localJson = "";

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string urlSelectLocate_Tel = _serviceUrl + ConfigurationManager.AppSettings["urlSelectLocate_Tel"];
    static string urlSelectTel = _serviceUrl + ConfigurationManager.AppSettings["urlSelectTel"];
    static string urlInsertMainTel = _serviceUrl + ConfigurationManager.AppSettings["urlInsertMainTel"];
    static string urlUpdateMainTel = _serviceUrl + ConfigurationManager.AppSettings["urlUpdateMainTel"];
    static string urlDeleteMainTel = _serviceUrl + ConfigurationManager.AppSettings["urlDeleteMainTel"];



    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            MvMaster.SetActiveView(ViewIndex);
            ViewState["EmpIDX"] = int.Parse(Session["emp_idx"].ToString());
            select_empIdx_present();
            SelectMasterList();

        }


    }

    protected void select_empIdx_present()
    {

        _dtEmployee = callServiceEmpProfile(urlGetMyProfile + ViewState["EmpIDX"].ToString());


        ViewState["rdept_name"] = _dtEmployee.employee_list[0].dept_name_th;
        ViewState["rdept_idx"] = _dtEmployee.employee_list[0].rdept_idx;
        ViewState["FullName"] = _dtEmployee.employee_list[0].emp_name_th;
        ViewState["Org_name"] = _dtEmployee.employee_list[0].org_name_th;
        ViewState["Org_idx"] = _dtEmployee.employee_list[0].org_idx;

        ViewState["EmpCode"] = _dtEmployee.employee_list[0].emp_code;
        ViewState["Positname"] = _dtEmployee.employee_list[0].pos_name_th;
        ViewState["Pos_idx"] = _dtEmployee.employee_list[0].rpos_idx;
        ViewState["Email"] = _dtEmployee.employee_list[0].emp_email;
        ViewState["Tel"] = _dtEmployee.employee_list[0].emp_mobile_no;
        ViewState["Secname"] = _dtEmployee.employee_list[0].sec_name_th;
        ViewState["Sec_idx"] = _dtEmployee.employee_list[0].rsec_idx;
        ViewState["CostIDX"] = _dtEmployee.employee_list[0].costcenter_idx;

    }

    protected void SelectLocate(DropDownList ddlName)
    {
        _dtEmployee = new data_employee();
        _dtEmployee.employee_list = new employee_detail[1];
        employee_detail _dtemp = new employee_detail();

        _dtEmployee.employee_list[0] = _dtemp;

        _dtEmployee = callServicePostEmp(urlSelectLocate_Tel, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.employee_list, "LocName", "LocIDX");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกสถานที่....", "0"));

    }


    protected void SelectMasterList()
    {
        _dtEmployee = new data_employee();
        _dtEmployee.BoxTel_list = new Tel_List[1];
        Tel_List _dtEmp = new Tel_List();

        _dtEmployee.BoxTel_list[0] = _dtEmp;
        // txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtEmployee));

        _dtEmployee = callServicePostEmp(urlSelectTel, _dtEmployee);
        setGridData(GvMaster, _dtEmployee.BoxTel_list);
    }

    protected void SetDefaultAdd()
    {
        txttel.Text = String.Empty;
        SelectLocate(ddllocname);
        ddStatusadd.SelectedValue = "1";
    }

    protected void InsertData()
    {
        _dtEmployee = new data_employee();
        _dtEmployee.BoxTel_list = new Tel_List[1];
        Tel_List _dtEmp = new Tel_List();

        _dtEmp.LocIDX = int.Parse(ddllocname.SelectedValue);
        _dtEmp.m0_tel = txttel.Text;
        _dtEmp.m0status = int.Parse(ddStatusadd.SelectedValue);
        _dtEmp.CEmpIDX = int.Parse(Session["emp_idx"].ToString());
        _dtEmployee.BoxTel_list[0] = _dtEmp;
        // txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtEmployee));

        _dtEmployee = callServicePostEmp(urlInsertMainTel, _dtEmployee);
    }

    protected void Update_Master_List()
    {
        _dtEmployee = new data_employee();
        _dtEmployee.BoxTel_list = new Tel_List[1];
        Tel_List _dtEmp = new Tel_List();

        _dtEmp.LocIDX = int.Parse(ViewState["ddllocname_edit"].ToString());
        _dtEmp.m0_tel = ViewState["txtlocate_edit"].ToString();
        _dtEmp.m0status = int.Parse(ViewState["StatusUpdate_Update"].ToString());
        _dtEmp.m0telidx = int.Parse(ViewState["m0telidx"].ToString());
        _dtEmp.CEmpIDX = int.Parse(Session["emp_idx"].ToString());
        _dtEmployee.BoxTel_list[0] = _dtEmp;
        // txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtEmployee));

        _dtEmployee = callServicePostEmp(urlUpdateMainTel, _dtEmployee);


    }

    protected void Delete_Master_List()
    {
        _dtEmployee = new data_employee();
        _dtEmployee.BoxTel_list = new Tel_List[1];
        Tel_List _dtEmp = new Tel_List();

        _dtEmp.m0telidx = int.Parse(ViewState["m0telidx"].ToString());
        _dtEmp.CEmpIDX = int.Parse(Session["emp_idx"].ToString());
        _dtEmployee.BoxTel_list[0] = _dtEmp;
        // txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtEmployee));

        _dtEmployee = callServicePostEmp(urlDeleteMainTel, _dtEmployee);
    }

    #region Call Services
    protected data_employee callServiceEmpProfile(string _cmdUrl)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }

    protected data_employee callServicePostEmp(string _cmdUrl, data_employee _dtEmployee)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);


        return _dtEmployee;
    }

    #endregion

    #region reuse
    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected string getStatus(int status)
    {
        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    #endregion

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster.EditIndex == e.Row.RowIndex) //to overlook header row
                    {

                        var ddllocname_edit = (DropDownList)e.Row.FindControl("ddllocname_edit");
                        var lblLocIDX = (TextBox)e.Row.FindControl("lblLocIDX");

                        SelectLocate(ddllocname_edit);
                        ddllocname_edit.SelectedValue = lblLocIDX.Text;

                    }

                }


                break;


        }

    }

    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();

                SelectMasterList();

                break;


        }
    }

    #endregion

    #region GvRowEditing

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.EditIndex = e.NewEditIndex;
                SelectMasterList();
                btnshow.Visible = false;
                break;

        }
    }

    #endregion

    #region GvRowCancelingEdit

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":
                GvMaster.EditIndex = -1;
                SelectMasterList();
                btnshow.Visible = true;
                break;

        }
    }

    #endregion

    #region GvRowUpdating

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                int m0telidx = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var ddllocname_edit = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddllocname_edit");
                var txtlocate_edit = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtlocate_edit");
                var StatusUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddStatusUpdate");

                GvMaster.EditIndex = -1;

                ViewState["m0telidx"] = m0telidx;
                ViewState["ddllocname_edit"] = ddllocname_edit.SelectedValue;
                ViewState["txtlocate_edit"] = txtlocate_edit.Text;
                ViewState["StatusUpdate_Update"] = StatusUpdate.SelectedValue;

                Update_Master_List();
                SelectMasterList();

                break;
        }
    }

    #endregion


    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {

            case "CmdAdd":
                btnshow.Visible = false;
                Panel_Add.Visible = true;
                SetDefaultAdd();
                break;

            case "btnCancel":
                btnshow.Visible = true;
                Panel_Add.Visible = false;
                break;

            case "btnAdd":
                InsertData();
                Panel_Add.Visible = false;
                btnshow.Visible = true;
                SelectMasterList();
                break;
            case "CmdDel":
                int m0telidx = int.Parse(cmdArg);
                ViewState["m0telidx"] = m0telidx;
                Delete_Master_List();
                SelectMasterList();

                break;
        }



    }
    #endregion
}