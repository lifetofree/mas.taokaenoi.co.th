﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="epn_certificate.aspx.cs" Inherits="websystem_epn_certificate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    <asp:Literal ID="Literal1" runat="server"></asp:Literal>

    <%-- <style type="text/css">
        .bg-system {
            background-image: url('../../images/qmr-problem/bg1.png');
            background-size: 100% 100%;
            /*width: 50%;*/
            height: 100%;
            text-align: center;
        }
    </style>--%>

    <style type="text/css">
        .bg-big {
            background-image: url('../../images/EPN/Header_MAS_EPN_Banner.png');
            background-size: 100% 100%;
            /*width: 50%;*/
            height: 100%;
            text-align: left;
        }
    </style>

    <%-- <style type="text/css">
        .bg-detail1 {
            background-image: url('../../images/qmr-problem/bg_detail1.png');
            background-size: 100% 100%;
            /*width: 50%;*/
            height: 50%;
            text-align: center;
        }
    </style>--%>


    <style type="text/css">
        .bg-color-active {
            background-color: #9999FF;
        }
    </style>

    <asp:Literal ID="txt" runat="server"></asp:Literal>
    <asp:HyperLink ID="setOntop" runat="server"></asp:HyperLink>
    <%-- ValidationGroup="Save" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')"  --%>
    <%--   <div class="form-group" runat="server">
                                        <div class="col-sm-2 col-sm-offset-10">
                                            <asp:LinkButton ID="btnAdddata" CssClass="btn btn-success btn-sm" runat="server" CommandName="CmdInsertx" OnCommand="btnCommand"  title="Save"><i class="fa fa-save"></i></asp:LinkButton>
                                            <asp:LinkButton ID="AddCancel" CssClass="btn btn-default  btn-sm" runat="server" CommandName="BtnBack" OnCommand="btnCommand" title="Cancel" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-times"></i></asp:LinkButton>
                                        </div>
                                    </div>--%>
    <%-- < navbar >--%>


    <%-- <asp:DropDownList ID="DropDownList1" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="0" Text="กรุณาเลือกประเภทการสร้าง" />
                                                        <asp:ListItem Value="1" Text="สร้างเอง" />
                                                        <asp:ListItem Value="2" Text="Import File" />
                                                    </asp:DropDownList>

    <asp:Panel ID="Panel2" runat="server">

        xxxxx
    </asp:Panel>--%>

    <%--<asp:Button ID="Button1" runat="server" Text="Button" />--%>


    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" style="color: black;">Menu</a>
            </div>
            <div class="collapse navbar-collapse p-l-10" style="overflow-x: hidden; background-color: #9999FF" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-left" runat="server">


                    <li id="_divMenuBtnMainPage" runat="server">
                        <asp:LinkButton ID="_divMenuBtnMain" runat="server"
                            CommandName="_divMenuBtnMain" ForeColor="black"
                            OnCommand="btnCommand" Text="รายการ" />
                    </li>

                    <li id="_divMenuLiToViewAdd" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivAdd" runat="server"
                            CommandName="_divMenuBtnToDivAdd" ForeColor="black"
                            OnCommand="btnCommand" Text="สร้างรายการ" />
                    </li>


                    <li id="_divMenuLiToViewMaster" runat="server" class="dropdown">

                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" style="color: black;" aria-haspopup="true" aria-expanded="false">Master Data 
                            <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu">
                            <li id="_divMenuBtnToDivMaster_Supplier" runat="server">
                                <asp:LinkButton ID="_divMenuBtnToDivMasterSupplier" runat="server"
                                    CommandName="_divMenuBtnToDivMasterSupplier" ForeColor="black"
                                    OnCommand="btnCommand" Text="ผู้จัดจำหน่าย" />
                            </li>

                            <li id="_divMenuLiToViewMaster_Certificate" runat="server">
                                <asp:LinkButton ID="_divMenuBtnToDivMasterCertificate" runat="server"
                                    CommandName="_divMenuBtnToDivMasterCertificate" ForeColor="black"
                                    OnCommand="btnCommand" Text="ประเภท Certificate" />
                            </li>
                        </ul>
                    </li>


                    <%--                    <li id="_divMenuLiToViewReport" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivReport" runat="server"
                            CommandName="_divMenuBtnToDivReport" ForeColor="black"
                            OnCommand="btnCommand" Text="รายงาน">
                            <asp:Label ID="Label43" Font-Bold="true" runat="server"></asp:Label>
                        </asp:LinkButton>

                    </li>--%>
                </ul>

                 <ul class="nav navbar-nav navbar-right" runat="server">

                    <li id="_divMenuLiToViewManual" runat="server">
                        <asp:HyperLink ID="_divMenuBtnToDivManual" runat="server"
                            NavigateUrl="https://docs.google.com/document/d/1YrG_LpsSPecUq0xQ9AQhgnh4XDSWvLOWU_W_Y8K5cmo/edit?usp=sharing" Target="_blank"
                            CommandName="_divMenuBtnToDivManual"
                            OnCommand="btnCommand" Text="คู่มือการใช้งาน" />
                    </li>


                </ul>
            </div>
        </nav>
    </div>

    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <%------ หน้า index ค้นหา  ------%>

        <asp:View ID="ViewIndex" runat="server">
            <div class="col-lg-12 ">
                <div class="bg-system">
                    <asp:Image ID="Image3" runat="server" class="img-responsive img-fluid" ImageUrl="~/images/EPN/Header_MAS_EPN.png" Style="width: 100%;" />
                </div>
            </div>
            <br />
            <br />
            <br />
            <br />
            <br />
            &nbsp;&nbsp;&nbsp;&nbsp;
            <%--ค้นหา--%>

            <%--<asp:FormView ID="FvSearch" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">--%>

            <div class="col-lg-12  ">
                <%-- <asp:FormView ID="FvSearch" runat="server" Width="100%" >--%>
                <div class="panel panel-default" style="border-width: 1pt">
                    <div class="panel-heading" style="background-color: #9999FF; color: black;">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-search"></i><strong>&nbsp; ค้นหาข้อมูล</strong></h3>
                    </div>

                    <div class="panel-body">
                        <div class="form-horizontal" role="form">



                            <div class="form-group">
                                <asp:Label ID="lbSearch_cer" runat="server" Text="ประเภท Certificate :" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                <div class="col-sm-3">
                                    <%--Text='<%# Eval("")%>'--%>
                                    <%--<asp:TextBox ID="ddlTypecertificatesearch" runat="server" CssClass="form-control" Visible="true" />--%>
                                    <asp:DropDownList ID="ddlTypecertificatesearch" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server">
                                        <asp:ListItem Value="0">กรุณาเลือกประเภท Certificate...</asp:ListItem>
                                    </asp:DropDownList>
                                </div>

                                <asp:Label ID="Label123" CssClass="col-sm-2 control-label" runat="server" Text="ชื่อ certificate :" />
                                <div class="col-sm-3">

                                    <asp:TextBox ID="certificatesearch" runat="server" CssClass="form-control" Visible="true" />
                                    <%--<asp:DropDownList ID="ddlcertificatesearch" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="0">กรุณาเลือกชื่อ certificate....</asp:ListItem>
                                    </asp:DropDownList>--%>
                                </div>
                            </div>


                            <div class="form-group">
                                <asp:Label ID="Label144" runat="server" Text="ชื่อ Supplier :" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                <div class="col-sm-3">
                                    <asp:DropDownList ID="ddlSuppliersearch" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" ValidationGroup="Save" CssClass="form-control" runat="server">
                                        <asp:ListItem Value="0">กรุณาเลือกชื่อ Supplier...</asp:ListItem>
                                    </asp:DropDownList>

                                </div>


                                <asp:Label ID="Label15" runat="server" Text="สถานที่ :" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtLocation" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("LocIDX")%>' />
                                    <asp:DropDownList ID="ddlLocationsheach" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" ValidationGroup="Save" CssClass="form-control" runat="server">
                                        <asp:ListItem Value="0">กรุณาเลือกสถานที่...</asp:ListItem>
                                    </asp:DropDownList>

                                </div>


                            </div>


                            <div class="form-group">
                                <asp:Label ID="Label13" runat="server" Text="สถานะการใช้งาน :" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                <div class="col-sm-3">
                                    <asp:DropDownList ID="ddlStatussearch" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" ValidationGroup="Save" CssClass="form-control" runat="server">
                                        <asp:ListItem Value="0">กรุณาเลือกสถานะการใช้งาน...</asp:ListItem>
                                        <asp:ListItem Value="1">เปิดการใช้งาน...</asp:ListItem>
                                        <asp:ListItem Value="9">ปิดการใช้งาน...</asp:ListItem>
                                    </asp:DropDownList>

                                </div>
                            </div>

                             
                     

                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <asp:LinkButton ID="btnSearchCer" CssClass="btn btn-success btn-sm button_search " ValidationGroup="SearchList" runat="server" CommandName="btnsearch_list" OnCommand="btnCommand" title="Search"><i class="fa fa-search"></i> Search</asp:LinkButton>
                                    <asp:LinkButton ID="btnRefreshCer" CssClass="btn btn-default  btn-sm  button_ref" runat="server" CommandName="btnrefresh_list" OnCommand="btnCommand" title="Refresh"><i class="glyphicon glyphicon-refresh"></i> Refresh</asp:LinkButton>

                                </div>
                            </div>


                        </div>
                    </div>
                </div>
                <%--</asp:FormView>--%>


                <div style="overflow-x: auto; width: 100%">
                    <asp:GridView ID="GvIndex" runat="server"
                        AutoGenerateColumns="false"
                        CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                        HeaderStyle-BackColor="#9999FF"
                        HeaderStyle-Height="40px"
                        AllowPaging="true"
                        BorderStyle="Solid"
                        HeaderStyle-BorderWidth="2pt"
                        PageSize="10"
                        OnRowDataBound="Master_RowDataBound"
                        OnPageIndexChanging="Master_PageIndexChanging"
                        DataKeyNames="u0doc_idx">

                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                        <EmptyDataTemplate>
                            <div style="text-align: center">Data Cannot Be Found</div>
                        </EmptyDataTemplate>
                        <Columns>

                            <asp:TemplateField HeaderText="ข้อมูลรายการ" ItemStyle-BorderWidth="2pt" ItemStyle-Width="20%" HeaderStyle-CssClass="text-center" ItemStyle-BackColor="#FFFAFA" ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="Medium">
                                <ItemTemplate>

                                    <small>
                                        <strong>
                                            <asp:Label ID="Label11" runat="server">ประเภท Certificate: </asp:Label></strong>
                                        <asp:Literal ID="Literal4" Visible="false" runat="server" Text='<%# Eval("type_idx") %>' />
                                        <asp:Literal ID="Literal5" runat="server" Text='<%# Eval("type_name") %>' />
                                        </p>
                                                
                                    </small>

                                    <small>
                                        <strong>
                                            <asp:Label ID="Label10" runat="server">ชื่อ Certificate: </asp:Label></strong>
                                        <asp:Literal ID="Literal2" Visible="false" runat="server" Text='<%# Eval("u0doc_idx") %>' />
                                        <asp:Literal ID="Literal3" runat="server" Text='<%# Eval("certificate_name") %>' />
                                        </p>
                                                
                                    </small>

                                    <small>
                                        <strong>
                                            <asp:Label ID="Labesl15" runat="server">สถานที่: </asp:Label></strong>
                                        <asp:Literal ID="litLocIDX" Visible="false" runat="server" Text='<%# Eval("LocIDX") %>' />
                                        <asp:Literal ID="LitLocName" runat="server" Text='<%# Eval("LocName") %>' />
                                        </p>
                                                
                                    </small>
                                </ItemTemplate>

                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ข้อมูลผู้สร้าง" ItemStyle-BorderWidth="2pt" ItemStyle-Width="25%" HeaderStyle-CssClass="text-center" ItemStyle-BackColor="#FFFAFA" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Medium">
                                <ItemTemplate>
                                    <small>
                                        <strong>
                                            <asp:Label ID="Labeslr15" runat="server">องค์กร: </asp:Label></strong>
                                        <asp:Literal ID="litorg_idx" Visible="false" runat="server" Text='<%# Eval("org_idx") %>' />
                                        <asp:Literal ID="Literal6" runat="server" Text='<%# Eval("org_name_th") %>' />
                                        </p>
                                                 <strong>
                                                     <asp:Label ID="Labelsd24" runat="server">ฝ่าย: </asp:Label></strong>
                                        <asp:Literal ID="litrdept_idx" Visible="false" runat="server" Text='<%# Eval("rel_idx") %>' />
                                        <asp:Literal ID="Literal7" runat="server" Text='<%# Eval("dept_name_th") %>' />
                                        </p>
                                                 <strong>
                                                     <asp:Label ID="Labsel25" runat="server">แผนก: </asp:Label></strong>
                                        <asp:Literal ID="litrsec_idx" Visible="false" runat="server" Text='<%# Eval("rsec_idx") %>' />
                                        <asp:Literal ID="Literal8" runat="server" Text='<%# Eval("sec_name_th") %>' />
                                        </p>
                                                 
                                                 <strong>
                                                     <asp:Label ID="Label26" runat="server">ชื่อ-สกุล: </asp:Label></strong>
                                        <asp:Literal ID="Literalemp" runat="server" Text='<%# Eval("emp_name_th") %>' />
                                        <asp:Literal ID="litcemp_idx" Visible="false" runat="server" Text='<%# Eval("cemp_idx") %>' />
                                        </p>
                                    </small>

                                </ItemTemplate>
                            </asp:TemplateField>



                            <asp:TemplateField HeaderText="สถานะรายการ" ItemStyle-BorderWidth="2pt" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-BackColor="#FFFAFA" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Medium">
                                <ItemTemplate>
                                    <small>
                                        <b>
                                            <asp:Label ID="lblStatusDoc" runat="server" Text='<%# Eval("status_name") %>' />
                                        </b>
                                    </small>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จัดการ" ItemStyle-BorderWidth="2pt" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-BackColor="#FFFAFA" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Medium">
                                <ItemTemplate>
                                    <small>
                                        <asp:LinkButton ID="btnItemEdit" runat="server" CssClass="btn btn-sm btn-warning button_edit"
                                            OnCommand="btnCommand" CommandName="btnItemEdit" title="แก้ไข"
                                            CommandArgument='<%# Eval("u0doc_idx") %>'>
                                                     <i class="fas fa-pencil-alt" aria-hidden="true"></i>
                                        </asp:LinkButton>
                                        <asp:LinkButton ID="btnItemDel" runat="server" CssClass="btn btn-sm btn-default button_del"
                                            OnCommand="btnCommand" CommandName="btnItemDel" CommandArgument='<%# Eval("u0doc_idx") %>'
                                            title="ลบ"
                                            OnClientClick="return confirm('คุณต้องการลบข้อมูลใช่หรือไม่?')">
                                                      <i class="far fa-trash-alt" aria-hidden="true"></i>
                                        </asp:LinkButton>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>
                </div>
            </div>


        </asp:View>

        <asp:View ID="ViewInsert" runat="server">
            <div class="col-lg-12">
                <div class="bg-system">
                    <asp:Image ID="Image6" runat="server" class="img-responsive img-fluid" ImageUrl="~/images/EPN/Header_MAS_EPN.png" Style="width: 100%;" />
                </div>
            </div>
            &nbsp;&nbsp;&nbsp;
            <div class="col-lg-12">
                <div class="panel panel-default" style="border-width: 1pt">
                    <div class="panel-heading" style="background-color: #9999FF; color: black;">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ข้อมูลส่วนตัวผู้ทำรายการ</strong></h3>
                    </div>
                    <%------ ข้อมูลผู้ทำรายการ  ------%>
                    <asp:FormView ID="FvDetailUser" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>
                            <div class="panel-body ">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">

                                        <asp:Label ID="Label7" CssClass="col-sm-2 control-label" runat="server" Text="รหัสผู้ทำรายการ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtempcode" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label9" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อผู้ทำรายการ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtrequesname" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>
                                    <div class="form-group">

                                        <asp:Label ID="Label4" CssClass="col-sm-2 control-label" runat="server" Text="บริษัท : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtorg" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label34" CssClass="col-sm-3 control-label" runat="server" Text="ฝ่าย : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtrequesdept" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>


                                    <div class="form-group">

                                        <asp:Label ID="Labedl1" class="col-sm-2 control-label" runat="server" Text="แผนก : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label8" CssClass="col-sm-3 control-label" runat="server" Text="ตำแหน่ง : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtpos" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                    <div class="form-group">

                                        <asp:Label ID="Label5" class="col-sm-2 control-label" runat="server" Text="เบอร์ติดต่อ : " />
                                        <div class="col-sm-3">
                                            <%--<asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("SecNameTH") %>'></asp:TextBox>--%>
                                            <asp:TextBox ID="txttel" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label6" CssClass="col-sm-3 control-label" runat="server" Text="E-mail : " />
                                        <div class="col-sm-3">
                                            <%--<asp:TextBox ID="txtdep" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("PosNameTH") %>'></asp:TextBox>--%>
                                            <asp:TextBox ID="txtemail" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>
                </div>
            </div>

            <%------ ข้อมูลการทำรายการ  ------%>
            <div class="col-lg-12">
                <div class="panel panel-default " style="border-width: 1pt">
                    <div class="panel-heading" style="background-color: #9999FF; color: black;">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-pencil"></i><strong>&nbsp; ข้อมูลการทำรายการ</strong></h3>
                    </div>
                    <asp:FormView ID="FvinsertProblem" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">

                        <InsertItemTemplate>
                            <%-- Form Insert--%>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <asp:Label ID="Label5" runat="server" Text="ประเภท Certificate:" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddltype" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" ValidationGroup="Save" CssClass="form-control" runat="server">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddltype" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกประเภท Certificate"
                                                ValidationExpression="กรุณาเลือกประเภท Certificate" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />

                                        </div>

                                        <asp:Label ID="lbPlan" runat="server" Text="Plan:" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlPlan" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" ValidationGroup="Save" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="0">กรุณาเลือก Plan...</asp:ListItem>
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddlPlan" Font-Size="11"
                                                ErrorMessage="กรุณาเลือก Plan"
                                                ValidationExpression="กรุณาเลือก Plan" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator4" Width="160" />
                                        </div>

                                    </div>


                                    <div class="form-group">
                                        <asp:Label ID="Label3" runat="server" Text="ชื่อ Supplier:" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlSupplier" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" ValidationGroup="Save" CssClass="form-control" runat="server">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddlSupplier" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกชื่อ Supplier"
                                                ValidationExpression="กรุณาเลือกชื่อ Supplier" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />

                                        </div>

                                        <asp:Label ID="Label12" runat="server" Visible="false" Text="ฝ่าย:" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlDept" Visible="false" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" ValidationGroup="Save" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="0">กรุณาเลือกฝ่าย...</asp:ListItem>
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddlDept" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกฝ่าย"
                                                ValidationExpression="กรุณาเลือกฝ่าย" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator4" Width="160" />
                                        </div>
                                    </div>



                                    <div class="form-group">

                                        <asp:Label ID="Label18" runat="server" Text="ชื่อ Certificate:" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtCertificate_name" runat="server" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="txtCertificate_name" Font-Size="11"
                                                ErrorMessage="กรุณาระบุชื่อ Certificate "
                                                ValidationExpression="กรุณาระบุชื่อ Certificate"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender15" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator11" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server"
                                                ValidationGroup="Save" Display="None"
                                                ErrorMessage="คุณกรอกตัวอักษรมากกว่า 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtCertificate_name"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender16" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator5" Width="160" />

                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="lbdetail" class="col-sm-2 control-label" runat="server" Text="รายละเอียดเพิ่มเติม : " />
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtdetail" TextMode="multiline" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="Save" runat="server"
                                                Display="None" ControlToValidate="txtdetail" Font-Size="11"
                                                ErrorMessage="กรุณากรอกรายละเอียดเพิ่มเติม"
                                                ValidationExpression="กรุณากรอกรายละเอียดเพิ่มเติม"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                                ValidationGroup="Save" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtdetail"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />
                                        </div>
                                    </div>

                                    <div class="form-group">

                                        <asp:Label ID="lbcontact" runat="server" Text="ที่อยู่ติดต่อ:" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtContact" runat="server" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="txtContact" Font-Size="11"
                                                ErrorMessage="กรุณาระบุที่อยู่ติดต่อ"
                                                ValidationExpression="กรุณาระบุที่อยู่ติดต่อ"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator11" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                                                ValidationGroup="Save" Display="None"
                                                ErrorMessage="คุณกรอกตัวอักษรมากกว่า 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtContact"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator5" Width="160" />

                                        </div>

                                        <asp:Label ID="lbTel" runat="server" Text="เบอร์โทรศัพท์:" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtTel" runat="server" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="txtContact" Font-Size="11"
                                                ErrorMessage="กรุณาระบุที่อยู่ติดต่อ"
                                                ValidationExpression="กรุณาระบุเบอร์โทรศัพท์"
                                                SetFocusOnError="true" />


                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator11" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server"
                                                ValidationGroup="Save" Display="None"
                                                ErrorMessage="คุณกรอกตัวอักษรมากกว่า 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtTel"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator5" Width="160" />

                                        </div>

                                    </div>


                                    <div class="form-group">
                                        <asp:Label ID="Label82" runat="server" Text="วันที่เริ่ม:" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <div class='input-group date'>
                                                <asp:TextBox ID="txtdatestart" runat="server" CssClass="form-control from-date-datepicker" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator22" ValidationGroup="Save" runat="server" Display="None"
                                                    ControlToValidate="txtdatestart" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกวันที่เริ่ม"
                                                    ValidationExpression="กรุณาเลือกวันที่เริ่ม" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender36" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator22" Width="160" />
                                            </div>
                                        </div>

                                        <asp:Label ID="Label83" runat="server" Text="วันที่หมดอายุ:" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <div class='input-group date'>
                                                <asp:TextBox ID="txtdateexp" runat="server" CssClass="form-control from-date-datepicker" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group">

                                        <asp:Label ID="Labedle1" class="col-sm-2 control-label" runat="server" Text="หมายเหตุ : " />
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtremark" TextMode="multiline" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="txtremark" Font-Size="11"
                                                ErrorMessage="กรุณากรอกหมายเหตุ"
                                                ValidationExpression="กรุณากรอกหมายเหตุ"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender24" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                ValidationGroup="Save" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtremark"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />
                                        </div>
                                    </div>




                                    <div class="form-group">
                                        <asp:Label ID="Label14" runat="server" Text="ความถี่:" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlfequncy" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" ValidationGroup="Save" CssClass="form-control" runat="server">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddlfequncy" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกความถี่"
                                                ValidationExpression="กรุณาเลือกความถี่" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender13" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />

                                        </div>

                                    </div>

                                    <asp:UpdatePanel ID="UpdatePnlddlAlert" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>

                                            <div class="form-group">

                                                <asp:Label ID="lbAlert" runat="server" Text="การแจ้งเตือน:" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlAlert" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" ValidationGroup="Save" CssClass="form-control" runat="server">
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" ValidationGroup="Save" runat="server" Display="None"
                                                        ControlToValidate="ddlAlert" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกการแจ้งเตือน"
                                                        ValidationExpression="กรุณาเลือกการแจ้งเตือน" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender18" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator13" Width="160" />

                                                </div>
                                            </div>



                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ddlAlert" EventName="SelectedIndexChanged" />
                                        </Triggers>

                                    </asp:UpdatePanel>


                                    <%--  <asp:Panel ID="Panel1" runat="server" Visible="false">
                                        kkkkkkkkkkkkk


                                    </asp:Panel>--%>

                                    <asp:Panel ID="Pnl_allmounth" runat="server" Visible="false">

                                        <div class="form-group">
                                            <asp:Label ID="lballMonth" runat="server" Visible="true" Text="ทุกๆ วันที่:" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlallMonth" Visible="true" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" ValidationGroup="Save" CssClass="form-control" runat="server">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ValidationGroup="Save" runat="server" Display="None"
                                                    ControlToValidate="ddlallMonth" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกทุกๆ"
                                                    ValidationExpression="กรุณาเลือกทุกๆ" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ddlallMonth_RequiredFieldValidator1"
                                                    runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator10" Width="160" />

                                            </div>
                                        </div>
                                    </asp:Panel>




                                    <asp:Panel ID="Pnl_month" runat="server" Visible="false">
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">ทุกๆ เดือน:</label>
                                            <div class="col-md-4">
                                                <asp:GridView ID="gvGroupList" runat="server" AutoGenerateColumns="False"
                                                    CssClass="table table-striped table-bordered table-hover table-responsive" DataKeyNames="month_idx">
                                                    <HeaderStyle CssClass="info" Font-Size="Small" />
                                                    <RowStyle Font-Size="Small" />
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="#">
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="cbGroupSelected" runat="server" AutoPostBack="true" Text='<%# Eval("month_idx") + "|" + Eval("month_name_th") %>' CssClass="hiddenText" Style="color: transparent;"></asp:CheckBox>
                                                                <asp:HiddenField ID="hfM0Idx" runat="server" Value='<%# Eval("month_idx")%>'></asp:HiddenField>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="เดือน">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblmonthName" runat="server" Text='<%# Eval("month_name_th") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>



                                            </div>

                                        </div>

                                        <br></br>



                                    </asp:Panel>




                                    <asp:Panel ID="pnl_amount" runat="server" Visible="false">

                                        <div class="form-group row">
                                            <asp:Label ID="lbAmountDate" runat="server" Text="จำนวนวันการแจ้งเตือน:" CssClass="col-sm-2 control-label text_right"></asp:Label>

                                            <div class="col-md-2">
                                                <div class="input-group">
                                                    <asp:TextBox ID="txtAddDate_Amount" runat="server" placeholder=""
                                                        CssClass="form-control" TextMode="Number" />

                                                    <asp:RequiredFieldValidator ID="AddDate_Amount"
                                                        ValidationGroup="AddData"
                                                        runat="server" Display="None"
                                                        ControlToValidate="txtAddDate_Amount" Font-Size="11"
                                                        ErrorMessage="กรุณากรอกข้อมูล ...." />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight"
                                                        TargetControlID="AddDate_Amount" Width="160" />

                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <asp:LinkButton ID="btnAddItem" runat="server" CssClass="btn btn-success button_color"
                                                    OnCommand="btnCommand" CommandName="btnAddItem" CommandArgument="1"
                                                    ValidationGroup="AddData">
                                                <i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;เพิ่ม
                                                </asp:LinkButton>

                                            </div>
                                        </div>

                                        <br></br>


                                        <div class="form-group row">
                                            <%--<div class="form-group row  AddItem">--%>
                                            <asp:Label ID="lbAddDate" runat="server" Text="" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                            <div class="col-md-9">
                                                <asp:GridView ID="GvAddDate"
                                                    runat="server"
                                                    AutoGenerateColumns="false"
                                                    CssClass="table table-striped table-bordered table-hover table-responsive "
                                                    AllowPaging="True"
                                                    PageSize="15"
                                                    OnPageIndexChanging="Master_PageIndexChanging"
                                                    OnRowCommand="onRowCommand"
                                                    OnRowDataBound="Master_RowDataBound">
                                                    <HeaderStyle CssClass="bg-secondary" />
                                                    <PagerStyle CssClass="pageCustom" />
                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                    <EmptyDataTemplate>
                                                        <div class="alert alert-warning" role="alert" style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                                                    </EmptyDataTemplate>

                                                    <Columns>

                                                        <asp:TemplateField HeaderStyle-CssClass="text-left" HeaderStyle-Font-Size="Small" HeaderText="#" ItemStyle-HorizontalAlign="center">
                                                            <ItemTemplate>

                                                                <%--<asp:Label ID="_lbu0idx" runat="server" Text='<%# Eval("id_redeem_product") %>' Visible="false" />--%>

                                                                <%# (Container.DataItemIndex +1) %>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderStyle-CssClass="text-left" HeaderStyle-Font-Size="Small" HeaderText="สินค้า" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>

                                                                <%--                                                            <asp:Label ID="lbid_product" runat="server" CssClass="col-sm-12" Visible="false" Text='<%# Eval("id_product") %>'></asp:Label>
                                                            <asp:Label ID="lbproduct_name" runat="server" CssClass="col-sm-12" Text='<%# Eval("product_name") %>'></asp:Label>--%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderStyle-CssClass="text-left" HeaderStyle-Font-Size="Small" HeaderText="จำนวน" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>

                                                                <asp:Label ID="lbproduct_qty" runat="server" CssClass="col-sm-12" Text='<%# Eval("date_condition") %>'></asp:Label>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="จัดการ"
                                                            HeaderStyle-Font-Size="Small"
                                                            HeaderStyle-CssClass="text-center"
                                                            ItemStyle-CssClass="text-center">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="btndeleteProduct" runat="server" Text="Delete" CssClass="btn btn-danger btn-xs button_del" OnClientClick="return confirm('คุณต้องการลบข้อมูลใช่หรือไม่?')" CommandName="btndeleteProduct"><i class="fas fa-trash-alt"></i></asp:LinkButton>


                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                    </Columns>

                                                </asp:GridView>
                                            </div>
                                        </div>

                                    </asp:Panel>

                                </div>


                                <%--<asp:Panel ID="Pnl_addDate" runat="server" Visible="false">
                                    <div class="form-group" runat="server">
                                        <asp:GridView ID="GvMasterDate" runat="server"
                                            AutoGenerateColumns="false"
                                            CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                            HeaderStyle-CssClass="primary"
                                            HeaderStyle-Height="40px"
                                            AllowPaging="true"
                                            DataKeyNames="m0_supidx"
                                            PageSize="10"
                                            OnRowDataBound="Master_RowDataBound"
                                            OnRowEditing="Master_RowEditing"
                                            OnRowCancelingEdit="Master_RowCancelingEdit"
                                            OnPageIndexChanging="Master_PageIndexChanging"
                                            OnRowUpdating="Master_RowUpdating">

                                            <PagerStyle CssClass="pageCustom" />
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                            <EmptyDataTemplate>
                                                <div style="text-align: center">Data Cannot Be Found</div>
                                            </EmptyDataTemplate>
                                            <Columns>
                                                <asp:TemplateField HeaderText="#">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbm0_supidx" runat="server" Visible="false" Text='<%# Eval("m0_supidx") %>' />
                                                        <%# (Container.DataItemIndex +1) %>
                                                    </ItemTemplate>

                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="จัดการข้อมูล" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="15%">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDel" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("m0_supidx") %>'><i class="fa fa-trash"></i></asp:LinkButton>
                                                    </ItemTemplate>


                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </asp:Panel>--%>
                                <%-- ValidationGroup="Save" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')"  --%>
                                <div class="form-group" runat="server">
                                    <div class=" col-sm-offset-10">
                                        <asp:LinkButton ID="btnSaveAll" CssClass="btn btn-success btn-sm button_color" runat="server" CommandName="CmdSave" OnCommand="btnCommand" title="Save" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                        <asp:LinkButton ID="AddCancel" CssClass="btn btn-default  btn-sm button_del" runat="server" CommandName="BtnBack" OnCommand="btnCommand" title="Cancel" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-times"></i></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                            </div>

                        </InsertItemTemplate>
                    </asp:FormView>

                    <%-- <asp:Label ID="Label17" runat="server" Text="xxxxxx"></asp:Label>--%>



                    <%-- กรณีแก้ไข--%>
                    <asp:FormView ID="update_FvinsertProblem" runat="server" Width="100%" DefaultMode="Edit">

                        <EditItemTemplate>
                            <%-- Form update--%>
                            <asp:TextBox ID="updatekey" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("u0doc_idx")%>' />
                            <asp:TextBox ID="updatem1_notification_idx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("m1_notification_idx")%>' />
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">

                                        <%--<asp:Label ID="Label16" runat="server" Text="aaaaaa"></asp:Label>--%>

                                        <asp:TextBox ID="Updatetxtm0_sup" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("m0_supidx")%>' />

                                        <asp:Label ID="update_Label5" runat="server" Text="ประเภท Certificate:" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="update_ddltype" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" ValidationGroup="Save" CssClass="form-control" runat="server">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="update_RequiredFieldValidator1" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="update_ddltype" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกประเภท Certificate"
                                                ValidationExpression="กรุณาเลือกประเภท Certificate" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="update_ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="update_RequiredFieldValidator1" Width="160" />

                                        </div>

                                        <asp:Label ID="update_lbPlan" runat="server" Text="Plan:" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="update_ddlPlan" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" ValidationGroup="Save" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="0">กรุณาเลือก Plan...</asp:ListItem>
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="update_RequiredFieldValidator4" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="update_ddlPlan" Font-Size="11"
                                                ErrorMessage="กรุณาเลือก Plan"
                                                ValidationExpression="กรุณาเลือก Plan" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="update_ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="update_RequiredFieldValidator4" Width="160" />
                                        </div>

                                    </div>


                                    <div class="form-group">
                                        <asp:Label ID="update_Label3" runat="server" Text="ชื่อ Supplier:" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-9">
                                            <asp:DropDownList ID="update_ddlSupplier" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" ValidationGroup="Save" CssClass="form-control" runat="server">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="update_RequiredFieldValidator3" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="update_ddlSupplier" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกชื่อ Supplier"
                                                ValidationExpression="กรุณาเลือกชื่อ Supplier" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="update_ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="update_RequiredFieldValidator1" Width="160" />

                                        </div>

                                        <asp:Label ID="update_Label12" runat="server" Visible="false" Text="ฝ่าย:" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="update_ddlDept" Visible="false" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" ValidationGroup="Save" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="0">กรุณาเลือกฝ่าย...</asp:ListItem>
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="update_RequiredFieldValidator5" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="update_ddlDept" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกฝ่าย"
                                                ValidationExpression="กรุณาเลือกฝ่าย" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="update_ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="update_RequiredFieldValidator4" Width="160" />
                                        </div>
                                    </div>



                                    <div class="form-group">

                                        <asp:Label ID="update_Label18" runat="server" Text="ชื่อ Certificate:" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-9">
                                            <%--<asp:TextBox ID="update_txtCertificate_name" runat="server" CssClass="form-control"></asp:TextBox>--%>
                                            <asp:TextBox ID="update_txtCertificate_name" runat="server" CssClass="form-control"
                                                placeholder="" MaxLength="100"
                                                Text='<%# Eval("certificate_name") %>'>
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="update_RequiredFieldValidator11"
                                                ValidationGroup="Save" runat="server" Display="None" ControlToValidate="update_txtCertificate_name" Font-Size="11"
                                                ErrorMessage="กรุณาระบุชื่อ Certificate "
                                                ValidationExpression="กรุณาระบุชื่อ Certificate"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="update_ValidatorCalloutExtender15" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="update_RequiredFieldValidator11" Width="160" />
                                            <asp:RegularExpressionValidator ID="update_RegularExpressionValidator5" runat="server"
                                                ValidationGroup="Save" Display="None"
                                                ErrorMessage="คุณกรอกตัวอักษรมากกว่า 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="update_txtCertificate_name"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="update_ValidatorCalloutExtender16" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="update_RegularExpressionValidator5" Width="160" />

                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="update_lbdetail" class="col-sm-2 control-label" runat="server" Text="รายละเอียดเพิ่มเติม : " />
                                        <div class="col-sm-9">
                                            <%--<asp:TextBox ID="update_txtdetail" TextMode="multiline" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>--%>
                                            <asp:TextBox ID="update_txtdetail" runat="server" CssClass="form-control"
                                                placeholder="" MaxLength="100"
                                                Text='<%# Eval("certificate_detail") %>'>
                                            </asp:TextBox>

                                            <asp:RequiredFieldValidator ID="update_RequiredFieldValidator2"
                                                ValidationGroup="Save" runat="server" Display="None" ControlToValidate="update_txtdetail" Font-Size="11"
                                                ErrorMessage="กรุณากรอกรายละเอียดเพิ่มเติม"
                                                ValidationExpression="กรุณากรอกรายละเอียดเพิ่มเติม"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="update_ValidatorCalloutExtender3"
                                                runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                TargetControlID="update_RequiredFieldValidator8" Width="160" />
                                            <asp:RegularExpressionValidator ID="update_RegularExpressionValidator2"
                                                runat="server"
                                                ValidationGroup="Save" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="update_txtdetail"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="update_ValidatorCalloutExtender4"
                                                runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                TargetControlID="update_RegularExpressionValidator1" Width="160" />
                                        </div>
                                    </div>

                                    <div class="form-group">

                                        <asp:Label ID="update_lbcontact" runat="server" Text="ที่อยู่ติดต่อ:" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <%--<asp:TextBox ID="update_txtContact" runat="server" CssClass="form-control"></asp:TextBox>--%>
                                            <asp:TextBox ID="update_txtContact" runat="server" CssClass="form-control"
                                                placeholder="" MaxLength="100"
                                                Text='<%# Eval("contact") %>'>
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="update_RequiredFieldValidator6"
                                                ValidationGroup="Save" runat="server" Display="None" ControlToValidate="update_txtContact" Font-Size="11"
                                                ErrorMessage="กรุณาระบุที่อยู่ติดต่อ"
                                                ValidationExpression="กรุณาระบุที่อยู่ติดต่อ"
                                                SetFocusOnError="true" />


                                            <ajaxToolkit:ValidatorCalloutExtender ID="update_ValidatorCalloutExtender8"
                                                runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                TargetControlID="update_RequiredFieldValidator11" Width="160" />
                                            <asp:RegularExpressionValidator ID="update_RegularExpressionValidator3" runat="server"
                                                ValidationGroup="Save" Display="None"
                                                ErrorMessage="คุณกรอกตัวอักษรมากกว่า 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="update_txtContact"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="update_ValidatorCalloutExtender9" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="update_RegularExpressionValidator5" Width="160" />

                                        </div>

                                        <asp:Label ID="update_lbTel" runat="server" Text="เบอร์โทรศัพท์:" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <%-- <asp:TextBox ID="update_txtTel" runat="server" CssClass="form-control"></asp:TextBox>--%>
                                            <asp:TextBox ID="update_txtTel" runat="server" CssClass="form-control"
                                                placeholder="" MaxLength="100"
                                                Text='<%# Eval("phone_no") %>'>
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="update_RequiredFieldValidator7"
                                                ValidationGroup="Save" runat="server" Display="None" ControlToValidate="update_txtTel" Font-Size="11"
                                                ErrorMessage="กรุณาระบุที่อยู่ติดต่อ"
                                                ValidationExpression="กรุณาระบุเบอร์โทรศัพท์"
                                                SetFocusOnError="true" />


                                            <ajaxToolkit:ValidatorCalloutExtender ID="update_ValidatorCalloutExtender10" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="update_RequiredFieldValidator11" Width="160" />
                                            <asp:RegularExpressionValidator ID="update_RegularExpressionValidator4" runat="server"
                                                ValidationGroup="Save" Display="None"
                                                ErrorMessage="คุณกรอกตัวอักษรมากกว่า 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="update_txtTel"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="update_ValidatorCalloutExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="update_RegularExpressionValidator5" Width="160" />

                                        </div>

                                    </div>

                                    <%-- <asp:TextBox ID="TextBox1" runat="server" 
                                                     Text='<%# Eval("date_start") %>'></asp:TextBox>--%>
                                    <div class="form-group">
                                        <asp:Label ID="update_Label82" runat="server" Text="วันที่เริ่ม:" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <div class='input-group date'>
                                                <asp:TextBox ID="update_txtdatestart" runat="server" CssClass="form-control from-date-datepicker"
                                                    placeholder="กรุณาเลือกวันที่" MaxLengh="100%" Text='<%# Eval("date_start") %>'></asp:TextBox>

                                                <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                <asp:RequiredFieldValidator ID="update_RequiredFieldValidator22"
                                                    ValidationGroup="Save" runat="server" Display="None"
                                                    ControlToValidate="update_txtdatestart" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกวันที่เริ่ม"
                                                    ValidationExpression="กรุณาเลือกวันที่เริ่ม" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="update_ValidatorCalloutExtender36"
                                                    runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                    TargetControlID="update_RequiredFieldValidator22" Width="160" />
                                            </div>
                                        </div>

                                        <asp:Label ID="update_Label83" runat="server" Text="วันที่หมดอายุ:" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <div class='input-group date'>
                                                <asp:TextBox ID="update_txtdateexp" runat="server" CssClass="form-control from-date-datepicker" placeholder="กรุณาเลือกวันที่" MaxLengh="100%" Text='<%# Eval("date_expire") %>'></asp:TextBox>
                                                <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group">

                                        <asp:Label ID="update_Labedle1" class="col-sm-2 control-label" runat="server" Text="หมายเหตุ : " />
                                        <div class="col-sm-9">
                                            <%-- <asp:TextBox ID="update_txtremark" TextMode="multiline" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>--%>
                                            <asp:TextBox ID="update_txtremark" runat="server" CssClass="form-control"
                                                placeholder="" MaxLength="100"
                                                Text='<%# Eval("remark ") %>'>
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="update_RequiredFieldValidator8" ValidationGroup="Save"
                                                runat="server" Display="None" ControlToValidate="update_txtremark" Font-Size="11"
                                                ErrorMessage="กรุณากรอกหมายเหตุ"
                                                ValidationExpression="กรุณากรอกหมายเหตุ"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="update_ValidatorCalloutExtender24" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="update_RequiredFieldValidator8" Width="160" />
                                            <asp:RegularExpressionValidator ID="update_RegularExpressionValidator1" runat="server"
                                                ValidationGroup="Save" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="update_txtremark"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="update_ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="update_RegularExpressionValidator1" Width="160" />
                                        </div>
                                    </div>




                                    <div class="form-group">
                                        <asp:Label ID="update_Label14" runat="server" Text="ความถี่:" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="update_ddlfequncy" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" ValidationGroup="Save" CssClass="form-control" runat="server">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="update_RequiredFieldValidator9" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="update_ddlfequncy" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกความถี่"
                                                ValidationExpression="กรุณาเลือกความถี่" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="update_ValidatorCalloutExtender13" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="update_RequiredFieldValidator1" Width="160" />

                                        </div>

                                    </div>


                                    <asp:UpdatePanel ID="UpdatePnlddlAlert" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="form-group">
                                                <asp:Label ID="update_lbAlert" runat="server" Text="การแจ้งเตือน:" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="update_ddlAlert" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" ValidationGroup="Save" CssClass="form-control" runat="server">
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="update_RequiredFieldValidator13" ValidationGroup="Save" runat="server" Display="None"
                                                        ControlToValidate="update_ddlAlert" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกการแจ้งเตือน"
                                                        ValidationExpression="กรุณาเลือกการแจ้งเตือน" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="update_ValidatorCalloutExtender18"
                                                        runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="update_RequiredFieldValidator13" Width="160" />

                                                </div>
                                            </div>
                                        </ContentTemplate>

                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="update_ddlAlert" EventName="SelectedIndexChanged" />
                                        </Triggers>

                                    </asp:UpdatePanel>

                                    <asp:Panel ID="update_Pnl_allmounth" runat="server" Visible="false">
                                        <div class="form-group">
                                            <asp:Label ID="update_lballMonth" runat="server" Visible="true" Text="ทุกๆ วันที่:" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="update_ddlallMonth" Visible="true" OnSelectedIndexChanged="SelectedIndexChanged"
                                                    AutoPostBack="true" ValidationGroup="Save" CssClass="form-control" runat="server">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ValidationGroup="Save" runat="server" Display="None"
                                                    ControlToValidate="update_ddlallMonth" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกทุกๆ"
                                                    ValidationExpression="กรุณาเลือกทุกๆ" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender14"
                                                    runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator10" Width="160" />

                                            </div>
                                        </div>
                                    </asp:Panel>




                                    <asp:Panel ID="update_Pnl_month" runat="server" Visible="false">
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">ทุกๆ เดือน:</label>

                                            <%--<label class="col-md-2 control-label">Hello</label>--%>

                                            <div class="col-md-4">
                                                <asp:GridView ID="update_gvGroupList" runat="server" AutoGenerateColumns="false" OnRowDataBound="Master_RowDataBound"
                                                    CssClass="table table-striped table-bordered table-hover table-responsive" DataKeyNames="month_idx">
                                                    <HeaderStyle CssClass="info" Font-Size="Small" />
                                                    <RowStyle Font-Size="Small" />
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="#">
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="update_cbGroupSelected" runat="server" AutoPostBack="true" Text='<%# Eval("month_idx") + "|" + Eval("month_name_th") %>' CssClass="hiddenText" Style="color: transparent;"></asp:CheckBox>
                                                                <asp:HiddenField ID="update_hfM0Idx" runat="server" Value='<%# Eval("month_idx")%>'></asp:HiddenField>
                                                                <asp:HiddenField ID="update_u1_idx" runat="server" Value='<%# Eval("u1doc_idx")%>'></asp:HiddenField>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="เดือน">
                                                            <ItemTemplate>
                                                                <asp:Label ID="update_lblmonthName" runat="server" Text='<%# Eval("month_name_th") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>



                                            </div>

                                        </div>

                                        <br></br>



                                    </asp:Panel>


                                    <div class="form-group">
                                        <%--<asp:Panel ID="update_pnl_alert" runat="server" Visible="false">
                                            <asp:Label ID="update_lbddlAlertall" runat="server" Text="ทุกๆ:" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                          <asp:RequiredFieldValidator ID="RequiredFieldValidator12" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="update_ddlAlert" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกการแจ้งเตือน"
                                                ValidationExpression="กรุณาเลือกการแจ้งเตือน" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender17" 
                                                runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="update_RequiredFieldValidator1" Width="160" />

                                        </asp:Panel>--%>

                                        <asp:Panel ID="update_pnl_amount" runat="server" Visible="false">

                                            <div class="form-group row">
                                                <asp:Label ID="update_lbAmountDate" runat="server"
                                                    Text="จำนวนวันการแจ้งเตือน:" CssClass="col-sm-2 control-label text_right"></asp:Label>

                                                <div class="col-md-2">
                                                    <div class="input-group">
                                                        <asp:TextBox ID="update_txtAddDate_Amount" runat="server" placeholder=""
                                                            CssClass="form-control" TextMode="Number" />

                                                        <asp:RequiredFieldValidator ID="update_RequiredFieldValidator20"
                                                            ValidationGroup="AddData"
                                                            runat="server" Display="None"
                                                            ControlToValidate="update_txtAddDate_Amount" Font-Size="11"
                                                            ErrorMessage="กรุณากรอกข้อมูล ...." />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="update_ValidatorCalloutExtender7" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            TargetControlID="update_RequiredFieldValidator20" Width="160" />

                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <asp:LinkButton ID="update_btnAddItem" runat="server" CssClass="btn btn-success button_color"
                                                        OnCommand="btnCommand" CommandName="update_btnAddItem" CommandArgument="1"
                                                        ValidationGroup="AddData">
                                                <i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;เพิ่ม
                                                    </asp:LinkButton>

                                                </div>
                                            </div>

                                            <br></br>


                                            <div class="form-group row">
                                                <div class="form-group row  AddItem">
                                                    <asp:Label ID="update_lbAddDate" runat="server" Text="" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                    <div class="col-md-9">
                                                        <asp:GridView ID="update_GvAddDate"
                                                            runat="server"
                                                            AutoGenerateColumns="false"
                                                            CssClass="table table-striped table-bordered table-hover table-responsive "
                                                            AllowPaging="True"
                                                            PageSize="15"
                                                            OnPageIndexChanging="Master_PageIndexChanging"
                                                            OnRowCommand="onRowCommand"
                                                            OnRowDataBound="Master_RowDataBound">
                                                            <HeaderStyle CssClass="bg-secondary" />
                                                            <PagerStyle CssClass="pageCustom" />
                                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                            <EmptyDataTemplate>
                                                                <div class="alert alert-warning" role="alert" style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                                                            </EmptyDataTemplate>

                                                            <Columns>

                                                                <asp:TemplateField HeaderStyle-CssClass="text-left" HeaderStyle-Font-Size="Small" HeaderText="#" ItemStyle-HorizontalAlign="center">
                                                                    <ItemTemplate>

                                                                        <asp:Label ID="update_lbu0idx" runat="server" Visible="false" />

                                                                        <%# (Container.DataItemIndex +1) %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <%--<asp:TemplateField HeaderStyle-CssClass="text-left" HeaderStyle-Font-Size="Small" HeaderText="สินค้า" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>

                                                                    <%--<asp:Label ID="update_lbid_product" runat="server" CssClass="col-sm-12" Visible="false" Text='<%# Eval("id_product") %>'></asp:Label>
                                                            <asp:Label ID="update_lbproduct_name" runat="server" CssClass="col-sm-12" Text='<%# Eval("product_name") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>

                                                                <asp:TemplateField HeaderStyle-CssClass="text-left" HeaderStyle-Font-Size="Small" HeaderText="จำนวน" ItemStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate>

                                                                        <asp:Label ID="update_lbproduct_qty" runat="server" CssClass="col-sm-12" Text='<%# Eval("date_condition") %>'></asp:Label>

                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="จัดการ"
                                                                    HeaderStyle-Font-Size="Small"
                                                                    HeaderStyle-CssClass="text-center"
                                                                    ItemStyle-CssClass="text-center">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="update_btndeleteProduct" runat="server" Text="Delete" CssClass="btn btn-danger btn-xs button_del" OnClientClick="return confirm('คุณต้องการลบข้อมูลใช่หรือไม่?')" CommandName="update_btndeleteProduct"><i class="fas fa-trash-alt"></i></asp:LinkButton>


                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                            </Columns>

                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                        </asp:Panel>

                                    </div>



                                    <%-- ValidationGroup="Save" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')"  --%>
                                    <div class="form-group" runat="server">
                                        <div class=" col-sm-offset-10">
                                            <asp:LinkButton ID="update_btnSaveAll" CssClass="btn btn-success btn-sm button_color" runat="server" CommandName="update_CmdSave" OnCommand="btnCommand" title="Save" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                            <asp:LinkButton ID="update_AddCancel" CssClass="btn btn-default  btn-sm button_del" runat="server" CommandName="update_BtnBack" OnCommand="btnCommand" title="Cancel" OnClientClick="return confirm('คุณต้องการกลับไปหน้าหลัก ?')"><i class="fa fa-times"></i></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </EditItemTemplate>
                    </asp:FormView>


                </div>
            </div>
        </asp:View>

        <asp:View ID="ViewMastersup" runat="server">

            <div class="col-lg-12">
                <div class="bg-system">
                    <asp:Image ID="Image1" runat="server" class="img-responsive img-fluid" ImageUrl="~/images/EPN/Header_MAS_Supplier-1.png" Style="width: 100%;" />
                </div>
            </div>
            &nbsp;&nbsp;&nbsp;
            <div class="col-lg-12">
                <div class="panel panel" style="border-width: 1pt">
                    <div class="panel-heading" style="background-color: #9999FF; color: black;">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-search"></i><strong>&nbsp; ข้อมูลรายละเอียดผู้ผลิต</strong></h3>
                    </div>

                    <div class="panel-body">
                        <div class="form-group">
                            <asp:LinkButton ID="btnshow" CssClass="btn btn-success btn-sm button_color" data-toggle="tooltip" CommandName="btnCmn" OnCommand="btnCommand" title="Add System" runat="server"><i class="fa fa-plus"></i></asp:LinkButton>

                        </div>

                    </div>

                    <%-------   การสร้างเอกสาร Supplier ------%>
                    <asp:Panel ID="Panel_AddMasterSupplier" runat="server" Visible="false">
                        <div class="panel-heading">
                            <div class="form-horizontal" role="form">
                                <div class="panel-heading">

                                    <%-- <asp:UpdatePanel ID="UpdatePnlSup" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>--%>

                                    <div class="form-group">
                                        <asp:Label ID="Label141" runat="server" Text="ประเภทการสร้าง" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddlchoose_addsup" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="0" Text="กรุณาเลือกประเภทการสร้าง" />
                                                <asp:ListItem Value="1" Text="สร้างเอง" />
                                                <asp:ListItem Value="2" Text="Import File" />
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator38" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddlchoose_addsup" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกประเภทการสร้าง"
                                                ValidationExpression="กรุณาเลือกประเภทการสร้าง" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender57" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator38" Width="160" />

                                        </div>
                                    </div>

                                    <%--</ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ddlchoose_addsup" EventName="SelectedIndexChanged" />
                                        </Triggers>
                                    </asp:UpdatePanel>--%>




                                    <div class="col-md-12" id="div_Supplier" runat="server" style="color: transparent;">
                                        <asp:FileUpload ID="FileUpload3" CssClass="btn btn-warning hidden" ClientIDMode="Static" runat="server" accept="xls" />
                                    </div>



                                    <%-----------   สร้างเอง -----------%>
                                    <div id="div_addmanualsup" runat="server" visible="false">

                                        <div class="form-group">
                                            <asp:Label ID="Label73" runat="server" Text="Supplier Name" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtsupname" runat="server" CssClass="form-control" PlaceHolder="........" />
                                                <asp:RequiredFieldValidator ID="RequiredFiheldValidator22" ValidationGroup="Save" runat="server" Display="None"
                                                    ControlToValidate="txtsupname" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกหัวข้อปัญหา"
                                                    ValidationExpression="กรุณากรอกหัวข้อปัญหา"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender32" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFiheldValidator22" Width="160" />
                                                <%--  <asp:RegularExpressionValidator ID="RegularExpresfsionValidator11" runat="server"
                                                    ValidationGroup="Save" Display="None"
                                                    ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                    ControlToValidate="txtsupname"
                                                    ValidationExpression="^[\s\S]{0,1000}$"
                                                    SetFocusOnError="true" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender35" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpresfsionValidator11" Width="160" />
                                                --%>
                                            </div>
                                        </div>



                                        <div class="form-group">
                                            <asp:Label ID="Label77" runat="server" Text="Email" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtemail_sup" runat="server" CssClass="form-control" PlaceHolder="........" />
                                            </div>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator23" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="txtemail_sup" Font-Size="11"
                                                ErrorMessage="กรุณากรอกหัวข้อปัญหา"
                                                ValidationExpression="กรุณากรอกหัวข้อปัญหา"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender33" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator23" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server"
                                                ValidationGroup="Save" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtemail_sup"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender34" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator10" Width="160" />

                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label78" CssClass="col-sm-3 control-label" runat="server" Text="สถานะการใช้งาน" />
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="ddlstatus_sup" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="1" Text="Online" />
                                                    <asp:ListItem Value="2" Text="Offline" />
                                                </asp:DropDownList>

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-2 col-sm-offset-3">
                                                <asp:LinkButton ID="LinkButton9" ValidationGroup="Save" CssClass="btn btn-success btn-sm button_color" data-toggle="tooltip" title="Save" runat="server" CommandName="CmdInsertMaster_Supplier" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')">บันทึก</asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton10" CssClass="btn btn-default btn-sm button_del" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancelMaster_Supplier" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                            </div>
                                        </div>

                                    </div>

                                    <%-----------   upload -----------%>
                                    <div id="div_addimportsup" runat="server" visible="false">

                                        <asp:UpdatePanel ID="UpdatePnlUpload" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>

                                                <div class="form-group">
                                                    <asp:Label ID="Label142" class="col-sm-3 control-label text_right" runat="server" Text="Upload File : " />
                                                    <div class="col-sm-6">
                                                        <asp:FileUpload ID="UploadFileSup" ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small" ClientIDMode="Static" runat="server" CssClass="btn btn-warning btn-sm multi max-1" accept="xls" />

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator39"
                                                            runat="server" ControlToValidate="UploadFileSup" Display="None" SetFocusOnError="true"
                                                            ValidationGroup="Save" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender58" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator39" Width="200" PopupPosition="BottomLeft" />

                                                        <small>
                                                            <p class="help-block"><font color="red">**Attach File name xls</font></p>
                                                        </small>
                                                    </div>
                                                </div>

                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="btnAdddata_supplier" />
                                                <asp:PostBackTrigger ControlID="btnCanclesup" />
                                            </Triggers>
                                        </asp:UpdatePanel>


                                        <div class="form-group">
                                            <div class="col-sm-2 col-sm-offset-3">
                                                <asp:LinkButton ID="btnAdddata_supplier" ValidationGroup="Save" CssClass="btn btn-success btn-sm button_color" data-toggle="tooltip" title="Search" runat="server" CommandName="CmdImportMaster_Sup" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')">บันทึก</asp:LinkButton>
                                                <asp:LinkButton ID="btnCanclesup" CssClass="btn btn-default btn-sm button_del" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancelMaster_Supplier" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                            </div>
                                        </div>

                                    </div>


                                </div>
                            </div>
                        </div>
                    </asp:Panel>

                    <%--ค้นหา--%>
                    <div id="div_searchmaster_supplier" runat="server">
                        <div class="form-horizontal" role="form">
                            <div class="form-group">
                                <asp:Label ID="Label130" runat="server" Text="ชื่อ Supplier" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtmaster_searchsupplier" runat="server" CssClass="form-control" PlaceHolder="........" />

                                </div>
                                <asp:Label ID="Label131" runat="server" Text="Email" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtmaster_searchemail" runat="server" CssClass="form-control" PlaceHolder="........" />
                                </div>

                            </div>

                            <div class="form-group">
                                <asp:Label ID="Label16" runat="server" Text="" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                <div class="col-sm-3 ">
                                    <asp:LinkButton ID="LinkButton25" CssClass="btn btn-success btn-sm button_search" data-toggle="tooltip" title="Save" runat="server" CommandName="CmdSearchMaster_Supplier" OnCommand="btnCommand" ><i class="glyphicon glyphicon-search"></i> Search</asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton26" CssClass="btn btn-default btn-sm button_ref" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnRefreshMaster_Supplier" data-toggle="tooltip" title="Refresh"><i class="glyphicon glyphicon-refresh"></i> Refresh</asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>




                </div>



                <asp:GridView ID="GvMasterSup" runat="server"
                    AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12 "
                    HeaderStyle-BackColor="#9999FF"
                    HeaderStyle-Height="40px"
                    AllowPaging="true"
                    DataKeyNames="m0_supidx"
                    PageSize="10"
                    OnRowDataBound="Master_RowDataBound"
                    OnRowEditing="Master_RowEditing"
                    OnRowCancelingEdit="Master_RowCancelingEdit"
                    OnPageIndexChanging="Master_PageIndexChanging"
                    OnRowUpdating="Master_RowUpdating">

                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                    <EmptyDataTemplate>
                        <div style="text-align: center">Data Cannot Be Found</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="#">
                            <ItemTemplate>
                                <asp:Label ID="lbm0_supidx" runat="server" Visible="false" Text='<%# Eval("m0_supidx") %>' />
                                <%# (Container.DataItemIndex +1) %>
                            </ItemTemplate>

                            <%--กรณี Edit--%>
                            <EditItemTemplate>
                                <div class="form-horizontal" role="form">
                                    <div class="panel-heading">


                                        <asp:TextBox ID="txtm0_sup" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("m0_supidx")%>' />

                                        <div class="form-group">
                                            <asp:Label ID="lbsup_namea" runat="server" Text="ชื่อ supplier" CssClass="col-sm-3 control-label text_right"></asp:Label>

                                            <div class="col-sm-8">
                                                <asp:TextBox ID="txtsup_name_edit" runat="server" CssClass="form-control" Text='<%# Eval("sup_name")%>' />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="lbEmail_sup" runat="server" Text="Email" CssClass="col-sm-3 control-label text_right"></asp:Label>

                                            <div class="col-sm-8">
                                                <asp:TextBox ID="txtEmail_sup_edit" runat="server" CssClass="form-control" Text='<%# Eval("email")%>' />
                                            </div>



                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="Save_edit" runat="server" Display="None" ControlToValidate="txtsup_name_edit" Font-Size="11"
                                                ErrorMessage="กรุณากรอกระบบ"
                                                ValidationExpression="กรุณากรอกระบบ"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                ValidationGroup="Save_edit" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtsup_name_edit"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />

                                        </div>

                                        <%--      <div class="form-group">
                                        <asp:Label ID="lbstatussupEdit" runat="server" Text="สถานะการใช้งาน" CssClass="col-sm-3 control-label"></asp:Label>
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddlstatussup_Edit" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="SelectedIndexChanged">
                                                <asp:ListItem Value="0">Offline</asp:ListItem>
                                                <asp:ListItem Value="1">Online</asp:ListItem>

                                            </asp:DropDownList>
                                        </div>
                                    </div>--%>

                                        <div class="form-group">
                                            <asp:Label ID="lbstatate" CssClass="col-sm-3 control-label" runat="server" Text="Status" />
                                            <div class="col-sm-8">
                                               <%-- <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" Text='<%# Eval("type_system")%>' />--%>
                                                <asp:DropDownList ID="ddlstatussup_Edit" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("type_system") %>'>
                                                    <asp:ListItem Value="1" Text="Online" />
                                                    <asp:ListItem Value="0" Text="Offline" />
                                                </asp:DropDownList>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <asp:Label ID="Label2" CssClass="col-sm-10 control-label" runat="server" Text="" />
                                            <div class=" pull-left">
                                                <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success btn-sm button_color" runat="server" ValidationGroup="Save_edit"   CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm button_del" runat="server"  CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </EditItemTemplate>
                        </asp:TemplateField>

                        <%--Show data grid--%>
                        <asp:TemplateField HeaderText="ชื่อ supplier" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lbsup_name" runat="server" Text='<%# Eval("sup_name") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Email" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lbemail" runat="server" Text='<%# Eval("email") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="สถานะการใช้งาน" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lbstatus" runat="server" Text='<%# Eval("status_name") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="จัดการข้อมูล" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="15%">
                            <ItemTemplate>
                                <asp:LinkButton ID="Edit" CssClass="btn btn-default btn-sm button_edit " runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                <asp:LinkButton ID="Delete" CssClass="btn btn-default btn-sm button_del" runat="server" CommandName="CmdDel" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("m0_supidx") %>'><i class="fa fa-trash"></i></asp:LinkButton>
                            </ItemTemplate>

                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>


        </asp:View>

        <asp:View ID="ViewMasterCer" runat="server">
            <div class="col-lg-12">
                <div class="bg-system">
                    <asp:Image ID="Image2" runat="server" class="img-responsive img-fluid" ImageUrl="~/images/EPN/Header_MAS_Certificate_3.png" Style="width: 100%;" />
                </div>
            </div>
            &nbsp;&nbsp;&nbsp;
            <div class="col-lg-12 ">
                <div class="panel panel-default" style="border-width: 1pt">
                    <div class="panel-heading" style="background-color: #9999FF; color: black;">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-search"></i><strong>&nbsp; ข้อมูลรายละเอียดประเภท Certificate </strong></h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <asp:LinkButton ID="btnshowCer" CssClass="btn btn-success btn-sm button_color" data-toggle="tooltip" CommandName="btnCmncer" OnCommand="btnCommand" title="Add System" runat="server"><i class="fa fa-plus"></i></asp:LinkButton>

                        </div>
                    </div>
                    <asp:Panel ID="Pnl_add" runat="server" Visible="false">
                        <div class="panel-heading">
                            <h4 class="panel-title"><i class="glyphicon glyphicon-plus"></i><strong>&nbsp; Add Data Certificate </strong></h4>
                            <div class="form-horizontal" role="form">
                                <div class="panel-heading">
                                    <%-- text box --%>
                                    <div class="form-group">
                                        <asp:Label ID="lbcer" runat="server" Text="ชื่อประเภทเอกสาร :" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txtnamecer" runat="server" CssClass="form-control" PlaceHolder="ระบุชื่อประเภทเอกสาร" />
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="lbstatus" CssClass="col-sm-3 control-label" runat="server" Text="สถานะการใช้เอกสาร :" />
                                    <div class="col-sm-6">
                                        <asp:DropDownList ID="ddlstatus" CssClass="form-control" runat="server">
                                            <asp:ListItem Value="1" Text="Online" />
                                            <asp:ListItem Value="0" Text="Offline" />
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label1" CssClass="col-sm-3 control-label" runat="server" Text="" />
                                    <div class="col-sm-6">
                                        <asp:LinkButton ID="btnsave" CssClass="btn  btn-sm button_color" data-toggle="tooltip" CommandName="btnCmnAcer" OnCommand="btnCommand"  OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')" title="Save" runat="server">บันทึก</asp:LinkButton>
                                        <asp:LinkButton ID="btncancle" CssClass="btn btn-default btn-sm button_del" data-toggle="tooltip" CommandName="btnCmnDcer" OnCommand="btnCommand" title="Cancle" runat="server">ยกเลิก</asp:LinkButton>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </asp:Panel>



                    
                    <%--ค้นหา--%>
                    <asp:Panel ID="PnlSerchCer" runat="server" Visible="true">
                    <div id="div1" runat="server">
                        <div class="form-horizontal" role="form">
                            <div class="form-group">
                                <asp:Label ID="lbSerchCer" runat="server" Text=" ประเภท Certificate" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtserchCer" runat="server" CssClass="form-control" PlaceHolder="........" />

                                </div>
                                <asp:Label ID="Label17" runat="server" Text="สถานะการใช้งาน" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                <div class="col-sm-3">
                                    <asp:DropDownList ID="ddlSearchCerstatus" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server">
                                        <asp:ListItem Value="0">กรุณาเลือก สถานะ...</asp:ListItem>
                                        <asp:ListItem Value="1">เปิดใช้งาน</asp:ListItem>
                                        <asp:ListItem Value="9">ปิดใช้งาน</asp:ListItem>
                                    </asp:DropDownList>
                                </div>

                            </div>

                            <div class="form-group">
                                <asp:Label ID="Label20" runat="server" Text="" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                <div class="col-sm-3 ">
                                    <asp:LinkButton ID="btn_SearchCer" CssClass="btn btn-success btn-sm button_search" data-toggle="tooltip" title="Save" runat="server" CommandName="CmdSearchMaster_cer" OnCommand="btnCommand" ><i class="glyphicon glyphicon-search"></i> Search</asp:LinkButton>
                                    <asp:LinkButton ID="btn_ReCer" CssClass="btn btn-default btn-sm button_ref" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnRefreshMaster_cer" data-toggle="tooltip" title="Refresh"><i class="glyphicon glyphicon-refresh"></i> Refresh</asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                    </asp:Panel>

                </div>


                <asp:GridView ID="GvMasterCer" runat="server"
                    AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12 "
                    HeaderStyle-BackColor="#9999FF"
                    HeaderStyle-Height="40px"
                    AllowPaging="true"
                    DataKeyNames="type_idx"
                    PageSize="10"
                    OnRowDataBound="Master_RowDataBound"
                    OnRowEditing="Master_RowEditing"
                    OnRowCancelingEdit="Master_RowCancelingEdit"
                    OnPageIndexChanging="Master_PageIndexChanging"
                    OnRowUpdating="Master_RowUpdating">

                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                    <EmptyDataTemplate>
                        <div style="text-align: center">Data Cannot Be Found</div>
                    </EmptyDataTemplate>
                    <Columns>

                        <%--กรณีแก้ไข--%>
                        <asp:TemplateField HeaderText="#">


                            <ItemTemplate>
                                <asp:Label ID="lbltype_idx" runat="server" Visible="false" Text='<%# Eval("type_idx") %>' />
                                <%# (Container.DataItemIndex +1) %>
                            </ItemTemplate>

                            <EditItemTemplate>
                                <div class="form-horizontal" role="form">
                                    <div class="panel-heading">


                                        <asp:TextBox ID="txttype_idx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("type_idx")%>' />

                                        <div class="form-group">
                                            <asp:Label ID="Label1" runat="server" Text="Name" CssClass="col-sm-3 control-label text_right"></asp:Label>



                                            <div class="col-sm-8">
                                                <asp:TextBox ID="txtname_edit" runat="server" CssClass="form-control" Text='<%# Eval("type_name")%>' />
                                            </div>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="Save_edit" runat="server" Display="None" ControlToValidate="txtname_edit" Font-Size="11"
                                                ErrorMessage="กรุณากรอกระบบ"
                                                ValidationExpression="กรุณากรอกระบบ"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                ValidationGroup="Save_edit" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtname_edit"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />

                                        </div>




                                        <div class="form-group">
                                            <asp:Label ID="lbstatate" CssClass="col-sm-3 control-label" runat="server" Text="Status" />
                                            <div class="col-sm-8">
                                                <asp:DropDownList ID="ddStatusUpdate" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("type_status") %>'>
                                                    <asp:ListItem Value="1" Text="Online" />
                                                    <asp:ListItem Value="0" Text="Offline" />
                                                </asp:DropDownList>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <div class="col-sm-2 col-sm-offset-10">
                                                <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-warning btn-sm button_color" runat="server" ValidationGroup="Save_edit" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger btn-sm button_del" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </EditItemTemplate>
                        </asp:TemplateField>



                        <asp:TemplateField HeaderText="ชื่อประเภท certificate" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lbstatus" runat="server" Text='<%# Eval("type_name") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สถานะการใช้งาน" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lbtype_status" runat="server" Text='<%# Eval("status_type_Name") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="จัดการข้อมูล" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="15%">
                            <ItemTemplate>
                                <asp:LinkButton ID="Edit" CssClass="btn btn-warning btn-sm button_edit" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-sm button_del" runat="server" CommandName="CmdDelcer" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("type_idx") %>'><i class="fa fa-trash"></i></asp:LinkButton>
                            </ItemTemplate>

                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </asp:View>
    </asp:MultiView>


    <script>
        $(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            });
        });


    </script>


    <script>
        $(document).ready(function () {
            window.Sys.WebForms.PageRequestManager.getInstance().add_endRequest(pageLoad);

            function pageLoad() {
                $("#UploadFileProblem").MultiFile();
            }
        })
    </script>

    <script>
        $(document).ready(function () {
            window.Sys.WebForms.PageRequestManager.getInstance().add_endRequest(pageLoad);

            function pageLoad() {
                $("#UploadFileProblem_QA").MultiFile();
            }
        })
    </script>

    <script>
        $(document).ready(function () {
            window.Sys.WebForms.PageRequestManager.getInstance().add_endRequest(pageLoad);

            function pageLoad() {
                $("#UploadFileProblem_QA_edit").MultiFile();
            }
        })
    </script>

    <script>
        $(document).ready(function () {
            window.Sys.WebForms.PageRequestManager.getInstance().add_endRequest(pageLoad);

            function pageLoad() {
                $("#UploadFileMat").MultiFile();
            }
        })
    </script>

    <script>
        $(document).ready(function () {
            window.Sys.WebForms.PageRequestManager.getInstance().add_endRequest(pageLoad);

            function pageLoad() {
                $("#UploadFileSup").MultiFile();
            }
        })
    </script>

    <%-- <script>
        $(document).ready(function () {
            window.Sys.WebForms.PageRequestManager.getInstance().add_endRequest(pageLoad);

            function pageLoad() {
                $("#UploadFileProblem_Detail").MultiFile();
            }
        })
    </script>--%>

    <%--  <script src='<%=ResolveUrl("~/Scripts/jquery.MultiFileNew.js")%>'></script>

    <script>
        $(function () {
            $('.UploadFileProblem').MultiFile({

            });
            alert("1");
        });
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.UploadFileProblem').MultiFile({

                });
                alert("2");
            });
    </script>--%>


    <script type="text/javascript">
        tinymce.init({
            mode: "specific_textareas",
            editor_selector: "tinymce",
            encoding: "xml",
            theme: "modern",
            menubar: false,
            resize: false,
            statusbar: false,
            plugins: ["advlist autolink lists charmap preview hr anchor",
                "pagebreak code nonbreaking table contextmenu directionality paste"],
            toolbar1: "styleselect | bold italic underline | undo redo",
            toolbar2: "alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
            setup: function (editor) {
                editor.on('change', function () { tinymce.triggerSave(); });
            }
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            tinymce.remove(".tinymce");

            tinymce.init({
                mode: "specific_textareas",
                editor_selector: "tinymce",
                encoding: "xml",
                theme: "modern",
                menubar: false,
                resize: false,
                statusbar: false,
                plugins: ["advlist autolink lists charmap preview hr anchor",
                    "pagebreak code nonbreaking table contextmenu directionality paste"],
                toolbar1: "styleselect | bold italic underline | undo redo",
                toolbar2: "alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
                setup: function (editor) {
                    editor.on('change', function () { tinymce.triggerSave(); });
                }
            });

            $(".multi").MultiFile();
        })



    </script>
    <style>
        .button_color {
            background-color: #32CD32;
            border: none;
            color: white;
            padding: 5px 10px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            margin: 4px 4px;
            cursor: pointer;
        }


        .button_edit {
            background-color: #FFCC00;
            border: none;
            color: white;
            padding: 5px 10px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            margin: 4px 4px;
            cursor: pointer;
        }




        .button_del {
            background-color: #FF3333;
            border: none;
            color: white;
            padding: 5px 10px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            margin: 4px 4px;
            cursor: pointer;
        }

        .button_search {
            background-color: #33CC33;
            border: none;
            color: white;
            padding: 10px 25px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            margin: 4px 4px;
            cursor: pointer;
        }

        .button_ref {
            background-color: white;
            //border: initial;
            border-bottom-style : solid;
            color: black;
            padding: 10px 25px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            margin: 4px 4px;
            cursor: pointer;
        }
    </style>


</asp:Content>

