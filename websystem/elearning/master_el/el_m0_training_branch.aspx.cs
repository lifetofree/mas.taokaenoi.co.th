﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_el_m0_training_branch : System.Web.UI.Page
{
    #region initial function/data
    function_tool _funcTool = new function_tool();
    data_elearning _data_elearning = new data_elearning();
    function_dmu _func_dmu = new function_dmu();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetel_m0_training_branch = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_m0_training_branch"];
    static string _urlSetInsel_m0_training_branch = _serviceUrl + ConfigurationManager.AppSettings["urlSetInsel_m0_training_branch"];
    static string _urlSetUpdel_m0_training_branch = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdel_m0_training_branch"];
    static string _urlDelel_m0_training_branch = _serviceUrl + ConfigurationManager.AppSettings["urlDelel_m0_training_branch"];
    static string _urlGetErrorel_m0_training_branch = _serviceUrl + ConfigurationManager.AppSettings["urlGetErrorel_m0_training_branch"];


    string _localJson = "";
    int _tempInt = 0;

    string _defaultDdlText;
    string _defaultDdlValue;

    int emp_idx = 0;
    #endregion initial function/data


    protected void Page_Load(object sender, EventArgs e)
    {
        emp_idx = int.Parse(Session["emp_idx"].ToString());

        if (!IsPostBack)
        {
            initPage();
            actionIndex();

        }
    }

    #region selected   
    protected void actionIndex()
    {
        pnlsearch.Visible = true;
        data_elearning data_elearning = new data_elearning();
        data_elearning.el_m0_training_branch_action = new m0_training_branch[1];

        m0_training_branch obj = new m0_training_branch();

        obj.m0_training_branch_idx = 0;
        obj.filter_keyword = txtFilterKeyword.Text;

        data_elearning.el_m0_training_branch_action[0] = obj;
        //litDebug.Text = _funcTool.convertObjectToXml(data_elearning);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_elearning));
        //litDebug.Text = _func_dmu.zJson(_urlGetel_m0_training_branch, data_elearning);
        data_elearning = _func_dmu.zCallServiceNetwork(_urlGetel_m0_training_branch, data_elearning);

        setGridData(GvMaster, data_elearning.el_m0_training_branch_action);

    }
    #endregion selected 
    
    public void setdataInsert()
    {
        DropDownList ddlm0_training_group_idx = (DropDownList)ViewInsert.FindControl("ddlm0_training_group_idx");
        _func_dmu.zDropDownList(ddlm0_training_group_idx,
                               "",
                               "m0_training_group_idx",
                               "training_group_name",
                               "0",
                               "trainingLoolup",
                               "",
                               "TRN-GROUP"
                               );
    }

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        int _m0_training_branch_idx;
        string _txttraining_branch_name, _txttraining_branch_code, _txttraining_branch_remark;
        int _cemp_idx;

        m0_training_branch objM0_ProductType = new m0_training_branch();

        switch (cmdName)
        {

            case "btnToInsert":
                MvMaster.SetActiveView(ViewInsert);
                setdataInsert();
                break;
            case "btnCancel":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btnInsert":
                //_txttraining_branch_code = ((TextBox)ViewInsert.FindControl("txttraining_branch_code")).Text.Trim();
                _txttraining_branch_name = ((TextBox)ViewInsert.FindControl("txttraining_branch_name")).Text.Trim();
                _txttraining_branch_remark = ((TextBox)ViewInsert.FindControl("txttraining_branch_remark")).Text.Trim();
                DropDownList _ddltraining_branch_status = (DropDownList)ViewInsert.FindControl("ddltraining_branch_status");
                DropDownList ddlm0_training_group_idx = (DropDownList)ViewInsert.FindControl("ddlm0_training_group_idx");
                _cemp_idx = emp_idx;
                //if(checkCodeError(0, _txttraining_branch_code) == true)
                //{
                //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('รหัสนี้มีในระบบแล้ว ไม่สามารถเพิ่มได้');", true);
                //    return;
                //}
                m0_training_branch obj = new m0_training_branch();
                _data_elearning.el_m0_training_branch_action = new m0_training_branch[1];
                obj.m0_training_branch_idx = 0;//_type_idx; 
                //obj.training_branch_code = _txttraining_branch_code;
                obj.training_branch_name = _txttraining_branch_name;
                obj.training_branch_remark = _txttraining_branch_remark;
                obj.training_branch_status = int.Parse(_ddltraining_branch_status.SelectedValue);
                obj.training_branch_created_by = _cemp_idx;
                obj.m0_training_group_idx_ref = int.Parse(ddlm0_training_group_idx.SelectedValue);

                _data_elearning.el_m0_training_branch_action[0] = obj;

               // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
               
                _data_elearning = _func_dmu.zCallServicePostNetwork(_urlSetInsel_m0_training_branch, _data_elearning);

                if (_data_elearning.return_code == 0)
                {
                    actionIndex();
                    //MvMaster.SetActiveView(ViewIndex);
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                }
                else
                {
                    setError(_data_elearning.return_code.ToString() + " - " + _data_elearning.return_msg);
                }

                //actionCreate();
                //Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btnDelete":

                _m0_training_branch_idx = int.Parse(cmdArg);
                _cemp_idx = emp_idx;


                _data_elearning.el_m0_training_branch_action = new m0_training_branch[1];
                objM0_ProductType.m0_training_branch_idx = _m0_training_branch_idx;
               // objM0_ProductType.CEmpIDX = _cemp_idx;

                _data_elearning.el_m0_training_branch_action[0] = objM0_ProductType;

               // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));

                _data_elearning = _func_dmu.zCallServiceNetwork(_urlDelel_m0_training_branch, _data_elearning);


                if (_data_elearning.return_code == 0)
                {

                    //actionIndex();
                    //MvMaster.SetActiveView(ViewIndex);
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);


                }
                else
                {
                    setError(_data_elearning.return_code.ToString() + " - " + _data_elearning.return_msg);
                }

                break;
            case "btnFilter":
                actionIndex();
                break;



        }
    }
    #endregion btnCommand

    #region bind data
    protected void getGridData(string _cmdName, int _actionType)
    {
        // setGridData(gvBookingType, ViewState["listData"]);
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected string getStatus(int status)
    {
        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();
                actionIndex();
                break;
        }
    }

    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    pnlsearch.Visible = false;
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster.EditIndex == e.Row.RowIndex) //to overlook header row
                    {
                       
                        DropDownList ddlm0_training_group_idxUpdate = (DropDownList)e.Row.Cells[1].FindControl("ddlm0_training_group_idxUpdate");
                        TextBox txtm0_training_group_idxUpdate = (TextBox)e.Row.Cells[1].FindControl("txtm0_training_group_idxUpdate");
                        string svalue = "";
                        if (txtm0_training_group_idxUpdate.Text != "0")
                        {
                            svalue = txtm0_training_group_idxUpdate.Text;
                        }
                        _func_dmu.zDropDownList(ddlm0_training_group_idxUpdate,
                                               "",
                                               "m0_training_group_idx",
                                               "training_group_name",
                                               "",
                                               "trainingLoolup",
                                               svalue,
                                               "TRN-GROUP"
                                               );
                    }
                }

                break;
        }
    }

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.EditIndex = e.NewEditIndex;
                actionIndex();
                break;
        }
    }

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.EditIndex = -1;
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
        }
    }

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":


                int m0_training_branch_idx_update = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
               // var txttraining_branch_code = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txttraining_branch_codeUpdate");
                var txttraining_branch_name = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txttraining_branch_nameUpdate");
                var txttraining_branch_remark = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txttraining_branch_remarkUpdate");
                var ddltraining_branch_status = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddltraining_branch_statusUpdate");
                var ddlm0_training_group_idxUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlm0_training_group_idxUpdate");

                //if (checkCodeError(m0_training_branch_idx_update, txttraining_branch_code.Text.Trim()) == true)
                //{
                //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('รหัสนี้มีในระบบแล้ว ไม่สามารถเพิ่มได้');", true);
                //    return;
                //}

                GvMaster.EditIndex = -1;

                _data_elearning.el_m0_training_branch_action = new m0_training_branch[1];
                m0_training_branch obj = new m0_training_branch();

                obj.m0_training_branch_idx = m0_training_branch_idx_update;
                //obj.training_branch_code = txttraining_branch_code.Text;
                obj.training_branch_name = txttraining_branch_name.Text;
                obj.training_branch_remark = txttraining_branch_remark.Text;
                obj.training_branch_status = int.Parse(ddltraining_branch_status.SelectedValue);
                obj.training_branch_updated_by = emp_idx;
                obj.m0_training_group_idx_ref = int.Parse(ddlm0_training_group_idxUpdate.SelectedValue);

                _data_elearning.el_m0_training_branch_action[0] = obj;

                // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
                _data_elearning = _func_dmu.zCallServicePostNetwork(_urlSetUpdel_m0_training_branch, _data_elearning);

                if (_data_elearning.return_code == 0)
                {
                    //initPage();
                    //setDataList(dtlMenu, _dataMenu.m0_menu_list);  
                    actionIndex();


                }
                else
                {
                    setError(_data_elearning.return_code.ToString() + " - " + _data_elearning.return_msg);
                }


                break;
        }
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj, string sysidx_menu, string menuidx_per, string role_per)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();

        //switch (fvName.ID)
        //{



        //}
    }

    protected void setDataList(DataList dtlName, Object obj)
    {
        dtlName.DataSource = obj;
        dtlName.DataBind();
    }

    #endregion bind data

    #region reuse
    protected void initPage()
    {
        clearSession();
        clearViewState();

        // divAction.Visible = true;
        // gvBookingType.Visible = true;
        // fvBookingType.Visible = false;

        // getGridData("booking_type", 20);
        //setFormData(fvMenu, FormViewMode.Insert, null, "0", "0", "0");


    }

    protected void initPagePermission()
    {
        clearSession();
        clearViewState();

        // divAction.Visible = true;
        // gvBookingType.Visible = true;
        // fvBookingType.Visible = false;

        // getGridData("booking_type", 20);
        //setFormData(fvPermission, FormViewMode.Insert, null, "0", "0", "0");


    }

    protected void setVisible()
    {
        // divAction.Visible = !divAction.Visible;
        // gvBookingType.Visible = !gvBookingType.Visible;
        // fvBookingType.Visible = !fvBookingType.Visible;
    }

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        // ViewState["listData"] = null;
    }

    protected void setError(string _errorText)
    {
        // divShowError.Visible = !divShowError.Visible;
        // litErrorCode.Text = _dataEmployee.return_code.ToString();
        if (_errorText != "")
        {
            litDebug.Text = "error : " + _errorText;
        }
        else
        {
            litDebug.Text = String.Empty;
        }
    }
    
    public Boolean checkCodeError(int _m0_training_branch_idx, string _training_branch_code)
    {
        Boolean error = false;
        data_elearning data_elearning = new data_elearning();
        data_elearning.el_m0_training_branch_action = new m0_training_branch[1];

        m0_training_branch obj = new m0_training_branch();

        obj.m0_training_branch_idx = _m0_training_branch_idx;
       // obj.training_branch_code = _training_branch_code;
        data_elearning.el_m0_training_branch_action[0] = obj;
        data_elearning = _func_dmu.zCallServiceNetwork(_urlGetErrorel_m0_training_branch, data_elearning);
        if(data_elearning.el_m0_training_branch_action != null)
        {
            //obj = data_elearning.el_m0_training_branch_action[0];
            //if (obj.training_branch_code.ToString().Trim()== _training_branch_code.Trim())
            //{

            //}
            error = true;

        }
        return error;
    }

    #endregion reuse

}