﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_qa_labis_details : System.Web.UI.Page
{
    function_tool _funcTool = new function_tool();
    data_qa _data_qa = new data_qa();

   
    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];

    //-- key sample code --//
    static string _keysamplecode = _serviceUrl + ConfigurationManager.AppSettings["keysamplecode"];
    static string _urlQaGetSampleInLab = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetSampleInLab"];
    static string _urlQaGetTestDetailLab = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetTestDetailLab"];

    string _localJson = "";
    int _tempInt = 0;
    int u1_qalab_idx = 0;


    protected void Page_Load(object sender, EventArgs e)
    {

        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        Label lblPanelTitle = (Label)Master.FindControl("lblPanelTitle");
        if (lblPanelTitle != null)
        {

            lblPanelTitle.Visible = false;

            ////lblPanelTitle.Text = "Toei";

            //litDebug.Text = u1_qalab_idx.ToString();
        }
        else
        {
            ////lblTest.Text = "123123213123";
        }



        string _key_idxu1_samplecode = Page.RouteData.Values["u1_qalab_idx"].ToString().ToLower();
        string _u1idx_samplecode = _funcTool.getDecryptRC4(_key_idxu1_samplecode.ToString(), _keysamplecode);

        //litDebug.Text = _funcTool.getDecryptRC4(_key_idxu1_samplecode.ToString(), _keysamplecode);//Page.RouteData.Values["u1_qalab_idx"].ToString().ToLower();
        ViewState["_u1idx_samplecode"] = _u1idx_samplecode;

      //  litDebug.Text = _u1idx_samplecode;// ViewState["_u1idx_samplecode"].ToString();
        //detail sample in lab
        data_qa _data_sample = new data_qa();
        _data_sample.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
        qa_lab_u1_qalab _u1_sample_vlab = new qa_lab_u1_qalab();

        _u1_sample_vlab.u1_qalab_idx = int.Parse(_u1idx_samplecode);
        _data_sample.qa_lab_u1_qalab_list[0] = _u1_sample_vlab;
        _data_sample = callServicePostQA(_urlQaGetSampleInLab, _data_sample);

        fvSampleCode.DataSource = _data_sample.qa_lab_u1_qalab_list;
        fvSampleCode.DataBind();

        // Bind repeater Test detail
        Repeater rp_testdetail_lab = (Repeater)fvSampleCode.FindControl("rp_testdetail_lab_view");

        data_qa _data_test_vlab = new data_qa();
        _data_test_vlab.bindlab_qa_m0_testdetail_list = new bindlab_m0_test_detail[1];
        bindlab_m0_test_detail _testvlab_detail = new bindlab_m0_test_detail();

         _testvlab_detail.u1_qalab_idx = int.Parse(_u1idx_samplecode);

        _data_test_vlab.bindlab_qa_m0_testdetail_list[0] = _testvlab_detail;
        _data_test_vlab = callServicePostQA(_urlQaGetTestDetailLab, _data_test_vlab);

        setRepeaterData(rp_testdetail_lab, _data_test_vlab.bindlab_qa_m0_testdetail_list);


    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }


    protected void setRepeaterData(Repeater rptName, Object obj)
    {
        rptName.DataSource = obj;
        rptName.DataBind();
    }
    protected data_qa callServicePostQA(string _cmdUrl, data_qa _data_qa)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_qa);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_qa = (data_qa)_funcTool.convertJsonToObject(typeof(data_qa), _localJson);

        return _data_qa;
    }
}