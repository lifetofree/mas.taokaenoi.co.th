<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="emp_data.aspx.cs" Inherits="websystem_mis_emp_data" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" Runat="Server">
	<div class="col-md-12" role="main">
        <asp:Literal ID="litDebug" runat="server"></asp:Literal>
        <!-- search -->
        <asp:FormView ID="fvEmpSearch" runat="server" DefaultMode="Insert" OndataBound="fvDataBound" Width="100%">
            <InsertItemTemplate>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Search</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal" role="form">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Employee Code</label>
                                <div class="col-sm-4">
                                    <asp:TextBox ID="tbEmpCode" runat="server" CssClass="form-control" placeholder="employee code" />
                                </div>
                                <label class="col-sm-2 control-label">Name(TH/EN)</label>
                                <div class="col-sm-4">
                                    <asp:TextBox ID="tbEmpName" runat="server" CssClass="form-control" placeholder="employee name" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Organization</label>
                                <div class="col-sm-4">
                                    <asp:DropDownList ID="ddlOrg" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" />
                                </div>
                                <label class="col-sm-2 control-label">Department</label>
                                <div class="col-sm-4">
                                    <asp:DropDownList ID="ddlDept" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                        <asp:ListItem Text="--- department ---" Value="-1" />
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Section</label>
                                <div class="col-sm-4">
                                    <asp:DropDownList ID="ddlSec" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                        <asp:ListItem Text="--- section ---" Value="-1" />
                                    </asp:DropDownList>
                                </div>
                                <label class="col-sm-2 control-label">Cost Center</label>
                                <div class="col-sm-4">
                                    <asp:TextBox ID="tbCostCenter" runat="server" CssClass="form-control" placeholder="employee cost center" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-2">
                                        <asp:DropDownList ID="ddlSelectedDate" runat="server" CssClass="form-control" Style="text-align: right;">
                                            <asp:ListItem Text="--- date condition ---" Value="-1" />
                                            <asp:ListItem Text="Resign Date" Value="emp_resign_date" />
                                            <asp:ListItem Text="Start Date" Value="emp_start_date" />
                                            <asp:ListItem Text="Create Date" Value="emp_createdate" />
                                            <asp:ListItem Text="Update Date" Value="emp_updatedate" />
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="tbMonthYear" runat="server" placeholder="--- select month and year ---"
                                        CssClass="form-control datetimepicker" />
                                    </div>
                                <label class="col-sm-2 control-label">Status</label>
                                <div class="col-sm-4">
                                    <asp:DropDownList ID="ddlEmpStatus" runat="server" CssClass="form-control">
                                        <asp:ListItem Text="not active(resigned)" Value="0" />
                                        <asp:ListItem Text="active" Value="1" />
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2"></label>
                                <div class="col-sm-4">
                                    <asp:LinkButton ID="lbSearch" CssClass="btn btn-success" runat="server" data-original-title="Search" data-toggle="tooltip" Text="Search" OnCommand="btnCommand" CommandName="cmdSearch"></asp:LinkButton>
                                    <asp:LinkButton ID="lbReset" CssClass="btn btn-info" runat="server" data-original-title="Reset" data-toggle="tooltip" Text="Reset" OnCommand="btnCommand" CommandName="cmdReset"></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </InsertItemTemplate>
        </asp:FormView>
        <!-- search -->
        <!--multiview-->
        <asp:MultiView ID="mvSystem" runat="server">
            <asp:View ID="docList" runat="server">
                <asp:GridView ID="gvEmployeeList" runat="server" AutoGenerateColumns="False"
                CssClass="table table-striped table-bordered table-hover table-responsive" OnPageIndexChanging="gvPageIndexChanging" AllowPaging="True" PageSize="10"
                DataKeyNames="emp_code"
                >
                    <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                    <RowStyle Font-Size="Small" />
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่พบข้อมูล</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="Employee Code">
                            <ItemTemplate>
                                <asp:Label ID="lblEmpCode" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Employee Name TH">
                            <ItemTemplate>
                                <asp:Label ID="lblPrefixNameTH" runat="server" Text='<%# Eval("prefix_th") %>'></asp:Label>
                                <asp:Label ID="lblFirstnameTH" runat="server" Text='<%# Eval("emp_firstname_th") %>'></asp:Label>
                                <asp:Label ID="lblLastnameTH" runat="server" Text='<%# Eval("emp_lastname_th") %>'></asp:Label>
                                (<asp:Label ID="lblNicknameTH" runat="server" Text='<%# Eval("emp_nickname_th") %>'></asp:Label>)
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%-- <asp:TemplateField HeaderText="Employee Name EN">
                            <ItemTemplate>
                                <asp:Label ID="lblPrefixNameEN" runat="server" Text='<%# Eval("prefix_en") %>'></asp:Label>
                                <asp:Label ID="lblFirstnameEN" runat="server" Text='<%# Eval("emp_firstname_en") %>'></asp:Label>
                                <asp:Label ID="lblLastnameEN" runat="server" Text='<%# Eval("emp_lastname_en") %>'></asp:Label>
                                (<asp:Label ID="lblNicknameEN" runat="server" Text='<%# Eval("emp_nickname_en") %>'></asp:Label>)
                            </ItemTemplate>
                        </asp:TemplateField> --%>
                        <asp:TemplateField HeaderText="Mobile No">
                            <ItemTemplate>
                                <asp:Label ID="lblMobileNo" runat="server" Text='<%# Eval("emp_mobile_no") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Email">
                            <ItemTemplate>
                                <asp:Label ID="lblEmail" runat="server" Text='<%# Eval("emp_email") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Position TH">
                            <ItemTemplate>
                                <asp:Label ID="lblPositionTH" runat="server" Text='<%# Eval("pos_name_th") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Section TH">
                            <ItemTemplate>
                                <asp:Label ID="lblSectionTH" runat="server" Text='<%# Eval("sec_name_th") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Department TH">
                            <ItemTemplate>
                                <asp:Label ID="lblDepartmentTH" runat="server" Text='<%# Eval("dept_name_th") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Organization TH">
                            <ItemTemplate>
                                <asp:Label ID="lblOrganizationTH" runat="server" Text='<%# Eval("org_name_th") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%-- <asp:TemplateField HeaderText="Position EN">
                            <ItemTemplate>
                                <asp:Label ID="lblPositionEN" runat="server" Text='<%# Eval("pos_name_en") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Department EN">
                            <ItemTemplate>
                                <asp:Label ID="lblDepartmentEN" runat="server" Text='<%# Eval("dept_name_en") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Section EN">
                            <ItemTemplate>
                                <asp:Label ID="lblSectionEN" runat="server" Text='<%# Eval("sec_name_en") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Organization EN">
                            <ItemTemplate>
                                <asp:Label ID="lblOrganizationEN" runat="server" Text='<%# Eval("org_name_en") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField> --%>
                        <asp:TemplateField HeaderText="Cost Center">
                            <ItemTemplate>
                                <asp:Label ID="lblCostCenter" runat="server" Text='<%# Eval("costcenter_no") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%-- <asp:TemplateField HeaderText="Sex TH">
                            <ItemTemplate>
                                <asp:Label ID="lblSexTH" runat="server" Text='<%# Eval("sex_name_th") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Sex EN">
                            <ItemTemplate>
                                <asp:Label ID="lblSexEN" runat="server" Text='<%# Eval("sex_name_en") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField> --%>
                        <asp:TemplateField HeaderText="Start Date">
                            <ItemTemplate>
                                <asp:Label ID="lblStartDate" runat="server" Text='<%# Eval("emp_start_date") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Resign Date">
                            <ItemTemplate>
                                <asp:Label ID="lblResignDate" runat="server" Text='<%# Eval("emp_end_date") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Create Date">
                            <ItemTemplate>
                                <asp:Label ID="lblCreateDate" runat="server" Text='<%# Eval("emp_createdate") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Update Date">
                            <ItemTemplate>
                                <asp:Label ID="lblCreateDate" runat="server" Text='<%# Eval("emp_updatedate") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </asp:View>
        </asp:MultiView>
    </div>
    <script type="text/javascript">
        $(function () {
            $('.datetimepicker').datetimepicker({
                format: 'MM/YYYY'
            });
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $('.datetimepicker').datetimepicker({
                format: 'MM/YYYY'
            });
        });
    </script>
</asp:Content>