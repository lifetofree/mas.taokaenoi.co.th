<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="master_machine.aspx.cs" Inherits="websystem_fingerscan_master_machine" %>

    <asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" Runat="Server">
    </asp:Content>

    <asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" Runat="Server">
        <asp:Literal ID="litDebug" runat="server"></asp:Literal>
        <div class="row">
            <!--multiview-->
            <asp:MultiView ID="mvSystem" runat="server">
                <asp:View ID="viewCreate" runat="server">
                    <div class="col-md-12">
                        <div class="panel panel-info" ID="div_heading" runat="server">
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    <asp:Literal ID="litHeadingTitle" runat="server" Text="เพิ่มข้อมูล"></asp:Literal>
                                </h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-horizontal" role="form">
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">ประเภท master data
                                                <span class="text-danger">*</span> :</label>
                                            <div class="col-md-8 control-label textleft">
                                                <asp:RadioButtonList ID="rblMasterType" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="rblSelectedIndexChanged"
                                                    AutoPostBack="True">
                                                    <asp:ListItem Text="&nbsp;เครื่อง&nbsp;" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="&nbsp;สถานที่ตั้ง&nbsp;" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="&nbsp;ตำแหน่งที่ตั้ง&nbsp;" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="&nbsp;Zone&nbsp;" Value="4"></asp:ListItem>
                                                    <asp:ListItem Text="&nbsp;Group&nbsp;" Value="5"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                    </div>
                                    <asp:FormView ID="fvDetail1" runat="server" Width="100%" DefaultMode="ReadOnly" OndataBound="fvDataBound">
                                        <InsertItemTemplate>
                                            <asp:Panel ID="pnIpHost" runat="server" Visible="False">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">IP / Host name
                                                    <span class="text-danger">*</span> :</label>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="tbIpHost" runat="server" CssClass="form-control" placeholder="IP / Host name" MaxLength="50" ValidationGroup="fvDetail1"></asp:TextBox>
                                                    <asp:RequiredFieldValidator id="rfvTbIpHost" runat="server" ErrorMessage="กรอก IP / Host name ด้วยค่ะ" ControlToValidate="tbIpHost" Display="None" SetFocusOnError="true" ValidationGroup="fvDetail1"></asp:RequiredFieldValidator>
                                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="vceTbIpHost" TargetControlID="rfvTbIpHost" HighlightCssClass="validatorCalloutHighlight" />
                                                </div>
                                                <label class="col-md-4 control-label"></label>
                                            </div>
                                            </asp:Panel>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Machine name
                                                    <span class="text-danger">*</span> :</label>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="tbMachineName" runat="server" CssClass="form-control" placeholder="Machine name" MaxLength="50" ValidationGroup="fvDetail1"></asp:TextBox>
                                                    <asp:RequiredFieldValidator id="rfvTbMachineName" runat="server" ErrorMessage="กรอก Machine name ด้วยค่ะ" ControlToValidate="tbMachineName" Display="None" SetFocusOnError="true" ValidationGroup="fvDetail1"></asp:RequiredFieldValidator>
                                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="vceTbMachineName" TargetControlID="rfvTbMachineName" HighlightCssClass="validatorCalloutHighlight" />
                                                </div>
                                                <label class="col-md-4 control-label"></label>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Zone
                                                    <span class="text-danger">*</span> :</label>
                                                <div class="col-md-4">
                                                    <asp:DropDownList ID="ddlMachineZone" runat="server" CssClass="form-control" ValidationGroup="fvDetail1"></asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="rfvDdlMachineZone" runat="server" ControlToValidate="ddlMachineZone" ErrorMessage="กรุณาเลือก Zone ด้วยค่ะ" InitialValue="0"  Display="None" SetFocusOnError="true" ValidationGroup="fvDetail1"></asp:RequiredFieldValidator>
                                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="vceDdlMachineZone" TargetControlID="rfvDdlMachineZone" HighlightCssClass="validatorCalloutHighlight" />
                                                </div>
                                                <label class="col-md-4 control-label"></label>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">สถานที่ตั้ง
                                                    <span class="text-danger">*</span> :</label>
                                                <div class="col-md-4">
                                                    <asp:DropDownList ID="ddlMachinePlace" runat="server" CssClass="form-control" ValidationGroup="fvDetail1"></asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="rfvDdlMachinePlace" runat="server" ControlToValidate="ddlMachinePlace" ErrorMessage="กรุณาเลือกสถานที่ตั้งด้วยค่ะ" InitialValue="0"  Display="None" SetFocusOnError="true" ValidationGroup="fvDetail1"></asp:RequiredFieldValidator>
                                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="vceDdlMachinePlace" TargetControlID="rfvDdlMachinePlace" HighlightCssClass="validatorCalloutHighlight" />
                                                </div>
                                                <label class="col-md-4 control-label"></label>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">ตำแหน่งที่ตั้ง
                                                    <span class="text-danger">*</span> :</label>
                                                <div class="col-md-4">
                                                    <asp:DropDownList ID="ddlMachineLoc" runat="server" CssClass="form-control" ValidationGroup="fvDetail1"></asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="rfvDdlMachineLoc" runat="server" ControlToValidate="ddlMachineLoc" ErrorMessage="กรุณาเลือกตำแหน่งที่ตั้งด้วยค่ะ" InitialValue="0"  Display="None" SetFocusOnError="true" ValidationGroup="fvDetail1"></asp:RequiredFieldValidator>
                                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="vceDdlMachineLoc" TargetControlID="rfvDdlMachineLoc" HighlightCssClass="validatorCalloutHighlight" />
                                                </div>
                                                <label class="col-md-4 control-label"></label>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">ผู้ดูแลเครื่อง
                                                    <span class="text-danger">*</span> :</label>
                                                <div class="col-md-4">
                                                    <asp:DropDownList ID="ddlMachineOrg" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" ValidationGroup="fvDetail1">
                                                        <asp:ListItem>--- องค์กร ---</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="rfvDdlMachineOrg" runat="server" ControlToValidate="ddlMachineOrg" ErrorMessage="กรุณาเลือกองค์กรด้วยค่ะ" InitialValue="0"  Display="None" SetFocusOnError="true" ValidationGroup="fvDetail1"></asp:RequiredFieldValidator>
                                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="vceDdlMachineOrg" TargetControlID="rfvDdlMachineOrg" HighlightCssClass="validatorCalloutHighlight" />
                                                </div>
                                                <label class="col-md-4 control-label"></label>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label"><span class="text-danger">*</span></label>
                                                <div class="col-md-4">
                                                    <asp:DropDownList ID="ddlMachineDept" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" ValidationGroup="fvDetail1">
                                                        <asp:ListItem>--- ฝ่าย ---</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="rfvDdlMachineDept" runat="server" ControlToValidate="ddlMachineDept" ErrorMessage="กรุณาเลือกฝ่ายด้วยค่ะ" InitialValue="0"  Display="None" SetFocusOnError="true" ValidationGroup="fvDetail1"></asp:RequiredFieldValidator>
                                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="vceDdlMachineDept" TargetControlID="rfvDdlMachineDept" HighlightCssClass="validatorCalloutHighlight" />
                                                </div>
                                                <label class="col-md-4 control-label"></label>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label"><span class="text-danger">*</span></label>
                                                <div class="col-md-4">
                                                    <asp:DropDownList ID="ddlMachineSec" runat="server" CssClass="form-control" ValidationGroup="fvDetail1">
                                                        <asp:ListItem>--- แผนก ---</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="rfvDdlMachineSec" runat="server" ControlToValidate="ddlMachineSec" ErrorMessage="กรุณาเลือกแผนกด้วยค่ะ" InitialValue="0"  Display="None" SetFocusOnError="true" ValidationGroup="fvDetail1"></asp:RequiredFieldValidator>
                                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="vceDdlMachineSec" TargetControlID="rfvDdlMachineSec" HighlightCssClass="validatorCalloutHighlight" />
                                                </div>
                                                <label class="col-md-4 control-label"></label>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">บันทึกเวลาเข้างาน
                                                    <span class="text-danger">*</span> :</label>
                                                <div class="col-md-8 control-label textleft">
                                                    <asp:RadioButtonList ID="rblAttendanceStatus" runat="server" RepeatDirection="Horizontal">
                                                        <asp:ListItem Text="&nbsp;บันทึก&nbsp;" Value="1" Selected="True"></asp:ListItem>
                                                        <asp:ListItem Text="&nbsp;ไม่บันทึก&nbsp;" Value="0"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </div>
                                                <label class="col-md-4 control-label"></label>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">สถานะ
                                                    <span class="text-danger">*</span> :</label>
                                                <div class="col-md-8 control-label textleft">
                                                    <asp:RadioButtonList ID="rblStatus" runat="server" RepeatDirection="Horizontal">
                                                        <asp:ListItem Text="&nbsp;แสดง&nbsp;" Value="1" Selected="True"></asp:ListItem>
                                                        <asp:ListItem Text="&nbsp;ไม่แสดง&nbsp;" Value="0"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label"></label>
                                                <div class="col-md-8">
                                                    <asp:LinkButton ID="lbSave" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="0" ValidationGroup="fvDetail1">
                                                        <i class="fas fa-save" aria-hidden="true"></i>&nbsp;บันทึก</asp:LinkButton>
                                                    <asp:LinkButton ID="lbCancel" runat="server" CssClass="btn btn-danger" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="0">
                                                        <i class="fas fa-times" aria-hidden="true"></i>&nbsp;ยกเลิก</asp:LinkButton>
                                                    <asp:LinkButton ID="lbReset" runat="server" CssClass="btn btn-info" OnCommand="btnCommand" CommandName="cmdReset" CommandArgument="0">
                                                        <i class="fas fa-sync-alt" aria-hidden="true"></i>&nbsp;เคลียร์ค่า</asp:LinkButton>
                                                </div>
                                            </div>
                                        </InsertItemTemplate>
                                        <EditItemTemplate>
                                            <asp:HiddenField ID="hfZoneIdx" runat="server" Value='<%# Eval("zone_idx") %>'></asp:HiddenField>
                                            <asp:HiddenField ID="hfPlaceIdx" runat="server" Value='<%# Eval("place_idx") %>'></asp:HiddenField>
                                            <asp:HiddenField ID="hfLocIdx" runat="server" Value='<%# Eval("loc_idx") %>'></asp:HiddenField>
                                            <asp:HiddenField ID="hfOrgIdx" runat="server" Value='<%# Eval("org_idx") %>'></asp:HiddenField>
                                            <asp:HiddenField ID="hfRdeptIdx" runat="server" Value='<%# Eval("rdept_idx") %>'></asp:HiddenField>
                                            <asp:HiddenField ID="hfRsecIdx" runat="server" Value='<%# Eval("rsec_idx") %>'></asp:HiddenField>
                                            <asp:Panel ID="pnIpHost" runat="server" Visible="False">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">IP / Host name
                                                    <span class="text-danger">*</span> :</label>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="tbIpHost" runat="server" CssClass="form-control" Text='<%# Eval("ip_host") %>' placeholder="IP / Host name" MaxLength="50" ValidationGroup="fvDetail1" Enabled="True"></asp:TextBox>
                                                    <asp:RequiredFieldValidator id="rfvTbIpHost" runat="server" ErrorMessage="กรอก IP / Host name ด้วยค่ะ" ControlToValidate="tbIpHost" Display="None" SetFocusOnError="true" ValidationGroup="fvDetail1"></asp:RequiredFieldValidator>
                                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="vceTbIpHost" TargetControlID="rfvTbIpHost" HighlightCssClass="validatorCalloutHighlight" />
                                                </div>
                                                <label class="col-md-4 control-label"></label>
                                            </div>
                                            </asp:Panel>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Machine name
                                                    <span class="text-danger">*</span> :</label>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="tbMachineName" runat="server" CssClass="form-control" Text='<%# Eval("machine_name") %>' placeholder="Machine name" MaxLength="50" ValidationGroup="fvDetail1"></asp:TextBox>
                                                    <asp:RequiredFieldValidator id="rfvTbMachineName" runat="server" ErrorMessage="กรอก Machine name ด้วยค่ะ" ControlToValidate="tbMachineName" Display="None" SetFocusOnError="true" ValidationGroup="fvDetail1"></asp:RequiredFieldValidator>
                                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="vceTbMachineName" TargetControlID="rfvTbMachineName" HighlightCssClass="validatorCalloutHighlight" />
                                                </div>
                                                <label class="col-md-4 control-label"></label>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Zone
                                                    <span class="text-danger">*</span> :</label>
                                                <div class="col-md-4">
                                                    <asp:DropDownList ID="ddlMachineZone" runat="server" CssClass="form-control" ValidationGroup="fvDetail1"></asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="rfvDdlMachineZone" runat="server" ControlToValidate="ddlMachineZone" ErrorMessage="กรุณาเลือก Zone ด้วยค่ะ" InitialValue="0"  Display="None" SetFocusOnError="true" ValidationGroup="fvDetail1"></asp:RequiredFieldValidator>
                                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="vceDdlMachineZone" TargetControlID="rfvDdlMachineZone" HighlightCssClass="validatorCalloutHighlight" />
                                                </div>
                                                <label class="col-md-4 control-label"></label>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">สถานที่ตั้ง
                                                    <span class="text-danger">*</span> :</label>
                                                <div class="col-md-4">
                                                    <asp:DropDownList ID="ddlMachinePlace" runat="server" CssClass="form-control" ValidationGroup="fvDetail1"></asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="rfvDdlMachinePlace" runat="server" ControlToValidate="ddlMachinePlace" ErrorMessage="กรุณาเลือกสถานที่ตั้งด้วยค่ะ" InitialValue="0"  Display="None" SetFocusOnError="true" ValidationGroup="fvDetail1"></asp:RequiredFieldValidator>
                                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="vceDdlMachinePlace" TargetControlID="rfvDdlMachinePlace" HighlightCssClass="validatorCalloutHighlight" />
                                                </div>
                                                <label class="col-md-4 control-label"></label>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">ตำแหน่งที่ตั้ง
                                                    <span class="text-danger">*</span> :</label>
                                                <div class="col-md-4">
                                                    <asp:DropDownList ID="ddlMachineLoc" runat="server" CssClass="form-control" ValidationGroup="fvDetail1"></asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="rfvDdlMachineLoc" runat="server" ControlToValidate="ddlMachineLoc" ErrorMessage="กรุณาเลือกตำแหน่งที่ตั้งด้วยค่ะ" InitialValue="0"  Display="None" SetFocusOnError="true" ValidationGroup="fvDetail1"></asp:RequiredFieldValidator>
                                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="vceDdlMachineLoc" TargetControlID="rfvDdlMachineLoc" HighlightCssClass="validatorCalloutHighlight" />
                                                </div>
                                                <label class="col-md-4 control-label"></label>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">ผู้ดูแลเครื่อง
                                                    <span class="text-danger">*</span> :</label>
                                                <div class="col-md-4">
                                                    <asp:DropDownList ID="ddlMachineOrg" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" ValidationGroup="fvDetail1">
                                                        <asp:ListItem>--- องค์กร ---</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="rfvDdlMachineOrg" runat="server" ControlToValidate="ddlMachineOrg" ErrorMessage="กรุณาเลือกองค์กรด้วยค่ะ" InitialValue="0"  Display="None" SetFocusOnError="true" ValidationGroup="fvDetail1"></asp:RequiredFieldValidator>
                                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="vceDdlMachineOrg" TargetControlID="rfvDdlMachineOrg" HighlightCssClass="validatorCalloutHighlight" />
                                                </div>
                                                <label class="col-md-4 control-label"></label>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label"><span class="text-danger">*</span></label>
                                                <div class="col-md-4">
                                                    <asp:DropDownList ID="ddlMachineDept" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" ValidationGroup="fvDetail1">
                                                        <asp:ListItem>--- ฝ่าย ---</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="rfvDdlMachineDept" runat="server" ControlToValidate="ddlMachineDept" ErrorMessage="กรุณาเลือกฝ่ายด้วยค่ะ" InitialValue="0"  Display="None" SetFocusOnError="true" ValidationGroup="fvDetail1"></asp:RequiredFieldValidator>
                                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="vceDdlMachineDept" TargetControlID="rfvDdlMachineDept" HighlightCssClass="validatorCalloutHighlight" />
                                                </div>
                                                <label class="col-md-4 control-label"></label>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label"><span class="text-danger">*</span></label>
                                                <div class="col-md-4">
                                                    <asp:DropDownList ID="ddlMachineSec" runat="server" CssClass="form-control" ValidationGroup="fvDetail1">
                                                        <asp:ListItem>--- แผนก ---</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="rfvDdlMachineSec" runat="server" ControlToValidate="ddlMachineSec" ErrorMessage="กรุณาเลือกแผนกด้วยค่ะ" InitialValue="0"  Display="None" SetFocusOnError="true" ValidationGroup="fvDetail1"></asp:RequiredFieldValidator>
                                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="vceDdlMachineSec" TargetControlID="rfvDdlMachineSec" HighlightCssClass="validatorCalloutHighlight" />
                                                </div>
                                                <label class="col-md-4 control-label"></label>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">บันทึกเวลาเข้างาน
                                                    <span class="text-danger">*</span> :</label>
                                                <div class="col-md-8 control-label textleft">
                                                    <asp:RadioButtonList ID="rblAttendanceStatus" runat="server" RepeatDirection="Horizontal" SelectedValue='<%# Eval("flag_attendance") %>'>
                                                        <asp:ListItem Text="&nbsp;บันทึก&nbsp;" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="&nbsp;ไม่บันทึก&nbsp;" Value="0"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </div>
                                                <label class="col-md-4 control-label"></label>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">สถานะ
                                                    <span class="text-danger">*</span> :</label>
                                                <div class="col-md-8 control-label textleft">
                                                    <asp:RadioButtonList ID="rblStatus" runat="server" RepeatDirection="Horizontal" SelectedValue='<%# Eval("machine_status") %>'>
                                                        <asp:ListItem Text="&nbsp;แสดง&nbsp;" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="&nbsp;ไม่แสดง&nbsp;" Value="0"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label"></label>
                                                <div class="col-md-8">
                                                    <asp:LinkButton ID="lbSave" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument='<%# Eval("m0_idx") %>' ValidationGroup="fvDetail1">
                                                        <i class="fas fa-save" aria-hidden="true"></i>&nbsp;บันทึก</asp:LinkButton>
                                                    <asp:LinkButton ID="lbCancel" runat="server" CssClass="btn btn-danger" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="0">
                                                        <i class="fas fa-times" aria-hidden="true"></i>&nbsp;ยกเลิก</asp:LinkButton>
                                                </div>
                                            </div>
                                        </EditItemTemplate>
                                    </asp:FormView>

                                    <asp:FormView ID="fvDetail2" runat="server" Width="100%" DefaultMode="ReadOnly">
                                        <InsertItemTemplate>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">สถานที่ตั้ง
                                                    <span class="text-danger">*</span> :</label>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="tbPlaceName" runat="server" CssClass="form-control" MaxLength="250" ValidationGroup="fvDetail2"></asp:TextBox>
                                                    <asp:RequiredFieldValidator id="rfvTbPlaceName" runat="server" ErrorMessage="กรอกสถานที่ตั้งด้วยค่ะ" ControlToValidate="tbPlaceName" Display="None" SetFocusOnError="true" ValidationGroup="fvDetail2"></asp:RequiredFieldValidator>
                                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="vceTbPlaceName" TargetControlID="rfvTbPlaceName" HighlightCssClass="validatorCalloutHighlight" />
                                                </div>
                                                <label class="col-md-4 control-label"></label>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">สถานะ
                                                    <span class="text-danger">*</span> :</label>
                                                <div class="col-md-8 control-label textleft">
                                                    <asp:RadioButtonList ID="rblStatus" runat="server" RepeatDirection="Horizontal">
                                                        <asp:ListItem Text="&nbsp;แสดง&nbsp;" Value="1" Selected="True"></asp:ListItem>
                                                        <asp:ListItem Text="&nbsp;ไม่แสดง&nbsp;" Value="0"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label"></label>
                                                <div class="col-md-8">
                                                    <asp:LinkButton ID="lbSave" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="0" ValidationGroup="fvDetail2">
                                                        <i class="fas fa-save" aria-hidden="true"></i>&nbsp;บันทึก</asp:LinkButton>
                                                    <asp:LinkButton ID="lbCancel" runat="server" CssClass="btn btn-danger" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="0">
                                                        <i class="fas fa-times" aria-hidden="true"></i>&nbsp;ยกเลิก</asp:LinkButton>
                                                    <asp:LinkButton ID="lbReset" runat="server" CssClass="btn btn-info" OnCommand="btnCommand" CommandName="cmdReset" CommandArgument="0">
                                                        <i class="fas fa-sync-alt" aria-hidden="true"></i>&nbsp;เคลียร์ค่า</asp:LinkButton>
                                                </div>
                                            </div>
                                        </InsertItemTemplate>
                                        <EditItemTemplate>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">สถานที่ตั้ง
                                                    <span class="text-danger">*</span> :</label>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="tbPlaceName" runat="server" CssClass="form-control" Text='<%# Eval("place_name") %>' MaxLength="250" ValidationGroup="fvDetail2"></asp:TextBox>
                                                    <asp:RequiredFieldValidator id="rfvTbPlaceName" runat="server" ErrorMessage="กรอกสถานที่ตั้งด้วยค่ะ" ControlToValidate="tbPlaceName" Display="None" SetFocusOnError="true" ValidationGroup="fvDetail2"></asp:RequiredFieldValidator>
                                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="vceTbPlaceName" TargetControlID="rfvTbPlaceName" HighlightCssClass="validatorCalloutHighlight" />
                                                </div>
                                                <label class="col-md-4 control-label"></label>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">สถานะ
                                                    <span class="text-danger">*</span> :</label>
                                                <div class="col-md-8 control-label textleft">
                                                    <asp:RadioButtonList ID="rblStatus" runat="server" RepeatDirection="Horizontal" SelectedValue='<%# Eval("place_status") %>'>
                                                        <asp:ListItem Text="&nbsp;แสดง&nbsp;" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="&nbsp;ไม่แสดง&nbsp;" Value="0"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label"></label>
                                                <div class="col-md-8">
                                                    <asp:LinkButton ID="lbSave" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument='<%# Eval("m0_idx") %>' ValidationGroup="fvDetail2">
                                                        <i class="fas fa-save" aria-hidden="true"></i>&nbsp;บันทึก</asp:LinkButton>
                                                    <asp:LinkButton ID="lbCancel" runat="server" CssClass="btn btn-danger" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="0">
                                                        <i class="fas fa-times" aria-hidden="true"></i>&nbsp;ยกเลิก</asp:LinkButton>
                                                </div>
                                            </div>
                                        </EditItemTemplate>
                                    </asp:FormView>

                                    <asp:FormView ID="fvDetail3" runat="server" Width="100%" DefaultMode="ReadOnly">
                                        <InsertItemTemplate>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">ตำแหน่งที่ตั้ง <span class="text-danger">*</span>
                                                    :</label>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="tbLocationName" runat="server" CssClass="form-control" MaxLength="250" ValidationGroup="fvDetail3"></asp:TextBox>
                                                    <asp:RequiredFieldValidator id="rfvTbLocationName" runat="server" ErrorMessage="กรอกตำแหน่งที่ตั้งด้วยค่ะ" ControlToValidate="tbLocationName" Display="None" SetFocusOnError="true" ValidationGroup="fvDetail3"></asp:RequiredFieldValidator>
                                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="vceTbLocationName" TargetControlID="rfvTbLocationName" HighlightCssClass="validatorCalloutHighlight" />
                                                </div>
                                                <label class="col-md-4 control-label"></label>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">สถานะ
                                                    <span class="text-danger">*</span> :</label>
                                                <div class="col-md-8 control-label textleft">
                                                    <asp:RadioButtonList ID="rblStatus" runat="server" RepeatDirection="Horizontal">
                                                        <asp:ListItem Text="&nbsp;แสดง&nbsp;" Value="1" Selected="True"></asp:ListItem>
                                                        <asp:ListItem Text="&nbsp;ไม่แสดง&nbsp;" Value="0"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label"></label>
                                                <div class="col-md-8">
                                                    <asp:LinkButton ID="lbSave" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="0" ValidationGroup="fvDetail3">
                                                        <i class="fas fa-save" aria-hidden="true"></i>&nbsp;บันทึก</asp:LinkButton>
                                                    <asp:LinkButton ID="lbCancel" runat="server" CssClass="btn btn-danger" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="0">
                                                        <i class="fas fa-times" aria-hidden="true"></i>&nbsp;ยกเลิก</asp:LinkButton>
                                                    <asp:LinkButton ID="lbReset" runat="server" CssClass="btn btn-info" OnCommand="btnCommand" CommandName="cmdReset" CommandArgument="0">
                                                        <i class="fas fa-sync-alt" aria-hidden="true"></i>&nbsp;เคลียร์ค่า</asp:LinkButton>
                                                </div>
                                            </div>
                                        </InsertItemTemplate>
                                        <EditItemTemplate>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">ตำแหน่งที่ตั้ง
                                                    <span class="text-danger">*</span> :</label>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="tbLocationName" runat="server" CssClass="form-control" Text='<%# Eval("location_name") %>' MaxLength="250" ValidationGroup="fvDetail3"></asp:TextBox>
                                                    <asp:RequiredFieldValidator id="rfvTbLocationName" runat="server" ErrorMessage="กรอกตำแหน่งที่ตั้งด้วยค่ะ" ControlToValidate="tbLocationName" Display="None" SetFocusOnError="true" ValidationGroup="fvDetail3"></asp:RequiredFieldValidator>
                                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="vceTbLocationName" TargetControlID="rfvTbLocationName" HighlightCssClass="validatorCalloutHighlight" />
                                                </div>
                                                <label class="col-md-4 control-label"></label>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">สถานะ
                                                    <span class="text-danger">*</span> :</label>
                                                <div class="col-md-8 control-label textleft">
                                                    <asp:RadioButtonList ID="rblStatus" runat="server" RepeatDirection="Horizontal" SelectedValue='<%# Eval("location_status") %>'>
                                                        <asp:ListItem Text="&nbsp;แสดง&nbsp;" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="&nbsp;ไม่แสดง&nbsp;" Value="0"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label"></label>
                                                <div class="col-md-8">
                                                    <asp:LinkButton ID="lbSave" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument='<%# Eval("m0_idx") %>' ValidationGroup="fvDetail3">
                                                        <i class="fas fa-save" aria-hidden="true"></i>&nbsp;บันทึก</asp:LinkButton>
                                                    <asp:LinkButton ID="lbCancel" runat="server" CssClass="btn btn-danger" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="0">
                                                        <i class="fas fa-times" aria-hidden="true"></i>&nbsp;ยกเลิก</asp:LinkButton>
                                                </div>
                                            </div>
                                        </EditItemTemplate>
                                    </asp:FormView>

                                    <asp:FormView ID="fvDetail4" runat="server" Width="100%" DefaultMode="ReadOnly">
                                        <InsertItemTemplate>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">ชื่อ Zone <span class="text-danger">*</span>
                                                    :</label>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="tbZoneName" runat="server" CssClass="form-control" MaxLength="100" ValidationGroup="fvDetail4"></asp:TextBox>
                                                    <asp:RequiredFieldValidator id="rfvTbZoneName" runat="server" ErrorMessage="กรอกชื่อ Zone ด้วยค่ะ" ControlToValidate="tbZoneName" Display="None" SetFocusOnError="true" ValidationGroup="fvDetail4"></asp:RequiredFieldValidator>
                                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="vceTbZoneName" TargetControlID="rfvTbZoneName" HighlightCssClass="validatorCalloutHighlight" />
                                                </div>
                                                <label class="col-md-4 control-label"></label>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">สถานะ
                                                    <span class="text-danger">*</span> :</label>
                                                <div class="col-md-8 control-label textleft">
                                                    <asp:RadioButtonList ID="rblStatus" runat="server" RepeatDirection="Horizontal">
                                                        <asp:ListItem Text="&nbsp;แสดง&nbsp;" Value="1" Selected="True"></asp:ListItem>
                                                        <asp:ListItem Text="&nbsp;ไม่แสดง&nbsp;" Value="0"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label"></label>
                                                <div class="col-md-8">
                                                    <asp:LinkButton ID="lbSave" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="0" ValidationGroup="fvDetail4">
                                                        <i class="fas fa-save" aria-hidden="true"></i>&nbsp;บันทึก</asp:LinkButton>
                                                    <asp:LinkButton ID="lbCancel" runat="server" CssClass="btn btn-danger" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="0">
                                                        <i class="fas fa-times" aria-hidden="true"></i>&nbsp;ยกเลิก</asp:LinkButton>
                                                    <asp:LinkButton ID="lbReset" runat="server" CssClass="btn btn-info" OnCommand="btnCommand" CommandName="cmdReset" CommandArgument="0">
                                                        <i class="fas fa-sync-alt" aria-hidden="true"></i>&nbsp;เคลียร์ค่า</asp:LinkButton>
                                                </div>
                                            </div>
                                        </InsertItemTemplate>
                                        <EditItemTemplate>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">ชื่อ Zone
                                                    <span class="text-danger">*</span> :</label>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="tbZoneName" runat="server" CssClass="form-control" Text='<%# Eval("zone_name") %>' MaxLength="100" ValidationGroup="fvDetail4"></asp:TextBox>
                                                    <asp:RequiredFieldValidator id="rfvTbZoneName" runat="server" ErrorMessage="กรอกชื่อ Zone ด้วยค่ะ" ControlToValidate="tbZoneName" Display="None" SetFocusOnError="true" ValidationGroup="fvDetail4"></asp:RequiredFieldValidator>
                                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="vceTbZoneName" TargetControlID="rfvTbZoneName" HighlightCssClass="validatorCalloutHighlight" />
                                                </div>
                                                <label class="col-md-4 control-label"></label>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">สถานะ
                                                    <span class="text-danger">*</span> :</label>
                                                <div class="col-md-8 control-label textleft">
                                                    <asp:RadioButtonList ID="rblStatus" runat="server" RepeatDirection="Horizontal" SelectedValue='<%# Eval("zone_status") %>'>
                                                        <asp:ListItem Text="&nbsp;แสดง&nbsp;" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="&nbsp;ไม่แสดง&nbsp;" Value="0"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label"></label>
                                                <div class="col-md-8">
                                                    <asp:LinkButton ID="lbSave" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument='<%# Eval("m0_idx") %>' ValidationGroup="fvDetail4">
                                                        <i class="fas fa-save" aria-hidden="true"></i>&nbsp;บันทึก</asp:LinkButton>
                                                    <asp:LinkButton ID="lbCancel" runat="server" CssClass="btn btn-danger" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="0">
                                                        <i class="fas fa-times" aria-hidden="true"></i>&nbsp;ยกเลิก</asp:LinkButton>
                                                </div>
                                            </div>
                                        </EditItemTemplate>
                                    </asp:FormView>

                                    <asp:FormView ID="fvDetail5" runat="server" Width="100%" DefaultMode="ReadOnly" OndataBound="fvDataBound">
                                        <InsertItemTemplate>
                                            <asp:HiddenField ID="hfM0Idx" runat="server" Value="0"></asp:HiddenField>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">ชื่อ Group <span class="text-danger">*</span>
                                                    :</label>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="tbGroupName" runat="server" CssClass="form-control" MaxLength="100" ValidationGroup="fvDetail5"></asp:TextBox>
                                                    <asp:RequiredFieldValidator id="rfvTbGroupName" runat="server" ErrorMessage="กรอกชื่อ Group ด้วยค่ะ" ControlToValidate="tbGroupName" Display="None" SetFocusOnError="true" ValidationGroup="fvDetail5"></asp:RequiredFieldValidator>
                                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="vceTbGroupName" TargetControlID="rfvTbGroupName" HighlightCssClass="validatorCalloutHighlight" />
                                                </div>
                                                <label class="col-md-4 control-label"></label>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">สถานที่ <span class="text-danger">*</span>
                                                    :</label>
                                                <div class="col-md-4">
                                                    <asp:DropDownList ID="ddlPlantList" runat="server" CssClass="form-control" ValidationGroup="fvDetail5">
                                                        <asp:ListItem Text="--- สถานที่ ---" Value="0"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="rfvDdlPlantList" runat="server" ControlToValidate="ddlPlantList" ErrorMessage="กรุณาเลือกสถานที่ด้วยค่ะ" InitialValue="0"  Display="None" SetFocusOnError="true" ValidationGroup="fvDetail5"></asp:RequiredFieldValidator>
                                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="vceDdlPlantList" TargetControlID="rfvDdlPlantList" HighlightCssClass="validatorCalloutHighlight" />
                                                </div>
                                                <label class="col-md-4 control-label"></label>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">สถานะ
                                                    <span class="text-danger">*</span> :</label>
                                                <div class="col-md-8 control-label textleft">
                                                    <asp:RadioButtonList ID="rblStatus" runat="server" RepeatDirection="Horizontal">
                                                        <asp:ListItem Text="&nbsp;แสดง&nbsp;" Value="1" Selected="True"></asp:ListItem>
                                                        <asp:ListItem Text="&nbsp;ไม่แสดง&nbsp;" Value="0"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </div>
                                            </div>
                                            <hr />
                                        </InsertItemTemplate>
                                        <EditItemTemplate>
                                            <asp:HiddenField ID="hfM0Idx" runat="server" Value='<%# Eval("m0_idx") %>'></asp:HiddenField>
                                            <asp:HiddenField ID="hfPlantIdx" runat="server" Value='<%# Eval("plant_idx") %>'></asp:HiddenField>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">ชื่อ Group
                                                    <span class="text-danger">*</span> :</label>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="tbGroupName" runat="server" CssClass="form-control" Text='<%# Eval("group_name") %>' MaxLength="100" ValidationGroup="fvDetail5"></asp:TextBox>
                                                    <asp:RequiredFieldValidator id="rfvTbGroupName" runat="server" ErrorMessage="กรอกชื่อ Group ด้วยค่ะ" ControlToValidate="tbGroupName" Display="None" SetFocusOnError="true" ValidationGroup="fvDetail5"></asp:RequiredFieldValidator>
                                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="vceTbGroupName" TargetControlID="rfvTbGroupName" HighlightCssClass="validatorCalloutHighlight" />
                                                </div>
                                                <label class="col-md-4 control-label"></label>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">สถานที่ <span class="text-danger">*</span>
                                                    :</label>
                                                <div class="col-md-4">
                                                    <asp:DropDownList ID="ddlPlantList" runat="server" CssClass="form-control" ValidationGroup="fvDetail5">
                                                        <asp:ListItem Text="--- สถานที่ ---" Value="0"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="rfvDdlPlantList" runat="server" ControlToValidate="ddlPlantList" ErrorMessage="กรุณาเลือกสถานที่ด้วยค่ะ" InitialValue="0"  Display="None" SetFocusOnError="true" ValidationGroup="fvDetail5"></asp:RequiredFieldValidator>
                                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="vceDdlPlantList" TargetControlID="rfvDdlPlantList" HighlightCssClass="validatorCalloutHighlight" />
                                                </div>
                                                <label class="col-md-4 control-label"></label>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">สถานะ
                                                    <span class="text-danger">*</span> :</label>
                                                <div class="col-md-8 control-label textleft">
                                                    <asp:RadioButtonList ID="rblStatus" runat="server" RepeatDirection="Horizontal" SelectedValue='<%# Eval("group_status") %>'>
                                                        <asp:ListItem Text="&nbsp;แสดง&nbsp;" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="&nbsp;ไม่แสดง&nbsp;" Value="0"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </div>
                                            </div>
                                            <hr />
                                        </EditItemTemplate>
                                    </asp:FormView>
                                    <div class="row" ID="div_manage_machine" runat="server">
                                        <div class="row">
                                            <div class="col-md-6">
                                                รายการเครื่องสแกนใน Group
                                                <asp:GridView ID="gvMachineListSelected" runat="server" AutoGenerateColumns="False"
                                                CssClass="table table-striped table-bordered table-hover table-responsive" OnRowDataBound="gvRowDataBound" OnPageIndexChanging="gvPageIndexChanging" AllowPaging="True" PageSize="10"
                                                DataKeyNames="m0_idx">
                                                    <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                                                    <RowStyle Font-Size="Small" />
                                                    <PagerStyle CssClass="pageCustom" />
                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">ไม่พบข้อมูล</div>
                                                    </EmptyDataTemplate>
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="#">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lbDelete" runat="server" CssClass="btn btn-xs btn-danger" OnCommand="btnCommand" CommandName="cmdActionDelete" CommandArgument='<%# Eval("m0_idx") %>'><i class="far fa-trash-alt" aria-hidden="true"></i></asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="IP / Host name">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblIpHost" runat="server" Text='<%# Eval("ip_host") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Zone">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblZoneName" runat="server" Text='<%# Eval("zone_name") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="สถานที่ตั้ง">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPlaceName" runat="server" Text='<%# Eval("place_name") + " - " + Eval("loc_name") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                            <div class="col-md-6">
                                                รายการเครื่องสแกนทั้งหมด
                                                <div class="row" style="margin-top: 5px; margin-bottom: 5px;">
                                                    <div class="col-md-3 clearpm">
                                                        <asp:DropDownList ID="ddlZone" runat="server" CssClass="form-control">
                                                            <asp:ListItem Text="--- Zone ---" Value="-1" />
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="col-md-3 clearpm">
                                                        <asp:DropDownList ID="ddlPlace" runat="server" CssClass="form-control">
                                                            <asp:ListItem Text="--- สถานที่ตั้ง ---" Value="-1" />
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="col-md-6 clearpm">
                                                        <div class="input-group">
                                                            <asp:DropDownList ID="ddlLoc" runat="server" CssClass="form-control">
                                                                <asp:ListItem Text="--- ตำแหน่งที่ตั้ง ---" Value="-1" />
                                                            </asp:DropDownList>
                                                            <asp:LinkButton ID="lbMachineSearch" runat="server" CssClass="input-group-addon" OnCommand="btnCommand" CommandName="cmdMachineSearch"><i class="fas fa-search"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="lbMachineReset" runat="server" CssClass="input-group-addon" OnCommand="btnCommand" CommandName="cmdMachineReset"><i class="fas fa-sync-alt"></i></asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                                <asp:GridView ID="gvMachineList" runat="server" AutoGenerateColumns="False"
                                                CssClass="table table-striped table-bordered table-hover table-responsive" OnRowDataBound="gvRowDataBound" OnPageIndexChanging="gvPageIndexChanging" AllowPaging="True" PageSize="10"
                                                DataKeyNames="m0_idx">
                                                    <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                                                    <RowStyle Font-Size="Small" />
                                                    <PagerStyle CssClass="pageCustom" />
                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">ไม่พบข้อมูล</div>
                                                    </EmptyDataTemplate>
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="#">
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="cbMachineSelected" runat="server" AutoPostBack="true" OnCheckedChanged="cbCheckedChanged" Text='<%# Eval("m0_idx") + "|" + Eval("ip_host") + "|" + Eval("zone_name") + "|" + Eval("place_name") + "|" + Eval("loc_name") %>' CssClass="hiddenText" Style="color: transparent;"></asp:CheckBox>
                                                                <asp:HiddenField ID="hfM0Idx" runat="server" Value='<%# Eval("m0_idx")%>'></asp:HiddenField>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="IP / Host name">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblIpHost" runat="server" Text='<%# Eval("ip_host") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Zone">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblZoneName" runat="server" Text='<%# Eval("zone_name") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="สถานที่ตั้ง">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPlaceName" runat="server" Text='<%# Eval("place_name") + " - " + Eval("loc_name") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                        <hr />
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                                รายชื่อตำแหน่งที่มีสิทธิ์อนุมัติ
                                                <asp:GridView ID="gvPositionListSelected" runat="server" AutoGenerateColumns="False"
                                                CssClass="table table-striped table-bordered table-hover table-responsive" OnRowDataBound="gvRowDataBound" OnPageIndexChanging="gvPageIndexChanging" AllowPaging="True" PageSize="10"
                                                DataKeyNames="m0_idx">
                                                    <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                                                    <RowStyle Font-Size="Small" />
                                                    <PagerStyle CssClass="pageCustom" />
                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">ไม่พบข้อมูล</div>
                                                    </EmptyDataTemplate>
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="#">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lbDelete" runat="server" CssClass="btn btn-xs btn-danger" OnCommand="btnCommand" CommandName="cmdActionDeletePosition" CommandArgument='<%# Eval("rpos_idx") %>'><i class="far fa-trash-alt" aria-hidden="true"></i></asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="แผนก">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblSecName" runat="server" Text='<%# Eval("sec_name_th") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="ตำแหน่ง">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPosName" runat="server" Text='<%# Eval("pos_name_th") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="ผู้อนุมัติหลัก">
                                                            <ItemTemplate>
                                                                <asp:RadioButton ID="rbFlagPriority" runat="server" AutoPostBack="true" OnCheckedChanged="rbCheckedChanged" Text='<%# Eval("rpos_idx") %>' CssClass="hiddenText" Style="color: transparent;"></asp:RadioButton>
                                                                <asp:HiddenField ID="hfFlagPriority" runat="server" Value='<%# Eval("flag_priority")%>'></asp:HiddenField>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                            <div class="col-md-6">
                                                รายชื่อตำแหน่ง
                                                <div class="row" style="margin-top: 5px; margin-bottom: 5px;">
                                                    <div class="col-md-3 clearpm">
                                                        <asp:DropDownList ID="ddlOrg" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                            <asp:ListItem Text="--- องค์กร ---" Value="-1" />
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="col-md-4 clearpm">
                                                        <asp:DropDownList ID="ddlDept" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                            <asp:ListItem Text="--- ฝ่าย ---" Value="-1" />
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="col-md-5 clearpm">
                                                        <div class="input-group">
                                                            <asp:DropDownList ID="ddlSec" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                                <asp:ListItem Text="--- แผนก ---" Value="-1" />
                                                            </asp:DropDownList>
                                                            <asp:LinkButton ID="lbPositionReset" runat="server" CssClass="input-group-addon" OnCommand="btnCommand" CommandName="cmdPositionReset"><i class="fas fa-sync-alt"></i></asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                                <asp:GridView ID="gvPositionList" runat="server" AutoGenerateColumns="False"
                                                CssClass="table table-striped table-bordered table-hover table-responsive" OnRowDataBound="gvRowDataBound" OnPageIndexChanging="gvPageIndexChanging" AllowPaging="True" PageSize="10"
                                                DataKeyNames="rpos_idx">
                                                    <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                                                    <RowStyle Font-Size="Small" />
                                                    <PagerStyle CssClass="pageCustom" />
                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">ไม่พบข้อมูล</div>
                                                    </EmptyDataTemplate>
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="#">
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="cbPositionSelected" runat="server" AutoPostBack="true" OnCheckedChanged="cbCheckedChanged" Text='<%# Eval("rpos_idx") + "|" + Eval("pos_name_th") + "|" + Eval("sec_name_th") + "|" + Eval("dept_name_th") %>' CssClass="hiddenText" Style="color: transparent;"></asp:CheckBox>
                                                                <asp:HiddenField ID="hfRposIdx" runat="server" Value='<%# Eval("rpos_idx")%>'></asp:HiddenField>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="ตำแหน่ง">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPosName" runat="server" Text='<%# Eval("pos_name_th") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-md-4 control-label"></label>
                                            <div class="col-md-8">
                                                <asp:LinkButton ID="lbSave" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="0" ValidationGroup="fvDetail5">
                                                    <i class="fas fa-save" aria-hidden="true"></i>&nbsp;บันทึก</asp:LinkButton>
                                                <asp:LinkButton ID="lbCancel" runat="server" CssClass="btn btn-danger" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="0">
                                                    <i class="fas fa-times" aria-hidden="true"></i>&nbsp;ยกเลิก</asp:LinkButton>
                                                <asp:LinkButton ID="lbReset" runat="server" CssClass="btn btn-info" OnCommand="btnCommand" CommandName="cmdReset" CommandArgument="0">
                                                    <i class="fas fa-sync-alt" aria-hidden="true"></i>&nbsp;เคลียร์ค่า</asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:View>
                <asp:View ID="viewList" runat="server">
                    <div class="col-md-12">
                        <div class="col-md-7">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="col-md-4 control-label">ประเภท master data
                                        <span class="text-danger">*</span> :</label>
                                    <div class="col-md-8 control-label textleft">
                                        <asp:RadioButtonList ID="rblMasterType2" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="rblSelectedIndexChanged"
                                            AutoPostBack="True">
                                            <asp:ListItem Text="&nbsp;เครื่อง&nbsp;" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="&nbsp;สถานที่ตั้ง&nbsp;" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="&nbsp;ตำแหน่งที่ตั้ง&nbsp;" Value="3"></asp:ListItem>
                                            <asp:ListItem Text="&nbsp;Zone&nbsp;" Value="4"></asp:ListItem>
                                            <asp:ListItem Text="&nbsp;Group&nbsp;" Value="5"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <asp:LinkButton ID="lbCreate" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdCreate" CommandArgument="0"><i
                                    class="fas fa-plus" aria-hidden="true"></i>&nbsp;เพิ่มข้อมูล</asp:LinkButton>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                <asp:TextBox ID="tbItemSearch" runat="server" CssClass="form-control" placeholder="ค้นหา"></asp:TextBox>
                                <asp:LinkButton ID="lbItemSearch" runat="server" CssClass="input-group-addon" OnCommand="btnCommand" CommandName="cmdItemSearch"><i class="fas fa-search"></i></asp:LinkButton>
                                <asp:LinkButton ID="lbItemReset" runat="server" CssClass="input-group-addon" OnCommand="btnCommand" CommandName="cmdItemReset"><i class="fas fa-sync-alt"></i></asp:LinkButton>
                            </div>
                        </div>

                        <asp:GridView ID="gvList1" runat="server" AutoGenerateColumns="False" CssClass="table table-striped table-bordered table-hover table-responsive" OnRowDataBound="gvRowDataBound" OnPageIndexChanging="gvPageIndexChanging" AllowPaging="True" PageSize="10" DataKeyNames="m0_idx">
                            <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                            <RowStyle Font-Size="Small" />
                            <PagerStyle CssClass="pageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                            <EmptyDataTemplate>No data found</EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="#">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNo" runat="server" Text='<%# (Container.DataItemIndex +1) %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="IP / Host name">
                                    <ItemTemplate>
                                        <asp:Label ID="lblIpHost" runat="server" Text='<%# Eval("ip_host") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Machine name">
                                    <ItemTemplate>
                                        <asp:Label ID="lblMachineName" runat="server" Text='<%# Eval("machine_name") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Zone">
                                    <ItemTemplate>
                                        <asp:Label ID="lblZoneName" runat="server" Text='<%# Eval("zone_name") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="สถานที่ตั้ง">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPlaceName" runat="server" Text='<%# Eval("place_name") + " - " + Eval("loc_name") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ผู้ดูแลเครื่อง">
                                    <ItemTemplate>
                                        <asp:Label ID="lblOwnerName" runat="server" Text='<%# Eval("sec_name_th") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="บันทึกเวลาเข้างาน">
                                    <ItemTemplate>
                                        <asp:Label ID="lbFlagAttendance" runat="server" Text='<%# getStatusIcon((Int32)Eval("flag_attendance")) %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="สถานะ">
                                    <ItemTemplate>
                                        <asp:Label ID="lbMachineStatus" runat="server" Text='<%# getStatusIcon((Int32)Eval("machine_status")) %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="การจัดการ">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbEdit" runat="server" CssClass="btn btn-warning" OnCommand="btnCommand" CommandName="cmdEdit" CommandArgument='<%# Eval("m0_idx") %>'><i class="far fa-edit" aria-hidden="true"></i>&nbsp;แก้ไข</asp:LinkButton>
                                        <asp:LinkButton ID="lbDelete" runat="server" CssClass="btn btn-danger" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?');" OnCommand="btnCommand" CommandName="cmdDelete" CommandArgument='<%# Eval("m0_idx") %>'><i class="far fa-trash-alt" aria-hidden="true"></i>&nbsp;ลบ</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>

                        <asp:GridView ID="gvList2" runat="server" AutoGenerateColumns="False" CssClass="table table-striped table-bordered table-hover table-responsive" OnPageIndexChanging="gvPageIndexChanging" AllowPaging="True" PageSize="10" DataKeyNames="m0_idx">
                            <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                            <RowStyle Font-Size="Small" />
                            <PagerStyle CssClass="pageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                            <EmptyDataTemplate>No data found</EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="#">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNo" runat="server" Text='<%# (Container.DataItemIndex +1) %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="สถานที่ตั้ง">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPlaceName" runat="server" Text='<%# Eval("place_name") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="สถานะ">
                                    <ItemTemplate>
                                        <asp:Label ID="lbPlaceStatus" runat="server" Text='<%# getStatusIcon((Int32)Eval("place_status")) %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="การจัดการ">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbEdit" runat="server" CssClass="btn btn-warning" OnCommand="btnCommand" CommandName="cmdEdit" CommandArgument='<%# Eval("m0_idx") %>'><i class="far fa-edit" aria-hidden="true"></i>&nbsp;แก้ไข</asp:LinkButton>
                                        <asp:LinkButton ID="lbDelete" runat="server" CssClass="btn btn-danger" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?');" OnCommand="btnCommand" CommandName="cmdDelete" CommandArgument='<%# Eval("m0_idx") %>'><i class="far fa-trash-alt" aria-hidden="true"></i>&nbsp;ลบ</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>

                        <asp:GridView ID="gvList3" runat="server" AutoGenerateColumns="False" CssClass="table table-striped table-bordered table-hover table-responsive" OnPageIndexChanging="gvPageIndexChanging" AllowPaging="True" PageSize="10" DataKeyNames="m0_idx">
                            <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                            <RowStyle Font-Size="Small" />
                            <PagerStyle CssClass="pageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                            <EmptyDataTemplate>No data found</EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="#">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNo" runat="server" Text='<%# (Container.DataItemIndex +1) %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ตำแหน่งที่ตั้ง">
                                    <ItemTemplate>
                                        <asp:Label ID="lblLocationName" runat="server" Text='<%# Eval("location_name") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="สถานะ">
                                    <ItemTemplate>
                                        <asp:Label ID="lblLocationStatus" runat="server" Text='<%# getStatusIcon((Int32)Eval("location_status")) %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="การจัดการ">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbEdit" runat="server" CssClass="btn btn-warning" OnCommand="btnCommand" CommandName="cmdEdit" CommandArgument='<%# Eval("m0_idx") %>'><i class="far fa-edit" aria-hidden="true"></i>&nbsp;แก้ไข</asp:LinkButton>
                                        <asp:LinkButton ID="lbDelete" runat="server" CssClass="btn btn-danger" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?');" OnCommand="btnCommand" CommandName="cmdDelete" CommandArgument='<%# Eval("m0_idx") %>'><i class="far fa-trash-alt" aria-hidden="true"></i>&nbsp;ลบ</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>

                        <asp:GridView ID="gvList4" runat="server" AutoGenerateColumns="False" CssClass="table table-striped table-bordered table-hover table-responsive" OnPageIndexChanging="gvPageIndexChanging" AllowPaging="True" PageSize="10" DataKeyNames="m0_idx">
                            <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                            <RowStyle Font-Size="Small" />
                            <PagerStyle CssClass="pageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                            <EmptyDataTemplate>No data found</EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="#">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNo" runat="server" Text='<%# (Container.DataItemIndex +1) %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Zone">
                                    <ItemTemplate>
                                        <asp:Label ID="lblZoneName" runat="server" Text='<%# Eval("zone_name") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="สถานะ">
                                    <ItemTemplate>
                                        <asp:Label ID="lblZoneStatus" runat="server" Text='<%# getStatusIcon((Int32)Eval("zone_status")) %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="การจัดการ">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbEdit" runat="server" CssClass="btn btn-warning" OnCommand="btnCommand" CommandName="cmdEdit" CommandArgument='<%# Eval("m0_idx") %>'><i class="far fa-edit" aria-hidden="true"></i>&nbsp;แก้ไข</asp:LinkButton>
                                        <asp:LinkButton ID="lbDelete" runat="server" CssClass="btn btn-danger" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?');" OnCommand="btnCommand" CommandName="cmdDelete" CommandArgument='<%# Eval("m0_idx") %>'><i class="far fa-trash-alt" aria-hidden="true"></i>&nbsp;ลบ</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>

                        <asp:GridView ID="gvList5" runat="server" AutoGenerateColumns="False" CssClass="table table-striped table-bordered table-hover table-responsive" OnPageIndexChanging="gvPageIndexChanging" AllowPaging="True" PageSize="10" DataKeyNames="m0_idx">
                            <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                            <RowStyle Font-Size="Small" />
                            <PagerStyle CssClass="pageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                            <EmptyDataTemplate>No data found</EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="#">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNo" runat="server" Text='<%# (Container.DataItemIndex +1) %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Group">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGroupName" runat="server" Text='<%# Eval("group_name") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="สถานที่">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPlantName" runat="server" Text='<%# Eval("plant_name") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="สถานะ">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGroupStatus" runat="server" Text='<%# getStatusIcon((Int32)Eval("group_status")) %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="การจัดการ">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbEdit" runat="server" CssClass="btn btn-warning" OnCommand="btnCommand" CommandName="cmdEdit" CommandArgument='<%# Eval("m0_idx") %>'><i class="far fa-edit" aria-hidden="true"></i>&nbsp;แก้ไข</asp:LinkButton>
                                        <asp:LinkButton ID="lbDelete" runat="server" CssClass="btn btn-danger" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?');" OnCommand="btnCommand" CommandName="cmdDelete" CommandArgument='<%# Eval("m0_idx") %>'><i class="far fa-trash-alt" aria-hidden="true"></i>&nbsp;ลบ</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </asp:View>
            </asp:MultiView>
        </div>
    </asp:Content>