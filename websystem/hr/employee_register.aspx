﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage_recurit.master" AutoEventWireup="true" CodeFile="employee_register.aspx.cs" Inherits="websystem_hr_employee_register" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <script>
        $(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });
        //});

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            });
        });

    </script>

    <script>
        $(document).ready(function () {
            window.Sys.WebForms.PageRequestManager.getInstance().add_endRequest(pageLoad);

            function pageLoad() {
                $("#FileUpload1").MultiFile();
            }
        })
    </script>

    <script>
        $(document).ready(function () {
            window.Sys.WebForms.PageRequestManager.getInstance().add_endRequest(pageLoad);

            function pageLoad() {
                $("#FileUpload_full").MultiFile();
            }
        })
    </script>

    <script>
        $(document).ready(function () {
            window.Sys.WebForms.PageRequestManager.getInstance().add_endRequest(pageLoad);

            function pageLoad() {
                $("#FileUpload_edit").MultiFile();
            }
        })
    </script>

    <script type="text/javascript">
        function openModal() {
            $('#ordine').modal('show');
        }
    </script>

    <script type="text/javascript">
        function openModal_mtt() {
            $('#ordine_mtt').modal('show');
        }
    </script>

    <script type="text/javascript">
        function openModal_npw() {
            $('#ordine_npw').modal('show');
        }
    </script>

    <script type="text/javascript">
        function openModal_rjn() {
            $('#ordine_rjn').modal('show');
        }
    </script>

    <script type="text/javascript">
        function openModal_test() {
            $('#ordine_test').modal('show');
        }
    </script>

    <script type="text/javascript">
        function openModal_test_edit() {
            $('#ordine_test_edit').modal('show');
        }
    </script>

    <script type="text/javascript">  
        function openModal_test_mini() {
            $('#ordine_test_mini').modal('show');
        }
    </script>

    <script type="text/javascript">
        function openModal_test_full() {
      
            $('#ordine_test_full').modal('show');
        }
    </script>

    <script type="text/javascript">
        function openModal_test_index() {
            $('#ordine_test_index').modal('show');
        }
    </script>


    <style type="text/css">
        .bg-regis {
            /*background-image: url('../../masterpage/images/hr/background-detail_register.png');*/
            /*background-image: url('http://172.16.11.44/dev.taokaenoi.co.th/masterpage/images/hr/background-box.png');*/
            background: url('../../masterpage/images/hr/background-box.png') no-repeat;
            background-size: 100% 100%;
        }

        .bg-logo {
            background-image: url('../../masterpage/images/hr/logo.png');
            background-size: 100% 100%;
            width: 75px;
            height: 40px;
            text-align: center;
        }

        .bg-regis-template {
            /*background-image: url('../../masterpage/images/hr/background_register.png');*/
            /*background-image: url('../../masterpage/images/hr/background_register_1.png');*/
            background-size: 100% 100%;
        }

        .bg-regis-template-full {
            /*background-image: url('../../masterpage/images/hr/background_register.png');*/
            /*background-image: url('../../masterpage/images/hr/background_register_2.png');*/
            background-size: 100% 100%;
        }

        .tab_top {
            margin-top: -15px;
        }

        .tab_bottom {
            margin-bottom: -10%;
        }

        .bg-template-top {
            background: url('../../masterpage/images/hr/modal_top.png') no-repeat;
            background-size: 100% 100%;
            background-color: #ef4b5e;
        }

        .bg-template-center {
            background: url('../../masterpage/images/hr/modal_center.png') no-repeat;
            background-size: 100% 100%;
        }

        .bg-template-center_tranfer {
            background: url('../../masterpage/images/hr/pic_tranfer_bg.png') no-repeat;
            height: 400px;
            background-size: 100% 100%;
        }

        .bg-template-buttom {
            background: url('../../masterpage/images/hr/modal_buttom.png') no-repeat;
            background-size: 100% 100%;
            background-color: #818285;
            height: 50px;
        }

        .bg-template-buttom_tranfer {
            background: url('../../masterpage/images/hr/pic_button_tranfer.png') no-repeat;
            background-size: 100% 100%;
            background-color: #939598;
            height: 50px;
            width: 100%;
        }

        .button_register {
            margin-top: -70px;
            margin-right: 5px;
            /*margin-bottom: auto;
            margin-right: -0px;*/
        }

        .button_tranfer {
            margin-top: 190px;
            /*margin-bottom: auto;
            margin-right: -0px;*/
        }

        .panel-red {
            border-color: #ec2839;
        }

            .panel-red > .panel-heading {
                color: #fff;
                background-color: #ec2839;
                border-color: #ec2839;
            }

                .panel-red > .panel-heading + .panel-collapse > .panel-body {
                    border-top-color: #ec2839;
                }

                .panel-red > .panel-heading .badge {
                    color: #ec2839;
                    background-color: #fff;
                }

            .panel-red > .panel-footer + .panel-collapse > .panel-body {
                border-bottom-color: #ec2839;
            }

        .bg-red {
            color: #ffffff;
            background-color: #ec2839;
            border-color: #ec2839;
        }

        .CsTextColor-SlateGrey {
            color: #708090;
        }

        .CsTextColor-while {
            color: #ffffff;
        }
    </style>

    <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    <%----------- Menu Tab Start---------------%>
    <asp:Panel ID="Box_Menu" runat="server" Visible="true">
        <div id="BoxTabMenuIndex" runat="server" class="tab_top">
            <div class="form-group">
                <nav class="navbar navbar-default" id="divBtnHead" runat="server" role="navigation">
                    <div class="container-fluid bg-red">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#Menu1">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <div class="sub-navbar">
                                <a class="navbar-brand" href="#">Menu</a>
                            </div>
                        </div>

                        <div class="collapse navbar-collapse" id="Menu1">
                            <asp:LinkButton ID="lbadd_mini" CssClass="btn_menulist" runat="server" CommandName="btnadd_mini" OnCommand="btnCommand" CommandArgument="1" Text="กรอกใบสมัครงาน (แบบย่อ)"></asp:LinkButton>
                            <asp:LinkButton ID="lbadd_full" CssClass="btn_menulist" runat="server" CommandName="btnadd_full" OnCommand="btnCommand" CommandArgument="2" Text="กรอกใบสมัครงาน (แบบเต็ม)"></asp:LinkButton>
                            <asp:LinkButton ID="lbedit" CssClass="btn_menulist" runat="server" CommandName="btnEdit" OnCommand="btnCommand" CommandArgument="3" Text="แก้ไขข้อมูล"></asp:LinkButton>
                            <asp:LinkButton ID="lbbenefit" CssClass="btn_menulist" runat="server" CommandName="btnBenefits" OnCommand="btnCommand" CommandArgument="4" Text="สวัสดิการ"></asp:LinkButton>
                            <asp:LinkButton ID="lbposition" CssClass="btn_menulist" runat="server" CommandName="btnPosition" OnCommand="btnCommand" CommandArgument="5" Text="ตำแหน่งเปิดรับสมัคร"></asp:LinkButton>
                            <asp:LinkButton ID="lbcontract" CssClass="btn_menulist" runat="server" CommandName="btnContract" OnCommand="btnCommand" CommandArgument="6" Text="ติดต่อ"></asp:LinkButton>
                            <asp:LinkButton ID="lbbacktkn" CssClass="btn_menulist" runat="server" CommandName="btnbacktaokaenoi" OnCommand="btnCommand" CommandArgument="7" Text="Back taokaenoi.co.th"></asp:LinkButton>
                            <label class="col-sm-2 control-label col-sm-offset-1">
                                <asp:Label ID="lbl_static" runat="server" class="text_right" /><asp:Label ID="lbl_static2" runat="server" class="text_right" /><asp:Label ID="lbl_static3" runat="server" class="text_right" />
                            </label>
                            <asp:Label ID="lblgetip" runat="server" class="text_right" Visible="false" />
                        </div>
                    </div>
                    <!-- /.container-fluid -->
                </nav>
            </div>
        </div>
    </asp:Panel>

    <%--<div class="col-lg-12">
        <div class="panel-body" style="background-color: transparent; background-image: url('../../masterpage/images/hr/pic_main_tranfer2.jpg'); height:50%; width:50%; " ></div>
    </div>--%>

    <%--<asp:Panel ID="pn_bg_transfer" runat="server">

    </asp:Panel>--%>


    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Box_panel_present" runat="server" Visible="false">
                <div id="Div1" runat="server" class="tab_top">
                    <div class="form-group">
                        <div class="container-fluid bg-red">

                            <%--<a id="ss" class="navbar-brand" href='<%=ResolveUrl("~/") %>' style="height: 73.5px;">--%>
                            <div class="col-md-12">
                                <img src='<%= ResolveUrl("~/masterpage/images/hr/pic_main_tranfer2.jpg") %>' class="img-fluid" width="100%" alt="MAS" />

                                <div class="col-lg-3 col-lg-offset-9 button_register">
                                    <asp:ImageButton ID="lbl_transfer" CssClass="img-responsive  img-fluid background-size: 50% 50%" Width="50%" ImageUrl="~/masterpage/images/hr/pic_button_tranfer1.png" OnCommand="btnCommand" CommandName="btnSave1" runat="server" />
                                    <%--<asp:LinkButton ID="LinkButton10" runat="server" CssClass="btn btn-default button_register" OnCommand="btnCommand" CommandName="btnSave">Save &nbsp;</asp:LinkButton>--%>
                                </div>
                            </div>
                        </div>
                        <%--<asp:UpdatePanel ID="UpdatePanel5" runat="server">
                <ContentTemplate>
                    <div class="panel-body" style="background-color: transparent; background-image: url('../../masterpage/images/hr/pic_button_tranfer1.png'); background-size: 100% 100%" id="pn_bgCalendar"></div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="lbttranfer_test_full" />
                </Triggers>
            </asp:UpdatePanel>--%>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:HiddenField ID="Hddfld_folderImage" runat="server" />

    <%-------------- Menu Tab End--------------%>
    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <asp:View ID="View_Present" runat="server">
            <%--<asp:Panel ID="pn_bg_present" runat="server" CssClass="bg-regis">
                <br />--%>
            <%--<asp:ImageButton ID="ImageButton3" CssClass="img-responsive img-fluid center-block background-size: 100% 100%" Width="95%"  ImageUrl="~/masterpage/images/hr/pic_main_tranfer2.jpg" runat="server" />--%>
            <%--<asp:UpdatePanel ID="UpdatePanel5" runat="server">
                <ContentTemplate>
                    <asp:LinkButton ID="LinkButton10" runat="server" CssClass="btn btn-default center-block button_register" OnCommand="btnCommand" CommandName="btntranfer_test_full"><img src="~/masterpage/images/hr/pic_button_tranfer1.png" alt="delete group" /></asp:LinkButton>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="lbttranfer_test_full" />
                </Triggers>
            </asp:UpdatePanel>--%>
            <%--<asp:ImageButton ID="ImageButton4" CssClass="img-responsive img-fluid center-block button_register" Width="10%" ImageUrl="~/masterpage/images/hr/pic_button_tranfer1.png" runat="server" />--%>
            <%--<br />
                <br />
                </asp:Panel>--%>
        </asp:View>

        <asp:View ID="ViewAdd_mini" runat="server">

            <asp:Panel ID="Panel1" runat="server">
                <div class="panel-body">
                    <div class="form-horizontal" role="form">
                        <asp:Label ID="Label2" runat="server"></asp:Label>
                        <asp:HyperLink runat="server" ID="txtfocus" />
                        <div class="form-group">
                            <label class="col-sm-1"></label>
                            <label class="col-sm-10">
                                <asp:Image ID="ImageButton1" CssClass="img-responsive" Width="100%" Height="400px" ImageUrl="~/masterpage/images/hr/banner_register_1.png" runat="server" />
                            </label>
                            <label class="col-sm-1"></label>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-1"></label>
                            <label class="col-sm-10">
                                <asp:Image ID="Image1" CssClass="img-responsive img-fluid" Width="35%" Height="30%" ImageUrl="~/masterpage/images/hr/topic_register.png" runat="server" />
                            </label>
                        </div>

                        <asp:FormView ID="FvInsert_Mini" runat="server" Width="80%" HorizontalAlign="Center" OnDataBound="FvDetail_DataBound">
                            <InsertItemTemplate>

                                <%-------------------------------- ข้อมูลทั่วไป --------------------------------%>
                                <asp:Panel ID="BoxGen" runat="server">

                                    <div class="panel panel-red">
                                        <div class="panel-heading">
                                            <h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i><strong>&nbsp; Position Interested / ประเภทงานที่สนใจ</strong></h3>
                                        </div>
                                        <asp:Panel ID="pn_bg_genarol" runat="server" CssClass="bg-regis">
                                            <div class="panel-body">
                                                <div class="form-horizontal" role="form">

                                                    <div class="col-sm-7">

                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label ">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label17" runat="server" Text="Employee In" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label19" runat="server" Text="วันที่สามารถเริ่มงานได้" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-5">
                                                                <asp:DropDownList ID="ddlstartdate_in" runat="server" CssClass="form-control" ValidationGroup="InsertData_Mini">
                                                                    <asp:ListItem Value="0">เลือกวันที่พร้อมเริ่มงาน.....</asp:ListItem>
                                                                    <asp:ListItem Value="1">ไม่แน่ใจ</asp:ListItem>
                                                                    <asp:ListItem Value="2">ทันที</asp:ListItem>
                                                                    <asp:ListItem Value="3">7 วัน</asp:ListItem>
                                                                    <asp:ListItem Value="4">15 วัน</asp:ListItem>
                                                                    <asp:ListItem Value="5">1 เดือน</asp:ListItem>
                                                                    <asp:ListItem Value="6">2 เดือน</asp:ListItem>
                                                                    <asp:ListItem Value="7">3 เดือน</asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="Requir2094edFieldValidator1" ValidationGroup="InsertData_Mini" runat="server" Display="None"
                                                                    ControlToValidate="ddlstartdate_in" Font-Size="11"
                                                                    ErrorMessage="วันที่สามารถเริ่มงานได้ ...."
                                                                    ValidationExpression="วันที่สามารถเริ่มงานได้ ...." InitialValue="0" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCal09loutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requir2094edFieldValidator1" Width="160" />
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <asp:Label ID="Label2209" runat="server" ForeColor="Red" Text="***" />
                                                            </div>

                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label204" runat="server" Text="Salary" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label205" runat="server" Text="เงินเดือนที่ต้องการ" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-5">
                                                                <asp:TextBox ID="txtsalary" CssClass="form-control" runat="server" ValidationGroup="InsertData_Mini"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="RequirValidator1" ValidationGroup="InsertData_Mini" runat="server" Display="None"
                                                                    ControlToValidate="txtsalary" Font-Size="11"
                                                                    ErrorMessage="เงินเดือนที่ต้องการ" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="Validtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequirValidator1" Width="160" />
                                                                <asp:RegularExpressionValidator ID="RetxwtUnit" runat="server" ValidationGroup="InsertData_Mini" Display="None"
                                                                    ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                                    ControlToValidate="txtsalary"
                                                                    ValidationExpression="^[0-9]{1,10}$" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="Valitender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RetxwtUnit" Width="160" />
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <asp:Label ID="Label202" runat="server" ForeColor="Red" Text="***" />
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label ">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label198" runat="server" Text="Job Category" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label201" runat="server" Text="ประเภทงานที่ต้องการ" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-5">
                                                                <asp:DropDownList ID="ddltypecategory" runat="server" CssClass="form-control" ValidationGroup="InsertData_Mini">
                                                                    <asp:ListItem Value="0">เลือกประเภทงานที่ต้องการ...</asp:ListItem>
                                                                    <asp:ListItem Value="1">ประจำ</asp:ListItem>
                                                                    <asp:ListItem Value="2">Part-time</asp:ListItem>
                                                                    <asp:ListItem Value="3">Freelance</asp:ListItem>
                                                                    <asp:ListItem Value="4">ฝึกงาน</asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="Requiidator1" ValidationGroup="InsertData_Mini" runat="server" Display="None"
                                                                    ControlToValidate="ddltypecategory" Font-Size="11"
                                                                    ErrorMessage="ประเภทงานที่ต้องการ ...."
                                                                    ValidationExpression="ประเภทงานที่ต้องการ ...." InitialValue="0" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="Validtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiidator1" Width="160" />
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <asp:Label ID="Label203" runat="server" ForeColor="Red" Text="***" />
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label ">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label74" runat="server" Text="Position" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label77" runat="server" Text="ตำแหน่งงานที่เปิดรับ" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-5">
                                                                <asp:DropDownList ID="ddlpositionfocus" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0">เลือกตำแหน่งงาน....</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label ">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label99" runat="server" Text="Position Group" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label100" runat="server" Text="กลุ่มประเภทงานที่ต้องการ" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-5">
                                                                <asp:DropDownList ID="ddlposition1" runat="server" CssClass="form-control" ValidationGroup="InsertData_Mini">
                                                                    <asp:ListItem Value="0">เลือกประเภทกลุ่มงาน 1....</asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="Rlidator1" ValidationGroup="InsertData_Mini" runat="server" Display="None"
                                                                    ControlToValidate="ddlposition1" Font-Size="11"
                                                                    ErrorMessage="กลุ่มประเภทงานที่ต้องการ ...."
                                                                    ValidationExpression="กลุ่มประเภทงานที่ต้องการ ...." InitialValue="0" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="Validaxtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Rlidator1" Width="160" />
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <asp:Label ID="Label206" runat="server" ForeColor="Red" Text="***" />
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label ">
                                                            </label>

                                                            <div class="col-sm-5">
                                                                <asp:DropDownList ID="ddlposition2" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0">เลือกประเภทกลุ่มงาน 2....</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label ">
                                                            </label>

                                                            <div class="col-sm-5">
                                                                <asp:DropDownList ID="ddlposition3" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0">เลือกประเภทกลุ่มงาน 3....</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label207" runat="server" Text="Description" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label208" runat="server" Text="รายละเอียดเพิ่มเติม/ความสามารถพิเศษ" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-5">
                                                                <asp:TextBox ID="txtdescription" CssClass="form-control" runat="server" autocomplete="off" MaxLength="250" TextMode="MultiLine" Rows="3" ValidationGroup="AddReference1"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="col-sm-5">
                                                        <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Always">
                                                            <ContentTemplate>
                                                                <div class="form-group">
                                                                    <div class="col-sm-8">
                                                                        <div class="panel panel-red">
                                                                            <div class="form-group">
                                                                                <div class="panel-body">
                                                                                    <center><asp:Image ID="img_profile" runat="server" style="width: 100px; height: 100px" class="img-thumbnail"></asp:Image></center>
                                                                                    <center><asp:Panel id="ImagProfile_default" runat="server">
                                                                            <img class="media-object img-thumbnail" src='<%=ResolveUrl("~/uploadfiles/profile_picture/default/user-default.png")%>' width="100px" height="100px" style="margin-right: 8px; margin-top: -5px;">
                                                                        </asp:Panel></center>

                                                                                    &nbsp;&nbsp;<asp:FileUpload ID="upload_profile_pi" AutoPostBack="true" Font-Size="small" ClientIDMode="Static" runat="server" accept="jpg" CssClass="btn btn-sm multi max-1" />
                                                                                    <center><font color="red"><small class="text_right"><p>**เฉพาะนามสกุล jpg</small></font></center>
                                                                                    <hr />
                                                                                    <center><asp:LinkButton ID="btn_save_file1" runat="server" CssClass="btn btn-warning btn-sm" data-toggle="tooltip" title="อัพโหลดภาพ" CommandName="savefile_picture" OnCommand="btnCommand"><i class="glyphicon glyphicon-picture"></i> อัพโหลดภาพ</asp:LinkButton></center>
                                                                                    <center><font color="red"><small class="text_right"><p>กรุณากรอกข้อมูลบัตรประชาชนให้เรียบร้อยก่อนทำการ upload รูปภาพ</small></font></center>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                        <asp:HyperLink runat="server" ID="txtfocus_picture" />
                                                    </div>

                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>

                                    <div class="panel panel-red">
                                        <div class="panel-heading">
                                            <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; Personal Information / ข้อมูลส่วนตัว</strong></h3>
                                        </div>
                                        <asp:Panel ID="Panel1" runat="server" CssClass="bg-regis">
                                            <div class="panel-body">
                                                <div class="form-horizontal" role="form">

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label222" runat="server" Text="Name(Thai)" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label224" runat="server" Text="ชื่อ(ไทย)" /></b></small>
                                                            </h2>
                                                        </label>
                                                        <div class="col-sm-1">
                                                            <asp:DropDownList ID="ddl_prefix_th" runat="server" AutoPostBack="true" EnableViewState="true" CssClass="form-control" Width="105" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                <asp:ListItem Value="1">นาย</asp:ListItem>
                                                                <asp:ListItem Value="2">นางสาว</asp:ListItem>
                                                                <asp:ListItem Value="3">นาง</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txt_name_th" CssClass="form-control" runat="server" ValidationGroup="InsertData_Mini"></asp:TextBox>

                                                            <asp:RequiredFieldValidator ID="C_NameTH_add" runat="server" ControlToValidate="txt_name_th" Display="None" SetFocusOnError="true" ErrorMessage="*กรอกข้อมูล" ValidationGroup="InsertData_Mini" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_NameTH_add" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="C_NameTH_add" Width="160" />
                                                            <asp:RegularExpressionValidator ID="R_NamTH_add" runat="server" ErrorMessage="*เฉพาะตัวอักษรภาษาไทยเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txt_name_th" ValidationExpression="^[ก-๙'.\s]{1,250}$" ValidationGroup="InsertData_Mini" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_NameTH2_add" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_NamTH_add" Width="160" />
                                                        </div>
                                                        <div class="col-sm-1">
                                                            <asp:Label ID="Label210" runat="server" ForeColor="Red" Text="***" />
                                                        </div>

                                                        <label class="col-sm-1 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label1" runat="server" Text="Lastname" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label4" runat="server" Text="นามสกุล" /></b></small>
                                                            </h2>
                                                        </label>
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txt_surname_th" CssClass="form-control" runat="server" ValidationGroup="InsertData_Mini"></asp:TextBox>

                                                            <asp:RequiredFieldValidator ID="C_LastnameTH_add" runat="server" ControlToValidate="txt_surname_th" Display="None" SetFocusOnError="true" ErrorMessage="*กรอกข้อมูล" ValidationGroup="InsertData_Mini" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_LastnameTH_add" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="C_LastnameTH_add" Width="160" />
                                                            <asp:RegularExpressionValidator ID="R_LastNameTH_add" runat="server" ErrorMessage="*เฉพาะตัวอักษรภาษาไทยเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txt_surname_th" ValidationExpression="^[ก-๙'.\s]{1,250}$" ValidationGroup="InsertData_Mini" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_LastNameTH2_add" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_LastNameTH_add" Width="160" />
                                                        </div>
                                                        <div class="col-sm-1">
                                                            <asp:Label ID="Label211" runat="server" ForeColor="Red" Text="***" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label5" runat="server" Text="Name(English)" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label22" runat="server" Text="ชื่อ(อังกฤษ)" /></b></small>
                                                            </h2>
                                                        </label>
                                                        <div class="col-sm-1">
                                                            <asp:DropDownList ID="ddl_prefix_en" runat="server" AutoPostBack="true" CssClass="form-control" Width="105" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                <asp:ListItem Value="1">Mr.</asp:ListItem>
                                                                <asp:ListItem Value="2">Miss</asp:ListItem>
                                                                <asp:ListItem Value="3">Mrs.</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txt_name_en" CssClass="form-control" runat="server" ValidationGroup="InsertData_Mini"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="C_NameEN_add" runat="server" ControlToValidate="txt_name_en" Display="None" SetFocusOnError="true" ErrorMessage="*กรอกข้อมูล" ValidationGroup="InsertData_Mini" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_NameEN_add" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="C_NameEN_add" Width="160" />
                                                            <asp:RegularExpressionValidator ID="R_NameEN_add" runat="server" ErrorMessage="*เเฉพาะตัวอักษรภาษาอังกฤษเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txt_name_en" ValidationExpression="^[a-zA-Z'.\s]{1,250}$" ValidationGroup="InsertData_Mini" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_NameEN2_add" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_NameEN_add" Width="160" />
                                                        </div>
                                                        <div class="col-sm-1">
                                                            <asp:Label ID="Label212" runat="server" ForeColor="Red" Text="***" />
                                                        </div>

                                                        <label class="col-sm-1 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label8" runat="server" Text="Lastname" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label24" runat="server" Text="นามสกุล" /></b></small>
                                                            </h2>
                                                        </label>
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txt_surname_en" CssClass="form-control" runat="server" ValidationGroup="InsertData_Mini"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="C_LastnameEN_add" runat="server" ControlToValidate="txt_surname_en" Display="None" SetFocusOnError="true" ErrorMessage="*กรอกข้อมูล" ValidationGroup="InsertData_Mini" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_LastnameEN_add" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="C_LastnameEN_add" Width="160" />

                                                            <asp:RegularExpressionValidator ID="R_LastNameEN_add" runat="server" ErrorMessage="*เฉพาะตัวอักษรภาษาอังกฤษเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txt_surname_en" ValidationExpression="^[a-zA-Z'.\s]{1,250}$" ValidationGroup="InsertData_Mini" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_LastnameEN2_add" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_LastNameEN_add" Width="160" />
                                                        </div>
                                                        <div class="col-sm-1">
                                                            <asp:Label ID="Label213" runat="server" ForeColor="Red" Text="***" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label ">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label27" runat="server" Text="Sex" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label28" runat="server" Text="เพศ" /></b></small>
                                                            </h2>
                                                        </label>

                                                        <div class="col-sm-2">
                                                            <asp:DropDownList ID="ddl_sex" runat="server" CssClass="form-control" ValidationGroup="InsertData_Mini">
                                                                <asp:ListItem Value="1">ชาย</asp:ListItem>
                                                                <asp:ListItem Value="2">หญิง</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="Requiator1" ValidationGroup="InsertData_Mini" runat="server" Display="None"
                                                                ControlToValidate="ddl_sex" Font-Size="11"
                                                                ErrorMessage="เพศ ...."
                                                                ValidationExpression="เพศ ...." InitialValue="0" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="Validatoder1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiator1" Width="160" />
                                                        </div>
                                                        <div class="col-sm-1">
                                                            <asp:Label ID="Label64" runat="server" ForeColor="Red" Text="***" />
                                                        </div>

                                                        <label class="col-sm-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label15" runat="server" Text="Birthday" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label16" runat="server" Text="วันเกิด" /></b></small>
                                                            </h2>
                                                        </label>
                                                        <div class="col-sm-2">
                                                            <div class='input-group date'>
                                                                <asp:TextBox ID="txtbirth" CssClass="form-control from-date-datepicker" MaxLengh="100%" runat="server" ValidationGroup="InsertData_Mini"></asp:TextBox>
                                                                <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                            </div>
                                                            <asp:RequiredFieldValidator ID="RdValid3ator2" ValidationGroup="InsertData_Mini" runat="server" Display="None"
                                                                ControlToValidate="txtbirth" Font-Size="11"
                                                                ErrorMessage="วันเกิด ...." />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCwalloer6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RdValid3ator2" Width="160" />
                                                        </div>
                                                        <div class="col-sm-1">
                                                            <asp:Label ID="Label6" runat="server" ForeColor="Red" Text="***" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label29" runat="server" Text="MilitaryStatus" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label31" runat="server" Text="สถานะทางทหาร" /></b></small>
                                                            </h2>
                                                        </label>

                                                        <div class="col-sm-2">
                                                            <asp:DropDownList ID="ddlmilitary" runat="server" CssClass="form-control" ValidationGroup="InsertData_Mini">
                                                                <asp:ListItem Value="1">ผ่านการเกณทหาร</asp:ListItem>
                                                                <asp:ListItem Value="2">เรียนรักษาดินแดน</asp:ListItem>
                                                                <asp:ListItem Value="3">ได้รับข้อยกเว้น</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="Requtor1" ValidationGroup="InsertData_Mini" runat="server" Display="None"
                                                                ControlToValidate="ddlmilitary" Font-Size="11"
                                                                ErrorMessage="สถานะทางทหาร ...."
                                                                ValidationExpression="สถานะทางทหาร ...." InitialValue="0" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalder1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requtor1" Width="160" />
                                                        </div>
                                                        <div class="col-sm-1">
                                                            <asp:Label ID="Label70" runat="server" ForeColor="Red" Text="***" />
                                                        </div>

                                                        <label class="col-sm-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label32" runat="server" Text="ID Card" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label33" runat="server" Text="เลขบัตรประชาชน / Tax ID (ต่างชาติ)" /></b></small>
                                                            </h2>
                                                        </label>
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txtidcard" CssClass="form-control" MaxLength="13" runat="server" ValidationGroup="InsertData_Mini"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="Requilidator1" ValidationGroup="InsertData_Mini" runat="server" Display="None"
                                                                ControlToValidate="txtidcard" Font-Size="11"
                                                                ErrorMessage="เลขบัตรประชาชน" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="Valr1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requilidator1" Width="160" />
                                                            <asp:RegularExpressionValidator ID="Regur1" runat="server" ValidationGroup="InsertData_Mini" Display="None"
                                                                ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                                ControlToValidate="txtidcard"
                                                                ValidationExpression="^[0-9]{1,15}$" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="Validader4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Regur1" Width="160" />
                                                        </div>
                                                        <div class="col-sm-1">
                                                            <asp:Label ID="Label101" runat="server" ForeColor="Red" Text="***" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label264" runat="server" Text="Tel" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label266" runat="server" Text="เบอร์โทรศัพท์" /></b></small>
                                                            </h2>
                                                        </label>
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txttelmobile_present" CssClass="form-control" MaxLength="10" runat="server" ValidationGroup="InsertData_Mini"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="Reqor1" ValidationGroup="InsertData_Mini" runat="server" Display="None"
                                                                ControlToValidate="txttelmobile_present" Font-Size="11"
                                                                ErrorMessage="เบอร์โทรศัพท์" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="VsExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Reqor1" Width="160" />
                                                            <asp:RegularExpressionValidator ID="RegularEator1" runat="server" ValidationGroup="InsertData_Mini" Display="None"
                                                                ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                                ControlToValidate="txttelmobile_present"
                                                                ValidationExpression="^[0-9]{1,10}$" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="Vder4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularEator1" Width="160" />
                                                        </div>
                                                        <div class="col-sm-1">
                                                            <asp:Label ID="Label102" runat="server" ForeColor="Red" Text="***" />
                                                        </div>

                                                        <label class="col-sm-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label83" runat="server" Text="Email" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label84" runat="server" Text="อีเมล" /></b></small>
                                                            </h2>
                                                        </label>
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txtemail_present" CssClass="form-control" runat="server" Text='<%# Eval("emp_email") %>' ValidationGroup="InsertData_Mini"></asp:TextBox>

                                                            <asp:RegularExpressionValidator ID="R_Email" runat="server" SetFocusOnError="true" Display="None" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtemail_present" ErrorMessage="*กรอกในรูปแบบEmail" ValidationGroup="InsertData_Mini"></asp:RegularExpressionValidator>
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_Email2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_Email" Width="160" />
                                                        </div>
                                                        <div class="col-sm-1">
                                                            <asp:Label ID="Label103" runat="server" ForeColor="Red" Text="***" />
                                                        </div>
                                                    </div>

                                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                                                        <ContentTemplate>
                                                            <div class="form-group">

                                                                <label class="col-sm-2 control-label">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label227" runat="server" Text="Upload document" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label269" runat="server" Text="เอกสารเพิ่มเติม" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-3">
                                                                    <div class="panel panel-red">
                                                                        <div class="form-group">
                                                                            <div class="panel-body">
                                                                                <asp:FileUpload ID="FileUpload1" ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small" ClientIDMode="Static" runat="server" CssClass="btn btn-sm multi" accept="gif|jpg|png|JPEG|pdf" />
                                                                                <center><font color="red"><small class="text_right"><p>**เฉพาะนามสกุล gif , jpg , png , pdf</small></font></center>
                                                                                <hr />
                                                                                <center><asp:LinkButton ID="btn_save_document" runat="server" CssClass="btn btn-warning btn-sm" data-toggle="tooltip" title="อัพโหลดภาพ" CommandName="savefile_document" OnCommand="btnCommand"><i class="glyphicon glyphicon-picture"></i> อัพโหลดภาพ</asp:LinkButton></center>
                                                                                <center><font color="red"><small class="text_right"><p>กรุณากรอกข้อมูลบัตรประชาชนให้เรียบร้อยก่อนทำการ upload รูปภาพ</small></font></center>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <label class="col-sm-2 control-label">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label23" runat="server" Text="Document List" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label30" runat="server" Text="เอกสารแนบทั้งหมด" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-3">
                                                                    <asp:GridView ID="gvFileEmp" Visible="true" runat="server"
                                                                        AutoGenerateColumns="false"
                                                                        CssClass="table table-striped table-bordered table-hover table-responsive"
                                                                        HeaderStyle-CssClass="warning"
                                                                        OnRowDataBound="Master_RowDataBound"
                                                                        BorderStyle="None"
                                                                        CellSpacing="2"
                                                                        Font-Size="Small">
                                                                        <EmptyDataTemplate>
                                                                            <div style="text-align: center">Data Cannot Be Found</div>
                                                                        </EmptyDataTemplate>

                                                                        <Columns>
                                                                            <asp:TemplateField HeaderText="ไฟล์เอกสารเพิ่มเติม">
                                                                                <ItemTemplate>
                                                                                    <div class="col-lg-8">
                                                                                        <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />

                                                                                    </div>
                                                                                    <div class="col-lg-2">
                                                                                        <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-default" data-original-title="Download" Target="_blank"><i class="fa fa-download"></i></asp:HyperLink>

                                                                                        <asp:HiddenField runat="server" ID="hidFile11" Value='<%# Eval("Download") %>' />
                                                                                    </div>

                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>

                                                            </div>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>

                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                    <asp:HyperLink runat="server" ID="txtfocus_" />
                                    <div class="panel panel-red">
                                        <div class="panel-heading">
                                            <h3 class="panel-title"><i class="glyphicon glyphicon-home"></i><strong>&nbsp; Address & Contact (Present) / ที่อยู่และการติดต่อ(ปัจจุบัน)</strong></h3>
                                        </div>
                                        <asp:Panel ID="Panel2" runat="server" CssClass="bg-regis">
                                            <div class="panel-body">
                                                <div class="form-horizontal" role="form">

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label63" runat="server" Text="PresentAddress" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label66" runat="server" Text="ที่อยู่ปัจจุบัน" /></b></small>
                                                            </h2>
                                                        </label>
                                                        <div class="col-sm-4">
                                                            <asp:TextBox ID="txtaddress_present_mini" autocomplete="off" MaxLength="250" TextMode="MultiLine" Rows="3" CssClass="form-control" runat="server" ValidationGroup="SaveAdd"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label68" runat="server" Text="Country" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label69" runat="server" Text="ประเทศ" /></b></small>
                                                            </h2>
                                                        </label>

                                                        <div class="col-sm-2">
                                                            <asp:DropDownList ID="ddlcountry_present_mini" runat="server" CssClass="form-control">
                                                                <asp:ListItem Value="0">เลือกประเทศ....</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>

                                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Lab3el15" runat="server" Text="Province" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label416" runat="server" Text="จังหวัด" /></b></small>
                                                            </h2>
                                                        </label>

                                                        <div class="col-sm-2">
                                                            <asp:DropDownList ID="ddlprovince_present_mini" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                <asp:ListItem Value="0">เลือกจังหวัด....</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label71" runat="server" Text="Amphoe" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label72" runat="server" Text="อำเภอ" /></b></small>
                                                            </h2>
                                                        </label>

                                                        <div class="col-sm-2">
                                                            <asp:DropDownList ID="ddlamphoe_present_mini" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                <asp:ListItem Value="0">เลือกอำเภอ....</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>

                                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label73" runat="server" Text="District" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label75" runat="server" Text="ตำบล" /></b></small>
                                                            </h2>
                                                        </label>

                                                        <div class="col-sm-2">
                                                            <asp:DropDownList ID="ddldistrict_present_mini" runat="server" CssClass="form-control">
                                                                <asp:ListItem Value="0">เลือกตำบล....</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label79" runat="server" Text="ZipCode" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label80" runat="server" Text="รหัสไปรษณีย์" /></b></small>
                                                            </h2>
                                                        </label>
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txtzipcode_present" CssClass="form-control" runat="server" ValidationGroup="SaveAdd"></asp:TextBox>
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>

                                    <%-----------------------------------------------------------------------------------------------%>
                                    <%--Prior Experiences--%>
                                    <%-----------------------------------------------------------------------------------------------%>
                                    <div class="panel panel-red">
                                        <div class="panel-heading">
                                            <h3 class="panel-title"><i class="glyphicon glyphicon-briefcase"></i><strong>&nbsp; Prior Experiences / ประวัติการทำงาน</strong></h3>
                                        </div>
                                        <asp:Panel ID="Panel3" runat="server" CssClass="bg-regis">
                                            <div class="panel-body">
                                                <div class="form-horizontal" role="form">

                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <div class="checkbox">
                                                                <asp:CheckBox ID="chkorg" runat="server" Text="เพิ่มสถานที่ทำงาน" CssClass="col-sm-4" AutoPostBack="true" OnCheckedChanged="chkSelectedIndexChanged" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <asp:Panel ID="BoxOrgOld_mini" runat="server" Visible="false">

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label159" runat="server" Text="Organization Name" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="lblorgold" runat="server" Text="ชื่อบริษัท" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtorgold" CssClass="form-control" runat="server" ValidationGroup="AddPrior_mini"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="Reqlidator1" ValidationGroup="AddPrior_mini" runat="server" Display="None"
                                                                    ControlToValidate="txtorgold" Font-Size="11"
                                                                    ErrorMessage="ชื่อบริษัท ...." />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="Validnder2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Reqlidator1" Width="160" />
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label161" runat="server" Text="Recent Positions" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label162" runat="server" Text="ตำแหน่งล่าสุด" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtposold" CssClass="form-control" runat="server" ValidationGroup="AddPrior_mini"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="Rtor1" ValidationGroup="AddPrior_mini" runat="server" Display="None"
                                                                    ControlToValidate="txtposold" Font-Size="11"
                                                                    ErrorMessage="ตำแหน่งล่าสุด ...." />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="Vaender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Rtor1" Width="160" />
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label253" runat="server" Text="Start Date" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label254" runat="server" Text="ปีที่เริ่มงาน" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <div class='input-group date'>
                                                                    <asp:TextBox ID="txtstartdate_ex" CssClass="form-control from-date-datepicker" MaxLengh="100%" runat="server" ValidationGroup="AddPrior_mini"></asp:TextBox>
                                                                    <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="Requisrer1" ValidationGroup="AddPrior_mini" runat="server" Display="None"
                                                                    ControlToValidate="txtstartdate_ex" Font-Size="11"
                                                                    ErrorMessage="ปีที่เริ่มงาน ...." />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="Validator5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requisrer1" Width="160" />
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label255" runat="server" Text="End Date" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label256" runat="server" Text="ปีที่ออกจากงาน" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <div class='input-group date'>
                                                                    <asp:TextBox ID="txtresigndate_ex" CssClass="form-control from-date-datepicker" MaxLengh="100%" runat="server" ValidationGroup="AddPrior_mini"></asp:TextBox>
                                                                    <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="RdValidatsar2" ValidationGroup="AddPrior_mini" runat="server" Display="None"
                                                                    ControlToValidate="txtresigndate_ex" Font-Size="11"
                                                                    ErrorMessage="ปีที่ออกจากงาน ...." />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="Validaas" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RdValidatsar2" Width="160" />
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label47" runat="server" Text="salary" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label49" runat="server" Text="เงินเดือนล่าสุด" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtprisalary" CssClass="form-control" runat="server" ValidationGroup="AddPrior_mini"></asp:TextBox>
                                                            </div>
                                                            <asp:RequiredFieldValidator ID="Requirea" ValidationGroup="AddPrior_mini" runat="server" Display="None"
                                                                ControlToValidate="txtprisalary" Font-Size="11"
                                                                ErrorMessage="เงินเดือนล่าสุด ...." />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="VautExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requirea" Width="160" />

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label42" runat="server" Text="Description" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label43" runat="server" Text="รายละเอียดงาน" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtpridescription" CssClass="form-control" runat="server" TextMode="MultiLine" Rows="3" ValidationGroup="AddPrior_mini"></asp:TextBox>
                                                            </div>
                                                            <asp:RequiredFieldValidator ID="Re1or1" ValidationGroup="AddPrior_mini" runat="server" Display="None"
                                                                ControlToValidate="txtpridescription" Font-Size="11"
                                                                ErrorMessage="รายละเอียดงาน ...." />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="Valor1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re1or1" Width="160" />

                                                            <div class="col-sm-1">
                                                                <asp:LinkButton ID="LinkButton2" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="AddPrior" ValidationGroup="AddPrior_mini">ADD + &nbsp;</asp:LinkButton>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-lg-12">

                                                                <asp:GridView ID="GvPri"
                                                                    runat="server"
                                                                    AutoGenerateColumns="false"
                                                                    CssClass="table table-striped table-bordered table-hover"
                                                                    HeaderStyle-CssClass="table_headCenter"
                                                                    HeaderStyle-Height="40px"
                                                                    ShowFooter="False"
                                                                    ShowHeaderWhenEmpty="True"
                                                                    AllowPaging="True"
                                                                    PageSize="5"
                                                                    BorderStyle="None"
                                                                    CellSpacing="2"
                                                                    OnRowDeleting="gvRowDeleting">

                                                                    <PagerStyle CssClass="PageCustom" />
                                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                                    <EmptyDataTemplate>
                                                                        <div style="text-align: center">Data Cannot Be Found</div>
                                                                    </EmptyDataTemplate>

                                                                    <Columns>

                                                                        <asp:TemplateField HeaderText="#">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <%# (Container.DataItemIndex +1) %>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="ชื่อบริษัท">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="Label5w8" runat="server" Text='<%# Eval("Pri_Nameorg")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="ตำแหน่งล่าสุด">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="lbP23ag212e1" runat="server" Text='<%# Eval("Pri_pos")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="วันที่เริ่มงาน">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="lbP2a1g2e1" runat="server" Text='<%# Eval("Pri_startdate")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="วันที่ออกจากงาน">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="lbP2ag2e1" runat="server" Text='<%# Eval("Pri_resigndate")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="รายละเอียดงาน">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="lbP22ag2e1" runat="server" Text='<%# Eval("Pri_description")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="เงินเดือนล่าสุด">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="lbP23ag2e1" runat="server" Text='<%# Eval("Pri_salary")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="จัดการ">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="btnDeleteGV" runat="server" ValidationGroup="fromgroup" CommandName="Delete" Text="Delete" CssClass="btn btn-danger" OnClientClick="return confirm('Do you want delete this item?')" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                    </Columns>

                                                                </asp:GridView>

                                                            </div>
                                                        </div>

                                                    </asp:Panel>

                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>

                                    <%-----------------------------------------------------------------------------------------------%>
                                    <%--Education--%>
                                    <%-----------------------------------------------------------------------------------------------%>
                                    <div class="panel panel-red">
                                        <div class="panel-heading">
                                            <h3 class="panel-title"><i class="glyphicon glyphicon-education"></i><strong>&nbsp; Education / ประวัติการศึกษา</strong></h3>
                                        </div>
                                        <asp:Panel ID="Panel4" runat="server" CssClass="bg-regis">
                                            <div class="panel-body">
                                                <div class="form-horizontal" role="form">

                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <div class="checkbox">
                                                                <asp:CheckBox ID="ckeducation" runat="server" Text="เพิ่มประวัติการศึกษา" CssClass="col-sm-4" AutoPostBack="true" OnCheckedChanged="chkSelectedIndexChanged" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <asp:Panel ID="BoxEducation" runat="server" Visible="false">
                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label ">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label176" runat="server" Text="Educational" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label177" runat="server" Text="วุฒิการศึกษา" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:DropDownList ID="ddleducationback" runat="server" CssClass="form-control" ValidationGroup="AddEducation_mini">
                                                                    <asp:ListItem Value="0">เลือกวุฒิการศึกษา....</asp:ListItem>
                                                                    <asp:ListItem Value="7">ไม่มีวุฒิการศึกษา</asp:ListItem>
																	<asp:ListItem Value="1">ประถมศึกษา ป.6</asp:ListItem>
																	<asp:ListItem Value="2">มัธยมศึกษาตอนต้น ม.3</asp:ListItem>
																	<asp:ListItem Value="3">มัธยมศึกษาตอนปลาย ม.6</asp:ListItem>
																	<asp:ListItem Value="8">อาชีวศึกษา ปวช</asp:ListItem>
																	<asp:ListItem Value="9">อาชีวศึกษา ปวส</asp:ListItem>
																	<asp:ListItem Value="4">ปริญญาตรี</asp:ListItem>
																	<asp:ListItem Value="5">ปริญญาโท</asp:ListItem>
																	<asp:ListItem Value="6">ปริญญาเอก</asp:ListItem>
																	<asp:ListItem Value="10">กศน.มัธยม ต้น</asp:ListItem>
																	<asp:ListItem Value="11">กศน.มัธยม ปลาย</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                            <asp:RequiredFieldValidator ID="Reqsuiredator1" ValidationGroup="AddEducation_mini" runat="server" Display="None"
                                                                ControlToValidate="ddleducationback" Font-Size="11"
                                                                ErrorMessage="เลือกวุฒิการศึกษา ...."
                                                                ValidationExpression="เลือกวุฒิการศึกษา ...." InitialValue="0" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="Valider2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Reqsuiredator1" Width="160" />

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label168" runat="server" Text="School Name" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label169" runat="server" Text="ชื่อสถานศึกษา" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtschoolname" CssClass="form-control" runat="server" ValidationGroup="AddEducation_mini"></asp:TextBox>
                                                            </div>
                                                            <asp:RequiredFieldValidator ID="Reqasar1" ValidationGroup="AddEducation_mini" runat="server" Display="None"
                                                                ControlToValidate="txtschoolname" Font-Size="11"
                                                                ErrorMessage="เลือกวุฒิการศึกษา ...." />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="Val3er2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Reqasar1" Width="160" />
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label172" runat="server" Text="Start Date" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label173" runat="server" Text="ปีที่เริ่มศึกษา" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <div class='input-group date'>
                                                                    <asp:TextBox ID="txtstarteducation" CssClass="form-control from-date-datepicker" MaxLengh="100%" runat="server" ValidationGroup="AddEducation_mini"></asp:TextBox>
                                                                    <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="Reaazidator1" ValidationGroup="AddEducation_mini" runat="server" Display="None"
                                                                    ControlToValidate="txtstarteducation" Font-Size="11"
                                                                    ErrorMessage="ปีที่เริ่มศึกษา ...." />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="Vaqr2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Reaazidator1" Width="160" />
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label174" runat="server" Text="End Date" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label175" runat="server" Text="ปีที่จบการศึกษา" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <div class='input-group date'>
                                                                    <asp:TextBox ID="txtendeducation" CssClass="form-control from-date-datepicker" MaxLengh="100%" runat="server" ValidationGroup="AddEducation_mini"></asp:TextBox>
                                                                    <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="Reqpxr1" ValidationGroup="AddEducation_mini" runat="server" Display="None"
                                                                    ControlToValidate="txtendeducation" Font-Size="11"
                                                                    ErrorMessage="ปีที่จบการศึกษา ...." />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="Validopr2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Reqpxr1" Width="160" />
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label170" runat="server" Text="Branch" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label171" runat="server" Text="สาขาวิชาที่ศึกษา" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtstudy" CssClass="form-control" runat="server" ValidationGroup="AddEducation_mini"></asp:TextBox>
                                                            </div>
                                                            <asp:RequiredFieldValidator ID="Repc1" ValidationGroup="AddEducation_mini" runat="server" Display="None"
                                                                ControlToValidate="txtstudy" Font-Size="11"
                                                                ErrorMessage="สาขาวิชาที่ศึกษา ...." />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="Val52" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Repc1" Width="160" />

                                                            <div class="col-sm-1 col-sm-offset-2">
                                                                <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="AddEducation" ValidationGroup="AddEducation_mini">ADD + &nbsp;</asp:LinkButton>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-lg-12">

                                                                <asp:GridView ID="GvEducation"
                                                                    runat="server"
                                                                    AutoGenerateColumns="false"
                                                                    CssClass="table table-striped table-bordered table-hover"
                                                                    HeaderStyle-CssClass="table_headCenter"
                                                                    HeaderStyle-Height="40px"
                                                                    ShowFooter="False"
                                                                    ShowHeaderWhenEmpty="True"
                                                                    AllowPaging="True"
                                                                    PageSize="5"
                                                                    BorderStyle="None"
                                                                    CellSpacing="2"
                                                                    OnRowDeleting="gvRowDeleting">

                                                                    <PagerStyle CssClass="PageCustom" />
                                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                                    <EmptyDataTemplate>
                                                                        <div style="text-align: center">Data Cannot Be Found</div>
                                                                    </EmptyDataTemplate>

                                                                    <Columns>

                                                                        <asp:TemplateField HeaderText="#">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <%# (Container.DataItemIndex +1) %>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="วุฒิการศึกษา">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="Label5w8" runat="server" Text='<%# Eval("Edu_qualification")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="ชื่อสถานศึกษา">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="lbP24ag2e1" runat="server" Text='<%# Eval("Edu_name")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="สาขาวิชาที่ศึกษา">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="lb4Pag2e1" runat="server" Text='<%# Eval("Edu_branch")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="ปีที่เริ่มศึกษา">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="lbP2ag2e1" runat="server" Text='<%# Eval("Edu_start")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="ปีที่จบศึกษา">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="lbP3ag2e1" runat="server" Text='<%# Eval("Edu_end")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="จัดการ">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="btnDeleteGV" runat="server" ValidationGroup="fromgroup" CommandName="Delete" Text="Delete" CssClass="btn btn-danger" OnClientClick="return confirm('Do you want delete this item?')" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                    </Columns>

                                                                </asp:GridView>

                                                            </div>
                                                        </div>

                                                    </asp:Panel>

                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>

                                </asp:Panel>

                            </InsertItemTemplate>
                        </asp:FormView>

                        <div class="col-sm-1 col-sm-offset-1">
                            <asp:LinkButton ID="btnsave_mini" runat="server" CssClass="btn btn-default" OnCommand="btnCommand" CommandName="btnSave" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" ValidationGroup="InsertData_Mini">Save &nbsp;</asp:LinkButton>
                        </div>


                    </div>
                </div>

            </asp:Panel>

            <div id="ordine_test_mini" class="modal open" role="dialog">
                <div class="modal-dialog" style="width: 50%;">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header bg-template-top">
                            <h3 class="modal-title">
                                <asp:Label ID="Label106" runat="server" CssClass="control-label" Text="แบบทดสอบ :" />
                                <asp:Label ID="Label110" runat="server" CssClass="control-label" /></h3>
                            <marquee><asp:Label ID="Label111" runat="server" CssClass="control-label" Font-Bold="True" /></marquee>
                        </div>
                        <div class="modal-body bg-template-center_tranfer">
                            <div class="panel-body">
                                <div class="col-sm-12">
                                    <%--<div class="form-group">--%>

                                    <%--</div>--%>
                                </div>
                            </div>

                        </div>
                        <%--<div class="modal-footer">--%>
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <asp:LinkButton ID="lbttranfer_test_mini" runat="server" CssClass="btn btn-default bg-template-buttom_tranfer" OnCommand="btnCommand" CommandName="btntranfer_test_mini"></asp:LinkButton>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="lbttranfer_test_mini" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </asp:View>

        <asp:View ID="ViewAdd_full" runat="server">
            <asp:Label ID="fsaw" runat="server"></asp:Label>

            <asp:Panel ID="Panel5" runat="server">
                <div class="panel-body">
                    <div class="form-horizontal" role="form">
                        <asp:Label ID="fsa" runat="server"></asp:Label>
                        <asp:HyperLink runat="server" ID="HyperLink1" />
                        <div class="form-group">
                            <label class="col-sm-1"></label>
                            <label class="col-sm-10">
                                <asp:Image ID="Image2" CssClass="img-responsive" ImageUrl="~/masterpage/images/hr/banner_register_1.png" runat="server" Style="max-height:400px;  width: auto;margin: 0 auto;" />
                            </label>
                            <label class="col-sm-1"></label>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-1"></label>
                            <label class="col-sm-10">
                                <asp:Image ID="Image32" CssClass="img-responsive img-fluid" Width="35%" Height="30%" ImageUrl="~/masterpage/images/hr/topic_register.png" runat="server" />
                            </label>
                        </div>

                        <asp:FormView ID="FvInsertEmp" runat="server" Width="80%" HorizontalAlign="Center" OnDataBound="FvDetail_DataBound">
                            <InsertItemTemplate>

                                <%-------------------------------- ข้อมูลทั่วไป --------------------------------%>
                                <asp:Panel ID="BoxGen" runat="server">

                                    <div class="panel panel-red">
                                        <div class="panel-heading">
                                            <h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i><strong>&nbsp; Position Interested / ประเภทงานที่สนใจ</strong></h3>
                                        </div>
                                        <asp:Panel ID="pn_bg_genarol" runat="server" CssClass="bg-regis">
                                            <div class="panel-body">
                                                <div class="form-horizontal" role="form">

                                                    <div class="col-sm-7">

                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label ">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label270" runat="server" Text="Employee In" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label271" runat="server" Text="วันที่สามารถเริ่มงานได้" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-5">
                                                                <asp:DropDownList ID="ddlstartdate_in" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0">เลือกวันที่พร้อมเริ่มงาน.....</asp:ListItem>
                                                                    <asp:ListItem Value="1">ไม่แน่ใจ</asp:ListItem>
                                                                    <asp:ListItem Value="2">ทันที</asp:ListItem>
                                                                    <asp:ListItem Value="3">7 วัน</asp:ListItem>
                                                                    <asp:ListItem Value="4">15 วัน</asp:ListItem>
                                                                    <asp:ListItem Value="5">1 เดือน</asp:ListItem>
                                                                    <asp:ListItem Value="6">2 เดือน</asp:ListItem>
                                                                    <asp:ListItem Value="7">3 เดือน</asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="Req1" ValidationGroup="InsertData" runat="server" Display="None"
                                                                    ControlToValidate="ddlstartdate_in" Font-Size="11"
                                                                    ErrorMessage="วันที่สามารถเริ่มงานได้ ...."
                                                                    ValidationExpression="วันที่สามารถเริ่มงานได้ ...." InitialValue="0" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="Valider4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req1" Width="160" />
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <asp:Label ID="Label272" runat="server" ForeColor="Red" Text="***" />
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label204" runat="server" Text="Salary" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label205" runat="server" Text="เงินเดือนที่ต้องการ" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-5">
                                                                <asp:TextBox ID="txtsalary" CssClass="form-control" runat="server" ValidationGroup="InsertData"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="RequirValidator1" ValidationGroup="InsertData" runat="server" Display="None"
                                                                    ControlToValidate="txtsalary" Font-Size="11"
                                                                    ErrorMessage="เงินเดือนที่ต้องการ" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="Validtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequirValidator1" Width="160" />
                                                                <asp:RegularExpressionValidator ID="RetxtUnit" runat="server" ValidationGroup="InsertData" Display="None"
                                                                    ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                                    ControlToValidate="txtsalary"
                                                                    ValidationExpression="^[0-9]{1,10}$" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="Valitender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RetxtUnit" Width="160" />
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <asp:Label ID="Label202" runat="server" ForeColor="Red" Text="***" />
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label ">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label198" runat="server" Text="Job Category" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label201" runat="server" Text="ประเภทงานที่ต้องการ" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-5">
                                                                <asp:DropDownList ID="ddltypecategory" runat="server" CssClass="form-control" ValidationGroup="InsertData">
                                                                    <asp:ListItem Value="0">เลือกประเภทงานที่ต้องการ...</asp:ListItem>
                                                                    <asp:ListItem Value="1">ประจำ</asp:ListItem>
                                                                    <asp:ListItem Value="2">Part-time</asp:ListItem>
                                                                    <asp:ListItem Value="3">Freelance</asp:ListItem>
                                                                    <asp:ListItem Value="4">ฝึกงาน</asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="RequiredFr1" ValidationGroup="InsertData" runat="server" Display="None"
                                                                    ControlToValidate="ddltypecategory" Font-Size="11"
                                                                    ErrorMessage="ประเภทงานที่ต้องการ ...."
                                                                    ValidationExpression="ประเภทงานที่ต้องการ ...." InitialValue="0" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="Validaer1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFr1" Width="160" />
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <asp:Label ID="Label203" runat="server" ForeColor="Red" Text="***" />
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label ">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label74" runat="server" Text="Position" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label77" runat="server" Text="ตำแหน่งงานที่เปิดรับ" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-5">
                                                                <asp:DropDownList ID="ddlpositionfocus" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0">เลือกตำแหน่งงาน....</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label ">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label99" runat="server" Text="Position Group" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label100" runat="server" Text="กลุ่มประเภทงานที่ต้องการ" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-5">
                                                                <asp:DropDownList ID="ddlposition1" runat="server" CssClass="form-control" ValidationGroup="InsertData">
                                                                    <asp:ListItem Value="0">เลือกประเภทกลุ่มงาน 1....</asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldVal1" ValidationGroup="InsertData" runat="server" Display="None"
                                                                    ControlToValidate="ddlposition1" Font-Size="11"
                                                                    ErrorMessage="กลุ่มประเภทงานที่ต้องการ ...."
                                                                    ValidationExpression="กลุ่มประเภทงานที่ต้องการ ...." InitialValue="0" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCala" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldVal1" Width="160" />
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <asp:Label ID="Label206" runat="server" ForeColor="Red" Text="***" />
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label ">
                                                            </label>

                                                            <div class="col-sm-5">
                                                                <asp:DropDownList ID="ddlposition2" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0">เลือกประเภทกลุ่มงาน 2....</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label ">
                                                            </label>

                                                            <div class="col-sm-5">
                                                                <asp:DropDownList ID="ddlposition3" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0">เลือกประเภทกลุ่มงาน 3....</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label207" runat="server" Text="Description" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label208" runat="server" Text="รายละเอียดเพิ่มเติม/ความสามารถพิเศษ" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-5">
                                                                <asp:TextBox ID="txtdescription" CssClass="form-control" runat="server" autocomplete="off" MaxLength="250" TextMode="MultiLine" Rows="3" ValidationGroup="AddReference1"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="col-sm-5">
                                                        <asp:UpdatePanel ID="UpdateP2anel4" runat="server">
                                                            <ContentTemplate>
                                                                <div class="form-group">
                                                                    <div class="col-sm-8">
                                                                        <div class="panel panel-red">
                                                                            <div class="form-group">
                                                                                <div class="panel-body">
                                                                                    <center><asp:Image ID="img_profile_full" runat="server" style="width: 100px; height: 100px" class="img-thumbnail"></asp:Image></center>
                                                                                    <center><asp:Panel id="ImagProfile_default_full" runat="server">
                                                                            <img class="media-object img-thumbnail" src='<%=ResolveUrl("~/uploadfiles/profile_picture/default/user-default.png")%>' width="100px" height="100px" style="margin-right: 8px; margin-top: -5px;">
                                                                        </asp:Panel></center>

                                                                                    &nbsp;&nbsp;<asp:FileUpload ID="upload_profile_pi_full" AutoPostBack="true" Font-Size="small" ClientIDMode="Static" runat="server" accept="jpg" CssClass="btn btn-sm multi max-1" />
                                                                                    <center><font color="red"><small class="text_right"><p>**เฉพาะนามสกุล jpg</small></font></center>
                                                                                    <hr />
                                                                                    <center><asp:LinkButton ID="btn_save_file1_full" runat="server" CssClass="btn btn-warning btn-sm" data-toggle="tooltip" title="อัพโหลดภาพ" CommandName="savefile_picture_full" OnCommand="btnCommand"><i class="glyphicon glyphicon-picture"></i> อัพโหลดภาพ</asp:LinkButton></center>
                                                                                    <center><font color="red"><small class="text_right"><p>กรุณากรอกข้อมูลบัตรประชาชนให้เรียบร้อยก่อนทำการ upload รูปภาพ</small></font></center>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="btn_save_file1_full" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                        <asp:HyperLink runat="server" ID="txtfocus_picture_full" />
                                                    </div>

                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>

                                    <div class="panel panel-red">
                                        <div class="panel-heading">
                                            <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; Personal Information / ข้อมูลส่วนตัว</strong></h3>
                                        </div>
                                        <asp:Panel ID="Panel1" runat="server" CssClass="bg-regis">
                                            <div class="panel-body">
                                                <div class="form-horizontal" role="form">

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label222" runat="server" Text="Name(Thai)" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label224" runat="server" Text="ชื่อ(ไทย)" /></b></small>
                                                            </h2>
                                                        </label>
                                                        <div class="col-sm-1">
                                                            <asp:DropDownList ID="ddl_prefix_th_full" runat="server" AutoPostBack="true" CssClass="form-control" Width="105" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                <asp:ListItem Value="1">นาย</asp:ListItem>
                                                                <asp:ListItem Value="2">นางสาว</asp:ListItem>
                                                                <asp:ListItem Value="3">นาง</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txt_name_th" CssClass="form-control" runat="server" ValidationGroup="InsertData"></asp:TextBox>

                                                            <asp:RequiredFieldValidator ID="C_NameTH_add" runat="server" ControlToValidate="txt_name_th" Display="None" SetFocusOnError="true" ErrorMessage="*กรอกข้อมูล" ValidationGroup="InsertData" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_NameTH_add" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="C_NameTH_add" Width="160" />
                                                            <asp:RegularExpressionValidator ID="R_NamTH_add" runat="server" ErrorMessage="*เฉพาะตัวอักษรภาษาไทยเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txt_name_th" ValidationExpression="^[ก-๙'.\s]{1,250}$" ValidationGroup="InsertData" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_NameTH2_add" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_NamTH_add" Width="160" />
                                                        </div>
                                                        <div class="col-sm-1">
                                                            <asp:Label ID="Label210" runat="server" ForeColor="Red" Text="***" />
                                                        </div>

                                                        <label class="col-sm-1 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label1" runat="server" Text="Lastname" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label4" runat="server" Text="นามสกุล" /></b></small>
                                                            </h2>
                                                        </label>
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txt_surname_th" CssClass="form-control" runat="server" ValidationGroup="InsertData"></asp:TextBox>

                                                            <asp:RequiredFieldValidator ID="C_LastnameTH_add" runat="server" ControlToValidate="txt_surname_th" Display="None" SetFocusOnError="true" ErrorMessage="*กรอกข้อมูล" ValidationGroup="InsertData" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_LastnameTH_add" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="C_LastnameTH_add" Width="160" />
                                                            <asp:RegularExpressionValidator ID="R_LastNameTH_add" runat="server" ErrorMessage="*เฉพาะตัวอักษรภาษาไทยเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txt_surname_th" ValidationExpression="^[ก-๙'.\s]{1,250}$" ValidationGroup="InsertData" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_LastNameTH2_add" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_LastNameTH_add" Width="160" />
                                                        </div>
                                                        <div class="col-sm-1">
                                                            <asp:Label ID="Label211" runat="server" ForeColor="Red" Text="***" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label5" runat="server" Text="Name(English)" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label22" runat="server" Text="ชื่อ(อังกฤษ)" /></b></small>
                                                            </h2>
                                                        </label>
                                                        <div class="col-sm-1">
                                                            <asp:DropDownList ID="ddl_prefix_en_full" runat="server" AutoPostBack="true" CssClass="form-control" Width="105" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                <asp:ListItem Value="1">Mr.</asp:ListItem>
                                                                <asp:ListItem Value="2">Miss</asp:ListItem>
                                                                <asp:ListItem Value="3">Mrs.</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txt_name_en" CssClass="form-control" runat="server" ValidationGroup="InsertData"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="C_NameEN_add" runat="server" ControlToValidate="txt_name_en" Display="None" SetFocusOnError="true" ErrorMessage="*กรอกข้อมูล" ValidationGroup="InsertData" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_NameEN_add" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="C_NameEN_add" Width="160" />

                                                            <asp:RegularExpressionValidator ID="R_NameEN_add" runat="server" ErrorMessage="*เเฉพาะตัวอักษรภาษาอังกฤษเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txt_name_en" ValidationExpression="^[a-zA-Z'.\s]{1,250}$" ValidationGroup="InsertData" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_NameEN2_add" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_NameEN_add" Width="160" />
                                                        </div>
                                                        <div class="col-sm-1">
                                                            <asp:Label ID="Label212" runat="server" ForeColor="Red" Text="***" />
                                                        </div>

                                                        <label class="col-sm-1 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label8" runat="server" Text="Lastname" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label24" runat="server" Text="นามสกุล" /></b></small>
                                                            </h2>
                                                        </label>
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txt_surname_en" CssClass="form-control" runat="server" ValidationGroup="InsertData"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="C_LastnameEN_add" runat="server" ControlToValidate="txt_surname_en" Display="None" SetFocusOnError="true" ErrorMessage="*กรอกข้อมูล" ValidationGroup="InsertData" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_LastnameEN_add" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="C_LastnameEN_add" Width="160" />

                                                            <asp:RegularExpressionValidator ID="R_LastNameEN_add" runat="server" ErrorMessage="*เฉพาะตัวอักษรภาษาอังกฤษเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txt_surname_en" ValidationExpression="^[a-zA-Z'.\s]{1,250}$" ValidationGroup="InsertData" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_LastnameEN2_add" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_LastNameEN_add" Width="160" />
                                                        </div>
                                                        <div class="col-sm-1">
                                                            <asp:Label ID="Label213" runat="server" ForeColor="Red" Text="***" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label10" runat="server" Text="NickName(Thai)" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label25" runat="server" Text="ชื่อเล่น(ไทย)" /></b></small>
                                                            </h2>
                                                        </label>
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txt_nickname_th" CssClass="form-control" runat="server" ValidationGroup="InsertData"></asp:TextBox>

                                                            <asp:RegularExpressionValidator ID="R_NickNameTH_add" runat="server" ErrorMessage="*เฉพาะตัวอักษรภาษาไทยเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txt_nickname_th" ValidationExpression="^[ก-๙'.\s]{1,250}$" ValidationGroup="InsertData" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_NickNameTH_add" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_NickNameTH_add" Width="160" />
                                                        </div>
                                                        <div class="col-sm-1">
                                                            <asp:Label ID="Label108" runat="server" ForeColor="Red" Text="***" />
                                                        </div>

                                                        <label class="col-sm-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label11" runat="server" Text="NickName(English)" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label26" runat="server" Text="ชื่อเล่น(อังกฤษ)" /></b></small>
                                                            </h2>
                                                        </label>
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txt_nickname_en" CssClass="form-control" runat="server" ValidationGroup="InsertData"></asp:TextBox>

                                                            <asp:RegularExpressionValidator ID="R_NickNameEN_add" runat="server" ErrorMessage="*เฉพาะตัวอักษรภาษาไทยเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txt_nickname_en" ValidationExpression="^[a-zA-Z'.\s]{1,250}$" ValidationGroup="InsertData" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_NickNameEN_add" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_NickNameEN_add" Width="160" />
                                                        </div>
                                                        <div class="col-sm-1">
                                                            <asp:Label ID="Label109" runat="server" ForeColor="Red" Text="***" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label15" runat="server" Text="Birthday" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label16" runat="server" Text="วันเกิด" /></b></small>
                                                            </h2>
                                                        </label>
                                                        <div class="col-sm-2">
                                                            <div class='input-group date'>
                                                                <asp:TextBox ID="txtbirth" CssClass="form-control from-date-datepicker" MaxLengh="100%" runat="server" ValidationGroup="InsertData"></asp:TextBox>
                                                                <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                            </div>
                                                            <asp:RequiredFieldValidator ID="RdValid23ator2" ValidationGroup="InsertData" runat="server" Display="None"
                                                                ControlToValidate="txtbirth" Font-Size="11"
                                                                ErrorMessage="วันเกิด ...." />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="Validlwoer6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RdValid23ator2" Width="160" />
                                                        </div>
                                                        <div class="col-sm-1">
                                                            <asp:Label ID="Label107" runat="server" ForeColor="Red" Text="***" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label ">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label27" runat="server" Text="Sex" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label28" runat="server" Text="เพศ" /></b></small>
                                                            </h2>
                                                        </label>

                                                        <div class="col-sm-2">
                                                            <asp:DropDownList ID="ddl_sex_full" runat="server" CssClass="form-control" ValidationGroup="InsertData">
                                                                <asp:ListItem Value="0">เลือกสถานะเพศ....</asp:ListItem>
                                                                <asp:ListItem Value="1">ชาย</asp:ListItem>
                                                                <asp:ListItem Value="2">หญิง</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="Reqquiar1" ValidationGroup="InsertData" runat="server" Display="None"
                                                                ControlToValidate="ddl_sex_full" Font-Size="11"
                                                                ErrorMessage="เพศ ...."
                                                                ValidationExpression="เพศ ...." InitialValue="0" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="Validastoder1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Reqquiar1" Width="160" />
                                                        </div>
                                                        <div class="col-sm-1">
                                                            <asp:Label ID="Label105" runat="server" ForeColor="Red" Text="***" />
                                                        </div>

                                                        <label class="col-sm-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Labe3l15" runat="server" Text="Status" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Lab2el16" runat="server" Text="สถานภาพ" /></b></small>
                                                            </h2>
                                                        </label>

                                                        <div class="col-sm-2">
                                                            <asp:DropDownList ID="ddl_status" runat="server" CssClass="form-control">
                                                                <asp:ListItem Value="0">เลือกสถานภาพ....</asp:ListItem>
                                                                <asp:ListItem Value="1">โสด</asp:ListItem>
                                                                <asp:ListItem Value="2">สมรส</asp:ListItem>
                                                                <asp:ListItem Value="3">หย่าร้าง</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Labe4l231" runat="server" Text="Nationality" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Lab3el232" runat="server" Text="สัญชาติ" /></b></small>
                                                            </h2>
                                                        </label>
                                                        <div class="col-sm-2">
                                                            <asp:DropDownList ID="ddlnation" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                <asp:ListItem Value="0">เลือกสัญชาติ....</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>

                                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="La2bel42" runat="server" Text="Race" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="La4bel43" runat="server" Text="เชื้อชาติ" /></b></small>
                                                            </h2>
                                                        </label>
                                                        <div class="col-sm-2">
                                                            <asp:DropDownList ID="ddlrace" runat="server" CssClass="form-control">
                                                                <asp:ListItem Value="0">เลือกเชื้อชาติ....</asp:ListItem>
                                                                <asp:ListItem Value="1">ไทย</asp:ListItem>
                                                                <asp:ListItem Value="2">ลาว</asp:ListItem>
                                                                <asp:ListItem Value="3">พม่า</asp:ListItem>
                                                                <asp:ListItem Value="4">กัมพูชา</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Lab4el47" runat="server" Text="Religion" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Lab3el49" runat="server" Text="ศาสนา" /></b></small>
                                                            </h2>
                                                        </label>

                                                        <div class="col-sm-2">
                                                            <asp:DropDownList ID="ddlreligion" runat="server" CssClass="form-control">
                                                                <asp:ListItem Value="0">เลือกศาสนา....</asp:ListItem>
                                                                <asp:ListItem Value="1">ศาสนาพุทธ</asp:ListItem>
                                                                <asp:ListItem Value="2">ศาสนาคริสต์</asp:ListItem>
                                                                <asp:ListItem Value="3">ศาสนาอิศลาม</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label29" runat="server" Text="MilitaryStatus" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label31" runat="server" Text="สถานะทางทหาร" /></b></small>
                                                            </h2>
                                                        </label>

                                                        <div class="col-sm-2">
                                                            <asp:DropDownList ID="ddlmilitary_full" runat="server" CssClass="form-control" ValidationGroup="InsertData">
                                                                <asp:ListItem Value="1">ผ่านการเกณทหาร</asp:ListItem>
                                                                <asp:ListItem Value="2">เรียนรักษาดินแดน</asp:ListItem>
                                                                <asp:ListItem Value="3">ได้รับข้อยกเว้น</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="Requsator1" ValidationGroup="InsertData" runat="server" Display="None"
                                                                ControlToValidate="ddlmilitary_full" Font-Size="11"
                                                                ErrorMessage="สถานะทางทหาร ...."
                                                                ValidationExpression="สถานะทางทหาร ...." InitialValue="0" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="Validat2orCalder1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requsator1" Width="160" />
                                                        </div>
                                                        <div class="col-sm-1">
                                                            <asp:Label ID="Label104" runat="server" ForeColor="Red" Text="***" />
                                                        </div>

                                                        <label class="col-sm-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label32" runat="server" Text="ID Card" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label33" runat="server" Text="เลขบัตรประชาชน / Tax ID (ต่างชาติ)" /></b></small>
                                                            </h2>
                                                        </label>
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txtidcard" CssClass="form-control" MaxLength="13" runat="server" ValidationGroup="InsertData"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="Reator1" ValidationGroup="InsertData" runat="server" Display="None"
                                                                ControlToValidate="txtidcard" Font-Size="11"
                                                                ErrorMessage="เลขบัตรประชาชน" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="Vaaalwr1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Reator1" Width="160" />
                                                            <asp:RegularExpressionValidator ID="Regaddaur1" runat="server" ValidationGroup="InsertData" Display="None"
                                                                ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                                ControlToValidate="txtidcard"
                                                                ValidationExpression="^[0-9]{1,13}$" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="Valiasdader4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Regaddaur1" Width="160" />
                                                        </div>
                                                        <div class="col-sm-1">
                                                            <asp:Label ID="Label101" runat="server" ForeColor="Red" Text="***" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label36" runat="server" Text="Issued At" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label37" runat="server" Text="อกกให้ ณ" /></b></small>
                                                            </h2>
                                                        </label>
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txtissued_at" CssClass="form-control" runat="server" ValidationGroup="SaveAdd"></asp:TextBox>
                                                        </div>

                                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label34" runat="server" Text="Expiration Date" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label35" runat="server" Text="วันหมดอายุ" /></b></small>
                                                            </h2>
                                                        </label>
                                                        <div class="col-sm-2">
                                                            <div class='input-group date'>
                                                                <asp:TextBox ID="txtexpcard" CssClass="form-control from-date-datepicker" MaxLengh="100%" runat="server" ValidationGroup="SaveAdd"></asp:TextBox>
                                                                <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                                                        <ContentTemplate>
                                                            <div class="form-group">

                                                                <label class="col-sm-2 control-label">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label2271" runat="server" Text="Upload document" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Labe2l269" runat="server" Text="เอกสารเพิ่มเติม" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-3">
                                                                    <div class="panel panel-red">
                                                                        <div class="form-group">
                                                                            <div class="panel-body">
                                                                                <asp:FileUpload ID="FileUpload_full" ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small" ClientIDMode="Static" runat="server" CssClass="btn btn-sm multi" accept="gif|jpg|png|JPEG|pdf" />
                                                                                <center><font color="red"><small class="text_right"><p>**เฉพาะนามสกุล gif , jpg , png , pdf</small></font></center>
                                                                                <hr />
                                                                                <center><asp:LinkButton ID="btn_save_document_full" runat="server" CssClass="btn btn-warning btn-sm" data-toggle="tooltip" title="อัพโหลดภาพ" CommandName="savefile_document_full" OnCommand="btnCommand"><i class="glyphicon glyphicon-picture"></i> อัพโหลดภาพ</asp:LinkButton></center>
                                                                                <center><font color="red"><small class="text_right"><p>กรุณากรอกข้อมูลบัตรประชาชนให้เรียบร้อยก่อนทำการ upload รูปภาพ</small></font></center>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <label class="col-sm-2 control-label">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label223" runat="server" Text="Document List" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Lab3el30" runat="server" Text="เอกสารแนบทั้งหมด" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-3">
                                                                    <asp:GridView ID="gvFileEmp_full" Visible="true" runat="server"
                                                                        AutoGenerateColumns="false"
                                                                        CssClass="table table-striped table-bordered table-hover table-responsive"
                                                                        HeaderStyle-CssClass="warning"
                                                                        OnRowDataBound="Master_RowDataBound"
                                                                        BorderStyle="None"
                                                                        CellSpacing="2"
                                                                        Font-Size="Small">
                                                                        <EmptyDataTemplate>
                                                                            <div style="text-align: center">Data Cannot Be Found</div>
                                                                        </EmptyDataTemplate>

                                                                        <Columns>
                                                                            <asp:TemplateField HeaderText="ไฟล์เอกสารเพิ่มเติม">
                                                                                <ItemTemplate>
                                                                                    <div class="col-lg-8">
                                                                                        <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />

                                                                                    </div>
                                                                                    <div class="col-lg-2">
                                                                                        <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-default" data-original-title="Download" Target="_blank"><i class="fa fa-download"></i></asp:HyperLink>

                                                                                        <asp:HiddenField runat="server" ID="hidFile11" Value='<%# Eval("Download") %>' />
                                                                                    </div>

                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>

                                                            </div>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="btn_save_document_full" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                    <asp:HyperLink runat="server" ID="txtfocus_detail_full" />
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>

                                    <div class="panel panel-red">
                                        <div class="panel-heading">
                                            <h3 class="panel-title"><i class="glyphicon glyphicon-road"></i><strong>&nbsp; Driving Status / สถานะการขับขี่ยานพาหนะ</strong></h3>
                                        </div>
                                        <asp:Panel ID="Panel6" runat="server" CssClass="bg-regis">
                                            <div class="panel-body">
                                                <div class="form-horizontal" role="form">

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label ">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label113" runat="server" Text="Driving Car" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label114" runat="server" Text="ขับขี่รถยนต์" /></b></small>
                                                            </h2>
                                                        </label>

                                                        <div class="col-sm-2">
                                                            <asp:DropDownList ID="ddldvcar" runat="server" CssClass="form-control">
                                                                <asp:ListItem Value="0">เลือกสถานะการขับขี่....</asp:ListItem>
                                                                <asp:ListItem Value="1">ขับได้</asp:ListItem>
                                                                <asp:ListItem Value="2">ขับไม่ได้</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>

                                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Lawbel115" runat="server" Text="Licens Driving Car" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Labelw116" runat="server" Text="เลขที่ใบขับขี่" /></b></small>
                                                            </h2>
                                                        </label>

                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txtlicens_car" CssClass="form-control" runat="server" ValidationGroup="SaveAdd"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label ">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label275" runat="server" Text="Driving Car Status" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label276" runat="server" Text="รถยนต์ส่วนตัว" /></b></small>
                                                            </h2>
                                                        </label>

                                                        <div class="col-sm-2">
                                                            <asp:DropDownList ID="ddldvcarstatus" runat="server" CssClass="form-control">
                                                                <asp:ListItem Value="0">เลือกสถานะรถ....</asp:ListItem>
                                                                <asp:ListItem Value="1">มีรถยนต์ส่วนตัว</asp:ListItem>
                                                                <asp:ListItem Value="2">ไม่มีรถยนต์ส่วนตัว</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label231" runat="server" Text="Driving Motocyle" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label232" runat="server" Text="ขับขี่รถจักรยานยนต์" /></b></small>
                                                            </h2>
                                                        </label>

                                                        <div class="col-sm-2">
                                                            <asp:DropDownList ID="ddldvmt" runat="server" CssClass="form-control">
                                                                <asp:ListItem Value="0">เลือกสถานะการขับขี่....</asp:ListItem>
                                                                <asp:ListItem Value="1">ขับได้</asp:ListItem>
                                                                <asp:ListItem Value="2">ขับไม่ได้</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>

                                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label228" runat="server" Text="Licens Driving Motocyle" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label230" runat="server" Text="เลขที่ใบขับขี่" /></b></small>
                                                            </h2>
                                                        </label>

                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txtlicens_moto" CssClass="form-control" runat="server" ValidationGroup="SaveAdd"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label ">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label277" runat="server" Text="Driving Motocyle Status" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label278" runat="server" Text="รถจักรยานยนต์ส่วนตัว" /></b></small>
                                                            </h2>
                                                        </label>

                                                        <div class="col-sm-2">
                                                            <asp:DropDownList ID="ddldvmtstatus" runat="server" CssClass="form-control">
                                                                <asp:ListItem Value="0">เลือกสถานะรถ....</asp:ListItem>
                                                                <asp:ListItem Value="1">มีรถจักรยานยนต์ส่วนตัว</asp:ListItem>
                                                                <asp:ListItem Value="2">ไม่มีรถจักรยานยนต์ส่วนตัว</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label233" runat="server" Text="Driving Forklift" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label234" runat="server" Text="ขับขี่รถโฟล์คลิฟท์" /></b></small>
                                                            </h2>
                                                        </label>

                                                        <div class="col-sm-2">
                                                            <asp:DropDownList ID="ddldvfork" runat="server" CssClass="form-control">
                                                                <asp:ListItem Value="0">เลือกสถานะการขับขี่....</asp:ListItem>
                                                                <asp:ListItem Value="1">ขับได้</asp:ListItem>
                                                                <asp:ListItem Value="2">ขับไม่ได้</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>

                                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label235" runat="server" Text="Licens Driving Forklift" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label236" runat="server" Text="เลขที่ใบขับขี่" /></b></small>
                                                            </h2>
                                                        </label>

                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txtlicens_fork" CssClass="form-control" runat="server" ValidationGroup="SaveAdd"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label237" runat="server" Text="Driving Truck" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label238" runat="server" Text="ขับขี่รถบรรทุก" /></b></small>
                                                            </h2>
                                                        </label>

                                                        <div class="col-sm-2">
                                                            <asp:DropDownList ID="ddldvtruck" runat="server" CssClass="form-control">
                                                                <asp:ListItem Value="0">เลือกสถานะการขับขี่....</asp:ListItem>
                                                                <asp:ListItem Value="1">ขับได้</asp:ListItem>
                                                                <asp:ListItem Value="2">ขับไม่ได้</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>

                                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label239" runat="server" Text="Licens Driving Truck" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label240" runat="server" Text="เลขที่ใบขับขี่" /></b></small>
                                                            </h2>
                                                        </label>

                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txtlicens_truck" CssClass="form-control" runat="server" ValidationGroup="SaveAdd"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>

                                    <div class="panel panel-red">
                                        <div class="panel-heading">
                                            <h3 class="panel-title"><i class="glyphicon glyphicon-phone-alt"></i><strong>&nbsp; Emergency / ข้อมูลติดต่อกรณีฉุกเฉิน</strong></h3>
                                        </div>
                                        <asp:Panel ID="Panel7" runat="server" CssClass="bg-regis">
                                            <div class="panel-body">
                                                <div class="form-horizontal" role="form">

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label135" runat="server" Text="Emergency Contact" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label136" runat="server" Text="กรณีฉุกเฉินติดต่อ" /></b></small>
                                                            </h2>
                                                        </label>
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txtemercon" CssClass="form-control" runat="server" ValidationGroup="SaveAdd"></asp:TextBox>
                                                        </div>

                                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label133" runat="server" Text="Relationship" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label134" runat="server" Text="ความสัมพันธ์" /></b></small>
                                                            </h2>
                                                        </label>

                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txtrelation" CssClass="form-control" runat="server" ValidationGroup="SaveAdd"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label137" runat="server" Text="Telephone" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label138" runat="server" Text="เบอร์โทร" /></b></small>
                                                            </h2>
                                                        </label>
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txttelemer" CssClass="form-control" runat="server" ValidationGroup="SaveAdd"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>

                                    <div class="panel panel-red">
                                        <div class="panel-heading">
                                            <h3 class="panel-title"><i class="glyphicon glyphicon-home"></i><strong>&nbsp; Family / ข้อมูลครอบครัว</strong></h3>
                                        </div>
                                        <asp:Panel ID="Panel8" runat="server" CssClass="bg-regis">
                                            <div class="panel-body">
                                                <div class="form-horizontal" role="form">

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label141" runat="server" Text="Father Name" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label142" runat="server" Text="ชื่อ-สกุล บิดา" /></b></small>
                                                            </h2>
                                                        </label>
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txtfathername" CssClass="form-control" runat="server" ValidationGroup="SaveAdd"></asp:TextBox>
                                                        </div>

                                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label1343" runat="server" Text="Licen no." /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Lab4el144" runat="server" Text="เลขบัตรประชาชน" /></b></small>
                                                            </h2>
                                                        </label>

                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txtlicenfather" CssClass="form-control" runat="server" ValidationGroup="SaveAdd"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label143" runat="server" Text="Telephone" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label144" runat="server" Text="เบอร์ติดต่อ" /></b></small>
                                                            </h2>
                                                        </label>

                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txttelfather" CssClass="form-control" runat="server" ValidationGroup="SaveAdd"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label145" runat="server" Text="Mother Name" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label146" runat="server" Text="ชื่อ-สกุล มารดา" /></b></small>
                                                            </h2>
                                                        </label>
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txtmothername" CssClass="form-control" runat="server" ValidationGroup="SaveAdd"></asp:TextBox>
                                                        </div>

                                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label243" runat="server" Text="Licen no." /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label244" runat="server" Text="เลขบัตรประชาชน" /></b></small>
                                                            </h2>
                                                        </label>

                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txtlicenmother" CssClass="form-control" runat="server" ValidationGroup="SaveAdd"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label147" runat="server" Text="Telephone" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label148" runat="server" Text="เบอร์ติดต่อ" /></b></small>
                                                            </h2>
                                                        </label>

                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txttelmother" CssClass="form-control" runat="server" ValidationGroup="SaveAdd"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label149" runat="server" Text="Wife Name" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label150" runat="server" Text="ชื่อ-สกุล คู่สมรส" /></b></small>
                                                            </h2>
                                                        </label>
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txtwifename" CssClass="form-control" runat="server" ValidationGroup="SaveAdd"></asp:TextBox>
                                                        </div>

                                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label245" runat="server" Text="Licen no." /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label246" runat="server" Text="เลขบัตรประชาชน" /></b></small>
                                                            </h2>
                                                        </label>

                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txtlicenwifename" CssClass="form-control" runat="server" ValidationGroup="SaveAdd"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label151" runat="server" Text="Telephone" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label152" runat="server" Text="เบอร์ติดต่อ" /></b></small>
                                                            </h2>
                                                        </label>

                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txttelwife" CssClass="form-control" runat="server" ValidationGroup="SaveAdd"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <div class="checkbox">
                                                                <asp:CheckBox ID="chkchild" runat="server" Text="เพิ่มข้อมูลบุตร" CssClass="col-sm-4" AutoPostBack="true" OnCheckedChanged="chkSelectedIndexChanged" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <asp:Panel ID="BoxChild" runat="server" Visible="false">
                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label155" runat="server" Text="Child Name" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label156" runat="server" Text="ชื่อ-สกุล บุตร" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtchildname" CssClass="form-control" runat="server" ValidationGroup="SaveAdd"></asp:TextBox>

                                                                <asp:RequiredFieldValidator ID="RequireasddFieldValidator1" ValidationGroup="AddChild" runat="server" Display="None"
                                                                    ControlToValidate="txtchildname" Font-Size="11"
                                                                    ErrorMessage="กรอกชื่อ ...." />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValdsidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequireasddFieldValidator1" Width="160" />
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label157" runat="server" Text="Child Number" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label158" runat="server" Text="เป็นบุตรคนที่" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:DropDownList ID="ddlchildnumber" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0">บุตรคนที่....</asp:ListItem>
                                                                    <asp:ListItem Value="1">1</asp:ListItem>
                                                                    <asp:ListItem Value="2">2</asp:ListItem>
                                                                    <asp:ListItem Value="3">3</asp:ListItem>
                                                                    <asp:ListItem Value="4">4</asp:ListItem>
                                                                    <asp:ListItem Value="5">5</asp:ListItem>
                                                                    <asp:ListItem Value="6">6</asp:ListItem>
                                                                    <asp:ListItem Value="7">7</asp:ListItem>
                                                                    <asp:ListItem Value="8">8</asp:ListItem>
                                                                    <asp:ListItem Value="9">9</asp:ListItem>
                                                                </asp:DropDownList>

                                                                <asp:RequiredFieldValidator ID="Requi3redFi5dews4e4ldValidator1" ValidationGroup="AddChild" runat="server" Display="None"
                                                                    ControlToValidate="ddlchildnumber" Font-Size="11"
                                                                    ErrorMessage="บุตรคนที่ ...."
                                                                    ValidationExpression="บุตรคนที่ ...." InitialValue="0" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCallosutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requi3redFi5dews4e4ldValidator1" Width="160" />
                                                            </div>

                                                            <div class="col-sm-1">
                                                                <asp:LinkButton ID="btnAddChild" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="AddChild" ValidationGroup="AddChild">ADD + &nbsp;</asp:LinkButton>
                                                            </div>
                                                        </div>


                                                        <div class="form-group">
                                                            <div class="col-lg-12">

                                                                <asp:GridView ID="GvChildAdd"
                                                                    runat="server"
                                                                    AutoGenerateColumns="false"
                                                                    CssClass="table table-striped table-bordered table-hover"
                                                                    HeaderStyle-CssClass="table_headCenter"
                                                                    HeaderStyle-Height="40px"
                                                                    ShowFooter="False"
                                                                    ShowHeaderWhenEmpty="True"
                                                                    AllowPaging="True"
                                                                    PageSize="5"
                                                                    BorderStyle="None"
                                                                    CellSpacing="2"
                                                                    OnRowDeleting="gvRowDeleting">

                                                                    <PagerStyle CssClass="PageCustom" />
                                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                                    <EmptyDataTemplate>
                                                                        <div style="text-align: center">Data Cannot Be Found</div>
                                                                    </EmptyDataTemplate>

                                                                    <Columns>

                                                                        <asp:TemplateField HeaderText="#">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <%# (Container.DataItemIndex +1) %>
                                                                                </div>
                                                                            </ItemTemplate>

                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="ชื่อ-นามสกุล">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="Label5w8" runat="server" Text='<%# Eval("Child_name")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="บุตรคนที่">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="lbPag2e1" runat="server" Text='<%# Eval("Child_num")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="จัดการ">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="btnDeleteGV" runat="server" ValidationGroup="fromgroup" CommandName="Delete" Text="Delete" CssClass="btn btn-danger" OnClientClick="return confirm('Do you want delete this item?')" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                    </Columns>

                                                                </asp:GridView>

                                                            </div>
                                                        </div>

                                                    </asp:Panel>

                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>

                                    <div class="panel panel-red">
                                        <div class="panel-heading">
                                            <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; Reference / บุคคลอ้างอิง</strong></h3>
                                        </div>
                                        <asp:Panel ID="Panel9" runat="server" CssClass="bg-regis">
                                            <div class="panel-body">
                                                <div class="form-horizontal" role="form">

                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <div class="checkbox">
                                                                <asp:CheckBox ID="ckreference" runat="server" Text="เพิ่มบุคคลอ้างอิง" CssClass="col-sm-4" AutoPostBack="true" OnCheckedChanged="chkSelectedIndexChanged" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <asp:Panel ID="BoxReference" runat="server" Visible="false">

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label258" runat="server" Text="Name" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label259" runat="server" Text="ชื่อ-นามสกุล" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtfullname_add" CssClass="form-control" runat="server" ValidationGroup="AddReference"></asp:TextBox>

                                                                <asp:RequiredFieldValidator ID="Re1ator1" ValidationGroup="AddReference" runat="server" Display="None"
                                                                    ControlToValidate="txtfullname_add" Font-Size="11"
                                                                    ErrorMessage="ชื่อ-นามสกุล ...." />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="Valr8" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re1ator1" Width="160" />
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label260" runat="server" Text="Positions" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label261" runat="server" Text="ตำแหน่ง" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtposition_add" CssClass="form-control" runat="server" ValidationGroup="AddReference"></asp:TextBox>

                                                                <asp:RequiredFieldValidator ID="Re1or2" ValidationGroup="AddReference" runat="server" Display="None"
                                                                    ControlToValidate="txtposition_add" Font-Size="11"
                                                                    ErrorMessage="ตำแหน่งล่าสุด ...." />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValideatorCalloutExtender9" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re1or2" Width="160" />
                                                            </div>

                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label262" runat="server" Text="Relation" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label263" runat="server" Text="ความสัมพันธ์" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtrelation_add" CssClass="form-control" runat="server" ValidationGroup="AddReference"></asp:TextBox>

                                                                <asp:RequiredFieldValidator ID="Redawtor3" ValidationGroup="AddReference" runat="server" Display="None"
                                                                    ControlToValidate="txtrelation_add" Font-Size="11"
                                                                    ErrorMessage="ความสัมพันธ์ ...." />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValExtender10" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Redawtor3" Width="160" />
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label264" runat="server" Text="Tel" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label266" runat="server" Text="เบอร์โทรศัพท์" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txttel_add" CssClass="form-control" runat="server" ValidationGroup="AddReference"></asp:TextBox>

                                                                <asp:RequiredFieldValidator ID="Requa4" ValidationGroup="AddReference" runat="server" Display="None"
                                                                    ControlToValidate="txttel_add" Font-Size="11"
                                                                    ErrorMessage="เบอร์โทรศัพท์ ...." />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValtExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requa4" Width="160" />
                                                            </div>
                                                            <div class="col-sm-1">
                                                                <asp:LinkButton ID="LinkButton7" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="AddReference" ValidationGroup="AddReference">ADD + &nbsp;</asp:LinkButton>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-lg-12">

                                                                <asp:GridView ID="GvReference"
                                                                    runat="server"
                                                                    AutoGenerateColumns="false"
                                                                    CssClass="table table-striped table-bordered table-hover"
                                                                    HeaderStyle-CssClass="table_headCenter"
                                                                    HeaderStyle-Height="40px"
                                                                    ShowFooter="False"
                                                                    ShowHeaderWhenEmpty="True"
                                                                    AllowPaging="True"
                                                                    PageSize="5"
                                                                    BorderStyle="None"
                                                                    CellSpacing="2"
                                                                    OnRowDeleting="gvRowDeleting">

                                                                    <PagerStyle CssClass="PageCustom" />
                                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                                    <EmptyDataTemplate>
                                                                        <div style="text-align: center">Data Cannot Be Found</div>
                                                                    </EmptyDataTemplate>

                                                                    <Columns>

                                                                        <asp:TemplateField HeaderText="#">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <%# (Container.DataItemIndex +1) %>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="ชื่อ-นามสกุล">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="Labe1" runat="server" Text='<%# Eval("ref_fullname")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="ตำแหน่ง">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="lb3P23ag2e1" runat="server" Text='<%# Eval("ref_position")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="ความสัมพันธ์">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="lb3P2a1g2e1" runat="server" Text='<%# Eval("ref_relation")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="เบอร์โทรศัพท์">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="lb3P2ag2e1" runat="server" Text='<%# Eval("ref_tel")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="จัดการ">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="btnDeleteGV" runat="server" ValidationGroup="fromgroup" CommandName="Delete" Text="Delete" CssClass="btn btn-danger" OnClientClick="return confirm('Do you want delete this item?')" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                    </Columns>

                                                                </asp:GridView>

                                                            </div>
                                                        </div>

                                                    </asp:Panel>

                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>

                                    <div class="panel panel-red">
                                        <div class="panel-heading">
                                            <h3 class="panel-title"><i class="glyphicon glyphicon-briefcase"></i><strong>&nbsp; Prior Experiences / ประวัติการทำงาน</strong></h3>
                                        </div>
                                        <asp:Panel ID="Panel10" runat="server" CssClass="bg-regis">
                                            <div class="panel-body">
                                                <div class="form-horizontal" role="form">

                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <div class="checkbox">
                                                                <asp:CheckBox ID="chkorg_full" runat="server" Text="เพิ่มสถานที่ทำงาน" CssClass="col-sm-4" AutoPostBack="true" OnCheckedChanged="chkSelectedIndexChanged" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <asp:Panel ID="BoxOrgOld_full" runat="server" Visible="false">

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label159" runat="server" Text="Organization Name" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="lblorgold" runat="server" Text="ชื่อบริษัท" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtorgold" CssClass="form-control" runat="server" ValidationGroup="AddPrior"></asp:TextBox>

                                                                <asp:RequiredFieldValidator ID="RequiraedFielasddValidator1" ValidationGroup="AddPrior" runat="server" Display="None"
                                                                    ControlToValidate="txtorgold" Font-Size="11"
                                                                    ErrorMessage="ชื่อบริษัท ...." />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidsaatorCalloutEaxtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiraedFielasddValidator1" Width="160" />
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label161" runat="server" Text="Recent Positions" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label162" runat="server" Text="ตำแหน่งล่าสุด" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtposold" CssClass="form-control" runat="server" ValidationGroup="AddPrior"></asp:TextBox>

                                                                <asp:RequiredFieldValidator ID="RssaequiredFieldValidator1" ValidationGroup="AddPrior" runat="server" Display="None"
                                                                    ControlToValidate="txtposold" Font-Size="11"
                                                                    ErrorMessage="ตำแหน่งล่าสุด ...." />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidafdtorCallsoutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RssaequiredFieldValidator1" Width="160" />
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label253" runat="server" Text="Start Date" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label254" runat="server" Text="ปีที่เริ่มงาน" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <div class='input-group date'>
                                                                    <asp:TextBox ID="txtstartdate_ex" CssClass="form-control from-date-datepicker" MaxLengh="100%" runat="server" ValidationGroup="AddPrior"></asp:TextBox>
                                                                    <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                </div>

                                                                <asp:RequiredFieldValidator ID="Requirer1" ValidationGroup="AddPrior" runat="server" Display="None"
                                                                    ControlToValidate="txtstartdate_ex" Font-Size="11"
                                                                    ErrorMessage="ปีที่เริ่มงาน ...." />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requirer1" Width="160" />
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label255" runat="server" Text="End Date" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label256" runat="server" Text="ปีที่ออกจากงาน" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <div class='input-group date'>
                                                                    <asp:TextBox ID="txtresigndate_ex" CssClass="form-control from-date-datepicker" MaxLengh="100%" runat="server" ValidationGroup="AddPrior"></asp:TextBox>
                                                                    <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                </div>

                                                                <asp:RequiredFieldValidator ID="RdValidator2" ValidationGroup="AddPrior" runat="server" Display="None"
                                                                    ControlToValidate="txtresigndate_ex" Font-Size="11"
                                                                    ErrorMessage="ปีที่ออกจากงาน ...." />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RdValidator2" Width="160" />
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label47" runat="server" Text="salary" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label49" runat="server" Text="เงินเดือนล่าสุด" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtprisalary" CssClass="form-control" runat="server" ValidationGroup="AddPrior"></asp:TextBox>
                                                            </div>
                                                            <asp:RequiredFieldValidator ID="Requirtor1" ValidationGroup="AddPrior" runat="server" Display="None"
                                                                ControlToValidate="txtprisalary" Font-Size="11"
                                                                ErrorMessage="เงินเดือนล่าสุด ...." />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="Vaaer1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requirtor1" Width="160" />

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label42" runat="server" Text="Description" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label43" runat="server" Text="รายละเอียดงาน" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtpridescription" CssClass="form-control" runat="server" TextMode="MultiLine" Rows="3" ValidationGroup="AddPrior"></asp:TextBox>
                                                            </div>
                                                            <asp:RequiredFieldValidator ID="Requiredator1" ValidationGroup="AddPrior" runat="server" Display="None"
                                                                ControlToValidate="txtpridescription" Font-Size="11"
                                                                ErrorMessage="รายละเอียดงาน ...." />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="Valider1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredator1" Width="160" />

                                                            <div class="col-sm-1">
                                                                <asp:LinkButton ID="LinkButton4" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="AddPrior_full" ValidationGroup="AddPrior">ADD + &nbsp;</asp:LinkButton>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-lg-12">

                                                                <asp:GridView ID="GvPri_full"
                                                                    runat="server"
                                                                    AutoGenerateColumns="false"
                                                                    CssClass="table table-striped table-bordered table-hover"
                                                                    HeaderStyle-CssClass="table_headCenter"
                                                                    HeaderStyle-Height="40px"
                                                                    ShowFooter="False"
                                                                    ShowHeaderWhenEmpty="True"
                                                                    AllowPaging="True"
                                                                    PageSize="5"
                                                                    BorderStyle="None"
                                                                    CellSpacing="2"
                                                                    OnRowDeleting="gvRowDeleting">

                                                                    <PagerStyle CssClass="PageCustom" />
                                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                                    <EmptyDataTemplate>
                                                                        <div style="text-align: center">Data Cannot Be Found</div>
                                                                    </EmptyDataTemplate>

                                                                    <Columns>

                                                                        <asp:TemplateField HeaderText="#">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <%# (Container.DataItemIndex +1) %>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="ชื่อบริษัท">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="Label5w8" runat="server" Text='<%# Eval("Pri_Nameorg")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="ตำแหน่งล่าสุด">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="lbP23ag2e1" runat="server" Text='<%# Eval("Pri_pos")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="วันที่เริ่มงาน">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="lbP2a1g2e1" runat="server" Text='<%# Eval("Pri_startdate")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="วันที่ออกจากงาน">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="lbP2ag2e1" runat="server" Text='<%# Eval("Pri_resigndate")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="รายละเอียดงาน">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="lb3P22ag2e1" runat="server" Text='<%# Eval("Pri_description")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="เงินเดือนล่าสุด">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="lbP23ag22e1" runat="server" Text='<%# Eval("Pri_salary")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="จัดการ">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="btnDeleteGV" runat="server" ValidationGroup="fromgroup" CommandName="Delete" Text="Delete" CssClass="btn btn-danger" OnClientClick="return confirm('Do you want delete this item?')" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                    </Columns>

                                                                </asp:GridView>

                                                            </div>
                                                        </div>

                                                    </asp:Panel>

                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>

                                    <div class="panel panel-red">
                                        <div class="panel-heading">
                                            <h3 class="panel-title"><i class="glyphicon glyphicon-education"></i><strong>&nbsp; Education / ประวัติการศึกษา</strong></h3>
                                        </div>
                                        <asp:Panel ID="Panel11" runat="server" CssClass="bg-regis">
                                            <div class="panel-body">
                                                <div class="form-horizontal" role="form">

                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <div class="checkbox">
                                                                <asp:CheckBox ID="ckeducation_full" runat="server" Text="เพิ่มประวัติการศึกษา" CssClass="col-sm-4" AutoPostBack="true" OnCheckedChanged="chkSelectedIndexChanged" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <asp:Panel ID="BoxEducation_full" runat="server" Visible="false">
                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label ">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label176" runat="server" Text="Educational" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label177" runat="server" Text="วุฒิการศึกษา" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:DropDownList ID="ddleducationback" runat="server" CssClass="form-control" ValidationGroup="AddEducation">
                                                                    <asp:ListItem Value="0">เลือกวุฒิการศึกษา....</asp:ListItem>
                                                                    <asp:ListItem Value="7">ไม่มีวุฒิการศึกษา</asp:ListItem>
																	<asp:ListItem Value="1">ประถมศึกษา ป.6</asp:ListItem>
																	<asp:ListItem Value="2">มัธยมศึกษาตอนต้น ม.3</asp:ListItem>
																	<asp:ListItem Value="3">มัธยมศึกษาตอนปลาย ม.6</asp:ListItem>
																	<asp:ListItem Value="8">อาชีวศึกษา ปวช</asp:ListItem>
																	<asp:ListItem Value="9">อาชีวศึกษา ปวส</asp:ListItem>
																	<asp:ListItem Value="4">ปริญญาตรี</asp:ListItem>
																	<asp:ListItem Value="5">ปริญญาโท</asp:ListItem>
																	<asp:ListItem Value="6">ปริญญาเอก</asp:ListItem>
																	<asp:ListItem Value="10">กศน.มัธยม ต้น</asp:ListItem>
																	<asp:ListItem Value="11">กศน.มัธยม ปลาย</asp:ListItem>
                                                                </asp:DropDownList>

                                                                <asp:RequiredFieldValidator ID="ReqsuiredFielsfdValidator1" ValidationGroup="AddEducation" runat="server" Display="None"
                                                                    ControlToValidate="ddleducationback" Font-Size="11"
                                                                    ErrorMessage="เลือกวุฒิการศึกษา ...."
                                                                    ValidationExpression="เลือกวุฒิการศึกษา ...." InitialValue="0" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValiccxdatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqsuiredFielsfdValidator1" Width="160" />
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label168" runat="server" Text="School Name" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label169" runat="server" Text="ชื่อสถานศึกษา" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtschoolname" CssClass="form-control" runat="server" ValidationGroup="AddEducation"></asp:TextBox>

                                                                <asp:RequiredFieldValidator ID="RequiredsdfFieldValidatoar1" ValidationGroup="AddEducation" runat="server" Display="None"
                                                                    ControlToValidate="txtschoolname" Font-Size="11"
                                                                    ErrorMessage="เลือกวุฒิการศึกษา ...." />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidafsawtorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredsdfFieldValidatoar1" Width="160" />
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label172" runat="server" Text="Start Date" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label173" runat="server" Text="ปีที่เริ่มศึกษา" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <div class='input-group date'>
                                                                    <asp:TextBox ID="txtstarteducation" CssClass="form-control from-date-datepicker" MaxLengh="100%" runat="server" ValidationGroup="AddEducation"></asp:TextBox>
                                                                    <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                </div>

                                                                <asp:RequiredFieldValidator ID="ReadquiredFieldValidator1" ValidationGroup="AddEducation" runat="server" Display="None"
                                                                    ControlToValidate="txtstarteducation" Font-Size="11"
                                                                    ErrorMessage="ปีที่เริ่มศึกษา ...." />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCallouootExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReadquiredFieldValidator1" Width="160" />
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label174" runat="server" Text="End Date" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label175" runat="server" Text="ปีที่จบการศึกษา" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <div class='input-group date'>
                                                                    <asp:TextBox ID="txtendeducation" CssClass="form-control from-date-datepicker" MaxLengh="100%" runat="server" ValidationGroup="AddEducation"></asp:TextBox>
                                                                    <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                </div>

                                                                <asp:RequiredFieldValidator ID="ReqpouiredFieldValidator1" ValidationGroup="AddEducation" runat="server" Display="None"
                                                                    ControlToValidate="txtendeducation" Font-Size="11"
                                                                    ErrorMessage="ปีที่จบการศึกษา ...." />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidopatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqpouiredFieldValidator1" Width="160" />
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label170" runat="server" Text="Branch" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label171" runat="server" Text="สาขาวิชาที่ศึกษา" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtstudy" CssClass="form-control" runat="server" ValidationGroup="AddEducation"></asp:TextBox>

                                                                <asp:RequiredFieldValidator ID="RepequiredFieldValidator1" ValidationGroup="AddEducation" runat="server" Display="None"
                                                                    ControlToValidate="txtstudy" Font-Size="11"
                                                                    ErrorMessage="สาขาวิชาที่ศึกษา ...." />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RepequiredFieldValidator1" Width="160" />
                                                            </div>

                                                            <div class="col-sm-1 col-sm-offset-2">
                                                                <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="AddEducation_full" ValidationGroup="AddEducation">ADD + &nbsp;</asp:LinkButton>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-lg-12">

                                                                <asp:GridView ID="GvEducation_full"
                                                                    runat="server"
                                                                    AutoGenerateColumns="false"
                                                                    CssClass="table table-striped table-bordered table-hover"
                                                                    HeaderStyle-CssClass="table_headCenter"
                                                                    HeaderStyle-Height="40px"
                                                                    ShowFooter="False"
                                                                    ShowHeaderWhenEmpty="True"
                                                                    AllowPaging="True"
                                                                    PageSize="5"
                                                                    BorderStyle="None"
                                                                    CellSpacing="2"
                                                                    OnRowDeleting="gvRowDeleting">

                                                                    <PagerStyle CssClass="PageCustom" />
                                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                                    <EmptyDataTemplate>
                                                                        <div style="text-align: center">Data Cannot Be Found</div>
                                                                    </EmptyDataTemplate>

                                                                    <Columns>

                                                                        <asp:TemplateField HeaderText="#">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <%# (Container.DataItemIndex +1) %>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="วุฒิการศึกษา">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="Label5w8" runat="server" Text='<%# Eval("Edu_qualification")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="ชื่อสถานศึกษา">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="lbP24ag2e1" runat="server" Text='<%# Eval("Edu_name")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="สาขาวิชาที่ศึกษา">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="lb4Pag2e1" runat="server" Text='<%# Eval("Edu_branch")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="ปีที่เริ่มศึกษา">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="lbP2ag2e1" runat="server" Text='<%# Eval("Edu_start")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="ปีที่จบศึกษา">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="lbP3ag2e1" runat="server" Text='<%# Eval("Edu_end")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="จัดการ">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="btnDeleteGV" runat="server" ValidationGroup="fromgroup" CommandName="Delete" Text="Delete" CssClass="btn btn-danger" OnClientClick="return confirm('Do you want delete this item?')" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                    </Columns>

                                                                </asp:GridView>

                                                            </div>
                                                        </div>

                                                    </asp:Panel>

                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>


                                    <%-----------------------------------------------------------------------------------------------%>
                                    <%--Training History--%>
                                    <%-----------------------------------------------------------------------------------------------%>

                                    <div class="panel panel-red">
                                        <div class="panel-heading">
                                            <h3 class="panel-title"><i class="glyphicon glyphicon-retweet"></i><strong>&nbsp; Training History / ประวัติการฝึกอบรม</strong></h3>
                                        </div>
                                        <asp:Panel ID="Panel12" runat="server" CssClass="bg-regis">
                                            <div class="panel-body">
                                                <div class="form-horizontal" role="form">

                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <div class="checkbox">
                                                                <asp:CheckBox ID="cktraining" runat="server" Text="เพิ่มประวัติฝึกอบรม" CssClass="col-sm-4" AutoPostBack="true" OnCheckedChanged="chkSelectedIndexChanged" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <asp:Panel ID="BoxTrain" runat="server" Visible="false">
                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label178" runat="server" Text="Training Courses" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label179" runat="server" Text="หลักสูตรฝึกอบรม" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txttraincourses" CssClass="form-control" runat="server" ValidationGroup="Addtraining"></asp:TextBox>

                                                                <asp:RequiredFieldValidator ID="RequpoosiredFieldValidator1" ValidationGroup="Addtraining" runat="server" Display="None"
                                                                    ControlToValidate="txttraincourses" Font-Size="11"
                                                                    ErrorMessage="หลักสูตรฝึกอบรม ...." />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="VadlidatorCalloutExtsender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequpoosiredFieldValidator1" Width="160" />
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label180" runat="server" Text="Training Date" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label181" runat="server" Text="วันที่ฝึกอบรม" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <div class='input-group date'>
                                                                    <asp:TextBox ID="txttraindate" CssClass="form-control from-date-datepicker" MaxLengh="100%" runat="server" ValidationGroup="Addtraining"></asp:TextBox>
                                                                    <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                </div>

                                                                <asp:RequiredFieldValidator ID="ReplkquiredFieldValidator1" ValidationGroup="Addtraining" runat="server" Display="None"
                                                                    ControlToValidate="txttraindate" Font-Size="11"
                                                                    ErrorMessage="หลักสูตรฝึกอบรม ...." />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValivdatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReplkquiredFieldValidator1" Width="160" />
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label182" runat="server" Text="Assessment" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label183" runat="server" Text="ผลการประเมิน" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txttrainassessment" CssClass="form-control" runat="server" ValidationGroup="Addtraining"></asp:TextBox>

                                                                <asp:RequiredFieldValidator ID="RequiredsdfFieldValidastor1" ValidationGroup="Addtraining" runat="server" Display="None"
                                                                    ControlToValidate="txttrainassessment" Font-Size="11"
                                                                    ErrorMessage="ผลการประเมิน ...." />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredsdfFieldValidastor1" Width="160" />
                                                            </div>

                                                            <div class="col-sm-1 col-sm-offset-2">
                                                                <asp:LinkButton ID="LinkButton3" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="Addtraining" ValidationGroup="Addtraining">ADD + &nbsp;</asp:LinkButton>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-lg-12">

                                                                <asp:GridView ID="GvTrain"
                                                                    runat="server"
                                                                    AutoGenerateColumns="false"
                                                                    CssClass="table table-striped table-bordered table-hover"
                                                                    HeaderStyle-CssClass="table_headCenter"
                                                                    HeaderStyle-Height="40px"
                                                                    ShowFooter="False"
                                                                    ShowHeaderWhenEmpty="True"
                                                                    AllowPaging="True"
                                                                    PageSize="5"
                                                                    BorderStyle="None"
                                                                    CellSpacing="2"
                                                                    OnRowDeleting="gvRowDeleting">

                                                                    <PagerStyle CssClass="PageCustom" />
                                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                                    <EmptyDataTemplate>
                                                                        <div style="text-align: center">Data Cannot Be Found</div>
                                                                    </EmptyDataTemplate>

                                                                    <Columns>

                                                                        <asp:TemplateField HeaderText="#">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <%# (Container.DataItemIndex +1) %>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="หลักสูตรอาบรม">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="Label5w8" runat="server" Text='<%# Eval("Train_courses")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="วันที่อบรม">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="lbP2a2g2e1" runat="server" Text='<%# Eval("Train_date")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="ผลการประเมิน">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="lbPag2e1" runat="server" Text='<%# Eval("Train_assessment")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="จัดการ">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="btnDeleteGV" runat="server" ValidationGroup="fromgroup" CommandName="Delete" Text="Delete" CssClass="btn btn-danger" OnClientClick="return confirm('Do you want delete this item?')" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                    </Columns>

                                                                </asp:GridView>

                                                            </div>
                                                        </div>
                                                    </asp:Panel>

                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>

                                    <div class="panel panel-red">
                                        <div class="panel-heading">
                                            <h3 class="panel-title"><i class="glyphicon glyphicon-home"></i><strong>&nbsp; Address & Contact (Present) / ที่อยู่และการติดต่อ(ปัจจุบัน)</strong></h3>
                                        </div>
                                        <asp:Panel ID="Panel13" runat="server" CssClass="bg-regis">
                                            <div class="panel-body">
                                                <div class="form-horizontal" role="form">

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label63" runat="server" Text="PresentAddress" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label66" runat="server" Text="ที่อยู่ปัจจุบัน" /></b></small>
                                                            </h2>
                                                        </label>
                                                        <div class="col-sm-4">
                                                            <asp:TextBox ID="txtaddress_present" autocomplete="off" MaxLength="250" TextMode="MultiLine" Rows="3" CssClass="form-control" runat="server" ValidationGroup="SaveAdd"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label68" runat="server" Text="Country" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label69" runat="server" Text="ประเทศ" /></b></small>
                                                            </h2>
                                                        </label>

                                                        <div class="col-sm-2">
                                                            <asp:DropDownList ID="ddlcountry_present" runat="server" CssClass="form-control">
                                                                <asp:ListItem Value="0">เลือกประเทศ....</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>

                                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Lab3el15" runat="server" Text="Province" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label416" runat="server" Text="จังหวัด" /></b></small>
                                                            </h2>
                                                        </label>

                                                        <div class="col-sm-2">
                                                            <asp:DropDownList ID="ddlprovince_present" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                <asp:ListItem Value="0">เลือกจังหวัด....</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label71" runat="server" Text="Amphoe" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label72" runat="server" Text="อำเภอ" /></b></small>
                                                            </h2>
                                                        </label>

                                                        <div class="col-sm-2">
                                                            <asp:DropDownList ID="ddlamphoe_present" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                <asp:ListItem Value="0">เลือกอำเภอ....</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>

                                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label73" runat="server" Text="District" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label75" runat="server" Text="ตำบล" /></b></small>
                                                            </h2>
                                                        </label>

                                                        <div class="col-sm-2">
                                                            <asp:DropDownList ID="ddldistrict_present" runat="server" CssClass="form-control">
                                                                <asp:ListItem Value="0">เลือกตำบล....</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label79" runat="server" Text="ZipCode" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label80" runat="server" Text="รหัสไปรษณีย์" /></b></small>
                                                            </h2>
                                                        </label>
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txtzipcode_present" CssClass="form-control" runat="server"></asp:TextBox>
                                                        </div>

                                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label83" runat="server" Text="Email" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label84" runat="server" Text="อีเมล" /></b></small>
                                                            </h2>
                                                        </label>
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txtemail_present" CssClass="form-control" runat="server" ValidationGroup="InsertData"></asp:TextBox>

                                                            <asp:RegularExpressionValidator ID="R_Email_add" runat="server" SetFocusOnError="true" Display="None" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtemail_present" ErrorMessage="*กรอกในรูปแบบEmail" ValidationGroup="InsertData"></asp:RegularExpressionValidator>
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_Email2_add" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_Email_add" Width="160" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label76" runat="server" Text="Mobile Phone" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label78" runat="server" Text="โทรศัพท์มือถือ" /></b></small>
                                                            </h2>
                                                        </label>
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txttelmobile_present" CssClass="form-control" MaxLength="15" runat="server" ValidationGroup="InsertData"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="Reqor21" ValidationGroup="InsertData_Mini" runat="server" Display="None"
                                                                ControlToValidate="txttelmobile_present" Font-Size="11"
                                                                ErrorMessage="เบอร์โทรศัพท์" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="VsExtewnder1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Reqor21" Width="160" />
                                                            <asp:RegularExpressionValidator ID="RegularEsator1" runat="server" ValidationGroup="InsertData" Display="None"
                                                                ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                                ControlToValidate="txttelmobile_present"
                                                                ValidationExpression="^[0-9]{1,15}$" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="Vder4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularEsator1" Width="160" />
                                                        </div>

                                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label81" runat="server" Text="Home Phone" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label82" runat="server" Text="โทรศัพท์บ้าน" /></b></small>
                                                            </h2>
                                                        </label>
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txttelhome_present" CssClass="form-control" runat="server" ValidationGroup="SaveAdd"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <hr />

                                                    <div class="form-group">
                                                        <label class="col-sm-12">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label85" runat="server" CssClass="h01" Text="Address & Contact (Permanent)" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label86" runat="server" Text="ที่อยู่และการติดต่อ(ทะเบียนบ้าน)" /></b></small>
                                                            </h2>
                                                        </label>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-labelnotop">
                                                            <asp:CheckBox ID="chkAddress" CssClass="text-primary" Text="เหมือนกับที่อยู่ปัจจุบัน" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelectedIndexChanged" />
                                                        </label>
                                                    </div>

                                                    <asp:Panel ID="BoxAddress_parent" runat="server">

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label87" runat="server" Text="Permanentaddress" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label88" runat="server" Text="ที่อยู่ตามทะเบียนบ้าน" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-4">
                                                                <asp:TextBox ID="txtaddress_permanentaddress" autocomplete="off" MaxLength="250" TextMode="MultiLine" Rows="3" CssClass="form-control" runat="server" ValidationGroup="SaveAdd"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label89" runat="server" Text="Country" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label90" runat="server" Text="ประเทศ" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:DropDownList ID="ddlcountry_permanentaddress" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0">เลือกประเทศ....</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label91" runat="server" Text="Province" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label92" runat="server" Text="จังหวัด" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:DropDownList ID="ddlprovince_permanentaddress" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                    <asp:ListItem Value="0">เลือกจังหวัด....</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label93" runat="server" Text="Amphoe" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label94" runat="server" Text="อำเภอ" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:DropDownList ID="ddlamphoe_permanentaddress" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                    <asp:ListItem Value="0">เลือกอำเภอ....</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label95" runat="server" Text="District" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label96" runat="server" Text="ตำบล" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:DropDownList ID="ddldistrict_permanentaddress" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0">เลือกตำบล....</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label97" runat="server" Text="Home Phone" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label98" runat="server" Text="โทรศัพท์บ้าน" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txttelmobile_permanentaddress" CssClass="form-control" runat="server" ValidationGroup="SaveAdd"></asp:TextBox>
                                                            </div>

                                                            <%--<label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label99" runat="server" Text="Home Phone" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label100" runat="server" Text="โทรศัพท์บ้าน" /></b></small>
                                            </h2>
                                        </label>
                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txttelhome_permanentaddress" CssClass="form-control" runat="server" ValidationGroup="SaveAdd"></asp:TextBox>
                                        </div>--%>
                                                        </div>

                                                    </asp:Panel>

                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>

                                    <div class="panel panel-red">
                                        <div class="panel-heading">
                                            <h3 class="panel-title"><i class="glyphicon glyphicon-heart"></i><strong>&nbsp; Health / ร่างกาย & สุขภาพ</strong></h3>
                                        </div>
                                        <asp:Panel ID="Panel14" runat="server" CssClass="bg-regis">
                                            <div class="panel-body">
                                                <div class="form-horizontal" role="form">

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label38" runat="server" Text="Currently used hospital" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label39" runat="server" Text="ปัจจุบันใช้สถานพยาบาล" /></b></small>
                                                            </h2>
                                                        </label>

                                                        <div class="col-sm-2">
                                                            <asp:DropDownList ID="ddlhospital" runat="server" CssClass="form-control">
                                                                <asp:ListItem Value="0">เลือกโรงพยาบาล</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>

                                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label121" runat="server" Text="Date of examination" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label122" runat="server" Text="วันที่ตรวจร่างกาย" /></b></small>
                                                            </h2>
                                                        </label>
                                                        <div class="col-sm-2">
                                                            <div class='input-group date'>
                                                                <asp:TextBox ID="txtexaminationdate" CssClass="form-control from-date-datepicker" MaxLengh="100%" runat="server" ValidationGroup="SaveAdd"></asp:TextBox>
                                                                <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label40" runat="server" Text="Social Security No" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label41" runat="server" Text="เลขที่ประกันสังคม" /></b></small>
                                                            </h2>
                                                        </label>
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txtsecurityid" CssClass="form-control" runat="server"></asp:TextBox>
                                                        </div>

                                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label45" runat="server" Text="Expiration Date" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label46" runat="server" Text="วันหมดอายุ" /></b></small>
                                                            </h2>
                                                        </label>
                                                        <div class="col-sm-2">
                                                            <div class='input-group date'>
                                                                <asp:TextBox ID="txtexpsecurity" CssClass="form-control from-date-datepicker" MaxLengh="100%" runat="server" ValidationGroup="SaveAdd"></asp:TextBox>
                                                                <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label50" runat="server" Text="Height" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label51" runat="server" Text="ส่วนสูง" /></b></small>
                                                            </h2>
                                                        </label>
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txtheight" CssClass="form-control" runat="server" ValidationGroup="InsertData"></asp:TextBox>

                                                            <asp:RegularExpressionValidator ID="R_Height_add" runat="server" ErrorMessage="*เฉพาะตัวเลขเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txtheight" ValidationExpression="^[0-9'.\s]{1,3}$" ValidationGroup="InsertData" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="VS_Height2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_Height_add" Width="160" />
                                                        </div>
                                                        <label class="col-sm-1 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label54" runat="server" Text="Cm" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label55" runat="server" Text="ซม." /></b></small>
                                                            </h2>
                                                        </label>

                                                        <label class="col-sm-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label52" runat="server" Text="Weight" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label53" runat="server" Text="น้ำหนัก" /></b></small>
                                                            </h2>
                                                        </label>
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txtweight" CssClass="form-control" runat="server" ValidationGroup="InsertData"></asp:TextBox>

                                                            <asp:RegularExpressionValidator ID="R_Weigth_add" runat="server" ErrorMessage="*เฉพาะตัวเลขเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txtweight" ValidationExpression="^[0-9'.\s]{1,3}$" ValidationGroup="InsertData" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_Weigth2_add" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_Weigth_add" Width="160" />
                                                        </div>
                                                        <label class="col-sm-1 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label56" runat="server" Text="Kg" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label58" runat="server" Text="กก." /></b></small>
                                                            </h2>
                                                        </label>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label59" runat="server" Text="BloodGroup" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label60" runat="server" Text="กรุ๊ปเลือด" /></b></small>
                                                            </h2>
                                                        </label>

                                                        <div class="col-sm-2">
                                                            <asp:DropDownList ID="ddlblood" runat="server" CssClass="form-control">
                                                                <asp:ListItem Value="0">เลือกกรุ๊ปเลือด....</asp:ListItem>
                                                                <asp:ListItem Value="1">เอ (A)</asp:ListItem>
                                                                <asp:ListItem Value="2">บี (B)</asp:ListItem>
                                                                <asp:ListItem Value="3">โอ (O)</asp:ListItem>
                                                                <asp:ListItem Value="4">เอบี (AB)</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>

                                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label61" runat="server" Text="Scar" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label62" runat="server" Text="ตำหนิ" /></b></small>
                                                            </h2>
                                                        </label>
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txtscar" CssClass="form-control" runat="server" ValidationGroup="SaveAdd"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label117" runat="server" Text="Medical certificate" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label118" runat="server" Text="ใบรับรองแพทย์" /></b></small>
                                                            </h2>
                                                        </label>

                                                        <div class="col-sm-2">
                                                            <asp:DropDownList ID="ddlmedicalcertificate" runat="server" CssClass="form-control">
                                                                <asp:ListItem Value="0">เลือกสถานะใบรับรอง....</asp:ListItem>
                                                                <asp:ListItem Value="1">มี</asp:ListItem>
                                                                <asp:ListItem Value="2">ไม่มี</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>

                                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label119" runat="server" Text="Result Lab" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label120" runat="server" Text="ผลตรวจ LAB" /></b></small>
                                                            </h2>
                                                        </label>

                                                        <div class="col-sm-2">
                                                            <asp:DropDownList ID="ddlresultlab" runat="server" CssClass="form-control">
                                                                <asp:ListItem Value="0">เลือกสถานะผลตรวจ....</asp:ListItem>
                                                                <asp:ListItem Value="1">มี</asp:ListItem>
                                                                <asp:ListItem Value="2">ไม่มี</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>

                                </asp:Panel>

                            </InsertItemTemplate>
                        </asp:FormView>

                        <div class="col-sm-1 col-sm-offset-1">
                            <asp:LinkButton ID="btnSave_full" runat="server" CssClass="btn btn-default" OnCommand="btnCommand" CommandName="btnSave_full" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" ValidationGroup="InsertData">Save &nbsp;</asp:LinkButton>
                        </div>
                    </div>
                </div>
            </asp:Panel>
      

            <div id="ordine_test_full" class="modal open" role="dialog">
                <div class="modal-dialog" style="width: 60%;">
                    <!-- Modal content-->
                    <div class="modal-content">

                        <div class="modal-body bg-template-center_tranfer">
                            <div class="panel-body">
                                <div class="col-sm-12">
                                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                        <ContentTemplate>
                                            <div class="col-sm-3 col-sm-offset-1 button_tranfer">
                                                <asp:ImageButton ID="lbttranfer_test_full" CssClass="img-responsive img-fluid background-size: 100% 100%" Width="100%" ImageUrl="~/masterpage/images/hr/pic_tranfer_button.png" OnCommand="btnCommand" CommandName="btntranfer_test_full" runat="server" />
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="lbttranfer_test_full" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                            </div>

                        </div>
                        <%--<asp:UpdatePanel ID="UpdatePanel6" runat="server">
                            <ContentTemplate>
                                <asp:LinkButton ID="lbttranfer_test_full" runat="server" CssClass="btn btn-default bg-template-buttom_tranfer" OnCommand="btnCommand" CommandName="btntranfer_test_full"></asp:LinkButton>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="lbttranfer_test_full" />
                            </Triggers>
                        </asp:UpdatePanel>--%>
                    </div>
                </div>
            </div>

        </asp:View>

        <asp:View ID="ViewEdit" runat="server">
            <asp:Panel ID="Panel15" runat="server">
                <div class="panel-body">
                    <div class="form-horizontal" role="form">
                        <asp:Label ID="Label3" runat="server"></asp:Label>
                        <asp:HyperLink runat="server" ID="txtfocus_edit" />
                        <div class="form-group">
                            <label class="col-sm-1"></label>
                            <label class="col-sm-10">
                                <asp:Image ID="Image3" CssClass="img-responsive" Width="100%" Height="400px" ImageUrl="~/masterpage/images/hr/banner_register_2.png" runat="server" />
                            </label>
                            <label class="col-sm-1"></label>
                        </div>
                        <%-------------- BoxSearch Start--------------%>

                        <asp:FormView ID="Fv_Search_Emp_Index" runat="server" Width="80%" HorizontalAlign="Center">
                            <InsertItemTemplate>

                                <asp:UpdatePanel ID="Up2dateP2s22anel4" runat="server">
                                    <ContentTemplate>
                                        <div class="panel panel-red">
                                            <div class="panel-heading">
                                                <h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i><strong>&nbsp; Search / ค้นหาข้อมูล</strong></h3>
                                            </div>
                                            <asp:Panel ID="pn_bg_genarol" runat="server" CssClass="bg-regis">
                                                <div class="panel-body">
                                                    <div class="form-horizontal" role="form">

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label38" runat="server" Text="ID Card" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label39" runat="server" Text="รหัสบัตรประชาชน" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtidcard_s" CssClass="form-control" runat="server" MaxLength="13" ValidationGroup="SaveAdd"></asp:TextBox>
                                                            </div>

                                                            <div class="col-sm-2">
                                                                <asp:LinkButton ID="btnsearch_index" class="btn btn-default" ValidationGroup="Search" runat="server" Text="ค้นหา" data-original-title="search" data-toggle="tooltip" CommandName="btn_search_index" OnCommand="btnCommand" />
                                                                <asp:LinkButton ID="btnclearsearch_index" class="btn btn-default" runat="server" Text="Clear" data-original-title="search" data-toggle="tooltip" CommandName="Cmdsearch_clear" OnCommand="btnCommand" />
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="btnsearch_index" />
                                    </Triggers>
                                </asp:UpdatePanel>

                            </InsertItemTemplate>
                        </asp:FormView>

                        <%-------------- BoxSearch End--------------%>

                        <asp:Panel ID="BoxEditEmployee" runat="server" Visible="false">
                            <asp:Label ID="gsd" runat="server"></asp:Label>
                            <asp:Label ID="ffs" runat="server"></asp:Label>
                            <asp:FormView ID="FvEdit_Detail" runat="server" Width="80%" HorizontalAlign="Center" OnDataBound="FvDetail_DataBound">
                                <EditItemTemplate>

                                    <%-------------------------------- ข้อมูลทั่วไป --------------------------------%>
                                    <asp:Panel ID="BoxGen_Edit" runat="server">

                                        <div class="panel panel-red">
                                            <div class="panel-heading">
                                                <h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i><strong>&nbsp; Position Interested / ประเภทงานที่สนใจ</strong></h3>
                                            </div>
                                            <asp:Panel ID="Panel17" runat="server" CssClass="bg-regis">
                                                <div class="panel-body">
                                                    <div class="form-horizontal" role="form">

                                                        <div class="col-sm-7">
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label ">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label270" runat="server" Text="Employee In" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label271" runat="server" Text="วันที่สามารถเริ่มงานได้" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-5">
                                                                    <asp:Label ID="lblemp_in_idx" runat="server" Visible="false" Text='<%# Eval("EmpINID") %>'></asp:Label>
                                                                    <asp:DropDownList ID="ddlstartdate_in" runat="server" CssClass="form-control" ValidationGroup="FormEdit">
                                                                        <asp:ListItem Value="0">เลือกวันที่พร้อมเริ่มงาน.....</asp:ListItem>
                                                                        <asp:ListItem Value="1">ไม่แน่ใจ</asp:ListItem>
                                                                        <asp:ListItem Value="2">ทันที</asp:ListItem>
                                                                        <asp:ListItem Value="3">7 วัน</asp:ListItem>
                                                                        <asp:ListItem Value="4">15 วัน</asp:ListItem>
                                                                        <asp:ListItem Value="5">1 เดือน</asp:ListItem>
                                                                        <asp:ListItem Value="6">2 เดือน</asp:ListItem>
                                                                        <asp:ListItem Value="7">3 เดือน</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                    <asp:RequiredFieldValidator ID="Req1" ValidationGroup="FormEdit" runat="server" Display="None"
                                                                        ControlToValidate="ddlstartdate_in" Font-Size="11"
                                                                        ErrorMessage="วันที่สามารถเริ่มงานได้ ...."
                                                                        ValidationExpression="วันที่สามารถเริ่มงานได้ ...." InitialValue="0" />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="Valider4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req1" Width="160" />
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <asp:Label ID="Label272" runat="server" ForeColor="Red" Text="***" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label204" runat="server" Text="Salary" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label205" runat="server" Text="เงินเดือนที่ต้องการ" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-5">
                                                                    <asp:TextBox ID="txtsalary" CssClass="form-control" runat="server" Text='<%# Eval("salary") %>' ValidationGroup="FormEdit"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="RequirValidator1" ValidationGroup="FormEdit" runat="server" Display="None"
                                                                        ControlToValidate="txtsalary" Font-Size="11"
                                                                        ErrorMessage="เงินเดือนที่ต้องการ" />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="Validtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequirValidator1" Width="160" />
                                                                    <asp:RegularExpressionValidator ID="RetxtUnit" runat="server" ValidationGroup="FormEdit" Display="None"
                                                                        ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                                        ControlToValidate="txtsalary"
                                                                        ValidationExpression="^[0-9]{1,10}$" />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="Valitender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RetxtUnit" Width="160" />
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <asp:Label ID="Label202" runat="server" ForeColor="Red" Text="***" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label ">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label198" runat="server" Text="Job Category" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label201" runat="server" Text="ประเภทงานที่ต้องการ" /></b></small>
                                                                    </h2>
                                                                </label>

                                                                <div class="col-sm-5">
                                                                    <asp:Label ID="lbljob_type_idx" runat="server" Visible="false" Text='<%# Eval("JobTypeIDX") %>'></asp:Label>
                                                                    <asp:DropDownList ID="ddltypecategory" runat="server" CssClass="form-control" ValidationGroup="FormEdit">
                                                                        <asp:ListItem Value="0">เลือกประเภทงานที่ต้องการ...</asp:ListItem>
                                                                        <asp:ListItem Value="1">ประจำ</asp:ListItem>
                                                                        <asp:ListItem Value="2">Part-time</asp:ListItem>
                                                                        <asp:ListItem Value="3">Freelance</asp:ListItem>
                                                                        <asp:ListItem Value="4">ฝึกงาน</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                    <asp:RequiredFieldValidator ID="RequiredFr1" ValidationGroup="FormEdit" runat="server" Display="None"
                                                                        ControlToValidate="ddltypecategory" Font-Size="11"
                                                                        ErrorMessage="ประเภทงานที่ต้องการ ...."
                                                                        ValidationExpression="ประเภทงานที่ต้องการ ...." InitialValue="0" />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="Validaer1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFr1" Width="160" />
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <asp:Label ID="Label203" runat="server" ForeColor="Red" Text="***" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label ">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label74" runat="server" Text="Position" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label77" runat="server" Text="ตำแหน่งงานที่เปิดรับ" /></b></small>
                                                                    </h2>
                                                                </label>

                                                                <div class="col-sm-5">
                                                                    <asp:Label ID="lblPIDX_edit" runat="server" Visible="false" Text='<%# Eval("PIDX") %>'></asp:Label>
                                                                    <asp:DropDownList ID="ddlpositionfocus" runat="server" CssClass="form-control">
                                                                        <asp:ListItem Value="0">เลือกตำแหน่งงาน....</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label ">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label99" runat="server" Text="Position Group" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label100" runat="server" Text="กลุ่มประเภทงานที่ต้องการ" /></b></small>
                                                                    </h2>
                                                                </label>

                                                                <div class="col-sm-5">
                                                                    <asp:Label ID="lblPosGroupIDX_1" runat="server" Visible="false" Text='<%# Eval("PosGrop1") %>'></asp:Label>
                                                                    <asp:DropDownList ID="ddlposition1" runat="server" CssClass="form-control" ValidationGroup="FormEdit">
                                                                        <asp:ListItem Value="0">เลือกประเภทกลุ่มงาน 1....</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldVal1" ValidationGroup="FormEdit" runat="server" Display="None"
                                                                        ControlToValidate="ddlposition1" Font-Size="11"
                                                                        ErrorMessage="กลุ่มประเภทงานที่ต้องการ ...."
                                                                        ValidationExpression="กลุ่มประเภทงานที่ต้องการ ...." InitialValue="0" />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCala" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldVal1" Width="160" />
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <asp:Label ID="Label206" runat="server" ForeColor="Red" Text="***" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label ">
                                                                </label>

                                                                <div class="col-sm-5">
                                                                    <asp:Label ID="lblPosGroupIDX_2" runat="server" Visible="false" Text='<%# Eval("PosGrop2") %>'></asp:Label>
                                                                    <asp:DropDownList ID="ddlposition2" runat="server" CssClass="form-control">
                                                                        <asp:ListItem Value="0">เลือกประเภทกลุ่มงาน 2....</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label ">
                                                                </label>

                                                                <div class="col-sm-5">
                                                                    <asp:Label ID="lblPosGroupIDX_3" runat="server" Visible="false" Text='<%# Eval("PosGrop3") %>'></asp:Label>
                                                                    <asp:DropDownList ID="ddlposition3" runat="server" CssClass="form-control">
                                                                        <asp:ListItem Value="0">เลือกประเภทกลุ่มงาน 3....</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label207" runat="server" Text="Description" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label208" runat="server" Text="รายละเอียดเพิ่มเติม/ความสามารถพิเศษ" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-5">
                                                                    <asp:TextBox ID="txtdescription" CssClass="form-control" runat="server" Text='<%# Eval("DetailProfile") %>' autocomplete="off" MaxLength="250" TextMode="MultiLine" Rows="3" ValidationGroup="AddReference1"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-5">
                                                            <asp:UpdatePanel ID="Up2dateP2anel4" runat="server">
                                                                <ContentTemplate>
                                                                    <div class="form-group">
                                                                        <div class="col-sm-8">
                                                                            <div class="panel panel-red">
                                                                                <div class="form-group">
                                                                                    <div class="panel-body">
                                                                                        <center><asp:Image ID="img_profile_edit" runat="server" style="width: 100px; height: 100px" class="img-thumbnail"></asp:Image></center>
                                                                                        <center><asp:Panel id="ImagProfile_default_edit" runat="server">
                                                                            <img class="media-object img-thumbnail" src='<%=ResolveUrl("~/uploadfiles/profile_picture/default/user-default.png")%>' width="100px" height="100px" style="margin-right: 8px; margin-top: -5px;">
                                                                        </asp:Panel></center>

                                                                                        &nbsp;&nbsp;<asp:FileUpload ID="upload_profile_pi_edit" AutoPostBack="true" Font-Size="small" ClientIDMode="Static" runat="server" accept="jpg" CssClass="btn btn-sm multi max-1" />
                                                                                        <center><font color="red"><small class="text_right"><p>**เฉพาะนามสกุล jpg</small></font></center>
                                                                                        <hr />
                                                                                        <center><asp:LinkButton ID="btn_save_file1_edit" runat="server" CssClass="btn btn-warning btn-sm" data-toggle="tooltip" title="อัพโหลดภาพ" CommandName="savefile_picture_edit" OnCommand="btnCommand"><i class="glyphicon glyphicon-picture"></i> อัพโหลดภาพ</asp:LinkButton></center>
                                                                                        <center><font color="red"><small class="text_right"><p>กรุณากรอกข้อมูลบัตรประชาชนให้เรียบร้อยก่อนทำการ upload รูปภาพ</small></font></center>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="col-sm-12">

                                                                            <asp:LinkButton ID="btntest" runat="server" CssClass="btn btn-info btn-md" OnCommand="btnCommand" CommandName="btntest"><i class="glyphicon glyphicon-picture"></i> ทำแบบทดสอบ &nbsp;</asp:LinkButton>

                                                                        </div>
                                                                    </div>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:PostBackTrigger ControlID="btn_save_file1_edit" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                            <asp:HyperLink runat="server" ID="txtfocus_picture_edit" />
                                                        </div>

                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>



                                        <div class="panel panel-red">
                                            <div class="panel-heading">
                                                <h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i><strong>&nbsp; Personal Information / ข้อมูลส่วนตัว</strong></h3>
                                                <asp:Label ID="lbl_emp_idx" runat="server" Visible="false" Text='<%# Eval("emp_idx") %>' />
                                            </div>


                                            <asp:Panel ID="pn_bg_genarol" runat="server" CssClass="bg-regis">
                                                <asp:UpdatePanel ID="Upd3ateaP2anel1" runat="server" UpdateMode="Always">
                                                    <ContentTemplate>
                                                        <div class="panel-body">
                                                            <div class="form-horizontal" role="form">

                                                                <div class="form-group">
                                                                    <label class="col-sm-2 control-label">
                                                                        <h2 class="panel-title">
                                                                            <asp:Label ID="Label222" runat="server" Text="Name(Thai)" /><br />
                                                                            <small><b>
                                                                                <asp:Label ID="Label224" runat="server" Text="ชื่อ(ไทย)" /></b></small>
                                                                        </h2>
                                                                    </label>
                                                                    <div class="col-sm-1">
                                                                        <asp:Label ID="lbl_prefix_idx" runat="server" Visible="false" Text='<%# Eval("prefix_idx") %>'></asp:Label>
                                                                        <asp:DropDownList ID="ddl_prefix_th_edit" runat="server" CssClass="form-control" Width="105" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                            <asp:ListItem Value="1">นาย</asp:ListItem>
                                                                            <asp:ListItem Value="2">นางสาว</asp:ListItem>
                                                                            <asp:ListItem Value="3">นาง</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="col-sm-2">
                                                                        <asp:TextBox ID="txt_name_th" CssClass="form-control" runat="server" Text='<%# Eval("emp_firstname_th") %>' ValidationGroup="FormEdit"></asp:TextBox>

                                                                        <asp:RequiredFieldValidator ID="C_NameTH" runat="server" ControlToValidate="txt_name_th" Display="None" SetFocusOnError="true" ErrorMessage="*กรอกข้อมูล" ValidationGroup="FormEdit" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="V_NameTH" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="C_NameTH" Width="160" />

                                                                        <asp:RegularExpressionValidator ID="R_NamTH" runat="server" ErrorMessage="*เฉพาะตัวอักษรภาษาไทยเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txt_name_th" ValidationExpression="^[ก-๙'.\s]{1,250}$" ValidationGroup="FormEdit" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="V_NameTH2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_NamTH" Width="160" />
                                                                    </div>
                                                                    <div class="col-sm-1">
                                                                        <asp:Label ID="Label215" runat="server" ForeColor="Red" Text="***" />
                                                                    </div>


                                                                    <label class="col-sm-1 control-label">
                                                                        <h2 class="panel-title">
                                                                            <asp:Label ID="Label1" runat="server" Text="Lastname" /><br />
                                                                            <small><b>
                                                                                <asp:Label ID="Label4" runat="server" Text="นามสกุล" /></b></small>
                                                                        </h2>
                                                                    </label>
                                                                    <div class="col-sm-2">
                                                                        <asp:TextBox ID="txt_surname_th" CssClass="form-control" runat="server" Text='<%# Eval("emp_lastname_th") %>' ValidationGroup="FormEdit"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="C_LastnameTH" runat="server" ControlToValidate="txt_surname_th" Display="None" SetFocusOnError="true" ErrorMessage="*กรอกข้อมูล" ValidationGroup="FormEdit" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="V_LastnameTH" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="C_LastnameTH" Width="160" />

                                                                        <asp:RegularExpressionValidator ID="R_LastNameTH" runat="server" ErrorMessage="*เฉพาะตัวอักษรภาษาไทยเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txt_surname_th" ValidationExpression="^[ก-๙'.\s]{1,250}$" ValidationGroup="FormEdit" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="V_LastNameTH2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_LastNameTH" Width="160" />
                                                                    </div>
                                                                    <div class="col-sm-1">
                                                                        <asp:Label ID="Label216" runat="server" ForeColor="Red" Text="***" />
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="col-sm-2 control-label">
                                                                        <h2 class="panel-title">
                                                                            <asp:Label ID="Label5" runat="server" Text="Name(English)" /><br />
                                                                            <small><b>
                                                                                <asp:Label ID="Label22" runat="server" Text="ชื่อ(อังกฤษ)" /></b></small>
                                                                        </h2>
                                                                    </label>
                                                                    <div class="col-sm-1">
                                                                        <asp:DropDownList ID="ddl_prefix_en_edit" runat="server" CssClass="form-control" Width="105" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                            <asp:ListItem Value="1">Mr.</asp:ListItem>
                                                                            <asp:ListItem Value="2">Miss</asp:ListItem>
                                                                            <asp:ListItem Value="3">Mrs.</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="col-sm-2">
                                                                        <asp:TextBox ID="txt_name_en" CssClass="form-control" runat="server" Text='<%# Eval("emp_firstname_en") %>' ValidationGroup="FormEdit"></asp:TextBox>

                                                                        <asp:RequiredFieldValidator ID="C_NameEN" runat="server" ControlToValidate="txt_name_en" Display="None" SetFocusOnError="true" ErrorMessage="*กรอกข้อมูล" ValidationGroup="FormEdit" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="V_NameEN" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="C_NameEN" Width="160" />

                                                                        <asp:RegularExpressionValidator ID="R_NameEN" runat="server" ErrorMessage="*เเฉพาะตัวอักษรภาษาอังกฤษเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txt_name_en" ValidationExpression="^[a-zA-Z'.\s]{1,250}$" ValidationGroup="FormEdit" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="V_NameEN2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_NameEN" Width="160" />
                                                                    </div>
                                                                    <div class="col-sm-1">
                                                                        <asp:Label ID="Label217" runat="server" ForeColor="Red" Text="***" />
                                                                    </div>

                                                                    <label class="col-sm-1 control-label">
                                                                        <h2 class="panel-title">
                                                                            <asp:Label ID="Label8" runat="server" Text="Lastname" /><br />
                                                                            <small><b>
                                                                                <asp:Label ID="Label24" runat="server" Text="นามสกุล" /></b></small>
                                                                        </h2>
                                                                    </label>
                                                                    <div class="col-sm-2">
                                                                        <asp:TextBox ID="txt_surname_en" CssClass="form-control" runat="server" Text='<%# Eval("emp_lastname_en") %>' ValidationGroup="FormEdit"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="C_LastnameEN" runat="server" ControlToValidate="txt_surname_en" Display="None" SetFocusOnError="true" ErrorMessage="*กรอกข้อมูล" ValidationGroup="FormEdit" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="V_LastnameEN" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="C_LastnameEN" Width="160" />

                                                                        <asp:RegularExpressionValidator ID="R_LastNameEN" runat="server" ErrorMessage="*เฉพาะตัวอักษรภาษาอังกฤษเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txt_surname_en" ValidationExpression="^[a-zA-Z'.\s]{1,250}$" ValidationGroup="FormEdit" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="V_LastnameEN2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_LastNameEN" Width="160" />
                                                                    </div>
                                                                    <div class="col-sm-1">
                                                                        <asp:Label ID="Label218" runat="server" ForeColor="Red" Text="***" />
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="col-sm-2 control-label">
                                                                        <h2 class="panel-title">
                                                                            <asp:Label ID="Label10" runat="server" Text="NickName(Thai)" /><br />
                                                                            <small><b>
                                                                                <asp:Label ID="Label25" runat="server" Text="ชื่อเล่น(ไทย)" /></b></small>
                                                                        </h2>
                                                                    </label>
                                                                    <div class="col-sm-2">
                                                                        <asp:TextBox ID="txt_nickname_th" CssClass="form-control" runat="server" Text='<%# Eval("emp_nickname_th") %>' ValidationGroup="FormEdit"></asp:TextBox>

                                                                        <asp:RegularExpressionValidator ID="R_NickNameTH" runat="server" ErrorMessage="*เฉพาะตัวอักษรภาษาไทยเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txt_nickname_th" ValidationExpression="^[ก-๙'.\s]{1,250}$" ValidationGroup="FormEdit" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="V_NickNameTH" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_NickNameTH" Width="160" />
                                                                    </div>
                                                                    <div class="col-sm-1">
                                                                        <asp:Label ID="Label14" runat="server" ForeColor="Red" Text="***" />
                                                                    </div>

                                                                    <label class="col-sm-2 control-label">
                                                                        <h2 class="panel-title">
                                                                            <asp:Label ID="Label11" runat="server" Text="NickName(English)" /><br />
                                                                            <small><b>
                                                                                <asp:Label ID="Label26" runat="server" Text="ชื่อเล่น(อังกฤษ)" /></b></small>
                                                                        </h2>
                                                                    </label>
                                                                    <div class="col-sm-2">
                                                                        <asp:TextBox ID="txt_nickname_en" CssClass="form-control" runat="server" Text='<%# Eval("emp_nickname_en") %>' ValidationGroup="FormEdit"></asp:TextBox>

                                                                        <asp:RegularExpressionValidator ID="R_NickNameEN" runat="server" ErrorMessage="*เฉพาะตัวอักษรภาษาไทยเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txt_nickname_en" ValidationExpression="^[a-zA-Z'.\s]{1,250}$" ValidationGroup="FormEdit" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="V_NickNameEN" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_NickNameEN" Width="160" />
                                                                    </div>
                                                                    <div class="col-sm-1">
                                                                        <asp:Label ID="Label18" runat="server" ForeColor="Red" Text="***" />
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="col-sm-2 control-label">
                                                                        <h2 class="panel-title">
                                                                            <asp:Label ID="Label15" runat="server" Text="Birthday" /><br />
                                                                            <small><b>
                                                                                <asp:Label ID="Label16" runat="server" Text="วันเกิด" /></b></small>
                                                                        </h2>
                                                                    </label>
                                                                    <div class="col-sm-2">
                                                                        <div class='input-group date'>
                                                                            <asp:TextBox ID="txtbirth" CssClass="form-control from-date-datepicker" MaxLengh="100%" runat="server" Text='<%# Eval("emp_birthday") %>' ValidationGroup="FormEdit"></asp:TextBox>
                                                                            <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                        </div>
                                                                        <asp:RequiredFieldValidator ID="RdVasator2" ValidationGroup="FormEdit" runat="server" Display="None"
                                                                            ControlToValidate="txtbirth" Font-Size="11"
                                                                            ErrorMessage="วันเกิด ...." />
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="Vsalidlwoer6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RdVasator2" Width="160" />
                                                                    </div>
                                                                    <div class="col-sm-1">
                                                                        <asp:Label ID="Label108" runat="server" ForeColor="Red" Text="***" />
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="col-sm-2 control-label ">
                                                                        <h2 class="panel-title">
                                                                            <asp:Label ID="Label27" runat="server" Text="Sex" /><br />
                                                                            <small><b>
                                                                                <asp:Label ID="Label28" runat="server" Text="เพศ" /></b></small>
                                                                        </h2>
                                                                    </label>

                                                                    <div class="col-sm-2">
                                                                        <asp:Label ID="lbl_sex_idx" runat="server" Visible="false" Text='<%# Eval("sex_idx") %>'></asp:Label>
                                                                        <asp:DropDownList ID="ddl_sex" runat="server" CssClass="form-control" ValidationGroup="FormEdit">
                                                                            <asp:ListItem Value="0">เลือกสถานะเพศ....</asp:ListItem>
                                                                            <asp:ListItem Value="1">ชาย</asp:ListItem>
                                                                            <asp:ListItem Value="2">หญิง</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                        <asp:RequiredFieldValidator ID="Reqqu9iar1" ValidationGroup="FormEdit" runat="server" Display="None"
                                                                            ControlToValidate="ddl_sex" Font-Size="11"
                                                                            ErrorMessage="เพศ ...."
                                                                            ValidationExpression="เพศ ...." InitialValue="0" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="Valistoder1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Reqqu9iar1" Width="160" />
                                                                    </div>
                                                                    <div class="col-sm-1">
                                                                        <asp:Label ID="Label20" runat="server" ForeColor="Red" Text="***" />
                                                                    </div>

                                                                    <label class="col-sm-2 control-label">
                                                                        <h2 class="panel-title">
                                                                            <asp:Label ID="Labe3l15" runat="server" Text="Status" /><br />
                                                                            <small><b>
                                                                                <asp:Label ID="Lab2el16" runat="server" Text="สถานภาพ" /></b></small>
                                                                        </h2>
                                                                    </label>

                                                                    <div class="col-sm-2">
                                                                        <asp:Label ID="lbl_married_status_idx" runat="server" Visible="false" Text='<%# Eval("married_status") %>'></asp:Label>
                                                                        <asp:DropDownList ID="ddl_status" runat="server" CssClass="form-control">
                                                                            <asp:ListItem Value="0">เลือกสถานภาพ....</asp:ListItem>
                                                                            <asp:ListItem Value="1">โสด</asp:ListItem>
                                                                            <asp:ListItem Value="2">สมรส</asp:ListItem>
                                                                            <asp:ListItem Value="3">หย่าร้าง</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="col-sm-2 control-label">
                                                                        <h2 class="panel-title">
                                                                            <asp:Label ID="Labe4l231" runat="server" Text="Nationality" /><br />
                                                                            <small><b>
                                                                                <asp:Label ID="Lab3el232" runat="server" Text="สัญชาติ" /></b></small>
                                                                        </h2>
                                                                    </label>
                                                                    <div class="col-sm-2">
                                                                        <asp:Label ID="lbl_nation_idx" runat="server" Visible="false" Text='<%# Eval("nat_idx") %>'></asp:Label>
                                                                        <asp:DropDownList ID="ddlnation_edit" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                            <asp:ListItem Value="0">เลือกสัญชาติ....</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>

                                                                    <label class="col-sm-2 control-label col-sm-offset-1">
                                                                        <h2 class="panel-title">
                                                                            <asp:Label ID="La2bel42" runat="server" Text="Race" /><br />
                                                                            <small><b>
                                                                                <asp:Label ID="La4bel43" runat="server" Text="เชื้อชาติ" /></b></small>
                                                                        </h2>
                                                                    </label>

                                                                    <div class="col-sm-2">
                                                                        <asp:Label ID="lbl_race_idx" runat="server" Visible="false" Text='<%# Eval("race_idx") %>'></asp:Label>
                                                                        <asp:DropDownList ID="ddlrace" runat="server" CssClass="form-control">
                                                                            <asp:ListItem Value="0">เลือกเชื้อชาติ....</asp:ListItem>
                                                                            <asp:ListItem Value="1">ไทย</asp:ListItem>
                                                                            <asp:ListItem Value="2">ลาว</asp:ListItem>
                                                                            <asp:ListItem Value="3">พม่า</asp:ListItem>
                                                                            <asp:ListItem Value="4">กัมพูชา</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="col-sm-2 control-label">
                                                                        <h2 class="panel-title">
                                                                            <asp:Label ID="Lab4el47" runat="server" Text="Religion" /><br />
                                                                            <small><b>
                                                                                <asp:Label ID="Lab3el49" runat="server" Text="ศาสนา" /></b></small>
                                                                        </h2>
                                                                    </label>

                                                                    <div class="col-sm-2">
                                                                        <asp:Label ID="lbl_rel_idx" runat="server" Visible="false" Text='<%# Eval("rel_idx") %>'></asp:Label>
                                                                        <asp:DropDownList ID="ddlreligion" runat="server" CssClass="form-control">
                                                                            <asp:ListItem Value="0">เลือกศาสนา....</asp:ListItem>
                                                                            <asp:ListItem Value="1">ศาสนาพุทธ</asp:ListItem>
                                                                            <asp:ListItem Value="2">ศาสนาคริสต์</asp:ListItem>
                                                                            <asp:ListItem Value="3">ศาสนาอิศลาม</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="col-sm-2 control-label">
                                                                        <h2 class="panel-title">
                                                                            <asp:Label ID="Label29" runat="server" Text="MilitaryStatus" /><br />
                                                                            <small><b>
                                                                                <asp:Label ID="Label31" runat="server" Text="สถานะทางทหาร" /></b></small>
                                                                        </h2>
                                                                    </label>

                                                                    <div class="col-sm-2">
                                                                        <asp:Label ID="lbl_mil_idx" runat="server" Visible="false" Text='<%# Eval("mil_idx") %>'></asp:Label>
                                                                        <asp:DropDownList ID="ddlmilitary_edit" runat="server" CssClass="form-control" ValidationGroup="FormEdit">
                                                                            <asp:ListItem Value="1">ผ่านการเกณทหาร</asp:ListItem>
                                                                            <asp:ListItem Value="2">เรียนรักษาดินแดน</asp:ListItem>
                                                                            <asp:ListItem Value="3">ได้รับข้อยกเว้น</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                        <asp:RequiredFieldValidator ID="Re3atsor1" ValidationGroup="FormEdit" runat="server" Display="None"
                                                                            ControlToValidate="ddlmilitary_edit" Font-Size="11"
                                                                            ErrorMessage="สถานะทางทหาร ...."
                                                                            ValidationExpression="สถานะทางทหาร ...." InitialValue="0" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValirCalder1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re3atsor1" Width="160" />
                                                                    </div>
                                                                    <div class="col-sm-1">
                                                                        <asp:Label ID="Label21" runat="server" ForeColor="Red" Text="***" />
                                                                    </div>

                                                                    <label class="col-sm-2 control-label">
                                                                        <h2 class="panel-title">
                                                                            <asp:Label ID="Label32" runat="server" Text="ID Card" /><br />
                                                                            <small><b>
                                                                                <asp:Label ID="Label33" runat="server" Text="เลขบัตรประชาชน / Tax ID (ต่างชาติ)" /></b></small>
                                                                        </h2>
                                                                    </label>
                                                                    <div class="col-sm-2">
                                                                        <asp:Label ID="lblidentity_card_edit" runat="server" Visible="false" Text='<%# Eval("identity_card") %>'></asp:Label>
                                                                        <asp:TextBox ID="txtidcard" CssClass="form-control" runat="server" ValidationGroup="FormEdit" Text='<%# Eval("identity_card") %>'></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="Reatsor1" ValidationGroup="FormEdit" runat="server" Display="None"
                                                                            ControlToValidate="txtidcard" Font-Size="11"
                                                                            ErrorMessage="เลขบัตรประชาชน" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="Vaasalwr1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Reatsor1" Width="160" />
                                                                        <asp:RegularExpressionValidator ID="Regaddaur1" runat="server" ValidationGroup="FormEdit" Display="None"
                                                                            ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                                            ControlToValidate="txtidcard"
                                                                            ValidationExpression="^[0-9]{1,13}$" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="Valiasdader4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Regaddaur1" Width="160" />
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="col-sm-2 control-label">
                                                                        <h2 class="panel-title">
                                                                            <asp:Label ID="Label36" runat="server" Text="Issued At" /><br />
                                                                            <small><b>
                                                                                <asp:Label ID="Label37" runat="server" Text="อกกให้ ณ" /></b></small>
                                                                        </h2>
                                                                    </label>
                                                                    <div class="col-sm-2">
                                                                        <asp:TextBox ID="txtissued_at" CssClass="form-control" runat="server" Text='<%# Eval("issued_at") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                                    </div>

                                                                    <label class="col-sm-2 control-label col-sm-offset-1">
                                                                        <h2 class="panel-title">
                                                                            <asp:Label ID="Label34" runat="server" Text="Expiration Date" /><br />
                                                                            <small><b>
                                                                                <asp:Label ID="Label35" runat="server" Text="วันหมดอายุ" /></b></small>
                                                                        </h2>
                                                                    </label>
                                                                    <div class="col-sm-2">
                                                                        <div class='input-group date'>
                                                                            <asp:TextBox ID="txtexpcard" CssClass="form-control from-date-datepicker" MaxLengh="100%" runat="server" Text='<%# Eval("idate_expired") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                                            <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                        </div>
                                                                    </div>
                                                                </div>


                                                                <div class="form-group">

                                                                    <label class="col-sm-2 control-label">
                                                                        <h2 class="panel-title">
                                                                            <asp:Label ID="Labsel2271" runat="server" Text="Upload document" /><br />
                                                                            <small><b>
                                                                                <asp:Label ID="Labe2dl269" runat="server" Text="เอกสารเพิ่มเติม" /></b></small>
                                                                        </h2>
                                                                    </label>
                                                                    <div class="col-sm-3">
                                                                        <div class="panel panel-red">
                                                                            <div class="form-group">
                                                                                <div class="panel-body">
                                                                                    <asp:FileUpload ID="FileUpload_edit" ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small" ClientIDMode="Static" runat="server" CssClass="btn btn-sm multi" accept="gif|jpg|png|JPEG|pdf" />
                                                                                    <center><font color="red"><small class="text_right"><p>**เฉพาะนามสกุล gif , jpg , png , pdf</small></font></center>
                                                                                    <hr />
                                                                                    <center><asp:LinkButton ID="btn_save_document_edit" runat="server" CssClass="btn btn-warning btn-sm" data-toggle="tooltip" title="อัพโหลดภาพ" CommandName="savefile_document_edit" OnCommand="btnCommand"><i class="glyphicon glyphicon-picture"></i> อัพโหลดภาพ</asp:LinkButton></center>
                                                                                    <center><font color="red"><small class="text_right"><p>กรุณากรอกข้อมูลบัตรประชาชนให้เรียบร้อยก่อนทำการ upload รูปภาพ</small></font></center>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <label class="col-sm-2 control-label">
                                                                        <h2 class="panel-title">
                                                                            <asp:Label ID="Labfel223" runat="server" Text="Document List" /><br />
                                                                            <small><b>
                                                                                <asp:Label ID="Lasb3el30" runat="server" Text="เอกสารแนบทั้งหมด" /></b></small>
                                                                        </h2>
                                                                    </label>
                                                                    <div class="col-sm-3">
                                                                        <asp:GridView ID="gvFileEmp_edit" Visible="true" runat="server"
                                                                            AutoGenerateColumns="false"
                                                                            CssClass="table table-striped table-bordered table-hover table-responsive"
                                                                            HeaderStyle-CssClass="warning"
                                                                            OnRowDataBound="Master_RowDataBound"
                                                                            BorderStyle="None"
                                                                            CellSpacing="2"
                                                                            Font-Size="Small">
                                                                            <EmptyDataTemplate>
                                                                                <div style="text-align: center">Data Cannot Be Found</div>
                                                                            </EmptyDataTemplate>

                                                                            <Columns>
                                                                                <asp:TemplateField HeaderText="ไฟล์เอกสารเพิ่มเติม">
                                                                                    <ItemTemplate>
                                                                                        <div class="col-lg-8">
                                                                                            <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />

                                                                                        </div>
                                                                                        <div class="col-lg-2">
                                                                                            <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-default" data-original-title="Download" Target="_blank"><i class="fa fa-download"></i></asp:HyperLink>

                                                                                            <asp:HiddenField runat="server" ID="hidFile11" Value='<%# Eval("Download") %>' />
                                                                                        </div>

                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>

                                                                </div>

                                                                <asp:HyperLink runat="server" ID="txtfocus_detail_edit" />

                                                            </div>
                                                        </div>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="btn_save_document_edit" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </asp:Panel>
                                        </div>


                                        <div class="panel panel-red">
                                            <div class="panel-heading">
                                                <h3 class="panel-title"><i class="glyphicon glyphicon-road"></i><strong>&nbsp; Driving Status / สถานะการขับขี่ยานพาหนะ</strong></h3>
                                            </div>
                                            <asp:Panel ID="Panel16" runat="server" CssClass="bg-regis">
                                                <div class="panel-body">
                                                    <div class="form-horizontal" role="form">

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label ">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label113" runat="server" Text="Driving Car" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label114" runat="server" Text="ขับขี่รถยนต์" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:Label ID="lbl_dvcar_idx" runat="server" Visible="false" Text='<%# Eval("dvcar_idx") %>'></asp:Label>
                                                                <asp:DropDownList ID="ddldvcar" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0">เลือกสถานะการขับขี่....</asp:ListItem>
                                                                    <asp:ListItem Value="1">ขับได้</asp:ListItem>
                                                                    <asp:ListItem Value="2">ขับไม่ได้</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label115" runat="server" Text="Licens Driving Car" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label116" runat="server" Text="เลขที่ใบขับขี่" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtlicen_car" CssClass="form-control" runat="server" Text='<%# Eval("dvlicen_car") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label ">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label275" runat="server" Text="Driving Car Status" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label276" runat="server" Text="รถยนต์ส่วนตัว" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:Label ID="lbl_dvcar_status" runat="server" Visible="false" Text='<%# Eval("dvcarstatus_idx") %>'></asp:Label>
                                                                <asp:DropDownList ID="ddldvcarstatus" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0">เลือกสถานะรถ....</asp:ListItem>
                                                                    <asp:ListItem Value="1">มีรถยนต์ส่วนตัว</asp:ListItem>
                                                                    <asp:ListItem Value="2">ไม่มีรถยนต์ส่วนตัว</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label241" runat="server" Text="Driving Motocyle" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label242" runat="server" Text="ขับขี่รถจักรยานยนต์" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:Label ID="lbl_dvmt_idx" runat="server" Visible="false" Text='<%# Eval("dvmt_idx") %>'></asp:Label>
                                                                <asp:DropDownList ID="ddldvmt" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0">เลือกสถานะการขับขี่....</asp:ListItem>
                                                                    <asp:ListItem Value="1">ขับได้</asp:ListItem>
                                                                    <asp:ListItem Value="2">ขับไม่ได้</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label228" runat="server" Text="Licens Driving Motocyle" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label230" runat="server" Text="เลขที่ใบขับขี่" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtlicens_moto" CssClass="form-control" runat="server" Text='<%# Eval("dvlicen_moto") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label ">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label277" runat="server" Text="Driving Motocyle Status" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label278" runat="server" Text="รถจักรยานยนต์ส่วนตัว" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:Label ID="lbl_dvmoto_status" runat="server" Visible="false" Text='<%# Eval("dvmtstatus_idx") %>'></asp:Label>
                                                                <asp:DropDownList ID="ddldvmtstatus" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0">เลือกสถานะรถ....</asp:ListItem>
                                                                    <asp:ListItem Value="1">มีรถจักรยานยนต์ส่วนตัว</asp:ListItem>
                                                                    <asp:ListItem Value="2">ไม่มีรถจักรยานยนต์ส่วนตัว</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label233" runat="server" Text="Driving Forklift" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label234" runat="server" Text="ขับขี่รถโฟล์คลิฟท์" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:Label ID="lbl_dvfork_idx" runat="server" Visible="false" Text='<%# Eval("dvfork_idx") %>'></asp:Label>
                                                                <asp:DropDownList ID="ddldvfork" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0">เลือกสถานะการขับขี่....</asp:ListItem>
                                                                    <asp:ListItem Value="1">ขับได้</asp:ListItem>
                                                                    <asp:ListItem Value="2">ขับไม่ได้</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label235" runat="server" Text="Licens Driving Forklift" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label236" runat="server" Text="เลขที่ใบขับขี่" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtlicens_fork" CssClass="form-control" runat="server" Text='<%# Eval("dvlicen_fork") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label237" runat="server" Text="Driving Truck" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label238" runat="server" Text="ขับขี่รถบรรทุก" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:Label ID="lbl_dvtruck_idx" runat="server" Visible="false" Text='<%# Eval("dvtruck_idx") %>'></asp:Label>
                                                                <asp:DropDownList ID="ddldvtruck" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0">เลือกสถานะการขับขี่....</asp:ListItem>
                                                                    <asp:ListItem Value="1">ขับได้</asp:ListItem>
                                                                    <asp:ListItem Value="2">ขับไม่ได้</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label239" runat="server" Text="Licens Driving Truck" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label240" runat="server" Text="เลขที่ใบขับขี่" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtlicens_truck" CssClass="form-control" runat="server" Text='<%# Eval("dvlicen_truck") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>

                                        <%-----------------------------------------------------------------------------------------------%>
                                        <%--Emergency--%>
                                        <%-----------------------------------------------------------------------------------------------%>

                                        <div class="panel panel-red">
                                            <div class="panel-heading">
                                                <h3 class="panel-title"><i class="glyphicon glyphicon-phone-alt"></i><strong>&nbsp; Emergency / ข้อมูลติดต่อกรณีฉุกเฉิน</strong></h3>
                                            </div>
                                            <asp:Panel ID="Panel18" runat="server" CssClass="bg-regis">
                                                <div class="panel-body">
                                                    <div class="form-horizontal" role="form">

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label135" runat="server" Text="Emergency Contact" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label136" runat="server" Text="กรณีฉุกเฉินติดต่อ" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtemercon" CssClass="form-control" runat="server" Text='<%# Eval("emer_name") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label133" runat="server" Text="Relationship" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label134" runat="server" Text="ความสัมพันธ์" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtrelation" CssClass="form-control" runat="server" Text='<%# Eval("emer_relation") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label137" runat="server" Text="Telephone" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label138" runat="server" Text="เบอร์โทร" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txttelemer" CssClass="form-control" runat="server" Text='<%# Eval("emer_tel") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>

                                        <%-----------------------------------------------------------------------------------------------%>
                                        <%--Family--%>
                                        <%-----------------------------------------------------------------------------------------------%>
                                        <div class="panel panel-red">
                                            <div class="panel-heading">
                                                <h3 class="panel-title"><i class="glyphicon glyphicon-home"></i><strong>&nbsp; Family / ข้อมูลครอบครัว</strong></h3>
                                            </div>
                                            <asp:Panel ID="Panel19" runat="server" CssClass="bg-regis">
                                                <div class="panel-body">
                                                    <div class="form-horizontal" role="form">

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label141" runat="server" Text="Father Name" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label142" runat="server" Text="ชื่อ-สกุล บิดา" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtfathername_edit" CssClass="form-control" runat="server" Text='<%# Eval("fathername") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label243" runat="server" Text="Licen no." /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label244" runat="server" Text="เลขบัตรประชาชน" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtlicenfather_edit" CssClass="form-control" runat="server" Text='<%# Eval("licen_father") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label251" runat="server" Text="Telephone" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label252" runat="server" Text="เบอร์ติดต่อ" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txttelfather_edit" CssClass="form-control" runat="server" Text='<%# Eval("telfather") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label145" runat="server" Text="Mother Name" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label146" runat="server" Text="ชื่อ-สกุล มารดา" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtmothername_edit" CssClass="form-control" runat="server" Text='<%# Eval("mothername") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label247" runat="server" Text="Licen no." /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label248" runat="server" Text="เลขบัตรประชาชน" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtlicenmother_edit" CssClass="form-control" runat="server" Text='<%# Eval("licen_mother") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label147" runat="server" Text="Telephone" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label148" runat="server" Text="เบอร์ติดต่อ" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txttelmother_edit" CssClass="form-control" runat="server" Text='<%# Eval("telmother") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label149" runat="server" Text="Wife Name" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label150" runat="server" Text="ชื่อ-สกุล คู่สมรส" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtwifename_edit" CssClass="form-control" runat="server" Text='<%# Eval("wifename") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label249" runat="server" Text="Licen no." /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label250" runat="server" Text="เลขบัตรประชาชน" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtlicenwife_edit" CssClass="form-control" runat="server" Text='<%# Eval("licen_wife") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label151" runat="server" Text="Telephone" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label152" runat="server" Text="เบอร์ติดต่อ" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txttelwife_edit" CssClass="form-control" runat="server" Text='<%# Eval("telwife") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-12">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label3" runat="server" CssClass="h01" Text="Family" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label7" runat="server" Text="ข้อมูลบุตร" /></b></small>
                                                                </h2>
                                                            </label>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-lg-12">

                                                                <asp:GridView ID="GvChildAdd_View"
                                                                    runat="server"
                                                                    AutoGenerateColumns="false"
                                                                    CssClass="table table-striped table-bordered table-hover"
                                                                    HeaderStyle-CssClass="table_headCenter"
                                                                    HeaderStyle-Height="40px"
                                                                    ShowFooter="False"
                                                                    ShowHeaderWhenEmpty="True"
                                                                    AllowPaging="True"
                                                                    PageSize="10"
                                                                    BorderStyle="None"
                                                                    CellSpacing="2"
                                                                    DataKeyNames="CHIDX"
                                                                    OnRowEditing="gvRowEditing"
                                                                    OnPageIndexChanging="Master_PageIndexChanging"
                                                                    OnRowCancelingEdit="gvRowCancelingEdit"
                                                                    OnRowDataBound="Master_RowDataBound"
                                                                    OnRowUpdating="gvRowUpdating">

                                                                    <PagerStyle CssClass="PageCustom" />
                                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                                    <EmptyDataTemplate>
                                                                        <div style="text-align: center">Data Cannot Be Found</div>
                                                                    </EmptyDataTemplate>

                                                                    <Columns>

                                                                        <asp:TemplateField HeaderText="#">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <%# (Container.DataItemIndex +1) %>
                                                                                    <asp:Label ID="Lab2el5w82" runat="server" Visible="false" Text='<%# Eval("CHIDX")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>

                                                                            <EditItemTemplate>
                                                                                <div class="row">
                                                                                    <div class="col-lg-10">
                                                                                        <h3>Child FormEdit<small> (แบบฟอร์มแก้ไขข้อมูลบุตร)</small></h3>
                                                                                        <asp:Label ID="lbl_Chidx_edit" runat="server" Visible="false" Text='<%# Eval("CHIDX")%>'></asp:Label>
                                                                                    </div>
                                                                                </div>
                                                                                <hr />

                                                                                <div class="form-group">
                                                                                    <asp:Label ID="Label65" runat="server" Text="ชื่อ-นามสกุล" CssClass="col-sm-3 control-label"></asp:Label>
                                                                                    <div class="col-sm-4">
                                                                                        <asp:TextBox ID="txtChild_name_edit" runat="server" CssClass="form-control" Text='<%# Eval("Child_name") %>'></asp:TextBox>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group">
                                                                                    <asp:Label ID="Label66" runat="server" Text="บุตรคนที่" CssClass="col-sm-3 control-label"></asp:Label>
                                                                                    <div class="col-sm-4">
                                                                                        <asp:Label ID="lbl_Child_num" runat="server" Visible="false" Text='<%# Eval("Child_num") %>'></asp:Label>
                                                                                        <asp:DropDownList ID="ddlchildnumber" runat="server" CssClass="form-control">
                                                                                            <asp:ListItem Value="0">เลือกจำนวนบุตร....</asp:ListItem>
                                                                                            <asp:ListItem Value="1">1</asp:ListItem>
                                                                                            <asp:ListItem Value="2">2</asp:ListItem>
                                                                                            <asp:ListItem Value="3">3</asp:ListItem>
                                                                                            <asp:ListItem Value="4">4</asp:ListItem>
                                                                                            <asp:ListItem Value="5">5</asp:ListItem>
                                                                                            <asp:ListItem Value="6">6</asp:ListItem>
                                                                                            <asp:ListItem Value="7">7</asp:ListItem>
                                                                                            <asp:ListItem Value="8">8</asp:ListItem>
                                                                                            <asp:ListItem Value="9">9</asp:ListItem>
                                                                                        </asp:DropDownList>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group">
                                                                                    <div class="col-lg-offset-10">
                                                                                        <div class="control-label" style="text-align: center;">
                                                                                            <asp:LinkButton ID="LinkButton1" runat="server" Text="Update" ValidationGroup="SaveUpdate" CssClass="btn btn-success" OnClientClick="return confirm('คุณต้องการแก้ไขและบันทึกรายการนี้ใช่หรือไม่ ?')" CommandName="Update" />
                                                                                            <asp:LinkButton ID="LinkButton2" runat="server" Text="Cancel" CssClass="btn btn-danger" CommandName="Cancel" />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </EditItemTemplate>

                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="ชื่อ-นามสกุล">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="Labe2l5w38" runat="server" Text='<%# Eval("Child_name")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="บุตรคนที่">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="lbP2a4g2e1" runat="server" Text='<%# Eval("Child_num")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="จัดการ">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="Edit" runat="server" ValidationGroup="fromgroup" Text="Edit" CssClass="btn btn-info" CommandName="Edit" />
                                                                                <asp:LinkButton ID="btnDeleteGV" runat="server" ValidationGroup="fromgroup" OnCommand="btnCommand" CommandName="DeleteGvChildAdd_View" CommandArgument='<%# Eval("CHIDX") %>' Text="Delete" CssClass="btn btn-danger" OnClientClick="return confirm('Do you want delete this item?')" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                    </Columns>

                                                                </asp:GridView>

                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-sm-12">
                                                                <div class="checkbox">
                                                                    <asp:CheckBox ID="chkchild_edit" runat="server" Text="เพิ่มข้อมูลบุตร" CssClass="col-sm-4" AutoPostBack="true" OnCheckedChanged="chkSelectedIndexChanged" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <asp:Panel ID="BoxChild" runat="server" Visible="false">
                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label155" runat="server" Text="Child Name" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label156" runat="server" Text="ชื่อ-สกุล บุตร" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-2">
                                                                    <asp:TextBox ID="txtchildname_edit" CssClass="form-control" runat="server" ValidationGroup="AddChild_Edit"></asp:TextBox>

                                                                    <asp:RequiredFieldValidator ID="RequirvceasddFieldValidator1" ValidationGroup="AddChild_Edit" runat="server" Display="None"
                                                                        ControlToValidate="txtchildname_edit" Font-Size="11"
                                                                        ErrorMessage="กรอกชื่อ ...." />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValdsidatorasCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequirvceasddFieldValidator1" Width="160" />
                                                                </div>

                                                                <label class="col-sm-2 control-label col-sm-offset-1">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label157" runat="server" Text="Child Number" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label158" runat="server" Text="เป็นบุตรคนที่" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-2">
                                                                    <asp:DropDownList ID="ddlchildnumber_edit" runat="server" CssClass="form-control">
                                                                        <asp:ListItem Value="0">บุตรคนที่....</asp:ListItem>
                                                                        <asp:ListItem Value="1">1</asp:ListItem>
                                                                        <asp:ListItem Value="2">2</asp:ListItem>
                                                                        <asp:ListItem Value="3">3</asp:ListItem>
                                                                        <asp:ListItem Value="4">4</asp:ListItem>
                                                                        <asp:ListItem Value="5">5</asp:ListItem>
                                                                        <asp:ListItem Value="6">6</asp:ListItem>
                                                                        <asp:ListItem Value="7">7</asp:ListItem>
                                                                        <asp:ListItem Value="8">8</asp:ListItem>
                                                                        <asp:ListItem Value="9">9</asp:ListItem>
                                                                    </asp:DropDownList>

                                                                    <asp:RequiredFieldValidator ID="R2equi3redFi5dews4e4ldValidator1" ValidationGroup="AddChild_Edit" runat="server" Display="None"
                                                                        ControlToValidate="ddlchildnumber_edit" Font-Size="11"
                                                                        ErrorMessage="เป็นบุตรคนที่ ...."
                                                                        ValidationExpression="เป็นบุตรคนที่ ...." InitialValue="0" />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="Validat4orCallosutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R2equi3redFi5dews4e4ldValidator1" Width="160" />
                                                                </div>

                                                                <div class="col-sm-1">
                                                                    <asp:LinkButton ID="btnAddChild" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="AddChild_Edit" ValidationGroup="AddChild_Edit">ADD + &nbsp;</asp:LinkButton>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <div class="col-lg-12">

                                                                    <asp:GridView ID="GvChildAdd_Edit"
                                                                        runat="server"
                                                                        AutoGenerateColumns="false"
                                                                        CssClass="table table-striped table-bordered table-hover"
                                                                        HeaderStyle-CssClass="table_headCenter"
                                                                        HeaderStyle-Height="40px"
                                                                        ShowFooter="False"
                                                                        ShowHeaderWhenEmpty="True"
                                                                        AllowPaging="True"
                                                                        PageSize="10"
                                                                        BorderStyle="None"
                                                                        CellSpacing="2"
                                                                        OnRowDeleting="gvRowDeleting">

                                                                        <PagerStyle CssClass="PageCustom" />
                                                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                                        <EmptyDataTemplate>
                                                                            <div style="text-align: center">Data Cannot Be Found</div>
                                                                        </EmptyDataTemplate>

                                                                        <Columns>

                                                                            <asp:TemplateField HeaderText="#">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <%# (Container.DataItemIndex +1) %>
                                                                                    </div>
                                                                                </ItemTemplate>

                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="ชื่อ-นามสกุล">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <asp:Label ID="Label5w8" runat="server" Text='<%# Eval("Child_name")%>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="บุตรคนที่">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <asp:Label ID="lbPag2e1" runat="server" Text='<%# Eval("Child_num")%>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="จัดการ">
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="btnDeleteGV" runat="server" ValidationGroup="fromgroup" CommandName="Delete" Text="Delete" CssClass="btn btn-danger" OnClientClick="return confirm('Do you want delete this item?')" />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                        </Columns>

                                                                    </asp:GridView>

                                                                </div>
                                                            </div>
                                                        </asp:Panel>

                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>

                                        <%-----------------------------------------------------------------------------------------------%>
                                        <%--Reference--%>
                                        <%-----------------------------------------------------------------------------------------------%>
                                        <div class="panel panel-red">
                                            <div class="panel-heading">
                                                <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; Reference / บุคคลอ้างอิง</strong></h3>
                                            </div>
                                            <asp:Panel ID="Panel20" runat="server" CssClass="bg-regis">
                                                <div class="panel-body">
                                                    <div class="form-horizontal" role="form">

                                                        <div class="form-group">
                                                            <div class="col-lg-12">

                                                                <asp:GridView ID="GvReference_View"
                                                                    runat="server"
                                                                    AutoGenerateColumns="false"
                                                                    CssClass="table table-striped table-bordered table-hover"
                                                                    HeaderStyle-CssClass="table_headCenter"
                                                                    HeaderStyle-Height="40px"
                                                                    ShowFooter="False"
                                                                    ShowHeaderWhenEmpty="True"
                                                                    AllowPaging="True"
                                                                    PageSize="5"
                                                                    BorderStyle="None"
                                                                    CellSpacing="2"
                                                                    DataKeyNames="ReIDX"
                                                                    OnRowEditing="gvRowEditing"
                                                                    OnPageIndexChanging="Master_PageIndexChanging"
                                                                    OnRowCancelingEdit="gvRowCancelingEdit"
                                                                    OnRowDataBound="Master_RowDataBound"
                                                                    OnRowUpdating="gvRowUpdating">

                                                                    <PagerStyle CssClass="PageCustom" />
                                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                                    <EmptyDataTemplate>
                                                                        <div style="text-align: center">Data Cannot Be Found</div>
                                                                    </EmptyDataTemplate>

                                                                    <Columns>

                                                                        <asp:TemplateField HeaderText="#">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <%# (Container.DataItemIndex +1) %>
                                                                                    <asp:Label ID="L2abs2del5w8" runat="server" Visible="false" Text='<%# Eval("ReIDX")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>

                                                                            <EditItemTemplate>
                                                                                <asp:Label ID="sf" runat="server"></asp:Label>
                                                                                <div class="row">
                                                                                    <div class="col-lg-10">
                                                                                        <h3>Experiences FormEdit<small> (แบบฟอร์มแก้ไขข้อมูลบุคคลอ้างอิง)</small></h3>
                                                                                        <asp:Label ID="lbl_Ref_edit" runat="server" Visible="false" Text='<%# Eval("ReIDX")%>'></asp:Label>
                                                                                    </div>
                                                                                </div>
                                                                                <hr />

                                                                                <div class="form-group">
                                                                                    <asp:Label ID="Label65" runat="server" Text="ชื่อ-นามสกุล" CssClass="col-sm-3 control-label"></asp:Label>
                                                                                    <div class="col-sm-4">
                                                                                        <asp:TextBox ID="txtRef_Name_edit" runat="server" CssClass="form-control" Text='<%# Eval("ref_fullname") %>'></asp:TextBox>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group">
                                                                                    <asp:Label ID="Label66" runat="server" Text="ตำแหน่ง" CssClass="col-sm-3 control-label"></asp:Label>
                                                                                    <div class="col-sm-4">
                                                                                        <asp:TextBox ID="txtRef_pos_edit" runat="server" CssClass="form-control" Text='<%# Eval("ref_position") %>'></asp:TextBox>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group">
                                                                                    <asp:Label ID="Label160" runat="server" Text="ความสัมพันธ์" CssClass="col-sm-3 control-label"></asp:Label>
                                                                                    <div class="col-sm-4">
                                                                                        <asp:TextBox ID="txtRef_relation_edit" runat="server" CssClass="form-control" Text='<%# Eval("ref_relation") %>'></asp:TextBox>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group">
                                                                                    <asp:Label ID="Label12" runat="server" Text="เบอร์โทรศัพท์" CssClass="col-sm-3 control-label"></asp:Label>
                                                                                    <div class="col-sm-4">
                                                                                        <asp:TextBox ID="txtRef_tel_edit" runat="server" CssClass="form-control" Text='<%# Eval("ref_tel") %>'></asp:TextBox>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group">
                                                                                    <div class="col-lg-offset-10">
                                                                                        <div class="control-label" style="text-align: center;">
                                                                                            <asp:LinkButton ID="LinkButton1" runat="server" Text="Update" ValidationGroup="SaveUpdate" CssClass="btn btn-success" OnClientClick="return confirm('คุณต้องการแก้ไขและบันทึกรายการนี้ใช่หรือไม่ ?')" CommandName="Update" />
                                                                                            <asp:LinkButton ID="LinkButton2" runat="server" Text="Cancel" CssClass="btn btn-danger" CommandName="Cancel" />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </EditItemTemplate>

                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="ชื่อ-นามสกุล">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="Labe1" runat="server" Text='<%# Eval("ref_fullname")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="ตำแหน่ง">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="lb3P23ag2e1" runat="server" Text='<%# Eval("ref_position")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="ความสัมพันธ์">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="lb3P2a1g2e1" runat="server" Text='<%# Eval("ref_relation")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="เบอร์โทรศัพท์">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="lb3P2ag2e1" runat="server" Text='<%# Eval("ref_tel")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="จัดการ">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="btnEditView" runat="server" ValidationGroup="fromgroup" Text="Edit" CssClass="btn btn-info" CommandName="Edit" />
                                                                                <asp:LinkButton ID="btnDeleteGV" runat="server" ValidationGroup="fromgroup" OnCommand="btnCommand" CommandName="DeleteGvRefer_View" CommandArgument='<%# Eval("ReIDX") %>' Text="Delete" CssClass="btn btn-danger" OnClientClick="return confirm('Do you want delete this item?')" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                    </Columns>

                                                                </asp:GridView>

                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-sm-12">
                                                                <div class="checkbox">
                                                                    <asp:CheckBox ID="ckreference_edit" runat="server" Text="เพิ่มบุคคลอ้างอิง" CssClass="col-sm-4" AutoPostBack="true" OnCheckedChanged="chkSelectedIndexChanged" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <asp:Panel ID="BoxReference" runat="server" Visible="false">

                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label258" runat="server" Text="Name" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label259" runat="server" Text="ชื่อ-นามสกุล" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-2">
                                                                    <asp:TextBox ID="txtfullname_add" CssClass="form-control" runat="server" ValidationGroup="AddReference"></asp:TextBox>

                                                                    <asp:RequiredFieldValidator ID="Re1ator1" ValidationGroup="AddReference" runat="server" Display="None"
                                                                        ControlToValidate="txtfullname_add" Font-Size="11"
                                                                        ErrorMessage="ชื่อ-นามสกุล ...." />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="Valr8" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re1ator1" Width="160" />
                                                                </div>

                                                                <label class="col-sm-2 control-label col-sm-offset-1">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label260" runat="server" Text="Positions" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label261" runat="server" Text="ตำแหน่ง" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-2">
                                                                    <asp:TextBox ID="txtposition_add" CssClass="form-control" runat="server" ValidationGroup="AddReference"></asp:TextBox>

                                                                    <asp:RequiredFieldValidator ID="Re1or2" ValidationGroup="AddReference" runat="server" Display="None"
                                                                        ControlToValidate="txtposition_add" Font-Size="11"
                                                                        ErrorMessage="ตำแหน่งล่าสุด ...." />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValideatorCalloutExtender9" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re1or2" Width="160" />
                                                                </div>

                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label262" runat="server" Text="Relation" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label263" runat="server" Text="ความสัมพันธ์" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-2">
                                                                    <asp:TextBox ID="txtrelation_add" CssClass="form-control" runat="server" ValidationGroup="AddReference"></asp:TextBox>

                                                                    <asp:RequiredFieldValidator ID="Redawtor3" ValidationGroup="AddReference" runat="server" Display="None"
                                                                        ControlToValidate="txtrelation_add" Font-Size="11"
                                                                        ErrorMessage="ความสัมพันธ์ ...." />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValExtender10" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Redawtor3" Width="160" />
                                                                </div>

                                                                <label class="col-sm-2 control-label col-sm-offset-1">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label264" runat="server" Text="Tel" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label266" runat="server" Text="เบอร์โทรศัพท์" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-2">
                                                                    <asp:TextBox ID="txttel_add" CssClass="form-control" runat="server" ValidationGroup="AddReference"></asp:TextBox>

                                                                    <asp:RequiredFieldValidator ID="Requa4" ValidationGroup="AddReference" runat="server" Display="None"
                                                                        ControlToValidate="txttel_add" Font-Size="11"
                                                                        ErrorMessage="เบอร์โทรศัพท์ ...." />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValtExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requa4" Width="160" />
                                                                </div>
                                                                <div class="col-sm-1">
                                                                    <asp:LinkButton ID="LinkButton7" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="AddReference_Edit" ValidationGroup="AddReference">ADD + &nbsp;</asp:LinkButton>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <div class="col-lg-12">

                                                                    <asp:GridView ID="GvReference_Edit"
                                                                        runat="server"
                                                                        AutoGenerateColumns="false"
                                                                        CssClass="table table-striped table-bordered table-hover"
                                                                        HeaderStyle-CssClass="table_headCenter"
                                                                        HeaderStyle-Height="40px"
                                                                        ShowFooter="False"
                                                                        ShowHeaderWhenEmpty="True"
                                                                        AllowPaging="True"
                                                                        PageSize="5"
                                                                        BorderStyle="None"
                                                                        CellSpacing="2"
                                                                        OnRowDeleting="gvRowDeleting">

                                                                        <PagerStyle CssClass="PageCustom" />
                                                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                                        <EmptyDataTemplate>
                                                                            <div style="text-align: center">Data Cannot Be Found</div>
                                                                        </EmptyDataTemplate>

                                                                        <Columns>

                                                                            <asp:TemplateField HeaderText="#">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <%# (Container.DataItemIndex +1) %>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="ชื่อ-นามสกุล">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <asp:Label ID="Labe1" runat="server" Text='<%# Eval("ref_fullname")%>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="ตำแหน่ง">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <asp:Label ID="lb3P23ag2e1" runat="server" Text='<%# Eval("ref_position")%>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="ความสัมพันธ์">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <asp:Label ID="lb3P2a1g2e1" runat="server" Text='<%# Eval("ref_relation")%>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="เบอร์โทรศัพท์">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <asp:Label ID="lb3P2ag2e1" runat="server" Text='<%# Eval("ref_tel")%>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="จัดการ">
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="btnDeleteGV" runat="server" ValidationGroup="fromgroup" CommandName="Delete" Text="Delete" CssClass="btn btn-danger" OnClientClick="return confirm('Do you want delete this item?')" />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                        </Columns>

                                                                    </asp:GridView>

                                                                </div>
                                                            </div>

                                                        </asp:Panel>

                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>

                                        <%-----------------------------------------------------------------------------------------------%>
                                        <%--Prior Experiences--%>
                                        <%-----------------------------------------------------------------------------------------------%>
                                        <div class="panel panel-red">
                                            <div class="panel-heading">
                                                <h3 class="panel-title"><i class="glyphicon glyphicon-briefcase"></i><strong>&nbsp; Prior Experiences / ประวัติการทำงาน</strong></h3>
                                            </div>
                                            <asp:Panel ID="Panel21" runat="server" CssClass="bg-regis">
                                                <div class="panel-body">
                                                    <div class="form-horizontal" role="form">

                                                        <div class="form-group">
                                                            <div class="col-lg-12">

                                                                <asp:GridView ID="GvPri_View"
                                                                    runat="server"
                                                                    AutoGenerateColumns="false"
                                                                    CssClass="table table-striped table-bordered table-hover"
                                                                    HeaderStyle-CssClass="table_headCenter"
                                                                    HeaderStyle-Height="40px"
                                                                    ShowFooter="False"
                                                                    ShowHeaderWhenEmpty="True"
                                                                    AllowPaging="True"
                                                                    PageSize="10"
                                                                    BorderStyle="None"
                                                                    CellSpacing="2"
                                                                    DataKeyNames="ExperIDX"
                                                                    OnRowEditing="gvRowEditing"
                                                                    OnPageIndexChanging="Master_PageIndexChanging"
                                                                    OnRowCancelingEdit="gvRowCancelingEdit"
                                                                    OnRowDataBound="Master_RowDataBound"
                                                                    OnRowUpdating="gvRowUpdating">

                                                                    <PagerStyle CssClass="PageCustom" />
                                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                                    <EmptyDataTemplate>
                                                                        <div style="text-align: center">Data Cannot Be Found</div>
                                                                    </EmptyDataTemplate>

                                                                    <Columns>

                                                                        <asp:TemplateField HeaderText="#">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <%# (Container.DataItemIndex +1) %>
                                                                                    <asp:Label ID="L2ab2del5w8" runat="server" Visible="false" Text='<%# Eval("ExperIDX")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>

                                                                            <EditItemTemplate>
                                                                                <asp:Label ID="sf" runat="server"></asp:Label>
                                                                                <div class="row">
                                                                                    <div class="col-lg-10">
                                                                                        <h3>Experiences FormEdit<small> (แบบฟอร์มแก้ไขข้อมูลประสบการณ์ทำงาน)</small></h3>
                                                                                        <asp:Label ID="lbl_Pri_edit" runat="server" Visible="false" Text='<%# Eval("ExperIDX")%>'></asp:Label>
                                                                                    </div>
                                                                                </div>
                                                                                <hr />

                                                                                <div class="form-group">
                                                                                    <asp:Label ID="Label65" runat="server" Text="ชื่อบริษัท" CssClass="col-sm-3 control-label"></asp:Label>
                                                                                    <div class="col-sm-4">
                                                                                        <asp:TextBox ID="txtPri_Nameorg_edit" runat="server" CssClass="form-control" Text='<%# Eval("Pri_Nameorg") %>'></asp:TextBox>
                                                                                    </div>
                                                                                </div>


                                                                                <div class="form-group">
                                                                                    <asp:Label ID="Label66" runat="server" Text="ตำแหน่งล่าสุด" CssClass="col-sm-3 control-label"></asp:Label>
                                                                                    <div class="col-sm-4">
                                                                                        <asp:TextBox ID="txtPri_pos_edit" runat="server" CssClass="form-control" Text='<%# Eval("Pri_pos") %>'></asp:TextBox>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group">
                                                                                    <asp:Label ID="Label160" runat="server" Text="วันที่เริ่มงาน" CssClass="col-sm-3 control-label"></asp:Label>
                                                                                    <div class="col-sm-4">
                                                                                        <div class='input-group date'>
                                                                                            <asp:TextBox ID="txt_Pri_start_edit" runat="server" CssClass="form-control from-date-datepicker" Text='<%# Eval("Pri_startdate") %>'></asp:TextBox>
                                                                                            <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group">
                                                                                    <asp:Label ID="Label12" runat="server" Text="วันที่ออกจากงาน" CssClass="col-sm-3 control-label"></asp:Label>
                                                                                    <div class="col-sm-4">
                                                                                        <div class='input-group date'>
                                                                                            <asp:TextBox ID="txt_Pri_resign_edit" runat="server" CssClass="form-control from-date-datepicker" Text='<%# Eval("Pri_resigndate") %>'></asp:TextBox>
                                                                                            <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>


                                                                                <div class="form-group">
                                                                                    <div class="col-lg-offset-10">
                                                                                        <div class="control-label" style="text-align: center;">
                                                                                            <asp:LinkButton ID="LinkButton1" runat="server" Text="Update" ValidationGroup="SaveUpdate" CssClass="btn btn-success" OnClientClick="return confirm('คุณต้องการแก้ไขและบันทึกรายการนี้ใช่หรือไม่ ?')" CommandName="Update" />
                                                                                            <asp:LinkButton ID="LinkButton2" runat="server" Text="Cancel" CssClass="btn btn-danger" CommandName="Cancel" />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </EditItemTemplate>

                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="ชื่อบริษัท">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="L2ab2el5w8" runat="server" Text='<%# Eval("Pri_Nameorg")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="ตำแหน่งล่าสุด">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="l2bP42ag2e1" runat="server" Text='<%# Eval("Pri_pos")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="วันที่เริ่มงาน">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="lb3wP2a1g2e1" runat="server" Text='<%# Eval("Pri_startdate")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="วันที่ออกจากงาน">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="lebP32ag2e1" runat="server" Text='<%# Eval("Pri_resigndate")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="เงินเดือนล่าสุด">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="lebP32ag2e1" runat="server" Text='<%# Eval("Pri_salary")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="รายละเอียดงาน">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="lebP32ag2e1" runat="server" Text='<%# Eval("Pri_description")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="จัดการ">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="btnEditView" runat="server" ValidationGroup="fromgroup" Text="Edit" CssClass="btn btn-info" CommandName="Edit" />
                                                                                <asp:LinkButton ID="btnDeleteGV" runat="server" ValidationGroup="fromgroup" OnCommand="btnCommand" CommandName="DeleteGvPri_View" CommandArgument='<%# Eval("ExperIDX") %>' Text="Delete" CssClass="btn btn-danger" OnClientClick="return confirm('Do you want delete this item?')" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                    </Columns>

                                                                </asp:GridView>

                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-sm-12">
                                                                <div class="checkbox">
                                                                    <asp:CheckBox ID="chkorg_edit" runat="server" Text="เพิ่มสถานที่ทำงาน" CssClass="col-sm-4" AutoPostBack="true" OnCheckedChanged="chkSelectedIndexChanged" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <asp:Panel ID="BoxOrgOld" runat="server" Visible="false">
                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label159" runat="server" Text="Organization Name" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="lblorgold" runat="server" Text="ชื่อบริษัท" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-2">
                                                                    <asp:TextBox ID="txtorgold_edit" CssClass="form-control" runat="server" ValidationGroup="AddPrior_Edit"></asp:TextBox>

                                                                    <asp:RequiredFieldValidator ID="R23equiredFieldValidator1" ValidationGroup="AddPrior_Edit" runat="server" Display="None"
                                                                        ControlToValidate="txtorgold_edit" Font-Size="11"
                                                                        ErrorMessage="ชื่อบริษัท ...." />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidsdatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R23equiredFieldValidator1" Width="160" />
                                                                </div>

                                                                <label class="col-sm-2 control-label col-sm-offset-1">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label161" runat="server" Text="Recent Positions" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label162" runat="server" Text="ตำแหน่งล่าสุด" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-2">
                                                                    <asp:TextBox ID="txtposold_edit" CssClass="form-control" runat="server" ValidationGroup="AddPrior_Edit"></asp:TextBox>

                                                                    <asp:RequiredFieldValidator ID="RdValidwator1" ValidationGroup="AddPrior_Edit" runat="server" Display="None"
                                                                        ControlToValidate="txtposold_edit" Font-Size="11"
                                                                        ErrorMessage="ตำแหน่งล่าสุด ...." />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="Va6lidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RdValidwator1" Width="160" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label253" runat="server" Text="Start Date" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label254" runat="server" Text="ปีที่เริ่มงาน" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-2">
                                                                    <div class='input-group date'>
                                                                        <asp:TextBox ID="txtstartdate_edit" CssClass="form-control from-date-datepicker" MaxLengh="100%" runat="server" ValidationGroup="AddPrior_Edit"></asp:TextBox>
                                                                        <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                    </div>

                                                                    <asp:RequiredFieldValidator ID="Requir4er1" ValidationGroup="AddPrior_Edit" runat="server" Display="None"
                                                                        ControlToValidate="txtstartdate_edit" Font-Size="11"
                                                                        ErrorMessage="ปีที่เริ่มงาน ...." />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="Validator2CalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requir4er1" Width="160" />
                                                                </div>

                                                                <label class="col-sm-2 control-label col-sm-offset-1">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label255" runat="server" Text="End Date" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label256" runat="server" Text="ปีที่ออกจากงาน" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-2">
                                                                    <div class='input-group date'>
                                                                        <asp:TextBox ID="txtresigndate_edit" CssClass="form-control from-date-datepicker" MaxLengh="100%" runat="server" ValidationGroup="AddPrior_Edit"></asp:TextBox>
                                                                        <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                    </div>

                                                                    <asp:RequiredFieldValidator ID="RdV2alidator2" ValidationGroup="AddPrior_Edit" runat="server" Display="None"
                                                                        ControlToValidate="txtresigndate_edit" Font-Size="11"
                                                                        ErrorMessage="ปีที่ออกจากงาน ...." />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RdV2alidator2" Width="160" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label47" runat="server" Text="salary" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label49" runat="server" Text="เงินเดือนล่าสุด" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-2">
                                                                    <asp:TextBox ID="txtprisalary_edit" CssClass="form-control" runat="server" ValidationGroup="AddPrior_Edit"></asp:TextBox>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="Reqor1" ValidationGroup="AddPrior_Edit" runat="server" Display="None"
                                                                    ControlToValidate="txtprisalary_edit" Font-Size="11"
                                                                    ErrorMessage="เงินเดือนล่าสุด ...." />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="Vaaer1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Reqor1" Width="160" />

                                                                <label class="col-sm-2 control-label col-sm-offset-1">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label42" runat="server" Text="Description" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label43" runat="server" Text="รายละเอียดงาน" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-2">
                                                                    <asp:TextBox ID="txtpridescription_edit" CssClass="form-control" runat="server" TextMode="MultiLine" Rows="3" ValidationGroup="AddPrior_Edit"></asp:TextBox>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="Requirator1" ValidationGroup="AddPrior_Edit" runat="server" Display="None"
                                                                    ControlToValidate="txtpridescription_edit" Font-Size="11"
                                                                    ErrorMessage="รายละเอียดงาน ...." />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="Valier1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requirator1" Width="160" />

                                                                <div class="col-sm-1">
                                                                    <asp:LinkButton ID="LinkButton6" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="AddPrior_Edit" ValidationGroup="AddPrior_Edit">ADD + &nbsp;</asp:LinkButton>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <div class="col-lg-12">

                                                                    <asp:GridView ID="GvPri_Edit"
                                                                        runat="server"
                                                                        AutoGenerateColumns="false"
                                                                        CssClass="table table-striped table-bordered table-hover"
                                                                        HeaderStyle-CssClass="table_headCenter"
                                                                        HeaderStyle-Height="40px"
                                                                        ShowFooter="False"
                                                                        ShowHeaderWhenEmpty="True"
                                                                        AllowPaging="True"
                                                                        PageSize="10"
                                                                        BorderStyle="None"
                                                                        CellSpacing="2"
                                                                        OnRowDeleting="gvRowDeleting">

                                                                        <PagerStyle CssClass="PageCustom" />
                                                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                                        <EmptyDataTemplate>
                                                                            <div style="text-align: center">Data Cannot Be Found</div>
                                                                        </EmptyDataTemplate>

                                                                        <Columns>

                                                                            <asp:TemplateField HeaderText="#">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <%# (Container.DataItemIndex +1) %>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="ชื่อบริษัท">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <asp:Label ID="Label5w8" runat="server" Text='<%# Eval("Pri_Nameorg")%>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="ตำแหน่งล่าสุด">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <asp:Label ID="lbP2ag2e1" runat="server" Text='<%# Eval("Pri_pos")%>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="วันที่เริ่มงาน">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <asp:Label ID="lb3P2a1g2e1" runat="server" Text='<%# Eval("Pri_startdate")%>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="วันที่ออกจากงาน">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <asp:Label ID="lbP32ag2e1" runat="server" Text='<%# Eval("Pri_resigndate")%>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="เงินเดือนล่าสุด">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <asp:Label ID="lebP3223ag2e1" runat="server" Text='<%# Eval("Pri_salary")%>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="รายละเอียดงาน">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <asp:Label ID="leb1P32ag2e1" runat="server" Text='<%# Eval("Pri_description")%>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="จัดการ">
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="btnDeleteGV" runat="server" ValidationGroup="fromgroup" CommandName="Delete" Text="Delete" CssClass="btn btn-danger" OnClientClick="return confirm('Do you want delete this item?')" />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                        </Columns>

                                                                    </asp:GridView>

                                                                </div>
                                                            </div>

                                                        </asp:Panel>

                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>


                                        <%-----------------------------------------------------------------------------------------------%>
                                        <%--Education--%>
                                        <%-----------------------------------------------------------------------------------------------%>
                                        <div class="panel panel-red">
                                            <div class="panel-heading">
                                                <h3 class="panel-title"><i class="glyphicon glyphicon-education"></i><strong>&nbsp; Education / ประวัติการศึกษา</strong></h3>
                                            </div>
                                            <asp:Panel ID="Panel22" runat="server" CssClass="bg-regis">
                                                <div class="panel-body">
                                                    <div class="form-horizontal" role="form">

                                                        <div class="form-group">
                                                            <div class="col-lg-12">

                                                                <asp:GridView ID="GvEducation_View"
                                                                    runat="server"
                                                                    AutoGenerateColumns="false"
                                                                    CssClass="table table-striped table-bordered table-hover"
                                                                    HeaderStyle-CssClass="table_headCenter"
                                                                    HeaderStyle-Height="40px"
                                                                    ShowFooter="False"
                                                                    ShowHeaderWhenEmpty="True"
                                                                    AllowPaging="True"
                                                                    PageSize="10"
                                                                    BorderStyle="None"
                                                                    CellSpacing="2"
                                                                    DataKeyNames="EDUIDX"
                                                                    OnRowEditing="gvRowEditing"
                                                                    OnPageIndexChanging="Master_PageIndexChanging"
                                                                    OnRowCancelingEdit="gvRowCancelingEdit"
                                                                    OnRowDataBound="Master_RowDataBound"
                                                                    OnRowUpdating="gvRowUpdating">

                                                                    <PagerStyle CssClass="PageCustom" />
                                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                                    <EmptyDataTemplate>
                                                                        <div style="text-align: center">Data Cannot Be Found</div>
                                                                    </EmptyDataTemplate>

                                                                    <Columns>

                                                                        <asp:TemplateField HeaderText="#">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <%# (Container.DataItemIndex +1) %>
                                                                                </div>
                                                                            </ItemTemplate>

                                                                            <EditItemTemplate>
                                                                                <asp:Label ID="sf" runat="server"></asp:Label>
                                                                                <div class="row">
                                                                                    <div class="col-lg-10">
                                                                                        <h3>Education FormEdit<small> (แบบฟอร์มแก้ไขข้อมูลประวัติการศึกษา)</small></h3>
                                                                                        <asp:Label ID="lbl_Edu_edit" runat="server" Visible="false" Text='<%# Eval("EDUIDX")%>'></asp:Label>
                                                                                    </div>
                                                                                </div>
                                                                                <hr />

                                                                                <div class="form-group">
                                                                                    <asp:Label ID="Label65" runat="server" Text="วุฒิการศึกษา" CssClass="col-sm-3 control-label"></asp:Label>
                                                                                    <div class="col-sm-4">
                                                                                        <asp:Label ID="lbl_Edu_qualification_ID" runat="server" Visible="false" Text='<%# Eval("Edu_qualification_ID")%>'></asp:Label>
                                                                                        <asp:DropDownList ID="ddleducationback" runat="server" CssClass="form-control">
                                                                                            <asp:ListItem Value="0">เลือกวุฒิการศึกษา....</asp:ListItem>
                                                                                            <asp:ListItem Value="7">ไม่มีวุฒิการศึกษา</asp:ListItem>
																							<asp:ListItem Value="1">ประถมศึกษา ป.6</asp:ListItem>
																							<asp:ListItem Value="2">มัธยมศึกษาตอนต้น ม.3</asp:ListItem>
																							<asp:ListItem Value="3">มัธยมศึกษาตอนปลาย ม.6</asp:ListItem>
																							<asp:ListItem Value="8">อาชีวศึกษา ปวช</asp:ListItem>
																							<asp:ListItem Value="9">อาชีวศึกษา ปวส</asp:ListItem>
																							<asp:ListItem Value="4">ปริญญาตรี</asp:ListItem>
																							<asp:ListItem Value="5">ปริญญาโท</asp:ListItem>
																							<asp:ListItem Value="6">ปริญญาเอก</asp:ListItem>
																							<asp:ListItem Value="10">กศน.มัธยม ต้น</asp:ListItem>
																							<asp:ListItem Value="11">กศน.มัธยม ปลาย</asp:ListItem>
                                                                                        </asp:DropDownList>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group">
                                                                                    <asp:Label ID="Label66" runat="server" Text="ชื่อสถานศึกษา" CssClass="col-sm-3 control-label"></asp:Label>
                                                                                    <div class="col-sm-4">
                                                                                        <asp:TextBox ID="txtEdu_name_edit" runat="server" CssClass="form-control" Text='<%# Eval("Edu_name") %>'></asp:TextBox>
                                                                                    </div>
                                                                                </div>


                                                                                <div class="form-group">
                                                                                    <asp:Label ID="Label67" runat="server" Text="สาขาวิชาที่ศึกษา" CssClass="col-sm-3 control-label"></asp:Label>
                                                                                    <div class="col-sm-4">
                                                                                        <asp:TextBox ID="txtEdu_branch_edit" runat="server" CssClass="form-control" Text='<%# Eval("Edu_branch") %>'></asp:TextBox>
                                                                                    </div>
                                                                                </div>


                                                                                <div class="form-group">
                                                                                    <asp:Label ID="Label13" runat="server" Text="ปีที่เริ่มศึกษา" CssClass="col-sm-3 control-label"></asp:Label>
                                                                                    <div class="col-sm-4">
                                                                                        <div class='input-group date'>
                                                                                            <asp:TextBox ID="txtEdu_start_edit" runat="server" CssClass="form-control from-date-datepicker" Text='<%# Eval("Edu_start") %>'></asp:TextBox>
                                                                                            <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>


                                                                                <div class="form-group">
                                                                                    <asp:Label ID="Label9" runat="server" Text="ปีที่จบศึกษา" CssClass="col-sm-3 control-label"></asp:Label>
                                                                                    <div class="col-sm-4">
                                                                                        <div class='input-group date'>
                                                                                            <asp:TextBox ID="txtEdu_end_edit" runat="server" CssClass="form-control from-date-datepicker" Text='<%# Eval("Edu_end") %>'></asp:TextBox>
                                                                                            <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>


                                                                                <div class="form-group">
                                                                                    <div class="col-lg-offset-10">
                                                                                        <div class="control-label" style="text-align: center;">
                                                                                            <asp:LinkButton ID="LinkButton1" runat="server" Text="Update" ValidationGroup="SaveUpdate" CssClass="btn btn-success" OnClientClick="return confirm('คุณต้องการแก้ไขและบันทึกรายการนี้ใช่หรือไม่ ?')" CommandName="Update" />
                                                                                            <asp:LinkButton ID="LinkButton2" runat="server" Text="Cancel" CssClass="btn btn-danger" CommandName="Cancel" />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </EditItemTemplate>

                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="วุฒิการศึกษา">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="La4bel5w8" runat="server" Text='<%# Eval("qualification_name")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="ชื่อสถานศึกษา">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="l4bP24ag2e1" runat="server" Text='<%# Eval("Edu_name")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="สาขาวิชาที่ศึกษา">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="l4b4Pag2e1" runat="server" Text='<%# Eval("Edu_branch")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="ปีที่จบศึกษา">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="l4bP3ag2e1" runat="server" Text='<%# Eval("Edu_end")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="จัดการ">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="btnEditView" runat="server" ValidationGroup="fromgroup" Text="Edit" CssClass="btn btn-info" CommandName="Edit" />
                                                                                <asp:LinkButton ID="btnDeleteGV" runat="server" ValidationGroup="fromgroup" OnCommand="btnCommand" CommandName="DeleteGvEducation_View" CommandArgument='<%# Eval("EDUIDX") %>' Text="Delete" CssClass="btn btn-danger" OnClientClick="return confirm('Do you want delete this item?')" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                    </Columns>

                                                                </asp:GridView>

                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-sm-12">
                                                                <div class="checkbox">
                                                                    <asp:CheckBox ID="ckeducation_edit" runat="server" Text="เพิ่มประวัติการศึกษา" CssClass="col-sm-4" AutoPostBack="true" OnCheckedChanged="chkSelectedIndexChanged" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <asp:Panel ID="BoxEducation" runat="server" Visible="false">
                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label ">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label176" runat="server" Text="Educational" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label177" runat="server" Text="วุฒิการศึกษา" /></b></small>
                                                                    </h2>
                                                                </label>

                                                                <div class="col-sm-2">
                                                                    <asp:DropDownList ID="ddleducationback_edit" runat="server" CssClass="form-control">
                                                                        <asp:ListItem Value="0">เลือกวุฒิการศึกษา....</asp:ListItem>
                                                                        <asp:ListItem Value="7">ไม่มีวุฒิการศึกษา</asp:ListItem>
																		<asp:ListItem Value="1">ประถมศึกษา ป.6</asp:ListItem>
																		<asp:ListItem Value="2">มัธยมศึกษาตอนต้น ม.3</asp:ListItem>
																		<asp:ListItem Value="3">มัธยมศึกษาตอนปลาย ม.6</asp:ListItem>
																		<asp:ListItem Value="8">อาชีวศึกษา ปวช</asp:ListItem>
																		<asp:ListItem Value="9">อาชีวศึกษา ปวส</asp:ListItem>
																		<asp:ListItem Value="4">ปริญญาตรี</asp:ListItem>
																		<asp:ListItem Value="5">ปริญญาโท</asp:ListItem>
																		<asp:ListItem Value="6">ปริญญาเอก</asp:ListItem>
																		<asp:ListItem Value="10">กศน.มัธยม ต้น</asp:ListItem>
																		<asp:ListItem Value="11">กศน.มัธยม ปลาย</asp:ListItem>
                                                                    </asp:DropDownList>

                                                                    <asp:RequiredFieldValidator ID="Require6dFieldValidator1" ValidationGroup="AddEducation_Edit" runat="server" Display="None"
                                                                        ControlToValidate="ddleducationback_edit" Font-Size="11"
                                                                        ErrorMessage="วุฒิการศึกษา ...."
                                                                        ValidationExpression="วุฒิการศึกษา ...." InitialValue="0" />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="VaqlidatorCal32loutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Require6dFieldValidator1" Width="160" />
                                                                </div>

                                                                <label class="col-sm-2 control-label col-sm-offset-1">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label168" runat="server" Text="School Name" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label169" runat="server" Text="ชื่อสถานศึกษา" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-2">
                                                                    <asp:TextBox ID="txtschoolname_edit" CssClass="form-control" runat="server" ValidationGroup="AddEducation_Edit"></asp:TextBox>

                                                                    <asp:RequiredFieldValidator ID="ReqwuiredFieldVali4dator1" ValidationGroup="AddEducation_Edit" runat="server" Display="None"
                                                                        ControlToValidate="txtschoolname_edit" Font-Size="11"
                                                                        ErrorMessage="วุฒิการศึกษา ...." />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCal32loutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqwuiredFieldVali4dator1" Width="160" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label172" runat="server" Text="Start Date" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label173" runat="server" Text="ปีที่เริ่มศึกษา" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-2">
                                                                    <div class='input-group date'>
                                                                        <asp:TextBox ID="txtstarteducation_edit" CssClass="form-control from-date-datepicker" MaxLengh="100%" runat="server" ValidationGroup="AddEducation_Edit"></asp:TextBox>
                                                                        <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                    </div>

                                                                    <asp:RequiredFieldValidator ID="RequiredFiel89dValidator1" ValidationGroup="AddEducation_Edit" runat="server" Display="None"
                                                                        ControlToValidate="txtstarteducation_edit" Font-Size="11"
                                                                        ErrorMessage="ปีที่เริ่มศึกษา ...." />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="Validat59orCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFiel89dValidator1" Width="160" />
                                                                </div>

                                                                <label class="col-sm-2 control-label col-sm-offset-1">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label174" runat="server" Text="End Date" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label175" runat="server" Text="ปีที่จบการศึกษา" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-2">
                                                                    <div class='input-group date'>
                                                                        <asp:TextBox ID="txtendeducation_edit" CssClass="form-control from-date-datepicker" MaxLengh="100%" runat="server" ValidationGroup="AddEducation_Edit"></asp:TextBox>
                                                                        <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                    </div>
                                                                </div>

                                                                <asp:RequiredFieldValidator ID="Req63uiredFieldValidator1" ValidationGroup="AddEducation_Edit" runat="server" Display="None"
                                                                    ControlToValidate="txtendeducation_edit" Font-Size="11"
                                                                    ErrorMessage="ปีที่จบการศึกษา ...." />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="V32alidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req63uiredFieldValidator1" Width="160" />
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label170" runat="server" Text="Branch" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label171" runat="server" Text="สาขาวิชาที่ศึกษา" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-2">
                                                                    <asp:TextBox ID="txtstudy_edit" CssClass="form-control" runat="server" ValidationGroup="AddEducation_Edit"></asp:TextBox>

                                                                    <asp:RequiredFieldValidator ID="Re57quiredFieldValidator1" ValidationGroup="AddEducation_Edit" runat="server" Display="None"
                                                                        ControlToValidate="txtstudy_edit" Font-Size="11"
                                                                        ErrorMessage="สาขาวิชาที่ศึกษา ...." />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="Validator432CalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re57quiredFieldValidator1" Width="160" />
                                                                </div>

                                                                <div class="col-sm-1 col-sm-offset-2">
                                                                    <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="AddEducation_Edit" ValidationGroup="AddEducation_Edit">ADD + &nbsp;</asp:LinkButton>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <div class="col-lg-12">

                                                                    <asp:GridView ID="GvEducation_Edit"
                                                                        runat="server"
                                                                        AutoGenerateColumns="false"
                                                                        CssClass="table table-striped table-bordered table-hover"
                                                                        HeaderStyle-CssClass="table_headCenter"
                                                                        HeaderStyle-Height="40px"
                                                                        ShowFooter="False"
                                                                        ShowHeaderWhenEmpty="True"
                                                                        AllowPaging="True"
                                                                        PageSize="10"
                                                                        BorderStyle="None"
                                                                        CellSpacing="2"
                                                                        OnRowDeleting="gvRowDeleting">

                                                                        <PagerStyle CssClass="PageCustom" />
                                                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                                        <EmptyDataTemplate>
                                                                            <div style="text-align: center">Data Cannot Be Found</div>
                                                                        </EmptyDataTemplate>

                                                                        <Columns>

                                                                            <asp:TemplateField HeaderText="#">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <%# (Container.DataItemIndex +1) %>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="วุฒิการศึกษา">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <asp:Label ID="Label5w8" runat="server" Text='<%# Eval("Edu_qualification")%>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="ชื่อสถานศึกษา">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <asp:Label ID="lbP24ag2e1" runat="server" Text='<%# Eval("Edu_name")%>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="สาขาวิชาที่ศึกษา">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <asp:Label ID="lb4Pag2e1" runat="server" Text='<%# Eval("Edu_branch")%>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="ปีที่เริ่มศึกษา">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <asp:Label ID="lbP2ag2e1" runat="server" Text='<%# Eval("Edu_start")%>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="ปีที่จบศึกษา">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <asp:Label ID="lbP3ag2e1" runat="server" Text='<%# Eval("Edu_end")%>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="จัดการ">
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="btnDeleteGV" runat="server" ValidationGroup="fromgroup" CommandName="Delete" Text="Delete" CssClass="btn btn-danger" OnClientClick="return confirm('Do you want delete this item?')" />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                        </Columns>

                                                                    </asp:GridView>

                                                                </div>
                                                            </div>

                                                        </asp:Panel>

                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>


                                        <%-----------------------------------------------------------------------------------------------%>
                                        <%--Training History--%>
                                        <%-----------------------------------------------------------------------------------------------%>
                                        <div class="panel panel-red">
                                            <div class="panel-heading">
                                                <h3 class="panel-title"><i class="glyphicon glyphicon-retweet"></i><strong>&nbsp; Training History / ประวัติการฝึกอบรม</strong></h3>
                                            </div>
                                            <asp:Panel ID="Panel23" runat="server" CssClass="bg-regis">
                                                <div class="panel-body">
                                                    <div class="form-horizontal" role="form">

                                                        <div class="form-group">
                                                            <div class="col-lg-12">

                                                                <asp:GridView ID="GvTrain_View"
                                                                    runat="server"
                                                                    AutoGenerateColumns="false"
                                                                    CssClass="table table-striped table-bordered table-hover"
                                                                    HeaderStyle-CssClass="table_headCenter"
                                                                    HeaderStyle-Height="40px"
                                                                    ShowFooter="False"
                                                                    ShowHeaderWhenEmpty="True"
                                                                    AllowPaging="True"
                                                                    PageSize="10"
                                                                    BorderStyle="None"
                                                                    CellSpacing="2"
                                                                    DataKeyNames="TNIDX"
                                                                    OnRowEditing="gvRowEditing"
                                                                    OnPageIndexChanging="Master_PageIndexChanging"
                                                                    OnRowCancelingEdit="gvRowCancelingEdit"
                                                                    OnRowDataBound="Master_RowDataBound"
                                                                    OnRowUpdating="gvRowUpdating">


                                                                    <PagerStyle CssClass="PageCustom" />
                                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                                    <EmptyDataTemplate>
                                                                        <div style="text-align: center">Data Cannot Be Found</div>
                                                                    </EmptyDataTemplate>

                                                                    <Columns>

                                                                        <asp:TemplateField HeaderText="#">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <%# (Container.DataItemIndex +1) %>
                                                                                </div>
                                                                            </ItemTemplate>

                                                                            <EditItemTemplate>
                                                                                <asp:Label ID="sf" runat="server"></asp:Label>
                                                                                <div class="row">
                                                                                    <div class="col-lg-10">
                                                                                        <h3>Training FormEdit<small> (แบบฟอร์มแก้ไขข้อมูลประวัติการอบรม)</small></h3>
                                                                                        <asp:Label ID="lbl_Tn_edit" runat="server" Visible="false" Text='<%# Eval("TNIDX")%>'></asp:Label>
                                                                                    </div>
                                                                                </div>
                                                                                <hr />

                                                                                <div class="form-group">
                                                                                    <asp:Label ID="Label265" runat="server" Text="หลักสูตรอาบรม" CssClass="col-sm-3 control-label"></asp:Label>
                                                                                    <div class="col-sm-4">
                                                                                        <asp:TextBox ID="txtTrain_courses_edit" runat="server" CssClass="form-control" Text='<%# Eval("Train_courses") %>'></asp:TextBox>
                                                                                    </div>
                                                                                </div>


                                                                                <div class="form-group">
                                                                                    <asp:Label ID="Label66" runat="server" Text="วันที่อบรม" CssClass="col-sm-3 control-label"></asp:Label>
                                                                                    <div class="col-sm-4">
                                                                                        <div class='input-group date'>
                                                                                            <asp:TextBox ID="txtTrain_date_edit" runat="server" CssClass="form-control from-date-datepicker" Text='<%# Eval("Train_date") %>'></asp:TextBox>
                                                                                            <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>


                                                                                <div class="form-group">
                                                                                    <asp:Label ID="Label67" runat="server" Text="ผลการประเมิน" CssClass="col-sm-3 control-label"></asp:Label>
                                                                                    <div class="col-sm-4">
                                                                                        <asp:TextBox ID="txtTrain_assessment_edit" runat="server" CssClass="form-control" Text='<%# Eval("Train_assessment") %>'></asp:TextBox>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group">
                                                                                    <div class="col-lg-offset-10">
                                                                                        <div class="control-label" style="text-align: center;">
                                                                                            <asp:LinkButton ID="LinkButton1" runat="server" Text="Update" ValidationGroup="SaveUpdate" CssClass="btn btn-success" OnClientClick="return confirm('คุณต้องการแก้ไขและบันทึกรายการนี้ใช่หรือไม่ ?')" CommandName="Update" />
                                                                                            <asp:LinkButton ID="LinkButton2" runat="server" Text="Cancel" CssClass="btn btn-danger" CommandName="Cancel" />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </EditItemTemplate>

                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="หลักสูตรอาบรม">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="Labewl5w8" runat="server" Text='<%# Eval("Train_courses")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="วันที่อบรม">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="lbP2aw2g2e1" runat="server" Text='<%# Eval("Train_date")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="ผลการประเมิน">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="lbPagw2e1" runat="server" Text='<%# Eval("Train_assessment")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="จัดการ">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="btnEditView" runat="server" ValidationGroup="fromgroup" Text="Edit" CssClass="btn btn-info" CommandName="Edit" />
                                                                                <asp:LinkButton ID="btnDeleteGV" runat="server" ValidationGroup="fromgroup" OnCommand="btnCommand" CommandName="DeleteGvTrain_View" CommandArgument='<%# Eval("TNIDX") %>' Text="Delete" CssClass="btn btn-danger" OnClientClick="return confirm('Do you want delete this item?')" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                    </Columns>

                                                                </asp:GridView>

                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-sm-12">
                                                                <div class="checkbox">
                                                                    <asp:CheckBox ID="cktraining_edit" runat="server" Text="เพิ่มประวัติฝึกอบรม" CssClass="col-sm-4" AutoPostBack="true" OnCheckedChanged="chkSelectedIndexChanged" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <asp:Panel ID="BoxTrain" runat="server" Visible="false">
                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label178" runat="server" Text="Training Courses" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label179" runat="server" Text="หลักสูตรฝึกอบรม" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-2">
                                                                    <asp:TextBox ID="txttraincourses_edit" CssClass="form-control" runat="server" ValidationGroup="Addtraining_Edit"></asp:TextBox>

                                                                    <asp:RequiredFieldValidator ID="RequiredFi486eldValidator1" ValidationGroup="Addtraining_Edit" runat="server" Display="None"
                                                                        ControlToValidate="txttraincourses_edit" Font-Size="11"
                                                                        ErrorMessage="หลักสูตรฝึกอบรม ...." />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorC432alloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFi486eldValidator1" Width="160" />
                                                                </div>

                                                                <label class="col-sm-2 control-label col-sm-offset-1">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label180" runat="server" Text="Training Date" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label181" runat="server" Text="วันที่ฝึกอบรม" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-2">
                                                                    <div class='input-group date'>
                                                                        <asp:TextBox ID="txttraindate_edit" CssClass="form-control from-date-datepicker" MaxLengh="100%" runat="server" ValidationGroup="Addtraining_Edit"></asp:TextBox>
                                                                        <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                    </div>

                                                                    <asp:RequiredFieldValidator ID="Require50dFieldValidator1" ValidationGroup="Addtraining_Edit" runat="server" Display="None"
                                                                        ControlToValidate="txttraindate_edit" Font-Size="11"
                                                                        ErrorMessage="วันที่ฝึกอบรม ...." />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="Validat44orCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Require50dFieldValidator1" Width="160" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label182" runat="server" Text="Assessment" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label183" runat="server" Text="ผลการประเมิน" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-2">
                                                                    <asp:TextBox ID="txttrainassessment_edit" CssClass="form-control" runat="server" ValidationGroup="Addtraining_Edit"></asp:TextBox>

                                                                    <asp:RequiredFieldValidator ID="Re41quiredFieldValidator1" ValidationGroup="Addtraining_Edit" runat="server" Display="None"
                                                                        ControlToValidate="txttrainassessment_edit" Font-Size="11"
                                                                        ErrorMessage="ผลการประเมิน ...." />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExt53ender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re41quiredFieldValidator1" Width="160" />
                                                                </div>

                                                                <div class="col-sm-1 col-sm-offset-2">
                                                                    <asp:LinkButton ID="LinkButton3" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="Addtraining_Edit" ValidationGroup="Addtraining_Edit">ADD + &nbsp;</asp:LinkButton>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <div class="col-lg-12">

                                                                    <asp:GridView ID="GvTrain_Edit"
                                                                        runat="server"
                                                                        AutoGenerateColumns="false"
                                                                        CssClass="table table-striped table-bordered table-hover"
                                                                        HeaderStyle-CssClass="table_headCenter"
                                                                        HeaderStyle-Height="40px"
                                                                        ShowFooter="False"
                                                                        ShowHeaderWhenEmpty="True"
                                                                        AllowPaging="True"
                                                                        PageSize="10"
                                                                        BorderStyle="None"
                                                                        CellSpacing="2"
                                                                        OnRowDeleting="gvRowDeleting">

                                                                        <PagerStyle CssClass="PageCustom" />
                                                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                                        <EmptyDataTemplate>
                                                                            <div style="text-align: center">Data Cannot Be Found</div>
                                                                        </EmptyDataTemplate>

                                                                        <Columns>

                                                                            <asp:TemplateField HeaderText="#">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <%# (Container.DataItemIndex +1) %>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="หลักสูตรอาบรม">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <asp:Label ID="Label5w8" runat="server" Text='<%# Eval("Train_courses")%>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="วันที่อบรม">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <asp:Label ID="lbP2a2g2e1" runat="server" Text='<%# Eval("Train_date")%>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="ผลการประเมิน">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <asp:Label ID="lbPag2e1" runat="server" Text='<%# Eval("Train_assessment")%>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="จัดการ">
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="btnDeleteGV" runat="server" ValidationGroup="fromgroup" CommandName="Delete" Text="Delete" CssClass="btn btn-danger" OnClientClick="return confirm('Do you want delete this item?')" />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                        </Columns>

                                                                    </asp:GridView>

                                                                </div>
                                                            </div>

                                                        </asp:Panel>

                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>

                                        <div class="panel panel-red">
                                            <div class="panel-heading">
                                                <h3 class="panel-title"><i class="glyphicon glyphicon-home"></i><strong>&nbsp; Address & Contact (Present) / ที่อยู่และการติดต่อ(ปัจจุบัน)</strong></h3>
                                            </div>
                                            <asp:Panel ID="BoxAddress" runat="server" CssClass="bg-regis">
                                                <div class="panel-body">
                                                    <div class="form-horizontal" role="form">


                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label63" runat="server" Text="PresentAddress" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label66" runat="server" Text="ที่อยู่ปัจจุบัน" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-4">
                                                                <asp:TextBox ID="txtaddress_present" autocomplete="off" MaxLength="250" TextMode="MultiLine" Rows="3" CssClass="form-control" runat="server" Text='<%# Eval("emp_address") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label68" runat="server" Text="Country" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label69" runat="server" Text="ประเทศ" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:Label ID="lbl_country_edit" runat="server" Visible="false" Text='<%# Eval("country_idx") %>'></asp:Label>
                                                                <asp:DropDownList ID="ddlcountry_present_edit" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0">เลือกประเทศ....</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Lab3el15" runat="server" Text="Province" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label416" runat="server" Text="จังหวัด" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:Label ID="lbl_prov_edit" runat="server" Visible="false" Text='<%# Eval("prov_idx") %>'></asp:Label>
                                                                <asp:DropDownList ID="ddlprovince_present_edit" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                    <asp:ListItem Value="0">เลือกจังหวัด....</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label71" runat="server" Text="Amphoe" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label72" runat="server" Text="อำเภอ" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:Label ID="lbl_amp_edit" runat="server" Visible="false" Text='<%# Eval("amp_idx") %>'></asp:Label>
                                                                <asp:DropDownList ID="ddlamphoe_present_edit" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                    <asp:ListItem Value="0">เลือกอำเภอ....</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label73" runat="server" Text="District" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label75" runat="server" Text="ตำบล" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:Label ID="lbl_dist_edit" runat="server" Visible="false" Text='<%# Eval("dist_idx") %>'></asp:Label>
                                                                <asp:DropDownList ID="ddldistrict_present_edit" runat="server" CssClass="form-control" AutoPostBack="true">
                                                                    <asp:ListItem Value="0">เลือกตำบล....</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label79" runat="server" Text="ZipCode" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label80" runat="server" Text="รหัสไปรษณีย์" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtzipcode_present" CssClass="form-control" runat="server" Text='<%# Eval("post_code") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label83" runat="server" Text="Email" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label84" runat="server" Text="อีเมล" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtemail_present" CssClass="form-control" runat="server" Text='<%# Eval("emp_email") %>' ValidationGroup="SaveAdd"></asp:TextBox>

                                                                <asp:RegularExpressionValidator ID="R_Email" runat="server" SetFocusOnError="true" Display="None" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtemail_present" ErrorMessage="*กรอกในรูปแบบEmail" ValidationGroup="SaveAdd"></asp:RegularExpressionValidator>
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="V_Email2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_Email" Width="160" />
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label76" runat="server" Text="Mobile Phone" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label78" runat="server" Text="โทรศัพท์มือถือ" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txttelmobile_present" CssClass="form-control" runat="server" Text='<%# Eval("emp_mobile_no") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label81" runat="server" Text="Home Phone" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label82" runat="server" Text="โทรศัพท์บ้าน" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txttelhome_present" CssClass="form-control" runat="server" Text='<%# Eval("emp_phone_no") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <hr />

                                                        <div class="form-group">
                                                            <label class="col-sm-12">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label85" runat="server" CssClass="h01" Text="Address & Contact (Permanent)" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label86" runat="server" Text="ที่อยู่และการติดต่อ(ทะเบียนบ้าน)" /></b></small>
                                                                </h2>
                                                            </label>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-labelnotop">
                                                                <asp:CheckBox ID="chkAddress_edit" CssClass="text-primary" Text="เหมือนกับที่อยู่ปัจจุบัน" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelectedIndexChanged" />
                                                            </label>
                                                        </div>

                                                        <asp:Panel ID="BoxAddress_edit" runat="server">

                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label87" runat="server" Text="Permanentaddress" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label88" runat="server" Text="ที่อยู่ตามทะเบียนบ้าน" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-4">
                                                                    <asp:TextBox ID="txtaddress_permanentaddress" autocomplete="off" MaxLength="250" TextMode="MultiLine" Rows="3" CssClass="form-control" runat="server" Text='<%# Eval("emp_address_permanent") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label89" runat="server" Text="Country" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label90" runat="server" Text="ประเทศ" /></b></small>
                                                                    </h2>
                                                                </label>

                                                                <div class="col-sm-2">
                                                                    <asp:Label ID="lbl_ddlcountry_permanent_edit" runat="server" Visible="false" Text='<%# Eval("country_idx_permanent") %>'></asp:Label>
                                                                    <asp:DropDownList ID="ddlcountry_permanentaddress_edit" runat="server" CssClass="form-control">
                                                                        <asp:ListItem Value="0">เลือกประเทศ....</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>

                                                                <label class="col-sm-2 control-label col-sm-offset-1">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label91" runat="server" Text="Province" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label92" runat="server" Text="จังหวัด" /></b></small>
                                                                    </h2>
                                                                </label>

                                                                <div class="col-sm-2">
                                                                    <asp:Label ID="lbl_ddlprovince_permanent_edit" runat="server" Visible="false" Text='<%# Eval("prov_idx_permanent") %>'></asp:Label>
                                                                    <asp:DropDownList ID="ddlprovince_permanentaddress_edit" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                        <asp:ListItem Value="0">เลือกจังหวัด....</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label93" runat="server" Text="Amphoe" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label94" runat="server" Text="อำเภอ" /></b></small>
                                                                    </h2>
                                                                </label>

                                                                <div class="col-sm-2">
                                                                    <asp:Label ID="lbl_ddlamphoe_permanent_edit" runat="server" Visible="false" Text='<%# Eval("amp_idx_permanent") %>'></asp:Label>
                                                                    <asp:DropDownList ID="ddlamphoe_permanentaddress_edit" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                        <asp:ListItem Value="0">เลือกอำเภอ....</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>

                                                                <label class="col-sm-2 control-label col-sm-offset-1">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label95" runat="server" Text="District" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label96" runat="server" Text="ตำบล" /></b></small>
                                                                    </h2>
                                                                </label>

                                                                <div class="col-sm-2">
                                                                    <asp:Label ID="lbl_ddldistrict_permanent_edit" runat="server" Visible="false" Text='<%# Eval("dist_idx_permanent") %>'></asp:Label>
                                                                    <asp:DropDownList ID="ddldistrict_permanentaddress_edit" runat="server" CssClass="form-control" AutoPostBack="true">
                                                                        <asp:ListItem Value="0">เลือกตำบล....</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label97" runat="server" Text="Home Phone" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label98" runat="server" Text="โทรศัพท์บ้าน" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-2">
                                                                    <asp:TextBox ID="txttelmobile_permanentaddress" CssClass="form-control" runat="server" Text='<%# Eval("PhoneNumber_permanent") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                                </div>

                                                            </div>

                                                        </asp:Panel>

                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>

                                        <div class="panel panel-red">
                                            <div class="panel-heading">
                                                <h3 class="panel-title"><i class="glyphicon glyphicon-heart"></i><strong>&nbsp; Health  / ร่างกาย & สุขภาพ</strong></h3>
                                            </div>
                                            <asp:Panel ID="Panel25" runat="server" CssClass="bg-regis">
                                                <div class="panel-body">
                                                    <div class="form-horizontal" role="form">

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label38" runat="server" Text="Currently used hospital" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label39" runat="server" Text="ปัจจุบันใช้สถานพยาบาล" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:Label ID="lbl_hos_edit" runat="server" Visible="false" Text='<%# Eval("soc_hos_idx") %>'></asp:Label>
                                                                <asp:DropDownList ID="ddlhospital" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0">เลือกโรงพยาบาล</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label121" runat="server" Text="Date of examination" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label122" runat="server" Text="วันที่ตรวจร่างกาย" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <div class='input-group date'>
                                                                    <%--Text='<%# formatDate((String)Eval("Examinationdate")) %>'--%>
                                                                    <asp:TextBox ID="txtexaminationdate" CssClass="form-control from-date-datepicker" MaxLengh="100%" runat="server" Text='<%# Eval("Examinationdate") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                                    <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label40" runat="server" Text="Social Security No" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label41" runat="server" Text="เลขที่ประกันสังคม" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtsecurityid" CssClass="form-control" runat="server" Text='<%# Eval("soc_no") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label45" runat="server" Text="Expiration Date" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label46" runat="server" Text="วันหมดอายุ" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <div class='input-group date'>
                                                                    <%--Text='<%# formatDate((String)Eval("soc_expired_date")) %>' --%>
                                                                    <asp:TextBox ID="txtexpsecurity" CssClass="form-control from-date-datepicker" MaxLengh="100%" runat="server" Text='<%# Eval("soc_expired_date") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                                    <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                </div>
                                                            </div>

                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label50" runat="server" Text="Height" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label51" runat="server" Text="ส่วนสูง" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtheight" CssClass="form-control" runat="server" Text='<%# Eval("height_s") %>' ValidationGroup="SaveAdd"></asp:TextBox>

                                                                <asp:RegularExpressionValidator ID="R_Height" runat="server" ErrorMessage="*เฉพาะตัวเลขเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txtheight" ValidationExpression="^[0-9'.\s]{1,3}$" ValidationGroup="SaveAdd" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="VS_Height2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_Height" Width="160" />
                                                            </div>
                                                            <label class="col-sm-1 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label54" runat="server" Text="Cm" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label55" runat="server" Text="ซม." /></b></small>
                                                                </h2>
                                                            </label>

                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label52" runat="server" Text="Weight" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label53" runat="server" Text="น้ำหนัก" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtweight" CssClass="form-control" runat="server" Text='<%# Eval("weight_s") %>' ValidationGroup="SaveAdd"></asp:TextBox>

                                                                <asp:RegularExpressionValidator ID="R_Weigth" runat="server" ErrorMessage="*เฉพาะตัวเลขเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txtweight" ValidationExpression="^[0-9'.\s]{1,3}$" ValidationGroup="SaveAdd" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="V_Weigth2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_Weigth" Width="160" />
                                                            </div>
                                                            <label class="col-sm-1 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label56" runat="server" Text="Kg" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label58" runat="server" Text="กก." /></b></small>
                                                                </h2>
                                                            </label>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label59" runat="server" Text="BloodGroup" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label60" runat="server" Text="กรุ๊ปเลือด" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:Label ID="lbl_BRHIDX" runat="server" Visible="false" Text='<%# Eval("BTypeIDX") %>' />
                                                                <asp:DropDownList ID="ddlblood" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0">เลือกกรุ๊ปเลือด....</asp:ListItem>
                                                                    <asp:ListItem Value="1">เอ (A)</asp:ListItem>
                                                                    <asp:ListItem Value="2">บี (B)</asp:ListItem>
                                                                    <asp:ListItem Value="3">โอ (O)</asp:ListItem>
                                                                    <asp:ListItem Value="4">เอบี (AB)</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label61" runat="server" Text="Scar" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label62" runat="server" Text="ตำหนิ" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtscar" CssClass="form-control" runat="server" Text='<%# Eval("scar_s") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label117" runat="server" Text="Medical certificate" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label118" runat="server" Text="ใบรับรองแพทย์" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:Label ID="lbl_medical_id" runat="server" Visible="false" Text='<%# Eval("medicalcertificate_id") %>' />
                                                                <asp:DropDownList ID="ddlmedicalcertificate" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0">เลือกสถานะใบรับรอง....</asp:ListItem>
                                                                    <asp:ListItem Value="1">มี</asp:ListItem>
                                                                    <asp:ListItem Value="2">ไม่มี</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label119" runat="server" Text="Result Lab" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label120" runat="server" Text="ผลตรวจ LAB" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:Label ID="lbl_resultlab_id" runat="server" Visible="false" Text='<%# Eval("resultlab_id") %>' />
                                                                <asp:DropDownList ID="ddlresultlab" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0">เลือกสถานะผลตรวจ....</asp:ListItem>
                                                                    <asp:ListItem Value="1">มี</asp:ListItem>
                                                                    <asp:ListItem Value="2">ไม่มี</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>

                                    </asp:Panel>

                                </EditItemTemplate>

                            </asp:FormView>

                            <div class="col-sm-1 col-sm-offset-1">
                                <asp:LinkButton ID="btnupdate" runat="server" CssClass="btn btn-default" OnCommand="btnCommand" CommandName="btnUpdate" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" ValidationGroup="FormEdit">Save &nbsp;</asp:LinkButton>
                            </div>

                        </asp:Panel>

                        <div id="ordine_test_edit" class="modal open" role="dialog">
                            <div class="modal-dialog" style="width: 60%;">
                                <!-- Modal content-->
                                <div class="modal-content">

                                    <div class="modal-body bg-template-center_tranfer">
                                        <div class="panel-body">
                                            <div class="col-sm-12">
                                                <asp:UpdatePanel ID="Up2dateP2s22anel4" runat="server">
                                                    <ContentTemplate>
                                                        <div class="col-sm-3 col-sm-offset-1 button_tranfer">
                                                            <asp:ImageButton ID="lbttranfer_test" CssClass="img-responsive img-fluid background-size: 100% 100%" Width="100%" ImageUrl="~/masterpage/images/hr/pic_tranfer_button.png" OnCommand="btnCommand" CommandName="btntranfer_test" runat="server" />
                                                        </div>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="lbttranfer_test" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </asp:Panel>
        </asp:View>

        <asp:View ID="ViewBenefits" runat="server">

            <asp:ImageButton ID="ImageButton2" CssClass="img-responsive img-fluid center-block" Width="60%" ImageUrl="~/masterpage/images/hr/benefits.jpg" runat="server" />
        </asp:View>

        <asp:View ID="ViewPosition" runat="server">

            <asp:Panel ID="Box_bg_position" runat="server">

                <div class="panel-body">
                    <div class="form-horizontal" role="form">
                        <asp:Label ID="Label44" runat="server"></asp:Label>
                        <asp:HyperLink runat="server" ID="HyperLink2" />
                        <div class="form-group">
                            <label class="col-sm-1"></label>
                            <label class="col-sm-10">
                                <asp:Image ID="Image4" CssClass="img-responsive img-fluid" Width="100%" Height="500px" ImageUrl="~/masterpage/images/hr/banner_register_6.png" runat="server" />
                            </label>
                            <label class="col-sm-1"></label>
                        </div>

                        <asp:Panel ID="box_size" runat="server" Width="84%" CssClass="col-lg-10 col-sm-offset-1">
                            <div class="panel panel-red">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="glyphicon glyphicon-briefcase"></i><strong>&nbsp; ตำแหน่งรับสมัครงาน</strong></h3>
                                </div>
                                <asp:Panel ID="Panel6" runat="server" CssClass="bg-regis">
                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">

                                            <asp:GridView ID="GvMaPosition" runat="server"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-hover table-striped table-responsive col-lg-12"
                                                Style="border-collapse: collapse;"
                                                GridLines="None"
                                                HorizontalAlign="Center"
                                                HeaderStyle-CssClass="primary"
                                                HeaderStyle-Height="40px"
                                                AllowPaging="true"
                                                DataKeyNames="PIDX"
                                                PageSize="25"
                                                OnPageIndexChanging="Master_PageIndexChanging"
                                                OnRowDataBound="Master_RowDataBound"
                                                Width="100%">

                                                <PagerStyle CssClass="pageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                </EmptyDataTemplate>
                                                <Columns>

                                                    <asp:TemplateField HeaderText="#" HeaderStyle-Width="2%">

                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPIDX" runat="server" Visible="false" Text='<%# Eval("PIDX") %>' />
                                                            <asp:Label ID="lblPiority" runat="server" Visible="false" Text='<%# Eval("P_Piority") %>' />
                                                            <%# (Container.DataItemIndex +1) %>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="ชื่อตำแหน่ง" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="10%">
                                                        <ItemTemplate>
                                                            <asp:Image ID="img_status" Width="15%" ImageUrl="~/masterpage/images/hr/pic_icon_new2.png" runat="server" Visible="false" />
                                                            <asp:Label ID="lblP_Name" runat="server" Text='<%# Eval("P_Name") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="สถานที่ปฏิบัติงาน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="5%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblP_Location" runat="server" Text='<%# Eval("P_Location") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="คุณสมบัติ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="30%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblP_Property" runat="server" Text='<%# Eval("P_Property") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="อัตรา" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblP_Salary" runat="server" Text='<%# Eval("P_Unit") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="เพิ่มเติม" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="2%">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="View" CssClass="btn btn-default" runat="server" CommandName="btnView_ma_position" OnCommand="btnCommand" data-toggle="tooltip" title="View" CommandArgument='<%# Eval("PIDX") %>'><i class="fa fa-reorder"></i></asp:LinkButton>
                                                        </ItemTemplate>

                                                        <EditItemTemplate />
                                                        <FooterTemplate />
                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                        </asp:Panel>

                        <div id="ordine" class="modal open" role="dialog">
                            <div class="modal-dialog" style="width: 50%;">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header bg-template-top">
                                        <h3 class="modal-title">
                                            <asp:Label ID="lblP_Namehead_View" runat="server" CssClass="control-label" Text="ตำแหน่ง :" />
                                            <asp:Label ID="lblP_Name_View" runat="server" CssClass="control-label" /></h3>
                                        <marquee><asp:Label ID="lblP_Piority_view" runat="server" CssClass="control-label" Font-Bold="True" /></marquee>
                                    </div>
                                    <div class="modal-body bg-template-center">
                                        <div class="panel-body">
                                            <div class="col-sm-9">
                                                <%--<div class="form-group">--%>

                                                <asp:FormView ID="FvDetail_Position" runat="server" OnDataBound="FvDetail_DataBound">
                                                    <ItemTemplate>

                                                        <div class="form-group">
                                                            <div class="col-sm-5 text_right">
                                                                <asp:Label ID="Label57" runat="server" Text="รายละเอียดงาน :" Font-Bold="True" CssClass="control-label"></asp:Label>
                                                                <asp:Label ID="txtPIDX" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("PIDX")%>' />
                                                                <asp:Label ID="lblP_Piority" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("P_Piority")%>' />
                                                                <asp:Label ID="lblP_Name_fv" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("P_Name")%>' />
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <asp:Label ID="txtP_Description_update" runat="server" Rows="7" CssClass="control-label text_right" Text='<%# Eval("P_Description")%>' />
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-sm-5 text_right">
                                                                <asp:Label ID="Label6" runat="server" Text="สถานที่ :" Font-Bold="True" CssClass="control-label"></asp:Label>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <asp:Label ID="txtP_Location_update" runat="server" CssClass="control-label" Text='<%# Eval("P_Location")%>' />
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-sm-5 text_right">
                                                                <asp:Label ID="Label8" runat="server" Text="คุณสมบัติ :" Font-Bold="True" CssClass="control-label"></asp:Label>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <asp:Label ID="txtP_Property_update" runat="server" Rows="7" CssClass="control-label" Text='<%# Eval("P_Property")%>' />
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-sm-5 text_right">
                                                                <asp:Label ID="Label7" runat="server" Text="อัตรา :" Font-Bold="True" CssClass="control-label"></asp:Label>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <asp:Label ID="txtP_Unit_update" runat="server" CssClass="control-label" Text='<%# Eval("P_Unit")%>' />
                                                                คน
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-sm-5 text_right">
                                                                <asp:Label ID="Label9" runat="server" Text="เงินเดือน :" Font-Bold="True" CssClass="control-label"></asp:Label>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <asp:Label ID="txtP_Salary_update" runat="server" CssClass="control-label" Text='<%# Eval("P_Salary")%>' />
                                                                บาท
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-sm-5 text_right">
                                                                <asp:Label ID="Label10" runat="server" Text="ติดต่อ :" Font-Bold="True" CssClass="control-label"></asp:Label>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <asp:Label ID="txtP_Contract_update" runat="server" CssClass="control-label" Text='<%# Eval("P_Contract")%>' />
                                                            </div>
                                                        </div>

                                                    </ItemTemplate>
                                                </asp:FormView>

                                                <%--</div>--%>
                                            </div>

                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <asp:Image ID="Image11" CssClass="img-responsive img-fluid" Width="100%" Height="280px" ImageUrl="~/masterpage/images/hr/modal_center_1.png" runat="server" />
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="modal-footer bg-template-buttom" data-dismiss="modal">
                                        <%--<asp:LinkButton ID="LinkButton1" runat="server" data-dismiss="modal" CssClass="btn btn-default" OnCommand="btnCommand" CommandName="btnClose_Change_Password">Close &nbsp;</asp:LinkButton>--%>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </asp:Panel>
        </asp:View>

        <asp:View ID="ViewContract" runat="server">

            <div class="panel-body">
                <div class="form-horizontal" role="form">
                    <asp:Label ID="Label48" runat="server"></asp:Label>
                    <asp:HyperLink runat="server" ID="HyperLink3" />
                    <div class="form-group">
                        <label class="col-sm-1"></label>
                        <label class="col-sm-10">
                            <asp:Image ID="Image6" CssClass="img-responsive" Width="100%" Height="450px" ImageUrl="~/masterpage/images/hr/banner_register_7.png" runat="server" />
                        </label>
                        <label class="col-sm-1"></label>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-1"></div>
                        <div class="col-sm-4">
                            <div class="panel panel-red">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i><strong>&nbsp; Location / สำนักงาน</strong></h3>
                                </div>
                                <asp:Panel ID="pn_bg_genarol1" runat="server" CssClass="bg-regis">
                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">
                                            <asp:ImageButton ID="ibmenu_mtt_white" CssClass="img-responsive img-fluid" Width="100%" Height="100%" CommandArgument="1" ImageUrl="~/masterpage/images/hr/menu_contract_mtt.png" runat="server" OnCommand="btnCommand" CommandName="btnView_plant" />
                                            <asp:ImageButton ID="ibmenu_mtt_black" CssClass="img-responsive img-fluid" Width="100%" Height="100%" CommandArgument="1" Visible="false" ImageUrl="~/masterpage/images/hr/menu_contract_mtt_gray.png" runat="server" OnCommand="btnCommand" CommandName="btnView_plant" />
                                            <asp:ImageButton ID="ibmenu_npw_white" CssClass="img-responsive img-fluid" Width="100%" Height="100%" CommandArgument="2" Visible="false" ImageUrl="~/masterpage/images/hr/menu_contract_npw.png" runat="server" OnCommand="btnCommand" CommandName="btnView_plant" />
                                            <asp:ImageButton ID="ibmenu_npw_black" CssClass="img-responsive img-fluid" Width="100%" Height="100%" CommandArgument="2" ImageUrl="~/masterpage/images/hr/menu_contract_npw_gray.png" runat="server" OnCommand="btnCommand" CommandName="btnView_plant" />
                                            <asp:ImageButton ID="ibmenu_rjn_white" CssClass="img-responsive img-fluid" Width="100%" Height="100%" CommandArgument="3" Visible="false" ImageUrl="~/masterpage/images/hr/menu_contract_rjn.png" runat="server" OnCommand="btnCommand" CommandName="btnView_plant" />
                                            <asp:ImageButton ID="ibmenu_rjn_black" CssClass="img-responsive img-fluid" Width="100%" Height="100%" CommandArgument="3" ImageUrl="~/masterpage/images/hr/menu_contract_rjn_gray.png" runat="server" OnCommand="btnCommand" CommandName="btnView_plant" />

                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="panel panel-red">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i><strong>&nbsp; Map / แผนที่การเดินทาง</strong></h3>
                                </div>

                                <asp:Panel ID="panel_mtt" runat="server">
                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">
                                            <asp:ImageButton ID="Image5" CssClass="img-responsive img-fluid" Width="100%" Height="100%" ImageUrl="~/masterpage/images/hr/map_mtt.png" OnCommand="btnCommand" CommandName="btnView_Mtt" runat="server" />
                                        </div>
                                    </div>

                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d797.2313005838072!2d100.53993991887562!3d13.917852995061484!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e28382a91a80ff%3A0x47c43814db66d5c7!2sTaokaenoi!5e0!3m2!1sen!2sth!4v1533729059740" width="100%" height="450" frameborder="0" style="border: 0" allowfullscreen></iframe>
                                </asp:Panel>

                                <div id="ordine_mtt" class="modal open" role="dialog">
                                    <div class="modal-dialog" style="width: 50%;">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <div class="panel-body">
                                                    <div class="form-group">

                                                        <asp:Image ID="ImageButwton3" CssClass="img-responsive img-fluid" Width="100%" Height="100%" ImageUrl="~/masterpage/images/hr/map_mtt.png" runat="server" />

                                                    </div>
                                                </div>

                                            </div>
                                            <div class="modal-footer bg-template">
                                                <asp:LinkButton ID="LinkButton8" runat="server" data-dismiss="modal" CssClass="btn btn-default" OnCommand="btnCommand" CommandName="btnClose_Change_Password">Close &nbsp;</asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <asp:Panel ID="panel_npw" runat="server" Visible="false">
                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">
                                            <asp:ImageButton ID="Image7" CssClass="img-responsive img-fluid" Width="100%" Height="100%" ImageUrl="~/masterpage/images/hr/map_npw.png" runat="server" OnCommand="btnCommand" CommandName="btnView_Npw" />
                                        </div>
                                    </div>

                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15481.225614675792!2d100.33177646977536!3d14.059075345690415!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e289fd0c532b4f%3A0x44bba0db8e9a650d!2z4Lia4Lij4Li04Lip4Lix4LiXIOC5gOC4luC5ieC4suC5geC4geC5iOC4meC5ieC4reC4oiDguJ_guLnguYrguJTguYHguK3guJnguJTguYzguKHguLLguKPguYzguYDguIHguYfguJXguJXguLTguYnguIcg4LiI4Liz4LiB4Lix4LiUICjguKHguKvguLLguIrguJkpIEZhY3Rvcnk!5e0!3m2!1sen!2sth!4v1533716731468" width="100%" height="450" frameborder="0" style="border: 0" allowfullscreen></iframe>
                                </asp:Panel>

                                <div id="ordine_npw" class="modal open" role="dialog">
                                    <div class="modal-dialog" style="width: 50%;">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <div class="panel-body">
                                                    <div class="form-group">

                                                        <asp:Image ID="Image9" CssClass="img-responsive img-fluid" Width="100%" Height="100%" ImageUrl="~/masterpage/images/hr/map_npw.png" runat="server" />

                                                    </div>
                                                </div>

                                            </div>
                                            <div class="modal-footer bg-template">
                                                <asp:LinkButton ID="LinkButton5" runat="server" data-dismiss="modal" CssClass="btn btn-default" OnCommand="btnCommand" CommandName="btnClose_Change_Password">Close &nbsp;</asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <asp:Panel ID="panel_rjn" runat="server" Visible="false">
                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">
                                            <asp:ImageButton ID="Image8" CssClass="img-responsive img-fluid" Width="100%" Height="100%" ImageUrl="~/masterpage/images/hr/map_rjn.png" runat="server" OnCommand="btnCommand" CommandName="btnView_Rjn" />
                                        </div>
                                    </div>

                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3301.931911847664!2d100.70793597249263!3d14.33754406706783!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x311d8bf639d6efa1%3A0x25601ae0c25a441b!2sTao+Kae+Noi%2C+Ayutthaya!5e0!3m2!1sen!2sth!4v1533728337373" width="100%" height="450" frameborder="0" style="border: 0" allowfullscreen></iframe>
                                </asp:Panel>

                                <div id="ordine_rjn" class="modal open" role="dialog">
                                    <div class="modal-dialog" style="width: 50%;">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <div class="panel-body">
                                                    <div class="form-group">

                                                        <asp:Image ID="Image10" CssClass="img-responsive img-fluid" Width="100%" Height="100%" ImageUrl="~/masterpage/images/hr/map_rjn.png" runat="server" />

                                                    </div>
                                                </div>

                                            </div>
                                            <div class="modal-footer bg-template">
                                                <asp:LinkButton ID="LinkButton9" runat="server" data-dismiss="modal" CssClass="btn btn-default" OnCommand="btnCommand" CommandName="btnClose_Change_Password">Close &nbsp;</asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>

                <script>
                    (function (i, s, o, g, r, a, m) {
                        i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                            (i[r].q = i[r].q || []).push(arguments)
                        }, i[r].l = 1 * new Date(); a = s.createElement(o),
                            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
                    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

                    ga('create', 'UA-59162640-3', 'auto');
                    ga('send', 'pageview');
                    //Universal Analytics Tracking Code (analytics.js):
                    ga('set', 'contentGroup5', 'Submit data');
                </script>
            </div>

        </asp:View>

    </asp:MultiView>

</asp:Content>

