﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.IO;
using System.Data;

public partial class websystem_qa_labis_print_recive_date : System.Web.UI.Page
{
    function_tool _funcTool = new function_tool();
    data_qa _data_qa = new data_qa();

    string _localJson = "";
    int _tempInt = 0;
    int _tempcounter;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlQaGetSearchDateForPrint = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetSearchDateForPrint"];
    static string _urlQaGetTestSample = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetTestSample"];
    static string _urlQaGetSampleView = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetSampleView"];
    static string _urlQaGetU1DocDateForPrint = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetU1DocDateForPrint"];
    
    protected void Page_Load(object sender, EventArgs e)
    {
        string url = Request.QueryString["url"];

        if (Session["SESSIONRECIVEDATE"].ToString() != null)
        {

            ViewState["_RECIVEDATEPRINT"] = Session["SESSIONRECIVEDATE"].ToString();
            ViewState["_U1DOC"] = Session["SESSIONU1DOC"].ToString();
            ViewState["_PRINTSHOWTESTITEMS"] = Session["SESSION_PrintShowTestItems"].ToString();
            ViewState["_PRINTSHOWQRCODE"] = Session["SESSION_PrintShowQrCode"].ToString();
            ViewState["_M0Lab"] = Session["SESSION_M0LAB"].ToString();
            ViewState["_M0Place"] = Session["SESSION_PlacePrint"].ToString();


        }
        else if (Session["SESSIONRECIVEDATE"].ToString() == null)
        {
            Response.AddHeader("REFRESH", "3;URL=" + ResolveUrl("~/login?url=" + url));
        }

        _GETPRINTDATE();
        data_qa _dataqaSearch = new data_qa();
        _dataqaSearch.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
        qa_lab_u1_qalab _searchDatePrint = new qa_lab_u1_qalab();
        _searchDatePrint.received_date = ViewState["_RECIVEDATEPRINT"].ToString();
        _searchDatePrint.m0_lab_idx = int.Parse(ViewState["_M0Lab"].ToString());
        _dataqaSearch.qa_lab_u1_qalab_list[0] = _searchDatePrint;

        _dataqaSearch = callServicePostQA(_urlQaGetSearchDateForPrint, _dataqaSearch);

        if (_dataqaSearch.qa_lab_u1_qalab_list != null)
        {
            ViewState["vsSampleTestedItems"] = _dataqaSearch.qa_lab_u1_qalab_list;
            setRepeaterData(rptPrintReciveDate, ViewState["vsSampleTestedItems"]);
        }
        else
        {
            //  divShowToolPrint.Visible = false;
            //  divAlertSearchnoneDate.Visible = true;
        }

    }
    protected void _GETPRINTDATE()
    {
        //lblPrintDate.Text = DateTime.Now.ToString();

        lblPrintDate.Text = DateTime.Now.AddMinutes(55).ToString("dd'/'MM'/'yyyy HH:mm:ss");
    }
    protected void setRepeaterData(Repeater rptName, Object obj)
    {
        rptName.DataSource = obj;
        rptName.DataBind();
    }
    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "gvPrintReciveDate":

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    
                    var lblsampleCode = (Label)e.Row.FindControl("lblsampleCode");                 
                    Image ImagShowQRCode = (Image)e.Row.FindControl("ImagShowQRCode");

          
                    string getPathShowimages = ConfigurationManager.AppSettings["path_qr_code_qa"];
                    string fileimageShowPath = Server.MapPath(getPathShowimages + ViewState["_M0Place"].ToString() + lblsampleCode.Text + ".jpg");

                    ImagShowQRCode.ImageUrl = getPathShowimages + ViewState["_M0Place"].ToString() + lblsampleCode.Text + ".jpg";

                    Label lbPrintU1Doc = (Label)e.Row.Cells[1].FindControl("lbPrintU1Doc");
                    Repeater rp_test_sample = (Repeater)e.Row.Cells[2].FindControl("rp_test_sample");

                    data_qa _data_test_sample_ = new data_qa();
                    _data_test_sample_.bind_qa_m0_test_detail_list = new bindqa_m0_test_detail[1];
                    bindqa_m0_test_detail _doc_sampledeail = new bindqa_m0_test_detail();

                    _doc_sampledeail.u1_qalab_idx = int.Parse(lbPrintU1Doc.Text);
                    _data_test_sample_.bind_qa_m0_test_detail_list[0] = _doc_sampledeail;
                    _data_test_sample_ = callServicePostQA(_urlQaGetTestSample, _data_test_sample_);

                    setRepeaterData(rp_test_sample, _data_test_sample_.bind_qa_m0_test_detail_list);

                    ViewState["CountTestedItems"] = _data_test_sample_.bind_qa_m0_test_detail_list.Count();

                    //var rpt = (Repeater)sender;
                    //var rowRpt = (RepeaterItem)rpt.NamingContainer;

                    //var gvPrintReciveDate = (GridView)rowRpt.FindControl("gvPrintReciveDate");

                    ////var gvPrintReciveDate = (GridView)rptPrintReciveDate.Items[0].FindControl("gvPrintReciveDate");

                    //if (ViewState["_PRINTSHOWQRCODE"].ToString() == "1")
                    //{
                    //    // e.Row.Cells[0].Visible = true;
                    //    gvPrintReciveDate.Columns[0].Visible = true;
                    //}
                    //else
                    //{
                    //    gvPrintReciveDate.Columns[0].Visible = false;
                    //    // e.Row.Cells[0].Visible = false;
                    //}
                    //if (ViewState["_PRINTSHOWTESTITEMS"].ToString() == "1")
                    //{
                    //   // e.Row.Cells[2].Visible = true;
                    //    gvPrintReciveDate.Columns[2].Visible = true;
                    //}
                    //else
                    //{
                    //    gvPrintReciveDate.Columns[2].Visible = false;
                    //    // e.Row.Cells[2].Visible = false;
                    //}
                    //int count = 0;
                    //foreach (RepeaterItem itemReciveDate in rptPrintReciveDate.Items)
                    //{
                    //    var gvPrintReciveDate = (GridView)itemReciveDate.FindControl("gvPrintReciveDate");

                    //    if (ViewState["_PRINTSHOWQRCODE"].ToString() == "1")
                    //    {
                    //        gvPrintReciveDate.Columns[0].Visible = false;
                    //    }
                    //    else
                    //    {
                    //        gvPrintReciveDate.Columns[0].Visible = true;
                    //    }



                    //    if (ViewState["_PRINTSHOWTESTITEMS"].ToString() == "1")
                    //    {

                    //        gvPrintReciveDate.Columns[2].Visible = true;
                    //    }
                    //    else
                    //    {
                    //        gvPrintReciveDate.Columns[2].Visible = false;
                    //    }

                    //    count++;
                    //}

                }



                break;
        }
    }

    protected void rptOnRowDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        var rptName = (Repeater)Sender;

        switch (rptName.ID)
        {
            case "rptPrintReciveDate":

                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {

                    var gvPrintReciveDate = (GridView)e.Item.FindControl("gvPrintReciveDate");
                    var lbPrintSampleCodeU1Doc = (Label)e.Item.FindControl("lbPrintSampleCodeU1Doc");
                    var sample_code = (Label)e.Item.FindControl("sample_code");
                    


                    string test = "1580, 1588";//ViewState["_U1DOC"].ToString();
                    string separators = "";
                    //string[] words = test.Split(separators, StringSplitOptions.RemoveEmptyEntries);

                    string[] recepientEmail = test.Split(',');
                    foreach (string rEmail in recepientEmail)
                    {
                        if (rEmail != String.Empty)
                        {
                            //  separators
                           // Label1.Text += rEmail.ToString() + "<br />";
                        }
                    }

                    if (gvPrintReciveDate.DataSource == null)
                    {

                        data_qa _data_sample_print = new data_qa();
                        _data_sample_print.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
                        qa_lab_u1_qalab _u1_doc_sampleeail = new qa_lab_u1_qalab();

                        _u1_doc_sampleeail.u1_list = ViewState["_U1DOC"].ToString();
                        _u1_doc_sampleeail.m0_lab_idx = int.Parse(ViewState["_M0Lab"].ToString());
                        // _u1_doc_sampleeail.u1_qalab_idx = 1588;//U0DocIdx;
                        _data_sample_print.qa_lab_u1_qalab_list[0] = _u1_doc_sampleeail;

                        _data_sample_print = callServicePostQA(_urlQaGetU1DocDateForPrint, _data_sample_print);

                        if (_data_sample_print.qa_lab_u1_qalab_list != null)
                        {


                            //lbPrintSampleCodeU1Doc

                            var _linqCheckMat = (from dataMat in _data_sample_print.qa_lab_u1_qalab_list
                                                 where dataMat.sample_code == sample_code.Text
                                                 select dataMat);


                            //string getPathShowimages = ConfigurationManager.AppSettings["path_qr_code_qa"];

                            //string fileimageShowPath = Server.MapPath(getPathShowimages + lblsampleCode.Text + ".jpg");


                            gvPrintReciveDate.DataSource = _linqCheckMat.ToList();
                            gvPrintReciveDate.DataBind();

                            //Label1.Text = "has";
                        }
                        else
                        {
                            //Label1.Text = "not";
                        }


                    }



                    data_qa _data_test_sample_search_print = new data_qa();
                    _data_test_sample_search_print.bind_qa_m0_test_detail_list = new bindqa_m0_test_detail[1];
                    bindqa_m0_test_detail _doc_sampledeail = new bindqa_m0_test_detail();

                    _doc_sampledeail.u1_qalab_idx = int.Parse(lbPrintSampleCodeU1Doc.Text);

                    _data_test_sample_search_print.bind_qa_m0_test_detail_list[0] = _doc_sampledeail;

                    _data_test_sample_search_print = callServicePostQA(_urlQaGetTestSample, _data_test_sample_search_print);

                    for (int a = 0; a < _data_test_sample_search_print.bind_qa_m0_test_detail_list.Count(); a++)
                    {

                        BoundField field = new BoundField();
                        field.HeaderText = _data_test_sample_search_print.bind_qa_m0_test_detail_list[a].test_detail_name.ToString();
                        field.HeaderStyle.Width = 10;
                        DataControlField col;
                        col = field;
                        gvPrintReciveDate.Columns.Add(col);
                        gvPrintReciveDate.DataBind();
                        //  Label1.Text = "111";

                    }

                    if (ViewState["_PRINTSHOWQRCODE"].ToString() == "1")
                    {
                        gvPrintReciveDate.Columns[0].Visible = true;
                    }
                    else
                    {
                        gvPrintReciveDate.Columns[0].Visible = false;
                    }

                    if (ViewState["_PRINTSHOWTESTITEMS"].ToString() == "1")
                    {

                        gvPrintReciveDate.Columns[2].Visible = true;
                    }
                    else
                    {
                        gvPrintReciveDate.Columns[2].Visible = false;
                    }



                    //(rp_test_sample_search_print, _data_test_sample_search_print.bind_qa_m0_test_detail_list);

                    //for (int k = 0; k <= rp_place.Items.Count; k++)
                    //{
                    //    btnPlaceLab.CssClass = ConfigureColors(k);
                    //    //Console.WriteLine(i);
                    //}


                }

                break;
        }

    }

    protected string _functionInputResultToColumn(int U0DocIdx, string sample_code, string IDtestdetails)
    {

        data_qa _dataqaSearch1 = new data_qa();
        _dataqaSearch1.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
        qa_lab_u1_qalab _searchDatePrint = new qa_lab_u1_qalab();
        _searchDatePrint.received_date = ViewState["_RECIVEDATEPRINT"].ToString();
        // _searchDatePrint.test_date = HiddenSearchDateTested.Value.ToString();
        _dataqaSearch1.qa_lab_u1_qalab_list[0] = _searchDatePrint;
        _dataqaSearch1 = callServicePostQA(_urlQaGetSearchDateForPrint, _dataqaSearch1);

        string returnResult = "";
        int t3 = 0;
        for (int t0 = 3; t0 < _dataqaSearch1.qa_lab_u1_qalab_list.Count(); t0++)
        {
            data_qa _data_sample_print = new data_qa();
            _data_sample_print.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
            qa_lab_u1_qalab _u1_doc_sampleeail = new qa_lab_u1_qalab();

            //_u1_doc_sampleeail.u1_list = "1580, 1588";
            _data_sample_print.qa_lab_u1_qalab_list[0] = _u1_doc_sampleeail;

            _data_sample_print = callServicePostQA(_urlQaGetU1DocDateForPrint, _data_sample_print);

            if (_data_sample_print.qa_lab_u1_qalab_list != null)
            {
               
            }
            else
            {
              //  Label1.Text = "not";
            }


          

            //for (int z = 0; z < _data_sample_print.qa_lab_u1_qalab_list.Count(); z++)
            //{
            //    if (_data_sample_print.qa_lab_u1_qalab_list[z].sample_code.ToString() != null)
            //    {
            //        returnResult += _data_sample_print.qa_lab_u1_qalab_list[z].sample_code.ToString();
            //    }
            //    else
            //    {

            //    }
            //}

            // returnResult
            //  lblsampleCode.Text = _data_sample_print.qa_lab_u1_qalab_list[t].sample_code.ToString();


           
            //  newRow1[t0] = _functionInputResultToColumn(U0IDXExpoetExcel, _loop_rowText.sample_code.ToString(), _dataExport_header.qa_lab_u1_qalab_list[t3].test_detail_idx.ToString());

            t3++;
        }


        return returnResult;
    }

    protected data_qa callServicePostQA(string _cmdUrl, data_qa _data_qa)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_qa);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_qa = (data_qa)_funcTool.convertJsonToObject(typeof(data_qa), _localJson);

        return _data_qa;
    }

}