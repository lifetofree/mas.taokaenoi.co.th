﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_tcm_m0_subdoctype : System.Web.UI.Page
{
    #region Connect
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();
    data_roombooking _data_roombooking = new data_roombooking();
    data_law _data_law = new data_law();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    //-- employee --//

    //--master--//
    static string _urlGetLawM0DocType = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawM0DocType"];
    static string _urlSetLawM0SubDocType = _serviceUrl + ConfigurationManager.AppSettings["urlSetLawM0SubDocType"];
    static string _urlGetLawM0SubDocType = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawM0SubDocType"];
    static string _urlSetDelLawM0SubDocType = _serviceUrl + ConfigurationManager.AppSettings["urlSetDelLawM0SubDocType"];


    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    bool _flag_qmr = false;
    #endregion Connect

    #region Page_Init
    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
        //check permission
        //foreach (int item in rdept_qmr)
        //{
        //    if (_dataEmployee.employee_list[0].rdept_idx == item)
        //    {
        //        _flag_qmr = true;
        //        break;
        //    }
        //}
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            mvSystem.SetActiveView(ViewIndex);
            SelectDetail();
        }
    }

    #region getEmployeeProfile
    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }
    #endregion

    #region setformdata
    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    #endregion setformdata

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        switch (cmdName)
        {
            case "cmdInsert":

                GvDetail.EditIndex = -1;
                SelectDetail();
                btnInsert.Visible = false;
                setFormData(FvInsert, FormViewMode.Insert, null);
                FvInsert.Visible = true;

                DropDownList ddlDocType = (DropDownList)FvInsert.FindControl("ddlDocType");
                SelectDoctypeDetail(ddlDocType, 0);


                break;

            case "CmdSave":
                InsertDetail();
                SelectDetail();
                btnInsert.Visible = true;
                FvInsert.Visible = false;
                break;

            case "CmdCancel":
                btnInsert.Visible = true;
                FvInsert.Visible = false;

                break;

            case "CmdDelete":

                int idx = int.Parse(cmdArg);

                data_law data_m0_subtype_del = new data_law();
                tcm_m0_subtype_detail m0_subtype_del = new tcm_m0_subtype_detail();
                data_m0_subtype_del.tcm_m0_subtype_list = new tcm_m0_subtype_detail[1];

                m0_subtype_del.subtype_idx = idx;
                m0_subtype_del.cemp_idx = _emp_idx;
                m0_subtype_del.subtype_status = 9;
                data_m0_subtype_del.tcm_m0_subtype_list[0] = m0_subtype_del;

                data_m0_subtype_del = callServicePostLaw(_urlSetDelLawM0SubDocType, data_m0_subtype_del);

                SelectDetail();

                btnInsert.Visible = true;
                FvInsert.Visible = false;
                break;
        }
    }
    #endregion btnCommand

    #region Custom Functions
    protected void InsertDetail()
    {
        DropDownList ddlDocType = (DropDownList)FvInsert.FindControl("ddlDocType");
        TextBox txtsubtype_name_th = (TextBox)FvInsert.FindControl("txtsubtype_name_th");
        TextBox txtsubtype_name_en = (TextBox)FvInsert.FindControl("txtsubtype_name_en");
        DropDownList ddlStatus = (DropDownList)FvInsert.FindControl("ddlStatus");

        data_law data_m0_subtype_detail = new data_law();
        tcm_m0_subtype_detail m0_subtype_detail = new tcm_m0_subtype_detail();
        data_m0_subtype_detail.tcm_m0_subtype_list = new tcm_m0_subtype_detail[1];

        m0_subtype_detail.subtype_idx = 0;
        m0_subtype_detail.type_idx = int.Parse(ddlDocType.SelectedValue);
        m0_subtype_detail.subtype_name_th = txtsubtype_name_th.Text;
        m0_subtype_detail.subtype_name_en = txtsubtype_name_en.Text;
        m0_subtype_detail.subtype_status = int.Parse(ddlStatus.SelectedValue);
        m0_subtype_detail.cemp_idx = _emp_idx;

        data_m0_subtype_detail.tcm_m0_subtype_list[0] = m0_subtype_detail;

        data_m0_subtype_detail = callServicePostLaw(_urlSetLawM0SubDocType, data_m0_subtype_detail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));

        if (data_m0_subtype_detail.return_code == 1)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);

        }
        else
        {
            ddlDocType.ClearSelection();
            txtsubtype_name_th.Text = String.Empty;
            txtsubtype_name_en.Text = String.Empty;

        }
    }

    protected void SelectDetail()
    {

        data_law data_m0_subtype_detail = new data_law();
        tcm_m0_subtype_detail m0_subtype_detail = new tcm_m0_subtype_detail();
        data_m0_subtype_detail.tcm_m0_subtype_list = new tcm_m0_subtype_detail[1];
        m0_subtype_detail.condition = 0;
        data_m0_subtype_detail.tcm_m0_subtype_list[0] = m0_subtype_detail;

        data_m0_subtype_detail = callServicePostLaw(_urlGetLawM0SubDocType, data_m0_subtype_detail);
        //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_subtype_detail));
        setGridData(GvDetail, data_m0_subtype_detail.tcm_m0_subtype_list);

    }

    protected void SelectDoctypeDetail(DropDownList ddlName, int _doctype_idx)
    {

        data_law data_m0_type_detail = new data_law();
        tcm_m0_type_detail m0_type_detail = new tcm_m0_type_detail();
        data_m0_type_detail.tcm_m0_type_list = new tcm_m0_type_detail[1];
        m0_type_detail.condition = 1;
        data_m0_type_detail.tcm_m0_type_list[0] = m0_type_detail;

        data_m0_type_detail = callServicePostLaw(_urlGetLawM0DocType, data_m0_type_detail);
        setDdlData(ddlName, data_m0_type_detail.tcm_m0_type_list, "type_name_en", "type_idx");
        ddlName.Items.Insert(0, new ListItem("--- Select Document Type ---", "0"));
        ddlName.SelectedValue = _doctype_idx.ToString();
        ////setGridData(GvDetail, data_m0_type_detail.tcm_m0_type_list);

    }

    #endregion Custom Functions

    #region setDataBind
    private void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected string getStatus(int status)
    {
        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }
    #endregion

    #region Masterbound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvDetail":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    btnInsert.Visible = true;
                    FvInsert.Visible = false;

                }


                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvDetail.EditIndex == e.Row.RowIndex) //to overlook header row
                    {
                        var lbl_type_idx_edit = (Label)e.Row.FindControl("lbl_type_idx_edit");
                        var ddlDocTypeEdit = (DropDownList)e.Row.FindControl("ddlDocTypeEdit");

                        ddlDocTypeEdit.AppendDataBoundItems = true;

                        data_law data_m0_type_select = new data_law();
                        tcm_m0_type_detail m0_type_select = new tcm_m0_type_detail();
                        data_m0_type_select.tcm_m0_type_list = new tcm_m0_type_detail[1];
                        m0_type_select.condition = 1;
                        data_m0_type_select.tcm_m0_type_list[0] = m0_type_select;

                        data_m0_type_select = callServicePostLaw(_urlGetLawM0DocType, data_m0_type_select);

                        ddlDocTypeEdit.DataSource = data_m0_type_select.tcm_m0_type_list;
                        ddlDocTypeEdit.DataTextField = "type_name_en";
                        ddlDocTypeEdit.DataValueField = "type_idx";
                        ddlDocTypeEdit.DataBind();
                        ddlDocTypeEdit.Items.Insert(0, new ListItem("--- Select Document Type ---", "0"));
                        ddlDocTypeEdit.SelectedValue = lbl_type_idx_edit.Text;
                    }

                }

                break;


        }
    }
    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvDetail":

                GvDetail.EditIndex = e.NewEditIndex;
                SelectDetail();

                ////DropDownList ddlDocTypeEdit = (DropDownList)GvDetail.Rows[e.NewEditIndex].FindControl("ddlDocTypeEdit");
                ////Label lbl_type_idx_edit = (Label)GvDetail.Rows[e.NewEditIndex].FindControl("lbl_type_idx_edit");

                ////SelectDoctypeDetail(ddlDocTypeEdit, int.Parse(lbl_type_idx_edit.Text));

                break;

        }
    }
    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

        var GvName = (GridView)sender;
        //Label5.Text = GvName.ID;
        switch (GvName.ID)
        {
            case "GvDetail":

                GvDetail.EditIndex = -1;

                var ddlDocTypeEdit = (DropDownList)GvDetail.Rows[e.RowIndex].FindControl("ddlDocTypeEdit");
                var txt_subtype_idx_edit = (TextBox)GvDetail.Rows[e.RowIndex].FindControl("txt_subtype_idx_edit");
                var txt_subtype_name_th_edit = (TextBox)GvDetail.Rows[e.RowIndex].FindControl("txt_subtype_name_th_edit");
                var txt_subtype_name_en_edit = (TextBox)GvDetail.Rows[e.RowIndex].FindControl("txt_subtype_name_en_edit");
                var ddlsubtype_status_edit = (DropDownList)GvDetail.Rows[e.RowIndex].FindControl("ddlsubtype_status_edit");


                data_law data_m0_subtype_edit = new data_law();
                tcm_m0_subtype_detail m0_subtype_edit = new tcm_m0_subtype_detail();
                data_m0_subtype_edit.tcm_m0_subtype_list = new tcm_m0_subtype_detail[1];


                m0_subtype_edit.type_idx = int.Parse(ddlDocTypeEdit.SelectedValue.ToString());
                m0_subtype_edit.subtype_idx = int.Parse(txt_subtype_idx_edit.Text);
                m0_subtype_edit.subtype_name_th = txt_subtype_name_th_edit.Text;
                m0_subtype_edit.subtype_name_en = txt_subtype_name_en_edit.Text;
                m0_subtype_edit.subtype_status = int.Parse(ddlsubtype_status_edit.SelectedValue);
                m0_subtype_edit.cemp_idx = _emp_idx;

                data_m0_subtype_edit.tcm_m0_subtype_list[0] = m0_subtype_edit;
                //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_subtype_edit));
                data_m0_subtype_edit = callServicePostLaw(_urlSetLawM0SubDocType, data_m0_subtype_edit);

                if (data_m0_subtype_edit.return_code == 1)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- Please Edit again ---');", true);
                    SelectDetail();
                }
                else
                {
                    SelectDetail();
                }
                break;

        }
    }
    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        //Label5.Text = GvName.ID;
        switch (GvName.ID)
        {
            case "GvDetail":
                GvDetail.EditIndex = -1;
                SelectDetail();

                btnInsert.Visible = true;
                FvInsert.Visible = false;
                break;

        }
    }
    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvDetail":
                GvDetail.PageIndex = e.NewPageIndex;
                GvDetail.DataBind();
                SelectDetail();
                break;

        }
    }
    #endregion

    #region callService 
    protected data_qa_cims callServicePostMasterQACIMS(string _cmdUrl, data_qa_cims _data_qa_cims)
    {
        _localJson = _funcTool.convertObjectToJson(_data_qa_cims);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_qa_cims = (data_qa_cims)_funcTool.convertJsonToObject(typeof(data_qa_cims), _localJson);


        return _data_qa_cims;
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_law callServicePostLaw(string _cmdUrl, data_law _data_law)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_law);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_law = (data_law)_funcTool.convertJsonToObject(typeof(data_law), _localJson);

        return _data_law;
    }


    protected data_roombooking callServicePostRoomBooking(string _cmdUrl, data_roombooking _data_roombooking)
    {
        _localJson = _funcTool.convertObjectToJson(_data_roombooking);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_roombooking = (data_roombooking)_funcTool.convertJsonToObject(typeof(data_roombooking), _localJson);


        return _data_roombooking;
    }
    #endregion callService Functions

}