﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="UFeedBack.aspx.cs" Inherits="websystem_masterdata_UFeedBack" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>

    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewIndex" runat="server">
            <div class="col-md-12">
                <asp:LinkButton ID="btnToInsert" CssClass="btn btn-primary" runat="server"
                    CommandName="btnToInsert" OnCommand="btnCommand">
                    <i class="fa fa-plus-square"></i>
                    สร้างข้อมูลคำถาม
                </asp:LinkButton>

                <asp:GridView ID="GvMaster"
                    runat="server"
                    AutoGenerateColumns="false"
                    DataKeyNames="MFBIDX"
                    CssClass="table table-striped table-bordered table-responsive col-md-12 m-t-10"
                    HeaderStyle-CssClass="info"
                    AllowPaging="true"
                    PageSize="10"
                    OnRowEditing="Master_RowEditing"
                    OnRowUpdating="Master_RowUpdating"
                    OnRowCancelingEdit="Master_RowCancelingEdit"
                    OnPageIndexChanging="Master_PageIndexChanging"
                    OnRowDataBound="Master_RowDataBound">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="1" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">No result</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <small>
                                    <%# (Container.DataItemIndex +1) %>
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="MFBIDX" runat="server" CssClass="form-control"
                                    Visible="False" Text='<%# Eval("MFBIDX")%>' />

                                <div class="row">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <small>
                                                <label class="pull-left">ชุดคำถาม</label>
                                                <asp:Label ID="lbddlMFBIDX1Update" runat="server" Text='<%# Bind("MFBIDX1") %>' Visible="false" />
                                                <asp:DropDownList ID="ddlMFBIDX1Update" AutoPostBack="true" runat="server"
                                                    CssClass="form-control">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredddlMFBIDX1Update"
                                                    ValidationGroup="saveTypeNameUpdate" runat="server"
                                                    Display="Dynamic"
                                                    SetFocusOnError="true"
                                                    ControlToValidate="ddlMFBIDX1Update"
                                                    InitialValue="0"
                                                    Font-Size="1em" ForeColor="Red"
                                                    CssClass="pull-left"
                                                    ErrorMessage="กรุณากรอกชื่อชุดคำถาม" />
                                            </small>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <small>
                                                <label class="pull-left">หัวข้อคำถาม</label>
                                                <asp:UpdatePanel ID="panelMFBIDX2Update" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Label ID="lbddlMFBIDX2Update" runat="server" Text='<%# Bind("MFBIDX2") %>' Visible="false" />
                                                        <asp:DropDownList ID="ddlMFBIDX2Update" AutoPostBack="true" runat="server"
                                                            CssClass="form-control">
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredddlMFBIDX2Update"
                                                            ValidationGroup="saveTypeNameUpdate" runat="server"
                                                            Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="ddlMFBIDX2Update"
                                                            InitialValue="0"
                                                            Font-Size="1em" ForeColor="Red"
                                                            CssClass="pull-left"
                                                            ErrorMessage="กรุณากรอกชื่อหัวข้อคำถาม" />
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </small>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <small>
                                                <label class="pull-left">คำถามย่อย</label>
                                                <asp:UpdatePanel ID="panelMFBIDX3Update" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Label ID="lbddlMFBIDX3Update" runat="server" Text='<%# Bind("MFBIDX3") %>' Visible="false" />
                                                        <asp:DropDownList ID="ddlMFBIDX3Update" AutoPostBack="true" runat="server"
                                                            CssClass="form-control">
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="requiredddlMFBIDX3Update"
                                                            ValidationGroup="saveTypeNameUpdate" runat="server"
                                                            Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="ddlMFBIDX3Update"
                                                            InitialValue="0"
                                                            Font-Size="1em" ForeColor="Red"
                                                            CssClass="pull-left"
                                                            ErrorMessage="กรุณากรอกชื่อคำถามย่อย" />
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </small>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <small>
                                                <label class="pull-left">สถานะ</label>
                                                <asp:DropDownList ID="ddlMFBStatusUpdate" AutoPostBack="false" runat="server"
                                                    CssClass="form-control" SelectedValue='<%# Eval("MFBStatus") %>'>
                                                    <asp:ListItem Value="1" Text="Online"></asp:ListItem>
                                                    <asp:ListItem Value="0" Text="Offline"></asp:ListItem>
                                                </asp:DropDownList>
                                            </small>
                                        </div>
                                    </div>
                                </div>

                                <div>
                                    <div class="col-md-2"></div>
                                    <div class="col-md-6">
                                        <div class="pull-left">
                                            <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server"
                                                ValidationGroup="saveTypeNameUpdate" CommandName="Update"
                                                OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')">
                                                บันทึก
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server"
                                                CommandName="Cancel">ยกเลิก</asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </EditItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="QuestionSet" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="QuestionSet" runat="server" Text='<%# Eval("QuestionSet") %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Topic" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="Topic" runat="server" Text='<%# Eval("Topic") %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Question" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="Question" runat="server" Text='<%# Eval("Question") %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="MFBStatus" runat="server" Text='<%# getStatus((int)Eval("MFBStatus")) %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Manage" ItemStyle-HorizontalAlign="center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                    data-toggle="tooltip" title="แก้ไข"><i class="glyphicon glyphicon-pencil"></i></asp:LinkButton>
                                <asp:LinkButton ID="delete" CssClass="text-trash" runat="server" data-toggle="tooltip"
                                    title="ลบ" CommandName="btnDelete" OnCommand="btnCommand"
                                    CommandArgument='<%# Eval("MFBIDX") %>'
                                    OnClientClick="return confirm('คุณค้องการลบข้อมูลนี้ใช่หรือไม่')">
                                   <i class="glyphicon glyphicon-trash"></i>
                                </asp:LinkButton>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>

            </div>
        </asp:View>

        <!-- Start Insert Form -->
        <asp:View ID="ViewInsert" runat="server">
            <div class="row col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <asp:LinkButton CssClass="btn btn-danger" data-toggle="tooltip" title="Back" runat="server"
                                CommandName="btnCancel" OnCommand="btnCommand">ย้อนกลับ</asp:LinkButton>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <small>
                                    <label class="pull-left">ชุดคำถาม</label>
                                    <asp:Label ID="lbddlMFBIDX1" runat="server" Visible="false" />
                                    <asp:DropDownList ID="ddlMFBIDX1" AutoPostBack="true" runat="server"
                                        CssClass="form-control">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredddlMFBIDX1"
                                        ValidationGroup="save" runat="server"
                                        Display="Dynamic"
                                        SetFocusOnError="true"
                                        ControlToValidate="ddlMFBIDX1"
                                        InitialValue="0"
                                        Font-Size="1em" ForeColor="Red"
                                        ErrorMessage="กรุณากรอกชื่อชุดคำถาม" />
                                </small>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <small>
                                    <label class="pull-left">หัวข้อคำถาม</label>
                                    <asp:Panel ID="panelMFBIDX2" runat="server">
                                        <asp:Label ID="lbddlMFBIDX2" runat="server" Visible="false" />
                                        <asp:DropDownList ID="ddlMFBIDX2" AutoPostBack="true" runat="server"
                                            CssClass="form-control">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredddlMFBIDX2"
                                            ValidationGroup="save" runat="server"
                                            Display="Dynamic"
                                            SetFocusOnError="true"
                                            ControlToValidate="ddlMFBIDX2"
                                            InitialValue="0"
                                            Font-Size="1em" ForeColor="Red"
                                            ErrorMessage="กรุณากรอกชื่อหัวข้อคำถาม" />
                                    </asp:Panel>
                                </small>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <small>
                                    <label class="pull-left">คำถามย่อย</label>
                                    <asp:Panel ID="panelMFBIDX3" runat="server">
                                        <asp:Label ID="lbddlMFBIDX3" runat="server" Visible="false" />
                                        <asp:DropDownList ID="ddlMFBIDX3" AutoPostBack="true" runat="server"
                                            CssClass="form-control">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="requiredddlMFBIDX3"
                                            ValidationGroup="save" runat="server"
                                            Display="Dynamic"
                                            SetFocusOnError="true"
                                            ControlToValidate="ddlMFBIDX3"
                                            InitialValue="0"
                                            Font-Size="1em" ForeColor="Red"
                                            ErrorMessage="กรุณากรอกชื่อคำถามย่อย" />
                                    </asp:Panel>
                                </small>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <small>
                                    <label class="pull-left">สถานะ</label>
                                    <asp:DropDownList ID="ddlMFBStatus" AutoPostBack="false" runat="server"
                                        CssClass="form-control">
                                        <asp:ListItem Value="1" Text="Online"></asp:ListItem>
                                        <asp:ListItem Value="0" Text="Offline"></asp:ListItem>
                                    </asp:DropDownList>
                                </small>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:Button CssClass="btn btn-success" runat="server" CommandName="btnInsert" OnCommand="btnCommand" Text="บันทึก" ValidationGroup="save" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:View>
        <!-- End Insert Form -->


    </asp:MultiView>

</asp:Content>
