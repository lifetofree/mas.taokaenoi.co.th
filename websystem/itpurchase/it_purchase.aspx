﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="it_purchase.aspx.cs" Inherits="websystem_ITPurchase_ITpurchase" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <!-- menu -->
    <asp:HyperLink ID="SETFOCUS_ONTOP" runat="server"></asp:HyperLink>
    <asp:Literal ID="check_XML" runat="server"></asp:Literal>
    <asp:Literal ID="Literal1" runat="server"></asp:Literal>
    <asp:Literal ID="Literal2" runat="server"></asp:Literal>
    <asp:Literal ID="Literal3" runat="server"></asp:Literal>
    <asp:Literal ID="check_mail" runat="server"></asp:Literal>
    <span style="color: red; font-size: 16px; font-weight: bold; margin-left: 20px;">
        <asp:Literal ID="litShowDetail" runat="server"></asp:Literal></span>
    <div id="divMenubar" runat="server" class="col-md-12">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand">Menu</a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav ulActiveMenu" runat="server">
                    <li id="IndexList" runat="server">
                        <asp:LinkButton ID="listMenu0" runat="server"
                            CommandName="viewmanu1"
                            OnCommand="btn_command" Text="ข้อมูลทั่วไป" />
                    </li>
                    <%--     <li id="listresult" runat="server">
                    <asp:LinkButton ID="listMenu2" runat="server"
                        CommandName="showboxsummarypurchase"
                        OnCommand="btn_command" Text="สรุปรายการขอซื้อ" />
                </li>--%>
                    <li id="listreport" runat="server">
                        <asp:LinkButton ID="listMenu3" runat="server"
                            CommandName="showbox_report"
                            OnCommand="btn_command" Text="รายงาน" />
                    </li>
                    <li id="li4" runat="server" class="dropdown">
                        <asp:LinkButton ID="lbtab5" runat="server" class="dropdown-toggle" data-toggle="dropdown"
                            role="button" aria-haspopup="true" aria-expanded="false">คู่มือการใช้งาน<span class="caret"></span></asp:LinkButton>

                        <ul class="dropdown-menu">
                            <li>
                                <asp:HyperLink runat="server" ID="HyperLink3" Visible="true" data-toggle="tooltip"
                                    data-original-title="คู่มือการใช้งาน" NavigateUrl='https://docs.google.com/document/d/1EZIUjBBubsY0CpNov7y7_PmQ4xWaGuDgcmwOtmaL4ug/edit?usp=sharing' Target="_blank"><i class="fa fa-book"></i> คู่มือการใช้งาน</asp:HyperLink>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li>
                                <asp:HyperLink runat="server" ID="HyperLink4" Visible="true" data-toggle="tooltip"
                                    data-original-title="Flow Chart" NavigateUrl='https://drive.google.com/file/d/0B2MFs0CHeN-sTk5kR2NqZjFFR0k/view' Target="_blank"><i class="fa fa-book"></i> Flow Chart</asp:HyperLink>
                            </li>
                        </ul>
                    </li>
                    <%-- <li id="listexport_excel" runat="server">
                <asp:LinkButton ID="listmenu4" runat="server"
                    CommandName="showboxsummarypurchase"
                    OnCommand="btn_command" Text="" />
            </li>--%>
                </ul>
            </div>
        </nav>
    </div>
    <!-- menu -->
    <!-- menu -->
    <asp:MultiView ID="mvMultiview" runat="server" ActiveViewIndex="0">
        <!-- view Home page -->
        <asp:View ID="viewHomepage" runat="server">
            <asp:HyperLink ID="setFocus_viewHomepage" runat="server"></asp:HyperLink>
            <div class="col-sm-12" runat="server" id="content_Homepage">
                <div class="form-group">
                    <asp:LinkButton CssClass="btn btn-primary" ID="btnshowboxcreate" data-toggle="tooltip" title="สร้างรายการขอซื้ออุปกรณ์" runat="server"
                        CommandName="showboxcreate" CommandArgument="0" OnCommand="btn_command"><i class="glyphicon glyphicon-file"></i> สร้างรายการขอซื้ออุปกรณ์</asp:LinkButton>&nbsp;
            <asp:LinkButton CssClass="btn btn-info btn-group-vertical" ID="btnshowBoxsearch" data-toggle="tooltip" title="ค้นหารายการ" runat="server"
                CommandName="showBoxsearch" OnCommand="btn_command"><i class="glyphicon glyphicon-search"></i> ค้นหารายการ</asp:LinkButton>
                    <asp:LinkButton CssClass="btn btn-danger btn-group-vertical" ID="btnhiddenBoxsearch" data-toggle="tooltip" title="ยกเลิกค้นหารายการ" runat="server"
                        CommandName="hiddenBoxsearch" Visible="false" OnCommand="btn_command"><i class="glyphicon glyphicon-search"></i> ยกเลิกค้นหารายการ</asp:LinkButton>
                    <asp:LinkButton CssClass="btn btn-defult pull-right" ID="btnviewdataofmountly" data-toggle="tooltip" title="แสดงข้อมูลแบบรายเดือน" runat="server"
                        CommandName="viewpurchasemountly" Visible="true" OnCommand="btn_command"><i class="glyphicon glyphicon-calendar"></i> แสดงข้อมูลแบบรายเดือน</asp:LinkButton>
                    <asp:LinkButton CssClass="btn btn-default pull-right" ID="btnviewall" data-toggle="tooltip" title="แสดงข้อมูลทั้งหมด" runat="server"
                        CommandName="viewall" Visible="true" OnCommand="btn_command">แสดงข้อมูลทั้งหมด</asp:LinkButton>

                </div>
                <%--     <form>
            <div class="form-group">
                <label for="sel1">Select list (select one):</label>
                <select class="form-control" id="sel1">
                    <option>sirirak</option>
                    <option>modtanoy</option>
                    <option>3</option>
                    <option>4</option>
                </select>
                </div>
            </form>--%>
                <%--   <h5>&nbsp;</h5>--%>
                <%-- เลือกดูข้อมูลแบบเป็นเดือน--%>
                <div class="form-group">
                    <asp:Panel ID="panel_mountly" runat="server" Visible="false">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <i class="glyphicon glyphicon-calendar"></i>&nbsp;
                        <b>Select Mountly</b> (เลือกเดือนเพื่อทำการอนุมัติรายการขอซื้อในแต่ละเดือน)
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label><small>เลือกเดือน (select mountly)</small></label>
                                            <asp:UpdatePanel ID="UpdatePanel_mountly" runat="server">
                                                <ContentTemplate>
                                                    <asp:DropDownList ID="ddl_mountly"
                                                        runat="server" CssClass="form-control fa-align-left"
                                                        AutoPostBack="true" Enabled="true">
                                                    </asp:DropDownList>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                        <div class="col-sm-3">
                                            <label><small>เลือกปี (select yearly)</small></label>
                                            <asp:UpdatePanel ID="UpdatePanel_yearly" runat="server">
                                                <ContentTemplate>
                                                    <asp:DropDownList ID="ddl_yearly"
                                                        runat="server" CssClass="form-control fa-align-left"
                                                        AutoPostBack="true" Enabled="true">
                                                    </asp:DropDownList>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                        <div class="col-sm-3">
                                            <label><small>&nbsp;</small></label>
                                            <asp:UpdatePanel ID="UpdatePanel_submit" runat="server">
                                                <ContentTemplate>
                                                    <asp:LinkButton CssClass="btn btn-primary" ID="btnsubmitview" data-toggle="tooltip" title="แสดงผลลัพธ์" runat="server"
                                                        CommandName="submit_mountly" OnCommand="btn_command"><i class="glyphicon glyphicon-file"></i> แสดงผลลัพธ์</asp:LinkButton>&nbsp;
                                                <asp:LinkButton CssClass="btn btn-danger" ID="btncancelview" data-toggle="tooltip" title="ยกเลิก" runat="server"
                                                    CommandName="hiddenBoxsearch" OnCommand="btn_command"><i class="glyphicon glyphicon-share-alt"></i> เลิกทำ</asp:LinkButton>&nbsp;
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
                <%--  ค้นหา--%>
                <div class="row">
                    <asp:Panel ID="panel_search" runat="server" Visible="false">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <i class="glyphicon glyphicon-search"></i>&nbsp;
                        <b>search</b> (ค้นหารายการ)
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <%-- เงื่อนไขการค้นหา--%>
                                    <%--                <div class="form-group">
                                    <asp:Label ID="lbyearsearch" CssClass="col-sm-2 control-label" runat="server" Text="ปี : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlyear_search" AutoPostBack="true" runat="server" CssClass="form-control">
                                            <asp:ListItem Text="กรุณาเลือกปี ...." Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <asp:Label ID="lbroomsearch" CssClass="col-sm-2 control-label" runat="server" Text="เดือน : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlmountly_search" AutoPostBack="true" runat="server" CssClass="form-control">
                                            <asp:ListItem Text="กรุณาเลือกเดือน ...." Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                </div>--%>
                                    <div class="form-group">
                                        <asp:Label ID="Label2" CssClass="col-sm-2 control-label" runat="server" Text="ระบุวันที่ค้นหา : " />
                                        <div class="col-sm-2">
                                            <asp:DropDownList ID="ddlcondition" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" CssClass="form-control">
                                                <asp:ListItem Text="เลือกเงื่อนไข...." Value="0"></asp:ListItem>
                                                <asp:ListItem Text="มากกว่า >" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="น้อยกว่า <" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="ระหว่าง <>" Value="3"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <%--    <asp:Label ID="Label15" CssClass="col-sm-1 control-label" runat="server" Text="จากวันที่ : " />--%>

                                        <div class="col-sm-3">
                                            <div class='input-group date from-date-datepicker'>
                                                <asp:TextBox ID="txtstartdate" runat="server" placeholder="จากวันที่..."
                                                    CssClass="form-control from-date-datepicker"></asp:TextBox><span class="input-group-addon"><span data-icon-element="" class="glyphicon glyphicon-calendar"></span></span>
                                            </div>
                                        </div>

                                        <%--   <asp:Label ID="Label16" CssClass="col-sm-1 control-label" runat="server" Text="ถึงวันที่ : " />--%>
                                        <div class="col-sm-3">
                                            <div class='input-group date from-date-datepicker'>
                                                <asp:TextBox ID="txtenddate" runat="server" Enabled="false" placeholder="ถึงวันที่..."
                                                    CssClass="form-control from-date-datepicker"></asp:TextBox><span class="input-group-addon"><span data-icon-element="" class="glyphicon glyphicon-calendar"></span></span>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label17" CssClass="col-sm-2 control-label" runat="server" Text="รหัสเอกสาร : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtdocumentcode" runat="server" placeholder="รหัสเอกสาร"
                                                CssClass="form-control"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label18" CssClass="col-sm-2 control-label" runat="server" Text="รหัสพนักงาน : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtempcode" runat="server" MaxLength="8" placeholder="รหัสพนักงาน"
                                                CssClass="form-control"></asp:TextBox>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label19" CssClass="col-sm-2 control-label" runat="server" Text="ชื่อ-นามสกุล : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtfirstname_lastname" runat="server" placeholder="ชื่อ-นามสกุล"
                                                CssClass="form-control"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label20" CssClass="col-sm-2 control-label" runat="server" Text="บริษัท : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlsearch_organization"
                                                runat="server" CssClass="form-control fa-align-left"
                                                AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" Enabled="true">
                                            </asp:DropDownList>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label21" CssClass="col-sm-2 control-label" runat="server" Text="ฝ่าย : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlsearch_department"
                                                runat="server" CssClass="form-control fa-align-left"
                                                AutoPostBack="true" Enabled="true">
                                            </asp:DropDownList>
                                        </div>

                                        <asp:Label ID="Label22" CssClass="col-sm-2 control-label" runat="server" Text="สถานที่ : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddllocation_search"
                                                runat="server" CssClass="form-control fa-align-left"
                                                Enabled="true">
                                            </asp:DropDownList>
                                        </div>

                                    </div>


                                    <div class="form-group">
                                        <asp:Label ID="Label23" CssClass="col-sm-2 control-label" runat="server" Text="Cost Center : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddl_costcenter"
                                                runat="server" CssClass="form-control fa-align-left selectpicker"
                                                AutoPostBack="true" Enabled="true">
                                            </asp:DropDownList>
                                        </div>

                                        <asp:Label ID="Label24" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทการขอซื้อ : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlsearch_typepurchase"
                                                runat="server" CssClass="form-control fa-align-left"
                                                AutoPostBack="true" Enabled="true">
                                            </asp:DropDownList>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label25" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทอุปกรณ์ : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlsearch_typequipment"
                                                runat="server" CssClass="form-control fa-align-left"
                                                AutoPostBack="true" Enabled="true">
                                            </asp:DropDownList>
                                        </div>

                                        <asp:Label ID="Label26" CssClass="col-sm-2 control-label" runat="server" Text="สถานะรายการ : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlsearch_status"
                                                runat="server" CssClass="form-control fa-align-left"
                                                AutoPostBack="true" Enabled="true">
                                            </asp:DropDownList>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <%--   <div class="col-sm-2"></div>--%>
                                        <div class="col-lg-2 col-lg-offset-2">
                                            <asp:LinkButton CssClass="btn btn-info btn-sm" ID="btnsearch" data-toggle="tooltip" title="ค้นหารายการ" runat="server"
                                                CommandName="searching" Visible="true" OnCommand="btn_command"><i class="glyphicon glyphicon-search"></i> ค้นหา</asp:LinkButton>
                                            <%--    </div>
                                        <div class="col-sm-1">--%>
                                            <asp:LinkButton CssClass="btn btn-default btn-sm" ID="btnresetsearch" data-toggle="tooltip" title="รีเซ็ต" runat="server"
                                                CommandName="reset_search" Visible="true" OnCommand="btn_command"><i class="glyphicon glyphicon-refresh"></i> รีเซ็ต</asp:LinkButton>
                                        </div>
                                    </div>
                                    <%-- <div class="row">
                                    <h5>&nbsp;</h5>
                                      
                                </div>--%>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
                <asp:HyperLink ID="HyperLink1" runat="server"></asp:HyperLink>
                <asp:Panel ID="Panel_list_purchase" runat="server" Visible="true">


                    <div class="panel panel-default">
                        <!-- Add Panel Heading Here -->
                        <div class="panel-body">
                            <i class="fa fa-file-text-o" aria-hidden="true"></i>&nbsp;รายการขอซื้อทั้งหมด
                           
                        </div>
                    </div>

                    <%--     <div class="alert alert-info" role="alert">
                <asp:Label ID="labellist_purchase" runat="server">
                            <strong><i class="glyphicon glyphicon-file"></i>&nbsp;รายการขอซื้อทั้งหมด</strong><small>&nbsp; </small>
                </asp:Label>
            </div>--%>
                </asp:Panel>

                <%--  select all อนุมัติทั้งหมด --%>
                <asp:Panel ID="Panel_approve" runat="server" Visible="true">


                    <%-- <div class="alert alert-default" role="alert">--%>

                    <div class="panel panel-default">
                        <!-- Add Panel Heading Here -->
                        <div class="panel-body">
                            <asp:CheckBox ID="check_approve_all" runat="server" AutoPostBack="true" RepeatDirection="Vertical" OnCheckedChanged="checkindexchange" />
                            <i class="fa fa-file-text-o" aria-hidden="true"></i>&nbsp;<strong>อนุมัติรายการขอซื้อทั้งหมด</strong>
                            <asp:LinkButton CssClass="btn btn-success btn-sm" ID="btnapproveall" data-toggle="tooltip" title="อนุมัติรายการ" runat="server"
                                CommandName="confirm_approve" Visible="true" OnClientClick="return confirm('คุณต้องการอนุมัติรายการขอซื้อนี้ใช่หรือไม่ ?')" OnCommand="btn_command">อนุมัติรายการ</asp:LinkButton>
                            <asp:LinkButton CssClass="btn btn-danger btn-sm" ID="btnnotapprove" data-toggle="tooltip" title="ไม่อนุมัติรายการ" runat="server"
                                CommandName="confirm_no_approve" Visible="true" OnClientClick="return confirm('คุณต้องการอนุมัติรายการขอซื้อนี้ใช่หรือไม่ ?')" OnCommand="btn_command">ไม่อนุมัติรายการ</asp:LinkButton>
                        </div>
                    </div>


                    <%--   <asp:Label ID="label3" runat="server">
                    <asp:CheckBox ID="check_approve_all" runat="server" AutoPostBack="true" RepeatDirection="Vertical" OnCheckedChanged="checkindexchange" />
                    <strong>&nbsp;อนุมัติรายการขอซื้อทั้งหมด</strong><small>&nbsp; </small>
                </asp:Label>--%>
                    <%--    <asp:LinkButton CssClass="btn btn-success btn-sm" ID="confirmsave" data-toggle="tooltip" title="ยืนยันการอนุมัติ" runat="server"
                    CommandName="confirm_approve" Visible="true" OnCommand="btn_command">ยืนยันการอนุมัติ</asp:LinkButton>
                <asp:Label ID="label4" runat="server" CssClass="text-right">--%>
                    <%--  <strong>&nbsp;รายการขอซื้อทั้งหมด......รายการ</strong><small>&nbsp; </small>--%>
                    <%--    </asp:Label>--%>
                </asp:Panel>

                <%--   gridview รายการขอซื้อ--%>
                <asp:GridView ID="gvpurchase_equipmentlist" Visible="true" runat="server" AutoGenerateColumns="false" DataKeyNames="u0_purchase_idx"
                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                    HeaderStyle-CssClass="info" HeaderStyle-Height="30px" AllowPaging="true" PageSize="5"
                    OnPageIndexChanging="gvPageIndexChanging" OnRowDataBound="gvRowDataBound">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                        FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่พบข้อมูลรายการขอซื้ออุปกรณ์ IT</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="เลือก" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="center" HeaderStyle-Width="5%" ControlStyle-Font-Size="10" HeaderStyle-Font-Size="8">
                            <ItemTemplate>
                                <%--   <div style="text-align: center; padding-top: 70px;">
                            </div>--%>
                                <%--  <asp:Label ID="u0_purchase_idx1" runat="server" Visible="false" Text='<%# Eval("u0_purchase_idx") %>' />--%>
                                <asp:CheckBox ID="cbrecipients" runat="server" AutoPostBack="true" Visible="true"
                                    OnCheckedChanged="checkindexchange" Text='<%# Container.DataItemIndex %>' Style="color: transparent;"></asp:CheckBox>
                                <asp:HiddenField ID="hfSelected" runat="server" Value='<%# Eval("selected") %>' />

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="รหัสเอกสาร" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="center" HeaderStyle-Width="5%" ControlStyle-Font-Size="10" HeaderStyle-Font-Size="8">
                            <ItemTemplate>
                                <%--  <div style="text-align: center; padding-top: 70px;">--%>
                                <asp:Label ID="u0_purchase_idx" runat="server" Visible="false" Text='<%# Eval("u0_purchase_idx") %>' />
                                <asp:Label ID="idx" Visible="false" runat="server"><%# (Container.DataItemIndex +1) %></asp:Label>
                                <asp:Label ID="lb_documentcode" runat="server" CssClass="col-sm-12">
                    <small><p></b> &nbsp;<%# Eval("document_code") %></p></small>
                                </asp:Label>
                                <asp:Label ID="lbdocumentcode" runat="server" Visible="false" Text='<%# Eval("document_code") %>' />
                                <%-- </div>--%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="รายละเอียดพนักงาน" HeaderStyle-Width="30%" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="left" ControlStyle-Font-Size="10" HeaderStyle-Font-Size="8">
                            <ItemTemplate>
                                <asp:Label ID="lb_empCode_employee" runat="server" CssClass="col-sm-12">
                    <small><p><b>รหัสพนักงาน:</b> &nbsp;<%# Eval("empcode_purchase") %></p></small>
                                </asp:Label>
                                <asp:Label ID="lb_fullName_employee" runat="server" CssClass="col-sm-12">
                    <small><p><b>ชื่อ - นามสกุล:</b> &nbsp;<%# Eval("fullname_purchase") %></p></small>
                                </asp:Label>
                                <asp:Label ID="lb_orgName_probation" runat="server" CssClass="col-sm-12">
                    <small><p><b>องค์กร:</b> &nbsp;<%# Eval("org_name_purchase") %></p></small>  
                                </asp:Label>
                                <asp:Label ID="lb_dpart_probation" runat="server" CssClass="col-sm-12">
                    <small><p><b>ฝ่าย:</b> &nbsp;<%# Eval("drept_name_purchase") %></p></small>
                                </asp:Label>
                                <asp:Label ID="lb_section_probation" runat="server" CssClass="col-sm-12">
                    <small><p><b>แผนก:</b> &nbsp;<%# Eval("sec_name_purchase") %></p></small>
                                </asp:Label>
                                <asp:Label ID="lb_position_probation" runat="server" CssClass="col-sm-12">
                    <small><p><b>ตำแหน่ง:</b> &nbsp;<%# Eval("pos_name_purchase") %></p></small>
                                </asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="รายละเอียดการขอซื้อ" HeaderStyle-Width="30%" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="left" ControlStyle-Font-Size="10" HeaderStyle-Font-Size="8">
                            <ItemTemplate>
                                <%--      <asp:Label ID="lb_documentcode" runat="server" CssClass="col-sm-12">
                    <small><p><b>รหัสเอกสาร:</b> &nbsp;<%# Eval("document_code") %></p></small>
                                </asp:Label>--%>



                                <asp:Label ID="lb_date_purchase" runat="server" CssClass="col-sm-12 control-label">
                    <small><p><b>วันที่ขอซื้อ:</b> &nbsp;<%# Eval("date_purchasequipment") %></p></small>
                                </asp:Label>
                                <%-- <asp:Label ID="lb_equipmenttype" runat="server" CssClass="col-sm-12">
                    <small><p><b>ประเภทอุปกรณ์ที่ซื้อ:</b> &nbsp;<%# Eval("name_equipment") %></p></small>
                            </asp:Label>--%>
                                <asp:Label ID="lb_purchasetype" runat="server" CssClass="col-sm-12 control-label">
                    <small><p><b>ประเภทการขอซื้อ:</b> &nbsp;<%# Eval("name_purchase") %></p></small>
                                </asp:Label>
                                <asp:Label ID="lbnamepurchase" runat="server" Visible="false" Text='<%# Eval("name_purchase") %>' />
                                <%--  <asp:Label ID="lb_quantity_equipment" runat="server" CssClass="col-sm-12">
                    <small><p><b>จำนวนเครื่อง:</b> &nbsp;<%# Eval("quantity_equipment") %></p></small>
                            </asp:Label>--%>
                                <asp:Label ID="lb_details_equipment" runat="server" CssClass="col-sm-12 control-label">
                    <small><p><b>รายละเอียด:</b> &nbsp;<%# Eval("details_purchase") %></p></small>
                                </asp:Label>

                                <asp:GridView ID="gvitems" Visible="true" runat="server" AutoGenerateColumns="false"
                                    CssClass="table table-bordered" GridLines="None"
                                    HeaderStyle-CssClass="default" HeaderStyle-Height="10px" AllowPaging="true"
                                    OnRowDataBound="gvRowDataBound">
                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                                        FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                            ItemStyle-HorizontalAlign="center" HeaderStyle-Width="1%" ControlStyle-Font-Size="8" HeaderStyle-Font-Size="8">
                                            <ItemTemplate>

                                                <asp:Label ID="u2_purchaserr_report" runat="server" Visible="false" Text='<%# Eval("u2_purchase_idx") %>' />
                                                <asp:Label ID="idx2reportrr" runat="server"><%# (Container.DataItemIndex +1) %></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="อุปกรณ์" HeaderStyle-CssClass="text-center"
                                            ItemStyle-HorizontalAlign="center" HeaderStyle-Width="5%" HeaderStyle-Font-Size="8" ControlStyle-Font-Size="8">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="equipment_report" runat="server" Font-Size="Small" Visible="true" Text='<%# Eval("type_equipment") %>' />

                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="จำนวน" HeaderStyle-CssClass="text-center"
                                            ItemStyle-HorizontalAlign="center" HeaderStyle-Width="5%" HeaderStyle-Font-Size="8" ControlStyle-Font-Size="8">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="equipment_rellport1" runat="server" Font-Size="Small" Visible="true" Text='<%# Eval("items_quantity") %>' />
                                                </small>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="งบประมาณ" HeaderStyle-CssClass="text-center"
                                            ItemStyle-HorizontalAlign="center" HeaderStyle-Width="5%" HeaderStyle-Font-Size="8" ControlStyle-Font-Size="8">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="equipment_repoxxrt1" runat="server" Font-Size="Small" Visible="true" Text='<%# Eval("items_expenditure") %>' />
                                                </small>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="สถานะการดำเนินการ" HeaderStyle-Width="10%" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="center" ControlStyle-Font-Size="10" HeaderStyle-Font-Size="8">
                            <ItemTemplate>
                                <%--     <div style="text-align: center; padding-top: 60px;">--%>
                                <asp:Label ID="lb_status_node" runat="server" CssClass="col-sm-12">
                            <small><span><%# Eval("node_status_purchase") %>&nbsp;<%# Eval("node_name_purchase") %>&nbsp;<br />
                                <b>โดย</b>&nbsp;<br />   <%# Eval("actor_name_purchase") %></span></small>
                             
                                </asp:Label>
                                <asp:Label ID="lbmailpurchase" runat="server" Visible="false" Text='<%# Eval("email_purchase") %>' />
                                <asp:Label ID="lb_nodeidx" runat="server" Visible="false" Text='<%# Eval("m0_node_idx") %>' />
                                <asp:Label ID="lb_actoridx" runat="server" Visible="false" Text='<%# Eval("m0_actor_idx") %>' />
                                <asp:Label ID="lbactorname" runat="server" Visible="false" Text='<%# Eval("actor_name_purchase") %>' />
                                <%--</div>--%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ความคิดเห็นจากไอที" ControlStyle-Font-Size="10" HeaderStyle-Width="20%" HeaderStyle-Font-Size="8" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="center" Visible="false">
                            <ItemTemplate>
                                <small>

                                    <asp:Panel ID="pn_price_comment_complete" runat="server" Visible="true">
                                        <asp:Repeater ID="rptcommentfromit" runat="server">
                                            <ItemTemplate>

                                                <asp:Label ID="lbcommentit" runat="server" CssClass="col-sm-12"
                                                    Text='<%# Eval("commentfromITSupport") %>'></asp:Label>
                                                <br />
                                                <label class="bg-success">:: ราคา :: </label>
                                                <br />
                                                <asp:Label ID="_lbprice" runat="server" CssClass="col-sm-12"
                                                    Text='<%# Eval("price_equipment") %>'></asp:Label>
                                                <br />

                                            </ItemTemplate>

                                        </asp:Repeater>
                                    </asp:Panel>

                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-Width="5%" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="center" ControlStyle-Font-Size="10" HeaderStyle-Font-Size="8">
                            <ItemTemplate>
                                <asp:UpdatePanel ID="updatebtnsaveEdit" runat="server">
                                    <ContentTemplate>
                                        <%-- <div style="text-align: center; padding-top: 50px;">--%>
                                        <asp:LinkButton ID="btnmanage_purchase" CssClass="btn btn-info btn-sm" runat="server"
                                            data-toggle="tooltip" title="รายละเอียดการขอซื้อ" OnCommand="btn_command"
                                            CommandArgument='<%#Eval("u0_purchase_idx")+ ";" + Eval("m0_node_idx")+ ";" + Eval("emp_idx_purchase")+ ";" + Eval("m0_purchase_idx") + ";" + Eval("m0_actor_idx")  + ";" + Eval("document_code") %>' CommandName="manage_purchase">
                        <i class="fa fa-file-o"></i></asp:LinkButton>
                                        <%--</div>--%>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="btnmanage_purchase" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </asp:View>

        <%-- สร้างรายการขอซื้อ--%>

        <asp:View ID="viewCreatePurchase" runat="server">
            <asp:HyperLink ID="setFocus_viewCreate" runat="server"></asp:HyperLink>
            <div class="col-sm-12" runat="server" id="content_create_purchase_equipment">
                <%--                  <div class="row">
        <asp:LinkButton CssClass="btn btn-danger" Visible="true" ID="btnhiddenboxcreate" data-toggle="tooltip" title="ยกเลิกการสร้างใบขอซื้อ" runat="server"
            CommandName="hiddenboxcreate" OnCommand="btn_command"><i class="glyphicon glyphicon-remove-sign"></i> ยกเลิกการสร้างใบขอซื้อ</asp:LinkButton>
 
                </div>--%>
                <div class="form-group">
                    <asp:Panel ID="Panel_alert_warning1" runat="server" Visible="true">
                        <div class="alert alert-danger" role="alert">
                            <strong><i class="glyphicon glyphicon-time"></i></strong>
                            <asp:Label ID="lb_warning_datecutpurchase" runat="server">
                                            <small>&nbsp; </small>
                            </asp:Label>
                        </div>
                    </asp:Panel>
                </div>


                <asp:FormView ID="fvdetails_employee_purchase" OnDataBound="formview_databound"
                    runat="server" DefaultMode="Edit" Width="100%">
                    <EditItemTemplate>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <i class="glyphicon glyphicon-user"></i>&nbsp;
                        <b>information employee</b> (รายละเอียดข้อมูลผู้ขอซื้ออุปกรณ์)
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="row">
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-5">
                                            <small class="list-group-item-heading textleft">
                                                <b><font color="#1A5276">รหัสพนักงาน :</font>&nbsp;</b>
                                                <asp:Label ID="lb_emps_code_purchase" Text='<%# Eval("empcode_purchase") %>' runat="server">
                                                </asp:Label></small>

                                        </div>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-5">
                                            <small class="list-group-item-heading textleft">
                                                <b><font color="#1A5276">ชื่อ - นามสกุล :</font>&nbsp;</b>
                                                <asp:Label ID="lb_emps_fullname_purchase" Text='<%# Eval("fullname_purchase") %>' runat="server">
                                                </asp:Label></small>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-5">
                                            <small class="list-group-item-heading textleft">
                                                <b><font color="#1A5276">องค์กร :</font>&nbsp;</b>
                                                <asp:Label ID="lb_name_org_purchase" Text='<%# Eval("org_name_purchase") %>' runat="server">
                                <%-- <span><%# Eval("name_org_probation") %></span>--%></asp:Label>
                                                <asp:Label ID="lb_idx_org_purchase" Visible="false" Text='<%# Eval("org_idx_purchase") %>' runat="server" />
                                            </small>
                                        </div>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-5">
                                            <small class="list-group-item-heading textleft">
                                                <b><font color="#1A5276">ฝ่าย :</font>&nbsp;</b>
                                                <span><%# Eval("drept_name_purchase") %></span></small>
                                            <asp:Label ID="lbdrept_name_purchase" Visible="false" Text='<%# Eval("drept_name_purchase") %>' runat="server" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-5">
                                            <small class="list-group-item-heading textleft">
                                                <b><font color="#1A5276">แผนก :</font>&nbsp;</b>
                                                <asp:Label ID="lb_name_sec_purchase" runat="server">
                                <span><%# Eval("sec_name_purchase") %></span></asp:Label></small>
                                        </div>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-5">
                                            <small class="list-group-item-heading textleft">
                                                <b><font color="#1A5276">ตำแหน่ง :</font>&nbsp;</b>
                                                <asp:Label ID="lb_name_pos_purchase" Text='<%# Eval("pos_name_purchase") %>' runat="server">
                                <%--  <span><%# Eval("name_pos_probation") %>--%><%--</span>--%></asp:Label></small>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-5">
                                            <small class="list-group-item-heading textleft">
                                                <b><font color="#1A5276">เบอร์ติดต่อ :</font>&nbsp;</b>
                                                <asp:Label ID="Label5" runat="server">
                                <span><%# Eval("numberphone") %></span></asp:Label></small>
                                        </div>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-5">
                                            <small class="list-group-item-heading textleft">
                                                <b><font color="#1A5276">e-mail :</font>&nbsp;</b>
                                                <asp:Label ID="email_purchase" Text='<%# Eval("email_purchase") %>' runat="server">
                                <%--  <span><%# Eval("name_pos_probation") %>--%><%--</span>--%></asp:Label></small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </EditItemTemplate>
                </asp:FormView>
                <div class="form-group">
                    <asp:LinkButton ID="LinkButton2" runat="server"
                        CommandName="viewmanu1" CssClass="btn btn-default"
                        OnCommand="btn_command"><i class="fa fa-home" aria-hidden="true"></i>&nbsp;หน้าแรก</asp:LinkButton>
                </div>
                <asp:FormView ID="fvCreatePurchasequipment" OnDataBound="formview_databound"
                    runat="server" DefaultMode="Edit" Width="100%">
                    <EditItemTemplate>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <i class="glyphicon glyphicon-file"></i>&nbsp;
                        <b>Purchase Equipment IT</b> (สร้างใบรายการขอซื้ออุปกรณ์ IT)
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-2">
                                            <font color="#1A5276"><small class="list-group-item-heading text_right">
                                <b>Equipment Type </b></small></font>
                                            <small>
                                                <p class="list-group-item-text">ประเภทอุปกรณ์ขอซื้อ</p>
                                            </small>
                                        </div>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txtquipmenttype_idx" runat="server" Visible="false" CssClass="form-control left"
                                                Text='<%# Eval("m0_equipment_idx") %>'></asp:TextBox>

                                            <asp:DropDownList ID="ddlequipmenttype" ValidationGroup="addpurchase" InitialValue="0"
                                                runat="server" CssClass="form-control fa-align-left" OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                AutoPostBack="true" Enabled="true">
                                            </asp:DropDownList>
                                        </div>
                                        <asp:RequiredFieldValidator ID="RequiredFieldddlequipmenttype" runat="server" InitialValue="0"
                                            ControlToValidate="ddlequipmenttype" Display="None" SetFocusOnError="true"
                                            ErrorMessage="*กรุณาเลือกประเภทอุปกรณ์ขอซื้อ" ValidationGroup="addpurchase" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="Validatorddlequipmenttype" runat="Server"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldddlequipmenttype" Width="180" />
                                    </div>


                                    <div class="form-group">
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-2">
                                            <font color="#1A5276"><small class="list-group-item-heading text_right">
                                <b>Purchase Type </b></small></font>
                                            <small>
                                                <p class="list-group-item-text">ประเภทการขอซื้อ</p>
                                            </small>
                                        </div>
                                        <div class="col-sm-6">
                                            <%-- <asp:HiddenField ID="hidden_m0_purchase_idx" runat="server" Value='<%# Eval("m0_purchase_idx") %>' />--%>
                                            <asp:TextBox ID="txtpurchasetype_idx" runat="server" Visible="false" CssClass="form-control left" Text='<%# Eval("m0_purchase_idx") %>'></asp:TextBox>

                                            <asp:DropDownList ID="ddlpurchasetype" ValidationGroup="addpurchase"
                                                runat="server" CssClass="form-control fa-align-left"
                                                AutoPostBack="true" Enabled="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldddlpurchasetype" runat="server" InitialValue="0"
                                                ControlToValidate="ddlpurchasetype" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกประเภทการขอซื้อ" ValidationGroup="addpurchase" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutddlpurchasetype" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldddlpurchasetype" Width="180" />


                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-2">
                                            <font color="#1A5276"><small class="list-group-item-heading text_right">
                                <b>Buy a position </b></small></font>
                                            <small>
                                                <p class="list-group-item-text">ขอซื้อให้พนักงานตำแหน่ง</p>
                                            </small>
                                        </div>
                                        <div class="col-sm-6">
                                            <%-- <asp:HiddenField ID="hidden_m0_purchase_idx" runat="server" Value='<%# Eval("m0_purchase_idx") %>' />--%>
                                            <%--   <asp:TextBox ID="txt_rsec_idx" runat="server" Visible="false" CssClass="form-control left" Text='<%# Eval("se") %>'></asp:TextBox>--%>

                                            <asp:DropDownList ID="ddlposition" ValidationGroup="addpurchase"
                                                runat="server" CssClass="form-control fa-align-left"
                                                AutoPostBack="true" Enabled="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldddlposition" runat="server" InitialValue="0"
                                                ControlToValidate="ddlposition" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกตำแหน่งที่ต้องการซื้ออุปกรณ์" ValidationGroup="addpurchase" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCallddlposition" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldddlposition" Width="180" />

                                        </div>
                                    </div>




                                    <div class="form-group">
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-2">
                                            <font color="#1A5276"><small class="list-group-item-heading text_right">
                                <b>Reasons to buy</b></small></font>
                                            <small>
                                                <p class="list-group-item-text">เหตุผลในการขอซื้อ</p>
                                            </small>
                                        </div>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txtdetails_purchase" runat="server" CssClass="form-control left" Text='<%# Eval("details_purchase") %>'
                                                ValidationGroup="addpurchase" MaxLength="4" TextMode="MultiLine" placeholder="กรุณากรอกรายละเอียดเพื่อขอซื้ออุปกรณ์..."></asp:TextBox>
                                        </div>
                                    </div>


                                    <asp:RequiredFieldValidator ID="RequiredFieldtxtdetails_purchase" runat="server"
                                        ControlToValidate="txtdetails_purchase" Display="None" SetFocusOnError="true"
                                        ErrorMessage="*กรุณากรอกรายละเอียดเพื่อขอซื้ออุปกรณ์" ValidationGroup="addpurchase" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCallouttxtdetails_purchase" runat="Server"
                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldtxtdetails_purchase" Width="180" />

                                    <asp:Panel ID="panel_spec_equipment" runat="server" Visible="false">
                                        <div class="form-group">
                                            <div class="col-sm-1"></div>
                                            <div class="col-sm-2">
                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                <b>selcet spec </b></small></font>
                                                <small>
                                                    <p class="list-group-item-text">เลือกสเปค</p>
                                                </small>
                                            </div>
                                            <div class="col-sm-6">
                                                <%--        <asp:TextBox ID="txtspecidx" runat="server" Visible="false" CssClass="form-control left" Text='<%# Eval("m0_spec_idx") %>'></asp:TextBox>--%>
                                                <asp:DropDownList ID="ddlspecidx"
                                                    runat="server" CssClass="form-control fa-align-left" OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                    AutoPostBack="true" Enabled="true">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldddlspecidx" runat="server" InitialValue="0"
                                                    ControlToValidate="ddlspecidx" Display="Dynamic" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกเลือกสเปค" ValidationGroup="addpurchase" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCallddlspecidx" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldddlspecidx" Width="180" />
                                                <%--   <asp:RadioButtonList ID="radiolist_spec" RepeatDirection="Horizontal" OnSelectedIndexChanged="radioChange" 
                                        AutoPostBack="true" GroupName="selectspec" runat="server" CellSpacing="3" />--%>
                                            </div>
                                        </div>



                                    </asp:Panel>

                                    <div class="form-group">
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-2">
                                            <font color="#1A5276"><small class="list-group-item-heading text_right">
                                <b>Reasons to buy</b></small></font>
                                            <small>
                                                <p class="list-group-item-text">รายละเอียดสเปค</p>
                                            </small>
                                        </div>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txtdetails_spec" runat="server" CssClass="form-control left" Text='<%# Eval("details_spec") %>'
                                                ValidationGroup="addpurchase" MaxLength="6" TextMode="MultiLine" placeholder="กรุณากรอกรายละเอียดเพื่อขอซื้ออุปกรณ์..."></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-2">
                                            <font color="#1A5276"><small class="list-group-item-heading text_right">
                                <b>Quantity Equipment </b></small></font>
                                            <small>
                                                <p class="list-group-item-text">จำนวนอุปกรณ์</p>
                                            </small>
                                        </div>
                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txtquantity" runat="server" CssClass="form-control left" Text='<%# Eval("quantity_equipment") %>'
                                                ValidationGroup="addpurchase" placeholder="กรุณากรอกจำนวน..."></asp:TextBox>
                                        </div>

                                        <div class="col-sm-1">
                                            <font color="#1A5276"><small class="list-group-item-heading text_right">
                                <b>unit </b></small></font>
                                            <small>
                                                <p class="list-group-item-text">หน่วยนับ</p>
                                            </small>
                                        </div>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddltype_unitequipment" OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                runat="server" CssClass="form-control fa-align-left"
                                                AutoPostBack="true" Enabled="false">
                                            </asp:DropDownList>
                                        </div>
                                    </div>


                                    <asp:RequiredFieldValidator ID="Regularquantity" runat="server"
                                        ControlToValidate="txtquantity" Display="None" SetFocusOnError="true"
                                        ErrorMessage="*กรุณากรอกข้อมูลจำนวนอุปกรณ์" ValidationGroup="addpurchase" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="Validatorquantity" runat="Server"
                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Regularquantity" Width="180" />

                                    <asp:RegularExpressionValidator ID="Regulartxtquantity"
                                        runat="server" ErrorMessage="*กรอกเฉพาะตัวเลขเท่านั้น" SetFocusOnError="true"
                                        Display="None" ControlToValidate="txtquantity" ValidationExpression="^[1-9'\s]{1,10}$"
                                        ValidationGroup="addpurchase" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="Validatortxtquantity" runat="Server"
                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Regulartxtquantity" Width="180" />



                                    <div class="form-group">
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-2">
                                            <font color="#1A5276"><small class="list-group-item-heading text_right">
                                <b>Expenditure </b></small></font>
                                            <small>
                                                <p class="list-group-item-text">งบประมาณที่ตั้งไว้</p>
                                            </small>
                                        </div>
                                        <div class="col-sm-5">
                                            <asp:TextBox ID="txtexpenditure" runat="server" ValidationGroup="addpurchase" OnTextChanged="textChanged" AutoPostBack="true"
                                                placeholder="กรุณากรอกงบประมาณเพื่อขอซื้ออุปกรณ์..." Visible="true" CssClass="form-control left"></asp:TextBox>

                                        </div>
                                        <div class="col-sm-1">
                                            <asp:TextBox ID="txtbath" runat="server" Enabled="false" CssClass="form-control left col-sm-12" Text="บาท"></asp:TextBox>
                                        </div>

                                        <asp:RequiredFieldValidator ID="RequiredFieldtxtexpenditure" runat="server"
                                            ControlToValidate="txtexpenditure" Display="None" SetFocusOnError="true"
                                            ErrorMessage="*กรุณากรอกงบประมาณเพื่อขอซื้ออุปกรณ์" ValidationGroup="addpurchase" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalltxtexpenditure" runat="Server"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldtxtexpenditure" Width="180" />

                                        <asp:RegularExpressionValidator ID="Regulartxtexpenditure"
                                            runat="server" ErrorMessage="*กรอกเฉพาะตัวเลขเท่านั้น" SetFocusOnError="true"
                                            Display="None" ControlToValidate="txtexpenditure" ValidationExpression="^[0-9',.\s]{1,10}$"
                                            ValidationGroup="addpurchase" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="Validatortxtexpenditure" runat="Server"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Regulartxtexpenditure" Width="180" />


                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-2">
                                            <font color="#1A5276"><small class="list-group-item-heading text_right">
                                <b>Cost Center </b></small></font>
                                            <small>
                                                <p class="list-group-item-text">ศูนย์งบประมาณ</p>
                                            </small>
                                        </div>
                                        <div class="col-sm-6">
                                            <%--  <asp:TextBox ID="TextBox1" runat="server" Visible="false" CssClass="form-control left" Text='<%# Eval("m0_location_idx") %>'></asp:TextBox>--%>
                                            <asp:DropDownList ID="ddlcostCenter" ValidationGroup="addpurchase" OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                runat="server" CssClass="form-control fa-align-left"
                                                Enabled="true">
                                            </asp:DropDownList>


                                            <asp:RequiredFieldValidator ID="RequiredFieldddlcostCenter" runat="server" InitialValue="0"
                                                ControlToValidate="ddlcostCenter" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกงบประมาณเพื่อขอซื้ออุปกรณ์" ValidationGroup="addpurchase" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCallddlcostCenter" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldddlcostCenter" Width="180" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-2">
                                            <font color="#1A5276"><small class="list-group-item-heading text_right">
                                <b>Location </b></small></font>
                                            <small>
                                                <p class="list-group-item-text">สถานที่</p>
                                            </small>
                                        </div>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txt_location" runat="server" Visible="false" CssClass="form-control left" Text='<%# Eval("m0_location_idx") %>'></asp:TextBox>
                                            <asp:DropDownList ID="ddllocation"
                                                runat="server" CssClass="form-control fa-align-left"
                                                Enabled="true">
                                            </asp:DropDownList>


                                            <asp:RequiredFieldValidator ID="RequiredFieldddllocation" runat="server" InitialValue="0"
                                                ControlToValidate="ddllocation" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกงบประมาณเพื่อขอซื้ออุปกรณ์" ValidationGroup="addpurchase" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCallddllocation" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldddllocation" Width="180" />


                                        </div>

                                        <div class="col-sm-2">
                                            <asp:LinkButton CssClass="btn btn-primary btn-md pull-right" Visible="true" ID="btnaddpurchase" ValidationGroup="addpurchase" data-toggle="tooltip" title="เพิ่มรายการขอซื้อ" runat="server"
                                                CommandName="add_purchase" OnCommand="btn_command"><i class="glyphicon glyphicon-plus-sign"></i> เพิ่มรายการขอซื้อ</asp:LinkButton>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-1"></div>
                                        <%-- <div class="col-sm-8">
                                    <asp:LinkButton CssClass="btn btn-primary pull-right" Visible="true" ID="LinkButton3" data-toggle="tooltip" title="เพิ่มรายการขอซื้อ" runat="server"
                                        CommandName="savepurchase"  OnClick="btnUpdate_Click"  OnCommand="btn_command" ValidationGroup="insertpurchase"><i class="glyphicon glyphicon-plus-sign"></i> เพิ่มรายการขอซื้อ</asp:LinkButton>
                                </div>--%>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-10">
                                            <%--  สรุปรายการขอซื้อ--%>
                                            <asp:HyperLink ID="setFocusedit" runat="server"></asp:HyperLink>
                                            <asp:GridView ID="gvpurchaselist" Visible="true" runat="server" AutoGenerateColumns="false"
                                                CssClass="table table-striped table-hover" GridLines="None"
                                                HeaderStyle-CssClass="info" HeaderStyle-Height="30px" AllowPaging="true" PageSize="5"
                                                OnPageIndexChanging="gvPageIndexChanging" OnRowDataBound="gvRowDataBound"
                                                OnRowEditing="gvRowEditing" OnRowUpdating="gvRowUpdating" OnRowDeleting="gvRowDeleted" OnRowCancelingEdit="gvRowCancelingEdit">
                                                <PagerStyle CssClass="pageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                                                    FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">ยังไม่มีรายการขอซื้อ</div>
                                                </EmptyDataTemplate>
                                                <Columns>
                                                    <asp:TemplateField HeaderText="#" HeaderStyle-CssClass="text-center"
                                                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbnumber" runat="server" Visible="false" />
                                                            <%# (Container.DataItemIndex +1) %>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <div class="panel-body">
                                                                <div class="form-horizontal" role="form">

                                                                    <div class="form-group">
                                                                        <asp:Label ID="Label4" CssClass="col-sm-3 control-label" runat="server" Text="ประเภทขอซื้อ" />
                                                                        <div class="col-sm-7">
                                                                            <asp:TextBox ID="txtupdate_purchasetype" runat="server" Visible="false"
                                                                                CssClass="form-control" Text='<%# Eval("purchase_type_idx")%>' />
                                                                            <asp:DropDownList ID="ddlpurchasetype_edit"
                                                                                CssClass="form-control fa-align-left" runat="server">
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <asp:Label ID="Label12" CssClass="col-sm-3 control-label" runat="server" Text="ประเภทอุปกรณ์" />
                                                                        <div class="col-sm-7">
                                                                            <asp:TextBox ID="txtupdate_equipmenttype" runat="server" Visible="false"
                                                                                CssClass="form-control" Text='<%# Eval("equipment_idx")%>' />
                                                                            <asp:DropDownList ID="ddlequipment_edit" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                                                CssClass="form-control fa-align-left" runat="server">
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <asp:Label ID="lbtiitlespec" CssClass="col-sm-3 control-label" runat="server" Text="สเปคเครื่อง" />
                                                                        <div class="col-sm-7">
                                                                            <asp:TextBox ID="txtupdate_spec" runat="server" Visible="false" CssClass="form-control"
                                                                                Text='<%# Eval("spec_type_idx")%>' />
                                                                            <asp:DropDownList ID="ddlspec_edit" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                                                CssClass="form-control fa-align-left" runat="server">
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <asp:Label ID="Label14" CssClass="col-sm-3 control-label" runat="server" Text="รายละเอียดสเปคเครื่อง" />
                                                                        <div class="col-sm-7">
                                                                            <asp:TextBox ID="txtupdate_detailspec" Enabled="false" MaxLength="6"
                                                                                TextMode="MultiLine" runat="server" CssClass="form-control" Text='<%# Eval("details_spec")%>' />
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <asp:Label ID="lb_expenditure" CssClass="col-sm-3 control-label" runat="server" Text="งบประมาณ" />
                                                                        <div class="col-sm-7">
                                                                            <asp:TextBox ID="txtupdate_expenditure" runat="server" CssClass="form-control"
                                                                                Text='<%# Eval("expenditure")%>' />
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="col-sm-4 col-sm-offset-7">
                                                                            <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" runat="server"
                                                                                CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')">
                                                                                <i class="fa fa-check"></i></asp:LinkButton>
                                                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default" runat="server"
                                                                                CommandName="Cancel"><i class="fa fa-times"></i></asp:LinkButton>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="ประเภทขอซื้อ" HeaderStyle-CssClass="text-center"
                                                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <small>
                                                                <asp:Label ID="_details_purchase" Visible="false" runat="server" CssClass="col-sm-12"
                                                                    Text='<%# Eval("details_purchase") %>'></asp:Label>
                                                                <asp:Label ID="idx_costcenter" Visible="false" runat="server" CssClass="col-sm-12"
                                                                    Text='<%# Eval("costcenter_idx") %>'></asp:Label>
                                                                <asp:Label ID="idx_location" Visible="false" runat="server" CssClass="col-sm-12"
                                                                    Text='<%# Eval("m0_location_idx") %>'></asp:Label>
                                                                <asp:Label ID="purchase_type" runat="server" CssClass="col-sm-12"
                                                                    Text='<%# Eval("purchase_type") %>'></asp:Label>
                                                                <asp:Label ID="idx_purchase" Visible="false" runat="server" CssClass="col-sm-12"
                                                                    Text='<%# Eval("purchase_type_idx") %>'></asp:Label>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="อุปกรณ์ที่ซื้อ" HeaderStyle-CssClass="text-center"
                                                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <small>

                                                                <asp:Label ID="equipment_name" runat="server" CssClass="col-sm-12"
                                                                    Text='<%# Eval("equipment_name") %>'></asp:Label>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="สเปค" HeaderStyle-CssClass="text-center"
                                                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <small>
                                                                <asp:Label ID="spec_type" runat="server" CssClass="col-sm-12"
                                                                    Text='<%# Eval("spec_type") %>'></asp:Label>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="รายละเอียด" HeaderStyle-CssClass="text-center"
                                                        ItemStyle-HorizontalAlign="left" HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <small>
                                                                <asp:Label ID="details_spec" runat="server" CssClass="col-sm-12"
                                                                    Text='<%# Eval("details_spec") %>'></asp:Label>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="จำนวน" HeaderStyle-CssClass="text-center"
                                                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <small>
                                                                <asp:Label ID="quantity" runat="server" CssClass="col-sm-12"
                                                                    Text='<%# Eval("quantity") %>'></asp:Label>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="งบประมาณ" HeaderStyle-CssClass="text-center"
                                                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <small>
                                                                <asp:Label ID="expenditure" runat="server" CssClass="col-sm-12"
                                                                    Text='<%# Eval("expenditure") %>'></asp:Label>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="จัดการ" HeaderStyle-CssClass="text-center"
                                                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <small>

                                                                <asp:LinkButton CssClass="btn btn-warning btn-sm" Visible="true" ID="Edit" data-toggle="tooltip" title="แก้ไขรายการ" runat="server" CommandName="Edit" OnCommand="btn_command" ValidationGroup="insertpurchase"> แก้ไข</asp:LinkButton>

                                                                <asp:LinkButton CssClass="btn btn-danger btn-sm" Visible="true" ID="btndelete" data-toggle="tooltip" title="ลบรายการ" runat="server" CommandName="Delete" OnCommand="btn_command" ValidationGroup="insertpurchase" OnClientClick="return confirm('คุณต้องการลบรายการขอซื้อนี้ใช่หรือไม่ ?')"> ลบ</asp:LinkButton>

                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                            <asp:HyperLink ID="Focusset" runat="server"></asp:HyperLink>

                                        </div>
                                    </div>




                                    <%--    อัพโหลดเอกสาร--%>
                                    <asp:Panel ID="panel_title_uploadfile" runat="server" Visible="true">
                                        <div class="form-group">
                                            <div class="col-sm-1"></div>
                                            <div class="col-sm-10">
                                                <div class="panel panel-info">
                                                    <div class="panel-heading">
                                                        <i class="glyphicon glyphicon-paperclip"></i>&nbsp;
                                    <b>แนบเอกสารสำคัญ</b>
                                                    </div>
                                                    <div class="panel-body">

                                                        <asp:Panel ID="panel_upload_memo" runat="server" Visible="true">
                                                            <div class="form-group">
                                                                <div class="col-sm-3">
                                                                    <font color="#1A5276"><small class="list-group-item-heading text_right">
                                <b>Select file memo</b></small></font>
                                                                    <small>
                                                                        <p class="list-group-item-text">เลือกไฟล์ memo การขอซื้อ</p>
                                                                    </small>
                                                                </div>

                                                                <div class="col-sm-4">
                                                                    <asp:FileUpload ID="uploadfile_memo" Font-Size="small" ViewStateMode="Enabled" runat="server"
                                                                        CssClass="control-label" accept="jpg|pdf" />
                                                                </div>


                                                                <asp:RequiredFieldValidator ID="RequiredFielduploadfile_memo" runat="server"
                                                                    ControlToValidate="uploadfile_memo" Display="None" SetFocusOnError="true"
                                                                    ErrorMessage="*กรุณาเลือกไฟล์ jpg หรือ pdf*" ValidationGroup="Saveinsert" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalluploadfile_memo" runat="Server"
                                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFielduploadfile_memo" Width="220" />

                                                                <asp:RegularExpressionValidator ID="RegularExpresuploadfile_memo"
                                                                    runat="server" ErrorMessage="*กรุณาเลือกเฉพาะไฟล์ jpg หรือ pdf*" SetFocusOnError="true"
                                                                    Display="None" ControlToValidate="uploadfile_memo" ValidationExpression="[a-zA-Z\\].*(.jpg|.JPG|.pdf|.PDF)$"
                                                                    ValidationGroup="Saveinsert" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="Validatoruploadfile_memo" runat="Server"
                                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpresuploadfile_memo" Width="220" />


                                                            </div>
                                                        </asp:Panel>

                                                        <asp:Panel ID="panel_upload_organization" runat="server" Visible="true">
                                                            <div class="form-group">
                                                                <div class="col-sm-3">
                                                                    <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                    <b>Select file organization</b></small></font>
                                                                    <small>
                                                                        <p class="list-group-item-text">เลือกไฟล์ organization </p>
                                                                    </small>
                                                                </div>

                                                                <div class="col-sm-6">
                                                                    <asp:FileUpload ID="uploadfile_organization" Font-Size="small" ViewStateMode="Enabled" runat="server"
                                                                        CssClass="control-label" accept="jpg|pdf" />
                                                                </div>
                                                            </div>


                                                            <asp:RequiredFieldValidator ID="RequiredFielduploadfile_organization" runat="server"
                                                                ControlToValidate="uploadfile_organization" Display="None" SetFocusOnError="true"
                                                                ErrorMessage="*กรุณาเลือกไฟล์ jpg หรือ pdf*" ValidationGroup="Saveinsert" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalluploadfile_organization" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFielduploadfile_organization" Width="220" />

                                                            <asp:RegularExpressionValidator ID="RegularExuploadfile_organization"
                                                                runat="server" ErrorMessage="*กรุณาเลือกเฉพาะไฟล์ jpg หรือ pdf*" SetFocusOnError="true"
                                                                Display="None" ControlToValidate="uploadfile_organization" ValidationExpression="[a-zA-Z\\].*(.jpg|.JPG|.pdf|.PDF)$"
                                                                ValidationGroup="Saveinsert" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="Validatoruploadfile_organization" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExuploadfile_organization" Width="220" />



                                                        </asp:Panel>

                                                        <asp:Panel ID="panel_upload_cutoff" runat="server" Visible="true">
                                                            <div class="form-group">
                                                                <div class="col-sm-3">
                                                                    <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                            <b>Select file Cut off</b></small></font>
                                                                    <small>
                                                                        <p class="list-group-item-text">เลือกเอกสารการตัดชำรุด </p>
                                                                    </small>
                                                                </div>

                                                                <div class="col-sm-6">
                                                                    <asp:FileUpload ID="uploadfile_cutoff" Font-Size="small" ViewStateMode="Enabled" runat="server"
                                                                        CssClass="control-label" accept="jpg|pdf" />

                                                                </div>
                                                            </div>

                                                            <asp:RequiredFieldValidator ID="RequiredFielduploadfile_cutoff" runat="server"
                                                                ControlToValidate="uploadfile_cutoff" Display="None" SetFocusOnError="true"
                                                                ErrorMessage="*กรุณาเลือกไฟล์ jpg หรือ pdf*" ValidationGroup="Saveinsert" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalluploadfile_cutoff" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFielduploadfile_cutoff" Width="220" />

                                                            <asp:RegularExpressionValidator ID="RegularExpresuploadfile_cutoff"
                                                                runat="server" ErrorMessage="*กรุณาเลือกเฉพาะไฟล์ jpg หรือ pdf*" SetFocusOnError="true"
                                                                Display="None" ControlToValidate="uploadfile_cutoff" ValidationExpression="[a-zA-Z\\].*(.jpg|.JPG|.pdf|.PDF)$"
                                                                ValidationGroup="Saveinsert" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="Validatoruploadfile_cutoff" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpresuploadfile_cutoff" Width="220" />


                                                        </asp:Panel>

                                                        <asp:Panel ID="panel_upload_refercutoff" runat="server" Visible="true">
                                                            <div class="form-group">
                                                                <div class="col-sm-3">
                                                                    <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                            <b>Select file refer Cut off</b></small></font>
                                                                    <small>
                                                                        <p class="list-group-item-text">เลือกเอกสารยืนยันการตัดชำรุด </p>
                                                                    </small>
                                                                </div>

                                                                <div class="col-sm-6">
                                                                    <asp:FileUpload ID="uploadfile_refercutoff" Font-Size="small" ViewStateMode="Enabled"
                                                                        runat="server" CssClass="control-label" accept="jpg|pdf" />

                                                                </div>
                                                            </div>

                                                            <asp:RequiredFieldValidator ID="RequiredFieluploadfile_refercutoff" runat="server"
                                                                ControlToValidate="uploadfile_refercutoff" Display="None" SetFocusOnError="true"
                                                                ErrorMessage="*กรุณาเลือกไฟล์ jpg หรือ pdf*" ValidationGroup="Saveinsert" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalluploadfile_refercutoff" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieluploadfile_refercutoff" Width="220" />

                                                            <asp:RegularExpressionValidator ID="RegularExpreuploadfile_refercutoff"
                                                                runat="server" ErrorMessage="*กรุณาเลือกเฉพาะไฟล์ jpg หรือ pdf*" SetFocusOnError="true"
                                                                Display="None" ControlToValidate="uploadfile_refercutoff" ValidationExpression="[a-zA-Z\\].*(.jpg|.JPG|.pdf|.PDF)$"
                                                                ValidationGroup="Saveinsert" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="Validatoruploadfile_refercutoff" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpreuploadfile_refercutoff" Width="220" />

                                                        </asp:Panel>
                                                        <div class="col-sm-3"></div>
                                                        <p class="help-block"><font color="#ff6666">**นามสกุลที่รองรับ  jpg และ pdf  </font></p>
                                                    </div>

                                                </div>


                                            </div>
                                        </div>



                                    </asp:Panel>





                                    <%--    <asp:RequiredFieldValidator ID="Validator_uplodefile" runat="server" ErrorMessage="***กรุณาเลือกเฉพาะไฟล์ jpg***" ForeColor="Red" ValidationGroup="Save" CssClass="row-validate" ControlToValidate="upload_file"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="ValidateEx" runat="server"
                                ValidationExpression="[a-zA-Z\\].*(.jpg|.JPG)$" ControlToValidate="upload_file" ValidationGroup="Saveinsert" Font-Size="Small" ForeColor="Red"
                                ErrorMessage="***กรุณาเลือกเฉพาะไฟล์ jpg***"></asp:RegularExpressionValidator>--%>


                                    <asp:UpdatePanel ID="updatebtnsave" runat="server">
                                        <ContentTemplate>

                                            <div class="form-group">
                                                <div class="col-sm-1"></div>
                                                <div class="col-sm-10">
                                                    <asp:LinkButton CssClass="btn btn-success pull-right" Visible="false" ID="btnsavepurchase" data-toggle="tooltip" title="บันทึกรายการขอซื้อ" runat="server" ValidationGroup="Saveinsert"
                                                        CommandName="savepurchase" OnCommand="btn_command" OnClientClick="return confirm('คุณต้องการบันทึกรายการขอซื้อนี้ใช่หรือไม่ ?')"> บันทึกรายการขอซื้อ</asp:LinkButton>
                                                </div>
                                            </div>

                                        </ContentTemplate>

                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnsavepurchase" />
                                        </Triggers>


                                    </asp:UpdatePanel>


                                    <div class="pull-right">
                                        <p class="help-block"><font color="#000000">FM_IT_MN_001/09 Rev.02 มีผลบังคับใช้วันที่ (30 Oct 15) </font></p>
                                    </div>
                                </div>
                            </div>
                    </EditItemTemplate>
                </asp:FormView>

                <div class="form-group">
                    <%--  แก้ไขรายการขอซื้อ --%>
                    <asp:GridView ID="gvEditpurchaselist" Visible="true" runat="server" AutoGenerateColumns="false"
                        CssClass="table table-striped table-hover" GridLines="None" DataKeyNames="u2_purchase_idx"
                        HeaderStyle-CssClass="info" HeaderStyle-Height="30px" AllowPaging="true" PageSize="5"
                        OnPageIndexChanging="gvPageIndexChanging" OnRowDataBound="gvRowDataBound"
                        OnRowEditing="gvRowEditing" OnRowUpdating="gvRowUpdating" OnRowDeleting="gvRowDeleted" OnRowCancelingEdit="gvRowCancelingEdit">
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                            FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">ยังไม่มีรายการขอซื้อ</div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-left"
                                ItemStyle-HorizontalAlign="left" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <asp:Label ID="labelu2purchaseidx" runat="server" Visible="false" Text='<%# Eval("u2_purchase_idx") %>' />
                                    <%# (Container.DataItemIndex +1) %>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">

                                            <div class="form-group">
                                                <asp:Label ID="Labesssl4" CssClass="col-sm-3 control-label" runat="server" Text="ประเภทขอซื้อ" />
                                                <div class="col-sm-7">
                                                    <asp:Label ID="labelu0purchaseidx" runat="server" Visible="false" Text='<%# Eval("u0_purchase_idx") %>' />
                                                    <asp:TextBox ID="txtedit_purchasetype" runat="server" Visible="false" CssClass="form-control" Text='<%# Eval("m0_purchase_idx")%>' />
                                                    <asp:DropDownList ID="ddlpurchase_typeedit" Enabled="false"
                                                        CssClass="form-control fa-align-left" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <asp:Label ID="Lassssbel12" CssClass="col-sm-3 control-label" runat="server" Text="ประเภทอุปกรณ์" />
                                                <div class="col-sm-7">
                                                    <asp:TextBox ID="txtedit_equipmenttype" runat="server" Visible="false" CssClass="form-control" Text='<%# Eval("m0_equipment_idx")%>' />
                                                    <asp:DropDownList ID="ddlequipment_typeedit" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                        CssClass="form-control fa-align-left" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <asp:Label ID="Label29" CssClass="col-sm-3 control-label" runat="server" Text="ขอซื้อให้ตำแหน่ง" />
                                                <div class="col-sm-7">
                                                    <asp:TextBox ID="txteditposition" runat="server" Visible="false" CssClass="form-control" Text='<%# Eval("rpos_idx")%>' />
                                                    <asp:DropDownList ID="ddleditposition"
                                                        CssClass="form-control fa-align-left" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <asp:Label ID="Label30" CssClass="col-sm-3 control-label" runat="server" Text="เหตุผลการขอซื้อ" />
                                                <div class="col-sm-7">
                                                    <asp:TextBox ID="txteditargumentation" runat="server" MaxLength="6" TextMode="MultiLine" Visible="true" Text='<%# Eval("argumentation")%>' CssClass="form-control" />
                                                    <%--    <asp:DropDownList ID="DropDownList3"
                                                    CssClass="form-control fa-align-left" runat="server">
                                                </asp:DropDownList>--%>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <asp:Label ID="Labelm0_spec_idx" CssClass="col-sm-3 control-label" runat="server" Text="สเปคเครื่อง" />
                                                <div class="col-sm-7">
                                                    <asp:TextBox ID="txtupdate_m0_spec_idx" runat="server" Visible="false" CssClass="form-control" Text='<%# Eval("m0_spec_idx")%>' />
                                                    <asp:DropDownList ID="ddlm0_spec_idx" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                        CssClass="form-control fa-align-left" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <asp:Label ID="Labssssel14" CssClass="col-sm-3 control-label" runat="server" Text="รายละเอียดสเปคเครื่อง" />
                                                <div class="col-sm-7">
                                                    <asp:TextBox ID="txtedit_detailspec" Enabled="true" MaxLength="6" TextMode="MultiLine" runat="server" CssClass="form-control" Text='<%# Eval("items_details")%>' />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <asp:Label ID="Label27" CssClass="col-sm-3 control-label" runat="server" Text="จำนวน" />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtedit_items_quantity" runat="server" CssClass="form-control" Text='<%# Eval("items_quantity")%>' />
                                                </div>
                                                <asp:Label ID="Label28" CssClass="col-sm-1 control-label" runat="server" Text="หน่วยนับ" />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txteditUnit" runat="server" Enabled="false" Text='<%# Eval("unitname")%>' CssClass="form-control" />
                                                    <%--  <asp:DropDownList ID="ddleditUnit" Enabled="false"
                                                    CssClass="form-control fa-align-left" runat="server">
                                                </asp:DropDownList>--%>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <asp:Label ID="lb_expenditjure" CssClass="col-sm-3 control-label" runat="server" Text="งบประมาณที่ตี้งไว้" />
                                                <div class="col-sm-7">
                                                    <asp:TextBox ID="txtedit_expenditure" runat="server" CssClass="form-control" Text='<%# Eval("items_expenditure")%>' />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <asp:Label ID="Label31" CssClass="col-sm-3 control-label" runat="server" Text="Cost Center" />
                                                <div class="col-sm-7">
                                                    <asp:TextBox ID="txtcostcenter_idxedit" runat="server" Visible="false" CssClass="form-control" Text='<%# Eval("costcenter_idx")%>' />
                                                    <asp:DropDownList ID="ddledit_costcenter"
                                                        CssClass="form-control fa-align-left" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <asp:Label ID="Label32" CssClass="col-sm-3 control-label" runat="server" Text="สถานที่" />
                                                <div class="col-sm-7">
                                                    <asp:TextBox ID="txteditlocation_idx" runat="server" Visible="false" CssClass="form-control" Text='<%# Eval("location_idx")%>' />
                                                    <asp:DropDownList ID="ddledit_location"
                                                        CssClass="form-control fa-align-left" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-10"></div>
                                                <div class="col-sm-2">
                                                    <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" runat="server" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')">บันทึก</asp:LinkButton>
                                                    <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default" runat="server" CommandName="Cancel">ยกเลิก</asp:LinkButton>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </EditItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="วันที่ขอซื้อ" HeaderStyle-CssClass="text-center"
                                ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="items_datess" runat="server" CssClass="col-sm-12"
                                            Text='<%# Eval("date_purchase") %>'></asp:Label>

                                        <asp:Label ID="lbargumentation_edit" runat="server" Visible="false" CssClass="col-sm-12"
                                            Text='<%# Eval("argumentation") %>'></asp:Label>
                                        <asp:Label ID="lbname_purchase_edit1" runat="server" Visible="false" CssClass="col-sm-12"
                                            Text='<%# Eval("type_purchase") %>'></asp:Label>
                                        <asp:Label ID="lbdoccode_edit" runat="server" Visible="false" CssClass="col-sm-12"
                                            Text='<%# Eval("doc_code") %>'></asp:Label>

                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ประเภทอุปกรณ์" HeaderStyle-CssClass="text-center"
                                ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="items_type_equipment" runat="server" CssClass="col-sm-12"
                                            Text='<%# Eval("type_equipment") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="รายละเอียด" HeaderStyle-CssClass="text-center"
                                ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="items_detailssss" runat="server" CssClass="col-sm-12"
                                            Text='<%# Eval("items_details") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="จำนวน" HeaderStyle-CssClass="text-center"
                                ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="items_typeqsss" runat="server" CssClass="col-sm-12"
                                            Text='<%# Eval("items_quantity") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="งบประมาณ" HeaderStyle-CssClass="text-center"
                                ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="items_expenditursse" runat="server" CssClass="col-sm-12"
                                            Text='<%# Eval("items_expenditure") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="จัดการ" HeaderStyle-CssClass="text-center"
                                ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>

                                        <asp:LinkButton CssClass="btn btn-warning btn-sm" Visible="true" ID="Edit" data-toggle="tooltip" title="แก้ไขรายการ" runat="server" CommandName="Edit" OnCommand="btn_command" ValidationGroup="insertpurchase"> แก้ไข</asp:LinkButton>

                                        <asp:LinkButton CssClass="btn btn-danger btn-sm" Visible="true" ID="Delete" data-toggle="tooltip" CommandArgument='<%#Eval("u2_purchase_idx") %>' title="ลบรายการ" runat="server" CommandName="cmddelete" OnCommand="btn_command" OnClientClick="return confirm('คุณต้องการลบรายการขอซื้อนี้ใช่หรือไม่ ?')"> ลบ</asp:LinkButton>

                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">

                    <asp:GridView ID="GridView1" Visible="true" runat="server" HeaderStyle-CssClass="info" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-hover " OnRowDataBound="gvRowDataBound">

                        <Columns>
                            <asp:TemplateField HeaderText="เอกสารในการขอซื้อ">
                                <ItemTemplate>
                                    <div class="col-lg-11">
                                        <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />

                                    </div>
                                    <div class="col-lg-1">
                                        <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-primary btn-sm pull-right" data-original-title="Download" Target="_blank"><i class="fa fa-download"></i></asp:HyperLink>

                                        <asp:HiddenField runat="server" ID="hidFile11" Value='<%# Eval("Download") %>' />
                                    </div>

                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>
                </div>
            </div>

            <asp:Panel ID="paneledituploadfile" runat="server" Visible="false">
                <div class="form-group">
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <i class="glyphicon glyphicon-paperclip"></i>&nbsp;
                        <b></b>แก้ไขไฟล์เอกสารแนบ
                            </div>
                            <div class="panel-body">

                                <%--    แก้ไขอัพโหลดเอกสาร--%>

                                <asp:Panel ID="panel_upload_memo_edit" runat="server" Visible="true">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                <b>Select file memo</b></small></font>
                                                <small>
                                                    <p class="list-group-item-text">เลือกไฟล์ memo การขอซื้อ</p>
                                                </small>
                                            </div>

                                            <div class="col-sm-4">
                                                <asp:FileUpload ID="uploadfile_memo_edit" Font-Size="small" ViewStateMode="Enabled" runat="server" CssClass="control-label" accept="jpg|pdf" />
                                                <%--  <br />--%>
                                            </div>


                                        </div>
                                    </div>
                                </asp:Panel>

                                <asp:Panel ID="panel_upload_organization_edit" runat="server" Visible="true">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                <b>Select file organization</b></small></font>
                                                <small>
                                                    <p class="list-group-item-text">เลือกไฟล์ organization </p>
                                                </small>
                                            </div>

                                            <div class="col-sm-6">
                                                <asp:FileUpload ID="uploadfile_organization_edit" Font-Size="small" ViewStateMode="Enabled" runat="server" CssClass="control-label" accept="jpg|pdf" />
                                                <%--   <p class="help-block"><font color="#ff6666">**นามสกุลที่รองรับ  jpg และ pdf  </font></p>--%>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>

                                <asp:Panel ID="panel_upload_cutoff_edit" runat="server" Visible="true">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                            <b>Select file Cut off</b></small></font>
                                                <small>
                                                    <p class="list-group-item-text">เลือกเอกสารการตัดชำรุด </p>
                                                </small>
                                            </div>

                                            <div class="col-sm-6">
                                                <asp:FileUpload ID="uploadfile_cutoff_edit" Font-Size="small" ViewStateMode="Enabled" runat="server" CssClass="control-label" accept="jpg|pdf" />
                                                <%--   <p class="help-block"><font color="#ff6666">**นามสกุลที่รองรับ  jpg และ pdf  </font></p>--%>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>

                                <asp:Panel ID="panel_upload_refercutoff_edit" runat="server" Visible="true">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                            <b>Select file refer Cut off</b></small></font>
                                                <small>
                                                    <p class="list-group-item-text">เลือกเอกสารยืนยันการตัดชำรุด </p>
                                                </small>
                                            </div>

                                            <div class="col-sm-6">
                                                <asp:FileUpload ID="uploadfile_refercutoff_edit" Font-Size="small" ViewStateMode="Enabled" runat="server" CssClass="control-label" accept="jpg|pdf" />
                                                <%--   <p class="help-block"><font color="#ff6666">**นามสกุลที่รองรับ  jpg และ pdf  </font></p>--%>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <div class="col-md-3"></div>
                                <p class="help-block"><font color="#ff6666">**นามสกุลที่รองรับ  jpg และ pdf  </font></p>
                            </div>

                        </div>
                    </div>
                </div>

            </asp:Panel>
            <asp:UpdatePanel ID="updatebtnsaveEdit1" runat="server">
                <ContentTemplate>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <asp:LinkButton CssClass="btn btn-success pull-right" Visible="false" ID="btnupdatepurchase" data-toggle="tooltip" title="บันทึกรายการขอซื้อ" runat="server"
                                CommandName="updatepurchase" OnCommand="btn_command" ValidationGroup="insertpurchase" OnClientClick="return confirm('คุณต้องการบันทึกรายการขอซื้อนี้ใช่หรือไม่ ?')"> บันทึกการเปลี่ยนแปลง</asp:LinkButton>
                        </div>

                    </div>
                </ContentTemplate>

                <Triggers>
                    <asp:PostBackTrigger ControlID="btnupdatepurchase" />
                </Triggers>
            </asp:UpdatePanel>

            <h5>&nbsp;</h5>
            <div class="form-group">
                <div class="col-md-12">
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <i class="glyphicon glyphicon-file"></i>&nbsp;
                        <b>performance purchase status</b> (สถานะการดำเนินการขอซื้ออุปกรณ์)
                        </div>
                        <div class="panel-body">
                            <asp:Repeater ID="history_purchase_action" runat="server">
                                <HeaderTemplate>
                                    <div class="row">
                                        <label class="col-sm-2 control-label"><small>วันที่ขอซื้อ(เวลา)</small></label>
                                        <label class="col-sm-3 control-label"><small>ผู้ดำเนินการ</small></label>
                                        <label class="col-sm-2 control-label"><small>ดำเนินการ</small></label>
                                        <label class="col-sm-2 control-label"><small>ผลการดำเนินการ</small></label>
                                        <label class="col-sm-2 control-label"><small>หมายเหตุ</small></label>
                                    </div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <small><span><%# Eval("date_purchasequipment") %>(<%# Eval("time_purchasequipment") %>)</span></small>
                                        </div>
                                        <div class="col-sm-3">
                                            <small><span><%# Eval("actor_name_purchase") %></span></small>
                                        </div>
                                        <div class="col-sm-2">
                                            <small><span><%# Eval("node_name_purchase") %></span></small>
                                        </div>
                                        <div class="col-sm-2">
                                            <small><span><%# Eval("node_status_purchase") %></span></small>
                                        </div>
                                        <div class="col-sm-2">
                                            <small><span><%# Eval("details_purchase") %></span></small>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </div>

            <%-- </div>--%>
        </asp:View>

        <%--view การจัดการข้อมูลต่างๆ --%>

        <asp:View ID="viewmanage_purchasequipment" runat="server">
            <asp:HyperLink ID="setFocus_viewManage" runat="server"></asp:HyperLink>
            <div class="col-sm-12" runat="server" id="content_manage_purchasequipment">
                <%--  <div class="form-group">
                    <asp:LinkButton CssClass="btn btn-danger" Visible="true" ID="LinkButton1" data-toggle="tooltip" title="ยกเลิก" runat="server"
                        CommandName="hiddenboxcreate" OnCommand="btn_command"><i class="glyphicon glyphicon-remove-sign"></i> ยกเลิก</asp:LinkButton>
                </div>--%>
                <div class="form-group">

                    <asp:LinkButton ID="LinkButton5" runat="server"
                        CommandName="viewmanu1" CssClass="btn btn-default" data-toggle="tooltip" title="ย้อนกลับ"
                        OnCommand="btn_command"><i class="fa fa-chevron-circle-left" aria-hidden="true"></i>&nbsp;ย้อนกลับ</asp:LinkButton>
                </div>
                <asp:FormView ID="fvinformation_purchasequipment" OnDataBound="formview_databound"
                    runat="server" DefaultMode="Edit" Width="100%">
                    <EditItemTemplate>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <i class="glyphicon glyphicon-user"></i>&nbsp;
                        <b>รายละเอียดข้อมูลผู้ขอซื้ออุปกรณ์</b>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                     <div class="form-group">
                                        <label class="col-sm-2 control-label">รหัสพนักงาน :</label>
                                        <div class="col-sm-4 control-label textleft">
                                                <asp:Label ID="lb_emps_code_purchase" Text='<%# Eval("empcode_purchase") %>' runat="server" />
                                            </div>
                                       
                                        <label class="col-sm-2 control-label">ชื่อ - นามสกุล  :</label>
                                         <div class="col-sm-4 control-label textleft">
                                           
                                                <asp:Label ID="fullname_purchase_approve" Text='<%# Eval("fullname_purchase") %>' runat="server" />
                                            </div>
                                        </div>
                                   

                                   <div class="form-group">
                                        <label class="col-sm-2 control-label">องค์กร :</label>
                                        <div class="col-sm-4 control-label textleft">
                                            <asp:Label ID="Label7" Text='<%# Eval("org_name_purchase") %>' runat="server" />
                                        </div>
                                      <label class="col-sm-2 control-label">ฝ่าย :</label>
                                       
                                       <div class="col-sm-4 control-label textleft">
                                                <asp:Label ID="lbdreptnamepurchase" Text='<%# Eval("drept_name_purchase") %>' runat="server" />
                                            </div>
                                        </div>
                                    

                                  <div class="form-group">
                                        <label class="col-sm-2 control-label">แผนก :</label>
                                         <div class="col-sm-4 control-label textleft">
                                                <asp:Label ID="Label9" Text='<%# Eval("sec_name_purchase") %>' runat="server" />
                                            </div>
                                       <label class="col-sm-2 control-label">ตำแหน่ง :</label>
                                               
                                        <div class="col-sm-4 control-label textleft">
                                                <asp:Label ID="Label10" Text='<%# Eval("pos_name_purchase") %>' runat="server" />
                                            </div>
                                        </div>
                                

                                     <div class="form-group">
                                             <label class="col-sm-2 control-label">เบอร์ติดต่อ :</label>
                                         <div class="col-sm-4 control-label textleft">
                                                <asp:Label ID="Label11" Text='<%# Eval("numberphone") %>' runat="server" />
                                            </div>
                                      <label class="col-sm-2 control-label">e-mail :</label>
                                              
                                       <div class="col-sm-4 control-label textleft">
                                                <asp:Label ID="lbemail" Text='<%# Eval("email_purchase") %>' runat="server" />
                                            </div>
                                        </div>

                            </div>
                        </div>
                    </EditItemTemplate>
                </asp:FormView>

            </div>

            <div class="col-sm-12" runat="server" id="Div1">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="glyphicon glyphicon-file"></i>&nbsp;
                        <b>รายละเอียดรายการขอซื้ออุปกรณ์</b>
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal" role="form">
                            <asp:FormView ID="fvdetails_purchasequipment" OnDataBound="formview_databound"
                                runat="server" DefaultMode="Edit" Width="100%">
                                <EditItemTemplate>
                                    <asp:HiddenField ID="hidden_m0_node" runat="server" Value='<%# Eval("m0_node_idx") %>' />
                                    <asp:HiddenField ID="hidden_m0_actor" runat="server" Value='<%# Eval("m0_actor_idx") %>' />
                                    <asp:HiddenField ID="hidden_emp_idx" runat="server" Value='<%# Eval("emp_idx_purchase") %>' />
                                    <asp:HiddenField ID="hidden_emp_director" runat="server" Value='<%# Eval("emp_idx_director") %>' />
                                    <asp:HiddenField ID="hidden_status_node" runat="server" Value='<%# Eval("status_purchase") %>' />


                                    <%--    <div class="row">
                                    <div class="col-sm-3">

                                        <b><font color="#1A5276">ประเภทอุปกรณ์ที่ขอซื้อ :</font>&nbsp;</b>

                                    </div>
                                    <div class="col-sm-9">
                                        <div style="text-align: left;">
                                            <asp:Label ID="lb_namequipment_type" Text='<%# Eval("name_equipment") %>' runat="server"> </asp:Label>
                                        </div>
                                    </div>
                                </div>--%>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">รหัสเอกสาร :</label>
                                        <div class="col-sm-4 control-label textleft">
                                            <asp:Label ID="lbdocument_code" Text='<%# Eval("document_code") %>' runat="server"></asp:Label>
                                            <asp:Label ID="hidden_name_actor" Visible="false" runat="server" Text='<%# Eval("actor_name_purchase") %>' />
                                        </div>
                                        <label class="col-sm-2 control-label">วันที่ขอซื้อ :</label>
                                        <div class="col-sm-4 control-label textleft">
                                            <asp:Label ID="Labellld" Text='<%# Eval("date_purchasequipment") %>' runat="server"></asp:Label>
                                        </div>
                                    </div>

                                    <div class="form-group">


                                        <label class="col-sm-2 control-label">ประเภทการขอซื้อ :</label>
                                        <div class="col-sm-4 control-label textleft">
                                            <asp:Label ID="lbtypePurchase" Text='<%# Eval("name_purchase") %>' runat="server"></asp:Label>
                                        </div>
                                        <label class="col-sm-2 control-label">Cost Center :</label>
                                        <div class="col-sm-4 control-label textleft">
                                            <asp:Label ID="Labeaal33" Text='<%# Eval("costcenter_no") %>' runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">เหตุผลการขอซื้อ :</label>
                                        <div class="col-sm-10 control-label textleft">
                                            <asp:Label ID="Label15" Text='<%# Eval("details_purchase") %>' runat="server"></asp:Label>
                                        </div>
                                    </div>


                                    </div>

                                </EditItemTemplate>
                            </asp:FormView>
                            <br />
                            <%-- Items purchase--%>
                            <div class="form-group">



                                <div class="col-sm-12">

                                    <blockquote class="danger" style="font-size: small; background-color: lavender;">
                                        <p><i class="fa fa-list-ul" aria-hidden="true"></i>&nbsp; รายการขอซื้อ</p>

                                    </blockquote>
                                    <asp:GridView ID="gvItemspurchase" Visible="true" runat="server" AutoGenerateColumns="false"
                                        CssClass="table table-bordered" GridLines="None"
                                        HeaderStyle-CssClass="default" HeaderStyle-Height="30px" AllowPaging="true"
                                        OnRowDataBound="gvRowDataBound">


                                        <EmptyDataTemplate>
                                            <div style="text-align: center">ยังไม่มีรายการขอซื้อ</div>
                                        </EmptyDataTemplate>
                                        <Columns>
                                            <%--     <asp:TemplateField HeaderText="เลือก" HeaderStyle-CssClass="text-center"
                                            ItemStyle-HorizontalAlign="center" HeaderStyle-Width="5%" HeaderStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="cbrecipients" runat="server" AutoPostBack="true" Visible="true"
                                                    OnCheckedChanged="checkindexchange" Text='<%# Container.DataItemIndex %>' Style="color: transparent;"></asp:CheckBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                                ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <asp:Label ID="_lbItemsnumber" runat="server" Visible="true" Text='<%# Eval("u2_purchase_idx") %>' />
                                                    <%# (Container.DataItemIndex +1) %>
                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <%--  <asp:TemplateField HeaderText="วันที่ขอซื้อ" HeaderStyle-CssClass="text-center"
                                            ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="items_date" runat="server" CssClass="col-sm-12"
                                                        Text='<%# Eval("date_purchase") %>'></asp:Label>
                                                    
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="อุปกรณ์" HeaderStyle-CssClass="text-center"
                                                ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Label ID="items_type1" runat="server" CssClass="col-sm-12"
                                                            Text='<%# Eval("type_equipment") %>'></asp:Label>
                                                        <asp:Label ID="lbnodeidxItems" runat="server" Visible="false" Text='<%# Eval("node_idx") %>' />
                                                        <asp:Label ID="lbactoridxItems" runat="server" Visible="false" Text='<%# Eval("actor_idx") %>' />
                                                        <%--  <asp:Label ID="lbstatus_action" runat="server" Visible="false" Text='<%# Eval("status_node") %>' />--%>
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="รายละเอียด" HeaderStyle-CssClass="text-center"
                                                ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Label ID="items_details" runat="server" CssClass="col-sm-12"
                                                            Text='<%# Eval("items_details") %>'></asp:Label>
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="จำนวน" HeaderStyle-CssClass="text-center"
                                                ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Label ID="items_quantity" runat="server" CssClass="col-sm-12"
                                                            Text='<%# Eval("items_quantity") %>'></asp:Label>
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="งบประมาณ" HeaderStyle-CssClass="text-center"
                                                ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Label ID="items_expenditure" runat="server" CssClass="col-sm-12"
                                                            Text='<%# Eval("items_expenditure") %>'></asp:Label>
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ความคิดเห็นจากไอที" HeaderStyle-CssClass="text-center"
                                                ItemStyle-HorizontalAlign="center" Visible="false" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>

                                                        <asp:Panel ID="price_comment_complete" runat="server" Visible="false">

                                                            <label class="bg-success">:: ความคิดเห็น :: </label>
                                                            <br />
                                                            <asp:Label ID="commentit" runat="server" CssClass="col-sm-12"
                                                                Text='<%# Eval("commentfromITSupport") %>'></asp:Label><br />
                                                            <label class="bg-success">:: ราคา :: </label>
                                                            <br />
                                                            <asp:Label ID="lbprice" runat="server" CssClass="col-sm-12"
                                                                Text='<%# Eval("price_equipment") %>'></asp:Label><br />

                                                        </asp:Panel>




                                                        <asp:TextBox ID="txtcommentformITSupport" placeholder="กรอกสรุปการขอซื้อ..." Visible="false" Enabled="false"
                                                            CssClass="form-control fa-align-left" runat="server">
                                                        </asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredtxtcommentformITSupport" runat="server"
                                                            ControlToValidate="txtcommentformITSupport" Display="None"
                                                            SetFocusOnError="true" ErrorMessage="*กรุณากรอกสรุปการขอซื้อ"
                                                            ValidationGroup="approvepurchase" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatortxtcommentformITSupport" runat="Server"
                                                            HighlightCssClass="RequiredtxtcommentformITSupport" PopupPosition="topLeft"
                                                            TargetControlID="RequiredtxtcommentformITSupport" Width="180" />

                                                        <br />

                                                        <asp:TextBox ID="txtminprice" Visible="false" placeholder="ราคาเริ่มต้น..." Text='<%# Eval("min_price") %>'
                                                            CssClass="form-control" runat="server" ValidationGroup="approvepurchase"></asp:TextBox>
                                                        <asp:Label ID="lbto" Visible="false" runat="server">ถึง</asp:Label>

                                                        <asp:TextBox ID="txtmaxprice" Visible="false" placeholder="ราคามากสุด..." Text='<%# Eval("max_price") %>'
                                                            CssClass="form-control" runat="server" ValidationGroup="approvepurchase"></asp:TextBox>

                                                        <asp:RequiredFieldValidator ID="Requitxtminprice" runat="server"
                                                            ControlToValidate="txtminprice" Display="None"
                                                            SetFocusOnError="true" ErrorMessage="*กรุณากรอกราคาเริ่มต้น"
                                                            ValidationGroup="approvepurchase" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="Validattxtminprice" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight" PopupPosition="topLeft"
                                                            TargetControlID="Requitxtminprice" Width="180" />

                                                        <asp:RequiredFieldValidator ID="Requitxtmaxprice" runat="server"
                                                            ControlToValidate="txtmaxprice" Display="None"
                                                            SetFocusOnError="true" ErrorMessage="*กรุณากรอกราคามากสุด"
                                                            ValidationGroup="approvepurchase" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="Validattxtmaxprice" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight" PopupPosition="topLeft"
                                                            TargetControlID="Requitxtmaxprice" Width="180" />


                                                        <asp:RegularExpressionValidator ID="Regulartxtminprice"
                                                            runat="server" ErrorMessage="*กรอกเฉพาะตัวเลขเท่านั้น" SetFocusOnError="true"
                                                            Display="None" ControlToValidate="txtminprice" ValidationExpression="^[0-9',.\s]{1,10}$"
                                                            ValidationGroup="approvepurchase" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="Validatortxtminprice" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Regulartxtminprice" Width="180" PopupPosition="topLeft" />




                                                        <asp:RegularExpressionValidator ID="Regulartxtmaxprice"
                                                            runat="server" ErrorMessage="*กรอกเฉพาะตัวเลขเท่านั้น" SetFocusOnError="true"
                                                            Display="None" ControlToValidate="txtmaxprice" ValidationExpression="^[0-9',.\s]{1,10}$"
                                                            ValidationGroup="approvepurchase" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="Validatortxtmaxprice" runat="Server" PopupPosition="topLeft"
                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Regulartxtmaxprice" Width="180" />


                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="NO.IO" HeaderStyle-CssClass="text-center"
                                                ItemStyle-HorizontalAlign="center" Visible="false" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <%-- <asp:Label ID="items_quantity" runat="server" CssClass="col-sm-12"
                                                            Text='<%# Eval("items_quantity") %>'></asp:Label>--%>

                                                        <asp:Label ID="lbnumitemsio" runat="server" Visible="false"
                                                            class="bg-danger">**กรุณากรอกเลข IO จำนวน <%# Eval("items_quantity") %>  ชุด </asp:Label>

                                                        <asp:TextBox ID="txtNoIO" Visible="false" MaxLength="9" placeholder="กรอกเลข IO..." ValidationGroup="addionumber" Enabled="true"
                                                            CssClass="form-control fa-align-left" runat="server">
                                                        </asp:TextBox>


                                                        <asp:RequiredFieldValidator ID="RequitxtNoIO" runat="server"
                                                            ControlToValidate="txtNoIO" Display="None"
                                                            SetFocusOnError="true" ErrorMessage="*กรุณากรอกเลข IO "
                                                            ValidationGroup="approvepurchase" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValtxtNoIO" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight" PopupPosition="topLeft"
                                                            TargetControlID="RequitxtNoIO" Width="180" />

                                                        <%--   <asp:RegularExpressionValidator ID="RegulartxtNoIO"
                                                            runat="server" ErrorMessage="*กรอกเฉพาะตัวเลขเท่านั้น" SetFocusOnError="true"
                                                            Display="None" ControlToValidate="txtNoIO" ValidationExpression="^[0-9']{1,10}$"
                                                            ValidationGroup="addionumber" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatortxtNoIO" runat="Server" PopupPosition="topLeft"
                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegulartxtNoIO" Width="180" />--%>


                                                        <asp:LinkButton CssClass="btn btn-success pull-right" Visible="false" ID="btn_addionumber" ValidationGroup="addionumber" data-toggle="tooltip" title="เพิ่ม" runat="server"
                                                            CommandName="btnadd_ionumber" OnCommand="btn_command" CommandArgument='<%# Eval("u2_purchase_idx")+ ";" + Eval("items_quantity") %>'
                                                            OnClientClick="return confirm('คุณต้องการเพิ่มข้อมูลนี้ใช่หรือไม่ ?')"> เพิ่ม</asp:LinkButton>

                                                        <%--  <asp:LinkButton CssClass="btn btn-success pull-right" Visible="false" ID="LinkButton2" ValidationGroup="addionumber" data-toggle="tooltip" title="เพิ่ม" runat="server"
                                                            CommandName="btnadd_ionumber" OnCommand="btn_command" CommandArgument="1;1"
                                                            OnClientClick="return confirm('คุณต้องการเพิ่มข้อมูลนี้ใช่หรือไม่ ?')"> เพิ่ม</asp:LinkButton>--%>
                                                        <asp:Label ID="alertsuccess_io" runat="server" class="bg-success"></asp:Label>


                                                        <asp:GridView ID="gvionumber" Visible="true" runat="server" AutoGenerateColumns="false"
                                                            CssClass="table table-bordered" GridLines="None" DataKeyNames="idx"
                                                            HeaderStyle-CssClass="default" HeaderStyle-Height="5px" AllowPaging="true"
                                                            OnRowDataBound="gvRowDataBound" OnRowDeleting="gvRowDeleted">
                                                            <Columns>

                                                                <asp:TemplateField HeaderText="NO." HeaderStyle-CssClass="text-center"
                                                                    ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                                    <ItemTemplate>
                                                                        <small>

                                                                            <asp:Label ID="_lbidxnumerid" runat="server" Visible="true" CssClass="col-sm-12"
                                                                                Text='<%# Eval("idx") %>'></asp:Label>
                                                                            <asp:Label ID="Labelz1" runat="server" CssClass="col-sm-12"></asp:Label>
                                                                            <%# (Container.DataItemIndex +1) %>
                                                                        </small>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="NO.IO" HeaderStyle-CssClass="text-center"
                                                                    ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                                    <ItemTemplate>
                                                                        <small>
                                                                            <asp:Label ID="_lbnumberio" runat="server" CssClass="col-sm-12"
                                                                                Text='<%# Eval("io") %>'></asp:Label>

                                                                        </small>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="manage" HeaderStyle-CssClass="text-center"
                                                                    ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                                    <ItemTemplate>
                                                                        <small>
                                                                            <asp:LinkButton CssClass="text-trash" Visible="true" ID="btndelete" data-toggle="tooltip" title="ลบรายการ" runat="server"
                                                                                CommandName="Delete" OnCommand="btn_command" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')"><i class="glyphicon glyphicon-trash"></i> </asp:LinkButton>
                                                                        </small>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>


                                                            </Columns>

                                                        </asp:GridView>

                                                        <asp:Label ID="quantityIO" runat="server" CssClass="col-sm-12 pull-left"></asp:Label>

                                                        <asp:Table ID="tbgentextboxNOIo" runat="server">
                                                        </asp:Table>
                                                        <asp:PlaceHolder runat="server" ID="ph" />

                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="NO.Asset" Visible="false" HeaderStyle-CssClass="text-center"
                                                ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Label ID="lbnumitemsasset" runat="server" Visible="false"
                                                            class="bg-danger">**กรุณากรอกเลข Asset จำนวน <%# Eval("items_quantity") %>  ชุด </asp:Label>

                                                        <asp:TextBox ID="txtNOAsset" MaxLength="13" placeholder="กรอกเลข Asset..." Visible="false" Enabled="false"
                                                            CssClass="form-control fa-align-left" runat="server">
                                                        </asp:TextBox>

                                                        <asp:RequiredFieldValidator ID="RequitxtNOAsset" runat="server"
                                                            ControlToValidate="txtNOAsset" Display="None"
                                                            SetFocusOnError="true" ErrorMessage="*กรุณากรอกเลข Asset "
                                                            ValidationGroup="approvepurchase" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValtxtNOAsset" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight" PopupPosition="topLeft"
                                                            TargetControlID="RequitxtNOAsset" Width="180" />

                                                        <%--   <asp:RegularExpressionValidator ID="RegulartxtNOAsset"
                                                            runat="server" ErrorMessage="*กรอกเฉพาะตัวเลขเท่านั้น" SetFocusOnError="true"
                                                            Display="None" ControlToValidate="txtNOAsset" ValidationExpression="^[0-9']{1,10}$"
                                                            />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatortxtNOAsset" runat="Server" PopupPosition="topLeft"
                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegulartxtNOAsset" Width="180" />--%>


                                                        <asp:LinkButton CssClass="btn btn-success pull-right" Visible="false" ID="btnaddasset" data-toggle="tooltip" title="เพิ่ม" runat="server"
                                                            CommandName="btnadd_assetnumber" OnCommand="btn_command" CommandArgument='<%# Eval("u2_purchase_idx")+ ";" + Eval("items_quantity") %>'
                                                            OnClientClick="return confirm('คุณต้องการเพิ่มข้อมูลนี้ใช่หรือไม่ ?')"> เพิ่ม</asp:LinkButton>



                                                        <asp:GridView ID="gvassetnumber" Visible="true" runat="server" AutoGenerateColumns="false"
                                                            CssClass="table table-bordered" GridLines="None" DataKeyNames="idx_asset"
                                                            HeaderStyle-CssClass="default" HeaderStyle-Height="5px" AllowPaging="true"
                                                            OnRowDataBound="gvRowDataBound" OnRowDeleting="gvRowDeleted">
                                                            <Columns>

                                                                <asp:TemplateField HeaderText="NO." HeaderStyle-CssClass="text-center"
                                                                    ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                                    <ItemTemplate>
                                                                        <small>

                                                                            <asp:Label ID="_lbidx_asset" runat="server" Visible="false" CssClass="col-sm-12"
                                                                                Text='<%# Eval("idx_asset") %>'></asp:Label>
                                                                            <asp:Label ID="Labelz1" runat="server" CssClass="col-sm-12"></asp:Label>
                                                                            <%# (Container.DataItemIndex +1) %>
                                                                        </small>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="No.Asset" HeaderStyle-CssClass="text-center"
                                                                    ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                                    <ItemTemplate>
                                                                        <small>
                                                                            <asp:Label ID="_lbasset_number" runat="server" CssClass="col-sm-12"
                                                                                Text='<%# Eval("asset_number") %>'></asp:Label>

                                                                        </small>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="manage" HeaderStyle-CssClass="text-center"
                                                                    ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                                    <ItemTemplate>
                                                                        <small>
                                                                            <asp:LinkButton CssClass="text-trash" Visible="true" ID="btndelete_numberasset" data-toggle="tooltip" title="ลบรายการ" runat="server"
                                                                                CommandName="Delete" OnCommand="btn_command" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')"><i class="glyphicon glyphicon-trash"></i> </asp:LinkButton>
                                                                        </small>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>


                                                            </Columns>

                                                        </asp:GridView>



                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12">

                                    <asp:GridView ID="gvFile" Visible="true" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-hover " OnRowDataBound="gvRowDataBound">

                                        <Columns>
                                            <asp:TemplateField HeaderText="เอกสารในการขอซื้อ">
                                                <ItemTemplate>
                                                    <div class="col-lg-11">
                                                        <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />

                                                    </div>
                                                    <div class="col-lg-1">
                                                        <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-primary btn-sm pull-right" data-original-title="Download" Target="_blank"><i class="fa fa-download"></i></asp:HyperLink>

                                                        <asp:HiddenField runat="server" ID="hidFile11" Value='<%# Eval("Download") %>' />
                                                    </div>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12">
                                </div>
                            </div>

                            <%--     MaxLength="2" TextMode="MultiLine"--%>
                        </div>
                    </div>
                </div>
            </div>


            <%-- IT support result purchase--%>
            <div class="col-sm-12" runat="server" id="content_result_itsupport">
                <asp:FormView ID="fvresultpurchase" Visible="false" OnDataBound="formview_databound"
                    runat="server" DefaultMode="Edit" Width="100%">
                    <EditItemTemplate>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <i class="glyphicon glyphicon-file"></i>&nbsp;
                        <b>Result purchase</b> (สรุปรายการ)
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="row">
                                        <asp:Panel ID="Panel_list_purchase" runat="server" Visible="true">
                                            <div class="alert alert-success" role="alert">
                                                <asp:Label ID="label_result_list" runat="server">
                                            <i class="fa fa-check" aria-hidden="true"></i> &nbsp;<strong>ราคาประมาณ</strong>&nbsp;<%# Eval("price_purchase") %> &nbsp; บาท&nbsp;
                                            <strong>สรุปผลโดย</strong> &nbsp;แผนก IT Support
                                                </asp:Label>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                    <div class="row">
                                        <asp:Panel ID="Panel_costcenter" runat="server" Visible="true">
                                            <div class="alert alert-success" role="alert">
                                                <asp:Label ID="label3" runat="server">
                                            <i class="fa fa-check" aria-hidden="true"></i> &nbsp;<strong>costcenter</strong> :&nbsp;<%# Eval("costcenter_no") %> &nbsp;
                                            <strong>สรุปผลโดย</strong> &nbsp;ฝ่าย งบประมาณและวิเคราะห์
                                                </asp:Label>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </div>
                            </div>
                    </EditItemTemplate>
                </asp:FormView>
            </div>

            <div class="col-sm-12" runat="server" id="Div3">

                <asp:FormView ID="fvmanage_approvepurchase" OnDataBound="formview_databound"
                    runat="server" DefaultMode="Insert" Width="100%">
                    <InsertItemTemplate>

                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <i class="glyphicon glyphicon-file"></i>&nbsp;
                        <b>การดำเนินการ การขอซื้ออุปกรณ์</b>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label>แสดงความคิดเห็น/หมายเหตุ</label>
                                            <asp:UpdatePanel ID="panelequipmenttype" runat="server">
                                                <ContentTemplate>
                                                    <asp:TextBox ID="txtcomment_purchase" TextMode="MultiLine" MaxLength="3" runat="server" CssClass="form-control"
                                                        placeholder="แสดงความคิดเห็น..." />

                                                    <asp:RequiredFieldValidator ID="RequiredFieldtxtcomment_purchase" runat="server"
                                                        ControlToValidate="txtcomment_purchase" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณาแสดงความคิดเห็น" ValidationGroup="approvepurchase" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="Validatortxtcomment_purchase" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldtxtcomment_purchase" PopupPosition="topLeft" Width="180" />

                                                    <asp:RequiredFieldValidator ID="Requtxtcomment_purchase" runat="server"
                                                        ControlToValidate="txtcomment_purchase" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณาแสดงความคิดเห็น" ValidationGroup="notapprovepurchase" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="Validattxtcomment_purchase" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requtxtcomment_purchase" PopupPosition="topLeft" Width="180" />


                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                        <h5>&nbsp;</h5>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <asp:Panel ID="Panel_alert_warning" runat="server" Visible="false">
                                                <div class="alert alert-warning" role="alert">
                                                    <asp:Label ID="lb_warning_action" runat="server">
                                                <strong>ขออภัยค่ะ</strong><small>&nbsp;ขณะนี้ยังไม่ถึงขั้นตอนการดำเนินการของท่านค่ะ</small>
                                                    </asp:Label>
                                                    <asp:Label ID="lb_warning_action_edit_doc" Visible="false" runat="server">
                                                <strong>ขออภัยค่ะ</strong><small>&nbsp;ขณะนี้ผู้ขอซื้อกำลังดำเนินการแก้ไขเอกสารอยู่ค่ะ</small>
                                                    </asp:Label>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <asp:LinkButton CssClass="btn btn-success" Visible="true" ID="btnsaveapprove" data-toggle="tooltip" title="อนุมัติ" runat="server"
                                                CommandName="saveapprove" ValidationGroup="approvepurchase" OnCommand="btn_command" CommandArgument="5"
                                                OnClientClick="return confirm('คุณต้องการบันทึกรายการขอซื้อนี้ใช่หรือไม่ ?')"> อนุมัติ</asp:LinkButton>&nbsp;
                                <asp:LinkButton CssClass="btn btn-warning" Visible="true" ID="btnnotapprove_editsheet" data-toggle="tooltip" title="ไม่อนุมัติรายการ(แก้ไขเอกสาร)" runat="server"
                                    CommandName="saveapprove" OnCommand="btn_command" CommandArgument="6" ValidationGroup="notapprovepurchase"
                                    OnClientClick="return confirm('คุณต้องการบันทึกรายการขอซื้อนี้ใช่หรือไม่ ?')"> ไม่อนุมัติ(แก้ไขเอกสาร)</asp:LinkButton>
                                            <asp:LinkButton CssClass="btn btn-danger" Visible="false" ID="btnnotapprove" data-toggle="tooltip" title="ไม่อนุมัติ(จบการดำเนินการ)" runat="server"
                                                CommandName="saveapprove" OnCommand="btn_command" CommandArgument="7" ValidationGroup="notapprovepurchase"
                                                OnClientClick="return confirm('คุณต้องการบันทึกรายการขอซื้อนี้ใช่หรือไม่ ?')"> ไม่อนุมัติ(จบการดำเนินการ)</asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </InsertItemTemplate>
                </asp:FormView>
            </div>
            <div class="col-sm-12" runat="server" id="content_Summary_items">
                <asp:FormView ID="fvsummary_itemspurchase" OnDataBound="formview_databound"
                    runat="server" DefaultMode="Insert" Width="100%">
                    <InsertItemTemplate>
                        <asp:HiddenField ID="hidden_node" runat="server" Value='<%# Eval("m0_node_idx") %>' />
                        <asp:HiddenField ID="hidden_actor" runat="server" Value='<%# Eval("m0_actor_idx") %>' />
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <i class="glyphicon glyphicon-file"></i>&nbsp;
                        <b>สรุปรายการขอซื้ออุปกรณ์</b>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="row">
                                        <asp:GridView ID="gvlist_summarypurchase" runat="server" CssClass="table table-striped table-hover " HeaderStyle-CssClass="info" GridLines="None"
                                            AutoGenerateColumns="false" Visible="true"
                                            OnRowDataBound="gvRowDataBound">
                                            <EmptyDataTemplate>
                                                <div style="text-align: center">

                                                    <div class="alert alert-warning">
                                                        ยังไม่มีรายการขอซื้ออุปกรณ์
                                                    </div>
                                                </div>
                                            </EmptyDataTemplate>
                                            <Columns>
                                                <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center"
                                                    HeaderStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <small>
                                                            <asp:Label ID="label_idx_purchase" runat="server" Visible="false" Text='<%# Eval("u0_purchase_idx") %>' /></small>
                                                        <%# (Container.DataItemIndex +1) %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="รายละเอียดพนักงาน" HeaderStyle-Width="30%" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="left" HeaderStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lb_empCode" runat="server" CssClass="col-sm-12">
                                                <small><p><b>รหัสพนักงาน:</b> &nbsp;<%# Eval("empcode_purchase") %></p></small>
                                                        </asp:Label>
                                                        <asp:Label ID="lb_fullName" runat="server" CssClass="col-sm-12">
                                                <small><p><b>ชื่อ - นามสกุล:</b> &nbsp;<%# Eval("fullname_purchase") %></p></small>
                                                        </asp:Label>
                                                        <asp:Label ID="lb_orgName" runat="server" CssClass="col-sm-12">
                                                    <small><p><b>องค์กร:</b> &nbsp;<%# Eval("org_name_purchase") %></p></small>
                                                        </asp:Label>
                                                        <asp:Label ID="lb_dpart" runat="server" CssClass="col-sm-12">
                                                <small><p><b>ฝ่าย:</b> &nbsp;<%# Eval("drept_name_purchase") %></p></small>
                                                        </asp:Label>
                                                        <asp:Label ID="lb_section" runat="server" CssClass="col-sm-12">
                                                <small><p><b>แผนก:</b> &nbsp;<%# Eval("sec_name_purchase") %></p></small>
                                                        </asp:Label>
                                                        <asp:Label ID="lb_position" runat="server" CssClass="col-sm-12">
                                                <small><p><b>ตำแหน่ง:</b> &nbsp;<%# Eval("pos_name_purchase") %></p></small>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="รายละเอียดการขอซื้อ" HeaderStyle-Width="40%" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="left" HeaderStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <asp:Label ID="actor_idx" runat="server" Visible="true" Text='<%# Eval("m0_actor_idx") %>' />
                                                        <asp:Label ID="node_idx" runat="server" Visible="true" Text='<%# Eval("m0_node_idx") %>' />
                                                        <asp:Label ID="documentcode" runat="server" CssClass="col-sm-12">
                                                <small><p><b>รหัสเอกสาร:</b> &nbsp;<%# Eval("document_code") %></p></small>
                                                        </asp:Label>
                                                        <asp:Label ID="date_purchase" runat="server" CssClass="col-sm-12">
                                                <small><p><b>วันที่ขอซื้อ:</b> &nbsp;<%# Eval("date_purchasequipment") %></p></small>
                                                        </asp:Label>
                                                        <%--  <asp:Label ID="lb_equipment" runat="server" CssClass="col-sm-12">
                                                <small><p><b>ประเภทอุปกรณ์ที่ซื้อ:</b> &nbsp;<%# Eval("name_equipment") %></p></small>
                                                    </asp:Label>--%>
                                                        <asp:Label ID="lb_purchase" runat="server" CssClass="col-sm-12">
                                                <small><p><b>ประเภทการขอซื้อ:</b> &nbsp;<%# Eval("name_purchase") %></p></small>
                                                        </asp:Label>
                                                        <%--   <asp:Label ID="quantity_equipment" runat="server" CssClass="col-sm-12">
                                                <small><p><b>จำนวนเครื่อง:</b> &nbsp;<%# Eval("quantity_equipment") %></p></small>
                                                    </asp:Label>--%>
                                                        <asp:Label ID="label_details_purchase" runat="server" CssClass="col-sm-12">
                                                <small><p><b>เหตุผลการขอซื้อ:</b> &nbsp;<%# Eval("details_purchase") %></p></small>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ราคาของอุปกรณ์" HeaderStyle-Width="25%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center"
                                                    HeaderStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <div class="col-sm-12">
                                                            <asp:DropDownList ID="ddlprice_equiptment"
                                                                runat="server" CssClass="form-control fa-align-left"
                                                                AutoPostBack="true" Enabled="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <asp:LinkButton CssClass="btn btn-success pull-right" Visible="true" ID="btnsaveapprove1" data-toggle="tooltip" title="อนุมัติรายการ" runat="server"
                                                CommandName="sentlistpurchase" OnCommand="btn_command"
                                                OnClientClick="return confirm('คุณต้องการบันทึกรายการขอซื้อนี้ใช่หรือไม่ ?')"> ส่งสรุปรายการขอซื้อ</asp:LinkButton>
                                            &nbsp;
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </InsertItemTemplate>
                </asp:FormView>
            </div>
            <%-- History purchase--%>
            <div class="col-sm-12" runat="server" id="content_history_purchase">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-list-ul" aria-hidden="true"></i>&nbsp;
                        <b>สถานะการดำเนินการขอซื้ออุปกรณ์</b>
                    </div>
                    <div class="panel-body">
                        <asp:Repeater ID="history__purchasequipment" runat="server">
                            <HeaderTemplate>
                                <div class="row">
                                    <label class="col-sm-2 control-label"><small>วันที่ดำเนินการ(เวลา)</small></label>
                                    <label class="col-sm-3 control-label"><small>ผู้ดำเนินการ</small></label>
                                    <label class="col-sm-2 control-label"><small>ดำเนินการ</small></label>
                                    <label class="col-sm-2 control-label"><small>ผลการดำเนินการ</small></label>
                                    <label class="col-sm-3 control-label"><small>หมายเหตุ</small></label>
                                </div>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <div class="row">
                                    <div class="col-sm-2">
                                        <small><span><%# Eval("date_purchasequipment") %>(<%# Eval("time_purchasequipment") %>)</span></small>
                                    </div>
                                    <div class="col-sm-3">
                                        <small><span><%# Eval("actor_name_purchase") %></span></small>
                                    </div>
                                    <div class="col-sm-2">
                                        <small><span><%# Eval("node_name_purchase") %></span></small>
                                    </div>
                                    <div class="col-sm-2">
                                        <small><span><%# Eval("node_status_purchase") %></span></small>
                                    </div>
                                    <div class="col-sm-3">
                                        <small><span><%# Eval("details_purchase") %></span></small>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
        </asp:View>


        <%-- view report purchase list--%>

        <asp:View ID="viewReport" runat="server">
            <asp:HyperLink ID="setfocus_report" runat="server"></asp:HyperLink>
            <div class="col-sm-12" runat="server" id="content_report">

                <div class="panel panel-default">
                    <!-- Add Panel Heading Here -->
                    <div class="panel-body">
                        <i class="fa fa-file-text-o" aria-hidden="true"></i>&nbsp;<strong>Report Purchase</strong>
                    </div>
                </div>

            </div>

            <div class="col-sm-12" runat="server" id="contect_selectViewreport">
                <div class="panel panel-info">


                    <%--  <div class="row">--%>
                    <asp:Panel ID="panelSearchViewreport" runat="server" Visible="false">

                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <%-- เงื่อนไขการค้นหา--%>
                                <%--                <div class="form-group">
                                    <asp:Label ID="lbyearsearch" CssClass="col-sm-2 control-label" runat="server" Text="ปี : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlyear_search" AutoPostBack="true" runat="server" CssClass="form-control">
                                            <asp:ListItem Text="กรุณาเลือกปี ...." Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <asp:Label ID="lbroomsearch" CssClass="col-sm-2 control-label" runat="server" Text="เดือน : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlmountly_search" AutoPostBack="true" runat="server" CssClass="form-control">
                                            <asp:ListItem Text="กรุณาเลือกเดือน ...." Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                </div>--%>
                                <div class="form-group">
                                    <asp:Label ID="Label33" CssClass="col-sm-2 control-label" runat="server" Text="ระบุวันที่ค้นหา : " />
                                    <div class="col-sm-2">
                                        <asp:DropDownList ID="ddlconditionReport" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" CssClass="form-control">
                                            <asp:ListItem Text="เลือกเงื่อนไข...." Value="0"></asp:ListItem>
                                            <asp:ListItem Text="มากกว่า >" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="น้อยกว่า <" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="ระหว่าง <>" Value="3"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <%--    <asp:Label ID="Label15" CssClass="col-sm-1 control-label" runat="server" Text="จากวันที่ : " />--%>

                                    <div class="col-sm-3">
                                        <div class='input-group date from-date-datepicker'>
                                            <asp:TextBox ID="txtStartdateReport" runat="server" placeholder="จากวันที่..."
                                                CssClass="form-control from-date-datepicker"></asp:TextBox><span class="input-group-addon"><span data-icon-element="" class="glyphicon glyphicon-calendar"></span></span>
                                        </div>
                                    </div>

                                    <%--   <asp:Label ID="Label16" CssClass="col-sm-1 control-label" runat="server" Text="ถึงวันที่ : " />--%>
                                    <div class="col-sm-3">
                                        <div class='input-group date from-date-datepicker'>
                                            <asp:TextBox ID="txtEnddateReport" runat="server" Enabled="false" placeholder="ถึงวันที่..."
                                                CssClass="form-control from-date-datepicker"></asp:TextBox><span class="input-group-addon"><span data-icon-element="" class="glyphicon glyphicon-calendar"></span></span>
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="Label34" CssClass="col-sm-2 control-label" runat="server" Text="รหัสเอกสาร : " />
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtDocumentCodeReport" runat="server" placeholder="รหัสเอกสาร"
                                            CssClass="form-control"></asp:TextBox>
                                    </div>

                                    <asp:Label ID="Label35" CssClass="col-sm-2 control-label" runat="server" Text="รหัสพนักงาน : " />
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtEmpCodeReport" runat="server" MaxLength="8" placeholder="รหัสพนักงาน"
                                            CssClass="form-control"></asp:TextBox>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <asp:Label ID="lbFname_LnameReport" CssClass="col-sm-2 control-label" runat="server" Text="ชื่อ-นามสกุล : " />
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtFname_LnameReport" runat="server" placeholder="ชื่อ-นามสกุล"
                                            CssClass="form-control"></asp:TextBox>
                                    </div>

                                    <asp:Label ID="Label37" CssClass="col-sm-2 control-label" runat="server" Text="บริษัท : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlOrganizationReport"
                                            runat="server" CssClass="form-control fa-align-left"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" Enabled="true">
                                        </asp:DropDownList>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label38" CssClass="col-sm-2 control-label" runat="server" Text="ฝ่าย : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlDepartmentReport"
                                            runat="server" CssClass="form-control fa-align-left"
                                            AutoPostBack="true" Enabled="true">
                                        </asp:DropDownList>
                                    </div>

                                    <asp:Label ID="Label39" CssClass="col-sm-2 control-label" runat="server" Text="สถานที่ : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlLocationReport"
                                            runat="server" CssClass="form-control fa-align-left"
                                            Enabled="true">
                                        </asp:DropDownList>
                                    </div>

                                </div>


                                <div class="form-group">
                                    <asp:Label ID="Label40" CssClass="col-sm-2 control-label" runat="server" Text="Cost Center : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlCostcenterReport"
                                            runat="server" CssClass="form-control fa-align-left selectpicker"
                                            AutoPostBack="true" Enabled="true">
                                        </asp:DropDownList>
                                    </div>

                                    <asp:Label ID="Label41" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทการขอซื้อ : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlPurchaseTypeReport"
                                            runat="server" CssClass="form-control fa-align-left"
                                            AutoPostBack="true" Enabled="true">
                                        </asp:DropDownList>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label42" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทอุปกรณ์ : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlEquipmentTypeReport"
                                            runat="server" CssClass="form-control fa-align-left"
                                            AutoPostBack="true" Enabled="true">
                                        </asp:DropDownList>
                                    </div>

                                    <asp:Label ID="Label43" CssClass="col-sm-2 control-label" runat="server" Text="สถานะรายการ : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlStatusReport"
                                            runat="server" CssClass="form-control fa-align-left"
                                            AutoPostBack="true" Enabled="true">
                                        </asp:DropDownList>
                                    </div>

                                </div>
                                <asp:UpdatePanel ID="updateexport" runat="server">
                                    <ContentTemplate>
                                        <div class="form-group">

                                            <div class="col-lg-3 col-lg-offset-2">
                                                <asp:LinkButton CssClass="btn btn-info btn-sm" ID="LinkButton3" data-toggle="tooltip" title="ค้นหารายการ" runat="server"
                                                    CommandName="searchingReport" Visible="true" OnCommand="btn_command"><i class="glyphicon glyphicon-search"></i> ค้นหา</asp:LinkButton>
                                                <asp:LinkButton CssClass="btn btn-default btn-sm" ID="LinkButton4" data-toggle="tooltip" title="รีเซ็ต" runat="server"
                                                    CommandName="resetSearchingReport" Visible="true" OnCommand="btn_command"><i class="glyphicon glyphicon-refresh"></i> รีเซ็ต</asp:LinkButton>
                                                <asp:LinkButton CssClass="btn btn-primary btn-sm" ID="btnExportExcel" data-toggle="tooltip" title=" Export Excel" runat="server"
                                                    CommandName="exportExcell" Visible="false" OnCommand="btn_command"> Export Excel</asp:LinkButton>
                                            </div>
                                        </div>
                                    </ContentTemplate>

                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="btnExportExcel" />
                                    </Triggers>

                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </asp:Panel>




                </div>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>

                        <div class="form-group">

                            <%--  <div class="col-lg-10 col-lg-offset-2">--%>
                            <asp:LinkButton CssClass="btn btn-success btn-sm pull-right" ID="printreport" data-toggle="tooltip" title="พิมพ์เอกสาร" runat="server"
                                CommandName="printreport" Visible="false" OnCommand="btn_command"><i class="glyphicon glyphicon-print"></i> พิมพ์เอกสาร</asp:LinkButton>
                            <%--   </div>--%>
                        </div>
                    </ContentTemplate>

                    <Triggers>
                        <asp:PostBackTrigger ControlID="printreport" />
                    </Triggers>

                </asp:UpdatePanel>

                <div class="form-group">
                    <asp:GridView ID="gvReport" Visible="true" runat="server" AutoGenerateColumns="false" DataKeyNames="u0_purchase_idx"
                        CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                        HeaderStyle-CssClass="info" HeaderStyle-Height="30px" AllowPaging="true" PageSize="5"
                        OnPageIndexChanging="gvPageIndexChanging" OnRowDataBound="gvRowDataBound">
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                            FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่พบข้อมูล</div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="รหัสเอกสาร" HeaderStyle-CssClass="text-center"
                                ItemStyle-HorizontalAlign="center" HeaderStyle-Width="2%" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <asp:Label ID="lb_documentcode" runat="server" CssClass="col-sm-12">
                    <small><p> &nbsp;<%# Eval("document_code") %></p></small></asp:Label>
                                    <asp:Label ID="u0_purchase_report" runat="server" Visible="false" Text='<%# Eval("u0_purchase_idx") %>' />
                                    <asp:Label ID="idxreport" Visible="false" runat="server"><%# (Container.DataItemIndex +1) %></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="รายละเอียดผู้ทำรายการขอซื้อ" HeaderStyle-Width="30%" HeaderStyle-CssClass="text-center"
                                ItemStyle-HorizontalAlign="left" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <asp:Label ID="lb_empCode_employee" runat="server" CssClass="col-sm-12">
                    <small><p><b>รหัสพนักงาน:</b> &nbsp;<%# Eval("empcode_purchase") %></p></small>
                                    </asp:Label>
                                    <asp:Label ID="lb_fullName_employee" runat="server" CssClass="col-sm-12">
                    <small><p><b>ชื่อ - นามสกุล:</b> &nbsp;<%# Eval("fullname_purchase") %></p></small>
                                    </asp:Label>
                                    <asp:Label ID="lb_orgName_probation" runat="server" CssClass="col-sm-12">
                    <small><p><b>องค์กร:</b> &nbsp;<%# Eval("org_name_purchase") %></p></small>
                                    </asp:Label>
                                    <asp:Label ID="lb_dpart_probation" runat="server" CssClass="col-sm-12">
                    <small><p><b>ฝ่าย:</b> &nbsp;<%# Eval("drept_name_purchase") %></p></small>
                                    </asp:Label>
                                    <asp:Label ID="lb_section_probation" runat="server" CssClass="col-sm-12">
                    <small><p><b>แผนก:</b> &nbsp;<%# Eval("sec_name_purchase") %></p></small>
                                    </asp:Label>
                                    <asp:Label ID="lb_position_probation" runat="server" CssClass="col-sm-12">
                    <small><p><b>ตำแหน่ง:</b> &nbsp;<%# Eval("pos_name_purchase") %></p></small>
                                    </asp:Label>
                                    <asp:Label ID="Label36" runat="server" CssClass="col-sm-12">
                    <small><p><b>cost center:</b> &nbsp;<%# Eval("costcenter_no") %></p></small>
                                    </asp:Label>
                                    <asp:Label ID="Label44" runat="server" CssClass="col-sm-12">
                    <small><p><b>เบอร์ติดต่อ:</b> &nbsp;<%# Eval("numberphone") %></p></small>
                                    </asp:Label>
                                    <asp:Label ID="Label45" runat="server" CssClass="col-sm-12">
                    <small><p><b>E-mail:</b> &nbsp;<%# Eval("email_purchase") %></p></small>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="รายละเอียดการขอซื้อ" HeaderStyle-Width="42%" HeaderStyle-CssClass="text-center"
                                ItemStyle-HorizontalAlign="left" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <%--  <asp:Label ID="lb_empCode_employedde" runat="server" CssClass="col-sm-12">
                    <small><p><b>รหัสเอกสาร:</b> &nbsp;<%# Eval("empcode_purchase") %></p></small>
                                </asp:Label>--%>
                                    <asp:Label ID="lb_date_purchase" runat="server" CssClass="col-sm-12">
                    <small><p><b>วันที่ขอซื้อ:</b> &nbsp;<%# Eval("date_purchasequipment") %></p></small>
                                    </asp:Label>
                                    <%-- <asp:Label ID="lb_equipmenttype" runat="server" CssClass="col-sm-12">
                    <small><p><b>ประเภทอุปกรณ์ที่ซื้อ:</b> &nbsp;<%# Eval("name_equipment") %></p></small>
                            </asp:Label>--%>
                                    <asp:Label ID="lb_purchasetype" runat="server" CssClass="col-sm-12">
                    <small><p><b>ประเภทการขอซื้อ:</b> &nbsp;<%# Eval("name_purchase") %></p></small>
                                    </asp:Label>
                                    <%--  <asp:Label ID="lb_quantity_equipment" runat="server" CssClass="col-sm-12">
                    <small><p><b>จำนวนเครื่อง:</b> &nbsp;<%# Eval("quantity_equipment") %></p></small>
                            </asp:Label>--%>
                                    <asp:Label ID="lb_details_equipment" runat="server" CssClass="col-sm-12">
                                <small>
                                    <p><b>รายละเอียดการขอซื้อ:</b> &nbsp;<%# Eval("details_purchase") %></p>
                                </small>
                                    </asp:Label>
                                    <asp:GridView ID="gvReportitems" Visible="true" runat="server" AutoGenerateColumns="false"
                                        CssClass="table table-bordered" GridLines="None"
                                        HeaderStyle-CssClass="default" HeaderStyle-Height="10px" AllowPaging="true"
                                        OnRowDataBound="gvRowDataBound">


                                        <EmptyDataTemplate>
                                            <div style="text-align: center">ยังไม่มีรายการขอซื้อ</div>
                                        </EmptyDataTemplate>
                                        <Columns>

                                            <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                                ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <asp:Label ID="_lbu2idxreport" runat="server" Visible="false" Text='<%# Eval("u2_purchase_idx") %>' />
                                                    <%# (Container.DataItemIndex +1) %>
                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="อุปกรณ์" HeaderStyle-CssClass="text-center"
                                                ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Label ID="items_typereport" runat="server" CssClass="col-sm-12"
                                                            Text='<%# Eval("type_equipment") %>'></asp:Label>
                                                        <asp:Label ID="lbnodeidxItems" runat="server" Visible="false" Text='<%# Eval("node_idx") %>' />
                                                        <asp:Label ID="lbactoridxItems" runat="server" Visible="false" Text='<%# Eval("actor_idx") %>' />
                                                        <%--  <asp:Label ID="lbstatus_action" runat="server" Visible="false" Text='<%# Eval("status_node") %>' />--%>
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="จำนวน" HeaderStyle-CssClass="text-center"
                                                ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Label ID="items_quantity_report" runat="server" CssClass="col-sm-12"
                                                            Text='<%# Eval("items_quantity") %>'></asp:Label>
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ความคิดเห็นจากไอที" HeaderStyle-CssClass="text-center"
                                                ItemStyle-HorizontalAlign="center" Visible="true" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>

                                                        <asp:Panel ID="price_comment_complete_report" runat="server" Visible="true">

                                                            <label class="bg-success">:: ความคิดเห็น :: </label>
                                                            <br />
                                                            <asp:Label ID="commentit_report" runat="server" CssClass="col-sm-12"
                                                                Text='<%# Eval("commentfromITSupport") %>'></asp:Label><br />
                                                            <label class="bg-success">:: ราคา :: </label>
                                                            <br />
                                                            <asp:Label ID="lbprice_report" runat="server" CssClass="col-sm-12"
                                                                Text='<%# Eval("price_equipment") %>'></asp:Label><br />

                                                        </asp:Panel>

                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="NO.IO" HeaderStyle-CssClass="text-center"
                                                ItemStyle-HorizontalAlign="center" Visible="true" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>

                                                        <asp:GridView ID="gvionumber_report" Visible="true" runat="server" AutoGenerateColumns="false"
                                                            CssClass="table table-bordered" GridLines="None" DataKeyNames="idx"
                                                            HeaderStyle-CssClass="default" HeaderStyle-Height="5px" AllowPaging="true"
                                                            OnRowDataBound="gvRowDataBound">
                                                            <Columns>

                                                                <asp:TemplateField HeaderText="NO." HeaderStyle-CssClass="text-center"
                                                                    ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                                    <ItemTemplate>
                                                                        <small>

                                                                            <asp:Label ID="_lbidxnumerid_report" runat="server" Visible="false" CssClass="col-sm-12"
                                                                                Text='<%# Eval("idx") %>'></asp:Label>
                                                                            <asp:Label ID="Labelz1" runat="server" CssClass="col-sm-12"></asp:Label>
                                                                            <%# (Container.DataItemIndex +1) %>
                                                                        </small>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="NO.IO" HeaderStyle-CssClass="text-center"
                                                                    ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                                    <ItemTemplate>
                                                                        <small>
                                                                            <asp:Label ID="_lbnumberio_report" runat="server" CssClass="col-sm-12"
                                                                                Text='<%# Eval("io") %>'></asp:Label>

                                                                        </small>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="NO.Asset" Visible="true" HeaderStyle-CssClass="text-center"
                                                ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>

                                                        <asp:GridView ID="gvassetnumber_report" Visible="true" runat="server" AutoGenerateColumns="false"
                                                            CssClass="table table-bordered" GridLines="None" DataKeyNames="idx_asset"
                                                            HeaderStyle-CssClass="default" HeaderStyle-Height="5px" AllowPaging="true"
                                                            OnRowDataBound="gvRowDataBound">
                                                            <Columns>

                                                                <asp:TemplateField HeaderText="NO." HeaderStyle-CssClass="text-center"
                                                                    ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                                    <ItemTemplate>
                                                                        <small>

                                                                            <asp:Label ID="_lbidx_asset_report" runat="server" Visible="false" CssClass="col-sm-12"
                                                                                Text='<%# Eval("idx_asset") %>'></asp:Label>
                                                                            <asp:Label ID="Labelz" runat="server" CssClass="col-sm-12"></asp:Label>
                                                                            <%# (Container.DataItemIndex +1) %>
                                                                        </small>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="No.Asset" HeaderStyle-CssClass="text-center"
                                                                    ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                                    <ItemTemplate>
                                                                        <small>
                                                                            <asp:Label ID="_lbasset_number_report" runat="server" CssClass="col-sm-12"
                                                                                Text='<%# Eval("asset_number") %>'></asp:Label>

                                                                        </small>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                            </Columns>

                                                        </asp:GridView>
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>


                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="สถานะ" HeaderStyle-Width="5%" HeaderStyle-CssClass="text-center"
                                ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small><b>
                                        <asp:Label ID="idx_status" Visible="false" runat="server" CssClass="col-sm-12"
                                            Text='<%# Eval("status_purchase") %>'></asp:Label>
                                        <asp:Label ID="status_purchasereport" runat="server" CssClass="col-sm-12"
                                            Text='<%# Eval("node_status_purchase") %>'></asp:Label></b></small>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <%--  <asp:TemplateField HeaderText="No. ASSET" HeaderStyle-Width="10%" HeaderStyle-CssClass="text-center"
                aderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="number_asssset" runat="server" CssClass="col-sm-12"
                                        Text='<%# Eval("number_asset") %>'></asp:Label></b></small>

                            </ItemTemplate>
                        </asp:TemplateField>--%>

                            <%--  <asp:TemplateField HeaderText="IO NO." HeaderStyle-Width="20%" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="left" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="number_assssxxet" runat="server" CssClass="col-sm-12"
                                        Text='<%# Eval("number_io") %>'></asp:Label></b></small>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        </Columns>
                    </asp:GridView>
                </div>

                <asp:GridView ID="GvExportExcel" runat="server" AutoGenerateColumns="true">
                    <Columns>
                        <asp:TemplateField HeaderText="ลำดับที่" ItemStyle-Width="100">
                            <ItemTemplate>
                                <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>


            </div>

        </asp:View>
    </asp:MultiView>


    <script>
        $(function () {
            $("#printreport").click(function () {
                window.print();
            });
        });
    </script>


    <script>
        $(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });


        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            });
        });

    </script>

    <script>
        $(document).ready(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        })
    </script>


    <script>
        $(function () {

            $('.from-date-datepickerexpire').datetimepicker({
                format: 'DD/MM/YYYY',
                minDate: new Date()

            });
        });


        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepickerexpire').datetimepicker({
                    format: 'DD/MM/YYYY',
                    minDate: new Date()

                });
            });
        });

    </script>

    <script>
        $(document).ready(function () {
            $('.from-date-datepickerexpire').datetimepicker({
                format: 'DD/MM/YYYY',
                minDate: new Date()

            });
        })
    </script>



</asp:Content>
