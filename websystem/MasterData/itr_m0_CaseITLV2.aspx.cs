﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_itr_m0_CaseITLV2 : System.Web.UI.Page
{
    #region Connect

    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();

    private string ODSP_Reletion = "ODSP_Reletion";
    data_employee dtEmployee = new data_employee();

    dataODSP_Relation dtODSP = new dataODSP_Relation();
    DataReservation datareservation = new DataReservation();
    DataSupportIT _dtsupport = new DataSupportIT();

    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";
    string _localJson = "";

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string urlSelect_CaseLV2 = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_CaseLV2"];
    static string urlSelect_InsertCaseLV2 = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_InsertCaseLV2"];
    static string urlSelect_UpdateCaseLV2 = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_UpdateCaseLV2"];
    static string urlSelect_DeleteCaseLV2 = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_DeleteCaseLV2"];

    static string urlSelect_ddlLV1IT = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_ddlLV1IT"];


    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            MvMaster.SetActiveView(ViewIndex);
            SelectMasterList();
            Select_ddlLV1IT(ddlITLV1);
        }

        ViewState["EmpIDX"] = int.Parse(Session["emp_idx"].ToString());

    }

    protected void Select_ddlLV1IT(DropDownList ddlName)
    {

        ddlName.Items.Clear();
        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Add(new ListItem("เลือกหัวข้อ....", "0"));


        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList closejobit = new UserRequestList();



        _dtsupport.BoxUserRequest[0] = closejobit;

        _dtsupport = callServicePostITRepair(urlSelect_ddlLV1IT, _dtsupport);

        ddlName.DataSource = _dtsupport.BoxUserRequest;
        ddlName.DataTextField = "Name_Code1";
        ddlName.DataValueField = "CIT1IDX";
        ddlName.DataBind();

    }


    protected void SelectMasterList()
    {

        _dtsupport.CaseIT_Detail = new CaseITlist[1];
        CaseITlist dtsupport = new CaseITlist();

        _dtsupport.CaseIT_Detail[0] = dtsupport;

        _dtsupport = callServicePostITRepair(urlSelect_CaseLV2, _dtsupport);
        setGridData(GvMaster, _dtsupport.CaseIT_Detail);
    }

    protected void Insert_CaseLV2()
    {
        _dtsupport.CaseIT_Detail = new CaseITlist[1];
        CaseITlist insert = new CaseITlist();

        insert.CIT2_Name = txtname.Text;
        insert.CIT1IDX = int.Parse(ddlITLV1.SelectedValue);
        insert.CIT2_Code = txtcode.Text;
        insert.CIT2Status = int.Parse(ddStatusadd.SelectedValue);
        insert.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

        _dtsupport.CaseIT_Detail[0] = insert;

        _dtsupport = callServicePostITRepair(urlSelect_InsertCaseLV2, _dtsupport);

    }

    protected void Update_Master_List()
    {
        _dtsupport.CaseIT_Detail = new CaseITlist[1];
        CaseITlist update = new CaseITlist();

        update.CIT2_Name = ViewState["txtname_edit"].ToString();
        update.CIT2_Code = ViewState["txtcode_edit"].ToString();
        update.CIT2Status = int.Parse(ViewState["StatusUpdate_Update"].ToString());
        update.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        update.CIT2IDX = int.Parse(ViewState["CIT2IDX"].ToString());
        update.CIT1IDX = int.Parse(ViewState["ddlITLV1_edit"].ToString());
        
        _dtsupport.CaseIT_Detail[0] = update;

        _dtsupport = callServicePostITRepair(urlSelect_UpdateCaseLV2, _dtsupport);

    }

    protected void Delete_Master_List()
    {
        _dtsupport.CaseIT_Detail = new CaseITlist[1];
        CaseITlist delete = new CaseITlist();

        delete.CIT2IDX = int.Parse(ViewState["CIT2IDX"].ToString());
        delete.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        _dtsupport.CaseIT_Detail[0] = delete;

        _dtsupport = callServicePostITRepair(urlSelect_DeleteCaseLV2, _dtsupport);
    }

    protected DataSupportIT callServicePostITRepair(string _cmdUrl, DataSupportIT _dtsupport)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtsupport);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtsupport = (DataSupportIT)_funcTool.convertJsonToObject(typeof(DataSupportIT), _localJson);

        return _dtsupport;
    }


    #region SetGrid
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster.EditIndex == e.Row.RowIndex)
                    {
                        DropDownList ddlITLV1_edit = (DropDownList)e.Row.FindControl("ddlITLV1_edit");
                        TextBox txtCIT1IDX = (TextBox)e.Row.FindControl("txtCIT1IDX");

                        Select_ddlLV1IT(ddlITLV1_edit);
                        ddlITLV1_edit.SelectedValue = txtCIT1IDX.Text;
                    }
                }

                        break;


        }

    }

    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();

                SelectMasterList();

                break;


        }
    }

    #endregion

    #region GvRowEditing

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.EditIndex = e.NewEditIndex;
                SelectMasterList();
                btnshow.Visible = false;
                break;

        }
    }

    #endregion

    #region GvRowCancelingEdit

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":
                GvMaster.EditIndex = -1;
                SelectMasterList();
                btnshow.Visible = true;
                break;

        }
    }

    #endregion

    #region GvRowUpdating

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                int CIT2IDX = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var txtname_edit = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtname_edit");
                var txtcode_edit = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtcode_edit");
                var StatusUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddStatusUpdate");
                var ddlITLV1_edit = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlITLV1_edit");

                GvMaster.EditIndex = -1;

                ViewState["CIT2IDX"] = CIT2IDX;
                ViewState["txtname_edit"] = txtname_edit.Text;
                ViewState["txtcode_edit"] = txtcode_edit.Text;
                ViewState["StatusUpdate_Update"] = StatusUpdate.SelectedValue;
                ViewState["ddlITLV1_edit"] = ddlITLV1_edit.SelectedValue;
                Update_Master_List();
                SelectMasterList();

                break;
        }
    }

    #endregion

    protected void SetDefaultAdd()
    {
        txtname.Text = String.Empty;
        txtcode.Text = String.Empty;
        ddStatusadd.SelectedValue = "1";
        ddlITLV1.SelectedValue = "0";
    }

    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {

            case "CmdAdd":
                btnshow.Visible = false;
                Panel_Add.Visible = true;
                SetDefaultAdd();
                break;

            case "btnCancel":
                btnshow.Visible = true;
                Panel_Add.Visible = false;
                break;

            case "btnAdd":
                Insert_CaseLV2();
                Panel_Add.Visible = false;
                btnshow.Visible = true;
                SelectMasterList();
                break;
            case "CmdDel":
                int CIT2IDX = int.Parse(cmdArg);
                ViewState["CIT2IDX"] = CIT2IDX;
                Delete_Master_List();
                SelectMasterList();

                break;
        }



    }
    #endregion
}