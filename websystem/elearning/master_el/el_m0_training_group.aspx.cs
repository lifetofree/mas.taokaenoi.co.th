﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_el_m0_training_group : System.Web.UI.Page
{
    #region initial function/data
    function_tool _funcTool = new function_tool();
    data_elearning _data_elearning = new data_elearning();
    function_dmu _func_dmu = new function_dmu();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetel_m0_training_group = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_m0_training_group"];
    static string _urlSetInsel_m0_training_group = _serviceUrl + ConfigurationManager.AppSettings["urlSetInsel_m0_training_group"];
    static string _urlSetUpdel_m0_training_group = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdel_m0_training_group"];
    static string _urlDelel_m0_training_group = _serviceUrl + ConfigurationManager.AppSettings["urlDelel_m0_training_group"];
    static string _urlGetErrorel_m0_training_group = _serviceUrl + ConfigurationManager.AppSettings["urlGetErrorel_m0_training_group"];


    string _localJson = "";
    int _tempInt = 0;

    string _defaultDdlText;
    string _defaultDdlValue;

    int emp_idx = 0;

    Button _gtxttraining_group_code;

    #endregion initial function/data


    protected void Page_Load(object sender, EventArgs e)
    {
        emp_idx = int.Parse(Session["emp_idx"].ToString());

        if (!IsPostBack)
        {
            initPage();
            actionIndex();

        }
       // injectJS();

    }

    #region Inject JS
    public void injectJS()
    {
        string jsTextStart = String.Empty;
        string jsText = String.Empty;
        string jsTextEnd = String.Empty;
        string jsT = String.Empty;
        jsTextStart +=
           "var prm = Sys.WebForms.PageRequestManager.getInstance();" +
           "prm.add_endRequest(function() {";
        /* START Create Form */
        jsText +=
           "$(function () {" +
              "$('.colorpicker').colorpicker({align: 'left'});" +
           "});";
        jsText +=
           "$(function () {" +
              "$('.txtShiftWorkStartTime').clockpicker({" +
                 "autoclose: true});" +
              "$('.txtShiftWorkEndTime').clockpicker({" +
                 "autoclose: true});" +
           "});";
        jsText +=
           "$(function () {" +
              "$('.txtShiftWorkBreakStartTime').clockpicker({" +
                 "autoclose: true});" +
              "$('.txtShiftWorkBreakEndTime').clockpicker({" +
                 "autoclose: true});" +
           "});";
        jsText +=
           "$(function () {" +
              "$('.txtShiftWorkBetweenStartTime').clockpicker({" +
                 "autoclose: true});" +
              "$('.txtShiftWorkBetweenEndTime').clockpicker({" +
                 "autoclose: true});" +
           "});";
        jsText +=
           "$(function () {" +
              "$('.chkShiftWorkAllowBreak input').click(function () {" +
                 "$('.ddlShiftWorkBreakStartTime').attr('disabled', !this.checked);" +
                 "$('.txtShiftWorkBreakStartTime').attr('disabled', !this.checked);" +
                 "$('.ddlShiftWorkBreakEndTime').attr('disabled', !this.checked);" +
                 "$('.txtShiftWorkBreakEndTime').attr('disabled', !this.checked);" +
              "});" +
           "});";
        /* END Create Form */
        /* START Update Form */
        jsText +=
           "$('.txtShiftWorkStartTimeUpdate').clockpicker({" +
              "autoclose: true});" +
           "$('.txtShiftWorkEndTimeUpdate').clockpicker({" +
              "autoclose: true});";
        jsText +=
           "$('.txtShiftWorkBreakStartTimeUpdate').clockpicker({" +
              "autoclose: true});" +
           "$('.txtShiftWorkBreakEndTimeUpdate').clockpicker({" +
              "autoclose: true});";
        jsText +=
           "$('.txtShiftWorkBetweenStartTimeUpdate').clockpicker({" +
              "autoclose: true});" +
           "$('.txtShiftWorkBetweenEndTimeUpdate').clockpicker({" +
              "autoclose: true});";
        jsText +=
           "$('.chkShiftWorkAllowBreakUpdate input').click(function () {" +
              "$('.ddlShiftWorkBreakStartTimeUpdate').attr('disabled', !this.checked);" +
              "$('.txtShiftWorkBreakStartTimeUpdate').attr('disabled', !this.checked);" +
              "$('.ddlShiftWorkBreakEndTimeUpdate').attr('disabled', !this.checked);" +
              "$('.txtShiftWorkBreakEndTimeUpdate').attr('disabled', !this.checked);" +
           "});";
        /* END Update Form */
        jsTextEnd +=
           "});";
        
        string jsCall = jsTextStart + jsText + jsTextEnd;
        Page.ClientScript.RegisterStartupScript(this.GetType(), "", jsCall, true);
    }
    #endregion Inject JS

    #region selected   
    protected void actionIndex()
    {
        pnlsearch.Visible = true;
        data_elearning data_elearning = new data_elearning();
        data_elearning.el_m0_training_group_action = new m0_training_group[1];

        m0_training_group obj = new m0_training_group();

        obj.m0_training_group_idx = 0;
        obj.filter_keyword = txtFilterKeyword.Text;

        data_elearning.el_m0_training_group_action[0] = obj;
        //litDebug.Text = _funcTool.convertObjectToXml(data_elearning);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_elearning));
        //litDebug.Text = _func_dmu.zJson(_urlGetel_m0_training_group, data_elearning);
        data_elearning = _func_dmu.zCallServiceNetwork(_urlGetel_m0_training_group, data_elearning);

        setGridData(GvMaster, data_elearning.el_m0_training_group_action);

    }
    #endregion selected 
    

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        int _m0_training_group_idx;
        string _txttraining_group_name, _txttraining_group_code
            , _txttraining_group_remark
            , _txttraining_group_color;
        int _cemp_idx;

        m0_training_group objM0_ProductType = new m0_training_group();

        switch (cmdName)
        {

            case "btnToInsert":
                MvMaster.SetActiveView(ViewInsert);
              //  txttraining_group_color.Text = "#008CBA";
               // txttraining_group_color.BackColor = System.Drawing.ColorTranslator.FromHtml("#008CBA");
               // ColorPickerExtender.SelectedColor = txttraining_group_color.Text;
                break;
            case "btnCancel":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btnInsert":
                _txttraining_group_code = ((TextBox)ViewInsert.FindControl("txttraining_group_code")).Text.Trim();
                _txttraining_group_name = ((TextBox)ViewInsert.FindControl("txttraining_group_name")).Text.Trim();
                _txttraining_group_remark = ((TextBox)ViewInsert.FindControl("txttraining_group_remark")).Text.Trim();
                _txttraining_group_color = ((TextBox)ViewInsert.FindControl("txttraining_group_color")).Text.Trim();
                DropDownList _ddltraining_group_status = (DropDownList)ViewInsert.FindControl("ddltraining_group_status");
                _cemp_idx = emp_idx;
                if (checkCodeError(0, _txttraining_group_code) == true)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('รหัสนี้มีในระบบแล้ว ไม่สามารถเพิ่มได้');", true);
                    return;
                }
                if (getColor(_txttraining_group_color) == Color.Empty)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('สีกลุ่มวิชา ไม่ถูกต้อง');", true);
                    return;
                }
                m0_training_group obj = new m0_training_group();
                _data_elearning.el_m0_training_group_action = new m0_training_group[1];
                obj.m0_training_group_idx = 0;//_type_idx; 
                obj.training_group_code = _txttraining_group_code;
                obj.training_group_name = _txttraining_group_name;
                obj.training_group_remark = _txttraining_group_remark;
                obj.training_group_color = _txttraining_group_color;
                obj.training_group_status = int.Parse(_ddltraining_group_status.SelectedValue);
                obj.training_group_created_by = _cemp_idx;

                _data_elearning.el_m0_training_group_action[0] = obj;

               // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
               
                _data_elearning = _func_dmu.zCallServicePostNetwork(_urlSetInsel_m0_training_group, _data_elearning);

                if (_data_elearning.return_code == 0)
                {
                    actionIndex();
                    //MvMaster.SetActiveView(ViewIndex);
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                }
                else
                {
                    setError(_data_elearning.return_code.ToString() + " - " + _data_elearning.return_msg);
                }

                //actionCreate();
                //Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btnDelete":

                _m0_training_group_idx = int.Parse(cmdArg);
                _cemp_idx = emp_idx;


                _data_elearning.el_m0_training_group_action = new m0_training_group[1];
                objM0_ProductType.m0_training_group_idx = _m0_training_group_idx;
               // objM0_ProductType.CEmpIDX = _cemp_idx;

                _data_elearning.el_m0_training_group_action[0] = objM0_ProductType;

               // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));

                _data_elearning = _func_dmu.zCallServiceNetwork(_urlDelel_m0_training_group, _data_elearning);


                if (_data_elearning.return_code == 0)
                {

                    //actionIndex();
                    //MvMaster.SetActiveView(ViewIndex);
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);


                }
                else
                {
                    setError(_data_elearning.return_code.ToString() + " - " + _data_elearning.return_msg);
                }

                break;
            case "btnFilter":
                actionIndex();
                break;



        }
    }
    #endregion btnCommand

    #region bind data
    protected void getGridData(string _cmdName, int _actionType)
    {
        // setGridData(gvBookingType, ViewState["listData"]);
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected string getStatus(int status)
    {
        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();
                actionIndex();
                break;
        }
    }

    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    pnlsearch.Visible = false;
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster.EditIndex == e.Row.RowIndex) //to overlook header row
                    {
                        
                    }
                }

                break;
        }
    }

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.EditIndex = e.NewEditIndex;
                actionIndex();
                break;
        }
    }

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.EditIndex = -1;
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
        }
    }

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":


                int m0_training_group_idx_update = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var _txttraining_group_code = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txttraining_group_codeUpdate");
                var _txttraining_group_name = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txttraining_group_nameUpdate");
                var _txttraining_group_remark = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txttraining_group_remarkUpdate");
                var _ddltraining_group_status = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddltraining_group_statusUpdate");
                TextBox _txttraining_group_color = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txttraining_group_colorUpdate");

                if (checkCodeError(m0_training_group_idx_update, _txttraining_group_code.Text.Trim()) == true)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('รหัสนี้มีในระบบแล้ว ไม่สามารถเพิ่มได้');", true);
                    return;
                }
                if (getColor(_txttraining_group_color.Text) == Color.Empty)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('สีกลุ่มวิชา ไม่ถูกต้อง');", true);
                    return;
                }
                GvMaster.EditIndex = -1;

                _data_elearning.el_m0_training_group_action = new m0_training_group[1];
                m0_training_group obj = new m0_training_group();

                obj.m0_training_group_idx = m0_training_group_idx_update;
                obj.training_group_code = _txttraining_group_code.Text;
                obj.training_group_name = _txttraining_group_name.Text;
                obj.training_group_remark = _txttraining_group_remark.Text;
                obj.training_group_color = _txttraining_group_color.Text;
                obj.training_group_status = int.Parse(_ddltraining_group_status.SelectedValue);
                obj.training_group_updated_by = emp_idx;

                _data_elearning.el_m0_training_group_action[0] = obj;

                // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
                _data_elearning = _func_dmu.zCallServicePostNetwork(_urlSetUpdel_m0_training_group, _data_elearning);

                if (_data_elearning.return_code == 0)
                {
                    actionIndex();
                }
                else
                {
                   setError(_data_elearning.return_code.ToString() + " - " + _data_elearning.return_msg);
                }


                break;
        }
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj, string sysidx_menu, string menuidx_per, string role_per)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();

        //switch (fvName.ID)
        //{



        //}
    }

    protected void setDataList(DataList dtlName, Object obj)
    {
        dtlName.DataSource = obj;
        dtlName.DataBind();
    }

    #endregion bind data

    #region reuse
    protected void initPage()
    {
        clearSession();
        clearViewState();

        // divAction.Visible = true;
        // gvBookingType.Visible = true;
        // fvBookingType.Visible = false;

        // getGridData("booking_type", 20);
        //setFormData(fvMenu, FormViewMode.Insert, null, "0", "0", "0");


    }

    protected void initPagePermission()
    {
        clearSession();
        clearViewState();

        // divAction.Visible = true;
        // gvBookingType.Visible = true;
        // fvBookingType.Visible = false;

        // getGridData("booking_type", 20);
        //setFormData(fvPermission, FormViewMode.Insert, null, "0", "0", "0");


    }

    protected void setVisible()
    {
        // divAction.Visible = !divAction.Visible;
        // gvBookingType.Visible = !gvBookingType.Visible;
        // fvBookingType.Visible = !fvBookingType.Visible;
    }

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        // ViewState["listData"] = null;
    }

    protected void setError(string _errorText)
    {
        // divShowError.Visible = !divShowError.Visible;
        // litErrorCode.Text = _dataEmployee.return_code.ToString();
        if (_errorText != "")
        {
            litDebug.Text = "error : " + _errorText;
        }
        else
        {
            litDebug.Text = String.Empty;
        }
    }
    
    public Boolean checkCodeError(int _m0_training_group_idx, string _training_group_code)
    {
        Boolean error = false;
        data_elearning data_elearning = new data_elearning();
        data_elearning.el_m0_training_group_action = new m0_training_group[1];

        m0_training_group obj = new m0_training_group();

        obj.m0_training_group_idx = _m0_training_group_idx;
        obj.training_group_code = _training_group_code;
        data_elearning.el_m0_training_group_action[0] = obj;
       // litDebug.Text = _func_dmu.zJson(_urlGetErrorel_m0_training_group, data_elearning);

         data_elearning = _func_dmu.zCallServiceNetwork(_urlGetErrorel_m0_training_group, data_elearning);

        if (data_elearning.el_m0_training_group_action != null)
        {
            //obj = data_elearning.el_m0_training_group_action[0];
            //if (obj.training_group_code.ToString().Trim()== _training_group_code.Trim())
            //{

            //}
            error = true;

        }
        return error;
    }

    public Color getColor(string sColor)
    {
        Color _color1 = Color.Empty;
        if (sColor != "")
        {
            try
            {
                _color1 = ColorTranslator.FromHtml(sColor);
            }
            catch
            {
                _color1 = Color.Empty;
            }
        }
        //txttraining_group_color.BackColor = System.Drawing.ColorTranslator.FromHtml("#008CBA")
        return _color1;
    }

    #endregion reuse

}