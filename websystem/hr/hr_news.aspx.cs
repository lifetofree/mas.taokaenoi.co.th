﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Diagnostics.Contracts;
using System.Data;
using System.Data.SqlClient;
using ASP;
using System.Diagnostics;
using System.Net;
using System.Collections;




using System.Globalization;
using System.IO;
using System.Web.UI.HtmlControls;

public partial class websystem_hr_hr_news : System.Web.UI.Page
{
    function_tool _functionTool = new function_tool();
    data_hr_news _data_hr_news = new data_hr_news();
    data_employee _dataEmployee = new data_employee();
    string _localJson = string.Empty;
    string textstatus = string.Empty;
    string textTypeFile = string.Empty;
    //string urlfortest = string.Empty;
    int _emp_idx = 0;
    int _rpos_idx = 0;
    //string textstatus = string.Empty;
    //string msg = string.Empty;
    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlHrGetHrNews = _serviceUrl + ConfigurationManager.AppSettings["urlHrGetHrNews"];
    static string _urlHrSetHrNews = _serviceUrl + ConfigurationManager.AppSettings["urlHrSetHrNews"];

    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        getEmployeeProfile(_emp_idx);

        //_rpos_idx = int.Parse(Session["rpos_idx"].ToString());
        //_rpos_idx.ToString();
        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];

        _rpos_idx = int.Parse(_dataEmployee.employee_list[0].rpos_idx.ToString());
        //check permission
        //foreach (int item in rdept_qmr)
        //{
        //    if (_dataEmployee.employee_list[0].rdept_idx == item)
        //    {
        //        _flag_qmr = true;
        //        break;
        //    }
        //}

    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    protected void DropDownListTrigger(DropDownList DropDownListID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = DropDownListID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    protected void fvTrigger(FormView FormviewID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = FormviewID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    protected void gvTrigger(GridView gvname)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = gvname.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }
    

    protected void HtmlControlTrigger(HtmlControl HtmlControl)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = HtmlControl.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ViewState["forSearch"] = null;
            //ClearChromCache();
            //ClearApplicationCache();
            //litdebug.Text = _rpos_idx.ToString();

            checkedPermision();
            setActiveTab("tab0");
            //divmyModal.Visible = false;

            selectModal();

        }
        ClearChromCache();
        ClearApplicationCache();
        checkedPermision();
        // divmyModal.Visible = false;
        setTrigger();

        //TextBox tinymce = (TextBox)FvListNews.FindControl("tb_listnewsDesc");
        ////_functionTool.setFvData(FvListNews, FormViewMode.Insert, null);
        //litdebug.Text = "...";
        //tinymce.Text = HttpUtility.HtmlDecode(tinymce.Text);
        
        ScriptManager.RegisterOnSubmitStatement(this, this.GetType(), "", "tinymce.triggerSave();");
    }

    protected void navCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        ViewState["forSearch"] = null;
        setActiveTab(cmdArg);
    }

    protected void setActiveTab(string activeTab)
    {
        li0.Attributes.Add("class", "xx");
        li1.Attributes.Add("class", "xx");
        li2.Attributes.Add("class", "xx");
        li3.Attributes.Add("class", "xx");
        li4.Attributes.Add("class", "xx");

        //button
        lbCreateTypenews.Visible = false;
        lbCreateTitleNews.Visible = false;
        lbCreateListNews.Visible = false;
        lbCreateSlideNews.Visible = false;
    

        //table
        gvTypeNews.Visible = false;
        gvTitleNews.Visible = false;
        gvListNews.Visible = false;
        gvSlideNews.Visible = false;
   

        //form
        FvTypeNews.Visible = false;
        FvTitleNews.Visible = false;
        FvListNews.Visible = false;
        FvSlideNews.Visible = false;
        searchListNews.Visible = false;
        searchTitleNews.Visible = false;
        searchTypeNews.Visible = false;
        searchSlideNews.Visible = false;
        //fvlistdetail.Visible = false;

        //detail
        //listdetail.Visible = false;
        switch (activeTab)
        {
            case "tab0":
                li0.Attributes.Add("class", "active");
                mvNews.SetActiveView((View)mvNews.FindControl("vNews"));
                selectTypeNews();
                //selecttitleNews(4);
                selectListNews(0,"",1);
                selectSlide();
                menulistNews.Visible = true;
                ViewState["btnmenuall_news"] = null;
                //divmyModal.Visible = true;
                
                break;

            case "tab1":
                li1.Attributes.Add("class", "active");
                mvNews.SetActiveView((View)mvNews.FindControl("vTypeNews"));
                showTypeNews();
                gvTypeNews.Visible = true;
                lbCreateTypenews.Visible = true;
                _functionTool.setFvData(searchTypeNews, FormViewMode.Insert, null);
                searchTypeNews.Visible = true;
                break;

            case "tab2":
                li2.Attributes.Add("class", "active");
                mvNews.SetActiveView((View)mvNews.FindControl("vTitleNews"));
                showTitleNews();
                gvTitleNews.Visible = true;
                lbCreateTitleNews.Visible = true;
                _functionTool.setFvData(searchTitleNews, FormViewMode.Insert, null);
                searchTitleNews.Visible = true;
                break;
              
                
            case "tab3":
                li3.Attributes.Add("class", "active");
                mvNews.SetActiveView((View)mvNews.FindControl("vListNews"));
                showListNews();
                gvListNews.Visible = true;
                lbCreateListNews.Visible = true;
                _functionTool.setFvData(searchListNews, FormViewMode.Insert, null);

                DropDownList DropDowntypeNews = (DropDownList)searchListNews.FindControl("DropDowntypeNews_s");
                selectTypeNews(DropDowntypeNews, 0);

                DropDownList DropDownTitleNews = (DropDownList)searchListNews.FindControl("DropDownTitleNews_s");
                DropDownTitleNews.Items.Insert(0, new ListItem("---เลือกหัวข้อข่าวสาร---", ""));
                DropDownTitleNews.SelectedValue = "";


                searchListNews.Visible = true;

                break;

            case "tab4":
                li4.Attributes.Add("class", "active");
                mvNews.SetActiveView((View)mvNews.FindControl("vSlideNews"));
                showSlideNews();
                gvSlideNews.Visible = true;
                lbCreateSlideNews.Visible = true;
                _functionTool.setFvData(searchSlideNews, FormViewMode.Insert, null);
                searchSlideNews.Visible = true;
                break;


        }
    }

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        data_hr_news _data_hr_news = new data_hr_news();
        hr_news_type_detail_m0 _hr_news_type_detail_m0 = new hr_news_type_detail_m0();
        hr_news_type_detail_m1 _hr_news_type_detail_m1 = new hr_news_type_detail_m1();
        hr_news_list_detail_u0 _hr_news_list_detail_u0 = new hr_news_list_detail_u0();
        hr_news_slide_detail_u0 _hr_news_slide_detail_u0 = new hr_news_slide_detail_u0();

        string cmdName = e.CommandName.ToString();
        //string cmdArg = e.CommandArgument.ToString();

        char[] delimiterChars = { ',' };
        string[] words = e.CommandArgument.ToString().Split(delimiterChars);
        string id = words[0];
        string name = words[1];
        divmyModal.Visible = false;
        //button
        lbCreateTypenews.Visible = false;
        lbCreateTitleNews.Visible = false;
        lbCreateListNews.Visible = false;
        lbCreateSlideNews.Visible = false;
        

        //table
        gvTypeNews.Visible = false;
        gvTitleNews.Visible = false;
        gvListNews.Visible = false;
        gvSlideNews.Visible = false;
       

        //form
        FvTypeNews.Visible = false;
        FvTitleNews.Visible = false;
        FvListNews.Visible = false;
        FvSlideNews.Visible = false;
        searchListNews.Visible = false;
        searchTitleNews.Visible = false;
        searchTypeNews.Visible = false;
        searchSlideNews.Visible = false;

        //detail
        //fvlistdetail.Visible = false;
        menulistNews.Visible = false;

        switch (cmdName)
        {
            case "cmdCreate":
                if (name == "TypeNews")
                {
                    _functionTool.setFvData(FvTypeNews, FormViewMode.Insert, null);
                    FvTypeNews.Visible = true;
                }
                if (name == "TitleNews")
                {
                    _functionTool.setFvData(FvTitleNews, FormViewMode.Insert, null);
                    DropDownList ddl = (DropDownList)FvTitleNews.FindControl("DropDowntypeNews");
                    selectTypeNews(ddl, 0);
                    FvTitleNews.Visible = true;
                }
                if (name == "ListNews")
                {
                    _functionTool.setFvData(FvListNews, FormViewMode.Insert, null);
                    DropDownList ddl = (DropDownList)FvListNews.FindControl("DropDowntypeNews");
                    selectTypeNews(ddl, 0);

                    DropDownList ddlTitle = (DropDownList)FvListNews.FindControl("DropDownTitleNews");
                    ddlTitle.Items.Insert(0, new ListItem("---เลือกหัวข้อข่าวสาร---", ""));

                    FvListNews.Visible = true;
                    //setTriggerForm();
                }
                if (name == "SlideNews")
                {
                    _functionTool.setFvData(FvSlideNews, FormViewMode.Insert, null);
                    FvSlideNews.Visible = true;
                }

                break;

              
            case "cmdCancel":
                if (name == "TypeNews")
                {
                    gvTypeNews.Visible = true;
                    lbCreateTypenews.Visible = true;
                    searchTypeNews.Visible = true;
                }
                if (name == "TitleNews")
                {
                    gvTitleNews.Visible = true;
                    lbCreateTitleNews.Visible = true;
                    searchTitleNews.Visible = true;
                }
                if (name == "ListNews")
                {
                    gvListNews.Visible = true;
                    lbCreateListNews.Visible = true;
                    searchListNews.Visible = true;
                }
                if (name == "SlideNews")
                {
                    gvSlideNews.Visible = true;
                    lbCreateSlideNews.Visible = true;
                    searchSlideNews.Visible = true;
                }

                break;

            case "cmdSave":
               
                if (name == "TypeNews")
                {
                    //litdebug.Text = id;
                    InsertTypeNews(_functionTool.convertToInt(id));
                }
                if (name == "TitleNews")
                {
                    //litdebug.Text = id;
                    InsertTitleNews(_functionTool.convertToInt(id));
                }

                if (name == "ListNews")
                {
                    //litdebug.Text = id;
                    InsertListNews(_functionTool.convertToInt(id));
                    
                }
                if (name == "SlideNews")
                {
                    //litdebug.Text = id;
                    InsertSlideNews(_functionTool.convertToInt(id));

                }


                break;

            case "cmdDelete":
                if (name == "TypeNews")
                {
                    //litdebug.Text = id;
                    cmdDeleteTypeNews(_functionTool.convertToInt(id));
                    
                }
                if (name == "TitleNews")
                {
                    //litdebug.Text = id;
                    cmdDeleteTitleNews(_functionTool.convertToInt(id));
                }
                if (name == "ListNews")
                {
                    //litdebug.Text = id;
                    cmdDeleteListNews(_functionTool.convertToInt(id));
                    //searchListNews.Visible = true;
                    
                }
                if (name == "SlideNews")
                {
                    //litdebug.Text = id;
                    cmdDeleteSlideNews(_functionTool.convertToInt(id));
                    //searchListNews.Visible = true;

                }
                break;

            case "cmdEdit":
                //litdebug.Text = id;
                //litdebug.Text += name;
                if (name == "TypeNews")
                {
                    
                    _hr_news_type_detail_m0.type_idx = _functionTool.convertToInt(id);
                    _data_hr_news.news_mode = 1;
                    _data_hr_news.hr_news_type_m0 = new hr_news_type_detail_m0[1];
                    _data_hr_news.hr_news_type_m0[0] = _hr_news_type_detail_m0;

                   // litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_hr_news));
                    _data_hr_news = callServiceNews(_urlHrGetHrNews, _data_hr_news);
                    //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_hr_news));
                    _functionTool.setFvData(FvTypeNews, FormViewMode.Edit, _data_hr_news.hr_news_type_m0);
                    FvTypeNews.Visible = true;
                }

                if (name == "TitleNews")
                {

                    _hr_news_type_detail_m1.title_idx = _functionTool.convertToInt(id);
                    _data_hr_news.news_mode = 2;
                    _data_hr_news.hr_news_type_m1 = new hr_news_type_detail_m1[1];
                    _data_hr_news.hr_news_type_m1[0] = _hr_news_type_detail_m1;

                    //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_hr_news));
                    _data_hr_news = callServiceNews(_urlHrGetHrNews, _data_hr_news);
                    //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_hr_news));
                    _functionTool.setFvData(FvTitleNews, FormViewMode.Edit, _data_hr_news.hr_news_type_m1);
                    
                    DropDownList ddl = (DropDownList)FvTitleNews.FindControl("DropDowntypeNews");
                    selectTypeNews(ddl, _data_hr_news.hr_news_type_m1[0].type_idx);
                    FvTitleNews.Visible = true;
                }

                if (name == "ListNews")
                {
                    _hr_news_list_detail_u0.list_idx = _functionTool.convertToInt(id);
                    //_hr_news_list_detail_u0.type_idx = 0;
                    //_hr_news_list_detail_u0.title_idx = 0;
                    _data_hr_news.news_mode = 3;
                    _data_hr_news.hr_news_list_u0 = new hr_news_list_detail_u0[1];
                    _data_hr_news.hr_news_list_u0[0] = _hr_news_list_detail_u0;

                    //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_hr_news));
                    _data_hr_news = callServiceNews(_urlHrGetHrNews, _data_hr_news);
                    //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_hr_news));
                    _functionTool.setFvData(FvListNews, FormViewMode.Edit, _data_hr_news.hr_news_list_u0);

                    LinkButton lbopennewWindow = (LinkButton)FvListNews.FindControl("LinkButtonedititem");
                    string url = FormatImageUrl(_data_hr_news.hr_news_list_u0[0].list_file, "list");

                    DropDownList ddl = (DropDownList)FvListNews.FindControl("DropDowntypeNews");
                    selectTypeNews(ddl, 0);
                    ddl.SelectedValue = _data_hr_news.hr_news_list_u0[0].type_idx.ToString();

                    //Response.ExpiresAbsolute = DateTime.Now.AddDays(-1d);
                    //Response.Expires = -1500;
                    //Response.CacheControl = "no-cache";
                    //IDictionaryEnumerator allCaches = HttpRuntime.Cache.GetEnumerator();

                    //while (allCaches.MoveNext())
                    //{
                    //    Cache.Remove(allCaches.Key.ToString());
                    //}
                    //ClearChromCache();
                    DropDownList ddlTitle = (DropDownList)FvListNews.FindControl("DropDownTitleNews");
                    selectDdl(ddlTitle, 2, _data_hr_news.hr_news_list_u0[0].type_idx);
                    ddlTitle.SelectedValue = _data_hr_news.hr_news_list_u0[0].title_idx.ToString();
                    //litdebug.Text = _data_hr_news.hr_news_list_u0[0].title_type_file.ToString();
                    //lbopennewWindow.Attributes.Add("OnClick", "window.open('" + ResolveUrl(url) + "'); return false;");

                    string extension = Path.GetExtension(_data_hr_news.hr_news_list_u0[0].list_file);

                   
                 

                        //preimg.Attributes.Add("OnClick", "window.open('www.google.com'); return false;");

                        if (_data_hr_news.hr_news_list_u0[0].title_type_file == 1 && extension != "") {
                        string file = FormatImageUrl(_data_hr_news.hr_news_list_u0[0].list_file, "list");
                        HtmlGenericControl preimgList = (HtmlGenericControl)FvListNews.FindControl("preimgList");
                        preimgList.InnerHtml = "<img id=\"preimgList\" src=\"" + ResolveUrl(file) + "\" class=\"img-responsive\"/>";
                        preimgList.Attributes.Add("OnClick", "window.open('" + ResolveUrl(file) + "'); return false;");
                        preimgList.Attributes.Add("class", "preimg cursor-pointer");
                        preimgList.Visible = true;
                    }
                    else if (_data_hr_news.hr_news_list_u0[0].title_type_file == 0 && extension != "")
                    {
                        string file = FormatImageUrl(_data_hr_news.hr_news_list_u0[0].list_file, "list");
                        HtmlGenericControl preimgList = (HtmlGenericControl)FvListNews.FindControl("preimgList");
                        preimgList.InnerHtml = "<i class=\"far fa-file-pdf fa-5x\"></i>";
                        preimgList.Attributes.Add("OnClick", "window.open('" + ResolveUrl(file) + "'); return false;");
                        preimgList.Attributes.Add("class", "prepdf cursor-pointer");
                        preimgList.Visible = true;
                    }
                    else
                    {

                        HtmlGenericControl preimgList = (HtmlGenericControl)FvListNews.FindControl("preimgList");
                        preimgList.Visible = false;

                    }
                    FvListNews.Visible = true;
                }

                if (name == "SlideNews")
                {


                    _hr_news_slide_detail_u0.slide_idx = _functionTool.convertToInt(id);
                    _data_hr_news.news_mode = 5;
                    _data_hr_news.hr_news_slide_u0 = new hr_news_slide_detail_u0[1];
                    _data_hr_news.hr_news_slide_u0[0] = _hr_news_slide_detail_u0;

                    //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_hr_news));
                    _data_hr_news = callServiceNews(_urlHrGetHrNews, _data_hr_news);
                    //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_hr_news));
                    string file = FormatImageUrl(_data_hr_news.hr_news_slide_u0[0].slide_file, "slide");
                    //litdebug.Text = ResolveUrl(file);
                    _functionTool.setFvData(FvSlideNews, FormViewMode.Edit, _data_hr_news.hr_news_slide_u0);
                    FvSlideNews.Visible = true;

                    HtmlGenericControl wordslide = (HtmlGenericControl)FvSlideNews.FindControl("wordslide");
                    HtmlGenericControl wordMain = (HtmlGenericControl)FvSlideNews.FindControl("wordMain");
                    DropDownList DropDownListshow = (DropDownList)FvSlideNews.FindControl("DropDownListshow");
                    FileUpload File_UploadSlideNews = (FileUpload)FvSlideNews.FindControl("File_Upload");
                    if (_data_hr_news.hr_news_slide_u0[0].slide_show == 1)
                    {
                        File_UploadSlideNews.Attributes.Add("onchange", "readURL(this);");
                        wordMain.Visible = false;
                        wordslide.Visible = true;

                    }
                    else if (_data_hr_news.hr_news_slide_u0[0].slide_show == 2)
                    {
                        File_UploadSlideNews.Attributes.Add("onchange", "");
                        wordMain.Visible = true;
                        wordslide.Visible = false;

                    }

                    HtmlGenericControl preimg = (HtmlGenericControl)FvSlideNews.FindControl("preimg");
                    //preimg.Attributes.Add("OnClick", "window.open('www.google.com'); return false;");
                    preimg.InnerHtml = "<img id=\"Imagepreimg\" src=\"" + ResolveUrl(file) + "\" class=\"img-responsive preimg cursor-pointer\"/>";
                    preimg.Attributes.Add("OnClick", "window.open('" + ResolveUrl(file) + "'); return false;");
                }
                break;

            case "cmdView":

                if (name == "SlideNews")
                {

                    //string FileNameWithOutextension = Path.GetFileNameWithoutExtension(id);

                    ////Response.Redirect("~/uploadfiles/hr_news/" + FileNameWithOutextension + "//" + id);

                    //string getPathfile = ConfigurationManager.AppSettings["path_flie_hr_news_slidenews"];
                    //string filePath_upload = getPathfile + FileNameWithOutextension + "\\" + id;
                    //litdebug.Text = filePath_upload;
                    //litdebug.Text = "ssss";
                    //Response.Write("<script type=\"text/javascript\"'>window.open('" + ResolveUrl(filePath_upload) + "');");
                    //Response.Write("</script>");
                    //setActiveTab("tab3");
                    //Response.ExpiresAbsolute = DateTime.Now.AddDays(-1d);
                    //Response.Expires = -1500;
                    //Response.CacheControl = "no-cache";
                    //IDictionaryEnumerator allCaches = HttpRuntime.Cache.GetEnumerator();

                    //while (allCaches.MoveNext())
                    //{
                    //    Cache.Remove(allCaches.Key.ToString());
                    //}

                }
                break;
                
                case "cmdReadMore":
                
                //litdebug.Text = "eiei";
                    if (name == "TypeNews")
                    {
                        //selecttitleNews(_functionTool.convertToInt(id));
                     }
                    if (name == "TitleNews")
                    {
                        selectListNews(_functionTool.convertToInt(id),"",0);
                        menulistNews.Visible = true;
                        //litdebug.Text = "eieiwewsf";
                    }

                    if (name == "ListNews")
                    {
                        selectListNewsDetail(_functionTool.convertToInt(id));


                    }
                


                break;

            case "cmdSearch":

                if (name == "ListNews")
                {
                    selectListNews(0,txtsearch.Text,0);
                    
                    menulistNews.Visible = true;

                }
                if (name == "TypeNewsmange")
                {
                    ViewState["forSearch"] = null;
                    selectListNewsDescSearch(1);
                    lbCreateTypenews.Visible = true;
                    gvTypeNews.Visible = true;
                    searchTypeNews.Visible = true;
                }
                if (name == "TitleNewsmange")
                {
                    ViewState["forSearch"] = null;
                    selectListNewsDescSearch(2);
                    lbCreateTitleNews.Visible = true;
                    gvTitleNews.Visible = true;
                    searchTitleNews.Visible = true;
                }
                if (name == "ListNewsmange")
                {
                    ViewState["forSearch"] = null;
                    //TextBox txtsearchMange = (TextBox)searchListNews.FindControl("tb_listnews_s");
                    //DropDownList ddl = (DropDownList)searchListNews.FindControl("DropDownstatus_s");
                    //selectListNewsDescSearch(0, txtsearchMange.Text, ddl.SelectedValue);
                    selectListNewsDescSearch(3);
                    lbCreateListNews.Visible = true;
                    gvListNews.Visible = true;

                    searchListNews.Visible = true;
                }
                if (name == "SlideNewsmange")
                {
                    ViewState["forSearch"] = null;
                    //TextBox txtsearchMange = (TextBox)searchListNews.FindControl("tb_listnews_s");
                    //DropDownList ddl = (DropDownList)searchListNews.FindControl("DropDownstatus_s");
                    //selectListNewsDescSearch(0, txtsearchMange.Text, ddl.SelectedValue);
                    selectListNewsDescSearch(5);
                    lbCreateSlideNews.Visible = true;
                    gvSlideNews.Visible = true;
                    searchSlideNews.Visible = true;
                }


                break;

            case "cmdHome":

                if (name == "ListNews")
                {
                    mvNews.SetActiveView((View)mvNews.FindControl("vNews"));
                    selectTypeNews();
                    //selecttitleNews(4);
                    selectListNews(0, "", 1);
                    menulistNews.Visible = true;
                    txtsearch.Text = String.Empty;



                }

                break;



            

            case "cmdReset":
                if (name == "TypeNewsmange")
                {
                    ViewState["forSearch"] = null;
                    showTypeNews();
                    gvTypeNews.Visible = true;
                    lbCreateTypenews.Visible = true;
                    _functionTool.setFvData(searchTypeNews, FormViewMode.Insert, null);
                    searchTypeNews.Visible = true;
                }

                if (name == "TitleNewsmange")
                {
                    ViewState["forSearch"] = null;
                    showTitleNews();
                    gvTitleNews.Visible = true;
                    lbCreateTitleNews.Visible = true;
                    _functionTool.setFvData(searchTitleNews, FormViewMode.Insert, null);
                    searchTitleNews.Visible = true;
                }
                if (name == "ListNewsmange")
                {
                    ViewState["forSearch"] = null;
                    showListNews();
                    gvListNews.Visible = true;
                    lbCreateListNews.Visible = true;
                    _functionTool.setFvData(searchListNews, FormViewMode.Insert, null);

                    DropDownList DropDowntypeNews = (DropDownList)searchListNews.FindControl("DropDowntypeNews_s");
                    selectTypeNews(DropDowntypeNews, 0);

                    DropDownList DropDownTitleNews = (DropDownList)searchListNews.FindControl("DropDownTitleNews_s");
                    DropDownTitleNews.Items.Insert(0, new ListItem("---เลือกหัวข้อข่าวสาร---", ""));
                    DropDownTitleNews.SelectedValue = "";


                    searchListNews.Visible = true;


                }
                if (name == "SlideNewsmange")
                {
                    ViewState["forSearch"] = null;
                    showSlideNews();
                    gvSlideNews.Visible = true;
                    lbCreateSlideNews.Visible = true;
                    _functionTool.setFvData(searchSlideNews, FormViewMode.Insert, null);
                    searchSlideNews.Visible = true;


                }
                break;
            case "btnmenuall_news":
                if (name == "btnmenuall_news")
                {
                    if (ViewState["btnmenuall_news"] != null)
                    {
                        RepeaterListNews.DataSource = ViewState["btnmenuall_news"];//_dataNews.u0_it_news_list;
                        RepeaterListNews.DataBind();
                        //ViewState["btnmenuall_news"]litdebug.Text = "ss";

                    }
                    else
                    {
                        _data_hr_news.news_mode = 4;
                        _hr_news_list_detail_u0.title_idx = 0;
                        _hr_news_list_detail_u0.list_name = "";
                        _data_hr_news.hr_news_list_u0 = new hr_news_list_detail_u0[1];
                        _data_hr_news.hr_news_list_u0[0] = _hr_news_list_detail_u0;

                        _data_hr_news = callServiceNews(_urlHrGetHrNews, _data_hr_news);

                        ViewState["btnmenuall_news"] = _data_hr_news.hr_news_list_u0;
                        RepeaterListNews.DataSource = _data_hr_news.hr_news_list_u0;//_dataNews.u0_it_news_list;
                        RepeaterListNews.DataBind();
                    }
                    btnmenuall_news.Visible = false;
                    btnmenuall_Hidden.Visible = true;
                    menulistNews.Visible = true;
                }
               
                break;

            case "btnmenuall_Hidden":
                if (name == "btnmenuall_Hidden")
                {

                    selectListNews(0, "",1);

                    btnmenuall_news.Visible = true;
                    btnmenuall_Hidden.Visible = false;
                    menulistNews.Visible = true;
                }
                break;


        }
    }

    protected void selectModal()
    {

        data_hr_news _data_hr_news = new data_hr_news();

        hr_news_slide_detail_u0 _hr_news_slide_detail_u0 = new hr_news_slide_detail_u0();

        _hr_news_slide_detail_u0.slide_idx = 0;
        _hr_news_slide_detail_u0.slide_status = 1;
        _hr_news_slide_detail_u0.slide_show = 2;
        _data_hr_news.news_mode = 6;
        _data_hr_news.hr_news_slide_u0 = new hr_news_slide_detail_u0[1];
        _data_hr_news.hr_news_slide_u0[0] = _hr_news_slide_detail_u0;
        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_hr_news));
        _data_hr_news = callServiceNews(_urlHrGetHrNews, _data_hr_news);
        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_hr_news));

        //var _linq_news = from data_news in _data_hr_news.hr_news_slide_u0.Take(1)
        //select data_news;


        //images.DataSource = _linq_news.ToList();
        if (_data_hr_news.hr_news_slide_u0 == null)
        {
            divmyModal.Visible = false;
           

        }
        else
        {
            Repeatermodal.DataSource = _data_hr_news.hr_news_slide_u0;
            Repeatermodal.DataBind();
            divmyModal.Visible = true;
        }
    }

    protected void selectSlide()
    {

        data_hr_news _data_hr_news = new data_hr_news();

        hr_news_slide_detail_u0 _hr_news_slide_detail_u0 = new hr_news_slide_detail_u0();

       // HtmlControl divmyModal = (HtmlControl)FindControl("divmyModal");
        //HtmlControl divcarousel = (HtmlControl)FindControl("divcarousel");
        


        _hr_news_slide_detail_u0.slide_idx = 0;
        _hr_news_slide_detail_u0.slide_status = 1;
        _hr_news_slide_detail_u0.slide_show = 1;
        _data_hr_news.news_mode = 6;
        _data_hr_news.hr_news_slide_u0 = new hr_news_slide_detail_u0[1];
        _data_hr_news.hr_news_slide_u0[0] = _hr_news_slide_detail_u0;
        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_hr_news));
        _data_hr_news = callServiceNews(_urlHrGetHrNews, _data_hr_news);
        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_hr_news));

        //var _linq_news = from data_news in _data_hr_news.hr_news_slide_u0.Take(1)
        //select data_news;


        //images.DataSource = _linq_news.ToList();
       

        if (_data_hr_news.hr_news_slide_u0 == null)
        {
            divcarousel.Visible = false;
           

        }
        else
        {
            images.DataSource = _data_hr_news.hr_news_slide_u0;
            images.DataBind();

            imagesNumber.DataSource = _data_hr_news.hr_news_slide_u0;
            imagesNumber.DataBind();
            divcarousel.Visible = true;
        }

        }

    protected void selectTypeNews()
    {

        data_hr_news _data_hr_news = new data_hr_news();
        
        hr_news_type_detail_m0 _hr_news_type_detail_m0 = new hr_news_type_detail_m0();

        _data_hr_news.news_mode = 1;
        _hr_news_type_detail_m0.type_idx = 0;
        _hr_news_type_detail_m0.type_status = 1;
        _data_hr_news.hr_news_type_m0 = new hr_news_type_detail_m0[1];
        _data_hr_news.hr_news_type_m0[0] = _hr_news_type_detail_m0;
        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_hr_news));
        _data_hr_news = callServiceNews(_urlHrGetHrNews, _data_hr_news);
        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_hr_news));

        //var _linq_news = from data_news in _data_hr_news.hr_news_type_m0.Take(5)
                         //select data_news;



        rplistnews.DataSource = _data_hr_news.hr_news_type_m0;//_dataNews.u0_it_news_list;
        rplistnews.DataBind();

    }

    protected void selecttitleNews(int _idx, Repeater RepeaterView)
    {

        data_hr_news _data_hr_news = new data_hr_news();

        hr_news_type_detail_m1 _hr_news_type_detail_m1 = new hr_news_type_detail_m1();


        _data_hr_news.news_mode = 2;
        _hr_news_type_detail_m1.type_idx = _idx;
        _hr_news_type_detail_m1.title_status = 1;
        _data_hr_news.hr_news_type_m1 = new hr_news_type_detail_m1[1];
        _data_hr_news.hr_news_type_m1[0] = _hr_news_type_detail_m1;
        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_hr_news));
        _data_hr_news = callServiceNews(_urlHrGetHrNews, _data_hr_news);
        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_hr_news));

        //try {
        //var _linq_news = from data_news in _data_hr_news.hr_news_type_m1.Take(5)
        //                 select data_news;




        RepeaterView.DataSource = _data_hr_news.hr_news_type_m1;//_dataNews.u0_it_news_list;
        RepeaterView.DataBind();
           //_functionTool.setRptData(Repeater1, _data_hr_news.hr_news_type_m1);
        //}
        //catch
        //{

        //}
    }

    protected void selectListNewsDescSearch(int mode)
    {
        DropDownList DropDownListshow = (DropDownList)searchSlideNews.FindControl("DropDownListshow");
        DropDownList ddlSlide = (DropDownList)searchSlideNews.FindControl("DropDownstatus_s");
        TextBox txt_timestart_createSlide = (TextBox)searchSlideNews.FindControl("txt_timestart_create");
        TextBox txt_timeend_createSlide = (TextBox)searchSlideNews.FindControl("txt_timeend_create");

        TextBox txtsearchMange = (TextBox)searchListNews.FindControl("tb_serchnews_s");
        DropDownList ddl = (DropDownList)searchListNews.FindControl("DropDownstatus_s");
        TextBox txt_timestart_create = (TextBox)searchListNews.FindControl("txt_timestart_create");
        TextBox txt_timeend_create = (TextBox)searchListNews.FindControl("txt_timeend_create");
        DropDownList DropDowntypeNews_s = (DropDownList)searchListNews.FindControl("DropDowntypeNews_s");
        DropDownList DropDownTitleNews_s = (DropDownList)searchListNews.FindControl("DropDownTitleNews_s");

        TextBox tb_serchnews_s = (TextBox)searchTitleNews.FindControl("tb_serchnews_s");
        DropDownList ddlTitle = (DropDownList)searchTitleNews.FindControl("DropDownstatus_s");

        TextBox tb_serchTypenews_s = (TextBox)searchTypeNews.FindControl("tb_serchnews_s");
        DropDownList ddlType = (DropDownList)searchTypeNews.FindControl("DropDownstatus_s");


        data_hr_news _data_hr_news = new data_hr_news();

        hr_news_list_detail_u0 _hr_news_list_detail_u0 = new hr_news_list_detail_u0();
        hr_news_search_detail _hr_news_search_detail = new hr_news_search_detail();

        if (mode == 1)
        {

            _data_hr_news.news_mode = mode;
            _hr_news_search_detail.s_name = tb_serchTypenews_s.Text.Trim();
            _hr_news_search_detail.s_status = ddlType.SelectedValue;
            _hr_news_search_detail.s_start_time = "";
            _hr_news_search_detail.s_end_time = "";
            _hr_news_search_detail.s_show = "";

            _data_hr_news.hr_news_search_list = new hr_news_search_detail[1];
            _data_hr_news.hr_news_search_list[0] = _hr_news_search_detail;
            //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_hr_news));
            _data_hr_news = callServiceNews(_urlHrGetHrNews, _data_hr_news);
            ViewState["forSearch"] = _data_hr_news;
            showTypeNews();
        }

        if (mode == 2)
        {

            _data_hr_news.news_mode = mode;
            _hr_news_search_detail.s_name = tb_serchnews_s.Text.Trim();
            _hr_news_search_detail.s_status = ddlTitle.SelectedValue;
            _hr_news_search_detail.s_start_time = "";
            _hr_news_search_detail.s_end_time = "";
            _hr_news_search_detail.s_show = "";

            _data_hr_news.hr_news_search_list = new hr_news_search_detail[1];
            _data_hr_news.hr_news_search_list[0] = _hr_news_search_detail;
            //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_hr_news));
            _data_hr_news = callServiceNews(_urlHrGetHrNews, _data_hr_news);
            ViewState["forSearch"] = _data_hr_news;
            showTitleNews();
        }
        if (mode == 3)
        {
            _data_hr_news.news_mode = mode;
            _hr_news_search_detail.s_type = DropDowntypeNews_s.SelectedValue;
            _hr_news_search_detail.s_title = DropDownTitleNews_s.SelectedValue;
            _hr_news_search_detail.s_name = txtsearchMange.Text.Trim();
            _hr_news_search_detail.s_status = ddl.SelectedValue;
            _hr_news_search_detail.s_start_time = txt_timestart_create.Text.Trim();
            _hr_news_search_detail.s_end_time = txt_timeend_create.Text.Trim();
            _hr_news_search_detail.s_show = "";
            

            _data_hr_news.hr_news_search_list = new hr_news_search_detail[1];
            _data_hr_news.hr_news_search_list[0] = _hr_news_search_detail;
            //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_hr_news));
            _data_hr_news = callServiceNews(_urlHrGetHrNews, _data_hr_news);
            ViewState["forSearch"] = _data_hr_news;
            showListNews();
        }
        if (mode == 5)
        {
            _data_hr_news.news_mode = mode;
            _hr_news_search_detail.s_name = "";
            _hr_news_search_detail.s_status = ddlSlide.SelectedValue;
            _hr_news_search_detail.s_start_time = txt_timestart_createSlide.Text.Trim();
            _hr_news_search_detail.s_end_time = txt_timeend_createSlide.Text.Trim();
            _hr_news_search_detail.s_show = DropDownListshow.SelectedValue;

            _data_hr_news.hr_news_search_list = new hr_news_search_detail[1];
            _data_hr_news.hr_news_search_list[0] = _hr_news_search_detail;
            //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_hr_news));
            _data_hr_news = callServiceNews(_urlHrGetHrNews, _data_hr_news);
            ViewState["forSearch"] = _data_hr_news;
            showSlideNews();
        }
    }

    protected void selectListNews(int _idx , string txtSearch , int i)
    {

        data_hr_news _data_hr_news = new data_hr_news();

        hr_news_list_detail_u0 _hr_news_list_detail_u0 = new hr_news_list_detail_u0();

        _data_hr_news.news_mode = 4;
        _hr_news_list_detail_u0.title_idx = _idx;
        _hr_news_list_detail_u0.list_name = txtSearch;
        _data_hr_news.hr_news_list_u0= new hr_news_list_detail_u0[1];
        _data_hr_news.hr_news_list_u0[0] = _hr_news_list_detail_u0;

        _data_hr_news = callServiceNews(_urlHrGetHrNews, _data_hr_news);

        

        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_hr_news));
        

        if (_data_hr_news.hr_news_list_u0 == null && i == 0)
        {
            //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่พบข้อมูล !!!');", true);
           // _data_hr_news.news_mode = 4;
            _hr_news_list_detail_u0.list_idx = 0;
            _hr_news_list_detail_u0.list_name = "ไม่พบข้อมูล";
            _hr_news_list_detail_u0.type_name = "ไม่พบข้อมูล";
            _hr_news_list_detail_u0.title_name = "ไม่พบข้อมูล";
            _data_hr_news.hr_news_list_u0 = new hr_news_list_detail_u0[1];
            _data_hr_news.hr_news_list_u0[0] = _hr_news_list_detail_u0;


            RepeaterListNews.DataSource = _data_hr_news.hr_news_list_u0;//_dataNews.u0_it_news_list;
            RepeaterListNews.DataBind();
            // RepeaterListNews.DataSource = _data_hr_news.hr_news_list_u0;//_dataNews.u0_it_news_list;
            // RepeaterListNews.DataBind();
            btnmenuall_news.Visible = false;
            btnmenuall_Hidden.Visible = false;


        }
        else if (i == 1)
        {
            var _linq_news = from data_news in _data_hr_news.hr_news_list_u0.Take(10)
                             select data_news;

            //RepeaterListNews.DataSource = _data_hr_news.hr_news_list_u0;//_dataNews.u0_it_news_list;
            //RepeaterListNews.DataBind();
            RepeaterListNews.DataSource = _linq_news;//_dataNews.u0_it_news_list;
            RepeaterListNews.DataBind();
            btnmenuall_news.Visible = true;
            btnmenuall_Hidden.Visible = false;
        }
        else if (i == 0)
        {

            //RepeaterListNews.DataSource = _data_hr_news.hr_news_list_u0;//_dataNews.u0_it_news_list;
            //RepeaterListNews.DataBind();
            RepeaterListNews.DataSource = _data_hr_news.hr_news_list_u0;//_dataNews.u0_it_news_list;
            RepeaterListNews.DataBind();
            btnmenuall_news.Visible = false;
            btnmenuall_Hidden.Visible = false;

        }


    }

    protected void selectListNewsDetail(int _idx)
    {

        //if (_idx != 0)
        //{
            data_hr_news _data_hr_news = new data_hr_news();

            hr_news_list_detail_u0 _hr_news_list_detail_u0 = new hr_news_list_detail_u0();

            _data_hr_news.news_mode = 4;
            _hr_news_list_detail_u0.list_idx = _idx;
            _data_hr_news.hr_news_list_u0 = new hr_news_list_detail_u0[1];
            _data_hr_news.hr_news_list_u0[0] = _hr_news_list_detail_u0;

            _data_hr_news = callServiceNews(_urlHrGetHrNews, _data_hr_news);
            //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_hr_news));
            //var _linq_news = from data_news in _data_hr_news.hr_news_list_u0.Take(5)
            //                 select data_news;
            //ViewState["detail"] = null;
            //ViewState["detail2"] = null;
            //ViewState["detail"] = _data_hr_news.hr_news_list_u0[0].list_desc;

            //ViewState["detail2"] = FormatImageUrl(_data_hr_news.hr_news_list_u0[0].list_file);
            string file = FormatImageUrl(_data_hr_news.hr_news_list_u0[0].list_file, "list");
            int typefile = _data_hr_news.hr_news_list_u0[0].title_type_file;
            //litdebug.Text = (string)ViewState["detail2"];
            string extension = Path.GetExtension(_data_hr_news.hr_news_list_u0[0].list_file);

            //LiteralName.Text = _data_hr_news.hr_news_list_u0[0].list_name;
            //LiteralDesc.Text = HttpUtility.HtmlDecode(HttpUtility.HtmlDecode(_data_hr_news.hr_news_list_u0[0].list_desc));
            if (typefile == 1 && extension != "")
            {

                //ltEmbed.Text = "<img id=\"Image3\" src=\"" + ResolveUrl(file) + "\" class=\"img-responsive\"/>";
               // fvlistdetail.Visible = true;
                menulistNews.Visible = false;

                //litdebug.Text = ResolveUrl(file);
            }
            else if (typefile == 0 && extension != "")
            {

               // ltEmbed.Text = "<embed id=\"Image2\" src=\"" + ResolveUrl(file) + "\"type=\"application/pdf\" width=\"100%\" height=\"800px\"/>";
                //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(file));
                //fvlistdetail.Visible = true;
                menulistNews.Visible = false;
            }
            else
            {
               // ltEmbed.Text = "";
                //fvlistdetail.Visible = true;
                menulistNews.Visible = false;
            }


            //RepeaterListNews.DataSource = _data_hr_news.hr_news_list_u0;//_dataNews.u0_it_news_list;
            //RepeaterListNews.DataBind();
        //}
        //else
        //{
            
        //    menulistNews.Visible = true;
        //}
    }

    public bool convertbuttonOnclick(int id)
    {
        if (id == 0)
        {
            return false;
        }
        else
        {
            return true;
        }

    }

    public bool convertVisible()
    {
        FileUpload File_Upload = (FileUpload)FvListNews.FindControl("File_Upload");
        if (File_Upload.HasFile)
        {
            return true;
        }
        else
        {
            return false;
        }

    }

    public string convertCssOnclick(string word)
    {
        if (word == "0")
        {
            word = "list-group-item btn-default";
        }
        else
        {
            word = "list-group-item btn-success";
        }
        return word;
    }

   

    protected void InsertTypeNews(int id)
    {
        TextBox text_name = (TextBox)FvTypeNews.FindControl("tb_typenews");
        DropDownList dropD_status = (DropDownList)FvTypeNews.FindControl("ddStatus");

        data_hr_news _data_hr_news = new data_hr_news();
        hr_news_type_detail_m0 _hr_news_type_detail_m0 = new hr_news_type_detail_m0();
        _data_hr_news.news_mode = 1;
        _hr_news_type_detail_m0.type_idx = id;
        _hr_news_type_detail_m0.type_name = text_name.Text.Trim();
        _hr_news_type_detail_m0.type_status = _functionTool.convertToInt(dropD_status.SelectedValue);
        _hr_news_type_detail_m0.cemp_idx = _emp_idx;

        _data_hr_news.hr_news_type_m0 = new hr_news_type_detail_m0[1];
        _data_hr_news.hr_news_type_m0[0] = _hr_news_type_detail_m0;

        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_hr_news));
        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_hr_news));
        _data_hr_news = callServiceNews(_urlHrSetHrNews, _data_hr_news);
        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_hr_news));

        if (_data_hr_news.return_code == 1)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ข้อมูลซ้ำ กรุณาแก้ไขใหม่อีกครั้ง!!!');", true);
            FvTypeNews.Visible = true;
        }
        else
        {
            lbCreateTypenews.Visible = true;
            FvTypeNews.Visible = false;
            showTypeNews();
            gvTypeNews.Visible = true;
            _functionTool.setFvData(searchTypeNews, FormViewMode.Insert, null);
            searchTypeNews.Visible = true;

        }
    }

    protected void InsertTitleNews(int id)
    {
        TextBox text_name = (TextBox)FvTitleNews.FindControl("tb_titlenews");
        DropDownList dropD_status = (DropDownList)FvTitleNews.FindControl("ddStatus");
        DropDownList dropD_typeNews = (DropDownList)FvTitleNews.FindControl("DropDowntypeNews");
        DropDownList dropD_typefile = (DropDownList)FvTitleNews.FindControl("ddltypefile");

        data_hr_news _data_hr_news = new data_hr_news();
        hr_news_type_detail_m1 _hr_news_type_detail_m1 = new hr_news_type_detail_m1();
        _data_hr_news.news_mode = 2;
        _hr_news_type_detail_m1.title_idx = id;
        _hr_news_type_detail_m1.title_name = text_name.Text.Trim();
        _hr_news_type_detail_m1.title_status = _functionTool.convertToInt(dropD_status.SelectedValue);
        _hr_news_type_detail_m1.type_idx = _functionTool.convertToInt(dropD_typeNews.SelectedValue);
        _hr_news_type_detail_m1.title_type_file = _functionTool.convertToInt(dropD_typefile.SelectedValue);
        _hr_news_type_detail_m1.cemp_idx = _emp_idx;

        _data_hr_news.hr_news_type_m1 = new hr_news_type_detail_m1[1];
        _data_hr_news.hr_news_type_m1[0] = _hr_news_type_detail_m1;

        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_hr_news));
        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_hr_news));
        _data_hr_news = callServiceNews(_urlHrSetHrNews, _data_hr_news);
        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_hr_news));

        if (_data_hr_news.return_code == 1)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ข้อมูลซ้ำ กรุณาแก้ไขใหม่อีกครั้ง!!!');", true);
            FvTitleNews.Visible = true;
        }
        else
        {
            lbCreateTitleNews.Visible = true;
            FvTitleNews.Visible = false;
            showTitleNews();
            gvTitleNews.Visible = true;
            _functionTool.setFvData(searchTitleNews, FormViewMode.Insert, null);
            searchTitleNews.Visible = true;
        }
    }

    protected void InsertListNews(int id)
    {
        TextBox text_name = (TextBox)FvListNews.FindControl("tb_listnewsname");
        TextBox text_desc = (TextBox)FvListNews.FindControl("tb_listnewsDesc");
        //TextBox text_date_start = (TextBox)FvListNews.FindControl("txtDateStart_create");
        TextBox text_time_start = (TextBox)FvListNews.FindControl("txt_timestart_create");
        //TextBox text_date_end = (TextBox)FvListNews.FindControl("txtDateEnd_create");
        TextBox text_time_end = (TextBox)FvListNews.FindControl("txt_timeend_create");
        TextBox fileuploadforEdit = (TextBox)FvListNews.FindControl("tbFile_Upload");

        DropDownList dropD_status = (DropDownList)FvListNews.FindControl("ddStatus");
        DropDownList dropD_typeNews = (DropDownList)FvListNews.FindControl("DropDowntypeNews");
        DropDownList dropD_titleNews = (DropDownList)FvListNews.FindControl("DropDownTitleNews");
        FileUpload File_Upload = (FileUpload)FvListNews.FindControl("File_Upload");

        CheckBox checkDelete = (CheckBox)FvListNews.FindControl("checkBoxDelete");
        //ClearChromCache();
        //ClearApplicationCache();
        data_hr_news _data_hr_news = new data_hr_news();
        hr_news_list_detail_u0 _hr_news_list_detail_u0 = new hr_news_list_detail_u0();
        ViewState["fileupload"] = null;

        if (File_Upload.HasFile)
        {

            string extension = Path.GetExtension(File_Upload.FileName);

            _data_hr_news.news_mode = 3;
            _hr_news_list_detail_u0.list_idx = id;
            _hr_news_list_detail_u0.list_name = text_name.Text.Trim();
            //_hr_news_list_detail_u0.list_desc = HttpUtility.HtmlEncode(text_desc.Text.Trim());
            _hr_news_list_detail_u0.list_desc = text_desc.Text.Trim();
            _hr_news_list_detail_u0.list_file = extension.Trim();
            _hr_news_list_detail_u0.list_status = _functionTool.convertToInt(dropD_status.SelectedValue);
            _hr_news_list_detail_u0.list_timeout_start = text_time_start.Text.Trim();
            _hr_news_list_detail_u0.list_timeout_end = text_time_end.Text.Trim();
            _hr_news_list_detail_u0.type_idx = _functionTool.convertToInt(dropD_typeNews.SelectedValue);
            _hr_news_list_detail_u0.title_idx = _functionTool.convertToInt(dropD_titleNews.SelectedValue);
            _hr_news_list_detail_u0.cemp_idx = _emp_idx;

            _data_hr_news.hr_news_list_u0 = new hr_news_list_detail_u0[1];
            _data_hr_news.hr_news_list_u0[0] = _hr_news_list_detail_u0;
            

            //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_hr_news));
            //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_hr_news));
            _data_hr_news = callServiceNews(_urlHrSetHrNews, _data_hr_news);
            //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_hr_news));

            if (_data_hr_news.return_code == 1)
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ข้อมูลซ้ำ กรุณาแก้ไขใหม่อีกครั้ง!!!');", true);
                FvListNews.Visible = true;
            }
            else
            {

                ViewState["fileupload"] = _data_hr_news.hr_news_list_u0[0].list_idx;
                //litdebug.Text = ViewState["fileupload"].ToString();
                string getPathfile = ConfigurationManager.AppSettings["path_flie_hr_news"];
                string list_idx = ViewState["fileupload"].ToString();//ddlPlace_insert.SelectedItem + txt_roomname_en.Text +ViewState["vs_createRoom_DetailFile"].ToString();
                string fileName_upload = list_idx;
                string filePath_upload = Server.MapPath(getPathfile + fileName_upload);

                //litdebug.Text = filePath_upload;

                if (checkDelete.Checked)
                {
                    File.Delete(Server.MapPath(getPathfile + list_idx) + "\\" + fileuploadforEdit.Text);
                }

                else if (!Directory.Exists(filePath_upload))
                    {
                        Directory.CreateDirectory(filePath_upload);
                    }
                        else
                        {
                            //litdebug.Text = "delete";
                            File.Delete(Server.MapPath(getPathfile + list_idx) + "\\" + fileName_upload + extension);
                        }

                File_Upload.SaveAs(Server.MapPath(getPathfile + list_idx) + "\\" + fileName_upload + extension);

                //Response.ExpiresAbsolute = DateTime.Now.AddDays(-1d);
                //Response.Expires = -1500;
                //Response.CacheControl = "no-cache";
                //IDictionaryEnumerator allCaches = HttpRuntime.Cache.GetEnumerator();

                //while (allCaches.MoveNext())
                //{
                //    Cache.Remove(allCaches.Key.ToString());
                //}
                //ClearChromCache();
                //ClearApplicationCache();
                lbCreateListNews.Visible = true;
                FvListNews.Visible = false;
                ViewState["forSearch"] = null;
                showListNews();
                gvListNews.Visible = true;
                setActiveTab("tab3");
                //_functionTool.setFvData(FvListNews, FormViewMode.Insert, null);
                //_functionTool.setGvData(gvListNews,null);
                _functionTool.setFvData(searchListNews, FormViewMode.Insert, null);

                DropDownList DropDowntypeNews = (DropDownList)searchListNews.FindControl("DropDowntypeNews_s");
                selectTypeNews(DropDowntypeNews, 0);

                DropDownList DropDownTitleNews = (DropDownList)searchListNews.FindControl("DropDownTitleNews_s");
                DropDownTitleNews.Items.Insert(0, new ListItem("---เลือกหัวข้อข่าวสาร---", ""));
                DropDownTitleNews.SelectedValue = "";


                searchListNews.Visible = true;
                //litdebug.Text = "AAAAA";
            }
        }
        else if (!File_Upload.HasFile && id != 0)//&& id != 0
        {
            //string extension = Path.GetExtension(File_Upload.FileName);
            string extenforNonEditImg = Path.GetExtension(fileuploadforEdit.Text.Trim());

            ViewState["fileupload"] = id;
            //litdebug.Text = ViewState["fileupload"].ToString();
            string getPathfile = ConfigurationManager.AppSettings["path_flie_hr_news"];
            string list_idx = ViewState["fileupload"].ToString();//ddlPlace_insert.SelectedItem + txt_roomname_en.Text +ViewState["vs_createRoom_DetailFile"].ToString();
            string fileName_upload = list_idx;
            string filePath_upload = Server.MapPath(getPathfile + fileName_upload);

            if (checkDelete.Checked)
            {
                File.Delete(Server.MapPath(getPathfile + list_idx) + "\\" + fileuploadforEdit.Text);
                extenforNonEditImg = "";
            }


            _data_hr_news.news_mode = 3;
            _hr_news_list_detail_u0.list_idx = id;
            _hr_news_list_detail_u0.list_name = text_name.Text.Trim();
            //_hr_news_list_detail_u0.list_desc = HttpUtility.HtmlEncode(text_desc.Text.Trim());
            _hr_news_list_detail_u0.list_desc = text_desc.Text.Trim();
            _hr_news_list_detail_u0.list_file = extenforNonEditImg;
            _hr_news_list_detail_u0.list_status = _functionTool.convertToInt(dropD_status.SelectedValue);
            _hr_news_list_detail_u0.list_timeout_start = text_time_start.Text.Trim();
            _hr_news_list_detail_u0.list_timeout_end = text_time_end.Text.Trim(); ;
            _hr_news_list_detail_u0.type_idx = _functionTool.convertToInt(dropD_typeNews.SelectedValue);
            _hr_news_list_detail_u0.title_idx = _functionTool.convertToInt(dropD_titleNews.SelectedValue);
            _hr_news_list_detail_u0.cemp_idx = _emp_idx;

            _data_hr_news.hr_news_list_u0 = new hr_news_list_detail_u0[1];
            _data_hr_news.hr_news_list_u0[0] = _hr_news_list_detail_u0;
            ViewState["fileupload"] = null;

            //litdebug.Text = "eiei";
            //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_hr_news));
            //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_hr_news));
            _data_hr_news = callServiceNews(_urlHrSetHrNews, _data_hr_news);

            if (_data_hr_news.return_code == 1)
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ข้อมูลซ้ำ กรุณาแก้ไขใหม่อีกครั้ง!!!');", true);
                FvListNews.Visible = true;
            }
            else
            {
                //Response.ExpiresAbsolute = DateTime.Now.AddDays(-1d);
                //Response.Expires = -1500;
                //Response.CacheControl = "no-cache";
                //IDictionaryEnumerator allCaches = HttpRuntime.Cache.GetEnumerator();

                //while (allCaches.MoveNext())
                //{
                //    Cache.Remove(allCaches.Key.ToString());
                //}
                //ClearChromCache();
                //ClearApplicationCache();

                lbCreateListNews.Visible = true;
                FvListNews.Visible = false;
                ViewState["forSearch"] = null;
                showListNews();
                gvListNews.Visible = true;

                setActiveTab("tab3");
                searchListNews.Visible = true;

                //litdebug.Text = "BBBBB";
            }

               
        }
        else
        {

            _data_hr_news.news_mode = 3;
            _hr_news_list_detail_u0.list_idx = id;
            _hr_news_list_detail_u0.list_name = text_name.Text.Trim();
            //_hr_news_list_detail_u0.list_desc = HttpUtility.HtmlEncode(text_desc.Text.Trim());
            _hr_news_list_detail_u0.list_desc = text_desc.Text.Trim();
            _hr_news_list_detail_u0.list_file = "";
            _hr_news_list_detail_u0.list_status = _functionTool.convertToInt(dropD_status.SelectedValue);
            _hr_news_list_detail_u0.list_timeout_start = text_time_start.Text.Trim();
            _hr_news_list_detail_u0.list_timeout_end = text_time_end.Text.Trim();
            _hr_news_list_detail_u0.type_idx = _functionTool.convertToInt(dropD_typeNews.SelectedValue);
            _hr_news_list_detail_u0.title_idx = _functionTool.convertToInt(dropD_titleNews.SelectedValue);
            _hr_news_list_detail_u0.cemp_idx = _emp_idx;

            _data_hr_news.hr_news_list_u0 = new hr_news_list_detail_u0[1];
            _data_hr_news.hr_news_list_u0[0] = _hr_news_list_detail_u0;
            ViewState["fileupload"] = null;

            //litdebug.Text = "CCCC";
            //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_hr_news));
            //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_hr_news));
            _data_hr_news = callServiceNews(_urlHrSetHrNews, _data_hr_news);

            //Response.ExpiresAbsolute = DateTime.Now.AddDays(-1d);
            //Response.Expires = -1500;
            //Response.CacheControl = "no-cache";
            //IDictionaryEnumerator allCaches = HttpRuntime.Cache.GetEnumerator();

            //while (allCaches.MoveNext())
            //{
            //    Cache.Remove(allCaches.Key.ToString());
            //}

            //ClearChromCache();
            //ClearApplicationCache();
            lbCreateListNews.Visible = true;
            FvListNews.Visible = false;
            ViewState["forSearch"] = null;
            showListNews();
            gvListNews.Visible = true;
            setActiveTab("tab3");
            searchListNews.Visible = true;
            
        }



    }

    protected void InsertSlideNews(int id)
    {
        FileUpload File_Upload = (FileUpload)FvSlideNews.FindControl("File_Upload");
        TextBox text_time_start = (TextBox)FvSlideNews.FindControl("txt_timestart_create");
        TextBox text_time_end = (TextBox)FvSlideNews.FindControl("txt_timeend_create");
        DropDownList dropD_status = (DropDownList)FvSlideNews.FindControl("ddStatus");
        CheckBox checkDelete = (CheckBox)FvSlideNews.FindControl("checkBoxDelete");
        TextBox tbFile_Upload = (TextBox)FvSlideNews.FindControl("tbFile_Upload");
        DropDownList DropDownListshow = (DropDownList)FvSlideNews.FindControl("DropDownListshow");
        
        //ClearChromCache();

        data_hr_news _data_hr_news = new data_hr_news();
        hr_news_slide_detail_u0 _hr_news_slide_detail_u0 = new hr_news_slide_detail_u0();
        ViewState["fileupload"] = null;
       
        if (File_Upload.HasFile)
        {

            string extension = Path.GetExtension(File_Upload.FileName);

            _data_hr_news.news_mode = 5;
            _hr_news_slide_detail_u0.slide_idx = id;
            _hr_news_slide_detail_u0.slide_file = extension.Trim();
            _hr_news_slide_detail_u0.slide_status = _functionTool.convertToInt(dropD_status.SelectedValue);
            _hr_news_slide_detail_u0.slide_show = _functionTool.convertToInt(DropDownListshow.SelectedValue);
            _hr_news_slide_detail_u0.slide_timeout_start = text_time_start.Text.Trim();
            _hr_news_slide_detail_u0.slide_timeout_end = text_time_end.Text.Trim();
            _hr_news_slide_detail_u0.cemp_idx = _emp_idx;
            
            _data_hr_news.hr_news_slide_u0 = new hr_news_slide_detail_u0[1];
            _data_hr_news.hr_news_slide_u0[0] = _hr_news_slide_detail_u0;


            //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_hr_news));
            //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_hr_news));
            _data_hr_news = callServiceNews(_urlHrSetHrNews, _data_hr_news);
            //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_hr_news));

            if (_data_hr_news.return_code == 1)
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ข้อมูลรูปหลักมีการใช้งานอยู่ กรุณาแก้การแสดงผลหรือสถานะใหม่อีกครั้ง!!!');", true);
                FvSlideNews.Visible = true;
            }
            else
            {


                ViewState["fileupload"] = _data_hr_news.hr_news_slide_u0[0].slide_idx;
                //litdebug.Text = ViewState["fileupload"].ToString();
                string getPathfile = ConfigurationManager.AppSettings["path_flie_hr_news_slidenews"];
                string slide_idx = ViewState["fileupload"].ToString();//ddlPlace_insert.SelectedItem + txt_roomname_en.Text +ViewState["vs_createRoom_DetailFile"].ToString();
                string fileName_upload = slide_idx;
                string filePath_upload = Server.MapPath(getPathfile + fileName_upload);

                //litdebug.Text = filePath_upload;

                //if (checkDelete.Checked && id != 0)
                //{
                //    File.Delete(Server.MapPath(getPathfile + slide_idx) + "\\" + fileName_upload + extension);
                //}

                //else 
                if (!Directory.Exists(filePath_upload))
                {
                    Directory.CreateDirectory(filePath_upload);
                }
                //else
                //{
                //    //litdebug.Text = "delete";
                //    File.Delete(Server.MapPath(getPathfile + slide_idx) + "\\" + fileName_upload + extension);
                //}

                if (id != 0)
                {
                    File.Delete(Server.MapPath(getPathfile + slide_idx) + "\\" + tbFile_Upload.Text);
                }

                File_Upload.SaveAs(Server.MapPath(getPathfile + slide_idx) + "\\" + fileName_upload + extension);


                //ClearChromCache();
                //ClearApplicationCache();
                ViewState["forSearch"] = null;
                setActiveTab("tab4");
            }
        }
        else if (!File_Upload.HasFile && id != 0)
        {

            string extension = Path.GetExtension(tbFile_Upload.Text.Trim());
            _data_hr_news.news_mode = 5;
            _hr_news_slide_detail_u0.slide_idx = id;
            _hr_news_slide_detail_u0.slide_file = extension.Trim();
            _hr_news_slide_detail_u0.slide_status = _functionTool.convertToInt(dropD_status.SelectedValue);
            _hr_news_slide_detail_u0.slide_show = _functionTool.convertToInt(DropDownListshow.SelectedValue);
            _hr_news_slide_detail_u0.slide_timeout_start = text_time_start.Text.Trim();
            _hr_news_slide_detail_u0.slide_timeout_end = text_time_end.Text.Trim();
            _hr_news_slide_detail_u0.cemp_idx = _emp_idx;

            _data_hr_news.hr_news_slide_u0 = new hr_news_slide_detail_u0[1];
            _data_hr_news.hr_news_slide_u0[0] = _hr_news_slide_detail_u0;


            //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_hr_news));
            //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_hr_news));
            _data_hr_news = callServiceNews(_urlHrSetHrNews, _data_hr_news);

            if (_data_hr_news.return_code == 1)
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ข้อมูลรูปหลักมีการใช้งานอยู่ กรุณาแก้การแสดงผลหรือสถานะใหม่อีกครั้ง!!!');", true);
                FvSlideNews.Visible = true;
            }
            else
            {
                //ClearChromCache();
                //ClearApplicationCache();
                ViewState["forSearch"] = null;
                setActiveTab("tab4");
            }
        }
        //ClearChromCache();
        //ClearApplicationCache();
    }


    protected void cmdDeleteTypeNews(int id)
    {
        //TextBox text_name = (TextBox)FvTypeNews.FindControl("tb_typenews");
        //DropDownList dropD_status = (DropDownList)FvTypeNews.FindControl("ddStatus");

        data_hr_news _data_hr_news = new data_hr_news();
        hr_news_type_detail_m0 _hr_news_type_detail_m0 = new hr_news_type_detail_m0();
        _data_hr_news.news_mode = 1;
        _hr_news_type_detail_m0.type_idx = id;
        //_hr_news_type_detail_m0.type_name = text_name.Text.Trim();
        _hr_news_type_detail_m0.type_status = 9;
        _hr_news_type_detail_m0.cemp_idx = _emp_idx;

        _data_hr_news.hr_news_type_m0 = new hr_news_type_detail_m0[1];
        _data_hr_news.hr_news_type_m0[0] = _hr_news_type_detail_m0;

        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_hr_news));
        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_hr_news));
        _data_hr_news = callServiceNews(_urlHrSetHrNews, _data_hr_news);
        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_hr_news));
        if (_data_hr_news.return_code == 1)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่สามารถลบข้อมูลนี้ได้!!!');", true);
            lbCreateTypenews.Visible = true;
       
            showTypeNews();
            gvTypeNews.Visible = true;
            searchTypeNews.Visible = true;
        }
        else
        {
            lbCreateTypenews.Visible = true;
            FvTypeNews.Visible = false;
            showTypeNews();
            gvTypeNews.Visible = true;
            _functionTool.setFvData(searchTypeNews, FormViewMode.Insert, null);
            searchTypeNews.Visible = true;
        }
        
    }

    protected void cmdDeleteTitleNews(int id)
    {
        //TextBox text_name = (TextBox)FvTypeNews.FindControl("tb_typenews");
        //DropDownList dropD_status = (DropDownList)FvTypeNews.FindControl("ddStatus");

        data_hr_news _data_hr_news = new data_hr_news();
        hr_news_type_detail_m1 _hr_news_type_detail_m1 = new hr_news_type_detail_m1();
        _data_hr_news.news_mode = 2;
        _hr_news_type_detail_m1.title_idx = id;
        //_hr_news_type_detail_m0.type_name = text_name.Text.Trim();
        _hr_news_type_detail_m1.title_status = 9;
        _hr_news_type_detail_m1.cemp_idx = _emp_idx;

        _data_hr_news.hr_news_type_m1 = new hr_news_type_detail_m1[1];
        _data_hr_news.hr_news_type_m1[0] = _hr_news_type_detail_m1;

        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_hr_news));
        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_hr_news));
        _data_hr_news = callServiceNews(_urlHrSetHrNews, _data_hr_news);
        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_hr_news));


        if (_data_hr_news.return_code == 1)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่สามารถลบข้อมูลนี้ได้!!!');", true);
            lbCreateTitleNews.Visible = true;
        
            showTitleNews();
            gvTitleNews.Visible = true;
            searchTitleNews.Visible = true;
        }
        else
        {
            lbCreateTitleNews.Visible = true;
    
            showTitleNews();
            gvTitleNews.Visible = true;
            _functionTool.setFvData(searchTitleNews, FormViewMode.Insert, null);
            searchTitleNews.Visible = true;
        }


    }

    protected void cmdDeleteListNews(int id)
    {
        //TextBox text_name = (TextBox)FvTypeNews.FindControl("tb_typenews");
        //DropDownList dropD_status = (DropDownList)FvTypeNews.FindControl("ddStatus");

        data_hr_news _data_hr_news = new data_hr_news();
        hr_news_list_detail_u0 _hr_news_list_detail_u0 = new hr_news_list_detail_u0();
        _data_hr_news.news_mode = 3;
        _hr_news_list_detail_u0.list_idx = id;
        //_hr_news_type_detail_m0.type_name = text_name.Text.Trim();
        _hr_news_list_detail_u0.list_status = 9;
        _hr_news_list_detail_u0.cemp_idx = _emp_idx;

        _data_hr_news.hr_news_list_u0 = new hr_news_list_detail_u0[1];
        _data_hr_news.hr_news_list_u0[0] = _hr_news_list_detail_u0;

        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_hr_news));
        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_hr_news));
        _data_hr_news = callServiceNews(_urlHrSetHrNews, _data_hr_news);
        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_hr_news));

        lbCreateListNews.Visible = true;
        FvListNews.Visible = false;
        showListNews();
        gvListNews.Visible = true;
        _functionTool.setFvData(searchListNews, FormViewMode.Insert, null);

        DropDownList DropDowntypeNews = (DropDownList)searchListNews.FindControl("DropDowntypeNews_s");
        selectTypeNews(DropDowntypeNews, 0);

        DropDownList DropDownTitleNews = (DropDownList)searchListNews.FindControl("DropDownTitleNews_s");
        DropDownTitleNews.Items.Insert(0, new ListItem("---เลือกหัวข้อข่าวสาร---", ""));
        DropDownTitleNews.SelectedValue = "";


        searchListNews.Visible = true;

    }
    

    protected void cmdDeleteSlideNews(int id)
    {
        //TextBox text_name = (TextBox)FvTypeNews.FindControl("tb_typenews");
        //DropDownList dropD_status = (DropDownList)FvTypeNews.FindControl("ddStatus");

        data_hr_news _data_hr_news = new data_hr_news();
        hr_news_slide_detail_u0 _hr_news_slide_detail_u0 = new hr_news_slide_detail_u0();
        _data_hr_news.news_mode = 5;
        _hr_news_slide_detail_u0.slide_idx = id;
        //_hr_news_type_detail_m0.type_name = text_name.Text.Trim();
        _hr_news_slide_detail_u0.slide_status = 9;
        _hr_news_slide_detail_u0.cemp_idx = _emp_idx;

        _data_hr_news.hr_news_slide_u0 = new hr_news_slide_detail_u0[1];
        _data_hr_news.hr_news_slide_u0[0] = _hr_news_slide_detail_u0;

        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_hr_news));
        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_hr_news));
        _data_hr_news = callServiceNews(_urlHrSetHrNews, _data_hr_news);
        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_hr_news));

        lbCreateSlideNews.Visible = true;
        FvSlideNews.Visible = false;
        showSlideNews();
        gvSlideNews.Visible = true;
        _functionTool.setFvData(searchSlideNews, FormViewMode.Insert, null);
        searchSlideNews.Visible = true;



    }

    protected void showTypeNews()
    {
        data_hr_news _data_hr_news = new data_hr_news();
        hr_news_type_detail_m0 _hr_news_type_detail_m0 = new hr_news_type_detail_m0();

        if (ViewState["forSearch"] == null)
        {
            _hr_news_type_detail_m0.type_idx = 0;
            _data_hr_news.news_mode = 1;
            _data_hr_news.hr_news_type_m0 = new hr_news_type_detail_m0[1];
            _data_hr_news.hr_news_type_m0[0] = _hr_news_type_detail_m0;
            //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_hr_news));
            _data_hr_news = callServiceNews(_urlHrGetHrNews, _data_hr_news);
            _functionTool.setGvData(gvTypeNews, _data_hr_news.hr_news_type_m0);
        }
        else
        {
            _functionTool.setGvData(gvTypeNews, ((data_hr_news)ViewState["forSearch"]).hr_news_type_m0);
        }
    }
   

    protected void showTitleNews()
    {
        data_hr_news _data_hr_news = new data_hr_news();
        hr_news_type_detail_m1 _hr_news_type_detail_m1 = new hr_news_type_detail_m1();

        if (ViewState["forSearch"] == null)
        {
            _hr_news_type_detail_m1.title_idx = 0;
            _data_hr_news.news_mode = 2;
            _data_hr_news.hr_news_type_m1 = new hr_news_type_detail_m1[1];
            _data_hr_news.hr_news_type_m1[0] = _hr_news_type_detail_m1;
            //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_hr_news));
            _data_hr_news = callServiceNews(_urlHrGetHrNews, _data_hr_news);

            _functionTool.setGvData(gvTitleNews, _data_hr_news.hr_news_type_m1);
        }
        else
        {
            _functionTool.setGvData(gvTitleNews, ((data_hr_news)ViewState["forSearch"]).hr_news_type_m1);
        }
    }

    protected void showListNews()
    {
        data_hr_news _data_hr_news = new data_hr_news();
        hr_news_list_detail_u0 _hr_news_list_detail_u0 = new hr_news_list_detail_u0();
        

        if (ViewState["forSearch"] == null)
        {
            _hr_news_list_detail_u0.list_idx = 0;
            _data_hr_news.news_mode = 3;
            _data_hr_news.hr_news_list_u0 = new hr_news_list_detail_u0[1];
            _data_hr_news.hr_news_list_u0[0] = _hr_news_list_detail_u0;
            //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_hr_news));
            _data_hr_news = callServiceNews(_urlHrGetHrNews, _data_hr_news);
            //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_hr_news));
            _functionTool.setGvData(gvListNews, _data_hr_news.hr_news_list_u0);
        }
        else
        {
            
            _functionTool.setGvData(gvListNews, ((data_hr_news)ViewState["forSearch"]).hr_news_list_u0);
        }
    }

    protected void showSlideNews()
    {
        data_hr_news _data_hr_news = new data_hr_news();
        hr_news_slide_detail_u0 _hr_news_slide_detail_u0 = new hr_news_slide_detail_u0();

        if (ViewState["forSearch"] == null)
        {
            _hr_news_slide_detail_u0.slide_idx = 0;
            _data_hr_news.news_mode = 5;
            _data_hr_news.hr_news_slide_u0 = new hr_news_slide_detail_u0[1];
            _data_hr_news.hr_news_slide_u0[0] = _hr_news_slide_detail_u0;
            //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_hr_news));
            _data_hr_news = callServiceNews(_urlHrGetHrNews, _data_hr_news);
            _functionTool.setGvData(gvSlideNews, _data_hr_news.hr_news_slide_u0);
        }
        else
        {
            _functionTool.setGvData(gvSlideNews, ((data_hr_news)ViewState["forSearch"]).hr_news_slide_u0);
        }
    }
    


    protected void selectTypeNews(DropDownList ddlName, int _type_idx)
    {
        data_hr_news _data_hr_news = new data_hr_news();
        hr_news_type_detail_m0 _hr_news_type_detail_m0 = new hr_news_type_detail_m0();

        _hr_news_type_detail_m0.type_idx = _type_idx;
        _data_hr_news.news_mode = 1;
        _data_hr_news.hr_news_type_m0 = new hr_news_type_detail_m0[1];
        _data_hr_news.hr_news_type_m0[0] = _hr_news_type_detail_m0;
        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_hr_news));
        _data_hr_news = callServiceNews(_urlHrGetHrNews, _data_hr_news);
        _functionTool.setDdlData(ddlName, _data_hr_news.hr_news_type_m0, "type_name", "type_idx");
        ddlName.Items.Insert(0, new ListItem("---เลือกประเภทข่าวสาร---", ""));
        ddlName.SelectedValue = _type_idx.ToString();
    }

    protected void rpt_item(Object Sender, RepeaterItemEventArgs e)
    {
        LinkButton linkButton = (LinkButton)e.Item.FindControl("btnmenuTypeNews");
        HtmlControl control = (HtmlControl)e.Item.FindControl("collapse4");//find <div>
        LinkButton LinkButtonhead = (LinkButton)e.Item.FindControl("btnmenuTypeNews");
        //Panel DivCtl1 = (Panel)e.Item.FindControl("divSubmitted");
         //Label labeltail = (Label)e.Item.FindControl("collapse4");

        //Control myControl1 = (Control)e.Item.FindControl("collapse1");

        char[] delimiterChars = { ',' };
        string[] words = linkButton.CommandArgument.ToString().Split(delimiterChars);
        string id = words[0];
        string name = words[1];

        if (e.Item.ItemType == e.Item.ItemType)
        {
            //litdebug.Text += id;
            //selecttitleNews(_functionTool.convertToInt(4));
            Repeater RepeaterView = (Repeater)e.Item.FindControl("RepeaterView");
            // Repeater Repeater2 = (Repeater)e.Item.FindControl("Repeater12");
            

            selecttitleNews(_functionTool.convertToInt(id) , RepeaterView);

            LinkButtonhead.Attributes.Add("href", ".collapse" + id);
            control.Attributes.Add("class", "panel-collapse collapse collapse" + id);
            
            //selecttitleNews(_functionTool.convertToInt(id), Repeater12);
        }
    }

    protected void check_item(Object Sender, RepeaterItemEventArgs e)
    {
        data_hr_news _data_hr_news = new data_hr_news();
        hr_news_list_detail_u0 _hr_news_list_detail_u0 = new hr_news_list_detail_u0();

     
        LinkButton linkButton = (LinkButton)e.Item.FindControl("btnmenulistNews");
        HtmlControl control = (HtmlControl)e.Item.FindControl("collapse4");//find <div>
        LinkButton LinkButtonhead = (LinkButton)e.Item.FindControl("btnmenulistNews");

        HtmlGenericControl collapseDesc = (HtmlGenericControl)e.Item.FindControl("collapseDesc");
        HtmlGenericControl collapsefile = (HtmlGenericControl)e.Item.FindControl("collapsefile");

        char[] delimiterChars = { ',' };
        string[] words = linkButton.CommandArgument.ToString().Split(delimiterChars);
        string id = words[0];
        string name = words[1];
        if (e.Item.ItemType == e.Item.ItemType && id != "0")
        {

            LinkButtonhead.Attributes.Add("href", ".collapse" + id);
            control.Attributes.Add("class", "panel-collapse collapse collapse" + id);


            _hr_news_list_detail_u0.list_idx = _functionTool.convertToInt(id);

            _data_hr_news.news_mode = 4;

            _data_hr_news.hr_news_list_u0 = new hr_news_list_detail_u0[1];
            _data_hr_news.hr_news_list_u0[0] = _hr_news_list_detail_u0;
            _data_hr_news = callServiceNews(_urlHrGetHrNews, _data_hr_news);
            collapseDesc.InnerHtml = HttpUtility.HtmlDecode(HttpUtility.HtmlDecode(_data_hr_news.hr_news_list_u0[0].list_desc));


            //viewdesc.Attributes.Add("href", ".collapse" + list_idx.Text);
            //control.Attributes.Add("class", "panel-collapse collapse collapse" + list_idx.Text);

            string file = FormatImageUrl(_data_hr_news.hr_news_list_u0[0].list_file, "list");
            string extension = Path.GetExtension(_data_hr_news.hr_news_list_u0[0].list_file);
            int typefile = _data_hr_news.hr_news_list_u0[0].title_type_file;
            //litdebug.Text = extension + "xx";

            //LiteralName.Text = _data_hr_news.hr_news_list_u0[0].list_name;
            //LiteralDesc.Text = HttpUtility.HtmlDecode(_data_hr_news.hr_news_list_u0[0].list_desc);

            if (typefile == 1 && extension != "")
            {

                collapsefile.InnerHtml = "<img id=\"Image3\" src=\"" + ResolveUrl(file) + "\" class=\"img-responsive\"/>";

            }
            else if (typefile == 0 && extension != "")
            {
                collapsefile.InnerHtml = "<embed id=\"Image4\" src=\"" + ResolveUrl(file) + "\" type=\"application/pdf\" width=\"100%\" height=\"800px\"/>";

                //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(test2.InnerHtml));
            }
            else
            {
                collapsefile.InnerHtml = "";
            }

            control.Visible = true;
        }
        else
        {
            control.Visible = false;
        }


    }


    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlName = (DropDownList)sender;
        DropDownList dropD_Type = (DropDownList)FvListNews.FindControl("DropDowntypeNews");
        DropDownList dropD_Title = (DropDownList)FvListNews.FindControl("DropDownTitleNews");
        TextBox tinymce = (TextBox)FvListNews.FindControl("tb_listnewsDesc");
        ViewState["checkforaccpet"] = null;
        data_hr_news _data_hr_news = new data_hr_news();
        hr_news_type_detail_m1 _hr_news_type_detail_m1 = new hr_news_type_detail_m1();
        FileUpload File_Upload = (FileUpload)FvListNews.FindControl("File_Upload");
        Label labelFile_Upload = (Label)FvListNews.FindControl("labelFile_Upload");

        DropDownList DropDownListshow = (DropDownList)FvSlideNews.FindControl("DropDownListshow");
        FileUpload File_UploadSlideNews = (FileUpload)FvSlideNews.FindControl("File_Upload");

        DropDownList DropDowntypeNews_s = (DropDownList)searchListNews.FindControl("DropDowntypeNews_s");
        DropDownList DropDownTitleNews_s = (DropDownList)searchListNews.FindControl("DropDownTitleNews_s");

        HtmlGenericControl wordslide = (HtmlGenericControl)FvSlideNews.FindControl("wordslide");
        HtmlGenericControl wordMain = (HtmlGenericControl)FvSlideNews.FindControl("wordMain");

        //HtmlGenericControl checkfileimg = (HtmlGenericControl)FvListNews.FindControl("checkfileimg");
        //HtmlGenericControl checkfilepdf = (HtmlGenericControl)FvListNews.FindControl("checkfilepdf");

        switch (ddlName.ID)
        {
            case "DropDowntypeNews":

                _data_hr_news.news_mode = 2;
                _hr_news_type_detail_m1.type_idx = _functionTool.convertToInt(dropD_Type.SelectedValue);
                _data_hr_news.hr_news_type_m1 = new hr_news_type_detail_m1[1];
                _data_hr_news.hr_news_type_m1[0] = _hr_news_type_detail_m1;
                _data_hr_news = callServiceNews(_urlHrGetHrNews, _data_hr_news);
                _functionTool.setDdlData(dropD_Title, _data_hr_news.hr_news_type_m1, "title_name", "title_idx");
                dropD_Title.Items.Insert(0, new ListItem("---เลือกหัวข้อข่าวสาร---", ""));
                
                //_functionTool.setFvData(FvListNews, FormViewMode.Insert, null);
                tinymce.Text = HttpUtility.HtmlDecode(tinymce.Text);
            
                break;

            case "DropDownTitleNews":
                //check for accpet jpg|png|pdf
                _data_hr_news.news_mode = 2;
                

                _hr_news_type_detail_m1.title_idx = _functionTool.convertToInt(dropD_Title.SelectedValue);
                _data_hr_news.hr_news_type_m1 = new hr_news_type_detail_m1[1];
                _data_hr_news.hr_news_type_m1[0] = _hr_news_type_detail_m1;
                _data_hr_news = callServiceNews(_urlHrGetHrNews, _data_hr_news);
                //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_hr_news));
                ViewState["checkforaccpet"] = _data_hr_news.hr_news_type_m1[0].title_type_file;

                if ((int)ViewState["checkforaccpet"] == 1)
                {
                    File_Upload.Attributes.Add("accept", "jpg|png|jpeg");
                    
                }
                else if ((int) ViewState["checkforaccpet"] == 0)
                {
                    File_Upload.Attributes.Add("accept", "pdf");
                    
                }

                //TextBox tinymce = (TextBox)FvListNews.FindControl("tb_listnewsDesc");
                //_functionTool.setFvData(FvListNews, FormViewMode.Insert, null);
                tinymce.Text = HttpUtility.HtmlDecode(tinymce.Text);
                ViewState["checkforaccpet"] = null;
               
                break;

            case "DropDownListshow":

               
                if(DropDownListshow.SelectedValue == "1")
                {
                    File_UploadSlideNews.Attributes.Add("onchange", "readURL(this);");
                    wordslide.Visible = true;
                    wordMain.Visible = false;
                }
                else if (DropDownListshow.SelectedValue == "2")
                {
                    File_UploadSlideNews.Attributes.Add("onchange", "");
                    wordMain.Visible = true;
                    wordslide.Visible = false;
                }
                break;

            case "DropDowntypeNews_s":


                _data_hr_news.news_mode = 2;
                _hr_news_type_detail_m1.type_idx = _functionTool.convertToInt(DropDowntypeNews_s.SelectedValue);
                _data_hr_news.hr_news_type_m1 = new hr_news_type_detail_m1[1];
                _data_hr_news.hr_news_type_m1[0] = _hr_news_type_detail_m1;
                _data_hr_news = callServiceNews(_urlHrGetHrNews, _data_hr_news);
                _functionTool.setDdlData(DropDownTitleNews_s, _data_hr_news.hr_news_type_m1, "title_name", "title_idx");
                DropDownTitleNews_s.Items.Insert(0, new ListItem("---เลือกหัวข้อข่าวสาร---", ""));

                break;
        }
       
        }

    protected void selectDdl(DropDownList ddlName, int newsMode, int _type_idx)
    {
        data_hr_news _data_hr_news = new data_hr_news();
        hr_news_type_detail_m1 _hr_news_type_detail_m1 = new hr_news_type_detail_m1();


        _data_hr_news.news_mode = newsMode;
        _hr_news_type_detail_m1.type_idx = _type_idx;


        _data_hr_news.hr_news_type_m1 = new hr_news_type_detail_m1[1];
        _data_hr_news.hr_news_type_m1[0] = _hr_news_type_detail_m1;
        
        // litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
        _data_hr_news = callServiceNews(_urlHrGetHrNews, _data_hr_news);
        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));

        switch (newsMode)
        {
            case 2:

                _functionTool.setDdlData(ddlName, _data_hr_news.hr_news_type_m1, "title_name", "title_idx");
                //ddlName.Items.Insert(0, new ListItem("---เลือกองค์กร---", ""));
                    

                break;
        }
        }


    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        data_hr_news _data_hr_news = new data_hr_news();
        hr_news_list_detail_u0 _hr_news_list_detail_u0 = new hr_news_list_detail_u0();

        hr_news_slide_detail_u0 _hr_news_slide_detail_u0 = new hr_news_slide_detail_u0();




        //Image image = (Image)
        switch (GvName.ID)
        {
            case "gvListNews":

                
                GridView gvListNewsDesc = (GridView)e.Row.FindControl("gvListNewsDesc");

                LinkButton viewdesc = (LinkButton)e.Row.FindControl("viewdesc");

                //HtmlControl viewdesc = (HtmlControl)e.Row.FindControl("viewdesc");
                HtmlControl control = (HtmlControl)e.Row.FindControl("collapsegvListNewsDesc");

                HtmlGenericControl collapseDesc = (HtmlGenericControl)e.Row.FindControl("collapseDesc");
                HtmlGenericControl collapsefile = (HtmlGenericControl)e.Row.FindControl("collapsefile");
                
                Label gvtypefile = (Label)e.Row.FindControl("gvtitle_type_file");

                Label list_idx = (Label)e.Row.FindControl("gvlist_idx");

               

                //Literal lit = (Literal)e.Row.FindControl("test1");

                if (e.Row.RowType == DataControlRowType.DataRow)
                    {

                        _hr_news_list_detail_u0.list_idx = _functionTool.convertToInt(list_idx.Text);
                        _data_hr_news.news_mode = 3;
                        _data_hr_news.hr_news_list_u0 = new hr_news_list_detail_u0[1];
                        _data_hr_news.hr_news_list_u0[0] = _hr_news_list_detail_u0;
                        _data_hr_news = callServiceNews(_urlHrGetHrNews, _data_hr_news);
                    //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_hr_news));
                    //_functionTool.setGvData(gvListNewsDesc, _data_hr_news.hr_news_list_u0);


                    // lit.Text = HttpUtility.HtmlDecode(_data_hr_news.hr_news_list_u0[0].list_desc);

                    collapseDesc.InnerHtml = HttpUtility.HtmlDecode(HttpUtility.HtmlDecode(_data_hr_news.hr_news_list_u0[0].list_desc));


                    //viewdesc.Attributes.Add("href", ".collapse" + list_idx.Text);
                    //control.Attributes.Add("class", "panel-collapse collapse collapse" + list_idx.Text);

                    string file = FormatImageUrl(_data_hr_news.hr_news_list_u0[0].list_file, "list");
                        string extension = Path.GetExtension(_data_hr_news.hr_news_list_u0[0].list_file);
                        int typefile = _data_hr_news.hr_news_list_u0[0].title_type_file;
                        //litdebug.Text = extension + "xx";

                        //LiteralName.Text = _data_hr_news.hr_news_list_u0[0].list_name;
                        //LiteralDesc.Text = HttpUtility.HtmlDecode(_data_hr_news.hr_news_list_u0[0].list_desc);

                        if (typefile == 1 && extension != "")
                        {

                        collapsefile.InnerHtml = "<img id=\"Image3\" src=\"" + ResolveUrl(file) + "\" class=\"img-responsive\"/>";

                        }
                        else if (typefile == 0 && extension != "")
                        {
                        collapsefile.InnerHtml = "<embed id=\"Image4\" src=\"" + ResolveUrl(file) + "\" type=\"application/pdf\" width=\"100%\" height=\"800px\"/>";

                            //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(test2.InnerHtml));
                        }
                        else
                        {
                        collapsefile.InnerHtml = "";
                        }

                        //If Salary is less than 10000 than set the row Background Color to Cyan  
                        //if (gvtypefile.Text == "รูปแบบไฟล์รูปภาพ")
                        //{
                        //    e.Row.FindControl("image").Visible = true;
                        //    e.Row.FindControl("LinkButtonImage").Visible = true;
                        //    e.Row.FindControl("file_pdf").Visible = false;
                        //    e.Row.FindControl("LinkButtonDoc").Visible = false;

                        //}
                        //else
                        //{
                        //    e.Row.FindControl("file_pdf").Visible = true;
                        //    e.Row.FindControl("LinkButtonDoc").Visible = true;
                        //    e.Row.FindControl("image").Visible = false;
                        //    e.Row.FindControl("LinkButtonImage").Visible = false;
                        //}
                    }
                
                
                break;
            case "gvSlideNews":
                LinkButton lbView = (LinkButton)e.Row.FindControl("lbView");
                Label slide_idx = (Label)e.Row.FindControl("gvslide_idx");

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    _hr_news_slide_detail_u0.slide_idx = _functionTool.convertToInt(slide_idx.Text);
                    _data_hr_news.news_mode = 5;
                    _data_hr_news.hr_news_slide_u0 = new hr_news_slide_detail_u0[1];
                    _data_hr_news.hr_news_slide_u0[0] = _hr_news_slide_detail_u0;
                    _data_hr_news = callServiceNews(_urlHrGetHrNews, _data_hr_news);

                    string url = FormatImageUrl(_data_hr_news.hr_news_slide_u0[0].slide_file, "slide");

                    lbView.Attributes.Add("OnClick", "window.open('" + ResolveUrl(url) + "'); return false;");
                }
                break;
        }
    }

    protected void gvOnRowCommand(object sender, GridViewCommandEventArgs e)
    {
        //var GvName = (GridView)sender;

        //GridView gvListNewsDesc = (GridView)gvListNews.FindControl("gvListNewsDesc");
        ////char[] delimiterChars = { ',' };
        //HtmlControl control = (HtmlControl)gvListNews.FindControl("collapsegvListNewsDesc");
        ////string[] words = e.CommandArgument.ToString().Split(delimiterChars);


        ////string id = words[0];
        ////string name = words[1];
        ////Determine the RowIndex of the Row whose Button was clicked.
        ////int rowIndex = _functionTool.convertToInt(e.CommandArgument.ToString());
        //string id = e.CommandArgument.ToString();
        //data_hr_news _data_hr_news = new data_hr_news();
        //hr_news_list_detail_u0 _hr_news_list_detail_u0 = new hr_news_list_detail_u0();

      
        //    _hr_news_list_detail_u0.list_idx = _functionTool.convertToInt(id);
        //    _data_hr_news.news_mode = 3;
        //    _data_hr_news.hr_news_list_u0 = new hr_news_list_detail_u0[1];
        //    _data_hr_news.hr_news_list_u0[0] = _hr_news_list_detail_u0;
        //    litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_hr_news));
        //    //_data_hr_news = callServiceNews(_urlHrGetHrNews, _data_hr_news);
        //    //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_hr_news));
        //   // _functionTool.setGvData(gvListNewsDesc, _data_hr_news.hr_news_list_u0);


        }


    protected void checkedPermision()
    {
        data_hr_news _data_hr_news = new data_hr_news();
        hr_news_per_detail _hr_news_per_detail = new hr_news_per_detail();
        _data_hr_news.news_mode = 7;
        _hr_news_per_detail.per_rpos_id = _rpos_idx.ToString();

        _data_hr_news.hr_news_per_list = new hr_news_per_detail[1];
        _data_hr_news.hr_news_per_list[0] = _hr_news_per_detail;
        //litdebug.Text += HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_hr_news));
        _data_hr_news = callServiceNews(_urlHrGetHrNews, _data_hr_news);
        //litdebug.Text += HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_hr_news));

        if (_data_hr_news.hr_news_per_list == null)
        {
            //litdebug.Text += "ปิด";
            //divBtnHead.Visible = false;
            li1.Visible = false;
            li2.Visible = false;
            li3.Visible = false;
            li4.Visible = false;

        }
        else if (_data_hr_news.hr_news_per_list != null)
        {
            //litdebug.Text += "เปิด";
           //divBtnHead.Visible = true;
            li1.Visible = true;
            li2.Visible = true;
            li3.Visible = true;
            li4.Visible = true;

        }
    }

        protected data_hr_news callServiceNews(string _cmdUrl, data_hr_news _data_hr_news)
    {
        _localJson = _functionTool.convertObjectToJson(_data_hr_news);

        _localJson = _functionTool.callServicePost(_cmdUrl, _localJson);

        _data_hr_news = (data_hr_news)_functionTool.convertJsonToObject(typeof(data_hr_news), _localJson);


        return _data_hr_news;
    }

    protected string convertStatus(int status)
    {
        if (status == 1)
            textstatus = "online";
        else
            textstatus = "offline";
        return textstatus;
    }
    protected string convertShow(int status)
    {
        if (status == 2)
            textstatus = "Main";
        else
            textstatus = "Slide";
        return textstatus;
    }
    protected string convertCss(int status)
    {
        if (status == 1)
            textstatus = "text-success";
        else
            textstatus = "text-danger";
        return textstatus;
    }

    protected string convertFileType(int TypeFile)
    {
        if (TypeFile == 1)
            textTypeFile = "ไฟล์รูปภาพ";
        else if(TypeFile == 0)
            textTypeFile = "ไฟล์เอกสาร";
        return textTypeFile;
    }

    

    protected string HaveFile(string File)
    {
        CheckBox checkDelete = (CheckBox)FvListNews.FindControl("checkBoxDelete");
        string extension = Path.GetExtension(File);
        if (extension == "")
        {
            File = "";
            checkDelete.Visible = false;
        }
        else
        {
            checkDelete.Visible = true;
        }

        return File;

    }

    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _functionTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_functionTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        data_cen_master _data_cen_master = new data_cen_master();
        switch (GvName.ID)
        {
            case "gvTypeNews":
                gvTypeNews.PageIndex = e.NewPageIndex;
                showTypeNews();
                break;

            case "gvTitleNews":
                gvTitleNews.PageIndex = e.NewPageIndex;
                showTitleNews();
                break;

            case "gvListNews":
                gvListNews.PageIndex = e.NewPageIndex;
                showListNews();
                break;

            case "gvSlideNews":
                gvSlideNews.PageIndex = e.NewPageIndex;
                showSlideNews();
                break;

        }
        hlSetTotop.Focus();
    }

    protected void setTrigger()
    {
        linkBtnTrigger(lbtab0);
        linkBtnTrigger(lbtab1);
        linkBtnTrigger(lbtab2);
        linkBtnTrigger(lbtab3);
        linkBtnTrigger(lbtab4);
        linkBtnTrigger(lbtab4);
        //linkBtnTrigger(lbtab4_2);
        linkBtnTrigger(lbCreateListNews);
        linkBtnTrigger(lbCreateTitleNews);
        linkBtnTrigger(lbCreateTypenews);
        linkBtnTrigger(lbCreateSlideNews);


        LinkButton LinkButtonSaveFvSlideNews = (LinkButton)FvSlideNews.FindControl("LinkButtonSave");
        //linkBtnTrigger(LinkButtonSave);
        try { linkBtnTrigger(LinkButtonSaveFvSlideNews); } catch { }

        LinkButton LinkButtonCancelFvSlideNews = (LinkButton)FvSlideNews.FindControl("LinkButtonCancel");
        try { linkBtnTrigger(LinkButtonCancelFvSlideNews); } catch { }

        LinkButton LinkButtonSaveFvListNews = (LinkButton)FvListNews.FindControl("LinkButtonSave");
        //linkBtnTrigger(LinkButtonSave);
        try { linkBtnTrigger(LinkButtonSaveFvListNews); } catch { }

        LinkButton LinkButtonCancelFvListNews = (LinkButton)FvListNews.FindControl("LinkButtonCancel");
        try { linkBtnTrigger(LinkButtonCancelFvListNews); } catch { }

        LinkButton LinkButtonSaveFvTypeNews = (LinkButton)FvTypeNews.FindControl("LinkButtonSave");
        //linkBtnTrigger(LinkButtonSave);
        try { linkBtnTrigger(LinkButtonSaveFvTypeNews); } catch { }

        LinkButton LinkButtonCancelFvTypeNews = (LinkButton)FvTypeNews.FindControl("LinkButtonCancel");
        try { linkBtnTrigger(LinkButtonCancelFvTypeNews); } catch { }

        LinkButton LinkButtonSaveFvTitleNews = (LinkButton)FvTitleNews.FindControl("LinkButtonSave");
        //linkBtnTrigger(LinkButtonSave);
        try { linkBtnTrigger(LinkButtonSaveFvTitleNews); } catch { }

        LinkButton LinkButtonCancelFvTitleNews = (LinkButton)FvTitleNews.FindControl("LinkButtonCancel");
        try { linkBtnTrigger(LinkButtonCancelFvTitleNews); } catch { }



        LinkButton LinkButtonSearchListNews = (LinkButton)searchListNews.FindControl("lbsearch");
        try { linkBtnTrigger(LinkButtonSearchListNews); } catch { }

        LinkButton LinkButtonSearchTypeNews = (LinkButton)searchTypeNews.FindControl("lbsearch");
        try { linkBtnTrigger(LinkButtonSearchTypeNews); } catch { }

        LinkButton LinkButtonSearchTitleNews = (LinkButton)searchTitleNews.FindControl("lbsearch");
        try { linkBtnTrigger(LinkButtonSearchTitleNews); } catch { }

        LinkButton LinkButtonSearchListNews_cancel = (LinkButton)searchListNews.FindControl("lbsearch_cancel");
        try { linkBtnTrigger(LinkButtonSearchListNews_cancel); } catch { }

        LinkButton LinkButtonSearchTypeNews_cancel = (LinkButton)searchTypeNews.FindControl("lbsearch_cancel");
        try { linkBtnTrigger(LinkButtonSearchTypeNews_cancel); } catch { }

        LinkButton LinkButtonSearchTitleNews_cancel = (LinkButton)searchTitleNews.FindControl("lbsearch_cancel");
        try { linkBtnTrigger(LinkButtonSearchTitleNews_cancel); } catch { }




        DropDownList DropDowntypeNewsFvListNews = (DropDownList)FvListNews.FindControl("DropDowntypeNews");
       
        try { DropDownListTrigger(DropDowntypeNewsFvListNews); } catch { }


        fvTrigger(FvListNews);
        fvTrigger(FvTypeNews);
        fvTrigger(FvTitleNews);
        fvTrigger(FvSlideNews);
        fvTrigger(searchListNews);
        fvTrigger(searchTypeNews);
        fvTrigger(searchTitleNews);


        gvTrigger(gvListNews);
        LinkButton LinkButtonEdit = (LinkButton)gvListNews.FindControl("lbEdit");
        try { linkBtnTrigger(LinkButtonEdit); } catch { }
        HtmlControl viewdesc = (HtmlControl)gvListNews.FindControl("viewdesc");
        try { HtmlControlTrigger(viewdesc); } catch { }
        HtmlControl viewdescminus = (HtmlControl)gvListNews.FindControl("viewdescminus");
        try { HtmlControlTrigger(viewdescminus); } catch { }
        LinkButton LinkButtonDeleteGvListNewsNews = (LinkButton)gvTypeNews.FindControl("lbDelete");
        try { linkBtnTrigger(LinkButtonDeleteGvListNewsNews); } catch { }
  
        gvTrigger(gvTypeNews);
        LinkButton LinkButtonEditGvTypeNews = (LinkButton)gvTypeNews.FindControl("lbEdit");
        try { linkBtnTrigger(LinkButtonEditGvTypeNews); } catch { }
        LinkButton LinkButtonDeleteGvTypeNews = (LinkButton)gvTypeNews.FindControl("lbDelete");
        try { linkBtnTrigger(LinkButtonDeleteGvTypeNews); } catch { }

        gvTrigger(gvTitleNews);
        LinkButton LinkButtonEditGvTitleNews = (LinkButton)gvTypeNews.FindControl("lbEdit");
        try { linkBtnTrigger(LinkButtonEditGvTitleNews); } catch { }
        LinkButton LinkButtonDeleteGvTitleNews = (LinkButton)gvTypeNews.FindControl("lbDelete");
        try { linkBtnTrigger(LinkButtonDeleteGvTitleNews); } catch { }

        gvTrigger(gvSlideNews);
        LinkButton LinkButtonEditGvSlideNews = (LinkButton)gvSlideNews.FindControl("lbEdit");
        try { linkBtnTrigger(LinkButtonEditGvSlideNews); } catch { }
        LinkButton LinkButtonDeleteGvSlideNews = (LinkButton)gvSlideNews.FindControl("lbDelete");
        try { linkBtnTrigger(LinkButtonDeleteGvSlideNews); } catch { }


    }

    protected void setTriggerForm()
    {
        LinkButton LinkButtonSave = (LinkButton)FvListNews.FindControl("LinkButtonSave");
        linkBtnTrigger(LinkButtonSave);
    }

    protected string Error(string str)
    {

        str = HttpUtility.HtmlDecode(HttpUtility.HtmlDecode(str));

       
        return str;
    }
    //protected void setTriggerForm(GridView gvname)
    //{

    //}

    public static void ClearChromCache()
    {
        Process proc = null;
        try
        {
            proc = new Process();
            proc.StartInfo.FileName = "ChromeClearCache.bat";
            proc.StartInfo.CreateNoWindow = false;
            proc.Start();
            proc.WaitForExit();
        }
        catch (Exception ex)
        {
            Console.WriteLine("Exception Occurred :{0},{1}", ex.Message, ex.StackTrace.ToString());
        }

    }

    public void ClearApplicationCache()
    {
        List<string> keys = new List<string>();
        // retrieve application Cache enumerator
        IDictionaryEnumerator enumerator = Cache.GetEnumerator();
        // copy all keys that currently exist in Cache
        while (enumerator.MoveNext())
        {
            keys.Add(enumerator.Key.ToString());
        }

        // delete every key from cache
        for (int i = 0; i < keys.Count; i++)
        {
            Cache.Remove(keys[i]);
        }
    }
    public string Guidimagecache()
    {
        Guid g = Guid.NewGuid();
        string GuidString = Convert.ToBase64String(g.ToByteArray());
        GuidString = GuidString.Replace("=", "");
        GuidString = GuidString.Replace("+", "");

        return GuidString;
    }

    protected string FormatImageUrl(string url,string type)
    {

        if (type == "list") {
            string getPathfile = ConfigurationManager.AppSettings["path_flie_hr_news"];

            if (url != null && url.Length > 0)
            {


                //string extension = Path.GetExtension(url);
                //litdebug.Text = extension.ToString();

                string WithOutextension = Path.GetFileNameWithoutExtension(url);
                //litdebug.Text = WithOutextension.ToString();
                //return ("~/uploadfiles/hr_news/" + WithOutextension + "/" + url);
                //litdebug.Text += Server.MapPath(getPathfile + WithOutextension + "\\" + url);

                string filePath_upload = getPathfile + WithOutextension + "\\" + url;
               
                return filePath_upload;
            }
            else return null;
        }
        else if (type == "slide")
        {
            string getPathfile = ConfigurationManager.AppSettings["path_flie_hr_news_slidenews"];

            if (url != null && url.Length > 0)
            {


                //string extension = Path.GetExtension(url);
                //litdebug.Text = extension.ToString();

                string WithOutextension = Path.GetFileNameWithoutExtension(url);
                //litdebug.Text = WithOutextension.ToString();
                //return ("~/uploadfiles/hr_news/" + WithOutextension + "/" + url);
                //litdebug.Text += Server.MapPath(getPathfile + WithOutextension + "\\" + url);

                string filePath_upload = getPathfile + WithOutextension + "\\" + url;
               
                return filePath_upload;
            }
            else return null;
        }

        return null;
    }


}