﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="nwd_m0_room.aspx.cs" Inherits="websystem_masterdata_nwd_m0_room" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" Runat="Server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" Runat="Server">
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>

    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewIndex" runat="server">
             <div class="col-md-12">
                 <asp:LinkButton ID="btnToInsert" CssClass="btn btn-primary" runat="server" CommandName="btnToInsert" OnCommand="btnCommand"><i class="fa fa-plus-square"></i> สร้างอาคาร Network</asp:LinkButton>

                 <asp:GridView ID="GvMaster"
                       runat="server"
                       AutoGenerateColumns="false"
                       DataKeyNames="room_idx"
                       CssClass="table table-striped table-bordered table-responsive col-md-12 m-t-10"
                       HeaderStyle-CssClass="info"
                       AllowPaging="true"
                       PageSize="10"
                       OnRowEditing="Master_RowEditing"
                       OnRowUpdating="Master_RowUpdating"
                       OnRowCancelingEdit="Master_RowCancelingEdit"
                       OnPageIndexChanging="Master_PageIndexChanging"
                       OnRowDataBound="Master_RowDataBound">
                       <PagerStyle CssClass="pageCustom" />
                       <PagerSettings Mode="NumericFirstLast" PageButtonCount="1" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                       <EmptyDataTemplate>
                          <div style="text-align: center">No result</div>
                       </EmptyDataTemplate>
                       <Columns>
                          <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center" 
                             HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
                             <ItemTemplate>
                                  <small>
                                <%# (Container.DataItemIndex +1) %>
                                   </small>
                             </ItemTemplate>
                             <EditItemTemplate>
                                <asp:TextBox ID="room_idx" runat="server" CssClass="form-control"
                                   Visible="False" Text='<%# Eval("room_idx")%>' />

                                 <div class="col-md-6">
                                   <div class="form-group">
                                      <small>
                                      <label class="pull-left">ชื่อสถานที่ติดตั้งอุปกรณ์</label>
                                      <asp:Label ID="lbddlPlaceName" runat="server" Text='<%# Bind("place_idx") %>' Visible="false" />
                                      <asp:DropDownList ID="ddlPlaceNameUpdate" AutoPostBack="true" runat="server"
                                         CssClass="form-control">                                        
                                      </asp:DropDownList>
                                      </small>
                                   </div>
                                </div>



                                <div class="col-md-6">
                                   <div class="form-group">
                                      <small>
                                      <label class="pull-left">ชื่ออาคาร</label>
                                      <asp:UpdatePanel ID="panelRoomUpdate" runat="server">
                                         <ContentTemplate>
                                            
                                            <asp:TextBox ID="txtRoomNameUpdate" runat="server" CssClass="form-control"
                                               Text='<%# Eval("room_name")%>' />
                                            <asp:RequiredFieldValidator ID="requiredRoomNameUpdate"
                                               ValidationGroup="saveRoomNameUpdate" runat="server"
                                               Display="Dynamic"
                                               SetFocusOnError="true"
                                               ControlToValidate="txtRoomNameUpdate"
                                               Font-Size="1em" ForeColor="Red"
                                               ErrorMessage="กรุณากรอกชื่ออาคาร" />
                                         </ContentTemplate>
                                      </asp:UpdatePanel>
                                      </small>
                                   </div>
                                </div>
                                
                                <div class="col-md-6">
                                   <div class="form-group">
                                      <small>
                                      <label class="pull-left">สถานะ</label>
                                      <asp:DropDownList ID="ddlRoomStatusUpdate" AutoPostBack="false" runat="server"
                                         CssClass="form-control" SelectedValue='<%# Eval("room_status") %>'>
                                         <asp:ListItem Value="1" Text="Online"></asp:ListItem>
                                         <asp:ListItem Value="0" Text="Offline"></asp:ListItem>
                                      </asp:DropDownList>
                                      </small>
                                   </div>
                                </div>

                                <div class="col-md-12">
                                   <div class="pull-left">
                                      <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server"
                                         ValidationGroup="saveTypeNameUpdate" CommandName="Update"
                                         OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')">
                                         บันทึก
                                      </asp:LinkButton>
                                      <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server"
                                         CommandName="Cancel">ยกเลิก</asp:LinkButton>
                                   </div>
                                </div>
                             </EditItemTemplate>
                          </asp:TemplateField>

                            <asp:TemplateField HeaderText="ชื่อสถานที่ติดตั้งอุปกรณ์" ItemStyle-HorizontalAlign="Left"
                             HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                             <ItemTemplate>
                                <small>
                                   <asp:Label ID="PlaceName" runat="server" Text='<%# Eval("place_name") %>' />
                                </small>
                             </ItemTemplate>
                             <EditItemTemplate />
                             <FooterTemplate />
                          </asp:TemplateField>

                           <asp:TemplateField HeaderText="ชื่ออาคาร" ItemStyle-HorizontalAlign="Left"
                             HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                             <ItemTemplate>
                                <small>
                                   <asp:Label ID="RoomName" runat="server" Text='<%# Eval("room_name") %>' />
                                </small>
                             </ItemTemplate>
                             <EditItemTemplate />
                             <FooterTemplate />
                          </asp:TemplateField>

                           <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center"
                             HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                             <ItemTemplate>
                                <small>
                                   <asp:Label ID="Statusroom" runat="server" Text='<%# getStatus((int)Eval("room_status")) %>' />
                                </small>
                             </ItemTemplate>
                             <EditItemTemplate />
                             <FooterTemplate />
                          </asp:TemplateField>

                           <asp:TemplateField HeaderText="Manage" ItemStyle-HorizontalAlign="center"
                             HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                             <ItemTemplate>
                                <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                   data-toggle="tooltip" title="แก้ไข"><i class="glyphicon glyphicon-pencil"></i></asp:LinkButton>
                                <asp:LinkButton ID="delete" CssClass="text-trash" runat="server" data-toggle="tooltip"
                                   title="ลบ" CommandName="btnDelete" OnCommand="btnCommand"
                                   CommandArgument='<%# Eval("room_idx") %>'
                                   OnClientClick="return confirm('คุณค้องการลบข้อมูลนี้ใช่หรือไม่')">
                                   <i class="glyphicon glyphicon-trash"></i>
                                </asp:LinkButton>
                             </ItemTemplate>
                             <EditItemTemplate />
                             <FooterTemplate />
                          </asp:TemplateField>
                            
                       </Columns>
                    </asp:GridView>

            </div>
        </asp:View>

       <!-- Start Insert Form -->
      <asp:View ID="ViewInsert" runat="server">
         <div class="row col-md-12">
            <div class="row">
               <div class="col-md-12">
                  <div class="form-group">
                     <asp:LinkButton CssClass="btn btn-danger" data-toggle="tooltip" title="Back" runat="server"
                        CommandName="btnCancel" OnCommand="btnCommand">ย้อนกลับ</asp:LinkButton>
                  </div>
               </div>

                <div class="col-md-6">
                  <div class="form-group">
                     <label>ชื่อสถานที่ติดตั้งอุปกรณ์</label>
                     <asp:UpdatePanel ID="panelplace" runat="server">
                        <ContentTemplate>
                          <asp:DropDownList ID="ddlplace" runat="server" CssClass="form-control"></asp:DropDownList>
                           <asp:RequiredFieldValidator ID="requiredddltype"
                              ValidationGroup="save" runat="server"
                              Display="Dynamic"
                              SetFocusOnError="true"
                              ControlToValidate="ddlplace"
                              InitialValue="00"
                              Font-Size="1em" ForeColor="Red"
                              ErrorMessage="กรุณากรอกชื่อสถานที่ติดตั้งอุปกรณ์" />
                        </ContentTemplate>
                     </asp:UpdatePanel>
                  </div>
               </div>



               <div class="col-md-6">
                  <div class="form-group">
                     <label>ชื่ออาคาร</label>
                     <asp:UpdatePanel ID="panelroomName" runat="server">
                        <ContentTemplate>
                           <asp:TextBox ID="txtroomName" runat="server" CssClass="form-control"
                              placeholder="ชื่ออาคาร..." />
                           <asp:RequiredFieldValidator ID="requiredroomName"
                              ValidationGroup="save" runat="server"
                              Display="Dynamic"
                              SetFocusOnError="true"
                              ControlToValidate="txtroomName"
                              Font-Size="1em" ForeColor="Red"
                              ErrorMessage="กรุณากรอกชื่ออาคาร" />
                        </ContentTemplate>
                     </asp:UpdatePanel>
                  </div>
               </div>
               
               <div class="col-md-12">
                  <div class="form-group">
                     <label>สถานะ</label>
                     <asp:DropDownList ID="ddlroomStatus" runat="server" CssClass="form-control">
                        <asp:ListItem Value="1" Text="Online" />
                        <asp:ListItem Value="0" Text="Offline" />
                     </asp:DropDownList>
                  </div>
               </div>

               <div class="col-md-12">
                  <div class="form-group">
                     <asp:Button CssClass="btn btn-success" runat="server" CommandName="btnInsert" OnCommand="btnCommand" Text="บันทึก" ValidationGroup="save" />
                  </div>
               </div>
            </div>
         </div>
      </asp:View>
      <!-- End Insert Form -->


    </asp:MultiView>

</asp:Content>