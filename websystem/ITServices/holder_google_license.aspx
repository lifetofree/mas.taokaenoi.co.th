﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="holder_google_license.aspx.cs" Inherits="websystem_ITServices_holder_google_license" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <asp:HyperLink ID="setOntop" runat="server"></asp:HyperLink>

    <asp:Literal ID="litDebug" runat="server"></asp:Literal>

    <!--*** START Menu ***-->
    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand">Menu</a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav" runat="server">
                    <li id="liToIndexList" runat="server">
                        <asp:LinkButton ID="btnToIndexList" runat="server"
                            CommandName="btnToIndexList"
                            OnCommand="btnCommand" Text="ข้อมูลผู้ถือครอง Google - License" />
                    </li>

                </ul>

                <ul class="nav navbar-nav navbar-right" runat="server">

                    <li id="_divMenuLiToDocument" runat="server">
                        <asp:HyperLink ID="_divMenuBtnToDocument" runat="server"
                            NavigateUrl="https://docs.google.com/document/d/1dF-UFvy2YemJ7hgBVbS69c5zdu3y99Q5jxQVjo3tHrg" Target="_blank" Text="คู่มือการใช้งาน" />
                    </li>
                </ul>

            </div>
        </nav>
    </div>
    <!--*** END Menu ***-->

    <%----------- MultiView Start ---------------%>
    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <%----------- ViewIndex Holder Google License Start ---------------%>
        <asp:View ID="ViewIndex" runat="server">

            <div class="form-group">
                <div class="col-md-12">
                    <asp:LinkButton ID="btnSearch" Visible="false" CssClass="btn btn-primary" runat="server" data-original-title="Search" data-toggle="tooltip" CommandName="btnSearch" OnCommand="btnCommand"><span class="glyphicon glyphicon-search"></span> ค้นหา</asp:LinkButton>

                    <asp:LinkButton ID="btnResetSearchPage" CssClass="btn btn-default" runat="server" data-original-title="Refresh" data-toggle="tooltip" CommandName="btnCancelIndex_First" OnCommand="btnCommand"><span class="glyphicon glyphicon-refresh"></span></asp:LinkButton>
                </div>
            </div>

            <!--*** START Back To Index ***-->
            <div class="form-group">
                <div class="col-md-12">
                    <div id="fvBacktoIndex" class="row" runat="server" visible="false">
                        <%--<div class="col-md-12">--%>
                        <asp:LinkButton ID="btnSearchBack" CssClass="btn btn-danger" runat="server" data-original-title="Back" data-toggle="tooltip" OnCommand="btnCommand" CommandName="btnSearchBack"><span class="fa fa-times" aria-hidden="true"></span> ซ่อนแถบเครื่องมือการค้นหา</asp:LinkButton>
                        <%--</div>--%>
                    </div>
                </div>

            </div>
            <!--*** END Back To Index ***-->

            <!--*** START Search Index ***-->
            <div class="form-group">
                <div class="col-md-12">
                    <div id="showsearch" runat="server" visible="false">
                        <br />
                        <div class="panel panel-primary">
                            <div class="panel-heading f-bold">ค้นหาข้อมูล google license</div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <%-------------- องค์กร,ฝ่าย --------------%>
                                    <div class="form-group">
                                        <asp:Label ID="lborg_idxsearch" CssClass="col-sm-2 control-label" runat="server" Text="องค์กร : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlorg_idxsearch" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            </asp:DropDownList>

                                        </div>

                                        <asp:Label ID="lbrdept_idxsearch" CssClass="col-sm-2 control-label" runat="server" Text="ฝ่าย : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlrdept_idxsearch" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                <asp:ListItem Text="เลือกฝ่าย ...." Value="00"></asp:ListItem>
                                            </asp:DropDownList>

                                        </div>

                                    </div>
                                    <%-------------- องค์กร,ฝ่าย --------------%>

                                    <%-------------- ตำแหน่ง,ชื่อพนักงาน --------------%>
                                    <div class="form-group">
                                        <asp:Label ID="lbrsec_idxsearch" CssClass="col-sm-2 control-label" runat="server" Text="แผนก : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlrsec_idxsearch" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                <asp:ListItem Text="เลือกแผนก ...." Value="00"></asp:ListItem>
                                            </asp:DropDownList>

                                        </div>


                                        <%--<asp:Label ID="lbddlposition_idxsearch" CssClass="col-sm-2 control-label" runat="server" Text="ตำแหน่ง : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlposition_idxsearch" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                <asp:ListItem Text="กรุณาเลือกตำแหน่ง ...." Value="00"></asp:ListItem>
                                            </asp:DropDownList>

                                        </div>--%>

                                        <asp:Label ID="lbemp_idxsearch" CssClass="col-sm-2 control-label" Visible="true" runat="server" Text="ชื่อพนักงาน : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlemp_idxsearch" runat="server" CssClass="form-control" Visible="true">
                                                <asp:ListItem Text="เลือกพนักงาน ...." Value="00"></asp:ListItem>
                                            </asp:DropDownList>

                                        </div>

                                    </div>
                                    <%-------------- ตำแหน่ง,ชื่อพนักงาน --------------%>

                                    <%-------------- Cost center,  ชื่อ e-mail --------------%>
                                    <div class="form-group">

                                        <%--<asp:Label ID="Label1" CssClass="col-sm-2 control-label" runat="server" Text="Cost center : " />
                                        <div class="col-sm-3">

                                            <asp:TextBox ID="txtcostcenter_no" runat="server" CssClass="form-control" MaxLength="6" placeholder="Cost center ...">
                                            </asp:TextBox>
                                        </div>--%>

                                        <asp:Label ID="lbname_email" CssClass="col-sm-2 control-label" runat="server" Text="ชื่อ e-mail : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtname_email" runat="server" ValidationGroup="SearchIndex" CssClass="form-control" placeholder="ชื่อ e-mail ...">                                                           
                                            </asp:TextBox>

                                            <asp:RegularExpressionValidator ID="Re_txtname_email" runat="server" ValidationGroup="SearchIndex" Display="None"
                                                ErrorMessage="กรุณากรอกอีเมล์ให้ถูกต้อง" Font-Size="11"
                                                ControlToValidate="txtname_email"
                                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                                            <%-- <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender81" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtname_license_gmail" Width="160" />--%>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender91" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txtname_email" Width="160" />

                                        </div>

                                    </div>
                                    <%-------------- Cost center, ชื่อ e-mail  --------------%>

                                    <%-------------- btnsearch --------------%>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <%--<div class="col-sm-12"></div>--%>
                                            <div class="pull-left">
                                                <asp:LinkButton ID="btnSearchIndex_First" CssClass="btn btn-primary" runat="server" ValidationGroup="SearchIndex" OnCommand="btnCommand" CommandName="btnSearchIndex_First" data-toggle="tooltip" title="Search" OnClientClick="return confirm('คุณต้องการค้นหารายการนี้ใช่หรือไม่ ?')"><span class="glyphicon glyphicon-search"></span></asp:LinkButton>


                                                <asp:LinkButton ID="btnCancelIndex_First" CssClass="btn btn-default" runat="server" OnCommand="btnCommand" CommandName="btnCancelIndex_First" data-toggle="tooltip" title="Cancel"><i class="fa fa-times"></i></asp:LinkButton>
                                            </div>

                                        </div>
                                    </div>
                                    <%-------------- btnsearch --------------%>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!--*** END Search Index ***-->

            <!--*** START GRIDVIEW Index ***-->
            <%-- col-md-12 m-t-10--%>
            <div class="col-md-12">
                <div id="gridviewindex" class="row" runat="server">
                    <br />
                    <asp:GridView ID="GvMaster"
                        runat="server"
                        AutoGenerateColumns="false"
                        DataKeyNames="u0_idx"
                        CssClass="table table-striped table-bordered table-responsive"
                        HeaderStyle-CssClass="info"
                        AllowPaging="true"
                        PageSize="10"
                        OnRowEditing="Master_RowEditing"
                        OnRowUpdating="Master_RowUpdating"
                        OnRowCancelingEdit="Master_RowCancelingEdit"
                        OnPageIndexChanging="Master_PageIndexChanging"
                        OnRowDataBound="Master_RowDataBound">
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">No result</div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center" Visible="true">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbu0_idx" runat="server" Visible="false" Text='<%# Eval("u0_idx") %>' />
                                        <asp:Label ID="lbm0_idx" runat="server" Visible="false" Text='<%# Eval("m0_idx") %>' />
                                        <asp:Label ID="lbunidx" runat="server" Visible="false" Text='<%# Eval("m0_node_idx") %>' />
                                        <%# (Container.DataItemIndex +1) %>
                                    </small>
                                </ItemTemplate>

                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-HorizontalAlign="Left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbemp_idx" runat="server" Visible="false" Text='<%# Eval("emp_idx") %>' />
                                        <asp:Label ID="DetailHolderGoogleLicense" runat="server">

                                            <p><b>&nbsp;&nbsp;&nbsp;ชื่อผู้ถือครอง :</b> <%# Eval("name_holder") %></p>   

                                            <p><b>&nbsp;&nbsp;&nbsp;องค์กร :</b> <%# Eval("org_name_th_holder") %></p>                                                                             
                                            <p><b>&nbsp;&nbsp;&nbsp;ฝ่าย :</b> <%# Eval("dept_name_th_holder") %></p>
                                       
                                            <p><b>&nbsp;&nbsp;&nbsp;แผนก :</b> <%# Eval("sec_name_th_holder") %></p>  

                                           <%-- <p><b>&nbsp;&nbsp;&nbsp;วันที่สร้าง :</b> <%# Eval("date_create") %></p>

                                            <p><b>&nbsp;&nbsp;&nbsp;เวลาที่สร้าง :</b> <%# Eval("time_create") %></p>--%>
                                            
                                            
                                        </asp:Label>

                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="วันที่สร้าง" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="date_create_detail" runat="server" Text='<%# Eval("date_create") %>' />
                                        <br />
                                        <asp:Label ID="time_create_detail" runat="server" Text='<%# Eval("time_create") %>' />
                                        <%--<asp:Label ID="Count_EmpGoogleLicnese" Visible="false" runat="server" Text='<%# Eval("Count_EmpGoogleLicnese") %>' />--%>
                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="รายชื่อ e-mail" ItemStyle-HorizontalAlign="left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="email_license_detail" runat="server" Text='<%# Eval("email_license") %>' />
                                        <%--<asp:Label ID="Count_EmpGoogleLicnese" Visible="false" runat="server" Text='<%# Eval("Count_EmpGoogleLicnese") %>' />--%>
                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ประเภท e-mail" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbl_m0type_email_idx_detail" runat="server" Visible="false" Text='<%# Eval("m0type_email_idx") %>' />
                                        <asp:Label ID="lbl_m0type_email_name_detail" runat="server" Text='<%# Eval("m0type_email_name") %>' />
                                        <%--<asp:Label ID="Count_EmpGoogleLicnese" Visible="false" runat="server" Text='<%# Eval("Count_EmpGoogleLicnese") %>' />--%>
                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ผู้สร้างรายการ" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="NameCreate" runat="server" Text='<%# Eval("name_create") %>' />
                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="status_documentdetail" runat="server" Text='<%#Eval("status_document") %>' />

                                        <%-- <asp:Label ID="lbstatus_active" runat="server" Text='<%# getStatus((int)Eval("Count_EmpGoogleLicnese")) %>' />--%>

                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ดำเนินการ" ItemStyle-HorizontalAlign="center"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>

                                    <asp:LinkButton ID="btnViewindex" CssClass="btn btn-primary" Font-Size="11px" runat="server" CommandName="btnViewindex" OnCommand="btnCommand" data-toggle="tooltip" title="ดูข้อมูล" CommandArgument='<%# Eval("u0_idx") + ";" + Eval("m0_node_idx")+ ";" + Eval("emp_idx")+ ";" + Eval("m0_idx") %>'><i class="fa fa-file"></i></asp:LinkButton>

                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>
                </div>
            </div>
            <!--*** END GRIDVIEW Index ***-->

            <!--*** START Show Data in Viewindex ***-->
            <div id="div_show_detailgoogle" runat="server" visible="false">
                <asp:FormView ID="FvViewDetail" runat="server" OnDataBound="FvDetail_DataBound" class="col-sm-12 font_text">
                    <ItemTemplate>
                        <div class="col-sm-12">
                            <div class="panel panel-info">
                                <div class="panel-heading f-bold">รายละเอียดผู้ถือครอง Google-License</div>
                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">
                                        <asp:HiddenField ID="hf_m0_node_idx" runat="server" Value='<%# Eval("m0_node_idx") %>' />
                                        <asp:HiddenField ID="hf_m0_actor_idx" runat="server" Value='<%# Eval("m0_actor_idx") %>' />
                                        <asp:HiddenField ID="hf_doc_decision" runat="server" Value='<%# Eval("doc_decision") %>' />


                                        <div class="form-group">
                                            <asp:Label ID="lbname_licenseview" CssClass="col-sm-2 control-label" runat="server" Text="ชื่อ : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtemp_idxview" runat="server" Visible="false" Enabled="false" CssClass="form-control" Text='<%# Eval("emp_idx") %>'>
                                                </asp:TextBox>
                                                <asp:TextBox ID="txtname_holder_view" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("name_holder") %>'>
                                                </asp:TextBox>
                                            </div>

                                            <asp:Label ID="lborg_name_th_licenseview" CssClass="col-sm-2 control-label" runat="server" Text="องค์กร : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtorg_idx_holderview" Enabled="false" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("org_idx_holder") %>'>
                                                </asp:TextBox>
                                                <asp:TextBox ID="txtorg_name_th_holderview" Enabled="false" runat="server" CssClass="form-control" Text='<%# Eval("org_name_th_holder") %>'>
                                                </asp:TextBox>
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="lbdept_name_th_licenseview" CssClass="col-sm-2 control-label" runat="server" Text="ฝ่าย : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtrdept_idx_holderview" runat="server" Visible="false" Enabled="false" CssClass="form-control" Text='<%# Eval("rdept_idx_holder") %>'>
                                                </asp:TextBox>
                                                <asp:TextBox ID="txtdept_name_th_holderview" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("dept_name_th_holder") %>'>
                                                </asp:TextBox>
                                            </div>

                                            <asp:Label ID="lbsec_name_th_licenseview" CssClass="col-sm-2 control-label" runat="server" Text="แผนก : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtrsec_idx_holderview" Enabled="false" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("rsec_idx_holder") %>'>
                                                </asp:TextBox>
                                                <asp:TextBox ID="txtsec_name_th_holderview" Enabled="false" runat="server" CssClass="form-control" Text='<%# Eval("sec_name_th_holder") %>'>
                                                </asp:TextBox>
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="lbemail_licenseview" CssClass="col-sm-2 control-label" runat="server" Text="e-mail : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtemail_licenseview" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("email_license") %>'>
                                                </asp:TextBox>

                                            </div>

                                            <asp:Label ID="lbl_typemail_view" CssClass="col-sm-2 control-label" runat="server" Text="ประเภท e-mail : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txt_m0type_email_idxview" Enabled="false" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("m0type_email_idx") %>'>
                                                </asp:TextBox>
                                                <asp:TextBox ID="txt_m0type_email_nameview" Enabled="false" runat="server" CssClass="form-control" Text='<%# Eval("m0type_email_name") %>'>
                                                </asp:TextBox>
                                            </div>

                                        </div>


                                    </div>
                                </div>
                    </ItemTemplate>
                </asp:FormView>
            </div>
            <!--*** END Show Data in Viewindex ***-->

            <!--*** START ปุ่มยกเลิก Viewindex ***-->
            <div class="form-group">
                <div id="div_btn" class="row" runat="server" visible="false">
                    <%--<div class="form-group">--%>
                    <div class="pull-right">
                        <div class="col-sm-12">
                            <asp:LinkButton ID="btnCancelView" CssClass="btn btn-default" data-toggle="tooltip" title="Back" runat="server" OnCommand="btnCommand" CommandName="btnCancelView" Visible="false"><i class="fa fa-undo"></i> ย้อนกลับ</asp:LinkButton>

                        </div>
                    </div>
                    <%--</div>--%>
                </div>
            </div>
            <!--*** END ปุ่มยกเลิก Viewindex ***-->

            <%-------------- START DDL Approve Status --------------%>
            <div class="form-group">
                <div class="col-sm-12">
                    <div id="div_approvstatecreate" class="row panel panel-info font_text" runat="server" visible="false">
                        <%--<div class="panel panel-info">--%>
                        <div class="panel-heading f-bold">รายการรับทราบ Google License</div>
                        <div class="form-group" id="approvecreate" runat="server">

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="col-md-8 col-md-offset-2">

                                        <div class="form-group">
                                            <%--<div class="col-sm-1"></div>--%>
                                            <asp:Label ID="lbapprovegooglelicense" CssClass="col-md-2 control-label" runat="server" Text="ผลการรับทราบ :" />
                                            <div class="col-sm-10">
                                                <asp:DropDownList CssClass="form-control" ID="ddl_approvestatus" runat="server">
                                                    <asp:ListItem Value="0">กรุณาเลือกสถานะ...</asp:ListItem>
                                                    <asp:ListItem Value="1">รับทราบ</asp:ListItem>
                                                    <%--<asp:ListItem Value="2">ไม่อนุมัติ</asp:ListItem>--%>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="Requiredddl_approvestatus" ValidationGroup="SaveApproveLicense" runat="server" Display="None"
                                                    ControlToValidate="ddl_approvestatus" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกสถานะ"
                                                    ValidationExpression="กรุณาเลือกสถานะ" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredddl_approvestatus" Width="160" />
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <%--<div class="col-sm-1"></div>--%>
                                            <asp:Label ID="lbtxtComment_approve" CssClass="col-md-2 control-label" runat="server" Text="ความเห็น :" />
                                            <div class="col-sm-10">
                                                <asp:TextBox CssClass="form-control" ID="txtComment_approve" TextMode="multiline" Rows="2" placeholder="แสดงความเห็นเพิ่มเติม ..." runat="server">
                                                
                                                </asp:TextBox>

                                            </div>

                                        </div>
                                    </div>

                                </div>


                            </div>
                        </div>
                        <%-- </div>--%>
                    </div>
                    <br />

                </div>
            </div>
            <%-------------- END DDL Approve Status --------------%>

            <%-------------- START ปุ่มบันทึกผู้อนุมัติ --------------%>
            <div class="form-group" id="show_approve_googlelicense" runat="server" visible="false">
                <%--<div class="col-sm-12">--%>
                <div id="div_approve" class="row" runat="server" visible="false">
                    <%-- <div class="form-group">--%>

                    <%--<div class="col-sm-6"></div>--%>
                    <%--<div class="col-sm-2">--%>
                    <div class="pull-right">
                        <div class="col-sm-12">
                            <asp:LinkButton ID="btnsaveapprove" CssClass="btn btn-success" runat="server" ValidationGroup="SaveApproveLicense" OnCommand="btnCommand" CommandName="btnsaveapprove" data-toggle="tooltip" title="บันทึก" OnClientClick="return confirm('คุณต้องการเพิ่มรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>


                            <asp:LinkButton ID="btnCancelApprove" CssClass="btn btn-default" runat="server" OnCommand="btnCommand" CommandName="btnCancelApprove" data-toggle="tooltip" title="ยกเลิก"><i class="fa fa-times"></i></asp:LinkButton>
                        </div>
                    </div>

                    <%--</div>--%>
                </div>
                <%--</div>--%>
            </div>

            <%-------------- END ปุ่มบันทึกผู้อนุมัติ --------------%>

            <!-- START Log ผู้ถือครอง License -->
            <div class="form-group">
                <div class="col-md-12">
                    <%--<br />--%>
                    <div class="row panel panel-default font_text" visible="false" runat="server" id="panel_detail_googlelicense">
                        <div class="panel panel-default">
                            <%-- <h3 class="panel-heading f-bold"><strong>ประวัติข้อมูล</strong></h3>--%>
                            <div class="panel-heading f-bold">ประวัติรายการ Google License</div>
                        </div>


                        <%-- <asp:TextBox ID="testbind" runat="server"></asp:TextBox>--%>
                        <div class="panel-body">
                            <table class="table table-striped f-s-12">
                                <%--<asp:GridView ID="gvTest" runat="server"></asp:GridView>--%>
                                <asp:Repeater ID="rptLogHolderGooglelicense" runat="server">
                                    <HeaderTemplate>
                                        <tr>
                                            <th>วัน/ เวลา</th>
                                            <th>ผู้ดำเนินการ</th>
                                            <th>ดำเนินการ</th>
                                            <th>ผลการดำเนินการ</th>
                                            <th>ความคิดเห็น</th>

                                        </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <%--<td data-th="ลำดับ">
                                                <%# (Container.ItemIndex + 1) %>

                                            </td>--%>

                                            <td data-th="วัน/ เวลา">
                                                <%--<%# Eval("OrgNameTH") %>--%>
                                                <asp:Label ID="lbcreate_datelog" runat="server" Text='<%# Eval("create_date") %>' />
                                                <%-- <asp:Label ID="lbsoftware_name_idxview" Visible="false" runat="server" Text='<%# Eval("software_name_idx") %>' />--%>

                                            </td>

                                            <td data-th="ผู้ดำเนินการ">
                                                <%--<%# Eval("OrgNameTH") %>--%>
                                                <asp:Label ID="lbname_createlog" runat="server" Text='<%# Eval("name_create") %>' />&nbsp;<asp:Label ID="lbactor_deslog" runat="server" Text='<%# Eval("actor_des") %>' />
                                                <%-- <asp:Label ID="lbsoftware_typeview" Visible="false" runat="server" Text='<%# Eval("software_type") %>' />--%>

                                            </td>

                                            <td data-th="ดำเนินการ">
                                                <%--<%# Eval("OrgNameTH") %>--%>
                                                <asp:Label ID="lbnode_namelog" runat="server" Text='<%# Eval("node_name") %>' />
                                                <%--<asp:Label ID="Label8" Visible="false" runat="server" Text='<%# Eval("software_name_idx") %>' />--%>
                                           
                                            </td>

                                            <td data-th="ผลการดำเนินการ">
                                                <%--<%# Eval("OrgNameTH") %>--%>
                                                <asp:Label ID="lbstatus_namelog" runat="server" Text='<%# Eval("status_name") %>' />
                                                <%--<asp:Label ID="Label8" Visible="false" runat="server" Text='<%# Eval("software_name_idx") %>' />--%>
                                           
                                            </td>

                                            <td data-th="ความคิดเห็น">
                                                <%--<%# Eval("OrgNameTH") %>--%>
                                                <asp:Label ID="lbcommentlog" runat="server" Text='<%# Eval("comment") %>' />
                                                <%--<asp:Label ID="Label8" Visible="false" runat="server" Text='<%# Eval("software_name_idx") %>' />--%>
                                           
                                            </td>


                                        </tr>

                                    </ItemTemplate>

                                </asp:Repeater>
                            </table>
                        </div>
                    </div>

                    <%--  <h5>&nbsp;</h5>--%>
                    <br />
                </div>
            </div>
            <!-- END Log ผู้ถือครอง License -->


        </asp:View>
        <%----------- ViewIndex Holder Google License END ---------------%>
    </asp:MultiView>
    <%-----------END MultiView END ---------------%>
</asp:Content>

