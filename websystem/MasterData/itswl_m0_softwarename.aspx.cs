﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_itswl_m0_softwarename : System.Web.UI.Page
{
    #region initial function/data
    function_tool _funcTool = new function_tool();

    data_softwarelicense _data_softwarelicense = new data_softwarelicense(); 

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlSetSWName = _serviceUrl + ConfigurationManager.AppSettings["urlSetSWName"];
    static string _urlGetSoftwareName = _serviceUrl + ConfigurationManager.AppSettings["urlGetSoftwareName"];
    static string _urlDelSWName = _serviceUrl + ConfigurationManager.AppSettings["urlDelSWName"];
    static string _urlGettypeSoftwareName = _serviceUrl + ConfigurationManager.AppSettings["urlGettypeSoftwareName"];
    static string _urlGetCountDeptm0 = _serviceUrl + ConfigurationManager.AppSettings["urlGetCountDeptm0"];
    static string _urlGetCountUse = _serviceUrl + ConfigurationManager.AppSettings["urlGetCountUse"];

    static string _urlGetCountGhost = _serviceUrl + ConfigurationManager.AppSettings["urlGetCountGhost"];
    static string _urlGetCountLicenseOk = _serviceUrl + ConfigurationManager.AppSettings["urlGetCountLicenseOk"];
    static string _urlGetCountAllLicense = _serviceUrl + ConfigurationManager.AppSettings["urlGetCountAllLicense"];


    string _localJson = "";
    int _tempInt = 0;

    string _defaultDdlText;
    string _defaultDdlValue;

    int emp_idx = 0;
    #endregion initial function/data



    protected void Page_Load(object sender, EventArgs e)
    {

        emp_idx = int.Parse(Session["emp_idx"].ToString());

        if (!IsPostBack)
        {
            initPage();
            actionIndex();

        }

    }

    #region selected index  
    protected void actionIndex()
    {

        data_softwarelicense _data_softwarelicenseindex = new data_softwarelicense();
        _data_softwarelicenseindex.softwarename_list = new softwarename_detail[1];

        softwarename_detail _softwarename_detailindex = new softwarename_detail();

        _softwarename_detailindex.software_name_idx = 0;

        _data_softwarelicenseindex.softwarename_list[0] = _softwarename_detailindex;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevicesindex));
        _data_softwarelicenseindex = callServiceSoftwareLicense(_urlGetSoftwareName, _data_softwarelicenseindex);


        setGridData(GvMaster, _data_softwarelicenseindex.softwarename_list);

        //ViewState["value_software_name_allcount"] = _data_softwarelicenseindex.softwarename_list[0].software_name_idx;

    }
  

    protected void ddltypeSoftware()
    {

        DropDownList ddltype_name = (DropDownList)ViewInsert.FindControl("ddltype_name");

        ddltype_name.Items.Clear();
        ddltype_name.AppendDataBoundItems = true;
        ddltype_name.Items.Add(new ListItem("กรุณาเลือกประเภท Software ...", "00"));

        _data_softwarelicense.softwarename_list = new softwarename_detail[1];
        softwarename_detail _softwarename_detailddl = new softwarename_detail();

        _data_softwarelicense.softwarename_list[0] = _softwarename_detailddl;

        ////litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense));

        _data_softwarelicense = callServiceSoftwareLicense(_urlGettypeSoftwareName, _data_softwarelicense);

        ddltype_name.DataSource = _data_softwarelicense.softwarename_list;
        ddltype_name.DataTextField = "type_name";
        ddltype_name.DataValueField = "type_idx";
        ddltype_name.DataBind();


    }

    protected void count_dept()
    {

        GridView GvMaster = (GridView)ViewIndex.FindControl("GvMaster");
        Repeater rpcountogdept = (Repeater)GvMaster.FindControl("rpcountogdept");

        data_softwarelicense _data_softwarelicense_countdept = new data_softwarelicense();
        _data_softwarelicense_countdept.bindu0_device_list = new bindu0_device_detail[1];

        bindu0_device_detail _bindu0_device_detail = new bindu0_device_detail();

        //_log_m0software_detail.software_idx = int.Parse(ViewState["software_idx"].ToString());

        _data_softwarelicense_countdept.bindu0_device_list[0] = _bindu0_device_detail;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkview));

        _data_softwarelicense_countdept = callServiceSoftwareLicense(_urlGetCountDeptm0, _data_softwarelicense_countdept);

        rpcountogdept.DataSource = _data_softwarelicense_countdept.bindu0_device_list;
        rpcountogdept.DataBind();



    }


    #endregion selected index    


    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();


        int _softwarename_idx;
        string _name;
        string _version;
        string _software_type;
        //string _txtemail;

        int _cemp_idx;

        softwarename_detail _softwarename_detail = new softwarename_detail();

        switch (cmdName)
        {

            case "btnToInsert":
                MvMaster.SetActiveView(ViewInsert);

                ddltypeSoftware();
                break;

            case "btnCancel":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "btnInsert":

                _name = ((TextBox)ViewInsert.FindControl("txtSoftwareName")).Text.Trim();
                _version = ((TextBox)ViewInsert.FindControl("txtversion")).Text.Trim();
                DropDownList _ddltype_name = (DropDownList)ViewInsert.FindControl("ddltype_name");
                DropDownList _ddlSoftwareStatus = (DropDownList)ViewInsert.FindControl("ddlSoftwareStatus");
                //DropDownList ddlCompanyStatus = (DropDownList)ViewInsert.FindControl("ddlCompanyStatus");

                _cemp_idx = emp_idx;

                //data_softwarelicense _data_softwarelicense = new data_softwarelicense();

                _data_softwarelicense.softwarename_list = new softwarename_detail[1];
                _softwarename_detail.software_name_idx = 0;//_type_idx; 
                _softwarename_detail.name = _name;
                _softwarename_detail.version = _version;
                _softwarename_detail.software_type = int.Parse(_ddltype_name.SelectedValue);
                _softwarename_detail.status = int.Parse(ddlSoftwareStatus.SelectedValue);
                _softwarename_detail.cemp_idx = _cemp_idx;

                _data_softwarelicense.softwarename_list[0] = _softwarename_detail;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense));
                _data_softwarelicense = callServiceSoftwareLicense(_urlSetSWName, _data_softwarelicense);

                if (_data_softwarelicense.return_code == 0)
                {
                    //actionIndex();
                    //MvMaster.SetActiveView(ViewIndex);
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);

                }
                else
                {
                    setError(_data_softwarelicense.return_code.ToString() + " - " + _data_softwarelicense.return_msg);
                }

                
                break;

            case "btnDelete":

                _softwarename_idx = int.Parse(cmdArg);
                _cemp_idx = emp_idx;


                _data_softwarelicense.softwarename_list = new softwarename_detail[1];
                _softwarename_detail.software_name_idx = _softwarename_idx;
                _softwarename_detail.cemp_idx = _cemp_idx;

                _data_softwarelicense.softwarename_list[0] = _softwarename_detail;

               // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense));

                _data_softwarelicense = callServiceSoftwareLicense(_urlDelSWName, _data_softwarelicense);


                if (_data_softwarelicense.return_code == 0)
                {

                    //actionIndex();
                    //MvMaster.SetActiveView(ViewIndex);
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);


                }
                else
                {

                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่สามารถลบ Software ได้ เนื่องจากมีการใช้งานแล้ว!!!');", true);
                    break;
                    //setError(_data_softwarelicense.return_code.ToString() + " - " + _data_softwarelicense.return_msg);
                }

                break;




        }
    }
    #endregion btnCommand

    #region bind data
    protected void getGridData(string _cmdName, int _actionType)
    {
        // setGridData(gvBookingType, ViewState["listData"]);
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected string getStatus(int status)
    {
        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();
                actionIndex();
                break;
        }
    }

    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";


                    var lbtype_nameUpdat = (Label)e.Row.FindControl("lbtype_nameUpdat");
                    var ddltype_nameUpdat = (DropDownList)e.Row.FindControl("ddltype_nameUpdat");

                    ddltype_nameUpdat.AppendDataBoundItems = true;

                    data_softwarelicense _data_softwarelicense_edit = new data_softwarelicense();
                    _data_softwarelicense_edit.softwarename_list = new softwarename_detail[1];
                    softwarename_detail _softwarename_detailedit = new softwarename_detail();

                    _data_softwarelicense_edit.softwarename_list[0] = _softwarename_detailedit;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

                    _data_softwarelicense_edit = callServiceSoftwareLicense(_urlGettypeSoftwareName, _data_softwarelicense_edit);

                    ddltype_nameUpdat.DataSource = _data_softwarelicense_edit.softwarename_list;
                    ddltype_nameUpdat.DataTextField = "type_name";
                    ddltype_nameUpdat.DataValueField = "type_idx";
                    ddltype_nameUpdat.DataBind();
                    ddltype_nameUpdat.Items.Insert(00, new ListItem("กรุณาเลือกชื่อสถานที่ติดตั้งอุปกรณ์ ....", "00"));
                    ddltype_nameUpdat.SelectedValue = lbtype_nameUpdat.Text;


                }

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {

                    //////////// จำนวน License ทั้งหมด ตามฝ่ายที่ถูกลิขสิทธิ์  ////////////////
                    //GridView GvMaster = (GridView)e.Row.Cells[4].FindControl("GvMaster");
                    TextBox txt_software_name_idx = (TextBox)e.Row.Cells[1].FindControl("txt_software_name_idx");
                    Repeater rpcountogdept = (Repeater)e.Row.Cells[4].FindControl("rpcountogdept");
                    Repeater rpcountuselicense = (Repeater)e.Row.Cells[4].FindControl("rpcountuselicense");
                    Repeater rpcountghostlicense = (Repeater)e.Row.Cells[4].FindControl("rpcountghostlicense");
                    Repeater rpcountuseoklicense = (Repeater)e.Row.Cells[4].FindControl("rpcountuseoklicense");
                    Repeater rpcountalllicense = (Repeater)e.Row.Cells[4].FindControl("rpcountalllicense");

                    TextBox txtcount_use = (TextBox)e.Row.Cells[4].FindControl("txtcount_use");
                    //TextBox test = (TextBox)e.Row.Cells[4].FindControl("test");
                    Label DetailLicense = (Label)e.Row.Cells[4].FindControl("DetailLicense");
                    Label t1 = (Label)e.Row.Cells[4].FindControl("t1");

                    Label detail_use = (Label)rpcountuselicense.FindControl("detail_use");
                                      

                    /// license ตามฝ่าย /////////
                    data_softwarelicense _data_softwarelicense_countdept = new data_softwarelicense();
                    _data_softwarelicense_countdept.bindu0_device_list = new bindu0_device_detail[1];

                    bindu0_device_detail _bindu0_device_detail = new bindu0_device_detail();

                    _bindu0_device_detail.software_name_idx = int.Parse(txt_software_name_idx.Text);

                    _data_softwarelicense_countdept.bindu0_device_list[0] = _bindu0_device_detail;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense_countdept));

                    _data_softwarelicense_countdept = callServiceSoftwareLicense(_urlGetCountDeptm0, _data_softwarelicense_countdept);

                    rpcountogdept.DataSource = _data_softwarelicense_countdept.bindu0_device_list;
                    rpcountogdept.DataBind();


                    //    if (_data_softwarelicense_countdept.bindu0_device_list[0].count_indept != 0)
                    //{
                    //    ViewState["Count_All"] = _data_softwarelicense_countdept.bindu0_device_list[0].count_indept;
                    //}

                    ////////////////// จำนวน License ทั้งหมด ตามฝ่าย /////////////


                    ////////////////// จำนวน License ทั้งหมด /////////////
                    data_softwarelicense _data_softwarelicense_countall = new data_softwarelicense();
                    _data_softwarelicense_countall.bindu0_device_list = new bindu0_device_detail[1];

                    bindu0_device_detail _bindu0_device_detailall = new bindu0_device_detail();

                    _bindu0_device_detailall.software_name_idx = int.Parse(txt_software_name_idx.Text);

                    _data_softwarelicense_countall.bindu0_device_list[0] = _bindu0_device_detailall;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense_countuse));

                    _data_softwarelicense_countall = callServiceSoftwareLicense(_urlGetCountAllLicense, _data_softwarelicense_countall);

                    rpcountalllicense.DataSource = _data_softwarelicense_countall.bindu0_device_list;
                    rpcountalllicense.DataBind();



                    ////////////////// จำนวน License ที่ถูกใช้ไป /////////////
                    data_softwarelicense _data_softwarelicense_countuse = new data_softwarelicense();
                    _data_softwarelicense_countuse.bindu0_device_list = new bindu0_device_detail[1];

                    bindu0_device_detail _bindu0_device_detailuse = new bindu0_device_detail();

                    _bindu0_device_detailuse.software_name_idx = int.Parse(txt_software_name_idx.Text);

                    _data_softwarelicense_countuse.bindu0_device_list[0] = _bindu0_device_detailuse;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense_countuse));

                    _data_softwarelicense_countuse = callServiceSoftwareLicense(_urlGetCountUse, _data_softwarelicense_countuse);

                    rpcountuselicense.DataSource = _data_softwarelicense_countuse.bindu0_device_list;
                    rpcountuselicense.DataBind();

                    ////////////////// จำนวน License ผี  /////////////
                    data_softwarelicense _data_softwarelicense_countghost = new data_softwarelicense();
                    _data_softwarelicense_countghost.bindu0_device_list = new bindu0_device_detail[1];

                    bindu0_device_detail _bindu0_device_detailghost = new bindu0_device_detail();

                    _bindu0_device_detailghost.software_name_idx = int.Parse(txt_software_name_idx.Text);

                    _data_softwarelicense_countghost.bindu0_device_list[0] = _bindu0_device_detailghost;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense_countuse));

                    _data_softwarelicense_countghost = callServiceSoftwareLicense(_urlGetCountGhost, _data_softwarelicense_countghost);

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense_countghost.bindu0_device_list));

                    rpcountghostlicense.DataSource = _data_softwarelicense_countghost.bindu0_device_list;
                    rpcountghostlicense.DataBind();

                    ////////////////// จำนวน License ถูกลิขสิทธิ์  /////////////
                    data_softwarelicense _data_softwarelicense_countuseok = new data_softwarelicense();
                    _data_softwarelicense_countuseok.bindu0_device_list = new bindu0_device_detail[1];

                    bindu0_device_detail _bindu0_device_detailuseok = new bindu0_device_detail();

                    _bindu0_device_detailuseok.software_name_idx = int.Parse(txt_software_name_idx.Text);

                    _data_softwarelicense_countuseok.bindu0_device_list[0] = _bindu0_device_detailuseok;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense_countuse));

                    _data_softwarelicense_countuseok = callServiceSoftwareLicense(_urlGetCountLicenseOk, _data_softwarelicense_countuseok);

                    rpcountuseoklicense.DataSource = _data_softwarelicense_countuseok.bindu0_device_list;
                    rpcountuseoklicense.DataBind();



                    //foreach (RepeaterItem rpall in rpcountuselicense.Items)
                    //{
                    //    Label detail_use 
                    //}



                    //if(_data_softwarelicense_countuse.bindu0_device_list[0].u0_use != 0)
                    //{
                    //    ViewState["Count_UseLicense"] = _data_softwarelicense_countuse.bindu0_device_list[0].u0_use;
                    //}


                    //////////////////// จำนวน License ที่ถูกใช้ไป /////////////

                    //test.Text = (int.Parse(ViewState["Count_UseLicense"].ToString()) - int.Parse(ViewState["Count_All"].ToString())).ToString();



                    ////////////////// จำนวน License คงเหลือ /////////////
                    //data_softwarelicense _data_softwarelicense_countli = new data_softwarelicense();
                    //_data_softwarelicense_countli.bindu0_device_list = new bindu0_device_detail[1];

                    //bindu0_device_detail _bindu0_device_detailcountli = new bindu0_device_detail();

                    //_bindu0_device_detailcountli.software_name_idx = int.Parse(txt_software_name_idx.Text);

                    //_data_softwarelicense_countli.bindu0_device_list[0] = _bindu0_device_detailcountli;

                    ////litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense_countuse));

                    //_data_softwarelicense_countuse = callServiceSoftwareLicense(_urlGetCountUse, _data_softwarelicense_countli);

                    //rpcountuselicense.DataSource = _data_softwarelicense_countuse.bindu0_device_list;
                    //rpcountuselicense.DataBind();
                    ////////////////// จำนวน License ที่ถูกใช้ไป /////////////




                    //if (_data_softwarelicense_countuse.bindu0_device_list[0].u0_use != 0)
                    //{
                    // ViewState["value_count_use"] = _data_softwarelicense_countuse.bindu0_device_list[0].u0_use;
                    //txtcount_use.Text = ViewState["value_count_use"].ToString();
                    //}
                    //else
                    //{
                    //    ViewState["value_count_use"] = 0;
                    //    //txtcount_use.Text = ViewState["value_count_use"].ToString();
                    //}

                    //txtcount_use.Text = ViewState["value_count_use"].ToString();

                    //if (GvMaster.EditIndex == e.Row.RowIndex) //to overlook header row
                    //{
                    //    var lbtype_nameUpdat = (Label)e.Row.FindControl("lbtype_nameUpdat");
                    //    var ddltype_nameUpdat = (DropDownList)e.Row.FindControl("ddltype_nameUpdat");

                    //    ddltype_nameUpdat.AppendDataBoundItems = true;

                    //    data_softwarelicense _data_softwarelicense_edit = new data_softwarelicense();
                    //    _data_softwarelicense_edit.softwarename_list = new softwarename_detail[1];
                    //    softwarename_detail _softwarename_detailedit = new softwarename_detail();

                    //    _data_softwarelicense_edit.softwarename_list[0] = _softwarename_detailedit;

                    //    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

                    //    _data_softwarelicense_edit = callServiceSoftwareLicense(_urlGettypeSoftwareName, _data_softwarelicense_edit);

                    //    ddltype_nameUpdat.DataSource = _data_softwarelicense_edit.softwarename_list;
                    //    ddltype_nameUpdat.DataTextField = "type_name";
                    //    ddltype_nameUpdat.DataValueField = "type_idx";
                    //    ddltype_nameUpdat.DataBind();
                    //    ddltype_nameUpdat.Items.Insert(00, new ListItem("กรุณาเลือกชื่อสถานที่ติดตั้งอุปกรณ์ ....", "00"));
                    //    ddltype_nameUpdat.SelectedValue = lbtype_nameUpdat.Text;

                    //    //ddltype_name.Items.Clear();
                    //    //ddltype_name.AppendDataBoundItems = true;
                    //    //ddltype_name.Items.Add(new ListItem("กรุณาเลือกประเภท Software ...", "00"));

                    //    //_data_softwarelicense.softwarename_list = new softwarename_detail[1];
                    //    //softwarename_detail _softwarename_detailddl = new softwarename_detail();

                    //    //_data_softwarelicense.softwarename_list[0] = _softwarename_detailddl;

                    //    //////litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense));

                    //    //_data_softwarelicense = callServiceSoftwareLicense(_urlGettypeSoftwareName, _data_softwarelicense);

                    //    //ddltype_name.DataSource = _data_softwarelicense.softwarename_list;
                    //    //ddltype_name.DataTextField = "type_name";
                    //    //ddltype_name.DataValueField = "type_idx";
                    //    //ddltype_name.DataBind();

                    //}


                }

                //////litDebug.Text = e.Row.RowState.ToString();
                ////switch (e.Row.RowState)
                ////{
                ////    case DataControlRowState.Edit:

                ////        if (e.Row.RowState.ToString().Contains("Edit"))
                ////        {

                ////            GridView editGrid = sender as GridView;
                ////            int colSpan = editGrid.Columns.Count;
                ////            for (int i = 1; i < colSpan; i++)
                ////            {
                ////                e.Row.Cells[i].Visible = false;
                ////                e.Row.Cells[i].Controls.Clear();
                ////            }
                ////            e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                ////            e.Row.Cells[0].CssClass = "";


                ////            var lbtype_nameUpdat = (Label)e.Row.FindControl("lbtype_nameUpdat");
                ////            var ddltype_nameUpdat = (DropDownList)e.Row.FindControl("ddltype_nameUpdat");

                ////            ddltype_nameUpdat.AppendDataBoundItems = true;

                ////            data_softwarelicense _data_softwarelicense_edit = new data_softwarelicense();
                ////            _data_softwarelicense_edit.softwarename_list = new softwarename_detail[1];
                ////            softwarename_detail _softwarename_detailedit = new softwarename_detail();

                ////            _data_softwarelicense_edit.softwarename_list[0] = _softwarename_detailedit;

                ////            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

                ////            _data_softwarelicense_edit = callServiceSoftwareLicense(_urlGettypeSoftwareName, _data_softwarelicense_edit);

                ////            ddltype_nameUpdat.DataSource = _data_softwarelicense_edit.softwarename_list;
                ////            ddltype_nameUpdat.DataTextField = "type_name";
                ////            ddltype_nameUpdat.DataValueField = "type_idx";
                ////            ddltype_nameUpdat.DataBind();
                ////            ddltype_nameUpdat.Items.Insert(00, new ListItem("กรุณาเลือกชื่อสถานที่ติดตั้งอุปกรณ์ ....", "00"));
                ////            ddltype_nameUpdat.SelectedValue = lbtype_nameUpdat.Text;
                ////        }
                ////        break;
                ////    case DataControlRowState.Normal:
                ////    case DataControlRowState.Alternate:
                ////        if (e.Row.RowType == DataControlRowType.DataRow)
                ////        {
                ////            //GridView GvMaster = (GridView)e.Row.Cells[4].FindControl("GvMaster");
                ////            TextBox txt_software_name_idx = (TextBox)e.Row.Cells[1].FindControl("txt_software_name_idx");
                ////            Repeater rpcountogdept = (Repeater)e.Row.Cells[4].FindControl("rpcountogdept");



                ////            data_softwarelicense _data_softwarelicense_countdept = new data_softwarelicense();
                ////            _data_softwarelicense_countdept.bindu0_device_list = new bindu0_device_detail[1];

                ////            bindu0_device_detail _bindu0_device_detail = new bindu0_device_detail();

                ////            _bindu0_device_detail.software_name_idx = int.Parse(txt_software_name_idx.Text);

                ////            _data_softwarelicense_countdept.bindu0_device_list[0] = _bindu0_device_detail;

                ////            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense_countdept));

                ////            _data_softwarelicense_countdept = callServiceSoftwareLicense(_urlGetCountDeptm0, _data_softwarelicense_countdept);

                ////            rpcountogdept.DataSource = _data_softwarelicense_countdept.bindu0_device_list;
                ////            rpcountogdept.DataBind();
                ////        }
                ////        break;
                ////}



                break;
        }
    }

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.EditIndex = e.NewEditIndex;
                actionIndex();
                break;
        }
    }

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.EditIndex = -1;
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
        }
    }

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":

                int software_name_idx_update = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var txtNameUpdate = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtNameUpdate");
                var txtversionUpdate = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtversionUpdate");
                var ddltype_nameUpdat = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddltype_nameUpdat");
                var ddlStatusUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlStatusUpdate");
             

                GvMaster.EditIndex = -1;

                _data_softwarelicense.softwarename_list = new softwarename_detail[1];
                softwarename_detail _softwarename_detail = new softwarename_detail();

                _softwarename_detail.software_name_idx = software_name_idx_update;
                _softwarename_detail.name = txtNameUpdate.Text;
                _softwarename_detail.version = txtversionUpdate.Text;
                _softwarename_detail.software_type = int.Parse(ddltype_nameUpdat.SelectedValue);
                _softwarename_detail.status = int.Parse(ddlStatusUpdate.SelectedValue);
                _softwarename_detail.cemp_idx = emp_idx;

                _data_softwarelicense.softwarename_list[0] = _softwarename_detail;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense));
                _data_softwarelicense = callServiceSoftwareLicense(_urlSetSWName, _data_softwarelicense);

                if (_data_softwarelicense.return_code == 0)
                {
                    //initPage();
                    //setDataList(dtlMenu, _dataMenu.m0_menu_list);  
                    actionIndex();


                }
                else
                {
                    setError(_data_softwarelicense.return_code.ToString() + " - " + _data_softwarelicense.return_msg);
                }


                break;
        }
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj, string sysidx_menu, string menuidx_per, string role_per)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();

        switch (fvName.ID)
        {



        }
    }

    protected void setDataList(DataList dtlName, Object obj)
    {
        dtlName.DataSource = obj;
        dtlName.DataBind();
    }

    #endregion bind data

    protected void rptRecords_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //DataRowView row = e.Item.DataItem as DataRowView;

            //Label _lbllicense_org_idxedit = e.Item.FindControl("lbllicense_org_idxedit") as Label;
            //DropDownList _ddlorg_idxedit = e.Item.FindControl("ddlorg_idxedit") as DropDownList;


            //// organization
            //data_softwarelicense _data_softwarelicenseorg11 = new data_softwarelicense();
            //_data_softwarelicenseorg11.organization_list = new organization_detail[1];

            //organization_detail _organization_detail = new organization_detail();

            //_data_softwarelicenseorg11.organization_list[0] = _organization_detail;

            ////litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_permission));
            //_data_softwarelicenseorg11 = callServiceSoftwareLicense(_urlGetddlOrganizationSW, _data_softwarelicenseorg11);

            //_ddlorg_idxedit.DataSource = _data_softwarelicenseorg11.organization_list;
            //_ddlorg_idxedit.DataTextField = "OrgNameTH";
            //_ddlorg_idxedit.DataValueField = "OrgIDX";
            ////_ddlorg_idxedit.SelectedValue = row["CategoryFK"].ToString();
            //_ddlorg_idxedit.DataBind();
            //_ddlorg_idxedit.Items.Insert(0, new ListItem("กรุณาเลือกองค์กร ....", "00"));
            //_ddlorg_idxedit.SelectedValue = _lbllicense_org_idxedit.Text;


            //// dep
            //Label _lblrdept_idxedit = e.Item.FindControl("lblrdept_idxedit") as Label;
            //DropDownList _ddlrdept_idxedit = e.Item.FindControl("ddlrdept_idxedit") as DropDownList;

            //data_softwarelicense _data_softwarelicensedept224 = new data_softwarelicense();
            //_data_softwarelicensedept224.organization_list = new organization_detail[1];
            //organization_detail _organization_detaildept224 = new organization_detail();

            ////_organization_detail.OrgIDX = int.Parse(ddlorg_idxinsert.SelectedValue);

            //_data_softwarelicensedept224.organization_list[0] = _organization_detaildept224;

            ////litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicensedept224));

            //_data_softwarelicensedept224 = callServiceSoftwareLicense(_urlGetddlDept, _data_softwarelicensedept224);


            //_ddlrdept_idxedit.DataSource = _data_softwarelicensedept224.organization_list;
            //_ddlrdept_idxedit.DataTextField = "DeptNameTH";
            //_ddlrdept_idxedit.DataValueField = "RDeptIDX";
            //_ddlrdept_idxedit.DataBind();
            //_ddlrdept_idxedit.Items.Insert(0, new ListItem("กรุณาเลือกฝ่าย ....", "00"));
            //_ddlrdept_idxedit.SelectedValue = _lblrdept_idxedit.Text;


        }


    }

    #region reuse
    protected void initPage()
    {
        clearSession();
        clearViewState();

        // divAction.Visible = true;
        // gvBookingType.Visible = true;
        // fvBookingType.Visible = false;

        // getGridData("booking_type", 20);
        //setFormData(fvMenu, FormViewMode.Insert, null, "0", "0", "0");


    }

    protected void initPagePermission()
    {
        clearSession();
        clearViewState();

        // divAction.Visible = true;
        // gvBookingType.Visible = true;
        // fvBookingType.Visible = false;

        // getGridData("booking_type", 20);
        //setFormData(fvPermission, FormViewMode.Insert, null, "0", "0", "0");


    }

    protected void setVisible()
    {
        // divAction.Visible = !divAction.Visible;
        // gvBookingType.Visible = !gvBookingType.Visible;
        // fvBookingType.Visible = !fvBookingType.Visible;
    }

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        // ViewState["listData"] = null;
    }

    protected void setError(string _errorText)
    {
        // divShowError.Visible = !divShowError.Visible;
        // litErrorCode.Text = _dataEmployee.return_code.ToString();
        if (_errorText != "")
        {
            litDebug.Text = "error : " + _errorText;
        }
        else
        {
            litDebug.Text = String.Empty;
        }
    }

    protected data_softwarelicense callServiceSoftwareLicense(string _cmdUrl, data_softwarelicense _data_softwarelicense)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_softwarelicense);
        // litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);

        // convert json to object
        _data_softwarelicense = (data_softwarelicense)_funcTool.convertJsonToObject(typeof(data_softwarelicense), _localJson);

        return _data_softwarelicense;
    }


    #endregion reuse
}