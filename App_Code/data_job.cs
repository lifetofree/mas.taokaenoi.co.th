﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

/// <summary>
/// Summary description for data_job
/// </summary>
[Serializable]
[XmlRoot("data_job")]
public class data_job
{
    [XmlElement("return_code")]
    public string return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }
    [XmlElement("return_Check")]
    public string return_Check { get; set; }
    [XmlElement("return_JenCode")]
    public string return_JenCode { get; set; }
    [XmlElement("return_AgeJenCode")]
    public string return_AgeJenCode { get; set; }


    [XmlElement("BoxAddwentposition")]
    public BoxAddwentpositionlist[] BoxAddwentposition { get; set; }

    [XmlElement("BoxSelectwantposition1")]
    public BoxSelectwantpositionList1[] BoxSelectwantposition1 { get; set; }

    [XmlElement("ddlocationstart")]
    public ddlocationstartlist[] ddlocationstart { get; set; }

    [XmlElement("ddnationality")]
    public ddnationalitylist[] ddnationality { get; set; }

    [XmlElement("ddblood")]
    public ddbloodlist[] ddblood { get; set; }

    [XmlElement("ddreligon")]
    public ddreligonlist[] ddreligon { get; set; }

    [XmlElement("ddsex")]
    public ddsexlist[] ddsex { get; set; }

    [XmlElement("ddpositionbusiness")]
    public ddpositionbusiness_list[] ddpositionbusiness { get; set; }

    [XmlElement("ddadvise")]
    public ddadvise_list[] ddadvise { get; set; }

    [XmlElement("ddAmphur")]
    public ddAmphur_list[] ddAmphur { get; set; }

    [XmlElement("ddDistrict")]
    public ddDistrict_list[] ddDistrict { get; set; }

    [XmlElement("ddPrefix")]
    public ddPrefix_list[] ddPrefix { get; set; }

    [XmlElement("ddPrefixEng")]
    public ddPrefixEng_list[] ddPrefixEng { get; set; }

    [XmlElement("ddCountry")]
    public ddCountry_list[] ddCountry { get; set; }

    [XmlElement("tbotherLanguage")]
    public tbotherLanguage_list[] tbotherLanguage { get; set; }

    [XmlElement("ddcompleted_edit")]
    public ddcompleted_edit_list[] ddcompleted_edit { get; set; }

    [XmlElement("status_speaking_other_edit")]
    public status_speaking_other_edit_list[] status_speaking_other_edit { get; set; }

    [XmlElement("decision")]
    public decision_list[] decision { get; set; }

    [XmlElement("parents")]
    public parents_list[] parents { get; set; }

    [XmlElement("education")]
    public education_list[] education { get; set; }

    [XmlElement("education_insert")]
    public education_insert_list[] education_insert { get; set; }

    [XmlElement("Employed_insert")]
    public Employed_insert_list[] Employed_insert { get; set; }

    [XmlElement("OtherLanguages_insert")]
    public OtherLanguages_insert_list[] OtherLanguages_insert { get; set; }

    [XmlElement("Languages_insert")]
    public Languages_insert_list[] Languages_insert { get; set; }

    [XmlElement("Languages_insertEng")]
    public Languages_insertEng_list[] Languages_insertEng { get; set; }

    [XmlElement("MainEmployee_insert")]
    public MainEmployee_insert_list[] MainEmployee_insert { get; set; }

    [XmlElement("ddTypesOfCar")]
    public ddTypesOfCar_list[] ddTypesOfCar { get; set; }

    [XmlElement("TypesOfCar")]
    public TypesOfCar_list[] TypesOfCar { get; set; }

    [XmlElement("Insert_Health")]
    public Insert_Health_list[] Insert_Health { get; set; }

    [XmlElement("Insert_ifMarried")]
    public Insert_ifMarried_list[] Insert_ifMarried { get; set; }

    [XmlElement("Insert_MTFT")]
    public Insert_MTFT_list[] Insert_MTFT { get; set; }

    [XmlElement("Insert_NotParent")]
    public Insert_NotParent_list[] Insert_NotParent { get; set; }

    [XmlElement("Insert_Other")]
    public Insert_Other_list[] Insert_Other { get; set; }

    [XmlElement("Insert_Talent")]
    public Insert_Talent_list[] Insert_Talent { get; set; }

    [XmlElement("Insert_FileDoc")]
    public Insert_FileDoc_list[] Insert_FileDoc { get; set; }

    [XmlElement("Out_TypeDoc")]
    public Out_TypeDoc_list[] Out_TypeDoc { get; set; }

    [XmlElement("Insert_Empbasic")]
    public Insert_Empbasic_list[] Insert_Empbasic { get; set; }

    [XmlElement("Out_Empprimary")]
    public Out_Empprimary_list[] Out_Empprimary { get; set; }

    [XmlElement("Out_ReportEmp")]
    public Out_ReportEmp_list[] Out_ReportEmp { get; set; }

    [XmlElement("Select_Faculty")]
    public Select_Faculty_list[] Select_Faculty { get; set; }

    [XmlElement("Select_Location")]
    public Select_Location_list[] Select_Location { get; set; }

    [XmlElement("Select_Department")]
    public Select_Department_list[] Select_Department { get; set; }

    [XmlElement("Select_Section")]
    public Select_Section_list[] Select_Section { get; set; }

    [XmlElement("Select_Position")]
    public Select_Position_list[] Select_Position { get; set; }

    [XmlElement("Select_Race")]
    public Select_Race_list[] Select_Race { get; set; }

    [XmlElement("Send_StatuJob")]
    public Send_StatuJob_list[] Send_StatuJob { get; set; }

    [XmlElement("Select_Row")]
    public Select_Row_list[] Select_Row { get; set; }

    [XmlElement("Insert_Genlink")]
    public Insert_Genlink_list[] Insert_Genlink { get; set; }

    [XmlElement("Data_LogicOut")]
    public Data_LogicOut_list[] Data_LogicOut { get; set; }

    [XmlElement("Data_ChoiceOut")]
    public Data_ChoiceOut_list[] Data_ChoiceOut { get; set; }

    [XmlElement("Data_ScoreLogic")]
    public Data_ScoreLogic_list[] Data_ScoreLogic { get; set; }
}

#region insert Viewsteat ธรรมดา

#region Empprimary
[Serializable]
public class Insert_Empbasic_list
{

    [XmlElement("Prefix")]
    public int Prefix { get; set; }

    [XmlElement("Name_Emp")]
    public string Name_Emp { get; set; }

    [XmlElement("Lastname_Emp")]
    public string Lastname_Emp { get; set; }

    [XmlElement("Age_Emp")]
    public int Age_Emp { get; set; }

    [XmlElement("Phone_number")]
    public string Phone_number { get; set; }

    [XmlElement("Email_Emp")]
    public string Email_Emp { get; set; }

    [XmlElement("NameINstitute")]
    public string NameINstitute { get; set; }

    [XmlElement("Education")]
    public int Education { get; set; }

    [XmlElement("Course")]
    public string Course { get; set; }

    [XmlElement("position1")]
    public int position1 { get; set; }

    [XmlElement("position2")]
    public int position2 { get; set; }

    [XmlElement("WantMoney")]
    public string WantMoney { get; set; }

}
#endregion

#region ความสามารถ
[Serializable]
public class Insert_Talent_list
{

    [XmlElement("Computer")]
    public string Computer { get; set; }

    [XmlElement("TypingTH")]
    public string TypingTH { get; set; }

    [XmlElement("TypingENG")]
    public string TypingENG { get; set; }

    [XmlElement("EmpIDX11")]
    public int EmpIDX11 { get; set; }

}
#endregion

#region ข้อมูลอื่นๆ
[Serializable]
public class Insert_Other_list
{

    [XmlElement("KnowGet")]
    public int KnowGet { get; set; }

    [XmlElement("BeenRegistered")]
    public int BeenRegistered { get; set; }

    [XmlElement("HadCaptured")]
    public int HadCaptured { get; set; }

    [XmlElement("DrugAllergy")]
    public int DrugAllergy { get; set; }

    [XmlElement("Disease")]
    public int Disease { get; set; }

    [XmlElement("HadSurgery")]
    public int HadSurgery { get; set; }

    [XmlElement("EmergencyFL")]
    public string EmergencyFL { get; set; }

    [XmlElement("RelationEmergency")]
    public string RelationEmergency { get; set; }

    [XmlElement("TelephoneEmergency")]
    public string TelephoneEmergency { get; set; }

    [XmlElement("KnowSomeone")]
    public string KnowSomeone { get; set; }

    [XmlElement("RelationKS")]
    public string RelationKS { get; set; }

    [XmlElement("EmpIDX10")]
    public int EmpIDX10 { get; set; }

    [XmlElement("tbaddresspreAccident")]
    public string tbaddresspreAccident { get; set; }
}
#endregion

#region คนรู้จักที่ติดต่อได้
[Serializable]
public class Insert_NotParent_list
{

    [XmlElement("FirstNameNP")]
    public string FirstNameNP { get; set; }

    [XmlElement("LastNameNP")]
    public string LastNameNP { get; set; }

    [XmlElement("RelationshipNP")]
    public string RelationshipNP { get; set; }

    [XmlElement("FirmAddressNP")]
    public string FirmAddressNP { get; set; }

    [XmlElement("PositionNP")]
    public string PositionNP { get; set; }

    [XmlElement("TelNP")]
    public string TelNP { get; set; }

    [XmlElement("EmpIDX9")]
    public int EmpIDX9 { get; set; }

}
#endregion

#region พ่อแม่
[Serializable]
public class Insert_MTFT_list
{

    [XmlElement("FirstNameFather")]
    public string FirstNameFather { get; set; }

    [XmlElement("LastNameFather")]
    public string LastNameFather { get; set; }

    [XmlElement("AgeFather")]
    public string AgeFather { get; set; }

    [XmlElement("OccupationFather")]
    public string OccupationFather { get; set; }

    [XmlElement("LifeStatusFather")]
    public int LifeStatusFather { get; set; }

    [XmlElement("FirstNameMother")]
    public string FirstNameMother { get; set; }

    [XmlElement("LastNameMother")]
    public string LastNameMother { get; set; }

    [XmlElement("AgeMother")]
    public string AgeMother { get; set; }

    [XmlElement("OccupationMother")]
    public string OccupationMother { get; set; }

    [XmlElement("LifeStatusMother")]
    public int LifeStatusMother { get; set; }

    [XmlElement("EmpIDX8")]
    public int EmpIDX8 { get; set; }

}
#endregion

#region Main Employee
[Serializable]
public class MainEmployee_insert_list
{
    [XmlElement("EmpIDXIm")]
    public int EmpIDXIm { get; set; }

    [XmlElement("EmpIDX")]
    public int EmpIDX { get; set; }

    [XmlElement("SexIDX")]
    public int SexIDX { get; set; }

    [XmlElement("SexNameTH")]
    public string SexNameTH { get; set; }

    [XmlElement("PrefixIDX")]
    public int PrefixIDX { get; set; }

    [XmlElement("PrefixNameTH")]
    public string PrefixNameTH { get; set; }

    [XmlElement("PrefixEngIDX")]
    public int PrefixEngIDX { get; set; }

    [XmlElement("PrefixNameEN")]
    public string PrefixNameEN { get; set; }

    [XmlElement("FirstNameTH")]
    public string FirstNameTH { get; set; }

    [XmlElement("LastNameTH")]
    public string LastNameTH { get; set; }

    [XmlElement("NickNameTH")]
    public string NickNameTH { get; set; }

    [XmlElement("FirstNameEN")]
    public string FirstNameEN { get; set; }

    [XmlElement("LastNameEN")]
    public string LastNameEN { get; set; }

    [XmlElement("NickNameEN")]
    public string NickNameEN { get; set; }

    [XmlElement("PhoneNo")]
    public string PhoneNo { get; set; }

    [XmlElement("MobileNo")]
    public string MobileNo { get; set; }

    [XmlElement("Email")]
    public string Email { get; set; }

    [XmlElement("Birthday")]
    public string Birthday { get; set; }

    [XmlElement("NatIDX")]
    public int NatIDX { get; set; }

    [XmlElement("NatName")]
    public string NatName { get; set; }

    [XmlElement("RaceIDX")]
    public int RaceIDX { get; set; }

    [XmlElement("RaceTxt")]
    public string RaceTxt { get; set; }

    [XmlElement("RelIDX")]
    public int RelIDX { get; set; }

    [XmlElement("RelNameTH")]
    public string RelNameTH { get; set; }

    [XmlElement("EmpAddress")]
    public string EmpAddress { get; set; }

    [XmlElement("DistIDX")]
    public int DistIDX { get; set; }

    [XmlElement("DistName")]
    public string DistName { get; set; }

    [XmlElement("AmpIDX")]
    public int AmpIDX { get; set; }

    [XmlElement("AmpName")]
    public string AmpName { get; set; }

    [XmlElement("ProvIDX")]
    public int ProvIDX { get; set; }

    [XmlElement("ProvName")]
    public string ProvName { get; set; }

    [XmlElement("PostCode")]
    public string PostCode { get; set; }

    [XmlElement("CountryIDX")]
    public int CountryIDX { get; set; }

    [XmlElement("CountryName")]
    public string CountryName { get; set; }

    [XmlElement("MarriedStatus")]
    public int MarriedStatus { get; set; }

    [XmlElement("NameTH")]
    public string NameTH { get; set; }

    [XmlElement("InhabitStatus")]
    public int InhabitStatus { get; set; }

    [XmlElement("LivingStaTxt")]
    public string LivingStaTxt { get; set; }

    [XmlElement("EmpCreate")]
    public string EmpCreate { get; set; }

    [XmlElement("EmpUpdate")]
    public string EmpUpdate { get; set; }

    [XmlElement("ProvBirthIDX")]
    public int ProvBirthIDX { get; set; }

    [XmlElement("CardID")]
    public string CardID { get; set; }

    [XmlElement("IssuedAtCardID")]
    public string IssuedAtCardID { get; set; }

    [XmlElement("IssuedDateCardID")]
    public string IssuedDateCardID { get; set; }

    [XmlElement("ExpiredDateCardID")]
    public string ExpiredDateCardID { get; set; }

    [XmlElement("SecurrityCard")]
    public string SecurrityCard { get; set; }

    [XmlElement("IssuedAtSecurrity")]
    public string IssuedAtSecurrity { get; set; }

    [XmlElement("ExpiredDateSecurrity")]
    public string ExpiredDateSecurrity { get; set; }

    [XmlElement("TaxCardID")]
    public string TaxCardID { get; set; }

    [XmlElement("IssuedAtTax")]
    public string IssuedAtTax { get; set; }

    [XmlElement("PresentAddress")]
    public string PresentAddress { get; set; }

    [XmlElement("MilitaryService")]
    public int MilitaryService { get; set; }

    [XmlElement("Children")]
    public string Children { get; set; }

    [XmlElement("ChildrenSchool")]
    public string ChildrenSchool { get; set; }

    [XmlElement("Children21")]
    public string Children21 { get; set; }

}
#endregion

#region ตำแหน่งที่ต้องการ
[Serializable]
public class BoxAddwentpositionlist
{
    [XmlElement("id")]
    public int id { get; set; }

    [XmlElement("tbposition1")]
    public int tbposition1 { get; set; }

    [XmlElement("tbposition2")]
    public int tbposition2 { get; set; }

    [XmlElement("tbmoney")]
    public string tbmoney { get; set; }

    [XmlElement("AddStartdate")]
    public string AddStartdate { get; set; }

    [XmlElement("EmpIDX0")]
    public int EmpIDX0 { get; set; }

}
#endregion

#region ร่างกาย
[Serializable]
public class Insert_Health_list
{

    [XmlElement("Height")]
    public float Height { get; set; }

    [XmlElement("Weight")]
    public float Weight { get; set; }

    [XmlElement("Scar")]
    public string Scar { get; set; }

    [XmlElement("BloodIDX")]
    public int BloodIDX { get; set; }

    [XmlElement("EmpIDX6")]
    public int EmpIDX6 { get; set; }

}
#endregion

#region ถ้าแต่งงาน
[Serializable]
public class Insert_ifMarried_list
{

    [XmlElement("SpouseMN")]
    public int SpouseMN { get; set; }

    [XmlElement("FirstNameSpouse")]
    public string FirstNameSpouse { get; set; }

    [XmlElement("LastNameSpouse")]
    public string LastNameSpouse { get; set; }

    [XmlElement("OccupationSpouse")]
    public string OccupationSpouse { get; set; }

    [XmlElement("FirmAddressS")]
    public string FirmAddressS { get; set; }

    [XmlElement("RegisterM")]
    public int RegisterM { get; set; }

    [XmlElement("EmpIDX7")]
    public int EmpIDX7 { get; set; }

}
#endregion

#endregion

#region Updateคะแนน Logic
[Serializable]
public class Data_ScoreLogic_list
{
    [XmlElement("Profile_IDXLogic")]
    public int Profile_IDXLogic { get; set; }

    [XmlElement("Score")]
    public int Score { get; set; }
}
#endregion

#region Choice
[Serializable]
public class Data_ChoiceOut_list
{
    [XmlElement("Logic_IDX")]
    public int Logic_IDX { get; set; }

    [XmlElement("name_Choice")]
    public string name_Choice { get; set; }

    [XmlElement("status_true")]
    public int status_true { get; set; }
}
#endregion

#region RandomLogic
[Serializable]
public class Data_LogicOut_list
{
    [XmlElement("Logic_Send")]
    public int Logic_Send { get; set; }

    [XmlElement("name_Logic")]
    public string name_Logic { get; set; }
}
#endregion

#region เก็บ Genlink
[Serializable]
public class Insert_Genlink_list
{
    [XmlElement("EmpPimary_idx")]
    public int EmpPimary_idx { get; set; }

    [XmlElement("JenCode")]
    public string JenCode { get; set; }

    [XmlElement("status_verify")]
    public int status_verify { get; set; }
}
#endregion

#region เปลี่ยน status job
[Serializable]
public class Select_Row_list
{

    [XmlElement("His_Row")]
    public int His_Row { get; set; }

    [XmlElement("EmpIDXSendMain2")]
    public int EmpIDXSendMain2 { get; set; }
}

[Serializable]
public class Send_StatuJob_list
{

    [XmlElement("idEmpMain")]
    public int idEmpMain { get; set; }

}
#endregion

#region เรียกข้อมูลเบื้องต้น
[Serializable]
public class Out_Empprimary_list //ddAmphur_list
{
    [XmlElement("Position_Search")]
    public int Position_Search { get; set; }

    [XmlElement("Age_Search")]
    public int Age_Search { get; set; }

    [XmlElement("Date_Search")]
    public string Date_Search { get; set; }

    [XmlElement("EmpPrimary_IDX")]
    public int EmpPrimary_IDX { get; set; }

    [XmlElement("EmpPrimary_IDX_send")]
    public int EmpPrimary_IDX_send { get; set; }

    [XmlElement("Name_Emp")]
    public string Name_Emp { get; set; }

    [XmlElement("PrefixNameTH")]
    public string PrefixNameTH { get; set; }

    [XmlElement("Lastname_Emp")]
    public string Lastname_Emp { get; set; }

    [XmlElement("Age_Emp")]
    public int Age_Emp { get; set; }

    [XmlElement("Phone_number")]
    public string Phone_number { get; set; }

    [XmlElement("Email")]
    public string Email { get; set; }

    [XmlElement("NameINstitute")]
    public string NameINstitute { get; set; }

    [XmlElement("Education")]
    public int Education { get; set; }

    [XmlElement("Edu_name")]
    public string Edu_name { get; set; }

    [XmlElement("Course")]
    public string Course { get; set; }

    [XmlElement("position1")]
    public int position1 { get; set; }

    [XmlElement("PosGroupNameEN")]
    public string PosGroupNameEN { get; set; }

    [XmlElement("position2")]
    public int position2 { get; set; }

    [XmlElement("PosGroupNameTH")]
    public string PosGroupNameTH { get; set; }

    [XmlElement("WantMoney")]
    public string WantMoney { get; set; }

    [XmlElement("Prefix")]
    public int Prefix { get; set; }

    [XmlElement("Date_Emp")]
    public string Date_Emp { get; set; }

    [XmlElement("IFSearchbetween")]
    public int IFSearchbetween { get; set; }

    [XmlElement("DatecloseJob")]
    public string DatecloseJob { get; set; }

    [XmlElement("Status_EmpPrimary")]
    public int Status_EmpPrimary { get; set; }
}
#endregion

#region Report ข้อมูลทั้งหมด
[Serializable]
public class Out_ReportEmp_list //ddAmphur_list
{
    [XmlElement("Score_logic")]
    public int Score_logic { get; set; }

    [XmlElement("Position_SearchM")]
    public int Position_SearchM { get; set; }

    [XmlElement("StatusEmpMain")]
    public int StatusEmpMain { get; set; }

    [XmlElement("Age_SearchM")]
    public int Age_SearchM { get; set; }

    [XmlElement("Date_SearchM")]
    public string Date_SearchM { get; set; }

    [XmlElement("EmpIDXSendMain")]
    public int EmpIDXSendMain { get; set; }

    [XmlElement("EmpIDX")]
    public int EmpIDX { get; set; }

    [XmlElement("SexIDX")]
    public int SexIDX { get; set; }

    [XmlElement("SexNameTH")]
    public string SexNameTH { get; set; }

    [XmlElement("PrefixIDX")]
    public int PrefixIDX { get; set; }

    [XmlElement("PrefixNameTH")]
    public string PrefixNameTH { get; set; }

    [XmlElement("PrefixEngIDX")]
    public int PrefixEngIDX { get; set; }

    [XmlElement("PrefixNameEN")]
    public string PrefixNameEN { get; set; }

    [XmlElement("FirstNameTH")]
    public string FirstNameTH { get; set; }

    [XmlElement("LastNameTH")]
    public string LastNameTH { get; set; }

    [XmlElement("NickNameTH")]
    public string NickNameTH { get; set; }

    [XmlElement("FirstNameEN")]
    public string FirstNameEN { get; set; }

    [XmlElement("LastNameEN")]
    public string LastNameEN { get; set; }

    [XmlElement("NickNameEN")]
    public string NickNameEN { get; set; }

    [XmlElement("PhoneNo")]
    public string PhoneNo { get; set; }

    [XmlElement("MobileNo")]
    public string MobileNo { get; set; }

    [XmlElement("Email")]
    public string Email { get; set; }

    [XmlElement("Birthday")]
    public string Birthday { get; set; }

    [XmlElement("NatIDX")]
    public int NatIDX { get; set; }

    [XmlElement("NatName")]
    public string NatName { get; set; }

    [XmlElement("RaceIDX")]
    public int RaceIDX { get; set; }

    [XmlElement("RaceTxt")]
    public string RaceTxt { get; set; }

    [XmlElement("RelIDX")]
    public int RelIDX { get; set; }

    [XmlElement("RelNameTH")]
    public string RelNameTH { get; set; }

    [XmlElement("EmpAddress")]
    public string EmpAddress { get; set; }

    [XmlElement("DistIDX")]
    public int DistIDX { get; set; }

    [XmlElement("DistName")]
    public string DistName { get; set; }

    [XmlElement("AmpIDX")]
    public int AmpIDX { get; set; }

    [XmlElement("AmpName")]
    public string AmpName { get; set; }

    [XmlElement("ProvIDX")]
    public int ProvIDX { get; set; }

    [XmlElement("ProvName")]
    public string ProvName { get; set; }

    [XmlElement("PostCode")]
    public string PostCode { get; set; }

    [XmlElement("CountryIDX")]
    public int CountryIDX { get; set; }

    [XmlElement("CountryName")]
    public string CountryName { get; set; }

    [XmlElement("MarriedStatus")]
    public int MarriedStatus { get; set; }

    [XmlElement("NameTH")]
    public string NameTH { get; set; }

    [XmlElement("InhabitStatus")]
    public int InhabitStatus { get; set; }

    [XmlElement("LivingStaTxt")]
    public string LivingStaTxt { get; set; }

    [XmlElement("EmpCreate")]
    public string EmpCreate { get; set; }

    [XmlElement("EmpUpdate")]
    public string EmpUpdate { get; set; }

    [XmlElement("ProvBirthIDX")]
    public int ProvBirthIDX { get; set; }

    [XmlElement("ProvBirthTxt")]
    public string ProvBirthTxt { get; set; }

    [XmlElement("CardID")]
    public string CardID { get; set; }

    [XmlElement("IssuedAtCardID")]
    public string IssuedAtCardID { get; set; }

    [XmlElement("IssuedDateCardID")]
    public string IssuedDateCardID { get; set; }

    [XmlElement("ExpiredDateCardID")]
    public string ExpiredDateCardID { get; set; }

    [XmlElement("SecurrityCard")]
    public string SecurrityCard { get; set; }

    [XmlElement("IssuedAtSecurrity")]
    public string IssuedAtSecurrity { get; set; }

    [XmlElement("ExpiredDateSecurrity")]
    public string ExpiredDateSecurrity { get; set; }

    [XmlElement("TaxCardID")]
    public string TaxCardID { get; set; }

    [XmlElement("IssuedAtTax")]
    public string IssuedAtTax { get; set; }

    [XmlElement("PresentAddress")]
    public string PresentAddress { get; set; }

    [XmlElement("MilitaryService")]
    public int MilitaryService { get; set; }

    [XmlElement("MilName")]
    public string MilName { get; set; }

    [XmlElement("Children")]
    public string Children { get; set; }

    [XmlElement("ChildrenSchool")]
    public string ChildrenSchool { get; set; }

    [XmlElement("Children21")]
    public string Children21 { get; set; }

    [XmlElement("Age")]
    public int Age { get; set; }

    [XmlElement("PosGroupNameEN")]
    public string PosGroupNameEN { get; set; }

    [XmlElement("PosGroupNameTH")]
    public string PosGroupNameTH { get; set; }

    [XmlElement("Status_")]
    public int Status_ { get; set; }

    [XmlElement("MoneyWant")]
    public string MoneyWant { get; set; }

    [XmlElement("AddStartdate")]
    public string AddStartdate { get; set; }

    [XmlElement("Height")]
    public float Height { get; set; }

    [XmlElement("Weight")]
    public float Weight { get; set; }

    [XmlElement("Scar")]
    public string Scar { get; set; }

    [XmlElement("BloodIDX")]
    public int BloodIDX { get; set; }

    [XmlElement("BGName")]
    public string BGName { get; set; }

    [XmlElement("FirstNameFather")]
    public string FirstNameFather { get; set; }

    [XmlElement("LastNameFather")]
    public string LastNameFather { get; set; }

    [XmlElement("AgeFather")]
    public string AgeFather { get; set; }

    [XmlElement("OccupationFather")]
    public string OccupationFather { get; set; }

    [XmlElement("ALIVEName")]
    public string ALIVEName { get; set; }

    [XmlElement("FirstNameMother")]
    public string FirstNameMother { get; set; }

    [XmlElement("LastNameMother")]
    public string LastNameMother { get; set; }

    [XmlElement("AgeMother")]
    public string AgeMother { get; set; }

    [XmlElement("OccupationMother")]
    public string OccupationMother { get; set; }

    [XmlElement("ALIVEName_Mother")]
    public string ALIVEName_Mother { get; set; }

    [XmlElement("FirstNameParent")]
    public string FirstNameParent { get; set; }

    [XmlElement("LastNameParent")]
    public string LastNameParent { get; set; }

    [XmlElement("AgeParent")]
    public string AgeParent { get; set; }

    [XmlElement("OccupationParent")]
    public string OccupationParent { get; set; }

    [XmlElement("FirmAddressParent")]
    public string FirmAddressParent { get; set; }

    [XmlElement("NumberParent")]
    public string NumberParent { get; set; }

    [XmlElement("Institute")]
    public string Institute { get; set; }

    [XmlElement("Edu_name")]
    public string Edu_name { get; set; }

    [XmlElement("Education")]
    public string Education { get; set; }

    [XmlElement("YearAttendedStart")]
    public string YearAttendedStart { get; set; }

    [XmlElement("ToAttended")]
    public string ToAttended { get; set; }

    [XmlElement("ProvEducation")]
    public string ProvEducation { get; set; }

    [XmlElement("Course")]
    public string Course { get; set; }

    [XmlElement("CompletedEdu")]
    public string CompletedEdu { get; set; }

    [XmlElement("NameEmployed")]
    public string NameEmployed { get; set; }

    [XmlElement("TelBusiness")]
    public string TelBusiness { get; set; }

    [XmlElement("TypeBusiness")]
    public string TypeBusiness { get; set; }

    [XmlElement("PositionBusiness")]
    public string PositionBusiness { get; set; }

    [XmlElement("AddressBusiness")]
    public string AddressBusiness { get; set; }

    [XmlElement("YearAttendedStartEmp")]
    public string YearAttendedStartEmp { get; set; }

    [XmlElement("ToAttendedEmp")]
    public string ToAttendedEmp { get; set; }

    [XmlElement("Supervisor")]
    public string Supervisor { get; set; }

    [XmlElement("SalaryEmp")]
    public string SalaryEmp { get; set; }

    [XmlElement("Revenue")]
    public string Revenue { get; set; }

    [XmlElement("WorkEmp")]
    public string WorkEmp { get; set; }

    [XmlElement("Computer")]
    public string Computer { get; set; }

    [XmlElement("TypingTH")]
    public string TypingTH { get; set; }

    [XmlElement("TypingENG")]
    public string TypingENG { get; set; }

    [XmlElement("NameEN")]
    public string NameEN { get; set; }

    [XmlElement("PROFICName")]
    public string PROFICName { get; set; }

    [XmlElement("ProficiencyUnder_txt")]
    public string ProficiencyUnder_txt { get; set; }

    [XmlElement("ProficiencyRead_txt")]
    public string ProficiencyRead_txt { get; set; }

    [XmlElement("ProficiencyWrint_txt")]
    public string ProficiencyWrint_txt { get; set; }

    [XmlElement("DriveLicense")]
    public string DriveLicense { get; set; }

    [XmlElement("Name")]
    public string Name { get; set; }

    [XmlElement("FirstNameNP")]
    public string FirstNameNP { get; set; }

    [XmlElement("LastNameNP")]
    public string LastNameNP { get; set; }

    [XmlElement("RelationshipNP")]
    public string RelationshipNP { get; set; }

    [XmlElement("FirmAddressNP")]
    public string FirmAddressNP { get; set; }

    [XmlElement("PositionNP")]
    public string PositionNP { get; set; }

    [XmlElement("TelNP")]
    public string TelNP { get; set; }

    [XmlElement("ADVISEName")]
    public string ADVISEName { get; set; }

    [XmlElement("DECIName")]
    public string DECIName { get; set; }

    [XmlElement("HadCaptured_txt")]
    public string HadCaptured_txt { get; set; }

    [XmlElement("DrugAllergy_txt")]
    public string DrugAllergy_txt { get; set; }

    [XmlElement("Disease_txt")]
    public string Disease_txt { get; set; }

    [XmlElement("HadSurgery_txt")]
    public string HadSurgery_txt { get; set; }

    [XmlElement("EmergencyFL")]
    public string EmergencyFL { get; set; }

    [XmlElement("RelationEmergency")]
    public string RelationEmergency { get; set; }

    [XmlElement("TelephoneEmergency")]
    public string TelephoneEmergency { get; set; }

    [XmlElement("KnowSomeone")]
    public string KnowSomeone { get; set; }

    [XmlElement("RelationKS")]
    public string RelationKS { get; set; }

    [XmlElement("AddressEmergency")]
    public string AddressEmergency { get; set; }

    [XmlElement("Grade")]
    public string Grade { get; set; }

    [XmlElement("Faculty_Name")]
    public string Faculty_Name { get; set; }

    [XmlElement("Faculty")]
    public int Faculty { get; set; }

    [XmlElement("Resignation")]
    public string Resignation { get; set; }

    [XmlElement("IFSearchbetweenM")]
    public int IFSearchbetweenM { get; set; }

    [XmlElement("DatecloseJobM")]
    public string DatecloseJobM { get; set; }
}
#endregion

#region เรียกข้อมูล FileDoc
[Serializable]
public class Out_TypeDoc_list //ddAmphur_list
{
    [XmlElement("Doc_IDX")]
    public int Doc_IDX { get; set; }

    [XmlElement("Name_Doc")]
    public string Name_Doc { get; set; }
}

[Serializable]
public class Insert_FileDoc_list //ddAmphur_list
{
    [XmlElement("File_IDX")]
    public int File_IDX { get; set; }

    [XmlElement("EmpIDX")]
    public int EmpIDX { get; set; }

    [XmlElement("EmpIDXFile")]
    public int EmpIDXFile { get; set; }

    [XmlElement("Name_Doc")]
    public string Name_Doc { get; set; }

    [XmlElement("NameDoc")]
    public string NameDoc { get; set; }
}
#endregion

#region เรียก master มาใน dropdown
[Serializable]
public class Select_Race_list //ddAmphur_list
{
    [XmlElement("RaceIDX")]
    public int RaceIDX { get; set; }
    
    [XmlElement("RaceName")]
    public string RaceName { get; set; }
}

[Serializable]
public class Select_Position_list //ddAmphur_list
{
    [XmlElement("RPosIDX")]
    public int RPosIDX { get; set; }

    [XmlElement("RPosIDX_")]
    public int RPosIDX_ { get; set; }

    [XmlElement("RDepart2_")]
    public int RDepart2_ { get; set; }

    [XmlElement("PosNameTH")]
    public string PosNameTH { get; set; }
}

[Serializable]
public class Select_Section_list //ddAmphur_list
{
    [XmlElement("RSecIDX")]
    public int RSecIDX { get; set; }

    [XmlElement("SecIDX_")]
    public int SecIDX_ { get; set; }

    [XmlElement("SecNameTH")]
    public string SecNameTH { get; set; }
}

[Serializable]
public class Select_Department_list //ddAmphur_list
{
    [XmlElement("RDeptIDX")]
    public int RDeptIDX { get; set; }

    [XmlElement("DeptIDX_")]
    public int DeptIDX_ { get; set; }

    [XmlElement("DeptNameTH")]
    public string DeptNameTH { get; set; }
}

[Serializable]
public class Select_Location_list //ddAmphur_list
{
    [XmlElement("OrgIDX")]
    public int OrgIDX { get; set; }

    [XmlElement("OrgNameTH")]
    public string OrgNameTH { get; set; }
}

[Serializable]
public class Select_Faculty_list //ddAmphur_list
{
    [XmlElement("FacIDX")]
    public int FacIDX { get; set; }

    [XmlElement("Faculty_Name")]
    public string Faculty_Name { get; set; }
}


[Serializable]
public class ddTypesOfCar_list //ddAmphur_list
{
    [XmlElement("DLIDX")]
    public int DLIDX { get; set; }

    [XmlElement("Name")]
    public string Name { get; set; }
}

[Serializable]
public class ddcompleted_edit_list //ddAmphur_list
{
    [XmlElement("Eduidx")]
    public int Eduidx { get; set; }

    [XmlElement("Edu_name")]
    public string Edu_name { get; set; }
}

[Serializable]
public class tbotherLanguage_list //ddAmphur_list
{
    [XmlElement("LanIDX")]
    public int LanIDX { get; set; }

    [XmlElement("NameEN")]
    public string NameEN { get; set; }
}

[Serializable]
public class status_speaking_other_edit_list //ddAmphur_list
{
    [XmlElement("PROFICIDX")]
    public int PROFICIDX { get; set; }

    [XmlElement("PROFICName")]
    public string PROFICName { get; set; }
}

[Serializable]
public class ddCountry_list //ddAmphur_list
{
    [XmlElement("CountryIDX")]
    public int CountryIDX { get; set; }

    [XmlElement("CountryName")]
    public string CountryName { get; set; }
}

[Serializable]
public class ddPrefixEng_list //ddAmphur_list
{
    [XmlElement("PrefixIDX")]
    public int PrefixIDX { get; set; }

    [XmlElement("PrefixNameEN")]
    public string PrefixNameEN { get; set; }
}

[Serializable]
public class ddPrefix_list //ddAmphur_list
{
    [XmlElement("PrefixIDX")]
    public int PrefixIDX { get; set; }

    [XmlElement("PrefixNameTH")]
    public string PrefixNameTH { get; set; }
}

[Serializable]
public class ddDistrict_list //ddAmphur_list
{
    [XmlElement("DistIDX")]
    public int DistIDX { get; set; }

    [XmlElement("AmpIDX_")]
    public int AmpIDX_ { get; set; }

    [XmlElement("DistName")]
    public string DistName { get; set; }
}

[Serializable]
public class ddAmphur_list //ddAmphur_list
{
    [XmlElement("AmpIDX")]
    public int AmpIDX { get; set; }

    [XmlElement("ProvIDX_")]
    public int ProvIDX_ { get; set; }

    [XmlElement("AmpName")]
    public string AmpName { get; set; }
}

[Serializable]
public class decision_list //decision
{
    [XmlElement("DECIIDX")]
    public int DECIIDX { get; set; }

    [XmlElement("DECIName")]
    public string DECIName { get; set; }
}

[Serializable]
public class ddadvise_list //Advise
{
    [XmlElement("ADVISEIDX")]
    public int ADVISEIDX { get; set; }

    [XmlElement("ADVISEName")]
    public string ADVISEName { get; set; }
}

[Serializable]
public class ddpositionbusiness_list //Employed
{
    [XmlElement("PosGroupIDX")]
    public int PosGroupIDX { get; set; }

    [XmlElement("PosGroupNameTH")]
    public string PosGroupNameTH { get; set; }
}

[Serializable]
public class education_list //education
{
    [XmlElement("Eduidx")]
    public int Eduidx { get; set; }

    [XmlElement("Edu_name")]
    public string Edu_name { get; set; }
}

[Serializable]
public class BoxSelectwantpositionList1 //position
{
    [XmlElement("PosGroupIDX")]
    public int PosGroupIDX { get; set; }

    [XmlElement("PosGroupNameTH")]
    public string PosGroupNameTH { get; set; }
}

[Serializable]
public class ddlocationstartlist //จังหวัด
{
    [XmlElement("ProvIDX")]
    public int ProvIDX { get; set; }

    [XmlElement("ProvName")]
    public string ProvName { get; set; }
}

[Serializable]
public class ddnationalitylist //จังหวัด
{
    [XmlElement("NatIDX")]
    public int NatIDX { get; set; }

    [XmlElement("NatName")]
    public string NatName { get; set; }
}

[Serializable]
public class ddbloodlist //กรุ๊ปเลือด
{
    [XmlElement("BGIDX")]
    public int BGIDX { get; set; }

    [XmlElement("BGName")]
    public string BGName { get; set; }
}

[Serializable]
public class ddreligonlist //ศาสนา
{
    [XmlElement("RelIDX")]
    public int RelIDX { get; set; }

    [XmlElement("RelNameTH")]
    public string RelNameTH { get; set; }
}

[Serializable]
public class ddsexlist //เพศ
{
    [XmlElement("SexIDX")]
    public int SexIDX { get; set; }

    [XmlElement("SexNameTH")]
    public string SexNameTH { get; set; }
}


#endregion

#region insert tabel จำลอง

    #region insert ประเภทรถที่ขับขี่ได้
[Serializable]
public class TypesOfCar_list
{
    [XmlElement("ddTypesOfCar")]
    public string ddTypesOfCar { get; set; }

    [XmlElement("tbdriving")]
    public string tbdriving { get; set; }

    [XmlElement("EmpIDX5")]
    public int EmpIDX5 { get; set; }
}
    #endregion

    #region insert ภาษาอื่นๆ
[Serializable]
public class OtherLanguages_insert_list
{
    [XmlElement("tbotherLanguage")]
    public int tbotherLanguage { get; set; }

    [XmlElement("status_speaking_other")]
    public int status_speaking_other { get; set; }

    [XmlElement("status_Understanding_other")]
    public int status_Understanding_other { get; set; }

    [XmlElement("status_Reading_other")]
    public int status_Reading_other { get; set; }

    [XmlElement("status_Wrinting_other")]
    public int status_Wrinting_other { get; set; }

    [XmlElement("EmpIDX4")]
    public int EmpIDX4 { get; set; }
}

[Serializable]
public class Languages_insert_list
{
    [XmlElement("tbotherLanguageTE")]
    public string tbotherLanguageTE { get; set; }

    [XmlElement("status_speaking_otherTE")]
    public int status_speaking_otherTE { get; set; }

    [XmlElement("status_Understanding_otherTE")]
    public int status_Understanding_otherTE { get; set; }

    [XmlElement("status_Reading_otherTE")]
    public int status_Reading_otherTE { get; set; }

    [XmlElement("status_Wrinting_otherTE")]
    public int status_Wrinting_otherTE { get; set; }

    [XmlElement("EmpIDXTE")]
    public int EmpIDXTE { get; set; }
}

[Serializable]
public class Languages_insertEng_list
{
    [XmlElement("tbotherLanguageEng")]
    public string tbotherLanguageEng { get; set; }

    [XmlElement("status_speaking_otherEng")]
    public int status_speaking_otherEng { get; set; }

    [XmlElement("status_Understanding_otherEng")]
    public int status_Understanding_otherEng { get; set; }

    [XmlElement("status_Reading_otherEng")]
    public int status_Reading_otherEng { get; set; }

    [XmlElement("status_Wrinting_otherEng")]
    public int status_Wrinting_otherEng { get; set; }

    [XmlElement("EmpIDXEng")]
    public int EmpIDXEng { get; set; }
}
#endregion //

    #region insert ประวัติการทำงาน
[Serializable]
public class Employed_insert_list
{
    [XmlElement("tbnameemployed")]
    public string tbnameemployed { get; set; }

    [XmlElement("tbtelbusiness")]
    public string tbtelbusiness { get; set; }

    [XmlElement("tbtypeBusiness")]
    public string tbtypeBusiness { get; set; }

    [XmlElement("tbpositionbusiness")]
    public string tbpositionbusiness { get; set; }

    [XmlElement("tbaddressbusiness")]
    public string tbaddressbusiness { get; set; }

    [XmlElement("Addyearattendedstart_emp")]
    public string Addyearattendedstart_emp { get; set; }

    [XmlElement("tbtoattended_emp")]
    public string tbtoattended_emp { get; set; }

    [XmlElement("tbsupervisor")]
    public string tbsupervisor { get; set; }

    [XmlElement("tbsalary_emp")]
    public int tbsalary_emp { get; set; }

    [XmlElement("tbrevenue")]
    public int tbrevenue { get; set; }

    [XmlElement("tbwork")]
    public string tbwork { get; set; }

    [XmlElement("EmpIDX3")]
    public int EmpIDX3 { get; set; }

    [XmlElement("tbResignation")] //
    public string tbResignation { get; set; }
}
#endregion

    #region insert การศึกษา
[Serializable]
public class education_insert_list
{
    [XmlElement("tbinstitute")]
    public string tbinstitute { get; set; }

    [XmlElement("ddeducation")]
    public int ddeducation { get; set; }

    [XmlElement("ddprovinceeducation")]
    public int ddprovinceeducation { get; set; }

    [XmlElement("Addyearattendedstart")]
    public string Addyearattendedstart { get; set; }

    [XmlElement("tbtoattended")]
    public string tbtoattended { get; set; }

    [XmlElement("tbcourse")]
    public string tbcourse { get; set; }

    [XmlElement("ddcompleted")]
    public int ddcompleted { get; set; }

    [XmlElement("EmpIDX2")]
    public int EmpIDX2 { get; set; }

    [XmlElement("ddFaculty")] //
    public int ddFaculty { get; set; }

    [XmlElement("tbGpa")] //
    public string tbGpa { get; set; }
}
#endregion

    #region insert ญาติพี่น้อง
[Serializable]
public class parents_list
{
    [XmlElement("tbnameparent1")]
    public string tbnameparent1 { get; set; }

    [XmlElement("tblastnameparent1")]
    public string tblastnameparent1 { get; set; }

    [XmlElement("ddageparent1")]
    public string ddageparent1 { get; set; }

    [XmlElement("tboccupationparent1")]
    public string tboccupationparent1 { get; set; }

    [XmlElement("tbfirmaddressparent1")]
    public string tbfirmaddressparent1 { get; set; }

    [XmlElement("tbnumberparent1")]
    public string tbnumberparent1 { get; set; }

    [XmlElement("EmpIDX1")]
    public int EmpIDX1 { get; set; }
}
#endregion

#endregion
