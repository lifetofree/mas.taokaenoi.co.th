﻿<%@ WebHandler Language="C#" Class="Handler" %>

using System;
using System.Web;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Globalization;
using System.Threading;

public class Handler : IHttpHandler
{
   public void ProcessRequest(HttpContext context)
   {
      string callback = context.Request.QueryString["callback"];
      int announceGroupDiaryIdx = 0;
      int nodeIdx = 0;
      int approveLogTypes = 0;
      int creator = 0;
      int.TryParse(context.Request.QueryString["announceGroupDiaryIdx"], out announceGroupDiaryIdx);
      int.TryParse(context.Request.QueryString["nodeIdx"], out nodeIdx);
      int.TryParse(context.Request.QueryString["approveLogTypes"], out approveLogTypes);
      int.TryParse(context.Request.QueryString["creator"], out creator);
      string json = this.createAnnounceLog(announceGroupDiaryIdx, nodeIdx, approveLogTypes, creator);
      if (!string.IsNullOrEmpty(callback))
      {
         json = string.Format("{0}({1});", callback, json);
      }
      context.Response.ContentType = "text/json";
      context.Response.Write(json);
   }

   private string createAnnounceLog(int announceGroupDiaryIdx, int nodeIdx, int approveLogTypes, int creator)
   {
      List<object> emps = new List<object>();
      using (SqlConnection conn = new SqlConnection())
      {
         conn.ConnectionString = ConfigurationManager.ConnectionStrings["conn_mas"].ConnectionString;
         using (SqlCommand cmd = new SqlCommand())
         {
            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;
            var now = DateTime.Now.ToString("yyyy-MM-dd H:m:s");
            cmd.Connection = conn;
            conn.Open();
            cmd.CommandText = "INSERT INTO emps_l0_approval_log (u0_typeof_idx_ref, u0_node_idx_ref, approval_log_types, approval_log_emp_types, approval_log_created_at, approval_log_created_by) VALUES (@announceGroupDiaryIdx, @nodeIdx, @approveLogTypes, 1, '" + now + "', @creator)";
            cmd.Parameters.AddWithValue("@announceGroupDiaryIdx", announceGroupDiaryIdx);
            cmd.Parameters.AddWithValue("@nodeIdx", nodeIdx);
            cmd.Parameters.AddWithValue("@approveLogTypes", approveLogTypes);
            cmd.Parameters.AddWithValue("@creator", creator);
            cmd.ExecuteNonQuery();
            conn.Close();
         }
         return (new JavaScriptSerializer().Serialize(emps));
      }
   }

   public bool IsReusable
   {
      get
      {
         return false;
      }
   }
}
