﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_masterdata_MD_ProductList : System.Web.UI.Page
{
    #region initial function/data
    function_tool _funcTool = new function_tool();

    data_product _data_product = new data_product();
    

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetm0ProductList = _serviceUrl + ConfigurationManager.AppSettings["urlGetm0ProductList"];
    static string _urlSetm0ProductList = _serviceUrl + ConfigurationManager.AppSettings["urlSetInsm0ProductList"];
    static string _urlSetUpdm0ProductList = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdm0ProductList"];
    static string _urlDeletem0ProductList = _serviceUrl + ConfigurationManager.AppSettings["urlDeletem0ProductList"];

    static string _urlGetm0ProductType = _serviceUrl + ConfigurationManager.AppSettings["urlGetm0ProductType"];

    string _localJson = "";
    int _tempInt = 0;

    string _defaultDdlText;
    string _defaultDdlValue;

    int emp_idx = 0;
    #endregion initial function/data


    protected void Page_Load(object sender, EventArgs e)
    {
        emp_idx = int.Parse(Session["emp_idx"].ToString());

        if (!IsPostBack)
        {
            initPage();
            actionIndex();

        }
    }

    #region selected   
    protected void actionIndex()
    {

        data_product data_product = new data_product();
        data_product.M0_ProductList_list = new M0_ProductList_detail[1];

        M0_ProductList_detail _M0_ProductList_detailindex = new M0_ProductList_detail();

        _M0_ProductList_detailindex.ProLIDX = 0;

        data_product.M0_ProductList_list[0] = _M0_ProductList_detailindex;
        //litDebug.Text = _funcTool.convertObjectToXml(data_product);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_product));
        data_product = callServiceNetwork(_urlGetm0ProductList, data_product);

         setGridData(GvMaster, data_product.M0_ProductList_list);

    }
    #endregion selected 

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        int _ProLIDX, _ProTIDX;
        string _txtProLName, _ProLFlavor;
        int _cemp_idx, _ProLWeight;

        M0_ProductList_detail objM0_ProductList = new M0_ProductList_detail();
        DropDownList ddlProTIDX;
        switch (cmdName)
        {

            case "btnToInsert":
                MvMaster.SetActiveView(ViewInsert);
                ddlProTIDX = (DropDownList)ViewInsert.FindControl("ddlProTIDX");
                actionddlplace(ddlProTIDX);
                break;
            case "btnCancel":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btnInsert":
                _ProTIDX = int.Parse(((DropDownList)ViewInsert.FindControl("ddlProTIDX")).SelectedValue);
                _txtProLName = ((TextBox)ViewInsert.FindControl("txtProLName")).Text.Trim();
                _ProLFlavor = ((TextBox)ViewInsert.FindControl("txtProLFlavor")).Text.Trim();
                _ProLWeight = int.Parse(((TextBox)ViewInsert.FindControl("txtProLWeight")).Text.Trim());
                DropDownList _ddlProLStatus = (DropDownList)ViewInsert.FindControl("ddlProLStatus");
                _cemp_idx = emp_idx;
                M0_ProductList_detail obj = new M0_ProductList_detail();
                _data_product.M0_ProductList_list = new M0_ProductList_detail[1];
                obj.ProLIDX = 0;
                obj.ProTIDX = _ProTIDX;
                obj.ProLName = _txtProLName;
                obj.ProLFlavor = _ProLFlavor;
                obj.ProLWeight = _ProLWeight;
                obj.ProLStatus = int.Parse(_ddlProLStatus.SelectedValue);
                //obj.CEmpIDX = _cemp_idx;

                _data_product.M0_ProductList_list[0] = obj;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_product));
               
                _data_product = callServiceNetwork(_urlSetm0ProductList, _data_product);

                if (_data_product.return_code == 0)
                {
                    actionIndex();
                    //MvMaster.SetActiveView(ViewIndex);
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                }
                else
                {
                    setError(_data_product.return_code.ToString() + " - " + _data_product.return_msg);
                }

                //actionCreate();
                //Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btnDelete":

                _ProLIDX = int.Parse(cmdArg);
                _cemp_idx = emp_idx;


                _data_product.M0_ProductList_list = new M0_ProductList_detail[1];
                objM0_ProductList.ProLIDX = _ProLIDX;
               // objM0_ProductList.CEmpIDX = _cemp_idx;

                _data_product.M0_ProductList_list[0] = objM0_ProductList;

               // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_product));

                _data_product = callServiceNetwork(_urlDeletem0ProductList, _data_product);


                if (_data_product.return_code == 0)
                {

                    //actionIndex();
                    //MvMaster.SetActiveView(ViewIndex);
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);


                }
                else
                {
                    setError(_data_product.return_code.ToString() + " - " + _data_product.return_msg);
                }

                break;




        }
    }
    #endregion btnCommand

    #region bind data
    protected void getGridData(string _cmdName, int _actionType)
    {
        // setGridData(gvBookingType, ViewState["listData"]);
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected string getStatus(int status)
    {
        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();
                actionIndex();
                break;
        }
    }

    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        DropDownList ddlProTIDX;
        switch (GvName.ID)
        {
            case "GvMaster":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster.EditIndex == e.Row.RowIndex) //to overlook header row
                    {
                        ddlProTIDX = (DropDownList)e.Row.FindControl("ddlProTIDXUpdate");
                        Label lbddlProTIDX = (Label)e.Row.FindControl("lbddlProTIDX");
                        actionddlplace(ddlProTIDX);
                        ddlProTIDX.SelectedValue = lbddlProTIDX.Text;
                    }
                }

                break;
        }
    }

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.EditIndex = e.NewEditIndex;
                actionIndex();
                break;
        }
    }

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.EditIndex = -1;
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
        }
    }

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":


                int ProLIDX_update = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var txtProLNameUpdate = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtProLNameUpdate");
                var ddlProLStatusUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlProLStatusUpdate");

                var ddlProTIDXUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlProTIDXUpdate");
                var txtProLFlavorUpdate = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtProLFlavorUpdate");
                var txtProLWeightUpdate = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtProLWeightUpdate");

                GvMaster.EditIndex = -1;

                _data_product.M0_ProductList_list = new M0_ProductList_detail[1];
                M0_ProductList_detail _ProductList_detail = new M0_ProductList_detail();

                _ProductList_detail.ProLIDX = ProLIDX_update;
                _ProductList_detail.ProLName = txtProLNameUpdate.Text;
                _ProductList_detail.ProLFlavor = txtProLFlavorUpdate.Text;
                _ProductList_detail.ProLWeight = int.Parse(txtProLWeightUpdate.Text);
                _ProductList_detail.ProTIDX = int.Parse(ddlProTIDXUpdate.SelectedValue);
                _ProductList_detail.ProLStatus = int.Parse(ddlProLStatusUpdate.SelectedValue);
              //  _ProductList_detail.CEmpIDX = emp_idx;

                _data_product.M0_ProductList_list[0] = _ProductList_detail;

                // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_product));
                _data_product = callServiceNetwork(_urlSetUpdm0ProductList, _data_product);

                if (_data_product.return_code == 0)
                {
                    //initPage();
                    //setDataList(dtlMenu, _dataMenu.m0_menu_list);  
                    actionIndex();


                }
                else
                {
                    setError(_data_product.return_code.ToString() + " - " + _data_product.return_msg);
                }


                break;
        }
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj, string sysidx_menu, string menuidx_per, string role_per)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();

        //switch (fvName.ID)
        //{



        //}
    }

    protected void setDataList(DataList dtlName, Object obj)
    {
        dtlName.DataSource = obj;
        dtlName.DataBind();
    }

    protected void actionddlplace(DropDownList ddl)
    {
        // DropDownList ddlProTIDX = (DropDownList)ViewInsert.FindControl("ddlProTIDX");

        ddl.Items.Clear();
        ddl.AppendDataBoundItems = true;
        ddl.Items.Add(new ListItem("กรุณาเลือกประเภทสินค้า...", "0"));

        _data_product.M0_ProductType_list = new M0_ProductType_detail[1];
        M0_ProductType_detail _M0_ProductType_list = new M0_ProductType_detail();
        _data_product.M0_ProductType_list[0] = _M0_ProductType_list;
        _data_product = callServiceNetwork(_urlGetm0ProductType, _data_product);
        ddl.DataSource = _data_product.M0_ProductType_list;
        ddl.DataTextField = "ProTName";
        ddl.DataValueField = "ProTIDX";
        ddl.DataBind();
    }

    #endregion bind data

    #region reuse
    protected void initPage()
    {
        clearSession();
        clearViewState();

        // divAction.Visible = true;
        // gvBookingType.Visible = true;
        // fvBookingType.Visible = false;

        // getGridData("booking_type", 20);
        //setFormData(fvMenu, FormViewMode.Insert, null, "0", "0", "0");


    }

    protected void initPagePermission()
    {
        clearSession();
        clearViewState();

        // divAction.Visible = true;
        // gvBookingType.Visible = true;
        // fvBookingType.Visible = false;

        // getGridData("booking_type", 20);
        //setFormData(fvPermission, FormViewMode.Insert, null, "0", "0", "0");


    }

    protected void setVisible()
    {
        // divAction.Visible = !divAction.Visible;
        // gvBookingType.Visible = !gvBookingType.Visible;
        // fvBookingType.Visible = !fvBookingType.Visible;
    }

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        // ViewState["listData"] = null;
    }

    protected void setError(string _errorText)
    {
        // divShowError.Visible = !divShowError.Visible;
        // litErrorCode.Text = _dataEmployee.return_code.ToString();
        if (_errorText != "")
        {
            litDebug.Text = "error : " + _errorText;
        }
        else
        {
            litDebug.Text = String.Empty;
        }
    }

    protected data_product callServiceNetwork(string _cmdUrl, data_product _data_product)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_product);
        // litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);

        // convert json to object
        _data_product = (data_product)_funcTool.convertJsonToObject(typeof(data_product), _localJson);

        return _data_product;
    }


    #endregion reuse

}