<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="ecom_report.aspx.cs" Inherits="websystem_ecommerce_ecom_report" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:HyperLink ID="hlSetTotop" runat="server"></asp:HyperLink>
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>

    <asp:MultiView ID="mvSystem" runat="server">
        <asp:View ID="viewList" runat="server">
            <div class="col-md-6 col-md-offset-3">
                <div class="form-horizontal" role="form">
                    <div class="form-group">
                        <label class="col-md-4 control-label">ตั้งแต่วันที่</label>
                        <div class="col-md-6">
                            <asp:TextBox ID="tbDateStart" runat="server" CssClass="form-control date-datepicker-date" MaxLength="20">
                            </asp:TextBox>
                        </div>
                        <label class="col-md-2"></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">ถึงวันที่</label>
                        <div class="col-md-6">
                            <asp:TextBox ID="tbDateEnd" runat="server" CssClass="form-control date-datepicker-date" MaxLength="20">
                            </asp:TextBox>
                        </div>
                        <label class="col-md-2"></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4"></label>
                        <div class="col-md-6">
                            <asp:LinkButton ID="lbSearch" runat="server" CssClass="btn btn-primary"
                                OnCommand="btnCommand" CommandName="cmdSearch"><i class="fas fa-search"></i></i>
                            </asp:LinkButton>
                            <asp:LinkButton ID="lbReset" runat="server" CssClass="btn btn-default"
                                OnCommand="btnCommand" CommandName="cmdReset"><i class="fas fa-sync-alt"></i>
                            </asp:LinkButton>
                            <asp:LinkButton ID="lbExport" runat="server" CssClass="btn btn-success"
                                OnCommand="btnCommand" CommandName="cmdExport"><i class="far fa-file-excel"></i></i>
                            </asp:LinkButton>
                        </div>
                        <label class="col-md-2"></label>
                    </div>
                </div>
            </div>
            <asp:GridView ID="gvReport" runat="server" AutoGenerateColumns="False"
                CssClass="table table-striped table-bordered table-responsive">
                <HeaderStyle CssClass="info" Font-Size="Small" />
                <RowStyle Font-Size="Small" />
                <EmptyDataTemplate>
                    <div style="text-align: center">ไม่พบข้อมูล</div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="รหัสโปรโมชั่น">
                        <ItemTemplate>
                            <asp:Label ID="lblProCode" runat="server" Text='<%# Eval("pro_code") %>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="โปรโมชั่น">
                        <ItemTemplate>
                            <asp:Label ID="lblProName" runat="server" Text='<%# Eval("pro_name") %>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="วันที่ซื้อ">
                        <ItemTemplate>
                            <asp:Label ID="lblDateAdd" runat="server" Text='<%# Eval("date_add") %>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="เลขที่เอกสาร">
                        <ItemTemplate>
                            <asp:Label ID="lblReference" runat="server" Text='<%# Eval("reference") %>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ชื่อลูกค้า">
                        <ItemTemplate>
                            <asp:Label ID="lblCustomerName" runat="server" Text='<%# Eval("customername") %>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="มูลค่า">
                        <ItemTemplate>
                            <asp:Label ID="lblTotalPaid" runat="server" Text='<%# String.Format("{0:N}", Eval("total_paid") ) %>'>
                            </asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="text-right"></ItemStyle>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:View>
    </asp:MultiView>

    <script type="text/javascript">
        $(function () {
            $('.date-datepicker-date').datetimepicker({
                format: 'YYYY-MM-DD'
            });
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.date-datepicker-date').datetimepicker({
                    format: 'YYYY-MM-DD'
                });
            });
        });
    </script>
</asp:Content>