﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="qa_lab_lab_type.aspx.cs" Inherits="websystem_MasterData_qa_lab_lab_type" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <asp:HyperLink ID="SETFOCUS" runat="server"></asp:HyperLink>
    <asp:Literal ID="lab_type" runat="server"></asp:Literal>

    <asp:MultiView ID="MvMaster_lab_type" runat="server">
        <asp:View ID="view1_lab_type" runat="server">
            <div class="form-group">
                <asp:LinkButton ID="btn_addlab_type" Visible="true" CssClass="btn btn-primary" data-original-title="เพิ่มข้อมูลประเภทของเเลป" data-toggle="tooltip" title="เพิ่มข้อมูลประเภทของเเลป" runat="server"
                    CommandName="cmdAddlab_type" OnCommand="btnCommand"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;เพิ่มข้อมูลประเภทของเเลป</asp:LinkButton>
            </div>

            <%--  DIV ADD--%>
            <asp:FormView ID="Fv_Insert_Result" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">เพิ่มข้อมูลประเภทของเเลป</h3>

                        </div>
                        <div class="panel-body">
                            <div class="panel-heading">

                                <div class="form-horizontal" role="form">

                                    <div class="panel-heading">

                                        <div class="form-group">
                                            <asp:Label ID="la_namelab_type" runat="server" Text="ชื่อประเภทของแลปที่ส่งตรวจ" CssClass="col-sm-3 control-label"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtlab_type_name" runat="server" CssClass="form-control" placeholder="กรอกประเภทของเเลปที่ส่งตรวจ ..." Enabled="true" />
                                                <asp:RequiredFieldValidator ID="Re_labtype_name" runat="server"
                                                    ControlToValidate="txtlab_type_name" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกประเภทของเเลปที่ส่งตรวจ" ValidationGroup="Save" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="aj_labTname" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_labtype_name" Width="250" />
                                            </div>
                                             <div class="col-sm-3"></div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="la_statuslab_type" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="ddlab_type_add" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="1" Text="Online" />
                                                    <asp:ListItem Value="0" Text="Offline" />
                                                </asp:DropDownList>
                                            </div>
                                             <div class="col-sm-3"></div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-2 col-sm-offset-3">
                                                <asp:LinkButton ID="Lbtn_submit_lab_type" ValidationGroup="Save" CssClass="btn btn-success" Text="Save" data-toggle="tooltip" title="บันทึก" runat="server" CommandName="Lbtn_submit_lab_type" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"></asp:LinkButton>
                                                <asp:LinkButton ID="Lbtn_cancel_lab_type" CssClass="btn btn-danger" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="Lbtn_cancel_lab_type" data-toggle="tooltip" title="ยกเลิก"></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <hr />
                                <%--  /DIV END ADD--%>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <%--select Place--%>
            <asp:GridView ID="Gv_select_labtype" runat="server"
                AutoGenerateColumns="false"
                CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                HeaderStyle-CssClass="success"
                AllowPaging="true"
                PageSize="10"
                OnRowDataBound="Master_RowDataBound"
                OnRowUpdating="Master_RowUpdating"
                OnRowEditing="Master_RowEditing"
                OnRowCancelingEdit="Master_RowCancelingEdit"
                OnPageIndexChanging="Master_PageIndexChanging"
                AutoPostBack="false">

                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                <EmptyDataTemplate>
                    <div style="text-align: center; color: red"><b>ไม่มีข้อมูลประเภทของเเลปที่ตรวจ</b> </div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style="text-align: center; padding-top: 5px;">
                                <asp:Label ID="place_idx" runat="server" Visible="false" Text='<%# Eval("lab_type_idx") %>' />
                                <%# (Container.DataItemIndex +1) %>
                            </div>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="ID_lab_type" Visible="false" runat="server" CssClass="form-control " Text='<%# Eval("lab_type_idx") %>'></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="La_labtype_name" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อประเภทของแลปที่ส่งตรวจ" />
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="Name_lab_type" runat="server" CssClass="form-control " Text='<%# Eval("lab_type_name") %>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="Re_Name_lab_type" runat="server"
                                                    ControlToValidate="Name_lab_type" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกชื่อแลปที่ส่งตรวจ" ValidationGroup="editlabtype" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="aj_labTname" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_Name_lab_type" Width="250" />
                                        </div>
                                        <div class="col-sm-3"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="lbupdates_labtype" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddEdit_lab_type" Text='<%# Eval("lab_type_status") %>'
                                                CssClass="form-control" runat="server">
                                                <asp:ListItem Value="1">Online</asp:ListItem>
                                                <asp:ListItem Value="0">Offline</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-3"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-9">
                                            <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" ValidationGroup="editlabtype" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                Text="Save" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"></asp:LinkButton>
                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                        </EditItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ประเภทของแลป" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="30%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lab_type_name" runat="server"
                                    Text='<%# Eval("lab_type_name") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lblab_type_status" Visible="false" runat="server"
                                        CssClass="col-sm-12" Text='<%# Eval("lab_type_status") %>'></asp:Label>
                                    <asp:Label ID="lab_type_statusOpen" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="แลปนี้ใช่งานอยู่"
                                        CssClass="col-sm-12">
                                                <div style="text-align: center; color:green;">
                                                    <span><b>Online</b></span>
                                    </asp:Label>
                                    <asp:Label ID="labtype_statusClose" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="แลปนี้ปิดใช่งานแล้ว" CssClass="col-sm-12">
                                                <div style="text-align: center; color:red;">
                                                <span><b>Offline</b></span>
                                    </asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="จัดการข้อมูล" HeaderStyle-Width="15%" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style="padding-top: 5px">
                                <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                    data-toggle="tooltip" OnCommand="btnCommand" title="แก้ไขข้อมูลสถานที่">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                                <asp:LinkButton ID="btnTodelete" CssClass="text-trash" runat="server" CommandName="btnTodelete_lab_type"
                                    data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบแลปนี้ใช่หรือไม่ ?')"
                                    CommandArgument='<%#Eval("lab_type_idx") %>' title="ลบแลปนี้">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>


        </asp:View>
    </asp:MultiView>
</asp:Content>

