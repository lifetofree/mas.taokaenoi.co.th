<%@ Page Language="C#" AutoEventWireup="true" CodeFile="login_conference.aspx.cs" Inherits="websystem_login_conference" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <title>Conference</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="./../../Content/bootstrap.css" runat="server" rel="stylesheet" />
    <link href="./../../Content/custom.css" runat="server" rel="stylesheet" />
    <link rel="shortcut icon" href="./../../images/Logo_TKN-01.png" type="image/x-icon" />
    <link href='https://fonts.googleapis.com/css?family=Kanit' rel='stylesheet' />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css"
        integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous" />

    <style>
        body,
        html {
            font-family: 'Kanit', 'Tahoma', 'Geneva', 'sans-serif' !important;
            height: 100%;
            color: #ffffff;
        }

        .vertical-offset-100 {
            margin-top: 180px;
            padding-top: 210px;
        }

        /* conf_1 */
        /* .inner-box {
            background: url('./../../masterpage/images/conference/bg_conference_inner.png') scroll 0px 100%;
            background-repeat: no-repeat;
            background-position: center top;
            -webkit-background-size: contain;
            -moz-background-size: contain;
            -o-background-size: contain;
            background-size: contain;
            min-height: 450px;
        }

        .btn-conference {
            color: #ffffff;
            background-color: #d8252f;
            border: solid 1px #333333;
            font-weight: bold;
        }

        @media (max-width: 767px) {
            .inner-box {
                background: url('./../../masterpage/images/conference/bg_conference_inner_mobile.png') scroll 0px 100%;
                background-repeat: no-repeat;
                background-position: center top;
                -webkit-background-size: contain;
                -moz-background-size: contain;
                -o-background-size: contain;
                background-size: contain;
            }

            .vertical-offset-100 {
                margin-top: 50px;
                padding-top: 300px;
            }
        }

        @media (max-width: 960px) {
            .inner-box {
                background: url('./../../masterpage/images/conference/bg_conference_inner_mobile.png') scroll 0px 100%;
                background-repeat: no-repeat;
                background-position: center top;
                -webkit-background-size: contain;
                -moz-background-size: contain;
                -o-background-size: contain;
                background-size: contain;
            }

            .vertical-offset-100 {
                margin-top: 150px;
                padding-top: 300px;
            }
        } */
        /* conf_1 */

        /* conf_2 */
        .outer-box {
            /* background-color: #f04e4c;
            border-radius: 60px;*/
            min-height: 700px;
            margin-top: 50px;
            padding: 50px;
            background: url('./../../masterpage/images/conference/bg_conference_outer.png') scroll 0px 100%;
            background-repeat: no-repeat;
            background-position: center top;
            -webkit-background-size: contain;
            -moz-background-size: contain;
            -o-background-size: contain;
            background-size: contain;
        }

        .title-text {
            color: #ffffff;
            font-size: xx-large;
            font-weight: bold;
        }

        .subtitle-text {
            color: #ffffff;
            font-size: x-large;
            font-weight: normal;
            margin-top: 10px;
        }

        .register-text {
            font-size: x-large;
            color: #fdb913;
            margin-top: 270px;
            text-align: right;
        }

        .form-register {
            margin-top: 30px;
        }

        .btn-conference-2 {
            background-color: #fdb913;
            color: #333333;
            border-radius: 15px;
            margin-top: 10px;
            width: 100%;
        }

        @media (max-width: 767px) {
            .outer-box {
                /* background-color: #f04e4c;
                border-radius: 60px;*/
                min-height: 700px;
                margin-top: 250px;
                padding: 0px !important;
                background: url('./../../masterpage/images/conference/bg_conference_outer_mobile_small.png') scroll 0px 100% !important;
                background-repeat: no-repeat !important;
                background-position: center top !important;
                -webkit-background-size: contain !important;
                -moz-background-size: contain !important;
                -o-background-size: contain !important;
                background-size: contain !important;
            }

            .title-text {
                color: #ffffff;
                font-size: x-large;
                font-weight: bold;
                margin-top: 220px !important;
            }

            .subtitle-text {
                color: #ffffff;
                font-size: large;
                font-weight: normal;
                margin-top: 10px;
            }

            .register-text {
                font-size: large;
                margin-top: 350px;
                text-align: left;
            }

            .form-register {
                margin-top: 10px;
            }

            .form-left-label {
                display: none;
            }
        }

        @media (max-width: 960px) {
            .outer-box {
                min-height: 700px;
                margin-top: 50px;
                padding: 120px;
                background: url('./../../masterpage/images/conference/bg_conference_outer_mobile.png') scroll 0px 100%;
                background-repeat: no-repeat;
                background-position: center top;
                -webkit-background-size: contain;
                -moz-background-size: contain;
                -o-background-size: contain;
                background-size: contain;
            }

            .title-text {
                color: #ffffff;
                font-size: x-large;
                font-weight: bold;
                margin-top: 250px;
            }

            .subtitle-text {
                color: #ffffff;
                font-size: large;
                font-weight: normal;
                margin-top: 10px;
            }

            .register-text {
                font-size: large;
                margin-top: 350px;
                text-align: left;
            }

            .form-register {
                margin-top: 10px;
            }

            .form-left-label {
                display: none;
            }
        }

        /* conf_2 */
    </style>
</head>

<body id="body_main" runat="server">
    <form id="formMaster" runat="server">
        <asp:ScriptManager ID="tsmMaster" runat="server"></asp:ScriptManager>
        <script src='<%=ResolveUrl("~/Scripts/jquery-3.1.1.min.js") %>'></script>
        <script src='<%=ResolveUrl("~/Scripts/bootstrap.js") %>'></script>
        <script src='<%=ResolveUrl("~/Scripts/custom.js") %>'></script>

        <asp:UpdatePanel ID="upLogin" runat="server">
            <ContentTemplate>
                <asp:Literal ID="litDebug" runat="server"></asp:Literal>
                <div id="div_conf_1" runat="server" class="container">
                    <div class="row vertical-offset-100 inner-box">
                        <div class="col-md-8 col-md-offset-4">
                            <div class="col-md-8 col-md-offset-1">
                                <div class="row display-flex">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="tbUserName" runat="server" CssClass="form-control input-md"
                                                placeholder="Username" MaxLength="30" ValidationGroup="formLogin">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvUserName" ValidationGroup="formLogin"
                                                runat="server" Display="None" SetFocusOnError="true"
                                                ControlToValidate="tbUserName" ErrorMessage="กรุณากรอกชื่อผู้ใช้งาน" />
                                            <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="vceUserName"
                                                TargetControlID="rfvUserName"
                                                HighlightCssClass="validatorCalloutHighlight" />
                                        </div>
                                        <div class="form-group">
                                            <asp:TextBox ID="tbUserPass" runat="server" CssClass="form-control input-md"
                                                placeholder="Password" MaxLength="100" TextMode="Password"
                                                ValidationGroup="formLogin">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvUserPass" ValidationGroup="formLogin"
                                                runat="server" Display="None" SetFocusOnError="true"
                                                ControlToValidate="tbUserPass" ErrorMessage="กรุณากรอกรหัสผ่าน" />
                                            <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="vceUserPass"
                                                TargetControlID="rfvUserPass"
                                                HighlightCssClass="validatorCalloutHighlight" />
                                        </div>
                                        <div class="form-group">
                                            <asp:Button ID="btnLogin" CssClass="btn btn-md btn-conference btn-block"
                                                runat="server" data-original-title="Login" data-toggle="tooltip"
                                                OnCommand="btnCommand" CommandName="cmdLogin" Text="Join Meeting"
                                                ValidationGroup="formLogin" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="div_conf_2" runat="server" class="container">
                    <div class="row outer-box">
                        <div class="col-md-12">
                            <div class="col-md-2"></div>
                            <div class="col-md-10">
                                <div class="col-md-12">
                                    <!-- <div class="title-text">Welcome TKN Conference</div>
                                    <div class="subtitle-text">June (Friday) 12, 2020
                                        14:00 p.m. - 15:00 p.m. (GMT+7 Bangkok time)</div> -->
                                    <label class="col-md-6 control-label register-text">Check-in</label>
                                    <label class="col-md-6"></label>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-horizontal form-register" role="form">
                                        <div class="form-group">
                                            <label class="col-md-6 control-label form-left-label">Name-Surname</label>
                                            <div class="col-md-6">
                                                <asp:TextBox ID="tbUserFirstname" runat="server"
                                                    CssClass="form-control input-md" placeholder="Name" MaxLength="50"
                                                    ValidationGroup="formLogin2">
                                                </asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvUserFirstname"
                                                    ValidationGroup="formLogin2" runat="server" Display="None"
                                                    SetFocusOnError="true" ControlToValidate="tbUserFirstname"
                                                    ErrorMessage="กรุณากรอกชื่อ" />
                                                <ajaxToolkit:ValidatorCalloutExtender runat="Server"
                                                    ID="vceUserFirstname" TargetControlID="rfvUserFirstname"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    PopupPosition="BottomRight" />
                                            </div>
                                        </div>
                                        <div class="form-group" style="display: none;">
                                            <label class="col-md-6 control-label form-left-label">Surname</label>
                                            <div class="col-md-6">
                                                <asp:TextBox ID="tbUserLastname" runat="server"
                                                    CssClass="form-control input-md" placeholder="Surname"
                                                    MaxLength="50" ValidationGroup="formLogin2">
                                                </asp:TextBox>
                                                <%--<asp:RequiredFieldValidator ID="rfvUserLastname"
                                                    ValidationGroup="formLogin2" runat="server" Display="None"
                                                    SetFocusOnError="true" ControlToValidate="tbUserLastname"
                                                    ErrorMessage="กรุณากรอกนามสกุล" />
                                                <ajaxToolkit:ValidatorCalloutExtender runat="Server"
                                                    ID="vceUserLastname" TargetControlID="rfvUserLastname"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    PopupPosition="BottomRight" />--%>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-6 control-label form-left-label">E-mail</label>
                                            <div class="col-md-6">
                                                <asp:TextBox ID="tbUserMail" runat="server"
                                                    CssClass="form-control input-md" placeholder="E-mail"
                                                    MaxLength="250" ValidationGroup="formLogin2">
                                                </asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvUserMail"
                                                    ValidationGroup="formLogin2" runat="server" Display="None"
                                                    SetFocusOnError="true" ControlToValidate="tbUserMail"
                                                    ErrorMessage="กรุณากรอกอีเมล" />
                                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="vceUserMail"
                                                    TargetControlID="rfvUserMail"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    PopupPosition="BottomRight" />
                                                <asp:RegularExpressionValidator ID="revUserMail" runat="server"
                                                    ValidationGroup="formLogin2" Display="None"
                                                    ErrorMessage="กรุณากรอกอีเมลให้ถูกต้อง"
                                                    ControlToValidate="tbUserMail"
                                                    ValidationExpression="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?" />
                                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="vceUserMail2"
                                                    TargetControlID="revUserMail"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    PopupPosition="BottomRight" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-6 control-label form-left-label">Company / Fund
                                                name</label>
                                            <div class="col-md-6">
                                                <asp:TextBox ID="tbUserCompany" runat="server"
                                                    CssClass="form-control input-md" placeholder="Company / Fund Name"
                                                    MaxLength="250" ValidationGroup="formLogin2">
                                                </asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvUserCompany"
                                                    ValidationGroup="formLogin2" runat="server" Display="None"
                                                    SetFocusOnError="true" ControlToValidate="tbUserCompany"
                                                    ErrorMessage="กรุณากรอก Company / Fund Name" />
                                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="vceUserCompany"
                                                    TargetControlID="rfvUserCompany"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    PopupPosition="BottomRight" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-6 control-label form-left-label">Your WebEx display
                                                name</label>
                                            <div class="col-md-6">
                                                <asp:TextBox ID="tbUserWebEx" runat="server"
                                                    CssClass="form-control input-md"
                                                    placeholder="Your WebEx display name" MaxLength="50"
                                                    ValidationGroup="formLogin2">
                                                </asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvUserWebEx"
                                                    ValidationGroup="formLogin2" runat="server" Display="None"
                                                    SetFocusOnError="true" ControlToValidate="tbUserWebEx"
                                                    ErrorMessage="กรุณากรอก WebEx display name" />
                                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="vceUserWebEx"
                                                    TargetControlID="rfvUserWebEx"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    PopupPosition="BottomRight" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-6 control-label form-left-label"></label>
                                            <div class="col-md-6">
                                                <asp:LinkButton ID="lbJoinMeeting" runat="server"
                                                    CssClass="btn btn-md btn-conference-2" OnCommand="btnCommand"
                                                    CommandName="cmdRegister" ValidationGroup="formLogin2"><span style="font-size: large; font-weight: bold;">Join
                                                    Meeting</span><br />
                                                    (Room open on 1:45 p.m. onward)<br />
                                                    After log-in please wait for approval
                                                </asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div>
            <asp:UpdateProgress ID="UpdateProgress" runat="server">
                <ProgressTemplate>
                    <div id="modalPopup" runat="server" class="modalPopup">
                        <div class="centerPopup">
                            <asp:Image ID="imgWaiting" ImageUrl="~/masterpage/images/loading.gif"
                                AlternateText="Processing" runat="server" Width="50" Height="50" />
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
    </form>
</body>