﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="hr_health_check.aspx.cs" Inherits="websystem_hr_hr_health_check" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <asp:HyperLink ID="setOntop" runat="server"></asp:HyperLink>
    <asp:TextBox ID="txt_emp_idx_historyhealth" runat="server" Visible="false"></asp:TextBox>
    <asp:TextBox ID="txt_historyhealth_idx" runat="server" Visible="false"></asp:TextBox>
    <asp:TextBox ID="txt_u2_history_idx" runat="server" Visible="false"></asp:TextBox>

    <!--tab menu-->
    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default" id="divBtnHead" runat="server" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-bar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="sub-navbar">
                        <a class="navbar-brand" href="#"><b>Menu</b></a>
                    </div>
                </div>

                <!--Collect the nav links, forms, and other content for toggling-->
                <div class="collapse navbar-collapse" id="menu-bar">
                    <ul class="nav navbar-nav" id="uiNav" runat="server">

                        <li id="li0" runat="server">
                            <asp:LinkButton ID="lbHistoryList" runat="server" CommandName="cmdHistoryList" OnCommand="navCommand" CommandArgument="docHistory"> ข้อมูลส่วนตัว</asp:LinkButton>
                        </li>

                        <% if (Session["emp_idx"].ToString().ToString() == "1413" || Session["emp_idx"].ToString().ToString() == "24333" || Session["emp_idx"].ToString().ToString() == "21425"
                                                                                              || Session["emp_idx"].ToString().ToString() == "24854" || Session["emp_idx"].ToString().ToString() == "332" || Session["emp_idx"].ToString().ToString() == "22908"
                                                                                              || Session["emp_idx"].ToString().ToString() == "24317")
                            { %>
                        <li id="li1" runat="server">
                            <asp:LinkButton ID="lbDetailList" runat="server" CommandName="cmdDetailList" OnCommand="navCommand" CommandArgument="docDetailList"> รายการทั่วไป</asp:LinkButton>
                        </li>

                        <li id="li2" runat="server">
                            <asp:LinkButton ID="lbCreateList" runat="server" CommandName="cmdCreateList" OnCommand="navCommand" CommandArgument="docCreateList"> กรอกรายละเอียด</asp:LinkButton>
                        </li>

                        <li id="li3" runat="server">
                            <asp:LinkButton ID="lbReportList" runat="server" CommandName="cmdReportList" OnCommand="navCommand" Visible="true" CommandArgument="docReportList"> รายงาน</asp:LinkButton>
                        </li>
                        <% } %>
                    </ul>

                    <ul class="nav navbar-nav navbar-right" runat="server">

                        <li id="_divMenuLiToDocument" runat="server">
                            <asp:HyperLink ID="_divMenuBtnToDocument" runat="server"
                                NavigateUrl="https://drive.google.com/open?id=1mTYw7Ms_O4C5SnonTz8En6D78UX-fsVWaM9Y0oImcJo" Target="_blank" Text="คู่มือการใช้งาน" />
                        </li>
                    </ul>

                </div>
                <!--Collect the nav links, forms, and other content for toggling-->
            </div>
        </nav>
    </div>
    <!--tab menu-->

    <div class="col-md-12">
        <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    </div>

    <asp:MultiView ID="mvSystem" runat="server">

        <!--View History-->
        <asp:View ID="docHistory" runat="server">
            <%--     history--%>
            <div id="div_DetailEmployee" runat="server" class="col-md-12">
                <asp:FormView ID="fvEmpDetailProfile" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <ItemTemplate>
                        <asp:HiddenField ID="hfEmpOrgIDXProfile" runat="server" Value='<%# Eval("org_idx") %>' />
                        <asp:HiddenField ID="hfEmpRdeptIDXProfile" runat="server" Value='<%# Eval("rdept_idx") %>' />
                        <asp:HiddenField ID="hfEmpRsecIDXProfile" runat="server" Value='<%# Eval("rsec_idx") %>' />
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายละเอียดผู้ใช้งาน</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">รหัสพนักงาน</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpCodeProfile" runat="server" CssClass="form-control" Text='<%# Eval("emp_code") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ชื่อ - นามสกุล</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpNameProfile" runat="server" CssClass="form-control" Text='<%# Eval("emp_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">องค์กร</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpOrgProfile" runat="server" CssClass="form-control" Text='<%# Eval("org_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ฝ่าย</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpDeptProfile" runat="server" CssClass="form-control" Text='<%# Eval("dept_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">แผนก</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpSecProfile" runat="server" CssClass="form-control" Text='<%# Eval("sec_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ตำแหน่ง</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpPosProfile" runat="server" CssClass="form-control" Text='<%# Eval("pos_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:FormView>
                <div class="form-group">
                    <ul class="nav nav-tabs bg-default">
                        <li id="liProfie1" runat="server">
                            <asp:LinkButton ID="btnViewProfile" runat="server" CommandName="cmdProfile" OnCommand="navProfileCommand" data-toggle="tooltip" title="ประวัติส่วนตัว" CommandArgument="docProfile"><i class="fa fa-user-md"></i> <b>ประวัติส่วนตัว</b> </asp:LinkButton>
                        </li>
                        <li id="liProfie2" runat="server">
                            <asp:LinkButton ID="btnViewHistoryWork" runat="server" CommandName="cmdHistoryWork" OnCommand="navProfileCommand" CommandArgument="docHistoryWork" data-toggle="tooltip" title="ประวัติการทำงาน"><i class="fa fa-briefcase"></i> <b>ประวัติการทำงาน</b></asp:LinkButton>
                        </li>
                        <li id="liProfie3" runat="server">
                            <asp:LinkButton ID="btnViewHistorySickness" runat="server" CommandName="cmdHistorySickness" OnCommand="navProfileCommand" CommandArgument="docSickness" data-toggle="tooltip" title="ประวัติการเจ็บป่วย"><i class="fa fa-medkit"></i> <b>ประวัติการเจ็บป่วย</b></asp:LinkButton>
                        </li>
                        <li id="liProfie4" runat="server">
                            <asp:LinkButton ID="btnViewHealthCheck" runat="server" CommandName="cmdHealthCheck" OnCommand="navProfileCommand" CommandArgument="docHealthCheck" data-toggle="tooltip" title="การตรวจสุขภาพ"><i class="fa fa-heartbeat"></i> <b>การตรวจสุขภาพ</b></asp:LinkButton>
                        </li>
                        <li id="liProfie5" runat="server">
                            <asp:LinkButton ID="btnViewHistoryInjury" runat="server" CommandName="cmdInjury" OnCommand="navProfileCommand" CommandArgument="docInjury" data-toggle="tooltip" title="บันทึกการบาดเจ็บ"><i class="fa fa-ambulance"></i> <b>บันทึกการบาดเจ็บ</b></asp:LinkButton>
                        </li>
                    </ul>
                </div>
            </div>

            <asp:Panel ID="divHistoryEmployee" runat="server" class="col-md-12 tab-pane fade in active" Visible="true">
                <div class="form-group">
                    <asp:FormView ID="fvEmployeeProfile" runat="server" DefaultMode="ReadOnly" Width="100%">
                        <ItemTemplate>
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">ข้อมูลส่วนตัว</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">รหัสพนักงาน</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbEmpCode_History_Profile" runat="server" CssClass="form-control" Text='<%# Eval("EmpCode") %>' Enabled="false" />
                                            </div>
                                            <label class="col-sm-2 control-label">ชื่อ - นามสกุล</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbEmpName_History_Profile" runat="server" CssClass="form-control" Text='<%# Eval("FullNameTH") %>' Enabled="false" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">วัน เดือน ปี เกิด</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbBirthday_History_Profile" runat="server" CssClass="form-control" Text='<%# Eval("Birthday_Ex") %>' Enabled="false" />
                                            </div>
                                            <label class="col-sm-2 control-label">เพศ</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbSexNameTH_History_Profile" runat="server" CssClass="form-control" Text='<%# Eval("SexNameTH") %>' Enabled="false" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">วันที่เข้างาน</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbEmpIN_Ex_History_Profile" runat="server" CssClass="form-control" Text='<%# Eval("EmpIN_Ex") %>' Enabled="false" />
                                            </div>
                                            <label class="col-sm-2 control-label">เลขที่บัตรประชาน</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbIdentityCard_History_Profile" runat="server" CssClass="form-control" Text='<%# Eval("IdentityCard") %>' Enabled="false" />
                                            </div>
                                        </div>

                                        <hr />

                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <div class="panel panel-info">
                                                    <div class="panel-heading">
                                                        <h3 class="panel-title">ที่อยู่ตามบัตรประชาชน</h3>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">ที่อยู่ตามบัตรประชาชน</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tb_EmpAddr_History_Profile" runat="server" CssClass="form-control" Text='<%# Eval("EmpAddr") %>' Enabled="false" />
                                            </div>
                                            <label class="col-sm-2 control-label">ตำบล</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbDistName_History_Profile" runat="server" CssClass="form-control" Text='<%# Eval("DistName") %>' Enabled="false" />
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">อำเภอ</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbAmpName_History_Profile" runat="server" CssClass="form-control" Text='<%# Eval("AmpName") %>' Enabled="false" />
                                            </div>
                                            <label class="col-sm-2 control-label">จังหวัด</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbProvName_History_Profile" runat="server" CssClass="form-control" Text='<%# Eval("ProvName") %>' Enabled="false" />
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">รหัสไปรษณีย์</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tb_PostCode_History_Profile" runat="server" CssClass="form-control" Text='<%# Eval("PostCode") %>' Enabled="false" />
                                            </div>
                                            <label class="col-sm-2 control-label">โทรศัพท์</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbMobileNo_History_Profile" runat="server" CssClass="form-control" Text='<%# Eval("MobileNo") %>' Enabled="false" />
                                            </div>

                                        </div>

                                        <hr />
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <div class="panel panel-info">
                                                    <div class="panel-heading">
                                                        <h3 class="panel-title">ที่อยู่ที่สามารถติดต่อได้</h3>
                                                    </div>
                                                </div>
                                            </div>
                                            <label class="col-sm-9 control-label"></label>

                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">ที่อยู่ปัจจุบัน</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbEmpAddrPresent_History_Profile" runat="server" CssClass="form-control" Text='<%# Eval("EmpAddr") %>' Enabled="false" />
                                            </div>
                                            <label class="col-sm-2 control-label">ตำบล</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbDistNamePresent_History_Profile" runat="server" CssClass="form-control" Text='<%# Eval("DistName") %>' Enabled="false" />
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">อำเภอ</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbAmpNamePresent_History_Profile" runat="server" CssClass="form-control" Text='<%# Eval("AmpName") %>' Enabled="false" />
                                            </div>
                                            <label class="col-sm-2 control-label">จังหวัด</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbProvNamePresent_History_Profile" runat="server" CssClass="form-control" Text='<%# Eval("ProvName") %>' Enabled="false" />
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">รหัสไปรษณีย์</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbPostCodePresent_History_Profile" runat="server" CssClass="form-control" Text='<%# Eval("PostCode") %>' Enabled="false" />
                                            </div>
                                            <label class="col-sm-2 control-label">โทรศัพท์</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbMobileNoPresent_History_Profile" runat="server" CssClass="form-control" Text='<%# Eval("MobileNo") %>' Enabled="false" />
                                            </div>

                                        </div>

                                        <hr />

                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <div class="panel panel-info">
                                                    <div class="panel-heading">
                                                        <h3 class="panel-title">สถานประกอบการ</h3>
                                                    </div>
                                                </div>
                                            </div>
                                            <label class="col-sm-9 control-label"></label>

                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">ที่อยู่</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbOrgAddress_History_Profile" runat="server" CssClass="form-control" Text='<%# Eval("OrgAddress") %>' Enabled="false" />
                                            </div>
                                            <label class="col-sm-2 control-label">รหัสไปรษณีย์</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbPostCode_Org_History_Profile" runat="server" CssClass="form-control" Text='<%# Eval("PostCode_Org") %>' Enabled="false" />
                                            </div>

                                        </div>

                                    </div>

                                </div>
                            </div>
                        </ItemTemplate>

                    </asp:FormView>
                </div>
            </asp:Panel>

            <div id="divHistoryWorkProfile" runat="server" class="col-md-12" visible="false">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">ประวัติการทำงาน (ตั้งแต่อดีตถึงปัจจุบัน)</h3>
                    </div>

                    <div class="panel-body">
                        <div id="gvHistoryWork_Scroll" style="overflow-x: scroll; width: 100%" runat="server">
                            <asp:GridView ID="gvHistoryWork"
                                runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-responsive"
                                HeaderStyle-CssClass="info"
                                AllowPaging="true"
                                PageSize="10"
                                OnPageIndexChanging="gvPageIndexChanging"
                                OnRowDataBound="gvRowDataBound">
                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                <EmptyDataTemplate>
                                    <div class="text-center">-- ไม่พบข้อมูล --</div>
                                </EmptyDataTemplate>
                                <Columns>

                                    <%-- <asp:TemplateField HeaderText="#" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" ItemStyle-CssClass="text-center" HeaderStyle-Width="5%">
                                <ItemTemplate>
                                    <small>
                                        <%# (Container.DataItemIndex + 1) %>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>--%>

                                    <asp:TemplateField HeaderText="ชื่อสถานประกอบกิจการ/แผนก" ItemStyle-HorizontalAlign="Left"
                                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="Detail_company_name_Profile" runat="server"> 
                                    <p><b>ชื่อสถานประกอบกิจการ :</b> <%# Eval("company_name") %></p>
                                    <p><b>แผนก :</b> <%# Eval("department_name") %></p>
                                  
                                                </asp:Label>

                                            </small>
                                        </ItemTemplate>
                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ประเภทกิจการ" ItemStyle-HorizontalAlign="Left"
                                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lbl_business_type_history_Profile" runat="server" Text='<%# Eval("business_type") %>' />
                                            </small>

                                        </ItemTemplate>
                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ลักษณะงานที่ทำ" ItemStyle-HorizontalAlign="Left"
                                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lbl_job_description_history_Profile" runat="server" Text='<%# Eval("job_description") %>' />
                                            </small>

                                        </ItemTemplate>
                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ระยะเวลาที่ทำ" ItemStyle-HorizontalAlign="Left"
                                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="25%">
                                        <ItemTemplate>
                                            <small>

                                                <asp:Label ID="Detail_DateWork_Profile" runat="server"> 
                                        <p><b>วัน/เดือน/ปี ที่เริ่ม :</b> <%# Eval("job_startdate") %></p>
                                        <p><b>วัน/เดือน/ปี ที่สิ้นสุด :</b> <%# Eval("job_enddate") %></p>
                                  
                                                </asp:Label>
                                            </small>

                                        </ItemTemplate>
                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ปัจจัยที่เสี่ยงต่อสุขภาพ" ItemStyle-HorizontalAlign="Left"
                                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lbl_risk_health_history_Profile" runat="server" Text='<%#Eval("risk_health") %>' />
                                            </small>

                                        </ItemTemplate>
                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="มี/ใช้อุปกรณ์ป้องกันอันตราย" ItemStyle-HorizontalAlign="Left"
                                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lbl_protection_equipment_history_Profile" runat="server" Text='<%#Eval("protection_equipment") %>' />
                                            </small>

                                        </ItemTemplate>
                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>

                </div>
            </div>

            <div id="divHistorySickProfile" runat="server" class="col-md-12" visible="false">
                <asp:FormView ID="fvHistorySick" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <ItemTemplate>
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">ประวัติการเจ็บป่วย</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <h3 class="text-center">
                                                    <asp:Label ID="lbFormName_Sick_hr_Profile" runat="server" Text="ประวัติการเจ็บป่วย" CssClass="control-label" Font-Bold="true" Font-Size="14"></asp:Label>
                                                </h3>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="div_SickCheck_Profile" class="panel panel-default" runat="server">

                                        <div class="panel-body">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label>เคยป่วยเป็นโรคหรือมีการบาดเจ็บ</label>
                                                </div>
                                                <asp:UpdatePanel ID="update_eversick_hr_Profile" runat="server">
                                                    <ContentTemplate>
                                                        <div class="col-md-12">
                                                            <div id="div_DetailEverSickCheck_hr_Profile" class="panel panel-default" runat="server">
                                                                <div class="panel-body">

                                                                    <table class="table table-striped f-s-12 table-empshift-responsive">
                                                                        <asp:Repeater ID="rpt_eversick_Profile" runat="server">
                                                                            <HeaderTemplate>
                                                                                <tr>
                                                                                    <th>รายละเอียด</th>
                                                                                    <th>เมื่อปี พ.ศ.</th>
                                                                                </tr>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <tr>
                                                                                    <td data-th="รายละเอียด"><%# Eval("eversick") %></td>
                                                                                    <td data-th="เมื่อปี พ.ศ."><%# Eval("sick_year") %></td>

                                                                                </tr>
                                                                            </ItemTemplate>
                                                                        </asp:Repeater>
                                                                    </table>
                                                                    <div class="clearfix"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label>มีโรคประจำตัวหรือโรคเรื้อรังหรือไม่</label>
                                                </div>

                                                <div class="col-md-12">
                                                    <label>ระบุ</label>
                                                    <asp:TextBox ID="txt_havedisease_hr_Profile" runat="server" CssClass="form-control" Text='<%# Eval("havedisease") %>' placeholder="ระบุโรคประจำตัวหรือโรคเรื้อรัง...." Enabled="false" />
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label>เคยได้รับการผ่าตัดหรือไม่</label>
                                                </div>

                                                <div class="col-md-12">
                                                    <label>ระบุ</label>
                                                    <asp:TextBox ID="txt_surgery_hr_Profile" runat="server" CssClass="form-control" Text='<%# Eval("surgery") %>' placeholder="ระบุเคยได้รับการผ่าตัด...." Enabled="false" />
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label>เคยได้รับภูมิคุ้มกันโรคกรณีเกิดโรคระบาด หรือเพื่อป้องกันโรคติดต่อหรือไม่</label>
                                                </div>

                                                <div class="col-md-12">
                                                    <label>ระบุ</label>
                                                    <asp:TextBox ID="txt_Immune_hr_Profile" runat="server" CssClass="form-control" Text='<%# Eval("immune") %>' placeholder="ระบุเคยได้รับภูมิคุ้มกันโรคกรณีเกิดโรคระบาด...." Enabled="false" />
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label>ประวัติการเจ็บป่วยของสมาชิกในครอบครัว (เช่น มะเร็ง โลหิตจาง วัณโรค เบาหวาน หอบหืด ภูมิแพ้ เป็นต้น)</label>
                                                </div>

                                                <asp:UpdatePanel ID="Update_Relationfamily_hr_Profile" runat="server">
                                                    <ContentTemplate>
                                                        <div class="col-md-12">
                                                            <div id="div_Identifyrelationship_hr_Profile" class="panel panel-default" runat="server">
                                                                <div class="panel-body">
                                                                    <div class="form-group">
                                                                        <div class="col-md-12">
                                                                            <label>ระบุความสัมพันธ์และโรค</label>
                                                                        </div>
                                                                    </div>

                                                                    <table class="table table-striped f-s-12 table-empshift-responsive">
                                                                        <asp:Repeater ID="rptRelationfamily_Profile" runat="server">
                                                                            <HeaderTemplate>
                                                                                <tr>
                                                                                    <th>ความสัมพันธ์</th>
                                                                                    <th>โรค</th>
                                                                                </tr>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <tr>
                                                                                    <td data-th="ความสัมพันธ์"><%# Eval("relation_family") %></td>
                                                                                    <td data-th="โรค"><%# Eval("disease_family") %></td>
                                                                                </tr>
                                                                            </ItemTemplate>
                                                                        </asp:Repeater>
                                                                    </table>

                                                                    <div class="clearfix"></div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label>ปัจจุบันมียาที่จำเป็นต้องรับประทานเป็นประจำบ้างหรือไม่</label>
                                                </div>

                                                <div class="col-md-12">
                                                    <label>ระบุ</label>
                                                    <asp:TextBox ID="txt_DrugsEat_hr_Profile" runat="server" CssClass="form-control" Text='<%# Eval("drugs_eat") %>' placeholder="ระบุปัจจุบันมียาที่จำเป็นต้องรับประทานเป็นประจำ...." Enabled="false" />
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label>มีประวัติการแพ้ยาหรือไม่</label>
                                                </div>

                                                <div class="col-md-12">
                                                    <label>ระบุ</label>
                                                    <asp:TextBox ID="txt_Allergyhistory_hr_Profile" runat="server" CssClass="form-control" Text='<%# Eval("allergy_history") %>' placeholder="ระบุประวัติการแพ้ยา...." Enabled="false" />
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label>เคยสูบบุหรี่บ้างหรือไม่</label>
                                                    <asp:Label ID="idx_smoking_profile" runat="server" Text='<%# Eval("smoking") %>' Visible="false"></asp:Label>
                                                    <asp:CheckBoxList ID="rdoSmoking_hr_Profile" runat="server" Enabled="false" OnSelectedIndexChanged="onSelectedIndexChanged" AutoPostBack="true">
                                                        <asp:ListItem Value="1">&nbsp;ไม่เคย</asp:ListItem>
                                                        <asp:ListItem Value="2">&nbsp;เคยและปัจจุบันยังสูบอยู่ปริมาณ</asp:ListItem>
                                                        <asp:ListItem Value="3">&nbsp;เคยแต่เลิกแล้ว ระยะที่เคยสูบนาน</asp:ListItem>
                                                    </asp:CheckBoxList>
                                                </div>

                                                <asp:UpdatePanel ID="update_stopsmoking_hr_Profile" runat="server" Visible="false">
                                                    <ContentTemplate>
                                                        <div class="col-md-3">
                                                            <%--<label>ปี</label>--%>
                                                            <asp:TextBox ID="txt_year_smoking_hr_Profile" runat="server" CssClass="form-control" Text='<%# Eval("smoking_value1") %>' placeholder="ปี...." Enabled="false" />
                                                        </div>

                                                        <div class="col-md-1">
                                                            <asp:TextBox ID="txt_year_smoking_hr_Profile_unit" runat="server" CssClass="form-control" Text="ปี" placeholder="ปี...." Enabled="false" />
                                                        </div>

                                                        <div class="col-md-3">
                                                            <%--<label>เดือน</label>--%>
                                                            <asp:TextBox ID="txt_month_smoking_hr_Profile" runat="server" CssClass="form-control" Text='<%# Eval("smoking_value2") %>' placeholder="เดือน...." Enabled="false" />
                                                        </div>

                                                        <div class="col-md-1">
                                                            <asp:TextBox ID="txt_month_smoking_hr_Profile_unit" runat="server" CssClass="form-control" Text="เดือน" placeholder="เดือน...." Enabled="false" />
                                                        </div>

                                                        <div class="col-md-3">
                                                            <%-- <label>ปริมาณขณะก่อนเลิก(มวน/วัน)</label>--%>
                                                            <asp:TextBox ID="txt_before_stopsmoking_hr_Profile" runat="server" CssClass="form-control" Text='<%# Eval("smoking_value3") %>' placeholder="มวน/วัน...." Enabled="false" />
                                                        </div>
                                                        <div class="col-md-1">
                                                            <asp:TextBox ID="txt_before_stopsmoking_hr_Profile_unit" runat="server" CssClass="form-control" Text="มวน/วัน" placeholder="(มวน/วัน)...." Enabled="false" />
                                                        </div>

                                                    </ContentTemplate>
                                                </asp:UpdatePanel>

                                                <asp:UpdatePanel ID="update_eversmoking_hr_Profile" runat="server" Visible="false">
                                                    <ContentTemplate>
                                                        <div class="col-md-5">
                                                            <%-- <label>ปริมาณ(มวน/วัน)</label>--%>
                                                            <asp:TextBox ID="txt_valum_smoking_hr_Profile" runat="server" CssClass="form-control" Text='<%# Eval("smoking_value4") %>' placeholder="เดือน...." Enabled="false" />
                                                        </div>

                                                        <div class="col-md-1">
                                                            <asp:TextBox ID="txt_valum_smoking_hr_Profile_unit" runat="server" CssClass="form-control" Text="มวน/วัน" placeholder="(มวน/วัน)...." Enabled="false" />
                                                        </div>

                                                        <div class="col-md-6">
                                                            <label>&nbsp;</label>
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>

                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label>เคยดื่มสุรา เบียร์ หรือเครื่องดื่มที่มีแอลกอฮอล์บ้างหรือไม่</label>
                                                    <asp:Label ID="idx_alcohol_hr_profile" runat="server" Visible="false" Text='<%# Eval("alcohol") %>'></asp:Label>
                                                    <asp:CheckBoxList ID="rdo_alcohol_hr_Profile" runat="server" Enabled="false" OnSelectedIndexChanged="onSelectedIndexChanged" AutoPostBack="true">
                                                        <asp:ListItem Value="1">&nbsp;ไม่เคย</asp:ListItem>
                                                        <asp:ListItem Value="2">&nbsp;โดยปกติดื่มน้อยกว่า 1 ครั้งต่อสัปดาห์</asp:ListItem>
                                                        <asp:ListItem Value="3">&nbsp;ดื่ม 1 ครั้งต่อสัปดาห์</asp:ListItem>
                                                        <asp:ListItem Value="4">&nbsp;ดื่ม 2-3 ครั้งต่อสัปดาห์</asp:ListItem>
                                                        <asp:ListItem Value="5">&nbsp;ดื่มมากกว่า 3 ครั้งต่อสัปดาห์</asp:ListItem>
                                                        <asp:ListItem Value="6">&nbsp;เคยแต่เลิกแล้วระยะเวลาที่ดื่มนาน</asp:ListItem>
                                                    </asp:CheckBoxList>
                                                </div>

                                                <asp:UpdatePanel ID="update_alcohol_hr_Profile" runat="server" Visible="false">
                                                    <ContentTemplate>
                                                        <div class="col-md-5">
                                                            <%--<label>ปี</label>--%>
                                                            <asp:TextBox ID="txt_year_alcohol_hr_Profile" runat="server" Text='<%# Eval("alcohol_value1") %>' CssClass="form-control" placeholder="ปี...." Enabled="false" />
                                                        </div>

                                                        <div class="col-md-1">
                                                            <%--<label>ปี</label>--%>
                                                            <asp:TextBox ID="txt_year_alcohol_hr_Profile_unit" runat="server" Text="ปี" CssClass="form-control" Enabled="false" />
                                                        </div>

                                                        <div class="col-md-5">
                                                            <%--<label>เดือน</label>--%>
                                                            <asp:TextBox ID="txt_month_alcohol_hr_Profile" runat="server" Text='<%# Eval("alcohol_value2") %>' CssClass="form-control" placeholder="เดือน...." Enabled="false" />
                                                        </div>
                                                        <div class="col-md-1">
                                                            <%--<label>ปี</label>--%>
                                                            <asp:TextBox ID="txt_month_alcohol_hr_Profile_unit" runat="server" Text="เดือน" CssClass="form-control" Enabled="false" />
                                                        </div>

                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label>เคยเสพยาเสพติดหรือสารเสพติดใดๆ บ้างหรือไม่</label>
                                                </div>

                                                <div class="col-md-12">
                                                    <label>ระบุ</label>
                                                    <asp:TextBox ID="txt_addict_drug_hr_Profile" runat="server" CssClass="form-control" Text='<%# Eval("addict_drug") %>' placeholder="ระบุเคยเสพยาเสพติดหรือสารเสพติด...." Enabled="false" />
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label>ข้อมูลทางสุขภาพอื่นๆ ที่เป็นประโยชน์</label>
                                                    <asp:TextBox ID="txt_datahealth_hr_Profile" runat="server" CssClass="form-control" Text='<%# Eval("datahealth") %>' placeholder="ระบุเข้อมูลทางสุขภาพอื่นๆ ที่เป็นประโยชน์...." Enabled="false" />
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </ItemTemplate>

                </asp:FormView>

                <asp:Panel class="alert alert-warning" ID="EmptyDataSick" runat="server" Visible="false">
                    <button type="button" class="close" data-dismiss="alert"></button>
                    <strong>-- ไม่พบข้อมูล --</strong>
                </asp:Panel>
            </div>

            <div id="divHistoryHealthProfile" runat="server" class="col-md-12" visible="false">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">การตรวจสุขภาพ</h3>
                    </div>
                    <div class="panel-body">
                        <asp:GridView ID="gvHistoryHealth"
                            runat="server"
                            AutoGenerateColumns="false"
                            CssClass="table table-striped table-bordered table-responsive"
                            HeaderStyle-CssClass="info"
                            AllowPaging="true"
                            PageSize="10"
                            OnPageIndexChanging="gvPageIndexChanging"
                            OnRowDataBound="gvRowDataBound">
                            <PagerStyle CssClass="pageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                            <EmptyDataTemplate>
                                <div class="text-center">-- ไม่พบข้อมูล --</div>
                            </EmptyDataTemplate>
                            <Columns>

                                <asp:TemplateField HeaderText="วันที่ตรวจสุขภาพ" ItemStyle-HorizontalAlign="Center"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%">
                                    <ItemTemplate>
                                        <small>
                                            <asp:Label ID="lbl_date_checksick_history_Profile" runat="server" Text='<%# Eval("date_check") %>' />

                                        </small>
                                    </ItemTemplate>
                                    <EditItemTemplate />
                                    <FooterTemplate />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="รายละเอียดการตรวจ" ItemStyle-HorizontalAlign="Left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="30%">
                                    <ItemTemplate>
                                        <small>
                                            <asp:Label ID="lbl_detail_typecheck_history_Profile" runat="server" Text='<%# Eval("detail_typecheck") %>' />
                                        </small>

                                    </ItemTemplate>
                                    <EditItemTemplate />
                                    <FooterTemplate />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ชื่อแพทย์ผู้ตรวจ" ItemStyle-HorizontalAlign="Left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="30%">
                                    <ItemTemplate>
                                        <small>
                                            <asp:Label ID="lbl_doctor_name_history_Profile" runat="server" Text='<%# Eval("doctor_name") %>' />
                                        </small>

                                    </ItemTemplate>
                                    <EditItemTemplate />
                                    <FooterTemplate />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="25%">
                                    <ItemTemplate>
                                        <small>

                                            <asp:LinkButton ID="btnViewHealthCheck_Profile" CssClass="btn-sm btn-info" runat="server" CommandName="cmdViewHealthCheck"
                                                OnCommand="btnCommand" CommandArgument='<%# Eval("u2_historyhealth_idx")%>'
                                                data-toggle="tooltip" title="View"><i class="fa fa-file"></i>

                                            </asp:LinkButton>

                                        </small>

                                    </ItemTemplate>
                                    <EditItemTemplate />
                                    <FooterTemplate />
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>
                    </div>

                </div>
            </div>

            <div id="divHistoryInjuryProfile" runat="server" class="col-md-12" visible="false">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">บันทึกเกี่ยวกับการบาดเจ็บหรือเจ็บป่วยเนื่องจากการทำงานและสาเหตุ</h3>
                    </div>
                    <div class="panel-body">
                        <asp:GridView ID="gvInjuryProfile"
                            runat="server"
                            AutoGenerateColumns="false"
                            CssClass="table table-striped table-bordered table-responsive"
                            HeaderStyle-CssClass="info"
                            AllowPaging="true"
                            PageSize="10"
                            OnPageIndexChanging="gvPageIndexChanging"
                            OnRowDataBound="gvRowDataBound">
                            <PagerStyle CssClass="pageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                            <EmptyDataTemplate>
                                <div class="text-center">-- ไม่พบข้อมูล --</div>
                            </EmptyDataTemplate>
                            <Columns>

                                <asp:TemplateField HeaderText="วันเดือนปี" ItemStyle-HorizontalAlign="Center"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%">
                                    <ItemTemplate>
                                        <small>
                                            <asp:Label ID="lbl_date_injuly_history_Profile" runat="server" Text='<%# Eval("date_injuly") %>' />

                                        </small>
                                    </ItemTemplate>
                                    <EditItemTemplate />
                                    <FooterTemplate />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ส่วนของร่างกายที่บาดเจ็บหรือการเจ็บป่วย" ItemStyle-HorizontalAlign="Left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="30%">
                                    <ItemTemplate>
                                        <small>
                                            <asp:Label ID="lbl_injury_detail_history_Profile" runat="server" Text='<%# Eval("injury_detail") %>' />
                                        </small>

                                    </ItemTemplate>
                                    <EditItemTemplate />
                                    <FooterTemplate />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ส่วนของการบาดเจ็บหรือการเจ็บป่วย" ItemStyle-HorizontalAlign="Left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="30%">
                                    <ItemTemplate>
                                        <small>
                                            <asp:Label ID="lbl_cause_Injury_history_Profile" runat="server" Text='<%# Eval("cause_Injury") %>' />
                                        </small>

                                    </ItemTemplate>
                                    <EditItemTemplate />
                                    <FooterTemplate />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ระดับความรุนแรง" ItemStyle-HorizontalAlign="Left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="25%">
                                    <ItemTemplate>
                                        <small>
                                            <asp:Label ID="Detail_Violence_Profile" runat="server"> 
                                                    <p><b>ทุพพลภาพ :</b> <%# Eval("disability") %></p>
                                                    <p><b>สูญเสียอวัยวะบางส่วน :</b> <%# Eval("lost_organ") %></p>
                                                    <p><b>ทำงานไม่ได้ช่วยคราว :</b> <%# Eval("detail_not_working") %></p>
                                  
                                            </asp:Label>
                                        </small>

                                    </ItemTemplate>
                                    <EditItemTemplate />
                                    <FooterTemplate />
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>
                    </div>
                </div>

            </div>

            <asp:UpdatePanel ID="UpdatePanel_BackToListHealthDetail_Profile" runat="server" Visible="false" class="col-md-12">
                <ContentTemplate>
                    <div class="form-group">
                        <asp:LinkButton ID="btnBackToListHealthDetail_Profile" CssClass="btn btn-danger" data-toggle="tooltip" title="" runat="server"
                            CommandName="cmdCancelToHistoryHealthProfile" OnCommand="btnCommand">< ย้อนกลับ</asp:LinkButton>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

            <div id="divHistoryViewHealthProfile" runat="server" class="col-md-12" visible="false">
                <asp:FormView ID="fvHistoryHealthProfile" runat="server" DefaultMode="ReadOnly" OnDataBound="FvDetail_DataBound" Width="100%">
                    <ItemTemplate>
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">การตรวสุขภาพ</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <h3 class="text-center">
                                                    <asp:Label ID="lbFormName_HealthCheck_detail_Profile" runat="server" Text="การตรวจสุขภาพ" CssClass="control-label" Font-Bold="true" Font-Size="14"></asp:Label>

                                                </h3>
                                                <h4 class="text-center">
                                                    <asp:Label ID="lbl_detailnum_health" runat="server" Text="ครั้งที่" CssClass="control-label" Font-Bold="true" Font-Size="12"> <%# Eval("num_check") %></asp:Label>
                                                </h4>

                                            </div>
                                        </div>
                                    </div>

                                    <div id="div_DetailHistoryHealthCheck_Profile" class="panel panel-default" runat="server">
                                        <div class="panel-body">

                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <asp:Label ID="rdo_idx_detailtype_profile" Visible="false" Text='<%# Eval("m0_detail_typecheck_idx") %>' runat="server"></asp:Label>
                                                    <asp:CheckBoxList ID="rdoDetailType_detail_Profile" runat="server" Enabled="false" CssClass="" Font-Bold="true"
                                                        RepeatDirection="Vertical" RepeatColumns="2" RepeatLayout="Table" CellPadding="5" CellSpacing="5" Width="100%"
                                                        AutoPostBack="true">
                                                    </asp:CheckBoxList>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <%--<div class="col-md-6">
                                                    <label>ครั้งที่</label>
                                                    <asp:TextBox ID="txt_numcheck_detail_Profile" runat="server" Text='<%# Eval("num_check") %>' CssClass="form-control" Enabled="false">
                                                    </asp:TextBox>
                                                </div>--%>

                                                <div class="col-md-6">
                                                    <label>วันที่ตรวจสุขภาพ</label>
                                                    <div class="input-group date">
                                                        <asp:TextBox ID="txt_date_healthCheck_detail_Profile" runat="server" Text='<%# Eval("date_check") %>' CssClass="form-control from-date-datepicker" Enabled="false">
                                                        </asp:TextBox>
                                                        <span class="input-group-addon">
                                                            <span data-icon-element="" class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                    </div>
                                                </div>

                                                <div class="col-md-6"></div>

                                            </div>

                                            <asp:UpdatePanel ID="update_detailDoctorDetail_Profile" runat="server">
                                                <ContentTemplate>
                                                    <div id="div_DetailDoctorCheck_detail_Profile" class="panel panel-default" runat="server">
                                                        <div class="panel-body">

                                                            <div class="form-group">
                                                                <div class="col-md-6">
                                                                    <label>ชื่อแพทย์ผู้ตรวจ</label>
                                                                    <asp:TextBox ID="txtEmpIDX_Doctor_detail_Profile" Text='<%# Eval("doctor_name") %>' runat="server" CssClass="form-control" Enabled="false" />
                                                                    <%-- <asp:DropDownList ID="ddl_Doctorname_detail" runat="server" 
                                                                CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" />--%>
                                                                </div>

                                                                <div class="col-md-6">
                                                                    <label>เลขที่ใบประกอบวิชาชีพ</label>
                                                                    <%--<asp:TextBox ID="txtEmpNewFullNameTHHidden" runat="server" Visible="false" />--%>
                                                                    <asp:TextBox ID="txtcard_number_doctor_detail_Profile" Text='<%# Eval("card_number") %>' runat="server" CssClass="form-control"
                                                                        placeholder="เลขที่ใบประกอบวิชาชีพ..." Enabled="false" />
                                                                </div>

                                                            </div>

                                                            <div class="form-group">
                                                                <div class="col-md-6">
                                                                    <label>ชื่อหน่วยงานที่ตรวจสุขภาพ</label>
                                                                    <asp:TextBox ID="txt_health_authority_name_detail_Profile" runat="server" Text='<%# Eval("health_authority_name") %>' CssClass="form-control" placeholder="ชื่อหน่วยงานที่ตรวจสุขภาพ..." Enabled="false" />

                                                                </div>
                                                                <div class="col-md-2">
                                                                    <label>ตั้งอยู่เลขที่</label>
                                                                    <asp:TextBox ID="txt_location_name_detail_Profile" runat="server" Text='<%# Eval("location_name") %>' CssClass="form-control"
                                                                        placeholder="ตั้งอยู่เลขที่..." Enabled="false" />

                                                                </div>

                                                                <div class="col-md-2">
                                                                    <label>หมู่ที่</label>
                                                                    <asp:TextBox ID="txt_village_no_detail_Profile" runat="server" Text='<%# Eval("village_no") %>' CssClass="form-control"
                                                                        placeholder="หมู่ที่..." Enabled="false" />

                                                                </div>

                                                                <div class="col-md-2">
                                                                    <label>ถนน</label>
                                                                    <asp:TextBox ID="txt_road_name_detail_Profile" runat="server" Text='<%# Eval("road_name") %>' CssClass="form-control"
                                                                        placeholder="ถนน..." Enabled="false" />

                                                                </div>
                                                            </div>

                                                        </div>

                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>

                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label>ตรวจสุขภาพทั่วไป</label>
                                                </div>
                                                <asp:UpdatePanel ID="Update_GenaralHealth_Detail_Profile" runat="server">
                                                    <ContentTemplate>
                                                        <div class="col-md-12">
                                                            <div id="div_genaralHealthCheck_detail_Profile" class="panel panel-default" runat="server">

                                                                <div class="panel-body">
                                                                    <div class="form-group">
                                                                        <div class="col-md-12">
                                                                            <label>ผลการตรวจเบื้องต้น</label>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">

                                                                        <div class="col-md-4">
                                                                            <label>น้ำหนัก</label>
                                                                            <asp:TextBox ID="txt_weight_detail_Profile" runat="server" Text='<%# Eval("weight") %>' CssClass="form-control"
                                                                                placeHolder="น้ำหนัก..." Enabled="false">
                                                                            </asp:TextBox>

                                                                        </div>
                                                                        <div class="col-md-2">
                                                                            <label>&nbsp;</label>
                                                                            <asp:TextBox ID="txt_unit_weight_detail_Profile" runat="server" Text="กิโลกรัม"
                                                                                CssClass="form-control" Enabled="false">
                                                                            </asp:TextBox>

                                                                        </div>


                                                                        <div class="col-md-4">
                                                                            <label>ความสูง</label>
                                                                            <asp:TextBox ID="txt_height_detail_Profile" runat="server" Text='<%# Eval("height") %>' CssClass="form-control"
                                                                                placeHolder="ความสูง..." Enabled="false">
                                                                            </asp:TextBox>

                                                                        </div>

                                                                        <div class="col-md-2">
                                                                            <label>&nbsp;</label>
                                                                            <asp:TextBox ID="txt_unit_height_detail_Profile" runat="server" Text="เซนติเมตร"
                                                                                CssClass="form-control" Enabled="false">
                                                                            </asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">

                                                                        <div class="col-md-2">
                                                                            <label>ดัชนีมวลกาย</label>
                                                                            <asp:TextBox ID="txt_body_mass_detail_Profile_Profile" runat="server" Text='<%# Eval("body_mass") %>' CssClass="form-control"
                                                                                placeHolder="ดัชนีมวลกาย..." Enabled="false">
                                                                            </asp:TextBox>

                                                                        </div>

                                                                        <div class="col-md-3">
                                                                            <label>ความดันโลหิต</label>
                                                                            <asp:TextBox ID="txt_bloodpressure_detail_Profile_Profile" runat="server" Text='<%# Eval("bloodpressure") %>' CssClass="form-control"
                                                                                placeHolder="ความดันโลหิต..." Enabled="false">
                                                                            </asp:TextBox>

                                                                        </div>

                                                                        <div class="col-md-2">
                                                                            <label>&nbsp;</label>
                                                                            <asp:TextBox ID="txt_bloodpressure_unit_detail_Profile_Profile" runat="server" Text="mm.Hg"
                                                                                CssClass="form-control" Enabled="false">
                                                                            </asp:TextBox>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <label>ชีพจร</label>
                                                                            <asp:TextBox ID="txt_pulse_detail_Profile" runat="server" Text='<%# Eval("pulse") %>' CssClass="form-control"
                                                                                placeHolder="ชีพจร..." Enabled="false">
                                                                            </asp:TextBox>

                                                                        </div>

                                                                        <div class="col-md-2">
                                                                            <label>&nbsp;</label>
                                                                            <asp:TextBox ID="txt_pulse_unit_detail_Profile" runat="server" Text="ครั้ง/นาที"
                                                                                CssClass="form-control" Enabled="false">
                                                                            </asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <div class="col-md-12">
                                                                            <label>ผลการตรวจร่างกายตามระบบ</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="col-md-12">
                                                                            <label>ระบุ</label>
                                                                            <asp:TextBox ID="txt_specify_resultbody_detail_Profile" runat="server" Text='<%# Eval("specify_resultbody") %>'
                                                                                CssClass="form-control" placeHolder="ระบุผลการตรวจร่างกายตามระบบ..." Enabled="false">
                                                                            </asp:TextBox>
                                                                        </div>

                                                                    </div>

                                                                    <div class="form-group">
                                                                        <div class="col-md-12">
                                                                            <label>ผลการตรวจทางห้องปฏิบัติการ</label>
                                                                            <asp:TextBox ID="txt_resultlab_detail_Profile" runat="server" Text='<%# Eval("resultlab") %>' CssClass="form-control"
                                                                                placeHolder="ระบุผลการตรวจทางห้องปฏิบัติการ..." Enabled="false">
                                                                            </asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                            </div>
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label>ตรวจสุขภาพตามปัจจัยเสี่ยงของงาน</label>
                                                </div>
                                                <asp:UpdatePanel ID="Update_RikeHealthCheck_Detail_Profile" runat="server">
                                                    <ContentTemplate>
                                                        <div class="col-md-12">
                                                            <div id="div_rikeHealthCheck_detail_Profile" class="panel panel-default" runat="server">
                                                                <div class="panel-body">

                                                                    <table class="table table-striped f-s-12 table-empshift-responsive">
                                                                        <asp:Repeater ID="rpt_RikeDetail_Profile" runat="server">
                                                                            <HeaderTemplate>
                                                                                <tr>
                                                                                    <th>ปัจจัยเสี่ยง</th>
                                                                                    <th>ผลการตรวจ</th>
                                                                                </tr>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <tr>
                                                                                    <td data-th="ปัจจัยเสี่ยง"><%# Eval("rike_health") %></td>
                                                                                    <td data-th="ผลการตรวจ"><%# Eval("result_health") %></td>

                                                                                </tr>
                                                                            </ItemTemplate>
                                                                        </asp:Repeater>
                                                                    </table>

                                                                    <div class="clearfix"></div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>

                                            </div>

                                        </div>

                                    </div>


                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:FormView>
            </div>


        </asp:View>
        <!--View History-->
        <!--View Detail-->
        <asp:View ID="docDetailList" runat="server">

            <asp:UpdatePanel ID="Update_BackTohistoryDetail" runat="server" Visible="false" class="col-md-12">
                <ContentTemplate>
                    <div class="form-group">
                        <asp:LinkButton ID="btnCancelToHistoryDetail" CssClass="btn btn-danger" data-toggle="tooltip" title="" runat="server"
                            CommandName="cmdCancelToHistoryDetail" OnCommand="btnCommand">< ย้อนกลับ</asp:LinkButton>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

            <div class="clearfix"></div>
            <div id="div_SearchHistoryEmployee" runat="server" class="col-md-12" visible="false">
                <asp:FormView ID="fvSearchHistoryEmployee" runat="server" DefaultMode="Insert" Width="100%">
                    <InsertItemTemplate>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">ค้นหาชื่อพนักงานที่ทำการกรอกข้อมูล</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">รหัสพนักงาน</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpCode_HistorySearch" runat="server" CssClass="form-control" MaxLength="8" placeholder="รหัสพนักงาน" />
                                        </div>
                                        <label class="col-sm-2 control-label">ชื่อพนักงาน(TH/EN)</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpName_HistorySearch" runat="server" CssClass="form-control" placeholder="ชื่อพนักงาน(TH/EN)" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2"></label>
                                        <div class="col-sm-4">
                                            <asp:LinkButton ID="lbSearchHistoryEmployee" CssClass="btn btn-success" runat="server" data-original-title="Search" data-toggle="tooltip" Text="ค้นหา" OnCommand="btnCommand" CommandName="cmdSearchHistoryEmployee"></asp:LinkButton>
                                            <asp:LinkButton ID="lbResetSearchHistoryEmployee" CssClass="btn btn-info" runat="server" data-original-title="Reset" data-toggle="tooltip" Text="ล้างค่า" OnCommand="btnCommand" CommandName="cmdResetSearchHistoryEmployee"></asp:LinkButton>
                                        </div>
                                    </div>

                                </div>
                            </div>
                    </InsertItemTemplate>
                </asp:FormView>
            </div>

            <div id="div_gvEmployeeDetail" runat="server" class="col-md-12">
                <asp:GridView ID="gvEmployeeDetail"
                    runat="server"
                    AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered table-responsive"
                    HeaderStyle-CssClass="info"
                    AllowPaging="true"
                    PageSize="10"
                    OnPageIndexChanging="gvPageIndexChanging"
                    OnRowDataBound="gvRowDataBound">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                    <EmptyDataTemplate>
                        <div class="text-center">-- ไม่พบข้อมูล -- </div>
                    </EmptyDataTemplate>
                    <Columns>

                        <asp:TemplateField HeaderText="#" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" ItemStyle-CssClass="text-center" HeaderStyle-Width="5%">
                            <ItemTemplate>
                                <small>
                                    <%# (Container.DataItemIndex + 1) %>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="รหัสพนักงาน" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="lbl_emp_idx_detail" runat="server" Visible="false" Text='<%# Eval("emp_idx")%>' />
                                    <asp:Label ID="lbl_emp_code_detail" runat="server" Text='<%# Eval("emp_code")%>' />

                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ชื่อพนักงาน" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="lbl_emp_firstname_th_detail" runat="server" Text='<%# Eval("emp_firstname_th")%>' />
                                    <asp:Label ID="lbl_emp_lastname_th_detail" runat="server" Text='<%# Eval("emp_lastname_th")%>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="30%">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="Detail_Employee" runat="server"> 
                                    <p><b>ฝ่าย :</b> <%# Eval("dept_name_th") %></p>
                                    <p><b>แผนก :</b> <%# Eval("sec_name_th") %></p>
                                    <p><b>ตำแหน่ง :</b> <%# Eval("pos_name_th") %></p>
                                    </asp:Label>
                                </small>

                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="25%">
                            <ItemTemplate>
                                <small>
                                    <asp:LinkButton ID="btnViewHistoryEmployee" CssClass="btn btn-info btn-sm" runat="server" CommandName="cmdViewHistoryEmployee"
                                        OnCommand="btnCommand" CommandArgument='<%# Eval("emp_idx") + ";" + "5"%>'
                                        data-toggle="tooltip" title="ประวัติส่วนตัว"><i class="fa fa-user-md"></i>
                                    </asp:LinkButton>

                                    <asp:LinkButton ID="btnViewHistoryWork" CssClass="btn btn-warning btn-sm" runat="server" CommandName="cmdViewHistoryWork"
                                        OnCommand="btnCommand" CommandArgument='<%# Eval("emp_idx") + ";" + "1"%>'
                                        data-toggle="tooltip" title="ประวัติการทำงาน"><i class="fa fa-briefcase"></i>
                                    </asp:LinkButton>

                                    <asp:LinkButton ID="btnViewHistorySick" CssClass="btn btn-primary btn-sm" runat="server" CommandName="cmdViewHistorySick"
                                        OnCommand="btnCommand" CommandArgument='<%# Eval("emp_idx") + ";" + "2"%>'
                                        data-toggle="tooltip" title="ประวัติการเจ็บป่วย"><i class="fa fa-medkit"></i>
                                    </asp:LinkButton>

                                    <asp:LinkButton ID="btnViewHistoryHealth" CssClass="btn btn-danger btn-sm" runat="server" CommandName="cmdViewHistoryHealth"
                                        OnCommand="btnCommand" CommandArgument='<%# Eval("emp_idx") + ";" + "3"%>'
                                        data-toggle="tooltip" title="การตรวจสุขภาพ"><i class="fa fa-heartbeat"></i>
                                    </asp:LinkButton>

                                    <asp:LinkButton ID="btnViewHistoryInjury" CssClass="btn btn-success btn-sm" runat="server" CommandName="cmdViewHistoryInjury"
                                        OnCommand="btnCommand" CommandArgument='<%# Eval("emp_idx") + ";" + "4"%>'
                                        data-toggle="tooltip" title="บันทึกการบาดเจ็บ"><i class="fa fa-ambulance"></i>
                                    </asp:LinkButton>

                                </small>

                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>
            </div>

            <div id="divDetailHistory" runat="server" class="col-md-12" visible="false">

                <div class="form-group">
                    <%--<asp:LinkButton ID="btnPrintHistoryEmployee" runat="server" CommandName="cmdPrint" CssClass="btn btn-default" OnCommand="btnCommand" CommandArgument="HistoryEmployee" data-toggle="tooltip" title="พิมพ์"><i class="fa fa-print"></i> <b>พิมพ์</b></asp:LinkButton>--%>

                    <asp:LinkButton ID="btnPrintHistoryEmployee" CssClass="btn btn-primary" runat="server" CommandName="cmdPrintHistoryEmp" Visible="false" OnCommand="btnCommand"
                        Text="<i class='fa fa-print'></i> พิมพ์" OnClientClick="return printDiv('printableAreaHistoryEmp');" />
                </div>

                <div id="printableAreaHistoryEmp">
                    <asp:FormView ID="fvHistoryEmployee" runat="server" DefaultMode="ReadOnly" Width="100%">
                        <ItemTemplate>
                            <div class="panel panel-default">

                                <%-- <div class="panel-heading">
                                    <h3 class="panel-title">ข้อมูลส่วนตัว</h3>
                                </div>--%>

                                <table class="table f-s-12 m-t-10" style="border: 0px">
                                    <h3 style="text-align: center;">ประวัติส่วนตัว</h3>
                                    <tr>
                                        <td style="text-align: left; border: 0px"><b>
                                            <h5>รหัสพนักงาน :</h5>
                                        </b></td>
                                        <td style="text-align: left; border: 0px">
                                            <h5><%# Eval("EmpCode") %></h5>
                                        </td>

                                        <td style="text-align: left; border: 0px"><b>
                                            <h5>ชื่อ-นามสกุล :</h5>
                                        </b></td>
                                        <td style="text-align: left; border: 0px">
                                            <h5><%# Eval("FullNameTH") %></h5>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="text-align: left; border: 0px"><b>
                                            <h5>วัน เดือน ปี เกิด :</h5>
                                        </b></td>
                                        <td style="text-align: left; border: 0px">
                                            <h5><%# Eval("Birthday_Ex") %></h5>
                                        </td>

                                        <td style="text-align: left; border: 0px"><b>
                                            <h5>เพศ :</h5>
                                        </b></td>
                                        <td style="text-align: left; border: 0px">
                                            <h5><%# Eval("SexNameTH") %><h5>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="text-align: left; border: 0px"><b>
                                            <h5>วันที่เข้างาน :</h5>
                                        </b></td>
                                        <td style="text-align: left; border: 0px">
                                            <h5><%# Eval("EmpIN_Ex") %></h5>
                                        </td>
                                    </tr>

                                    <br />

                                    <tr>
                                        <td style="text-align: left; border: 0px"><b>
                                            <h5>1. เลขที่บัตรประชาชน :</h5>
                                        </b></td>
                                        <td style="text-align: left; border: 0px">
                                            <h5><%# Eval("IdentityCard") %></h5>
                                        </td>
                                    </tr>

                                    <br />
                                    <tr>
                                        <td style="text-align: left; border: 0px"><b>
                                            <h5>2. ที่อยู่ตามบัตรประชาชน เลขที่ : </h5>
                                        </b></td>
                                        <td style="text-align: left; border: 0px">
                                            <h5><%# Eval("EmpAddr") %></h5>
                                        </td>
                                        <%--<td style="text-align: left; border: 0px"><%# Eval("EmpAddr") %></td>--%>
                                    </tr>

                                    <tr>
                                        <td style="text-align: left; border: 0px"><b>
                                            <h5>ถนน :</h5>
                                        </b></td>
                                        <td style="text-align: left; border: 0px">
                                            <%-- <h5><%# Eval("EmpAddr") %></h5>--%>
                                        </td>

                                        <td style="text-align: left; border: 0px"><b>
                                            <h5>ตำบล(แขวง) :</h5>
                                        </b></td>
                                        <td style="text-align: left; border: 0px">
                                            <h5><%# Eval("DistName") %></h5>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="text-align: left; border: 0px"><b>
                                            <h5>อำเภอ(เขต) :</h5>
                                        </b></td>
                                        <td style="text-align: left; border: 0px">
                                            <h5><%# Eval("AmpName") %></h5>
                                        </td>

                                        <td style="text-align: left; border: 0px"><b>
                                            <h5>จังหวัด :</h5>
                                        </b></td>
                                        <td style="text-align: left; border: 0px">
                                            <h5><%# Eval("ProvName") %></h5>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="text-align: left; border: 0px"><b>
                                            <h5>รหัสไปรษณีย์ :</h5>
                                        </b></td>
                                        <td style="text-align: left; border: 0px">
                                            <h5><%# Eval("PostCode") %></h5>
                                        </td>

                                        <td style="text-align: left; border: 0px"><b>
                                            <h5>โทรศัพท์ :</h5>
                                        </b></td>
                                        <td style="text-align: left; border: 0px">
                                            <h5><%# Eval("MobileNo") %></h5>
                                        </td>
                                        <%--<td style="text-align: left; border: 0px"><%# Eval("MobileNo") %></td>--%>
                                    </tr>

                                    <br />
                                    <tr>
                                        <td style="text-align: left; border: 0px"><b>
                                            <h5>3. ที่อยู่ที่สามารถติดต่อได้ เลขที่ :</h5>
                                        </b></td>
                                        <td style="text-align: left; border: 0px">
                                            <h5><%# Eval("EmpAddr") %></h5>
                                        </td>
                                        <%-- <td style="text-align: left; border: 0px"><h5><%# Eval("EmpAddr") %></h5></td>--%>
                                    </tr>

                                    <tr>
                                        <td style="text-align: left; border: 0px"><b>
                                            <h5>ถนน :</h5>
                                        </b></td>
                                        <td style="text-align: left; border: 0px">
                                            <%--<h5><%# Eval("EmpAddr") %></h5>--%>
                                        </td>

                                        <td style="text-align: left; border: 0px"><b>
                                            <h5>ตำบล(แขวง) :</h5>
                                        </b></td>
                                        <td style="text-align: left; border: 0px">
                                            <h5><%# Eval("DistName") %></h5>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="text-align: left; border: 0px"><b>
                                            <h5>อำเภอ(เขต) :</h5>
                                        </b></td>
                                        <td style="text-align: left; border: 0px">
                                            <h5><%# Eval("AmpName") %></h5>
                                        </td>

                                        <td style="text-align: left; border: 0px"><b>
                                            <h5>จังหวัด :</h5>
                                        </b></td>
                                        <td style="text-align: left; border: 0px">
                                            <h5><%# Eval("ProvName") %></h5>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="text-align: left; border: 0px"><b>
                                            <h5>รหัสไปรษณีย์ :</h5>
                                        </b></td>
                                        <td style="text-align: left; border: 0px">
                                            <h5><%# Eval("PostCode") %></h5>
                                        </td>

                                        <td style="text-align: left; border: 0px"><b>
                                            <h5>โทรศัพท์ :</h5>
                                        </b></td>
                                        <td style="text-align: left; border: 0px">
                                            <h5><%# Eval("MobileNo") %></h5>
                                        </td>
                                    </tr>

                                    <br />
                                    <tr>
                                        <td style="text-align: left; border: 0px"><b>
                                            <h5>4. สถานประกอบการ :</h5>
                                        </b></td>
                                        <%--<td style="text-align: left; border: 0px"><%# Eval("OrgAddress") %></td>--%>
                                    </tr>

                                    <tr>
                                        <td style="text-align: left; border: 0px"><b>
                                            <h5>ที่อยู่ :</h5>
                                        </b></td>
                                        <td style="text-align: left; border: 0px">
                                            <h5><%# Eval("OrgAddress") %></h5>
                                        </td>

                                        <td style="text-align: left; border: 0px"><b>
                                            <h5>รหัสไปรษณีย์ :</h5>
                                        </b></td>
                                        <td style="text-align: left; border: 0px">
                                            <h5><%# Eval("PostCode_Org") %></h5>
                                        </td>
                                    </tr>

                                </table>
                            </div>
                        </ItemTemplate>
                    </asp:FormView>
                </div>
            </div>

            <asp:UpdatePanel ID="Update_CancelEditHistory" runat="server" Visible="false" class="col-md-12">
                <ContentTemplate>
                    <div class="form-group">
                        <asp:LinkButton ID="btnCancelEditHistory" CssClass="btn btn-danger" data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                            CommandName="cmdCancelEditHistory" OnCommand="btnCommand">< ย้อนกลับ</asp:LinkButton>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

            <div id="divHistoryWork" runat="server" class="col-md-12" visible="false">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">ประวัติการทำงาน (ตั้งแต่อดีตถึงปัจจุบัน)</h3>
                    </div>

                    <div class="panel-body">

                        <div class="form-group">

                            <asp:LinkButton ID="btnPrintHistoryWork1" CssClass="btn btn-primary" CommandName="cmdPrintHistoryWork" OnCommand="btnCommand" Visible="false" CommandArgument="HistoryWork" runat="server" Text="<i class='fa fa-print'></i> พิมพ์" OnClientClick="return printDiv('divprintableAreaHistoryWork');" />

                            <div id="divprintableAreaHistoryWork" class="preview_print hidden">
                                <h3 style="text-align: center;">ประวัติการทำงาน</h3>
                                <asp:GridView ID="gvPrintWork"
                                    runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="table table-striped table-bordered table-responsive"
                                    HeaderStyle-CssClass="info"
                                    AllowPaging="true"
                                    PageSize="10"
                                    OnPageIndexChanging="gvPageIndexChanging"
                                    OnRowDataBound="gvRowDataBound">
                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                    <EmptyDataTemplate>
                                        <div class="text-center">-- ไม่พบข้อมูล --</div>
                                    </EmptyDataTemplate>
                                    <Columns>

                                        <asp:TemplateField HeaderText="ชื่อสถานประกอบกิจการ/แผนก" ItemStyle-HorizontalAlign="Left"
                                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="Detail_company_name1" runat="server"> 
                                                    <p><b>ชื่อสถานประกอบกิจการ :</b> <%# Eval("company_name") %></p>
                                                    <p><b>แผนก :</b> <%# Eval("department_name") %></p>
                                  
                                                    </asp:Label>

                                                </small>
                                            </ItemTemplate>
                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ประเภทกิจการ" ItemStyle-HorizontalAlign="Left"
                                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lbl_business_type_history1" runat="server" Text='<%# Eval("business_type") %>' />
                                                </small>

                                            </ItemTemplate>
                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ลักษณะงานที่ทำ" ItemStyle-HorizontalAlign="Left"
                                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lbl_job_description_history1" runat="server" Text='<%# Eval("job_description") %>' />
                                                </small>

                                            </ItemTemplate>
                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ระยะเวลาที่ทำ" ItemStyle-HorizontalAlign="Left"
                                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="Detail_DateWork1" runat="server"> 
                                                    <p><b>วัน/เดือน/ปี ที่เริ่ม :</b> <%# Eval("job_startdate") %></p>
                                                    <p><b>วัน/เดือน/ปี ที่สิ้นสุด :</b> <%# Eval("job_enddate") %></p>
                                                    </asp:Label>
                                                </small>

                                            </ItemTemplate>
                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ปัจจัยที่เสี่ยงต่อสุขภาพ" ItemStyle-HorizontalAlign="Left"
                                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lbl_risk_health_history1" runat="server" Text='<%#Eval("risk_health") %>' />
                                                </small>

                                            </ItemTemplate>
                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="มี/ใช้อุปกรณ์ป้องกันอันตราย" ItemStyle-HorizontalAlign="Left"
                                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lbl_protection_equipment_history1" runat="server" Text='<%#Eval("protection_equipment") %>' />
                                                </small>

                                            </ItemTemplate>
                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>


                                    </Columns>
                                </asp:GridView>
                            </div>
                            <div id="printableAreaHistoryWork_print" class="preview_print">
                                <h3 style="text-align: center;">ประวัติการทำงาน</h3>

                                <div id="gvWork_scroll" style="overflow-x: scroll; width: 100%" runat="server">
                                    <asp:GridView ID="gvWork"
                                        runat="server"
                                        AutoGenerateColumns="false"
                                        CssClass="table table-striped table-bordered table-responsive"
                                        HeaderStyle-CssClass="info"
                                        AllowPaging="true"
                                        PageSize="10"
                                        OnPageIndexChanging="gvPageIndexChanging"
                                        OnRowDataBound="gvRowDataBound">
                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                        <EmptyDataTemplate>
                                            <div class="text-center">-- ไม่พบข้อมูล --</div>
                                        </EmptyDataTemplate>
                                        <Columns>

                                            <asp:TemplateField HeaderText="ชื่อสถานประกอบกิจการ/แผนก" ItemStyle-HorizontalAlign="Left"
                                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Label ID="Detail_company_name" runat="server"> 
                                                    <p><b>ชื่อสถานประกอบกิจการ :</b> <%# Eval("company_name") %></p>
                                                    <p><b>แผนก :</b> <%# Eval("department_name") %></p>
                                  
                                                        </asp:Label>

                                                    </small>
                                                </ItemTemplate>
                                                <EditItemTemplate />
                                                <FooterTemplate />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ประเภทกิจการ" ItemStyle-HorizontalAlign="Left"
                                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Label ID="lbl_business_type_history" runat="server" Text='<%# Eval("business_type") %>' />
                                                    </small>

                                                </ItemTemplate>
                                                <EditItemTemplate />
                                                <FooterTemplate />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ลักษณะงานที่ทำ" ItemStyle-HorizontalAlign="Left"
                                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Label ID="lbl_job_description_history" runat="server" Text='<%# Eval("job_description") %>' />
                                                    </small>

                                                </ItemTemplate>
                                                <EditItemTemplate />
                                                <FooterTemplate />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ระยะเวลาที่ทำ" ItemStyle-HorizontalAlign="Left"
                                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Label ID="Detail_DateWork" runat="server"> 
                                                    <p><b>วัน/เดือน/ปี ที่เริ่ม :</b> <%# Eval("job_startdate") %></p>
                                                    <p><b>วัน/เดือน/ปี ที่สิ้นสุด :</b> <%# Eval("job_enddate") %></p>
                                                        </asp:Label>
                                                    </small>

                                                </ItemTemplate>
                                                <EditItemTemplate />
                                                <FooterTemplate />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ปัจจัยที่เสี่ยงต่อสุขภาพ" ItemStyle-HorizontalAlign="Left"
                                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Label ID="lbl_risk_health_history" runat="server" Text='<%#Eval("risk_health") %>' />
                                                    </small>

                                                </ItemTemplate>
                                                <EditItemTemplate />
                                                <FooterTemplate />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="มี/ใช้อุปกรณ์ป้องกันอันตราย" ItemStyle-HorizontalAlign="Left"
                                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Label ID="lbl_protection_equipment_history" runat="server" Text='<%#Eval("protection_equipment") %>' />
                                                    </small>

                                                </ItemTemplate>
                                                <EditItemTemplate />
                                                <FooterTemplate />
                                            </asp:TemplateField>

                                            <asp:TemplateField ItemStyle-HorizontalAlign="center" HeaderText="Action" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small"
                                                HeaderStyle-Width="10%">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="btnUpdateHistoryWork" CssClass="btn btn-warning btn-sm" runat="server" CommandName="cmdUpdateHistoryWork" OnCommand="btnCommand" data-toggle="tooltip" title="แก้ไข" CommandArgument='<%# Eval("u2_historywork_idx") %>'>
                                                <i class="fa fa-pencil"></i>
                                                    </asp:LinkButton>

                                                    <asp:LinkButton ID="btnDelHistoryWorkList" CssClass="btn btn-danger btn-sm" runat="server"
                                                        CommandName="cmdDelHistoryWorkList" OnCommand="btnCommand" data-toggle="tooltip" title="ลบ" CommandArgument='<%# Eval("u2_historywork_idx") %>'
                                                        OnClientClick="return confirm('คุณต้องการลบข้อมูลนี้ใช่หรือไม่')">
                                                <i class="fa fa-trash"></i>
                                                    </asp:LinkButton>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div id="div_fvEditHistoryWork" runat="server" class="col-md-12" visible="false">
                <asp:FormView ID="fvEditHistoryWork" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                    <EditItemTemplate>
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">แก้ไขประวัติการทำงาน</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <h3 class="text-center">
                                                <asp:Label ID="lbFormName_Injuryedit" runat="server" Text="แก้ไขประวัติการทำงาน" CssClass="control-label" Font-Bold="true" Font-Size="14"></asp:Label>
                                            </h3>
                                        </div>
                                    </div>
                                    <div id="div_DetailHistoryWork_edit" class="panel panel-default" runat="server">
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <div class="col-md-6">
                                                    <label>ชื่อสถานประกอบการ</label>
                                                    <asp:TextBox ID="txt_business_work_edit" runat="server" Text='<%# Eval("company_name") %>' CssClass="form-control" Enabled="true">
                                                    </asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="Re_txt_business_work"
                                                        runat="server"
                                                        ControlToValidate="txt_business_work_edit" Display="None"
                                                        SetFocusOnError="true"
                                                        ErrorMessage="*กรุณากรอกชื่อสถานประกอบการ"
                                                        ValidationGroup="AddHistoryWorkList" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="Validator_txt_business_work" runat="Server" PopupPosition="BottomLeft"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_business_work" Width="180" />
                                                </div>

                                                <div class="col-md-6">

                                                    <label>แผนก</label>
                                                    <asp:TextBox ID="txt_Department_work_edit" runat="server" Text='<%# Eval("department_name") %>' CssClass="form-control" Enabled="true">
                                                    </asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="Re_txt_Department_work"
                                                        runat="server"
                                                        ControlToValidate="txt_Department_work_edit" Display="None"
                                                        SetFocusOnError="true"
                                                        ErrorMessage="*กรุณากรอกแผนก"
                                                        ValidationGroup="AddHistoryWorkList" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="Validator_txt_Department_work" runat="Server" PopupPosition="BottomLeft"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_Department_work" Width="180" />
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-6">
                                                    <label>ประเภทกิจการ</label>
                                                    <asp:TextBox ID="txt_business_typework_edit" runat="server" Text='<%# Eval("business_type") %>' CssClass="form-control" Enabled="true">
                                                    </asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="Re_txt_business_typework"
                                                        runat="server"
                                                        ControlToValidate="txt_business_typework_edit" Display="None"
                                                        SetFocusOnError="true"
                                                        ErrorMessage="*กรุณากรอกประเภทกิจการ"
                                                        ValidationGroup="AddHistoryWorkList" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="Validator_txt_business_typework" runat="Server" PopupPosition="BottomLeft"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_business_typework" Width="180" />
                                                </div>

                                                <div class="col-md-6">

                                                    <label>ลักษณะงานที่ทำ</label>
                                                    <asp:TextBox ID="txt_jobdescription_work_edit" runat="server" Text='<%# Eval("job_description") %>' CssClass="form-control" Enabled="true">
                                                    </asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="Re_txt_jobdescription_work"
                                                        runat="server"
                                                        ControlToValidate="txt_jobdescription_work_edit" Display="None"
                                                        SetFocusOnError="true"
                                                        ErrorMessage="*กรุณากรอกลักษณะงานที่ทำ"
                                                        ValidationGroup="AddHistoryWorkList" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="Validator_txt_jobdescription_work" runat="Server" PopupPosition="BottomLeft"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_jobdescription_work" Width="180" />
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-6">
                                                    <label>วัน/เดือน/ปี ที่เริ่มทำ</label>
                                                    <div class='input-group date'>
                                                        <asp:TextBox ID="txt_StartDate_work_edit" runat="server" Text='<%# Eval("job_startdate") %>' CssClass="form-control datetimepicker-filter-perm-from cursor-pointer" Enabled="true">
                                                        </asp:TextBox>
                                                        <span class="input-group-addon show-filter-perm-from-onclick">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <asp:RequiredFieldValidator ID="Re_txt_StartDate_work"
                                                            runat="server"
                                                            ControlToValidate="txt_StartDate_work_edit" Display="None"
                                                            SetFocusOnError="true"
                                                            ErrorMessage="*กรุณากรอก วัน/เดือน/ปี ที่เริ่มทำ"
                                                            ValidationGroup="AddHistoryWorkList" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="Validator_txt_StartDate_work" runat="Server" PopupPosition="BottomLeft"
                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_StartDate_work" Width="180" />
                                                    </div>
                                                </div>

                                                <div class="col-md-6">

                                                    <label>วัน/เดือน/ปี ที่สิ้นสุด</label>
                                                    <div class='input-group date'>
                                                        <asp:TextBox ID="txt_EndDate_work_edit" runat="server" Text='<%# Eval("job_enddate") %>' CssClass="form-control datetimepicker-filter-perm-to cursor-pointer" Enabled="true">
                                                        </asp:TextBox>
                                                        <span class="input-group-addon show-filter-perm-to-onclick">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <asp:RequiredFieldValidator ID="Re_txt_EndDate_work"
                                                            runat="server"
                                                            ControlToValidate="txt_EndDate_work_edit" Display="None"
                                                            SetFocusOnError="true"
                                                            ErrorMessage="*กรุณากรอก วัน/เดือน/ปี ที่สิ้นสุด"
                                                            ValidationGroup="AddHistoryWorkList" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="Validator_txt_EndDate_work" runat="Server" PopupPosition="BottomLeft"
                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_EndDate_work" Width="180" />
                                                    </div>


                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-6">
                                                    <label>ปัจจัยที่เสี่ยงต่อสุขภาพ</label>
                                                    <asp:TextBox ID="txt_HealthRisk_work_edit" runat="server" Text='<%# Eval("risk_health") %>' CssClass="form-control" Enabled="true">
                                                    </asp:TextBox>

                                                </div>

                                                <div class="col-md-6">

                                                    <label>มี/ใช้อุปกรณ์ป้องกันอันตราย (ระบุชนิด)</label>
                                                    <asp:TextBox ID="txt_UseMaterial_work_edit" runat="server" Text='<%# Eval("protection_equipment") %>' CssClass="form-control" Enabled="true">
                                                    </asp:TextBox>

                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <asp:LinkButton ID="btnEditHistoryWorkCheck" CssClass="btn btn-success" runat="server" OnCommand="btnCommand"
                                                        CommandName="cmdSaveEditHistoryWork" Text="บันทึกการเปลี่ยนแปลง" CommandArgument='<%# Eval("u2_historywork_idx") %>' ValidationGroup="btnSave"
                                                        OnClientClick="return confirm('ยืนยันการบันทึก')" />
                                                    <asp:LinkButton CssClass="btn btn-danger" runat="server" OnCommand="btnCommand"
                                                        CommandName="cmdCancelEditHistory" Text="ยกเลิก" />
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </EditItemTemplate>
                </asp:FormView>
            </div>

            <div id="divHistorySick" runat="server" class="col-md-12" visible="false">

                <div class="form-group" id="div_btnedit_Historysick" runat="server" visible="false">
                    <asp:LinkButton ID="btnEditHistorySick" CssClass="btn btn-default" data-toggle="tooltip" title="แก้ไข" runat="server"
                        CommandName="cmdEditHistorySick" CommandArgument='<%# Eval("u2_historysick_idx") %>' OnCommand="btnCommand">แก้ไขประวัติการเจ็บป่วย</asp:LinkButton>

                    <asp:LinkButton ID="btnPrintHistorySick" CssClass="btn btn-primary" runat="server" Visible="false"
                        Text="<i class='fa fa-print'></i> พิมพ์" OnClientClick="return printDiv('printableAreaHistorySick');" />


                </div>



                <div id="printableAreaHistorySick">
                    <asp:FormView ID="fvSick" runat="server" DefaultMode="ReadOnly" OnDataBound="FvDetail_DataBound" Width="100%">
                        <ItemTemplate>
                            <div class="panel panel-default">
                                <table class="table f-s-12 m-t-10" style="border: 0px">
                                    <h3 style="text-align: center;">ประวัติการเจ็บป่วย</h3>
                                    <tr>
                                        <td style="text-align: left; border: 0px"><b>
                                            <h5>๑. เคยป่วยเป็นโรคหรือมีการบาดเจ็บ</h5>
                                        </b></td>

                                    </tr>

                                    <tr>
                                        <td style="text-align: left; border: 0px">
                                            <asp:UpdatePanel ID="update_eversick_hr" runat="server">
                                                <ContentTemplate>
                                                    <div class="col-md-12">
                                                        <div id="div_DetailEverSickCheck_hr" class="panel panel-default" runat="server">
                                                            <div class="panel-body">
                                                                <table class="table table-striped f-s-12 table-empshift-responsive">
                                                                    <asp:Repeater ID="rpt_eversick" runat="server">
                                                                        <HeaderTemplate>
                                                                            <tr>
                                                                                <th>รายละเอียด</th>
                                                                                <th>เมื่อปี พ.ศ.</th>
                                                                            </tr>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <tr>
                                                                                <td data-th="รายละเอียด"><%# Eval("eversick") %></td>
                                                                                <td data-th="เมื่อปี พ.ศ."><%# Eval("sick_year") %></td>

                                                                            </tr>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </table>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="text-align: left; border: 0px"><b>
                                            <h5>๒. มีโรคประจำตัวหรือโรคเรื้อรังหรือไม่ :</h5>
                                        </b></td>
                                    </tr>

                                    <tr>
                                        <td style="text-align: left; border: 0px">
                                            <h5 style="padding-left: 25px;"><%# Eval("havedisease") %></h5>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="text-align: left; border: 0px"><b>
                                            <h5>๓. เคยได้รับการผ่าตัดหรือไม่ :</h5>
                                        </b></td>

                                    </tr>

                                    <tr>
                                        <td style="text-align: left; border: 0px">
                                            <h5 style="padding-left: 25px;"><%# Eval("surgery") %></h5>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="text-align: left; border: 0px"><b>
                                            <h5>๔. เคยได้รับภูมิคุ้มกันโรคกรณีเกิดโรคระบาด หรือเพื่อป้องกันโรคติดต่อหรือไม่ :</h5>
                                        </b></td>
                                    </tr>

                                    <tr>
                                        <td style="text-align: left; border: 0px"><b>
                                            <h5 style="padding-left: 25px;">ระบุ : <%# Eval("immune") %></h5>
                                        </b></td>
                                        <%--<td style="text-align: left; border: 0px">
                                            <h5><%# Eval("immune") %></h5>
                                        </td>--%>
                                    </tr>

                                    <tr>
                                        <td style="text-align: left; border: 0px"><b>
                                            <h5>๕. ประวัติการเจ็บป่วยของสมาชิกในครอบครัว (เช่น มะเร็ง โลหิตจาง วัณโรค เบาหวาน หอบหืด ภูมิแพ้ เป็นต้น) :</h5>
                                        </b></td>

                                    </tr>

                                    <tr>
                                        <td style="text-align: left; border: 0px">
                                            <asp:UpdatePanel ID="Update_Relationfamily_hr" runat="server">
                                                <ContentTemplate>
                                                    <div class="col-md-12">
                                                        <div id="div_Identifyrelationship_hr" class="panel panel-default" runat="server">
                                                            <div class="panel-body">
                                                                <div class="form-group">
                                                                    <div class="col-md-12">
                                                                        <label>ระบุความสัมพันธ์และโรค</label>
                                                                    </div>
                                                                </div>
                                                                <table class="table table-striped f-s-12 table-empshift-responsive">
                                                                    <asp:Repeater ID="rptRelationfamily" runat="server">
                                                                        <HeaderTemplate>
                                                                            <tr>
                                                                                <th>ความสัมพันธ์</th>
                                                                                <th>โรค</th>
                                                                            </tr>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <tr>
                                                                                <td data-th="ความสัมพันธ์"><%# Eval("relation_family") %></td>
                                                                                <td data-th="โรค"><%# Eval("disease_family") %></td>

                                                                            </tr>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </table>

                                                                <div class="clearfix"></div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="text-align: left; border: 0px"><b>
                                            <h5>๖. ปัจจุบันมียาที่จำเป็นต้องรับประทานเป็นประจำบ้างหรือไม่ :</h5>
                                        </b></td>
                                        <%-- <td style="text-align: left; border: 0px">
                                            <h5><%# Eval("drugs_eat") %></h5>
                                        </td>--%>
                                    </tr>

                                    <tr>
                                        <td style="text-align: left; border: 0px"><b>
                                            <h5 style="padding-left: 25px;">ระบุ : <%# Eval("drugs_eat") %></h5>
                                        </b></td>
                                        <%-- <td style="text-align: left; border: 0px">
                                            <h5><%# Eval("drugs_eat") %></h5>
                                        </td>--%>
                                    </tr>

                                    <tr>
                                        <td style="text-align: left; border: 0px"><b>
                                            <h5>๗. มีประวัติการแพ้ยาหรือไม่ :</h5>
                                        </b></td>
                                        <%-- <td style="text-align: left; border: 0px">
                                            <h5><%# Eval("drugs_eat") %></h5>
                                        </td>--%>
                                    </tr>

                                    <tr>
                                        <td style="text-align: left; border: 0px"><b>
                                            <h5 style="padding-left: 25px;">ระบุ : <%# Eval("allergy_history") %></h5>

                                        </b></td>
                                        <%-- <td style="text-align: left; border: 0px">
                                            <h5><%# Eval("allergy_history") %></h5>
                                        </td>--%>
                                    </tr>

                                    <tr>
                                        <td style="text-align: left; border: 0px"><b>
                                            <h5>๘. เคยสูบบุหรี่บ้างหรือไม่ :</h5>
                                        </b></td>
                                        <%-- <td style="text-align: left; border: 0px">
                                            <h5><%# Eval("drugs_eat") %></h5>
                                        </td>--%>
                                    </tr>

                                    <tr>
                                        <td style="text-align: left; border: 0px; padding-left: 25px;"><b>
                                            <h5>
                                                <asp:Label ID="idx_Smoking_hr" runat="server" Visible="false" Text='<%# Eval("smoking") %>'></asp:Label>
                                                <asp:CheckBoxList ID="rdoSmoking_hr" runat="server" Enabled="false" OnSelectedIndexChanged="onSelectedIndexChanged" AutoPostBack="true">
                                                    <asp:ListItem Value="1">&nbsp;ไม่เคย</asp:ListItem>
                                                    <asp:ListItem Value="2">&nbsp;เคยและปัจจุบันยังสูบอยู่ปริมาณ</asp:ListItem>
                                                    <asp:ListItem Value="3">&nbsp;เคยแต่เลิกแล้ว ระยะที่เคยสูบนาน</asp:ListItem>
                                                </asp:CheckBoxList>
                                            </h5>
                                        </b>
                                            <asp:UpdatePanel ID="update_stopsmoking_hr" runat="server" Visible="false">
                                                <ContentTemplate>
                                                    <div class="col-md-12">
                                                        <h5><%# Eval("smoking_value1") %> ปี  <%# Eval("smoking_value2") %> เดือน  ปริมาณที่สูบ <%# Eval("smoking_value3") %> มวน/วัน</h5>
                                                        <%-- <h5><%# Eval("smoking_value2") %> เดือน</h5>
                                                        <h5><%# Eval("smoking_value3") %> มวน/วัน</h5>--%>
                                                        <%--<label>ปี</label>--%>
                                                        <%--<asp:TextBox ID="txt_year_smoking_hr" runat="server" CssClass="form-control" Text='<%# Eval("smoking_value1") %>' placeholder="ปี...." Enabled="false" />--%>
                                                    </div>

                                                </ContentTemplate>
                                            </asp:UpdatePanel>

                                            <asp:UpdatePanel ID="update_eversmoking_hr" runat="server" Visible="false">
                                                <ContentTemplate>
                                                    <div class="col-md-6">
                                                        <h5><%# Eval("smoking_value4") %> (มวน/วัน)</h5>
                                                        <%--<label>ปริมาณ(มวน/วัน)</label>--%>
                                                        <%-- <asp:TextBox ID="txt_valum_smoking_hr" runat="server" CssClass="form-control" Text='<%# Eval("smoking_value4") %>' placeholder="เดือน...." Enabled="false" />--%>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <label>&nbsp;</label>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                        <%-- <td style="text-align: left; border: 0px">
                                        </td>--%>
                                    </tr>

                                    <tr>
                                        <td style="text-align: left; border: 0px"><b>
                                            <h5>๙. เคยดื่มสุรา เบียร์ หรือเครื่องดื่มที่มีแอลกอฮอล์บ้างหรือไม่ :</h5>
                                        </b></td>
                                        <%-- <td style="text-align: left; border: 0px">
                                            <h5><%# Eval("drugs_eat") %></h5>
                                        </td>--%>
                                    </tr>

                                    <tr>
                                        <td style="text-align: left; border: 0px; padding-left: 25px;"><b>
                                            <h5>
                                                <asp:Label ID="idx_alcohol_hr" runat="server" Visible="false" Text='<%# Eval("alcohol") %>'></asp:Label>
                                                <asp:CheckBoxList ID="rdo_alcohol_hr" runat="server" Enabled="false" OnSelectedIndexChanged="onSelectedIndexChanged" AutoPostBack="true">
                                                    <asp:ListItem Value="1">&nbsp;ไม่เคย</asp:ListItem>
                                                    <asp:ListItem Value="2">&nbsp;โดยปกติดื่มน้อยกว่า 1 ครั้งต่อสัปดาห์</asp:ListItem>
                                                    <asp:ListItem Value="3">&nbsp;ดื่ม 1 ครั้งต่อสัปดาห์</asp:ListItem>
                                                    <asp:ListItem Value="4">&nbsp;ดื่ม 2-3 ครั้งต่อสัปดาห์</asp:ListItem>
                                                    <asp:ListItem Value="5">&nbsp;ดื่มมากกว่า 3 ครั้งต่อสัปดาห์</asp:ListItem>
                                                    <asp:ListItem Value="6">&nbsp;เคยแต่เลิกแล้วระยะเวลาที่ดื่มนาน</asp:ListItem>
                                                </asp:CheckBoxList>
                                            </h5>
                                        </b>
                                            <asp:UpdatePanel ID="update_alcohol_hr" runat="server" Visible="false">
                                                <ContentTemplate>
                                                    <div class="col-md-12">
                                                        <%--<label>ปี</label>--%>
                                                        <h5><%# Eval("alcohol_value1") %> ปี  <%# Eval("alcohol_value2") %> เดือน</h5>
                                                        <%--<h5><%# Eval("alcohol_value2") %> เดือน</h5>--%>
                                                        <%-- <asp:TextBox ID="txt_year_alcohol_hr" runat="server" Text='<%# Eval("alcohol_value1") %>' CssClass="form-control" placeholder="ปี...." Enabled="false" />--%>
                                                    </div>

                                                </ContentTemplate>
                                            </asp:UpdatePanel>

                                        </td>
                                        <%-- <td style="text-align: left; border: 0px">
                                            
                                        </td>--%>
                                    </tr>

                                    <tr>
                                        <td style="text-align: left; border: 0px"><b>
                                            <h5>๑๐. เคยเสพยาเสพติดหรือสารเสพติดใดๆ บ้างหรือไม่ :</h5>
                                        </b></td>
                                        <%-- <td style="text-align: left; border: 0px">
                                            <h5><%# Eval("drugs_eat") %></h5>
                                        </td>--%>
                                    </tr>

                                    <tr>
                                        <td style="text-align: left; border: 0px"><b>
                                            <h5 style="padding-left: 25px;">ระบุ : <%# Eval("addict_drug") %></h5>
                                        </b></td>
                                        <%--  <td style="text-align: left; border: 0px">
                                            <h5><%# Eval("addict_drug") %></h5>
                                        </td>--%>
                                    </tr>

                                    <tr>
                                        <td style="text-align: left; border: 0px"><b>
                                            <h5>๑๑. ข้อมูลทางสุขภาพอื่นๆ ที่เป็นประโยชน์ :</h5>
                                        </b></td>
                                        <%-- <td style="text-align: left; border: 0px">
                                            <h5><%# Eval("drugs_eat") %></h5>
                                        </td>--%>
                                    </tr>

                                    <tr>
                                        <%--<td style="text-align: left; border: 0px"><b>
                                            <h5>ระบุ :</h5>
                                        </b></td>--%>
                                        <td style="text-align: left; border: 0px">
                                            <h5 style="padding-left: 25px;"><%# Eval("datahealth") %></h5>
                                        </td>
                                    </tr>



                                </table>
                            </div>
                        </ItemTemplate>

                        <EditItemTemplate>
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">แก้ไขประวัติการเจ็บป่วย</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <h3 class="text-center">
                                                        <asp:Label ID="lbFormName_SickCheck_edit" runat="server" Text="แก้ไขประวัติการเจ็บป่วย" CssClass="control-label" Font-Bold="true" Font-Size="14"></asp:Label>
                                                    </h3>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="div_SickCheck_edit" class="panel panel-default" runat="server">

                                            <div class="panel-body">

                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <label>เคยป่วยเป็นโรคหรือมีการบาดเจ็บ</label>
                                                    </div>

                                                    <asp:UpdatePanel ID="update_eversick_edit" runat="server">
                                                        <ContentTemplate>
                                                            <div class="col-md-12">
                                                                <div id="div_DetailEverSickCheck_edit" class="panel panel-default" runat="server">
                                                                    <div class="panel-body">
                                                                        <div class="form-group">
                                                                            <div class="col-md-5">
                                                                                <label>&nbsp;</label>
                                                                                <asp:TextBox ID="txt_eversick_edit" runat="server" CssClass="form-control"
                                                                                    placeholder="เคยป่วยเป็นโรคหรืออาการบาดเจ็บ..." Enabled="true" />

                                                                            </div>
                                                                            <div class="col-md-5">
                                                                                <label>เมื่อปี พ.ศ.</label>
                                                                                <asp:TextBox ID="txt_lastyear_edit" runat="server" MaxLength="4" CssClass="form-control"
                                                                                    placeholder="เมื่อปี พ.ศ...." Enabled="true" />

                                                                            </div>
                                                                            <div class="col-md-2">
                                                                                <label>&nbsp;</label>
                                                                                <div class="clearfix"></div>
                                                                                <asp:LinkButton ID="btnAddEverSickList_edit" runat="server"
                                                                                    CssClass="btn btn-primary col-md-12"
                                                                                    Text="เพิ่มข้อมูล" OnCommand="btnCommand" CommandName="cmdAddEditEverSickList"
                                                                                    ValidationGroup="addAddEverSickList" />
                                                                            </div>

                                                                        </div>

                                                                        <asp:GridView ID="gvEverSickListEdit" runat="server"
                                                                            CssClass="table table-striped table-responsive"
                                                                            GridLines="None" OnRowCommand="onRowCommand"
                                                                            AutoGenerateColumns="false">
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center"
                                                                                    HeaderStyle-CssClass="text-center"
                                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller"
                                                                                    HeaderStyle-Width="10%">
                                                                                    <ItemTemplate>
                                                                                        <%# (Container.DataItemIndex + 1) %>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:BoundField DataField="drEversickEditText" HeaderText="เคยป่วยเป็นโรค" ItemStyle-HorizontalAlign="center"
                                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller"
                                                                                    HeaderStyle-CssClass="text-center" />

                                                                                <asp:BoundField DataField="drLastyearEditText" HeaderText="เมื่อปี พ.ศ." ItemStyle-HorizontalAlign="center"
                                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller"
                                                                                    HeaderStyle-CssClass="text-center" />

                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="center"
                                                                                    HeaderStyle-CssClass="text-center">
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="btnRemoveEverSickHealthEdit" runat="server"
                                                                                            CssClass="btn btn-danger btn-xs"
                                                                                            OnClientClick="return confirm('คุณต้องการลบรายการนี้ใชหรือไม่ ?')"
                                                                                            CommandName="btnRemoveEverSickHealthEdit">
                                                                                        <i class="fa fa-times"></i>
                                                                                        </asp:LinkButton>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>

                                                                        <div class="clearfix"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <label>มีโรคประจำตัวหรือโรคเรื้อรังหรือไม่</label>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <label>ระบุ</label>
                                                        <asp:TextBox ID="txt_u0_historysick_idx_edit" runat="server" Text='<%# Eval("u0_historysick_idx") %>' Visible="false" CssClass="form-control" Enabled="true" />
                                                        <asp:TextBox ID="txt_u2_historysick_idx_edit" runat="server" Text='<%# Eval("u2_historysick_idx") %>' Visible="false" CssClass="form-control" Enabled="true" />
                                                        <asp:TextBox ID="txt_havedisease_edit" runat="server" Text='<%# Eval("havedisease") %>' CssClass="form-control" placeholder="ระบุโรคประจำตัวหรือโรคเรื้อรัง...." Enabled="true" />
                                                    </div>

                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <label>เคยได้รับการผ่าตัดหรือไม่</label>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <label>ระบุ</label>
                                                        <asp:TextBox ID="txt_surgery_edit" runat="server" CssClass="form-control" Text='<%# Eval("surgery") %>' placeholder="ระบุเคยได้รับการผ่าตัด...." Enabled="true" />
                                                    </div>

                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <label>เคยได้รับภูมิคุ้มกันโรคกรณีเกิดโรคระบาด หรือเพื่อป้องกันโรคติดต่อหรือไม่</label>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <label>ระบุ</label>
                                                        <asp:TextBox ID="txt_Immune_edit" runat="server" CssClass="form-control" Text='<%# Eval("immune") %>' placeholder="ระบุเคยได้รับภูมิคุ้มกันโรคกรณีเกิดโรคระบาด...." Enabled="true" />
                                                    </div>

                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <label>ประวัติการเจ็บป่วยของสมาชิกในครอบครัว (เช่น มะเร็ง โลหิตจาง วัณโรค เบาหวาน หอบหืด ภูมิแพ้ เป็นต้น)</label>
                                                    </div>

                                                    <asp:UpdatePanel ID="update_relationFamily_edit" runat="server">
                                                        <ContentTemplate>
                                                            <div class="col-md-12">
                                                                <div id="div_Identifyrelationship_edit" class="panel panel-default" runat="server">
                                                                    <div class="panel-body">
                                                                        <div class="form-group">
                                                                            <div class="col-md-12">
                                                                                <label>ระบุความสัมพันธ์และโรค</label>
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <div class="col-md-5">
                                                                                <label>ความสัมพันธ์</label>
                                                                                <asp:TextBox ID="txt_relation_family_edit" runat="server" CssClass="form-control"
                                                                                    placeholder="ความสัมพันธ์..." Enabled="true" />

                                                                            </div>
                                                                            <div class="col-md-5">
                                                                                <label>โรค</label>
                                                                                <asp:TextBox ID="txt_Disease_Family_edit" runat="server" CssClass="form-control"
                                                                                    placeholder="โรค...." Enabled="true" />

                                                                            </div>
                                                                            <div class="col-md-2">
                                                                                <label>&nbsp;</label>
                                                                                <div class="clearfix"></div>
                                                                                <asp:LinkButton ID="btnAddDiseaseFamilyEdit" runat="server"
                                                                                    CssClass="btn btn-primary col-md-12"
                                                                                    Text="เพิ่มข้อมูล" OnCommand="btnCommand" CommandName="cmdAddDiseaseFamilyEdit"
                                                                                    ValidationGroup="addAddDiseaseFamilyList" />
                                                                            </div>


                                                                        </div>

                                                                        <asp:GridView ID="gvDiseaseFamilyEdit" runat="server"
                                                                            CssClass="table table-striped table-responsive"
                                                                            GridLines="None" OnRowCommand="onRowCommand"
                                                                            AutoGenerateColumns="false">
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center"
                                                                                    HeaderStyle-CssClass="text-center"
                                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller"
                                                                                    HeaderStyle-Width="10%">
                                                                                    <ItemTemplate>
                                                                                        <%# (Container.DataItemIndex + 1) %>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:BoundField DataField="drRelation_familyEditText" HeaderText="ความสัมพันธ์" ItemStyle-HorizontalAlign="center"
                                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller"
                                                                                    HeaderStyle-CssClass="text-center" />

                                                                                <asp:BoundField DataField="drDisease_FamilyEditText" HeaderText="โรค" ItemStyle-HorizontalAlign="center"
                                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller"
                                                                                    HeaderStyle-CssClass="text-center" />

                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="center"
                                                                                    HeaderStyle-CssClass="text-center">
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="btnRemoveRelationFamilyEdit" runat="server"
                                                                                            CssClass="btn btn-danger btn-xs"
                                                                                            OnClientClick="return confirm('คุณต้องการลบรายการนี้ใชหรือไม่ ?')"
                                                                                            CommandName="btnRemoveRelationFamilyEdit">
                                                                                <i class="fa fa-times"></i>
                                                                                        </asp:LinkButton>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>

                                                                        <div class="clearfix"></div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <label>ปัจจุบันมียาที่จำเป็นต้องรับประทานเป็นประจำบ้างหรือไม่</label>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <label>ระบุ</label>
                                                        <asp:TextBox ID="txt_DrugsEat_Edit" runat="server" Text='<%# Eval("drugs_eat") %>' CssClass="form-control" placeholder="ระบุปัจจุบันมียาที่จำเป็นต้องรับประทานเป็นประจำ...." Enabled="true" />
                                                    </div>

                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <label>มีประวัติการแพ้ยาหรือไม่</label>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <label>ระบุ</label>
                                                        <asp:TextBox ID="txt_Allergyhistory_Edit" runat="server" Text='<%# Eval("allergy_history") %>' CssClass="form-control" placeholder="ระบุประวัติการแพ้ยา...." Enabled="true" />
                                                    </div>

                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <label>เคยสูบบุหรี่บ้างหรือไม่</label>
                                                        <%--<asp:Label ID="idx_rdoSmoking_edit" runat="server" Visible="false" Text='<%# Eval("smoking") %>'></asp:Label>--%>
                                                        <asp:RadioButtonList ID="rdoSmoking_edit" runat="server" OnSelectedIndexChanged="onSelectedIndexChanged" Text='<%# Eval("smoking") %>' AutoPostBack="true">
                                                            <asp:ListItem Value="1">&nbsp;ไม่เคย</asp:ListItem>
                                                            <asp:ListItem Value="2">&nbsp;เคยและปัจจุบันยังสูบอยู่ปริมาณ</asp:ListItem>
                                                            <asp:ListItem Value="3">&nbsp;เคยแต่เลิกแล้ว ระยะที่เคยสูบนาน</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </div>

                                                    <asp:UpdatePanel ID="updateInsert_stopsmoking_edit" runat="server" Visible="false">
                                                        <ContentTemplate>
                                                            <div class="col-md-3">

                                                                <asp:TextBox ID="txt_year_smoking_edit" MaxLength="2" runat="server" Text='<%# Eval("smoking_value1") %>' CssClass="form-control" placeholder="ปี...." Enabled="true" />
                                                            </div>
                                                            <div class="col-md-1">
                                                                <asp:TextBox ID="txt_year_smoking_edit_unit" runat="server" Text="ปี" CssClass="form-control" Enabled="false" />
                                                            </div>
                                                            <div class="col-md-3">

                                                                <asp:TextBox ID="txt_month_smoking_edit" MaxLength="2" runat="server" Text='<%# Eval("smoking_value2") %>' CssClass="form-control" placeholder="เดือน...." Enabled="true" />
                                                            </div>
                                                            <div class="col-md-1">
                                                                <asp:TextBox ID="txt_month_smoking_edit_unit" runat="server" Text="เดือน" CssClass="form-control" Enabled="false" />
                                                            </div>
                                                            <div class="col-md-3">

                                                                <asp:TextBox ID="txt_before_stopsmoking_edit" runat="server" Text='<%# Eval("smoking_value3") %>' CssClass="form-control" placeholder="ปริมาณขณะก่อนเลิก(มวน/วัน)...." Enabled="true" />
                                                            </div>
                                                            <div class="col-md-1">
                                                                <asp:TextBox ID="txt_before_stopsmoking_edit_unit" runat="server" Text="มวน/วัน" CssClass="form-control" Enabled="false" />
                                                            </div>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>

                                                    <asp:UpdatePanel ID="updateInsert_eversmoking_edit" runat="server" Visible="false">
                                                        <ContentTemplate>
                                                            <div class="col-md-5">


                                                                <asp:TextBox ID="txt_valum_smoking_edit" runat="server" CssClass="form-control" Text='<%# Eval("smoking_value4") %>' placeholder="ปริมาณ(มวน/วัน)...." Enabled="true" />
                                                            </div>
                                                            <div class="col-md-1">
                                                                <asp:TextBox ID="txt_valum_smoking_edit_unit" runat="server" Text="มวน/วัน" CssClass="form-control" Enabled="false" />
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label>&nbsp;</label>
                                                            </div>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>

                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <label>เคยดื่มสุรา เบียร์ หรือเครื่องดื่มที่มีแอลกอฮอล์บ้างหรือไม่</label>
                                                        <%--<asp:Label ID="idx_rdo_alcohol_edit" Visible="false" Text='<%# Eval("alcohol") %>' runat="server"></asp:Label>--%>
                                                        <asp:RadioButtonList ID="rdo_alcohol_edit" runat="server" OnSelectedIndexChanged="onSelectedIndexChanged" Text='<%# Eval("alcohol") %>' AutoPostBack="true">
                                                            <asp:ListItem Value="1">&nbsp;ไม่เคย</asp:ListItem>
                                                            <asp:ListItem Value="2">&nbsp;โดยปกติดื่มน้อยกว่า 1 ครั้งต่อสัปดาห์</asp:ListItem>
                                                            <asp:ListItem Value="3">&nbsp;ดื่ม 1 ครั้งต่อสัปดาห์</asp:ListItem>
                                                            <asp:ListItem Value="4">&nbsp;ดื่ม 2-3 ครั้งต่อสัปดาห์</asp:ListItem>
                                                            <asp:ListItem Value="5">&nbsp;ดื่มมากกว่า 3 ครั้งต่อสัปดาห์</asp:ListItem>
                                                            <asp:ListItem Value="6">&nbsp;เคยแต่เลิกแล้วระยะเวลาที่ดื่มนาน</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </div>

                                                    <asp:UpdatePanel ID="updateInsert_alcohol_edit" runat="server" Visible="false">
                                                        <ContentTemplate>
                                                            <div class="col-md-5">

                                                                <asp:TextBox ID="txt_year_alcohol_edit" MaxLength="2" runat="server" Text='<%# Eval("alcohol_value1") %>' CssClass="form-control" placeholder="ปี...." Enabled="true" />
                                                            </div>
                                                            <div class="col-md-1">
                                                                <asp:TextBox ID="txt_year_alcohol_edit_unit" runat="server" Text="ปี" CssClass="form-control" Enabled="false" />

                                                            </div>
                                                            <div class="col-md-5">

                                                                <asp:TextBox ID="txt_month_alcohol_edit" runat="server" MaxLength="2" Text='<%# Eval("alcohol_value2") %>' CssClass="form-control" placeholder="เดือน...." Enabled="true" />
                                                            </div>
                                                            <div class="col-md-1">
                                                                <asp:TextBox ID="txt_month_alcohol_edit_unit" runat="server" Text="เดือน" CssClass="form-control" Enabled="false" />

                                                            </div>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <label>เคยเสพยาเสพติดหรือสารเสพติดใดๆ บ้างหรือไม่</label>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <label>ระบุ</label>
                                                        <asp:TextBox ID="txt_addict_drug_edit" runat="server" Text='<%# Eval("addict_drug") %>' CssClass="form-control" placeholder="ระบุเคยเสพยาเสพติดหรือสารเสพติด...." Enabled="true" />
                                                    </div>

                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <label>ข้อมูลทางสุขภาพอื่นๆ ที่เป็นประโยชน์</label>
                                                        <asp:TextBox ID="txt_datahealth_edit" runat="server" Text='<%# Eval("datahealth") %>' CssClass="form-control" placeholder="ระบุเข้อมูลทางสุขภาพอื่นๆ ที่เป็นประโยชน์...." Enabled="true" />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <asp:LinkButton ID="btnSaveSickList_edit" CssClass="btn btn-success" runat="server" OnCommand="btnCommand"
                                                            CommandName="cmdSaveEditSickList" Text="บันทึกการเปลี่ยนแปลง" CommandArgument='<%# Eval("u2_historysick_idx") %>' ValidationGroup="btnSave"
                                                            OnClientClick="return confirm('ยืนยันการบันทึก')" />
                                                        <asp:LinkButton CssClass="btn btn-danger" runat="server" OnCommand="btnCommand"
                                                            CommandName="cmdCancelEditHistory" Text="ยกเลิก" />
                                                    </div>
                                                </div>

                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </EditItemTemplate>

                    </asp:FormView>
                </div>

                <asp:Panel class="alert alert-warning" ID="EmptyData" runat="server" Visible="false">
                    <button type="button" class="close" data-dismiss="alert"></button>
                    <%--<h5 style="text-align: center;">-- ไม่พบข้อมูล --</h5>--%>
                    <center>-- ไม่พบข้อมูล --</center>
                </asp:Panel>
            </div>

            <div id="divHistoryInjury" runat="server" class="col-md-12" visible="false">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">บันทึกเกี่ยวกับการบาดเจ็บหรือเจ็บป่วยเนื่องจากการทำงานและสาเหตุ</h3>
                    </div>
                    <div class="panel-body">

                        <div class="form-group">
                            <%--<asp:LinkButton ID="btnPrintInjry" runat="server" CommandName="cmdPrint" CssClass="btn btn-default" OnCommand="btnCommand" CommandArgument="Injury" data-toggle="tooltip" title="พิมพ์"><i class="fa fa-print"></i> <b>พิมพ์</b></asp:LinkButton>--%>

                            <asp:LinkButton ID="btnPrintInjry" CssClass="btn btn-primary" runat="server" Visible="false" Text="<i class='fa fa-print'></i> พิมพ์"
                                OnClientClick="return printDiv('div_printableAreaHistoryWork');" />
                        </div>
                        <div id="div_printableAreaHistoryWork" class="print_report hidden">
                            <h3 style="text-align: center;">บันทึกเกี่ยวกับการบาดเจ็บหรือเจ็บป่วยเนื่องจากการทำงานและสาเหตุ</h3>
                            <asp:GridView ID="gvPrintInjry"
                                runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-responsive"
                                HeaderStyle-CssClass="info"
                                OnRowDataBound="gvRowDataBound">
                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                <EmptyDataTemplate>
                                    <div class="text-center">-- ไม่พบข้อมูล --</div>
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="วันเดือนปี" ItemStyle-HorizontalAlign="Center"
                                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lbl_date_injuly_history1" runat="server" Text='<%# Eval("date_injuly") %>' />
                                            </small>
                                        </ItemTemplate>
                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ส่วนของร่างกายที่บาดเจ็บหรือการเจ็บป่วย" ItemStyle-HorizontalAlign="Left"
                                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="25%">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lbl_injury_detail_history1" runat="server" Text='<%# Eval("injury_detail") %>' />
                                            </small>

                                        </ItemTemplate>
                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ส่วนของการบาดเจ็บหรือการเจ็บป่วย" ItemStyle-HorizontalAlign="Left"
                                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="25%">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lbl_cause_Injury_history1" runat="server" Text='<%# Eval("cause_Injury") %>' />
                                            </small>

                                        </ItemTemplate>
                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ระดับความรุนแรง" ItemStyle-HorizontalAlign="Left"
                                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="25%">
                                        <ItemTemplate>
                                            <small>

                                                <asp:Label ID="Detail_Violence1" runat="server"> 
                                                    <p><b>ทุพพลภาพ :</b> <%# Eval("disability") %></p>
                                                    <p><b>สูญเสียอวัยวะบางส่วน :</b> <%# Eval("lost_organ") %></p>
                                                    <p><b>ทำงานไม่ได้ช่วยคราว :</b> <%# Eval("detail_not_working") %></p>
                                  
                                                </asp:Label>
                                            </small>

                                        </ItemTemplate>
                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </div>
                        <div id="printableAreaHistoryWork">
                            <h3 style="text-align: center;">บันทึกเกี่ยวกับการบาดเจ็บหรือเจ็บป่วยเนื่องจากการทำงานและสาเหตุ</h3>
                            <asp:GridView ID="gvInjury"
                                runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-responsive"
                                HeaderStyle-CssClass="info"
                                AllowPaging="true"
                                PageSize="10"
                                OnPageIndexChanging="gvPageIndexChanging"
                                OnRowDataBound="gvRowDataBound">
                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                <EmptyDataTemplate>
                                    <div class="text-center">-- ไม่พบข้อมูล --</div>
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="วันเดือนปี" ItemStyle-HorizontalAlign="Center"
                                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lbl_date_injuly_history" runat="server" Text='<%# Eval("date_injuly") %>' />
                                            </small>
                                        </ItemTemplate>
                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ส่วนของร่างกายที่บาดเจ็บหรือการเจ็บป่วย" ItemStyle-HorizontalAlign="Left"
                                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="25%">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lbl_injury_detail_history" runat="server" Text='<%# Eval("injury_detail") %>' />
                                            </small>

                                        </ItemTemplate>
                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ส่วนของการบาดเจ็บหรือการเจ็บป่วย" ItemStyle-HorizontalAlign="Left"
                                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="25%">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lbl_cause_Injury_history" runat="server" Text='<%# Eval("cause_Injury") %>' />
                                            </small>

                                        </ItemTemplate>
                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ระดับความรุนแรง" ItemStyle-HorizontalAlign="Left"
                                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="25%">
                                        <ItemTemplate>
                                            <small>

                                                <asp:Label ID="Detail_Violence" runat="server"> 
                                                    <p><b>ทุพพลภาพ :</b> <%# Eval("disability") %></p>
                                                    <p><b>สูญเสียอวัยวะบางส่วน :</b> <%# Eval("lost_organ") %></p>
                                                    <p><b>ทำงานไม่ได้ช่วยคราว :</b> <%# Eval("detail_not_working") %></p>
                                  
                                                </asp:Label>
                                            </small>

                                        </ItemTemplate>
                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-HorizontalAlign="center" HeaderText="Action" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small"
                                        HeaderStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnUpdateHistoryInjury" CssClass="btn btn-warning btn-sm" runat="server" CommandName="cmdUpdateHistoryInjury" OnCommand="btnCommand" data-toggle="tooltip" title="แก้ไข" CommandArgument='<%# Eval("u2_historyinjury_idx") %>'>
                                                <i class="fa fa-pencil"></i>
                                            </asp:LinkButton>

                                            <asp:LinkButton ID="btnDelHistoryInjuryList" CssClass="btn btn-danger btn-sm" runat="server"
                                                CommandName="cmdDelHistoryInjuryList" OnCommand="btnCommand" data-toggle="tooltip" title="ลบ" CommandArgument='<%# Eval("u2_historyinjury_idx") %>'
                                                OnClientClick="return confirm('คุณต้องการลบข้อมูลนี้ใช่หรือไม่')">
                                                <i class="fa fa-trash"></i>
                                            </asp:LinkButton>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>

            </div>

            <div id="div_fvEditHistoryHealthInjury" runat="server" class="col-md-12" visible="false">
                <asp:FormView ID="fvEditHistoryHealthInjury" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                    <EditItemTemplate>
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">แก้ไขบันทึกเกี่ยวกับการบาดเจ็บหรือเจ็บป่วยเนื่องจากการทำงานและสาเหตุ</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <h3 class="text-center">
                                                <asp:Label ID="lbFormName_Injuryedit" runat="server" Text="แก้ไขบันทึกเกี่ยวกับการบาดเจ็บหรือเจ็บป่วยเนื่องจากการทำงานและสาเหตุ" CssClass="control-label" Font-Bold="true" Font-Size="14"></asp:Label>
                                            </h3>
                                        </div>
                                    </div>
                                    <div id="div_DetailHistoryHealth_edit" class="panel panel-default" runat="server">
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <div class="col-md-6">
                                                    <label>วัน/เดือน/ปี</label>
                                                    <asp:TextBox ID="txt_u2_historyinjury_idx_edit" runat="server" Visible="false" CssClass="form-control from-date-datepicker" Text='<%# Eval("u2_historyinjury_idx") %>' Enabled="true">
                                                    </asp:TextBox>
                                                    <div class="input-group date">

                                                        <asp:TextBox ID="txt_date_injury_edit" runat="server" CssClass="form-control from-date-datepicker" Text='<%# Eval("date_injuly") %>' Enabled="true">
                                                        </asp:TextBox>
                                                        <span class="input-group-addon">
                                                            <span data-icon-element="" class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <asp:RequiredFieldValidator ID="Re_txt_date_injury_edit"
                                                            runat="server"
                                                            ControlToValidate="txt_date_injury_edit" Display="None"
                                                            SetFocusOnError="true"
                                                            ErrorMessage="*กรุณากรอก วัน/เดือน/ปี"
                                                            ValidationGroup="AddHistoryInjuryList" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="Validator_txt_date_injury" runat="Server" PopupPosition="BottomLeft"
                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_date_injury_edit" Width="180" />
                                                    </div>

                                                </div>

                                                <div class="col-md-6">

                                                    <label>ส่วนของร่างกายที่บาดเจ็บหรือการเจ็บป่วย</label>
                                                    <asp:TextBox ID="txt_injurydetail_edit" runat="server" Text='<%# Eval("injury_detail") %>' CssClass="form-control" Enabled="true">
                                                    </asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="Re_txt_injurydetail_edit"
                                                        runat="server"
                                                        ControlToValidate="txt_injurydetail_edit" Display="None"
                                                        SetFocusOnError="true"
                                                        ErrorMessage="*กรุณากรอกส่วนของร่างกายที่บาดเจ็บหรือการเจ็บป่วย"
                                                        ValidationGroup="AddHistoryInjuryList" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="Validator_txt_injurydetail" runat="Server" PopupPosition="BottomLeft"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_injurydetail_edit" Width="180" />
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label>สาเหตุของการบาดเจ็บหรือการเจ็บป่วย</label>
                                                    <asp:TextBox ID="txt_cause_Injury_edit" runat="server" Text='<%# Eval("cause_Injury") %>' CssClass="form-control" Enabled="true">
                                                    </asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="Re_txt_cause_Injury_edit"
                                                        runat="server"
                                                        ControlToValidate="txt_cause_Injury_edit" Display="None"
                                                        SetFocusOnError="true"
                                                        ErrorMessage="*กรุณากรอกส่วนของร่างกายที่บาดเจ็บหรือการเจ็บป่วย"
                                                        ValidationGroup="AddHistoryInjuryList" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="Validator_txt_cause_Injury" runat="Server" PopupPosition="BottomLeft"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_cause_Injury_edit" Width="180" />
                                                </div>
                                            </div>

                                            <div class="form-group">

                                                <div class="col-md-12">
                                                    <label>ระดับความรุนแรง</label>
                                                </div>
                                                <asp:UpdatePanel ID="updateInsert_notwork_edit" runat="server">
                                                    <ContentTemplate>
                                                        <div class="col-md-12">

                                                            <div id="div_Detailnot_work_edit" class="panel panel-default" runat="server">
                                                                <div class="panel-body">
                                                                    <div class="form-group">

                                                                        <div class="col-md-6">
                                                                            <label>ทุพพลภาพ</label>
                                                                            <asp:TextBox ID="txt_Disability_edit" runat="server" Text='<%# Eval("disability") %>' CssClass="form-control" Enabled="true">
                                                                            </asp:TextBox>


                                                                        </div>

                                                                        <div class="col-md-6">
                                                                            <label>สูญเสียอวัยวะบางส่วน</label>
                                                                            <asp:TextBox ID="txt_LostOrgan_edit" runat="server" Text='<%# Eval("lost_organ") %>' CssClass="form-control" Enabled="true">
                                                                            </asp:TextBox>

                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <div class="col-md-12">
                                                                            <label>ทำงานไม่ได้ชั่วคราว</label>
                                                                            <asp:TextBox ID="txt_not_working_idx" runat="server" Visible="false" Text='<%# Eval("not_working") %>' CssClass="form-control" Enabled="true">
                                                                            </asp:TextBox>
                                                                            <asp:RadioButtonList ID="rdo_notworking_edit" runat="server">
                                                                                <asp:ListItem Value="1">&nbsp;หยุดงานเกิน 3 วัน</asp:ListItem>
                                                                                <asp:ListItem Value="2">&nbsp;หยุดงานไม่เกิน 3 วัน</asp:ListItem>

                                                                            </asp:RadioButtonList>
                                                                        </div>

                                                                    </div>


                                                                </div>
                                                            </div>

                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <asp:LinkButton ID="btnEditHistoryInjury" CssClass="btn btn-success" runat="server" OnCommand="btnCommand"
                                                        CommandName="cmdSaveEditHistoryInjury" Text="บันทึกการเปลี่ยนแปลง" CommandArgument='<%# Eval("u2_historyinjury_idx") %>' ValidationGroup="btnSave"
                                                        OnClientClick="return confirm('ยืนยันการบันทึก')" />
                                                    <asp:LinkButton CssClass="btn btn-danger" runat="server" OnCommand="btnCommand"
                                                        CommandName="cmdCancelEditHistory" Text="ยกเลิก" />
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </EditItemTemplate>
                </asp:FormView>
            </div>

            <div id="divHistoryHealth" runat="server" class="col-md-12" visible="false">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">การตรวจสุขภาพ</h3>
                    </div>
                    <div class="panel-body">
                        <asp:GridView ID="gvHealth"
                            runat="server"
                            AutoGenerateColumns="false"
                            CssClass="table table-striped table-bordered table-responsive"
                            HeaderStyle-CssClass="info"
                            AllowPaging="true"
                            PageSize="10"
                            OnPageIndexChanging="gvPageIndexChanging"
                            OnRowDataBound="gvRowDataBound">
                            <PagerStyle CssClass="pageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                            <EmptyDataTemplate>
                                <div class="text-center">-- ไม่พบข้อมูล --</div>
                            </EmptyDataTemplate>
                            <Columns>

                                <%-- <asp:TemplateField HeaderText="#" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" ItemStyle-CssClass="text-center" HeaderStyle-Width="5%">
                                <ItemTemplate>
                                    <small>
                                        <%# (Container.DataItemIndex + 1) %>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>--%>

                                <asp:TemplateField HeaderText="วันที่ตรวจสุขภาพ" ItemStyle-HorizontalAlign="Center"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%">
                                    <ItemTemplate>
                                        <small>
                                            <asp:Label ID="lbl_date_checksick_history" runat="server" Text='<%# Eval("date_check") %>' />
                                            <%--<asp:Label ID="Detail_company_name" runat="server"> 
                                                    <p><b>ชื่อสถานประกอบกิจการ :</b> <%# Eval("company_name") %></p>
                                                    <p><b>แผนก :</b> <%# Eval("department_name") %></p>
                                  
                                                </asp:Label>--%>

                                        </small>
                                    </ItemTemplate>
                                    <EditItemTemplate />
                                    <FooterTemplate />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="รายละเอียดการตรวจ" ItemStyle-HorizontalAlign="Left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="30%">
                                    <ItemTemplate>
                                        <small>
                                            <asp:Label ID="lbl_detail_typecheck_history" runat="server" Text='<%# Eval("detail_typecheck") %>' />
                                        </small>

                                    </ItemTemplate>
                                    <EditItemTemplate />
                                    <FooterTemplate />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ชื่อแพทย์ผู้ตรวจ" ItemStyle-HorizontalAlign="Left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="30%">
                                    <ItemTemplate>
                                        <small>
                                            <asp:Label ID="lbl_doctor_name_history" runat="server" Text='<%# Eval("doctor_name") %>' />
                                        </small>

                                    </ItemTemplate>
                                    <EditItemTemplate />
                                    <FooterTemplate />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="25%">
                                    <ItemTemplate>
                                        <small>

                                            <asp:LinkButton ID="btnViewHealthDetailCheck" CssClass="btn btn-info btn-sm" runat="server" CommandName="cmdViewHealthDetailCheck"
                                                OnCommand="btnCommand" CommandArgument='<%# Eval("u2_historyhealth_idx")%>'
                                                data-toggle="tooltip" title="ดูข้อมูล"><i class="fa fa-file"></i>

                                            </asp:LinkButton>

                                            <asp:LinkButton ID="btnDeleteHistorHealth" CssClass="btn btn-danger btn-sm" runat="server" CommandName="cmdDelHistoryHealth"
                                                OnCommand="btnCommand" CommandArgument='<%# Eval("u2_historyhealth_idx")%>'
                                                data-toggle="tooltip" title="ลบ" OnClientClick="return confirm('คุณต้องการลบข้อมูลนี้ใช่หรือไม่')"><i class="fa fa-trash"></i>

                                            </asp:LinkButton>

                                        </small>

                                    </ItemTemplate>
                                    <EditItemTemplate />
                                    <FooterTemplate />
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>
                    </div>

                </div>
            </div>

            <asp:UpdatePanel ID="UpdatePanel_BackToListHealthDetail" runat="server" Visible="false" class="col-md-12">
                <ContentTemplate>
                    <div class="form-group">
                        <asp:LinkButton ID="btnBackToHistoryHealth" CssClass="btn btn-danger" data-toggle="tooltip" title="" runat="server"
                            CommandName="cmdCancelToHistoryHealth" OnCommand="btnCommand">< ย้อนกลับ</asp:LinkButton>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

            <div id="div_DetailHealthCheck" runat="server" class="col-md-12" visible="false">

                <div class="form-group" id="div_EditHealthCheck" runat="server" visible="false">
                    <asp:LinkButton ID="btnEditHistoryHealth" CssClass="btn btn-default" data-toggle="tooltip" title="แก้ไข" runat="server"
                        CommandName="cmdEditHistoryHealth" CommandArgument='<%# Eval("u2_historyhealth_idx") %>' OnCommand="btnCommand">แก้ไขการตรวจสุขภาพ</asp:LinkButton>

                    <%--<asp:LinkButton ID="btnPrintHistoryHealth" runat="server" CommandName="cmdPrint" CssClass="btn btn-default" OnCommand="btnCommand" CommandArgument="HistoryHealth" data-toggle="tooltip" title="พิมพ์"><i class="fa fa-print"></i> <b>พิมพ์</b></asp:LinkButton>--%>

                    <asp:LinkButton ID="btnPrintHistoryHealth" CssClass="btn btn-primary" runat="server" Visible="false"
                        Text="<i class='fa fa-print'></i> พิมพ์" OnClientClick="return printDiv('printableAreaHistoryHealth');" />

                </div>

                <div id="printableAreaHistoryHealth">
                    <asp:FormView ID="fvDetailHealthCheck" runat="server" DefaultMode="ReadOnly" OnDataBound="FvDetail_DataBound" Width="100%">
                        <ItemTemplate>
                            <div class="panel panel-default">
                                <%-- <div class="panel-heading">
                                    <h3 class="panel-title">การตรวจสุขภาพ</h3>
                                </div>--%>
                                <table class="table f-s-12 m-t-10" style="border: 0px">
                                    <h3 style="text-align: center;">การตรวจสุขภาพ</h3>
                                    <br />
                                    <h4 style="text-align: center;">ครั้งที่ : <%# Eval("num_check") %></h4>

                                    <tr>
                                        <td style="text-align: left; border: 0px"><b>
                                            <h5>
                                                <asp:Label ID="rao_idx_detailtype" Visible="false" Text='<%# Eval("m0_detail_typecheck_idx") %>' runat="server"></asp:Label>
                                                <asp:RadioButtonList ID="rdoDetailType_detail" Enabled="false" runat="server" CssClass="" Font-Bold="true" CellPadding="5" CellSpacing="5" RepeatColumns="2" Width="100%" RepeatDirection="Vertical" AutoPostBack="true" RepeatLayout="Table" TextAlign="Right">
                                                </asp:RadioButtonList>
                                        </b></h5></td>
                                    </tr>

                                </table>

                                <table class="table f-s-12 m-t-10" style="border: 0px">
                                    <tr>
                                        <td style="text-align: left; border: 0px"><b>
                                            <h5>วันที่ตรวจสุขภาพ : <%# Eval("date_check") %></h5>
                                        </b></td>
                                    </tr>

                                    <tr>
                                        <td style="text-align: left; border: 0px"><b>
                                            <h5>ชื่อแพทย์ผู้ตรวจ : <%# Eval("doctor_name") %></h5>
                                        </b></td>
                                    </tr>

                                    <tr>
                                        <td style="text-align: left; border: 0px"><b>
                                            <h5>เลขที่ใบประกอบวิชาชีพเวชกรรม : <%# Eval("card_number") %></h5>
                                        </b></td>
                                    </tr>

                                    <tr>
                                        <td style="text-align: left; border: 0px"><b>
                                            <h5>ชื่อหน่วยงานที่ตรวจสุขภาพ : <%# Eval("health_authority_name") %></h5>
                                        </b></td>
                                    </tr>
                                </table>

                                <table class="table f-s-12 m-t-10" style="border: 0px">

                                    <tr>
                                        <td style="text-align: left; border: 0px"><b>
                                            <h5>ตั้งอยู่เลขที่ : <%# Eval("location_name") %></h5>
                                        </b></td>

                                        <td style="text-align: left; border: 0px"><b>
                                            <h5>หมู่ที่ : <%# Eval("village_no") %></h5>
                                        </b></td>

                                        <td style="text-align: left; border: 0px"><b>
                                            <h5>ถนน : <%# Eval("road_name") %></h5>
                                        </b></td>
                                    </tr>
                                </table>

                                <table class="table f-s-12 m-t-10" style="border: 0px">

                                    <tr>
                                        <td style="text-align: left; border: 0px"><b>
                                            <h5>๑. ตรวจสุขภาพทั่วไป</h5>
                                        </b></td>
                                    </tr>

                                    <tr>
                                        <td style="text-align: left; border: 0px"><b>
                                            <asp:UpdatePanel ID="Update_GenaralHealth_Detail" runat="server">
                                                <ContentTemplate>
                                                    <%--  <div id="div_genaralHealthCheck_detail" class="panel panel-default" runat="server">--%>
                                                    <%-- <h5>๑.๑ ผลการตรวจเบื้องต้น </h5>--%>

                                                    <tr>
                                                        <td style="text-align: left; border: 0px" colspan="4"><b>
                                                            <h5 style="padding-left: 25px">๑.๑ ผลการตรวจเบื้องต้น</h5>
                                                        </b></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>

                                                    <tr>
                                                        <td style="text-align: left; border: 0px"><b>
                                                            <h5 style="padding-left: 40px">น้ำหนัก : </h5>
                                                        </b></td>

                                                        <td style="text-align: left; border: 0px"><b>
                                                            <h5><%# Eval("weight") %> กิโลกรัม</h5>
                                                        </b></td>

                                                        <td style="text-align: left; border: 0px"><b>
                                                            <h5>ความสูง : </h5>
                                                        </b></td>

                                                        <td style="text-align: left; border: 0px"><b>
                                                            <h5><%# Eval("height") %> เซนติเมตร</h5>
                                                        </b></td>
                                                    </tr>

                                                    <tr>
                                                        <td style="text-align: left; border: 0px"><b>
                                                            <h5 style="padding-left: 40px">ดัชนีมวลกาย : </h5>
                                                        </b></td>

                                                        <td style="text-align: left; border: 0px"><b>
                                                            <h5><%# Eval("body_mass") %></h5>
                                                        </b></td>

                                                        <td style="text-align: left; border: 0px"><b>
                                                            <h5>ความดันโลหิต : </h5>
                                                        </b></td>

                                                        <td style="text-align: left; border: 0px"><b>
                                                            <h5><%# Eval("bloodpressure") %> mm.Hg</h5>
                                                        </b></td>
                                                    </tr>

                                                    <tr>

                                                        <td style="text-align: left; border: 0px"><b>
                                                            <h5 style="padding-left: 40px">ชีพจร : </h5>
                                                        </b></td>

                                                        <td style="text-align: left; border: 0px"><b>
                                                            <h5><%# Eval("pulse") %> ครั้ง/นาที</h5>
                                                        </b></td>

                                                        <td style="text-align: left; border: 0px"></td>
                                                        <td style="text-align: left; border: 0px"></td>
                                                    </tr>

                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </b></td>
                                    </tr>
                                </table>
                                <table class="table f-s-12 m-t-10" style="border: 0px">
                                    <tr>
                                        <td style="text-align: left; border: 0px"><b>
                                            <asp:UpdatePanel ID="Update_specify_resultbody" runat="server">
                                                <ContentTemplate>
                                                    <h5 style="padding-left: 25px">๑.๒ ผลการตรวจร่างกายตามระบบ </h5>
                                                    <tr>
                                                        <td style="text-align: left; border: 0px"><b>
                                                            <h5 style="padding-left: 40px">ระบุ : <%# Eval("specify_resultbody") %>  </h5>
                                                        </b></td>
                                                    </tr>

                                                </ContentTemplate>
                                            </asp:UpdatePanel></td>
                                    </tr>

                                    <tr>
                                        <td style="text-align: left; border: 0px"><b>
                                            <asp:UpdatePanel ID="UpdatePanel_resultlab" runat="server">
                                                <ContentTemplate>
                                                    <h5 style="padding-left: 25px">๑.๓ ผลการตรวจทางห้องปฏิบัติการ </h5>
                                                    <tr>
                                                        <td style="text-align: left; border: 0px"><b>
                                                            <h5 style="padding-left: 40px">ระบุ : <%# Eval("resultlab") %>  </h5>
                                                        </b></td>
                                                    </tr>

                                                </ContentTemplate>
                                            </asp:UpdatePanel></td>
                                    </tr>

                                    <tr>
                                        <td style="text-align: left; border: 0px"><b>
                                            <h5>๒. ตรวจสุขภาพตามปัจจัยเสี่ยงของงาน</h5>

                                        </b></td>
                                    </tr>

                                    <tr>
                                        <td style="text-align: left; border: 0px"><b>
                                            <asp:UpdatePanel ID="Update_RikeHealthCheck_Detail" runat="server">
                                                <ContentTemplate>
                                                    <div class="col-md-12">
                                                        <div id="div_rikeHealthCheck_detail" class="panel panel-default" runat="server">
                                                            <div class="panel-body">
                                                                <table class="table table-striped f-s-12 table-empshift-responsive">
                                                                    <asp:Repeater ID="rpt_RikeDetail" runat="server">
                                                                        <HeaderTemplate>
                                                                            <tr>
                                                                                <th>ปัจจัยเสี่ยง</th>
                                                                                <th>ผลการตรวจ</th>
                                                                            </tr>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <tr>
                                                                                <td data-th="ปัจจัยเสี่ยง"><%# Eval("rike_health") %></td>
                                                                                <td data-th="ผลการตรวจ"><%# Eval("result_health") %></td>

                                                                            </tr>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </table>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>

                                        </b></td>
                                    </tr>


                                </table>

                            </div>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">แก้ไขการตรวจสุขภาพ</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <h3 class="text-center">
                                                        <asp:Label ID="lbFormName_HealthCheck_edit" runat="server" Text="แก้ไขการตรวจสุขภาพ" CssClass="control-label" Font-Bold="true" Font-Size="14"></asp:Label>
                                                    </h3>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="div_DetailHistoryHealthCheck" class="panel panel-default" runat="server">
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <asp:TextBox ID="txt_u0_historyhealth_idx_edit" runat="server" Text='<%# Eval("u0_historyhealth_idx") %>' Visible="false" />
                                                        <asp:TextBox ID="txt_u2_historyhealth_idx_edit" runat="server" Text='<%# Eval("u2_historyhealth_idx") %>' Visible="false" />
                                                        <asp:Label ID="rao_idx_detailtype_edit" Visible="false" Text='<%# Eval("m0_detail_typecheck_idx") %>' runat="server"></asp:Label>
                                                        <asp:RadioButtonList ID="rdoDetailType_edit" runat="server" CssClass="" Font-Bold="true" RepeatDirection="Vertical"
                                                            AutoPostBack="true">
                                                        </asp:RadioButtonList>

                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-6">
                                                        <label>ครั้งที่</label>
                                                        <asp:TextBox ID="txt_numcheck_edit" runat="server" Text='<%# Eval("num_check") %>' CssClass="form-control" Enabled="true">
                                                        </asp:TextBox>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <label>วันที่ตรวจสุขภาพ</label>
                                                        <div class="input-group date">
                                                            <asp:TextBox ID="txt_date_healthCheck_edit" runat="server" Text='<%# Eval("date_check") %>' CssClass="form-control from-date-datepicker" Enabled="true">
                                                            </asp:TextBox>
                                                            <span class="input-group-addon">
                                                                <span data-icon-element="" class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>

                                                </div>

                                                <asp:UpdatePanel ID="update_detailDoctorEdit" runat="server">
                                                    <ContentTemplate>
                                                        <div id="div_DetailDoctorCheck_edit" class="panel panel-default" runat="server">
                                                            <div class="panel-body">

                                                                <div class="form-group">
                                                                    <div class="col-md-6">
                                                                        <label>ชื่อแพทย์ผู้ตรวจ</label>
                                                                        <asp:TextBox ID="txtEmpIDX_Doctor_edit" runat="server" Text='<%# Eval("m0_doctor_idx") %>' Visible="false" />
                                                                        <asp:DropDownList ID="ddl_Doctorname_edit" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                                            AutoPostBack="true" />
                                                                    </div>

                                                                    <div class="col-md-6">
                                                                        <label>เลขที่ใบประกอบวิชาชีพ</label>
                                                                        <%--<asp:TextBox ID="txtEmpNewFullNameTHHidden" runat="server" Visible="false" />--%>
                                                                        <asp:TextBox ID="txtcard_number_doctor_edit" Text='<%# Eval("card_number") %>' runat="server" CssClass="form-control"
                                                                            placeholder="เลขที่ใบประกอบวิชาชีพ..." Enabled="false" />
                                                                    </div>

                                                                </div>

                                                                <div class="form-group">
                                                                    <div class="col-md-6">
                                                                        <label>ชื่อหน่วยงานที่ตรวจสุขภาพ</label>
                                                                        <asp:TextBox ID="txt_health_authority_name_edit" Text='<%# Eval("health_authority_name") %>' runat="server" CssClass="form-control"
                                                                            placeholder="ชื่อหน่วยงานที่ตรวจสุขภาพ..." Enabled="false" />

                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        <label>ตั้งอยู่เลขที่</label>
                                                                        <asp:TextBox ID="txt_location_name_edit" runat="server" Text='<%# Eval("location_name") %>' CssClass="form-control"
                                                                            placeholder="ตั้งอยู่เลขที่..." Enabled="false" />

                                                                    </div>

                                                                    <div class="col-md-2">
                                                                        <label>หมู่ที่</label>
                                                                        <asp:TextBox ID="txt_village_no_edit" runat="server" Text='<%# Eval("village_no") %>' CssClass="form-control"
                                                                            placeholder="หมู่ที่..." Enabled="false" />

                                                                    </div>

                                                                    <div class="col-md-2">
                                                                        <label>ถนน</label>
                                                                        <asp:TextBox ID="txt_road_name_edit" runat="server" Text='<%# Eval("road_name") %>' CssClass="form-control"
                                                                            placeholder="ถนน..." Enabled="false" />

                                                                    </div>
                                                                </div>

                                                            </div>

                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>

                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <label>ตรวจสุขภาพทั่วไป</label>
                                                    </div>
                                                    <asp:UpdatePanel ID="Update_GenaralHealth_edit" runat="server">
                                                        <ContentTemplate>
                                                            <div class="col-md-12">
                                                                <div id="div_genaralHealthCheck_edit" class="panel panel-default" runat="server">

                                                                    <div class="panel-body">
                                                                        <div class="form-group">
                                                                            <div class="col-md-12">
                                                                                <label>ผลการตรวจเบื้องต้น</label>
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group">

                                                                            <div class="col-md-4">
                                                                                <label>น้ำหนัก</label>
                                                                                <asp:TextBox ID="txt_weight_edit" Text='<%# Eval("weight") %>' runat="server" CssClass="form-control"
                                                                                    placeHolder="น้ำหนัก..." Enabled="true">
                                                                                </asp:TextBox>

                                                                            </div>
                                                                            <div class="col-md-2">
                                                                                <label>&nbsp;</label>
                                                                                <asp:TextBox ID="txt_unit_weight_edit" runat="server" Text="กิโลกรัม"
                                                                                    CssClass="form-control" Enabled="false">
                                                                                </asp:TextBox>

                                                                            </div>


                                                                            <div class="col-md-4">
                                                                                <label>ความสูง</label>
                                                                                <asp:TextBox ID="txt_height_edit" Text='<%# Eval("height") %>' runat="server" CssClass="form-control"
                                                                                    placeHolder="ความสูง..." Enabled="true">
                                                                                </asp:TextBox>

                                                                            </div>

                                                                            <div class="col-md-2">
                                                                                <label>&nbsp;</label>
                                                                                <asp:TextBox ID="txt_unit_height_edit" runat="server" Text="เซนติเมตร"
                                                                                    CssClass="form-control" Enabled="false">
                                                                                </asp:TextBox>
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group">

                                                                            <div class="col-md-2">
                                                                                <label>ดัชนีมวลกาย</label>
                                                                                <asp:TextBox ID="txt_body_mass_edit" runat="server" Text='<%# Eval("body_mass") %>' CssClass="form-control"
                                                                                    placeHolder="ดัชนีมวลกาย..." Enabled="true">
                                                                                </asp:TextBox>

                                                                            </div>

                                                                            <div class="col-md-3">
                                                                                <label>ความดันโลหิต</label>
                                                                                <asp:TextBox ID="txt_bloodpressure_edit" runat="server" Text='<%# Eval("bloodpressure") %>' CssClass="form-control"
                                                                                    placeHolder="ความดันโลหิต..." Enabled="true">
                                                                                </asp:TextBox>

                                                                            </div>

                                                                            <div class="col-md-2">
                                                                                <label>&nbsp;</label>
                                                                                <asp:TextBox ID="txt_bloodpressure_unit_edit" runat="server" Text="mm.Hg"
                                                                                    CssClass="form-control" Enabled="false">
                                                                                </asp:TextBox>
                                                                            </div>
                                                                            <div class="col-md-3">
                                                                                <label>ชีพจร</label>
                                                                                <asp:TextBox ID="txt_pulse_edit" runat="server" Text='<%# Eval("pulse") %>' CssClass="form-control"
                                                                                    placeHolder="ชีพจร..." Enabled="true">
                                                                                </asp:TextBox>

                                                                            </div>

                                                                            <div class="col-md-2">
                                                                                <label>&nbsp;</label>
                                                                                <asp:TextBox ID="txt_pulse_unit_edit" runat="server" Text="ครั้ง/นาที"
                                                                                    CssClass="form-control" Enabled="false">
                                                                                </asp:TextBox>
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <div class="col-md-12">
                                                                                <label>ผลการตรวจร่างกายตามระบบ</label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="col-md-12">
                                                                                <label>ระบุ</label>
                                                                                <asp:TextBox ID="txt_specify_resultbody_edit" Text='<%# Eval("specify_resultbody") %>' runat="server" CssClass="form-control"
                                                                                    placeHolder="ระบุผลการตรวจร่างกายตามระบบ..." Enabled="true">
                                                                                </asp:TextBox>
                                                                            </div>

                                                                        </div>

                                                                        <div class="form-group">
                                                                            <div class="col-md-12">
                                                                                <label>ผลการตรวจทางห้องปฏิบัติการ</label>
                                                                                <asp:TextBox ID="txt_resultlab_edit" runat="server" Text='<%# Eval("resultlab") %>' CssClass="form-control"
                                                                                    placeHolder="ระบุผลการตรวจทางห้องปฏิบัติการ..." Enabled="true">
                                                                                </asp:TextBox>
                                                                            </div>
                                                                        </div>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <label>ตรวจสุขภาพตามปัจจัยเสี่ยงของงาน</label>
                                                    </div>
                                                    <asp:UpdatePanel ID="Update_RikeHealthCheck_edit" runat="server">
                                                        <ContentTemplate>
                                                            <div class="col-md-12">
                                                                <div id="div_rikeHealthCheck_edit" class="panel panel-default" runat="server">
                                                                    <div class="panel-body">

                                                                        <div class="form-group">

                                                                            <div class="col-md-5">
                                                                                <label>ปัจจัยเสี่ยง</label>
                                                                                <asp:TextBox ID="txt_rikeHealthCheck_edit" runat="server" CssClass="form-control"
                                                                                    placeHolder="ปัจจัยเสี่ยง..." Enabled="true">
                                                                                </asp:TextBox>
                                                                            </div>

                                                                            <div class="col-md-5">
                                                                                <label>ผลการตรวจ</label>
                                                                                <asp:TextBox ID="txt_ResultHealthCheck_edit" runat="server" CssClass="form-control"
                                                                                    placeHolder="ผลการตรวจ..." Enabled="true">
                                                                                </asp:TextBox>
                                                                            </div>

                                                                            <div class="col-md-2">
                                                                                <label>&nbsp;</label>
                                                                                <div class="clearfix"></div>
                                                                                <asp:LinkButton ID="btnAddRikeTolistEdit" runat="server"
                                                                                    CssClass="btn btn-primary col-md-12"
                                                                                    Text="เพิ่มข้อมูล" OnCommand="btnCommand" CommandName="cmdAddRikeTolistEdit"
                                                                                    ValidationGroup="addAddRikeTolist" />
                                                                            </div>

                                                                        </div>

                                                                        <asp:GridView ID="gvRikeListEdit" runat="server"
                                                                            CssClass="table table-striped table-responsive"
                                                                            GridLines="None" OnRowCommand="onRowCommand"
                                                                            AutoGenerateColumns="false">
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center"
                                                                                    HeaderStyle-CssClass="text-center"
                                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller"
                                                                                    HeaderStyle-Width="10%">
                                                                                    <ItemTemplate>
                                                                                        <%# (Container.DataItemIndex + 1) %>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:BoundField DataField="drrikeHealthEditText" HeaderText="ปัจจัยเสี่ยง" ItemStyle-HorizontalAlign="center"
                                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller"
                                                                                    HeaderStyle-CssClass="text-center" />

                                                                                <asp:BoundField DataField="drResultHealthEditText" HeaderText="ผลการตรวจ" ItemStyle-HorizontalAlign="center"
                                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller"
                                                                                    HeaderStyle-CssClass="text-center" />

                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="center"
                                                                                    HeaderStyle-CssClass="text-center">
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="btnRemoveRikeHealthEdit" runat="server"
                                                                                            CssClass="btn btn-danger btn-xs"
                                                                                            OnClientClick="return confirm('คุณต้องการลบรายการนี้ใชหรือไม่ ?')"
                                                                                            CommandName="btnRemoveRikeHealthEdit">
                                                                                        <i class="fa fa-times"></i>
                                                                                        </asp:LinkButton>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>

                                                                        <div class="clearfix"></div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>

                                                </div>


                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <asp:LinkButton ID="btnEditHealthCheck" CssClass="btn btn-success" runat="server" OnCommand="btnCommand"
                                                            CommandName="cmdSaveEditHealthCheck" Text="บันทึกการเปลี่ยนแปลง" ValidationGroup="btnSave"
                                                            OnClientClick="return confirm('ยืนยันการบันทึก')" />
                                                        <asp:LinkButton CssClass="btn btn-danger" runat="server" OnCommand="btnCommand"
                                                            CommandName="cmdCancelEditHistory" Text="ยกเลิก" />
                                                    </div>
                                                </div>


                                            </div>

                                        </div>


                                    </div>
                                </div>
                            </div>
                        </EditItemTemplate>
                    </asp:FormView>
                </div>
            </div>


        </asp:View>
        <!--View Detail-->

        <!--View Create-->
        <asp:View ID="docCreateList" runat="server">
            <div id="divCreate" runat="server" class="col-md-12" visible="false">

                <asp:FormView ID="fvEmpDetail" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <ItemTemplate>
                        <asp:HiddenField ID="hfEmpOrgIDX" runat="server" Value='<%# Eval("org_idx") %>' />
                        <asp:HiddenField ID="hfEmpRdeptIDX" runat="server" Value='<%# Eval("rdept_idx") %>' />
                        <asp:HiddenField ID="hfEmpRsecIDX" runat="server" Value='<%# Eval("rsec_idx") %>' />
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายละเอียดผู้ใช้งาน</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">รหัสพนักงาน</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpCode" runat="server" CssClass="form-control" Text='<%# Eval("emp_code") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ชื่อ - นามสกุล</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpName" runat="server" CssClass="form-control" Text='<%# Eval("emp_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">องค์กร</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpOrg" runat="server" CssClass="form-control" Text='<%# Eval("org_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ฝ่าย</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpDept" runat="server" CssClass="form-control" Text='<%# Eval("dept_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">แผนก</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpSec" runat="server" CssClass="form-control" Text='<%# Eval("sec_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ตำแหน่ง</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpPos" runat="server" CssClass="form-control" Text='<%# Eval("pos_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:FormView>

                <div class="panel panel-default" id="div_SearchEmployee" runat="server">
                    <div class="panel-heading">
                        <h3 class="panel-title">ค้นหาชื่อพนักงานที่ทำการกรอกข้อมูล</h3>
                    </div>
                    <div class="panel-body">
                        <asp:FormView ID="fvEmpSearch" runat="server" DefaultMode="Insert" Width="100%">
                            <InsertItemTemplate>

                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Employee Code</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpCode_Search" runat="server" CssClass="form-control" MaxLength="8" placeholder="รหัสพนักงาน" />
                                        </div>
                                        <label class="col-sm-2 control-label">Name(TH/EN)</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpName_Search" runat="server" CssClass="form-control" placeholder="ชื่อพนักงาน(TH/EN)" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2"></label>
                                        <div class="col-sm-4">
                                            <asp:LinkButton ID="lbSearch" CssClass="btn btn-success" runat="server" data-original-title="Search" data-toggle="tooltip" Text="ค้นหา" OnCommand="btnCommand" CommandName="cmdSearch"></asp:LinkButton>
                                            <asp:LinkButton ID="lbReset" CssClass="btn btn-info" runat="server" data-original-title="Reset" data-toggle="tooltip" Text="ล้างค่า" OnCommand="btnCommand" CommandName="cmdReset"></asp:LinkButton>
                                        </div>
                                    </div>


                                </div>


                            </InsertItemTemplate>
                        </asp:FormView>
                        <asp:UpdatePanel ID="Update_DetailEmployee" runat="server" Visible="false">
                            <ContentTemplate>
                                <div id="gvEmployeeList_scroll" style="overflow-x: scroll; width: 100%" runat="server">
                                    <asp:GridView ID="gvEmployeeList" runat="server"
                                        AutoGenerateColumns="False"
                                        CssClass="table table-striped table-bordered table-hover table-responsive"
                                        OnPageIndexChanging="gvPageIndexChanging" AllowPaging="True" PageSize="10">
                                        <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                                        <RowStyle Font-Size="Small" />
                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                        <EmptyDataTemplate>
                                            <div style="text-align: center">ไม่พบข้อมูล</div>
                                        </EmptyDataTemplate>
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:UpdatePanel ID="updatechecked" runat="server">
                                                        <ContentTemplate>

                                                            <asp:LinkButton ID="btn_AddEmployee" CssClass="btn btn-success btn-sm" runat="server" data-toggle="tooltip"
                                                                title="Insert" CommandName="cmdAddEmployee" OnCommand="btnCommand"
                                                                CommandArgument='<%# Eval("emp_idx") %>'><i class="fa fa-plus-square"></i>
                                                            </asp:LinkButton>

                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="btn_AddEmployee" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </ItemTemplate>

                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Employee Code">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEmpIDX" runat="server" Visible="false" Text='<%# Eval("emp_idx") %>'></asp:Label>
                                                    <asp:Label ID="lblEmpCode" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Employee Name TH">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPrefixNameTH" runat="server" Text='<%# Eval("prefix_th") %>'></asp:Label>
                                                    <asp:Label ID="lblFirstnameTH" runat="server" Text='<%# Eval("emp_firstname_th") %>'></asp:Label>
                                                    <asp:Label ID="lblLastnameTH" runat="server" Text='<%# Eval("emp_lastname_th") %>'></asp:Label>
                                                    (<asp:Label ID="lblNicknameTH" runat="server" Text='<%# Eval("emp_nickname_th") %>'></asp:Label>)
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Position TH">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPositionTH" runat="server" Text='<%# Eval("pos_name_th") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Section TH">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSectionTH" runat="server" Text='<%# Eval("sec_name_th") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Department TH">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDepartmentTH" runat="server" Text='<%# Eval("dept_name_th") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Organization TH">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblOrganizationTH" runat="server" Text='<%# Eval("org_name_th") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>

                <asp:UpdatePanel ID="Update_BackToSearch" runat="server" Visible="false">
                    <ContentTemplate>
                        <div class="form-group">
                            <asp:LinkButton ID="btnCancelToSearch" CssClass="btn btn-danger" data-toggle="tooltip" title="" runat="server"
                                CommandName="cmdCancelToSerach" OnCommand="btnCommand">< ย้อนกลับไปค้นหา</asp:LinkButton>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>

                <div class="clearfix"></div>
                <!--Formview Add User Check History-->
                <asp:FormView ID="fvAddUserCheckHistory" runat="server" DefaultMode="Insert" Width="100%" Visible="false">
                    <InsertItemTemplate>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายชื่อผู้ที่ทำการบันทึก</h3>
                            </div>

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <label>รหัสพนักงาน</label>
                                            <asp:TextBox ID="txtEmpIDXHidden_Add" runat="server" Visible="false" />
                                            <asp:TextBox ID="txtEmpNewEmpCode_Add" runat="server" CssClass="form-control" placeholder="รหัสพนักงาน..." OnTextChanged="onTextChanged" AutoPostBack="true" MaxLength="8" autocomplete="off" />
                                        </div>

                                        <div class="col-md-6">
                                            <label>ชื่อ - นามสกุล</label>
                                            <%--<asp:TextBox ID="txtEmpNewFullNameTHHidden" runat="server" Visible="false" />--%>
                                            <asp:TextBox ID="txtEmpNewFullNameTH_Add" runat="server" CssClass="form-control" placeholder="ชื่อ - สกุล (TH)..." Enabled="false" />
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <label>บริษัท</label>
                                            <asp:TextBox ID="txtOrgIDXHidden_Add" runat="server" Visible="false" />
                                            <asp:TextBox ID="txtEmpNewOrgNameTH_Add" runat="server" CssClass="form-control" placeholder="บริษัท..." Enabled="false" />
                                        </div>

                                        <div class="col-md-6">
                                            <label>ฝ่าย</label>
                                            <asp:TextBox ID="txtRdepIDXHidden_Add" runat="server" Visible="false" />
                                            <asp:TextBox ID="txtEmpNewDeptNameTH_Add" runat="server" CssClass="form-control" placeholder="ฝ่าย..." Enabled="false" />
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <label>แผนก</label>
                                            <asp:TextBox ID="txtEmpNewSecNameTH_Add" runat="server" CssClass="form-control" placeholder="แผนก..." Enabled="false" />
                                        </div>

                                        <div class="col-md-6">
                                            <label>ตำแหน่ง</label>
                                            <asp:TextBox ID="txtRposIDXHidden_Add" runat="server" Visible="false" />
                                            <asp:TextBox ID="txtEmpNewPosNameTH_Add" runat="server" CssClass="form-control" placeholder="ตำแหน่ง..." Enabled="false" />
                                        </div>

                                    </div>


                                </div>
                            </div>

                        </div>
                    </InsertItemTemplate>
                </asp:FormView>
                <!--Formview Add User Check History-->

                <asp:FormView ID="fvDetailEmployeeHistory" runat="server" DefaultMode="ReadOnly" Width="100%" Visible="false">
                    <ItemTemplate>

                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายชื่อผู้ที่ทำการบันทึก</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <label>รหัสพนักงาน</label>
                                            <asp:TextBox ID="txtEmpIDXHidden_detail" runat="server" Text='<%# Eval("emp_idx") %>' Visible="false" />
                                            <asp:TextBox ID="txtEmpNewEmpCode_detail" runat="server" CssClass="form-control" Text='<%# Eval("emp_code") %>' Enabled="false" placeholder="รหัสพนักงาน..." MaxLength="8" autocomplete="off" />
                                        </div>

                                        <div class="col-md-6">
                                            <label>ชื่อ - นามสกุล</label>
                                            <%--<asp:TextBox ID="txtEmpNewFullNameTHHidden" runat="server" Visible="false" />--%>
                                            <asp:TextBox ID="txtEmpNewFullNameTH_detail" Text='<%# Eval("emp_firstname_th") + " " + Eval("emp_lastname_th") %>' runat="server" CssClass="form-control" placeholder="ชื่อ - สกุล (TH)..." Enabled="false" />
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <label>บริษัท</label>

                                            <asp:TextBox ID="txtEmpNewOrgNameTH_detail" Text='<%# Eval("org_name_th") %>' runat="server" CssClass="form-control" placeholder="บริษัท..." Enabled="false" />
                                        </div>

                                        <div class="col-md-6">
                                            <label>ฝ่าย</label>

                                            <asp:TextBox ID="txtEmpNewDeptNameTH_detail" Text='<%# Eval("dept_name_th") %>' runat="server" CssClass="form-control" placeholder="ฝ่าย..." Enabled="false" />
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <label>แผนก</label>
                                            <asp:TextBox ID="txtEmpNewSecNameTH_detail" runat="server" Text='<%# Eval("sec_name_th") %>' CssClass="form-control" placeholder="แผนก..." Enabled="false" />
                                        </div>

                                        <div class="col-md-6">
                                            <label>ตำแหน่ง</label>

                                            <asp:TextBox ID="txtEmpNewPosNameTH_detail" runat="server" Text='<%# Eval("pos_name_th") %>' CssClass="form-control" placeholder="ตำแหน่ง..." Enabled="false" />
                                        </div>

                                    </div>


                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:FormView>

                <div class="form-group" id="div_showname_Form" runat="server" visible="false">
                    <asp:Repeater ID="rptBindNameForm" OnItemDataBound="rptOnRowDataBound" runat="server">
                        <ItemTemplate>
                            <asp:Label ID="lbcheck_coler_nameform" runat="server" Visible="false" Text='<%# Eval("m0_type_idx") %>'></asp:Label>
                            <asp:LinkButton ID="btnNameForm" CssClass="" runat="server" data-original-title='<%# Eval("type_name") %>' data-toggle="tooltip" CommandArgument='<%# Eval("m0_type_idx")%>' OnCommand="btnCommand" Text='<%# Eval("type_name") %>' CommandName="cmdNameForm"></asp:LinkButton>

                        </ItemTemplate>
                    </asp:Repeater>
                </div>

                <hr />

                <!--Form History Work-->
                <div id="div_HistoryWork_Check" runat="server" visible="true">
                    <asp:FormView ID="fvWorkHistoryForm" runat="server" DefaultMode="ReadOnly" Width="100%">
                        <InsertItemTemplate>
                            <div class="panel panel-default">
                                <div class="panel-body">

                                    <div class="form-horizontal" role="form">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <h3 class="text-center">
                                                    <asp:Label ID="lbFormName_Injury" runat="server" Text="ประวัติการทำงาน" CssClass="control-label" Font-Bold="true" Font-Size="14"></asp:Label>
                                                </h3>
                                            </div>
                                        </div>
                                        <div id="div_DetailHistoryWork" class="panel panel-default" runat="server">
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <div class="col-md-6">
                                                        <label>ชื่อสถานประกอบการ</label>
                                                        <asp:TextBox ID="txt_business_work" runat="server" CssClass="form-control" Enabled="true">
                                                        </asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="Re_txt_business_work"
                                                            runat="server"
                                                            ControlToValidate="txt_business_work" Display="None"
                                                            SetFocusOnError="true"
                                                            ErrorMessage="*กรุณากรอกชื่อสถานประกอบการ"
                                                            ValidationGroup="AddHistoryWorkList" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="Validator_txt_business_work" runat="Server" PopupPosition="BottomLeft"
                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_business_work" Width="180" />
                                                    </div>

                                                    <div class="col-md-6">

                                                        <label>แผนก</label>
                                                        <asp:TextBox ID="txt_Department_work" runat="server" CssClass="form-control" Enabled="true">
                                                        </asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="Re_txt_Department_work"
                                                            runat="server"
                                                            ControlToValidate="txt_Department_work" Display="None"
                                                            SetFocusOnError="true"
                                                            ErrorMessage="*กรุณากรอกแผนก"
                                                            ValidationGroup="AddHistoryWorkList" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="Validator_txt_Department_work" runat="Server" PopupPosition="BottomLeft"
                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_Department_work" Width="180" />
                                                    </div>

                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-6">
                                                        <label>ประเภทกิจการ</label>
                                                        <asp:TextBox ID="txt_business_typework" runat="server" CssClass="form-control" Enabled="true">
                                                        </asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="Re_txt_business_typework"
                                                            runat="server"
                                                            ControlToValidate="txt_business_typework" Display="None"
                                                            SetFocusOnError="true"
                                                            ErrorMessage="*กรุณากรอกประเภทกิจการ"
                                                            ValidationGroup="AddHistoryWorkList" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="Validator_txt_business_typework" runat="Server" PopupPosition="BottomLeft"
                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_business_typework" Width="180" />
                                                    </div>

                                                    <div class="col-md-6">

                                                        <label>ลักษณะงานที่ทำ</label>
                                                        <asp:TextBox ID="txt_jobdescription_work" runat="server" CssClass="form-control" Enabled="true">
                                                        </asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="Re_txt_jobdescription_work"
                                                            runat="server"
                                                            ControlToValidate="txt_jobdescription_work" Display="None"
                                                            SetFocusOnError="true"
                                                            ErrorMessage="*กรุณากรอกลักษณะงานที่ทำ"
                                                            ValidationGroup="AddHistoryWorkList" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="Validator_txt_jobdescription_work" runat="Server" PopupPosition="BottomLeft"
                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_jobdescription_work" Width="180" />
                                                    </div>

                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-6">
                                                        <label>วัน/เดือน/ปี ที่เริ่มทำ</label>
                                                        <div class='input-group date'>
                                                            <asp:TextBox ID="txt_StartDate_work" runat="server" CssClass="form-control datetimepicker-filter-perm-from cursor-pointer" Enabled="true">
                                                            </asp:TextBox>
                                                            <span class="input-group-addon show-filter-perm-from-onclick">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                            <asp:RequiredFieldValidator ID="Re_txt_StartDate_work"
                                                                runat="server"
                                                                ControlToValidate="txt_StartDate_work" Display="None"
                                                                SetFocusOnError="true"
                                                                ErrorMessage="*กรุณากรอก วัน/เดือน/ปี ที่เริ่มทำ"
                                                                ValidationGroup="AddHistoryWorkList" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="Validator_txt_StartDate_work" runat="Server" PopupPosition="BottomLeft"
                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_StartDate_work" Width="180" />
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">

                                                        <label>วัน/เดือน/ปี ที่สิ้นสุด</label>
                                                        <div class='input-group date'>
                                                            <asp:TextBox ID="txt_EndDate_work" runat="server" CssClass="form-control datetimepicker-filter-perm-to cursor-pointer" Enabled="true">
                                                            </asp:TextBox>
                                                            <span class="input-group-addon show-filter-perm-to-onclick">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                            <asp:RequiredFieldValidator ID="Re_txt_EndDate_work"
                                                                runat="server"
                                                                ControlToValidate="txt_EndDate_work" Display="None"
                                                                SetFocusOnError="true"
                                                                ErrorMessage="*กรุณากรอก วัน/เดือน/ปี ที่สิ้นสุด"
                                                                ValidationGroup="AddHistoryWorkList" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="Validator_txt_EndDate_work" runat="Server" PopupPosition="BottomLeft"
                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_EndDate_work" Width="180" />
                                                        </div>


                                                    </div>

                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-6">
                                                        <label>ปัจจัยที่เสี่ยงต่อสุขภาพ</label>
                                                        <asp:TextBox ID="txt_HealthRisk_work" runat="server" CssClass="form-control" Enabled="true">
                                                        </asp:TextBox>

                                                    </div>

                                                    <div class="col-md-6">

                                                        <label>มี/ใช้อุปกรณ์ป้องกันอันตราย (ระบุชนิด)</label>
                                                        <asp:TextBox ID="txt_UseMaterial_work" runat="server" CssClass="form-control" Enabled="true">
                                                        </asp:TextBox>

                                                    </div>

                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <asp:LinkButton ID="btnAddWorkTolist" runat="server"
                                                            CssClass="btn btn-primary"
                                                            Text="เพิ่มประวัติการทำงาน" OnCommand="btnCommand" CommandName="cmdAddHistoryWorkList"
                                                            ValidationGroup="AddHistoryWorkList" />
                                                    </div>
                                                </div>



                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                        </InsertItemTemplate>

                    </asp:FormView>

                    <asp:UpdatePanel ID="Update_GridHistoryWork" runat="server" Visible="false">
                        <ContentTemplate>
                            <div id="gvHistoryWorkList_scroll" style="overflow-x: scroll; width: 100%" runat="server">
                                <asp:GridView ID="gvHistoryWorkList"
                                    RowStyle-Wrap="false"
                                    runat="server"
                                    BorderStyle="None"
                                    CssClass="table table-striped table-bordered table-responsive"
                                    OnRowCommand="onRowCommand"
                                    AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center"
                                            HeaderStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller">
                                            <ItemTemplate>
                                                <%# (Container.DataItemIndex + 1) %>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:BoundField DataField="drbusiness_workText" HeaderText="ชื่อสถานประกอบการ" ItemStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                        <asp:BoundField DataField="drDepartment_workText" HeaderText="แผนก" ItemStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                        <asp:BoundField DataField="drbusiness_typeworkText" HeaderText="ประเภทกิจการ" ItemStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                        <asp:BoundField DataField="drjobdescription_workText" HeaderText="ลักษณะงานที่ทำ" ItemStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                        <asp:BoundField DataField="drStartDate_workText" HeaderText="วัน/เดือน/ปี ที่เริ่มทำ" ItemStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                        <asp:BoundField DataField="drEndDate_workText" HeaderText="วัน/เดือน/ปี ที่สิ้นสุด" ItemStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                        <asp:BoundField DataField="drHealthRisk_workText" HeaderText="ปัจจัยที่เสี่ยงต่อสุขภาพ" ItemStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                        <asp:BoundField DataField="drUseMaterial_workText" HeaderText="มี/ใช้อุปกรณ์ป้องกันอันตราย (ระบุชนิด)" ItemStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                        <asp:TemplateField ItemStyle-HorizontalAlign="center"
                                            HeaderStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btnRemoveHistoryWorkList" runat="server"
                                                    CssClass="btn btn-danger btn-xs"
                                                    OnClientClick="return confirm('คุณต้องการลบรายการนี้ใชหรือไม่ ?')"
                                                    CommandName="btnRemoveHistoryWorkList"><i class="fa fa-times"></i>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <br />

                    <asp:UpdatePanel ID="Update_SaveHistoryCheck" runat="server" Visible="false">
                        <ContentTemplate>
                            <div class="form-group" id="div_saveHistoryWork" runat="server">
                                <asp:LinkButton ID="btnInsertHistoryWorkCheck" CssClass="btn btn-success" runat="server" OnCommand="btnCommand"
                                    CommandName="cmdInsertHistoryWork" Text="บันทึก" ValidationGroup="btnSave"
                                    OnClientClick="return confirm('ยืนยันการบันทึก')" />
                                <asp:LinkButton CssClass="btn btn-danger" runat="server" OnCommand="btnCommand"
                                    CommandName="btnCancel" Text="ยกเลิก" />

                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </div>
                <!--Form History Worke-->

                <!--Form History Sick-->
                <asp:FormView ID="fvSickCheckForm" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <InsertItemTemplate>

                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <h3 class="text-center">
                                                    <asp:Label ID="lbFormName_SickCheck" runat="server" Text="ประวัติการเจ็บป่วย" CssClass="control-label" Font-Bold="true" Font-Size="14"></asp:Label>
                                                </h3>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="div_SickCheck" class="panel panel-default" runat="server">

                                        <div class="panel-body">

                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label>เคยป่วยเป็นโรคหรือมีการบาดเจ็บ</label>
                                                </div>

                                                <asp:UpdatePanel ID="update_eversickInsert" runat="server">
                                                    <ContentTemplate>
                                                        <div class="col-md-12">
                                                            <div id="div_DetailEverSickCheck" class="panel panel-default" runat="server">
                                                                <div class="panel-body">
                                                                    <div class="form-group">
                                                                        <div class="col-md-5">
                                                                            <label>&nbsp;</label>
                                                                            <asp:TextBox ID="txt_eversick_insert" runat="server" CssClass="form-control"
                                                                                placeholder="เคยป่วยเป็นโรคหรืออาการบาดเจ็บ..." Enabled="true" />

                                                                        </div>
                                                                        <div class="col-md-5">
                                                                            <label>เมื่อปี พ.ศ.</label>
                                                                            <asp:TextBox ID="txt_lastyear_insert" runat="server" MaxLength="4" CssClass="form-control"
                                                                                placeholder="เมื่อปี พ.ศ...." Enabled="true" />

                                                                        </div>
                                                                        <div class="col-md-2">
                                                                            <label>&nbsp;</label>
                                                                            <div class="clearfix"></div>
                                                                            <asp:LinkButton ID="btnAddEverSickList" runat="server"
                                                                                CssClass="btn btn-primary col-md-12"
                                                                                Text="เพิ่มข้อมูล" OnCommand="btnCommand" CommandName="cmdAddEverSickList"
                                                                                ValidationGroup="addAddEverSickList" />
                                                                        </div>

                                                                    </div>

                                                                    <asp:GridView ID="gvEverSickList" runat="server"
                                                                        CssClass="table table-striped table-responsive"
                                                                        GridLines="None" OnRowCommand="onRowCommand"
                                                                        AutoGenerateColumns="false">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center"
                                                                                HeaderStyle-CssClass="text-center"
                                                                                HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller"
                                                                                HeaderStyle-Width="10%">
                                                                                <ItemTemplate>
                                                                                    <%# (Container.DataItemIndex + 1) %>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:BoundField DataField="drEversickText" HeaderText="เคยป่วยเป็นโรค" ItemStyle-HorizontalAlign="center"
                                                                                HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller"
                                                                                HeaderStyle-CssClass="text-center" />

                                                                            <asp:BoundField DataField="drLastyearText" HeaderText="เมื่อปี พ.ศ." ItemStyle-HorizontalAlign="center"
                                                                                HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller"
                                                                                HeaderStyle-CssClass="text-center" />

                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="center"
                                                                                HeaderStyle-CssClass="text-center">
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="btnRemoveEverSickHealth" runat="server"
                                                                                        CssClass="btn btn-danger btn-xs"
                                                                                        OnClientClick="return confirm('คุณต้องการลบรายการนี้ใชหรือไม่ ?')"
                                                                                        CommandName="btnRemoveEverSickHealth">
                                                               <i class="fa fa-times"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>

                                                                    <div class="clearfix"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label>มีโรคประจำตัวหรือโรคเรื้อรังหรือไม่</label>
                                                </div>

                                                <div class="col-md-12">
                                                    <label>ระบุ</label>
                                                    <asp:TextBox ID="txt_havedisease" runat="server" CssClass="form-control" placeholder="ระบุโรคประจำตัวหรือโรคเรื้อรัง...." Enabled="true" />
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label>เคยได้รับการผ่าตัดหรือไม่</label>
                                                </div>

                                                <div class="col-md-12">
                                                    <label>ระบุ</label>
                                                    <asp:TextBox ID="txt_surgery" runat="server" CssClass="form-control" placeholder="ระบุเคยได้รับการผ่าตัด...." Enabled="true" />
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label>เคยได้รับภูมิคุ้มกันโรคกรณีเกิดโรคระบาด หรือเพื่อป้องกันโรคติดต่อหรือไม่</label>
                                                </div>

                                                <div class="col-md-12">
                                                    <label>ระบุ</label>
                                                    <asp:TextBox ID="txt_Immune" runat="server" CssClass="form-control" placeholder="ระบุเคยได้รับภูมิคุ้มกันโรคกรณีเกิดโรคระบาด...." Enabled="true" />
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label>ประวัติการเจ็บป่วยของสมาชิกในครอบครัว (เช่น มะเร็ง โลหิตจาง วัณโรค เบาหวาน หอบหืด ภูมิแพ้ เป็นต้น)</label>
                                                </div>

                                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                    <ContentTemplate>
                                                        <div class="col-md-12">
                                                            <div id="div_Identifyrelationship" class="panel panel-default" runat="server">
                                                                <div class="panel-body">
                                                                    <div class="form-group">
                                                                        <div class="col-md-12">
                                                                            <label>ระบุความสัมพันธ์และโรค</label>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <div class="col-md-5">
                                                                            <label>ความสัมพันธ์</label>
                                                                            <asp:TextBox ID="txt_relation_family" runat="server" CssClass="form-control"
                                                                                placeholder="ความสัมพันธ์..." Enabled="true" />

                                                                        </div>
                                                                        <div class="col-md-5">
                                                                            <label>โรค</label>
                                                                            <asp:TextBox ID="txt_Disease_Family" runat="server" CssClass="form-control"
                                                                                placeholder="โรค...." Enabled="true" />

                                                                        </div>
                                                                        <div class="col-md-2">
                                                                            <label>&nbsp;</label>
                                                                            <div class="clearfix"></div>
                                                                            <asp:LinkButton ID="btnAddDiseaseFamily" runat="server"
                                                                                CssClass="btn btn-primary col-md-12"
                                                                                Text="เพิ่มข้อมูล" OnCommand="btnCommand" CommandName="cmdAddDiseaseFamily"
                                                                                ValidationGroup="addAddDiseaseFamilyList" />
                                                                        </div>


                                                                    </div>

                                                                    <asp:GridView ID="gvDiseaseFamily" runat="server"
                                                                        CssClass="table table-striped table-responsive"
                                                                        GridLines="None" OnRowCommand="onRowCommand"
                                                                        AutoGenerateColumns="false">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center"
                                                                                HeaderStyle-CssClass="text-center"
                                                                                HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller"
                                                                                HeaderStyle-Width="10%">
                                                                                <ItemTemplate>
                                                                                    <%# (Container.DataItemIndex + 1) %>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:BoundField DataField="drRelation_familyText" HeaderText="ความสัมพันธ์" ItemStyle-HorizontalAlign="center"
                                                                                HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller"
                                                                                HeaderStyle-CssClass="text-center" />

                                                                            <asp:BoundField DataField="drDisease_FamilyText" HeaderText="โรค" ItemStyle-HorizontalAlign="center"
                                                                                HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller"
                                                                                HeaderStyle-CssClass="text-center" />

                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="center"
                                                                                HeaderStyle-CssClass="text-center">
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="btnRemoveRelationFamily" runat="server"
                                                                                        CssClass="btn btn-danger btn-xs"
                                                                                        OnClientClick="return confirm('คุณต้องการลบรายการนี้ใชหรือไม่ ?')"
                                                                                        CommandName="btnRemoveRelationFamily">
                                                                                <i class="fa fa-times"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>

                                                                    <div class="clearfix"></div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label>ปัจจุบันมียาที่จำเป็นต้องรับประทานเป็นประจำบ้างหรือไม่</label>
                                                </div>

                                                <div class="col-md-12">
                                                    <label>ระบุ</label>
                                                    <asp:TextBox ID="txt_DrugsEat" runat="server" CssClass="form-control" placeholder="ระบุปัจจุบันมียาที่จำเป็นต้องรับประทานเป็นประจำ...." Enabled="true" />
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label>มีประวัติการแพ้ยาหรือไม่</label>
                                                </div>

                                                <div class="col-md-12">
                                                    <label>ระบุ</label>
                                                    <asp:TextBox ID="txt_Allergyhistory" runat="server" CssClass="form-control" placeholder="ระบุประวัติการแพ้ยา...." Enabled="true" />
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label>เคยสูบบุหรี่บ้างหรือไม่</label>
                                                    <asp:RadioButtonList ID="rdoSmoking" runat="server" OnSelectedIndexChanged="onSelectedIndexChanged" AutoPostBack="true">
                                                        <asp:ListItem Value="1">&nbsp;ไม่เคย</asp:ListItem>
                                                        <asp:ListItem Value="2">&nbsp;เคยและปัจจุบันยังสูบอยู่ปริมาณ</asp:ListItem>
                                                        <asp:ListItem Value="3">&nbsp;เคยแต่เลิกแล้ว ระยะที่เคยสูบนาน</asp:ListItem>
                                                    </asp:RadioButtonList>

                                                    <%-- <asp:RequiredFieldValidator ID="Re_rdoSmoking2" runat="server" InitialValue="0"
                                                        ControlToValidate="rdoSmoking" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณาเลือกยี่ห้อ/รุ่น" ValidationGroup="saveFormRegis" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValddlBrand" runat="Server" PopupPosition="BottomRight"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_rdoSmoking2" Width="180" />--%>

                                                    <asp:RequiredFieldValidator ID="Require_rdoSmoking" runat="server"
                                                        InitialValue="0"
                                                        ControlToValidate="rdoSmoking" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณาเลือกเคยสูบบุหรี่บ้างหรือไม่" ValidationGroup="btnSave" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" PopupPosition="BottomRight"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Require_rdoSmoking" Width="250" />
                                                </div>


                                                <asp:UpdatePanel ID="updateInsert_stopsmoking" runat="server" Visible="false">
                                                    <ContentTemplate>
                                                        <div class="col-md-3">
                                                            <%--<label>ปี</label>--%>
                                                            <asp:TextBox ID="txt_year_smoking" runat="server" CssClass="form-control" placeholder="กรอกจำนวนปี...." Enabled="true" />

                                                            <asp:RequiredFieldValidator ID="Required_txt_year_smoking" runat="server"
                                                                ControlToValidate="txt_year_smoking" Display="None" SetFocusOnError="true"
                                                                ErrorMessage="*กรุณากรอกจำนวนปี" ValidationGroup="btnSave" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" PopupPosition="BottomRight"
                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Required_txt_year_smoking" Width="250" />

                                                            <asp:RegularExpressionValidator ID="Required_txt_year_smoking1" runat="server"
                                                                ValidationGroup="btnSave"
                                                                Display="None"
                                                                SetFocusOnError="true"
                                                                ControlToValidate="txt_year_smoking"
                                                                Font-Size="13px" ForeColor="Red"
                                                                ValidationExpression="^\d+?$"
                                                                ErrorMessage="*จำนวนปีไม่ถูกต้อง" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender18" runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                                TargetControlID="Required_txt_year_smoking1" Width="250" PopupPosition="BottomRight" />
                                                        </div>

                                                        <div class="col-md-1">
                                                            <%--<label>ปี</label>--%>
                                                            <asp:TextBox ID="txt_unit_year_smoking" runat="server" Text="ปี" CssClass="form-control" Enabled="false" />
                                                        </div>

                                                        <div class="col-md-3">
                                                            <%--<label>เดือน</label>--%>
                                                            <asp:TextBox ID="txt_month_smoking" runat="server" CssClass="form-control" placeholder="กรอกจำนวนเดือน...." Enabled="true" />

                                                            <asp:RequiredFieldValidator ID="Required_txt_month_smoking" runat="server"
                                                                ControlToValidate="txt_month_smoking" Display="None" SetFocusOnError="true"
                                                                ErrorMessage="*กรุณากรอกจำนวนเดือน" ValidationGroup="btnSave" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" PopupPosition="BottomRight"
                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Required_txt_month_smoking" Width="250" />

                                                            <asp:RegularExpressionValidator ID="Required_txt_month_smoking1" runat="server"
                                                                ValidationGroup="btnSave"
                                                                Display="None"
                                                                SetFocusOnError="true"
                                                                ControlToValidate="txt_month_smoking"
                                                                Font-Size="13px" ForeColor="Red"
                                                                ValidationExpression="^\d+?$"
                                                                ErrorMessage="*จำนวนเดือนไม่ถูกต้อง" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender19" runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                                TargetControlID="Required_txt_month_smoking1" Width="250" PopupPosition="BottomRight" />
                                                        </div>

                                                        <div class="col-md-1">
                                                            <%--<label>ปี</label>--%>
                                                            <asp:TextBox ID="txt_unit_month_smoking" runat="server" Text="เดือน" CssClass="form-control" Enabled="false" />
                                                        </div>

                                                        <div class="col-md-3">
                                                            <%--<label>ปริมาณขณะก่อนเลิก(มวน/วัน)</label>--%>
                                                            <asp:TextBox ID="txt_before_stopsmoking" runat="server" CssClass="form-control" placeholder="กรอกจำนวน มวน/วัน...." Enabled="true" />

                                                            <asp:RequiredFieldValidator ID="Required_txt_before_stopsmoking" runat="server"
                                                                ControlToValidate="txt_before_stopsmoking" Display="None" SetFocusOnError="true"
                                                                ErrorMessage="*กรุณากรอกจำนวน มวน/วัน" ValidationGroup="btnSave" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" PopupPosition="BottomRight"
                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Required_txt_before_stopsmoking" Width="250" />

                                                            <asp:RegularExpressionValidator ID="Required_txt_before_stopsmoking1" runat="server"
                                                                ValidationGroup="btnSave"
                                                                Display="None"
                                                                SetFocusOnError="true"
                                                                ControlToValidate="txt_before_stopsmoking"
                                                                Font-Size="13px" ForeColor="Red"
                                                                ValidationExpression="^\d+?$"
                                                                ErrorMessage="*จำนวนมวนไม่ถูกต้อง" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender20" runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                                TargetControlID="Required_txt_before_stopsmoking1" Width="250" PopupPosition="BottomRight" />

                                                        </div>
                                                        <div class="col-md-1">
                                                            <%--<label>ปี</label>--%>
                                                            <asp:TextBox ID="txt_unit_before_stopsmoking" runat="server" Text="มวน/วัน" CssClass="form-control" Enabled="false" />
                                                        </div>

                                                    </ContentTemplate>
                                                </asp:UpdatePanel>

                                                <asp:UpdatePanel ID="updateInsert_eversmoking" runat="server" Visible="false">
                                                    <ContentTemplate>
                                                        <div class="col-md-5">
                                                            <%--<label>ปริมาณ(มวน/วัน)</label>--%>
                                                            <asp:TextBox ID="txt_valum_smoking" runat="server" CssClass="form-control" placeholder="กรอกปริมาณที่ยังสูบอยู่ในปัจจุบัน...." Enabled="true" />

                                                            <asp:RequiredFieldValidator ID="Required_txt_valum_smoking" runat="server"
                                                                ControlToValidate="txt_valum_smoking" Display="None" SetFocusOnError="true"
                                                                ErrorMessage="*กรุณากรอกปริมาณที่ยังสูบอยู่ในปัจจุบัน" ValidationGroup="btnSave" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" PopupPosition="BottomRight"
                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Required_txt_valum_smoking" Width="250" />
                                                        </div>
                                                        <div class="col-md-1">
                                                            <%--<label>ปี</label>--%>
                                                            <asp:TextBox ID="txt_unit_valum_smoking" runat="server" Text="มวน/วัน" CssClass="form-control" Enabled="false" />
                                                        </div>

                                                        <div class="col-md-6">
                                                            <label>&nbsp;</label>
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>

                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label>เคยดื่มสุรา เบียร์ หรือเครื่องดื่มที่มีแอลกอฮอล์บ้างหรือไม่</label>
                                                    <asp:RadioButtonList ID="rdo_alcohol" runat="server" OnSelectedIndexChanged="onSelectedIndexChanged" AutoPostBack="true">
                                                        <asp:ListItem Value="1">&nbsp;ไม่เคย</asp:ListItem>
                                                        <asp:ListItem Value="2">&nbsp;โดยปกติดื่มน้อยกว่า 1 ครั้งต่อสัปดาห์</asp:ListItem>
                                                        <asp:ListItem Value="3">&nbsp;ดื่ม 1 ครั้งต่อสัปดาห์</asp:ListItem>
                                                        <asp:ListItem Value="4">&nbsp;ดื่ม 2-3 ครั้งต่อสัปดาห์</asp:ListItem>
                                                        <asp:ListItem Value="5">&nbsp;ดื่มมากกว่า 3 ครั้งต่อสัปดาห์</asp:ListItem>
                                                        <asp:ListItem Value="6">&nbsp;เคยแต่เลิกแล้วระยะเวลาที่ดื่มนาน</asp:ListItem>
                                                    </asp:RadioButtonList>

                                                    <asp:RequiredFieldValidator ID="Required_rdo_alcohol" runat="server"
                                                        ControlToValidate="rdo_alcohol" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณาเลือกเคยดื่มสุรา เบียร์ หรือเครื่องดื่มที่มีแอลกอฮอล์บ้างหรือไม่" ValidationGroup="btnSave" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" PopupPosition="BottomRight"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Required_rdo_alcohol" Width="250" />

                                                </div>

                                                <asp:UpdatePanel ID="updateInsert_alcohol" runat="server" Visible="false">
                                                    <ContentTemplate>
                                                        <div class="col-md-5">
                                                            <%--<label>ปี</label>--%>
                                                            <asp:TextBox ID="txt_year_alcohol" runat="server" CssClass="form-control" MaxLength="2" placeholder="กรอกจำนวนปี...." Enabled="true" />

                                                            <asp:RequiredFieldValidator ID="Required_txt_year_alcohol" runat="server"
                                                                ControlToValidate="txt_year_alcohol" Display="None" SetFocusOnError="true"
                                                                ErrorMessage="*กรุณากรอกจำนวนปี" ValidationGroup="btnSave" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" PopupPosition="BottomRight"
                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Required_txt_year_alcohol" Width="250" />

                                                            <asp:RegularExpressionValidator ID="Required_txt_year_alcohol1" runat="server"
                                                                ValidationGroup="btnSave"
                                                                Display="None"
                                                                SetFocusOnError="true"
                                                                ControlToValidate="txt_year_alcohol"
                                                                Font-Size="13px" ForeColor="Red"
                                                                ValidationExpression="^\d+?$"
                                                                ErrorMessage="*จำนวนปีไม่ถูกต้อง" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="toolkitRegExTxtEmpVisitorIDCard" runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                                TargetControlID="Required_txt_year_alcohol1" Width="250" PopupPosition="BottomRight" />

                                                        </div>
                                                        <div class="col-md-1">
                                                            <%--<label>ปี</label>--%>
                                                            <asp:TextBox ID="txt__unit_year_alcohol" runat="server" CssClass="form-control" Text="ปี" Enabled="false" />
                                                        </div>
                                                        <div class="col-md-5">
                                                            <%--<label>เดือน</label>--%>
                                                            <asp:TextBox ID="txt_month_alcohol" runat="server" CssClass="form-control" placeholder="กรอกจำนวนเดือน...." Enabled="true" />

                                                            <asp:RequiredFieldValidator ID="Required_txt_month_alcohol" runat="server"
                                                                ControlToValidate="txt_month_alcohol" Display="None" SetFocusOnError="true"
                                                                ErrorMessage="*กรุณากรอกจำนวนเดือน" ValidationGroup="btnSave" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server" PopupPosition="BottomRight"
                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Required_txt_month_alcohol" Width="250" />

                                                            <asp:RegularExpressionValidator ID="Required_txt_month_alcohol1" runat="server"
                                                                ValidationGroup="btnSave"
                                                                Display="None"
                                                                SetFocusOnError="true"
                                                                ControlToValidate="txt_month_alcohol"
                                                                Font-Size="13px" ForeColor="Red"
                                                                ValidationExpression="^\d+?$"
                                                                ErrorMessage="*จำนวนเดือนไม่ถูกต้อง" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender17" runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                                TargetControlID="Required_txt_month_alcohol1" Width="250" PopupPosition="BottomRight" />
                                                        </div>

                                                        <div class="col-md-1">
                                                            <%--<label>ปี</label>--%>
                                                            <asp:TextBox ID="txt_unit_month_alcohol" runat="server" CssClass="form-control" Text="เดือน" Enabled="false" />
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label>เคยเสพยาเสพติดหรือสารเสพติดใดๆ บ้างหรือไม่</label>
                                                </div>

                                                <div class="col-md-12">
                                                    <label>ระบุ</label>
                                                    <asp:TextBox ID="txt_addict_drug" runat="server" CssClass="form-control" placeholder="ระบุเคยเสพยาเสพติดหรือสารเสพติด...." Enabled="true" />
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label>ข้อมูลทางสุขภาพอื่นๆ ที่เป็นประโยชน์</label>
                                                    <asp:TextBox ID="txt_datahealth" runat="server" CssClass="form-control" placeholder="ระบุเข้อมูลทางสุขภาพอื่นๆ ที่เป็นประโยชน์...." Enabled="true" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <asp:LinkButton ID="btnSaveSickList" CssClass="btn btn-success" runat="server" OnCommand="btnCommand"
                                                        CommandName="cmdSaveSickList" Text="บันทึก" ValidationGroup="btnSave"
                                                        OnClientClick="return confirm('ยืนยันการบันทึก')" />
                                                    <asp:LinkButton CssClass="btn btn-danger" runat="server" OnCommand="btnCommand"
                                                        CommandName="btnCancel" Text="ยกเลิก" />
                                                </div>
                                            </div>

                                        </div>


                                    </div>


                                </div>
                            </div>
                        </div>
                    </InsertItemTemplate>
                </asp:FormView>
                <!--Form History Sick-->

                <!--Form History Injury-->
                <asp:FormView ID="fvInjuryHistoryForm" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <InsertItemTemplate>

                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <h3 class="text-center">
                                                    <asp:Label ID="lbFormName_Injury" runat="server" Text="บันทึกเกี่ยวกับการบาดเจ็บหรือเจ็บป่วยเนื่องจากการทำงานและสาเหตุ"
                                                        CssClass="control-label" Font-Bold="true" Font-Size="14"></asp:Label>
                                                </h3>
                                            </div>

                                        </div>
                                    </div>

                                    <div id="div_DetailHistoryInjury" class="panel panel-default" runat="server">
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <div class="col-md-6">
                                                    <label>วัน/เดือน/ปี</label>
                                                    <div class="input-group date">
                                                        <asp:TextBox ID="txt_date_injury" runat="server" CssClass="form-control from-date-datepicker" Enabled="true">
                                                        </asp:TextBox>
                                                        <span class="input-group-addon">
                                                            <span data-icon-element="" class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <asp:RequiredFieldValidator ID="Re_txt_date_injury"
                                                            runat="server"
                                                            ControlToValidate="txt_date_injury" Display="None"
                                                            SetFocusOnError="true"
                                                            ErrorMessage="*กรุณากรอก วัน/เดือน/ปี"
                                                            ValidationGroup="SaveHistoryInjuryList" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="Validator_txt_date_injury" runat="Server" PopupPosition="BottomLeft"
                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_date_injury" Width="180" />
                                                    </div>

                                                </div>

                                                <div class="col-md-6">

                                                    <label>ส่วนของร่างกายที่บาดเจ็บหรือการเจ็บป่วย</label>
                                                    <asp:TextBox ID="txt_injurydetail" runat="server" CssClass="form-control" Enabled="true">
                                                    </asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="Re_txt_injurydetail"
                                                        runat="server"
                                                        ControlToValidate="txt_injurydetail" Display="None"
                                                        SetFocusOnError="true"
                                                        ErrorMessage="*กรุณากรอกส่วนของร่างกายที่บาดเจ็บหรือการเจ็บป่วย"
                                                        ValidationGroup="SaveHistoryInjuryList" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="Validator_txt_injurydetail" runat="Server" PopupPosition="BottomLeft"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_injurydetail" Width="180" />
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label>สาเหตุของการบาดเจ็บหรือการเจ็บป่วย</label>
                                                    <asp:TextBox ID="txt_cause_Injury" runat="server" CssClass="form-control" Enabled="true">
                                                    </asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="Re_txt_cause_Injury"
                                                        runat="server"
                                                        ControlToValidate="txt_cause_Injury" Display="None"
                                                        SetFocusOnError="true"
                                                        ErrorMessage="*กรุณากรอกสาเหตุของการบาดเจ็บหรือการเจ็บป่วย"
                                                        ValidationGroup="SaveHistoryInjuryList" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="Validator_txt_cause_Injury" runat="Server" PopupPosition="BottomLeft"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_cause_Injury" Width="180" />
                                                </div>
                                            </div>

                                            <div class="form-group">

                                                <div class="col-md-12">
                                                    <label>ระดับความรุนแรง</label>
                                                </div>
                                                <asp:UpdatePanel ID="updateInsert_notwork" runat="server">
                                                    <ContentTemplate>
                                                        <div class="col-md-12">

                                                            <div id="div_Detailnot_work" class="panel panel-default" runat="server">
                                                                <div class="panel-body">
                                                                    <div class="form-group">

                                                                        <div class="col-md-6">
                                                                            <label>ทุพพลภาพ</label>
                                                                            <asp:TextBox ID="txt_Disability" runat="server" CssClass="form-control" Enabled="true">
                                                                            </asp:TextBox>


                                                                        </div>

                                                                        <div class="col-md-6">
                                                                            <label>สูญเสียอวัยวะบางส่วน</label>
                                                                            <asp:TextBox ID="txt_LostOrgan" runat="server" CssClass="form-control" Enabled="true">
                                                                            </asp:TextBox>

                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <div class="col-md-12">
                                                                            <label>ทำงานไม่ได้ชั่วคราว</label>
                                                                            <asp:RadioButtonList ID="rdo_notworking" runat="server">
                                                                                <asp:ListItem Value="1">&nbsp;หยุดงานเกิน 3 วัน</asp:ListItem>
                                                                                <asp:ListItem Value="2">&nbsp;หยุดงานไม่เกิน 3 วัน</asp:ListItem>

                                                                            </asp:RadioButtonList>
                                                                        </div>

                                                                    </div>


                                                                </div>
                                                            </div>

                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <asp:LinkButton ID="btnAddinjulyTolist" runat="server"
                                                        CssClass="btn btn-primary"
                                                        Text="เพิ่มบันทึกการบาดเจ็บ" OnCommand="btnCommand" CommandName="cmdAddHistoryInjuryList"
                                                        ValidationGroup="SaveHistoryInjuryList" />
                                                </div>
                                            </div>


                                        </div>


                                    </div>

                                </div>
                            </div>
                        </div>

                    </InsertItemTemplate>

                </asp:FormView>

                <asp:UpdatePanel ID="Update_GridHistoryInjuly" runat="server" Visible="false">
                    <ContentTemplate>
                        <div id="gvHistoryInjulyList_scroll" style="overflow-x: scroll; width: 100%" runat="server">
                            <asp:GridView ID="gvHistoryInjury"
                                RowStyle-Wrap="false"
                                runat="server"
                                BorderStyle="None"
                                CssClass="table table-striped table-bordered table-responsive"
                                OnRowCommand="onRowCommand"
                                AutoGenerateColumns="false">
                                <Columns>
                                    <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center"
                                        HeaderStyle-CssClass="text-center"
                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller">
                                        <ItemTemplate>
                                            <%# (Container.DataItemIndex + 1) %>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:BoundField DataField="drdate_injuryText" HeaderText="วัน/เดือน/ปี" ItemStyle-CssClass="text-center"
                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                    <asp:BoundField DataField="drinjurydetailText" HeaderText="ส่วนของร่างกายที่บาดเจ็บหรือการเจ็บป่วย" ItemStyle-CssClass="text-center"
                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                    <asp:BoundField DataField="drcause_InjuryText" HeaderText="สาเหตุของการบาดเจ็บหรือการเจ็บป่วย" ItemStyle-CssClass="text-center"
                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                    <asp:BoundField DataField="drDisabilityText" HeaderText="ทุพพลภาพ" ItemStyle-CssClass="text-center"
                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                    <asp:BoundField DataField="drLostOrganText" HeaderText="สูญเสียอวัยวะบางส่วน" ItemStyle-CssClass="text-center"
                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                    <asp:BoundField DataField="drnotworkingText" HeaderText="ทำงานไม่ได้ชั่วคราว" ItemStyle-CssClass="text-center"
                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                    <asp:TemplateField ItemStyle-HorizontalAlign="center"
                                        HeaderStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnRemoveHistoryInjuryList" runat="server"
                                                CssClass="btn btn-danger btn-xs"
                                                OnClientClick="return confirm('คุณต้องการลบรายการนี้ใชหรือไม่ ?')"
                                                CommandName="btnRemoveHistoryInjuryList"><i class="fa fa-times"></i>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <br />
                <asp:UpdatePanel ID="Update_SaveHistoryInjury" runat="server" Visible="false">
                    <ContentTemplate>
                        <div class="form-group">

                            <asp:LinkButton ID="btnInsertHistoryInjury" CssClass="btn btn-success" runat="server" OnCommand="btnCommand"
                                CommandName="cmdInsertHistoryInjury" Text="บันทึก" ValidationGroup="btnSave"
                                OnClientClick="return confirm('ยืนยันการบันทึก')" />
                            <asp:LinkButton CssClass="btn btn-danger" runat="server" OnCommand="btnCommand"
                                CommandName="btnCancel" Text="ยกเลิก" />
                        </div>

                    </ContentTemplate>
                </asp:UpdatePanel>
                <!--Form History Injury-->

                <!--Form History Health Check-->
                <asp:FormView ID="fvHealcheckForm" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <InsertItemTemplate>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <h3 class="text-center">
                                                    <asp:Label ID="lbFormName_HealthCheck" runat="server" Text="การตรวจสุขภาพ" CssClass="control-label" Font-Bold="true" Font-Size="14"></asp:Label>
                                                </h3>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="div_DetailHistoryHealthCheck" class="panel panel-default" runat="server">
                                        <div class="panel-body">

                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <asp:RadioButtonList ID="rdoDetailType" runat="server" CssClass="" Font-Bold="true" RepeatDirection="Vertical"
                                                        AutoPostBack="true">
                                                    </asp:RadioButtonList>

                                                    <asp:RequiredFieldValidator ID="Requiredrd_rdoDetailType" runat="server"
                                                        ControlToValidate="rdoDetailType" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณาเลือกประเภทการตรวจ" ValidationGroup="SaveHealthCheck" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" PopupPosition="BottomRight"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredrd_rdoDetailType" Width="200" />

                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-6">
                                                    <label>ครั้งที่</label>
                                                    <asp:TextBox ID="txt_numcheck" runat="server" CssClass="form-control" MaxLength="2" Enabled="true">
                                                    </asp:TextBox>

                                                    <asp:RequiredFieldValidator ID="Required_txt_numcheck" runat="server"
                                                        ControlToValidate="txt_numcheck" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณากรอกครั้งที่ตรวจ" ValidationGroup="SaveHealthCheck" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server" PopupPosition="BottomRight"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Required_txt_numcheck" Width="200" />

                                                    <asp:RegularExpressionValidator ID="Required_txt_numcheck1" runat="server"
                                                        ValidationGroup="SaveHealthCheck"
                                                        Display="None"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="txt_numcheck"
                                                        Font-Size="13px" ForeColor="Red"
                                                        ValidationExpression="^\d+?$"
                                                        ErrorMessage="*ครั้งที่ตรวจไม่ถูกต้อง" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="toolkitRegExTxtEmpVisitorIDCard11" runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                        TargetControlID="Required_txt_numcheck1" Width="200" PopupPosition="BottomRight" />

                                                </div>

                                                <div class="col-md-6">
                                                    <label>วันที่ตรวจสุขภาพ</label>
                                                    <div class="input-group date">
                                                        <asp:TextBox ID="txt_date_healthCheck" runat="server" CssClass="form-control from-date-datepicker" Enabled="true">
                                                        </asp:TextBox>
                                                        <span class="input-group-addon">
                                                            <span data-icon-element="" class="glyphicon glyphicon-calendar"></span>
                                                        </span>

                                                        <asp:RequiredFieldValidator ID="Required_txt_date_healthCheck" runat="server"
                                                            ControlToValidate="txt_date_healthCheck" Display="None" SetFocusOnError="true"
                                                            ErrorMessage="*กรุณาเลือกวันที่ตรวจสุขภาพ" ValidationGroup="SaveHealthCheck" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="Server" PopupPosition="BottomRight"
                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Required_txt_date_healthCheck" Width="200" />


                                                    </div>
                                                </div>

                                            </div>

                                            <asp:UpdatePanel ID="update_detailDoctorInsert" runat="server">
                                                <ContentTemplate>
                                                    <div id="div_DetailDoctorCheck" class="panel panel-default" runat="server">
                                                        <div class="panel-body">

                                                            <div class="form-group">
                                                                <div class="col-md-6">
                                                                    <label>ชื่อแพทย์ผู้ตรวจ</label>
                                                                    <asp:TextBox ID="txtEmpIDX_Doctor" runat="server" Visible="false" />
                                                                    <asp:DropDownList ID="ddl_Doctorname" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                                        AutoPostBack="true" />

                                                                    <asp:RequiredFieldValidator ID="Required_ddl_Doctorname" runat="server"
                                                                        ControlToValidate="ddl_Doctorname" Display="None" SetFocusOnError="true" InitialValue="0"
                                                                        ErrorMessage="*กรุณาเลือกวันที่ตรวจสุขภาพ" ValidationGroup="SaveHealthCheck" />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" PopupPosition="BottomRight"
                                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Required_ddl_Doctorname" Width="200" />
                                                                </div>

                                                                <div class="col-md-6">
                                                                    <label>เลขที่ใบประกอบวิชาชีพ</label>
                                                                    <%--<asp:TextBox ID="txtEmpNewFullNameTHHidden" runat="server" Visible="false" />--%>
                                                                    <asp:TextBox ID="txtcard_number_doctor" runat="server" CssClass="form-control"
                                                                        placeholder="เลขที่ใบประกอบวิชาชีพ..." Enabled="false" />
                                                                </div>

                                                            </div>

                                                            <div class="form-group">
                                                                <div class="col-md-6">
                                                                    <label>ชื่อหน่วยงานที่ตรวจสุขภาพ</label>
                                                                    <asp:TextBox ID="txt_health_authority_name_" runat="server" CssClass="form-control"
                                                                        placeholder="ชื่อหน่วยงานที่ตรวจสุขภาพ..." Enabled="false" />

                                                                </div>
                                                                <div class="col-md-2">
                                                                    <label>ตั้งอยู่เลขที่</label>
                                                                    <asp:TextBox ID="txt_location_name" runat="server" CssClass="form-control"
                                                                        placeholder="ตั้งอยู่เลขที่..." Enabled="false" />

                                                                </div>

                                                                <div class="col-md-2">
                                                                    <label>หมู่ที่</label>
                                                                    <asp:TextBox ID="txt_village_no" runat="server" CssClass="form-control"
                                                                        placeholder="หมู่ที่..." Enabled="false" />

                                                                </div>

                                                                <div class="col-md-2">
                                                                    <label>ถนน</label>
                                                                    <asp:TextBox ID="txt_road_name" runat="server" CssClass="form-control"
                                                                        placeholder="ถนน..." Enabled="false" />

                                                                </div>
                                                            </div>

                                                        </div>

                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>

                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label>ตรวจสุขภาพทั่วไป</label>
                                                </div>
                                                <asp:UpdatePanel ID="Update_GenaralHealth" runat="server">
                                                    <ContentTemplate>
                                                        <div class="col-md-12">
                                                            <div id="div_genaralHealthCheck" class="panel panel-default" runat="server">

                                                                <div class="panel-body">
                                                                    <div class="form-group">
                                                                        <div class="col-md-12">
                                                                            <label>ผลการตรวจเบื้องต้น</label>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">

                                                                        <div class="col-md-4">
                                                                            <label>น้ำหนัก</label>
                                                                            <asp:TextBox ID="txt_weight" runat="server" CssClass="form-control" MaxLength="3"
                                                                                placeHolder="น้ำหนัก..." Enabled="true">
                                                                            </asp:TextBox>

                                                                            <asp:RequiredFieldValidator ID="Required_txt_weight" runat="server"
                                                                                ControlToValidate="txt_weight" Display="None" SetFocusOnError="true"
                                                                                ErrorMessage="*กรุณากรอกน้ำหนัก" ValidationGroup="SaveHealthCheck" />
                                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" PopupPosition="BottomRight"
                                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Required_txt_weight" Width="200" />

                                                                            <asp:RegularExpressionValidator ID="Required_txt_weight1" runat="server"
                                                                                ValidationGroup="SaveHealthCheck"
                                                                                Display="None"
                                                                                SetFocusOnError="true"
                                                                                ControlToValidate="txt_weight"
                                                                                Font-Size="13px" ForeColor="Red"
                                                                                ValidationExpression="^\d+?$"
                                                                                ErrorMessage="*น้ำหนักไม่ถูกต้อง" />
                                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender21" runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                                                TargetControlID="Required_txt_weight1" Width="200" PopupPosition="BottomRight" />

                                                                        </div>
                                                                        <div class="col-md-2">
                                                                            <label>&nbsp;</label>
                                                                            <asp:TextBox ID="txt_unit_weight" runat="server" Text="กิโลกรัม"
                                                                                CssClass="form-control" Enabled="false">
                                                                            </asp:TextBox>

                                                                        </div>


                                                                        <div class="col-md-4">
                                                                            <label>ความสูง</label>
                                                                            <asp:TextBox ID="txt_height" runat="server" CssClass="form-control" MaxLength="3"
                                                                                placeHolder="ความสูง..." Enabled="true">
                                                                            </asp:TextBox>

                                                                            <asp:RequiredFieldValidator ID="Required_txt_height" runat="server"
                                                                                ControlToValidate="txt_height" Display="None" SetFocusOnError="true"
                                                                                ErrorMessage="*กรุณากรอกความสูง" ValidationGroup="SaveHealthCheck" />
                                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender13" runat="Server" PopupPosition="BottomRight"
                                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Required_txt_height" Width="200" />

                                                                            <asp:RegularExpressionValidator ID="Required_txt_height1" runat="server"
                                                                                ValidationGroup="SaveHealthCheck"
                                                                                Display="None"
                                                                                SetFocusOnError="true"
                                                                                ControlToValidate="txt_height"
                                                                                Font-Size="13px" ForeColor="Red"
                                                                                ValidationExpression="^\d+?$"
                                                                                ErrorMessage="*น้ำหนักไม่ถูกต้อง" />
                                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender22" runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                                                TargetControlID="Required_txt_height1" Width="200" PopupPosition="BottomRight" />

                                                                        </div>

                                                                        <div class="col-md-2">
                                                                            <label>&nbsp;</label>
                                                                            <asp:TextBox ID="txt_unit_height" runat="server" Text="เซนติเมตร"
                                                                                CssClass="form-control" Enabled="false">
                                                                            </asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">

                                                                        <div class="col-md-2">
                                                                            <label>ดัชนีมวลกาย</label>
                                                                            <asp:TextBox ID="txt_body_mass" runat="server" CssClass="form-control"
                                                                                placeHolder="ดัชนีมวลกาย..." Enabled="true">
                                                                            </asp:TextBox>

                                                                            <asp:RequiredFieldValidator ID="Required_txt_body_mass" runat="server"
                                                                                ControlToValidate="txt_body_mass" Display="None" SetFocusOnError="true"
                                                                                ErrorMessage="*กรุณากรอกดัชนีมวลกาย" ValidationGroup="SaveHealthCheck" />
                                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender14" runat="Server" PopupPosition="BottomRight"
                                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Required_txt_body_mass" Width="200" />

                                                                        </div>

                                                                        <div class="col-md-3">
                                                                            <label>ความดันโลหิต</label>
                                                                            <asp:TextBox ID="txt_bloodpressure" runat="server" CssClass="form-control"
                                                                                placeHolder="ความดันโลหิต..." Enabled="true">
                                                                            </asp:TextBox>

                                                                            <asp:RequiredFieldValidator ID="Required_txt_bloodpressure" runat="server"
                                                                                ControlToValidate="txt_bloodpressure" Display="None" SetFocusOnError="true"
                                                                                ErrorMessage="*กรุณากรอกความดันโลหิต" ValidationGroup="SaveHealthCheck" />
                                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender15" runat="Server" PopupPosition="BottomRight"
                                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Required_txt_bloodpressure" Width="200" />

                                                                        </div>

                                                                        <div class="col-md-2">
                                                                            <label>&nbsp;</label>
                                                                            <asp:TextBox ID="txt_bloodpressure_unit" runat="server" Text="mm.Hg"
                                                                                CssClass="form-control" Enabled="false">
                                                                            </asp:TextBox>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <label>ชีพจร</label>
                                                                            <asp:TextBox ID="txt_pulse" runat="server" CssClass="form-control"
                                                                                placeHolder="ชีพจร..." Enabled="true">
                                                                            </asp:TextBox>

                                                                            <asp:RequiredFieldValidator ID="Required_txt_pulse" runat="server"
                                                                                ControlToValidate="txt_pulse" Display="None" SetFocusOnError="true"
                                                                                ErrorMessage="*กรุณากรอกชีพจร" ValidationGroup="SaveHealthCheck" />
                                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender16" runat="Server" PopupPosition="BottomRight"
                                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Required_txt_pulse" Width="200" />

                                                                        </div>

                                                                        <div class="col-md-2">
                                                                            <label>&nbsp;</label>
                                                                            <asp:TextBox ID="txt_pulse_unit" runat="server" Text="ครั้ง/นาที"
                                                                                CssClass="form-control" Enabled="false">
                                                                            </asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <div class="col-md-12">
                                                                            <label>ผลการตรวจร่างกายตามระบบ</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="col-md-12">
                                                                            <label>ระบุ</label>
                                                                            <asp:TextBox ID="txt_specify_resultbody" runat="server" CssClass="form-control"
                                                                                placeHolder="ระบุผลการตรวจร่างกายตามระบบ..." Enabled="true">
                                                                            </asp:TextBox>
                                                                        </div>

                                                                    </div>

                                                                    <div class="form-group">
                                                                        <div class="col-md-12">
                                                                            <label>ผลการตรวจทางห้องปฏิบัติการ</label>
                                                                            <asp:TextBox ID="txt_resultlab" runat="server" CssClass="form-control"
                                                                                placeHolder="ระบุผลการตรวจทางห้องปฏิบัติการ..." Enabled="true">
                                                                            </asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                            </div>
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label>ตรวจสุขภาพตามปัจจัยเสี่ยงของงาน</label>
                                                </div>
                                                <asp:UpdatePanel ID="Update_RikeHealthCheck" runat="server">
                                                    <ContentTemplate>
                                                        <div class="col-md-12">
                                                            <div id="div_rikeHealthCheck" class="panel panel-default" runat="server">
                                                                <div class="panel-body">

                                                                    <div class="form-group">

                                                                        <div class="col-md-5">
                                                                            <label>ปัจจัยเสี่ยง</label>
                                                                            <asp:TextBox ID="txt_rikeHealthCheck" runat="server" CssClass="form-control"
                                                                                placeHolder="ปัจจัยเสี่ยง..." Enabled="true">
                                                                            </asp:TextBox>
                                                                        </div>

                                                                        <div class="col-md-5">
                                                                            <label>ผลการตรวจ</label>
                                                                            <asp:TextBox ID="txt_ResultHealthCheck" runat="server" CssClass="form-control"
                                                                                placeHolder="ผลการตรวจ..." Enabled="true">
                                                                            </asp:TextBox>
                                                                        </div>

                                                                        <div class="col-md-2">
                                                                            <label>&nbsp;</label>
                                                                            <div class="clearfix"></div>
                                                                            <asp:LinkButton ID="btnAddRikeTolist" runat="server"
                                                                                CssClass="btn btn-primary col-md-12"
                                                                                Text="เพิ่มข้อมูล" OnCommand="btnCommand" CommandName="cmdAddRikeTolist"
                                                                                ValidationGroup="addAddRikeTolist" />
                                                                        </div>

                                                                    </div>

                                                                    <asp:GridView ID="gvRikeList" runat="server"
                                                                        CssClass="table table-striped table-responsive"
                                                                        GridLines="None" OnRowCommand="onRowCommand"
                                                                        AutoGenerateColumns="false">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center"
                                                                                HeaderStyle-CssClass="text-center"
                                                                                HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller"
                                                                                HeaderStyle-Width="10%">
                                                                                <ItemTemplate>
                                                                                    <%# (Container.DataItemIndex + 1) %>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:BoundField DataField="drrikeHealthText" HeaderText="ปัจจัยเสี่ยง" ItemStyle-HorizontalAlign="center"
                                                                                HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller"
                                                                                HeaderStyle-CssClass="text-center" />

                                                                            <asp:BoundField DataField="drResultHealthText" HeaderText="ผลการตรวจ" ItemStyle-HorizontalAlign="center"
                                                                                HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller"
                                                                                HeaderStyle-CssClass="text-center" />

                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="center"
                                                                                HeaderStyle-CssClass="text-center">
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="btnRemoveRikeHealth" runat="server"
                                                                                        CssClass="btn btn-danger btn-xs"
                                                                                        OnClientClick="return confirm('คุณต้องการลบรายการนี้ใชหรือไม่ ?')"
                                                                                        CommandName="btnRemoveRikeHealth">
                                                               <i class="fa fa-times"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>

                                                                    <div class="clearfix"></div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>

                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <asp:LinkButton ID="btnInsertHealthCheck" CssClass="btn btn-success" runat="server" OnCommand="btnCommand"
                                                        CommandName="cmdInsertHealthCheck" Text="บันทึก" ValidationGroup="SaveHealthCheck"
                                                        OnClientClick="return confirm('ยืนยันการบันทึก')" />
                                                    <asp:LinkButton CssClass="btn btn-danger" runat="server" OnCommand="btnCommand"
                                                        CommandName="btnCancel" Text="ยกเลิก" />
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </InsertItemTemplate>
                </asp:FormView>
                <!--Form History Health Check-->
            </div>
        </asp:View>
        <!--View Create-->

        <!--View Report-->
        <asp:View ID="docReportList" runat="server">

            <div id="div_SearchReport" runat="server" class="col-md-12" visible="false">
                <asp:FormView ID="FvSearchReport" runat="server" DefaultMode="Insert" Width="100%">
                    <InsertItemTemplate>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">ค้นหาชื่อพนักงาน</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">รหัสพนักงาน</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpCode_HistorySearchreport" runat="server" CssClass="form-control" MaxLength="8" placeholder="รหัสพนักงาน" />
                                        </div>
                                        <label class="col-sm-2 control-label">ชื่อพนักงาน(TH/EN)</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpName_HistorySearchreport" runat="server" CssClass="form-control" placeholder="ชื่อพนักงาน(TH/EN)" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">องค์กร</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlOrgSearchReport" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" />
                                        </div>
                                        <label class="col-sm-2 control-label">ฝ่าย</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlDeptSearchReport" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                <asp:ListItem Text="--- ฝ่าย ---" Value="-1" />
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">แผนก</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlSecSearchReport" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                <asp:ListItem Text="--- แผนก ---" Value="-1" />
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-6"></div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2"></label>
                                        <div class="col-sm-4">
                                            <asp:LinkButton ID="lbSearchHistoryEmployee_Report" CssClass="btn btn-success" runat="server" data-original-title="Search" data-toggle="tooltip" Text="ค้นหา" OnCommand="btnCommand" CommandName="cmdSearchHistoryEmployeeReport"></asp:LinkButton>
                                            <asp:LinkButton ID="lbResetSearchHistoryEmployee_Report" CssClass="btn btn-info" runat="server" data-original-title="Reset" data-toggle="tooltip" Text="ล้างค่า" OnCommand="btnCommand" CommandName="cmdResetSearchReport"></asp:LinkButton>
                                        </div>
                                    </div>

                                </div>
                            </div>
                    </InsertItemTemplate>
                </asp:FormView>
            </div>

            <div id="div_GvPrintDetail" runat="server" class="col-md-12">
                <asp:GridView ID="GvPrintDetail"
                    runat="server"
                    AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered table-responsive"
                    HeaderStyle-CssClass="info"
                    AllowPaging="true"
                    PageSize="10"
                    OnPageIndexChanging="gvPageIndexChanging"
                    OnRowDataBound="gvRowDataBound">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                    <EmptyDataTemplate>
                        <div class="text-center">-- ไม่พบข้อมูล -- </div>
                    </EmptyDataTemplate>
                    <Columns>

                        <asp:TemplateField HeaderText="#" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" ItemStyle-CssClass="text-center" HeaderStyle-Width="5%">
                            <ItemTemplate>
                                <small>
                                    <%# (Container.DataItemIndex + 1) %>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="รหัสพนักงาน" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="lbl_emp_idx_detail" runat="server" Visible="false" Text='<%# Eval("emp_idx")%>' />
                                    <asp:Label ID="lbl_emp_code_detail" runat="server" Text='<%# Eval("emp_code")%>' />

                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ชื่อพนักงาน" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="lbl_emp_firstname_th_detail" runat="server" Text='<%# Eval("emp_firstname_th")%>' />
                                    <asp:Label ID="lbl_emp_lastname_th_detail" runat="server" Text='<%# Eval("emp_lastname_th")%>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="30%">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="Detail_Employee" runat="server"> 
                                    <p><b>ฝ่าย :</b> <%# Eval("dept_name_th") %></p>
                                    <p><b>แผนก :</b> <%# Eval("sec_name_th") %></p>
                                    <p><b>ตำแหน่ง :</b> <%# Eval("pos_name_th") %></p>
                                    </asp:Label>
                                </small>

                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="25%">
                            <ItemTemplate>
                                <small>
                                    <asp:LinkButton ID="btnViewPrint" CssClass="btn btn-info btn-sm" runat="server" CommandName="cmdViewPrint"
                                        OnCommand="btnCommand" CommandArgument='<%# Eval("emp_idx")%>'
                                        data-toggle="tooltip" title="ดูข้อมูล"><i class="fa fa-file"></i>
                                    </asp:LinkButton>

                                </small>

                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>
            </div>

            <div id="printableAreaDetail" runat="server" visible="false">
                <div id="div_DetailAllPrint" runat="server" class="col-md-12">

                    <div class="form-group">
                        <asp:LinkButton ID="btnbacksearch_report" CssClass="btn btn-danger" data-toggle="tooltip" title="" runat="server"
                            CommandName="cmdCancelToSearchReport" CommandArgument="1" OnCommand="btnCommand"> < ย้อนกลับ</asp:LinkButton>
                    </div>

                    <div class="form-group">

                        <asp:LinkButton ID="btnPrintAllReport" CssClass="btn btn-primary" runat="server" CommandName="cmdPrintAllReport" OnCommand="btnCommand"
                            Text="<i class='fa fa-print'></i> พิมพ์" OnClientClick="return printDiv('printableAreaDetail1');" />
                    </div>

                    <div id="printableAreaDetail1">
                        <div class="panel panel-default">
                            <div id="div_CoverDetail" runat="server" visible="true">
                                <h1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1>
                                <h3 style="text-align: center;">สมุดสุขภาพประจำตัวของลูกจ้าง</h3>

                                <h3 style="text-align: center;">ที่ทำงานเกี่ยวกับปัจจัยเสี่ยง</h3>

                                <h1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1>
                                <h1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1>

                                <h3 style="text-align: center;">ตามกฎกระทรวงกำหนดหลักเกณฑ์</h3>
                                <%--<br />--%>
                                <h3 style="text-align: center;">และวิธีการตรวจสุขภาพของลูกจ้าง</h3>
                                <%--<br />--%>
                                <h3 style="text-align: center;">และส่งผลการตรวจแก่พนักงานตรวจแรงงาน พ.ศ. ๒๕๔๗</h3>

                                <br />

                                <h1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1>
                                <h1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1>
                                <h1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1>



                                <asp:FormView ID="FvDetailCover" runat="server" DefaultMode="ReadOnly" Width="100%">
                                    <ItemTemplate>
                                        <table class="table f-s-12 m-t-10" style="border: 0px">
                                            <tr>
                                                <td style="text-align: center; border: 0px"><b>
                                                    <%-- <h2 style="padding-left: 15px; font-size: 20px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ชื่อ : &nbsp;&nbsp;&nbsp;&nbsp;<%# Eval("FirstNameTH") %></h2>--%>
                                                    <h3>ชื่อ : &nbsp;&nbsp;&nbsp; <%# Eval("FirstNameTH") %>
                                                         &nbsp;&nbsp;&nbsp;&nbsp; นามสกุล : &nbsp;&nbsp;&nbsp;<%# Eval("LastNameTH") %></h3>
                                                </b>

                                                </td>


                                            </tr>
                                        </table>

                                        <table class="table f-s-12 m-t-10" style="border: 0px">
                                            <tr>
                                                <td style="text-align: center; border: 0px"><b>
                                                    <h3>ชื่อสถานประกอบกิจการ :&nbsp;บริษัท เถ้าแก่น้อย ฟู๊ดแอนด์มาร์เก็ตติ้ง จำกัด(มหาชน)</h3>
                                                </b></td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </asp:FormView>

                            </div>
                        </div>
                        <div style="page-break-after: always;"></div>

                        <div class="panel panel-default">
                            <h1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1>
                            <h1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1>
                            <table class="table">
                                <tr>
                                    <td style="text-align: center; border: 0px">
                                        <h3 style="text-align: center;">รัฐมนตรีว่าการกระทรวงแรงงานออกกฏกระทรวงกำหนดหลักเกณฑ์และวิธีการตรวจสุขภาพ</h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center; border: 0px">
                                        <h3 style="text-align: left;">&nbsp;&nbsp;ของลูกจ้างและส่งผลการตรวจแก่พนักงานตรวจแรงงาน พ.ศ. ๒๕๔๗ โดยกำหนดให้นายจ้างจัดให้มีสมุดสุขภาพ</h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center; border: 0px">
                                        <h3 style="text-align: left;">&nbsp;&nbsp;ประจำตัวของลูกจ้างที่ทำงานกับปัจจัยเสี่ยงตามแบบที่อธิบดีประกาศกำหนด และให้นายจ้างบันทึกผลการตรวจสุขภาพ</h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center; border: 0px">
                                        <h3 style="text-align: left;">&nbsp;&nbsp;ของลูกจ้างในสมุดสุขภาพประจำตัวของลูกจ้างตามผลการตรวจของแพทย์ทุกครั้งที่มีการตรวจสุขภาพงานเกี่ยวกับปัจจัยเสี่ยง</h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center; border: 0px">
                                        <h3 style="text-align: left;">&nbsp;&nbsp;หมายความว่า งานที่ลูกจ้างทำเกี่ยวกับ</h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center; border: 0px">
                                        <h3 style="text-align: left;">&nbsp;&nbsp;&nbsp;&nbsp;๑. สารเคมีอันตรายตามที่รัฐมนตรีประกาศกำหนด</h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center; border: 0px">
                                        <h3 style="text-align: left;">&nbsp;&nbsp;&nbsp;&nbsp;๒. จุลชีวันเป็นพิษซึ่งอาจเป็นเชื้อไวรัส แบคทีเรีย รา หรือสารชีวภาพอื่นตามที่รัฐมนตรีประกาศกำหนด</h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center; border: 0px">
                                        <h3 style="text-align: left;">&nbsp;&nbsp;&nbsp;&nbsp;๓. กัมมันตภาพรังสี</h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center; border: 0px">
                                        <h3 style="text-align: left;">&nbsp;&nbsp;&nbsp;&nbsp;๔. ความร้อน ความเย็น ความสั่นสะเทือน ความกดดัน บรรยากาศ แสง เสียง หรือสภาพแวดล้อมอื่นที่อาจเป็นอันตราย</h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center; border: 0px">
                                        <h3 style="text-align: left;">&nbsp;&nbsp;&nbsp;&nbsp;ทั้งนี้ ตามที่รัฐมนตรีประกาศกำหนด</h3>
                                    </td>
                                </tr>
                            </table>

                            <h1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1>
                            <h1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1>
                        </div>


                        <div style="page-break-after: always;"></div>

                        <asp:FormView ID="fvPrintHistoryEmployee" runat="server" DefaultMode="ReadOnly" Width="100%">
                            <ItemTemplate>
                                <div class="panel panel-default">

                                    <h1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1>
                                    <h2 style="text-align: center;">ประวัติส่วนตัว</h2>
                                    <h1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1>
                                    <table class="table f-s-12 m-t-10" style="border: 0px">
                                        <tr>
                                            <td style="text-align: left; border: 0px"><b>
                                                <h3 style="padding-left: 10px;">ชื่อ-นามสกุล : </h3>
                                            </b></td>
                                            <td style="text-align: left; border: 0px">
                                                <h3><%# Eval("FirstNameTH") %>&nbsp;<%# Eval("LastNameTH") %></h3>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="text-align: left; border: 0px"><b>
                                                <h3 style="padding-left: 10px;">วัน เดือน ปี เกิด :</h3>
                                            </b></td>
                                            <td style="text-align: left; border: 0px">
                                                <h3><%# Eval("Birthday_Ex") %></h3>
                                            </td>

                                            <td style="text-align: left; border: 0px"><b>
                                                <h3>เพศ :</h3>
                                            </b></td>
                                            <td style="text-align: left; border: 0px">
                                                <h3>
                                                    <asp:Label ID="lbl_SexNameTH_hr" runat="server" Text='<%# Eval("SexNameTH") %>' Visible="false" />
                                                    <asp:CheckBoxList ID="check_SexNameTH" Font-Size="Large" runat="server" RepeatLayout="Table"
                                                        RepeatColumns="2" RepeatDirection="Vertical"
                                                        Enabled="false" OnSelectedIndexChanged="onSelectedIndexChanged" AutoPostBack="true">
                                                        <asp:ListItem Value="ชาย">&nbsp;ชาย&nbsp;</asp:ListItem>
                                                        <asp:ListItem Value="หญิง">&nbsp;หญิง</asp:ListItem>

                                                    </asp:CheckBoxList>
                                                </h3>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="text-align: left; border: 0px"><b>
                                                <h3 style="padding-left: 10px;">วันที่เข้างาน :</h3>
                                            </b></td>
                                            <td style="text-align: left; border: 0px">
                                                <h3><%# Eval("EmpIN_Ex") %></h3>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="text-align: left; border: 0px"><b>
                                                <h3 style="padding-left: 10px;">๑. เลขที่บัตรประชาชน :</h3>
                                            </b></td>
                                            <td style="text-align: left; border: 0px">
                                                <h3><%# Eval("IdentityCard") %></h3>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="text-align: left; border: 0px"><b>
                                                <h3 style="padding-left: 10px;">๒. ที่อยู่ตามบัตรประชาชน : </h3>
                                            </b></td>
                                            <%--  <td style="text-align: left; border: 0px">
                                                <h3 style="font-size:20px"><%# Eval("EmpAddr") %></h3>
                                            </td>

                                            <td style="text-align: left; border: 0px"><b>
                                                <h3 style="font-size:20px">ซอย : </h3>
                                            </b></td>
                                            <td style="text-align: left; border: 0px">
                                               
                                            </td>--%>
                                        </tr>

                                        <tr>
                                            <%--<td style="text-align: left; border: 0px"><b>
                                                <h3 style="padding-left: 5px;font-size:20px">๒. ที่อยู่ตามบัตรประชาชน เลขที่ : </h3>
                                            </b></td>
                                            <td style="text-align: left; border: 0px">
                                                <h3 style="font-size:20px"><%# Eval("EmpAddr") %></h3>
                                            </td>--%>

                                            <td style="text-align: left; border: 0px"><b>
                                                <h3 style="padding-left: 25px;">เลขที่ : </h3>
                                            </b></td>
                                            <td style="text-align: left; border: 0px">
                                                <h3><%# Eval("EmpAddr") %></h3>
                                            </td>

                                            <td style="text-align: left; border: 0px"><b>
                                                <h3>ซอย : </h3>
                                            </b></td>
                                            <td style="text-align: left; border: 0px"></td>
                                        </tr>

                                        <tr>
                                            <td style="text-align: left; border: 0px"><b>
                                                <h3 style="padding-left: 25px;">ถนน : </h3>
                                            </b></td>
                                            <td style="text-align: left; border: 0px"></td>

                                            <td style="text-align: left; border: 0px"><b>
                                                <h3>ตำบล(แขวง) :</h3>
                                            </b></td>
                                            <td style="text-align: left; border: 0px">
                                                <h3><%# Eval("DistName") %></h3>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="text-align: left; border: 0px"><b>
                                                <h3 style="padding-left: 25px;">อำเภอ(เขต)  : </h3>
                                            </b></td>
                                            <td style="text-align: left; border: 0px">
                                                <h3><%# Eval("AmpName") %></h3>
                                            </td>

                                            <td style="text-align: left; border: 0px"><b>
                                                <h3>จังหวัด :</h3>
                                            </b></td>
                                            <td style="text-align: left; border: 0px">
                                                <h3><%# Eval("ProvName") %></h3>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="text-align: left; border: 0px"><b>
                                                <h3 style="padding-left: 25px;">รหัสไปรษณีย์  : </h3>
                                            </b></td>
                                            <td style="text-align: left; border: 0px">
                                                <h3><%# Eval("PostCode") %></h3>
                                            </td>

                                            <td style="text-align: left; border: 0px"><b>
                                                <h3>โทรศัพท์ :</h3>
                                            </b></td>
                                            <td style="text-align: left; border: 0px">
                                                <h3><%# Eval("MobileNo") %></h3>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="text-align: left; border: 0px"><b>
                                                <h3 style="padding-left: 10px;">๓. ที่อยู่ที่สามารถติดต่อได้ : </h3>
                                            </b></td>

                                        </tr>

                                        <tr>


                                            <td style="text-align: left; border: 0px"><b>
                                                <h3 style="padding-left: 25px;">เลขที่ : </h3>
                                            </b></td>
                                            <td style="text-align: left; border: 0px">
                                                <h3><%# Eval("EmpAddr") %></h3>
                                            </td>

                                            <td style="text-align: left; border: 0px"><b>
                                                <h3>ซอย : </h3>
                                            </b></td>
                                            <td style="text-align: left; border: 0px"></td>
                                        </tr>

                                        <tr>
                                            <td style="text-align: left; border: 0px"><b>
                                                <h3 style="padding-left: 25px;">ถนน : </h3>
                                            </b></td>
                                            <td style="text-align: left; border: 0px"></td>

                                            <td style="text-align: left; border: 0px"><b>
                                                <h3>ตำบล(แขวง) :</h3>
                                            </b></td>
                                            <td style="text-align: left; border: 0px">
                                                <h3><%# Eval("DistName") %></h3>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="text-align: left; border: 0px"><b>
                                                <h3 style="padding-left: 25px;">อำเภอ(เขต)  : </h3>
                                            </b></td>
                                            <td style="text-align: left; border: 0px">
                                                <h3><%# Eval("AmpName") %></h3>
                                            </td>

                                            <td style="text-align: left; border: 0px"><b>
                                                <h3>จังหวัด :</h3>
                                            </b></td>
                                            <td style="text-align: left; border: 0px">
                                                <h3><%# Eval("ProvName") %></h3>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="text-align: left; border: 0px"><b>
                                                <h3 style="padding-left: 25px;">รหัสไปรษณีย์  : </h3>
                                            </b></td>
                                            <td style="text-align: left; border: 0px">
                                                <h3><%# Eval("PostCode") %></h3>
                                            </td>

                                            <td style="text-align: left; border: 0px"><b>
                                                <h3>โทรศัพท์ :</h3>
                                            </b></td>
                                            <td style="text-align: left; border: 0px">
                                                <h3><%# Eval("MobileNo") %></h3>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="text-align: left; border: 0px"><b>
                                                <h3 style="padding-left: 10px;">๔. สถานประกอบกิจการ : </h3>
                                            </b></td>
                                            <td style="text-align: left; border: 0px">
                                                <h3><%# Eval("OrgAddress") %></h3>
                                            </td>

                                            <td style="text-align: left; border: 0px"><b>
                                                <h3>เลขที่ : </h3>
                                            </b></td>
                                            <td style="text-align: left; border: 0px"></td>
                                        </tr>

                                        <tr>
                                            <td style="text-align: left; border: 0px"><b>
                                                <h3 style="padding-left: 25px;">หมู่ : </h3>
                                            </b></td>
                                            <td style="text-align: left; border: 0px"></td>

                                            <td style="text-align: left; border: 0px"><b>
                                                <h3>ถนน :</h3>
                                            </b></td>
                                            <td style="text-align: left; border: 0px"></td>
                                        </tr>

                                        <tr>
                                            <td style="text-align: left; border: 0px"><b>
                                                <h3 style="padding-left: 25px;">ตำบล(แขวง) : </h3>
                                            </b></td>
                                            <td style="text-align: left; border: 0px"></td>

                                            <td style="text-align: left; border: 0px"><b>
                                                <h3>อำเภอ(เขต) :</h3>
                                            </b></td>
                                            <td style="text-align: left; border: 0px"></td>
                                        </tr>

                                        <tr>
                                            <td style="text-align: left; border: 0px"><b>
                                                <h3 style="padding-left: 25px;">จังหวัด : </h3>
                                            </b></td>
                                            <td style="text-align: left; border: 0px"></td>

                                            <td style="text-align: left; border: 0px"><b>
                                                <h3>รหัสไปรษณีย์ :</h3>
                                            </b></td>
                                            <td style="text-align: left; border: 0px"></td>
                                        </tr>

                                        <tr>
                                            <td style="text-align: left; border: 0px"><b>
                                                <h3 style="padding-left: 25px;">โทรศัพท์ :</h3>
                                            </b></td>
                                            <td style="text-align: left; border: 0px"></td>
                                        </tr>

                                    </table>

                                </div>
                            </ItemTemplate>
                        </asp:FormView>

                        <div style="page-break-after: always;"></div>
                        <div id="div_HisToryWorkPrint" runat="server" visible="true">

                            <div class="panel panel-default">
                                <h1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1>

                                <h3 style="text-align: center;">ประวัติการทำงาน</h3>
                                <br />
                                <asp:GridView ID="gvWorkPrintReport"
                                    runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="table table-striped table-bordered table-responsive"
                                    HeaderStyle-CssClass="info"
                                    AllowPaging="true"
                                    OnPageIndexChanging="gvPageIndexChanging"
                                    OnRowDataBound="gvRowDataBound">
                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                    <EmptyDataTemplate>
                                        <div class="text-center">-- ไม่พบข้อมูล --</div>
                                    </EmptyDataTemplate>
                                    <Columns>

                                        <asp:TemplateField HeaderText="ชื่อสถานประกอบกิจการ/แผนก" ItemStyle-HorizontalAlign="Left"
                                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="10" HeaderStyle-Width="20%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="Detail_company_name" runat="server" Font-Size="10"> 
                                                    <p><b>ชื่อสถานประกอบกิจการ :</b> <%# Eval("company_name") %></p>
                                                    <p><b>แผนก :</b> <%# Eval("department_name") %></p>
                                                    </asp:Label>

                                                </small>
                                            </ItemTemplate>
                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ประเภทกิจการ" ItemStyle-HorizontalAlign="Left"
                                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="10" HeaderStyle-Width="15%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lbl_business_type_history" Font-Size="10" runat="server" Text='<%# Eval("business_type") %>' />
                                                </small>

                                            </ItemTemplate>
                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ลักษณะงานที่ทำ" ItemStyle-HorizontalAlign="Left"
                                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="10" HeaderStyle-Width="15%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lbl_job_description_history" Font-Size="10" runat="server" Text='<%# Eval("job_description") %>' />
                                                </small>

                                            </ItemTemplate>
                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ระยะเวลาที่ทำ" ItemStyle-HorizontalAlign="Left"
                                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="10" HeaderStyle-Width="20%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="Detail_DateWork" Font-Size="10" runat="server"> 
                                                <p><b>วัน/เดือน/ปี ที่เริ่ม :</b> <%# Eval("job_startdate") %></p>
                                                <p><b>วัน/เดือน/ปี ที่สิ้นสุด :</b> <%# Eval("job_enddate") %></p>
                                                    </asp:Label>
                                                </small>

                                            </ItemTemplate>
                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ปัจจัยที่เสี่ยงต่อสุขภาพ" ItemStyle-HorizontalAlign="Left"
                                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="10" HeaderStyle-Width="10%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lbl_risk_health_history" Font-Size="10" runat="server" Text='<%#Eval("risk_health") %>' />
                                                </small>

                                            </ItemTemplate>
                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="มี/ใช้อุปกรณ์ป้องกันอันตราย" ItemStyle-HorizontalAlign="Left"
                                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="10" HeaderStyle-Width="10%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lbl_protection_equipment_history" Font-Size="10" runat="server" Text='<%#Eval("protection_equipment") %>' />
                                                </small>

                                            </ItemTemplate>
                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>

                        <div style="page-break-after: always;"></div>
                        <asp:FormView ID="fvSickPrinReport" runat="server" DefaultMode="ReadOnly" OnDataBound="FvDetail_DataBound" Width="100%">
                            <ItemTemplate>
                                <div class="panel panel-default">
                                    <h1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1>
                                    <table class="table f-s-12 m-t-10" style="border: 0px; width: 100%">
                                        <h2 style="text-align: center;">ประวัติการเจ็บป่วย</h2>
                                        <br />
                                        <tr>
                                            <td style="text-align: left; border: 0px" colspan="2"><b>
                                                <h3 style="padding-left: 50px;">๑. เคยป่วยเป็นโรคหรือมีการบาดเจ็บ</h3>
                                            </b></td>
                                            <td></td>

                                        </tr>

                                        <tr>
                                            <td style="text-align: left; border: 0px">
                                                <asp:UpdatePanel ID="update_eversick_hr_print" runat="server" style="padding-left: 50px;">
                                                    <ContentTemplate>
                                                        <div class="col-md-12">
                                                            <div id="div_DetailEverSickCheck_hr_print" class="panel panel-default" runat="server">
                                                                <div class="panel-body">
                                                                    <table class="table table-striped f-s-12 table-empshift-responsive">
                                                                        <asp:Repeater ID="rpt_eversick_print" runat="server">
                                                                            <HeaderTemplate>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <tr>
                                                                                    <h2>
                                                                                        <td data-th="รายละเอียด" style="padding-left: 50px;"><%# Eval("eversick") %></td>
                                                                                        <td data-th="เมื่อปี พ.ศ.">เมื่อปี พ.ศ. <%# Eval("sick_year") %></td>
                                                                                    </h2>
                                                                                </tr>
                                                                            </ItemTemplate>
                                                                        </asp:Repeater>
                                                                    </table>
                                                                    <div class="clearfix"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="text-align: left; border: 0px" colspan="2"><b>
                                                <h3 style="padding-left: 50px;">๒. มีโรคประจำตัวหรือโรคเรื้อรังหรือไม่</h3>
                                            </b></td>
                                            <td style="border: 0px"></td>
                                        </tr>

                                        <tr>
                                            <td style="text-align: left; border: 0px; width: 50%">
                                                <h3 style="padding-left: 50px;">
                                                    <asp:Label ID="id_havedisease_idx" runat="server" Text='<%# Eval("havedisease") %>' Visible="false"></asp:Label>

                                                    <asp:CheckBoxList ID="rdo_havedisease" runat="server" Font-Size="10" RepeatLayout="Table"
                                                        RepeatColumns="0" RepeatDirection="Vertical"
                                                        Enabled="false" OnSelectedIndexChanged="onSelectedIndexChanged" AutoPostBack="true">
                                                        <asp:ListItem Value="ไม่มี">&nbsp;ไม่มี&nbsp;</asp:ListItem>
                                                        <asp:ListItem Value="มี">&nbsp;มี</asp:ListItem>
                                                        <%-- <asp:ListItem Value="3">&nbsp;เคยแต่เลิกแล้ว ระยะที่เคยสูบนาน</asp:ListItem>--%>
                                                    </asp:CheckBoxList>
                                                </h3>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td style="text-align: left; border: 0px" colspan="2">
                                                <asp:UpdatePanel ID="Panel_havedisease" runat="server" Visible="false">
                                                    <ContentTemplate>

                                                        <span style="padding-left: 50px">ระบุ : &nbsp;<%# Eval("havedisease") %></span>

                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td style="border: 0px"></td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: left; border: 0px" colspan="2"><b>
                                                <h3 style="padding-left: 50px;">๓. เคยได้รับการผ่าตัดหรือไม่</h3>
                                            </b></td>
                                            <td style="border: 0px"></td>
                                        </tr>

                                        <tr>
                                            <td style="text-align: left; border: 0px;">
                                                <h3 style="padding-left: 50px;">
                                                    <asp:Label ID="lbl_surgery_print" runat="server" Text='<%# Eval("surgery") %>' Visible="false"></asp:Label>
                                                    <asp:CheckBoxList ID="rdo_surgery" runat="server" Font-Size="10" RepeatLayout="Table"
                                                        RepeatColumns="0" RepeatDirection="Vertical"
                                                        Enabled="false" OnSelectedIndexChanged="onSelectedIndexChanged" AutoPostBack="true">
                                                        <asp:ListItem Value="ไม่เคย">&nbsp;ไม่เคย&nbsp;</asp:ListItem>
                                                        <asp:ListItem Value="เคย">&nbsp;เคย</asp:ListItem>
                                                        <%-- <asp:ListItem Value="3">&nbsp;เคยแต่เลิกแล้ว ระยะที่เคยสูบนาน</asp:ListItem>--%>
                                                    </asp:CheckBoxList>
                                                </h3>


                                            </td>


                                        </tr>
                                        <tr>
                                            <td style="text-align: left; border: 0px" colspan="2">
                                                <asp:UpdatePanel ID="Panel_surgery" runat="server" Visible="false">
                                                    <ContentTemplate>

                                                        <span style="padding-left: 50px;">ระบุ : &nbsp;<%# Eval("surgery") %></span>
                                                        <%--<label>ปี</label>--%>
                                                        <%--<asp:TextBox ID="txt_year_smoking_hr" runat="server" CssClass="form-control" Text='<%# Eval("smoking_value1") %>' placeholder="ปี...." Enabled="false" />--%>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td style="border: 0px"></td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: left; border: 0px" colspan="2">

                                                <b>
                                                    <h3 style="padding-left: 50px;">๔. เคยได้รับภูมิคุ้มกันโรคกรณีเกิดโรคระบาด หรือเพื่อป้องกันโรคติดต่อหรือไม่</h3>
                                                </b>

                                            </td>
                                            <td style="border: 0px"></td>

                                        </tr>

                                        <tr>
                                            <td style="text-align: left; border: 0px">
                                                <h3 style="padding-left: 50px;">
                                                    <asp:Label ID="lbl_immune_print" runat="server" Text='<%# Eval("immune") %>' Visible="false"></asp:Label>
                                                    <asp:CheckBoxList ID="rdo_immune" runat="server" Font-Size="10" RepeatLayout="Table"
                                                        RepeatColumns="0" RepeatDirection="Vertical"
                                                        Enabled="false" OnSelectedIndexChanged="onSelectedIndexChanged" AutoPostBack="true">
                                                        <asp:ListItem Value="ไม่เคย">&nbsp;ไม่เคย&nbsp;</asp:ListItem>
                                                        <asp:ListItem Value="เคย">&nbsp;เคย</asp:ListItem>
                                                        <%-- <asp:ListItem Value="3">&nbsp;เคยแต่เลิกแล้ว ระยะที่เคยสูบนาน</asp:ListItem>--%>
                                                    </asp:CheckBoxList>
                                                </h3>


                                            </td>

                                        </tr>

                                        <tr>
                                            <td style="text-align: left; border: 0px" colspan="2">
                                                <asp:UpdatePanel ID="Panel_immune" runat="server" Visible="false">
                                                    <ContentTemplate>

                                                        <span style="padding-left: 50px;">ระบุ : &nbsp;<%# Eval("immune") %></span>

                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td style="border: 0px"></td>
                                        </tr>

                                        <tr>
                                            <td style="text-align: left; border: 0px" colspan="2"><b>
                                                <h3 style="padding-left: 50px;">๕ . ประวัติการเจ็บป่วยของสมาชิกในครอบครัว (เช่น มะเร็ง โลหิตจาง วัณโรค เบาหวาน หอบหืด ภูมิแพ้ เป็นต้น)</h3>
                                            </b></td>
                                            <td style="border: 0px"></td>

                                        </tr>

                                        <tr>
                                            <td style="text-align: left; border: 0px">
                                                <h3 style="padding-left: 50px;">
                                                    <asp:CheckBoxList ID="rdo_disease_family" runat="server" Font-Size="10" RepeatLayout="Table"
                                                        RepeatColumns="0" RepeatDirection="Vertical"
                                                        Enabled="false" OnSelectedIndexChanged="onSelectedIndexChanged" AutoPostBack="true">
                                                        <asp:ListItem Value="1">&nbsp;ไม่มี&nbsp;</asp:ListItem>
                                                        <asp:ListItem Value="0">&nbsp;มี</asp:ListItem>

                                                    </asp:CheckBoxList>
                                                </h3>
                                            </td>
                                            <td style="text-align: left; border: 0px"><b></b></td>
                                        </tr>

                                        <tr>
                                            <td style="text-align: left; border: 0px;">
                                                <asp:UpdatePanel ID="Update_Relationfamily_hr_print" Visible="false" style="padding-left: 50px;" runat="server">
                                                    <ContentTemplate>
                                                        <div class="col-md-12">
                                                            <label id="lbl_relation_" runat="server" style="text-align: left;">ระบุความสัมพันธ์และโรค</label>
                                                            <div id="div_Identifyrelationship_hr_print" class="panel panel-default" runat="server">
                                                                <div class="panel-body">
                                                                    <%--<div class="form-group">--%>
                                                                    <%-- <div class="col-md-12">
                                                                        <label id="lbl_relation_" runat="server" style="padding: 50px">
                                                                            ระบุความสัมพันธ์และโรค</label>
                                                                    </div>--%>
                                                                    <%--</div>--%>
                                                                    <table class="table table-striped f-s-12 table-empshift-responsive">
                                                                        <asp:Repeater ID="rptRelationfamily_print" runat="server">
                                                                            <HeaderTemplate>
                                                                                <%--<tr>
                                                                                    <th>ความสัมพันธ์</th>
                                                                                    <th>โรค</th>
                                                                                </tr>--%>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <tr>
                                                                                    <h2>
                                                                                        <td data-th="ความสัมพันธ์" style="padding-left: 50px;">ความสัมพันธ์ &nbsp;<%# Eval("relation_family") %></td>
                                                                                        <td data-th="โรค">โรค &nbsp;<%# Eval("disease_family") %></td>
                                                                                    </h2>

                                                                                </tr>
                                                                            </ItemTemplate>
                                                                        </asp:Repeater>
                                                                    </table>

                                                                    <div class="clearfix"></div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="text-align: left; border: 0px" colspan="2"><b>
                                                <h3 style="padding-left: 50px;">๖ . ปัจจุบันมียาที่จำเป็นต้องรับประทานเป็นประจำบ้างหรือไม่</h3>
                                            </b></td>
                                            <td style="border: 0px"></td>
                                        </tr>

                                        <tr>
                                            <td style="text-align: left; border: 0px">
                                                <h3 style="padding-left: 50px;">
                                                    <asp:Label ID="lbl_drugs_eat" runat="server" Text='<%# Eval("drugs_eat") %>' Visible="false"></asp:Label>
                                                    <asp:CheckBoxList ID="rdo_drugs_eat" runat="server" Font-Size="10" RepeatLayout="Table"
                                                        RepeatColumns="0" RepeatDirection="Vertical"
                                                        Enabled="false" OnSelectedIndexChanged="onSelectedIndexChanged" AutoPostBack="true">
                                                        <asp:ListItem Value="ไม่มี">&nbsp;ไม่มี&nbsp;</asp:ListItem>
                                                        <asp:ListItem Value="มี">&nbsp;มี</asp:ListItem>
                                                        <%-- <asp:ListItem Value="3">&nbsp;เคยแต่เลิกแล้ว ระยะที่เคยสูบนาน</asp:ListItem>--%>
                                                    </asp:CheckBoxList>
                                                </h3>

                                            </td>

                                        </tr>
                                        <tr>
                                            <td style="text-align: left; border: 0px" colspan="2">
                                                <%--<td style="text-align: left; border: 0px; text-wrap: inherit; padding-left: 50px" colspan="2">--%>
                                                <asp:UpdatePanel ID="Panel_drugs_eat" runat="server" Visible="false">
                                                    <ContentTemplate>
                                                        <h4><span style="padding: 50px; margin-top: 0px">ระบุ : &nbsp;<%# Eval("drugs_eat") %></span></h4>



                                                    </ContentTemplate>
                                                </asp:UpdatePanel>

                                            </td>
                                            <td style="border: 0px"></td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: left; border: 0px" colspan="2"><b>
                                                <h3 style="padding-left: 50px;">๗. มีประวัติการแพ้ยาหรือไม่</h3>
                                            </b></td>
                                            <td style="border: 0px"></td>
                                        </tr>

                                        <tr>
                                            <td style="text-align: left; border: 0px">
                                                <h3 style="padding-left: 50px;">
                                                    <asp:Label ID="lbl_allergy_history" runat="server" Text='<%# Eval("allergy_history") %>' Visible="false"></asp:Label>
                                                    <asp:CheckBoxList ID="rdo_allergy_history" runat="server" Font-Size="10" RepeatLayout="Table"
                                                        RepeatColumns="0" RepeatDirection="Vertical"
                                                        Enabled="false" OnSelectedIndexChanged="onSelectedIndexChanged" AutoPostBack="true">
                                                        <asp:ListItem Value="ไม่มี">&nbsp;ไม่มี&nbsp;</asp:ListItem>
                                                        <asp:ListItem Value="มี">&nbsp;มี</asp:ListItem>

                                                    </asp:CheckBoxList>
                                                </h3>

                                            </td>

                                        </tr>

                                        <tr>
                                            <td style="text-align: left; border: 0px" colspan="2">
                                                <asp:UpdatePanel ID="Panel_allergy_history" runat="server" Visible="false">
                                                    <ContentTemplate>
                                                        <h4>
                                                            <span style="padding-left: 50px">ระบุ : &nbsp;<%# Eval("allergy_history") %></span>
                                                        </h4>

                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td style="border: 0px"></td>

                                        </tr>

                                        <tr>
                                            <td style="text-align: left; border: 0px" colspan="2"><b>
                                                <h3 style="padding-left: 50px;">๘. เคยสูบบุหรี่บ้างหรือไม่</h3>
                                            </b></td>
                                            <td style="border: 0px"></td>
                                        </tr>

                                        <tr>
                                            <td style="text-align: left; border: 0px"><b>
                                                <h3 style="padding-left: 50px;">
                                                    <asp:Label ID="lbl_smoking_print" runat="server" Text='<%# Eval("smoking") %>' Visible="false"></asp:Label>
                                                    <asp:CheckBoxList ID="rdoSmoking_hr_print" runat="server" Font-Size="10" Enabled="false" OnSelectedIndexChanged="onSelectedIndexChanged" AutoPostBack="true">
                                                        <asp:ListItem Value="1">&nbsp;ไม่เคย</asp:ListItem>
                                                        <asp:ListItem Value="2">&nbsp;เคยและปัจจุบันยังสูบอยู่ปริมาณ</asp:ListItem>
                                                        <asp:ListItem Value="3">&nbsp;เคยแต่เลิกแล้ว ระยะที่เคยสูบนาน</asp:ListItem>
                                                    </asp:CheckBoxList>
                                                </h3>
                                            </b>

                                            </td>


                                        </tr>

                                        <tr>
                                            <td style="text-align: left; border: 0px" colspan="2">
                                                <asp:UpdatePanel ID="update_stopsmoking_hr_print" runat="server" Visible="false">
                                                    <ContentTemplate>

                                                        <h4 style="padding-left: 50px;">
                                                            <span><%# Eval("smoking_value1") %> ปี &nbsp;&nbsp;<%# Eval("smoking_value2") %> เดือน &nbsp;&nbsp;<%# Eval("smoking_value3") %> มวน/วัน

                                                            </span>

                                                        </h4>

                                                    </ContentTemplate>
                                                </asp:UpdatePanel>

                                                <asp:UpdatePanel ID="update_eversmoking_hr_print" runat="server" Visible="false">
                                                    <ContentTemplate>
                                                        <h4>
                                                            <span style="padding-left: 50px"><%# Eval("smoking_value4") %> (มวน/วัน)</span>
                                                        </h4>

                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td style="border: 0px"></td>
                                        </tr>

                                        <tr>
                                            <td style="text-align: left; border: 0px" colspan="2"><b>
                                                <h3 style="padding-left: 50px;">๙. เคยดื่มสุรา เบียร์ หรือเครื่องดื่มที่มีแอลกอฮอล์บ้างหรือไม่</h3>
                                            </b></td>
                                            <td style="border: 0px"></td>
                                        </tr>

                                        <tr>
                                            <td style="text-align: left; border: 0px"><b>
                                                <h3 style="padding-left: 50px;">
                                                    <asp:Label ID="lbl_alcohol_print" runat="server" Text='<%# Eval("alcohol") %>' Visible="false"></asp:Label>
                                                    <asp:CheckBoxList ID="rdo_alcohol_hr_print" Font-Size="10" runat="server" Enabled="false" OnSelectedIndexChanged="onSelectedIndexChanged" AutoPostBack="true">
                                                        <asp:ListItem Value="1">&nbsp;ไม่เคย</asp:ListItem>
                                                        <asp:ListItem Value="2">&nbsp;โดยปกติดื่มน้อยกว่า 1 ครั้งต่อสัปดาห์</asp:ListItem>
                                                        <asp:ListItem Value="3">&nbsp;ดื่ม 1 ครั้งต่อสัปดาห์</asp:ListItem>
                                                        <asp:ListItem Value="4">&nbsp;ดื่ม 2-3 ครั้งต่อสัปดาห์</asp:ListItem>
                                                        <asp:ListItem Value="5">&nbsp;ดื่มมากกว่า 3 ครั้งต่อสัปดาห์</asp:ListItem>
                                                        <asp:ListItem Value="6">&nbsp;เคยแต่เลิกแล้วระยะเวลาที่ดื่มนาน</asp:ListItem>
                                                    </asp:CheckBoxList>
                                                </h3>
                                            </b>


                                            </td>

                                        </tr>


                                        <tr>
                                            <td style="text-align: left; border: 0px" colspan="2">
                                                <asp:UpdatePanel ID="update_alcohol_hr_print" runat="server" Visible="false">
                                                    <ContentTemplate>

                                                        <h4>
                                                            <span style="padding-left: 50px"><%# Eval("alcohol_value1") %> ปี &nbsp;&nbsp;<%# Eval("alcohol_value2") %> เดือน</span>
                                                        </h4>


                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td style="border: 0px"></td>
                                        </tr>

                                        <tr>
                                            <td style="text-align: left; border: 0px" colspan="2">
                                                <h3 style="padding-left: 50px;">๑๐. เคยเสพยาเสพติดหรือสารเสพติดใดๆ บ้างหรือไม่</h3>
                                                </b></td>
                                            <td style="border: 0px"></td>
                                        </tr>

                                        <tr>
                                            <td style="text-align: left; border: 0px">
                                                <h3 style="padding-left: 50px;">
                                                    <asp:Label ID="lbl_addict_drug" runat="server" Text='<%# Eval("addict_drug") %>' Visible="false"></asp:Label>
                                                    <asp:CheckBoxList ID="rdo_addict_drug" runat="server" Font-Size="10" RepeatLayout="Table"
                                                        RepeatColumns="0" RepeatDirection="Vertical"
                                                        Enabled="false" OnSelectedIndexChanged="onSelectedIndexChanged" AutoPostBack="true">
                                                        <asp:ListItem Value="ไม่เคย">&nbsp;ไม่เคย&nbsp;</asp:ListItem>
                                                        <asp:ListItem Value="เคย">&nbsp;เคย</asp:ListItem>
                                                        <%-- <asp:ListItem Value="3">&nbsp;เคยแต่เลิกแล้ว ระยะที่เคยสูบนาน</asp:ListItem>--%>
                                                    </asp:CheckBoxList>
                                                </h3>


                                            </td>

                                        </tr>

                                        <tr>
                                            <td style="text-align: left; border: 0px" colspan="2">
                                                <asp:UpdatePanel ID="Panel_addict_drug" runat="server" Visible="false">
                                                    <ContentTemplate>
                                                        <h4>
                                                            <span style="padding-left: 50px">ระบุ : &nbsp;<%# Eval("addict_drug") %></span>
                                                        </h4>

                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td style="border: 0px"></td>

                                        </tr>

                                        <tr>
                                            <td style="text-align: left; border: 0px" colspan="2"><b>
                                                <h3 style="padding-left: 50px;">๑๑. ข้อมูลทางสุขภาพอื่นๆ ที่เป็นประโยชน์</h3>
                                            </b></td>
                                            <td style="border: 0px"></td>
                                        </tr>

                                        <tr>
                                            <td style="text-align: left; border: 0px" colspan="2">
                                                <h4>
                                                    <span style="padding-left: 50px;"><%# Eval("datahealth") %></span>
                                                </h4>
                                            </td>
                                            <td style="border: 0px"></td>
                                        </tr>

                                    </table>
                                </div>
                            </ItemTemplate>
                        </asp:FormView>

                        <div style="page-break-after: always;"></div>
                        <div id="div_HistoryInjuryPrint" runat="server" visible="false">
                            <div class="panel panel-default">
                                <h1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1>
                                <h3 style="text-align: center;">บันทึกเกี่ยวกับการบาดเจ็บหรือเจ็บป่วยเนื่องจากการทำงานและสาเหตุ</h3>
                                <br />
                                <asp:GridView ID="gvInjuryPrintReport"
                                    runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="table table-striped table-bordered table-responsive"
                                    HeaderStyle-CssClass="info"
                                    AllowPaging="true"
                                    OnPageIndexChanging="gvPageIndexChanging"
                                    OnRowDataBound="gvRowDataBound">
                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                    <EmptyDataTemplate>
                                        <div class="text-center">-- ไม่พบข้อมูล --</div>
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:TemplateField HeaderText="วันเดือนปี" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="10" HeaderStyle-Width="15%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lbl_date_injuly_history" Font-Size="10" runat="server" Text='<%# Eval("date_injuly") %>' />
                                                </small>
                                            </ItemTemplate>
                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ส่วนของร่างกายที่บาดเจ็บหรือการเจ็บป่วย" ItemStyle-HorizontalAlign="Left"
                                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="10" HeaderStyle-Width="25%">
                                            <ItemTemplate>
                                                <small>

                                                    <asp:Label ID="lbl_injury_detail_history" Font-Size="10" runat="server" Text='<%# Eval("injury_detail") %>' />

                                                </small>

                                            </ItemTemplate>
                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ส่วนของการบาดเจ็บหรือการเจ็บป่วย" ItemStyle-HorizontalAlign="Left"
                                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="10" HeaderStyle-Width="25%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lbl_cause_Injury_history" Font-Size="10" runat="server" Text='<%# Eval("cause_Injury") %>' />
                                                </small>

                                            </ItemTemplate>
                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ระดับความรุนแรง" ItemStyle-HorizontalAlign="Left"
                                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="10" HeaderStyle-Width="25%">
                                            <ItemTemplate>
                                                <small>

                                                    <asp:Label ID="Detail_Violence" Font-Size="10" runat="server"> 
                                                    <p><b>ทุพพลภาพ :</b> <%# Eval("disability") %></p>
                                                    <p><b>สูญเสียอวัยวะบางส่วน :</b> <%# Eval("lost_organ") %></p>
                                                    <p><b>ทำงานไม่ได้ช่วยคราว :</b> <%# Eval("detail_not_working") %></p>
                                  
                                                    </asp:Label>
                                                </small>

                                            </ItemTemplate>
                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>

                        <%--<div style="page-break-after:always;"></div>--%>
                        <asp:Repeater ID="rpt_bindPrintHealth" runat="server" OnItemDataBound="rptOnRowDataBound">
                            <ItemTemplate>
                                <%--<td data-th="emp"><%# Eval("emp_idx") %></td>--%>
                                <asp:Label ID="lbl_emp_idx_print" runat="server" Visible="false" Text='<%# Eval("emp_idx") %>'></asp:Label>
                                <asp:Label ID="lbl_u2_historyhealth_idx_print" runat="server" Visible="false" Text='<%# Eval("u2_historyhealth_idx") %>'></asp:Label>

                                <asp:FormView ID="FvHealthCheckPrint" runat="server" DefaultMode="ReadOnly" OnDataBound="FvDetail_DataBound" Width="100%">
                                    <ItemTemplate>
                                        <div style="page-break-after: always;"></div>
                                        <div class="panel panel-default">
                                            <h2>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h2>
                                            <table class="table f-s-12 m-t-10" style="border: 0px; width: 100%">
                                                <h2 style="text-align: center;">การตรวจสุขภาพ</h2>

                                                <h3 style="text-align: center;">ครั้งที่ : <%# Eval("num_check") %></h3>

                                                <tr>
                                                    <td style="text-align: left; border: 0px" colspan="4">
                                                        <h3 style="text-align: left; width: 100%">
                                                            <asp:Label ID="rdo_idx_detailtype_print" Visible="false" Text='<%# Eval("m0_detail_typecheck_idx") %>' runat="server"></asp:Label>
                                                            <asp:CheckBoxList ID="rdoDetailType_detail_print" Font-Size="10" Enabled="false" runat="server" CssClass="" Font-Bold="true" RepeatDirection="Vertical" RepeatColumns="2" Width="100%"
                                                                AutoPostBack="true">
                                                            </asp:CheckBoxList>
                                                        </h3>
                                                    </td>
                                                    <td style="border: 0px"></td>
                                                    <td style="border: 0px"></td>
                                                    <td style="border: 0px"></td>
                                                </tr>
                                            </table>

                                            <table class="table f-s-12 m-t-10" style="border: 0px; width: 100%; text-align: center">
                                                <tr>
                                                    <td style="text-align: left; border: 0px"><b>
                                                        <h3>
                                                            <span style="padding-left: 10px;">วันที่ตรวจสุขภาพ : </span>
                                                        </h3>
                                                        
                                                    </b></td>

                                                    <td style="text-align: left; border: 0px">
                                                        <h3><span ><%# Eval("date_check") %></span></h3>
                                                        
                                                    </td>
                                                    <td style="border: 0px"></td>

                                                </tr>

                                                <tr>
                                                    <td style="text-align: left; border: 0px; width: 30%" class="col-sm-2"><b>
                                                        <h3>
                                                            <span style="padding-left: 10px;">ชื่อแพทย์ผู้ตรวจ : </span>
                                                        </h3>
                                                        
                                                    </b></td>
                                                    <td style="text-align: left; border: 0px; width: 70%" class="col-10" colspan="2">
                                                        <h3><span><%# Eval("doctor_name") %></span>

                                                        </h3>
                                                        
                                                    </td>
                                                    <td style="border: 0px"></td>
                                                </tr>
                                            </table>
                                            <table class="table f-s-12 m-t-10" style="border: 0px; width: 100%">
                                                <tr>
                                                    <td style="text-align: left; border: 0px; width: 50%"><b>
                                                        <h3>
                                                            <span style="padding-left: 10px;">เลขที่ใบประกอบวิชาชีพเวชกรรม : </span>
                                                        </h3>
                                                        
                                                    </b></td>
                                                    <td style="text-align: left; border: 0px; width: 50%">
                                                        <h3>
                                                            <span ><%# Eval("card_number") %></span>
                                                        </h3>
                                                        
                                                    </td>
                                                    <td style="border: 0px">></td>
                                                </tr>

                                                <tr>
                                                    <td style="text-align: left; border: 0px"><b>
                                                        <h3>
                                                            <span style="padding-left: 10px;">ชื่อหน่วยงานที่ตรวจสุขภาพ : </span>
                                                        </h3>
                                                        
                                                    </b></td>
                                                    <td style="text-align: left; border: 0px">
                                                        <h3>
                                                            <span style=""><%# Eval("health_authority_name") %></span>
                                                        </h3>
                                                        
                                                    </td>
                                                </tr>
                                            </table>
                                            <table class="table f-s-12 m-t-10" style="border: 0px; width: 100%;">
                                                <tr>
                                                    <td style="text-align: left; border: 0px; width: 30%"><b>
                                                        <h3>
                                                            <span style="padding-left: 10px;">ตั้งอยู่เลขที่ : <%# Eval("location_name") %></span>
                                                        </h3>
                                                        </b>
                                                    </td>

                                                    <td style="text-align: left; border: 0px; width: 20%"><b>
                                                        <h3>
                                                            <span style="">หมู่ที่ : <%# Eval("village_no") %></span>
                                                        </h3>
                                                        </b>
                                                    </td>

                                                    <td style="text-align: left; border: 0px; width: 50%"><b>
                                                        <h3>
                                                            <span style="">ถนน : <%# Eval("road_name") %></span>
                                                        </h3>
                                                        </b>
                                                    </td>
                                                </tr>
                                            </table>

                                            <table class="table f-s-12 m-t-10" style="border: 0px; width: 100%;">
                                                <tr>
                                                    <td style="text-align: left; border: 0px" colspan="4"><b>
                                                        <h3 style="padding-left: 10px;">๑. ตรวจสุขภาพทั่วไป : </h3>
                                                    </b></td>
                                                    <td style="border: 0px"></td>
                                                    <td style="border: 0px"></td>
                                                    <td style="border: 0px"></td>
                                                </tr>

                                                <tr>
                                                    <td style="text-align: left; border: 0px">
                                                        <asp:UpdatePanel ID="Update_GenaralHealth_Detail_print" runat="server">
                                                            <ContentTemplate>
                                                                <%--  <div id="div_genaralHealthCheck_detail" class="panel panel-default" runat="server">--%>
                                                                <tr>
                                                                    <td style="text-align: left; border: 0px" colspan="4"><b>
                                                                        <h3 style="padding-left: 25px">๑.๑ ผลการตรวจเบื้องต้น : </h3>
                                                                    </b></td>
                                                                    <td style="border: 0px"></td>
                                                                    <td style="border: 0px"></td>
                                                                    <td style="border: 0px"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="text-align: left; border: 0px; width: 25%"><b>
                                                                        <h3>
                                                                            <span style="padding-left: 40px;">น้ำหนัก : </span>
                                                                        </h3>
                                                                        
                                                                    </b></td>

                                                                    <td style="text-align: left; border: 0px; width: 25%">
                                                                        <h3>
                                                                            <span style=""><%# Eval("weight") %> กิโลกรัม</span>
                                                                        </h3>
                                                                        
                                                                    </td>

                                                                    <td style="text-align: left; border: 0px; width: 25%"><b>
                                                                        <h3>
                                                                            <span style="">ความสูง : </span>
                                                                        </h3>
                                                                        
                                                                    </b></td>

                                                                    <td style="text-align: left; border: 0px; width: 25%">
                                                                        <h3>
                                                                            <span style=""><%# Eval("height") %> เซนติเมตร</span>
                                                                        </h3>
                                                                        
                                                                    </td>
                                                                </tr>


                                                                <tr>
                                                                    <td style="text-align: left; border: 0px; width: 25%"><b>
                                                                        <h3>
                                                                            <span style="padding-left: 40px;">ดัชนีมวลกาย :
                                                                        </h3>
                                                                        </b></span>
                                                                    </td>
                                                                    <td style="text-align: left; border: 0px; width: 25%">
                                                                        <h3>
                                                                            <span style=""><%# Eval("body_mass") %>  </span>
                                                                        </h3>
                                                                        
                                                                    </td>
                                                                    <td style="text-align: left; border: 0px; width: 25%"><b>
                                                                        <h3>
                                                                            <span style="">ความดันโลหิต : </span>
                                                                        </h3>
                                                                        
                                                                    </b></td>

                                                                    <td style="text-align: left; border: 0px; width: 25%">
                                                                        <h3>
                                                                            <span style=""><%# Eval("bloodpressure") %> mm.Hg</span>
                                                                        </h3>
                                                                        
                                                                    </td>

                                                                </tr>

                                                                <tr>

                                                                    <td style="text-align: left; border: 0px;"><b>
                                                                        <h3>
                                                                            <span style="padding-left: 40px;">ชีพจร : </span>
                                                                        </h3>
                                                                        
                                                                    </b></td>

                                                                    <td style="text-align: left; border: 0px">
                                                                        <h3>
                                                                            <span style=""><%# Eval("pulse") %> ครั้ง/นาที</span>
                                                                        </h3>
                                                                        
                                                                    </td>
                                                                    <td style="border: 0px;"></td>
                                                                    <td style="border: 0px;"></td>
                                                                </tr>

                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table class="table f-s-12 m-t-10" style="border: 0px; width: 100%">
                                                <tr>
                                                    <td style="text-align: left; border: 0px"><b>
                                                        <asp:UpdatePanel ID="Update_specify_resultbody_print" runat="server">
                                                            <ContentTemplate>
                                                                <tr>
                                                                    <%--  <td>--%>
                                                                    <h3 style="padding-left: 25px;">๑.๒ ผลการตรวจร่างกายตามระบบ : </h3>
                                                                    <%--  </td>--%>
                                                                </tr>
                                                                <tr>
                                                                    <td style="text-align: left; border: 0px">
                                                                        <h3 style="padding-left: 50px;">
                                                                            <asp:Label ID="lbl_checksprcify" Visible="false" Text='<%# Eval("specify_resultbody") %>' runat="server"></asp:Label>

                                                                            <asp:CheckBoxList ID="chk_specify_resultbody" Font-Size="10" runat="server" RepeatLayout="Table"
                                                                                RepeatColumns="2" RepeatDirection="Vertical" Enabled="false">
                                                                                <asp:ListItem Value="ปกติ">&nbsp;ปกติ&nbsp;</asp:ListItem>
                                                                                <asp:ListItem Value="ไม่ปกติ">&nbsp;ไม่ปกติ</asp:ListItem>

                                                                            </asp:CheckBoxList>
                                                                        </h3>

                                                                        <asp:UpdatePanel ID="Panel_specify_resultbody" runat="server" Visible="false">
                                                                            <ContentTemplate>
                                                                                <h4>
                                                                                    <span style="padding-left: 50px;">ระบุ : &nbsp;<%# Eval("specify_resultbody") %></span>
                                                                                </h4>
                                                                                


                                                                            </ContentTemplate>
                                                                        </asp:UpdatePanel>


                                                                        <%--<h5>ระบุ : <%# Eval("specify_resultbody") %>  </h5>--%>
                                                                    </td>
                                                                </tr>

                                                            </ContentTemplate>
                                                        </asp:UpdatePanel></td>
                                                </tr>

                                                <tr>
                                                    <td style="text-align: left; border: 0px"><b>
                                                        <asp:UpdatePanel ID="UpdatePanel_resultlab_print" runat="server" style="padding-left: 25px;">
                                                            <ContentTemplate>
                                                                <h3 style="">๑.๓ ผลการตรวจทางห้องปฏิบัติการ : </h3>
                                                                <tr>
                                                                    <td style="text-align: left; border: 0px">
                                                                        <h3>
                                                                            <span style="padding-left: 50px"><%# Eval("resultlab") %>  </span>
                                                                        </h3>
                                                                        
                                                                    </td>
                                                                </tr>

                                                            </ContentTemplate>
                                                        </asp:UpdatePanel></td>
                                                </tr>

                                                <tr>
                                                    <td style="text-align: left; border: 0px"><b>
                                                        <h3 style="padding-left: 10px;">๒. ตรวจสุขภาพตามปัจจัยเสี่ยงของงาน : </h3>

                                                    </b></td>
                                                </tr>

                                                <tr>
                                                    <td style="text-align: left; border: 0px"><b>
                                                        <asp:UpdatePanel ID="Update_RikeHealthCheck_Detail_print" runat="server">
                                                            <ContentTemplate>
                                                                <div class="col-md-12">
                                                                    <div id="div_rikeHealthCheck_detail_print" class="panel panel-default" runat="server">
                                                                        <div class="panel-body">
                                                                            <table class="table table-striped f-s-12 table-empshift-responsive">
                                                                                <asp:Repeater ID="rpt_RikeDetail_print" runat="server">
                                                                                    <HeaderTemplate>
                                                                                        <%--<tr>
                                                                                            <th>ปัจจัยเสี่ยง</th>
                                                                                            <th>ผลการตรวจ</th>
                                                                                        </tr>--%>
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <tr>
                                                                                            <h3>
                                                                                                <td data-th="ปัจจัยเสี่ยง" style="padding-left: 50px;">ปัจจัยเสี่ยง : <%# Eval("rike_health") %></td>
                                                                                                <td data-th="ผลการตรวจ" style="">ผลการตรวจ : <%# Eval("result_health") %></td>
                                                                                            </h3>

                                                                                        </tr>
                                                                                    </ItemTemplate>
                                                                                </asp:Repeater>
                                                                            </table>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>

                                                    </b></td>
                                                </tr>


                                            </table>

                                        </div>
                                    </ItemTemplate>

                                </asp:FormView>

                            </ItemTemplate>

                        </asp:Repeater>

                        <div style="page-break-after: always;"></div>
                        <div class="panel panel-default">
                            <div id="div_detail_page4" runat="server" visible="true">
                                <h1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1>
                                <h3 style="text-align: center;">การตรวจสุขภาพตามปัจจัยเสี่ยงต้องประกอบด้วยการซักประวัติด้วยแบบสอบถาม</h3>
                                <h3 style="text-align: center;">การตรวจร่างกายและการตรวจพิเศษอื่นๆ เพิ่มเติมตามปัจจัยเสี่ยง ดังตัวอย่างต่อไปนี้</h3>
                               
                                <br />

                                <table class="table table-striped table-bordered table-hover table-responsive" style="width: 100%; text-align: center; border-width: 1px; border-style: solid">
                                    <tr class="info" style="font-size: Small; height: 40px;">

                                        <th scope="col" style="border-width: 1px; border-style: solid; text-align: center;">ปัจจัยเสี่ยง</th>
                                        <th scope="col" style="border-width: 1px; border-style: solid; text-align: center;">รายการตรวจสุขภาพ</th>


                                    </tr>
                                    <tr>
                                        <td style="border-width: 1px; border-style: solid; text-align: left;">
                                            <h3 style="">
                                                <span id="">๑. - ทำงานเกี่ยวกับสารเคมีอันตราย</span>
                                                <br />
                                                <span id="">เช่น สารตะกั่ว โทลูอีน เบนซิน</span>
                                                <br />
                                                <span id="">แคดเมียม เป็นต้น</span>
                                                <br />
                                                <span id="">- ทำงานสัมผัสฝุ่นแร่ เช่น ฝุ่นหิน </span>
                                                <br />
                                                <span id="">ฝุ่นทราย เป็นต้น</span>
                                                <br />
                                                <span id="">- ทำงานสัมผัสฝุ่นพืช เช่น ฝุ่นฝ้าย ป่าน </span>
                                                <br />
                                                <span id="">ปอ เป็นต้น</span>
                                            </h3>

                                        </td>
                                        <td style="border-width: 1px; border-style: solid; text-align: left;">
                                            <h3 style="">
                                                <span id="">- ตรวจวัดปริมาณสารเคมีในเลือด </span>
                                                <br />
                                                <span id="">หรือปัสสาวะหรือลมหายใจออก </span>
                                                <br />
                                                <span id="">- เอ็กซเรย์ปอดด้วยฟิล์มมาตรฐานและ </span>
                                                <br />
                                                <span id="">ตรวจสมรรถภาพปอด </span>
                                                <br />
                                                <span id="">- ตรวจสมรรถภาพปอดและเอ็กซเรย์ปอด </span>
                                                <br />
                                                <span id="">ด้วยฟิล์มขนาดมาตรฐาน (ถ้ามีข้อบ่งชี้) </span>
                                            </h3>

                                        </td>

                                    </tr>

                                    <tr>
                                        <td style="border-width: 1px; border-style: solid; text-align: left;">
                                            <h3 style="">
                                                <span id="">๒. - ทำงานกับผู้ป่วยติดเชื้อ</span>
                                                <br />
                                                <span id="">งานวิเคราะห์เกี่ยวกับจุลชีวัน</span>
                                                <br />
                                                <span id="">งานปศุสัตว์ เป็นต้น</span>
                                            </h3>


                                        </td>

                                        <td style="border-width: 1px; border-style: solid; text-align: left;">
                                            <h3 style="">
                                                <span id="">- ตรวจสุขภาพหาโรคติดเชื้อจากการ </span>
                                                <br />
                                                <span id="">ทำงานแต่ละชนิดโดยเฉพาะ </span>

                                            </h3>

                                        </td>

                                    </tr>

                                    <tr>
                                        <td style="border-width: 1px; border-style: solid; text-align: left;">
                                            <h3 style="">
                                                <span id="">๓. - ทำงานเกี่ยวกับรังสีชนิดก่อไอออน</span>

                                            </h3>

                                        </td>

                                        <td style="border-width: 1px; border-style: solid; text-align: left;">
                                            <h3 style="">
                                                <span id="">- ตรวจความสมบูรณ์ของเลือด </span>
                                                <br />
                                                <span id="">(complete Blood count) </span>
                                                <br />
                                                <span id="">หรือตรวจหาจำนวนสเปิร์ม (ในเพศชาย) </span>
                                            </h3>


                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="border-width: 1px; border-style: solid; text-align: left;">
                                            <h3 style="">
                                                <span id="">๔. - ทำงานสัมผัสเสียงดัง</span>
                                            </h3>


                                        </td>

                                        <td style="border-width: 1px; border-style: solid; text-align: left;">
                                            <h3 style="">
                                                <span id="">- ตรวจสมรรถภาพการได้ยิน </span>
                                            </h3>


                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="border-width: 1px; border-style: solid; text-align: left;">
                                            <h3 style="">
                                                <span id="">๕. - ทำงานที่ต้องใช้สายตาเพ่งนาน</span>
                                            </h3>


                                        </td>

                                        <td style="border-width: 1px; border-style: solid; text-align: left;">
                                            <h3 style="">
                                                <span id="">- ตรวจสมรรถภาพการมองเห็น </span>
                                            </h3>


                                        </td>
                                    </tr>

                                </table>

                                <br />
                                <br />

                            </div>
                        </div>

                        <div style="page-break-after: always;"></div>
                        <div class="panel panel-default">
                            <div id="div_pagenumber6" runat="server" visible="true">
                                <h1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1>
                                <h3 style="text-align: center;">คำแนะนำเกี่ยวกับการตรวจสุขภาพ</h3>
                                <br />

                                <h3 style="text-align: left; padding-left: 100px;">๑. การตรวจสุขภาพครั้งแรกภายใน ๓๐ วันเป็นการตรวจเพื่อประโยชน์ </h3>
                                <h3 style="text-align: left; padding-left: 50px;">ของผู้ที่จะเข้าทำงานและลดความเสี่ยงของโรคหรือคัดเลือกผู้มีสภาพร่างกาย </h3>
                                <h3 style="text-align: left; padding-left: 50px;">เหมาะสมในการทำงานนั้นๆ ในกรณีที่ตรวจพบว่ามีความผิดปกติ </h3>
                                <h3 style="text-align: left; padding-left: 50px;">บางอย่างควรปรึกษาแพทย์ก่อนว่าจะสามารถทำงานนั้นได้อย่างปลอดภัย </h3>
                                <h3 style="text-align: left; padding-left: 50px;">หรือไม่ และจะต้องดูแลสุขภาพในระหว่างการทำงานดังกล่าวอย่างไร </h3>
                                <h3 style="text-align: left; padding-left: 100px;">๒. ในระหว่างการทำงาน ลูกจ้างควรสำรวจสุขภาพของตัวเองเป็น </h3>
                                <h3 style="text-align: left; padding-left: 50px;">ประจำอย่างน้อยปีละ ๑ ครั้ง เพื่อจะได้ดำเนินการป้องกันและแก้ไขต่อไป</h3>
                                <h3 style="text-align: left; padding-left: 50px;">ซึ่งความผิดปกติหรือการเกิดโรคตามระบบต่างๆ เช่น</h3>
                                <h3 style="text-align: left; padding-left: 100px;">- ระบบสายตา เช่น ปวดตา มองเห็นไม่ชัด </h3>
                                <h3 style="text-align: left; padding-left: 100px;">- ระบบการได้ยิน เช่น หูตึง หูหนวก </h3>
                                <h3 style="text-align: left; padding-left: 100px;">- ระบบหายใจ เช่น หอบ ไอเรื้อรัง เสมหะปนเลือด เจ็บหน้าอก ปอดอักเสบ หายใจขัด</h3>
                                <h3 style="text-align: left; padding-left: 100px;">- ระบบกระดูกและกล้ามเนื้อ เช่น ปวดหลัง ปวดคอ หมอนรองกระดูกเคลื่อน ปวดตามเอ็นหรือ กล้ามเนื้อ</h3>
                                <h3 style="text-align: left; padding-left: 100px;">- ระบบผิวหนัง เช่น ผื่นคัน ผื่นแดงอักเสบ </h3>
                                <h3 style="text-align: left; padding-left: 100px;">- ระบบประสาท เช่น ปวดศรีษะ มึนงง ความจำเสื่อม ลมชัก </h3>
                                <h3 style="text-align: left; padding-left: 100px;">หากมีอาการดังกล่าวซึ่งอาจเกี่ยวข้องกับสภาพการทำงานที่ไม่ปลอดภัย </h3>
                                <h3 style="text-align: left; padding-left: 50px;">ต้องรีบดำเนินการหาสาเหตุและแก้ไขทันที รวมทั้งควรปรึกษาแพทย์เพื่อรับการรักษาต่อไป </h3>

                                <br />
                                <br />
                            </div>
                        </div>

                    </div>

                </div>
            </div>

        </asp:View>
        <!--View Report-->
    </asp:MultiView>


    <script type="text/javascript">
        function printDiv(divName) {
            var panel = document.getElementById(divName);


            //var gvname_ = document.getElementById(gvname);
            var printWindow = window.open('', '', 'height=1000,width=800');
            printWindow.document.write('<html><head><title></title></head>');
            printWindow.document.write('<body style="font-size:12px;">');
            printWindow.document.write('<style>table{width:100%;color:#000000;font-size:10px;};</style>');
            printWindow.document.write(panel.innerHTML);
            printWindow.document.write('</body></html>');
            //document.getElementById(gvname).Enable = true;
            printWindow.document.close();
            setTimeout(function () {
                printWindow.print();
            }, 500);
            //document.getElementById('gvWork_scroll').Visible = true;
            //return false;
        }
    </script>

    <script>
        $(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });
        });


        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY',
                    ignoreReadonly: true
                });
            });

            /* START Filter Datetimepicker Permission Permanent */
            $('.datetimepicker-filter-perm-from').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });
            $('.datetimepicker-filter-perm-from').on('dp.change', function (e) {
                var dateTo = $('.datetimepicker-filter-perm-to').data("DateTimePicker").date();
                if (e.date > dateTo) {
                    $('.datetimepicker-filter-perm-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
<%-- $('#<%= HiddenCalDate.ClientID %>').val($('.datetimepicker-filter-perm-from').val());--%>
            });
            $('.show-filter-perm-from-onclick').click(function () {
                $('.datetimepicker-filter-perm-from').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-perm-to').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });
            $('.show-filter-perm-to-onclick').click(function () {
                $('.datetimepicker-filter-perm-to').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-perm-to').on('dp.change', function (e) {
                var dateFrom = $('.datetimepicker-filter-perm-from').data("DateTimePicker").date();
                if (e.date < dateFrom) {
                    $('.datetimepicker-filter-perm-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
<%--$('#<%= txtFilterIndexPermPermToHidden.ClientID %>').val($('.datetimepicker-filter-perm-to').val());--%>
            });

            $('.show-filter').click(function () {
                $('.datetimepicker-filter').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });

            /* END Filter Datetimepicker Permission Permanent */
        });


        /* START Filter Datetimepicker Permission Permanent */
        $('.datetimepicker-filter-perm-from').datetimepicker({
            format: 'DD/MM/YYYY',
            ignoreReadonly: true
        });
        $('.datetimepicker-filter-perm-from').on('dp.change', function (e) {
            var dateTo = $('.datetimepicker-filter-perm-to').data("DateTimePicker").date();
            if (e.date > dateTo) {
                $('.datetimepicker-filter-perm-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
            }
<%-- $('#<%= HiddenCalDate.ClientID %>').val($('.datetimepicker-filter-perm-from').val());--%>
        });
        $('.show-filter-perm-from-onclick').click(function () {
            $('.datetimepicker-filter-perm-from').data("DateTimePicker").show();
        });
        $('.datetimepicker-filter-perm-to').datetimepicker({
            format: 'DD/MM/YYYY',
            ignoreReadonly: true
        });
        $('.show-filter-perm-to-onclick').click(function () {
            $('.datetimepicker-filter-perm-to').data("DateTimePicker").show();
        });
        $('.datetimepicker-filter-perm-to').on('dp.change', function (e) {
            var dateFrom = $('.datetimepicker-filter-perm-from').data("DateTimePicker").date();
            if (e.date < dateFrom) {
                $('.datetimepicker-filter-perm-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
            }
<%--$('#<%= txtFilterIndexPermPermToHidden.ClientID %>').val($('.datetimepicker-filter-perm-to').val());--%>
        });
        $('.show-filter').click(function () {
            $('.datetimepicker-filter').data("DateTimePicker").show();
        });
        $('.datetimepicker-filter').datetimepicker({
            format: 'DD/MM/YYYY',
            ignoreReadonly: true
        });
    /* END Filter Datetimepicker Permission Permanent */


    </script>

</asp:Content>

