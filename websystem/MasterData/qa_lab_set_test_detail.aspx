﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="qa_lab_set_test_detail.aspx.cs" Inherits="websystem_MasterData_qa_lab_set_test_detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    <asp:HyperLink ID="SETFOCUS" runat="server"></asp:HyperLink>
    <!--multiview-->
    <asp:MultiView ID="mvMaster" runat="server">
        <asp:View ID="pageGenaral" runat="server">
            <div class="form-group">
                <asp:LinkButton ID="btnCreateForm" CssClass="btn btn-primary" runat="server" data-original-title="เพิ่มชุดรายการตรวจ" data-toggle="tooltip"
                    OnCommand="btnCommand" CommandName="cmdCreateTest" CommandArgument="0" title="เพิ่มชุดรายการตรวจ"><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่มชุดรายการตรวจ</asp:LinkButton>
                <asp:LinkButton ID="btnCancel" CssClass="btn btn-default" runat="server" data-original-title="Back" data-toggle="tooltip" Visible="false"
                    OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="0" title=""><i class="fa fa-reply" aria-hidden="true"></i> กลับ</asp:LinkButton>
                <%--  <asp:LinkButton ID="btnCancel" CssClass="btn btn-danger" runat="server" Visible="false" data-original-title="Cancel" data-toggle="tooltip"
                    Text="ยกเลิก" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="0"></asp:LinkButton>--%>
            </div>
            <!--formview insert-->
            <asp:FormView ID="fvformInsert" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>

                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">&nbsp;เพิ่มชุดรายการตรวจวิเคราะห์</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">


                                    <label class="col-sm-2 control-label">ชื่อชุดการตรวจ</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="tbSetTestName" runat="server" CssClass="form-control" placeholder="กรอกชื่อชุดการตรวจ ..." Enabled="true" />
                                        <asp:RequiredFieldValidator ID="RequiredtbSetTestName" runat="server"
                                            ControlToValidate="tbSetTestName" Display="None" SetFocusOnError="true"
                                            ErrorMessage="*กรุณากรอกชื่อชุดตรวจ" ValidationGroup="save" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatortbSetTestName" runat="Server"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredtbSetTestName" Width="180" />
                                    </div>


                                    <div class="col-sm-1"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ประเภทการตรวจ</label>
                                    <div class="col-sm-8">
                                        <asp:DropDownList ID="ddlTestType" runat="server" CssClass="form-control" Enabled="true"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredddlTestType" runat="server" InitialValue="0"
                                            ControlToValidate="ddlTestType" Display="None" SetFocusOnError="true"
                                            ErrorMessage="*กรุณาเลือกประเภทการตรวจ" ValidationGroup="save" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorddlTestType" runat="Server"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredddlTestType" Width="180" />
                                    </div>


                                    <div class="col-sm-1">
                                        <asp:LinkButton ID="btnAddForm" CssClass="btn btn-default pull-right" runat="server" data-original-title="เพิ่ม" data-toggle="tooltip"
                                            OnCommand="btnCommand" CommandName="cmdAddSetTest" ValidationGroup="save" title="เพิ่ม"><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่ม</asp:LinkButton>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">สถานะ</label>
                                    <div class="col-sm-8">
                                        <asp:DropDownList ID="ddlFormStatus" runat="server" CssClass="form-control" Enabled="true">
                                            <asp:ListItem Text=" Online " Value="1"></asp:ListItem>
                                            <asp:ListItem Text=" Offline " Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-1"></div>
                                </div>
                                <div class="form-group">
                                    <asp:Panel ID="panelActionTime" runat="server" Visible="false">
                                        <label class="col-sm-2 control-label">ระยะเวลา</label>
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtActionTime" runat="server" CssClass="form-control" placeholder="กรอกระยะเวลาในการดำเนินการ.." Enabled="true" />
                                            <asp:RequiredFieldValidator ID="RequiredtxtActionTime" runat="server"
                                                ControlToValidate="txtActionTime" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกระยะเวลาในการดำเนินการ" ValidationGroup="save" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatortxtActionTime" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredtxtActionTime" Width="180" />

                                            <asp:RegularExpressionValidator ID="RegulartxtActionTime"
                                                runat="server" ErrorMessage="*กรอกเฉพาะตัวเลขเท่านั้น" SetFocusOnError="true"
                                                Display="None" ControlToValidate="txtActionTime" ValidationExpression="^[1-9'\s]{1,10}$"
                                                ValidationGroup="save" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValtxtActionTime" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegulartxtActionTime" Width="180" />
                                        </div>
                                        <label class="control-label">วัน</label>

                                    </asp:Panel>
                                    <div class="col-sm-6"></div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">หัวข้อรายการที่เพิ่ม</label>
                                    <div class="col-sm-9">

                                        <asp:GridView ID="gvCreateSetTest"
                                            runat="server"
                                            AutoGenerateColumns="false"
                                            CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                            HeaderStyle-CssClass="success"
                                            HeaderStyle-Height="30px"
                                            OnRowDeleting="gvRowDeleted"
                                            OnPageIndexChanging="gvPageIndexChanging"
                                            OnRowEditing="gvRowEditing"
                                            OnRowUpdating="gvRowUpdating"
                                            OnRowCancelingEdit="gvRowCancelingEdit"
                                            OnRowDataBound="gvRowDataBound">
                                            <EmptyDataTemplate>
                                                <div style="text-align: center">Data Cannot Be Found</div>
                                            </EmptyDataTemplate>
                                            <Columns>
                                                <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbnumber" runat="server" Visible="false" />
                                                        <%# (Container.DataItemIndex +1) %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ประเภทการตรวจ" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="left" HeaderStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <small>
                                                            <asp:Label ID="lblNameTestDetail" runat="server"
                                                                Text='<%# Eval("nameTestDetail") %>'></asp:Label>
                                                        </small>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <small>
                                                            <asp:LinkButton ID="btndelete" CssClass="text-trash small" runat="server" CommandName="Delete"
                                                                data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('Do you want delete this item?')" title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                                                        </small>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                    <div class="col-sm-1"></div>
                                </div>

                                <asp:Panel ID="panelFormButton" runat="server" Visible="false">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-10">
                                            <asp:LinkButton ID="lnkbtnSave" CssClass="btn btn-success" ValidationGroup="save" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                Text="Save" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="0"></asp:LinkButton>
                                            <asp:LinkButton ID="lnkbtnCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                Text="Cancel" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="0"></asp:LinkButton>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                        </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:GridView ID="gvMaster"
                runat="server"
                AutoGenerateColumns="false"
                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                HeaderStyle-CssClass="success"
                HeaderStyle-Height="30px"
                AllowPaging="true"
                PageSize="5"
                OnPageIndexChanging="gvPageIndexChanging"
                OnRowEditing="gvRowEditing"
                OnRowUpdating="gvRowUpdating"
                OnRowCancelingEdit="gvRowCancelingEdit"
                OnRowDataBound="gvRowDataBound">
                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                    FirstPageText="First" LastPageText="Last" />
                <EmptyDataTemplate>
                    <div style="text-align: center">Data Cannot Be Found</div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <%--      <div style="text-align: center;">--%>
                            <asp:Label ID="lbFormidx" runat="server" CssClass="left left" Visible="false" Text='<%# Eval("set_test_detail_idx")%>'></asp:Label>
                            <%# (Container.DataItemIndex +1) %>
                            <%-- </div>--%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ชื่อชุดรายการตรวจ" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lblFormName" runat="server" Text='<%# Eval("set_test_detail_name")%>'></asp:Label>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ประเภทรายการตรวจ" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <asp:GridView ID="gvTopic" Visible="true" runat="server" AutoGenerateColumns="false"
                                    CssClass="table" GridLines="None" ShowHeader="false" ShowFooter="false"
                                    HeaderStyle-Height="10px" AllowPaging="true" BackColor="Transparent" BorderColor="Transparent"
                                    OnRowDataBound="gvRowDataBound">
                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                                        FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="ชื่อประเภทการตรวจ" HeaderStyle-CssClass="text_left" ItemStyle-BorderColor="Transparent"
                                            ItemStyle-HorizontalAlign="left" HeaderStyle-Width="99%" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="9">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblTopicName" runat="server" Text='<%# " - " + Eval("test_detail_name")%>'></asp:Label>

                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>


                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ระยะเวลาในการดำเนินการ" ItemStyle-HorizontalAlign="center"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lbTestType" runat="server" Text='<%# Eval("test_detail_time") + " " + " วัน "%>'></asp:Label>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>


                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="center"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>

                                <asp:Label ID="lbstatus" Visible="false" runat="server"
                                    CssClass="col-sm-12" Text='<%# Eval("set_test_detail_status") %>'></asp:Label>
                                <asp:Label ID="status_offline" runat="server" Visible="false"
                                    data-toggle="tooltip" data-placement="top" title="offline"
                                    CssClass="col-sm-12">
                                                <div style="text-align: center; color: red;">
                                                    <span><b>offline</b></span>
                                </asp:Label>
                                <asp:Label ID="status_online" runat="server" Visible="false"
                                    data-toggle="tooltip" data-placement="top" title="online" CssClass="col-sm-12">
                                                <div style="text-align: center; color: green;">
                                                <span><b>online</b></span>
                                </asp:Label>

                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>

                            <asp:LinkButton ID="btndelettypeNews" CssClass="text-trash" runat="server" CommandName="cmdDelete"
                                data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('Do you want delete this item?')"
                                CommandArgument='<%#Eval("set_test_detail_idx") %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:View>
    </asp:MultiView>




</asp:Content>

