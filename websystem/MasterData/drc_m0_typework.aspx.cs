﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_drc_m0_typework : System.Web.UI.Page
{
    #region Connect
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();

    data_drc _data_drc = new data_drc();


    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];

    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    //-- employee --//


    //master
    static string _urlGetM0TypeWorkDRC = _serviceUrl + ConfigurationManager.AppSettings["urlGetM0TypeWorkDRC"];
    static string _urlSetM0TypeWorkDRC = _serviceUrl + ConfigurationManager.AppSettings["urlSetM0TypeWorkDRC"];
    static string _urlSetUpdateM0TypeWorkDRC = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateM0TypeWorkDRC"];
    static string _urlSetDelM0TypeWorkDRC = _serviceUrl + ConfigurationManager.AppSettings["urlSetDelM0TypeWorkDRC"];
    //master



    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    bool _flag_qmr = false;
    #endregion Connect

    #region Page_Init
    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        ////getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];

    }
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SelectMaster();
            ViewState["empIDX"] = Session["emp_idx"];
            ////Select_Equipment_result();


        }
    }
    #endregion Page Load

    protected void SelectMaster()
    {
        data_drc _data_drc = new data_drc();
        TypeWorkDetail _m0_typework = new TypeWorkDetail();

        _data_drc.BoxTypeWorkList = new TypeWorkDetail[1];

        _data_drc.BoxTypeWorkList[0] = _m0_typework;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));
        _data_drc = callServicePostDRC(_urlGetM0TypeWorkDRC, _data_drc);

        setGridData(GvMaster, _data_drc.BoxTypeWorkList);

    }

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        switch (cmdName)
        {

            case "cmdInsert":

                MvMaster.SetActiveView(ViewInsert);

                break;

            case "cmdSave":
               
                data_drc _data_drc = new data_drc();
                TypeWorkDetail _m0_typework = new TypeWorkDetail();

                _data_drc.BoxTypeWorkList = new TypeWorkDetail[1];

                _m0_typework.Type_Work = txtType_Work.Text;
                _m0_typework.Work_Status = int.Parse(ddl_Work_Status.SelectedValue);
                _m0_typework.CEmpIDX = _emp_idx;

                _data_drc.BoxTypeWorkList[0] = _m0_typework;
                //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));
                _data_drc = callServicePostDRC(_urlSetM0TypeWorkDRC, _data_drc);

                //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_resulution));
                if (_data_drc.return_code == 0)
                {
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);

                }

                break;

            case "cmdDelete":

                int TWIDX_del = int.Parse(cmdArg);

                data_drc _data_drc_del = new data_drc();
                TypeWorkDetail _m0_devices_name_del = new TypeWorkDetail();

                _data_drc_del.BoxTypeWorkList = new TypeWorkDetail[1];

                _m0_devices_name_del.TWIDX = TWIDX_del;
                _m0_devices_name_del.CEmpIDX = _emp_idx;

                _data_drc_del.BoxTypeWorkList[0] = _m0_devices_name_del;
                //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));
                _data_drc = callServicePostDRC(_urlSetDelM0TypeWorkDRC, _data_drc_del);


                SelectMaster();


                break;

            case "cmdCancel":

                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;




        }
    }
    #endregion btnCommand

    #region bind data
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected string getStatus(int status)
    {
        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }
    #endregion bind data

    #region Gridview
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":


                if (e.Row.RowType == DataControlRowType.DataRow)
                {


                    Label lbl_Work_Status = (Label)e.Row.FindControl("lbl_Work_Status");
                    Label lbl_Work_StatusOnline = (Label)e.Row.FindControl("lbl_Work_StatusOnline");
                    Label lbl_Work_StatusOffline = (Label)e.Row.FindControl("lbl_Work_StatusOffline");

                    ViewState["vs_Type_Workstatus"] = lbl_Work_Status.Text;


                    if (ViewState["vs_Type_Workstatus"].ToString() == "1")
                    {
                        lbl_Work_StatusOnline.Visible = true;
                    }
                    else if (ViewState["vs_Type_Workstatus"].ToString() == "0")
                    {
                        lbl_Work_StatusOffline.Visible = true;
                    }
                    else
                    {

                    }

                }

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";


                }

                break;
        }
    }

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.EditIndex = e.NewEditIndex;
                SelectMaster();

                break;
        }
    }

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":

                var txt_TWIDX_edit = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txt_TWIDX_edit");
                var txt_Type_Work_edit = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txt_Type_Work_edit");
                var ddl_Work_Status_edit = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddl_Work_Status_edit");

                GvMaster.EditIndex = -1;

                data_drc _data_drc = new data_drc();
                TypeWorkDetail _m0_typework = new TypeWorkDetail();

                _data_drc.BoxTypeWorkList = new TypeWorkDetail[1];

                _m0_typework.Type_Work = txt_Type_Work_edit.Text;
                _m0_typework.Work_Status = int.Parse(ddl_Work_Status_edit.SelectedValue);
                _m0_typework.CEmpIDX = _emp_idx;
                _m0_typework.TWIDX = int.Parse(txt_TWIDX_edit.Text);

                _data_drc.BoxTypeWorkList[0] = _m0_typework;
                //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));
                _data_drc = callServicePostDRC(_urlSetUpdateM0TypeWorkDRC, _data_drc);


                if (_data_drc.return_code == 0)
                {
                    SelectMaster();
                }

                break;
        }
    }

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.EditIndex = -1;
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
        }
    }

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();
                SelectMaster();
                break;
        }
    }

    #endregion Gridview

    #region event dropdownlist

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }
    #endregion dropdownlist

    #region callService 

    protected data_drc callServicePostDRC(string _cmdUrl, data_drc _data_drc)
    {
        _localJson = _funcTool.convertObjectToJson(_data_drc);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_drc = (data_drc)_funcTool.convertJsonToObject(typeof(data_drc), _localJson);


        return _data_drc;
    }

    #endregion callService
}