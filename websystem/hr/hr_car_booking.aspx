﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="hr_car_booking.aspx.cs" Inherits="websystem_hr_hr_car_booking" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:HyperLink ID="setOntop" runat="server"></asp:HyperLink>

    <!--tab menu-->
    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default" id="divBtnHead" runat="server" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-bar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="sub-navbar">
                        <a class="navbar-brand" href="#"><b>Menu</b></a>
                    </div>
                </div>

                <!--Collect the nav links, forms, and other content for toggling-->
                <div class="collapse navbar-collapse" id="menu-bar">
                    <ul class="nav navbar-nav" id="uiNav" runat="server">
                        <li id="li0" runat="server">
                            <asp:LinkButton ID="lbDetailRoom" runat="server" CommandName="cmdDetailRoom" OnCommand="navCommand" CommandArgument="docDetailCar"> รายการรถ</asp:LinkButton>
                        </li>
                        <li id="li1" runat="server">
                            <asp:LinkButton ID="lbDetail" runat="server" CommandName="cmdDetail" OnCommand="navCommand" CommandArgument="docDetail"> รายการทั่วไป</asp:LinkButton>
                        </li>
                        <li id="li2" runat="server">
                            <asp:LinkButton ID="lbCreate" runat="server" CommandName="cmdCreate" OnCommand="navCommand" CommandArgument="docCreate"> สร้างรายการจองรถ</asp:LinkButton>
                        </li>
                        <li id="li3" runat="server">
                            <asp:LinkButton ID="lbApprove" runat="server" CommandName="cmdApprove" OnCommand="navCommand" CommandArgument="docApprove"> รายการรออนุมัติ</asp:LinkButton>
                        </li>
                        <li id="li4" runat="server">
                            <asp:LinkButton ID="lbReport" runat="server" CommandName="cmdReport" OnCommand="navCommand" CommandArgument="docReport"> รายงาน</asp:LinkButton>
                        </li>

                    </ul>

                    <ul class="nav navbar-nav navbar-right" runat="server">

                        <li id="_divMenuLiToDocument" runat="server">
                            <asp:HyperLink ID="_divMenuBtnToDocument" runat="server"
                                NavigateUrl="https://docs.google.com/document/d/1qjlYvqPlYoq0uldvKZOVs7BQAl4emwGql_SSOipUNP8/edit?usp=sharing" Target="_blank" Text="คู่มือการใช้งาน" />
                        </li>
                    </ul>
                </div>
                <!--Collect the nav links, forms, and other content for toggling-->

            </div>
        </nav>
    </div>
    <!--tab menu-->


    <div class="col-md-12">
        <asp:Literal ID="litDebug" runat="server"></asp:Literal>
        <asp:Literal ID="litDebug1" runat="server"></asp:Literal>
    </div>

    <!--multiview-->
    <asp:MultiView ID="mvSystem" runat="server">

        <!--View Detail Car Booking-->
        <asp:View ID="docDetailCar" runat="server">

            <div class="col-md-12">

                <%-- <div class="panel panel-info" id="Calendar_Selected" runat="server" visible="true">
                    <div class="panel-body">--%>

                <%-- <div class="alert alert-primary f-s-14" role="alert">
                    <h3 class="panel-title">รายการจองรถ</h3>
                </div>--%>

                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">รายการจองรถ</h3>
                    </div>
                </div>

                <div class="panel panel-success" id="Calendar_Selected" runat="server" visible="true">
                    <div class="panel-body">

                        <div class="row col-md-3">
                            <asp:FormView ID="FvDetailUseCarSearch" runat="server" DefaultMode="Insert" Width="100%">
                                <InsertItemTemplate>

                                    <div class="panel panel-info">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">ค้นหารายการจองรถ</h3>
                                        </div>
                                        <div class="panel-body">
                                            <%--<div class="form-horizontal" role="form">--%>

                                            <asp:UpdatePanel ID="Update_SearchTypeCarUse" runat="server">
                                                <ContentTemplate>
                                                    <div class="col-sm-12">
                                                        <asp:DropDownList ID="ddlCarUseNameSearch" runat="server" CssClass="form-control ddlCarUseNameSearch" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                        </asp:DropDownList>

                                                        <br />
                                                    </div>

                                                </ContentTemplate>
                                            </asp:UpdatePanel>

                                            <asp:UpdatePanel ID="Update_SearchUserCar" runat="server" Visible="false">
                                                <ContentTemplate>

                                                    <div class="col-sm-12">
                                                        <asp:DropDownList ID="ddlDetailtypecarSearch" runat="server" CssClass="form-control ddlDetailtypecarSearch" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                        <%-- <asp:RequiredFieldValidator ID="Re_ddlDetailtypecarSearch"
                                                            runat="server"
                                                            InitialValue="0"
                                                            ControlToValidate="ddlDetailtypecarSearch" Display="None" SetFocusOnError="true"
                                                            ErrorMessage="*กรุณาเลือกสถานที่"
                                                            ValidationGroup="SearchDetail" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="Validator1" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlPlaceSearch" Width="200" PopupPosition="BottomLeft" />--%>
                                                        <br />
                                                    </div>


                                                    <div class="col-sm-12">
                                                        <asp:DropDownList ID="ddlm0car_idxSearch" runat="server" CssClass="form-control ddlm0car_idxSearch" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                            <asp:ListItem Value="0">--- เลือกทะเบียนรถ ---</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <br />
                                                    </div>

                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <%--</div>--%>
                                        </div>
                                    </div>

                                </InsertItemTemplate>
                            </asp:FormView>
                        </div>


                        <div class="col-md-9">
                            <div id="calendar">
                                <div id="fullCalModal" class="modal fade">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">close</span></button>
                                                <h4 id="modalTitle" class="modal-title"></h4>
                                            </div>
                                            <div id="modalBody" class="modal-body">uteueuetuteu</div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                <%-- <button class="btn btn-primary"><a id="eventUrl" target="_blank">Event Page</a></button>--%>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </asp:View>
        <!--View Detail Car Booking-->

        <!--View Detail-->
        <asp:View ID="docDetail" runat="server">
            <div class="col-md-12">

                <div class="form-group" id="Panel_searchDetailIndex" runat="server">
                    <asp:LinkButton ID="btnSearchDetailIndex" CssClass="btn btn-primary" runat="server" data-original-title="แสดงแถบเครื่องมือค้นหา" data-toggle="tooltip" CommandName="cmdSearchDetailIndex" OnCommand="btnCommand"><span class="glyphicon glyphicon-search"></span> ค้นหา</asp:LinkButton>

                </div>

                <!-- Form Search Detail Index -->
                <asp:FormView ID="FvSearchDetailIndex" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <InsertItemTemplate>

                        <!-- Hide Search Detail Index -->
                        <asp:UpdatePanel ID="Update_HideSearchDetailIndex" runat="server">
                            <ContentTemplate>
                                <div class="form-group">
                                    <asp:LinkButton ID="btnHideSearchDetailIndex" CssClass="btn btn-danger" data-toggle="tooltip" title="ซ่อนแถบเครื่องมือค้นหา" runat="server"
                                        CommandName="cmdHideSearchDetailIndex" OnCommand="btnCommand">< ซ่อนแถบเครื่องมือค้นหา</asp:LinkButton>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <!-- Hide Search Detail Index -->

                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">ค้นหารายการจองรถ</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <asp:UpdatePanel ID="Panel_SearchDetailIndex" runat="server">
                                        <ContentTemplate>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">สถานที่</label>
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddlPlaceSearchDetail" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                    </asp:DropDownList>

                                                </div>

                                                <label class="col-sm-2 control-label">ประเภทรถที่ใช้</label>
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddlCarUseSearchDetail" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                    </asp:DropDownList>

                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <asp:UpdatePanel ID="Update_DetailtypecarSearchDetail" Visible="true" runat="server">
                                                    <ContentTemplate>
                                                        <label class="col-sm-2 control-label">ประเภทรถ</label>
                                                        <div class="col-sm-4">
                                                            <asp:DropDownList ID="ddlDetailtypecarSearchDetail" runat="server" Enabled="false" CssClass="form-control">
                                                                <asp:ListItem Value="0">--- เลือกประเภทรถ ---</asp:ListItem>
                                                            </asp:DropDownList>

                                                        </div>

                                                        <label class="col-sm-2 control-label">เลขทะเบียน</label>
                                                        <div class="col-sm-4">
                                                            <asp:DropDownList ID="ddlm0caridxSearchDetail" runat="server" Enabled="false" CssClass="form-control">
                                                                <asp:ListItem Value="0">--- เลือกเลขทะเบียน ---</asp:ListItem>
                                                            </asp:DropDownList>

                                                        </div>

                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>

                                            <div class="form-group" id="panel_searchdate" runat="server">
                                                <%-- <asp:Label ID="Label9" runat="server" Text="ระบุวันค้นหา" CssClass="col-sm-2 control-label"></asp:Label>--%>
                                                <label class="col-sm-2 control-label">ระบุวันค้นหา</label>
                                                <div class="col-sm-2">
                                                    <asp:DropDownList ID="ddlSearchDate" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                        <asp:ListItem Value="00">เลือกเงื่อนไข</asp:ListItem>
                                                        <asp:ListItem Value="1">มากกว่า  ></asp:ListItem>
                                                        <asp:ListItem Value="2">น้อยกว่า <</asp:ListItem>
                                                        <asp:ListItem Value="3">ระหว่าง  <></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-sm-4 ">

                                                    <div class='input-group date'>
                                                        <asp:TextBox ID="AddStartdate" runat="server" CssClass="form-control from-date-datepicker" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                        <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="SearchDetail" runat="server" Display="None"
                                                            ControlToValidate="AddStartdate" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกวันที่ต้องการ"
                                                            ValidationExpression="กรุณาเลือกวันที่ต้องการ" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender51" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator3" Width="160" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class='input-group date'>
                                                        <asp:TextBox ID="AddEndDate" runat="server" CssClass="form-control from-date-datepicker" Enabled="false" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                        <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                    </div>
                                                </div>
                                                <%--<p class="help-block"><font color="red">**</font></p>--%>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">สถานะรายการ</label>
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddl_StatusDetail" runat="server" CssClass="form-control">
                                                    </asp:DropDownList>

                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label"></label>
                                                <div class="col-sm-4">
                                                    <asp:LinkButton ID="btnSearchIndexDetailCar" runat="server"
                                                        CssClass="btn btn-primary" OnCommand="btnCommand" CommandName="cmdSearchIndexDetailCar" data-original-title="ค้นหา" data-toggle="tooltip" ValidationGroup="SearchDetail"><span class="glyphicon glyphicon-search"></span> ค้นหา</asp:LinkButton>

                                                </div>
                                                <label class="col-sm-6 control-label"></label>
                                            </div>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>


                                </div>
                            </div>
                        </div>

                    </InsertItemTemplate>
                </asp:FormView>
                <!-- Form Search Detail Index -->


                <!-- Back To Detail Car Booking -->
                <asp:UpdatePanel ID="Update_BackToDetail" runat="server" Visible="false">
                    <ContentTemplate>
                        <div class="form-group">
                            <asp:LinkButton ID="btnBackToDetail" CssClass="btn btn-danger" data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                                CommandName="cmdBackToDetail" OnCommand="btnCommand">< ย้อนกลับ</asp:LinkButton>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- Back To Detail Car Booking -->


                <asp:GridView ID="GvDetail" runat="server"
                    AutoGenerateColumns="False"
                    CssClass="table table-striped table-bordered table-hover table-responsive"
                    OnPageIndexChanging="gvPageIndexChanging"
                    OnRowDataBound="gvRowDataBound"
                    ShowHeaderWhenEmpty="True"
                    AllowPaging="True"
                    PageSize="10"
                    BorderStyle="None"
                    CellSpacing="2">
                    <HeaderStyle CssClass="success" Height="40px" Font-Size="Small" />
                    <RowStyle Font-Size="Small" />
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">--- ไม่พบข้อมูล ---</div>
                    </EmptyDataTemplate>
                    <Columns>

                        <asp:TemplateField HeaderText="วันที่ทำรายการ" HeaderStyle-CssClass="text-center" ItemStyle-Width="15%" ItemStyle-CssClass="text-center">
                            <ItemTemplate>
                                <p>
                                    <asp:Label ID="lbl_create_date_detail" runat="server" Text='<%# Eval("create_date") %>'></asp:Label>
                                     
                                </p>
                                <p>
                                    <asp:Label ID="lbl_time_create_date_detail" runat="server" Text='<%# Eval("time_create_date") %>'></asp:Label>
                                </p>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="รายละเอียดผู้สร้าง" HeaderStyle-CssClass="text-center" ItemStyle-Width="25%" ItemStyle-CssClass="left">
                            <ItemTemplate>
                                <asp:Label ID="lbl_u0_document_idx_detail" runat="server" Visible="false" Text='<%# Eval("u0_document_idx") %>'></asp:Label>

                                <p>
                                    <b>ชื่อ-สกุลผู้สร้าง:</b>
                                    <asp:Label ID="lbl_cemp_idx_detail" Visible="false" runat="server" Text='<%# Eval("cemp_idx") %>' />
                                    <asp:Label ID="lbl_emp_name_th_detail" runat="server" Text='<%# Eval("emp_name_th") %>' />
                                </p>
                                <p>
                                    <b>องค์กร:</b>
                                    <asp:Label ID="lbl_org_name_th_detail" runat="server" Text='<%# Eval("org_name_th") %>' />
                                </p>
                                <p>
                                    <b>ฝ่าย:</b>
                                    <asp:Label ID="lbl_dept_name_th_detail" runat="server" Text='<%# Eval("dept_name_th") %>' />
                                </p>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="รายละเอียดการจอง" ItemStyle-Width="25%" HeaderStyle-CssClass="text-center">
                            <ItemTemplate>

                                <p>
                                    <b>ประเภทรถที่ใช้:</b>

                                    <asp:Label ID="lbl_car_use_name_detail" runat="server" Text='<%# Eval("car_use_name") %>' />
                                </p>
                                <p>
                                    <b>สถานที่:</b>
                                    <asp:Label ID="lbl_place_idx_detail" runat="server" Visible="false" Text='<%# Eval("place_idx") %>' />
                                    <asp:Label ID="lbl_place_name_detail" runat="server" Text='<%# Eval("place_name") %>' />
                                </p>

                                <p>
                                    <b>อื่นๆ:</b>
                                    <asp:Label ID="lbl_place_name_other" runat="server" Text='<%# Eval("place_name_other") %>' />
                                </p>

                                <p>
                                    <b>วันที่เดินทาง:</b>

                                    <asp:Label ID="lbl_date_start_detail" runat="server" Text='<%# Eval("date_start") %>' />
                                    <b>เวลา:</b>
                                    <asp:Label ID="lbl_time_start_detail" runat="server" Text='<%# Eval("time_start") %>' />
                                    <b>น.</b>
                                </p>
                                <p>
                                    <b>ถึงวันที่:</b>
                                    <asp:Label ID="lbl_date_end_detail" runat="server" Text='<%# Eval("date_end") %>' />
                                    <b>เวลา:</b>
                                    <asp:Label ID="lbl_time_end_detail" runat="server" Text='<%# Eval("time_end") %>' />
                                    <b>น.</b>
                                </p>


                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สถานะรายการ" HeaderStyle-CssClass="center" ItemStyle-Width="25%" ItemStyle-CssClass="text-center">

                            <HeaderTemplate>
                                <%-- <label class="col-sm-4 control-label">สถานะรายการ : </label>
                                <div class="col-sm-6">--%>

                                <div class="col-md-10 col-md-offset-1">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>สถานะรายการ</label>
                                            <asp:UpdatePanel ID="UpdatePanel_Header" runat="server">
                                                <ContentTemplate>
                                                    <small>
                                                        <asp:DropDownList ID="ddlStatusFilter" runat="server" CssClass="form-control" Font-Size="Small"
                                                            OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </small>

                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                </div>

                                <%-- </div>
                                <label class="col-sm-2 control-label"></label>--%>
                            </HeaderTemplate>

                            <ItemTemplate>

                                <asp:Label ID="lbl_staidx_detail" runat="server" Text='<%# Eval("staidx") %>' Visible="false" />
                                <asp:Label ID="lbl_status_name_detail" runat="server" Text='<%# Eval("status_name") %>' />
                                <asp:Label ID="lbl_m0_node_idx_detail" runat="server" Text='<%# Eval("m0_node_idx") %>' Visible="false" />
                                <asp:Label ID="lbl_node_name_detail" runat="server" Text='<%# Eval("node_name") %>' />
                                <br />
                                โดย<br />
                                <asp:Label ID="lbl_m0_actor_idx_detail" runat="server" Text='<%# Eval("m0_actor_idx") %>' Visible="false" />
                                <asp:Label ID="lbl_actor_name_detail" runat="server" Text='<%# Eval("actor_name") %>' />
                                <asp:Label ID="lbl_car_use_idx_detail" runat="server" Text='<%# Eval("car_use_idx") %>' Visible="false" />

                                <%--  <asp:Label ID="lbl_current_status" runat="server" Text='<%# Eval("current_status") %>'></asp:Label>--%>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-center">
                            <ItemTemplate>

                                <asp:UpdatePanel ID="Update_PnbtnViewDetail" runat="server">
                                    <ContentTemplate>
                                        <asp:LinkButton ID="btnViewDetail" CssClass="btn-sm btn-info" target="" runat="server" CommandName="cmdViewDetail" OnCommand="btnCommand"
                                            CommandArgument='<%# Eval("u0_document_idx")+ ";" + Eval("m0_node_idx") + ";" + Eval("m0_actor_idx") + ";" + Eval("staidx") + ";" + Eval("cemp_idx") + ";" + Eval("car_use_idx")%>'
                                            data-toggle="tooltip" title="ดูข้อมูล"><i class="fa fa-file"></i></asp:LinkButton>
                                    </ContentTemplate>

                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="btnViewDetail" />
                                    </Triggers>
                                </asp:UpdatePanel>

                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>


                <asp:FormView ID="FvDetailCarBooking" runat="server" OnDataBound="FvDetail_DataBound" Width="100%">
                    <ItemTemplate>
                        <asp:HiddenField ID="hfu0_document_idx" runat="server" Value='<%# Eval("u0_document_idx") %>' />
                        <asp:HiddenField ID="hfM0NodeIDX" runat="server" Value='<%# Eval("m0_node_idx") %>' />
                        <asp:HiddenField ID="hfM0ActoreIDX" runat="server" Value='<%# Eval("m0_actor_idx") %>' />
                        <asp:HiddenField ID="hfM0StatusIDX" runat="server" Value='<%# Eval("staidx") %>' />
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายละเอียดรายการจองรถ</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <%--<div class="col-md-12">--%>
                                        <asp:Label ID="lbl_emp_code" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="รหัสผู้ทำรายการ : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_u0_document_idx_view" Visible="false" CssClass="control-labelnotop col-xs-5" runat="server" Text='<%# Eval("u0_document_idx") %>' />
                                            <asp:Label ID="lbl_emp_code_view" CssClass="control-labelnotop col-xs-5" runat="server" Text='<%# Eval("emp_code") %>' />
                                            <asp:Label ID="lbl_cemp_idx_view" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("cemp_idx")%>' />
                                            <asp:Label ID="lbl_car_use_idx_view" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("car_use_idx")%>' />

                                        </div>

                                        <asp:Label ID="lbl_emp_name_th" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ชื่อผู้ทำรายการ : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_emp_name_th_view" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("emp_name_th") %>' />
                                        </div>
                                        <%-- </div>--%>
                                    </div>

                                    <div class="form-group">
                                        <%--<div class="col-md-12">--%>
                                        <asp:Label ID="Label4" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="บริษัท : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_org_name_th_view" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("org_name_th")%>' />
                                            <asp:Label ID="lbl_cemp_org_idx_view" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("org_idx")%>' />
                                        </div>

                                        <asp:Label ID="lbl_dept_name_th" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ฝ่าย : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_dept_name_th_view" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("dept_name_th")%>' />
                                            <asp:Label ID="lbl_cemp_rdept_idx_view" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("rdept_idx")%>' />
                                        </div>
                                        <%-- </div>--%>
                                    </div>

                                    <div class="form-group">
                                        <%--<div class="col-md-12">--%>
                                        <asp:Label ID="Label1" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="แผนก : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_sec_name_th_view" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("sec_name_th")%>' />
                                            <asp:Label ID="lbl_rsec_idx_view" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("rsec_idx")%>' />
                                        </div>

                                        <asp:Label ID="Label5" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ตำแหน่ง : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_pos_name_th_view" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("pos_name_th")%>' />
                                            <asp:Label ID="lbl_rpos_idx_view" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("rpos_idx")%>' />
                                        </div>
                                        <%-- </div>--%>
                                    </div>

                                    <div class="form-group">
                                        <%--<div class="col-md-12">--%>

                                        <asp:Label ID="Label7" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="E-mail : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_emp_email_view" runat="server" CssClass="control-labelnotop word-wrap col-xs-12" Text='<%# Eval("emp_email")%>' />

                                        </div>

                                        <asp:Label ID="Label2" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="Cost Center : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_costcenter_no_view" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("costcenter_no")%>' />
                                            <asp:Label ID="lbl_costcenter_idx_view" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("costcenter_idx")%>' />
                                        </div>


                                        <%-- </div>--%>
                                    </div>

                                    <div class="form-group">
                                        <%--<div class="col-md-12">--%>
                                        <asp:Label ID="Label3" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ประเภทรถ : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_detailtype_car_name_view" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("detailtype_car_name")%>' />
                                            <asp:Label ID="lbl_detailtype_car_idx_view" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("detailtype_car_idx")%>' />
                                        </div>

                                        <div class="col-xs-6 control-labelnotop text_right"></div>
                                        <%-- <asp:Label ID="Label9" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="E-mail : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="Label10" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("emp_email")%>' />

                                        </div>--%>
                                        <%-- </div>--%>
                                    </div>

                                    <div class="form-group">
                                        <%--<div class="col-md-12">--%>
                                        <asp:Label ID="Label6" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ประเภทการจอง : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_type_booking_name_view" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("type_booking_name")%>' />
                                            <asp:Label ID="lbl_type_booking_idx_view" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("type_booking_idx")%>' />
                                        </div>

                                        <asp:Label ID="Label10" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ประเภทการเดินทาง : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_type_travel_name_view" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("type_travel_name")%>' />
                                            <asp:Label ID="lbl_type_travel_idx_view" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("type_travel_idx")%>' />

                                        </div>
                                        <%-- </div>--%>
                                    </div>

                                    <div class="form-group">
                                        <%--<div class="col-md-12">--%>
                                        <asp:Label ID="Label8" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="สถานที่ : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_place_name" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("place_name")%>' />
                                            <asp:Label ID="lbl_place_idx_view" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("place_idx")%>' />
                                        </div>

                                        <asp:Label ID="Label12" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="อื่นๆ : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_place_name_other_view" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("place_name_other")%>' />


                                        </div>
                                        <%-- </div>--%>
                                    </div>

                                    <div class="form-group">
                                        <%--<div class="col-md-12">--%>
                                        <asp:Label ID="Label9" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ระยะทาง : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_distance_view" runat="server" CssClass="control-labelnotop col-xs-8" Text='<%# Eval("distance")%>' />
                                            <asp:Label ID="lbl_distance_view_unit" runat="server" CssClass="control-labelnotop col-xs-4" Text="กิโลเมตร" />
                                        </div>

                                        <asp:Label ID="Label14" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ระยะเวลา : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_time_length_view" runat="server" CssClass="control-labelnotop col-xs-8" Text='<%# Eval("time_length")%>' />
                                            <asp:Label ID="lbl_time_length_view_unit" runat="server" CssClass="control-labelnotop col-xs-4" Text="ชั่วโมง" />

                                        </div>
                                        <%-- </div>--%>
                                    </div>

                                    <div class="form-group">
                                        <%--<div class="col-md-12">--%>
                                        <asp:Label ID="Label11" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="หมายเหตุ : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_note_booking_view" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("note_booking")%>' />

                                        </div>
                                        <div class="col-xs-6 control-labelnotop text_right"></div>
                                        <%--  <asp:Label ID="Label15" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ระยะเวลา : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="Label16" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("time_length")%>' />


                                        </div>--%>
                                        <%-- </div>--%>
                                    </div>

                                    <div class="form-group">
                                        <%--<div class="col-md-12">--%>
                                        <asp:Label ID="Label13" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="วันที่เดินทาง : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_date_start_view" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("date_start")%>' />
                                            <asp:Label ID="lbl_date_time_chekbutton_view" runat="server" Visible="false" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("date_time_chekbutton")%>' />

                                        </div>

                                        <asp:Label ID="Label17" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="เวลา : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_time_start_view" runat="server" CssClass="control-labelnotop col-xs-8" Text='<%# Eval("time_start")%>' />
                                            <asp:Label ID="lbl_time_start_viewunit" runat="server" CssClass="control-labelnotop col-xs-4" Text="น." />

                                        </div>
                                        <%-- </div>--%>
                                    </div>

                                    <div class="form-group">
                                        <%--<div class="col-md-12">--%>
                                        <asp:Label ID="Label15" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ถึงวันที่ : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_date_end_view" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("date_end")%>' />

                                        </div>

                                        <asp:Label ID="Label18" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="เวลา : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_time_end_view" runat="server" CssClass="control-labelnotop col-xs-8" Text='<%# Eval("time_end")%>' />
                                            <asp:Label ID="lbl_time_end_view_unit" runat="server" CssClass="control-labelnotop col-xs-4" Text="น." />
                                        </div>
                                        <%-- </div>--%>
                                    </div>

                                    <div class="form-group">
                                        <%--<div class="col-md-12">--%>
                                        <asp:Label ID="Label16" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="วัตถุประสงค์ : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_objective_booking_view" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("objective_booking")%>' />

                                        </div>
                                        <div class="col-xs-6 control-labelnotop text_right"></div>
                                        <%--<asp:Label ID="Label20" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="เวลา : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="Label21" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("time_end")%>' />

                                        </div>--%>
                                        <%-- </div>--%>
                                    </div>

                                    <div class="form-group">
                                        <%--<div class="col-md-12">--%>
                                        <asp:Label ID="Label19" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="จำนวนผู้เดินทาง : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_count_travel_view" runat="server" CssClass="control-labelnotop col-xs-8" Text='<%# Eval("count_travel")%>' />
                                            <asp:Label ID="lbl_count_travel_view_unit" runat="server" CssClass="control-labelnotop col-xs-4" Text="คน" />

                                        </div>
                                        <asp:Label ID="Label20" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ประเภทรถที่ใช้ : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_car_use_name_view" runat="server" CssClass="control-labelnotop col-xs-8" Text='<%# Eval("car_use_name")%>' />
                                            <asp:Label ID="lbl_car_use_idx_" runat="server" Visible="false" CssClass="control-labelnotop col-xs-8" Text='<%# Eval("car_use_idx")%>' />
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <%--<div class="col-md-12">--%>
                                        <asp:Label ID="Label21" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="รายละเอียดไฟล์แนบ : " />
                                        <div class="col-xs-4">
                                            <asp:HyperLink runat="server" ID="btnViewFileCarDetailCreate" CssClass="f-s-13" Target="_blank"><i class="fa fa-file"></i> ดูรายละเอียดไฟล์แนบ</asp:HyperLink>

                                        </div>
                                        <asp:Label ID="Label22" CssClass="col-xs-6 control-labelnotop text_right" runat="server" Text="" />

                                    </div>

                                    <asp:UpdatePanel ID="Update_DetailCarInBooking" runat="server" Visible="false">
                                        <ContentTemplate>
                                            <%-- <div class="form-group">
                                            </div>--%>

                                            <div class="col-md-12">
                                                <div id="div_DetailCarInBooking" class="panel panel-default" runat="server">

                                                    <div class="panel-heading">
                                                        <h3 class="panel-title">รายละเอียดข้อมูลรถ</h3>
                                                    </div>
                                                    <div class="panel-body">

                                                        <asp:GridView ID="GvDetailCarInBooking" runat="server"
                                                            AutoGenerateColumns="False"
                                                            CssClass="table table-striped table-bordered table-hover table-responsive"
                                                            OnPageIndexChanging="gvPageIndexChanging"
                                                            OnRowDataBound="gvRowDataBound"
                                                            AllowPaging="True" PageSize="10">
                                                            <HeaderStyle CssClass="success" Height="40px" Font-Size="Small" />
                                                            <RowStyle Font-Size="Small" />
                                                            <PagerStyle CssClass="pageCustom" />
                                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                                            <EmptyDataTemplate>
                                                                <div style="text-align: center">--- ไม่พบข้อมูล ---</div>
                                                            </EmptyDataTemplate>
                                                            <Columns>

                                                                <asp:TemplateField HeaderText="รูปรถ" HeaderStyle-CssClass="text-center" ItemStyle-Width="25%" ItemStyle-CssClass="text-center">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_m0_car_idx_viewdetail" runat="server" Visible="false" Text='<%# Eval("m0_car_idx") %>'></asp:Label>
                                                                        <asp:HyperLink runat="server" ID="btnViewFileCar" CssClass="pull-letf" data-toggle="tooltip"
                                                                            title="ดูรูปรถ" data-original-title="" Target="_blank">
                                                                                <i class="fa fa-file"></i> ดูรูปรถ</asp:HyperLink>

                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="ประเภทรถ" HeaderStyle-CssClass="text-center" ItemStyle-Width="15%" ItemStyle-CssClass="text-center">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_detailtype_car_name" runat="server" Text='<%# Eval("detailtype_car_name") %>'></asp:Label>
                                                                        <p>
                                                                            <asp:Label ID="lbl_car_use_name" Visible="false" runat="server" Text='<%# Eval("car_use_name") %>'></asp:Label>
                                                                        </p>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>


                                                                <asp:TemplateField HeaderText="ทะเบียนรถ" HeaderStyle-CssClass="text-center" ItemStyle-Width="30%" ItemStyle-CssClass="text-center">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_car_register" runat="server" Text='<%# Eval("car_register") %>'></asp:Label>

                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="ชื่อผู้ขับ" HeaderStyle-CssClass="text-center" ItemStyle-Width="30%" ItemStyle-CssClass="text-center">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_emp_name_th_driver" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>

                                                                    </ItemTemplate>
                                                                </asp:TemplateField>


                                                            </Columns>
                                                        </asp:GridView>

                                                    </div>
                                                </div>
                                            </div>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>


                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:FormView>

                <asp:UpdatePanel ID="UpDate_InsertCarUseHR" runat="server" Visible="false">
                    <ContentTemplate>
                        <div class="form-group">

                            <asp:LinkButton ID="btnInsertCarUseHr" CssClass="btn btn-success" data-toggle="tooltip" title="กรอกข้อมูลการใช้รถ" runat="server"
                                CommandName="cmdInsertCarUseHr" OnCommand="btnCommand"><i class="fa fa-plus-square" aria-hidden="true"></i> กรอกข้อมูลการใช้รถ</asp:LinkButton>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnInsertCarUseHr" />
                    </Triggers>
                </asp:UpdatePanel>


                <!-- Add Use Car By HR -->
                <asp:UpdatePanel ID="Panel_AddCarUseHr" runat="server">
                    <ContentTemplate>

                        <asp:UpdatePanel ID="Update_PnBackToDetailHRAddUseCar" runat="server" Visible="false">
                            <ContentTemplate>
                                <div class="form-group">

                                    <asp:LinkButton ID="btnBackToHrAddUseCar" CssClass="btn btn-danger" data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                                        CommandName="cmdBackToHrAddUseCar" OnCommand="btnCommand">< ย้อนกลับ</asp:LinkButton>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">บันทึกข้อมูลการใช้รถ</h3>
                            </div>
                            <div class="panel-body">

                                <asp:Label ID="lbl_u0_document_idx_caruse" runat="server" Visible="false"></asp:Label>
                                <asp:Label ID="lbl_node_idx_caruse" runat="server" Visible="false"></asp:Label>
                                <asp:Label ID="lbl_actor_idx_caruse" runat="server" Visible="false"></asp:Label>
                                <asp:Label ID="lbl_status_idx_caruse" runat="server" Visible="false"></asp:Label>



                                <%-- <div id="divGvDetailCarUse_scroll" style="overflow-x: scroll; width: 100%" runat="server">--%>
                                <asp:GridView ID="GvDetailCarUse" runat="server"
                                    AutoGenerateColumns="False"
                                    CssClass="table table-striped table-bordered table-hover table-responsive"
                                    OnPageIndexChanging="gvPageIndexChanging"
                                    OnRowDataBound="gvRowDataBound">
                                    <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                                    <RowStyle Font-Size="Small" />
                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                    <EmptyDataTemplate>
                                        <div style="text-align: center">--- ไม่พบข้อมูล ---</div>
                                    </EmptyDataTemplate>
                                    <Columns>

                                        <asp:TemplateField HeaderText="เลขทะเบียน" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_u2_document_idx_hr_add" runat="server" Visible="false" Text='<%# Eval("u2_document_idx") %>'></asp:Label>
                                                <asp:Label ID="lbl_m0_car_idx_hr_add" runat="server" Visible="false" Text='<%# Eval("m0_car_idx") %>'></asp:Label>
                                                <asp:Label ID="lbl_car_register_hr_add" runat="server" Text='<%# Eval("car_register") %>'></asp:Label>


                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--OnSelectedIndexChanged="onSelectedIndexChanged" AutoPostBack="true"--%>
                                        <asp:TemplateField HeaderText="เลือกการใช้งานรถ" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="left">
                                            <ItemTemplate>
                                                <asp:UpdatePanel ID="Update_rdoStatusUseViewDetail" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Label ID="Check_Count_List" Visible="false" runat="server"></asp:Label>
                                                        <asp:RadioButtonList ID="rdoStatusUse" runat="server" OnSelectedIndexChanged="onSelectedIndexChanged" AutoPostBack="true">
                                                            <asp:ListItem Value="1">&nbsp;ใช้รถ</asp:ListItem>
                                                            <asp:ListItem Value="2">&nbsp;ไม่ใช้รถ</asp:ListItem>
                                                        </asp:RadioButtonList>

                                                        <asp:RequiredFieldValidator ID="Re_rdoStatusUse"
                                                            runat="server"
                                                            ControlToValidate="rdoStatusUse" Display="None" SetFocusOnError="true"
                                                            ErrorMessage="*กรุณาเลือกการใช้งานรถ"
                                                            ValidationGroup="SaveCarUseHr" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_rdoStatusUse" Width="200" PopupPosition="BottomLeft" />
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="เลขไมล์รถเริ่มต้น - สิ้นสุด" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="left">
                                            <ItemTemplate>
                                                <p>
                                                    <b>เลขไมล์รถเริ่มต้น :</b>
                                                    <asp:TextBox ID="txt_mile_start" runat="server" CssClass="form-control" placeholder="กรอกเลขไมล์เริ่มต้น ..." />
                                                    <%-- <asp:RequiredFieldValidator ID="Re_txt_mile_start"
                                                                runat="server"
                                                                ControlToValidate="txt_mile_start" Display="None" SetFocusOnError="true"
                                                                ErrorMessage="*กรุณากรอกเลขไมล์รถเริ่มต้น"
                                                                ValidationGroup="SaveCarUseHr" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender52222" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_mile_start" Width="200" PopupPosition="BottomLeft" />--%>
                                                </p>
                                                <p>
                                                    <b>เลขไมล์รถสิ้นสุด :</b>
                                                    <asp:TextBox ID="txt_mile_end" runat="server" CssClass="form-control" placeholder="กรอกเลขไมล์สิ้นสุด ..." />
                                                    <%--<asp:RequiredFieldValidator ID="Required_txt_mile_end"
                                                                runat="server"
                                                                ControlToValidate="txt_mile_end" Display="None" SetFocusOnError="true"
                                                                ErrorMessage="*กรุณากรอกเลขไมล์รถสิ้นสุด"
                                                                ValidationGroup="SaveCarUseHr" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender15222" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Required_txt_mile_end" Width="200" PopupPosition="BottomLeft" />--%>
                                                </p>


                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ค่าใช้จ่าย" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                            <ItemTemplate>

                                                <asp:UpdatePanel ID="Panel_AddCarUseHr" runat="server">
                                                    <ContentTemplate>

                                                        <asp:GridView ID="GvExpenses" runat="server"
                                                            AutoGenerateColumns="false"
                                                            CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                                            HeaderStyle-CssClass="success small"
                                                            HeaderStyle-Height="40px"
                                                            AllowPaging="false"
                                                            OnRowDataBound="gvRowDataBound">

                                                            <PagerStyle CssClass="pageCustom" />
                                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                            <EmptyDataTemplate>
                                                                <div style="text-align: center">--- ไม่พบข้อมูล ---</div>
                                                            </EmptyDataTemplate>
                                                            <Columns>

                                                                <asp:TemplateField HeaderText="รายละเอียดค่าใช้จ่าย" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">

                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblexpenses_idx" runat="server" Visible="false" Text='<%# Eval("expenses_idx") %>' />
                                                                        <asp:Label ID="lblexpenses_name" runat="server" Text='<%# Eval("expenses_name") %>' />

                                                                    </ItemTemplate>

                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="มูลค่า" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                                    <ItemTemplate>

                                                                        <asp:TextBox ID="txt_value_caruse" runat="server" CssClass="form-control" placeholder="กรอกมูลค่า ..." />

                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                            </Columns>
                                                        </asp:GridView>

                                                    </ContentTemplate>
                                                </asp:UpdatePanel>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="หมายเหตุ" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="left">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txt_note_caruse" runat="server" CssClass="form-control" TextMode="multiline" Rows="2" placeholder="หมายเหตุ ..." />

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="แนบไฟล์" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="left">

                                            <ItemTemplate>

                                                <asp:FileUpload ID="UploadFile_UseCar" ClientIDMode="Static" runat="server" accept="jpg|png" />
                                                <p class="help-block"><font color="red">**เฉพาะนามสกุล jpg, png</font></p>

                                            </ItemTemplate>

                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>

                                <%-- </div>--%>
                                <div class="form-group">
                                    <label>เพิ่มเติม</label>
                                    <asp:RadioButtonList ID="rdouse_outplan" runat="server" CssClass="radio-list-inline-emps" AutoPostBack="true" RepeatDirection="Vertical"
                                        OnSelectedIndexChanged="onSelectedIndexChanged">
                                        <%-- <asp:ListItem Value="1"> ใช้ระยะทางเกินกว่ากำหนด</asp:ListItem>
                                        <asp:ListItem Value="2"> เพิ่มเส้นทางรถจากเส้นทางเดิม</asp:ListItem>--%>
                                    </asp:RadioButtonList>

                                </div>

                                <asp:UpdatePanel ID="updateUseDistanceOutInsert" runat="server">
                                    <ContentTemplate>

                                        <div id="div_DetailInsertdistance" class="panel panel-default" runat="server">
                                            <div class="panel-body">
                                                <div class="col-md-10 col-md-offset-1">

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>องค์กร</label>
                                                            <asp:DropDownList ID="ddlorg_idx" runat="server" ValidationGroup="addOutplandistanceTolist" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" />
                                                            <asp:RequiredFieldValidator ID="Reqddlorg_idx"
                                                                runat="server"
                                                                InitialValue="0"
                                                                ControlToValidate="ddlorg_idx" Display="None" SetFocusOnError="true"
                                                                ErrorMessage="*กรุณาเลือกองค์กร"
                                                                ValidationGroup="addOutplandistanceTolist" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender19" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Reqddlorg_idx" Width="250" />
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>ฝ่าย</label>
                                                            <asp:DropDownList ID="ddlrdept_idx" runat="server" CssClass="form-control" ValidationGroup="addOutplandistanceTolist" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" />
                                                            <asp:RequiredFieldValidator ID="Reqddlrdept_idx"
                                                                runat="server"
                                                                InitialValue="0"
                                                                ControlToValidate="ddlrdept_idx" Display="None" SetFocusOnError="true"
                                                                ErrorMessage="*กรุณาเลือกฝ่าย"
                                                                ValidationGroup="addOutplandistanceTolist" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender17221" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Reqddlrdept_idx" Width="250" />
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>แผนก</label>
                                                            <asp:DropDownList ID="ddlrsec_idx" runat="server" CssClass="form-control" ValidationGroup="addOutplandistanceTolist" />
                                                            <asp:RequiredFieldValidator ID="Reqddlrsec_idx"
                                                                runat="server"
                                                                InitialValue="0"
                                                                ControlToValidate="ddlrsec_idx" Display="None" SetFocusOnError="true"
                                                                ErrorMessage="*กรุณาเลือกแผนก"
                                                                ValidationGroup="addOutplandistanceTolist" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1722" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Reqddlrsec_idx" Width="250" />
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>ระยะทาง</label>
                                                            <asp:TextBox ID="txt_Distance" runat="server" CssClass="form-control" placeholder="ระยะทาง ..." />
                                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                                runat="server"
                                                                InitialValue="0"
                                                                ControlToValidate="ddl_Placeidx" Display="None" SetFocusOnError="true"
                                                                ErrorMessage="*กรุณาเลือกสถานที่"
                                                                ValidationGroup="addPlacePerList" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender17" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldDdl_Placeidx" Width="250" />--%>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>ค่าใช้จ่าย</label>
                                                            <asp:TextBox ID="txt_expenses_outplan" runat="server" CssClass="form-control" TextMode="multiline" Rows="2" placeholder="ค่าใช้จ่าย ..." />
                                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                                runat="server"
                                                                InitialValue="0"
                                                                ControlToValidate="ddl_Placeidx" Display="None" SetFocusOnError="true"
                                                                ErrorMessage="*กรุณาเลือกสถานที่"
                                                                ValidationGroup="addPlacePerList" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender17" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldDdl_Placeidx" Width="250" />--%>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>รายละเอียดเพิ่มเติม</label>
                                                            <asp:TextBox ID="txt_Detailoutplan" runat="server" CssClass="form-control" TextMode="multiline" Rows="2" placeholder="รายละเอียดเพิ่มเติม ..." />
                                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                                runat="server"
                                                                InitialValue="0"
                                                                ControlToValidate="ddl_Placeidx" Display="None" SetFocusOnError="true"
                                                                ErrorMessage="*กรุณาเลือกสถานที่"
                                                                ValidationGroup="addPlacePerList" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender17" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldDdl_Placeidx" Width="250" />--%>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <%--<label>&nbsp;</label>
                                                            <div class="clearfix"></div>--%>
                                                            <asp:LinkButton ID="btnAddOutplandistanceTolist" runat="server"
                                                                CssClass="btn btn-primary col-md-12"
                                                                Text="เพิ่มข้อมูล" OnCommand="btnCommand" CommandName="cmdAddOutplandistanceTolist"
                                                                ValidationGroup="addOutplandistanceTolist" />
                                                            <div class="clearfix"></div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <asp:GridView ID="gvDetailOutplanList"
                                                            runat="server"
                                                            CssClass="table table-striped table-responsive"
                                                            GridLines="None"
                                                            OnRowCommand="onRowCommand"
                                                            AutoGenerateColumns="false">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center"
                                                                    HeaderStyle-CssClass="text-center"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller">
                                                                    <ItemTemplate>
                                                                        <%# (Container.DataItemIndex + 1) %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:BoundField DataField="drOutplanOrgText" HeaderText="องค์กร" ItemStyle-CssClass="left"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                                <asp:BoundField DataField="drOutplanRdeptText" HeaderText="ฝ่าย" ItemStyle-CssClass="left"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                                <asp:BoundField DataField="drOutplanRsecText" HeaderText="แผนก" ItemStyle-CssClass="left"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                                <asp:BoundField DataField="drOutplanDistanceText" HeaderText="ระยะทาง" ItemStyle-CssClass="left"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                                <asp:BoundField DataField="drOutplanExpensesText" HeaderText="ค่าใช้จ่าย" ItemStyle-CssClass="left"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                                <asp:BoundField DataField="drOutplanDetailText" HeaderText="รายละเอียดเพิ่มเติม" ItemStyle-CssClass="left"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                                <asp:TemplateField ItemStyle-HorizontalAlign="center"
                                                                    HeaderStyle-CssClass="text-center">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="btnRemove" runat="server"
                                                                            CssClass="btn btn-danger btn-xs"
                                                                            OnClientClick="return confirm('คุณต้องการลบรายการนี้ใชหรือไม่ ?')"
                                                                            CommandName="btnRemove"><i class="fa fa-times"></i>
                                                                        </asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>

                                    </ContentTemplate>
                                </asp:UpdatePanel>

                                <asp:UpdatePanel ID="Update_SaveCarUseHr" runat="server">
                                    <ContentTemplate>

                                        <label>&nbsp;</label>
                                        <div class="clearfix"></div>
                                        <div class="form-group pull-right">

                                            <asp:LinkButton ID="btnSaveCarUseHr" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="บันทึกการใช้รถ" OnCommand="btnCommand" CommandName="cmdSaveCarUseHr" ValidationGroup="SaveCarUseHr"></asp:LinkButton>

                                            <asp:LinkButton ID="btnCancelCarUseHr" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="ยกเลิก" OnCommand="btnCommand" CommandName="cmdBackToCarUseHr"></asp:LinkButton>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="btnSaveCarUseHr" />
                                    </Triggers>

                                </asp:UpdatePanel>

                                <asp:UpdatePanel ID="Update_SaveCarUseHrOut" runat="server">
                                    <ContentTemplate>
                                        <div class="panel-body">
                                            <div class="col-md-8 col-md-offset-4">

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>แนบไฟล์</label>
                                                        <asp:FileUpload ID="UploadFile_UseCarOut" CssClass="btn btn-warning" ValidationGroup="SaveCarUseHrOut" ClientIDMode="Static" runat="server" accept="jpg|png" />
                                                        <p class="help-block"><font color="red">**เฉพาะนามสกุล jpg, png</font></p>


                                                        <asp:RequiredFieldValidator ID="required_UploadFile_UseCarOut"
                                                            runat="server" ValidationGroup="SaveCarUseHrOut"
                                                            Display="None"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="UploadFile_UseCarOut"
                                                            Font-Size="13px" ForeColor="Red"
                                                            ErrorMessage="กรุณาเลือกไฟล์" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="toolkitRequiredTxtEmpVisitorReason" runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                            TargetControlID="required_UploadFile_UseCarOut" Width="200" PopupPosition="BottomRight" />


                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <asp:LinkButton ID="btnSaveCarUseHrOut" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="บันทึกการใช้รถ" OnCommand="btnCommand" CommandName="cmdSaveCarUseHrOut" ValidationGroup="SaveCarUseHrOut"></asp:LinkButton>

                                                        <asp:LinkButton ID="btnBackToCarUseHrOut" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="ยกเลิก" OnCommand="btnCommand" CommandName="cmdBackToCarUseHr"></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </ContentTemplate>

                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="btnSaveCarUseHrOut" />
                                    </Triggers>
                                </asp:UpdatePanel>

                            </div>
                        </div>

                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- Add Use Car By HR -->

                <asp:UpdatePanel ID="UpdatePanel_ShowDetailCarUse" runat="server">
                    <ContentTemplate>
                        <div class="form-group">

                            <asp:LinkButton ID="btnViewDetailUseCar" CssClass="btn btn-success" data-toggle="tooltip" title="ดูรายละเอียดการใช้รถ" runat="server"
                                CommandName="cmdViewDetailUseCar" OnCommand="btnCommand"><i class="fa fa-list-ul" aria-hidden="true"></i> ดูรายละเอียดการใช้รถ</asp:LinkButton>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>

                <!-- Show Detail Use Car By HR -->
                <asp:UpdatePanel ID="Panel_ShowDetailCarUse" runat="server">
                    <ContentTemplate>

                        <asp:UpdatePanel ID="Update_ideDatailUseCar" runat="server">
                            <ContentTemplate>
                                <div class="form-group">

                                    <asp:LinkButton ID="btnHideDatailUseCar" CssClass="btn btn-danger" data-toggle="tooltip" title="ซ่อนดูรายละเอียดการใช้รถ" runat="server"
                                        CommandName="cmdHideDatailUseCar" OnCommand="btnCommand">< ซ่อนดูรายละเอียดการใช้รถ</asp:LinkButton>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>


                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายละเอียดข้อมูลการใช้รถ</h3>
                            </div>
                            <div class="panel-body">

                                <asp:Label ID="lbl_u0_document_idx_caruse_detail" runat="server" Visible="false"></asp:Label>
                                <asp:Label ID="lbl_node_idx_caruse_detail" runat="server" Visible="false"></asp:Label>
                                <asp:Label ID="lbl_actor_idx_caruse_detail" runat="server" Visible="false"></asp:Label>
                                <asp:Label ID="lbl_status_idx_caruse_detail" runat="server" Visible="false"></asp:Label>

                                <asp:UpdatePanel ID="Update_ShowDetailCarUse" runat="server">
                                    <ContentTemplate>

                                        <%-- <div id="divGvDetailCarUse_scroll" style="overflow-x: scroll; width: 100%" runat="server">--%>
                                        <asp:GridView ID="GvShowDetailCarUse" runat="server"
                                            AutoGenerateColumns="False"
                                            CssClass="table table-striped table-bordered table-hover table-responsive"
                                            OnPageIndexChanging="gvPageIndexChanging"
                                            OnRowDataBound="gvRowDataBound"
                                            AllowPaging="True" PageSize="10">
                                            <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                                            <RowStyle Font-Size="Small" />
                                            <PagerStyle CssClass="pageCustom" />
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                            <EmptyDataTemplate>
                                                <div style="text-align: center">--- ไม่พบข้อมูล ---</div>
                                            </EmptyDataTemplate>
                                            <Columns>

                                                <asp:TemplateField HeaderText="เลขทะเบียน" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_u0_document_idx_viewcaruse" runat="server" Visible="false" Text='<%# Eval("u0_document_idx") %>'></asp:Label>
                                                        <asp:Label ID="lbl_u2_document_idx_viewcaruse" runat="server" Visible="false" Text='<%# Eval("u2_document_idx") %>'></asp:Label>
                                                        <asp:Label ID="lbl_m0_car_idx_viewcaruse" runat="server" Visible="false" Text='<%# Eval("m0_car_idx") %>'></asp:Label>
                                                        <asp:Label ID="lbl_car_register_viewcaruse" runat="server" Text='<%# Eval("car_register") %>'></asp:Label>
                                                        <p>
                                                            <asp:Label ID="lbl_detailtype_car_name_detail" runat="server" Text='<%# Eval("detailtype_car_name") %>'></asp:Label>
                                                        </p>

                                                        <%--<p>
                                                    <asp:Label ID="lbl_create_date_detail" runat="server" Text='<%# Eval("create_date") %>'></asp:Label>
                                                </p>
                                                <p>
                                                    <asp:Label ID="lbl_time_create_date_detail" runat="server" Text='<%# Eval("time_create_date") %>'></asp:Label> OnSelectedIndexChanged="onSelectedIndexChanged" AutoPostBack="true"
                                                </p>--%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="เลือกการใช้งาน" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="left">
                                                    <ItemTemplate>

                                                        <asp:Label ID="lbl_car_use_status_idx" runat="server" Visible="false" Text='<%# Eval("car_use_status") %>'></asp:Label>

                                                        <asp:RadioButtonList ID="rdoStatusUseViewDetail" runat="server" Enabled="false">
                                                            <asp:ListItem Value="1">&nbsp;ใช้รถ</asp:ListItem>
                                                            <asp:ListItem Value="2">&nbsp;ไม่ใช้รถ</asp:ListItem>
                                                        </asp:RadioButtonList>



                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="เลขไมล์รถเริ่มต้น - สิ้นสุด" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="left">
                                                    <ItemTemplate>
                                                        <p>
                                                            <b>เลขไมล์รถเริ่มต้น :</b>
                                                            <asp:TextBox ID="txt_mile_start" runat="server" CssClass="form-control" Enabled="false" Text='<%# Eval("miles_start") %>' placeholder="กรอกเลขไมล์เริ่มต้น ..." />
                                                        </p>
                                                        <p>
                                                            <b>เลขไมล์รถสิ้นสุด :</b>
                                                            <asp:TextBox ID="txt_mile_end" runat="server" CssClass="form-control" Enabled="false" Text='<%# Eval("miles_end") %>' placeholder="กรอกเลขไมล์สิ้นสุด ..." />
                                                        </p>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="ค่าใช้จ่าย" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                    <ItemTemplate>

                                                        <asp:UpdatePanel ID="Panel_ShowDetailCarUse" runat="server">
                                                            <ContentTemplate>

                                                                <asp:GridView ID="GvShowDetailExpenses" runat="server"
                                                                    AutoGenerateColumns="false"
                                                                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                                                    HeaderStyle-CssClass="success small"
                                                                    HeaderStyle-Height="40px"
                                                                    AllowPaging="false"
                                                                    OnRowDataBound="gvRowDataBound"
                                                                    ShowFooter="true">

                                                                    <PagerStyle CssClass="pageCustom" />
                                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                                    <EmptyDataTemplate>
                                                                        <div style="text-align: center">--- ไม่พบข้อมูล ---</div>
                                                                    </EmptyDataTemplate>
                                                                    <Columns>

                                                                        <asp:TemplateField HeaderText="รายละเอียดค่าใช้จ่าย" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">

                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblexpenses_idx" runat="server" Visible="false" Text='<%# Eval("expenses_idx") %>' />
                                                                                <asp:Label ID="lblexpenses_name" runat="server" Text='<%# Eval("expenses_name") %>' />

                                                                            </ItemTemplate>

                                                                            <FooterTemplate>
                                                                                <div style="text-align: right;">
                                                                                    <asp:Literal ID="lit_total_expenses" runat="server" Text="รวม :"></asp:Literal>
                                                                                </div>
                                                                            </FooterTemplate>

                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="มูลค่า" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                                            <ItemTemplate>

                                                                                <asp:TextBox ID="txt_value_caruse" runat="server" CssClass="form-control" Enabled="false" Text='<%# Eval("count_expenses") %>' />

                                                                            </ItemTemplate>

                                                                            <FooterTemplate>
                                                                                <div style="text-align: right;">
                                                                                    <asp:Label ID="lit_value_expenses" runat="server"></asp:Label>
                                                                                </div>
                                                                            </FooterTemplate>



                                                                        </asp:TemplateField>

                                                                    </Columns>
                                                                </asp:GridView>

                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="หมายเหตุ" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="left">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txt_note_caruse" runat="server" CssClass="form-control" Enabled="false" Text='<%# Eval("note_detail_caruse") %>' TextMode="multiline" Rows="2" placeholder="หมายเหตุ ..." />

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="แนบไฟล์" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="left">

                                                    <ItemTemplate>
                                                        <asp:HyperLink runat="server" ID="btnViewFileUseCar" CssClass="pull-letf" data-toggle="tooltip" title="view" data-original-title="" Target="_blank"><i class="fa fa-file"></i> ดูรายละเอียด</asp:HyperLink>


                                                    </ItemTemplate>

                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>

                                        <div class="form-group">
                                            <label>เพิ่มเติม</label>
                                            <asp:RadioButtonList ID="rdouse_outplandetail" runat="server" Enabled="false" CssClass="radio-list-inline-emps" AutoPostBack="true" RepeatDirection="Vertical"
                                                OnSelectedIndexChanged="onSelectedIndexChanged">
                                                <%--<asp:ListItem Value="1"> ใช้ระยะทางเกินกว่ากำหนด</asp:ListItem>
                                                <asp:ListItem Value="2"> เพิ่มเส้นทางรถจากเส้นทางเดิม</asp:ListItem>--%>
                                            </asp:RadioButtonList>

                                        </div>

                                        <asp:GridView ID="GvDetailOutplan" runat="server"
                                            AutoGenerateColumns="False"
                                            CssClass="table table-striped table-bordered table-hover table-responsive"
                                            OnPageIndexChanging="gvPageIndexChanging"
                                            OnRowDataBound="gvRowDataBound"
                                            AllowPaging="True" PageSize="10">
                                            <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                                            <RowStyle Font-Size="Small" />
                                            <PagerStyle CssClass="pageCustom" />
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                            <EmptyDataTemplate>
                                                <div style="text-align: center">--- ไม่พบข้อมูล ---</div>
                                            </EmptyDataTemplate>
                                            <Columns>

                                                <asp:TemplateField HeaderText="ลำดับ" ItemStyle-HorizontalAlign="center"
                                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" HeaderStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <small>
                                                            <%# (Container.DataItemIndex +1) %>
                                                        </small>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="รายละเอียดข้อมูล" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="left">
                                                    <ItemTemplate>
                                                        <p>
                                                            <b>องค์กร :</b>
                                                            <asp:Label ID="lbl_org_name_th_detailoutplan" runat="server" Text='<%# Eval("org_name_th") %>' />
                                                            <%--<asp:TextBox ID="txt_org_name_th_detailoutplan" runat="server" CssClass="form-control" Enabled="false" Text='<%# Eval("org_name_th") %>' placeholder="องค์กร ..." />--%>
                                                        </p>
                                                        <p>
                                                            <b>ฝ่าย :</b>
                                                            <asp:Label ID="lbl_dept_name_th_detailoutplan" runat="server" Text='<%# Eval("dept_name_th") %>' />
                                                            <%--<asp:TextBox ID="txt_dept_name_th_detailoutplan" runat="server" CssClass="form-control" Enabled="false" Text='<%# Eval("dept_name_th") %>' placeholder="ฝ่าย ..." />--%>
                                                        </p>
                                                        <p>
                                                            <b>แผนก :</b>
                                                            <asp:Label ID="lbl_sec_name_th_detailoutplan" runat="server" Text='<%# Eval("sec_name_th") %>' />
                                                            <%--<asp:TextBox ID="txt_sec_name_th_detailoutplan" runat="server" CssClass="form-control" Enabled="false" Text='<%# Eval("sec_name_th") %>' placeholder="แผนก ..." />--%>
                                                        </p>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="ระยะทาง(กิโลเมตร)" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_distance_outplan" runat="server" Text='<%# Eval("distance_outplan") %>' />
                                                        <%-- <asp:TextBox ID="txt_distance_outplan" runat="server" CssClass="form-control" Enabled="false" Text='<%# Eval("distance_outplan") %>' TextMode="multiline" Rows="2" placeholder="ระยะทาง ..." />--%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="ค่าใช้จ่าย(บาท)" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_expenses_outplan" runat="server" Text='<%# Eval("expenses_outplan") %>' />
                                                        <%--<asp:TextBox ID="txt_expenses_outplan" runat="server" CssClass="form-control" Enabled="false" Text='<%# Eval("expenses_outplan") %>' TextMode="multiline" Rows="2" placeholder="ค่าใช้จ่าย ..." />--%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="รายละเอียดเพิ่มเติม" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_detail_outplan" runat="server" Text='<%# Eval("detail_outplan") %>' />
                                                        <%--<asp:TextBox ID="txt_note_caruse" runat="server" CssClass="form-control" Enabled="false" Text='<%# Eval("detail_outplan") %>' TextMode="multiline" Rows="2" placeholder="รายละเอียดเพิ่มเติม ..." />--%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                            </Columns>
                                        </asp:GridView>



                                    </ContentTemplate>

                                </asp:UpdatePanel>





                                <asp:UpdatePanel ID="Update_ShowDetailCarUseOut" runat="server">
                                    <ContentTemplate>
                                        <div class="col-md-9 col-md-offset-3">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>ไฟล์แนบเพิ่มเติม</label>
                                                    <asp:HyperLink runat="server" ID="btnViewCarDetailOut" CssClass="btn btn-success" data-original-title="ดูรายละเอียด" data-toggle="tooltip" Target="_blank"><i class="fa fa-file"></i> ดูรายละเอียด</asp:HyperLink>
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            </div>
                        </div>

                    </ContentTemplate>

                </asp:UpdatePanel>
                <!-- Show Detail Use Car By HR -->

                <asp:UpdatePanel ID="Update_CancelCarDetail" runat="server">
                    <ContentTemplate>
                        <div class="form-group">

                            <asp:LinkButton ID="btnCancelCarDetail" CssClass="btn btn-danger" data-toggle="tooltip" title="ยกเลิกรายการจองรถ" runat="server" CommandName="cmdCancelCarDetail" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-close" aria-hidden="true"></i> ยกเลิกรายการจองรถ</asp:LinkButton>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>

                <!-- Log Detail Car Booking -->
                <div class="panel panel-info" id="div_LogViewDetail" runat="server" visible="false">
                    <div class="panel-heading">ประวัติการดำเนินการ</div>
                    <table class="table table-striped f-s-12 table-empshift-responsive">
                        <asp:Repeater ID="rptLogCarBooking" runat="server">
                            <HeaderTemplate>
                                <tr>
                                    <th>วัน / เวลา</th>
                                    <th>ผู้ดำเนินการ</th>
                                    <th>ดำเนินการ</th>
                                    <%--<th>ผลการดำเนินการ</th>--%>
                                    <th>เหตุผล</th>
                                </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td data-th="วัน / เวลา"><%# Eval("create_date") %> <%# Eval("time_create") %></td>
                                    <td data-th="ผู้ดำเนินการ"><%# Eval("emp_name_th") %> ( <%# Eval("current_artor") %> )</td>
                                    <td data-th="ดำเนินการ"><%# Eval("current_decision") %></td>
                                    <td data-th="เหตุผล"><%# Eval("comment") %></td>
                                    <%--<td data-th="ผลการดำเนินการ"><%# Eval("current_status_name") %></td>--%>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                    <div class="m-t-10 m-b-10"></div>
                </div>
                <!-- Log Detail Car Booking -->

            </div>

        </asp:View>
        <!--View Detail-->

        <!--View Create-->
        <asp:View ID="docCreate" runat="server">
            <div class="col-md-12">
                <!-- Form Detail Employee Create -->
                <asp:FormView ID="fvEmpDetail" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <ItemTemplate>

                        <asp:HiddenField ID="hfEmpOrgIDX" runat="server" Value='<%# Eval("org_idx") %>' />
                        <asp:HiddenField ID="hfEmpRdeptIDX" runat="server" Value='<%# Eval("rdept_idx") %>' />
                        <asp:HiddenField ID="hfEmpRsecIDX" runat="server" Value='<%# Eval("rsec_idx") %>' />
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายละเอียดผู้ใช้งาน</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">รหัสพนักงาน</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpCode" runat="server" CssClass="form-control" Text='<%# Eval("emp_code") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ชื่อ - นามสกุล</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpName" runat="server" CssClass="form-control" Text='<%# Eval("emp_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">องค์กร</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpOrg" runat="server" CssClass="form-control" Text='<%# Eval("org_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ฝ่าย</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpDept" runat="server" CssClass="form-control" Text='<%# Eval("dept_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">แผนก</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpSec" runat="server" CssClass="form-control" Text='<%# Eval("sec_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ตำแหน่ง</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpPos" runat="server" CssClass="form-control" Text='<%# Eval("pos_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </ItemTemplate>
                </asp:FormView>
                <!-- Form Detail Employee Create -->

                <!-- Form Create -->
                <asp:FormView ID="FvCreateCarBooking" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <InsertItemTemplate>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">สร้างรายการจองรถ</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">ประเภทรถ</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlDetailtypecar" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="Re_ddlDetailtypecar"
                                                runat="server"
                                                InitialValue="0"
                                                ControlToValidate="ddlDetailtypecar" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกประเภทรถ"
                                                ValidationGroup="SaveIndex" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlDetailtypecar" Width="200" PopupPosition="BottomLeft" />
                                        </div>
                                        <%--<label class="col-sm-1 control-label" style="color: Red;">***</label>--%>
                                        <%--<div class="col-sm-1">
                                            <span id="" style="color: Red;">***</span>
                                        </div>--%>
                                        <label class="col-sm-6 control-label"></label>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">วันที่เดินทาง</label>
                                        <div class="col-sm-4">
                                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                <ContentTemplate>
                                                    <div class="input-group date">

                                                        <asp:TextBox ID="txtDateStart_create" runat="server" placeholder="วันที่เดินทาง..." CssClass="form-control datetimepicker-from cursor-pointer" />

                                                        <span class="input-group-addon show-from-onclick">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>

                                                        <asp:RequiredFieldValidator ID="Re_txtDateStart_create"
                                                            runat="server"
                                                            ControlToValidate="txtDateStart_create" Display="None" SetFocusOnError="true"
                                                            ErrorMessage="*กรุณาเลือกวันที่เดินทาง"
                                                            ValidationGroup="SaveIndex" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txtDateStart_create" Width="200" PopupPosition="BottomLeft" />

                                                    </div>
                                                    <div class="col-md-1" id="div_showfiletestaddmore" runat="server" style="color: transparent;">
                                                        <asp:FileUpload ID="FileUpload1_show" CssClass="btn btn-warning hidden" ClientIDMode="Static" runat="server" accept="jpg|png" />
                                                    </div>
                                                    <p class="help-block" style="font-size: small"><font color="red">*** กรุณาทำรายการจองรถก่อนวันเดินทาง 4 ชั่วโมง ***</font></p>
                                                </ContentTemplate>

                                            </asp:UpdatePanel>
                                        </div>
                                        <label class="col-sm-2 control-label">เวลา</label>
                                        <div class="col-sm-4">
                                            <div class="input-group time">

                                                <asp:TextBox ID="txt_timestart_create" placeholder="เวลา ..." runat="server" CssClass="form-control clockpicker cursor-pointer" value="" />
                                                <span class="input-group-addon show-time-onclick cursor-pointer">
                                                    <span class="glyphicon glyphicon-time"></span>
                                                </span>
                                                <asp:RequiredFieldValidator ID="Re_txt_timestart_create"
                                                    runat="server"
                                                    ControlToValidate="txt_timestart_create" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกเวลาที่เริ่มต้นจอง"
                                                    ValidationGroup="SaveIndex" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_timestart_create" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">ถึงวันที่</label>
                                        <div class="col-sm-4">
                                            <div class="input-group date">

                                                <asp:TextBox ID="txtDateEnd_create" runat="server" placeholder="ถึงวันที่..."
                                                    CssClass="form-control datetimepicker-to cursor-pointer" />
                                                <span class="input-group-addon show-to-onclick">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                                <asp:RequiredFieldValidator ID="Re_txtDateEnd_create"
                                                    runat="server"
                                                    ControlToValidate="txtDateEnd_create" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกวันที่"
                                                    ValidationGroup="SaveIndex" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txtDateEnd_create" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>
                                        <label class="col-sm-2 control-label">เวลา</label>
                                        <div class="col-sm-4">
                                            <div class="input-group time">

                                                <asp:TextBox ID="txt_timeend_create" placeholder="เวลา ..." runat="server" CssClass="form-control clockpickerto cursor-pointer" value="" />
                                                <span class="input-group-addon show-timeto-onclick cursor-pointer">
                                                    <span class="glyphicon glyphicon-time"></span>
                                                </span>
                                                <asp:RequiredFieldValidator ID="Re_txt_timeend_create"
                                                    runat="server"
                                                    ControlToValidate="txt_timeend_create" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกเวลาที่สิ้นสุดการจอง"
                                                    ValidationGroup="SaveIndex" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_timeend_create" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-4">
                                            <asp:LinkButton ID="btnSearchCarBusy" runat="server"
                                                CssClass="btn btn-primary" OnCommand="btnCommand" CommandName="cmdSearchCarBusy" data-original-title="ตรวจสอบรถ" data-toggle="tooltip"><span class="glyphicon glyphicon-search"></span> ตรวจสอบข้อมูลรถ</asp:LinkButton>

                                        </div>
                                        <label class="col-sm-6 control-label"></label>

                                    </div>

                                    <!-- Gridview Search Detail Car Busy/ Not Busy -->
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-4">
                                            <asp:UpdatePanel ID="Update_DetailCarSearchCreate" runat="server" Visible="false">
                                                <ContentTemplate>

                                                    <asp:GridView ID="GvDetailCarSearchCreate" runat="server"
                                                        AutoGenerateColumns="False"
                                                        CssClass="table table-striped table-bordered table-hover table-responsive"
                                                        OnPageIndexChanging="gvPageIndexChanging"
                                                        OnRowDataBound="gvRowDataBound"
                                                        AllowPaging="True" PageSize="10">
                                                        <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                                                        <RowStyle Font-Size="Small" />
                                                        <PagerStyle CssClass="pageCustom" />
                                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                                        <EmptyDataTemplate>
                                                            <div style="text-align: center">ไม่พบข้อมูล</div>
                                                        </EmptyDataTemplate>
                                                        <Columns>

                                                            <%--<asp:TemplateField HeaderText="รูป" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                <ItemTemplate>
                                                                    <small>

                                                                        <asp:HyperLink runat="server" ID="btnViewFileRoom" CssClass="pull-letf" data-toggle="tooltip" title="view" data-original-title="" Target="_blank">
                                                    <i class="fa fa-file-text-o"></i> ดูรายละเอียด</asp:HyperLink>

                                                                    </small>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>

                                                            <asp:TemplateField HeaderText="ทะเบียนรถ" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_m0_car_idx" runat="server" Visible="false" Text='<%# Eval("m0_car_idx") %>'></asp:Label>
                                                                    <asp:Label ID="lbl_car_register" runat="server" Text='<%# Eval("car_register") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                            <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_status_carbusy" runat="server" Text='<%# Eval("car_busy") %>'></asp:Label>
                                                                    <%--<asp:Label ID="lbl_room_detail_statusroom" runat="server" Visible="true"></asp:Label>--%>
                                                                    <%--<asp:Label ID="lbl_room_detail_notbusy" runat="server" Visible="false" Text="ไม่ว่าง"></asp:Label>--%>
                                                                    <%--<asp:Label ID="lbl_room_detail_busy" runat="server" Visible="false" Text="ว่าง"></asp:Label>--%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                        </Columns>
                                                    </asp:GridView>

                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="btn_SaveCreate" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </div>

                                        <label class="col-sm-6 control-label"></label>
                                    </div>
                                    <!-- Gridview Search Detail Car Busy/ Not Busy -->

                                    <div class="clearfix"></div>
                                    <hr />

                                    <asp:UpdatePanel ID="Update_SaveFileCreateCarUser" runat="server" Visible="true">
                                        <ContentTemplate>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">ประเภทการจอง</label>
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddlType_booking_idx" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="Re_ddlType_booking_idx"
                                                        runat="server"
                                                        InitialValue="0"
                                                        ControlToValidate="ddlType_booking_idx" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณาเลือกประเภทการจอง"
                                                        ValidationGroup="SaveIndex" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlType_booking_idx" Width="200" PopupPosition="BottomLeft" />
                                                </div>

                                                <asp:UpdatePanel ID="PanelTypeTravel" runat="server" Visible="false">
                                                    <ContentTemplate>
                                                        <label class="col-sm-2 control-label">ประเภทการเดินทาง</label>
                                                        <div class="col-sm-4">
                                                            <asp:DropDownList ID="ddlType_travel_idx" runat="server" CssClass="form-control">
                                                                <asp:ListItem Value="0">--- เลือกประเภทการเดินทาง ---</asp:ListItem>
                                                            </asp:DropDownList>

                                                            <asp:RequiredFieldValidator ID="RequiredddlType_travel_idx"
                                                                runat="server"
                                                                InitialValue="0"
                                                                ControlToValidate="ddlType_travel_idx" Display="None" SetFocusOnError="true"
                                                                ErrorMessage="*กรุณาเลือกประเภทการเดินทาง"
                                                                ValidationGroup="SaveIndex" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredddlType_travel_idx" Width="200" PopupPosition="BottomLeft" />

                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>

                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">สถานที่</label>
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddlPlace" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="Required_ddlPlace"
                                                        runat="server"
                                                        InitialValue="0"
                                                        ControlToValidate="ddlPlace" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณาเลือกสถานที่"
                                                        ValidationGroup="SaveIndex" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Required_ddlPlace" Width="200" PopupPosition="BottomLeft" />


                                                </div>

                                                <asp:UpdatePanel ID="Panel_OtherPlace" runat="server" Visible="false">
                                                    <ContentTemplate>
                                                        <label class="col-sm-2 control-label">อื่นๆ</label>
                                                        <div class="col-sm-4">
                                                            <asp:TextBox ID="txt_PlaceOther" runat="server" CssClass="form-control" placeholder="กรอกสถานที่อื่นๆ..."></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="Requiredtxt_PlaceOther"
                                                                runat="server"
                                                                ControlToValidate="txt_PlaceOther" Display="None" SetFocusOnError="true"
                                                                ErrorMessage="*กรุณากรอกสถานที่อื่นๆ"
                                                                ValidationGroup="SaveIndex" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxt_PlaceOther" Width="200" PopupPosition="BottomLeft" />

                                                            <%-- <asp:DropDownList ID="DropDownList2" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">--- เลือกประเภทการเดินทาง ---</asp:ListItem>
                                            </asp:DropDownList>--%>
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">ระยะทาง</label>
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txt_distance" runat="server" CssClass="form-control" placeholder="ระยะทาง..."></asp:TextBox>

                                                    <asp:RequiredFieldValidator ID="Required_txt_distance"
                                                        runat="server"
                                                        ControlToValidate="txt_distance" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณากรอกระยะทาง"
                                                        ValidationGroup="SaveIndex" />

                                                    <asp:RegularExpressionValidator ID="Requiredtxt_distance1" runat="server" ValidationGroup="SaveCreate" Display="None"
                                                        ErrorMessage="กรุณากรอกเฉพาะตัวเลข หรือทศนิยมเท่านั้น"
                                                        ControlToValidate="txt_distance"
                                                        ValidationExpression="^\d+\d*\.?\d*$" />


                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Required_txt_distance" Width="200" PopupPosition="BottomLeft" />

                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender121111" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxt_distance1" Width="200" PopupPosition="BottomLeft" />


                                                    <p class="help-block" style="font-size: small">
                                                        <font color="red">*** กรอกข้อมูลระยะทางตาม Google Map ***</font>
                                                        <asp:HyperLink ID="hp_gotogooglemap" CssClass="" runat="server" NavigateUrl="https://www.google.com/maps" Target="_blank"> 
                                                            <span style="font-size: 16px;" class="pull-left hidden-xs showopacity glyphicon glyphicon-map-marker"></span>
                                                        </asp:HyperLink>
                                                    </p>

                                                </div>

                                                <label class="col-sm-1 control-label">กิโลเมตร</label>

                                                <label class="col-sm-2 control-label">ระยะเวลา</label>
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txt_time_length" runat="server" CssClass="form-control" placeholder="ระยะเวลา..."></asp:TextBox>

                                                    <asp:RequiredFieldValidator ID="Requiredtxt_time_length"
                                                        runat="server"
                                                        ControlToValidate="txt_time_length" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณากรอกระยะเวลา"
                                                        ValidationGroup="SaveIndex" />

                                                    <asp:RegularExpressionValidator ID="Regulartxt_time_length1" runat="server" ValidationGroup="SaveCreate" Display="None"
                                                        ErrorMessage="กรุณากรอกเฉพาะตัวเลข หรือทศนิยมเท่านั้น"
                                                        ControlToValidate="txt_time_length"
                                                        ValidationExpression="^\d+\d*\.?\d*$" />

                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxt_time_length" Width="200" PopupPosition="BottomLeft" />

                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender13" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Regulartxt_time_length1" Width="200" PopupPosition="BottomLeft" />

                                                    <%-- <asp:DropDownList ID="DropDownList2" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">--- เลือกประเภทการเดินทาง ---</asp:ListItem>
                                            </asp:DropDownList>--%>
                                                </div>
                                                <label class="col-sm-1 control-label">ชั่วโมง</label>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">หมายเหตุ</label>
                                                <div class="col-sm-10">
                                                    <asp:TextBox ID="txt_note_booking" runat="server" CssClass="form-control" placeholder="กรอกหมายเหตุ ..." TextMode="multiline" Rows="3"></asp:TextBox>

                                                </div>

                                            </div>


                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">วัตถุประสงค์</label>
                                                <div class="col-sm-4">
                                                    <asp:TextBox ID="txt_objective_booking" runat="server" CssClass="form-control" placeholder="กรอกวัตถุประสงค์ ..." TextMode="multiline" Rows="3"></asp:TextBox>

                                                </div>

                                                <label class="col-sm-2 control-label">จำนวนผู้เดินทาง</label>
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txt_count_travel" runat="server" CssClass="form-control" placeholder="กรอกจำนวนผู้เดินทาง ..."></asp:TextBox>

                                                    <asp:RequiredFieldValidator ID="Required_txt_count_travel"
                                                        runat="server"
                                                        ControlToValidate="txt_count_travel" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณากรอกจำนวนผู้เดินทาง"
                                                        ValidationGroup="SaveIndex" />

                                                    <asp:RegularExpressionValidator ID="Required_txt_count_travel1" runat="server" ValidationGroup="SaveCreate" Display="None"
                                                        ErrorMessage="กรุณากรอกเฉพาะตัวเลขเท่านั้น"
                                                        ControlToValidate="txt_count_travel"
                                                        ValidationExpression="^\d+" />

                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Required_txt_count_travel" Width="200" PopupPosition="BottomLeft" />

                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender14" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Required_txt_count_travel1" Width="200" PopupPosition="BottomLeft" />

                                                </div>
                                                <label class="col-sm-1 control-label">คน</label>

                                            </div>


                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">แนบไฟล์</label>
                                                <div class="col-sm-4">
                                                    <asp:FileUpload ID="UploadFileCreateCarUser" CssClass="btn btn-warning" ClientIDMode="Static" runat="server" accept="jpg|png" />
                                                    <p class="help-block" style="font-size: small">
                                                        <font color="red">***เฉพาะนามสกุล jpg, png</font>

                                                    </p>

                                                </div>

                                                <label class="col-sm-6 control-label"></label>


                                            </div>

                                            <div class="form-group pull-right">
                                                <%--<label class="col-sm-2 control-label"></label>--%>
                                                <div class="col-sm-12">
                                                    <asp:LinkButton ID="btn_SaveCreate" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="บันทึก" OnCommand="btnCommand" CommandName="cmdSave" ValidationGroup="SaveIndex" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')"></asp:LinkButton>

                                                    <asp:LinkButton ID="btn_CancelCreate" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="ยกเลิก" OnCommand="btnCommand" CommandName="cmdCancel"></asp:LinkButton>
                                                </div>
                                            </div>
                                        </ContentTemplate>

                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btn_SaveCreate" />
                                        </Triggers>
                                    </asp:UpdatePanel>



                                </div>
                            </div>
                        </div>
                    </InsertItemTemplate>
                </asp:FormView>
                <!-- Form Create -->

            </div>
        </asp:View>
        <!--View Create-->

        <!--View Approve-->
        <asp:View ID="docApprove" runat="server">
            <div class="col-md-12">

                <div class="form-group" id="Div_Search_WaitApprove" runat="server">
                    <asp:LinkButton ID="btn_SearchWaitAppove" CssClass="btn btn-primary" runat="server" data-original-title="แสดงแถบเครื่องมือค้นหา" data-toggle="tooltip" CommandName="cmdSearchWaitApprove" OnCommand="btnCommand"><span class="glyphicon glyphicon-search"></span> ค้นหา</asp:LinkButton>

                </div>

                <!-- Form Search Detail Wait Approve -->
                <asp:FormView ID="FvSearchWaitApprove" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <InsertItemTemplate>

                        <!-- Hide Search Detail Index -->
                        <asp:UpdatePanel ID="Update_HideSearchDetailWaitApprove" runat="server" Visible="false">
                            <ContentTemplate>
                                <div class="form-group">
                                    <asp:LinkButton ID="btnHideSearchDetailWaitApprove" CssClass="btn btn-danger" data-toggle="tooltip" title="ซ่อนแถบเครื่องมือค้นหา" runat="server"
                                        CommandName="cmdHideSearchDetailWaitApprove" OnCommand="btnCommand">< ซ่อนแถบเครื่องมือค้นหา</asp:LinkButton>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <!-- Hide Search Detail Index -->

                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">ค้นหารายการ</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <asp:UpdatePanel ID="Panel_SearchDetailWaitApprove" runat="server">
                                        <ContentTemplate>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">สถานที่</label>
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddlPlaceSearchDetailWaitApprove" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                    </asp:DropDownList>

                                                </div>
                                                <asp:UpdatePanel ID="Update_DetailtypecarSearchDetailWaitApprove" Visible="true" runat="server">
                                                    <ContentTemplate>
                                                        <label class="col-sm-2 control-label">ประเภทรถ</label>
                                                        <div class="col-sm-4">
                                                            <asp:DropDownList ID="ddlDetailtypecarSearchDetailWaitApprove" runat="server" CssClass="form-control">
                                                                <asp:ListItem Value="0">--- เลือกประเภทรถ ---</asp:ListItem>
                                                            </asp:DropDownList>

                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>

                                            <div class="form-group" id="panel_searchdateWaitApprove" runat="server">
                                                <%-- <asp:Label ID="Label9" runat="server" Text="ระบุวันค้นหา" CssClass="col-sm-2 control-label"></asp:Label>--%>
                                                <label class="col-sm-2 control-label">ระบุวันค้นหา</label>
                                                <div class="col-sm-2">
                                                    <asp:DropDownList ID="ddlSearchDateWaitApprove" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                        <asp:ListItem Value="00">เลือกเงื่อนไข</asp:ListItem>
                                                        <asp:ListItem Value="1">มากกว่า  ></asp:ListItem>
                                                        <asp:ListItem Value="2">น้อยกว่า <</asp:ListItem>
                                                        <asp:ListItem Value="3">ระหว่าง  <></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-sm-4 ">

                                                    <div class='input-group date'>
                                                        <asp:TextBox ID="AddStartdateWaitApprove" runat="server" CssClass="form-control from-date-datepicker" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                        <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                        <asp:RequiredFieldValidator ID="Required_AddStartdateWaitApprove" ValidationGroup="SearchDetailWaitApprove" runat="server" Display="None"
                                                            ControlToValidate="AddStartdateWaitApprove" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกวันที่ต้องการ"
                                                            ValidationExpression="กรุณาเลือกวันที่ต้องการ" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender51222" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Required_AddStartdateWaitApprove" Width="160" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class='input-group date'>
                                                        <asp:TextBox ID="AddEndDateWaitApprove" runat="server" CssClass="form-control from-date-datepicker" Enabled="false" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                        <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                    </div>
                                                </div>
                                                <%--<p class="help-block"><font color="red">**</font></p>--%>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label"></label>
                                                <div class="col-sm-4">
                                                    <asp:LinkButton ID="btnSearchDetailWaitApproveCar" runat="server"
                                                        CssClass="btn btn-primary" OnCommand="btnCommand" CommandName="cmdSearchDetailWaitApproveCar" data-original-title="ค้นหา" data-toggle="tooltip" ValidationGroup="SearchDetailWaitApprove"><span class="glyphicon glyphicon-search"></span> ค้นหา</asp:LinkButton>

                                                </div>
                                                <label class="col-sm-6 control-label"></label>
                                            </div>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>


                                </div>
                            </div>
                        </div>

                    </InsertItemTemplate>
                </asp:FormView>
                <!-- Form Search Detail Wait Approve -->


                <!-- Back To Detail Room Booking -->
                <asp:UpdatePanel ID="Update_BackToWaitDetail" runat="server" Visible="false">
                    <ContentTemplate>
                        <div class="form-group">
                            <asp:LinkButton ID="btnBackToWaitDetail" CssClass="btn btn-danger" data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                                CommandName="cmdBackToWaitDetail" OnCommand="btnCommand">< ย้อนกลับ</asp:LinkButton>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- Back To Detail Room Booking -->

                <asp:GridView ID="GvWaitDetailApprove" runat="server"
                    AutoGenerateColumns="False"
                    CssClass="table table-striped table-bordered table-hover table-responsive"
                    OnPageIndexChanging="gvPageIndexChanging"
                    OnRowDataBound="gvRowDataBound"
                    AllowPaging="True" PageSize="10">
                    <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                    <RowStyle Font-Size="Small" />
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">--- ไม่พบข้อมูล ---</div>
                    </EmptyDataTemplate>
                    <Columns>

                        <asp:TemplateField HeaderText="วันที่ทำรายการ" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                            <ItemTemplate>
                                <p>
                                    <asp:Label ID="lbl_create_date_detail" runat="server" Text='<%# Eval("create_date") %>'></asp:Label>
                                </p>
                                <p>
                                    <asp:Label ID="lbl_time_create_date_detail" runat="server" Text='<%# Eval("time_create_date") %>'></asp:Label>
                                </p>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="รายละเอียดผู้สร้าง" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="left">
                            <ItemTemplate>
                                <asp:Label ID="lbl_u0_document_idx_detail" runat="server" Visible="false" Text='<%# Eval("u0_document_idx") %>'></asp:Label>

                                <p>
                                    <b>ชื่อ-สกุลผู้สร้าง:</b>
                                    <asp:Label ID="lbl_cemp_idx_detail" Visible="false" runat="server" Text='<%# Eval("cemp_idx") %>' />
                                    <asp:Label ID="lbl_emp_name_th_detail" runat="server" Text='<%# Eval("emp_name_th") %>' />
                                </p>
                                <p>
                                    <b>องค์กร:</b>
                                    <asp:Label ID="lbl_org_name_th_detail" runat="server" Text='<%# Eval("org_name_th") %>' />
                                </p>
                                <p>
                                    <b>ฝ่าย:</b>
                                    <asp:Label ID="lbl_dept_name_th_detail" runat="server" Text='<%# Eval("dept_name_th") %>' />
                                </p>

                            </ItemTemplate>
                        </asp:TemplateField>



                        <asp:TemplateField HeaderText="รายละเอียดการจอง" HeaderStyle-CssClass="text-center">
                            <ItemTemplate>

                                <p>
                                    <b>สถานที่:</b>
                                    <asp:Label ID="lbl_place_idx_detail" runat="server" Visible="false" Text='<%# Eval("place_idx") %>' />
                                    <asp:Label ID="lbl_place_name_detail" runat="server" Text='<%# Eval("place_name") %>' />
                                </p>

                                <p>
                                    <b>อื่นๆ:</b>
                                    <asp:Label ID="lbl_place_name_other" runat="server" Text='<%# Eval("place_name_other") %>' />
                                </p>

                                <p>
                                    <b>วันที่เดินทาง:</b>

                                    <asp:Label ID="lbl_date_start_detail" runat="server" Text='<%# Eval("date_start") %>' />
                                    <b>เวลา:</b>
                                    <asp:Label ID="lbl_time_start_detail" runat="server" Text='<%# Eval("time_start") %>' />
                                    <b>น.</b>
                                </p>
                                <p>
                                    <b>ถึงวันที่:</b>
                                    <asp:Label ID="lbl_date_end_detail" runat="server" Text='<%# Eval("date_end") %>' />
                                    <b>เวลา:</b>
                                    <asp:Label ID="lbl_time_end_detail" runat="server" Text='<%# Eval("time_end") %>' />
                                    <b>น.</b>
                                </p>


                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สถานะรายการ" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                            <ItemTemplate>

                                <asp:Label ID="lbl_staidx_detail" runat="server" Text='<%# Eval("staidx") %>' Visible="false" />
                                <asp:Label ID="lbl_status_name_detail" runat="server" Text='<%# Eval("status_name") %>' />
                                <asp:Label ID="lbl_m0_node_idx_detail" runat="server" Text='<%# Eval("m0_node_idx") %>' Visible="false" />
                                <asp:Label ID="lbl_node_name_detail" runat="server" Text='<%# Eval("node_name") %>' />
                                <br />
                                โดย<br />
                                <asp:Label ID="lbl_m0_actor_idx_detail" runat="server" Text='<%# Eval("m0_actor_idx") %>' Visible="false" />
                                <asp:Label ID="lbl_actor_name_detail" runat="server" Text='<%# Eval("actor_name") %>' />

                                <%--  <asp:Label ID="lbl_current_status" runat="server" Text='<%# Eval("current_status") %>'></asp:Label>--%>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                            <ItemTemplate>



                                <asp:LinkButton ID="btnViewDetailApprove" CssClass="btn-sm btn-info" target="" runat="server" CommandName="cmdViewDetailApprove" OnCommand="btnCommand"
                                    CommandArgument='<%# Eval("u0_document_idx")+ ";" + Eval("m0_node_idx") + ";" + Eval("m0_actor_idx") + ";" + Eval("staidx") + ";" + Eval("cemp_idx")%>'
                                    data-toggle="tooltip" title="ดูข้อมูล"><i class="fa fa-file"></i></asp:LinkButton>

                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>

                <asp:FormView ID="FvDetailWaitApprove" DefaultMode="ReadOnly" runat="server" OnDataBound="FvDetail_DataBound" Width="100%">
                    <ItemTemplate>

                        <asp:HiddenField ID="hfu0_document_idx" runat="server" Value='<%# Eval("u0_document_idx") %>' />
                        <asp:HiddenField ID="hfM0NodeIDX" runat="server" Value='<%# Eval("m0_node_idx") %>' />
                        <asp:HiddenField ID="hfM0ActoreIDX" runat="server" Value='<%# Eval("m0_actor_idx") %>' />
                        <asp:HiddenField ID="hfM0StatusIDX" runat="server" Value='<%# Eval("staidx") %>' />
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายละเอียดรายการจองรถ</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <%--<div class="col-md-12">--%>
                                        <asp:Label ID="lbl_emp_code_" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="รหัสผู้ทำรายการ : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_u0_document_idx_approve" Visible="false" CssClass="control-labelnotop col-xs-5" runat="server" Text='<%# Eval("u0_document_idx") %>' />
                                            <asp:Label ID="lbl_emp_code_approve" CssClass="control-labelnotop col-xs-5" runat="server" Text='<%# Eval("emp_code") %>' />
                                            <asp:Label ID="lbl_cemp_idx_approve" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("cemp_idx")%>' />

                                        </div>

                                        <asp:Label ID="lbl_emp_name_th_" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ชื่อผู้ทำรายการ : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_emp_name_th_approve" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("emp_name_th") %>' />
                                        </div>
                                        <%-- </div>--%>
                                    </div>

                                    <div class="form-group">
                                        <%--<div class="col-md-12">--%>
                                        <asp:Label ID="Label4" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="บริษัท : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_org_name_th_approve" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("org_name_th")%>' />
                                            <asp:Label ID="lbl_cemp_org_idx_approve" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("org_idx")%>' />
                                        </div>

                                        <asp:Label ID="lbl_dept_name_th" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ฝ่าย : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_dept_name_th_approve" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("dept_name_th")%>' />
                                            <asp:Label ID="lbl_cemp_rdept_idx_approve" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("rdept_idx")%>' />
                                        </div>
                                        <%-- </div>--%>
                                    </div>

                                    <div class="form-group">
                                        <%--<div class="col-md-12">--%>
                                        <asp:Label ID="Label1" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="แผนก : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_sec_name_th_approve" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("sec_name_th")%>' />
                                            <asp:Label ID="lbl_rsec_idx_approve" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("rsec_idx")%>' />
                                        </div>

                                        <asp:Label ID="Label5" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ตำแหน่ง : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_pos_name_th_approve" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("pos_name_th")%>' />
                                            <asp:Label ID="lbl_rpos_idx_approve" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("rpos_idx")%>' />
                                        </div>
                                        <%-- </div>--%>
                                    </div>

                                    <div class="form-group">
                                        <%--<div class="col-md-12">--%>

                                        <asp:Label ID="Label7" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="E-mail : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_emp_email_approve" runat="server" CssClass="control-labelnotop word-wrap col-xs-12" Text='<%# Eval("emp_email")%>' />

                                        </div>

                                        <asp:Label ID="Label2" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="Cost Center : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_costcenter_no_approve" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("costcenter_no")%>' />
                                            <asp:Label ID="lbl_costcenter_idx_approve" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("costcenter_idx")%>' />
                                        </div>

                                        <%-- </div>--%>
                                    </div>

                                    <div class="form-group">
                                        <%--<div class="col-md-12">--%>
                                        <asp:Label ID="Label3" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ประเภทรถ : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_detailtype_car_name_approve" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("detailtype_car_name")%>' />
                                            <asp:Label ID="lbl_detailtype_car_idx_approve" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("detailtype_car_idx")%>' />
                                        </div>

                                        <div class="col-xs-6 control-labelnotop text_right"></div>
                                        <%-- <asp:Label ID="Label9" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="E-mail : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="Label10" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("emp_email")%>' />

                                        </div>--%>
                                        <%-- </div>--%>
                                    </div>

                                    <div class="form-group">
                                        <%--<div class="col-md-12">--%>
                                        <asp:Label ID="Label6" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ประเภทการจอง : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_type_booking_name_approve" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("type_booking_name")%>' />
                                            <asp:Label ID="lbl_type_booking_idx_approve" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("type_booking_idx")%>' />
                                        </div>

                                        <asp:Label ID="Label10" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ประเภทการเดินทาง : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_type_travel_name_approve" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("type_travel_name")%>' />
                                            <asp:Label ID="lbl_type_travel_idx_approve" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("type_travel_idx")%>' />

                                        </div>
                                        <%-- </div>--%>
                                    </div>

                                    <div class="form-group">
                                        <%--<div class="col-md-12">--%>
                                        <asp:Label ID="Label8" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="สถานที่ : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_place_approve" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("place_name")%>' />
                                            <asp:Label ID="lbl_place_idx_approve" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("place_idx")%>' />
                                        </div>

                                        <asp:Label ID="Label12" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="อื่นๆ : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_place_name_other_approve" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("place_name_other")%>' />


                                        </div>
                                        <%-- </div>--%>
                                    </div>

                                    <div class="form-group">
                                        <%--<div class="col-md-12">--%>
                                        <asp:Label ID="Label9" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ระยะทาง : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_distance_approve" runat="server" CssClass="control-labelnotop col-xs-8" Text='<%# Eval("distance")%>' />
                                            <asp:Label ID="lbl_distance_approve_unit" runat="server" CssClass="control-labelnotop col-xs-4" Text="กิโลเมตร" />
                                        </div>

                                        <asp:Label ID="Label14" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ระยะเวลา : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_time_length_approve" runat="server" CssClass="control-labelnotop col-xs-8" Text='<%# Eval("time_length")%>' />
                                            <asp:Label ID="lbl_time_length_approve_unit" runat="server" CssClass="control-labelnotop col-xs-4" Text="ชั่วโมง" />

                                        </div>
                                        <%-- </div>--%>
                                    </div>

                                    <div class="form-group">
                                        <%--<div class="col-md-12">--%>
                                        <asp:Label ID="Label11" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="หมายเหตุ : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_note_booking_approve" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("note_booking")%>' />

                                        </div>
                                        <div class="col-xs-6 control-labelnotop text_right"></div>
                                        <%--  <asp:Label ID="Label15" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ระยะเวลา : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="Label16" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("time_length")%>' />


                                        </div>--%>
                                        <%-- </div>--%>
                                    </div>

                                    <div class="form-group">
                                        <%--<div class="col-md-12">--%>
                                        <asp:Label ID="Label13" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="วันที่เดินทาง : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_date_start_approve" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("date_start")%>' />

                                        </div>

                                        <asp:Label ID="Label17" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="เวลา : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_time_start_approve" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("time_start")%>' />

                                        </div>
                                        <%-- </div>--%>
                                    </div>

                                    <div class="form-group">
                                        <%--<div class="col-md-12">--%>
                                        <asp:Label ID="Label15" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ถึงวันที่ : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_date_end_approve" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("date_end")%>' />

                                        </div>

                                        <asp:Label ID="Label18" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="เวลา : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_time_end_approve" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("time_end")%>' />

                                        </div>
                                        <%-- </div>--%>
                                    </div>

                                    <div class="form-group">
                                        <%--<div class="col-md-12">--%>
                                        <asp:Label ID="Label16" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="วัตถุประสงค์ : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_objective_booking_approve" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("objective_booking")%>' />

                                        </div>
                                        <div class="col-xs-6 control-labelnotop text_right"></div>
                                        <%--<asp:Label ID="Label20" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="เวลา : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="Label21" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("time_end")%>' />

                                        </div>--%>
                                        <%-- </div>--%>
                                    </div>

                                    <div class="form-group">
                                        <%--<div class="col-md-12">--%>
                                        <asp:Label ID="Label19" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="จำนวนผู้เดินทาง : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_count_travel_approve" runat="server" CssClass="control-labelnotop col-xs-8" Text='<%# Eval("count_travel")%>' />
                                            <asp:Label ID="lbl_count_travel_approve_unit" runat="server" CssClass="control-labelnotop col-xs-4" Text="คน" />

                                        </div>
                                        <asp:Label ID="Label21" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ไฟล์แนบ : " />
                                        <div class="col-xs-4">
                                            <asp:HyperLink runat="server" ID="btnViewFileCarDetailWaitapprove" CssClass="f-s-13" Target="_blank"><i class="fa fa-file"></i> ดูรายละเอียดไฟล์แนบ</asp:HyperLink>

                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:FormView>

                <asp:FormView ID="FvHeadUserApprove" DefaultMode="ReadOnly" runat="server" OnDataBound="FvDetail_DataBound" Width="100%">
                    <InsertItemTemplate>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">ส่วนของผู้มีสิทธิ์อนุมัติ (หัวหน้าของผู้สร้าง)</h3>
                            </div>
                            <div class="panel-body">

                                <div class="col-md-8 col-md-offset-2">

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>เลือกสถานะการอนุมัติ</label>
                                            <asp:DropDownList ID="ddlApproveStatus" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="Re_ddlApproveStatus"
                                                runat="server"
                                                InitialValue="0"
                                                ControlToValidate="ddlApproveStatus" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกสถานะการอนุมัติ"
                                                ValidationGroup="SaveApproveHeaduser" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="Vali_Re_ddlApproveStatus" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlApproveStatus" Width="200" PopupPosition="BottomLeft" />
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Comment ผู้มีสิทธิ์อนุมัติ</label>
                                            <asp:TextBox ID="txt_cooment_approve" runat="server" CssClass="form-control" TextMode="multiline" Rows="3" placeholder="Comment ผู้มีสิทธิ์อนุมัติ ..." />
                                            <%--<asp:RequiredFieldValidator ID="Re_ddlPlace"
                                                runat="server"
                                                InitialValue="0"
                                                ControlToValidate="ddlPlace" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกสถานที่"
                                                ValidationGroup="btnSave" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlPlace" Width="200" PopupPosition="BottomLeft" />--%>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group pull-right">
                                            <asp:LinkButton ID="btnSaveApprove" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="บันทึก" OnCommand="btnCommand" CommandName="cmdSaveApprove" ValidationGroup="SaveApproveHeaduser"></asp:LinkButton>

                                            <asp:LinkButton ID="btnCancelApprove" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="ยกเลิก" OnCommand="btnCommand" CommandName="cmdBackToWaitDetail"></asp:LinkButton>

                                        </div>
                                    </div>


                                </div>

                            </div>
                        </div>
                    </InsertItemTemplate>
                </asp:FormView>

                <asp:FormView ID="FvHrApprove" DefaultMode="ReadOnly" runat="server" OnDataBound="FvDetail_DataBound" Width="100%">
                    <InsertItemTemplate>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">ส่วนของเจ้าหน้าที่ HR ผู้มีสิทธิ์อนุมัติ</h3>
                            </div>
                            <div class="panel-body">

                                <div class="col-md-10 col-md-offset-1">

                                    <asp:UpdatePanel ID="Update_SelectCarInsert" runat="server">
                                        <ContentTemplate>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>เลือกสถานะการอนุมัติ</label>
                                                    <asp:DropDownList ID="ddlHrApproveStatus" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="Re_ddlHrApproveStatus"
                                                        runat="server"
                                                        InitialValue="0"
                                                        ControlToValidate="ddlHrApproveStatus" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณาเลือกสถานะการอนุมัติ"
                                                        ValidationGroup="SaveApproveHr" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="Vali_Re_ddlHrApproveStatus" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlHrApproveStatus" Width="200" PopupPosition="BottomLeft" />
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Comment ผู้มีสิทธิ์อนุมัติ</label>
                                                    <asp:TextBox ID="txt_cooment_approve" runat="server" CssClass="form-control" TextMode="multiline" Rows="3" placeholder="Comment ผู้มีสิทธิ์อนุมัติ ..." />
                                                    <%--<asp:RequiredFieldValidator ID="Re_ddlPlace"
                                                runat="server"
                                                InitialValue="0"
                                                ControlToValidate="ddlPlace" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกสถานที่"
                                                ValidationGroup="btnSave" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlPlace" Width="200" PopupPosition="BottomLeft" />--%>
                                                </div>
                                            </div>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                    <asp:UpdatePanel ID="update_InsertSelectCar" runat="server" Visible="false">
                                        <ContentTemplate>
                                            <div class="col-md-12">
                                                <div id="div_DetailEquiment" class="panel panel-default" runat="server">
                                                    <div class="panel-body">

                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label>เลือกประเภทรถที่ใช้</label>
                                                                <asp:DropDownList ID="ddlcar_use_idx" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                </asp:DropDownList>

                                                                <asp:RequiredFieldValidator ID="Re_ddlcar_use_idx"
                                                                    runat="server"
                                                                    InitialValue="0"
                                                                    ControlToValidate="ddlcar_use_idx" Display="None" SetFocusOnError="true"
                                                                    ErrorMessage="*กรุณาเลือกประเภทรถที่ใช้"
                                                                    ValidationGroup="SaveApproveHr" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="Vali_Re_ddlcar_use_idx" runat="Server"
                                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlcar_use_idx" Width="200" PopupPosition="BottomLeft" />
                                                            </div>
                                                        </div>

                                                        <asp:Panel ID="Panel_SelectCarInBooking" runat="server" Visible="false">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label>เลือกรถ</label>
                                                                    <asp:DropDownList ID="ddlm0_car_idx" ValidationGroup="addCarDetailList" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                    </asp:DropDownList>

                                                                    <asp:RequiredFieldValidator ID="Re_ddlm0_car_idx"
                                                                        runat="server"
                                                                        InitialValue="0"
                                                                        ControlToValidate="ddlm0_car_idx" Display="None" SetFocusOnError="true"
                                                                        ErrorMessage="*กรุณาเลือกรถ"
                                                                        ValidationGroup="addCarDetailList" />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender15" runat="Server"
                                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlm0_car_idx" Width="200" PopupPosition="BottomLeft" />
                                                                </div>
                                                            </div>

                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label>เลือกคนขับ</label>
                                                                    <asp:DropDownList ID="ddl_emp_idx_driver" runat="server" CssClass="form-control">
                                                                    </asp:DropDownList>

                                                                    <asp:RequiredFieldValidator ID="Re_ddl_emp_idx_driver"
                                                                        runat="server"
                                                                        InitialValue="0"
                                                                        ControlToValidate="ddl_emp_idx_driver" Display="None" SetFocusOnError="true"
                                                                        ErrorMessage="*กรุณาเลือกคนขับ"
                                                                        ValidationGroup="addCarDetailList" />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender16" runat="Server"
                                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddl_emp_idx_driver" Width="200" PopupPosition="BottomLeft" />
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <div class="clearfix"></div>
                                                                    <asp:LinkButton ID="btnAddCar" runat="server"
                                                                        CssClass="btn btn-primary col-md-12"
                                                                        Text="เพิ่ม" OnCommand="btnCommand" CommandName="cmdAddCar"
                                                                        ValidationGroup="addCarDetailList" />
                                                                    <div class="clearfix"></div>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-12">

                                                                <asp:GridView ID="gvAddCarBooking"
                                                                    runat="server"
                                                                    CssClass="table table-striped table-responsive"
                                                                    GridLines="None"
                                                                    OnRowCommand="onRowCommand"
                                                                    AutoGenerateColumns="false">
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center"
                                                                            HeaderStyle-CssClass="text-center"
                                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller">
                                                                            <ItemTemplate>
                                                                                <%# (Container.DataItemIndex + 1) %>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:BoundField DataField="drCarDetailText" HeaderText="ชื่อรถ" ItemStyle-CssClass="text-center"
                                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                                        <asp:BoundField DataField="drAdminCarText" HeaderText="ชื่อผู้ขับ" ItemStyle-CssClass="text-center"
                                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="center"
                                                                            HeaderStyle-CssClass="text-center">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="btnRemoveCar" runat="server"
                                                                                    CssClass="btn btn-danger btn-xs"
                                                                                    OnClientClick="return confirm('คุณต้องการลบรายการนี้ใชหรือไม่ ?')"
                                                                                    CommandName="cmdRemoveCar"><i class="fa fa-times"></i>
                                                                                </asp:LinkButton>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                            <div class="clearfix"></div>

                                                        </asp:Panel>

                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                    <div class="col-md-12">
                                        <div class="form-group pull-right">
                                            <asp:LinkButton ID="btnSaveApproveHr" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="บันทึก" OnCommand="btnCommand" CommandName="cmdSaveApproveHr" ValidationGroup="SaveApproveHr"></asp:LinkButton>

                                            <asp:LinkButton ID="btnCancelApprove" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="ยกเลิก" OnCommand="btnCommand" CommandName="cmdBackToWaitDetail"></asp:LinkButton>

                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </InsertItemTemplate>
                </asp:FormView>

                <!-- Log Detail Wait Approve Car Booking -->
                <div class="panel panel-info" id="div_LogViewDetailWaitApprove" runat="server" visible="false">
                    <div class="panel-heading">ประวัติการดำเนินการ</div>
                    <table class="table table-striped f-s-12 table-empshift-responsive">
                        <asp:Repeater ID="rptDetailWaitApprove" runat="server">
                            <HeaderTemplate>
                                <tr>
                                    <th>วัน / เวลา</th>
                                    <th>ผู้ดำเนินการ</th>
                                    <th>ดำเนินการ</th>
                                    <%--<th>ผลการดำเนินการ</th>--%>
                                    <th>เหตุผล</th>
                                </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td data-th="วัน / เวลา"><%# Eval("create_date") %> <%# Eval("time_create") %></td>
                                    <td data-th="ผู้ดำเนินการ"><%# Eval("emp_name_th") %> ( <%# Eval("current_artor") %> )</td>
                                    <td data-th="ดำเนินการ"><%# Eval("current_decision") %></td>
                                    <td data-th="เหตุผล"><%# Eval("comment") %></td>
                                    <%--<td data-th="ผลการดำเนินการ"><%# Eval("current_status_name") %></td>--%>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                    <div class="m-t-10 m-b-10"></div>
                </div>
                <!-- Log Detail Wait Approve Car Booking -->

            </div>

        </asp:View>
        <!--View Approve-->

        <!--View Report-->
        <asp:View ID="docReport" runat="server">
            <div class="col-md-12">

                <asp:UpdatePanel ID="Update_PanelSearchReport" runat="server">
                    <ContentTemplate>
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title">ค้นหารายงาน </h3>
                            </div>

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">ประเภทรายงาน</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlTypeReport" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" CssClass="form-control">
                                                <asp:ListItem Value="0">--- เลือกประเภทรายงาน ---</asp:ListItem>
                                                <asp:ListItem Value="1">ตาราง</asp:ListItem>
                                                <asp:ListItem Value="2">กราฟ</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="Re_ddlTypeReport"
                                                runat="server"
                                                InitialValue="0"
                                                ControlToValidate="ddlTypeReport" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกประเภทรายงาน"
                                                ValidationGroup="SearchReportDetail" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1233" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlTypeReport" Width="200" PopupPosition="BottomLeft" />
                                        </div>
                                        <label class="col-sm-6 control-label"></label>
                                    </div>

                                    <!-- START Report Table -->
                                    <asp:UpdatePanel ID="Update_PnTableReportDetail" runat="server" Visible="false">
                                        <ContentTemplate>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">ค้นหาจากสถานะ</label>
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddlStatusCarBooking" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" CssClass="form-control">
                                                        <asp:ListItem Value="0">--- เลือกประเภทการค้นหา ---</asp:ListItem>
                                                    </asp:DropDownList>

                                                </div>
                                                <label class="col-sm-2 control-label">สถานะการใช้รถนอกเส้นทาง</label>
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddlStatusOutplan" runat="server" Enabled="false" AutoPostBack="true" CssClass="form-control">
                                                        <asp:ListItem Value="0">--- เลือกสถานะการใช้รถนอกเส้นทาง ---</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <%-- <asp:Label ID="Label9" runat="server" Text="ระบุวันค้นหา" CssClass="col-sm-2 control-label"></asp:Label>--%>
                                                <label class="col-sm-2 control-label">ระบุวันค้นหา</label>
                                                <div class="col-sm-2">
                                                    <asp:DropDownList ID="ddlSearchDateReport" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                        <asp:ListItem Value="0">--- เลือกเงื่อนไข ---</asp:ListItem>
                                                        <asp:ListItem Value="1">มากกว่า ></asp:ListItem>
                                                        <asp:ListItem Value="2">น้อยกว่า <</asp:ListItem>
                                                        <asp:ListItem Value="3">ระหว่าง <></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="col-sm-4 ">

                                                    <div class='input-group date'>
                                                        <asp:TextBox ID="txtAddStartdateReport" runat="server" CssClass="form-control from-date-datepicker" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                        <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                        <asp:RequiredFieldValidator ID="Required_txtAddStartdateReport" ValidationGroup="SearchReportDetail" runat="server" Display="None"
                                                            ControlToValidate="txtAddStartdateReport" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกวันที่ต้องการ"
                                                            ValidationExpression="กรุณาเลือกวันที่ต้องการ" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5133" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Required_txtAddStartdateReport" Width="160" />
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class='input-group date'>
                                                        <asp:TextBox ID="txtAddEndDateReport" runat="server" CssClass="form-control from-date-datepicker" Enabled="false" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                        <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">ค้นหาจากองค์กร</label>
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddlorgidx" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control">
                                                        <asp:ListItem Value="0">--- เลือกองค์กร ---</asp:ListItem>
                                                    </asp:DropDownList>

                                                </div>
                                                <label class="col-sm-2 control-label">ค้นหาจากฝ่าย</label>
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddlrdeptidx" runat="server" CssClass="form-control">
                                                        <asp:ListItem Value="0">--- เลือกฝ่าย ---</asp:ListItem>
                                                    </asp:DropDownList>

                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">สถานที่</label>
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddlPlaceReportSearchDetail" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                    <%--<asp:RequiredFieldValidator ID="Re_ddlPlaceSearchDetail"
                                                        runat="server"
                                                        InitialValue="0"
                                                        ControlToValidate="ddlPlaceSearchDetail" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณาเลือกสถานที่"
                                                        ValidationGroup="SearchDetail" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8444" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlPlaceSearchDetail" Width="200" PopupPosition="BottomLeft" />--%>
                                                </div>
                                                <label class="col-sm-2 control-label">ประเภทรถที่ใช้</label>
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddlCarUseReportSearchDetail" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                        <asp:ListItem Value="0">--- เลือกประเภทรถที่ใช้ ---</asp:ListItem>
                                                    </asp:DropDownList>

                                                </div>
                                            </div>

                                            <div class="form-group" id="Div_CarUseStatus" runat="server" visible="false">
                                                <label class="col-sm-2 control-label">สถานะการใช้รถ</label>
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddlCarUseStatus" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                        <asp:ListItem Value="0">--- เลือกสถานะการใช้รถ ---</asp:ListItem>
                                                        <asp:ListItem Value="1"> ใช้รถ</asp:ListItem>
                                                        <asp:ListItem Value="2"> ไม่ใช้รถ</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <%--<asp:RequiredFieldValidator ID="Re_ddlPlaceSearchDetail"
                                                        runat="server"
                                                        InitialValue="0"
                                                        ControlToValidate="ddlPlaceSearchDetail" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณาเลือกสถานที่"
                                                        ValidationGroup="SearchDetail" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8444" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlPlaceSearchDetail" Width="200" PopupPosition="BottomLeft" />--%>
                                                </div>
                                                <label class="col-sm-6 control-label"></label>

                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label"></label>
                                                <div class="col-sm-4">
                                                    <asp:LinkButton ID="btnSearchReportCarDetail" runat="server"
                                                        CssClass="btn btn-primary" OnCommand="btnCommand" CommandName="cmdSearchReportCarDetail" data-original-title="ค้นหา" data-toggle="tooltip" ValidationGroup="SearchReportDetail"><span class="glyphicon glyphicon-search"></span> ค้นหา</asp:LinkButton>

                                                    <asp:LinkButton ID="btnExportReportCarDetail" runat="server"
                                                        CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdExportReportCarDetail" data-original-title="Export" data-toggle="tooltip" ValidationGroup="SearchReportDetail"><span class="glyphicon glyphicon-export"></span> Export</asp:LinkButton>

                                                </div>
                                                <label class="col-sm-6 control-label"></label>
                                            </div>

                                            <%--<p class="help-block"><font color="red">**</font></p>--%>
                                        </ContentTemplate>
                                        <Triggers>

                                            <asp:PostBackTrigger ControlID="btnExportReportCarDetail" />
                                        </Triggers>

                                    </asp:UpdatePanel>
                                    <!-- START Report Table -->

                                    <!-- START Report Graph -->
                                    <asp:UpdatePanel ID="Update_PnGraphReportDetail" runat="server" Visible="false" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">ประเภทกราฟ</label>
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddlTypeGraph" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" CssClass="form-control">
                                                        <asp:ListItem Value="0">--- เลือกประเภทกราฟ ---</asp:ListItem>
                                                        <asp:ListItem Value="1">จำนวนการจองรถ/หน่วยงาน</asp:ListItem>
                                                        <asp:ListItem Value="2">จำนวนการยกเลิก/หน่วยงาน</asp:ListItem>
                                                        <asp:ListItem Value="3">จำนวนค่าใช้จ่าย/หน่วยงาน</asp:ListItem>
                                                        <asp:ListItem Value="4">จำนวนครั้งที่ไม่ใช้รถ/หน่วยงาน</asp:ListItem>
                                                    </asp:DropDownList>

                                                    <asp:RequiredFieldValidator ID="Re_ddlTypeGraph"
                                                        runat="server"
                                                        InitialValue="0"
                                                        ControlToValidate="ddlTypeGraph" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณาเลือกประเภทกราฟ"
                                                        ValidationGroup="SearchReportDetailGraph" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender512212" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlTypeGraph" Width="200" PopupPosition="BottomLeft" />


                                                </div>
                                                <label class="col-sm-2 control-label" runat="server" id="status_report" visible="false">ค้นหาจากสถานะ</label>
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddlStatusBooking" runat="server" CssClass="form-control" Visible="false">
                                                        <asp:ListItem Value="0">--- เลือกสถานะเอกสาร ---</asp:ListItem>
                                                        <%--<asp:ListItem Value="3">เสร็จสมบูรณ์</asp:ListItem>
                                                        <asp:ListItem Value="2">ถูกยกเลิก</asp:ListItem>--%>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">เดือน</label>
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddlmonth" runat="server" CssClass="form-control">
                                                        <asp:ListItem Value="0">--- เลือกเดือน ---</asp:ListItem>
                                                        <asp:ListItem Value="1">มกราคม</asp:ListItem>
                                                        <asp:ListItem Value="2">กุมภาพันธ์</asp:ListItem>
                                                        <asp:ListItem Value="3">มีนาคม</asp:ListItem>
                                                        <asp:ListItem Value="4">เมษายน</asp:ListItem>
                                                        <asp:ListItem Value="5">พฤษภาคม</asp:ListItem>
                                                        <asp:ListItem Value="6">มิถุนายน</asp:ListItem>
                                                        <asp:ListItem Value="7">กรกฎาคม</asp:ListItem>
                                                        <asp:ListItem Value="8">สิงหาคม</asp:ListItem>
                                                        <asp:ListItem Value="9">กันยายน</asp:ListItem>
                                                        <asp:ListItem Value="10">ตุลาคม</asp:ListItem>
                                                        <asp:ListItem Value="11">พฤศจิกายน</asp:ListItem>
                                                        <asp:ListItem Value="12">ธันวาคม</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <label class="col-sm-2 control-label">ปี</label>
                                                <div class="col-sm-4">

                                                    <asp:DropDownList ID="ddlyear" CssClass="form-control" runat="server">
                                                    </asp:DropDownList>
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label"></label>
                                                <div class="col-sm-4">
                                                    <asp:LinkButton ID="btnSearchGraph" runat="server"
                                                        CssClass="btn btn-primary" OnCommand="btnCommand" CommandName="cmdSearchReportGraph" data-original-title="ค้นหา" data-toggle="tooltip" ValidationGroup="SearchReportDetailGraph"><span class="glyphicon glyphicon-search"></span> ค้นหา</asp:LinkButton>
                                                </div>
                                                <label class="col-sm-6 control-label"></label>
                                            </div>


                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnSearchGraph" />
                                            <asp:AsyncPostBackTrigger ControlID="ddlTypeGraph" EventName="SelectedIndexChanged" />
                                        </Triggers>

                                    </asp:UpdatePanel>
                                    <!-- START Report Graph -->


                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>


                <!-- Detail Search Room Report-->
                <asp:UpdatePanel ID="Update_PanelReportTable" runat="server" Visible="false">
                    <ContentTemplate>
                        <asp:GridView ID="GvDetailTableReport" runat="server"
                            AutoGenerateColumns="False"
                            CssClass="table table-striped table-bordered table-hover table-responsive"
                            OnPageIndexChanging="gvPageIndexChanging"
                            OnRowDataBound="gvRowDataBound"
                            AllowPaging="True" PageSize="10">
                            <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                            <RowStyle Font-Size="Small" />
                            <PagerStyle CssClass="pageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                            <EmptyDataTemplate>
                                <div style="text-align: center">ไม่พบข้อมูล</div>
                            </EmptyDataTemplate>
                            <Columns>

                                <asp:TemplateField HeaderText="ฝ่าย" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_rdept_idx_reporttable" runat="server" Visible="false" Text='<%# Eval("rdept_idx") %>'></asp:Label>
                                        <asp:Label ID="lbl_dept_name_th_reporttable" runat="server" Text='<%# Eval("dept_name_th") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                 <asp:TemplateField HeaderText="สถานะเอกสาร" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                       
                                        <asp:Label ID="lbl_status_name_reporttable" runat="server" Text='<%# Eval("status_name") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Cost Center" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_costcenter_idx_reporttable" runat="server" Visible="false" Text='<%# Eval("costcenter_idx") %>'></asp:Label>
                                        <asp:Label ID="lbl_costcenter_no_reporttable" runat="server" Text='<%# Eval("costcenter_no") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="จำนวนครั้ง" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_count_booking_reporttable" runat="server" Text='<%# Eval("count_carbooking") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ดูรายละเอียด" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>

                                        <asp:LinkButton ID="btnViewDetailReportTable" CssClass="btn-sm btn-info" runat="server" CommandName="cmdViewDetailReportTable"
                                            OnCommand="btnCommand" CommandArgument='<%# Eval("costcenter_idx") + ";" + Eval("rdept_idx") + ";" + Eval("staidx")%>'
                                            data-toggle="tooltip" title="ดูรายละเอียด"><i class="fa fa-list"></i></asp:LinkButton>

                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- Detail Search Room Report-->

                <!-- Back To Search Report-->
                <asp:UpdatePanel ID="Update_PanelBackToSearch" runat="server" Visible="false">
                    <ContentTemplate>
                        <div class="form-group">
                            <asp:LinkButton ID="btnBackToSearchReport" CssClass="btn btn-danger" data-toggle="tooltip" title="" runat="server"
                                CommandName="cmdBackToSearchReport" OnCommand="btnCommand">< ย้อนกลับ</asp:LinkButton>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- Back To Search Report-->

                <!-- View Detail Car Report-->
                <asp:UpdatePanel ID="Update_PanelDetailReportTable" runat="server" Visible="false">
                    <ContentTemplate>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายละเอียดรายงานการจองรถ </h3>
                            </div>
                            <div class="panel-body">

                                <div class="form-group">
                                    <asp:UpdatePanel ID="updatepanelbutton" runat="server">
                                        <ContentTemplate>

                                            <asp:LinkButton ID="btnExportDetailReportCarDetail" runat="server"
                                                CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdExportDetailReportCarDetail" data-original-title="Export" data-toggle="tooltip" ValidationGroup="SearchReportDetail"><span class="glyphicon glyphicon-export"></span> Export</asp:LinkButton>

                                        </ContentTemplate>
                                        <Triggers>
                                            <%--<asp:PostBackTrigger ControlID="btnPrintSampleCode" />--%>
                                            <asp:PostBackTrigger ControlID="btnExportDetailReportCarDetail" />
                                        </Triggers>

                                    </asp:UpdatePanel>
                                </div>


                                <asp:GridView ID="GvViewDetailReport" runat="server"
                                    AutoGenerateColumns="False"
                                    CssClass="table table-striped table-bordered table-hover table-responsive"
                                    OnPageIndexChanging="gvPageIndexChanging"
                                    OnRowDataBound="gvRowDataBound"
                                    AllowPaging="True" PageSize="10"
                                    ShowFooter="true">
                                    <HeaderStyle CssClass="success" Height="40px" Font-Size="Small" />
                                    <RowStyle Font-Size="Small" />
                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                    <EmptyDataTemplate>
                                        <div style="text-align: center">ไม่พบข้อมูล</div>
                                    </EmptyDataTemplate>
                                    <Columns>

                                        <%--<asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
                                                

                                            </ItemTemplate>
                                        </asp:TemplateField>--%>

                                        <asp:TemplateField HeaderText="ฝ่าย" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_u0_document_idx_reportview" Visible="false" runat="server" Text='<%# Eval("u0_document_idx") %>'></asp:Label>
                                                <asp:Label ID="lbl_rdept_idx_reportview" Visible="false" runat="server" Text='<%# Eval("rdept_idx") %>'></asp:Label>
                                                <asp:Label ID="lbl_dept_name_th_reportview" runat="server" Text='<%# Eval("dept_name_th") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Coscenter" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_costcenter_idx_reportview" Visible="false" runat="server" Text='<%# Eval("costcenter_idx") %>'></asp:Label>
                                                <asp:Label ID="lbl_costcenter_no_reportview" runat="server" Text='<%# Eval("costcenter_no") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="รายละเอียดการจอง" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                            <ItemTemplate>

                                                <p>
                                                    <b>สถานที่:</b>
                                                    <asp:Label ID="lbl_place_idx_reportview" runat="server" Visible="false" Text='<%# Eval("place_idx") %>'></asp:Label>
                                                    <asp:Label ID="lbl_place_name_reportview" runat="server" Text='<%# Eval("place_name") %>'></asp:Label>

                                                </p>

                                                <p>
                                                    <b>ตั้งแต่วันที่:</b>
                                                    <asp:Label ID="lbl_date_start_reportview" runat="server" Text='<%# Eval("date_start") %>' />
                                                    <b>เวลา:</b>
                                                    <asp:Label ID="lbl_time_start_reportview" runat="server" Text='<%# Eval("time_start") %>' />
                                                    <b>น.</b>
                                                </p>
                                                <p>
                                                    <b>ถึงวันที่:</b>
                                                    <asp:Label ID="lbl_date_end_reportview" runat="server" Text='<%# Eval("date_end") %>' />
                                                    <b>เวลา:</b>
                                                    <asp:Label ID="lbl_time_end_reportview" runat="server" Text='<%# Eval("time_end") %>' />
                                                    <b>น.</b>
                                                </p>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <div style="text-align: right;">
                                                    <p>
                                                        <asp:Literal ID="lit_tot" runat="server" Text="Total :"></asp:Literal>
                                                    </p>
                                                    <p>
                                                        <asp:Literal ID="lit_totoutplan" runat="server" Visible="false" Text="ราคารวมนอกเส้นทาง :"></asp:Literal>
                                                    </p>

                                                </div>
                                            </FooterTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ค่าใช้จ่าย" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                            <ItemTemplate>

                                                <asp:Label ID="lbl_sum_expenses_reportview" Visible="false" runat="server" Text='<%# Eval("sum_expenses_decimal") %>'></asp:Label>
                                                <asp:Label ID="lbl_sum_expenses_reportview_value" runat="server"></asp:Label>
                                            </ItemTemplate>

                                            <FooterTemplate>
                                                <div style="text-align: right; background-color: chartreuse;">
                                                    <p>
                                                        <asp:Label ID="lit_total" runat="server" Font-Bold="true"></asp:Label>
                                                    </p>

                                                </div>
                                            </FooterTemplate>

                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ค่าใช้จ่ายนอกเส้นทาง" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                            <ItemTemplate>

                                                <asp:Label ID="lbl_sum_expenses_outplan_decimal_reportview" Visible="false" runat="server" Text='<%# Eval("sum_expenses_outplan_decimal") %>'></asp:Label>
                                                <asp:Label ID="lbl_sum_expenses_outplan_decimal_reportview_value" runat="server"></asp:Label>
                                            </ItemTemplate>

                                            <FooterTemplate>
                                                <div style="text-align: right; background-color: chartreuse;">
                                                    <p>
                                                        <asp:Label ID="lit_totaloutplan" runat="server" Font-Bold="true"></asp:Label>
                                                    </p>
                                                </div>
                                            </FooterTemplate>

                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>

                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- View Detail Car Report-->

                <!-- View Detail Car Report Graph-->
                <asp:UpdatePanel ID="Update_PanelGraph" runat="server" Visible="false">
                    <ContentTemplate>
                        <div class="panel panel-primary ">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-stats"></i><strong>&nbsp; ข้อมูลรายงาน</strong></h3>
                            </div>
                            <div class="panel-body">
                                <asp:Literal ID="litReportChart" runat="server" />
                                <asp:Literal ID="litReportChart1" runat="server" />


                                <asp:GridView ID="GvDetailGraph" runat="server"
                                    AutoGenerateColumns="False"
                                    CssClass="table table-striped table-bordered table-hover table-responsive"
                                    OnPageIndexChanging="gvPageIndexChanging"
                                    OnRowDataBound="gvRowDataBound"
                                    AllowPaging="True" PageSize="10">
                                    <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                                    <RowStyle Font-Size="Small" />
                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                    <EmptyDataTemplate>
                                        <div style="text-align: center">ไม่พบข้อมูล</div>
                                    </EmptyDataTemplate>
                                    <Columns>

                                        <asp:TemplateField HeaderText="ฝ่าย" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                            <ItemTemplate>

                                                <asp:Label ID="lbl_dept_name_th_reportgraph" runat="server" Text='<%# Eval("dept_name_en") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Coscenter" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_costcenter_idx_reportgraph" Visible="false" runat="server" Text='<%# Eval("costcenter_idx") %>'></asp:Label>
                                                <asp:Label ID="lbl_costcenter_no_reportview" runat="server" Text='<%# Eval("costcenter_no") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="จำนวนครั้ง" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                            <ItemTemplate>

                                                <asp:Label ID="lbl_count_booking_reportgraph" runat="server" Text='<%# Eval("count_booking") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ค่าใช้จ่าย" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                            <ItemTemplate>

                                                <asp:Label ID="lbl_sum_expenses_reportgraph" Visible="false" runat="server" Text='<%# Eval("sum_expenses_decimal") %>'></asp:Label>
                                                <asp:Label ID="lbl_sum_expenses_reportgraph_value" runat="server"></asp:Label>
                                            </ItemTemplate>

                                            <%-- <FooterTemplate>
                                                <div style="text-align: center;">
                                                    <asp:Label ID="lit_total" runat="server"></asp:Label>
                                                </div>
                                            </FooterTemplate>--%>

                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>




                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- View Detail Car Report Graph-->

            </div>

        </asp:View>
        <!--View Report-->

    </asp:MultiView>
    <!--multiview-->

    <%--<link href="Content/fullcalendar-content/fullcalendar.css" rel="stylesheet" type="text/css" />--%>
    <script src='<%= ResolveUrl("~/Scripts/script-dar-dar.js") %>'></script>


    <script type="text/javascript">
        function confirmWithOutValidated(message, validateGroup) {
            if (validateGroup != '') {
                var validated = Page_ClientValidate(validateGroup);
                if (validated) {
                    return confirm(message);
                }
            } else {
                return confirm(message);
            }
        }
        $(function () {
            $('.datetimepicker-from').datetimepicker({

                format: 'DD/MM/YYYY',
                minDate: moment().add(-1, 'day'),
                maxDate: moment().add(30, 'day'),
            });

            $('.datetimepicker-from').on('dp.change', function (e) {
                var dateTo = $('.datetimepicker-to').data("DateTimePicker").date();
                if (e.date > dateTo) {
                    $('.datetimepicker-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }


                //else {
                //    $('.datetimepicker-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                //}

            });

            $('.show-from-onclick').click(function () {
                $('.datetimepicker-from').data("DateTimePicker").show;
            });
            $('.datetimepicker-to').datetimepicker({
                format: 'DD/MM/YYYY',
                minDate: moment().add(-1, 'day'),
                maxDate: moment().add(30, 'day'),
                //ignoreReadonly: true
            });
            $('.show-to-onclick').click(function () {
                $('.datetimepicker-to').data("DateTimePicker").show();
            });
            $('.datetimepicker-to').on('dp.change', function (e) {
                var dateFrom = $('.datetimepicker-from').data("DateTimePicker").date();
                if (e.date < dateFrom) {
                    $('.datetimepicker-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                //else {
                //    $('.datetimepicker-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));

                //}
                //$('.txtSearchToHidden').val($('.datetimepicker-to').val());
                <%--$('#<%= txtSearchToHidden.ClientID %>').val($('.datetimepicker-to').val());--%>
            });
        });
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            //var dateToday = new Date();
            //currentTime = new Date();
            $('.datetimepicker-from').datetimepicker({

                format: 'DD/MM/YYYY',
                minDate: moment().add(-1, 'day'),
                maxDate: moment().add(30, 'day'),
                //ignoreReadonly: true

            });

            $('.datetimepicker-from').on('dp.change', function (e) {
                var dateTo = $('.datetimepicker-to').data("DateTimePicker").date();
                if (e.date > dateTo) {
                    $('.datetimepicker-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                //else {
                //    $('.datetimepicker-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                //}

            });

            $('.show-from-onclick').click(function () {
                $('.datetimepicker-from').data("DateTimePicker").show();
            });
            $('.datetimepicker-to').datetimepicker({
                format: 'DD/MM/YYYY',
                minDate: moment().add(-1, 'day'),
                maxDate: moment().add(30, 'day'),
                //ignoreReadonly: true
            });
            $('.show-to-onclick').click(function () {
                $('.datetimepicker-to').data("DateTimePicker").show();
            });
            $('.datetimepicker-to').on('dp.change', function (e) {
                var dateFrom = $('.datetimepicker-from').data("DateTimePicker").date();
                if (e.date < dateFrom) {
                    $('.datetimepicker-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                //else {
                //    $('.datetimepicker-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));

                //}
                //$('.txtSearchToHidden').val($('.datetimepicker-to').val());
                <%--$('#<%= txtSearchToHidden.ClientID %>').val($('.datetimepicker-to').val());--%>
            });

            /* START Filter DatetimePicker Permission Temporary */
            $('.datetimepicker-filter-from').datetimepicker({
                format: 'DD/MM/YYYY',
                //ignoreReadonly: true
            });
            $('.datetimepicker-filter-from').on('dp.change', function (e) {
                var dateTo = $('.datetimepicker-filter-to').data("DateTimePicker").date();
                if (e.date > dateTo) {
                    $('.datetimepicker-filter-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
               <%-- $('#<%= txtFilterIndexPermTempFromHidden.ClientID %>').val($('.datetimepicker-filter-from').val());--%>
            });
            $('.show-filter-from-onclick').click(function () {
                $('.datetimepicker-filter-from').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-to').datetimepicker({
                format: 'DD/MM/YYYY',
                //ignoreReadonly: true
            });
            $('.show-filter-to-onclick').click(function () {
                $('.datetimepicker-filter-to').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-to').on('dp.change', function (e) {
                var dateFrom = $('.datetimepicker-filter-from').data("DateTimePicker").date();
                if (e.date < dateFrom) {
                    $('.datetimepicker-filter-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                <%--$('#<%= txtFilterIndexPermTempToHidden.ClientID %>').val($('.datetimepicker-filter-to').val());--%>
            });
            $('.datetimepicker-filter-only').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });
            $('.show-filter-only-onclick').click(function () {
                $('.datetimepicker-filter-only').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-only').on('dp.change', function (e) {
                <%--$('#<%= txtFilterIndexPermTempOnlyHidden.ClientID %>').val($('.datetimepicker-filter-only').val());--%>
            });
            /* END Filter Datetimepicker Permission Temporary */
            /* START Filter Datetimepicker Permission Permanent */
            $('.datetimepicker-filter-perm-from').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });
            $('.datetimepicker-filter-perm-from').on('dp.change', function (e) {
                var dateTo = $('.datetimepicker-filter-perm-to').data("DateTimePicker").date();
                if (e.date > dateTo) {
                    $('.datetimepicker-filter-perm-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                <%--$('#<%= txtFilterIndexPermPermFromHidden.ClientID %>').val($('.datetimepicker-filter-perm-from').val());--%>
            });
            $('.show-filter-perm-from-onclick').click(function () {
                $('.datetimepicker-filter-perm-from').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-perm-to').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });
            $('.show-filter-perm-to-onclick').click(function () {
                $('.datetimepicker-filter-perm-to').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-perm-to').on('dp.change', function (e) {
                var dateFrom = $('.datetimepicker-filter-perm-from').data("DateTimePicker").date();
                if (e.date < dateFrom) {
                    $('.datetimepicker-filter-perm-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                <%--$('#<%= txtFilterIndexPermPermToHidden.ClientID %>').val($('.datetimepicker-filter-perm-to').val());--%>
            });
            $('.datetimepicker-filter-perm-only').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });
            $('.show-filter-perm-only-onclick').click(function () {
                $('.datetimepicker-filter-perm-only').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-perm-only').on('dp.change', function (e) {
                <%--$('#<%= txtFilterIndexPermPermOnlyHidden.ClientID %>').val($('.datetimepicker-filter-perm-only').val());--%>
            });
            /* END Filter Datetimepicker Permission Permanent */
        });
    </script>

    <script type="text/javascript">

        //no post back
        $('.clockpicker').datetimepicker({
            format: 'HH:mm',
            stepping: 15,
            ignoreReadonly: true
        });
        $('.show-time-onclick').click(function () {
            $('.clockpicker').data("DateTimePicker").show();
        });

        $('.clockpickerto').datetimepicker({
            format: 'HH:mm',
            stepping: 15,
            ignoreReadonly: true
        });
        $('.show-timeto-onclick').click(function () {
            $('.clockpickerto').data("DateTimePicker").show();
        });

        //is
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {

            $('.loading-icon-approve').hide();

            $('.clockpicker').datetimepicker({
                format: 'HH:mm',
                stepping: 15,
                ignoreReadonly: true
            });
            $('.show-time-onclick').click(function () {
                $('.clockpicker').data("DateTimePicker").show();
            });

            $('.clockpickerto').datetimepicker({
                format: 'HH:mm',
                stepping: 15,
                ignoreReadonly: true
            });
            $('.show-timeto-onclick').click(function () {
                $('.clockpickerto').data("DateTimePicker").show();
            });
        });


        function getAlertSuccess() {
            $("#success_alert").show();
            $("#success_alert").fadeTo(500, 500).slideUp(500, function () {
                $("#success_alert").slideUp(500);
            });
        }

        function ValidatePage(toHide, toShow) {

            $(toHide).hide();
            $(toShow).show();

        }
    </script>

    <script>
        $(function () {
            $('.UploadFileCar').MultiFile({

            });

        });
        $(function () {
            $('.UploadFile_UseCar').MultiFile({

            });

        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.UploadFileCar').MultiFile({

                });

            });

            $(function () {
                $('.UploadFile_UseCar').MultiFile({

                });

            });
        });

    </script>

    <script type="text/javascript">
        $(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });

        //});

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            });
        });

    </script>

    <script type="text/javascript">

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('#calendar').fullCalendar({
                    theme: true,

                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'month,agendaWeek,agendaDay,listWeek,listDay'
                    },

                    nowIndicator: true,


                    views: {
                        listDay: { buttonText: 'list day' },
                        listWeek: { buttonText: 'list week' }
                    },

                    defaultView: 'month',

                    dayCount: 7,
                    timeFormat: 'h:mm a',
                    allDayText: 'all-day', // set replace all-day
                    selectable: false,
                    selectHelper: false,

                    //select: selectDate, //allDay: true,// this decides if the all day slot should be showed at the top
                    editable: false,
                    events: '<%= ResolveUrl("~/websystem/hr/JsonResponseCar.ashx") %>',
                    eventDrop: eventDropped,
                    eventResize: eventResized,
                    eventLimit: true,

                    eventClick: function (event, jsEvent, view) {
                        $('#modalTitle').html(
                            '<strong>' + 'รถที่ใช้' + ' : ' + event.car_use_name + '</strong>'
                        );
                        $('#modalBody').html(
                            //event.description
                            '<strong>' + 'สถานที่ : ' + '</strong>' + event.place_name + '<br>'
                            + '<strong>' + 'ประเภทการจอง : ' + '</strong>' + event.type_booking_name + '<br>'
                            + '<strong>' + 'ประเภทรถ : ' + '</strong>' + event.detailtype_car_name + '<br>'
                            + '<strong>' + 'เลขทะเบียน : ' + '</strong>' + event.car_register + '<br>'
                            + '<strong>' + 'ชื่อผู้จอง : ' + '</strong>' + event.emp_name_th + '<br>'
                            + '<strong>' + 'ตั้งแต่วันที่ : ' + '</strong>' + event.start.format("DD/MM/YYYY h:mm a") + '<br>'
                            + '<strong>' + 'ถึงวันที่ : ' + '</strong>' + event.end.format("DD/MM/YYYY h:mm a") + '<br>'
                            + '<strong>' + 'รายละเอียดเพิ่มเติม : ' + '</strong>' + event.description + '<br>'
                        );
                        $('#eventUrl').attr('href', event.url);
                        $('#fullCalModal').modal();
                    },

                    eventRender: function (event, element) {
                        var today = moment(new Date()).format("YYYY-MM-DD");
                        //var start = moment(event.start).format("YYYY-MM-DD");
                        var date_end = moment(event.end, "YYYY-MM-DD");
                        var date_start = moment(event.start, "YYYY-MM-DD");

                        //var current = moment().startOf('day');

                        //alert(moment.duration(given.diff(today)).asDays());

                        if (event != null) {
                            element.qtip({
                                content: {
                                    text: qTipText(event.topic_booking, event.place_name, event.start, event.end, event.description, event.car_register, event.detailtype_car_name, event.car_use_name),
                                    //text: qTipText(event.topic_booking, event.place_name, event.room_name_th, event.start, event.end, event.description),//qTipText(event.start, event.end, event.description, event.topic_booking),
                                    //title: '<strong>' + 'ทะเบียน' + ' : ' + event.car_register + '</strong>'
                                    title: '<strong>' + 'รถที่ใช้' + ' : ' + event.car_use_name + '</strong>'
                                    //title: '<strong>' + 'หัวข้อ' + ' : ' + event.title + '</strong>'
                                },
                                position: {
                                    my: 'top left',
                                    at: 'center left'
                                    //my: 'bottom left',
                                    //at: 'left center'
                                },
                                hide: {
                                    event: 'mouseout'
                                },
                                show: {
                                    event: 'mouseover'
                                },
                                style: {
                                    //classes: 'qtip-shadow qtip-rounded qtip-youtube'
                                    classes: 'qtip-tipsy'
                                }

                            });

                            var now = moment().format('YYYY-MM-DD') + " " + "24:00:00";//"04/09/2013 15:00:00";
                            //var now = moment().format('YYYY-MM-DD HH:mm:ss');//"04/09/2013 15:00:00";
                            //var then = "2018-09-28 14:00:00";
                            var then = moment(event.start).format('YYYY-MM-DD HH:mm');
                            var then_end = moment(event.end).format('YYYY-MM-DD HH:mm');

                            var ms = moment(now, "YYYY-MM-DD HH:mm:ss").diff(moment(then, "YYYY-MM-DD HH:mm:ss"));
                            var d = moment.duration(ms);
                            //var s = Math.floor(d.asHours()) + moment.utc(ms).format(":mm:ss");
                            var s_start = Math.floor(d.asHours());

                            var ms_end = moment(now, "YYYY-MM-DD HH:mm:ss").diff(moment(then_end, "YYYY-MM-DD HH:mm:ss"));
                            var d_end = moment.duration(ms_end);
                            //var s = Math.floor(d.asHours()) + moment.utc(ms).format(":mm:ss");
                            var s_end = Math.floor(d_end.asHours());

                            if (s_start >= 24 && s_end >= 24) {
                                element.css('background-color', '#D02090');//pink
                                element.find(".fc-event-dot").css('background-color', '#D02090')
                            }
                            else if ((s_start >= 0 && s_end >= 0) || (s_start >= 0 && s_end < 0)) {
                                element.css('background-color', '#00CC00');
                                element.find(".fc-event-dot").css('background-color', '#00CC00')//green
                            }
                            else {
                                element.css('background-color', '#1E90FF');
                                element.find(".fc-event-dot").css('background-color', '#1E90FF')
                            }

                            //var now = moment().format('YYYY-MM-DD HH:mm:ss');//"04/09/2013 15:00:00";
                            ////var then = "2018-09-28 14:00:00";
                            //var then = moment(event.start).format('YYYY-MM-DD HH:mm');
                            //var then_end = moment(event.end).format('YYYY-MM-DD HH:mm');

                            //var ms = moment(now, "YYYY-MM-DD HH:mm:ss").diff(moment(then, "YYYY-MM-DD HH:mm:ss"));
                            //var d = moment.duration(ms);
                            ////var s = Math.floor(d.asHours()) + moment.utc(ms).format(":mm:ss");
                            //var s_start = Math.floor(d.asHours());

                            //var ms_end = moment(now, "YYYY-MM-DD HH:mm:ss").diff(moment(then_end, "YYYY-MM-DD HH:mm:ss"));
                            //var d_end = moment.duration(ms_end);
                            ////var s = Math.floor(d.asHours()) + moment.utc(ms).format(":mm:ss");
                            //var s_end = Math.floor(d_end.asHours());

                            ////alert(s_start + ',' + moment(event.start).format('YYYY-MM-DD HH:mm') + ',' + s_end + ',' + moment(event.end).format('YYYY-MM-DD HH:mm'));
                            //if (s_start > 24 && s_end > 0) {
                            //    //alert("1");
                            //    element.css('background-color', '#D02090');//pink
                            //    element.find(".fc-event-dot").css('background-color', '#D02090')
                            //}
                            //else if ((s_start > 0 && s_start <= 24) || (s_start > 0 && s_start > 24 && s_end <= 0)) {
                            //    //alert("2");
                            //    element.css('background-color', '#00CC00');
                            //    element.find(".fc-event-dot").css('background-color', '#00CC00')//green
                            //    //element.find(".fc-event-dot").after($("<span class=\"fc-event-icons\"></span>").html("Whatever you want the content of the span to be")); 
                            //}
                            //else if (s_start < 0) {
                            //    //alert("3");
                            //    element.css('background-color', '#1E90FF');
                            //    element.find(".fc-event-dot").css('background-color', '#1E90FF')
                            //}

                        }

                    }
                    //defaultView: 'listWeek'

                });
            });
        });
        $(function () {
            $('#calendar').fullCalendar({
                theme: true,

                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay,listWeek,listDay'
                },

                //eventColor: '#1E90FF',

                nowIndicator: true,

                views: {
                    listDay: { buttonText: 'list day' },
                    listWeek: { buttonText: 'list week' }
                },


                defaultView: 'month',
                dayCount: 7,

                timeFormat: 'h:mm a',
                allDayText: 'all-day', // set replace all-day
                //eventClick: updateEvent,
                selectable: false,
                selectHelper: false,

                //allDay: true,// this decides if the all day slot should be showed at the top
                //select: selectDate,
                editable: false,
                events: '<%= ResolveUrl("~/websystem/hr/JsonResponseCar.ashx") %>',
                eventDrop: eventDropped,
                eventResize: eventResized,
                eventLimit: true,

                eventClick: function (event, jsEvent, view) {
                    $('#modalTitle').html(
                        '<strong>' + 'รถที่ใช้' + ' : ' + event.car_use_name + '</strong>'
                    );
                    $('#modalBody').html(
                        //event.description
                        '<strong>' + 'สถานที่ : ' + '</strong>' + event.place_name + '<br>'
                        + '<strong>' + 'ประเภทการจอง : ' + '</strong>' + event.type_booking_name + '<br>'
                        + '<strong>' + 'ประเภทรถ : ' + '</strong>' + event.detailtype_car_name + '<br>'
                        + '<strong>' + 'เลขทะเบียน : ' + '</strong>' + event.car_register + '<br>'
                        + '<strong>' + 'ชื่อผู้จอง : ' + '</strong>' + event.emp_name_th + '<br>'
                        + '<strong>' + 'ตั้งแต่วันที่ : ' + '</strong>' + event.start.format("DD/MM/YYYY h:mm a") + '<br>'
                        + '<strong>' + 'ถึงวันที่ : ' + '</strong>' + event.end.format("DD/MM/YYYY h:mm a") + '<br>'
                        + '<strong>' + 'รายละเอียดเพิ่มเติม : ' + '</strong>' + event.description + '<br>'
                    );
                    $('#eventUrl').attr('href', event.url);
                    $('#fullCalModal').modal();
                },

                eventRender: function (event, element) {
                    var today = moment(new Date()).format("YYYY-MM-DD")
                    //var start = moment(event.start).format("YYYY-MM-DD");

                    var date_start = moment(event.start, "YYYY-MM-DD");
                    var date_end = moment(event.end, "YYYY-MM-DD");
                    //var current = moment().startOf('day');



                    if (event != null) {
                        element.qtip({
                            content: {
                                text: qTipText(event.topic_booking, event.place_name, event.start, event.end, event.description, event.car_register, event.detailtype_car_name, event.car_use_name),
                                //text: qTipText(event.topic_booking, event.place_name, event.start, event.end, event.description),
                                //text: qTipText(event.topic_booking, event.place_name, event.room_name_th, event.start, event.end, event.description),//qTipText(event.start, event.end, event.description, event.topic_booking),
                                title: '<strong>' + 'รถที่ใช้' + ' : ' + event.car_use_name + '</strong>'
                                //title: '<strong>' + 'หัวข้อ' + ' : ' + event.title + '</strong>'
                            },
                            position: {
                                my: 'top left',
                                at: 'center left'
                                //target: 'mouse',
                                //adjust: {
                                //    mouse: false
                                //}
                                //my: 'bottom left',Panel_AddCarUseHr
                                //at: 'left center'
                            },
                            hide: {
                                event: 'mouseout'
                            },
                            show: {
                                event: 'mouseover'
                                //solo: true 
                            },
                            style: {
                                //classes: 'qtip-shadow  qtip-youtube'
                                classes: 'qtip-tipsy'
                            }

                        });

                        var now = moment().format('YYYY-MM-DD') + " " + "24:00:00";//"04/09/2013 15:00:00";
                        //var now = moment().format('YYYY-MM-DD HH:mm:ss');//"04/09/2013 15:00:00";
                        //var then = "2018-09-28 14:00:00";
                        var then = moment(event.start).format('YYYY-MM-DD HH:mm');
                        var then_end = moment(event.end).format('YYYY-MM-DD HH:mm');

                        var ms = moment(now, "YYYY-MM-DD HH:mm:ss").diff(moment(then, "YYYY-MM-DD HH:mm:ss"));
                        var d = moment.duration(ms);
                        //var s = Math.floor(d.asHours()) + moment.utc(ms).format(":mm:ss");
                        var s_start = Math.floor(d.asHours());

                        var ms_end = moment(now, "YYYY-MM-DD HH:mm:ss").diff(moment(then_end, "YYYY-MM-DD HH:mm:ss"));
                        var d_end = moment.duration(ms_end);
                        //var s = Math.floor(d.asHours()) + moment.utc(ms).format(":mm:ss");
                        var s_end = Math.floor(d_end.asHours());

                        if (s_start >= 24 && s_end >= 24) {
                            element.css('background-color', '#D02090');//pink
                            element.find(".fc-event-dot").css('background-color', '#D02090')
                        }
                        else if ((s_start >= 0 && s_end >= 0) || (s_start >= 0 && s_end < 0)) {
                            element.css('background-color', '#00CC00');
                            element.find(".fc-event-dot").css('background-color', '#00CC00')//green
                        }
                        else {
                            element.css('background-color', '#1E90FF');
                            element.find(".fc-event-dot").css('background-color', '#1E90FF')
                        }




                    }
                }
                //defaultView: 'listWeek'


            });
        });

    </script>



</asp:Content>

