﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="qa_cims_m2_form_calculate.aspx.cs" Inherits="websystem_MasterData_qa_cims_m2_form_calculate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:HyperLink ID="setFocus" runat="server"></asp:HyperLink>
    <asp:MultiView ID="mvMultiview" runat="server" ActiveViewIndex="0">
        <asp:View ID="docManageSignature" runat="server">
            <div class="form-group">
                <asp:LinkButton ID="btnAddSignature" CssClass="btn btn-primary" Visible="true" data-original-title="เพิ่มแบบฟอร์มสำหรับการเซ็น" data-toggle="tooltip" title="เพิ่มแบบฟอร์มสำหรับการเซ็น" runat="server"
                    CommandName="cmdAddSignature" OnCommand="btnCommand"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;เพิ่มแบบฟอร์มสำหรับการเซ็น</asp:LinkButton>
            </div>

            <asp:FormView ID="fvSignature" runat="server" DefaultMode="readonly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <asp:Label ID="showNameForm" Text="เพิ่มแบบฟอร์มสำหรับการเซ็น" runat="server"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <asp:Label ID="lbTopicNameSignature" runat="server" Text="หัวข้อในการเซ็นชื่อ" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtTopicNameSignature" placeholder="กรอกหัวข้อในการเซ็นชื่อ ตัวอย่างเช่น ผู้ตรวจสอบ..." runat="server" CssClass="form-control" Enabled="true" />
                                        <asp:RequiredFieldValidator ID="ReqtxtTopicNameSignature" runat="server"
                                            ControlToValidate="txtTopicNameSignature" Display="None" SetFocusOnError="true"
                                            ErrorMessage="*กรุณากรอกหัวข้อการเซ็นชื่อ" ValidationGroup="save_signature" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValtxtTopicNameSignature" runat="Server"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqtxtTopicNameSignature" Width="220" />
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="lbNameSignaturePosition" runat="server" Text="ชื่อตำแหน่งที่เกี่ยวข้อง" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtNameSignaturePosition" placeholder="กรอกชื่อตำแหน่งที่เกี่ยวข้อง..." runat="server" CssClass="form-control" Enabled="true" />
                                        <asp:RequiredFieldValidator ID="ReqtxtNameSignaturePosition" runat="server"
                                            ControlToValidate="txtNameSignaturePosition" Display="None" SetFocusOnError="true"
                                            ErrorMessage="*กรุณากรอกชื่อตำแหน่งที่เกี่ยวข้อง" ValidationGroup="save_signature" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValtxtNameSignaturePosition" runat="Server"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqtxtNameSignaturePosition" Width="220" />
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="lbdatetime_signature" runat="server" Text="ช่องสำหรับวันที่เซ็นเอกสาร" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtDatetimeSignature" placeholder="กรอกช่องสำหรับวันที่เซ็นเอกสาร..." runat="server"
                                            Text="(........../........../..........)" CssClass="form-control" Enabled="true" />
                                        <asp:RequiredFieldValidator ID="ReqtxtDatetimeSignature" runat="server"
                                            ControlToValidate="txtDatetimeSignature" Display="None" SetFocusOnError="true"
                                            ErrorMessage="*กรุณากรอกช่องสำหรับวันที่เซ็นเอกสาร" ValidationGroup="save_signature" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValitxtDatetimeSignature" runat="Server"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqtxtDatetimeSignature" Width="220" />
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="lbposition" runat="server" Text="ตำแหน่งของการจัดวาง" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-6">
                                        <asp:DropDownList ID="ddl_position" runat="server" CssClass="form-control" Enabled="true" />
                                        <asp:RequiredFieldValidator ID="Reqddl_position" runat="server" InitialValue="0"
                                            ControlToValidate="ddl_position" Display="None" SetFocusOnError="true"
                                            ErrorMessage="*กรุณาเลือกตำแหน่งของการจัดวาง" ValidationGroup="save_signature" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="Valddl_position" runat="Server"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Reqddl_position" Width="220" />
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="lbform_name" runat="server" Text="ชื่อฟอร์ม" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-6">
                                        <asp:DropDownList ID="ddl_form_name" runat="server" CssClass="form-control" Enabled="true" />
                                        <asp:RequiredFieldValidator ID="Requddl_form_name" runat="server" InitialValue="0"
                                            ControlToValidate="ddl_form_name" Display="None" SetFocusOnError="true"
                                            ErrorMessage="*กรุณาเลือกชื่อฟอร์ม" ValidationGroup="save_signature" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="Valddl_form_name" runat="Server"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requddl_form_name" Width="220" />
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="lbl_form_name_status" runat="server" Text="สถานะ" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-6">
                                        <asp:DropDownList ID="ddl_status_form_name" runat="server" CssClass="form-control" Enabled="true">
                                            <asp:ListItem Value="1">Online</asp:ListItem>
                                            <asp:ListItem Value="0">Offline</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-2 col-sm-offset-3">
                                        <asp:LinkButton ID="btnSaveSignature" ValidationGroup="save_signature" CssClass="btn btn-success" Text="Save" data-toggle="tooltip" title="บันทึก" runat="server" CommandName="cmdSaveSignature" OnCommand="btnCommand"></asp:LinkButton>
                                        <asp:LinkButton ID="btnCancelSignature" CssClass="btn btn-danger" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="cmdCancelSignature" data-toggle="tooltip" title="ยกเลิก"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:GridView ID="gvSignature" runat="server" Visible="true"
                AutoGenerateColumns="false" Font-Size="9.5" HeaderStyle-Font-Size="9.5"
                CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                HeaderStyle-CssClass="success"
                AllowPaging="true"
                PageSize="10"
                OnPageIndexChanging="gvPageIndexChanging"
                OnRowEditing="gvRowEditing"
                OnRowUpdating="gvRowUpdating"
                OnRowCancelingEdit="gvRowCancelingEdit"
                OnRowDataBound="gvRowDataBound"
                AutoPostBack="false">
                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                <EmptyDataTemplate>
                    <div style="text-align: center;"><b>ไม่มีข้อมูล</b> </div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Width="1%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style="text-align: center;">
                                <asp:Label ID="lbm2_formcal_idx" runat="server" Visible="false" Text='<%# Eval("m2_formcal_idx") %>' />
                                <%# (Container.DataItemIndex +1) %>
                            </div>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txt_m2_formcal_idx" Visible="false" runat="server" CssClass="form-control " Text='<%# Eval("m2_formcal_idx") %>'></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label1" CssClass="col-sm-3 control-label" runat="server" Text="ตำแหน่งที่จัดวาง" />
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txt_update_m0_formcal_idx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("m0_formcal_idx") %>'></asp:TextBox>
                                            <asp:DropDownList ID="ddl_formcal_update" runat="server" CssClass="form-control"></asp:DropDownList>
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="lbupdate_m2_signature_name" CssClass="col-sm-3 control-label" runat="server" Text="หัวข้อการเซ็นชื่อ" />
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txt_update_signature_name" runat="server" CssClass="form-control " Text='<%# Eval("m2_signature_name") %>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="Requiredtxt_update_signature_name" runat="server"
                                                ControlToValidate="txt_update_signature_name" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกหัวข้อการเซ็นชื่อ" ValidationGroup="update_signature_name" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="Valtxt_update_signature_name" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxt_update_signature_name" Width="220" />
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="lbNameSignaturePosition_update" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อตำแหน่งที่เกี่ยวข้อง" />
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txt_update_signature_position" runat="server" CssClass="form-control " Text='<%# Eval("m2_position_signature") %>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="Reqtxt_update_signature_position" runat="server"
                                                ControlToValidate="txt_update_signature_position" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกชื่อตำแหน่งที่เกี่ยวข้อง" ValidationGroup="update_signature_name" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="Valitxt_update_signature_position" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Reqtxt_update_signature_position" Width="220" />
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="lbm2_select_detail_update" CssClass="col-sm-3 control-label" runat="server" Text="ตำแหน่งที่จัดวาง" />
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txt_update_select_detail" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("m2_select_detail") %>'></asp:TextBox>
                                            <asp:DropDownList ID="ddl_update_position" runat="server" CssClass="form-control"></asp:DropDownList>
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="lbdatetime_signature_update" CssClass="col-sm-3 control-label" runat="server" Text="ช่องวันที่" />
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txt_update_datetime_signature" runat="server" CssClass="form-control " Text='<%# Eval("m2_datetime_signature") %>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="Reqtxt_update_datetime_signature" runat="server"
                                                ControlToValidate="txt_update_datetime_signature" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกช่องวันที่" ValidationGroup="update_signature_name" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="Valtxt_update_datetime_signature" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Reqtxt_update_datetime_signature" Width="220" />
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="lbupdates_form_table_status" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddlUpdateSignatureStatus" Text='<%# Eval("m2_formcal_status") %>'
                                                CssClass="form-control" runat="server">
                                                <asp:ListItem Value="1">Online</asp:ListItem>
                                                <asp:ListItem Value="0">Offline</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-3"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-9">
                                            <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" ValidationGroup="update_signature_name" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                Text="Save" CommandName="Update"></asp:LinkButton>
                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ชื่อฟอร์ม" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Width="20%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <asp:Label ID="lbform_name_preview" runat="server" Visible="true" Text='<%# Eval("m0_formcal_name") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="หัวข้อการเซ็นชื่อ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Width="15%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <asp:Label ID="lbTopicNameSignature_preview" runat="server" Visible="true" Text='<%# Eval("m2_signature_name") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ชื่อตำแหน่งที่เกี่ยวข้อง" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Width="20%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <asp:Label ID="lbNameSignaturePosition_preview" runat="server" Visible="true" Text='<%# Eval("m2_position_signature") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ตำแหน่งที่จัดวาง" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <asp:Label ID="lbposition_preview" runat="server" Visible="true" Text='<%# Eval("m2_select_detail") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ช่องวันที่" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <asp:Label ID="lbposition" runat="server" Visible="true" Text='<%# Eval("m2_datetime_signature") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lbformcal_table_status" Visible="false" runat="server"
                                    CssClass="col-sm-12" Text='<%# Eval("m2_formcal_status") %>'></asp:Label>
                                <asp:Label ID="lbsignature_statusOnline" runat="server" Visible="false"
                                    data-toggle="tooltip" data-placement="top" title="Online"
                                    CssClass="col-sm-12">
                                                <div style="text-align: center; color:green;">
                                                    <span><b>Online</b></span>
                                </asp:Label>
                                <asp:Label ID="lbsignature_statusOffline" runat="server" Visible="false"
                                    data-toggle="tooltip" data-placement="top" title="Offline" CssClass="col-sm-12">
                                                <div style="text-align: center; color:red;">
                                                <span><b>Offline</b></span>
                                </asp:Label>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Width="9%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <asp:LinkButton ID="btn_edit_table" CssClass="text-edit" runat="server" CommandName="Edit"
                                data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                            <asp:LinkButton ID="btndeletetable" CssClass="text-trash" runat="server" CommandName="cmdDeleteSignature"
                                data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')"
                                CommandArgument='<%#Eval("m2_formcal_idx") %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>

        </asp:View>
    </asp:MultiView>

</asp:Content>

