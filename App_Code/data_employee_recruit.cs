﻿using System;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_employee_recruit")]
public class data_employee_recruit
{
    [XmlElement("return_code")]
    public string return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }
    [XmlElement("return_value")]
    public string return_value { get; set; }

    [XmlElement("employee_vdo_list")]
    public employee_vdo[] employee_vdo_list { get; set; }

    [XmlElement("topic_list")]
    public topic_data[] topic_list { get; set; }

    [XmlElement("type_answer_list")]
    public type_answer_data[] type_answer_list { get; set; }

    [XmlElement("question_list")]
    public question_data[] question_list { get; set; }//recruit_m0_question_data

    [XmlElement("requset_more_list")]
    public requset_more_data[] requset_more_list { get; set; }

    [XmlElement("recruit_u0_answer_list")]
    public recruit_u0_answer_data[] recruit_u0_answer_list { get; set; }

    [XmlElement("recruit_u1_answer_list")]
    public recruit_u1_answer_data[] recruit_u1_answer_list { get; set; }

    [XmlElement("exam_sp_list")]
    public exam_sp_detail[] exam_sp_list { get; set; }

}

[Serializable]
public class employee_vdo
{
    [XmlElement("rq_idx")]
    public int rq_idx { get; set; }
    [XmlElement("EmpIDX")]
    public int EmpIDX { get; set; }
    [XmlElement("rq_vdo")]
    public int rq_vdo { get; set; }
    [XmlElement("vdo_name")]
    public string vdo_name { get; set; }
    [XmlElement("vdo_d_edit")]
    public int vdo_d_edit { get; set; }
    [XmlElement("identity_card")]
    public string identity_card { get; set; }
    [XmlElement("FirstNameTH")]
    public string FirstNameTH { get; set; }
    [XmlElement("LastNameTH")]
    public string LastNameTH { get; set; }
    [XmlElement("unidx")]
    public int unidx { get; set; }
    [XmlElement("acidx")]
    public int acidx { get; set; }
    [XmlElement("doc_decision")]
    public int doc_decision { get; set; }
}
[Serializable]
public class requset_more_data
{

    [XmlElement("rq_idx")]
    public int rq_idx { get; set; }
    [XmlElement("EmpIDX")]
    public int EmpIDX { get; set; }
    [XmlElement("rq_vdo")]
    public int rq_vdo { get; set; }
    [XmlElement("vdo_name")]
    public string vdo_name { get; set; }
    [XmlElement("vdo_d_edit")]
    public int vdo_d_edit { get; set; }
    [XmlElement("identity_card")]
    public string identity_card { get; set; }
    [XmlElement("FirstNameTH")]
    public string FirstNameTH { get; set; }
    [XmlElement("LastNameTH")]
    public string LastNameTH { get; set; }
    [XmlElement("rq_exam")]
    public int rq_exam { get; set; }
    [XmlElement("exam_status")]
    public int exam_status { get; set; }
    [XmlElement("unidx")]
    public int unidx { get; set; }
    [XmlElement("acidx")]
    public int acidx { get; set; }
    [XmlElement("doc_decision")]
    public int doc_decision { get; set; }
    [XmlElement("rq_resmore")]
    public int rq_resmore { get; set; }
    [XmlElement("resmore_status")]
    public int resmore_status { get; set; }

    [XmlElement("m0_tqidx")]
    public int m0_tqidx { get; set; }

    

}
[Serializable]
public class exam_sp_detail
{

    [XmlElement("FirstNameTH")]
    public string FirstNameTH { get; set; }
    [XmlElement("LastNameTH")]
    public string LastNameTH { get; set; }

    [XmlElement("EmpIDX")]
    public int EmpIDX { get; set; }
    [XmlElement("u0_anidx")]
    public int u0_anidx { get; set; }

    [XmlElement("unidx")]
    public int unidx { get; set; }
    [XmlElement("acidx")]
    public int acidx { get; set; }
    [XmlElement("doc_decision")]
    public int doc_decision { get; set; }
}

[Serializable]
public class topic_data
{
    [XmlElement("m0_toidx")]
    public int m0_toidx { get; set; }
    [XmlElement("m0_tqidx")]
    public int m0_tqidx { get; set; }
    [XmlElement("topic_name")]
    public string topic_name { get; set; }
    [XmlElement("orgidx")]
    public int orgidx { get; set; }
    [XmlElement("rdeptidx")]
    public int rdeptidx { get; set; }
    [XmlElement("rsecidx")]
    public int rsecidx { get; set; }
    [XmlElement("rposidx")]
    public int rposidx { get; set; }
    [XmlElement("status")]
    public int status { get; set; }
    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }
    [XmlElement("UEmpIDX")]
    public int UEmpIDX { get; set; }
    [XmlElement("CreateDate")]
    public string CreateDate { get; set; }
    [XmlElement("UpdateDate")]
    public string UpdateDate { get; set; }
}
[Serializable]
public class type_answer_data
{

    [XmlElement("m0_taidx")]
    public int m0_taidx { get; set; }
    [XmlElement("type_asn")]
    public string type_asn { get; set; }
    [XmlElement("status")]
    public int UEmpstatusIDX { get; set; }

}
[Serializable]
public class question_data
{

    [XmlElement("m0_quidx")]
    public int m0_quidx { get; set; }

    [XmlElement("m0_toidx")]
    public int m0_toidx { get; set; }

    [XmlElement("m0_taidx")]
    public int m0_taidx { get; set; }

    [XmlElement("no_choice")]
    public int no_choice { get; set; }

    [XmlElement("quest_name")]
    public string quest_name { get; set; }

    [XmlElement("choice_a")]
    public string choice_a { get; set; }

    [XmlElement("choice_b")]
    public string choice_b { get; set; }

    [XmlElement("choice_c")]
    public string choice_c { get; set; }

    [XmlElement("choice_d")]
    public string choice_d { get; set; }

    [XmlElement("choice_ans")]
    public int choice_ans { get; set; }

    [XmlElement("status")]
    public int status { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("UEmpIDX")]
    public int UEmpIDX { get; set; }

    [XmlElement("CreateDate")]
    public string CreateDate { get; set; }

    [XmlElement("UpdateDate")]
    public string UpdateDate { get; set; }


}

[Serializable]
public class recruit_u0_answer_data
{
    [XmlElement("topic_name")]
    public string topic_name { get; set; }

    [XmlElement("u0_anidx")]
    public int u0_anidx { get; set; }

    [XmlElement("m0_toidx")]
    public int m0_toidx { get; set; }

    [XmlElement("total_score")]
    public decimal total_score { get; set; }

    [XmlElement("score_choice")]
    public int score_choice { get; set; }

    [XmlElement("score_comment")]
    public decimal score_comment { get; set; }

    [XmlElement("unidx")]
    public int unidx { get; set; }

    [XmlElement("acidx")]
    public int acidx { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("node_decision")]
    public int node_decision { get; set; }

    [XmlElement("determine")]
    public string determine { get; set; }

    [XmlElement("status")]
    public int status { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("CreateDate")]
    public string CreateDate { get; set; }

    [XmlElement("UEmpIDX")]
    public int UEmpIDX { get; set; }

    [XmlElement("UpdateDate")]
    public string UpdateDate { get; set; }

    [XmlElement("check_exam")]
    public int check_exam { get; set; }
}

[Serializable]
public class recruit_u1_answer_data
{
    [XmlElement("u1_anidx")]
    public int u1_anidx { get; set; }

    [XmlElement("m0_quidx")]
    public int m0_quidx { get; set; }

    [XmlElement("m0_toidx")]
    public int m0_toidx { get; set; }

    [XmlElement("u0_anidx")]
    public int u0_anidx { get; set; }

    [XmlElement("answer")]
    public string answer { get; set; }

    [XmlElement("score")]
    public decimal score { get; set; }

    [XmlElement("status")]
    public int status { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("CreateDate")]
    public string CreateDate { get; set; }

    [XmlElement("UEmpIDX")]
    public int UEmpIDX { get; set; }

    [XmlElement("UpdateDate")]
    public string UpdateDate { get; set; }
}



