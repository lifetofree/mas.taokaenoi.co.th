﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

using DotNet.Highcharts;
using DotNet.Highcharts.Options;
using DotNet.Highcharts.Helpers;
using System.Collections;
using System.Web.Script.Serialization;
using Image = System.Web.UI.WebControls.Image;

public partial class websystem_hr_employeeform : System.Web.UI.Page
{

    #region Connect

    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();
    
    private string ConCL = "conn_centralized";

    //dataODSP_Relation dtODSP = new dataODSP_Relation();
    data_employee _dtEmployee = new data_employee();

    //function_scan _funcScan = new function_scan();
    function_db _funcdb = new function_db();
    data_employee _dataEmployee = new data_employee();
    data_tpm_form _dtpmform = new data_tpm_form();

    //data cen
    data_cen_master _data_cen_master = new data_cen_master();
    data_cen_employee _data_cen_employee = new data_cen_employee ();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string urlGetEmployee = _serviceUrl + ConfigurationManager.AppSettings["urlGetEmployee"];
    static string urlGetEmployeelist = _serviceUrl + ConfigurationManager.AppSettings["urlGetEmployeeList"];
    static string _urlGetCostcenterOld = _serviceUrl + ConfigurationManager.AppSettings["urlGetCostcenterOld"];
    static string _urlGetHospitalOld = _serviceUrl + ConfigurationManager.AppSettings["urlGetHospitalOld"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    static string _urlGetPositionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetPositionList"];

    static string _urlGetCountryList = _serviceUrl + ConfigurationManager.AppSettings["urlGetCountryList"];
    static string _urlGetProvincetList = _serviceUrl + ConfigurationManager.AppSettings["urlGetProvincetList"];
    static string _urlGetAmphurList = _serviceUrl + ConfigurationManager.AppSettings["urlGetAmphurList"];
    static string _urlGetDistList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDistList"];
    static string _urlGetApproveLonList = _serviceUrl + ConfigurationManager.AppSettings["urlGetApproveLonList"];
    static string _UrlSetEmployeeList = _serviceUrl + ConfigurationManager.AppSettings["UrlSetEmployeeList"];
    static string _urlSetODSPList = _serviceUrl + ConfigurationManager.AppSettings["urlSetODSPList"];
    static string _urlSetODSPList_Update = _serviceUrl + ConfigurationManager.AppSettings["urlSetODSPList_Update"];
    static string _urlSetODSPList_Delete = _serviceUrl + ConfigurationManager.AppSettings["urlSetODSPList_Delete"];
    static string _urlGetChildList = _serviceUrl + ConfigurationManager.AppSettings["urlGetChildList"];
    static string _urlGetPriorList = _serviceUrl + ConfigurationManager.AppSettings["urlGetPriorList"];
    static string _urlGetEducationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetEducationList"];
    static string _urlGetTrainList = _serviceUrl + ConfigurationManager.AppSettings["urlGetTrainList"];
    static string _urlSetUpdateChildList = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateChildList"];
    static string _urlSetUpdatePriorList = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdatePriorList"];
    static string _urlSetUpdateEducationList = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateEducationList"];
    static string _urlSetUpdateTrainList = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateTrainList"];
    static string _urlSetDeleteChildList = _serviceUrl + ConfigurationManager.AppSettings["urlSetDeleteChildList"];
    static string _urlSetDeletePriorList = _serviceUrl + ConfigurationManager.AppSettings["urlSetDeletePriorList"];
    static string _urlSetDeleteEducationList = _serviceUrl + ConfigurationManager.AppSettings["urlSetDeleteEducationList"];
    static string _urlSetDeleteTrainList = _serviceUrl + ConfigurationManager.AppSettings["urlSetDeleteTrainList"];
    static string _urlSetDeleteEmployeeListList = _serviceUrl + ConfigurationManager.AppSettings["urlSetDeleteEmployeeList"];
    static string _urlSetResetpass_employeeListList = _serviceUrl + ConfigurationManager.AppSettings["urlSetResetpass_employeeList"];
    static string _urlurlSetInsertEmployeeList = _serviceUrl + ConfigurationManager.AppSettings["urlSetInsertEmployeeList"];
    static string _urlSetUpdateDetail_employeeList = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateDetail_employeeList"];
    static string _urlSetApprove_employeeList = _serviceUrl + ConfigurationManager.AppSettings["urlSetApprove_employeeList"];
    static string _urlSetupdateapprove_employeeList = _serviceUrl + ConfigurationManager.AppSettings["urlSetupdateapprove_employeeList"];
    static string _urlSelect_Location = _serviceUrl + ConfigurationManager.AppSettings["urlGetLocation"];
    static string _urlGetNationalityList = _serviceUrl + ConfigurationManager.AppSettings["urlGetNationalityList"];
    static string _urlGetReferList = _serviceUrl + ConfigurationManager.AppSettings["urlGetReferList"];
    static string _urlSetUpdateReferList = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateReferList"];
    static string _urlSetDeleteReferList = _serviceUrl + ConfigurationManager.AppSettings["urlSetDeleteReferList"];
    static string _urlGetSelectVisatype = _serviceUrl + ConfigurationManager.AppSettings["urlGetSelectVisatype"];
    static string _urlSetOrganizationList_shift = _serviceUrl + ConfigurationManager.AppSettings["urlSetOrganizationList_shift"];
    static string _urlGetM0_Asset_Select = _serviceUrl + ConfigurationManager.AppSettings["urlGetM0_Asset_Select"];
    static string _urlGetM0_UnitType_Select = _serviceUrl + ConfigurationManager.AppSettings["urlGetM0_UnitType_Select"];
    static string _urlSelect_Report_Probation = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Report_Probation"];
    static string _urlGetUpdateGurantee = _serviceUrl + ConfigurationManager.AppSettings["urlGetUpdateGurantee"];
    static string _urlGetDeleteGurantee = _serviceUrl + ConfigurationManager.AppSettings["urlGetDeleteGurantee"];
    static string _urlGetLeaveTypeList = _serviceUrl + ConfigurationManager.AppSettings["urlGetLeaveTypeList"];
    static string _urlSelectMaster = _serviceUrl + ConfigurationManager.AppSettings["urlSelectMaster_FormResult"];

    //data cen master
    static string _urlGetCenMasterList = _serviceUrl + ConfigurationManager.AppSettings["urlGetCenMasterList"];
    static string _urlCenGetCenMasterList = _serviceUrl + ConfigurationManager.AppSettings["urlCenGetCenMasterList"];
    static string _urlGetCenEmployeeList = _serviceUrl + ConfigurationManager.AppSettings["urlGetCenEmployeeList"];


    //get approve cen
    static string _urlgetApproveCen = _serviceUrl + ConfigurationManager.AppSettings["urlgetApproveCen"];
    static string _urlGetPositionListCen = _serviceUrl + ConfigurationManager.AppSettings["urlGetPositionListCen"];
    static string _urlSetUpdatePositionCen = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdatePositionCen"];  
    static string _urlSetDelPositionCen = _serviceUrl + ConfigurationManager.AppSettings["urlSetDelPositionCen"];
    
    


    static string imgPath = ConfigurationSettings.AppSettings["path_flie_employee"];

    int emp_idx = 0;
    int defaultInt = 0;
    decimal tot_actual_all_time = 0;
    decimal tot_actual_all_time_salary = 0;
    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";
    string _localJson = "";
    int _tempInt = 0;
    string temp = "";

    int _rpos_idx_insert = 11748;

    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        ViewState["RPos_idx_login"] = 0;
        ViewState["Org_idx_login"] = 0;

        _dtEmployee = callServiceEmpProfile(urlGetMyProfile + Session["emp_idx"].ToString());
        ViewState["RPos_idx_login"] = _dtEmployee.employee_list[0].rpos_idx;
        ViewState["Org_idx_login"] = _dtEmployee.employee_list[0].org_idx;

        ViewState["EmpAllList"] = "";

        //ViewState["EmpIDX_Print"] = "0";
        if (
            (int.Parse(ViewState["RPos_idx_login"].ToString()) == 1017 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 1018 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 1019 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5916 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5901 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5899 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5900 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5902 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5884 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5903 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5904 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5905 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5906 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5907 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5909 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5872 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5908 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5873 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5874 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5875 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5876 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5885 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5886 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5888 ||
			int.Parse(ViewState["RPos_idx_login"].ToString()) == 5889 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5989 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 3543 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 377 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 378 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 867 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5947 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5945 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 7135 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 7086 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 8150 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 7108 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 7062 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5891 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5878 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 8153 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5964 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 9182 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 9199 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 7085 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 7062 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 23069 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 11641 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 11761 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 11538 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 12207 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 11637 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 13667 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 11754 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 7124 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 13862 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5941
            )
            || int.Parse(ViewState["Org_idx_login"].ToString()) != 1
            )
        {

        }
        else
        {
            _funcTool.showAlert(this, "คุณไม่มีสิทธิ์ใช้งาน");
            Response.Redirect("~/");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

            
            //data table
            DataTableInsertRePosition();
            DataTableInsertEditRePosition();


            ViewState["EmpIDX"] = Session["emp_idx"].ToString();
            select_empIdx_present();
            Select_Employee_index();
            Menu_Color(1);
            MvMaster.SetActiveView(ViewIndex);

           
        }

        linkBtnTrigger(lbindex);
        linkBtnTrigger(lbadd);
        linkBtnTrigger(lbsetapprove);
        linkBtnTrigger(lbprobation);
        linkBtnTrigger(lblreport);

        // LinkButton btnSave = (LinkButton)FvInsertEmp.FindControl("btnSave");
        // linkBtnTrigger(btnSave);

    }

    protected void gridViewTrigger(GridView linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    protected void linkFvTrigger(FormView linkFvID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerFv = new PostBackTrigger();
        triggerFv.ControlID = linkFvID.UniqueID;
        updatePanel.Triggers.Add(triggerFv);
    }

    protected void GetPathPhoto(string idemp, Image imgname)
    {
        string getPathLotus = ConfigurationSettings.AppSettings["path_emp_profile"];
        string path = idemp + "/";
        string namefile = idemp + ".jpg";
        string pic = getPathLotus + path + namefile;

        string filePathLotus = Server.MapPath(getPathLotus + idemp);
        DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);
        SearchDirectories(myDirLotus, idemp);

        if (ViewState["path"].ToString() != "0")
        {
            imgname.Visible = true;
            imgname.ImageUrl = pic;
            imgname.Height = 150;
            imgname.Width = 150;
        }
        else
        {
            imgname.Visible = false;
        }
    }

    protected string MapURL(string path)
    {

        string appPath = Server.MapPath("~/").ToLower();
        return string.Format("/{0}", path.ToLower().Replace(appPath, "").Replace(@"\", "/"));
    }

    public void SearchDirectories(DirectoryInfo dir, String target)
    {

        string dirfiles = dir.ToString();

        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        DataRow dr = dt.NewRow();
        DataColumn dc = new DataColumn("FileName", typeof(string));
        dc = new DataColumn("Download", typeof(string));
        dt.Columns.Add("FileName");
        dt.Columns.Add("Download");

        try
        {
            FileInfo[] files = dir.GetFiles();
            int i = 0;
            foreach (FileInfo file in files)
            {
                if (file.Name.ToLower().IndexOf(target.ToLower()) > -1)
                {
                    string[] f = Directory.GetFiles(dirfiles, file.Name, SearchOption.AllDirectories);
                    dt.Rows.Add(file.Name);
                    dt.Rows[i][1] = f[0];
                    i++;
                }
            }

            ViewState["path"] = "1";
        }
        catch
        {
            ViewState["path"] = "0";
        }
    }


    /*[WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<string> GetCity(string prefixText, int count)
    {
        data_employee _dtEmployee = new data_employee();
        function_tool _funcTool = new function_tool();
        function_db _funcdb = new function_db();
        data_employee _dataEmployee = new data_employee();
        string _localJson = "";

        _dataEmployee.hospital_list = new Hospital[1];
        Hospital dtemployee = new Hospital();
        dtemployee.Name = prefixText;
        _dataEmployee.hospital_list[0] = dtemployee;

        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        // call services
        _localJson = _funcTool.callServicePost(_urlGetHospitalOld, _localJson);
        //fsa.Text = _cmdUrl + _localJson;

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        List<string> PositionTHAI = new List<string>();
        foreach (var Position in _dataEmployee.hospital_list)
        {
            PositionTHAI.Add(Position.Name);
        }

        return PositionTHAI;
    }*/

    #region INSERT&SELECT&UPDATE

    #region SELECT

    

    protected void GetEmployeeGroup(DropDownList ddlName)
    {


        data_cen_master _data_cen_master = new data_cen_master();
        search_cen_master_detail _search_cen_master_detail = new search_cen_master_detail();

        _search_cen_master_detail.s_status = "1";
        _data_cen_master.master_mode = "2";

        _data_cen_master.search_cen_master_list = new search_cen_master_detail[1];
        _data_cen_master.search_cen_master_list[0] = _search_cen_master_detail;

        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
        _data_cen_master = callServiceMaster(_urlGetCenMasterList, _data_cen_master);
        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));

        _funcTool.setDdlData(ddlName, _data_cen_master.cen_empgroup_list_m0, "empgroup_name_th", "empgroup_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกประเภทกลุ่มพนักงาน...", "0"));


    }

    protected void GetCostcenter(DropDownList ddlName)
    {


        data_cen_master _data_cen_master = new data_cen_master();
        search_cen_master_detail _search_cen_master_detail = new search_cen_master_detail();

        _search_cen_master_detail.s_status = "1";
        _data_cen_master.master_mode = "11";

        _data_cen_master.search_cen_master_list = new search_cen_master_detail[1];
        _data_cen_master.search_cen_master_list[0] = _search_cen_master_detail;

        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
        _data_cen_master = callServiceMaster(_urlGetCenMasterList, _data_cen_master);
        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));

        _funcTool.setDdlData(ddlName, _data_cen_master.cen_costcenter_list_m0, "cost_no", "cost_idx");
        ddlName.Items.Insert(0, new ListItem("เลือก Costcenter...", "0"));


    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */
    }

    protected void setDir(string dirName)
    {
        if (!Directory.Exists(Server.MapPath(dirName)))
        {
            Directory.CreateDirectory(Server.MapPath(dirName));
        }
    }

    protected void select_empIdx_present()
    {
        _dtEmployee = callServiceEmpProfile(urlGetMyProfile + ViewState["EmpIDX"].ToString());

        ViewState["rdept_name"] = _dtEmployee.employee_list[0].dept_name_th;
        ViewState["rdept_idx"] = _dtEmployee.employee_list[0].rdept_idx;
        ViewState["FullName"] = _dtEmployee.employee_list[0].emp_name_th;
        ViewState["Org_name"] = _dtEmployee.employee_list[0].org_name_th;
        ViewState["Org_idx"] = _dtEmployee.employee_list[0].org_idx;

        ViewState["EmpCode"] = _dtEmployee.employee_list[0].emp_code;
        ViewState["Positname"] = _dtEmployee.employee_list[0].pos_name_th;
        ViewState["Pos_idx"] = _dtEmployee.employee_list[0].rpos_idx;
        ViewState["Email"] = _dtEmployee.employee_list[0].emp_email;
        ViewState["Tel"] = _dtEmployee.employee_list[0].emp_mobile_no;
        ViewState["Secname"] = _dtEmployee.employee_list[0].sec_name_th;
        ViewState["Sec_idx"] = _dtEmployee.employee_list[0].rsec_idx;
        ViewState["CostIDX"] = _dtEmployee.employee_list[0].costcenter_idx;
        ViewState["jobgrade_level"] = _dtEmployee.employee_list[0].jobgrade_level;
        ViewState["return_code"] = 0;
        ViewState["Gv_emp_idx"] = 0;
        ViewState["Box_dataEmployee"] = null;
        ViewState["Box_dataEmployee_Report"] = null;
        Fv_Search_Emp_Index.ChangeMode(FormViewMode.Insert);
        Fv_Search_Emp_Index.DataBind();

        ViewState["vsTemp_Asset"] = null;
        var dsPosadd = new DataSet();
        dsPosadd.Tables.Add("Asset_edit");

        dsPosadd.Tables[0].Columns.Add("OrgIDX_add", typeof(int));
        dsPosadd.Tables[0].Columns.Add("OrgNameTH_S", typeof(String));
        dsPosadd.Tables[0].Columns.Add("midx_add", typeof(int));
        dsPosadd.Tables[0].Columns.Add("TypeWork_add", typeof(String));
        dsPosadd.Tables[0].Columns.Add("emp_idx_create_add", typeof(int));
        ViewState["vsTemp_Asset"] = dsPosadd;
    }

    protected void Select_Employee_index()
    {
        TextBox txtempcode_s = (TextBox)Fv_Search_Emp_Index.FindControl("txtempcode_s");
        TextBox txtempname_s = (TextBox)Fv_Search_Emp_Index.FindControl("txtempname_s");

        DropDownList ddlorg_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddlorg_s");
        DropDownList ddldep_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddldep_s");
        DropDownList ddlsec_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddlsec_s");
        DropDownList ddlpos_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddlpos_s");
        DropDownList ddlemptype_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddlemptype_s");
        DropDownList ddlempstatus_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddlempstatus_s");

        GridView GvEmployee = (GridView)ViewIndex.FindControl("GvEmployee");

        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _dataEmployee_ = new employee_detail();

        if (txtempname_s.Text != "")
        {
            _dataEmployee_.emp_name_th = txtempname_s.Text;
        }
        else
        {
            _dataEmployee_.emp_name_th = "";
        }

        if (txtempcode_s.Text != "")
        {
            _dataEmployee_.emp_code = txtempcode_s.Text;
        }
        else
        {
            _dataEmployee_.emp_code = "";
        }

        _dataEmployee_.org_idx = int.Parse(ddlorg_s.SelectedValue);
        _dataEmployee_.rdept_idx = int.Parse(ddldep_s.SelectedValue);
        _dataEmployee_.rsec_idx = int.Parse(ddlsec_s.SelectedValue);
        _dataEmployee_.rpos_idx = int.Parse(ddlpos_s.SelectedValue);
        _dataEmployee_.emp_type_idx = int.Parse(ddlemptype_s.SelectedValue);
        _dataEmployee_.emp_status = int.Parse(ddlempstatus_s.SelectedValue);
        _dataEmployee_.type_select_emp = 1; //Index     

        _dataEmployee.employee_list[0] = _dataEmployee_;
        _dataEmployee = callServiceEmployee(urlGetEmployee, _dataEmployee);
        ViewState["Box_dataEmployee_index_"] = _dataEmployee.employee_list;
        setGridData(GvEmployee, _dataEmployee.employee_list);

        employee_detail[] _templist_cheack = (employee_detail[])ViewState["Box_dataEmployee_index_"];
        var _linqCheck = (from dataMat in _templist_cheack

                          select new
                          {
                              dataMat
                          }).ToList();

        lblsum_list_index.Text = "จำนวน : " + _linqCheck.Count().ToString() + " รายการ / ทั้งหมด : " + GvEmployee.PageCount.ToString() + " หน้า";
    }

    protected void Select_Employee_index_search()
    {
        TextBox txtempcode_s = (TextBox)Fv_Search_Emp_Index.FindControl("txtempcode_s");
        TextBox txtempname_s = (TextBox)Fv_Search_Emp_Index.FindControl("txtempname_s");

        DropDownList ddlorg_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddlorg_s");
        DropDownList ddldep_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddldep_s");
        DropDownList ddlsec_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddlsec_s");
        DropDownList ddlpos_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddlpos_s");
        DropDownList ddlemptype_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddlemptype_s");
        DropDownList ddlempstatus_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddlempstatus_s");

        TextBox txtworkpermit_search = (TextBox)Fv_Search_Emp_Index.FindControl("txtworkpermit_search");
        TextBox txtpassport_serach = (TextBox)Fv_Search_Emp_Index.FindControl("txtpassport_serach");

        GridView GvEmployee = (GridView)ViewIndex.FindControl("GvEmployee");

        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _dataEmployee_ = new employee_detail();

        if (txtempname_s.Text != "")
        {
            _dataEmployee_.emp_name_th = txtempname_s.Text;
        }
        else
        {
            _dataEmployee_.emp_name_th = "";
        }

        if (txtempcode_s.Text != "")
        {
            _dataEmployee_.emp_code = txtempcode_s.Text;
        }
        else
        {
            _dataEmployee_.emp_code = "";
        }

        if (txtpassport_serach.Text != "")
        {
            _dataEmployee_.passportID = txtpassport_serach.Text;
        }
        else
        {
            _dataEmployee_.passportID = "";
        }

        if (txtworkpermit_search.Text != "")
        {
            _dataEmployee_.workpermitID = txtworkpermit_search.Text;
        }
        else
        {
            _dataEmployee_.workpermitID = "";
        }
        _dataEmployee_.org_idx = int.Parse(ddlorg_s.SelectedValue);
        _dataEmployee_.rdept_idx = int.Parse(ddldep_s.SelectedValue);
        _dataEmployee_.rsec_idx = int.Parse(ddlsec_s.SelectedValue);
        _dataEmployee_.rpos_idx = int.Parse(ddlpos_s.SelectedValue);
        _dataEmployee_.emp_type_idx = int.Parse(ddlemptype_s.SelectedValue);
        _dataEmployee_.emp_status = int.Parse(ddlempstatus_s.SelectedValue);
        _dataEmployee_.type_select_emp = 2; //Index     

        _dataEmployee.employee_list[0] = _dataEmployee_;
        //fss.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        _dataEmployee = callServiceEmployee(urlGetEmployee, _dataEmployee);
        ViewState["Box_dataEmployee_index"] = _dataEmployee.employee_list;
        setGridData(GvEmployee, _dataEmployee.employee_list);
    }

    protected void Select_Employee_report_search()
    {
        TextBox txtempcode_s = (TextBox)Fv_Search_Emp_Report.FindControl("txtempcode_s");
        TextBox txtempname_s = (TextBox)Fv_Search_Emp_Report.FindControl("txtempname_s");
        TextBox txtdatestart = (TextBox)Fv_Search_Emp_Report.FindControl("txtdatestart");
        TextBox txtdateend = (TextBox)Fv_Search_Emp_Report.FindControl("txtdateend");

        DropDownList ddlorg_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlorg_rp");
        DropDownList ddldep_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddldep_rp");
        DropDownList ddlsec_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlsec_rp");
        DropDownList ddlpos_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlpos_rp");
        DropDownList ddlemptype_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlemptype_s");
        DropDownList ddlempstatus_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlempstatus_s");
        DropDownList ddldatetype = (DropDownList)Fv_Search_Emp_Report.FindControl("ddldatetype");
        DropDownList ddljobgrade_start = (DropDownList)Fv_Search_Emp_Report.FindControl("ddljobgrade_start");
        DropDownList ddljobgrade_end = (DropDownList)Fv_Search_Emp_Report.FindControl("ddljobgrade_end");
        DropDownList ddlAffiliation_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlAffiliation_s");
        DropDownList ddllocation_search = (DropDownList)Fv_Search_Emp_Report.FindControl("ddllocation_search");
        DropDownList ddl_target_report = (DropDownList)Fv_Search_Emp_Report.FindControl("ddl_target_report");

        GridView GvEmployee = (GridView)ViewIndex.FindControl("GvEmployee");

        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _dataEmployee_ = new employee_detail();

        if (txtempname_s.Text != "")
        {
            _dataEmployee_.emp_name_th = txtempname_s.Text;
        }
        else
        {
            _dataEmployee_.emp_name_th = "";
        }

        if (txtempcode_s.Text != "")
        {
            _dataEmployee_.emp_code = txtempcode_s.Text;
        }
        else
        {
            _dataEmployee_.emp_code = "";
        }

        if (txtdatestart.Text != "")
        {
            _dataEmployee_.startdate_search = txtdatestart.Text;
        }
        else
        {
            _dataEmployee_.startdate_search = "";
        }

        if (txtdateend.Text != "")
        {
            _dataEmployee_.enddate_search = txtdateend.Text;
        }
        else
        {
            _dataEmployee_.enddate_search = "";
        }

        _dataEmployee_.org_idx = int.Parse(ddlorg_rp.SelectedValue);
        _dataEmployee_.rdept_idx = int.Parse(ddldep_rp.SelectedValue);
        _dataEmployee_.rsec_idx = int.Parse(ddlsec_rp.SelectedValue);
        _dataEmployee_.rpos_idx = int.Parse(ddlpos_rp.SelectedValue);
        _dataEmployee_.emp_type_idx = int.Parse(ddlemptype_s.SelectedValue);
        _dataEmployee_.emp_status = int.Parse(ddlempstatus_s.SelectedValue);
        _dataEmployee_.type_select_emp = 2; //Index     
        _dataEmployee_.datetype_search = int.Parse(ddldatetype.SelectedValue);
        _dataEmployee_.Search_jobgrade1 = int.Parse(ddljobgrade_start.SelectedValue);
        _dataEmployee_.Search_jobgrade2 = int.Parse(ddljobgrade_end.SelectedValue);
        _dataEmployee_.ACIDX = int.Parse(ddlAffiliation_s.SelectedValue);
        _dataEmployee_.LocIDX = int.Parse(ddllocation_search.SelectedValue);
        _dataEmployee_.TIDX = int.Parse(ddl_target_report.SelectedValue);

        _dataEmployee.employee_list[0] = _dataEmployee_;
        //adf.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        _dataEmployee = callServiceEmployee(urlGetEmployee, _dataEmployee);
        ViewState["Box_dataEmployee_Report"] = _dataEmployee.employee_list;
        setGridData(GvEmployee_Report, _dataEmployee.employee_list);
    }

    protected void Select_Employee_Export() // 1 - general , 2 - b-plus
    {
        TextBox txtempcode_s = (TextBox)Fv_Search_Emp_Report.FindControl("txtempcode_s");
        TextBox txtempname_s = (TextBox)Fv_Search_Emp_Report.FindControl("txtempname_s");
        TextBox txtdatestart = (TextBox)Fv_Search_Emp_Report.FindControl("txtdatestart");
        TextBox txtdateend = (TextBox)Fv_Search_Emp_Report.FindControl("txtdateend");

        DropDownList ddlorg_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlorg_rp");
        DropDownList ddldep_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddldep_rp");
        DropDownList ddlsec_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlsec_rp");
        DropDownList ddlpos_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlpos_rp");
        DropDownList ddlemptype_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlemptype_s");
        DropDownList ddlempstatus_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlempstatus_s");
        DropDownList ddldatetype = (DropDownList)Fv_Search_Emp_Report.FindControl("ddldatetype");
        DropDownList ddlAffiliation_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlAffiliation_s");

        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _dataEmployee_ = new employee_detail();

        if (txtempcode_s.Text != "")
        {
            _dataEmployee_.emp_code = txtempcode_s.Text;
        }
        else
        {
            _dataEmployee_.emp_code = "";
        }
        //else if (int.Parse(ViewState["return_code"].ToString()) > 0 && ViewState["return_code"].ToString() != "")
        //{
        //_dataEmployee_.emp_code = ViewState["return_code"].ToString();
        //}

        if (txtdatestart.Text != "")
        {
            _dataEmployee_.startdate_search = txtdatestart.Text;
        }
        else
        {
            _dataEmployee_.startdate_search = "";
        }

        if (txtdateend.Text != "")
        {
            _dataEmployee_.enddate_search = txtdateend.Text;
        }
        else
        {
            _dataEmployee_.enddate_search = "";
        }

        _dataEmployee_.emp_name_th = txtempname_s.Text;
        _dataEmployee_.org_idx = int.Parse(ddlorg_s.SelectedValue);
        _dataEmployee_.rdept_idx = int.Parse(ddldep_s.SelectedValue);
        _dataEmployee_.rsec_idx = int.Parse(ddlsec_s.SelectedValue);
        _dataEmployee_.rpos_idx = int.Parse(ddlpos_s.SelectedValue);
        _dataEmployee_.emp_type_idx = int.Parse(ddlemptype_s.SelectedValue);
        _dataEmployee_.emp_status = int.Parse(ddlempstatus_s.SelectedValue);
        _dataEmployee_.type_select_emp = 3; //General / Bplus
        _dataEmployee_.datetype_search = int.Parse(ddldatetype.SelectedValue);
        _dataEmployee_.ACIDX = int.Parse(ddlAffiliation_s.SelectedValue);

        _dataEmployee.employee_list[0] = _dataEmployee_;
        //adf.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        _dataEmployee = callServiceEmployee(urlGetEmployee, _dataEmployee);
        ViewState["Box_dataEmployee"] = _dataEmployee.employee_list;
    }

    protected void Select_Employee_Export_Exp_visa()
    {
        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _dataEmployee_ = new employee_detail();

        _dataEmployee_.type_select_emp = 5; //Exp. Visa

        _dataEmployee.employee_list[0] = _dataEmployee_;
        //adf.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        _dataEmployee = callServiceEmployee(urlGetEmployee, _dataEmployee);
        ViewState["Box_Visa_dataEmployee"] = _dataEmployee.employee_list;
    }

    protected void Select_ApproveEmployee(GridView GName, String Empcode)
    {
        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _dataEmployee_ = new employee_detail();

        _dataEmployee_.emp_code = Empcode;

        _dataEmployee.employee_list[0] = _dataEmployee_;
        _dataEmployee = callServiceEmployee(_urlSetApprove_employeeList, _dataEmployee);
        setGridData(GName, _dataEmployee.employee_list);

        //ViewState["vsTemp_Approve_Database"] = _dataEmployee;
    }

    protected void Select_FvEdit_Detail(int Empidx)
    {
        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _dataEmployee_ = new employee_detail();

        _dataEmployee_.emp_idx = Empidx;
        _dataEmployee_.type_select_emp = 19;//4;

        _dataEmployee.employee_list[0] = _dataEmployee_;
        _dataEmployee = callServiceEmployee(urlGetEmployee, _dataEmployee);
        //fss.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        //setFormData(FvEdit_Detail, _dataEmployee.employee_list);
        setFormData(FvEdit_Detail, _dataEmployee.employee_cen_list);
    }

    protected void Select_Fv_Detail(FormView FvName, String Empcode)
    {
        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _dataEmployee_ = new employee_detail();

        _dataEmployee_.emp_code = Empcode;
        _dataEmployee_.type_select_emp = 4;

        _dataEmployee.employee_list[0] = _dataEmployee_;
        _dataEmployee = callServiceEmployee(urlGetEmployee, _dataEmployee);
        setFormData(FvName, _dataEmployee.employee_list);
    }

    protected void Select_Coscenter(DropDownList ddlName)
    {
        ddlName.Items.Clear();
        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Add(new ListItem("เลือก CostCenter...", "0"));

        _dataEmployee.CostCenterDetail = new CostCenter[1];
        CostCenter dtemployee = new CostCenter();

        _dataEmployee.CostCenterDetail[0] = dtemployee;

        _dataEmployee = callServiceEmployee(_urlGetCostcenterOld, _dataEmployee);

        ddlName.DataSource = _dataEmployee.CostCenterDetail;
        ddlName.DataTextField = "costcenter_name";
        ddlName.DataValueField = "CostIDX";
        ddlName.DataBind();
    }

    protected void Select_Location(DropDownList ddlName)
    {
        ddlName.Items.Clear();
        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Add(new ListItem("ประจำสำนักงาน....", "0"));

        _dtEmployee = new data_employee();

        _dtEmployee.employee_list = new employee_detail[1];
        employee_detail search = new employee_detail();

        search.org_idx = int.Parse(ViewState["Org_idx"].ToString());

        _dtEmployee.employee_list[0] = search;

        _dtEmployee = callServiceEmployee(_urlSelect_Location, _dtEmployee);

        ddlName.DataSource = _dtEmployee.employee_list;
        ddlName.DataTextField = "LocName";
        ddlName.DataValueField = "LocIDX";
        ddlName.DataBind();
    }

    protected void Select_hospital(DropDownList ddlName)
    {
        ddlName.Items.Clear();
        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Add(new ListItem("เลือกโรงพยาบาล...", "0"));

        _dataEmployee.hospital_list = new Hospital[1];
        Hospital dtemployee = new Hospital();

        _dataEmployee.hospital_list[0] = dtemployee;

        _dataEmployee = callServiceEmployee(_urlGetHospitalOld, _dataEmployee);

        ddlName.DataSource = _dataEmployee.hospital_list;
        ddlName.DataTextField = "Name";
        ddlName.DataValueField = "HosIDX";
        ddlName.DataBind();
    }

    protected void select_org(DropDownList ddlName)
    {
        _dataEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dataEmployee.organization_list[0] = _orgList;

        _dataEmployee = callServiceEmployee(_urlGetOrganizationList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกองค์กร....", "0"));
    }

    protected void select_empshift(DropDownList ddlName, int org_idx)
    {
        _dataEmployee.ShiftTime_details = new ShiftTime[1];
        ShiftTime _orgList = new ShiftTime();
        _orgList.org_idx_shiftTime = org_idx;
        _orgList.type_selection = 1; //ddl
        _dataEmployee.ShiftTime_details[0] = _orgList;

        _dataEmployee = callServiceEmployee(_urlSetOrganizationList_shift, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.ShiftTime_details, "TypeWork", "midx");
        ddlName.Items.Insert(0, new ListItem("เลือกกะทำงาน....", "0"));
    }

    protected void select_dep(DropDownList ddlName, int _org_idx)
    {
        _dataEmployee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dataEmployee.department_list[0] = _deptList;

        _dataEmployee = callServiceEmployee(_urlGetDepartmentList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกฝ่าย....", "0"));
    }

    protected void select_dep_checkbox(CheckBoxList YrChkBox, int _org_idx)
    {
        YrChkBox.Items.Clear();
        YrChkBox.AppendDataBoundItems = true;

        _dataEmployee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dataEmployee.department_list[0] = _deptList;

        _dataEmployee = callServiceEmployee(_urlGetDepartmentList, _dataEmployee);
        YrChkBox.DataSource = _dataEmployee.department_list;
        YrChkBox.DataTextField = "dept_name_th";
        YrChkBox.DataValueField = "rdept_idx";
        YrChkBox.DataBind();
    }

    protected void select_sec(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {
        _dataEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _dataEmployee.section_list[0] = _secList;

        _dataEmployee = callServiceEmployee(_urlGetSectionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.section_list, "sec_name_th", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกหน่วยงาน....", "0"));
    }

    protected void select_pos(DropDownList ddlName, int _org_idx, int _rdept_idx, int _rsec_idx)
    {
        _dataEmployee.position_list = new position_details[1];
        position_details _posList = new position_details();
        _posList.org_idx = _org_idx;
        _posList.rdept_idx = _rdept_idx;
        _posList.rsec_idx = _rsec_idx;
        _dataEmployee.position_list[0] = _posList;

        _dataEmployee = callServiceEmployee(_urlGetPositionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.position_list, "pos_name_th", "rpos_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกตำแหน่ง....", "0"));
    }

    protected void select_country(DropDownList ddlName)
    {
        _dataEmployee.R0Address_list = new Address_List[1];
        Address_List _addresslist = new Address_List();
        //_addresslist.org_idx = 0;
        _dataEmployee.R0Address_list[0] = _addresslist;

        _dataEmployee = callServiceEmployee(_urlGetCountryList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.R0Address_list, "country_name", "country_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกประเทศ....", "0"));
    }

    protected void select_nationality(DropDownList ddlName)
    {
        _dataEmployee.R0Address_list = new Address_List[1];
        Address_List _addresslist = new Address_List();

        _dataEmployee.R0Address_list[0] = _addresslist;

        _dataEmployee = callServiceEmployee(_urlGetNationalityList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.R0Address_list, "NatName", "NatIDX");
        ddlName.Items.Insert(0, new ListItem("เลือกสัญชาติ....", "0"));
    }

    protected void select_province(DropDownList ddlName)
    {
        _dataEmployee.R0Address_list = new Address_List[1];
        Address_List _addresslist = new Address_List();
        //_addresslist.org_idx = 0;
        _dataEmployee.R0Address_list[0] = _addresslist;

        _dataEmployee = callServiceEmployee(_urlGetProvincetList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.R0Address_list, "prov_name", "prov_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกจังหวัด....", "0"));
    }

    protected void select_amphur(DropDownList ddlName, int prov_idx)
    {
        _dataEmployee.R0Address_list = new Address_List[1];
        Address_List _addresslist = new Address_List();
        _addresslist.prov_idx = prov_idx;
        _dataEmployee.R0Address_list[0] = _addresslist;

        _dataEmployee = callServiceEmployee(_urlGetAmphurList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.R0Address_list, "amp_name", "amp_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกอำเภอ....", "0"));
    }

    protected void select_dist(DropDownList ddlName, int prov_idx, int amphur_idx)
    {
        _dataEmployee.R0Address_list = new Address_List[1];
        Address_List _addresslist = new Address_List();
        _addresslist.prov_idx = prov_idx;
        _addresslist.amp_idx = amphur_idx;
        _dataEmployee.R0Address_list[0] = _addresslist;

        _dataEmployee = callServiceEmployee(_urlGetDistList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.R0Address_list, "dist_name", "dist_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกตำบล....", "0"));
    }

    protected void select_approve_add(DropDownList ddlName, int org_idx, int dep_idx, int sec_idx, int pos_idx)
    {
        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _dataEmployee_ = new employee_detail();

        _dataEmployee_.emp_idx = 0;
        _dataEmployee_.org_idx = org_idx;
        _dataEmployee_.rdept_idx = dep_idx;
        _dataEmployee_.rsec_idx = sec_idx;
        _dataEmployee_.rpos_idx = pos_idx;

        _dataEmployee.employee_list[0] = _dataEmployee_;
        _dataEmployee = callServiceEmployee(_urlGetApproveLonList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.employee_list, "emp_approve1", "emp_idx_approve1");
        ddlName.Items.Insert(0, new ListItem("เลือกผู้อนุมัติ ....", "0"));
    }

    protected void getApproveCen(DropDownList ddlName, int org_idx, int dep_idx, int sec_idx, int pos_idx)
    {
        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _dataEmployee_ = new employee_detail();

        _dataEmployee_.emp_idx = 0;
        _dataEmployee_.org_idx = org_idx;
        _dataEmployee_.rdept_idx = dep_idx;
        _dataEmployee_.rsec_idx = sec_idx;
        _dataEmployee_.rpos_idx = pos_idx;

        _dataEmployee.employee_list[0] = _dataEmployee_;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        _dataEmployee = callServiceEmployee(_urlgetApproveCen, _dataEmployee);

        setDdlData(ddlName, _dataEmployee.employee_list, "emp_approve1", "emp_idx_approve1");
        ddlName.Items.Insert(0, new ListItem("เลือกผู้อนุมัติ ....", "0"));
    }

    protected void Select_asset_type(DropDownList ddlName)
    {
        _dataEmployee.Asset_Type_List = new Asset_Type_Detail[1];
        Asset_Type_Detail dtemployee = new Asset_Type_Detail();

        _dataEmployee.Asset_Type_List[0] = dtemployee;

        _dataEmployee = callServiceEmployee(_urlGetM0_Asset_Select, _dataEmployee);

        ddlName.DataSource = _dataEmployee.Asset_Type_List;
        ddlName.DataTextField = "as_name";
        ddlName.DataValueField = "asidx";
        ddlName.DataBind();
    }

    protected void Select_unit_type(DropDownList ddlName)
    {
        _dataEmployee.Unit_Type_List = new Unit_Type_Detail[1];
        Unit_Type_Detail dtemployee = new Unit_Type_Detail();

        _dataEmployee.Unit_Type_List[0] = dtemployee;

        _dataEmployee = callServiceEmployee(_urlGetM0_UnitType_Select, _dataEmployee);

        ddlName.DataSource = _dataEmployee.Unit_Type_List;
        ddlName.DataTextField = "m0_name";
        ddlName.DataValueField = "m0_unidx";
        ddlName.DataBind();
    }

    protected void GetPositionListCen(int emp_idx)
    {
        var GvPositionList = (GridView)FvEdit_Detail.FindControl("GvPositionList");

        _dataEmployee.cen_position_emp_r0_list = new cen_position_emp_r0_detail[1];
        cen_position_emp_r0_detail position_emp_r0 = new cen_position_emp_r0_detail();

        position_emp_r0.emp_idx = emp_idx;

        _dataEmployee.cen_position_emp_r0_list[0] = position_emp_r0;
        _dataEmployee = callServiceEmployee(_urlGetPositionListCen, _dataEmployee);
        
        ViewState["vs_PositionList_View"] = _dataEmployee.cen_position_emp_r0_list
;
        setGridData(GvPositionList, _dataEmployee.cen_position_emp_r0_list
);
    }

    protected void Select_ODSP(int emp_idx)
    {
        var GvPosAdd_View = (GridView)FvEdit_Detail.FindControl("GvPosAdd_View");

        _dataEmployee.ODSPRelation_list = new ODSP_List[1];
        ODSP_List _dataEmployee_ = new ODSP_List();

        _dataEmployee_.emp_idx = emp_idx;

        _dataEmployee.ODSPRelation_list[0] = _dataEmployee_;
        _dataEmployee = callServiceEmployee(_urlSetODSPList, _dataEmployee);
        
        ViewState["BoxGvPosAdd_View"] = _dataEmployee.ODSPRelation_list;
        setGridData(GvPosAdd_View, _dataEmployee.ODSPRelation_list);
    }

    protected void Select_Gv_EmpShift(int emp_idx)
    {
        var GvEmpShift_view = (GridView)FvEdit_Detail.FindControl("GvEmpShift");

        _dataEmployee.ShiftTime_details = new ShiftTime[1];
        ShiftTime _orgList = new ShiftTime();
        _orgList.emp_shift_idx = emp_idx;
        _orgList.type_selection = 2; //geidview
        _dataEmployee.ShiftTime_details[0] = _orgList;

        _dataEmployee = callServiceEmployee(_urlSetOrganizationList_shift, _dataEmployee);
        setGridData(GvEmpShift_view, _dataEmployee.ShiftTime_details);
        ViewState["BoxGvEmpshiftAdd_View"] = _dataEmployee.ShiftTime_details;
    }

    protected void Select_Refer_view(int emp_idx)
    {
        data_employee _dataEmployee_c = new data_employee();
        GridView GvReference_View = (GridView)FvEdit_Detail.FindControl("GvReference_View");

        _dataEmployee_c.BoxReferent_list = new Referent_List[1];
        Referent_List _dataEmployee_1 = new Referent_List();

        _dataEmployee_1.EmpIDX = emp_idx;

        _dataEmployee_c.BoxReferent_list[0] = _dataEmployee_1;
        _dataEmployee_c = callServiceEmployee(_urlGetReferList, _dataEmployee_c);
        setGridData(GvReference_View, _dataEmployee_c.BoxReferent_list);
        ViewState["BoxGvRefer_view"] = _dataEmployee_c.BoxReferent_list;
    }

    protected void Select_Child_view(int emp_idx)
    {
        data_employee _dataEmployee_c = new data_employee();
        GridView GvChildAdd_View = (GridView)FvEdit_Detail.FindControl("GvChildAdd_View");

        _dataEmployee_c.BoxChild_list = new Child_List[1];
        Child_List _dataEmployee_1 = new Child_List();

        _dataEmployee_1.EmpIDX = emp_idx;

        _dataEmployee_c.BoxChild_list[0] = _dataEmployee_1;
        _dataEmployee_c = callServiceEmployee(_urlGetChildList, _dataEmployee_c);
        setGridData(GvChildAdd_View, _dataEmployee_c.BoxChild_list);
        ViewState["BoxGvChild_view"] = _dataEmployee_c.BoxChild_list;
    }

    protected void Select_Prior_view(int emp_idx)
    {
        data_employee _dataEmployee_p = new data_employee();
        GridView GvPri_View = (GridView)FvEdit_Detail.FindControl("GvPri_View");

        _dataEmployee_p.BoxPrior_list = new Prior_List[1];
        Prior_List _dataEmployee_2 = new Prior_List();

        _dataEmployee_2.EmpIDX = emp_idx;

        _dataEmployee_p.BoxPrior_list[0] = _dataEmployee_2;
        _dataEmployee_p = callServiceEmployee(_urlGetPriorList, _dataEmployee_p);
        setGridData(GvPri_View, _dataEmployee_p.BoxPrior_list);
        ViewState["BoxGvPrior_view"] = _dataEmployee_p.BoxPrior_list;
    }

    protected void Select_Education_view(int emp_idx)
    {
        data_employee _dataEmployee_e = new data_employee();
        GridView GvEducation_View = (GridView)FvEdit_Detail.FindControl("GvEducation_View");

        _dataEmployee_e.BoxEducation_list = new Education_List[1];
        Education_List _dataEmployee_ = new Education_List();

        _dataEmployee_.EmpIDX = emp_idx;

        _dataEmployee_e.BoxEducation_list[0] = _dataEmployee_;
        _dataEmployee_e = callServiceEmployee(_urlGetEducationList, _dataEmployee_e);
        setGridData(GvEducation_View, _dataEmployee_e.BoxEducation_list);
        ViewState["BoxGvEducation_view"] = _dataEmployee_e.BoxEducation_list;
    }

    protected void Select_Train_view(int emp_idx)
    {
        data_employee _dataEmployee_t = new data_employee();
        GridView GvTrain_View = (GridView)FvEdit_Detail.FindControl("GvTrain_View");

        _dataEmployee_t.BoxTrain_list = new Train_List[1];
        Train_List _dataEmployee_ = new Train_List();

        _dataEmployee_.EmpIDX = emp_idx;

        _dataEmployee_t.BoxTrain_list[0] = _dataEmployee_;
        _dataEmployee_t = callServiceEmployee(_urlGetTrainList, _dataEmployee_t);
        setGridData(GvTrain_View, _dataEmployee_t.BoxTrain_list);
        ViewState["BoxGvTrain_view"] = _dataEmployee_t.BoxTrain_list;
    }

    protected void Select_Gv_Asset(GridView GvMaster, int emp_idx)
    {
        _dataEmployee.ShiftTime_details = new ShiftTime[1];
        ShiftTime _orgList = new ShiftTime();
        _orgList.emp_shift_idx = emp_idx;
        _orgList.type_selection = 5;
        _dataEmployee.ShiftTime_details[0] = _orgList;

        //Label2.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        _dataEmployee = callServiceEmployee(_urlSetOrganizationList_shift, _dataEmployee);
        setGridData(GvMaster, _dataEmployee.Holder_List);
    }

    protected void Select_Gurantee_view(int emp_idx)
    {
        data_employee _dataEmployee_c = new data_employee();
        GridView GvGurantee_View = (GridView)FvEdit_Detail.FindControl("GvGurantee_View");

        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _dataEmployee_ = new employee_detail();

        _dataEmployee_.emp_idx = emp_idx;
        _dataEmployee_.type_select_emp = 10; //Index     

        _dataEmployee.employee_list[0] = _dataEmployee_;
        _dataEmployee = callServiceEmployee(urlGetEmployee, _dataEmployee);
        setGridData(GvGurantee_View, _dataEmployee.BoxGurantee_list);
        ViewState["Boxgv_Gurantee"] = _dataEmployee.BoxGurantee_list;
    }

    protected string formatDateTimeCheck(string dateIN)
    {
        //return DateTime.ParseExact(dateIN, "dd/MM/yyyy", null).Month.ToString();
        return DateTime.ParseExact(dateIN, "MM", null).Month.ToString();
    }

    protected void Select_TypeVisa(DropDownList ddlName)
    {
        _dataEmployee.BoxTypevisa_list = new Typevisa_List[1];
        Typevisa_List _dataEmployee_ = new Typevisa_List();

        //_dataEmployee_.type_select_emp = 5; //Visa   

        _dataEmployee.BoxTypevisa_list[0] = _dataEmployee_;
        //fss.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        _dataEmployee = callServiceEmployee(_urlGetSelectVisatype, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.BoxTypevisa_list, "name_visa", "VisaIDX");
        ddlName.Items.Insert(0, new ListItem("ประเภท Passport Type....", "0"));
    }

    protected void Select_Report_Probation()
    {
        _dataEmployee.BoxEmployee_ProbationList = new Employee_Probation[1];
        Employee_Probation _dataEmployee_ = new Employee_Probation();

        _dataEmployee_.OrgIDX = int.Parse(ddlorgidx.SelectedValue);
        _dataEmployee_.RDeptID = int.Parse(ddlrdeptidx.SelectedValue);
        _dataEmployee_.RSecID = int.Parse(ddlsecidx.SelectedValue);
        _dataEmployee_.EmpProbation = int.Parse(ddlprobation.SelectedValue);
        _dataEmployee_.EmpType = int.Parse(ddlemptype.SelectedValue);
        _dataEmployee_.IFSearchBetween = int.Parse(ddlSearchDate.SelectedValue);
        _dataEmployee_.EmpProbationDate_Start = AddStartdate.Text;
        _dataEmployee_.EmpProbationDate_End = AddEndDate.Text;
        _dataEmployee_.LocIDX = int.Parse(ddllocation_probation.SelectedValue);
        _dataEmployee_.IFLenght = int.Parse(ddllenght_empcode.SelectedValue);
        _dataEmployee_.EmpCode = txtempcode.Text;
        _dataEmployee_.EmpCode1 = txtempcode1.Text;
        _dataEmployee_.Condition = 1;

        _dataEmployee.BoxEmployee_ProbationList[0] = _dataEmployee_;
        //fss.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        _dataEmployee = callServiceEmployee(_urlSelect_Report_Probation, _dataEmployee);
        setGridData(GvEmpProbationList, _dataEmployee.BoxEmployee_ProbationList);

    }

    protected void Select_Report_Probation_Print(FormView fvName, int empidx)
    {

        _dataEmployee.BoxEmployee_ProbationList = new Employee_Probation[1];
        Employee_Probation _dataEmployee_ = new Employee_Probation();

        _dataEmployee_.EmpIDX = empidx;
        _dataEmployee_.Condition = 2;

        _dataEmployee.BoxEmployee_ProbationList[0] = _dataEmployee_;
        //fss.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        _dataEmployee = callServiceEmployee(_urlSelect_Report_Probation, _dataEmployee);
        setFormData(fvName, _dataEmployee.BoxEmployee_ProbationList);
        ////Label lblfullnameth = (Label)fvName.FindControl("lblfullnameth");
        ////lblfullnameth.Text = _dataEmployee.BoxEmployee_ProbationList[0].FullNameTH.ToString();

    }



    public void Select_Chart_Education_colum()
    {
        DropDownList ddlorg_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlorg_rp");
        DropDownList ddldep_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddldep_rp");
        DropDownList ddlsec_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlsec_rp");
        DropDownList ddlpos_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlpos_rp");
        DropDownList ddlemptype_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlemptype_s");
        DropDownList ddlempstatus_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlempstatus_s");
        DropDownList ddldatetype = (DropDownList)Fv_Search_Emp_Report.FindControl("ddldatetype");
        DropDownList ddljobgrade_start = (DropDownList)Fv_Search_Emp_Report.FindControl("ddljobgrade_start");
        DropDownList ddljobgrade_end = (DropDownList)Fv_Search_Emp_Report.FindControl("ddljobgrade_end");
        DropDownList ddlAffiliation_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlAffiliation_s");
        DropDownList ddleducation_search = (DropDownList)Fv_Search_Emp_Report.FindControl("ddleducation_search");
        DropDownList ddllocation_search = (DropDownList)Fv_Search_Emp_Report.FindControl("ddllocation_search");
        DropDownList ddl_target_report = (DropDownList)Fv_Search_Emp_Report.FindControl("ddl_target_report");

        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _dataEmployee_edu = new employee_detail();

        _dataEmployee_edu.org_idx = int.Parse(ddlorg_rp.SelectedValue);
        _dataEmployee_edu.rdept_idx = int.Parse(ddldep_rp.SelectedValue);
        _dataEmployee_edu.rsec_idx = int.Parse(ddlsec_rp.SelectedValue);
        _dataEmployee_edu.rpos_idx = int.Parse(ddlpos_rp.SelectedValue);
        _dataEmployee_edu.emp_type_idx = int.Parse(ddlemptype_s.SelectedValue);
        _dataEmployee_edu.emp_status = int.Parse(ddlempstatus_s.SelectedValue);
        _dataEmployee_edu.type_select_emp = 6; //education     
        //_dataEmployee_edu.datetype_search = int.Parse(ddldatetype.SelectedValue);
        _dataEmployee_edu.Search_jobgrade1 = int.Parse(ddljobgrade_start.SelectedValue);
        _dataEmployee_edu.Search_jobgrade2 = int.Parse(ddljobgrade_end.SelectedValue);
        _dataEmployee_edu.ACIDX = int.Parse(ddlAffiliation_s.SelectedValue);
        _dataEmployee_edu.EDUIDX = int.Parse(ddleducation_search.SelectedValue);
        _dataEmployee_edu.LocIDX = int.Parse(ddllocation_search.SelectedValue);
        //_dataEmployee_edu.TIDX = int.Parse(ddl_target_report.SelectedValue);

        _dataEmployee.employee_list[0] = _dataEmployee_edu;
        _dataEmployee = callServiceEmployee(urlGetEmployee, _dataEmployee);
        setGridData(Gv_education, _dataEmployee.BoxEducation_list);
        ViewState["Education_chart"] = _dataEmployee;
        ViewState["Education_chart_list"] = _dataEmployee.BoxEducation_list;

        if (_dataEmployee.return_code == "0")
        {
            //int count = _dataEmployee.BoxEducation_list.Length;
            //string[] lv1code = new string[count];
            //object[] lv1count = new object[count];

            string[] c_color = new string[10] { "#91e8e1", "#aa4643", "#90ed7d", "#f7a35c", "#8085e9", "#f15c80", "#e4d354", "#2b908f", "#f45b5b", "#91e8e1" };
            // prepare type list
            data_employee _data_type = (data_employee)ViewState["Education_chart"];
            string[] lv1code = new string[_data_type.BoxEducation_list.Length];
            object[] lv1count = new object[_data_type.BoxEducation_list.Length];
            // check number of type. not over than 10(c_color)
            if (lv1code.Length > c_color.Length + 1) { return; }
            int i = 0;
            foreach (var data in _dataEmployee.BoxEducation_list)
            {
                lv1code[i] = data.Edu_name.ToString();
                lv1count[i] = data.Edu_branch.ToString();
                i++;
            }

            Highcharts chart = new Highcharts("chart");



            chart.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of times" } } });
            chart.SetXAxis(new XAxis { Categories = lv1code });
            //Highcharts.setOptions({ colors: ['#3B97B2', '#67BC42', '#FF56DE', '#E6D605', '#BC36FE'] });

            chart.SetPlotOptions(new PlotOptions
            {
                Column = new PlotOptionsColumn
                {
                    Stacking = DotNet.Highcharts.Enums.Stackings.Normal,
                    BorderWidth = 2,
                    BorderColor = Color.Gray,
                    Shadow = false,
                    DataLabels = new PlotOptionsColumnDataLabels
                    {
                        Enabled = true,
                        Color = Color.White,
                        Shadow = true

                    }

                }

            });

            object[,] v_zcount = new object[lv1code.Length, lv1count.Length];
            Series[] s_list = new Series[lv1code.Length];
            for (int m = 0; m < lv1code.Length; m++)
            {
                object[] v_display = new object[lv1count.Length];
                for (int n = 0; n < lv1count.Length; n++)
                {
                    v_display[n] = v_zcount[m, n];
                }

                s_list[m] = new Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = lv1code[m],
                    Data = new Data(v_display),
                    Color = System.Drawing.ColorTranslator.FromHtml(c_color[m])
                };
            }

            //for (int m = 0; m < lv1code.Length; m++)
            //{
            chart.SetSeries(
            //s_list
            new Series
            {
                Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                Name = "Name Education",
                Data = new Data(lv1count),
                    //Color = System.Drawing.ColorTranslator.FromHtml(c_color[m])
                }
        );
            //}
            litReportEducation.Text = chart.ToHtmlString();
        }
        else
        {
            litReportEducation.Text = "<p class='bg-danger'>No result.</p>";
        }

    }

    public void Select_Chart_Sex()
    {
        DropDownList ddlorg_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlorg_rp");
        DropDownList ddldep_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddldep_rp");
        DropDownList ddlsec_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlsec_rp");
        DropDownList ddlpos_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlpos_rp");
        DropDownList ddlemptype_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlemptype_s");
        DropDownList ddlempstatus_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlempstatus_s");
        //DropDownList ddldatetype = (DropDownList)Fv_Search_Emp_Report.FindControl("ddldatetype");
        DropDownList ddljobgrade_start = (DropDownList)Fv_Search_Emp_Report.FindControl("ddljobgrade_start");
        DropDownList ddljobgrade_end = (DropDownList)Fv_Search_Emp_Report.FindControl("ddljobgrade_end");
        DropDownList ddlAffiliation_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlAffiliation_s");
        DropDownList ddllocation_search = (DropDownList)Fv_Search_Emp_Report.FindControl("ddllocation_search");
        DropDownList ddl_target_report = (DropDownList)Fv_Search_Emp_Report.FindControl("ddl_target_report");

        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _dataEmployee_ = new employee_detail();

        _dataEmployee_.org_idx = int.Parse(ddlorg_rp.SelectedValue);
        _dataEmployee_.rdept_idx = int.Parse(ddldep_rp.SelectedValue);
        _dataEmployee_.rsec_idx = int.Parse(ddlsec_rp.SelectedValue);
        _dataEmployee_.rpos_idx = int.Parse(ddlpos_rp.SelectedValue);
        _dataEmployee_.emp_type_idx = int.Parse(ddlemptype_s.SelectedValue);
        _dataEmployee_.emp_status = int.Parse(ddlempstatus_s.SelectedValue);
        _dataEmployee_.type_select_emp = 7; //sex     
        //_dataEmployee_.datetype_search = int.Parse(ddldatetype.SelectedValue);
        _dataEmployee_.Search_jobgrade1 = int.Parse(ddljobgrade_start.SelectedValue);
        _dataEmployee_.Search_jobgrade2 = int.Parse(ddljobgrade_end.SelectedValue);
        _dataEmployee_.ACIDX = int.Parse(ddlAffiliation_s.SelectedValue);
        _dataEmployee_.LocIDX = int.Parse(ddllocation_search.SelectedValue);
        //_dataEmployee_.TIDX = int.Parse(ddl_target_report.SelectedValue);

        _dataEmployee.employee_list[0] = _dataEmployee_;
        _dataEmployee = callServiceEmployee(urlGetEmployee, _dataEmployee);
        setGridData(Gv_sex, _dataEmployee.employee_list);
        ViewState["sex_chart_list"] = _dataEmployee.employee_list;

        if (_dataEmployee.return_code == "0")
        {
            var caseclose = new List<object>();

            foreach (var data in _dataEmployee.employee_list)
            {
                caseclose.Add(new object[] { data.sex_name_th.ToString(), data.sex_idx.ToString() });
            }

            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new Title { Text = "เพศ" });
            chart.InitChart(new Chart { DefaultSeriesType = DotNet.Highcharts.Enums.ChartTypes.Pie });

            chart.SetPlotOptions(new PlotOptions
            {
                Pie = new PlotOptionsPie
                {

                    ShowInLegend = true,
                    AllowPointSelect = true,
                    DataLabels = new PlotOptionsPieDataLabels
                    {
                        Enabled = true,
                        Formatter = "function() { return this.point.name +' : '+  this.y.toFixed(0)+' '; }"
                    }
                }
            });

            chart.SetTooltip(new Tooltip
            {
                Enabled = true,
                Formatter = "function() { return this.point.name +' : '+  this.percentage.toFixed(2)+' % '; }"

            });

            chart.SetSeries(new Series[]
                 {
                        new Series()
                        {
                            Data = new Data (caseclose.ToArray())
                        }
                 });

            litReportsex.Text = chart.ToHtmlString();
        }
        else
        {
            litReportsex.Text = "<p class='bg-danger'>No result.</p>";
        }

    }

    public void Select_Chart_Age()
    {
        DropDownList ddlorg_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlorg_rp");
        DropDownList ddldep_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddldep_rp");
        DropDownList ddlsec_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlsec_rp");
        DropDownList ddlpos_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlpos_rp");
        DropDownList ddlemptype_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlemptype_s");
        DropDownList ddlempstatus_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlempstatus_s");
        DropDownList ddldatetype = (DropDownList)Fv_Search_Emp_Report.FindControl("ddldatetype");
        TextBox txtage_start = (TextBox)Fv_Search_Emp_Report.FindControl("txtage_start");
        TextBox txtage_end = (TextBox)Fv_Search_Emp_Report.FindControl("txtage_end");
        DropDownList ddlAffiliation_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlAffiliation_s");
        DropDownList ddllocation_search = (DropDownList)Fv_Search_Emp_Report.FindControl("ddllocation_search");
        DropDownList ddl_target_report = (DropDownList)Fv_Search_Emp_Report.FindControl("ddl_target_report");

        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _dataEmployee_ = new employee_detail();

        _dataEmployee_.org_idx = int.Parse(ddlorg_rp.SelectedValue);
        _dataEmployee_.rdept_idx = int.Parse(ddldep_rp.SelectedValue);
        _dataEmployee_.rsec_idx = int.Parse(ddlsec_rp.SelectedValue);
        _dataEmployee_.rpos_idx = int.Parse(ddlpos_rp.SelectedValue);
        _dataEmployee_.emp_type_idx = int.Parse(ddlemptype_s.SelectedValue);
        _dataEmployee_.emp_status = int.Parse(ddlempstatus_s.SelectedValue);
        _dataEmployee_.type_select_emp = 8; //Age     
        _dataEmployee_.datetype_search = int.Parse(ddldatetype.SelectedValue);
        if (txtage_start.Text != "")
        {
            _dataEmployee_.Search_jobgrade1 = int.Parse(txtage_start.Text);
        }
        else
        {
            _dataEmployee_.Search_jobgrade1 = 0;
        }

        if (txtage_end.Text != "")
        {
            _dataEmployee_.Search_jobgrade2 = int.Parse(txtage_end.Text);
        }
        else
        {
            _dataEmployee_.Search_jobgrade2 = 0;
        }

        _dataEmployee_.ACIDX = int.Parse(ddlAffiliation_s.SelectedValue);
        _dataEmployee_.LocIDX = int.Parse(ddllocation_search.SelectedValue);
        //_dataEmployee_.TIDX = int.Parse(ddl_target_report.SelectedValue);

        _dataEmployee.employee_list[0] = _dataEmployee_;
        _dataEmployee = callServiceEmployee(urlGetEmployee, _dataEmployee);
        setGridData(Gv_age, _dataEmployee.employee_list);
        ViewState["age_chart_list"] = _dataEmployee.employee_list;
        //fss.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        //fss.Text = 
        if (_dataEmployee.return_code == "0")
        {
            int count = _dataEmployee.employee_list.Length;
            string[] lv1code = new string[count];
            object[] lv1count = new object[count];
            int i = 0;
            foreach (var data in _dataEmployee.employee_list)
            {
                lv1code[i] = data.P_Name.ToString();
                lv1count[i] = data.PIDX.ToString();
                i++;
            }
            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of Age" } } });
            chart.SetXAxis(new XAxis { Categories = lv1code });
            chart.SetPlotOptions(new PlotOptions
            {
                Column = new PlotOptionsColumn
                {
                    Stacking = DotNet.Highcharts.Enums.Stackings.Normal,
                    BorderWidth = 2,
                    BorderColor = Color.Gray,
                    Shadow = false,
                    DataLabels = new PlotOptionsColumnDataLabels
                    {
                        Enabled = true,
                        Color = Color.White,
                        Shadow = true,
                    }
                }
            });
            chart.SetLegend(new Legend
            {
                Layout = DotNet.Highcharts.Enums.Layouts.Vertical,
            });

            chart.SetSeries(
                new Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "Name Age",
                    Stack = "1",
                    Data = new Data(lv1count),
                }
            );


            litReportage.Text = chart.ToHtmlString();
        }
        else
        {
            litReportage.Text = "<p class='bg-danger'>No result.</p>";
        }

    }

    public void Select_Chart_Car_colum()
    {
        //TextBox txtempcode_s = (TextBox)Fv_Search_Emp_Report.FindControl("txtempcode_s");
        //TextBox txtempname_s = (TextBox)Fv_Search_Emp_Report.FindControl("txtempname_s");
        TextBox txtdatestart = (TextBox)Fv_Search_Emp_Report.FindControl("txtdatestart");
        TextBox txtdateend = (TextBox)Fv_Search_Emp_Report.FindControl("txtdateend");

        DropDownList ddlorg_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlorg_rp");
        DropDownList ddldep_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddldep_rp");
        DropDownList ddlsec_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlsec_rp");
        DropDownList ddlpos_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlpos_rp");
        DropDownList ddlemptype_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlemptype_s");
        DropDownList ddlempstatus_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlempstatus_s");
        //DropDownList ddldatetype = (DropDownList)Fv_Search_Emp_Report.FindControl("ddldatetype");
        //DropDownList ddljobgrade_start = (DropDownList)Fv_Search_Emp_Report.FindControl("ddljobgrade_start");
        //DropDownList ddljobgrade_end = (DropDownList)Fv_Search_Emp_Report.FindControl("ddljobgrade_end");
        DropDownList ddlAffiliation_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlAffiliation_s");
        DropDownList ddllocation_search = (DropDownList)Fv_Search_Emp_Report.FindControl("ddllocation_search");
        DropDownList ddl_target_report = (DropDownList)Fv_Search_Emp_Report.FindControl("ddl_target_report");

        if (txtdatestart.Text != "" && txtdateend.Text != "" && txtdatestart.Text != String.Empty && txtdateend.Text != String.Empty)
        {
            _dataEmployee.employee_list = new employee_detail[1];
            employee_detail _dataEmployee_ = new employee_detail();

            if (txtdatestart.Text != "")
            {
                _dataEmployee_.startdate_search = txtdatestart.Text;
            }
            else
            {
                _dataEmployee_.startdate_search = "";
            }

            if (txtdateend.Text != "")
            {
                _dataEmployee_.enddate_search = txtdateend.Text;
            }
            else
            {
                _dataEmployee_.enddate_search = "";
            }

            _dataEmployee_.org_idx = int.Parse(ddlorg_rp.SelectedValue);
            _dataEmployee_.rdept_idx = int.Parse(ddldep_rp.SelectedValue);
            _dataEmployee_.rsec_idx = int.Parse(ddlsec_rp.SelectedValue);
            _dataEmployee_.rpos_idx = int.Parse(ddlpos_rp.SelectedValue);
            _dataEmployee_.emp_type_idx = int.Parse(ddlemptype_s.SelectedValue);
            _dataEmployee_.emp_status = int.Parse(ddlempstatus_s.SelectedValue);
            _dataEmployee_.type_select_emp = 9; //Education    
            _dataEmployee_.ACIDX = int.Parse(ddlAffiliation_s.SelectedValue);
            _dataEmployee_.LocIDX = int.Parse(ddllocation_search.SelectedValue);
            //_dataEmployee_.TIDX = int.Parse(ddl_target_report.SelectedValue);

            _dataEmployee.employee_list[0] = _dataEmployee_;
            _dataEmployee = callServiceEmployee(urlGetEmployee, _dataEmployee);
            setGridData(Gv_car, _dataEmployee.employee_list);
            ViewState["Temp_Gv_car"] = _dataEmployee.employee_list;
            //fss.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));

            if (_dataEmployee.return_code == "0")
            {
                int count = _dataEmployee.employee_list.Length;
                string[] lv1code = new string[count];
                object[] lv1count = new object[count];
                int i = 0;
                foreach (var data in _dataEmployee.employee_list)
                {
                    lv1code[i] = data.determine.ToString();
                    lv1count[i] = data.dvcar_idx.ToString();
                    i++;
                }
                Highcharts chart = new Highcharts("chart");
                chart.SetTitle(new Title { Text = "" });
                chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of times" } } });
                chart.SetXAxis(new XAxis { Categories = lv1code });
                chart.SetPlotOptions(new PlotOptions
                {
                    Column = new PlotOptionsColumn
                    {
                        Stacking = DotNet.Highcharts.Enums.Stackings.Normal,
                        BorderWidth = 2,
                        BorderColor = Color.Gray,
                        Shadow = false,
                        DataLabels = new PlotOptionsColumnDataLabels
                        {
                            Enabled = true,
                            Color = Color.White,
                            Shadow = true,
                        }
                    }
                });
                chart.SetSeries(
                    new Series
                    {
                        Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                        Name = "Date Time",
                        Data = new Data(lv1count)
                    }
                );
                litReportcar.Text = chart.ToHtmlString();
            }
            else
            {
                litReportcar.Text = "<p class='bg-danger'>No result.</p>";
            }
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "แจ้งเตือน", "alert('" + "รบกวนกรอกข้อมูลวันที่ให้ครบ" + "')", true);
        }
    }

    public void Select_Chart_Count_Employee()
    {
        DropDownList ddlorg_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlorg_rp");
        DropDownList ddldep_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddldep_rp");
        DropDownList ddlsec_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlsec_rp");
        DropDownList ddlpos_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlpos_rp");
        DropDownList ddlemptype_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlemptype_s");
        DropDownList ddlempstatus_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlempstatus_s");
        DropDownList ddldatetype = (DropDownList)Fv_Search_Emp_Report.FindControl("ddldatetype");
        TextBox txtage_start = (TextBox)Fv_Search_Emp_Report.FindControl("txtage_start");
        TextBox txtage_end = (TextBox)Fv_Search_Emp_Report.FindControl("txtage_end");
        DropDownList ddlAffiliation_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlAffiliation_s");
        DropDownList ddllocation_search = (DropDownList)Fv_Search_Emp_Report.FindControl("ddllocation_search");
        DropDownList ddl_target_report = (DropDownList)Fv_Search_Emp_Report.FindControl("ddl_target_report");

        if (int.Parse(ddlorg_rp.SelectedValue) != 0)
        {
            _dataEmployee.employee_list = new employee_detail[1];
            employee_detail _dataEmployee_ = new employee_detail();

            _dataEmployee_.org_idx = int.Parse(ddlorg_rp.SelectedValue);
            _dataEmployee_.rdept_idx = int.Parse(ddldep_rp.SelectedValue);
            _dataEmployee_.rsec_idx = int.Parse(ddlsec_rp.SelectedValue);
            _dataEmployee_.rpos_idx = int.Parse(ddlpos_rp.SelectedValue);
            _dataEmployee_.emp_type_idx = int.Parse(ddlemptype_s.SelectedValue);
            _dataEmployee_.emp_status = int.Parse(ddlempstatus_s.SelectedValue);
            _dataEmployee_.type_select_emp = 11; //count employee     
            _dataEmployee_.datetype_search = int.Parse(ddldatetype.SelectedValue);
            _dataEmployee_.dept_name_en = ViewState["Columns_Check_dep_Search"].ToString(); //checkboxlist dept

            if (txtage_start.Text != "")
            {
                _dataEmployee_.Search_jobgrade1 = int.Parse(txtage_start.Text);
            }
            else
            {
                _dataEmployee_.Search_jobgrade1 = 0;
            }

            if (txtage_end.Text != "")
            {
                _dataEmployee_.Search_jobgrade2 = int.Parse(txtage_end.Text);
            }
            else
            {
                _dataEmployee_.Search_jobgrade2 = 0;
            }

            _dataEmployee_.ACIDX = int.Parse(ddlAffiliation_s.SelectedValue);
            _dataEmployee_.LocIDX = int.Parse(ddllocation_search.SelectedValue);
            //_dataEmployee_.TIDX = int.Parse(ddl_target_report.SelectedValue);

            _dataEmployee.employee_list[0] = _dataEmployee_;
            _dataEmployee = callServiceEmployee(urlGetEmployee, _dataEmployee);
            setGridData(Gv_CountEmployee, _dataEmployee.employee_list);
            ViewState["countemp_chart_list"] = _dataEmployee.employee_list;
            //fss.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));

            if (_dataEmployee.return_code == "0")
            {
                string[] c_color = new string[10] { "#7cb5ec", "#aa4643", "#90ed7d", "#f7a35c", "#8085e9", "#f15c80", "#e4d354", "#2b908f", "#f45b5b", "#91e8e1" };

                int count = _dataEmployee.employee_list.Length;
                string[] lv1code = new string[count];
                string[] lv1count = new string[count];
                string[] lv1count_1 = new string[count];
                int i = 0;
                foreach (var data in _dataEmployee.employee_list)
                {
                    lv1code[i] = data.dept_name_th.ToString();
                    //lv1count[i] = data.rdept_idx.ToString();
                    //lv1count_1[i] = data.r0idx.ToString();
                    i++;
                }

                Highcharts chart = new Highcharts("chart");
                chart.SetTitle(new Title { Text = "" });
                chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of Count Employee" } } });
                chart.SetXAxis(new XAxis { Categories = lv1code });
                //chart.InitChart(new Chart { Height = 1500 });
                int a1 = 5;
                chart.SetPlotOptions(new PlotOptions
                {
                    Column = new PlotOptionsColumn
                    {
                        Stacking = DotNet.Highcharts.Enums.Stackings.Normal,
                        BorderWidth = 2,
                        BorderColor = Color.Gray,
                        Shadow = false,
                        DataLabels = new PlotOptionsColumnDataLabels
                        {
                            Enabled = true,
                            Color = Color.White,
                            Shadow = true,
                            //Style = FontSize.XXSmall.ToString(),
                        }
                    },
                    Bar = new PlotOptionsBar
                    {
                        Stacking = DotNet.Highcharts.Enums.Stackings.Normal,
                        BorderWidth = 1,
                        BorderColor = Color.Gray,
                        Shadow = false,
                        DataLabels = new PlotOptionsBarDataLabels
                        {
                            Enabled = true,
                            Color = Color.White,
                            Shadow = true,
                            //Style = FontSize.XXSmall.ToString(),
                            //Formatter = new Style { CssClass="s" }
                            //Style = new Style { BorderColor = s }
                        }
                    }
                });

                chart.SetLegend(new Legend
                {
                    Layout = DotNet.Highcharts.Enums.Layouts.Horizontal,
                    Reversed = true,
                    BorderWidth = 1,
                    Shadow = true,
                });

                System.Text.StringBuilder builder_day = new System.Text.StringBuilder();
                System.Text.StringBuilder builder_month = new System.Text.StringBuilder();
                foreach (var safePrime in _dataEmployee.employee_list)
                {
                    // Append each int to the StringBuilder overload.
                    builder_day.Append(safePrime.rdept_idx.ToString()).Append(",");
                    builder_month.Append(safePrime.r0idx.ToString()).Append(",");
                }
                string employee_day = builder_day.ToString();
                string employee_month = builder_month.ToString();

                chart.SetSeries(
                new Series[]
                {
                new Series{
                        Type = DotNet.Highcharts.Enums.ChartTypes.Bar,
                        Name = "พนักงานรายวัน",
                        Data = new Data(new object[] { employee_day }),
                        Color = ColorTranslator.FromHtml(c_color[7])
                 },
                new Series{
                        Type = DotNet.Highcharts.Enums.ChartTypes.Bar,
                        Name = "พนักงานรายเดือน",
                        Data = new Data(new object[] { employee_month }),
                        Color = ColorTranslator.FromHtml(c_color[5])
                    },
                }

                );

                litReportCountEmployee.Text = chart.ToHtmlString();
            }
            else
            {
                litReportCountEmployee.Text = "<p class='bg-danger'>No result.</p>";
            }
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "แจ้งเตือน", "alert('" + "กรุณาเลือกบริษัท" + "')", true);
        }

    }

    public void Select_Chart_Count_Employee_Detail()
    {
        DropDownList ddlorg_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlorg_rp");
        DropDownList ddldep_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddldep_rp");
        DropDownList ddlsec_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlsec_rp");
        DropDownList ddlpos_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlpos_rp");
        DropDownList ddlemptype_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlemptype_s");
        DropDownList ddlempstatus_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlempstatus_s");
        DropDownList ddldatetype = (DropDownList)Fv_Search_Emp_Report.FindControl("ddldatetype");
        TextBox txtage_start = (TextBox)Fv_Search_Emp_Report.FindControl("txtage_start");
        TextBox txtage_end = (TextBox)Fv_Search_Emp_Report.FindControl("txtage_end");
        DropDownList ddlAffiliation_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlAffiliation_s");
        DropDownList ddllocation_search = (DropDownList)Fv_Search_Emp_Report.FindControl("ddllocation_search");
        DropDownList ddl_target_report = (DropDownList)Fv_Search_Emp_Report.FindControl("ddl_target_report");

        if (int.Parse(ddlorg_rp.SelectedValue) != 0)
        {

            _dataEmployee.employee_list = new employee_detail[1];
            employee_detail _dataEmployee_ = new employee_detail();

            _dataEmployee_.org_idx = int.Parse(ddlorg_rp.SelectedValue);
            //_dataEmployee_.rdept_idx = int.Parse(ddldep_rp.SelectedValue);
            //_dataEmployee_.rsec_idx = int.Parse(ddlsec_rp.SelectedValue);
            //_dataEmployee_.rpos_idx = int.Parse(ddlpos_rp.SelectedValue);
            _dataEmployee_.emp_type_idx = int.Parse(ddlemptype_s.SelectedValue);
            _dataEmployee_.emp_status = int.Parse(ddlempstatus_s.SelectedValue);
            _dataEmployee_.type_select_emp = 11; //Age     
            _dataEmployee_.datetype_search = int.Parse(ddldatetype.SelectedValue);
            _dataEmployee_.dept_name_en = ViewState["Columns_Check_dep_Search"].ToString();
            if (txtage_start.Text != "")
            {
                _dataEmployee_.Search_jobgrade1 = int.Parse(txtage_start.Text);
            }
            else
            {
                _dataEmployee_.Search_jobgrade1 = 0;
            }

            if (txtage_end.Text != "")
            {
                _dataEmployee_.Search_jobgrade2 = int.Parse(txtage_end.Text);
            }
            else
            {
                _dataEmployee_.Search_jobgrade2 = 0;
            }

            _dataEmployee_.ACIDX = int.Parse(ddlAffiliation_s.SelectedValue);
            _dataEmployee_.LocIDX = int.Parse(ddllocation_search.SelectedValue);
            //_dataEmployee_.TIDX = int.Parse(ddl_target_report.SelectedValue);

            _dataEmployee.employee_list[0] = _dataEmployee_;
            _dataEmployee = callServiceEmployee(urlGetEmployee, _dataEmployee);
            setGridData(Gv_CountEmployee, _dataEmployee.employee_list);
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "แจ้งเตือน", "alert('" + "กรุณาเลือกบริษัท" + "')", true);
        }
    }

    public void Select_Chart_Leave_Employee()
    {
        //TextBox txtempcode_s = (TextBox)Fv_Search_Emp_Report.FindControl("txtempcode_s");
        //TextBox txtempname_s = (TextBox)Fv_Search_Emp_Report.FindControl("txtempname_s");
        TextBox txtdatestart = (TextBox)Fv_Search_Emp_Report.FindControl("txtdatestart");
        TextBox txtdateend = (TextBox)Fv_Search_Emp_Report.FindControl("txtdateend");

        DropDownList ddlorg_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlorg_rp");
        DropDownList ddldep_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddldep_rp");
        DropDownList ddlsec_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlsec_rp");
        DropDownList ddlpos_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlpos_rp");
        DropDownList ddlemptype_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlemptype_s");
        DropDownList ddlempstatus_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlempstatus_s");
        //DropDownList ddldatetype = (DropDownList)Fv_Search_Emp_Report.FindControl("ddldatetype");
        //DropDownList ddljobgrade_start = (DropDownList)Fv_Search_Emp_Report.FindControl("ddljobgrade_start");
        //DropDownList ddljobgrade_end = (DropDownList)Fv_Search_Emp_Report.FindControl("ddljobgrade_end");
        DropDownList ddlAffiliation_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlAffiliation_s");
        DropDownList ddllocation_search = (DropDownList)Fv_Search_Emp_Report.FindControl("ddllocation_search");
        DropDownList ddl_target_report = (DropDownList)Fv_Search_Emp_Report.FindControl("ddl_target_report");

        if (txtdatestart.Text != "" && txtdateend.Text != "" && txtdatestart.Text != String.Empty && txtdateend.Text != String.Empty && int.Parse(ddlorg_rp.SelectedValue) != 0)
        {
            _dataEmployee.employee_list = new employee_detail[1];
            employee_detail _dataEmployee_ = new employee_detail();

            if (txtdatestart.Text != "")
            {
                _dataEmployee_.startdate_search = txtdatestart.Text;
            }
            else
            {
                _dataEmployee_.startdate_search = "";
            }

            if (txtdateend.Text != "")
            {
                _dataEmployee_.enddate_search = txtdateend.Text;
            }
            else
            {
                _dataEmployee_.enddate_search = "";
            }

            _dataEmployee_.org_idx = int.Parse(ddlorg_rp.SelectedValue);
            //_dataEmployee_.rdept_idx = int.Parse(ddldep_rp.SelectedValue);
            //_dataEmployee_.rsec_idx = int.Parse(ddlsec_rp.SelectedValue);
            //_dataEmployee_.rpos_idx = int.Parse(ddlpos_rp.SelectedValue);
            _dataEmployee_.emp_type_idx = int.Parse(ddlemptype_s.SelectedValue);
            _dataEmployee_.emp_status = int.Parse(ddlempstatus_s.SelectedValue);
            _dataEmployee_.type_select_emp = 12; //leave    
            _dataEmployee_.ACIDX = int.Parse(ddlAffiliation_s.SelectedValue);
            _dataEmployee_.LocIDX = int.Parse(ddllocation_search.SelectedValue);
            _dataEmployee_.dept_name_en = ViewState["Columns_Check_dep_leave_Search"].ToString(); //checkboxlist
            //_dataEmployee_.TIDX = int.Parse(ddl_target_report.SelectedValue);

            _dataEmployee.employee_list[0] = _dataEmployee_;
            _dataEmployee = callServiceEmployee(urlGetEmployee, _dataEmployee);
            setGridData(Gv_LeaveEmployee, _dataEmployee.BoxEmployeeLeave_list);
            ViewState["leave_chart_list"] = _dataEmployee.BoxEmployeeLeave_list;
            //fss.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));

            if (_dataEmployee.return_code == "0")
            {
                string[] c_color = new string[14] { "#7cb5ec", "#aa4643", "#90ed7d", "#f7a35c", "#8085e9", "#f15c80", "#e4d354", "#2b908f", "#f45b5b", "#91e8e1", "#FFCC66", "#CCFF66", "#CCCCFF", "#999900" };

                int count = _dataEmployee.BoxEmployeeLeave_list.Length;
                string[] lv1code = new string[count];
                //string[] lv1count = new string[count];
                //string[] lv1count_1 = new string[count];
                int i = 0;
                foreach (var data in _dataEmployee.BoxEmployeeLeave_list)
                {
                    lv1code[i] = data.dept_name_th.ToString();
                    //lv1count[i] = data.rdept_idx.ToString();
                    //lv1count_1[i] = data.r0idx.ToString();
                    i++;
                }

                Highcharts chart = new Highcharts("chart");
                chart.SetTitle(new Title { Text = "" });
                chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of Leave Employee" } } });
                chart.SetXAxis(new XAxis { Categories = lv1code });
                //chart.InitChart(new Chart { Height = 1500 });
                int a1 = 5;
                chart.SetPlotOptions(new PlotOptions
                {
                    Column = new PlotOptionsColumn
                    {
                        Stacking = DotNet.Highcharts.Enums.Stackings.Normal,
                        BorderWidth = 2,
                        BorderColor = Color.Gray,
                        Shadow = false,
                        DataLabels = new PlotOptionsColumnDataLabels
                        {
                            Enabled = true,
                            Color = Color.White,
                            Shadow = true,
                            //Style = FontSize.XXSmall.ToString(),
                        }
                    },
                    Bar = new PlotOptionsBar
                    {
                        Stacking = DotNet.Highcharts.Enums.Stackings.Normal,
                        BorderWidth = 1,
                        BorderColor = Color.Gray,
                        Shadow = false,
                        DataLabels = new PlotOptionsBarDataLabels
                        {
                            Enabled = true,
                            Color = Color.White,
                            Shadow = true,
                            //Style = FontSize.XXSmall.ToString(),
                            //Formatter = new Style { CssClass="s" }
                            //Style = new Style { BorderColor = s }
                        }
                    }
                });

                chart.SetLegend(new Legend
                {
                    Layout = DotNet.Highcharts.Enums.Layouts.Horizontal,
                    Reversed = true,
                    BorderWidth = 1,
                    Shadow = true,
                });

                System.Text.StringBuilder builder_leave_1 = new System.Text.StringBuilder();
                System.Text.StringBuilder builder_leave_2 = new System.Text.StringBuilder();
                System.Text.StringBuilder builder_leave_3 = new System.Text.StringBuilder();
                System.Text.StringBuilder builder_leave_4 = new System.Text.StringBuilder();
                System.Text.StringBuilder builder_leave_5 = new System.Text.StringBuilder();
                System.Text.StringBuilder builder_leave_6 = new System.Text.StringBuilder();
                System.Text.StringBuilder builder_leave_17 = new System.Text.StringBuilder();
                System.Text.StringBuilder builder_leave_18 = new System.Text.StringBuilder();
                System.Text.StringBuilder builder_leave_19 = new System.Text.StringBuilder();
                System.Text.StringBuilder builder_leave_20 = new System.Text.StringBuilder();
                System.Text.StringBuilder builder_leave_21 = new System.Text.StringBuilder();

                foreach (var safePrime in _dataEmployee.BoxEmployeeLeave_list)
                {
                    // Append each int to the StringBuilder overload.
                    builder_leave_1.Append(safePrime.leavetype_1.ToString()).Append(",");
                    builder_leave_2.Append(safePrime.leavetype_2.ToString()).Append(",");
                    builder_leave_3.Append(safePrime.leavetype_3.ToString()).Append(",");
                    builder_leave_4.Append(safePrime.leavetype_4.ToString()).Append(",");
                    builder_leave_5.Append(safePrime.leavetype_5.ToString()).Append(",");
                    builder_leave_6.Append(safePrime.leavetype_6.ToString()).Append(",");
                    builder_leave_17.Append(safePrime.leavetype_17.ToString()).Append(",");
                    builder_leave_18.Append(safePrime.leavetype_18.ToString()).Append(",");
                    builder_leave_19.Append(safePrime.leavetype_19.ToString()).Append(",");
                    builder_leave_20.Append(safePrime.leavetype_20.ToString()).Append(",");
                    builder_leave_21.Append(safePrime.leavetype_21.ToString()).Append(",");
                }
                string employee_leave_1 = builder_leave_1.ToString();
                string employee_leave_2 = builder_leave_2.ToString();
                string employee_leave_3 = builder_leave_3.ToString();
                string employee_leave_4 = builder_leave_4.ToString();
                string employee_leave_5 = builder_leave_5.ToString();
                string employee_leave_6 = builder_leave_6.ToString();
                string employee_leave_17 = builder_leave_17.ToString();
                string employee_leave_18 = builder_leave_18.ToString();
                string employee_leave_19 = builder_leave_19.ToString();
                string employee_leave_20 = builder_leave_20.ToString();
                string employee_leave_21 = builder_leave_21.ToString();

                chart.SetSeries(
                new Series[]
                {


                new Series{
                        Type = DotNet.Highcharts.Enums.ChartTypes.Bar,
                        Name = "พักผ่อนประจำปี",
                        Data = new Data(new object[] { employee_leave_3 }),
                        Color = ColorTranslator.FromHtml(c_color[3])
                },
                new Series{
                        Type = DotNet.Highcharts.Enums.ChartTypes.Bar,
                        Name = "ลากิจ",
                        Data = new Data(new object[] { employee_leave_2 }),
                        Color = ColorTranslator.FromHtml(c_color[2])
                },
                new Series{
                        Type = DotNet.Highcharts.Enums.ChartTypes.Bar,
                        Name = "ลาป่วย",
                        Data = new Data(new object[] { employee_leave_1 }),
                        Color = ColorTranslator.FromHtml(c_color[1])
                },
                    /*new Series{
                            Type = DotNet.Highcharts.Enums.ChartTypes.Bar,
                            Name = "ลาคลอด",
                            Data = new Data(new object[] { employee_leave_4 }),
                            Color = ColorTranslator.FromHtml(c_color[4])
                    },*/
                    /*new Series
                    {
                            Type = DotNet.Highcharts.Enums.ChartTypes.Bar,
                            Name = "ลาเพื่อประกอบพิธีทางศาสนา",
                            Data = new Data(new object[] { employee_leave_5 }),
                            Color = ColorTranslator.FromHtml(c_color[5])
                    },*/
                    /*new Series
                    {
                            Type = DotNet.Highcharts.Enums.ChartTypes.Bar,
                            Name = "ลาทำหมัน",
                            Data = new Data(new object[] { employee_leave_6 }),
                            Color = ColorTranslator.FromHtml(c_color[6])
                    },*/
                    new Series{
                            Type = DotNet.Highcharts.Enums.ChartTypes.Bar,
                            Name = "ลาวันเกิด",
                            Data = new Data(new object[] { employee_leave_17}),
                            Color = ColorTranslator.FromHtml(c_color[7])
                    },
                    /*new Series{
                            Type = DotNet.Highcharts.Enums.ChartTypes.Bar,
                            Name = "ลาเพื่อดูแลภรรยาคลอดบุตร",
                            Data = new Data(new object[] { employee_leave_18}),
                            Color = ColorTranslator.FromHtml(c_color[8])
                    },*/
                    /*new Series{
                            Type = DotNet.Highcharts.Enums.ChartTypes.Bar,
                            Name = "ลาพิเศษ (Drop งาน)",
                            Data = new Data(new object[] { employee_leave_19}),
                            Color = ColorTranslator.FromHtml(c_color[9])
                    },*/
                    /*new Series{
                            Type = DotNet.Highcharts.Enums.ChartTypes.Bar,
                            Name = "ลาเพื่อราชการทหาร",
                            Data = new Data(new object[] { employee_leave_20}),
                            Color = ColorTranslator.FromHtml(c_color[10])
                    },*/
                    /*new Series{
                            Type = DotNet.Highcharts.Enums.ChartTypes.Bar,
                            Name = "ลาเพื่อฝึกอบรมหรือพัฒนาความรู้",
                            Data = new Data(new object[] { employee_leave_21}),
                            Color = ColorTranslator.FromHtml(c_color[11])
                    },*/
                }

                );

                litReportLeaveEmployee.Text = chart.ToHtmlString();
            }
            else
            {
                litReportLeaveEmployee.Text = "<p class='bg-danger'>No result.</p>";
            }
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "แจ้งเตือน", "alert('" + "รบกวนกรอกข้อมูลวันที่และบริษัทให้ครบ" + "')", true);
        }
    }

    public void Select_Chart_Leave_Employee_Detail()
    {
        //TextBox txtempcode_s = (TextBox)Fv_Search_Emp_Report.FindControl("txtempcode_s");
        //TextBox txtempname_s = (TextBox)Fv_Search_Emp_Report.FindControl("txtempname_s");
        TextBox txtdatestart = (TextBox)Fv_Search_Emp_Report.FindControl("txtdatestart");
        TextBox txtdateend = (TextBox)Fv_Search_Emp_Report.FindControl("txtdateend");

        DropDownList ddlorg_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlorg_rp");
        DropDownList ddldep_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddldep_rp");
        DropDownList ddlsec_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlsec_rp");
        DropDownList ddlpos_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlpos_rp");
        DropDownList ddlemptype_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlemptype_s");
        DropDownList ddlempstatus_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlempstatus_s");
        //DropDownList ddldatetype = (DropDownList)Fv_Search_Emp_Report.FindControl("ddldatetype");
        //DropDownList ddljobgrade_start = (DropDownList)Fv_Search_Emp_Report.FindControl("ddljobgrade_start");
        //DropDownList ddljobgrade_end = (DropDownList)Fv_Search_Emp_Report.FindControl("ddljobgrade_end");
        DropDownList ddlAffiliation_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlAffiliation_s");
        DropDownList ddllocation_search = (DropDownList)Fv_Search_Emp_Report.FindControl("ddllocation_search");
        DropDownList ddl_target_report = (DropDownList)Fv_Search_Emp_Report.FindControl("ddl_target_report");

        if (txtdatestart.Text != "" && txtdateend.Text != "" && txtdatestart.Text != String.Empty && txtdateend.Text != String.Empty && int.Parse(ddlorg_rp.SelectedValue) != 0)
        {
            _dataEmployee.employee_list = new employee_detail[1];
            employee_detail _dataEmployee_ = new employee_detail();

            if (txtdatestart.Text != "")
            {
                _dataEmployee_.startdate_search = txtdatestart.Text;
            }
            else
            {
                _dataEmployee_.startdate_search = "";
            }

            if (txtdateend.Text != "")
            {
                _dataEmployee_.enddate_search = txtdateend.Text;
            }
            else
            {
                _dataEmployee_.enddate_search = "";
            }

            _dataEmployee_.org_idx = int.Parse(ddlorg_rp.SelectedValue);
            _dataEmployee_.rdept_idx = int.Parse(ddldep_rp.SelectedValue);
            _dataEmployee_.rsec_idx = int.Parse(ddlsec_rp.SelectedValue);
            _dataEmployee_.rpos_idx = int.Parse(ddlpos_rp.SelectedValue);
            _dataEmployee_.emp_type_idx = int.Parse(ddlemptype_s.SelectedValue);
            _dataEmployee_.emp_status = int.Parse(ddlempstatus_s.SelectedValue);
            _dataEmployee_.type_select_emp = 12; //Education    
            _dataEmployee_.ACIDX = int.Parse(ddlAffiliation_s.SelectedValue);
            _dataEmployee_.LocIDX = int.Parse(ddllocation_search.SelectedValue);
            _dataEmployee_.dept_name_en = ViewState["Columns_Check_dep_leave_Search"].ToString(); //checkboxlist
            //_dataEmployee_.TIDX = int.Parse(ddl_target_report.SelectedValue);

            _dataEmployee.employee_list[0] = _dataEmployee_;
            _dataEmployee = callServiceEmployee(urlGetEmployee, _dataEmployee);
            setGridData(Gv_LeaveEmployee, _dataEmployee.BoxEmployeeLeave_list);
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "แจ้งเตือน", "alert('" + "รบกวนกรอกข้อมูลวันที่และบริษัทให้ครบ" + "')", true);
        }

    }

    public void Select_Chart_Race()
    {
        DropDownList ddlorg_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlorg_rp");
        DropDownList ddldep_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddldep_rp");
        DropDownList ddlsec_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlsec_rp");
        DropDownList ddlpos_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlpos_rp");
        DropDownList ddlemptype_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlemptype_s");
        DropDownList ddlempstatus_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlempstatus_s");
        //DropDownList ddldatetype = (DropDownList)Fv_Search_Emp_Report.FindControl("ddldatetype");
        DropDownList ddljobgrade_start = (DropDownList)Fv_Search_Emp_Report.FindControl("ddljobgrade_start");
        DropDownList ddljobgrade_end = (DropDownList)Fv_Search_Emp_Report.FindControl("ddljobgrade_end");
        DropDownList ddlAffiliation_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlAffiliation_s");
        DropDownList ddllocation_search = (DropDownList)Fv_Search_Emp_Report.FindControl("ddllocation_search");
        DropDownList ddl_race_search = (DropDownList)Fv_Search_Emp_Report.FindControl("ddl_race_search");
        DropDownList ddl_target_report = (DropDownList)Fv_Search_Emp_Report.FindControl("ddl_target_report");

        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _dataEmployee_ = new employee_detail();

        _dataEmployee_.org_idx = int.Parse(ddlorg_rp.SelectedValue);
        _dataEmployee_.rdept_idx = int.Parse(ddldep_rp.SelectedValue);
        _dataEmployee_.rsec_idx = int.Parse(ddlsec_rp.SelectedValue);
        _dataEmployee_.rpos_idx = int.Parse(ddlpos_rp.SelectedValue);
        _dataEmployee_.emp_type_idx = int.Parse(ddlemptype_s.SelectedValue);
        _dataEmployee_.emp_status = int.Parse(ddlempstatus_s.SelectedValue);
        _dataEmployee_.type_select_emp = 13; //Race     
        //_dataEmployee_.datetype_search = int.Parse(ddldatetype.SelectedValue);
        _dataEmployee_.Search_jobgrade1 = int.Parse(ddljobgrade_start.SelectedValue);
        _dataEmployee_.Search_jobgrade2 = int.Parse(ddljobgrade_end.SelectedValue);
        _dataEmployee_.ACIDX = int.Parse(ddlAffiliation_s.SelectedValue);
        _dataEmployee_.LocIDX = int.Parse(ddllocation_search.SelectedValue);
        _dataEmployee_.race_idx = int.Parse(ddl_race_search.SelectedValue);
        //_dataEmployee_.TIDX = int.Parse(ddl_target_report.SelectedValue);

        _dataEmployee.employee_list[0] = _dataEmployee_;
        _dataEmployee = callServiceEmployee(urlGetEmployee, _dataEmployee);
        setGridData(Gv_race, _dataEmployee.employee_list);
        ViewState["nationlity_chart_list"] = _dataEmployee.employee_list;

        if (_dataEmployee.return_code == "0")
        {
            var caseclose = new List<object>();

            foreach (var data in _dataEmployee.employee_list)
            {
                caseclose.Add(new object[] { data.race_name_th.ToString(), data.race_idx.ToString() });
            }

            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new Title { Text = "สัญชาติ" });
            chart.InitChart(new Chart { DefaultSeriesType = DotNet.Highcharts.Enums.ChartTypes.Pie });

            chart.SetPlotOptions(new PlotOptions
            {
                Pie = new PlotOptionsPie
                {

                    ShowInLegend = true,
                    AllowPointSelect = true,
                    DataLabels = new PlotOptionsPieDataLabels
                    {
                        Enabled = true,
                        Formatter = "function() { return this.point.name +' : '+  this.y.toFixed(0)+' '; }"
                    }
                }
            });

            chart.SetTooltip(new Tooltip
            {
                Enabled = true,
                Formatter = "function() { return this.point.name +' : '+  this.percentage.toFixed(2)+' % '; }"

            });

            chart.SetSeries(new Series[]
                 {
                        new Series()
                        {
                            Data = new Data (caseclose.ToArray())
                        }
                 });

            litReportRace.Text = chart.ToHtmlString();
        }
        else
        {
            litReportRace.Text = "<p class='bg-danger'>No result.</p>";
        }

    }

    public void Select_Chart_New_Employee()
    {
        TextBox txtdatestart = (TextBox)Fv_Search_Emp_Report.FindControl("txtdatestart");
        TextBox txtdateend = (TextBox)Fv_Search_Emp_Report.FindControl("txtdateend");

        DropDownList ddlorg_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlorg_rp");
        DropDownList ddldep_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddldep_rp");
        DropDownList ddlsec_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlsec_rp");
        DropDownList ddlpos_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlpos_rp");
        DropDownList ddlemptype_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlemptype_s");
        DropDownList ddlempstatus_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlempstatus_s");
        DropDownList ddljobgrade_start = (DropDownList)Fv_Search_Emp_Report.FindControl("ddljobgrade_start");
        DropDownList ddljobgrade_end = (DropDownList)Fv_Search_Emp_Report.FindControl("ddljobgrade_end");
        DropDownList ddldatetype = (DropDownList)Fv_Search_Emp_Report.FindControl("ddldatetype");
        //TextBox txtage_start = (TextBox)Fv_Search_Emp_Report.FindControl("txtage_start");
        //TextBox txtage_end = (TextBox)Fv_Search_Emp_Report.FindControl("txtage_end");
        DropDownList ddlAffiliation_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlAffiliation_s");
        DropDownList ddllocation_search = (DropDownList)Fv_Search_Emp_Report.FindControl("ddllocation_search");
        DropDownList ddl_target_report = (DropDownList)Fv_Search_Emp_Report.FindControl("ddl_target_report");

        if (txtdatestart.Text != "" && txtdateend.Text != "" && txtdatestart.Text != String.Empty && txtdateend.Text != String.Empty)
        {
            _dataEmployee.employee_list = new employee_detail[1];
            employee_detail _dataEmployee_ = new employee_detail();

            _dataEmployee_.org_idx = int.Parse(ddlorg_rp.SelectedValue);
            _dataEmployee_.rdept_idx = int.Parse(ddldep_rp.SelectedValue);
            _dataEmployee_.rsec_idx = int.Parse(ddlsec_rp.SelectedValue);
            _dataEmployee_.rpos_idx = int.Parse(ddlpos_rp.SelectedValue);
            _dataEmployee_.emp_type_idx = int.Parse(ddlemptype_s.SelectedValue);
            _dataEmployee_.Search_jobgrade1 = int.Parse(ddljobgrade_start.SelectedValue);
            _dataEmployee_.Search_jobgrade2 = int.Parse(ddljobgrade_end.SelectedValue);
            _dataEmployee_.emp_status = int.Parse(ddlempstatus_s.SelectedValue);
            _dataEmployee_.type_select_emp = 14; //new employee     
            _dataEmployee_.datetype_search = int.Parse(ddldatetype.SelectedValue);
            if (txtdatestart.Text != "")
            {
                _dataEmployee_.startdate_search = txtdatestart.Text;
            }
            else
            {
                _dataEmployee_.startdate_search = "";
            }

            if (txtdateend.Text != "")
            {
                _dataEmployee_.enddate_search = txtdateend.Text;
            }
            else
            {
                _dataEmployee_.enddate_search = "";
            }

            _dataEmployee_.ACIDX = int.Parse(ddlAffiliation_s.SelectedValue);
            _dataEmployee_.LocIDX = int.Parse(ddllocation_search.SelectedValue);
            //_dataEmployee_.TIDX = int.Parse(ddl_target_report.SelectedValue);

            _dataEmployee.employee_list[0] = _dataEmployee_;
            //fss.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
            _dataEmployee = callServiceEmployee(urlGetEmployee, _dataEmployee);
            setGridData(Gv_new_employee, _dataEmployee.employee_list);
            ViewState["newemp_chart_list"] = _dataEmployee.employee_list;

            lbltotal_emp.Text = _dataEmployee.return_msg.ToString();
            //fss.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));

            if (_dataEmployee.return_code == "0")
            {
                int count = _dataEmployee.employee_list.Length;
                string[] lv1code = new string[count];
                object[] lv1count = new object[count];
                int i = 0;
                foreach (var data in _dataEmployee.employee_list)
                {
                    lv1code[i] = data.dept_name_th.ToString();
                    lv1count[i] = data.rdept_idx.ToString();
                    i++;
                }
                Highcharts chart = new Highcharts("chart");
                chart.SetTitle(new Title { Text = "" });
                chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of New Employee" } } });
                chart.SetXAxis(new XAxis { Categories = lv1code });
                chart.SetPlotOptions(new PlotOptions
                {
                    Column = new PlotOptionsColumn
                    {
                        Stacking = DotNet.Highcharts.Enums.Stackings.Normal,
                        BorderWidth = 2,
                        BorderColor = Color.Gray,
                        Shadow = false,
                        DataLabels = new PlotOptionsColumnDataLabels
                        {
                            Enabled = true,
                            Color = Color.White,
                            Shadow = true,
                        }
                    },
                    Bar = new PlotOptionsBar
                    {
                        Stacking = DotNet.Highcharts.Enums.Stackings.Normal,
                        BorderWidth = 1,
                        BorderColor = Color.Gray,
                        Shadow = false,
                        DataLabels = new PlotOptionsBarDataLabels
                        {
                            Enabled = true,
                            Color = Color.White,
                            Shadow = true,
                            //Style = FontSize.XXSmall.ToString(),
                            //Formatter = new Style { CssClass="s" }
                            //Style = new Style { BorderColor = s }
                        }
                    }
                });
                chart.SetLegend(new Legend
                {
                    Layout = DotNet.Highcharts.Enums.Layouts.Vertical,
                });

                chart.SetSeries(
                    new Series
                    {
                        Type = DotNet.Highcharts.Enums.ChartTypes.Bar,
                        Name = "Name New Employee",
                        Stack = "1",
                        Data = new Data(lv1count),
                    }
                );

                litReportnew_employee.Text = chart.ToHtmlString();
            }
            else
            {
                litReportnew_employee.Text = "<p class='bg-danger'>No result.</p>";
            }

            Select_Chart_New_Employee_detail();
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "แจ้งเตือน", "alert('" + "รบกวนกรอกข้อมูลวันที่ให้ครบ" + "')", true);

        }
    }

    public void Select_Chart_New_Employee_detail()
    {
        TextBox txtdatestart = (TextBox)Fv_Search_Emp_Report.FindControl("txtdatestart");
        TextBox txtdateend = (TextBox)Fv_Search_Emp_Report.FindControl("txtdateend");

        DropDownList ddlorg_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlorg_rp");
        DropDownList ddldep_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddldep_rp");
        DropDownList ddlsec_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlsec_rp");
        DropDownList ddlpos_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlpos_rp");
        DropDownList ddlemptype_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlemptype_s");
        DropDownList ddlempstatus_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlempstatus_s");
        DropDownList ddljobgrade_start = (DropDownList)Fv_Search_Emp_Report.FindControl("ddljobgrade_start");
        DropDownList ddljobgrade_end = (DropDownList)Fv_Search_Emp_Report.FindControl("ddljobgrade_end");
        DropDownList ddldatetype = (DropDownList)Fv_Search_Emp_Report.FindControl("ddldatetype");
        //TextBox txtage_start = (TextBox)Fv_Search_Emp_Report.FindControl("txtage_start");
        //TextBox txtage_end = (TextBox)Fv_Search_Emp_Report.FindControl("txtage_end");
        DropDownList ddlAffiliation_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlAffiliation_s");
        DropDownList ddllocation_search = (DropDownList)Fv_Search_Emp_Report.FindControl("ddllocation_search");
        DropDownList ddl_target_report = (DropDownList)Fv_Search_Emp_Report.FindControl("ddl_target_report");

        if (txtdatestart.Text != "" && txtdateend.Text != "" && txtdatestart.Text != String.Empty && txtdateend.Text != String.Empty)
        {
            _dataEmployee.employee_list = new employee_detail[1];
            employee_detail _dataEmployee_ = new employee_detail();

            _dataEmployee_.org_idx = int.Parse(ddlorg_rp.SelectedValue);
            _dataEmployee_.rdept_idx = int.Parse(ddldep_rp.SelectedValue);
            _dataEmployee_.rsec_idx = int.Parse(ddlsec_rp.SelectedValue);
            _dataEmployee_.rpos_idx = int.Parse(ddlpos_rp.SelectedValue);
            _dataEmployee_.emp_type_idx = int.Parse(ddlemptype_s.SelectedValue);
            _dataEmployee_.Search_jobgrade1 = int.Parse(ddljobgrade_start.SelectedValue);
            _dataEmployee_.Search_jobgrade2 = int.Parse(ddljobgrade_end.SelectedValue);
            _dataEmployee_.emp_status = int.Parse(ddlempstatus_s.SelectedValue);
            _dataEmployee_.type_select_emp = 15; //new employee     
            _dataEmployee_.datetype_search = 1; //int.Parse(ddldatetype.SelectedValue);

            if (txtdatestart.Text != "")
            {
                _dataEmployee_.startdate_search = txtdatestart.Text;
            }
            else
            {
                _dataEmployee_.startdate_search = "";
            }

            if (txtdateend.Text != "")
            {
                _dataEmployee_.enddate_search = txtdateend.Text;
            }
            else
            {
                _dataEmployee_.enddate_search = "";
            }

            _dataEmployee_.ACIDX = int.Parse(ddlAffiliation_s.SelectedValue);
            _dataEmployee_.LocIDX = int.Parse(ddllocation_search.SelectedValue);
            //_dataEmployee_.TIDX = int.Parse(ddl_target_report.SelectedValue);

            _dataEmployee.employee_list[0] = _dataEmployee_;
            //fss.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
            _dataEmployee = callServiceEmployee(urlGetEmployee, _dataEmployee);
            setGridData(Gv_report_new_employee_name, _dataEmployee.employee_list);
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "แจ้งเตือน", "alert('" + "รบกวนกรอกข้อมูลวันที่ให้ครบ" + "')", true);

        }

    }

    protected void select_positiongroup(DropDownList ddlName)
    {
        _dtpmform = new data_tpm_form();
        _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
        tpmu0_DocFormDetail dataselect = new tpmu0_DocFormDetail();

        dataselect.condition = 5;

        _dtpmform.Boxtpmu0_DocFormDetail[0] = dataselect;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        _dtpmform = callServicePostTPMForm(_urlSelectMaster, _dtpmform);
        setDdlData(ddlName, _dtpmform.Boxtpmm0_DocFormDetail, "pos_name", "posidx");
        ddlName.Items.Insert(0, new ListItem("เลือกประเภทกลุ่มพนักงาน...", "0"));
    }

    protected void Select_ddl_resgin_comment(DropDownList ddlName)
    {
        _dataEmployee.BoxEmployee_ProbationList = new Employee_Probation[1];
        Employee_Probation _resignList = new Employee_Probation();
        _resignList.Condition = 4;
        _dataEmployee.BoxEmployee_ProbationList[0] = _resignList;

        _dataEmployee = callServiceEmployee(_urlSelect_Report_Probation, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.BoxEmployeeResign_list, "RT_Name", "RTIDX");
        ddlName.Items.Insert(0, new ListItem("เลือก....", "0"));
    }

    #endregion

    #region INSERT / UPDATE

    protected void Insert_Employee()
    {
        //------------------------------- Page 1

        //re organization
        var ddl_target_cen = (DropDownList)FvInsertEmp.FindControl("ddl_target_cen");
        var ddl_costcenter_cen = (DropDownList)FvInsertEmp.FindControl("ddl_costcenter_cen");

        // re organization

        var txt_startdate = (TextBox)FvInsertEmp.FindControl("txt_startdate");
        var ddl_target = (DropDownList)FvInsertEmp.FindControl("ddl_target");
        var ddlemptype = (DropDownList)FvInsertEmp.FindControl("ddlemptype");
        var ddlAffiliation = (DropDownList)FvInsertEmp.FindControl("ddlAffiliation");
        var ddl_prefix_th = (DropDownList)FvInsertEmp.FindControl("ddl_prefix_th");
        var txt_name_th = (TextBox)FvInsertEmp.FindControl("txt_name_th");
        var txt_surname_th = (TextBox)FvInsertEmp.FindControl("txt_surname_th");
        var ddl_prefix_en = (DropDownList)FvInsertEmp.FindControl("ddl_prefix_en");
        var txt_name_en = (TextBox)FvInsertEmp.FindControl("txt_name_en");
        var txt_surname_en = (TextBox)FvInsertEmp.FindControl("txt_surname_en");
        var txt_nickname_th = (TextBox)FvInsertEmp.FindControl("txt_nickname_th");
        var txt_nickname_en = (TextBox)FvInsertEmp.FindControl("txt_nickname_en");
        var ddlcostcenter = (DropDownList)FvInsertEmp.FindControl("ddlcostcenter");
        var txtbirth = (TextBox)FvInsertEmp.FindControl("txtbirth");
        var ddl_sex = (DropDownList)FvInsertEmp.FindControl("ddl_sex");
        var ddl_status = (DropDownList)FvInsertEmp.FindControl("ddl_status");
        var ddlnation = (DropDownList)FvInsertEmp.FindControl("ddlnation");
        var ddlrace = (DropDownList)FvInsertEmp.FindControl("ddlrace");
        var ddlreligion = (DropDownList)FvInsertEmp.FindControl("ddlreligion");
        var ddlmilitary = (DropDownList)FvInsertEmp.FindControl("ddlmilitary");
        var txtidcard = (TextBox)FvInsertEmp.FindControl("txtidcard");
        var txtissued_at = (TextBox)FvInsertEmp.FindControl("txtissued_at");
        var txtexpcard = (TextBox)FvInsertEmp.FindControl("txtexpcard");

        var txtid_passportcard = (TextBox)FvInsertEmp.FindControl("txtid_passportcard");
        var txtdate_passport = (TextBox)FvInsertEmp.FindControl("txtdate_passport");
        var txtdate_Workpermit = (TextBox)FvInsertEmp.FindControl("txtdate_Workpermit");

        var txt_passport_issued_add = (TextBox)FvInsertEmp.FindControl("txt_passport_issued_add"); //Passport Issued at
        var txtdatestart_passport_add = (TextBox)FvInsertEmp.FindControl("txtdatestart_passport_add"); //Passport Startdate
        var txt_workpermit_no_add = (TextBox)FvInsertEmp.FindControl("txt_workpermit_no_add"); //Workprtmit ID
        var txt_workpermit_issued_add = (TextBox)FvInsertEmp.FindControl("txt_workpermit_issued_add"); //Workpermit Issued at
        var txt_Workpermit_start_add = (TextBox)FvInsertEmp.FindControl("txt_Workpermit_start_add"); //Workpermit Startdate

        var ddllocation = (DropDownList)FvInsertEmp.FindControl("ddllocation");
        var ddltypevisa = (DropDownList)FvInsertEmp.FindControl("ddltypevisa");
        var txtidcard_visa = (TextBox)FvInsertEmp.FindControl("txtidcard_visa");
        var txtdatevisa_s = (TextBox)FvInsertEmp.FindControl("txtdatevisa_s");
        var txtdatevisa_e = (TextBox)FvInsertEmp.FindControl("txtdatevisa_e");

        var ddldvcar = (DropDownList)FvInsertEmp.FindControl("ddldvcar");
        var ddldvmt = (DropDownList)FvInsertEmp.FindControl("ddldvmt");
        var ddldvfork = (DropDownList)FvInsertEmp.FindControl("ddldvfork");
        var ddldvtruck = (DropDownList)FvInsertEmp.FindControl("ddldvtruck");

        var ddldvcarstatus = (DropDownList)FvInsertEmp.FindControl("ddldvcarstatus");
        var ddldvmtstatus = (DropDownList)FvInsertEmp.FindControl("ddldvmtstatus");

        var txtlicens_car = (TextBox)FvInsertEmp.FindControl("txtlicens_car");
        var txtlicens_moto = (TextBox)FvInsertEmp.FindControl("txtlicens_moto");
        var txtlicens_fork = (TextBox)FvInsertEmp.FindControl("txtlicens_fork");
        var txtlicens_truck = (TextBox)FvInsertEmp.FindControl("txtlicens_truck");

        var dllbank = (DropDownList)FvInsertEmp.FindControl("dllbank");
        var txtaccountname = (TextBox)FvInsertEmp.FindControl("txtaccountname");
        var txtaccountnumber = (TextBox)FvInsertEmp.FindControl("txtaccountnumber");
        var txtemercon = (TextBox)FvInsertEmp.FindControl("txtemercon");
        var txtrelation = (TextBox)FvInsertEmp.FindControl("txtrelation");
        var txttelemer = (TextBox)FvInsertEmp.FindControl("txttelemer");

        var txtfathername = (TextBox)FvInsertEmp.FindControl("txtfathername");
        var txttelfather = (TextBox)FvInsertEmp.FindControl("txttelfather");
        var txtlicenfather = (TextBox)FvInsertEmp.FindControl("txtlicenfather");
        var txtmothername = (TextBox)FvInsertEmp.FindControl("txtmothername");
        var txttelmother = (TextBox)FvInsertEmp.FindControl("txttelmother");
        var txtlicenmother = (TextBox)FvInsertEmp.FindControl("txtlicenmother");
        var txtwifename = (TextBox)FvInsertEmp.FindControl("txtwifename");
        var txttelwife = (TextBox)FvInsertEmp.FindControl("txttelwife");
        var txtlicenwifename = (TextBox)FvInsertEmp.FindControl("txtlicenwifename");

        var txtchildname = (TextBox)FvInsertEmp.FindControl("txtchildname");
        var ddlchildnumber = (DropDownList)FvInsertEmp.FindControl("ddlchildnumber");
        var GvChildAdd = (GridView)FvInsertEmp.FindControl("GvChildAdd");

        var txtorgold = (TextBox)FvInsertEmp.FindControl("txtorgold");
        var txtposold = (TextBox)FvInsertEmp.FindControl("txtposold");
        //var txtworktimeold = (TextBox)FvInsertEmp.FindControl("txtworktimeold");
        var txtstartdate_ex = (TextBox)FvInsertEmp.FindControl("txtstartdate_ex");
        var txtresigndate_ex = (TextBox)FvInsertEmp.FindControl("txtresigndate_ex");
        var GvPri = (GridView)FvInsertEmp.FindControl("GvPri");

        var ddleducationback = (DropDownList)FvInsertEmp.FindControl("ddleducationback");
        var txtschoolname = (TextBox)FvInsertEmp.FindControl("txtschoolname");
        var txtstarteducation = (TextBox)FvInsertEmp.FindControl("txtstarteducation");
        var txtendeducation = (TextBox)FvInsertEmp.FindControl("txtendeducation");
        var txtstudy = (TextBox)FvInsertEmp.FindControl("txtstudy");
        var GvEducation = (GridView)FvInsertEmp.FindControl("GvEducation");

        var txttraincourses = (TextBox)FvInsertEmp.FindControl("txttraincourses");
        var txttraindate = (TextBox)FvInsertEmp.FindControl("txttraindate");
        var txttrainassessment = (TextBox)FvInsertEmp.FindControl("txttrainassessment");
        var txttraindateend = (TextBox)FvInsertEmp.FindControl("txttraindateend");
        var GvTrain = (GridView)FvInsertEmp.FindControl("GvTrain");


        //------------------------------- Page 2

        var ddlorg = (DropDownList)FvInsertEmp.FindControl("ddlorg");
        var ddldep = (DropDownList)FvInsertEmp.FindControl("ddldep");
        var ddlsec = (DropDownList)FvInsertEmp.FindControl("ddlsec");
        var ddlpos = (DropDownList)FvInsertEmp.FindControl("ddlpos");
        var ddlorg_shift = (DropDownList)FvInsertEmp.FindControl("ddlorg_shift");
        var ddlshift_add = (DropDownList)FvInsertEmp.FindControl("ddlshift_add");
        var ddlshift_add_ = (DropDownList)FvInsertEmp.FindControl("ddlshift_add_");

        //re organization
        
        var ddlPosition = (DropDownList)FvInsertEmp.FindControl("ddlPosition");

        //------------------------------- Page 3

        var txtaddress_present = (TextBox)FvInsertEmp.FindControl("txtaddress_present");
        var ddlcountry_present = (DropDownList)FvInsertEmp.FindControl("ddlcountry_present");
        var ddlprovince_present = (DropDownList)FvInsertEmp.FindControl("ddlprovince_present");
        var ddlamphoe_present = (DropDownList)FvInsertEmp.FindControl("ddlamphoe_present");
        var ddldistrict_present = (DropDownList)FvInsertEmp.FindControl("ddldistrict_present");
        var txttelmobile_present = (TextBox)FvInsertEmp.FindControl("txttelmobile_present");
        var txttelhome_present = (TextBox)FvInsertEmp.FindControl("txttelhome_present");
        var txtzipcode_present = (TextBox)FvInsertEmp.FindControl("txtzipcode_present");
        var txtemail_present = (TextBox)FvInsertEmp.FindControl("txtemail_present");
        var txtpersonal_email = (TextBox)FvInsertEmp.FindControl("txtpersonal_email");

        var txtaddress_permanentaddress = (TextBox)FvInsertEmp.FindControl("txtaddress_permanentaddress");
        var ddlcountry_permanentaddress = (DropDownList)FvInsertEmp.FindControl("ddlcountry_permanentaddress");
        var ddlprovince_permanentaddress = (DropDownList)FvInsertEmp.FindControl("ddlprovince_permanentaddress");
        var ddlamphoe_permanentaddress = (DropDownList)FvInsertEmp.FindControl("ddlamphoe_permanentaddress");
        var ddldistrict_permanentaddress = (DropDownList)FvInsertEmp.FindControl("ddldistrict_permanentaddress");
        var txttelmobile_permanentaddress = (TextBox)FvInsertEmp.FindControl("txttelmobile_permanentaddress");
        var txttelhome_permanentaddress = (TextBox)FvInsertEmp.FindControl("txttelhome_permanentaddress");

        //------------------------------- Page 4

        var ddlhospital = (DropDownList)FvInsertEmp.FindControl("ddlhospital");
        var txtexaminationdate = (TextBox)FvInsertEmp.FindControl("txtexaminationdate");
        var txtsecurityid = (TextBox)FvInsertEmp.FindControl("txtsecurityid");
        var txtexpsecurity = (TextBox)FvInsertEmp.FindControl("txtexpsecurity");
        var txtheight = (TextBox)FvInsertEmp.FindControl("txtheight");
        var txtweight = (TextBox)FvInsertEmp.FindControl("txtweight");
        var ddlblood = (DropDownList)FvInsertEmp.FindControl("ddlblood");
        var txtscar = (TextBox)FvInsertEmp.FindControl("txtscar");
        var ddlmedicalcertificate = (DropDownList)FvInsertEmp.FindControl("ddlmedicalcertificate");
        var ddlresultlab = (DropDownList)FvInsertEmp.FindControl("ddlresultlab");

        //------------------------------- Page 5

        var ddlapprove1 = (DropDownList)FvInsertEmp.FindControl("ddlapprove1");
        var ddlapprove2 = (DropDownList)FvInsertEmp.FindControl("ddlapprove2");

        var ddlapprove1_cen = (DropDownList)FvInsertEmp.FindControl("ddlapprove1_cen");
        var ddlapprove2_cen = (DropDownList)FvInsertEmp.FindControl("ddlapprove2_cen");

        var ddlEmpApprove1Cen = (DropDownList)FvInsertEmp.FindControl("ddlEmpApprove1Cen");
        var ddlEmpApprove2Cen = (DropDownList)FvInsertEmp.FindControl("ddlEmpApprove2Cen");


        int a = 0;
        int b = 0;
        int c = 0;
        int d = 0;
        int e = 0;
        int f = 0;

        var _dataEmployee_ = new employee_detail[1];
        _dataEmployee_[0] = new employee_detail();

        if (txt_startdate.Text != "") { _dataEmployee_[0].emp_start_date = txt_startdate.Text; } else { _dataEmployee_[0].emp_start_date = "01/01/1900"; }
        _dataEmployee_[0].TIDX = int.Parse(ddl_target.SelectedValue);
        //_dataEmployee_[0].empgroup_idx = int.Parse(ddl_target_cen.SelectedValue); // re organization
        _dataEmployee_[0].emp_type_idx = int.Parse(ddlemptype.SelectedValue);
        _dataEmployee_[0].ACIDX = int.Parse(ddlAffiliation.SelectedValue);
        _dataEmployee_[0].prefix_idx = int.Parse(ddl_prefix_th.SelectedValue);

        if (txt_name_th.Text != "") { _dataEmployee_[0].emp_firstname_th = txt_name_th.Text; } else { _dataEmployee_[0].emp_firstname_th = "-"; }
        if (txt_surname_th.Text != "") { _dataEmployee_[0].emp_lastname_th = txt_surname_th.Text; } else { _dataEmployee_[0].emp_lastname_th = "-"; }

        _dataEmployee_[0].emp_firstname_en = txt_name_en.Text;
        _dataEmployee_[0].emp_lastname_en = txt_surname_en.Text;
        _dataEmployee_[0].emp_nickname_th = txt_nickname_th.Text;
        _dataEmployee_[0].emp_nickname_en = txt_nickname_en.Text;
        _dataEmployee_[0].costcenter_idx = int.Parse(ddl_costcenter_cen.SelectedValue);
        //_dataEmployee_[0].cost_idx = int.Parse(ddlcostcenter.SelectedValue); // re organization
        if (txtbirth.Text != "") { _dataEmployee_[0].emp_birthday = txtbirth.Text; } else { _dataEmployee_[0].emp_birthday = "01/01/1900"; }

        _dataEmployee_[0].LocIDX = int.Parse(ddllocation.SelectedValue); //

        _dataEmployee_[0].sex_idx = int.Parse(ddl_sex.SelectedValue);
        _dataEmployee_[0].married_status = int.Parse(ddl_status.SelectedValue); // สถานะแต่งงาน
        _dataEmployee_[0].nat_idx = int.Parse(ddlnation.SelectedValue);
        _dataEmployee_[0].race_idx = int.Parse(ddlrace.SelectedValue);
        _dataEmployee_[0].rel_idx = int.Parse(ddlreligion.SelectedValue);
        _dataEmployee_[0].mil_idx = int.Parse(ddlmilitary.SelectedValue); // สถานะทางทหาร
        _dataEmployee_[0].identity_card = txtidcard.Text; // รหัสบัตรประชน
        _dataEmployee_[0].issued_at = txtissued_at.Text; // สถานที่ออกบัตร


        if (txtexpcard.Text != "") { _dataEmployee_[0].idate_expired = txtexpcard.Text; } else { _dataEmployee_[0].idate_expired = "01/01/1900"; } // วันที่หมดอายุ
        
        // U0_EmployeeTH

        //U0_Employee_Other
        //ข้อมูลต่างด้าว
        _dataEmployee_[0].passportID = txtid_passportcard.Text; // เลขที่ passport
        _dataEmployee_[0].passport_enddate = txtdate_passport.Text; // วันหมดอายุ passport
        _dataEmployee_[0].workpermit_enddate = txtdate_Workpermit.Text; // วันหมดอายุ workpermis

        _dataEmployee_[0].passport_issuat = txt_passport_issued_add.Text; // Passport Issued at
        _dataEmployee_[0].passport_start = txtdatestart_passport_add.Text; // Passport Startdate
        _dataEmployee_[0].workpermitID = txt_workpermit_no_add.Text; // Workprtmit ID
        _dataEmployee_[0].workpermit_issuat = txt_workpermit_issued_add.Text; // Workpermit Issued at
        _dataEmployee_[0].workpermit_start = txt_Workpermit_start_add.Text; // Workpermit Startdate
        //ข้อมูลต่างด้าว


        _dataEmployee_[0].dvcar_idx = int.Parse(ddldvcar.SelectedValue); // สถานะขับขี่รถยนต์ - New Table
        _dataEmployee_[0].dvmt_idx = int.Parse(ddldvmt.SelectedValue); // สถานะขับขี่รถมอเตอร์ไซ - New Table
        _dataEmployee_[0].dvfork_idx = int.Parse(ddldvfork.SelectedValue); //  
        _dataEmployee_[0].dvtruck_idx = int.Parse(ddldvtruck.SelectedValue); //  
        _dataEmployee_[0].dvcarstatus_idx = int.Parse(ddldvcarstatus.SelectedValue); //  มีรถยนต์ 
        _dataEmployee_[0].dvmtstatus_idx = int.Parse(ddldvmtstatus.SelectedValue); //  มีรถมอเตอร์ไซต์ 
        _dataEmployee_[0].dvlicen_car = txtlicens_car.Text; //  
        _dataEmployee_[0].dvlicen_moto = txtlicens_moto.Text; //  
        _dataEmployee_[0].dvlicen_fork = txtlicens_fork.Text; // 
        _dataEmployee_[0].dvlicen_truck = txtlicens_truck.Text; //  

        _dataEmployee_[0].bank_idx = int.Parse(dllbank.SelectedValue); // ธนาคาร - New Table
        _dataEmployee_[0].bank_name = txtaccountname.Text; // ชื่อบัญชี
        _dataEmployee_[0].bank_no = txtaccountnumber.Text; // เลขที่บัญชี

        _dataEmployee_[0].emer_name = txtemercon.Text; // กรณีฉุกเฉินติดต่อ
        _dataEmployee_[0].emer_relation = txtrelation.Text; // ความสัมพันธ์
        _dataEmployee_[0].emer_tel = txttelemer.Text; // เบอร์โทร

        _dataEmployee_[0].fathername = txtfathername.Text; // ชื่อบิดา
        _dataEmployee_[0].telfather = txttelfather.Text; // เบอร์ติดต่อ
        _dataEmployee_[0].licen_father = txtlicenfather.Text; //
        _dataEmployee_[0].mothername = txtmothername.Text; // ชื่อมารดา
        _dataEmployee_[0].telmother = txttelmother.Text; // เบอร์ติดต่อ
        _dataEmployee_[0].licen_mother = txtlicenmother.Text; //
        _dataEmployee_[0].wifename = txtwifename.Text; // ชื่อภรรยา
        _dataEmployee_[0].telwife = txttelwife.Text; // เบอร์ติดต่อ
        _dataEmployee_[0].licen_wife = txtlicenwifename.Text; // 

        //U0_Employee_Other



        _dataEmployee_[0].org_idx = int.Parse(ddlorg.SelectedValue);
        _dataEmployee_[0].rdept_idx = int.Parse(ddldep.SelectedValue);
        _dataEmployee_[0].rsec_idx = int.Parse(ddlsec.SelectedValue);

        if (int.Parse(ddlemptype.SelectedValue) == 2) //รายเดือน
        {
            _dataEmployee_[0].rpos_idx = int.Parse(ddlpos.SelectedValue);
        }
        /*else //รายวัน
        {
            _dataEmployee_[0].rpos_idx = 3835;
        }*/

        if (int.Parse(ddlpos.SelectedValue) == 0)
        {
            _dataEmployee_[0].rpos_idx = _rpos_idx_insert;
        }
        else
        {
            _dataEmployee_[0].rpos_idx = int.Parse(ddlpos.SelectedValue);
        }

        //------------------------------- Page 3
        _dataEmployee_[0].emp_address = txtaddress_present.Text; // ******************* ที่อยู่ปัจจุบัน
        _dataEmployee_[0].country_idx = int.Parse(ddlcountry_present.SelectedValue);
        _dataEmployee_[0].prov_idx = int.Parse(ddlprovince_present.SelectedValue);
        _dataEmployee_[0].amp_idx = int.Parse(ddlamphoe_present.SelectedValue);
        _dataEmployee_[0].dist_idx = int.Parse(ddldistrict_present.SelectedValue);
        _dataEmployee_[0].emp_mobile_no = txttelmobile_present.Text;  // ******
        _dataEmployee_[0].emp_phone_no = txttelhome_present.Text;   // ******
        _dataEmployee_[0].post_code = txtzipcode_present.Text;
        _dataEmployee_[0].emp_email = txtemail_present.Text;
        _dataEmployee_[0].personal_email = txtpersonal_email.Text;

        _dataEmployee_[0].emp_address_permanent = txtaddress_permanentaddress.Text;  // ******************* ที่อยู่ตามทะเบียนบ้าน
        _dataEmployee_[0].country_idx_permanent = int.Parse(ddlcountry_permanentaddress.SelectedValue);
        _dataEmployee_[0].prov_idx_permanent = int.Parse(ddlprovince_permanentaddress.SelectedValue);
        _dataEmployee_[0].amp_idx_permanent = int.Parse(ddlamphoe_permanentaddress.SelectedValue);
        _dataEmployee_[0].dist_idx_permanent = int.Parse(ddldistrict_permanentaddress.SelectedValue);
        _dataEmployee_[0].PhoneNumber_permanent = txttelmobile_permanentaddress.Text;  // ******


        //------------------------------- Page 4

        // _dataEmployee_[0].soc_hos_idx = int.Parse(ddlhospital.SelectedValue);  // โรงพยาบาล
        // if (txtexaminationdate.Text != "") { _dataEmployee_[0].examinationdate = txtexaminationdate.Text; } else { _dataEmployee_[0].examinationdate = "01/01/1900"; } // วันที่ตรวจสุขภาพ
        // _dataEmployee_[0].soc_no = txtsecurityid.Text;  // เลขที่ประกันสังคม       
        // if (txtexpsecurity.Text != "") { _dataEmployee_[0].soc_expired_date = txtexpsecurity.Text; } else { _dataEmployee_[0].soc_expired_date = "01/01/1900"; } // วันหมดอายุประกันสังคม ปิดไป


        // if (txtheight.Text == "") { _dataEmployee_[0].height_s = 0; } else { _dataEmployee_[0].height_s = int.Parse(txtheight.Text); } // ส่วนสูง
        // if (txtweight.Text == "") { _dataEmployee_[0].weight_s = 0; } else { _dataEmployee_[0].weight_s = int.Parse(txtweight.Text); } // น้ำหนัก  

        

        // _dataEmployee_[0].btype_idx = int.Parse(ddlblood.SelectedValue);  // กรุ๊ปเลือด
        // _dataEmployee_[0].scar_s = txtscar.Text;  // แผลเป้น
        // _dataEmployee_[0].medicalcertificate_id = int.Parse(ddlmedicalcertificate.SelectedValue); //ใบรับรอง
        // _dataEmployee_[0].resultlab_id = int.Parse(ddlresultlab.SelectedValue); // ผลตรวจ LAB
        //------------------------------- Page 4

        _dataEmployee_[0].emp_idx_approve1 = int.Parse(ddlapprove1.SelectedValue);
        _dataEmployee_[0].emp_idx_approve2 = int.Parse(ddlapprove2.SelectedValue);
        _dataEmployee_[0].PIDX = int.Parse(ddlshift_add_.SelectedValue);

        _dtEmployee.employee_list = _dataEmployee_;

        // page 4
        var _data_u0_health = new employee_health_detail[1];
        _data_u0_health[0] = new employee_health_detail();

         _data_u0_health[0].soc_hos_idx = int.Parse(ddlhospital.SelectedValue);  // โรงพยาบาล
        if (txtexaminationdate.Text != "") { _data_u0_health[0].examinationdate = txtexaminationdate.Text; } else { _data_u0_health[0].examinationdate = "01/01/1900"; } // วันที่ตรวจสุขภาพ
        _data_u0_health[0].soc_no = txtsecurityid.Text;  // เลขที่ประกันสังคม       
        if (txtexpsecurity.Text != "") { _data_u0_health[0].soc_expired_date = txtexpsecurity.Text; } else { _data_u0_health[0].soc_expired_date = "01/01/1900"; } // วันหมดอายุประกันสังคม ปิดไป


        if (txtheight.Text == "") { _data_u0_health[0].height_s = 0; } else { _data_u0_health[0].height_s = int.Parse(txtheight.Text); } // ส่วนสูง
        if (txtweight.Text == "") { _data_u0_health[0].weight_s = 0; } else { _data_u0_health[0].weight_s = int.Parse(txtweight.Text); } // น้ำหนัก  

        

        _data_u0_health[0].btype_idx = int.Parse(ddlblood.SelectedValue);  // กรุ๊ปเลือด
        _data_u0_health[0].scar_s = txtscar.Text;  // แผลเป้น
        _data_u0_health[0].medicalcertificate_id = int.Parse(ddlmedicalcertificate.SelectedValue); //ใบรับรอง
        _data_u0_health[0].resultlab_id = int.Parse(ddlresultlab.SelectedValue); // ผลตรวจ LAB
        
        _dtEmployee.employee_health_detail_list = _data_u0_health;
        // page 4

        // page 5
        var _data_approve_u0 = new cen_approve_u0_detail[1];
        _data_approve_u0[0] = new cen_approve_u0_detail();

        _data_approve_u0[0].emp_idx_approve1 = int.Parse(ddlEmpApprove1Cen.SelectedValue);
        _data_approve_u0[0].emp_idx_approve2 = int.Parse(ddlEmpApprove2Cen.SelectedValue);
   
        _dtEmployee.cen_approve_u0_list = _data_approve_u0;
        // page 5


        // re organization page 1/ 2
        var _data_cen_reorganization = new cen_employee_detail[1];
        _data_cen_reorganization[0] = new cen_employee_detail();

        _data_cen_reorganization[0].empgroup_idx = int.Parse(ddl_target_cen.SelectedValue); // re organization
        _data_cen_reorganization[0].cost_idx = int.Parse(ddl_costcenter_cen.SelectedValue); // re organization
        _data_cen_reorganization[0].pos_idx = int.Parse(ddlPosition.SelectedValue); // re organization
        _data_cen_reorganization[0].cemp_idx = int.Parse(ViewState["EmpIDX"].ToString()); // re organization
        
        _dtEmployee.cen_employee_detail_list = _data_cen_reorganization;
        // re organization page 1

        var _data_visa = new Typevisa_List[1];
        _data_visa[0] = new Typevisa_List();

        _data_visa[0].VisaIDX = int.Parse(ddltypevisa.SelectedValue);
        _data_visa[0].id_card_visa = txtidcard_visa.Text;
        _data_visa[0].visa_startdate = txtdatevisa_s.Text;
        _data_visa[0].visa_enddate = txtdatevisa_e.Text;

        _dtEmployee.BoxTypevisa_list = _data_visa;

        var _data_shift = new ShiftTime[1];
        _data_shift[0] = new ShiftTime();

        _data_shift[0].org_idx_shiftTime = int.Parse(ddlorg_shift.SelectedValue);
        _data_shift[0].midx = int.Parse(ddlshift_add.SelectedValue);
        _data_shift[0].emp_shift_idx = int.Parse(ViewState["EmpIDX"].ToString());

        _dtEmployee.ShiftTime_details = _data_shift;

        if (ViewState["vsTemp_Refer"] != null)
        {
            var dsrefer = (DataSet)ViewState["vsTemp_Refer"];
            var Addrefer = new Referent_List[dsrefer.Tables[0].Rows.Count];
            foreach (DataRow dr in dsrefer.Tables[0].Rows)
            {
                Addrefer[f] = new Referent_List();
                Addrefer[f].ref_fullname = dr["ref_fullname"].ToString();
                Addrefer[f].ref_position = dr["ref_position"].ToString();
                Addrefer[f].ref_relation = dr["ref_relation"].ToString();
                Addrefer[f].ref_tel = dr["ref_tel"].ToString();

                f++;
            }

            if (dsrefer.Tables[0].Rows.Count != 0) // Save To Database
            {
                _dtEmployee.BoxReferent_list = Addrefer;
            }
        }


        if (ViewState["vsTemp_Child"] != null)
        {
            var dschild = (DataSet)ViewState["vsTemp_Child"];
            var AddChild = new Child_List[dschild.Tables[0].Rows.Count];
            foreach (DataRow dr in dschild.Tables[0].Rows)
            {
                AddChild[a] = new Child_List();
                AddChild[a].Child_name = dr["Child_name"].ToString(); //ชื่อบัตร
                AddChild[a].Child_num = dr["Child_num"].ToString(); //จำนวนบุตร

                a++;
            }

            if (dschild.Tables[0].Rows.Count != 0) // Save To Database
            {
                _dtEmployee.BoxChild_list = AddChild;
            }
        }


        if (ViewState["vsTemp_Prior"] != null)
        {
            var dsPrior = (DataSet)ViewState["vsTemp_Prior"];
            var AddPrior = new Prior_List[dsPrior.Tables[0].Rows.Count];
            foreach (DataRow dr in dsPrior.Tables[0].Rows)
            {
                AddPrior[b] = new Prior_List();
                AddPrior[b].Pri_Nameorg = dr["Pri_Nameorg"].ToString();
                AddPrior[b].Pri_pos = dr["Pri_pos"].ToString();
                //AddPrior[b].Pri_worktime = dr["Pri_worktime"].ToString();
                AddPrior[b].Pri_startdate = dr["Pri_startdate"].ToString();
                AddPrior[b].Pri_resigndate = dr["Pri_resigndate"].ToString();

                b++;
            }

            if (dsPrior.Tables[0].Rows.Count != 0) // Save To Database
            {
                _dtEmployee.BoxPrior_list = AddPrior;
            }
        }


        if (ViewState["vsTemp_Education"] != null)
        {
            var dsEdu = (DataSet)ViewState["vsTemp_Education"];
            var AddEdu = new Education_List[dsEdu.Tables[0].Rows.Count];
            foreach (DataRow dr in dsEdu.Tables[0].Rows)
            {
                AddEdu[c] = new Education_List();
                AddEdu[c].Edu_qualification_ID = dr["Edu_qualification_ID"].ToString();
                AddEdu[c].Edu_qualification = dr["Edu_qualification"].ToString();
                AddEdu[c].Edu_name = dr["Edu_name"].ToString();
                AddEdu[c].Edu_branch = dr["Edu_branch"].ToString();
                AddEdu[c].Edu_start = dr["Edu_start"].ToString();
                AddEdu[c].Edu_end = dr["Edu_end"].ToString();

                c++;
            }

            if (dsEdu.Tables[0].Rows.Count != 0) // Save To Database
            {
                _dtEmployee.BoxEducation_list = AddEdu;
            }
        }


        if (ViewState["vsTemp_Train"] != null)
        {
            var dsTrain = (DataSet)ViewState["vsTemp_Train"];
            var AddTrain = new Train_List[dsTrain.Tables[0].Rows.Count];
            foreach (DataRow dr in dsTrain.Tables[0].Rows)
            {
                AddTrain[d] = new Train_List();
                AddTrain[d].Train_courses = dr["Train_courses"].ToString();
                AddTrain[d].Train_date = dr["Train_date"].ToString();
                AddTrain[d].Train_assessment = dr["Train_assessment"].ToString();
                AddTrain[d].Train_enddate = dr["Train_enddate"].ToString();

                d++;
            }

            if (dsTrain.Tables[0].Rows.Count != 0) // Save To Database
            {
                _dtEmployee.BoxTrain_list = AddTrain;
            }
        }


        if (ViewState["vsTemp_PosAdd"] != null)
        {
            var dsPos = (DataSet)ViewState["vsTemp_PosAdd"];
            var AddPos = new ODSP_List[dsPos.Tables[0].Rows.Count];
            foreach (DataRow dr in dsPos.Tables[0].Rows)
            {
                AddPos[e] = new ODSP_List();
                AddPos[e].OrgIDX_S = int.Parse(dr["OrgIDX_S"].ToString());
                AddPos[e].RDeptIDX_S = int.Parse(dr["RDeptIDX_S"].ToString());
                AddPos[e].RSecIDX_S = int.Parse(dr["RSecIDX_S"].ToString());
                AddPos[e].RPosIDX_S = int.Parse(dr["RPosIDX_S"].ToString());
                AddPos[e].emp_idx = int.Parse(ViewState["EmpIDX"].ToString());

                e++;
            }

            if (dsPos.Tables[0].Rows.Count != 0) // Save To Database
            {
                _dtEmployee.ODSPRelation_list = AddPos;
            }
        }


        if (ViewState["vsTemp_Asset"] != null)
        {
            var dsAsset = (DataSet)ViewState["vsTemp_Asset"];
            var AddAsset = new Holder_Detail[dsAsset.Tables[0].Rows.Count];
            foreach (DataRow dr in dsAsset.Tables[0].Rows)
            {
                AddAsset[e] = new Holder_Detail();
                AddAsset[e].m0_asidx = int.Parse(dr["asidx"].ToString());
                AddAsset[e].as_name = dr["as_name"].ToString();
                AddAsset[e].m0_unidx = int.Parse(dr["m0_unidx"].ToString());
                AddAsset[e].m0_name = dr["m0_name"].ToString();
                AddAsset[e].u0_number = int.Parse(dr["u0_number"].ToString());
                AddAsset[e].u0_asset_no = dr["u0_asset_no"].ToString();
                AddAsset[e].u0_holderdate = dr["u0_holderdate"].ToString();
                AddAsset[e].u0_detail = dr["u0_detail"].ToString();
                AddAsset[e].u0_emp_create = int.Parse(ViewState["EmpIDX"].ToString());
                AddAsset[e].emp_idx = int.Parse(ViewState["EmpIDX"].ToString());

                e++;
            }

            if (dsAsset.Tables[0].Rows.Count != 0) // Save To Database
            {
                _dtEmployee.Holder_List = AddAsset;
            }
        }


        if (ViewState["vsTemp_Gurantee"] != null)
        {
            var ds_gurantee = (DataSet)ViewState["vsTemp_Gurantee"];
            var Addguran = new Gurantee_list[ds_gurantee.Tables[0].Rows.Count];
            foreach (DataRow dr in ds_gurantee.Tables[0].Rows)
            {
                Addguran[f] = new Gurantee_list();
                Addguran[f].gu_fullname = dr["gu_fullname"].ToString();
                Addguran[f].gu_startdate = dr["gu_startdate"].ToString();
                Addguran[f].gu_relation = dr["gu_relation"].ToString();
                Addguran[f].gu_tel = dr["gu_tel"].ToString();

                f++;
            }

            if (ds_gurantee.Tables[0].Rows.Count != 0) // Save To Database
            {
                _dtEmployee.BoxGurantee_list = Addguran;
            }
        }

        //insert InsertPositionCenAdd
        if (ViewState["vs_dsInsertPositionCenAdd"] != null)
        {
            var _datasetInsertPositionCenAdd = (DataSet)ViewState["vs_dsInsertPositionCenAdd"];
            var _addPositionCen = new cen_position_emp_r0_detail[_datasetInsertPositionCenAdd.Tables[0].Rows.Count];
            int _PositionCen = 0;

            foreach (DataRow dtrowInsertPositionCen in _datasetInsertPositionCenAdd.Tables[0].Rows)
            {
                _addPositionCen[_PositionCen] = new cen_position_emp_r0_detail();
                _addPositionCen[_PositionCen].cemp_idx = int.Parse(ViewState["EmpIDX"].ToString());
                _addPositionCen[_PositionCen].pos_idx = int.Parse(dtrowInsertPositionCen["PositionTH_IDX"].ToString());

                _PositionCen++;

            }
            _dtEmployee.cen_position_emp_r0_list = _addPositionCen;
        }
        //insert fvPrioritiesDetail

        //fsa.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtEmployee));
        //fsa.Text = _funcTool.convertObjectToJson(_dtEmployee);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtEmployee));
        _dtEmployee = callServiceEmployee_Check(_urlurlSetInsertEmployeeList, _dtEmployee);
       
    }

    protected void Update_Employee()
    {
        var txt_startdate = (TextBox)FvEdit_Detail.FindControl("txt_startdate");
        var ddl_target = (DropDownList)FvEdit_Detail.FindControl("ddl_target_edit");
        var ddlemptype = (DropDownList)FvEdit_Detail.FindControl("ddlemptype");
        var ddlAffiliation = (DropDownList)FvEdit_Detail.FindControl("ddlAffiliation");
        var chk_Affiliation_edit = (CheckBox)FvEdit_Detail.FindControl("chk_Affiliation_edit");
        var ddl_prefix_th = (DropDownList)FvEdit_Detail.FindControl("ddl_prefix_th_edit");
        var chkprobation = (CheckBox)FvEdit_Detail.FindControl("chkprobation");
        var chkempout = (CheckBox)FvEdit_Detail.FindControl("chkempout");
        var ddlresign_comment = (DropDownList)FvEdit_Detail.FindControl("ddlresign_comment");
        var txt_comment_resign = (TextBox)FvEdit_Detail.FindControl("txt_comment_resign");
        var txt_empoutdate = (TextBox)FvEdit_Detail.FindControl("txt_empoutdate");
        var txt_probationdate = (TextBox)FvEdit_Detail.FindControl("txt_probationdate");
        var txt_name_th = (TextBox)FvEdit_Detail.FindControl("txt_name_th");
        var txt_surname_th = (TextBox)FvEdit_Detail.FindControl("txt_surname_th");
        var ddl_prefix_en = (DropDownList)FvEdit_Detail.FindControl("ddl_prefix_en_edit");
        var txt_name_en = (TextBox)FvEdit_Detail.FindControl("txt_name_en");
        var txt_surname_en = (TextBox)FvEdit_Detail.FindControl("txt_surname_en");
        var txt_nickname_th = (TextBox)FvEdit_Detail.FindControl("txt_nickname_th");
        var txt_nickname_en = (TextBox)FvEdit_Detail.FindControl("txt_nickname_en");
        var ddlcostcenter = (DropDownList)FvEdit_Detail.FindControl("ddlcostcenter");
        var txtbirth = (TextBox)FvEdit_Detail.FindControl("txtbirth");
        var ddl_sex = (DropDownList)FvEdit_Detail.FindControl("ddl_sex");
        var ddl_status = (DropDownList)FvEdit_Detail.FindControl("ddl_status");
        var ddlnation = (DropDownList)FvEdit_Detail.FindControl("ddlnation_edit");
        var ddlrace = (DropDownList)FvEdit_Detail.FindControl("ddlrace");
        var ddlreligion = (DropDownList)FvEdit_Detail.FindControl("ddlreligion");
        var ddlmilitary = (DropDownList)FvEdit_Detail.FindControl("ddlmilitary_edit");
        var txtidcard = (TextBox)FvEdit_Detail.FindControl("txtidcard");
        var txtissued_at = (TextBox)FvEdit_Detail.FindControl("txtissued_at");
        var txtexpcard = (TextBox)FvEdit_Detail.FindControl("txtexpcard");

        var txtid_passportcard_edit = (TextBox)FvEdit_Detail.FindControl("txtid_passportcard_edit");
        var txtdate_passport_edit = (TextBox)FvEdit_Detail.FindControl("txtdate_passport_edit"); //Passport EndDate
        var txtdate_Workpermit_edit = (TextBox)FvEdit_Detail.FindControl("txtdate_Workpermit_edit"); //Workpermit EndDate

        var txt_passport_issued_edit = (TextBox)FvEdit_Detail.FindControl("txt_passport_issued_edit"); //Passport Issued at
        var txtdate_passport_start = (TextBox)FvEdit_Detail.FindControl("txtdate_passport_start"); //Passport Startdate
        var txt_workpermit_id_edit = (TextBox)FvEdit_Detail.FindControl("txt_workpermit_id_edit"); //Workprtmit ID
        var txt_workpermit_issued_edit = (TextBox)FvEdit_Detail.FindControl("txt_workpermit_issued_edit"); //Workpermit Issued at
        var txt_workpermit_start_edit = (TextBox)FvEdit_Detail.FindControl("txt_workpermit_start_edit"); //Workpermit Startdate

        var ddllocation = (DropDownList)FvEdit_Detail.FindControl("ddllocation");
        var ddltypevisa = (DropDownList)FvEdit_Detail.FindControl("ddltypevisa_edit");
        var txtidcard_visa = (TextBox)FvEdit_Detail.FindControl("txtidcard_visa_edit");
        var txtdatevisa_s = (TextBox)FvEdit_Detail.FindControl("txtdatevisa_s_edit");
        var txtdatevisa_e = (TextBox)FvEdit_Detail.FindControl("txtdatevisa_e_edit");

        var ddldvcar = (DropDownList)FvEdit_Detail.FindControl("ddldvcar");
        var ddldvmt = (DropDownList)FvEdit_Detail.FindControl("ddldvmt");
        var ddldvfork = (DropDownList)FvEdit_Detail.FindControl("ddldvfork");
        var ddldvtruck = (DropDownList)FvEdit_Detail.FindControl("ddldvtruck");

        var ddldvcarstatus = (DropDownList)FvEdit_Detail.FindControl("ddldvcarstatus");
        var ddldvmtstatus = (DropDownList)FvEdit_Detail.FindControl("ddldvmtstatus");

        var txtlicens_car = (TextBox)FvEdit_Detail.FindControl("txtlicen_car");
        var txtlicens_moto = (TextBox)FvEdit_Detail.FindControl("txtlicens_moto");
        var txtlicens_fork = (TextBox)FvEdit_Detail.FindControl("txtlicens_fork");
        var txtlicens_truck = (TextBox)FvEdit_Detail.FindControl("txtlicens_truck");

        var dllbank = (DropDownList)FvEdit_Detail.FindControl("dllbank");
        var txtaccountname = (TextBox)FvEdit_Detail.FindControl("txtaccountname");
        var txtaccountnumber = (TextBox)FvEdit_Detail.FindControl("txtaccountnumber");
        var txtemercon = (TextBox)FvEdit_Detail.FindControl("txtemercon");
        var txtrelation = (TextBox)FvEdit_Detail.FindControl("txtrelation");
        var txttelemer = (TextBox)FvEdit_Detail.FindControl("txttelemer");

        var txtfathername = (TextBox)FvEdit_Detail.FindControl("txtfathername_edit");
        var txttelfather = (TextBox)FvEdit_Detail.FindControl("txttelfather_edit");
        var txtlicenfather = (TextBox)FvEdit_Detail.FindControl("txtlicenfather_edit");
        var txtmothername = (TextBox)FvEdit_Detail.FindControl("txtmothername_edit");
        var txttelmother = (TextBox)FvEdit_Detail.FindControl("txttelmother_edit");
        var txtlicenmother = (TextBox)FvEdit_Detail.FindControl("txtlicenmother_edit");
        var txtwifename = (TextBox)FvEdit_Detail.FindControl("txtwifename_edit");
        var txttelwife = (TextBox)FvEdit_Detail.FindControl("txttelwife_edit");
        var txtlicenwifename = (TextBox)FvEdit_Detail.FindControl("txtlicenwife_edit");

        var txtchildname = (TextBox)FvEdit_Detail.FindControl("txtchildname");
        var ddlchildnumber = (DropDownList)FvEdit_Detail.FindControl("ddlchildnumber");
        var GvChildAdd = (GridView)FvEdit_Detail.FindControl("GvChildAdd");

        var txtorgold = (TextBox)FvEdit_Detail.FindControl("txtorgold_edit");
        var txtposold = (TextBox)FvEdit_Detail.FindControl("txtposold_edit");
        //var txtworktimeold = (TextBox)FvEdit_Detail.FindControl("txtworktimeold");
        var txtstartdate_ex = (TextBox)FvEdit_Detail.FindControl("txtstartdate_ex");
        var txtresigndate_ex = (TextBox)FvEdit_Detail.FindControl("txtresigndate_ex");
        var GvPri = (GridView)FvEdit_Detail.FindControl("GvPri");

        var ddleducationback = (DropDownList)FvEdit_Detail.FindControl("ddleducationback");
        var txtschoolname = (TextBox)FvEdit_Detail.FindControl("txtschoolname");
        var txtstarteducation = (TextBox)FvEdit_Detail.FindControl("txtstarteducation");
        var txtendeducation = (TextBox)FvEdit_Detail.FindControl("txtendeducation");
        var txtstudy = (TextBox)FvEdit_Detail.FindControl("txtstudy");
        var GvEducation = (GridView)FvEdit_Detail.FindControl("GvEducation");

        var txttraincourses = (TextBox)FvEdit_Detail.FindControl("txttraincourses");
        var txttraindate = (TextBox)FvEdit_Detail.FindControl("txttraindate");
        var txttrainassessment = (TextBox)FvEdit_Detail.FindControl("txttrainassessment");
        var GvTrain = (GridView)FvEdit_Detail.FindControl("GvTrain");

        //------------------------------- Page 2

        var ddlorg = (DropDownList)FvEdit_Detail.FindControl("ddlorg_add_edit");
        var ddldep = (DropDownList)FvEdit_Detail.FindControl("ddldep_add_edit");
        var ddlsec = (DropDownList)FvEdit_Detail.FindControl("ddlsec_add_edit");
        var ddlpos = (DropDownList)FvEdit_Detail.FindControl("ddlpos_add_edit");
        var ddlshift_ = (DropDownList)FvEdit_Detail.FindControl("ddlshift_edit");

        //------------------------------- Page 3

        var txtaddress_present = (TextBox)FvEdit_Detail.FindControl("txtaddress_present");
        var ddlcountry_present = (DropDownList)FvEdit_Detail.FindControl("ddlcountry_present_edit");
        var ddlprovince_present = (DropDownList)FvEdit_Detail.FindControl("ddlprovince_present_edit");
        var ddlamphoe_present = (DropDownList)FvEdit_Detail.FindControl("ddlamphoe_present_edit");
        var ddldistrict_present = (DropDownList)FvEdit_Detail.FindControl("ddldistrict_present_edit");
        var txttelmobile_present = (TextBox)FvEdit_Detail.FindControl("txttelmobile_present");
        var txttelhome_present = (TextBox)FvEdit_Detail.FindControl("txttelhome_present");
        var txtzipcode_present = (TextBox)FvEdit_Detail.FindControl("txtzipcode_present");
        var txtemail_present = (TextBox)FvEdit_Detail.FindControl("txtemail_present");
        var txtpersonal_email = (TextBox)FvEdit_Detail.FindControl("txtpersonal_email");

        var txtaddress_permanentaddress = (TextBox)FvEdit_Detail.FindControl("txtaddress_permanentaddress");
        var ddlcountry_permanentaddress = (DropDownList)FvEdit_Detail.FindControl("ddlcountry_permanentaddress_edit");
        var ddlprovince_permanentaddress = (DropDownList)FvEdit_Detail.FindControl("ddlprovince_permanentaddress_edit");
        var ddlamphoe_permanentaddress = (DropDownList)FvEdit_Detail.FindControl("ddlamphoe_permanentaddress_edit");
        var ddldistrict_permanentaddress = (DropDownList)FvEdit_Detail.FindControl("ddldistrict_permanentaddress_edit");
        var txttelmobile_permanentaddress = (TextBox)FvEdit_Detail.FindControl("txttelmobile_permanentaddress");
        var txttelhome_permanentaddress = (TextBox)FvEdit_Detail.FindControl("txttelhome_permanentaddress");

        //------------------------------- Page 4

        var ddlhospital = (DropDownList)FvEdit_Detail.FindControl("ddlhospital");
        var txtexaminationdate = (TextBox)FvEdit_Detail.FindControl("txtexaminationdate");
        var txtsecurityid = (TextBox)FvEdit_Detail.FindControl("txtsecurityid");
        var txtexpsecurity = (TextBox)FvEdit_Detail.FindControl("txtexpsecurity");
        var txtheight = (TextBox)FvEdit_Detail.FindControl("txtheight");
        var txtweight = (TextBox)FvEdit_Detail.FindControl("txtweight");
        var ddlblood = (DropDownList)FvEdit_Detail.FindControl("ddlblood");
        var txtscar = (TextBox)FvEdit_Detail.FindControl("txtscar");
        var ddlmedicalcertificate = (DropDownList)FvEdit_Detail.FindControl("ddlmedicalcertificate");
        var ddlresultlab = (DropDownList)FvEdit_Detail.FindControl("ddlresultlab");
        //var txthospital_edit = (TextBox)FvEdit_Detail.FindControl("txthospital_edit");
        var lbl_hos_edit = (Label)FvEdit_Detail.FindControl("lbl_hos_edit");

        //------------------------------- Page 5

        var ddlapprove1 = (DropDownList)FvEdit_Detail.FindControl("ddlapprove1");
        var ddlapprove2 = (DropDownList)FvEdit_Detail.FindControl("ddlapprove2");

        int a = 0;
        int b = 0;
        int c = 0;
        int d = 0;
        int e = 0;
        int f = 0;

        /*try
        {
            _dataEmployee.hospital_list = new Hospital[1];
            Hospital dtemployee = new Hospital();
            dtemployee.Name = txthospital_edit.Text;
            _dataEmployee.hospital_list[0] = dtemployee;

            _dataEmployee = callServiceEmployee(_urlGetHospitalOld, _dataEmployee);

            if (_dataEmployee.return_code.ToString() == "1")
            {
                ddlhospital.SelectedValue = lbl_hos_edit.Text;
            }
            else
            {
                ddlhospital.SelectedValue = _dataEmployee.hospital_list[0].HosIDX.ToString();
            }
            
        }
        catch (Exception ex)
        {

        }*/


        var _dataEmployee_ = new employee_detail[1];
        _dataEmployee_[0] = new employee_detail();

        _dataEmployee_[0].emp_idx = int.Parse(ViewState["Gv_emp_idx"].ToString());
        _dataEmployee_[0].EmpINID = int.Parse(ViewState["EmpIDX"].ToString());
        if (txt_startdate.Text != "") { _dataEmployee_[0].emp_start_date = txt_startdate.Text; } else { _dataEmployee_[0].emp_start_date = "01/01/1900"; }
        _dataEmployee_[0].TIDX = int.Parse(ddl_target.SelectedValue);
        _dataEmployee_[0].emp_type_idx = int.Parse(ddlemptype.SelectedValue);
        if (chk_Affiliation_edit.Checked)
        {
            _dataEmployee_[0].ACIDX = int.Parse(ddlAffiliation.SelectedValue);
        }
        else
        {
            _dataEmployee_[0].ACIDX = 0;
        }
        if (chkprobation.Checked) { _dataEmployee_[0].emp_probation_status = 1; } else { _dataEmployee_[0].emp_probation_status = 0; }
        if (chkempout.Checked) { _dataEmployee_[0].emp_status = 0; _dataEmployee_[0].perm_pol = int.Parse(ddlresign_comment.SelectedValue); } else { _dataEmployee_[0].emp_status = 1; _dataEmployee_[0].perm_pol = 0; }
        if (txt_empoutdate.Text != "") { _dataEmployee_[0].emp_resign_date = txt_empoutdate.Text; } else { _dataEmployee_[0].emp_resign_date = "01/01/1900"; }
        if (txt_probationdate.Text != "") { _dataEmployee_[0].emp_probation_date = txt_probationdate.Text; } else { _dataEmployee_[0].emp_probation_date = "01/01/1900"; }
        if (chkempout.Checked)
        {
            if (int.Parse(ddlresign_comment.SelectedValue) == 6)
            {
                _dataEmployee_[0].perm_pol_name = txt_comment_resign.Text;
            }
            else
            {
                _dataEmployee_[0].perm_pol_name = "";
            }
        }
        else
        {
            _dataEmployee_[0].perm_pol_name = "";
        }
        _dataEmployee_[0].prefix_idx = int.Parse(ddl_prefix_th.SelectedValue);

        if (txt_name_th.Text != "") { _dataEmployee_[0].emp_firstname_th = txt_name_th.Text; } else { _dataEmployee_[0].emp_firstname_th = "-"; }
        if (txt_surname_th.Text != "") { _dataEmployee_[0].emp_lastname_th = txt_surname_th.Text; } else { _dataEmployee_[0].emp_lastname_th = "-"; }

        _dataEmployee_[0].emp_firstname_en = txt_name_en.Text;
        _dataEmployee_[0].emp_lastname_en = txt_surname_en.Text;
        _dataEmployee_[0].emp_nickname_th = txt_nickname_th.Text;
        _dataEmployee_[0].emp_nickname_en = txt_nickname_en.Text;
        _dataEmployee_[0].costcenter_idx = int.Parse(ddlcostcenter.SelectedValue);
        if (txtbirth.Text != "") { _dataEmployee_[0].emp_birthday = txtbirth.Text; } else { _dataEmployee_[0].emp_birthday = "01/01/1900"; }

        _dataEmployee_[0].LocIDX = int.Parse(ddllocation.SelectedValue); //No Base

        _dataEmployee_[0].sex_idx = int.Parse(ddl_sex.SelectedValue);
        _dataEmployee_[0].married_status = int.Parse(ddl_status.SelectedValue); // สถานะแต่งงาน
        _dataEmployee_[0].nat_idx = int.Parse(ddlnation.SelectedValue);
        _dataEmployee_[0].race_idx = int.Parse(ddlrace.SelectedValue);
        _dataEmployee_[0].rel_idx = int.Parse(ddlreligion.SelectedValue);
        _dataEmployee_[0].mil_idx = int.Parse(ddlmilitary.SelectedValue); // สถานะทางทหาร
        _dataEmployee_[0].identity_card = txtidcard.Text; // รหัสบัตรประชน
        _dataEmployee_[0].issued_at = txtissued_at.Text; // สถานที่ออกบัตร
        if (txtexpcard.Text != "") { _dataEmployee_[0].idate_expired = txtexpcard.Text; } else { _dataEmployee_[0].idate_expired = "01/01/1900"; } // วันที่หมดอายุ
        _dataEmployee_[0].passportID = txtid_passportcard_edit.Text; // เลขที่ passport
        _dataEmployee_[0].passport_enddate = txtdate_passport_edit.Text; // วันหมดอายุ passport
        _dataEmployee_[0].workpermit_enddate = txtdate_Workpermit_edit.Text; // วันหมดอายุ workpermis

        _dataEmployee_[0].passport_issuat = txt_passport_issued_edit.Text; // Passport Issued at
        _dataEmployee_[0].passport_start = txtdate_passport_start.Text; // Passport Startdate
        _dataEmployee_[0].workpermitID = txt_workpermit_id_edit.Text; // Workprtmit ID
        _dataEmployee_[0].workpermit_issuat = txt_workpermit_issued_edit.Text; // Workpermit Issued at
        _dataEmployee_[0].workpermit_start = txt_workpermit_start_edit.Text; // Workpermit Startdate

        _dataEmployee_[0].dvcar_idx = int.Parse(ddldvcar.SelectedValue); // สถานะขับขี่รถยนต์ - New Table
        _dataEmployee_[0].dvmt_idx = int.Parse(ddldvmt.SelectedValue); // สถานะขับขี่รถมอเตอร์ไซ - New Table
        _dataEmployee_[0].dvfork_idx = int.Parse(ddldvfork.SelectedValue); //  No Base
        _dataEmployee_[0].dvtruck_idx = int.Parse(ddldvtruck.SelectedValue); //  No Base
        _dataEmployee_[0].dvcarstatus_idx = int.Parse(ddldvcarstatus.SelectedValue); //  มีรถยนต์ 
        _dataEmployee_[0].dvmtstatus_idx = int.Parse(ddldvmtstatus.SelectedValue); //  มีรถมอเตอร์ไซต์ 
        _dataEmployee_[0].dvlicen_car = txtlicens_car.Text; //  No Base
        _dataEmployee_[0].dvlicen_moto = txtlicens_moto.Text; //  No Base
        _dataEmployee_[0].dvlicen_fork = txtlicens_fork.Text; //  No Base
        _dataEmployee_[0].dvlicen_truck = txtlicens_truck.Text; //  No Base

        _dataEmployee_[0].bank_idx = int.Parse(dllbank.SelectedValue); // ธนาคาร - New Table
        _dataEmployee_[0].bank_name = txtaccountname.Text; // ชื่อบัญชี
        _dataEmployee_[0].bank_no = txtaccountnumber.Text; // เลขที่บัญชี

        _dataEmployee_[0].emer_name = txtemercon.Text; // กรณีฉุกเฉินติดต่อ
        _dataEmployee_[0].emer_relation = txtrelation.Text; // ความสัมพันธ์
        _dataEmployee_[0].emer_tel = txttelemer.Text; // เบอร์โทร

        _dataEmployee_[0].fathername = txtfathername.Text; // ชื่อบิดา
        _dataEmployee_[0].telfather = txttelfather.Text; // เบอร์ติดต่อ
        _dataEmployee_[0].licen_father = txtlicenfather.Text; //  No Base
        _dataEmployee_[0].mothername = txtmothername.Text; // ชื่อมารดา
        _dataEmployee_[0].telmother = txttelmother.Text; // เบอร์ติดต่อ
        _dataEmployee_[0].licen_mother = txtlicenmother.Text; //  No Base
        _dataEmployee_[0].wifename = txtwifename.Text; // ชื่อภรรยา
        _dataEmployee_[0].telwife = txttelwife.Text; // เบอร์ติดต่อ
        _dataEmployee_[0].licen_wife = txtlicenwifename.Text; //  No Base

        _dataEmployee_[0].org_idx = int.Parse(ddlorg.SelectedValue);
        _dataEmployee_[0].rdept_idx = int.Parse(ddldep.SelectedValue);
        _dataEmployee_[0].rsec_idx = int.Parse(ddlsec.SelectedValue);
        _dataEmployee_[0].rpos_idx = int.Parse(ddlpos.SelectedValue);

        _dataEmployee_[0].emp_address = txtaddress_present.Text; // ******************* ที่อยู่ปัจจุบัน
        _dataEmployee_[0].country_idx = int.Parse(ddlcountry_present.SelectedValue);
        _dataEmployee_[0].prov_idx = int.Parse(ddlprovince_present.SelectedValue);
        _dataEmployee_[0].amp_idx = int.Parse(ddlamphoe_present.SelectedValue);
        _dataEmployee_[0].dist_idx = int.Parse(ddldistrict_present.SelectedValue);
        _dataEmployee_[0].emp_mobile_no = txttelmobile_present.Text;  // ******
        _dataEmployee_[0].emp_phone_no = txttelhome_present.Text;   // ******
        _dataEmployee_[0].post_code = txtzipcode_present.Text;
        _dataEmployee_[0].emp_email = txtemail_present.Text;
        _dataEmployee_[0].personal_email = txtpersonal_email.Text;

        _dataEmployee_[0].emp_address_permanent = txtaddress_permanentaddress.Text;  // ******************* ที่อยู่ตามทะเบียนบ้าน
        _dataEmployee_[0].country_idx_permanent = int.Parse(ddlcountry_permanentaddress.SelectedValue);
        _dataEmployee_[0].prov_idx_permanent = int.Parse(ddlprovince_permanentaddress.SelectedValue);
        _dataEmployee_[0].amp_idx_permanent = int.Parse(ddlamphoe_permanentaddress.SelectedValue);
        _dataEmployee_[0].dist_idx_permanent = int.Parse(ddldistrict_permanentaddress.SelectedValue);
        _dataEmployee_[0].PhoneNumber_permanent = txttelmobile_permanentaddress.Text;  // ******

        _dataEmployee_[0].soc_hos_idx = int.Parse(ddlhospital.SelectedValue);  // โรงพยาบาล
        if (txtexaminationdate.Text != "") { _dataEmployee_[0].examinationdate = txtexaminationdate.Text; } else { _dataEmployee_[0].examinationdate = "01/01/1900"; } // วันที่ตรวจสุขภาพ
        _dataEmployee_[0].soc_no = txtsecurityid.Text;  // รหัสบัตรประกันสังคม       
        if (txtexpsecurity.Text != "") { _dataEmployee_[0].soc_expired_date = txtexpsecurity.Text; } else { _dataEmployee_[0].soc_expired_date = "01/01/1900"; } // วันหมดอายุประกันสังคม

        //-- edit re organization
        // if (txtheight.Text == "") { _dataEmployee_[0].height_s = 0; } else { _dataEmployee_[0].height_s = int.Parse(txtheight.Text); } // ส่วนสูง
        // if (txtweight.Text == "") { _dataEmployee_[0].weight_s = 0; } else { _dataEmployee_[0].weight_s = int.Parse(txtweight.Text); } // น้ำหนัก  

        // _dataEmployee_[0].btype_idx = int.Parse(ddlblood.SelectedValue);  // กรุ๊ปเลือด
        // _dataEmployee_[0].scar_s = txtscar.Text;  // แผลเป้น
        // _dataEmployee_[0].medicalcertificate_id = int.Parse(ddlmedicalcertificate.SelectedValue); //ใบรับรอง
        // _dataEmployee_[0].resultlab_id = int.Parse(ddlresultlab.SelectedValue); // ผลตรวจ LAB

        _dataEmployee_[0].emp_idx_approve1 = int.Parse(ddlapprove1.SelectedValue);
        _dataEmployee_[0].emp_idx_approve2 = int.Parse(ddlapprove2.SelectedValue);
        _dataEmployee_[0].PIDX = int.Parse(ddlshift_.SelectedValue);

        _dtEmployee.employee_list = _dataEmployee_;

        var _data_visa_ = new Typevisa_List[1];
        _data_visa_[0] = new Typevisa_List();

        _data_visa_[0].VisaIDX = int.Parse(ddltypevisa.SelectedValue);
        _data_visa_[0].id_card_visa = txtidcard_visa.Text;
        _data_visa_[0].visa_startdate = txtdatevisa_s.Text;
        _data_visa_[0].visa_enddate = txtdatevisa_e.Text;

        _dtEmployee.BoxTypevisa_list = _data_visa_;

        if (ViewState["vsTemp_Refer_edit"] != null)
        {
            var dsrefer = (DataSet)ViewState["vsTemp_Refer_edit"];
            var Addrefer = new Referent_List[dsrefer.Tables[0].Rows.Count];
            foreach (DataRow dr in dsrefer.Tables[0].Rows)
            {
                Addrefer[f] = new Referent_List();
                Addrefer[f].ref_fullname = dr["ref_fullname"].ToString();
                Addrefer[f].ref_position = dr["ref_position"].ToString();
                Addrefer[f].ref_relation = dr["ref_relation"].ToString();
                Addrefer[f].ref_tel = dr["ref_tel"].ToString();

                f++;
            }

            if (dsrefer.Tables[0].Rows.Count != 0) // Save To Database
            {
                _dtEmployee.BoxReferent_list = Addrefer;
            }
        }


        if (ViewState["vsTemp_Child_edit"] != null)
        {
            var dschild = (DataSet)ViewState["vsTemp_Child_edit"];
            var AddChild = new Child_List[dschild.Tables[0].Rows.Count];
            foreach (DataRow dr in dschild.Tables[0].Rows)
            {
                AddChild[a] = new Child_List();
                AddChild[a].Child_name = dr["Child_name"].ToString(); //ชื่อบัตร
                AddChild[a].Child_num = dr["Child_num"].ToString(); //จำนวนบุตร

                a++;
            }

            if (dschild.Tables[0].Rows.Count != 0) // Save To Database
            {
                _dtEmployee.BoxChild_list = AddChild;
            }
        }


        if (ViewState["vsTemp_Prior_edit"] != null)
        {
            var dsPrior = (DataSet)ViewState["vsTemp_Prior_edit"];
            var AddPrior = new Prior_List[dsPrior.Tables[0].Rows.Count];
            foreach (DataRow dr in dsPrior.Tables[0].Rows)
            {
                AddPrior[b] = new Prior_List();
                AddPrior[b].Pri_Nameorg = dr["Pri_Nameorg"].ToString();
                AddPrior[b].Pri_pos = dr["Pri_pos"].ToString();
                //AddPrior[b].Pri_worktime = dr["Pri_worktime"].ToString();
                AddPrior[b].Pri_startdate = dr["Pri_startdate"].ToString();
                AddPrior[b].Pri_resigndate = dr["Pri_resigndate"].ToString();

                b++;
            }

            if (dsPrior.Tables[0].Rows.Count != 0) // Save To Database
            {
                _dtEmployee.BoxPrior_list = AddPrior;
            }
        }


        if (ViewState["vsTemp_Education_edit"] != null)
        {
            var dsEdu = (DataSet)ViewState["vsTemp_Education_edit"];
            var AddEdu = new Education_List[dsEdu.Tables[0].Rows.Count];
            foreach (DataRow dr in dsEdu.Tables[0].Rows)
            {
                AddEdu[c] = new Education_List();
                AddEdu[c].Edu_qualification_ID = dr["Edu_qualification_ID"].ToString();
                AddEdu[c].Edu_qualification = dr["Edu_qualification"].ToString();
                AddEdu[c].Edu_name = dr["Edu_name"].ToString();
                AddEdu[c].Edu_branch = dr["Edu_branch"].ToString();
                AddEdu[c].Edu_start = dr["Edu_start"].ToString();
                AddEdu[c].Edu_end = dr["Edu_end"].ToString();

                c++;
            }

            if (dsEdu.Tables[0].Rows.Count != 0) // Save To Database
            {
                _dtEmployee.BoxEducation_list = AddEdu;
            }
        }


        if (ViewState["vsTemp_Train_edit"] != null)
        {
            var dsTrain = (DataSet)ViewState["vsTemp_Train_edit"];
            var AddTrain = new Train_List[dsTrain.Tables[0].Rows.Count];
            foreach (DataRow dr in dsTrain.Tables[0].Rows)
            {
                AddTrain[d] = new Train_List();
                AddTrain[d].Train_courses = dr["Train_courses"].ToString();
                AddTrain[d].Train_date = dr["Train_date"].ToString();
                AddTrain[d].Train_assessment = dr["Train_assessment"].ToString();
                AddTrain[d].Train_enddate = dr["Train_enddate"].ToString();

                d++;
            }

            if (dsTrain.Tables[0].Rows.Count != 0) // Save To Database
            {
                _dtEmployee.BoxTrain_list = AddTrain;
            }
        }


        if (ViewState["vsTemp_PosAdd_edit"] != null)
        {
            var dsPos = (DataSet)ViewState["vsTemp_PosAdd_edit"];
            var AddPos = new ODSP_List[dsPos.Tables[0].Rows.Count];
            foreach (DataRow dr in dsPos.Tables[0].Rows)
            {
                AddPos[e] = new ODSP_List();
                //AddPos[e].OrgIDX_S = int.Parse(dr["OrgIDX_S"].ToString());
                //AddPos[e].RDeptIDX_S = int.Parse(dr["RDeptIDX_S"].ToString());
                //AddPos[e].RSecIDX_S = int.Parse(dr["RSecIDX_S"].ToString());
                AddPos[e].RPosIDX_S = int.Parse(dr["RPosIDX_S"].ToString());
                AddPos[e].emp_idx = int.Parse(ViewState["EmpIDX"].ToString());

                e++;
            }

            if (dsPos.Tables[0].Rows.Count != 0) // Save To Database
            {
                _dtEmployee.ODSPRelation_list = AddPos;
            }
        }


        if (ViewState["vsTemp_Empshift_edit"] != null)
        {
            var dsPos1 = (DataSet)ViewState["vsTemp_Empshift_edit"];
            var AddPos1 = new ShiftTime[dsPos1.Tables[0].Rows.Count];
            foreach (DataRow dr in dsPos1.Tables[0].Rows)
            {
                AddPos1[e] = new ShiftTime();
                AddPos1[e].org_idx_shiftTime = int.Parse(dr["OrgIDX_add"].ToString());
                //AddPos[e].RDeptIDX_S = int.Parse(dr["RDeptIDX_S"].ToString());
                //AddPos[e].RSecIDX_S = int.Parse(dr["RSecIDX_S"].ToString());
                AddPos1[e].midx = int.Parse(dr["midx_add"].ToString());
                AddPos1[e].emp_shift_idx = int.Parse(ViewState["EmpIDX"].ToString());

                e++;
            }

            if (dsPos1.Tables[0].Rows.Count != 0) // Save To Database
            {
                _dtEmployee.ShiftTime_details = AddPos1;
            }
        }


        if (ViewState["vsTemp_Asset_Edit"] != null)
        {
            var dsAsset = (DataSet)ViewState["vsTemp_Asset_Edit"];
            var AddAsset = new Holder_Detail[dsAsset.Tables[0].Rows.Count];
            foreach (DataRow dr in dsAsset.Tables[0].Rows)
            {
                AddAsset[e] = new Holder_Detail();
                AddAsset[e].m0_asidx = int.Parse(dr["asidx"].ToString());
                AddAsset[e].as_name = dr["as_name"].ToString();
                AddAsset[e].m0_unidx = int.Parse(dr["m0_unidx"].ToString());
                AddAsset[e].m0_name = dr["m0_name"].ToString();
                AddAsset[e].u0_number = int.Parse(dr["u0_number"].ToString());
                AddAsset[e].u0_asset_no = dr["u0_asset_no"].ToString();
                AddAsset[e].u0_holderdate = dr["u0_holderdate"].ToString();
                AddAsset[e].u0_detail = dr["u0_detail"].ToString();
                AddAsset[e].u0_emp_create = int.Parse(ViewState["EmpIDX"].ToString());
                AddAsset[e].emp_idx = int.Parse(ViewState["EmpIDX"].ToString());

                e++;
            }

            if (dsAsset.Tables[0].Rows.Count != 0) // Save To Database
            {
                _dtEmployee.Holder_List = AddAsset;
            }
        }


        if (ViewState["vsTemp_Gurantee_edit"] != null)
        {
            var ds_gurantee = (DataSet)ViewState["vsTemp_Gurantee_edit"];
            var Addguran = new Gurantee_list[ds_gurantee.Tables[0].Rows.Count];
            foreach (DataRow dr in ds_gurantee.Tables[0].Rows)
            {
                Addguran[f] = new Gurantee_list();
                Addguran[f].gu_fullname = dr["gu_fullname"].ToString();
                Addguran[f].gu_startdate = dr["gu_startdate"].ToString();
                Addguran[f].gu_relation = dr["gu_relation"].ToString();
                Addguran[f].gu_tel = dr["gu_tel"].ToString();

                f++;
            }

            if (ds_gurantee.Tables[0].Rows.Count != 0) // Save To Database
            {
                _dtEmployee.BoxGurantee_list = Addguran;
            }
        }

        try
        {
            //Label2.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtEmployee));
            _dtEmployee = callServiceEmployee_Check(_urlSetUpdateDetail_employeeList, _dtEmployee);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + "บันทึกข้อมูลเสร็จเรียบร้อย" + "')", true);
            //Page.Response.Redirect(Page.Request.Url.ToString(), true);

        }
        catch (FileNotFoundException ex)
        {
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
        }
    }

    protected void Update_Employee_Setapprove()
    {
        var FvDetailUser_temp_ = (FormView)ViewSetApprove.FindControl("FvDetailUser_temp");
        var FvDetailUser = (FormView)ViewSetApprove.FindControl("FvDetailUser");
        var GvRsecApproveTemp = (GridView)ViewSetApprove.FindControl("GvRsecApproveTemp");
        var lbl_emp_idx_temp = (Label)FvDetailUser_temp_.FindControl("lbl_emp_idx_temp");
        var lbl_emp_idx = (Label)FvDetailUser.FindControl("lbl_emp_idx");
        //var emp_idx_approve1 = (Label)FvDetailUser_temp_.FindControl("emp_idx_approve1");
        //var emp_idx_approve2 = (Label)FvDetailUser_temp_.FindControl("emp_idx_approve2");

        string Rsec_Temp = "";
        string Temp = "";
        foreach (GridViewRow row in GvRsecApproveTemp.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                Temp = (row.Cells[0].FindControl("lbl_rsec_idx") as Label).Text;
                Rsec_Temp += Temp + ",";
            }
        }

        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _dataEmployee_ = new employee_detail();

        _dataEmployee_.Rsec_ID = Rsec_Temp.ToString();
        _dataEmployee_.emp_idx = int.Parse(lbl_emp_idx_temp.Text);
        _dataEmployee_.emp_idx_approve1 = int.Parse(lbl_emp_idx.Text);
        _dataEmployee_.emp_idx_approve2 = int.Parse(lbl_emp_idx.Text);
        //_dataEmployee_.type_approve = int.Parse(ddl_approve.SelectedValue); // ประเภท อนุมัติ - aprrove 1 / aprrove 2 / approve 1 & 2 

        _dataEmployee.employee_list[0] = _dataEmployee_;
        _dataEmployee = callServiceEmployee(_urlSetupdateapprove_employeeList, _dataEmployee);
    }

    #endregion

    #endregion

    #region FvDetail_DataBound

    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        var FvName = (FormView)sender;

        switch (FvName.ID)
        {
            case "FvInsertEmp":
                FormView FvInsertEmp = (FormView)ViewAdd.FindControl("FvInsertEmp");
                DropDownList ddlcostcenter = (DropDownList)FvInsertEmp.FindControl("ddlcostcenter");
                
                
                DropDownList ddlorg = (DropDownList)FvInsertEmp.FindControl("ddlorg");
                DropDownList ddlorg_add = (DropDownList)FvInsertEmp.FindControl("ddlorg_add");
                
                DropDownList ddlhospital = (DropDownList)FvInsertEmp.FindControl("ddlhospital");

                DropDownList ddlcountry_present = (DropDownList)FvInsertEmp.FindControl("ddlcountry_present");
                DropDownList ddlprovince_present = (DropDownList)FvInsertEmp.FindControl("ddlprovince_present");
                DropDownList ddlamphoe_present = (DropDownList)FvInsertEmp.FindControl("ddlamphoe_present");
                DropDownList ddldistrict_present = (DropDownList)FvInsertEmp.FindControl("ddldistrict_present");

                DropDownList ddlcountry_permanentaddress = (DropDownList)FvInsertEmp.FindControl("ddlcountry_permanentaddress");
                DropDownList ddlprovince_permanentaddress = (DropDownList)FvInsertEmp.FindControl("ddlprovince_permanentaddress");

                DropDownList ddllocation = (DropDownList)FvInsertEmp.FindControl("ddllocation");
                DropDownList ddlnation = (DropDownList)FvInsertEmp.FindControl("ddlnation");

                DropDownList ddltypevisa = (DropDownList)FvInsertEmp.FindControl("ddltypevisa");
                
                DropDownList ddl_target = (DropDownList)FvInsertEmp.FindControl("ddl_target");
                DropDownList ddl_target_cen = (DropDownList)FvInsertEmp.FindControl("ddl_target_cen");
                DropDownList ddl_costcenter_cen = (DropDownList)FvInsertEmp.FindControl("ddl_costcenter_cen");

                // get organization
                DropDownList ddlorg_shift = (DropDownList)FvInsertEmp.FindControl("ddlorg_shift");
                UpdatePanel _PanelRePositionCen = (UpdatePanel)FvInsertEmp.FindControl("_PanelRePositionCen");
                DropDownList ddlOrganization = (DropDownList)_PanelRePositionCen.FindControl("ddlOrganization");
                DropDownList ddlWorkGroup = (DropDownList)_PanelRePositionCen.FindControl("ddlWorkGroup");
                DropDownList ddlLineWork = (DropDownList)_PanelRePositionCen.FindControl("ddlLineWork");
                DropDownList ddlDepartment = (DropDownList)_PanelRePositionCen.FindControl("ddlDepartment");
                DropDownList ddlSection = (DropDownList)_PanelRePositionCen.FindControl("ddlSection");
                DropDownList ddlPosition = (DropDownList)_PanelRePositionCen.FindControl("ddlPosition");

                UpdatePanel _PanelRePositionCenAction = (UpdatePanel)FvInsertEmp.FindControl("_PanelRePositionCenAction");
                DropDownList ddlOrganizationAction = (DropDownList)_PanelRePositionCenAction.FindControl("ddlOrganizationAction");
                DropDownList ddlWorkGroupAction = (DropDownList)_PanelRePositionCenAction.FindControl("ddlWorkGroupAction");
                DropDownList ddlLineWorkAction = (DropDownList)_PanelRePositionCenAction.FindControl("ddlLineWorkAction");
                DropDownList ddlDepartmentAction = (DropDownList)_PanelRePositionCenAction.FindControl("ddlDepartmentAction");
                DropDownList ddlSectionAction = (DropDownList)_PanelRePositionCenAction.FindControl("ddlSectionAction");
                DropDownList ddlPositionAction = (DropDownList)_PanelRePositionCenAction.FindControl("ddlPositionAction");



                if (FvInsertEmp.CurrentMode == FormViewMode.Insert)
                {
                    Select_Coscenter(ddlcostcenter);
                    Select_hospital(ddlhospital);
                    select_org(ddlorg);
                    select_org(ddlorg_add);
                    select_country(ddlcountry_present);
                    ddlcountry_present.SelectedValue = "218";
                    select_province(ddlprovince_present);
                    select_country(ddlcountry_permanentaddress);
                    select_province(ddlprovince_permanentaddress);
                    Select_Location(ddllocation);
                    select_nationality(ddlnation);
                    ddlnation.SelectedValue = "177";
                    Select_TypeVisa(ddltypevisa);

                    select_org(ddlorg_shift);
                    select_positiongroup(ddl_target);

                    GetEmployeeGroup(ddl_target_cen);
                    GetCostcenter(ddl_costcenter_cen);


                    _data_cen_master = getMasterList("3", null);
                    ddlOrganization.Items.Clear();
                    ddlWorkGroup.Items.Clear();
                    ddlLineWork.Items.Clear();
                    ddlDepartment.Items.Clear();
                    ddlSection.Items.Clear();
                    ddlPosition.Items.Clear();
                   

                    _funcTool.setDdlData(ddlOrganization, _data_cen_master.cen_org_list_m0, "org_name_th", "org_idx");
                    ddlOrganization.Items.Insert(0, new ListItem("--- เลือกองค์กร ---", "0"));
                    ddlWorkGroup.Items.Insert(0, new ListItem("--- เลือกกลุ่มงาน ---", "0"));
                    ddlLineWork.Items.Insert(0, new ListItem("--- เลือกสายงาน ---", "0"));
                    ddlDepartment.Items.Insert(0, new ListItem("--- เลือกฝ่าย ---", "0"));
                    ddlSection.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "0"));
                    ddlPosition.Items.Insert(0, new ListItem("--- เลือกตำแหน่ง ---", "0"));

                    //Position Action
                    
                    ddlOrganizationAction.Items.Clear();
                    ddlWorkGroupAction.Items.Clear();
                    ddlLineWorkAction.Items.Clear();
                    ddlDepartmentAction.Items.Clear();
                    ddlSectionAction.Items.Clear();
                    ddlPositionAction.Items.Clear();
                   

                    _funcTool.setDdlData(ddlOrganizationAction, _data_cen_master.cen_org_list_m0, "org_name_th", "org_idx");
                    ddlOrganizationAction.Items.Insert(0, new ListItem("--- เลือกองค์กร ---", "0"));
                    ddlWorkGroupAction.Items.Insert(0, new ListItem("--- เลือกกลุ่มงาน ---", "0"));
                    ddlLineWorkAction.Items.Insert(0, new ListItem("--- เลือกสายงาน ---", "0"));
                    ddlDepartmentAction.Items.Insert(0, new ListItem("--- เลือกฝ่าย ---", "0"));
                    ddlSectionAction.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "0"));
                    ddlPositionAction.Items.Insert(0, new ListItem("--- เลือกตำแหน่ง ---", "0"));


                    
                }

                break;

            case "Fv_Search_Emp_Index":
                FormView Fv_Search_Emp_Index = (FormView)ViewIndex.FindControl("Fv_Search_Emp_Index");
                DropDownList ddlorg_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddlorg_s");
                DropDownList ddldep_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddldep_s");
                DropDownList ddlsec_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddlsec_s");
                DropDownList ddlpos_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddlpos_s");
                LinkButton btnsearch_index = (LinkButton)Fv_Search_Emp_Index.FindControl("btnsearch_index");

                if (Fv_Search_Emp_Index.CurrentMode == FormViewMode.Insert)
                {
                    select_org(ddlorg_s);
                    linkBtnTrigger(btnsearch_index);
                }
                break;

            case "FvEdit_Detail":

                Label lbl_emp_idx = (Label)FvEdit_Detail.FindControl("lbl_emp_idx");
                ViewState["fv_emp_idx"] = lbl_emp_idx.Text;
                
                
                
                DropDownList ddlcostcenter_edit = (DropDownList)FvEdit_Detail.FindControl("ddlcostcenter");
                Label lbl_cost_idx = (Label)FvEdit_Detail.FindControl("lbl_cost_idx");
                TextBox txttax_mounth = (TextBox)FvEdit_Detail.FindControl("txttax_mounth");
                TextBox txt_startdate = (TextBox)FvEdit_Detail.FindControl("txt_startdate");

                DropDownList ddlmilitary_edit = (DropDownList)FvEdit_Detail.FindControl("ddlmilitary_edit");
                Label lbl_mil_idx = (Label)FvEdit_Detail.FindControl("lbl_mil_idx");

                Label lbl_probation_status = (Label)FvEdit_Detail.FindControl("lbl_probation_status");
                Label lbl_emp_status = (Label)FvEdit_Detail.FindControl("lbl_emp_status");
                CheckBox chkprobation = (CheckBox)FvEdit_Detail.FindControl("chkprobation");
                CheckBox chkempout = (CheckBox)FvEdit_Detail.FindControl("chkempout");

                DropDownList ddlresign_comment_edit = (DropDownList)FvEdit_Detail.FindControl("ddlresign_comment");
                Label lbl_RTIDX_edit = (Label)FvEdit_Detail.FindControl("lbl_RTIDX");
                Panel Boxresign_date = (Panel)FvEdit_Detail.FindControl("Boxresign_date");
                Panel box_txt_comment = (Panel)FvEdit_Detail.FindControl("box_txt_comment");

                DropDownList ddlAffiliation = (DropDownList)FvEdit_Detail.FindControl("ddlAffiliation");
                Label lblAffiliation = (Label)FvEdit_Detail.FindControl("lblAffiliation");
                CheckBox chk_Affiliation_edit = (CheckBox)FvEdit_Detail.FindControl("chk_Affiliation_edit");
                Panel box_ddlAffiliation = (Panel)FvEdit_Detail.FindControl("box_ddlAffiliation");

                Label lbl_org_edit = (Label)FvEdit_Detail.FindControl("lbl_org_edit");
                Label lbl_rdep_edit = (Label)FvEdit_Detail.FindControl("lbl_rdep_edit");
                Label lbl_rsec_edit = (Label)FvEdit_Detail.FindControl("lbl_rsec_edit");
                Label lbl_rpos_edit = (Label)FvEdit_Detail.FindControl("lbl_rpos_edit");
                Label lblVisaIDX_edit = (Label)FvEdit_Detail.FindControl("lblVisaIDX_edit");
                Label lblshift_edit = (Label)FvEdit_Detail.FindControl("lblshift_edit");

                DropDownList ddltypevisa_edit = (DropDownList)FvEdit_Detail.FindControl("ddltypevisa_edit");
                DropDownList ddlshift_edit = (DropDownList)FvEdit_Detail.FindControl("ddlshift_edit");

                //ddl ข้อมูล Dropdown fix
                DropDownList ddlemptype = (DropDownList)FvEdit_Detail.FindControl("ddlemptype");
                DropDownList ddl_target_edit = (DropDownList)FvEdit_Detail.FindControl("ddl_target_edit");
                DropDownList ddl_prefix_th = (DropDownList)FvEdit_Detail.FindControl("ddl_prefix_th_edit");
                DropDownList ddl_prefix_en = (DropDownList)FvEdit_Detail.FindControl("ddl_prefix_en_edit");
                DropDownList ddl_sex = (DropDownList)FvEdit_Detail.FindControl("ddl_sex");
                DropDownList ddl_status = (DropDownList)FvEdit_Detail.FindControl("ddl_status");
                DropDownList ddlrace = (DropDownList)FvEdit_Detail.FindControl("ddlrace");
                DropDownList ddlreligion = (DropDownList)FvEdit_Detail.FindControl("ddlreligion");
                DropDownList ddlmilitary = (DropDownList)FvEdit_Detail.FindControl("ddlmilitary");
                DropDownList ddldvcar = (DropDownList)FvEdit_Detail.FindControl("ddldvcar");
                DropDownList ddldvmt = (DropDownList)FvEdit_Detail.FindControl("ddldvmt");
                DropDownList ddldvfork = (DropDownList)FvEdit_Detail.FindControl("ddldvfork");
                DropDownList ddldvtruck = (DropDownList)FvEdit_Detail.FindControl("ddldvtruck");
                DropDownList ddldvcarstatus = (DropDownList)FvEdit_Detail.FindControl("ddldvcarstatus");
                DropDownList ddldvmtstatus = (DropDownList)FvEdit_Detail.FindControl("ddldvmtstatus");

                DropDownList dllbank = (DropDownList)FvEdit_Detail.FindControl("dllbank");
                DropDownList ddlblood = (DropDownList)FvEdit_Detail.FindControl("ddlblood");
                DropDownList ddlmedicalcertificate = (DropDownList)FvEdit_Detail.FindControl("ddlmedicalcertificate");
                DropDownList ddlresultlab = (DropDownList)FvEdit_Detail.FindControl("ddlresultlab");

                Label lbl_emptype = (Label)FvEdit_Detail.FindControl("lbl_emptype");
                Label lbl_target = (Label)FvEdit_Detail.FindControl("lbl_target");
                Label lbl_prefix_idx = (Label)FvEdit_Detail.FindControl("lbl_prefix_idx");
                Label lbl_sex_idx = (Label)FvEdit_Detail.FindControl("lbl_sex_idx");
                Label lbl_married_status_idx = (Label)FvEdit_Detail.FindControl("lbl_married_status_idx");
                Label lbl_race_idx = (Label)FvEdit_Detail.FindControl("lbl_race_idx");
                Label lbl_rel_idx = (Label)FvEdit_Detail.FindControl("lbl_rel_idx");
                Label lbl_dvcar_idx = (Label)FvEdit_Detail.FindControl("lbl_dvcar_idx");
                Label lbl_dvmt_idx = (Label)FvEdit_Detail.FindControl("lbl_dvmt_idx");
                Label lbl_dvcarstatus_idx = (Label)FvEdit_Detail.FindControl("lbl_dvcarstatus_idx");
                Label lbl_dvmtstatus_idx = (Label)FvEdit_Detail.FindControl("lbl_dvmtstatus_idx");
                Label lbl_dvfork_idx = (Label)FvEdit_Detail.FindControl("lbl_dvfork_idx");
                Label lbl_dvtruck_idx = (Label)FvEdit_Detail.FindControl("lbl_dvtruck_idx");
                Label lbl_bank_idx = (Label)FvEdit_Detail.FindControl("lbl_bank_idx");
                Label lbl_BRHIDX = (Label)FvEdit_Detail.FindControl("lbl_BRHIDX");
                Label lbl_medical_id = (Label)FvEdit_Detail.FindControl("lbl_medical_id");
                Label lbl_resultlab_id = (Label)FvEdit_Detail.FindControl("lbl_resultlab_id");

                //ที่อยู่ปัจจุบัน
                DropDownList ddlcountry_present_edit = (DropDownList)FvEdit_Detail.FindControl("ddlcountry_present_edit");
                DropDownList ddlprovince_present_edit = (DropDownList)FvEdit_Detail.FindControl("ddlprovince_present_edit");
                DropDownList ddlamphoe_present_edit = (DropDownList)FvEdit_Detail.FindControl("ddlamphoe_present_edit");
                DropDownList ddldistrict_present_edit = (DropDownList)FvEdit_Detail.FindControl("ddldistrict_present_edit");
                Label lbl_country_edit = (Label)FvEdit_Detail.FindControl("lbl_country_edit");
                Label lbl_prov_edit = (Label)FvEdit_Detail.FindControl("lbl_prov_edit");
                Label lbl_amp_edit = (Label)FvEdit_Detail.FindControl("lbl_amp_edit");
                Label lbl_dist_edit = (Label)FvEdit_Detail.FindControl("lbl_dist_edit");

                //ที่อยู่ตามทะเบียนบ้าน
                DropDownList ddlcountry_permanentaddress_edit = (DropDownList)FvEdit_Detail.FindControl("ddlcountry_permanentaddress_edit");
                DropDownList ddlprovince_permanentaddress_edit = (DropDownList)FvEdit_Detail.FindControl("ddlprovince_permanentaddress_edit");
                DropDownList ddlamphoe_permanentaddress_edit = (DropDownList)FvEdit_Detail.FindControl("ddlamphoe_permanentaddress_edit");
                DropDownList ddldistrict_permanentaddress_edit = (DropDownList)FvEdit_Detail.FindControl("ddldistrict_permanentaddress_edit");
                Label lbl_ddlcountry_permanent_edit = (Label)FvEdit_Detail.FindControl("lbl_ddlcountry_permanent_edit");
                Label lbl_ddlprovince_permanent_edit = (Label)FvEdit_Detail.FindControl("lbl_ddlprovince_permanent_edit");
                Label lbl_ddlamphoe_permanent_edit = (Label)FvEdit_Detail.FindControl("lbl_ddlamphoe_permanent_edit");
                Label lbl_ddldistrict_permanent_edit = (Label)FvEdit_Detail.FindControl("lbl_ddldistrict_permanent_edit");

                DropDownList ddlapprove1 = (DropDownList)FvEdit_Detail.FindControl("ddlapprove1");
                DropDownList ddlapprove2 = (DropDownList)FvEdit_Detail.FindControl("ddlapprove2");
                Label lbl_approve_1_edit = (Label)FvEdit_Detail.FindControl("lbl_approve_1_edit");
                Label lbl_approve_2_edit = (Label)FvEdit_Detail.FindControl("lbl_approve_2_edit");

                DropDownList ddlorg_add_edit = (DropDownList)FvEdit_Detail.FindControl("ddlorg_add_edit");

                DropDownList ddlhospital_edit = (DropDownList)FvEdit_Detail.FindControl("ddlhospital");
                Label lbl_hos_edit = (Label)FvEdit_Detail.FindControl("lbl_hos_edit");
                DropDownList ddllocation_edit = (DropDownList)FvEdit_Detail.FindControl("ddllocation");
                Label lbl_location_idx = (Label)FvEdit_Detail.FindControl("lbl_location_idx");
                DropDownList ddlnation_edit = (DropDownList)FvEdit_Detail.FindControl("ddlnation_edit");
                Label lbl_nation_idx = (Label)FvEdit_Detail.FindControl("lbl_nation_idx");
                //TextBox txthospital_edit = (TextBox)FvEdit_Detail.FindControl("txthospital_edit");
                DropDownList ddlorg_add_group = (DropDownList)FvEdit_Detail.FindControl("ddlorg_add_group");

                DropDownList ddlassettype_add = (DropDownList)FvEdit_Detail.FindControl("ddlassettype_add");
                DropDownList ddlunittype_add = (DropDownList)FvEdit_Detail.FindControl("ddlunittype_add");
                GridView GvAsset = (GridView)FvEdit_Detail.FindControl("GvAsset_View");
                TextBox txtidcard = (TextBox)FvEdit_Detail.FindControl("txtidcard");
                Image image = (Image)FvEdit_Detail.FindControl("image");

                // page 1 //
                DropDownList ddl_target_cen_edit = (DropDownList)FvEdit_Detail.FindControl("ddl_target_cen");
                DropDownList ddl_costcenter_cen_edit = (DropDownList)FvEdit_Detail.FindControl("ddl_costcenter_cen");

                // page 1 //

                // page 2 //
                DropDownList ddlOrganizationAction_Edit = (DropDownList)FvEdit_Detail.FindControl("ddlOrganizationAction_Edit");
                DropDownList ddlWorkGroupAction_Edit = (DropDownList)FvEdit_Detail.FindControl("ddlWorkGroupAction_Edit");
                DropDownList ddlLineWorkAction_Edit = (DropDownList)FvEdit_Detail.FindControl("ddlLineWorkAction_Edit");
                DropDownList ddlDepartmentAction_Edit = (DropDownList)FvEdit_Detail.FindControl("ddlDepartmentAction_Edit");
                DropDownList ddlSectionAction_Edit = (DropDownList)FvEdit_Detail.FindControl("ddlSectionAction_Edit");
                DropDownList ddlPositionAction_Edit = (DropDownList)FvEdit_Detail.FindControl("ddlPositionAction_Edit");
                Label lbl_cost_idx_edit = (Label)FvEdit_Detail.FindControl("lbl_cost_idx_edit");
                Label lbl_empgroup_idx_edit = (Label)FvEdit_Detail.FindControl("lbl_empgroup_idx_edit");

                // page 5 //

                DropDownList ddlEmpApprove1CenEdit = (DropDownList)FvEdit_Detail.FindControl("ddlEmpApprove1CenEdit");
                DropDownList ddlEmpApprove2CenEdit = (DropDownList)FvEdit_Detail.FindControl("ddlEmpApprove2CenEdit");
                Label lbl_ddlEmpApprove1CenEdit = (Label)FvEdit_Detail.FindControl("lbl_ddlEmpApprove1CenEdit");
                Label lbl_ddlEmpApprove2CenEdit = (Label)FvEdit_Detail.FindControl("lbl_ddlEmpApprove2CenEdit");






                int year = 0;
                var temp_startdate = txt_startdate.Text.Replace("/", "").Substring(4, 4);
                var temp_year = DateTime.Now.Year.ToString();
                var temp_startmount = txt_startdate.Text.Replace("/", "").Substring(2, 2);
                if (txt_startdate.Text != null)
                {
                    // (Year วันเริ่มงาน) != (Year ปัจจุบัน)
                    //if (int.Parse(DateTime.Parse(txt_startdate.Text).Year.ToString()) != int.Parse(DateTime.Now.Year.ToString()))  //กรณีปีไม่เท่ากัน
                    if (int.Parse(temp_startdate.ToString()) != int.Parse(temp_year.ToString()))  //กรณีปีไม่เท่ากัน
                    {
                        //เดือนปัจจุบัน
                        year = 12; //int.Parse(DateTime.Now.Month.ToString());
                    }
                    else //กรณีปีเท่ากัน
                    {
                        //เดือนปัจจุบัน - เดือนที่เริ่มงาน
                        //year = 13 - int.Parse(formatDateTimeCheck(temp_startdate.ToString()).ToString());
                        year = 13 - int.Parse(temp_startmount.ToString());
                    }
                }
                else
                {
                    year = 0;
                }

                if (FvEdit_Detail.CurrentMode == FormViewMode.Edit)
                {

                    // page 1 //
                    GetEmployeeGroup(ddl_target_cen_edit);
                    ddl_target_cen_edit.SelectedValue = lbl_empgroup_idx_edit.Text;
                    
                    GetCostcenter(ddl_costcenter_cen_edit);
                    ddl_costcenter_cen_edit.SelectedValue = lbl_cost_idx_edit.Text;

                    // page 2 //
                    _data_cen_master = getMasterList("3", null);
                    ddlOrganizationAction_Edit.Items.Clear();
                    ddlWorkGroupAction_Edit.Items.Clear();
                    ddlLineWorkAction_Edit.Items.Clear();
                    ddlDepartmentAction_Edit.Items.Clear();
                    ddlSectionAction_Edit.Items.Clear();
                    ddlPositionAction_Edit.Items.Clear();
                   

                    _funcTool.setDdlData(ddlOrganizationAction_Edit, _data_cen_master.cen_org_list_m0, "org_name_th", "org_idx");
                    ddlOrganizationAction_Edit.Items.Insert(0, new ListItem("--- เลือกองค์กร ---", "0"));
                    ddlWorkGroupAction_Edit.Items.Insert(0, new ListItem("--- เลือกกลุ่มงาน ---", "0"));
                    ddlLineWorkAction_Edit.Items.Insert(0, new ListItem("--- เลือกสายงาน ---", "0"));
                    ddlDepartmentAction_Edit.Items.Insert(0, new ListItem("--- เลือกฝ่าย ---", "0"));
                    ddlSectionAction_Edit.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "0"));
                    ddlPositionAction_Edit.Items.Insert(0, new ListItem("--- เลือกตำแหน่ง ---", "0"));


                    GetPositionListCen(int.Parse(lbl_emp_idx.Text.ToString()));
                    // page 2 //

                    // page 5 //
                    // get employee
                    search_cen_employee_detail _allEmployee = new search_cen_employee_detail ();
                    _data_cen_employee = getEmployeeList ("99", _allEmployee);
                  
                    ViewState["EmpAllList"] = _data_cen_employee;
                    _data_cen_employee = (data_cen_employee) ViewState["EmpAllList"];
                     
                    _funcTool.setDdlData (ddlEmpApprove1CenEdit, _data_cen_employee.cen_employee_list_u0, "emp_name_th", "emp_idx");
                    ddlEmpApprove1CenEdit.Items.Insert (0, new ListItem ("เลือกผู้อนุมัติ 1...", "0"));
                    ddlEmpApprove1CenEdit.SelectedValue = lbl_ddlEmpApprove1CenEdit.Text;

                    _funcTool.setDdlData (ddlEmpApprove2CenEdit, _data_cen_employee.cen_employee_list_u0, "emp_name_th", "emp_idx");
                    ddlEmpApprove2CenEdit.Items.Insert (0, new ListItem ("เลือกผู้อนุมัติ 2...", "0"));
                    ddlEmpApprove2CenEdit.SelectedValue = lbl_ddlEmpApprove2CenEdit.Text;
                   


                    ddlemptype.SelectedValue = lbl_emptype.Text;
                    txttax_mounth.Text = year.ToString() + " เดือน";
                    ddl_prefix_th.SelectedValue = lbl_prefix_idx.Text;
                    ddl_prefix_en.SelectedValue = lbl_prefix_idx.Text;
                    ddl_sex.SelectedValue = lbl_sex_idx.Text;
                    ddl_status.SelectedValue = lbl_married_status_idx.Text;
                    ddlrace.SelectedValue = lbl_race_idx.Text;
                    ddlreligion.SelectedValue = lbl_rel_idx.Text;
                    ddldvcar.SelectedValue = lbl_dvcar_idx.Text;
                    ddldvmt.SelectedValue = lbl_dvmt_idx.Text;
                    ddldvcarstatus.SelectedValue = lbl_dvcarstatus_idx.Text;
                    ddldvmtstatus.SelectedValue = lbl_dvmtstatus_idx.Text;
                    ddldvfork.SelectedValue = lbl_dvfork_idx.Text;
                    ddldvtruck.SelectedValue = lbl_dvtruck_idx.Text;
                    dllbank.SelectedValue = lbl_bank_idx.Text;
                    ddlblood.SelectedValue = lbl_BRHIDX.Text;
                    ddlmedicalcertificate.SelectedValue = lbl_medical_id.Text;
                    ddlresultlab.SelectedValue = lbl_resultlab_id.Text;
                    ddllocation_edit.SelectedValue = lbl_location_idx.Text;
                    ddlmilitary_edit.SelectedValue = lbl_mil_idx.Text;
                    ddlshift_edit.SelectedValue = lblshift_edit.Text;


                    if (lbl_probation_status.Text == "1")
                    {
                        chkprobation.Checked = true;
                    }
                    else
                    {
                        chkprobation.Checked = false;
                    }

                    if (lbl_emp_status.Text == "0")
                    {
                        chkempout.Checked = true;
                        Boxresign_date.Visible = true;
                        Select_ddl_resgin_comment(ddlresign_comment_edit);
                        ddlresign_comment_edit.SelectedValue = lbl_RTIDX_edit.Text;
                        if (lbl_RTIDX_edit.Text == "6")
                        {
                            box_txt_comment.Visible = true;
                        }
                        else
                        {
                            box_txt_comment.Visible = false;
                        }
                    }
                    else
                    {
                        chkempout.Checked = false;
                    }

                    if (lblAffiliation.Text == "0")
                    {
                        chk_Affiliation_edit.Checked = false;
                        box_ddlAffiliation.Visible = false;
                    }
                    else
                    {
                        chk_Affiliation_edit.Checked = true;
                        box_ddlAffiliation.Visible = true;
                    }

                    
                    Select_Coscenter(ddlcostcenter_edit);
                    ddlcostcenter_edit.SelectedValue = lbl_cost_idx.Text;
                    Select_hospital(ddlhospital_edit);
                    ddlhospital_edit.SelectedValue = lbl_hos_edit.Text;


                    /*_dataEmployee.hospital_list = new Hospital[1];
                    Hospital dtemployee = new Hospital();
                    dtemployee.HosIDX = int.Parse(lbl_hos_edit.Text);
                    _dataEmployee.hospital_list[0] = dtemployee;

                    _dataEmployee = callServiceEmployee(_urlGetHospitalOld, _dataEmployee);
                    txthospital_edit.Text = _dataEmployee.hospital_list[0].Name;*/

                    select_approve_add(ddlapprove1, int.Parse(lbl_org_edit.Text), int.Parse(lbl_rdep_edit.Text), int.Parse(lbl_rsec_edit.Text), int.Parse(lbl_rpos_edit.Text));
                    select_approve_add(ddlapprove2, int.Parse(lbl_org_edit.Text), int.Parse(lbl_rdep_edit.Text), int.Parse(lbl_rsec_edit.Text), int.Parse(lbl_rpos_edit.Text));
                    ddlapprove1.SelectedValue = lbl_approve_1_edit.Text;
                    ddlapprove2.SelectedValue = lbl_approve_2_edit.Text;

                    select_country(ddlcountry_present_edit);
                    ddlcountry_present_edit.SelectedValue = lbl_country_edit.Text;
                    select_province(ddlprovince_present_edit);
                    ddlprovince_present_edit.SelectedValue = lbl_prov_edit.Text;
                    select_amphur(ddlamphoe_present_edit, int.Parse(ddlprovince_present_edit.SelectedValue));
                    ddlamphoe_present_edit.SelectedValue = lbl_amp_edit.Text;
                    select_dist(ddldistrict_present_edit, int.Parse(ddlprovince_present_edit.SelectedValue), int.Parse(ddlamphoe_present_edit.SelectedValue));
                    ddldistrict_present_edit.SelectedValue = lbl_dist_edit.Text;

                    select_country(ddlcountry_permanentaddress_edit);
                    ddlcountry_permanentaddress_edit.SelectedValue = lbl_ddlcountry_permanent_edit.Text;
                    select_province(ddlprovince_permanentaddress_edit);
                    ddlprovince_permanentaddress_edit.SelectedValue = lbl_ddlprovince_permanent_edit.Text;
                    select_amphur(ddlamphoe_permanentaddress_edit, int.Parse(ddlprovince_permanentaddress_edit.SelectedValue));
                    ddlamphoe_permanentaddress_edit.SelectedValue = lbl_ddlamphoe_permanent_edit.Text;
                    select_dist(ddldistrict_permanentaddress_edit, int.Parse(ddlprovince_permanentaddress_edit.SelectedValue), int.Parse(ddlamphoe_permanentaddress_edit.SelectedValue));
                    ddldistrict_permanentaddress_edit.SelectedValue = lbl_ddldistrict_permanent_edit.Text;

                    select_positiongroup(ddl_target_edit);
                    ddl_target_edit.SelectedValue = lbl_target.Text;

                    select_org(ddlorg_add_edit);
                    Select_ODSP(int.Parse(lbl_emp_idx.Text.ToString()));
                    Select_Refer_view(int.Parse(ViewState["fv_emp_idx"].ToString()));
                    Select_Child_view(int.Parse(ViewState["fv_emp_idx"].ToString()));
                    Select_Prior_view(int.Parse(ViewState["fv_emp_idx"].ToString()));
                    Select_Education_view(int.Parse(ViewState["fv_emp_idx"].ToString()));
                    Select_Train_view(int.Parse(ViewState["fv_emp_idx"].ToString()));
                    Select_Gurantee_view(int.Parse(ViewState["fv_emp_idx"].ToString()));
                    Select_Location(ddllocation_edit);
                    select_nationality(ddlnation_edit);
                    ddlnation_edit.SelectedValue = lbl_nation_idx.Text;
                    Select_TypeVisa(ddltypevisa_edit);
                    ddltypevisa_edit.SelectedValue = lblVisaIDX_edit.Text;
                    select_org(ddlorg_add_group);

                    ddlAffiliation.SelectedValue = lblAffiliation.Text;
                    Select_Gv_EmpShift(int.Parse(ViewState["fv_emp_idx"].ToString()));

                    Select_asset_type(ddlassettype_add);
                    Select_unit_type(ddlunittype_add);
                    Select_Gv_Asset(GvAsset, int.Parse(ViewState["fv_emp_idx"].ToString()));

                    ViewState["EmpIDX_Pic"] = lbl_emp_idx.Text;
                    GetPathPhoto(lbl_emp_idx.Text, image);
                }
                break;

            case "Fv_Search_Emp_Report":
                FormView Fv_Search_Emp_Report = (FormView)ViewIndex.FindControl("Fv_Search_Emp_Report");
                DropDownList ddlorg_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlorg_rp");
                DropDownList ddldep_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddldep_rp");
                DropDownList ddlsec_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlsec_rp");
                DropDownList ddlpos_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlpos_rp");
                DropDownList ddllocation_search = (DropDownList)Fv_Search_Emp_Report.FindControl("ddllocation_search");
                CheckBoxList YrChkBoxColumns = (CheckBoxList)Fv_Search_Emp_Report.FindControl("YrChkBoxColumns");
                DropDownList ddl_target_report = (DropDownList)Fv_Search_Emp_Report.FindControl("ddl_target_report");

                ArrayList deviceStatus = new ArrayList() { "รหัสพนักงาน", "วันเข้างาน", "ชื่อ-สกุล(TH)", "ชื่อ-สกุล(EN)", "ฝ่าย", "แผนก", "ตำแหน่ง", "Costcenter", "สัญชาติ", "เชื้อชาติ", "ศาสนา", "วันเกิด", "อายุงาน", "Passport", "วันที่หมดอายุ Passport", "สังกัด", "job grade", "สถานะ", "วันที่ออก Visa", "วันที่หมดอายุ Visa", "หมายเลข Workpermit", "วันที่ออก Work Permit", "วันที่หมดอายุ Work Permit", "หมายเลขบัตรประชาชน", "วันหมดอายุบัตรประชาชน", "Plant (Location)", "กลุ่มพนักงาน", "ผู้อนุมัติที่ 1", "ผู้อนุมัติที่ 2", "วันลาออก", "วันผ่านทดลองงาน", "ธนาคาร", "เลขที่บัญชี", "กรณีฉุกเฉินติดต่อ", "เบอร์ติดต่อฉุกเฉิน", "ประเภทกะทำงาน", "Email บริษัท", "Email ส่วนตัว", "เบอร์มือถือ", "โรงพยาบาลประกันสังคม", "เลขที่ประกันสังคม", "กรุ๊ปเลือด", "ประเภทลาออก", "เหตุผลลาออก", "ที่อยู่", "วุฒิการศึกษา", "ประวัติฝึกอบรม" }; 


                if (Fv_Search_Emp_Report.CurrentMode == FormViewMode.Insert)
                {
                    select_org(ddlorg_rp);
                    Select_Location(ddllocation_search);

                    YrChkBoxColumns.Items.Clear();
                    YrChkBoxColumns.AppendDataBoundItems = true;

                    YrChkBoxColumns.DataSource = deviceStatus;
                    YrChkBoxColumns.DataBind();

                    select_positiongroup(ddl_target_report);

                    //Select_(); aaa
                }
                break;

            case "FvTemplate_print":

                if (FvTemplate_print.CurrentMode == FormViewMode.ReadOnly)
                {

                    //Select_Report_Probation_Print(FvTemplate_print, int.Parse(ViewState["EmpIDX_Print"].ToString()));

                    //_dataEmployee.BoxEmployee_ProbationList = new Employee_Probation[1];
                    //Employee_Probation _dataEmployee_ = new Employee_Probation();

                    //_dataEmployee_.EmpIDX = 174;// int.Parse(ViewState["EmpIDX_Print"].ToString());
                    //_dataEmployee_.Condition = 2;

                    //_dataEmployee.BoxEmployee_ProbationList[0] = _dataEmployee_;
                    ////fss.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
                    //_dataEmployee = callServiceEmployee(_urlSelect_Report_Probation, _dataEmployee);
                    //setFormDataMode(FvTemplate_print, FormViewMode.ReadOnly, _dataEmployee.BoxEmployee_ProbationList);

                    //Label lblfullnameth = (Label)FvTemplate_print.FindControl("lblfullnameth");
                    //lblfullnameth.Text = _dataEmployee.BoxEmployee_ProbationList[0].FullNameTH.ToString();


                }
                break;
        }
    }

    #endregion

    #region Switch-Munu Color
    protected void Menu_Color(int choice)
    {

        GridView GvEmployee = (GridView)ViewIndex.FindControl("GvEmployee");
        switch (choice)
        {
            case 1: //Index
                lbindex.BackColor = System.Drawing.Color.LightGray;
                lbadd.BackColor = System.Drawing.Color.Transparent;
                lbsetapprove.BackColor = System.Drawing.Color.Transparent;
                lblreport.BackColor = System.Drawing.Color.Transparent;
                lbprobation.BackColor = System.Drawing.Color.Transparent;
                GvEmployee.Visible = true;
                Fv_Search_Emp_Index.ChangeMode(FormViewMode.Insert);
                Fv_Search_Emp_Index.DataBind();
                break;
            case 2: //Add
                lbindex.BackColor = System.Drawing.Color.Transparent;
                lbadd.BackColor = System.Drawing.Color.LightGray;
                lbsetapprove.BackColor = System.Drawing.Color.Transparent;
                lblreport.BackColor = System.Drawing.Color.Transparent;
                lbprobation.BackColor = System.Drawing.Color.Transparent;
                break;
            case 3: //Approve
                lbindex.BackColor = System.Drawing.Color.Transparent;
                lbadd.BackColor = System.Drawing.Color.Transparent;
                lbsetapprove.BackColor = System.Drawing.Color.LightGray;
                lblreport.BackColor = System.Drawing.Color.Transparent;
                lbprobation.BackColor = System.Drawing.Color.Transparent;
                break;
            case 4: //Report
                lbindex.BackColor = System.Drawing.Color.Transparent;
                lbadd.BackColor = System.Drawing.Color.Transparent;
                lbsetapprove.BackColor = System.Drawing.Color.Transparent;
                lblreport.BackColor = System.Drawing.Color.LightGray;
                lbprobation.BackColor = System.Drawing.Color.Transparent;
                Fv_Search_Emp_Report.ChangeMode(FormViewMode.Insert);
                Fv_Search_Emp_Report.DataBind();

                btnexport.Visible = false;
                btnexport_general.Visible = false;
                btnexport_beplus.Visible = false;
                btnexport_Visa.Visible = false;
                btnexport_chart_education.Visible = false;
                btnexport_chart_sex.Visible = false;
                btnexport_chart_age.Visible = false;
                btnexport_chart_car.Visible = false;
                btnexport_chart_emp_by_dept.Visible = false;
                btnexport_chart_leave_by_dept.Visible = false;
                btnexport_chart_nationality.Visible = false;
                btnexport_chart_new_emp.Visible = false;

                //aaa
                break;
            case 5: // Probation
                lbindex.BackColor = System.Drawing.Color.Transparent;
                lbadd.BackColor = System.Drawing.Color.Transparent;
                lbsetapprove.BackColor = System.Drawing.Color.Transparent;
                lblreport.BackColor = System.Drawing.Color.Transparent;
                lbprobation.BackColor = System.Drawing.Color.LightGray;

                break;
        }
    }

    protected void Menu_Color_Add(int choice)
    {
        Panel BoxGen = (Panel)FvInsertEmp.FindControl("BoxGen");
        Panel BoxPos = (Panel)FvInsertEmp.FindControl("BoxPos");
        Panel BoxAddress = (Panel)FvInsertEmp.FindControl("BoxAddress");
        Panel BoxHeal = (Panel)FvInsertEmp.FindControl("BoxHeal");
        Panel BoxApprove = (Panel)FvInsertEmp.FindControl("BoxApprove");
        Panel BoxAsset = (Panel)FvInsertEmp.FindControl("BoxAsset");
        LinkButton bt_scroll_down_add = (LinkButton)FvInsertEmp.FindControl("bt_scroll_down_add");
        LinkButton bt_scroll_up_add = (LinkButton)FvInsertEmp.FindControl("bt_scroll_up_add");

        UpdatePanel _PanelReApprove = (UpdatePanel)FvInsertEmp.FindControl("_PanelReApprove");
        DropDownList ddlEmpApprove1Cen = (DropDownList)_PanelReApprove.FindControl("ddlEmpApprove1Cen");
        DropDownList ddlEmpApprove2Cen = (DropDownList)_PanelReApprove.FindControl("ddlEmpApprove2Cen");


        //set tap
        _PanelReApprove.Visible = false;


        switch (choice)
        {
            case 1: //Gen
                btngen.ImageUrl = "~/masterpage/images/hr/color_1.png";
                btnpos.ImageUrl = "~/masterpage/images/hr/gray_2.png";
                btnaddress.ImageUrl = "~/masterpage/images/hr/gray_3.png";
                btnheal.ImageUrl = "~/masterpage/images/hr/gray_4.png";
                btmapprove.ImageUrl = "~/masterpage/images/hr/gray_5.png";
                btnadd_asset.ImageUrl = "~/masterpage/images/hr/gray_6.png";

                BoxGen.Visible = true;
                BoxPos.Visible = false;
                BoxAddress.Visible = false;
                BoxHeal.Visible = false;
                BoxApprove.Visible = false;
                BoxAsset.Visible = false;

                bt_scroll_down_add.Visible = false;
                bt_scroll_up_add.Visible = false;

                break;
            case 2: //Pos
                btngen.ImageUrl = "~/masterpage/images/hr/gray_1.png";
                btnpos.ImageUrl = "~/masterpage/images/hr/color_2.png";
                btnaddress.ImageUrl = "~/masterpage/images/hr/gray_3.png";
                btnheal.ImageUrl = "~/masterpage/images/hr/gray_4.png";
                btmapprove.ImageUrl = "~/masterpage/images/hr/gray_5.png";
                btnadd_asset.ImageUrl = "~/masterpage/images/hr/gray_6.png";

                BoxGen.Visible = false;
                BoxPos.Visible = true;
                BoxAddress.Visible = false;
                BoxHeal.Visible = false;
                BoxApprove.Visible = false;
                BoxAsset.Visible = false;

                bt_scroll_down_add.Visible = false;
                bt_scroll_up_add.Visible = false;

                break;
            case 3: //Address
                btngen.ImageUrl = "~/masterpage/images/hr/gray_1.png";
                btnpos.ImageUrl = "~/masterpage/images/hr/gray_2.png";
                btnaddress.ImageUrl = "~/masterpage/images/hr/color_3.png";
                btnheal.ImageUrl = "~/masterpage/images/hr/gray_4.png";
                btmapprove.ImageUrl = "~/masterpage/images/hr/gray_5.png";
                btnadd_asset.ImageUrl = "~/masterpage/images/hr/gray_6.png";

                BoxGen.Visible = false;
                BoxPos.Visible = false;
                BoxAddress.Visible = true;
                BoxHeal.Visible = false;
                BoxApprove.Visible = false;
                BoxAsset.Visible = false;

                bt_scroll_down_add.Visible = false;
                bt_scroll_up_add.Visible = false;

                break;
            case 4: //Heal
                btngen.ImageUrl = "~/masterpage/images/hr/gray_1.png";
                btnpos.ImageUrl = "~/masterpage/images/hr/gray_2.png";
                btnaddress.ImageUrl = "~/masterpage/images/hr/gray_3.png";
                btnheal.ImageUrl = "~/masterpage/images/hr/color_4.png";
                btmapprove.ImageUrl = "~/masterpage/images/hr/gray_5.png";
                btnadd_asset.ImageUrl = "~/masterpage/images/hr/gray_6.png";

                BoxGen.Visible = false;
                BoxPos.Visible = false;
                BoxAddress.Visible = false;
                BoxHeal.Visible = true;
                BoxApprove.Visible = false;
                BoxAsset.Visible = false;

                bt_scroll_down_add.Visible = false;
                bt_scroll_up_add.Visible = false;

                break;
            case 5: //Approve
                btngen.ImageUrl = "~/masterpage/images/hr/gray_1.png";
                btnpos.ImageUrl = "~/masterpage/images/hr/gray_2.png";
                btnaddress.ImageUrl = "~/masterpage/images/hr/gray_3.png";
                btnheal.ImageUrl = "~/masterpage/images/hr/gray_4.png";
                btmapprove.ImageUrl = "~/masterpage/images/hr/color_5.png";
                btnadd_asset.ImageUrl = "~/masterpage/images/hr/gray_6.png";

                BoxGen.Visible = false;
                BoxPos.Visible = false;
                BoxAddress.Visible = false;
                BoxHeal.Visible = false;
                BoxApprove.Visible = true;
                _PanelReApprove.Visible = true;

                search_cen_employee_detail _allEmployee = new search_cen_employee_detail ();
                _data_cen_employee = getEmployeeList ("99", _allEmployee);
                
                ViewState["EmpAllList"] = _data_cen_employee;

                _data_cen_employee = (data_cen_employee)ViewState["EmpAllList"];
                _funcTool.setDdlData (ddlEmpApprove1Cen, _data_cen_employee.cen_employee_list_u0, "emp_name_th", "emp_idx");
                ddlEmpApprove1Cen.Items.Insert (0, new ListItem ("เลือกผู้อนุมัติ 1...", "0"));
                _funcTool.setDdlData (ddlEmpApprove2Cen, _data_cen_employee.cen_employee_list_u0, "emp_name_th", "emp_idx");
                ddlEmpApprove2Cen.Items.Insert (0, new ListItem ("เลือกผู้อนุมัติ 2...", "0"));




                BoxAsset.Visible = false;

                bt_scroll_down_add.Visible = false;
                bt_scroll_up_add.Visible = false;

                break;
            case 6: //Asset
                btngen.ImageUrl = "~/masterpage/images/hr/gray_1.png";
                btnpos.ImageUrl = "~/masterpage/images/hr/gray_2.png";
                btnaddress.ImageUrl = "~/masterpage/images/hr/gray_3.png";
                btnheal.ImageUrl = "~/masterpage/images/hr/gray_4.png";
                btmapprove.ImageUrl = "~/masterpage/images/hr/gray_5.png";
                btnadd_asset.ImageUrl = "~/masterpage/images/hr/color_6.png";

                BoxGen.Visible = false;
                BoxPos.Visible = false;
                BoxAddress.Visible = false;
                BoxHeal.Visible = false;
                BoxApprove.Visible = false;
                BoxAsset.Visible = true;

                bt_scroll_down_add.Visible = false;
                bt_scroll_up_add.Visible = false;
                break;
        }
    }

    protected void Menu_Color_Edit(int choice)
    {
        Panel BoxGen = (Panel)FvEdit_Detail.FindControl("BoxGen");
        Panel BoxPos = (Panel)FvEdit_Detail.FindControl("BoxPos");
        Panel BoxAddress = (Panel)FvEdit_Detail.FindControl("BoxAddress");
        Panel BoxHeal = (Panel)FvEdit_Detail.FindControl("BoxHeal");
        Panel BoxApprove = (Panel)FvEdit_Detail.FindControl("BoxApprove");
        Panel BoxAsset = (Panel)FvEdit_Detail.FindControl("BoxAsset");
        Panel BoxAsset_edit = (Panel)FvEdit_Detail.FindControl("BoxAsset_Edit");

        

        switch (choice)
        {
            case 1: //Gen
                btngen_edit.ImageUrl = "~/masterpage/images/hr/color_1.png";
                btnpos_edit.ImageUrl = "~/masterpage/images/hr/gray_2.png";
                btnaddress_edit.ImageUrl = "~/masterpage/images/hr/gray_3.png";
                btnheal_edit.ImageUrl = "~/masterpage/images/hr/gray_4.png";
                btmapprove_edit.ImageUrl = "~/masterpage/images/hr/gray_5.png";
                btnadd_asset_edit.ImageUrl = "~/masterpage/images/hr/gray_6.png";

                BoxGen.Visible = true;
                BoxPos.Visible = false;
                BoxAddress.Visible = false;
                BoxHeal.Visible = false;
                BoxApprove.Visible = false;
                BoxAsset.Visible = false;

                bt_scroll_down.Visible = false;
                bt_scroll_up.Visible = false;

                break;
            case 2: //Pos
                btngen_edit.ImageUrl = "~/masterpage/images/hr/gray_1.png";
                btnpos_edit.ImageUrl = "~/masterpage/images/hr/color_2.png";
                btnaddress_edit.ImageUrl = "~/masterpage/images/hr/gray_3.png";
                btnheal_edit.ImageUrl = "~/masterpage/images/hr/gray_4.png";
                btmapprove_edit.ImageUrl = "~/masterpage/images/hr/gray_5.png";
                btnadd_asset_edit.ImageUrl = "~/masterpage/images/hr/gray_6.png";

                BoxGen.Visible = false;
                BoxPos.Visible = true;
                BoxAddress.Visible = false;
                BoxHeal.Visible = false;
                BoxApprove.Visible = false;
                BoxAsset.Visible = false;

                bt_scroll_down.Visible = false;
                bt_scroll_up.Visible = false;

                break;
            case 3: //Address
                btngen_edit.ImageUrl = "~/masterpage/images/hr/gray_1.png";
                btnpos_edit.ImageUrl = "~/masterpage/images/hr/gray_2.png";
                btnaddress_edit.ImageUrl = "~/masterpage/images/hr/color_3.png";
                btnheal_edit.ImageUrl = "~/masterpage/images/hr/gray_4.png";
                btmapprove_edit.ImageUrl = "~/masterpage/images/hr/gray_5.png";
                btnadd_asset_edit.ImageUrl = "~/masterpage/images/hr/gray_6.png";

                BoxGen.Visible = false;
                BoxPos.Visible = false;
                BoxAddress.Visible = true;
                BoxHeal.Visible = false;
                BoxApprove.Visible = false;
                BoxAsset.Visible = false;

                bt_scroll_down.Visible = false;
                bt_scroll_up.Visible = false;

                break;
            case 4: //Heal
                btngen_edit.ImageUrl = "~/masterpage/images/hr/gray_1.png";
                btnpos_edit.ImageUrl = "~/masterpage/images/hr/gray_2.png";
                btnaddress_edit.ImageUrl = "~/masterpage/images/hr/gray_3.png";
                btnheal_edit.ImageUrl = "~/masterpage/images/hr/color_4.png";
                btmapprove_edit.ImageUrl = "~/masterpage/images/hr/gray_5.png";
                btnadd_asset_edit.ImageUrl = "~/masterpage/images/hr/gray_6.png";

                BoxGen.Visible = false;
                BoxPos.Visible = false;
                BoxAddress.Visible = false;
                BoxHeal.Visible = true;
                BoxApprove.Visible = false;
                BoxAsset.Visible = false;

                bt_scroll_down.Visible = false;
                bt_scroll_up.Visible = false;
                break;
            case 5: //Approve
                btngen_edit.ImageUrl = "~/masterpage/images/hr/gray_1.png";
                btnpos_edit.ImageUrl = "~/masterpage/images/hr/gray_2.png";
                btnaddress_edit.ImageUrl = "~/masterpage/images/hr/gray_3.png";
                btnheal_edit.ImageUrl = "~/masterpage/images/hr/gray_4.png";
                btmapprove_edit.ImageUrl = "~/masterpage/images/hr/color_5.png";
                btnadd_asset_edit.ImageUrl = "~/masterpage/images/hr/gray_6.png";

                BoxGen.Visible = false;
                BoxPos.Visible = false;
                BoxAddress.Visible = false;
                BoxHeal.Visible = false;
                BoxApprove.Visible = true;
                BoxAsset.Visible = false;

                bt_scroll_down.Visible = false;
                bt_scroll_up.Visible = false;
                break;
            case 6: //Asset
                btngen_edit.ImageUrl = "~/masterpage/images/hr/gray_1.png";
                btnpos_edit.ImageUrl = "~/masterpage/images/hr/gray_2.png";
                btnaddress_edit.ImageUrl = "~/masterpage/images/hr/gray_3.png";
                btnheal_edit.ImageUrl = "~/masterpage/images/hr/gray_4.png";
                btmapprove_edit.ImageUrl = "~/masterpage/images/hr/gray_5.png";
                btnadd_asset_edit.ImageUrl = "~/masterpage/images/hr/color_6.png";

                BoxGen.Visible = false;
                BoxPos.Visible = false;
                BoxAddress.Visible = false;
                BoxHeal.Visible = false;
                BoxApprove.Visible = false;
                BoxAsset.Visible = true;

                bt_scroll_down.Visible = false;
                bt_scroll_up.Visible = false;
                break;
        }
    }
    #endregion

    #region Gridview

    #region SetGrid
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormData(FormView fvName, Object obj)
    {
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }
    #endregion

    //protected void onRowCommand(Object sender, GridViewCommandEventArgs e)
    //{
    //    if (sender is GridView)
    //    {
    //        string cmdName = e.CommandName;
    //        switch (cmdName)
    //        {
    //            case "CmdPrint":

    //                GridViewRow rowSelect = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
    //                Label lblEmpIDX = (Label)rowSelect.FindControl("lblEmpIDX");


    //                //FvTemplate_print.ChangeMode(FormViewMode.ReadOnly);

    //                _dataEmployee.BoxEmployee_ProbationList = new Employee_Probation[1];
    //                Employee_Probation _dataEmployee_ = new Employee_Probation();

    //                _dataEmployee_.EmpIDX = int.Parse(lblEmpIDX.Text);
    //                _dataEmployee_.Condition = 2;

    //                _dataEmployee.BoxEmployee_ProbationList[0] = _dataEmployee_;
    //                //fss.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
    //                _dataEmployee = callServiceEmployee(_urlSelect_Report_Probation, _dataEmployee);
    //                setFormData(FvTemplate_print, _dataEmployee.BoxEmployee_ProbationList);

    //                //Label lblfullnameth = (Label)FvTemplate_print.FindControl("lblfullnameth");

    //                //lblfullnameth.Text = litFullname.Text;// _dataEmployee.BoxEmployee_ProbationList[0].FullNameTH.ToString();


    //                fss.Text = lblEmpIDX.Text + "   dddddd   " + _dataEmployee.BoxEmployee_ProbationList[0].FullNameTH.ToString();
    //                break;
    //        }
    //    }
    //}


    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvEmployee":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton btnedit_emp = (LinkButton)e.Row.FindControl("btnedit_emp");

                    linkBtnTrigger(btnedit_emp);
                }
                break;
            case "GvEmployee_Report":
                if (ViewState["Columns_Check_Search"].ToString() != "0")
                {
                    string To = ViewState["Columns_Check_Search"].ToString();
                    string Sum;
                    string[] ToId = To.Split(',');
                    int _i = 0;
                    foreach (string ToEmail in ToId)
                    {
                        if (ToEmail != String.Empty)
                        {
                            if (ToEmail != "0")
                            {
                                GvEmployee_Report.Columns[_i].Visible = true;
                            }
                            else
                            {
                                GvEmployee_Report.Columns[_i].Visible = false;
                            }
                        }
                        _i++;
                    }
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    var lbl_Education = (Label)e.Row.FindControl("lbl_Education");
                    var lbl_eduname = (Label)e.Row.FindControl("lbl_eduname");
                    string temp_edu = "";

                    string To_1 = lbl_Education.Text;
                    string[] ToId_ = To_1.Split(',');
                    int _i_ = 0;
                    foreach (string ToEmail_ in ToId_)
                    {
                        if (ToEmail_ != String.Empty)
                        {
                            temp_edu += ToEmail_ + "<br />";
                        }
                        _i_++;
                    }
                    lbl_eduname.Text = temp_edu;

                    var lbl_Train = (Label)e.Row.FindControl("lbl_Train");
                    var lbl_Train_name = (Label)e.Row.FindControl("lbl_Train_name");
                    string temp_train = "";

                    string To_1_ = lbl_Train.Text;
                    string[] ToId_1 = To_1_.Split(',');
                    int _i_1 = 0;
                    foreach (string ToEmail_1 in ToId_1)
                    {
                        if (ToEmail_1 != String.Empty)
                        {
                            temp_train += ToEmail_1 + "<br />";
                        }
                        _i_1++;
                    }
                    lbl_Train_name.Text = temp_train;
                }
                break;
            case "GvPosAdd_View":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    var txtEditorgIDX_Select = (TextBox)e.Row.FindControl("txtEditorgIDX_Select");
                    var txtEditDepIDX_Select = (TextBox)e.Row.FindControl("txtEditDepIDX_Select");
                    var txtEditSecIDX_Select = (TextBox)e.Row.FindControl("txtEditSecIDX_Select");
                    var txtEditPosIDX_Select = (TextBox)e.Row.FindControl("txtEditPosIDX_Select");

                    var ddlOrganizationEdit_Select = (DropDownList)e.Row.FindControl("ddlOrganizationEdit_Select");
                    var ddlDepartmentEdit_Select = (DropDownList)e.Row.FindControl("ddlDepartmentEdit_Select");
                    var ddlSectionEdit_Select = (DropDownList)e.Row.FindControl("ddlSectionEdit_Select");
                    var ddlPositionEdit_Select = (DropDownList)e.Row.FindControl("ddlPositionEdit_Select");

                    select_org(ddlOrganizationEdit_Select);
                    ddlOrganizationEdit_Select.SelectedValue = txtEditorgIDX_Select.Text;
                    select_dep(ddlDepartmentEdit_Select, int.Parse(txtEditorgIDX_Select.Text));
                    ddlDepartmentEdit_Select.SelectedValue = txtEditDepIDX_Select.Text;
                    select_sec(ddlSectionEdit_Select, int.Parse(txtEditorgIDX_Select.Text), int.Parse(txtEditDepIDX_Select.Text));
                    ddlSectionEdit_Select.SelectedValue = txtEditSecIDX_Select.Text;
                    select_pos(ddlPositionEdit_Select, int.Parse(txtEditorgIDX_Select.Text), int.Parse(txtEditDepIDX_Select.Text), int.Parse(txtEditSecIDX_Select.Text));
                    ddlPositionEdit_Select.SelectedValue = txtEditPosIDX_Select.Text;
                }
                break;
            case "GvPositionList":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    var lbl_org_idx = (Label)e.Row.FindControl("lbl_org_idx");
                    var lbl_wg_idx = (Label)e.Row.FindControl("lbl_wg_idx");
                    var lbl_lw_idx = (Label)e.Row.FindControl("lbl_lw_idx");
                    var lbl_dept_idx = (Label)e.Row.FindControl("lbl_dept_idx");
                    var lbl_sec_idx = (Label)e.Row.FindControl("lbl_sec_idx");
                    var lbl_pos_idx = (Label)e.Row.FindControl("lbl_pos_idx");


                    var ddlOrganizationEdit = (DropDownList)e.Row.FindControl("ddlOrganizationEdit");
                    var ddlWorkGroupEdit = (DropDownList)e.Row.FindControl("ddlWorkGroupEdit");
                    var ddlLineWorkEdit = (DropDownList)e.Row.FindControl("ddlLineWorkEdit");
                    var ddlDepartmentEdit = (DropDownList)e.Row.FindControl("ddlDepartmentEdit");
                    var ddlSectionEdit = (DropDownList)e.Row.FindControl("ddlSectionEdit");
                    var ddlPositionEdit = (DropDownList)e.Row.FindControl("ddlPositionEdit");

                    _data_cen_master = getMasterList("3", null);

                    _funcTool.setDdlData(ddlOrganizationEdit, _data_cen_master.cen_org_list_m0, "org_name_th", "org_idx"); 
                    ddlOrganizationEdit.SelectedValue = lbl_org_idx.Text;

                    getWorkGroupCen(ddlWorkGroupEdit, int.Parse(lbl_org_idx.Text));
                    ddlWorkGroupEdit.SelectedValue = lbl_wg_idx.Text;

                    getLineWorkCen(ddlLineWorkEdit, int.Parse(lbl_org_idx.Text), int.Parse(lbl_wg_idx.Text));
                    ddlLineWorkEdit.SelectedValue = lbl_lw_idx.Text;

                    getDepartmentCen(ddlDepartmentEdit, int.Parse(lbl_org_idx.Text), int.Parse(lbl_wg_idx.Text), int.Parse(lbl_lw_idx.Text));
                    ddlDepartmentEdit.SelectedValue = lbl_dept_idx.Text;

                    getSectionCen(ddlSectionEdit, int.Parse(lbl_org_idx.Text), int.Parse(lbl_wg_idx.Text), int.Parse(lbl_lw_idx.Text), int.Parse(lbl_dept_idx.Text));
                    ddlSectionEdit.SelectedValue = lbl_sec_idx.Text;

                    getPositionCen(ddlPositionEdit, int.Parse(lbl_org_idx.Text), int.Parse(lbl_wg_idx.Text), int.Parse(lbl_lw_idx.Text), int.Parse(lbl_dept_idx.Text), int.Parse(lbl_sec_idx.Text));
                    ddlPositionEdit.SelectedValue = lbl_pos_idx.Text;

                }
                break;

            case "GvReference_View":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                break;

            case "GvChildAdd_View":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                var GvChildAdd_View = (GridView)FvEdit_Detail.FindControl("GvChildAdd_View");

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvChildAdd_View.EditIndex == e.Row.RowIndex)
                    {
                        var lbl_Child_num = (Label)e.Row.FindControl("lbl_Child_num");
                        var ddlchildnumber = (DropDownList)e.Row.FindControl("ddlchildnumber");
                        ddlchildnumber.SelectedValue = lbl_Child_num.Text;
                    }
                }
                break;
            case "GvPri_View":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                }
                break;
            case "GvEducation_View":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                }
                var GvEducation_View = (GridView)FvEdit_Detail.FindControl("GvEducation_View");

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvEducation_View.EditIndex == e.Row.RowIndex)
                    {
                        var lbl_Edu_qualification_ID = (Label)e.Row.FindControl("lbl_Edu_qualification_ID");
                        var ddleducationback = (DropDownList)e.Row.FindControl("ddleducationback");
                        ddleducationback.SelectedValue = lbl_Edu_qualification_ID.Text;
                    }
                }
                break;
            case "GvTrain_View":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                }
                break;
            case "GvEmpShift":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    var txtEditorgIDX_Select = (TextBox)e.Row.FindControl("txtEditorgIDX_Select");
                    var txtEdit_midx = (TextBox)e.Row.FindControl("txtEdit_midx");
                    //var txtEditSecIDX_Select = (TextBox)e.Row.FindControl("txtEditSecIDX_Select");
                    //var txtEditPosIDX_Select = (TextBox)e.Row.FindControl("txtEditPosIDX_Select");

                    var ddlOrganizationEdit_Select = (DropDownList)e.Row.FindControl("ddlOrganizationEdit_");
                    var ddlmidxEdit_Select = (DropDownList)e.Row.FindControl("ddlmidxEdit_Select");
                    //var ddlSectionEdit_Select = (DropDownList)e.Row.FindControl("ddlSectionEdit_Select");
                    //var ddlPositionEdit_Select = (DropDownList)e.Row.FindControl("ddlPositionEdit_Select");

                    select_org(ddlOrganizationEdit_Select);
                    ddlOrganizationEdit_Select.SelectedValue = txtEditorgIDX_Select.Text;

                    select_empshift(ddlmidxEdit_Select, int.Parse(txtEditorgIDX_Select.Text));
                    ddlmidxEdit_Select.SelectedValue = txtEdit_midx.Text;
                }
                break;
            case "GvAsset_View":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    var ddlassettype_edit = (DropDownList)e.Row.FindControl("ddlassettype_edit");
                    var ddlunittype_edit = (DropDownList)e.Row.FindControl("ddlunittype_edit");

                    var txtEdit_Asset_Type = (TextBox)e.Row.FindControl("txtEdit_Asset_Type");
                    var txtunittype_edit = (TextBox)e.Row.FindControl("txtunittype_edit"); ;

                    Select_asset_type(ddlassettype_edit);
                    ddlassettype_edit.SelectedValue = txtEdit_Asset_Type.Text;
                    Select_unit_type(ddlunittype_edit);
                    ddlunittype_edit.SelectedValue = txtunittype_edit.Text;
                }
                break;

            case "GvEmpProbationList":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvEmpProbationList.EditIndex != e.Row.RowIndex)
                    {
                        var lbEmpProbation = (Label)e.Row.FindControl("lbEmpProbation");
                        var lblprobation_yes = (Label)e.Row.FindControl("lblprobation_yes");
                        var lblprobation_no = (Label)e.Row.FindControl("lblprobation_no");
                        var lblJobGradeIDX = (Label)e.Row.FindControl("lblJobGradeIDX");
                        //var btndaily = (LinkButton)e.Row.FindControl("btndaily");
                        //var btnofficer = (LinkButton)e.Row.FindControl("btnofficer");
                        //var btnmanager = (LinkButton)e.Row.FindControl("btnmanager");
                        var lblEmpIDX = (Label)e.Row.FindControl("lblEmpIDX");
                        var litFullname = (Literal)e.Row.FindControl("litFullname");


                        if (lbEmpProbation.Text == "1")
                        {
                            lblprobation_yes.Visible = true;
                            lblprobation_no.Visible = false;
                        }
                        else
                        {
                            lblprobation_yes.Visible = false;
                            lblprobation_no.Visible = true;
                        }

                        //if (int.Parse(lblJobGradeIDX.Text) < 3)
                        //{
                        //    btndaily.Visible = true;
                        //    btnofficer.Visible = false;
                        //    btnmanager.Visible = false;
                        //    linkBtnTrigger(btndaily);
                        //}
                        //else if (int.Parse(lblJobGradeIDX.Text) > 2 && int.Parse(lblJobGradeIDX.Text) < 8)
                        //{
                        //    btndaily.Visible = false;
                        //    btnofficer.Visible = true;
                        //    btnmanager.Visible = false;
                        //    linkBtnTrigger(btnofficer);
                        //}
                        //else
                        //{
                        //    btndaily.Visible = false;
                        //    btnofficer.Visible = false;
                        //    btnmanager.Visible = true;
                        //    linkBtnTrigger(btnmanager);
                        //}

                        //ViewState["EmpIDX_Print"] = lblEmpIDX.Text;


                        //FvTemplate_print.ChangeMode(FormViewMode.ReadOnly);
                        //Select_Report_Probation_Print(FvTemplate_print, int.Parse(lblEmpIDX.Text));



                    }
                }
                break;
            case "Gv_education":
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {

                    try
                    {

                        Label lbleducation_all_sum = (Label)e.Row.FindControl("lbleducation_all_sum");

                        decimal value = Convert.ToDecimal(lbleducation_all_sum.Text);
                        int n = Convert.ToInt32(value);

                        tot_actual_all_time += Convert.ToDecimal(value);
                    }
                    catch
                    {

                    }

                }
                if (e.Row.RowType == DataControlRowType.Footer)
                {
                    try
                    {
                        Label lit_education_all_sum = (Label)e.Row.FindControl("lit_education_all_sum");

                        lit_education_all_sum.Text = String.Format("{0:N2}", tot_actual_all_time);
                    }
                    catch
                    {

                    }

                }
                break;
            case "Gv_sex":
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    try
                    {
                        Label lbltime_all_sum = (Label)e.Row.FindControl("lbltime_all_sum");

                        decimal value = Convert.ToDecimal(lbltime_all_sum.Text);
                        int n = Convert.ToInt32(value);

                        tot_actual_all_time += Convert.ToDecimal(value);
                    }
                    catch
                    {

                    }
                }
                if (e.Row.RowType == DataControlRowType.Footer)
                {
                    try
                    {
                        Label lit_time_all_sum = (Label)e.Row.FindControl("lit_time_all_sum");

                        lit_time_all_sum.Text = String.Format("{0:N2}", tot_actual_all_time);
                    }
                    catch
                    {

                    }

                }
                break;

            case "Gv_age":
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {

                    try
                    {

                        Label lblage_all_sum = (Label)e.Row.FindControl("lblage_all_sum");

                        decimal value = Convert.ToDecimal(lblage_all_sum.Text);
                        int n = Convert.ToInt32(value);

                        tot_actual_all_time += Convert.ToDecimal(value);
                    }
                    catch
                    {

                    }

                }
                if (e.Row.RowType == DataControlRowType.Footer)
                {
                    try
                    {
                        Label lit_age_all_sum = (Label)e.Row.FindControl("lit_age_all_sum");

                        lit_age_all_sum.Text = String.Format("{0:N2}", tot_actual_all_time);
                    }
                    catch
                    {

                    }

                }
                break;

            case "Gv_car":
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {

                    try
                    {

                        Label lblcar_all_sum = (Label)e.Row.FindControl("lblcar_all_sum");
                        Label lblcar_all_sum_salary = (Label)e.Row.FindControl("lblcar_all_sum_salary");

                        decimal value = Convert.ToDecimal(lblcar_all_sum.Text);
                        decimal value1 = Convert.ToDecimal(lblcar_all_sum_salary.Text);
                        int n = Convert.ToInt32(value);
                        int n1 = Convert.ToInt32(value1);

                        tot_actual_all_time += Convert.ToDecimal(value);
                        tot_actual_all_time_salary += Convert.ToDecimal(value1);
                    }
                    catch
                    {

                    }

                }
                if (e.Row.RowType == DataControlRowType.Footer)
                {
                    try
                    {
                        Label lit_car_all_sum = (Label)e.Row.FindControl("lit_car_all_sum");
                        Label lit_car_all_sum_salary = (Label)e.Row.FindControl("lit_car_all_sum_salary");

                        lit_car_all_sum.Text = String.Format("{0:N2}", tot_actual_all_time);
                        lit_car_all_sum_salary.Text = String.Format("{0:N2}", tot_actual_all_time_salary);
                    }
                    catch
                    {

                    }

                }
                break;

            case "GvGurantee_View":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                break;

            case "Gv_CountEmployee":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                }
                /*if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    try
                    {
                        Label lbltime_all_sum = (Label)e.Row.FindControl("lbltime_all_sum");

                        decimal value = Convert.ToDecimal(lbltime_all_sum.Text);
                        int n = Convert.ToInt32(value);

                        tot_actual_all_time += Convert.ToDecimal(value);
                    }
                    catch
                    {

                    }
                }
                if (e.Row.RowType == DataControlRowType.Footer)
                {
                    try
                    {
                        Label lit_time_all_sum = (Label)e.Row.FindControl("lit_time_all_sum");

                        lit_time_all_sum.Text = String.Format("{0:N2}", tot_actual_all_time);
                    }
                    catch
                    {

                    }

                }*/
                break;

            case "Gv_race":
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    try
                    {
                        Label lbltime_all_sum = (Label)e.Row.FindControl("lbltime_all_sum");

                        decimal value = Convert.ToDecimal(lbltime_all_sum.Text);
                        int n = Convert.ToInt32(value);

                        tot_actual_all_time += Convert.ToDecimal(value);
                    }
                    catch
                    {

                    }
                }
                if (e.Row.RowType == DataControlRowType.Footer)
                {
                    try
                    {
                        Label lit_time_all_sum = (Label)e.Row.FindControl("lit_time_all_sum");

                        lit_time_all_sum.Text = String.Format("{0:N2}", tot_actual_all_time);
                    }
                    catch
                    {

                    }

                }
                break;

            case "Gv_new_employee":
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    try
                    {
                        Label lbltime_all_sum = (Label)e.Row.FindControl("lbltime_all_sum");

                        decimal value = Convert.ToDecimal(lbltime_all_sum.Text);
                        int n = Convert.ToInt32(value);

                        tot_actual_all_time += Convert.ToDecimal(value);
                    }
                    catch
                    {

                    }
                }
                if (e.Row.RowType == DataControlRowType.Footer)
                {
                    try
                    {
                        Label lit_time_all_sum = (Label)e.Row.FindControl("lit_time_all_sum");

                        lit_time_all_sum.Text = String.Format("{0:N2}", tot_actual_all_time);
                    }
                    catch
                    {

                    }

                }
                break;
            case "Gv_report_new_employee_name":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                }
                break;
            case "Gv_LeaveEmployee":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                }
                break;

        }
    }
    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvEmployee":
                GvEmployee.PageIndex = e.NewPageIndex;
                GvEmployee.DataBind();
                Select_Employee_index_search();
                break;
            case "GvEmployee_Report":
                GvEmployee_Report.PageIndex = e.NewPageIndex;
                GvEmployee_Report.DataBind();
                Select_Employee_report_search();
                break;
            case "GvPosAdd_View":
                var GvPosAdd_View = (GridView)FvEdit_Detail.FindControl("GvPosAdd_View");
                GvPosAdd_View.PageIndex = e.NewPageIndex;
                GvPosAdd_View.DataSource = ViewState["BoxGvPosAdd_View"];
                GvPosAdd_View.DataBind();
                break;
            case "GvReference_View":
                var GvReference_View = (GridView)FvEdit_Detail.FindControl("GvReference_View");
                GvReference_View.PageIndex = e.NewPageIndex;
                GvReference_View.DataSource = ViewState["BoxGvRefer_view"];
                GvReference_View.DataBind();
                break;
            case "GvChildAdd_View":
                var GvChildAdd_View = (GridView)FvEdit_Detail.FindControl("GvChildAdd_View");
                GvChildAdd_View.PageIndex = e.NewPageIndex;
                GvChildAdd_View.DataSource = ViewState["BoxGvChild_view"];
                GvChildAdd_View.DataBind();
                break;
            case "GvPri_View":
                var GvPri_View = (GridView)FvEdit_Detail.FindControl("GvPri_View");
                GvPri_View.PageIndex = e.NewPageIndex;
                GvPri_View.DataSource = ViewState["BoxGvPrior_view"];
                GvPri_View.DataBind();
                break;
            case "GvEducation_View":
                var GvEducation_View = (GridView)FvEdit_Detail.FindControl("GvEducation_View");
                GvEducation_View.PageIndex = e.NewPageIndex;
                GvEducation_View.DataSource = ViewState["BoxGvEducation_view"];
                GvEducation_View.DataBind();
                break;
            case "GvTrain_View":
                var GvTrain_View = (GridView)FvEdit_Detail.FindControl("GvTrain_View");
                GvTrain_View.PageIndex = e.NewPageIndex;
                GvTrain_View.DataSource = ViewState["BoxGvTrain_view"];
                GvTrain_View.DataBind();
                break;
            case "GvRsecApproveDatabase":
                TextBox txtempcode = (TextBox)FvDetailUser.FindControl("txtempcode");

                GvRsecApproveDatabase.PageIndex = e.NewPageIndex;
                GvRsecApproveDatabase.DataBind();
                Select_ApproveEmployee(GvRsecApproveDatabase, txtempcode.Text);
                break;
            case "GvEmpShift":
                var GvEmpShift_view = (GridView)FvEdit_Detail.FindControl("GvEmpShift");
                GvEmpShift_view.PageIndex = e.NewPageIndex;
                GvEmpShift_view.DataBind();
                Select_Gv_EmpShift(int.Parse(ViewState["fv_emp_idx"].ToString()));
                //Select_ApproveEmployee(GvRsecApproveDatabase, txtempcode.Text);
                break;

            case "GvEmpProbationList":
                GvEmpProbationList.PageIndex = e.NewPageIndex;
                Select_Report_Probation();
                break;

            case "Gv_car":
                Gv_car.PageIndex = e.NewPageIndex;
                Gv_car.DataSource = ViewState["Temp_Gv_car"];
                Gv_car.DataBind();
                break;
            case "Gv_CountEmployee":
                Gv_CountEmployee.PageIndex = e.NewPageIndex;
                Select_Chart_Count_Employee_Detail();
                break;
            case "Gv_LeaveEmployee":
                Gv_LeaveEmployee.PageIndex = e.NewPageIndex;
                Select_Chart_Leave_Employee_Detail();
                break;
            case "GvGurantee_View":
                var GvGurantee_View = (GridView)FvEdit_Detail.FindControl("GvGurantee_View");
                GvGurantee_View.PageIndex = e.NewPageIndex;
                GvGurantee_View.DataSource = ViewState["Boxgv_Gurantee"];
                GvGurantee_View.DataBind();
                break;
            case "Gv_new_employee":
                Gv_new_employee.PageIndex = e.NewPageIndex;
                Select_Chart_New_Employee();
                break;
            case "Gv_report_new_employee_name":
                Gv_report_new_employee_name.PageIndex = e.NewPageIndex;
                Select_Chart_New_Employee_detail();
                break;
        }
    }

    #endregion

    #region GvRowEditing
    protected void gvRowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvPositionList":
                var GvPositionList = (GridView)FvEdit_Detail.FindControl("GvPositionList");
                GvPositionList.EditIndex = e.NewEditIndex;
                GvPositionList.DataSource = ViewState["vs_PositionList_View"];
                GvPositionList.DataBind();
                break;

            case "GvPosAdd_View":
                var GvPosAdd_View = (GridView)FvEdit_Detail.FindControl("GvPosAdd_View");
                GvPosAdd_View.EditIndex = e.NewEditIndex;
                GvPosAdd_View.DataSource = ViewState["BoxGvPosAdd_View"];
                GvPosAdd_View.DataBind();
                break;

            case "GvChildAdd_View":
                var GvChildAdd_View = (GridView)FvEdit_Detail.FindControl("GvChildAdd_View");
                GvChildAdd_View.EditIndex = e.NewEditIndex;
                GvChildAdd_View.DataSource = ViewState["BoxGvChild_view"];
                GvChildAdd_View.DataBind();
                break;

            case "GvReference_View":
                var GvReference_View = (GridView)FvEdit_Detail.FindControl("GvReference_View");
                GvReference_View.EditIndex = e.NewEditIndex;
                GvReference_View.DataSource = ViewState["BoxGvRefer_view"];
                GvReference_View.DataBind();
                break;

            case "GvPri_View":
                var GvPri_View = (GridView)FvEdit_Detail.FindControl("GvPri_View");
                GvPri_View.EditIndex = e.NewEditIndex;
                GvPri_View.DataSource = ViewState["BoxGvPrior_view"];
                GvPri_View.DataBind();
                break;

            case "GvEducation_View":
                var GvEducation_View = (GridView)FvEdit_Detail.FindControl("GvEducation_View");
                GvEducation_View.EditIndex = e.NewEditIndex;
                GvEducation_View.DataSource = ViewState["BoxGvEducation_view"];
                GvEducation_View.DataBind();
                break;

            case "GvTrain_View":
                var GvTrain_View = (GridView)FvEdit_Detail.FindControl("GvTrain_View");
                GvTrain_View.EditIndex = e.NewEditIndex;
                GvTrain_View.DataSource = ViewState["BoxGvTrain_view"];
                GvTrain_View.DataBind();
                break;
            case "GvEmpShift":
                var GvEmpShift_view = (GridView)FvEdit_Detail.FindControl("GvEmpShift");
                GvEmpShift_view.EditIndex = e.NewEditIndex;
                Select_Gv_EmpShift(int.Parse(ViewState["fv_emp_idx"].ToString()));
                break;
            case "GvAsset_View":
                var GvAsset_View = (GridView)FvEdit_Detail.FindControl("GvAsset_View");
                GvAsset_View.EditIndex = e.NewEditIndex;
                Select_Gv_Asset(GvAsset_View, int.Parse(ViewState["fv_emp_idx"].ToString()));
                break;
            case "GvGurantee_View":
                var GvGurantee_View = (GridView)FvEdit_Detail.FindControl("GvGurantee_View");
                GvGurantee_View.EditIndex = e.NewEditIndex;
                GvGurantee_View.DataSource = ViewState["Boxgv_Gurantee"];
                GvGurantee_View.DataBind();
                break;
        }

    }
    #endregion

    #region GvRowUpdating
    protected void gvRowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var gvName = (GridView)sender;

        switch (gvName.ID)
        {

            case "GvPosAdd_View":

                var GvPosAdd_View = (GridView)FvEdit_Detail.FindControl("GvPosAdd_View");

                int EODSPIDX1 = Convert.ToInt32(GvPosAdd_View.DataKeys[e.RowIndex].Values[0].ToString());
                var ddlOrganizationEdit_Select = (DropDownList)GvPosAdd_View.Rows[e.RowIndex].FindControl("ddlOrganizationEdit_Select");
                var ddlDepartmentEdit_Select = (DropDownList)GvPosAdd_View.Rows[e.RowIndex].FindControl("ddlDepartmentEdit_Select");
                var ddlSectionEdit_Select = (DropDownList)GvPosAdd_View.Rows[e.RowIndex].FindControl("ddlSectionEdit_Select");
                var ddlPositionEdit_Select = (DropDownList)GvPosAdd_View.Rows[e.RowIndex].FindControl("ddlPositionEdit_Select");
                var ddStatusPosition = (DropDownList)GvPosAdd_View.Rows[e.RowIndex].FindControl("ddStatusPosition");
                Label lbl_emp_idx = (Label)FvEdit_Detail.FindControl("lbl_emp_idx");

                var txtEdito = (TextBox)GvPosAdd_View.Rows[e.RowIndex].FindControl("txtEdito");

                _dataEmployee.ODSPRelation_list = new ODSP_List[1];
                ODSP_List _dataEmployee_ = new ODSP_List();

                _dataEmployee_.EODSPIDX = EODSPIDX1;
                _dataEmployee_.emp_idx = int.Parse(lbl_emp_idx.Text);
                _dataEmployee_.RPosIDX_S = int.Parse(ddlPositionEdit_Select.SelectedValue);
                _dataEmployee_.EStaOut = int.Parse(ddStatusPosition.SelectedValue);
                _dataEmployee_.OrgIDX_S = int.Parse(ddlOrganizationEdit_Select.SelectedValue);
                _dataEmployee_.RDeptIDX_S = int.Parse(ddlDepartmentEdit_Select.SelectedValue);
                _dataEmployee_.RSecIDX_S = int.Parse(ddlSectionEdit_Select.SelectedValue);
                _dataEmployee_.RPosIDX_S = int.Parse(ddlPositionEdit_Select.SelectedValue);

                _dataEmployee.ODSPRelation_list[0] = _dataEmployee_;
                _dataEmployee = callServiceEmployee(_urlSetODSPList_Update, _dataEmployee);

                Select_FvEdit_Detail(int.Parse(ViewState["fv_emp_idx"].ToString()));
                Menu_Color_Edit(2);
                txtfocus.Focus();
                GvPosAdd_View.EditIndex = -1;
                GvPosAdd_View.DataSource = ViewState["BoxGvPosAdd_View"];
                GvPosAdd_View.DataBind();
                break;

            case "GvPositionList":

                var GvPositionList = (GridView)FvEdit_Detail.FindControl("GvPositionList");

                int pos_emp_idx = Convert.ToInt32(GvPositionList.DataKeys[e.RowIndex].Values[0].ToString());
                DropDownList ddlPositionEdit = (DropDownList)GvPositionList.Rows[e.RowIndex].FindControl("ddlPositionEdit");
                DropDownList ddlpos_emp_status = (DropDownList)GvPositionList.Rows[e.RowIndex].FindControl("ddlpos_emp_status");
                
                Label lbl_emp_idx_ = (Label)FvEdit_Detail.FindControl("lbl_emp_idx");

             
                _dataEmployee.cen_position_emp_r0_list = new cen_position_emp_r0_detail[1];
                cen_position_emp_r0_detail dataposition_r0 = new cen_position_emp_r0_detail();

                dataposition_r0.pos_emp_idx = pos_emp_idx;
                dataposition_r0.emp_idx = int.Parse(lbl_emp_idx_.Text);
                dataposition_r0.pos_idx = int.Parse(ddlPositionEdit.SelectedValue);
                dataposition_r0.flag_main = int.Parse(ddlpos_emp_status.SelectedValue);
                dataposition_r0.cemp_idx = int.Parse(ViewState["EmpIDX"].ToString());

                _dataEmployee.cen_position_emp_r0_list[0] = dataposition_r0;
                _dataEmployee = callServiceEmployee(_urlSetUpdatePositionCen, _dataEmployee);

                Select_FvEdit_Detail(int.Parse(ViewState["fv_emp_idx"].ToString()));
                Menu_Color_Edit(2);
                txtfocus.Focus();
                GvPositionList.EditIndex = -1;
                GvPositionList.DataSource = ViewState["vs_PositionList_View"];
                GvPositionList.DataBind();
                break;

            case "GvReference_View":

                var GvReference_View = (GridView)FvEdit_Detail.FindControl("GvReference_View");
                int ReIDX = Convert.ToInt32(GvReference_View.DataKeys[e.RowIndex].Values[0].ToString());
                var txtRef_Name_edit = (TextBox)GvReference_View.Rows[e.RowIndex].FindControl("txtRef_Name_edit");
                var txtRef_pos_edit = (TextBox)GvReference_View.Rows[e.RowIndex].FindControl("txtRef_pos_edit");
                var txtRef_relation_edit = (TextBox)GvReference_View.Rows[e.RowIndex].FindControl("txtRef_relation_edit");
                var txtRef_tel_edit = (TextBox)GvReference_View.Rows[e.RowIndex].FindControl("txtRef_tel_edit");

                _dataEmployee.BoxReferent_list = new Referent_List[1];
                Referent_List _dataEmployee_ref = new Referent_List();

                _dataEmployee_ref.ReIDX = ReIDX;
                _dataEmployee_ref.ref_fullname = txtRef_Name_edit.Text;
                _dataEmployee_ref.ref_position = txtRef_pos_edit.Text;
                _dataEmployee_ref.ref_relation = txtRef_relation_edit.Text;
                _dataEmployee_ref.ref_tel = txtRef_tel_edit.Text;

                _dataEmployee.BoxReferent_list[0] = _dataEmployee_ref;
                _dataEmployee = callServiceEmployee(_urlSetUpdateReferList, _dataEmployee);

                Select_FvEdit_Detail(int.Parse(ViewState["fv_emp_idx"].ToString()));
                Menu_Color_Edit(1);
                txtfocus.Focus();
                GvReference_View.EditIndex = -1;
                GvReference_View.DataSource = ViewState["BoxGvRefer_view"];
                GvReference_View.DataBind();
                break;

            case "GvChildAdd_View":

                var GvChildAdd_View = (GridView)FvEdit_Detail.FindControl("GvChildAdd_View");
                int CHIDX = Convert.ToInt32(GvChildAdd_View.DataKeys[e.RowIndex].Values[0].ToString());
                var txtChild_name_edit = (TextBox)GvChildAdd_View.Rows[e.RowIndex].FindControl("txtChild_name_edit");
                var ddlchildnumber = (DropDownList)GvChildAdd_View.Rows[e.RowIndex].FindControl("ddlchildnumber");

                _dataEmployee.BoxChild_list = new Child_List[1];
                Child_List _dataEmployee_1 = new Child_List();

                _dataEmployee_1.CHIDX = CHIDX;
                _dataEmployee_1.Child_name = txtChild_name_edit.Text;
                _dataEmployee_1.Child_num = ddlchildnumber.SelectedValue.ToString();

                _dataEmployee.BoxChild_list[0] = _dataEmployee_1;
                _dataEmployee = callServiceEmployee(_urlSetUpdateChildList, _dataEmployee);

                Select_FvEdit_Detail(int.Parse(ViewState["fv_emp_idx"].ToString()));
                Menu_Color_Edit(1);
                txtfocus.Focus();
                GvChildAdd_View.EditIndex = -1;
                GvChildAdd_View.DataSource = ViewState["BoxGvChild_view"];
                GvChildAdd_View.DataBind();
                break;

            case "GvPri_View":

                var GvPri_View = (GridView)FvEdit_Detail.FindControl("GvPri_View");
                int ExperIDX = Convert.ToInt32(GvPri_View.DataKeys[e.RowIndex].Values[0].ToString());
                var txtPri_Nameorg_edit = (TextBox)GvPri_View.Rows[e.RowIndex].FindControl("txtPri_Nameorg_edit");
                var txtPri_pos_edit = (TextBox)GvPri_View.Rows[e.RowIndex].FindControl("txtPri_pos_edit");
                //var txtPri_worktime_edit = (TextBox)GvPri_View.Rows[e.RowIndex].FindControl("txtPri_worktime_edit");
                var txt_Pri_start_edit = (TextBox)GvPri_View.Rows[e.RowIndex].FindControl("txt_Pri_start_edit");
                var txt_Pri_resign_edit = (TextBox)GvPri_View.Rows[e.RowIndex].FindControl("txt_Pri_resign_edit");

                _dataEmployee.BoxPrior_list = new Prior_List[1];
                Prior_List _dataEmployee_2 = new Prior_List();

                _dataEmployee_2.ExperIDX = ExperIDX;
                _dataEmployee_2.Pri_Nameorg = txtPri_Nameorg_edit.Text;
                _dataEmployee_2.Pri_pos = txtPri_pos_edit.Text;
                //_dataEmployee_2.Pri_worktime = txtPri_worktime_edit.Text;
                _dataEmployee_2.Pri_startdate = txt_Pri_start_edit.Text;
                _dataEmployee_2.Pri_resigndate = txt_Pri_resign_edit.Text;

                _dataEmployee.BoxPrior_list[0] = _dataEmployee_2;
                _dataEmployee = callServiceEmployee(_urlSetUpdatePriorList, _dataEmployee);

                Select_FvEdit_Detail(int.Parse(ViewState["fv_emp_idx"].ToString()));
                Menu_Color_Edit(1);
                txtfocus.Focus();
                GvPri_View.EditIndex = -1;
                GvPri_View.DataSource = ViewState["BoxGvPrior_view"];
                GvPri_View.DataBind();
                break;

            case "GvEducation_View":

                var GvEducation_View = (GridView)FvEdit_Detail.FindControl("GvEducation_View");
                int EDUIDX = Convert.ToInt32(GvEducation_View.DataKeys[e.RowIndex].Values[0].ToString());
                var ddleducationback = (DropDownList)GvEducation_View.Rows[e.RowIndex].FindControl("ddleducationback");
                var txtEdu_name_edit = (TextBox)GvEducation_View.Rows[e.RowIndex].FindControl("txtEdu_name_edit");
                var txtEdu_branch_edit = (TextBox)GvEducation_View.Rows[e.RowIndex].FindControl("txtEdu_branch_edit");
                var txtEdu_start_edit = (TextBox)GvEducation_View.Rows[e.RowIndex].FindControl("txtEdu_start_edit");
                var txtEdu_end_edit = (TextBox)GvEducation_View.Rows[e.RowIndex].FindControl("txtEdu_end_edit");

                _dataEmployee.BoxEducation_list = new Education_List[1];
                Education_List _dataEmployee_3 = new Education_List();

                _dataEmployee_3.EDUIDX = EDUIDX;
                _dataEmployee_3.Edu_qualification_ID = ddleducationback.SelectedValue;
                _dataEmployee_3.Edu_name = txtEdu_name_edit.Text;
                _dataEmployee_3.Edu_branch = txtEdu_branch_edit.Text;
                _dataEmployee_3.Edu_start = txtEdu_start_edit.Text;
                _dataEmployee_3.Edu_end = txtEdu_end_edit.Text;

                _dataEmployee.BoxEducation_list[0] = _dataEmployee_3;
                _dataEmployee = callServiceEmployee(_urlSetUpdateEducationList, _dataEmployee);

                Select_FvEdit_Detail(int.Parse(ViewState["fv_emp_idx"].ToString()));
                Menu_Color_Edit(1);
                txtfocus.Focus();
                GvEducation_View.EditIndex = -1;
                GvEducation_View.DataSource = ViewState["BoxGvEducation_view"];
                GvEducation_View.DataBind();
                break;

            case "GvTrain_View":

                var GvTrain_View = (GridView)FvEdit_Detail.FindControl("GvTrain_View");
                int TNIDX = Convert.ToInt32(GvTrain_View.DataKeys[e.RowIndex].Values[0].ToString());
                var txtTrain_courses_edit = (TextBox)GvTrain_View.Rows[e.RowIndex].FindControl("txtTrain_courses_edit");
                var txtTrain_date_edit = (TextBox)GvTrain_View.Rows[e.RowIndex].FindControl("txtTrain_date_edit");
                var txtTrain_assessment_edit = (TextBox)GvTrain_View.Rows[e.RowIndex].FindControl("txtTrain_assessment_edit");
                var txtTrain_enddate_edit = (TextBox)GvTrain_View.Rows[e.RowIndex].FindControl("txtTrain_enddate_edit");

                _dataEmployee.BoxTrain_list = new Train_List[1];
                Train_List _dataEmployee_4 = new Train_List();

                _dataEmployee_4.TNIDX = TNIDX;
                _dataEmployee_4.Train_courses = txtTrain_courses_edit.Text;
                _dataEmployee_4.Train_date = txtTrain_date_edit.Text;
                _dataEmployee_4.Train_assessment = txtTrain_assessment_edit.Text;
                _dataEmployee_4.Train_enddate = txtTrain_enddate_edit.Text;

                _dataEmployee.BoxTrain_list[0] = _dataEmployee_4;
                _dataEmployee = callServiceEmployee(_urlSetUpdateTrainList, _dataEmployee);

                Select_FvEdit_Detail(int.Parse(ViewState["fv_emp_idx"].ToString()));
                Menu_Color_Edit(1);
                txtfocus.Focus();
                GvTrain_View.EditIndex = -1;
                GvTrain_View.DataSource = ViewState["BoxGvTrain_view"];
                GvTrain_View.DataBind();
                break;

            case "GvEmpShift":

                var GvEmpShift_view = (GridView)FvEdit_Detail.FindControl("GvEmpShift");
                int shiftTime_idx = Convert.ToInt32(GvEmpShift_view.DataKeys[e.RowIndex].Values[0].ToString());
                var ddlOrganizationEdit = (DropDownList)GvEmpShift_view.Rows[e.RowIndex].FindControl("ddlOrganizationEdit_");
                var ddlmidxEdit = (DropDownList)GvEmpShift_view.Rows[e.RowIndex].FindControl("ddlmidxEdit_Select");
                var ddStatus_select = (DropDownList)GvEmpShift_view.Rows[e.RowIndex].FindControl("ddStatus");
                var lblshift_time = (Label)GvEmpShift_view.Rows[e.RowIndex].FindControl("lblshift_time");
                var sfs = (Label)GvEmpShift_view.Rows[e.RowIndex].FindControl("sfs");

                _dataEmployee.ShiftTime_details = new ShiftTime[1];
                ShiftTime _orgList = new ShiftTime();
                _orgList.emp_shift_idx = int.Parse(ViewState["fv_emp_idx"].ToString());
                _orgList.type_selection = 3; //geidview
                //_orgList.shif_status = int.Parse(ddStatus_select.SelectedValue);
                _orgList.shiftTime_idx = int.Parse(lblshift_time.Text); //shiftTime_idx;
                _orgList.midx = int.Parse(ddlmidxEdit.SelectedValue);
                _orgList.shif_use = int.Parse(ddStatus_select.SelectedValue);
                _dataEmployee.ShiftTime_details[0] = _orgList;

                //sfs.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
                _dataEmployee = callServiceEmployee(_urlSetOrganizationList_shift, _dataEmployee);
                //sfs.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));

                if (int.Parse(_dataEmployee.return_code) == 1)
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('*** ไม่สามารถเปลี่ยนสถานะรายการปัจจุบันได้ เนื่องจากเป็นมีสถานะใช้งาน')", true);
                }
                else
                {
                    setGridData(GvEmpShift_view, _dataEmployee.ShiftTime_details);

                    //Select_FvEdit_Detail(int.Parse(ViewState["fv_emp_idx"].ToString()));
                    //Menu_Color_Edit(1);
                    txtfocus.Focus();
                    GvEmpShift_view.EditIndex = -1;
                    Select_Gv_EmpShift(int.Parse(ViewState["fv_emp_idx"].ToString()));
                }
                break;
            case "GvAsset_View":

                var GvAsset_View = (GridView)FvEdit_Detail.FindControl("GvAsset_View");
                int lblasidx_edit = Convert.ToInt32(GvAsset_View.DataKeys[e.RowIndex].Values[0].ToString());
                var ddlassettype_edit = (DropDownList)GvAsset_View.Rows[e.RowIndex].FindControl("ddlassettype_edit");
                var txtholderdate_edit = (TextBox)GvAsset_View.Rows[e.RowIndex].FindControl("txtholderdate_edit");
                var txtnum_edit = (TextBox)GvAsset_View.Rows[e.RowIndex].FindControl("txtnum_edit");
                var ddlunittype_edit = (DropDownList)GvAsset_View.Rows[e.RowIndex].FindControl("ddlunittype_edit");
                var txtassnumber_edit = (TextBox)GvAsset_View.Rows[e.RowIndex].FindControl("txtassnumber_edit");
                var txtdetail_edit = (TextBox)GvAsset_View.Rows[e.RowIndex].FindControl("txtdetail_edit");

                //var sfsa = (Label)FvEdit_Detail.FindControl("sfsa");
                //var sfsa = (Label)GvAsset_View.Rows[e.RowIndex].FindControl("sfsa");

                _dataEmployee.Holder_List = new Holder_Detail[1];
                Holder_Detail _AssetList = new Holder_Detail();
                //_AssetList.emp_shift_idx = int.Parse(ViewState["fv_emp_idx"].ToString());
                _AssetList.Select_action = 6; //update
                _AssetList.m0_asidx = int.Parse(ddlassettype_edit.SelectedValue);
                _AssetList.u0_asset_no = txtassnumber_edit.Text;
                _AssetList.u0_detail = txtdetail_edit.Text;
                _AssetList.u0_number = int.Parse(txtnum_edit.Text);
                _AssetList.u0_holderdate = txtholderdate_edit.Text;
                _AssetList.u0_asidx = lblasidx_edit;
                _dataEmployee.Holder_List[0] = _AssetList;

                //sfsa.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
                _dataEmployee = callServiceEmployee(_urlSetOrganizationList_shift, _dataEmployee);

                GvAsset_View.EditIndex = -1;
                Select_Gv_Asset(GvAsset_View, int.Parse(ViewState["fv_emp_idx"].ToString()));

                break;

            case "GvGurantee_View":

                var GvGurantee_View = (GridView)FvEdit_Detail.FindControl("GvGurantee_View");
                int GUAIDX_edit = Convert.ToInt32(GvGurantee_View.DataKeys[e.RowIndex].Values[0].ToString());
                var txtgu_fullname_edit = (TextBox)GvGurantee_View.Rows[e.RowIndex].FindControl("txtgu_fullname_edit");
                var txtgu_startdate_edit = (TextBox)GvGurantee_View.Rows[e.RowIndex].FindControl("txtgu_startdate_edit");
                var txtgu_relation_edit = (TextBox)GvGurantee_View.Rows[e.RowIndex].FindControl("txtgu_relation_edit");
                var txtgu_tel_edit = (TextBox)GvGurantee_View.Rows[e.RowIndex].FindControl("txtgu_tel_edit");

                _dataEmployee.BoxGurantee_list = new Gurantee_list[1];
                Gurantee_list _dataEmployee_gur = new Gurantee_list();

                _dataEmployee_gur.GUAIDX = GUAIDX_edit;
                _dataEmployee_gur.gu_fullname = txtgu_fullname_edit.Text;
                _dataEmployee_gur.gu_startdate = txtgu_startdate_edit.Text;
                _dataEmployee_gur.gu_relation = txtgu_relation_edit.Text;
                _dataEmployee_gur.gu_tel = txtgu_tel_edit.Text;

                _dataEmployee.BoxGurantee_list[0] = _dataEmployee_gur;

                _dataEmployee = callServiceEmployee(_urlGetUpdateGurantee, _dataEmployee);
                Select_FvEdit_Detail(int.Parse(ViewState["fv_emp_idx"].ToString()));
                Menu_Color_Edit(1);
                txtfocus.Focus();
                GvGurantee_View.EditIndex = -1;
                GvGurantee_View.DataSource = ViewState["Boxgv_Gurantee"];
                GvGurantee_View.DataBind();

                break;

        }

    }

    #endregion

    #region GvRowCancelingEdit
    protected void gvRowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var gvName = (GridView)sender;

        switch (gvName.ID)
        {

            case "GvPositionList":
                var GvPositionList = (GridView)FvEdit_Detail.FindControl("GvPositionList");
                GvPositionList.EditIndex = -1;
                GvPositionList.DataSource = ViewState["vs_PositionList_View"];
                GvPositionList.DataBind();
                break;

            case "GvPosAdd_View":
                var GvPosAdd_View = (GridView)FvEdit_Detail.FindControl("GvPosAdd_View");
                GvPosAdd_View.EditIndex = -1;
                GvPosAdd_View.DataSource = ViewState["BoxGvPosAdd_View"];
                GvPosAdd_View.DataBind();
                break;
            case "GvReference_View":
                var GvReference_View = (GridView)FvEdit_Detail.FindControl("GvReference_View");
                GvReference_View.EditIndex = -1;
                GvReference_View.DataSource = ViewState["BoxGvRefer_view"];
                GvReference_View.DataBind();
                break;
            case "GvChildAdd_View":
                var GvChildAdd_View = (GridView)FvEdit_Detail.FindControl("GvChildAdd_View");
                GvChildAdd_View.EditIndex = -1;
                GvChildAdd_View.DataSource = ViewState["BoxGvChild_view"];
                GvChildAdd_View.DataBind();
                break;
            case "GvPri_View":
                var GvPri_View = (GridView)FvEdit_Detail.FindControl("GvPri_View");
                GvPri_View.EditIndex = -1;
                GvPri_View.DataSource = ViewState["BoxGvPrior_view"];
                GvPri_View.DataBind();
                break;
            case "GvEducation_View":
                var GvEducation_View = (GridView)FvEdit_Detail.FindControl("GvEducation_View");
                GvEducation_View.EditIndex = -1;
                GvEducation_View.DataSource = ViewState["BoxGvEducation_view"];
                GvEducation_View.DataBind();
                break;
            case "GvTrain_View":
                var GvTrain_View = (GridView)FvEdit_Detail.FindControl("GvTrain_View");
                GvTrain_View.EditIndex = -1;
                GvTrain_View.DataSource = ViewState["BoxGvTrain_view"];
                GvTrain_View.DataBind();
                break;
            case "GvEmpShift":
                var GvEmpShift_view = (GridView)FvEdit_Detail.FindControl("GvEmpShift");
                GvEmpShift_view.EditIndex = -1;
                //GvEmpShift_view.DataBind();
                Select_Gv_EmpShift(int.Parse(ViewState["fv_emp_idx"].ToString()));
                break;
            case "GvAsset_View":
                var GvAsset_View = (GridView)FvEdit_Detail.FindControl("GvAsset_View");
                GvAsset_View.EditIndex = -1;
                Select_Gv_Asset(GvAsset_View, int.Parse(ViewState["fv_emp_idx"].ToString()));
                break;
            case "GvGurantee_View":
                var GvGurantee_View = (GridView)FvEdit_Detail.FindControl("GvGurantee_View");
                GvGurantee_View.EditIndex = -1;
                GvGurantee_View.DataSource = ViewState["Boxgv_Gurantee"];
                GvGurantee_View.DataBind();
                break;
        }
    }

    #endregion

    #region GvRowDeleting
    protected void gvRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvReference":

                var GvReference = (GridView)FvInsertEmp.FindControl("GvReference");
                var dsvsDelete = (DataSet)ViewState["vsTemp_Refer"];
                var drRefer = dsvsDelete.Tables[0].Rows;

                drRefer.RemoveAt(e.RowIndex);

                ViewState["vsTemp_Refer"] = dsvsDelete;
                GvReference.EditIndex = -1;
                GvReference.DataSource = ViewState["vsTemp_Refer"];
                GvReference.DataBind();

                break;

            case "GvReference_Edit":

                var GvReference_Edit = (GridView)FvEdit_Detail.FindControl("GvReference_Edit");
                var dsvsBu_ = (DataSet)ViewState["vsTemp_Refer_edit"];
                var drDr1_ = dsvsBu_.Tables[0].Rows;

                drDr1_.RemoveAt(e.RowIndex);

                ViewState["vsTemp_Refer_edit"] = dsvsBu_;
                GvReference_Edit.EditIndex = -1;
                GvReference_Edit.DataSource = ViewState["vsTemp_Refer_edit"];
                GvReference_Edit.DataBind();

                break;

            case "GvChildAdd":

                var GvChildAdd = (GridView)FvInsertEmp.FindControl("GvChildAdd");
                var dsvsBuyequipmentDelete = (DataSet)ViewState["vsTemp_Child"];
                var drDriving = dsvsBuyequipmentDelete.Tables[0].Rows;

                drDriving.RemoveAt(e.RowIndex);

                ViewState["vsTemp_Child"] = dsvsBuyequipmentDelete;
                GvChildAdd.EditIndex = -1;
                GvChildAdd.DataSource = ViewState["vsTemp_Child"];
                GvChildAdd.DataBind();

                break;

            case "GvChildAdd_Edit":

                var GvChildAdd_Edit = (GridView)FvEdit_Detail.FindControl("GvChildAdd_Edit");
                var dsvsBuyequipmentDelete_ = (DataSet)ViewState["vsTemp_Child_edit"];
                var drDriving_ = dsvsBuyequipmentDelete_.Tables[0].Rows;

                drDriving_.RemoveAt(e.RowIndex);

                ViewState["vsTemp_Child_edit"] = dsvsBuyequipmentDelete_;
                GvChildAdd_Edit.EditIndex = -1;
                GvChildAdd_Edit.DataSource = ViewState["vsTemp_Child_edit"];
                GvChildAdd_Edit.DataBind();

                break;

            case "GvPri":

                var GvPri = (GridView)FvInsertEmp.FindControl("GvPri");
                var dsvsBuyequipmentDelete1 = (DataSet)ViewState["vsTemp_Prior"];
                var drDriving1 = dsvsBuyequipmentDelete1.Tables[0].Rows;

                drDriving1.RemoveAt(e.RowIndex);

                ViewState["vsTemp_Prior"] = dsvsBuyequipmentDelete1;
                GvPri.EditIndex = -1;
                GvPri.DataSource = ViewState["vsTemp_Prior"];
                GvPri.DataBind();

                break;

            case "GvPri_Edit":

                var GvPri_Edit = (GridView)FvEdit_Detail.FindControl("GvPri_Edit");
                var dsvsBuyequipmentDelete1_ = (DataSet)ViewState["vsTemp_Prior_edit"];
                var drDriving1_ = dsvsBuyequipmentDelete1_.Tables[0].Rows;

                drDriving1_.RemoveAt(e.RowIndex);

                ViewState["vsTemp_Prior_edit"] = dsvsBuyequipmentDelete1_;
                GvPri_Edit.EditIndex = -1;
                GvPri_Edit.DataSource = ViewState["vsTemp_Prior_edit"];
                GvPri_Edit.DataBind();

                break;

            case "GvEducation":

                var GvEducation = (GridView)FvInsertEmp.FindControl("GvEducation");
                var dsvsBuyequipmentDelete2 = (DataSet)ViewState["vsTemp_Education"];
                var drDriving2 = dsvsBuyequipmentDelete2.Tables[0].Rows;

                drDriving2.RemoveAt(e.RowIndex);

                ViewState["vsTemp_Education"] = dsvsBuyequipmentDelete2;
                GvEducation.EditIndex = -1;
                GvEducation.DataSource = ViewState["vsTemp_Education"];
                GvEducation.DataBind();

                break;

            case "GvEducation_Edit":

                var GvEducation_Edit = (GridView)FvEdit_Detail.FindControl("GvEducation_Edit");
                var dsvsBuyequipmentDelete2_ = (DataSet)ViewState["vsTemp_Education_edit"];
                var drDriving2_ = dsvsBuyequipmentDelete2_.Tables[0].Rows;

                drDriving2_.RemoveAt(e.RowIndex);

                ViewState["vsTemp_Education_edit"] = dsvsBuyequipmentDelete2_;
                GvEducation_Edit.EditIndex = -1;
                GvEducation_Edit.DataSource = ViewState["vsTemp_Education_edit"];
                GvEducation_Edit.DataBind();

                break;

            case "GvTrain":

                var GvTrain = (GridView)FvInsertEmp.FindControl("GvTrain");
                var dsvsBuyequipmentDelete3 = (DataSet)ViewState["vsTemp_Train"];
                var drDriving3 = dsvsBuyequipmentDelete3.Tables[0].Rows;

                drDriving3.RemoveAt(e.RowIndex);

                ViewState["vsTemp_Train"] = dsvsBuyequipmentDelete3;
                GvTrain.EditIndex = -1;
                GvTrain.DataSource = ViewState["vsTemp_Train"];
                GvTrain.DataBind();

                break;

            case "GvTrain_Edit":

                var GvTrain_Edit = (GridView)FvEdit_Detail.FindControl("GvTrain_Edit");
                var dsvsBuyequipmentDelete3_ = (DataSet)ViewState["vsTemp_Train_edit"];
                var drDriving3_ = dsvsBuyequipmentDelete3_.Tables[0].Rows;

                drDriving3_.RemoveAt(e.RowIndex);

                ViewState["vsTemp_Train_edit"] = dsvsBuyequipmentDelete3_;
                GvTrain_Edit.EditIndex = -1;
                GvTrain_Edit.DataSource = ViewState["vsTemp_Train_edit"];
                GvTrain_Edit.DataBind();

                break;

            case "GvPosAdd":

                var GvPosAdd = (GridView)FvInsertEmp.FindControl("GvPosAdd");
                var Delete3 = (DataSet)ViewState["vsTemp_PosAdd"];
                var driving3 = Delete3.Tables[0].Rows;

                driving3.RemoveAt(e.RowIndex);

                ViewState["vsTemp_PosAdd"] = Delete3;
                GvPosAdd.EditIndex = -1;
                GvPosAdd.DataSource = ViewState["vsTemp_PosAdd"];
                GvPosAdd.DataBind();

                break;

            case "GvPosAdd_Edit":

                var GvPosAdd_Edit = (GridView)FvEdit_Detail.FindControl("GvPosAdd_Edit");
                var Delete4 = (DataSet)ViewState["vsTemp_PosAdd_edit"];
                var driving4 = Delete4.Tables[0].Rows;

                driving4.RemoveAt(e.RowIndex);

                ViewState["vsTemp_PosAdd_edit"] = Delete4;
                GvPosAdd_Edit.EditIndex = -1;
                GvPosAdd_Edit.DataSource = ViewState["vsTemp_PosAdd_edit"];
                GvPosAdd_Edit.DataBind();

                break;
            case "GvEmpShift":

                var GvEmpShift = (GridView)FvInsertEmp.FindControl("GvEmpShift");
                var Delete31 = (DataSet)ViewState["BoxGvEmpshiftAdd_View"];
                var driving32 = Delete31.Tables[0].Rows;

                driving32.RemoveAt(e.RowIndex);

                //ViewState["vsTemp_PosAdd"] = driving32;
                GvEmpShift.EditIndex = -1;
                Select_Gv_EmpShift(int.Parse(ViewState["fv_emp_idx"].ToString()));
                //GvEmpShift.DataSource = ViewState["BoxGvEmpshiftAdd_View"];
                //GvEmpShift.DataBind();
                break;
            case "GvEmpshiftTemp_edit":

                var GvEmpshiftTemp_edit_ = (GridView)FvEdit_Detail.FindControl("GvEmpshiftTemp_edit");
                var Delete321 = (DataSet)ViewState["vsTemp_Empshift_edit"];
                var driving322 = Delete321.Tables[0].Rows;

                driving322.RemoveAt(e.RowIndex);

                ViewState["vsTemp_Empshift_edit"] = Delete321;
                GvEmpshiftTemp_edit_.EditIndex = -1;
                GvEmpshiftTemp_edit_.DataSource = ViewState["vsTemp_Empshift_edit"];
                GvEmpshiftTemp_edit_.DataBind();

                break;
            case "GvAsset":
                var GvAsset_add = (GridView)FvInsertEmp.FindControl("GvAsset");
                var Dele_ = (DataSet)ViewState["vsTemp_Asset"];
                var dri_ = Dele_.Tables[0].Rows;

                dri_.RemoveAt(e.RowIndex);

                ViewState["vsTemp_Asset"] = Dele_;
                GvAsset_add.EditIndex = -1;
                GvAsset_add.DataSource = ViewState["vsTemp_Asset"];
                GvAsset_add.DataBind();
                break;

            case "GvAsset_edit":
                var GvAsset_edit = (GridView)FvEdit_Detail.FindControl("GvAsset_edit");
                var Dele_edit = (DataSet)ViewState["vsTemp_Asset_Edit"];
                var dri_Edit = Dele_edit.Tables[0].Rows;

                dri_Edit.RemoveAt(e.RowIndex);

                ViewState["vsTemp_Asset_Edit"] = Dele_edit;
                GvAsset_edit.EditIndex = -1;
                GvAsset_edit.DataSource = ViewState["vsTemp_Asset_Edit"];
                GvAsset_edit.DataBind();
                break;

            case "GvGurantee":

                var GvGurantee = (GridView)FvInsertEmp.FindControl("GvGurantee");
                var dsvsDelete_gu = (DataSet)ViewState["vsTemp_Gurantee"];
                var drGur = dsvsDelete_gu.Tables[0].Rows;

                drGur.RemoveAt(e.RowIndex);

                ViewState["vsTemp_Gurantee"] = dsvsDelete_gu;
                GvGurantee.EditIndex = -1;
                GvGurantee.DataSource = ViewState["vsTemp_Gurantee"];
                GvGurantee.DataBind();

                break;

            case "GvGurantee_View":
                var GvGurantee_View = (GridView)FvEdit_Detail.FindControl("GvGurantee_View");
                var Dele_gu_edit = (DataSet)ViewState["vsTemp_Gurantee"];
                var dri_gu_Edit = Dele_gu_edit.Tables[0].Rows;

                dri_gu_Edit.RemoveAt(e.RowIndex);

                GvGurantee_View.EditIndex = -1;
                GvGurantee_View.DataSource = ViewState["vsTemp_Gurantee"];
                GvGurantee_View.DataBind();
                break;

            case "GvGurantee_Edit":
                var GvGurantee_Edit = (GridView)FvEdit_Detail.FindControl("GvGurantee_Edit");
                var Dele_gu_edit_ = (DataSet)ViewState["vsTemp_Gurantee_edit"];
                var dri_gu_Edit_ = Dele_gu_edit_.Tables[0].Rows;

                dri_gu_Edit_.RemoveAt(e.RowIndex);

                GvGurantee_Edit.EditIndex = -1;
                GvGurantee_Edit.DataSource = ViewState["vsTemp_Gurantee_edit"];
                GvGurantee_Edit.DataBind();
                break;
        }

    }
    #endregion

    #endregion

    #region CheckBoxList_DataBound
    protected void CheckBoxList1_DataBound(object sender, EventArgs e)
    {
        var ddName = (CheckBoxList)sender;

        switch (ddName.ID)
        {
            case "YrChkBoxColumns":
                //var chkallcolumns = (CheckBox)Fv_Search_Emp_Report.FindControl("chkallcolumns");
                var YrChkBoxColumns = (CheckBoxList)Fv_Search_Emp_Report.FindControl("YrChkBoxColumns");
                for (int i = 0; i < YrChkBoxColumns.Items.Count; i++)
                {
                    YrChkBoxColumns.Items[i].Selected = true;
                }
                break;

            case "YrChkBox":
                var YrChkBox = (CheckBoxList)Fv_Search_Emp_Report.FindControl("YrChkBox");
                for (int i = 0; i < YrChkBox.Items.Count; i++)
                {
                    YrChkBox.Items[i].Selected = true;
                }
                break;
        }

    }
    #endregion

    #region ChkSelectedIndexChanged
    protected void chkSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (CheckBox)sender;

        switch (ddName.ID)
        {

            case "chkreposition":

                var chkreposition = (CheckBox)FvInsertEmp.FindControl("chkreposition");
                var _PanelRePositionCenAction = (UpdatePanel)FvInsertEmp.FindControl("_PanelRePositionCenAction");

                ClearDataInsertRePosition();
                

                if (chkreposition.Checked)
                {
                    _PanelRePositionCenAction.Visible = true;
                }
                else
                {
 
                    _PanelRePositionCenAction.Visible = false;
                   
                }
                break;
            case "chkrepositionView":

                var chkrepositionView = (CheckBox)FvEdit_Detail.FindControl("chkrepositionView");
                var _PanelRePositionCenActionEdit = (UpdatePanel)FvEdit_Detail.FindControl("_PanelRePositionCenActionEdit");

                ClearDataInsertEditRePosition();
                

                if (chkrepositionView.Checked)
                {
                    _PanelRePositionCenActionEdit.Visible = true;
                }
                else
                {
 
                    _PanelRePositionCenActionEdit.Visible = false;
                   
                }
                break;
            case "ckreference":

                var ckreference = (CheckBox)FvInsertEmp.FindControl("ckreference");
                var GvReferenceAdd = (GridView)FvInsertEmp.FindControl("GvReference");
                var BoxReference = (Panel)FvInsertEmp.FindControl("BoxReference");

                if (ckreference.Checked)
                {
                    ViewState["vsTemp_Refer"] = null;

                    var dsRefer = new DataSet();
                    dsRefer.Tables.Add("Refer_add");

                    dsRefer.Tables[0].Columns.Add("ref_fullname", typeof(String));
                    dsRefer.Tables[0].Columns.Add("ref_position", typeof(String));
                    dsRefer.Tables[0].Columns.Add("ref_relation", typeof(String));
                    dsRefer.Tables[0].Columns.Add("ref_tel", typeof(String));

                    ViewState["vsTemp_Refer"] = dsRefer;
                    GvReferenceAdd.DataSource = dsRefer.Tables[0];
                    GvReferenceAdd.DataBind();

                    BoxReference.Visible = true;
                }
                else
                {
                    ViewState["vsTemp_Refer"] = null;

                    var dsRefer = new DataSet();
                    dsRefer.Tables.Add("Refer_add");

                    dsRefer.Tables[0].Columns.Add("ref_fullname", typeof(String));
                    dsRefer.Tables[0].Columns.Add("ref_position", typeof(String));
                    dsRefer.Tables[0].Columns.Add("ref_relation", typeof(String));
                    dsRefer.Tables[0].Columns.Add("ref_tel", typeof(String));

                    ViewState["vsTemp_Refer"] = dsRefer;
                    GvReferenceAdd.DataSource = dsRefer.Tables[0];
                    GvReferenceAdd.DataBind();

                    BoxReference.Visible = false;
                    GvReferenceAdd.DataSource = null;
                    GvReferenceAdd.DataBind();
                }
                break;

            case "ckreference_edit":

                var ckreference_edit = (CheckBox)FvEdit_Detail.FindControl("ckreference_edit");
                var GvReference_Edit = (GridView)FvEdit_Detail.FindControl("GvReference_Edit");
                var BoxReference_edit = (Panel)FvEdit_Detail.FindControl("BoxReference");

                if (ckreference_edit.Checked)
                {
                    ViewState["vsTemp_Refer_edit"] = null;

                    var dsRefer = new DataSet();
                    dsRefer.Tables.Add("Refer_add");

                    dsRefer.Tables[0].Columns.Add("ref_fullname", typeof(String));
                    dsRefer.Tables[0].Columns.Add("ref_position", typeof(String));
                    dsRefer.Tables[0].Columns.Add("ref_relation", typeof(String));
                    dsRefer.Tables[0].Columns.Add("ref_tel", typeof(String));

                    ViewState["vsTemp_Refer_edit"] = dsRefer;
                    GvReference_Edit.DataSource = dsRefer.Tables[0];
                    GvReference_Edit.DataBind();

                    BoxReference_edit.Visible = true;
                }
                else
                {
                    ViewState["vsTemp_Refer_edit"] = null;

                    var dsRefer = new DataSet();
                    dsRefer.Tables.Add("Refer_add");

                    dsRefer.Tables[0].Columns.Add("ref_fullname", typeof(String));
                    dsRefer.Tables[0].Columns.Add("ref_position", typeof(String));
                    dsRefer.Tables[0].Columns.Add("ref_relation", typeof(String));
                    dsRefer.Tables[0].Columns.Add("ref_tel", typeof(String));

                    ViewState["vsTemp_Refer_edit"] = dsRefer;
                    GvReference_Edit.DataSource = dsRefer.Tables[0];
                    GvReference_Edit.DataBind();

                    BoxReference_edit.Visible = false;
                    GvReference_Edit.DataSource = null;
                    GvReference_Edit.DataBind();
                }
                break;

            case "chkchild":

                var chkchild = (CheckBox)FvInsertEmp.FindControl("chkchild");
                var GvChildAdd = (GridView)FvInsertEmp.FindControl("GvChildAdd");
                var BoxChild = (Panel)FvInsertEmp.FindControl("BoxChild");

                if (chkchild.Checked)
                {
                    ViewState["vsTemp_Child"] = null;

                    var dsEquipment = new DataSet();
                    dsEquipment.Tables.Add("Equipment");

                    dsEquipment.Tables[0].Columns.Add("Child_name", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Child_num", typeof(String));

                    ViewState["vsTemp_Child"] = dsEquipment;
                    GvChildAdd.DataSource = dsEquipment.Tables[0];
                    GvChildAdd.DataBind();

                    BoxChild.Visible = true;

                }
                else
                {
                    ViewState["vsTemp_Child"] = null;

                    var dsEquipment = new DataSet();
                    dsEquipment.Tables.Add("Equipment");

                    dsEquipment.Tables[0].Columns.Add("Child_name", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Child_num", typeof(String));

                    ViewState["vsTemp_Child"] = dsEquipment;
                    GvChildAdd.DataSource = dsEquipment.Tables[0];
                    GvChildAdd.DataBind();

                    BoxChild.Visible = false;
                    GvChildAdd.DataSource = null;
                    GvChildAdd.DataBind();
                }
                break;

            case "chkchild_edit":

                var chkchild_edit = (CheckBox)FvEdit_Detail.FindControl("chkchild_edit");
                var GvChildAdd_Edit = (GridView)FvEdit_Detail.FindControl("GvChildAdd_Edit");
                var BoxChild_edit = (Panel)FvEdit_Detail.FindControl("BoxChild");

                if (chkchild_edit.Checked)
                {
                    ViewState["vsTemp_Child_edit"] = null;

                    var dsEquipment = new DataSet();
                    dsEquipment.Tables.Add("Equipment_");

                    dsEquipment.Tables[0].Columns.Add("Child_name", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Child_num", typeof(String));

                    ViewState["vsTemp_Child_edit"] = dsEquipment;
                    GvChildAdd_Edit.DataSource = dsEquipment.Tables[0];
                    GvChildAdd_Edit.DataBind();

                    BoxChild_edit.Visible = true;

                }
                else
                {
                    ViewState["vsTemp_Child_edit"] = null;

                    var dsEquipment = new DataSet();
                    dsEquipment.Tables.Add("Equipment_");

                    dsEquipment.Tables[0].Columns.Add("Child_name", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Child_num", typeof(String));

                    ViewState["vsTemp_Child_edit"] = dsEquipment;
                    GvChildAdd_Edit.DataSource = dsEquipment.Tables[0];
                    GvChildAdd_Edit.DataBind();

                    BoxChild_edit.Visible = false;
                    GvChildAdd_Edit.DataSource = null;
                    GvChildAdd_Edit.DataBind();
                }
                break;

            case "chkorg":

                var chkorg = (CheckBox)FvInsertEmp.FindControl("chkorg");
                var GvPri = (GridView)FvInsertEmp.FindControl("GvPri");
                var BoxOrgOld = (Panel)FvInsertEmp.FindControl("BoxOrgOld");


                if (chkorg.Checked)
                {
                    ViewState["vsTemp_Prior"] = null;

                    var dsEquipment = new DataSet();
                    dsEquipment.Tables.Add("OrgOld");

                    dsEquipment.Tables[0].Columns.Add("Pri_Nameorg", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Pri_pos", typeof(String));
                    //dsEquipment.Tables[0].Columns.Add("Pri_worktime", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Pri_startdate", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Pri_resigndate", typeof(String));

                    ViewState["vsTemp_Prior"] = dsEquipment;
                    GvPri.DataSource = dsEquipment.Tables[0];
                    GvPri.DataBind();

                    BoxOrgOld.Visible = true;

                }
                else
                {
                    ViewState["vsTemp_Prior"] = null;

                    var dsEquipment = new DataSet();
                    dsEquipment.Tables.Add("OrgOld");

                    dsEquipment.Tables[0].Columns.Add("Pri_Nameorg", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Pri_pos", typeof(String));
                    //dsEquipment.Tables[0].Columns.Add("Pri_worktime", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Pri_startdate", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Pri_resigndate", typeof(String));

                    ViewState["vsTemp_Prior"] = dsEquipment;
                    GvPri.DataSource = dsEquipment.Tables[0];
                    GvPri.DataBind();

                    BoxOrgOld.Visible = false;
                    GvPri.DataSource = null;
                    GvPri.DataBind();
                }
                break;

            case "chkorg_edit":

                var chkorg_edit = (CheckBox)FvEdit_Detail.FindControl("chkorg_edit");
                var GvPri_Edit = (GridView)FvEdit_Detail.FindControl("GvPri_Edit");
                var BoxOrgOld_edit = (Panel)FvEdit_Detail.FindControl("BoxOrgOld");


                if (chkorg_edit.Checked)
                {
                    ViewState["vsTemp_Prior_edit"] = null;

                    var dsEquipment_ = new DataSet();
                    dsEquipment_.Tables.Add("OrgOld_edit");

                    dsEquipment_.Tables[0].Columns.Add("Pri_Nameorg", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Pri_pos", typeof(String));
                    //dsEquipment_.Tables[0].Columns.Add("Pri_worktime", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Pri_startdate", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Pri_resigndate", typeof(String));


                    ViewState["vsTemp_Prior_edit"] = dsEquipment_;
                    GvPri_Edit.DataSource = dsEquipment_.Tables[0];
                    GvPri_Edit.DataBind();

                    BoxOrgOld_edit.Visible = true;

                }
                else
                {
                    ViewState["vsTemp_Prior_edit"] = null;

                    var dsEquipment_ = new DataSet();
                    dsEquipment_.Tables.Add("OrgOld_edit");

                    dsEquipment_.Tables[0].Columns.Add("Pri_Nameorg", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Pri_pos", typeof(String));
                    //dsEquipment_.Tables[0].Columns.Add("Pri_worktime", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Pri_startdate", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Pri_resigndate", typeof(String));

                    ViewState["vsTemp_Prior_edit"] = dsEquipment_;
                    GvPri_Edit.DataSource = dsEquipment_.Tables[0];
                    GvPri_Edit.DataBind();

                    BoxOrgOld_edit.Visible = false;
                    GvPri_Edit.DataSource = null;
                    GvPri_Edit.DataBind();
                }


                break;

            case "ckeducation":

                var ckeducation = (CheckBox)FvInsertEmp.FindControl("ckeducation");
                var GvEducation = (GridView)FvInsertEmp.FindControl("GvEducation");
                var BoxEducation = (Panel)FvInsertEmp.FindControl("BoxEducation");

                if (ckeducation.Checked)
                {
                    ViewState["vsTemp_Education"] = null;

                    var dsEquipment = new DataSet();
                    dsEquipment.Tables.Add("Edu");

                    dsEquipment.Tables[0].Columns.Add("Edu_qualification_ID", typeof(int));
                    dsEquipment.Tables[0].Columns.Add("Edu_qualification", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Edu_name", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Edu_branch", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Edu_start", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Edu_end", typeof(String));

                    ViewState["vsTemp_Education"] = dsEquipment;
                    GvEducation.DataSource = dsEquipment.Tables[0];
                    GvEducation.DataBind();

                    BoxEducation.Visible = true;

                }
                else
                {
                    ViewState["vsTemp_Education"] = null;

                    var dsEquipment = new DataSet();
                    dsEquipment.Tables.Add("Edu");

                    dsEquipment.Tables[0].Columns.Add("Edu_qualification_ID", typeof(int));
                    dsEquipment.Tables[0].Columns.Add("Edu_qualification", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Edu_name", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Edu_branch", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Edu_start", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Edu_end", typeof(String));

                    ViewState["vsTemp_Education"] = dsEquipment;
                    GvEducation.DataSource = dsEquipment.Tables[0];
                    GvEducation.DataBind();

                    BoxEducation.Visible = false;
                    GvEducation.DataSource = null;
                    GvEducation.DataBind();
                }
                break;

            case "ckeducation_edit":

                var ckeducation_edit = (CheckBox)FvEdit_Detail.FindControl("ckeducation_edit");
                var GvEducation_Edit = (GridView)FvEdit_Detail.FindControl("GvEducation_Edit");
                var BoxEducation_edit = (Panel)FvEdit_Detail.FindControl("BoxEducation");

                if (ckeducation_edit.Checked)
                {
                    ViewState["vsTemp_Education_edit"] = null;

                    var dsEquipment_ = new DataSet();
                    dsEquipment_.Tables.Add("Edu_edit");

                    dsEquipment_.Tables[0].Columns.Add("Edu_qualification_ID", typeof(int));
                    dsEquipment_.Tables[0].Columns.Add("Edu_qualification", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Edu_name", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Edu_branch", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Edu_start", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Edu_end", typeof(String));

                    ViewState["vsTemp_Education_edit"] = dsEquipment_;
                    GvEducation_Edit.DataSource = dsEquipment_.Tables[0];
                    GvEducation_Edit.DataBind();

                    BoxEducation_edit.Visible = true;

                }
                else
                {
                    ViewState["vsTemp_Education_edit"] = null;

                    var dsEquipment_ = new DataSet();
                    dsEquipment_.Tables.Add("Edu_edit");

                    dsEquipment_.Tables[0].Columns.Add("Edu_qualification_ID", typeof(int));
                    dsEquipment_.Tables[0].Columns.Add("Edu_qualification", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Edu_name", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Edu_branch", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Edu_start", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Edu_end", typeof(String));

                    ViewState["vsTemp_Education_edit"] = dsEquipment_;
                    GvEducation_Edit.DataSource = dsEquipment_.Tables[0];
                    GvEducation_Edit.DataBind();

                    BoxEducation_edit.Visible = false;
                    GvEducation_Edit.DataSource = null;
                    GvEducation_Edit.DataBind();
                }


                break;

            case "cktraining":

                var cktraining = (CheckBox)FvInsertEmp.FindControl("cktraining");
                var GvTrain = (GridView)FvInsertEmp.FindControl("GvTrain");
                var BoxTrain = (Panel)FvInsertEmp.FindControl("BoxTrain");


                if (cktraining.Checked)
                {
                    ViewState["vsTemp_Train"] = null;

                    var dsEquipment = new DataSet();
                    dsEquipment.Tables.Add("Train");

                    dsEquipment.Tables[0].Columns.Add("Train_courses", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Train_date", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Train_assessment", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Train_enddate", typeof(String));

                    ViewState["vsTemp_Train"] = dsEquipment;
                    GvTrain.DataSource = dsEquipment.Tables[0];
                    GvTrain.DataBind();

                    BoxTrain.Visible = true;

                }
                else
                {
                    ViewState["vsTemp_Train"] = null;

                    var dsEquipment = new DataSet();
                    dsEquipment.Tables.Add("Train");

                    dsEquipment.Tables[0].Columns.Add("Train_courses", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Train_date", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Train_assessment", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Train_enddate", typeof(String));

                    ViewState["vsTemp_Train"] = dsEquipment;
                    GvTrain.DataSource = dsEquipment.Tables[0];
                    GvTrain.DataBind();

                    BoxTrain.Visible = false;
                    GvTrain.DataSource = null;
                    GvTrain.DataBind();
                }
                break;

            case "cktraining_edit":

                var cktraining_edit = (CheckBox)FvEdit_Detail.FindControl("cktraining_edit");
                var GvTrain_Edit = (GridView)FvEdit_Detail.FindControl("GvTrain_Edit");
                var BoxTrain_edit = (Panel)FvEdit_Detail.FindControl("BoxTrain");


                if (cktraining_edit.Checked)
                {
                    ViewState["vsTemp_Train_edit"] = null;

                    var dsEquipment_ = new DataSet();
                    dsEquipment_.Tables.Add("Train_edit");

                    dsEquipment_.Tables[0].Columns.Add("Train_courses", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Train_date", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Train_assessment", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Train_enddate", typeof(String));

                    ViewState["vsTemp_Train_edit"] = dsEquipment_;
                    GvTrain_Edit.DataSource = dsEquipment_.Tables[0];
                    GvTrain_Edit.DataBind();

                    BoxTrain_edit.Visible = true;

                }
                else
                {
                    ViewState["vsTemp_Train_edit"] = null;

                    var dsEquipment_ = new DataSet();
                    dsEquipment_.Tables.Add("Train_edit");

                    dsEquipment_.Tables[0].Columns.Add("Train_courses", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Train_date", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Train_assessment", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Train_enddate", typeof(String));

                    ViewState["vsTemp_Train_edit"] = dsEquipment_;
                    GvTrain_Edit.DataSource = dsEquipment_.Tables[0];
                    GvTrain_Edit.DataBind();

                    BoxTrain_edit.Visible = false;
                    GvTrain_Edit.DataSource = null;
                    GvTrain_Edit.DataBind();
                }


                break;

            case "chkposition":

                var chkposition = (CheckBox)FvInsertEmp.FindControl("chkposition");
                var GvPosAdd = (GridView)FvInsertEmp.FindControl("GvPosAdd");
                var BoxPosition = (Panel)FvInsertEmp.FindControl("BoxPosition");

                if (chkposition.Checked)
                {
                    ViewState["vsTemp_PosAdd"] = null;

                    var dsPosadd = new DataSet();
                    dsPosadd.Tables.Add("PosAdd");

                    dsPosadd.Tables[0].Columns.Add("OrgIDX_S", typeof(int));
                    dsPosadd.Tables[0].Columns.Add("OrgNameTH_S", typeof(String));
                    dsPosadd.Tables[0].Columns.Add("RDeptIDX_S", typeof(int));
                    dsPosadd.Tables[0].Columns.Add("DeptNameTH_S", typeof(String));
                    dsPosadd.Tables[0].Columns.Add("RSecIDX_S", typeof(int));
                    dsPosadd.Tables[0].Columns.Add("SecNameTH_S", typeof(String));
                    dsPosadd.Tables[0].Columns.Add("RPosIDX_S", typeof(int));
                    dsPosadd.Tables[0].Columns.Add("PosNameTH_S", typeof(String));

                    ViewState["vsTemp_PosAdd"] = dsPosadd;
                    GvPosAdd.DataSource = dsPosadd.Tables[0];
                    GvPosAdd.DataBind();

                    BoxPosition.Visible = true;

                }
                else
                {
                    ViewState["vsTemp_PosAdd"] = null;

                    var dsPosadd = new DataSet();
                    dsPosadd.Tables.Add("PosAdd");

                    dsPosadd.Tables[0].Columns.Add("OrgIDX_S", typeof(int));
                    dsPosadd.Tables[0].Columns.Add("OrgNameTH_S", typeof(String));
                    dsPosadd.Tables[0].Columns.Add("RDeptIDX_S", typeof(int));
                    dsPosadd.Tables[0].Columns.Add("DeptNameTH_S", typeof(String));
                    dsPosadd.Tables[0].Columns.Add("RSecIDX_S", typeof(int));
                    dsPosadd.Tables[0].Columns.Add("SecNameTH_S", typeof(String));
                    dsPosadd.Tables[0].Columns.Add("RPosIDX_S", typeof(int));
                    dsPosadd.Tables[0].Columns.Add("PosNameTH_S", typeof(String));

                    ViewState["vsTemp_PosAdd"] = dsPosadd;
                    GvPosAdd.DataSource = dsPosadd.Tables[0];
                    GvPosAdd.DataBind();

                    BoxPosition.Visible = false;
                    GvPosAdd.DataSource = null;
                    GvPosAdd.DataBind();
                }


                break;

            case "chkposition_edit":

                var chkposition_edit = (CheckBox)FvEdit_Detail.FindControl("chkposition_edit");
                var GvPosAdd_Edit = (GridView)FvEdit_Detail.FindControl("GvPosAdd_Edit");
                var BoxPosition_edit = (Panel)FvEdit_Detail.FindControl("BoxPosition_edit");

                if (chkposition_edit.Checked)
                {
                    ViewState["vsTemp_PosAdd_edit"] = null;

                    var dsPosadd = new DataSet();
                    dsPosadd.Tables.Add("PosAdd_edit");

                    dsPosadd.Tables[0].Columns.Add("OrgIDX_S", typeof(int));
                    dsPosadd.Tables[0].Columns.Add("OrgNameTH_S", typeof(String));
                    dsPosadd.Tables[0].Columns.Add("RDeptIDX_S", typeof(int));
                    dsPosadd.Tables[0].Columns.Add("DeptNameTH_S", typeof(String));
                    dsPosadd.Tables[0].Columns.Add("RSecIDX_S", typeof(int));
                    dsPosadd.Tables[0].Columns.Add("SecNameTH_S", typeof(String));
                    dsPosadd.Tables[0].Columns.Add("RPosIDX_S", typeof(int));
                    dsPosadd.Tables[0].Columns.Add("PosNameTH_S", typeof(String));

                    ViewState["vsTemp_PosAdd_edit"] = dsPosadd;
                    GvPosAdd_Edit.DataSource = dsPosadd.Tables[0];
                    GvPosAdd_Edit.DataBind();

                    BoxPosition_edit.Visible = true;

                }
                else
                {
                    ViewState["vsTemp_PosAdd_edit"] = null;

                    var dsPosadd = new DataSet();
                    dsPosadd.Tables.Add("PosAdd_edit");

                    dsPosadd.Tables[0].Columns.Add("OrgIDX_S", typeof(int));
                    dsPosadd.Tables[0].Columns.Add("OrgNameTH_S", typeof(String));
                    dsPosadd.Tables[0].Columns.Add("RDeptIDX_S", typeof(int));
                    dsPosadd.Tables[0].Columns.Add("DeptNameTH_S", typeof(String));
                    dsPosadd.Tables[0].Columns.Add("RSecIDX_S", typeof(int));
                    dsPosadd.Tables[0].Columns.Add("SecNameTH_S", typeof(String));
                    dsPosadd.Tables[0].Columns.Add("RPosIDX_S", typeof(int));
                    dsPosadd.Tables[0].Columns.Add("PosNameTH_S", typeof(String));

                    ViewState["vsTemp_PosAdd_edit"] = dsPosadd;
                    GvPosAdd_Edit.DataSource = dsPosadd.Tables[0];
                    GvPosAdd_Edit.DataBind();

                    BoxPosition_edit.Visible = false;
                    GvPosAdd_Edit.DataSource = null;
                    GvPosAdd_Edit.DataBind();
                }
                break;

            case "chkempshift_edit":

                var chkempshift_edit = (CheckBox)FvEdit_Detail.FindControl("chkempshift_edit");
                var GvEmpshiftTemp_edit = (GridView)FvEdit_Detail.FindControl("GvEmpshiftTemp_edit");
                var Boxempshift_edit = (Panel)FvEdit_Detail.FindControl("Boxempshift_edit");

                if (chkempshift_edit.Checked)
                {
                    ViewState["vsTemp_Empshift_edit"] = null;

                    var dsPosadd = new DataSet();
                    dsPosadd.Tables.Add("Empshift_edit");

                    dsPosadd.Tables[0].Columns.Add("OrgIDX_add", typeof(int));
                    dsPosadd.Tables[0].Columns.Add("OrgNameTH_S", typeof(String));
                    dsPosadd.Tables[0].Columns.Add("midx_add", typeof(int));
                    dsPosadd.Tables[0].Columns.Add("TypeWork_add", typeof(String));
                    dsPosadd.Tables[0].Columns.Add("emp_idx_create_add", typeof(int));

                    ViewState["vsTemp_Empshift_edit"] = dsPosadd;
                    GvEmpshiftTemp_edit.DataSource = dsPosadd.Tables[0];
                    GvEmpshiftTemp_edit.DataBind();

                    Boxempshift_edit.Visible = true;

                }
                else
                {
                    ViewState["vsTemp_Empshift_edit"] = null;

                    var dsPosadd = new DataSet();
                    dsPosadd.Tables.Add("Empshift_edit");

                    dsPosadd.Tables[0].Columns.Add("OrgIDX_add", typeof(int));
                    dsPosadd.Tables[0].Columns.Add("OrgNameTH_S", typeof(String));
                    dsPosadd.Tables[0].Columns.Add("midx_add", typeof(int));
                    dsPosadd.Tables[0].Columns.Add("TypeWork_add", typeof(String));
                    dsPosadd.Tables[0].Columns.Add("emp_idx_create_add", typeof(int));

                    ViewState["vsTemp_Empshift_edit"] = dsPosadd;
                    GvEmpshiftTemp_edit.DataSource = dsPosadd.Tables[0];
                    GvEmpshiftTemp_edit.DataBind();

                    Boxempshift_edit.Visible = false;
                    GvEmpshiftTemp_edit.DataSource = null;
                    GvEmpshiftTemp_edit.DataBind();
                }
                break;

            case "chkAddress":
                var chkAddress = (CheckBox)FvInsertEmp.FindControl("chkAddress");

                //ViewState["ddlamphoe_permanentaddress"] = 0;
                //ViewState["ddldistrict_present"] = 0;

                var BoxAddress_parent = (Panel)FvInsertEmp.FindControl("BoxAddress_parent");
                var txtaddress_present = (TextBox)BoxAddress_parent.FindControl("txtaddress_present");
                var ddlcountry_present = (DropDownList)BoxAddress_parent.FindControl("ddlcountry_present");
                var ddlprovince_present = (DropDownList)BoxAddress_parent.FindControl("ddlprovince_present");
                var ddlamphoe_present = (DropDownList)BoxAddress_parent.FindControl("ddlamphoe_present");
                var ddldistrict_present = (DropDownList)BoxAddress_parent.FindControl("ddldistrict_present");
                var txttelmobile_present = (TextBox)BoxAddress_parent.FindControl("txttelmobile_present");
                var txttelhome_present = (TextBox)BoxAddress_parent.FindControl("txttelhome_present");
                var txtzipcode_present = (TextBox)BoxAddress_parent.FindControl("txtzipcode_present");
                var txtemail_present = (TextBox)BoxAddress_parent.FindControl("txtemail_present");

                var txtaddress_permanentaddress = (TextBox)BoxAddress_parent.FindControl("txtaddress_permanentaddress");
                var ddlcountry_permanentaddress = (DropDownList)BoxAddress_parent.FindControl("ddlcountry_permanentaddress");
                var ddlprovince_permanentaddress = (DropDownList)BoxAddress_parent.FindControl("ddlprovince_permanentaddress");
                var ddlamphoe_permanentaddress = (DropDownList)BoxAddress_parent.FindControl("ddlamphoe_permanentaddress");
                var ddldistrict_permanentaddress = (DropDownList)BoxAddress_parent.FindControl("ddldistrict_permanentaddress");
                var txttelmobile_permanentaddress = (TextBox)BoxAddress_parent.FindControl("txttelmobile_permanentaddress");


                //ViewState["ddldistrict_present"] = ddldistrict_present.SelectedValue;

                if (chkAddress.Checked)
                {
                    BoxAddress_parent.Visible = false;
                    txtaddress_permanentaddress.Text = txtaddress_present.Text;
                    select_country(ddlcountry_permanentaddress);
                    ddlcountry_permanentaddress.SelectedValue = ddlcountry_present.SelectedValue;
                    select_province(ddlprovince_permanentaddress);
                    ddlprovince_permanentaddress.SelectedValue = ddlprovince_present.SelectedValue;
                    select_amphur(ddlamphoe_permanentaddress, int.Parse(ddlprovince_permanentaddress.SelectedValue));
                    ddlamphoe_permanentaddress.SelectedValue = ddlamphoe_present.SelectedValue;
                    select_dist(ddldistrict_permanentaddress, int.Parse(ddlprovince_permanentaddress.SelectedValue), int.Parse(ddlamphoe_permanentaddress.SelectedValue));
                    ddldistrict_permanentaddress.SelectedValue = ddldistrict_present.SelectedValue;
                    txttelmobile_permanentaddress.Text = txttelmobile_present.Text;

                    txtaddress_permanentaddress.Enabled = false;
                    ddlcountry_permanentaddress.Enabled = false;
                    ddlprovince_permanentaddress.Enabled = false;
                    ddlamphoe_permanentaddress.Enabled = false;
                    ddldistrict_permanentaddress.Enabled = false;
                    txttelmobile_permanentaddress.Enabled = false;
                }
                else
                {
                    BoxAddress_parent.Visible = true;
                    select_country(ddlcountry_permanentaddress);
                    select_province(ddlprovince_permanentaddress);

                    txtaddress_permanentaddress.Text = null;
                    txtaddress_permanentaddress.Enabled = true;
                    ddlcountry_permanentaddress.Enabled = true;
                    ddlprovince_permanentaddress.Enabled = true;
                    ddlamphoe_permanentaddress.Enabled = true;
                    ddlamphoe_permanentaddress.SelectedValue = null;
                    ddldistrict_permanentaddress.Enabled = true;
                    ddldistrict_permanentaddress.SelectedValue = null;
                    txttelmobile_permanentaddress.Text = null;
                    txttelmobile_permanentaddress.Enabled = true;
                }

                break;

            case "chkAddress_edit":
                var chkAddress_edit = (CheckBox)FvEdit_Detail.FindControl("chkAddress_edit");
                var BoxAddress_edit = (Panel)FvEdit_Detail.FindControl("BoxAddress_edit");
                var BoxAddress = (Panel)FvEdit_Detail.FindControl("BoxAddress");

                var txtaddress_present_edit = (TextBox)BoxAddress.FindControl("txtaddress_present");
                var ddlcountry_present_edit = (DropDownList)BoxAddress.FindControl("ddlcountry_present_edit");
                var ddlprovince_present_edit = (DropDownList)BoxAddress.FindControl("ddlprovince_present_edit");
                var ddlamphoe_present_edit = (DropDownList)BoxAddress.FindControl("ddlamphoe_present_edit");
                var ddldistrict_present_edit = (DropDownList)BoxAddress.FindControl("ddldistrict_present_edit");
                var txttelmobile_present_edit = (TextBox)BoxAddress.FindControl("txttelmobile_present");
                var txttelhome_present_edit = (TextBox)BoxAddress.FindControl("txttelhome_present");
                var txtzipcode_present_edit = (TextBox)BoxAddress.FindControl("txtzipcode_present");
                var txtemail_present_edit = (TextBox)BoxAddress.FindControl("txtemail_present");

                var txtaddress_permanentaddress_edit = (TextBox)BoxAddress_edit.FindControl("txtaddress_permanentaddress");
                var ddlcountry_permanentaddress_edit = (DropDownList)BoxAddress_edit.FindControl("ddlcountry_permanentaddress_edit");
                var ddlprovince_permanentaddress_edit = (DropDownList)BoxAddress_edit.FindControl("ddlprovince_permanentaddress_edit");
                var ddlamphoe_permanentaddress_edit = (DropDownList)BoxAddress_edit.FindControl("ddlamphoe_permanentaddress_edit");
                var ddldistrict_permanentaddress_edit = (DropDownList)BoxAddress_edit.FindControl("ddldistrict_permanentaddress_edit");
                var txttelmobile_permanentaddress_edit = (TextBox)BoxAddress_edit.FindControl("txttelmobile_permanentaddress");


                if (chkAddress_edit.Checked)
                {
                    BoxAddress_edit.Visible = false;
                    txtaddress_permanentaddress_edit.Text = txtaddress_present_edit.Text;
                    select_country(ddlcountry_permanentaddress_edit);
                    ddlcountry_permanentaddress_edit.SelectedValue = ddlcountry_present_edit.SelectedValue;
                    select_province(ddlprovince_permanentaddress_edit);
                    ddlprovince_permanentaddress_edit.SelectedValue = ddlprovince_present_edit.SelectedValue;
                    select_amphur(ddlamphoe_permanentaddress_edit, int.Parse(ddlprovince_permanentaddress_edit.SelectedValue));
                    ddlamphoe_permanentaddress_edit.SelectedValue = ddlamphoe_present_edit.SelectedValue;
                    select_dist(ddldistrict_permanentaddress_edit, int.Parse(ddlprovince_permanentaddress_edit.SelectedValue), int.Parse(ddlamphoe_permanentaddress_edit.SelectedValue));
                    ddldistrict_permanentaddress_edit.SelectedValue = ddldistrict_present_edit.SelectedValue;
                    txttelmobile_permanentaddress_edit.Text = txttelmobile_present_edit.Text;

                    txtaddress_permanentaddress_edit.Enabled = false;
                    ddlcountry_permanentaddress_edit.Enabled = false;
                    ddlprovince_permanentaddress_edit.Enabled = false;
                    ddlamphoe_permanentaddress_edit.Enabled = false;
                    ddldistrict_permanentaddress_edit.Enabled = false;
                    txttelmobile_permanentaddress_edit.Enabled = false;
                }
                else
                {
                    BoxAddress_edit.Visible = true;
                    select_country(ddlcountry_permanentaddress_edit);
                    select_province(ddlprovince_permanentaddress_edit);

                    txtaddress_permanentaddress_edit.Text = null;
                    txtaddress_permanentaddress_edit.Enabled = true;
                    ddlcountry_permanentaddress_edit.Enabled = true;
                    ddlprovince_permanentaddress_edit.Enabled = true;
                    ddlamphoe_permanentaddress_edit.Enabled = true;
                    ddlamphoe_permanentaddress_edit.SelectedValue = null;
                    ddldistrict_permanentaddress_edit.Enabled = true;
                    ddldistrict_permanentaddress_edit.SelectedValue = null;
                    txttelmobile_permanentaddress_edit.Text = null;
                    txttelmobile_permanentaddress_edit.Enabled = true;
                }
                break;

            case "chk_Affiliation_edit":
                var FvEdit_Detail_ = (FormView)ViewEdit.FindControl("FvEdit_Detail");
                var BoxGen = (Panel)FvEdit_Detail_.FindControl("BoxGen");
                var chk_Affiliation_edit = (CheckBox)BoxGen.FindControl("chk_Affiliation_edit");
                var box_ddlAffiliation = (Panel)BoxGen.FindControl("box_ddlAffiliation");

                if (chk_Affiliation_edit.Checked)
                {
                    box_ddlAffiliation.Visible = true;
                }
                else
                {
                    box_ddlAffiliation.Visible = false;
                }
                break;
            case "chk_Affiliation_add":
                var FvInsertEmp_ = (FormView)ViewAdd.FindControl("FvInsertEmp");
                var BoxGen_ = (Panel)FvInsertEmp_.FindControl("BoxGen");
                var chk_Affiliation_add = (CheckBox)BoxGen_.FindControl("chk_Affiliation_add");
                var box_ddlAffiliation_add = (Panel)BoxGen_.FindControl("box_ddlAffiliation_add");

                if (chk_Affiliation_add.Checked)
                {
                    box_ddlAffiliation_add.Visible = true;
                }
                else
                {
                    box_ddlAffiliation_add.Visible = false;
                }
                //chk_Affiliation_add.Focus();
                break;
            case "chkasset_edit":

                var chkasset_edit = (CheckBox)FvEdit_Detail.FindControl("chkasset_edit");
                var GvAsset_edit = (GridView)FvEdit_Detail.FindControl("GvAsset_edit");
                var BoxAsset_edit = (Panel)FvEdit_Detail.FindControl("BoxAsset_Edit");

                var txtholderdate_add = (TextBox)FvEdit_Detail.FindControl("txtholderdate_add");
                var txtnum_add = (TextBox)FvEdit_Detail.FindControl("txtnum_add");
                var txtassnumber_add = (TextBox)FvEdit_Detail.FindControl("txtassnumber_add");
                var txtdetail_add = (TextBox)FvEdit_Detail.FindControl("txtdetail_add");
                var ddlassettype_add = (DropDownList)FvEdit_Detail.FindControl("ddlassettype_add");
                var ddlunittype_add = (DropDownList)FvEdit_Detail.FindControl("ddlunittype_add");


                if (chkasset_edit.Checked)
                {
                    ViewState["vsTemp_Asset_Edit"] = null;

                    var dsAsset_null = new DataSet();
                    dsAsset_null.Tables.Add("AssetAdd");
                    dsAsset_null.Tables[0].Columns.Add("asidx", typeof(int));
                    dsAsset_null.Tables[0].Columns.Add("as_name", typeof(String));
                    dsAsset_null.Tables[0].Columns.Add("m0_unidx", typeof(int));
                    dsAsset_null.Tables[0].Columns.Add("m0_name", typeof(String));
                    dsAsset_null.Tables[0].Columns.Add("u0_number", typeof(String));
                    dsAsset_null.Tables[0].Columns.Add("u0_asset_no", typeof(String));
                    dsAsset_null.Tables[0].Columns.Add("u0_holderdate", typeof(String));
                    dsAsset_null.Tables[0].Columns.Add("u0_detail", typeof(String));
                    ViewState["vsTemp_Asset_Edit"] = dsAsset_null;

                    GvAsset_edit.DataSource = dsAsset_null.Tables[0];
                    GvAsset_edit.DataBind();

                    BoxAsset_edit.Visible = true;
                    txtholderdate_add.Text = null;
                    txtnum_add.Text = null;
                    txtassnumber_add.Text = null;
                    txtdetail_add.Text = null;
                    Select_asset_type(ddlassettype_add);
                    Select_unit_type(ddlunittype_add);

                    //ddlassettype_add.SelectedValue = "0";
                    //ddlunittype_add.SelectedValue = "0";
                    //712
                }
                else
                {
                    ViewState["vsTemp_Asset_Edit"] = null;

                    var dsAsset_null = new DataSet();
                    dsAsset_null.Tables.Add("AssetAdd");
                    dsAsset_null.Tables[0].Columns.Add("asidx", typeof(int));
                    dsAsset_null.Tables[0].Columns.Add("as_name", typeof(String));
                    dsAsset_null.Tables[0].Columns.Add("m0_unidx", typeof(int));
                    dsAsset_null.Tables[0].Columns.Add("m0_name", typeof(String));
                    dsAsset_null.Tables[0].Columns.Add("u0_number", typeof(String));
                    dsAsset_null.Tables[0].Columns.Add("u0_asset_no", typeof(String));
                    dsAsset_null.Tables[0].Columns.Add("u0_holderdate", typeof(String));
                    dsAsset_null.Tables[0].Columns.Add("u0_detail", typeof(String));
                    ViewState["vsTemp_Asset_Edit"] = dsAsset_null;
                    GvAsset_edit.DataSource = dsAsset_null.Tables[0];
                    GvAsset_edit.DataBind();

                    BoxAsset_edit.Visible = false;
                    GvAsset_edit.DataSource = null;
                    GvAsset_edit.DataBind();

                    txtholderdate_add.Text = null;
                    txtnum_add.Text = null;
                    txtassnumber_add.Text = null;
                    txtdetail_add.Text = null;
                }

                /*var dsAsset_null = new DataSet();
                dsAsset_null.Tables.Add("AssetAdd");
                dsAsset_null.Tables[0].Columns.Add("asidx", typeof(int));
                dsAsset_null.Tables[0].Columns.Add("as_name", typeof(String));
                dsAsset_null.Tables[0].Columns.Add("m0_unidx", typeof(int));
                dsAsset_null.Tables[0].Columns.Add("m0_name", typeof(String));
                dsAsset_null.Tables[0].Columns.Add("u0_number", typeof(String));
                dsAsset_null.Tables[0].Columns.Add("u0_asset_no", typeof(String));
                dsAsset_null.Tables[0].Columns.Add("u0_holderdate", typeof(String));
                dsAsset_null.Tables[0].Columns.Add("u0_detail", typeof(String));
                ViewState["vsTemp_Asset"] = dsAsset_null;*/
                break;

            case "chkallcolumns":

                var chkallcolumns = (CheckBox)Fv_Search_Emp_Report.FindControl("chkallcolumns");
                var YrChkBoxColumns = (CheckBoxList)Fv_Search_Emp_Report.FindControl("YrChkBoxColumns");
                /*var GvAsset_edit = (GridView)FvEdit_Detail.FindControl("GvAsset_edit");
                var BoxAsset_edit = (Panel)FvEdit_Detail.FindControl("BoxAsset_Edit");*/
                bool c = (sender as CheckBox).Checked;

                for (int i = 0; i < YrChkBoxColumns.Items.Count; i++)
                {
                    YrChkBoxColumns.Items[i].Selected = c;
                }

                break;

            case "chkAll":

                var chkAll = (CheckBox)Fv_Search_Emp_Report.FindControl("chkAll");
                var YrChkBox = (CheckBoxList)Fv_Search_Emp_Report.FindControl("YrChkBox");
                /*var GvAsset_edit = (GridView)FvEdit_Detail.FindControl("GvAsset_edit");
                var BoxAsset_edit = (Panel)FvEdit_Detail.FindControl("BoxAsset_Edit");*/
                bool c1 = (sender as CheckBox).Checked;

                for (int i = 0; i < YrChkBox.Items.Count; i++)
                {
                    YrChkBox.Items[i].Selected = c1;
                }

                break;

            case "chkgurantee":

                var chkgurantee = (CheckBox)FvInsertEmp.FindControl("chkgurantee");
                var GvGurantee = (GridView)FvInsertEmp.FindControl("GvGurantee");
                var BoxGurantee = (Panel)FvInsertEmp.FindControl("BoxGurantee");

                if (chkgurantee.Checked)
                {
                    ViewState["vsTemp_Gurantee"] = null;

                    var dsgur = new DataSet();
                    dsgur.Tables.Add("Guran_add");

                    dsgur.Tables[0].Columns.Add("gu_fullname", typeof(String));
                    dsgur.Tables[0].Columns.Add("gu_startdate", typeof(String));
                    dsgur.Tables[0].Columns.Add("gu_relation", typeof(String));
                    dsgur.Tables[0].Columns.Add("gu_tel", typeof(String));

                    ViewState["vsTemp_Gurantee"] = dsgur;
                    GvGurantee.DataSource = dsgur.Tables[0];
                    GvGurantee.DataBind();

                    BoxGurantee.Visible = true;
                }
                else
                {
                    ViewState["vsTemp_Gurantee"] = null;

                    var dsgur = new DataSet();
                    dsgur.Tables.Add("Guran_add");

                    dsgur.Tables[0].Columns.Add("gu_fullname", typeof(String));
                    dsgur.Tables[0].Columns.Add("gu_startdate", typeof(String));
                    dsgur.Tables[0].Columns.Add("gu_relation", typeof(String));
                    dsgur.Tables[0].Columns.Add("gu_tel", typeof(String));

                    ViewState["vsTemp_Gurantee"] = dsgur;
                    GvGurantee.DataSource = dsgur.Tables[0];
                    GvGurantee.DataBind();

                    BoxGurantee.Visible = false;
                    GvGurantee.DataSource = null;
                    GvGurantee.DataBind();
                }
                break;

            case "ckgurantee_edit":

                var ckgurantee_edit = (CheckBox)FvEdit_Detail.FindControl("ckgurantee_edit");
                var GvGurantee_Edit = (GridView)FvEdit_Detail.FindControl("GvGurantee_Edit");
                var BoxGurantee_edit = (Panel)FvEdit_Detail.FindControl("BoxGurantee");

                if (ckgurantee_edit.Checked)
                {
                    ViewState["vsTemp_Gurantee_edit"] = null;

                    var dsgur = new DataSet();
                    dsgur.Tables.Add("Guran_edit");

                    dsgur.Tables[0].Columns.Add("gu_fullname", typeof(String));
                    dsgur.Tables[0].Columns.Add("gu_startdate", typeof(String));
                    dsgur.Tables[0].Columns.Add("gu_relation", typeof(String));
                    dsgur.Tables[0].Columns.Add("gu_tel", typeof(String));

                    ViewState["vsTemp_Gurantee_edit"] = dsgur;
                    GvGurantee_Edit.DataSource = dsgur.Tables[0];
                    GvGurantee_Edit.DataBind();

                    BoxGurantee_edit.Visible = true;
                }
                else
                {
                    ViewState["vsTemp_Gurantee_edit"] = null;

                    var dsgur = new DataSet();
                    dsgur.Tables.Add("Guran_edit");

                    dsgur.Tables[0].Columns.Add("gu_fullname", typeof(String));
                    dsgur.Tables[0].Columns.Add("gu_startdate", typeof(String));
                    dsgur.Tables[0].Columns.Add("gu_relation", typeof(String));
                    dsgur.Tables[0].Columns.Add("gu_tel", typeof(String));

                    ViewState["vsTemp_Gurantee_edit"] = dsgur;
                    GvGurantee_Edit.DataSource = dsgur.Tables[0];
                    GvGurantee_Edit.DataBind();

                    BoxGurantee_edit.Visible = false;
                    GvGurantee_Edit.DataSource = null;
                    GvGurantee_Edit.DataBind();
                }
                break;
            case "chkempout":
                var FvEdit_Detail_1 = (FormView)ViewEdit.FindControl("FvEdit_Detail");
                var Boxresign_date = (Panel)FvEdit_Detail_1.FindControl("Boxresign_date");
                var chkempout = (CheckBox)Boxresign_date.FindControl("chkempout");
                var ddlresign_comment = (DropDownList)Boxresign_date.FindControl("ddlresign_comment");

                if (chkempout.Checked)
                {
                    Boxresign_date.Visible = true;
                    Select_ddl_resgin_comment(ddlresign_comment);
                }
                else
                {
                    Boxresign_date.Visible = false;
                }
                break;

        }
    }
    #endregion

    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (DropDownList)sender;

        // Insert Employee //

        //page 2
        DropDownList ddlorg = (DropDownList)FvInsertEmp.FindControl("ddlorg");
        DropDownList ddldep = (DropDownList)FvInsertEmp.FindControl("ddldep");
        DropDownList ddlsec = (DropDownList)FvInsertEmp.FindControl("ddlsec");
        DropDownList ddlpos = (DropDownList)FvInsertEmp.FindControl("ddlpos");

        DropDownList ddlorg_shift = (DropDownList)FvInsertEmp.FindControl("ddlorg_shift");
        DropDownList ddlshift_add = (DropDownList)FvInsertEmp.FindControl("ddlshift_add");

        //page 2

        //page3
        DropDownList ddlcountry_present = (DropDownList)FvInsertEmp.FindControl("ddlcountry_present");
        DropDownList ddlprovince_present = (DropDownList)FvInsertEmp.FindControl("ddlprovince_present");
        DropDownList ddlamphoe_present = (DropDownList)FvInsertEmp.FindControl("ddlamphoe_present");
        DropDownList ddldistrict_present = (DropDownList)FvInsertEmp.FindControl("ddldistrict_present");

        DropDownList ddlprovince_permanentaddress = (DropDownList)FvInsertEmp.FindControl("ddlprovince_permanentaddress");
        DropDownList ddlamphoe_permanentaddress = (DropDownList)FvInsertEmp.FindControl("ddlamphoe_permanentaddress");
        DropDownList ddldistrict_permanentaddress = (DropDownList)FvInsertEmp.FindControl("ddldistrict_permanentaddress");

        //page3


        //page 5
        DropDownList ddlapprove1 = (DropDownList)FvInsertEmp.FindControl("ddlapprove1");
        DropDownList ddlapprove2 = (DropDownList)FvInsertEmp.FindControl("ddlapprove2");


        DropDownList ddlorg_add = (DropDownList)FvInsertEmp.FindControl("ddlorg_add");
        DropDownList ddldep_add = (DropDownList)FvInsertEmp.FindControl("ddldep_add");
        DropDownList ddlsec_add = (DropDownList)FvInsertEmp.FindControl("ddlsec_add");
        DropDownList ddlpos_add = (DropDownList)FvInsertEmp.FindControl("ddlpos_add");

        //page 5

        DropDownList ddlnation = (DropDownList)FvInsertEmp.FindControl("ddlnation");
        TextBox txtidcard = (TextBox)FvInsertEmp.FindControl("txtidcard");
        TextBox txtsecurityid = (TextBox)FvInsertEmp.FindControl("txtsecurityid");

        // Insert Employee //
        
        
        // Edit Employee //
        
        DropDownList ddlorg_add_edit = (DropDownList)FvEdit_Detail.FindControl("ddlorg_add_edit");
        DropDownList ddldep_add_edit = (DropDownList)FvEdit_Detail.FindControl("ddldep_add_edit");
        DropDownList ddlsec_add_edit = (DropDownList)FvEdit_Detail.FindControl("ddlsec_add_edit");
        DropDownList ddlpos_add_edit = (DropDownList)FvEdit_Detail.FindControl("ddlpos_add_edit");

        DropDownList ddlprovince_present_edit = (DropDownList)FvEdit_Detail.FindControl("ddlprovince_present_edit");
        DropDownList ddlamphoe_present_edit = (DropDownList)FvEdit_Detail.FindControl("ddlamphoe_present_edit");
        DropDownList ddldistrict_present_edit = (DropDownList)FvEdit_Detail.FindControl("ddldistrict_present_edit");
        Label lbl_prov_edit = (Label)FvEdit_Detail.FindControl("lbl_prov_edit");
        Label lbl_amp_edit = (Label)FvEdit_Detail.FindControl("lbl_amp_edit");
        Label lbl_dist_edit = (Label)FvEdit_Detail.FindControl("lbl_dist_edit");

        DropDownList ddl_prefix_th = (DropDownList)FvInsertEmp.FindControl("ddl_prefix_th");
        DropDownList ddl_prefix_en = (DropDownList)FvInsertEmp.FindControl("ddl_prefix_en");
        DropDownList ddlmilitary = (DropDownList)FvInsertEmp.FindControl("ddlmilitary");

        DropDownList ddl_prefix_th_edit = (DropDownList)FvEdit_Detail.FindControl("ddl_prefix_th_edit");
        DropDownList ddl_prefix_en_edit = (DropDownList)FvEdit_Detail.FindControl("ddl_prefix_en_edit");
        DropDownList ddlmilitary_edit = (DropDownList)FvEdit_Detail.FindControl("ddlmilitary_edit");

       

        
        DropDownList ddlmidxEdit_add = (DropDownList)FvInsertEmp.FindControl("ddlmidxEdit_Select");
        DropDownList ddlOrganizationEdit_add = (DropDownList)FvInsertEmp.FindControl("ddlOrganizationEdit_Select");

        DropDownList ddlorg_add_group = (DropDownList)FvEdit_Detail.FindControl("ddlorg_add_group");
        DropDownList ddlshift_add_group = (DropDownList)FvEdit_Detail.FindControl("ddlshift_add_group");

        DropDownList ddlprovince_permanentaddress_edit = (DropDownList)FvEdit_Detail.FindControl("ddlprovince_permanentaddress_edit");
        DropDownList ddlamphoe_permanentaddress_edit = (DropDownList)FvEdit_Detail.FindControl("ddlamphoe_permanentaddress_edit");
        DropDownList ddldistrict_permanentaddress_edit = (DropDownList)FvEdit_Detail.FindControl("ddldistrict_permanentaddress_edit");

        DropDownList ddlnation_edit = (DropDownList)FvEdit_Detail.FindControl("ddlnation_edit");
        TextBox txtidcard_edit = (TextBox)FvEdit_Detail.FindControl("txtidcard");
        TextBox txtsecurityid_edit = (TextBox)FvEdit_Detail.FindControl("txtsecurityid");

        DropDownList ddlresign_comment = (DropDownList)FvEdit_Detail.FindControl("ddlresign_comment");
        Panel box_txt_comment = (Panel)FvEdit_Detail.FindControl("box_txt_comment");



        // Edit Employee //

       

        

        DropDownList ddlorg_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlorg_rp");
        DropDownList ddldep_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddldep_rp");
        DropDownList ddlsec_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlsec_rp");
        DropDownList ddlpos_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlpos_rp");
        DropDownList ddldatetype = (DropDownList)Fv_Search_Emp_Report.FindControl("ddldatetype");
        Panel BoxSearch_Date = (Panel)Fv_Search_Emp_Report.FindControl("BoxSearch_Date");

        DropDownList ddlorg_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddlorg_s");
        DropDownList ddldep_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddldep_s");
        DropDownList ddlsec_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddlsec_s");
        DropDownList ddlpos_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddlpos_s");

        DropDownList ddlOrganizationEdit_edit_ = (DropDownList)Fv_Search_Emp_Index.FindControl("ddlOrganizationEdit_edit_");
        DropDownList ddlmidxEdit_Select_edit_ = (DropDownList)Fv_Search_Emp_Index.FindControl("ddlmidxEdit_Select");

       

       

        DropDownList ddlOrganizationEdit_ = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlOrganizationEdit_");
        DropDownList ddlmidxEdit_Select = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlmidxEdit_Select");

        DropDownList ddlreport_type = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlreport_type");
        Panel pn_chart_type = (Panel)Fv_Search_Emp_Report.FindControl("pn_chart_type");

        DropDownList ddlchart_type = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlchart_type");
        Panel pn_emp_search = (Panel)Fv_Search_Emp_Report.FindControl("pn_emp_search");
        Panel pn_ddl_dept = (Panel)Fv_Search_Emp_Report.FindControl("pn_ddl_dept");
        Panel pn_ddl_sec_pos = (Panel)Fv_Search_Emp_Report.FindControl("pn_ddl_sec_pos");

        Panel pn_type_date_search = (Panel)Fv_Search_Emp_Report.FindControl("pn_type_date_search");
        Panel pn_empstatus_search = (Panel)Fv_Search_Emp_Report.FindControl("pn_empstatus_search");
        Panel pn_jobgrade_search = (Panel)Fv_Search_Emp_Report.FindControl("pn_jobgrade_search");
        Panel pn_age_search = (Panel)Fv_Search_Emp_Report.FindControl("pn_age_search");
        Panel pn_education_search = (Panel)Fv_Search_Emp_Report.FindControl("pn_education_search");
        Panel pn_location_search = (Panel)Fv_Search_Emp_Report.FindControl("pn_location_search");
        Panel pn_race_search = (Panel)Fv_Search_Emp_Report.FindControl("pn_race_search");
        Panel pn_ch_colums = (Panel)Fv_Search_Emp_Report.FindControl("pn_ch_colums");
        Panel pn_leave_colums = (Panel)Fv_Search_Emp_Report.FindControl("pn_leave_colums");
        TextBox txtdatestart = (TextBox)Fv_Search_Emp_Report.FindControl("txtdatestart");
        TextBox txtdateend = (TextBox)Fv_Search_Emp_Report.FindControl("txtdateend");
        Panel pn_grouptype_search = (Panel)Fv_Search_Emp_Report.FindControl("pn_grouptype_search");

       

        //ddl re position
        DropDownList ddlOrganization = (DropDownList)FvInsertEmp.FindControl("ddlOrganization");
        DropDownList ddlWorkGroup = (DropDownList)FvInsertEmp.FindControl("ddlWorkGroup");
        DropDownList ddlLineWork = (DropDownList)FvInsertEmp.FindControl("ddlLineWork");
        DropDownList ddlDepartment = (DropDownList)FvInsertEmp.FindControl("ddlDepartment");
        DropDownList ddlSection = (DropDownList)FvInsertEmp.FindControl("ddlSection");
        DropDownList ddlPosition = (DropDownList)FvInsertEmp.FindControl("ddlPosition");

        DropDownList ddlapprove1_cen = (DropDownList)FvInsertEmp.FindControl("ddlapprove1_cen");
        DropDownList ddlapprove2_cen = (DropDownList)FvInsertEmp.FindControl("ddlapprove2_cen");

        //ddl re position
        DropDownList ddlOrganizationAction = (DropDownList)FvInsertEmp.FindControl("ddlOrganizationAction");
        DropDownList ddlWorkGroupAction = (DropDownList)FvInsertEmp.FindControl("ddlWorkGroupAction");
        DropDownList ddlLineWorkAction = (DropDownList)FvInsertEmp.FindControl("ddlLineWorkAction");
        DropDownList ddlDepartmentAction = (DropDownList)FvInsertEmp.FindControl("ddlDepartmentAction");
        DropDownList ddlSectionAction = (DropDownList)FvInsertEmp.FindControl("ddlSectionAction");
        DropDownList ddlPositionAction = (DropDownList)FvInsertEmp.FindControl("ddlPositionAction");

         //ddl re position
        DropDownList ddlOrganizationAction_Edit = (DropDownList)FvEdit_Detail.FindControl("ddlOrganizationAction_Edit");
        DropDownList ddlWorkGroupAction_Edit = (DropDownList)FvEdit_Detail.FindControl("ddlWorkGroupAction_Edit");
        DropDownList ddlLineWorkAction_Edit = (DropDownList)FvEdit_Detail.FindControl("ddlLineWorkAction_Edit");
        DropDownList ddlDepartmentAction_Edit = (DropDownList)FvEdit_Detail.FindControl("ddlDepartmentAction_Edit");
        DropDownList ddlSectionAction_Edit = (DropDownList)FvEdit_Detail.FindControl("ddlSectionAction_Edit");
        DropDownList ddlPositionAction_Edit = (DropDownList)FvEdit_Detail.FindControl("ddlPositionAction_Edit");

        
        // var ddNameOrg = (DropDownList)sender;
        // var row = (GridViewRow)ddNameOrg.NamingContainer;

        
       

        switch (ddName.ID)
        {
            case "ddlorg":
                select_dep(ddldep, int.Parse(ddlorg.SelectedValue));
                break;
            case "ddldep":
                select_sec(ddlsec, int.Parse(ddlorg.SelectedValue), int.Parse(ddldep.SelectedValue));
                break;
            case "ddlsec":
                select_pos(ddlpos, int.Parse(ddlorg.SelectedValue), int.Parse(ddldep.SelectedValue), int.Parse(ddlsec.SelectedValue));
                break;
            case "ddlpos":
                select_approve_add(ddlapprove1, int.Parse(ddlorg.SelectedValue), int.Parse(ddldep.SelectedValue), int.Parse(ddlsec.SelectedValue), int.Parse(ddlpos.SelectedValue));
                select_approve_add(ddlapprove2, int.Parse(ddlorg.SelectedValue), int.Parse(ddldep.SelectedValue), int.Parse(ddlsec.SelectedValue), int.Parse(ddlpos.SelectedValue));
                break;

            case "ddlchart_type":

                if (ddlchart_type.SelectedValue == "1") //education
                {
                    pn_emp_search.Visible = false;
                    pn_type_date_search.Visible = false;
                    //pn_empstatus_search.Visible = false;
                    pn_jobgrade_search.Visible = true;
                    pn_age_search.Visible = false;
                    BoxSearch_Date.Visible = false;
                    pn_education_search.Visible = true;
                    pn_ch_colums.Visible = false;
                    pn_race_search.Visible = false;
                    pn_leave_colums.Visible = false;
                    pn_ddl_dept.Visible = true;
                    pn_ddl_sec_pos.Visible = true;
                }
                else if (ddlchart_type.SelectedValue == "2") //sex
                {
                    pn_emp_search.Visible = false;
                    pn_type_date_search.Visible = false;
                    //pn_empstatus_search.Visible = false;
                    pn_jobgrade_search.Visible = false;
                    pn_age_search.Visible = false;
                    BoxSearch_Date.Visible = false;
                    pn_education_search.Visible = false;
                    pn_ch_colums.Visible = false;
                    pn_race_search.Visible = false;
                    pn_leave_colums.Visible = false;
                    pn_ddl_dept.Visible = true;
                    pn_ddl_sec_pos.Visible = true;
                }
                else if (ddlchart_type.SelectedValue == "3") //age
                {
                    pn_emp_search.Visible = false;
                    pn_type_date_search.Visible = false;
                    //pn_empstatus_search.Visible = false;
                    pn_jobgrade_search.Visible = false;
                    pn_age_search.Visible = true;
                    BoxSearch_Date.Visible = false;
                    pn_education_search.Visible = false;
                    pn_ch_colums.Visible = false;
                    pn_race_search.Visible = false;
                    pn_leave_colums.Visible = false;
                    pn_ddl_dept.Visible = true;
                    pn_ddl_sec_pos.Visible = true;
                }
                else if (ddlchart_type.SelectedValue == "4") //car
                {
                    pn_emp_search.Visible = false;
                    pn_type_date_search.Visible = false;
                    //pn_empstatus_search.Visible = false;
                    pn_jobgrade_search.Visible = false;
                    pn_age_search.Visible = false;
                    BoxSearch_Date.Visible = true;
                    pn_education_search.Visible = false;
                    txtdatestart.CssClass = "form-control from-date-datepicker-time";
                    txtdateend.CssClass = "form-control from-date-datepicker-time";
                    pn_ch_colums.Visible = false;
                    pn_race_search.Visible = false;
                    pn_leave_colums.Visible = false;
                    pn_ddl_dept.Visible = true;
                    pn_ddl_sec_pos.Visible = true;
                }
                else if (ddlchart_type.SelectedValue == "5") //count employee
                {
                    pn_emp_search.Visible = false;
                    pn_type_date_search.Visible = false;
                    //pn_empstatus_search.Visible = false;
                    pn_jobgrade_search.Visible = true;
                    pn_age_search.Visible = false;
                    BoxSearch_Date.Visible = false;
                    pn_education_search.Visible = false;
                    pn_ch_colums.Visible = false;
                    pn_race_search.Visible = false;
                    pn_leave_colums.Visible = false;
                    pn_ddl_dept.Visible = false;
                    pn_ddl_sec_pos.Visible = false;

                    ddlorg_rp.SelectedValue = "0";
                    ddldep_s.SelectedValue = "0";
                    ddlsec_s.SelectedValue = "0";
                }
                else if (ddlchart_type.SelectedValue == "6") //leave
                {
                    pn_emp_search.Visible = false;
                    pn_type_date_search.Visible = false;
                    //pn_empstatus_search.Visible = false;
                    pn_jobgrade_search.Visible = false;
                    pn_age_search.Visible = false;
                    BoxSearch_Date.Visible = true;
                    pn_education_search.Visible = false;
                    txtdatestart.CssClass = "form-control from-date-datepicker-time";
                    txtdateend.CssClass = "form-control from-date-datepicker-time";
                    pn_ch_colums.Visible = false;
                    pn_race_search.Visible = false;
                    pn_leave_colums.Visible = false;
                    pn_ddl_dept.Visible = false;
                    pn_ddl_sec_pos.Visible = false;

                    ddlorg_rp.SelectedValue = "0";
                    ddldep_s.SelectedValue = "0";
                    ddlsec_s.SelectedValue = "0";
                }
                else if (ddlchart_type.SelectedValue == "7") //race
                {
                    pn_emp_search.Visible = false;
                    pn_type_date_search.Visible = false;
                    //pn_empstatus_search.Visible = false;
                    pn_jobgrade_search.Visible = false;
                    pn_age_search.Visible = false;
                    BoxSearch_Date.Visible = false;
                    pn_education_search.Visible = false;
                    pn_ch_colums.Visible = false;
                    pn_race_search.Visible = true;
                    pn_leave_colums.Visible = false;
                    pn_ddl_dept.Visible = true;
                    pn_ddl_sec_pos.Visible = true;
                }
                else if (ddlchart_type.SelectedValue == "8") //new employee
                {
                    pn_emp_search.Visible = false;
                    pn_type_date_search.Visible = false;
                    //pn_empstatus_search.Visible = false;
                    pn_jobgrade_search.Visible = true;
                    pn_age_search.Visible = false;
                    BoxSearch_Date.Visible = true;
                    pn_education_search.Visible = false;
                    txtdatestart.CssClass = "form-control from-date-datepicker-time";
                    txtdateend.CssClass = "form-control from-date-datepicker-time";
                    pn_ch_colums.Visible = false;
                    pn_race_search.Visible = false;
                    pn_leave_colums.Visible = false;
                    pn_ddl_dept.Visible = true;
                    pn_ddl_sec_pos.Visible = true;
                }
                else
                {
                    pn_emp_search.Visible = false;
                    pn_type_date_search.Visible = false;
                    //pn_empstatus_search.Visible = false;
                    pn_jobgrade_search.Visible = false;
                    pn_age_search.Visible = false;
                    BoxSearch_Date.Visible = false;
                    pn_education_search.Visible = false;
                    pn_ch_colums.Visible = false;
                    pn_leave_colums.Visible = false;
                    pn_ddl_dept.Visible = true;
                    pn_ddl_sec_pos.Visible = true;
                }

                break;

            case "ddlorg_shift":
                select_empshift(ddlshift_add, int.Parse(ddlorg_shift.SelectedValue));
                break;
            case "ddlorg_add_group":
                select_empshift(ddlshift_add_group, int.Parse(ddlorg_add_group.SelectedValue));
                break;
            case "ddlOrganizationEdit_":
                var ddNameOrg1_ = (DropDownList)sender;
                var row1_ = (GridViewRow)ddNameOrg1_.NamingContainer;

                var ddlOrg_edit = (DropDownList)row1_.FindControl("ddlOrganizationEdit_");
                var ddlMidx_edit = (DropDownList)row1_.FindControl("ddlmidxEdit_Select");
                select_empshift(ddlMidx_edit, int.Parse(ddlOrg_edit.SelectedValue));
                break;

            case "ddlorg_add":
                select_dep(ddldep_add, int.Parse(ddlorg_add.SelectedValue));
                break;
            case "ddldep_add":
                select_sec(ddlsec_add, int.Parse(ddlorg_add.SelectedValue), int.Parse(ddldep_add.SelectedValue));
                break;
            case "ddlsec_add":
                select_pos(ddlpos_add, int.Parse(ddlorg_add.SelectedValue), int.Parse(ddldep_add.SelectedValue), int.Parse(ddlsec_add.SelectedValue));
                break;

            case "ddlorg_add_edit":
                select_dep(ddldep_add_edit, int.Parse(ddlorg_add_edit.SelectedValue));
                break;
            case "ddldep_add_edit":
                select_sec(ddlsec_add_edit, int.Parse(ddlorg_add_edit.SelectedValue), int.Parse(ddldep_add_edit.SelectedValue));
                break;
            case "ddlsec_add_edit":
                select_pos(ddlpos_add_edit, int.Parse(ddlorg_add_edit.SelectedValue), int.Parse(ddldep_add_edit.SelectedValue), int.Parse(ddlsec_add_edit.SelectedValue));
                break;

            case "ddlOrganizationEdit_Select":
                var ddNameOrg = (DropDownList)sender;
                var row = (GridViewRow)ddNameOrg.NamingContainer;

                var ddlOrg = (DropDownList)row.FindControl("ddlOrganizationEdit_Select");
                var ddlDep = (DropDownList)row.FindControl("ddlDepartmentEdit_Select");
                var ddlSec = (DropDownList)row.FindControl("ddlSectionEdit_Select");
                var ddlPos = (DropDownList)row.FindControl("ddlPositionEdit_Select");

                select_dep(ddlDep, int.Parse(ddlOrg.SelectedValue));
                break;
            case "ddlDepartmentEdit_Select":
                var ddNameOrg_1 = (DropDownList)sender;
                var row_1 = (GridViewRow)ddNameOrg_1.NamingContainer;

                var ddlOrg_1 = (DropDownList)row_1.FindControl("ddlOrganizationEdit_Select");
                var ddlDep_1 = (DropDownList)row_1.FindControl("ddlDepartmentEdit_Select");
                var ddlSec_1 = (DropDownList)row_1.FindControl("ddlSectionEdit_Select");
                var ddlPos_1 = (DropDownList)row_1.FindControl("ddlPositionEdit_Select");

                select_sec(ddlSec_1, int.Parse(ddlOrg_1.SelectedValue), int.Parse(ddlDep_1.SelectedValue));
                break;
            case "ddlSectionEdit_Select":
                var ddNameOrg_2 = (DropDownList)sender;
                var row_2 = (GridViewRow)ddNameOrg_2.NamingContainer;

                var ddlOrg_2 = (DropDownList)row_2.FindControl("ddlOrganizationEdit_Select");
                var ddlDep_2 = (DropDownList)row_2.FindControl("ddlDepartmentEdit_Select");
                var ddlSec_2 = (DropDownList)row_2.FindControl("ddlSectionEdit_Select");
                var ddlPos_2 = (DropDownList)row_2.FindControl("ddlPositionEdit_Select");

                select_pos(ddlPos_2, int.Parse(ddlOrg_2.SelectedValue), int.Parse(ddlDep_2.SelectedValue), int.Parse(ddlSec_2.SelectedValue));
                break;

            case "ddlprovince_present":
                select_amphur(ddlamphoe_present, int.Parse(ddlprovince_present.SelectedValue));
                break;
            case "ddlamphoe_present":
                select_dist(ddldistrict_present, int.Parse(ddlprovince_present.SelectedValue), int.Parse(ddlamphoe_present.SelectedValue));
                break;

            case "ddlprovince_permanentaddress":
                select_amphur(ddlamphoe_permanentaddress, int.Parse(ddlprovince_permanentaddress.SelectedValue));
                break;
            case "ddlamphoe_permanentaddress":
                select_dist(ddldistrict_permanentaddress, int.Parse(ddlprovince_permanentaddress.SelectedValue), int.Parse(ddlamphoe_permanentaddress.SelectedValue));
                break;

            case "ddlorg_s":
                select_dep(ddldep_s, int.Parse(ddlorg_s.SelectedValue));
                break;
            case "ddldep_s":
                select_sec(ddlsec_s, int.Parse(ddlorg_s.SelectedValue), int.Parse(ddldep_s.SelectedValue));
                break;
            case "ddlsec_s":
                select_pos(ddlpos_s, int.Parse(ddlorg_s.SelectedValue), int.Parse(ddldep_s.SelectedValue), int.Parse(ddlsec_s.SelectedValue));
                break;

            case "ddlorg_rp":
                select_dep(ddldep_rp, int.Parse(ddlorg_rp.SelectedValue));

                CheckBoxList YrChkBox = (CheckBoxList)Fv_Search_Emp_Report.FindControl("YrChkBox");
                CheckBox chkAll = (CheckBox)Fv_Search_Emp_Report.FindControl("chkAll");

                if (int.Parse(ddlchart_type.SelectedValue) == 5 || int.Parse(ddlchart_type.SelectedValue) == 6)
                {
                    if (int.Parse(ddlorg_rp.SelectedValue) != 0)
                    {
                        chkAll.Checked = true;
                        select_dep_checkbox(YrChkBox, int.Parse(ddlorg_rp.SelectedValue));
                        pn_leave_colums.Visible = true;
                    }
                    else
                    {
                        pn_leave_colums.Visible = false;
                    }
                }
                break;
            case "ddldep_rp":
                select_sec(ddlsec_rp, int.Parse(ddlorg_rp.SelectedValue), int.Parse(ddldep_rp.SelectedValue));
                break;
            case "ddlsec_rp":
                select_pos(ddlpos_rp, int.Parse(ddlorg_rp.SelectedValue), int.Parse(ddldep_rp.SelectedValue), int.Parse(ddlsec_rp.SelectedValue));
                break;

            case "ddldatetype":
                if (ddldatetype.SelectedValue == "1" || ddldatetype.SelectedValue == "2" || ddldatetype.SelectedValue == "3" || ddldatetype.SelectedValue == "4")
                {
                    BoxSearch_Date.Visible = true;
                }
                else
                {
                    BoxSearch_Date.Visible = false;
                }
                break;
            case "ddlprovince_present_edit":
                select_amphur(ddlamphoe_present_edit, int.Parse(ddlprovince_present_edit.SelectedValue));
                break;
            case "ddlamphoe_present_edit":
                select_dist(ddldistrict_present_edit, int.Parse(ddlprovince_present_edit.SelectedValue), int.Parse(ddlamphoe_present_edit.SelectedValue));
                break;

            case "ddlprovince_permanentaddress_edit":
                select_amphur(ddlamphoe_permanentaddress_edit, int.Parse(ddlprovince_permanentaddress_edit.SelectedValue));
                break;
            case "ddlamphoe_permanentaddress_edit":
                select_dist(ddldistrict_permanentaddress_edit, int.Parse(ddlprovince_permanentaddress_edit.SelectedValue), int.Parse(ddlamphoe_permanentaddress_edit.SelectedValue));
                break;
            case "ddl_prefix_th":
                if (ddl_prefix_th.SelectedValue == "1") //นาย
                {
                    ddl_prefix_en.SelectedValue = "1";
                    ddlmilitary.SelectedValue = "1";
                }
                else if (ddl_prefix_th.SelectedValue == "2") //นางสาว
                {
                    ddl_prefix_en.SelectedValue = "2";
                    ddlmilitary.SelectedValue = "3";
                }
                else if (ddl_prefix_th.SelectedValue == "3") //นาง
                {
                    ddl_prefix_en.SelectedValue = "3";
                    ddlmilitary.SelectedValue = "3";
                }
                else
                {
                    ddl_prefix_en.SelectedValue = "4";
                    ddlmilitary.SelectedValue = "3";
                }
                break;
            case "ddl_prefix_en":
                if (ddl_prefix_en.SelectedValue == "1") //นาย
                {
                    ddl_prefix_th.SelectedValue = "1";
                    ddlmilitary.SelectedValue = "1";
                }
                else if (ddl_prefix_en.SelectedValue == "2") //นางสาว
                {
                    ddl_prefix_th.SelectedValue = "2";
                    ddlmilitary.SelectedValue = "3";
                }
                else if (ddl_prefix_en.SelectedValue == "3") //นาง
                {
                    ddl_prefix_th.SelectedValue = "3";
                    ddlmilitary.SelectedValue = "3";
                }
                else
                {
                    ddl_prefix_th.SelectedValue = "4";
                    ddlmilitary.SelectedValue = "3";
                }
                break;
            case "ddl_prefix_th_edit":
                if (ddl_prefix_th_edit.SelectedValue == "1") //นาย
                {
                    ddl_prefix_en_edit.SelectedValue = "1";
                    ddlmilitary_edit.SelectedValue = "1";
                }
                else if (ddl_prefix_th_edit.SelectedValue == "2") //นางสาว
                {
                    ddl_prefix_en_edit.SelectedValue = "2";
                    ddlmilitary_edit.SelectedValue = "3";
                }
                else if (ddl_prefix_th_edit.SelectedValue == "3") //นาง
                {
                    ddl_prefix_en_edit.SelectedValue = "3";
                    ddlmilitary_edit.SelectedValue = "3";
                }
                else
                {
                    ddl_prefix_en_edit.SelectedValue = "4";
                    ddlmilitary_edit.SelectedValue = "3";
                }
                break;
            case "ddl_prefix_en_edit":
                if (ddl_prefix_en_edit.SelectedValue == "1") //นาย
                {
                    ddl_prefix_th_edit.SelectedValue = "1";
                    ddlmilitary_edit.SelectedValue = "1";
                }
                else if (ddl_prefix_en_edit.SelectedValue == "2") //นางสาว
                {
                    ddl_prefix_th_edit.SelectedValue = "2";
                    ddlmilitary_edit.SelectedValue = "3";
                }
                else if (ddl_prefix_en_edit.SelectedValue == "3") //นาง
                {
                    ddl_prefix_th_edit.SelectedValue = "3";
                    ddlmilitary_edit.SelectedValue = "3";
                }
                else
                {
                    ddl_prefix_th_edit.SelectedValue = "4";
                    ddlmilitary_edit.SelectedValue = "3";
                }
                break;

            case "ddlnation":
                if (ddlnation.SelectedValue == "177")
                {
                    txtsecurityid.Text = txtidcard.Text;
                }
                else
                {
                    ddlmilitary.SelectedValue = "3";
                }
                break;
            case "ddlnation_edit":
                if (ddlnation_edit.SelectedValue == "177")
                {
                    txtsecurityid_edit.Text = txtidcard_edit.Text;
                }
                else
                {
                    ddlmilitary_edit.SelectedValue = "3";
                }
                break;

            case "ddlorgidx":
                select_dep(ddlrdeptidx, int.Parse(ddlorgidx.SelectedValue));
                break;
            case "ddlrdeptidx":
                select_sec(ddlsecidx, int.Parse(ddlorgidx.SelectedValue), int.Parse(ddlrdeptidx.SelectedValue));
                break;

            case "ddlSearchDate":

                if (int.Parse(ddlSearchDate.SelectedValue) == 3)
                {
                    AddEndDate.Enabled = true;
                }
                else
                {
                    AddEndDate.Enabled = false;
                    AddEndDate.Text = string.Empty;
                }

                break;

            case "ddlreport_type":
                if (ddlreport_type.SelectedValue == "2")
                {
                    pn_chart_type.Visible = true;
                    ddlchart_type.Visible = true;
                    pn_grouptype_search.Visible = false;
                }
                else
                {
                    ddlchart_type.Visible = false;
                    pn_chart_type.Visible = false;
                    pn_emp_search.Visible = true;
                    pn_type_date_search.Visible = true;
                    //pn_empstatus_search.Visible = false;
                    pn_jobgrade_search.Visible = true;
                    pn_age_search.Visible = false;
                    BoxSearch_Date.Visible = false;
                    pn_education_search.Visible = false;
                    txtdatestart.CssClass = "form-control from-date-datepicker";
                    txtdateend.CssClass = "form-control from-date-datepicker";
                    pn_ch_colums.Visible = true;
                    pn_grouptype_search.Visible = true;
                }
                break;
            case "ddlresign_comment":
                if (int.Parse(ddlresign_comment.SelectedValue) == 6) //etc
                {
                    box_txt_comment.Visible = true;
                }
                else
                {
                    box_txt_comment.Visible = false;
                }
                break;

            case "ddllenght_empcode":
                if (int.Parse(ddllenght_empcode.SelectedValue) == 3)
                {
                    txtempcode1.Enabled = true;
                }
                else
                {
                    txtempcode1.Enabled = false;
                    txtempcode1.Text = string.Empty;
                }
                break;
             case "ddlOrganization":
                if (_funcTool.convertToInt (ddlOrganization.SelectedValue) > 0) {
                    search_cen_master_detail _orgSelected = new search_cen_master_detail ();
                    _orgSelected.s_org_idx = ddlOrganization.SelectedValue;
                    _data_cen_master = getMasterList ("4", _orgSelected);
                    _funcTool.setDdlData (ddlWorkGroup, _data_cen_master.cen_wg_list_m0, "wg_name_th", "wg_idx");
                } else {
                    ddlWorkGroup.Items.Clear ();
                }

                ddlLineWork.Items.Clear ();
                ddlDepartment.Items.Clear ();
                ddlSection.Items.Clear ();
                ddlPosition.Items.Clear ();
                ddlWorkGroup.Items.Insert (0, new ListItem ("--- เลือกกลุ่มงาน ---", "0"));
                ddlLineWork.Items.Insert (0, new ListItem ("--- เลือกสายงาน ---", "0"));
                ddlDepartment.Items.Insert (0, new ListItem ("--- เลือกฝ่าย ---", "0"));
                ddlSection.Items.Insert (0, new ListItem ("--- เลือกแผนก ---", "0"));
                ddlPosition.Items.Insert (0, new ListItem ("--- เลือกตำแหน่ง ---", "0"));
                break;
            case "ddlWorkGroup":
                if (_funcTool.convertToInt (ddlWorkGroup.SelectedValue) > 0) {
                    search_cen_master_detail _wgSelected = new search_cen_master_detail ();
                    _wgSelected.s_org_idx = ddlOrganization.SelectedValue;
                    _wgSelected.s_wg_idx = ddlWorkGroup.SelectedValue;
                    _data_cen_master = getMasterList ("5", _wgSelected);
                    _funcTool.setDdlData (ddlLineWork, _data_cen_master.cen_lw_list_m0, "lw_name_th", "lw_idx");
                } else {
                    ddlLineWork.Items.Clear ();
                }

                ddlDepartment.Items.Clear ();
                ddlSection.Items.Clear ();
                ddlPosition.Items.Clear ();
                ddlLineWork.Items.Insert (0, new ListItem ("--- เลือกสายงาน ---", "0"));
                ddlDepartment.Items.Insert (0, new ListItem ("--- เลือกฝ่าย ---", "0"));
                ddlSection.Items.Insert (0, new ListItem ("--- เลือกแผนก ---", "0"));
                ddlPosition.Items.Insert (0, new ListItem ("--- เลือกตำแหน่ง ---", "0"));
                break;
            case "ddlLineWork":
                if (_funcTool.convertToInt (ddlLineWork.SelectedValue) > 0) {
                    search_cen_master_detail _lgSelected = new search_cen_master_detail ();
                    _lgSelected.s_org_idx = ddlOrganization.SelectedValue;
                    _lgSelected.s_wg_idx = ddlWorkGroup.SelectedValue;
                    _lgSelected.s_lw_idx = ddlLineWork.SelectedValue;
                    _data_cen_master = getMasterList ("6", _lgSelected);
                    _funcTool.setDdlData (ddlDepartment, _data_cen_master.cen_dept_list_m0, "dept_name_th", "dept_idx");
                } else {
                    ddlDepartment.Items.Clear ();
                }

                ddlSection.Items.Clear ();
                ddlPosition.Items.Clear ();
                ddlDepartment.Items.Insert (0, new ListItem ("--- เลือกฝ่าย ---", "0"));
                ddlSection.Items.Insert (0, new ListItem ("--- เลือกแผนก ---", "0"));
                ddlPosition.Items.Insert (0, new ListItem ("--- เลือกตำแหน่ง ---", "0"));
                break;
            case "ddlDepartment":
                if (_funcTool.convertToInt (ddlDepartment.SelectedValue) > 0) {
                    search_cen_master_detail _deptSelected = new search_cen_master_detail ();
                    _deptSelected.s_org_idx = ddlOrganization.SelectedValue;
                    _deptSelected.s_wg_idx = ddlWorkGroup.SelectedValue;
                    _deptSelected.s_lw_idx = ddlLineWork.SelectedValue;
                    _deptSelected.s_dept_idx = ddlDepartment.SelectedValue;
                    _data_cen_master = getMasterList ("7", _deptSelected);
                    _funcTool.setDdlData (ddlSection, _data_cen_master.cen_sec_list_m0, "sec_name_th", "sec_idx");
                } else {
                    ddlSection.Items.Clear ();
                }

                ddlPosition.Items.Clear ();
                ddlSection.Items.Insert (0, new ListItem ("--- เลือกแผนก ---", "0"));
                ddlPosition.Items.Insert (0, new ListItem ("--- เลือกตำแหน่ง ---", "0"));
                break;
            case "ddlSection":
                if (_funcTool.convertToInt (ddlSection.SelectedValue) > 0) {
                    search_cen_master_detail _secSelected = new search_cen_master_detail ();
                    _secSelected.s_org_idx = ddlOrganization.SelectedValue;
                    _secSelected.s_wg_idx = ddlWorkGroup.SelectedValue;
                    _secSelected.s_lw_idx = ddlLineWork.SelectedValue;
                    _secSelected.s_dept_idx = ddlDepartment.SelectedValue;
                    _secSelected.s_sec_idx = ddlSection.SelectedValue;
                    _data_cen_master = getMasterList ("8", _secSelected);
                    _funcTool.setDdlData (ddlPosition, _data_cen_master.cen_pos_list_m0, "pos_name_th", "pos_idx");
                } else {
                    ddlPosition.Items.Clear ();
                }

                ddlPosition.Items.Insert (0, new ListItem ("--- เลือกตำแหน่ง ---", "0"));
                break;
            case "ddlPosition":
                // getApproveCen(ddlapprove1_cen, int.Parse(ddlOrganization.SelectedValue), int.Parse(ddlDepartment.SelectedValue), int.Parse(ddlSection.SelectedValue), int.Parse(ddlPosition.SelectedValue));
                // getApproveCen(ddlapprove2_cen, int.Parse(ddlOrganization.SelectedValue), int.Parse(ddlDepartment.SelectedValue), int.Parse(ddlSection.SelectedValue), int.Parse(ddlPosition.SelectedValue));
                
                break;

             case "ddlOrganizationAction":
                if (_funcTool.convertToInt (ddlOrganizationAction.SelectedValue) > 0) {
                    search_cen_master_detail _orgSelected = new search_cen_master_detail ();
                    _orgSelected.s_org_idx = ddlOrganizationAction.SelectedValue;
                    _data_cen_master = getMasterList ("4", _orgSelected);
                    _funcTool.setDdlData (ddlWorkGroupAction, _data_cen_master.cen_wg_list_m0, "wg_name_th", "wg_idx");
                } else {
                    ddlWorkGroupAction.Items.Clear ();
                }

                ddlLineWorkAction.Items.Clear ();
                ddlDepartmentAction.Items.Clear ();
                ddlSectionAction.Items.Clear ();
                ddlPositionAction.Items.Clear ();
                ddlWorkGroupAction.Items.Insert (0, new ListItem ("--- เลือกกลุ่มงาน ---", "0"));
                ddlLineWorkAction.Items.Insert (0, new ListItem ("--- เลือกสายงาน ---", "0"));
                ddlDepartmentAction.Items.Insert (0, new ListItem ("--- เลือกฝ่าย ---", "0"));
                ddlSectionAction.Items.Insert (0, new ListItem ("--- เลือกแผนก ---", "0"));
                ddlPositionAction.Items.Insert (0, new ListItem ("--- เลือกตำแหน่ง ---", "0"));
                break;
            case "ddlWorkGroupAction":
                if (_funcTool.convertToInt (ddlWorkGroupAction.SelectedValue) > 0) {
                    search_cen_master_detail _wgSelected = new search_cen_master_detail ();
                    _wgSelected.s_org_idx = ddlOrganizationAction.SelectedValue;
                    _wgSelected.s_wg_idx = ddlWorkGroupAction.SelectedValue;
                    _data_cen_master = getMasterList ("5", _wgSelected);
                    _funcTool.setDdlData (ddlLineWorkAction, _data_cen_master.cen_lw_list_m0, "lw_name_th", "lw_idx");
                } else {
                    ddlLineWorkAction.Items.Clear ();
                }

                ddlDepartmentAction.Items.Clear ();
                ddlSectionAction.Items.Clear ();
                ddlPositionAction.Items.Clear ();
                ddlLineWorkAction.Items.Insert (0, new ListItem ("--- เลือกสายงาน ---", "0"));
                ddlDepartmentAction.Items.Insert (0, new ListItem ("--- เลือกฝ่าย ---", "0"));
                ddlSectionAction.Items.Insert (0, new ListItem ("--- เลือกแผนก ---", "0"));
                ddlPositionAction.Items.Insert (0, new ListItem ("--- เลือกตำแหน่ง ---", "0"));
                break;
            case "ddlLineWorkAction":
                if (_funcTool.convertToInt (ddlLineWorkAction.SelectedValue) > 0) {
                    search_cen_master_detail _lgSelected = new search_cen_master_detail ();
                    _lgSelected.s_org_idx = ddlOrganizationAction.SelectedValue;
                    _lgSelected.s_wg_idx = ddlWorkGroupAction.SelectedValue;
                    _lgSelected.s_lw_idx = ddlLineWorkAction.SelectedValue;
                    _data_cen_master = getMasterList ("6", _lgSelected);
                    _funcTool.setDdlData (ddlDepartmentAction, _data_cen_master.cen_dept_list_m0, "dept_name_th", "dept_idx");
                } else {
                    ddlDepartmentAction.Items.Clear ();
                }

                ddlSectionAction.Items.Clear ();
                ddlPositionAction.Items.Clear ();
                ddlDepartmentAction.Items.Insert (0, new ListItem ("--- เลือกฝ่าย ---", "0"));
                ddlSectionAction.Items.Insert (0, new ListItem ("--- เลือกแผนก ---", "0"));
                ddlPositionAction.Items.Insert (0, new ListItem ("--- เลือกตำแหน่ง ---", "0"));
                break;
            case "ddlDepartmentAction":
                if (_funcTool.convertToInt (ddlDepartmentAction.SelectedValue) > 0) {
                    search_cen_master_detail _deptSelected = new search_cen_master_detail ();
                    _deptSelected.s_org_idx = ddlOrganizationAction.SelectedValue;
                    _deptSelected.s_wg_idx = ddlWorkGroupAction.SelectedValue;
                    _deptSelected.s_lw_idx = ddlLineWorkAction.SelectedValue;
                    _deptSelected.s_dept_idx = ddlDepartmentAction.SelectedValue;
                    _data_cen_master = getMasterList ("7", _deptSelected);
                    _funcTool.setDdlData (ddlSectionAction, _data_cen_master.cen_sec_list_m0, "sec_name_th", "sec_idx");
                } else {
                    ddlSectionAction.Items.Clear ();
                }

                ddlPositionAction.Items.Clear ();
                ddlSectionAction.Items.Insert (0, new ListItem ("--- เลือกแผนก ---", "0"));
                ddlPositionAction.Items.Insert (0, new ListItem ("--- เลือกตำแหน่ง ---", "0"));
                break;
            case "ddlSectionAction":
                if (_funcTool.convertToInt (ddlSectionAction.SelectedValue) > 0) {
                    search_cen_master_detail _secSelected = new search_cen_master_detail ();
                    _secSelected.s_org_idx = ddlOrganizationAction.SelectedValue;
                    _secSelected.s_wg_idx = ddlWorkGroupAction.SelectedValue;
                    _secSelected.s_lw_idx = ddlLineWorkAction.SelectedValue;
                    _secSelected.s_dept_idx = ddlDepartmentAction.SelectedValue;
                    _secSelected.s_sec_idx = ddlSectionAction.SelectedValue;
                    _data_cen_master = getMasterList ("8", _secSelected);
                    _funcTool.setDdlData (ddlPositionAction, _data_cen_master.cen_pos_list_m0, "pos_name_th", "pos_idx");
                } else {
                    ddlPositionAction.Items.Clear ();
                }

                ddlPositionAction.Items.Insert (0, new ListItem ("--- เลือกตำแหน่ง ---", "0"));
                break;
            case "ddlOrganizationAction_Edit":
                if (_funcTool.convertToInt (ddlOrganizationAction_Edit.SelectedValue) > 0) {
                    search_cen_master_detail _orgSelected = new search_cen_master_detail ();
                    _orgSelected.s_org_idx = ddlOrganizationAction_Edit.SelectedValue;
                    _data_cen_master = getMasterList ("4", _orgSelected);
                    _funcTool.setDdlData (ddlWorkGroupAction_Edit, _data_cen_master.cen_wg_list_m0, "wg_name_th", "wg_idx");
                } else {
                    ddlWorkGroupAction_Edit.Items.Clear ();
                }

                ddlLineWorkAction_Edit.Items.Clear ();
                ddlDepartmentAction_Edit.Items.Clear ();
                ddlSectionAction_Edit.Items.Clear ();
                ddlPositionAction_Edit.Items.Clear ();
                ddlWorkGroupAction_Edit.Items.Insert (0, new ListItem ("--- เลือกกลุ่มงาน ---", "0"));
                ddlLineWorkAction_Edit.Items.Insert (0, new ListItem ("--- เลือกสายงาน ---", "0"));
                ddlDepartmentAction_Edit.Items.Insert (0, new ListItem ("--- เลือกฝ่าย ---", "0"));
                ddlSectionAction_Edit.Items.Insert (0, new ListItem ("--- เลือกแผนก ---", "0"));
                ddlPositionAction_Edit.Items.Insert (0, new ListItem ("--- เลือกตำแหน่ง ---", "0"));
                break;
            case "ddlWorkGroupAction_Edit":
                if (_funcTool.convertToInt (ddlWorkGroupAction_Edit.SelectedValue) > 0) {
                    search_cen_master_detail _wgSelected = new search_cen_master_detail ();
                    _wgSelected.s_org_idx = ddlOrganizationAction_Edit.SelectedValue;
                    _wgSelected.s_wg_idx = ddlWorkGroupAction_Edit.SelectedValue;
                    _data_cen_master = getMasterList ("5", _wgSelected);
                    _funcTool.setDdlData (ddlLineWorkAction_Edit, _data_cen_master.cen_lw_list_m0, "lw_name_th", "lw_idx");
                } else {
                    ddlLineWorkAction_Edit.Items.Clear ();
                }

                ddlDepartmentAction_Edit.Items.Clear ();
                ddlSectionAction_Edit.Items.Clear ();
                ddlPositionAction_Edit.Items.Clear ();
                ddlLineWorkAction_Edit.Items.Insert (0, new ListItem ("--- เลือกสายงาน ---", "0"));
                ddlDepartmentAction_Edit.Items.Insert (0, new ListItem ("--- เลือกฝ่าย ---", "0"));
                ddlSectionAction_Edit.Items.Insert (0, new ListItem ("--- เลือกแผนก ---", "0"));
                ddlPositionAction_Edit.Items.Insert (0, new ListItem ("--- เลือกตำแหน่ง ---", "0"));
                break;
            case "ddlLineWorkAction_Edit":
                if (_funcTool.convertToInt (ddlLineWorkAction_Edit.SelectedValue) > 0) {
                    search_cen_master_detail _lgSelected = new search_cen_master_detail ();
                    _lgSelected.s_org_idx = ddlOrganizationAction_Edit.SelectedValue;
                    _lgSelected.s_wg_idx = ddlWorkGroupAction_Edit.SelectedValue;
                    _lgSelected.s_lw_idx = ddlLineWorkAction_Edit.SelectedValue;
                    _data_cen_master = getMasterList ("6", _lgSelected);
                    _funcTool.setDdlData (ddlDepartmentAction_Edit, _data_cen_master.cen_dept_list_m0, "dept_name_th", "dept_idx");
                } else {
                    ddlDepartmentAction_Edit.Items.Clear ();
                }

                ddlSectionAction_Edit.Items.Clear ();
                ddlPositionAction_Edit.Items.Clear ();
                ddlDepartmentAction_Edit.Items.Insert (0, new ListItem ("--- เลือกฝ่าย ---", "0"));
                ddlSectionAction_Edit.Items.Insert (0, new ListItem ("--- เลือกแผนก ---", "0"));
                ddlPositionAction_Edit.Items.Insert (0, new ListItem ("--- เลือกตำแหน่ง ---", "0"));
                break;
            case "ddlDepartmentAction_Edit":
                if (_funcTool.convertToInt (ddlDepartmentAction_Edit.SelectedValue) > 0) {
                    search_cen_master_detail _deptSelected = new search_cen_master_detail ();
                    _deptSelected.s_org_idx = ddlOrganizationAction_Edit.SelectedValue;
                    _deptSelected.s_wg_idx = ddlWorkGroupAction_Edit.SelectedValue;
                    _deptSelected.s_lw_idx = ddlLineWorkAction_Edit.SelectedValue;
                    _deptSelected.s_dept_idx = ddlDepartmentAction_Edit.SelectedValue;
                    _data_cen_master = getMasterList ("7", _deptSelected);
                    _funcTool.setDdlData (ddlSectionAction_Edit, _data_cen_master.cen_sec_list_m0, "sec_name_th", "sec_idx");
                } else {
                    ddlSectionAction_Edit.Items.Clear ();
                }

                ddlPositionAction_Edit.Items.Clear ();
                ddlSectionAction_Edit.Items.Insert (0, new ListItem ("--- เลือกแผนก ---", "0"));
                ddlPositionAction_Edit.Items.Insert (0, new ListItem ("--- เลือกตำแหน่ง ---", "0"));
                break;
            case "ddlSectionAction_Edit":
                if (_funcTool.convertToInt (ddlSectionAction_Edit.SelectedValue) > 0) {
                    search_cen_master_detail _secSelected = new search_cen_master_detail ();
                    _secSelected.s_org_idx = ddlOrganizationAction_Edit.SelectedValue;
                    _secSelected.s_wg_idx = ddlWorkGroupAction_Edit.SelectedValue;
                    _secSelected.s_lw_idx = ddlLineWorkAction_Edit.SelectedValue;
                    _secSelected.s_dept_idx = ddlDepartmentAction_Edit.SelectedValue;
                    _secSelected.s_sec_idx = ddlSectionAction_Edit.SelectedValue;
                    _data_cen_master = getMasterList ("8", _secSelected);
                    _funcTool.setDdlData (ddlPositionAction_Edit, _data_cen_master.cen_pos_list_m0, "pos_name_th", "pos_idx");
                } else {
                    ddlPositionAction_Edit.Items.Clear ();
                }

                ddlPositionAction_Edit.Items.Insert (0, new ListItem ("--- เลือกตำแหน่ง ---", "0"));
                break;

            case "ddlOrganizationEdit":

                var ddlOrganizationEdit = (DropDownList)sender;
                var row_org_edit = (GridViewRow)ddlOrganizationEdit.NamingContainer;

                DropDownList ddlOrganizationEdit1 = (DropDownList)row_org_edit.FindControl("ddlOrganizationEdit");
                DropDownList ddlWorkGroupEdit1 = (DropDownList)row_org_edit.FindControl("ddlWorkGroupEdit");
                DropDownList ddlLineWorkEdit1 = (DropDownList)row_org_edit.FindControl("ddlLineWorkEdit");
                DropDownList ddlDepartmentEdit1 = (DropDownList)row_org_edit.FindControl("ddlDepartmentEdit");
                DropDownList ddlSectionEdit1 = (DropDownList)row_org_edit.FindControl("ddlSectionEdit");
                DropDownList ddlPositionEdit1 = (DropDownList)row_org_edit.FindControl("ddlPositionEdit");



                if (_funcTool.convertToInt (ddlOrganizationEdit1.SelectedValue) > 0) {
                    getWorkGroupCen(ddlWorkGroupEdit1, int.Parse(ddlOrganizationEdit1.SelectedValue));
                } else {
                    ddlWorkGroupEdit1.Items.Clear ();
                }

                ddlLineWorkEdit1.Items.Clear ();
                ddlDepartmentEdit1.Items.Clear ();
                ddlSectionEdit1.Items.Clear ();
                ddlPositionEdit1.Items.Clear ();

                ddlWorkGroupEdit1.Items.Insert (0, new ListItem ("--- เลือกกลุ่มงาน ---", "0"));
                ddlLineWorkEdit1.Items.Insert (0, new ListItem ("--- เลือกสายงาน ---", "0"));
                ddlDepartmentEdit1.Items.Insert (0, new ListItem ("--- เลือกฝ่าย ---", "0"));
                ddlSectionEdit1.Items.Insert (0, new ListItem ("--- เลือกแผนก ---", "0"));
                ddlPositionEdit1.Items.Insert (0, new ListItem ("--- เลือกตำแหน่ง ---", "0"));
                break;
            case "ddlWorkGroupEdit":

                var ddlWorkGroupEdit = (DropDownList)sender;
                var row_org_edit2 = (GridViewRow)ddlWorkGroupEdit.NamingContainer;

                DropDownList ddlOrganizationEdit2 = (DropDownList)row_org_edit2.FindControl("ddlOrganizationEdit");
                DropDownList ddlWorkGroupEdit2 = (DropDownList)row_org_edit2.FindControl("ddlWorkGroupEdit");
                DropDownList ddlLineWorkEdit2 = (DropDownList)row_org_edit2.FindControl("ddlLineWorkEdit");
                DropDownList ddlDepartmentEdit2 = (DropDownList)row_org_edit2.FindControl("ddlDepartmentEdit");
                DropDownList ddlSectionEdit2 = (DropDownList)row_org_edit2.FindControl("ddlSectionEdit");
                DropDownList ddlPositionEdit2 = (DropDownList)row_org_edit2.FindControl("ddlPositionEdit");


                if (_funcTool.convertToInt (ddlWorkGroupEdit2.SelectedValue) > 0) {
                     getLineWorkCen(ddlLineWorkEdit2, int.Parse(ddlOrganizationEdit2.SelectedValue), int.Parse(ddlWorkGroupEdit2.SelectedValue));
                } else {
                    ddlLineWorkEdit2.Items.Clear ();
                }

                ddlDepartmentEdit2.Items.Clear ();
                ddlSectionEdit2.Items.Clear ();
                ddlPositionEdit2.Items.Clear ();

                ddlLineWorkEdit2.Items.Insert (0, new ListItem ("--- เลือกสายงาน ---", "0"));
                ddlDepartmentEdit2.Items.Insert (0, new ListItem ("--- เลือกฝ่าย ---", "0"));
                ddlSectionEdit2.Items.Insert (0, new ListItem ("--- เลือกแผนก ---", "0"));
                ddlPositionEdit2.Items.Insert (0, new ListItem ("--- เลือกตำแหน่ง ---", "0"));
                break;
            case "ddlLineWorkEdit":

                var ddlLineWorkEdit = (DropDownList)sender;
                var row_org_edit3 = (GridViewRow)ddlLineWorkEdit.NamingContainer;

                DropDownList ddlOrganizationEdit3 = (DropDownList)row_org_edit3.FindControl("ddlOrganizationEdit");
                DropDownList ddlWorkGroupEdit3 = (DropDownList)row_org_edit3.FindControl("ddlWorkGroupEdit");
                DropDownList ddlLineWorkEdit3 = (DropDownList)row_org_edit3.FindControl("ddlLineWorkEdit");
                DropDownList ddlDepartmentEdit3 = (DropDownList)row_org_edit3.FindControl("ddlDepartmentEdit");
                DropDownList ddlSectionEdit3 = (DropDownList)row_org_edit3.FindControl("ddlSectionEdit");
                DropDownList ddlPositionEdit3 = (DropDownList)row_org_edit3.FindControl("ddlPositionEdit");


                if (_funcTool.convertToInt (ddlLineWorkEdit3.SelectedValue) > 0) {
                    getDepartmentCen(ddlDepartmentEdit3, int.Parse(ddlOrganizationEdit3.SelectedValue), int.Parse(ddlWorkGroupEdit3.SelectedValue), int.Parse(ddlLineWorkEdit3.SelectedValue));
                } else {
                    ddlDepartmentEdit3.Items.Clear ();
                }

                ddlSectionEdit3.Items.Clear ();
                ddlPositionEdit3.Items.Clear ();

                ddlDepartmentEdit3.Items.Insert (0, new ListItem ("--- เลือกฝ่าย ---", "0"));
                ddlSectionEdit3.Items.Insert (0, new ListItem ("--- เลือกแผนก ---", "0"));
                ddlPositionEdit3.Items.Insert (0, new ListItem ("--- เลือกตำแหน่ง ---", "0"));
                break;
            case "ddlDepartmentEdit":

                var ddlDepartmentEdit = (DropDownList)sender;
                var row_org_edit4 = (GridViewRow)ddlDepartmentEdit.NamingContainer;

                DropDownList ddlOrganizationEdit4 = (DropDownList)row_org_edit4.FindControl("ddlOrganizationEdit");
                DropDownList ddlWorkGroupEdit4 = (DropDownList)row_org_edit4.FindControl("ddlWorkGroupEdit");
                DropDownList ddlLineWorkEdit4 = (DropDownList)row_org_edit4.FindControl("ddlLineWorkEdit");
                DropDownList ddlDepartmentEdit4 = (DropDownList)row_org_edit4.FindControl("ddlDepartmentEdit");
                DropDownList ddlSectionEdit4 = (DropDownList)row_org_edit4.FindControl("ddlSectionEdit");
                DropDownList ddlPositionEdit4 = (DropDownList)row_org_edit4.FindControl("ddlPositionEdit");
                if (_funcTool.convertToInt (ddlDepartmentEdit4.SelectedValue) > 0) {
                    getSectionCen(ddlSectionEdit4, int.Parse(ddlOrganizationEdit4.SelectedValue), int.Parse(ddlWorkGroupEdit4.SelectedValue), int.Parse(ddlLineWorkEdit4.SelectedValue), int.Parse(ddlDepartmentEdit4.SelectedValue));
                } else {
                    ddlSectionEdit4.Items.Clear ();
                }

                ddlPositionEdit4.Items.Clear ();

                ddlSectionEdit4.Items.Insert (0, new ListItem ("--- เลือกแผนก ---", "0"));
                ddlPositionEdit4.Items.Insert (0, new ListItem ("--- เลือกตำแหน่ง ---", "0"));
                break;
            case "ddlSectionEdit":
                var ddlSectionEdit = (DropDownList)sender;
                var row_org_edit5 = (GridViewRow)ddlSectionEdit.NamingContainer;

                DropDownList ddlOrganizationEdit5 = (DropDownList)row_org_edit5.FindControl("ddlOrganizationEdit");
                DropDownList ddlWorkGroupEdit5 = (DropDownList)row_org_edit5.FindControl("ddlWorkGroupEdit");
                DropDownList ddlLineWorkEdit5 = (DropDownList)row_org_edit5.FindControl("ddlLineWorkEdit");
                DropDownList ddlDepartmentEdit5 = (DropDownList)row_org_edit5.FindControl("ddlDepartmentEdit");
                DropDownList ddlSectionEdit5 = (DropDownList)row_org_edit5.FindControl("ddlSectionEdit");
                DropDownList ddlPositionEdit5 = (DropDownList)row_org_edit5.FindControl("ddlPositionEdit");
                if (_funcTool.convertToInt (ddlSectionEdit5.SelectedValue) > 0) {
                    getPositionCen(ddlPositionEdit5, int.Parse(ddlOrganizationEdit5.SelectedValue), int.Parse(ddlWorkGroupEdit5.SelectedValue), int.Parse(ddlLineWorkEdit5.SelectedValue), int.Parse(ddlDepartmentEdit5.SelectedValue), int.Parse(ddlSectionEdit5.SelectedValue));
                } else {
                    ddlPositionEdit5.Items.Clear ();
                }

                ddlPositionEdit5.Items.Insert (0, new ListItem ("--- เลือกตำแหน่ง ---", "0"));
                break;
            
        }
    }

    #endregion

    protected void onRowCommand(Object sender, GridViewCommandEventArgs e)
    {
        if (sender is GridView)
        {
            string cmdName = e.CommandName;
            switch (cmdName)
            {
                case "cmdRemovePosition":
                    GridView GvRePosition = (GridView)FvInsertEmp.FindControl("GvRePosition");
                    GridViewRow rowSelect = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    int rowIndex = rowSelect.RowIndex;
                    DataSet dsContacts = (DataSet)ViewState["vs_dsInsertPositionCenAdd"];
                    dsContacts.Tables["dsInsertPositionCenAddTable"].Rows[rowIndex].Delete();
                    dsContacts.AcceptChanges();
                    setGridData(GvRePosition, dsContacts.Tables["dsInsertPositionCenAddTable"]);
                    if (dsContacts.Tables["dsInsertPositionCenAddTable"].Rows.Count < 1)
                    {
                        GvRePosition.Visible = false;
                    }
                    break;
                case "cmdRemoveEditPosition":
                    GridView GvRePosition_Edit = (GridView)FvEdit_Detail.FindControl("GvRePosition_Edit");
                    GridViewRow rowSelect_Edit = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    int rowIndex_Edit = rowSelect_Edit.RowIndex;
                    DataSet dsContacts_Edit = (DataSet)ViewState["vs_dsInsertPositionCenAdd_Edit"];
                    dsContacts_Edit.Tables["dsInsertEditPositionCenAddTable"].Rows[rowIndex_Edit].Delete();
                    dsContacts_Edit.AcceptChanges();
                    setGridData(GvRePosition_Edit, dsContacts_Edit.Tables["dsInsertEditPositionCenAddTable"]);
                    if (dsContacts_Edit.Tables["dsInsertEditPositionCenAddTable"].Rows.Count < 1)
                    {
                        GvRePosition_Edit.Visible = false;
                    }
                    break;
                

            }
        }
    }

    #region datatable

    protected void DataTableInsertEditRePosition()
    {
        var ds_InsertPositionCenAdd_edit = new DataSet();
        ds_InsertPositionCenAdd_edit.Tables.Add("dsInsertEditPositionCenAddTable");
        
        
        ds_InsertPositionCenAdd_edit.Tables[0].Columns.Add("OrganizationTH_IDX", typeof(int));
        ds_InsertPositionCenAdd_edit.Tables[0].Columns.Add("OrganizationTH", typeof(String));
        ds_InsertPositionCenAdd_edit.Tables[0].Columns.Add("WorkGroupTH_IDX", typeof(int));
        ds_InsertPositionCenAdd_edit.Tables[0].Columns.Add("WorkGroupTH", typeof(String));
        ds_InsertPositionCenAdd_edit.Tables[0].Columns.Add("LineWorkTH_IDX", typeof(int));
        ds_InsertPositionCenAdd_edit.Tables[0].Columns.Add("LineWorkTH", typeof(String));
        ds_InsertPositionCenAdd_edit.Tables[0].Columns.Add("DepartmentTH_IDX", typeof(int));
        ds_InsertPositionCenAdd_edit.Tables[0].Columns.Add("DepartmentTH", typeof(String));
        ds_InsertPositionCenAdd_edit.Tables[0].Columns.Add("SectionTH_IDX", typeof(int));
        ds_InsertPositionCenAdd_edit.Tables[0].Columns.Add("SectionTH", typeof(String));
        ds_InsertPositionCenAdd_edit.Tables[0].Columns.Add("PositionTH_IDX", typeof(int));
        ds_InsertPositionCenAdd_edit.Tables[0].Columns.Add("PositionTH", typeof(String));
        ds_InsertPositionCenAdd_edit.Tables[0].Columns.Add("ActionPositionTH_IDX", typeof(int));
        ds_InsertPositionCenAdd_edit.Tables[0].Columns.Add("ActionPositionTH", typeof(String));
        
       
        ViewState["vs_dsInsertPositionCenAdd_Edit"] = ds_InsertPositionCenAdd_edit;

    }

    protected void setInsertEditRePosition()
    {
        if (ViewState["vs_dsInsertPositionCenAdd_Edit"] != null)
        {
            var GvRePosition_Edit = (GridView)FvEdit_Detail.FindControl("GvRePosition_Edit");
            var ddlOrganization = (DropDownList)FvEdit_Detail.FindControl("ddlOrganizationAction_Edit");
            var ddlWorkGroup = (DropDownList)FvEdit_Detail.FindControl("ddlWorkGroupAction_Edit");
            var ddlLineWork = (DropDownList)FvEdit_Detail.FindControl("ddlLineWorkAction_Edit");
            var ddlDepartment = (DropDownList)FvEdit_Detail.FindControl("ddlDepartmentAction_Edit");
            var ddlSection = (DropDownList)FvEdit_Detail.FindControl("ddlSectionAction_Edit");
            var ddlPosition = (DropDownList)FvEdit_Detail.FindControl("ddlPositionAction_Edit");
          
            
            DataSet dsContacts = (DataSet)ViewState["vs_dsInsertPositionCenAdd_Edit"];

            foreach (DataRow dr in dsContacts.Tables["dsInsertEditPositionCenAddTable"].Rows)
            {
                if (dr["OrganizationTH"].ToString() == ddlOrganization.SelectedItem.Text && dr["PositionTH"].ToString() == ddlPosition.SelectedItem.Text)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- Check Data Again ---');", true);
                    return;
                }
            }
            DataRow drContacts = dsContacts.Tables["dsInsertEditPositionCenAddTable"].NewRow();
            drContacts["OrganizationTH_IDX"] = int.Parse(ddlOrganization.SelectedValue);
            drContacts["OrganizationTH"] = ddlOrganization.SelectedItem.Text;

            drContacts["WorkGroupTH_IDX"] = int.Parse(ddlWorkGroup.SelectedValue);
            drContacts["WorkGroupTH"] = ddlWorkGroup.SelectedItem.Text;
            
            drContacts["LineWorkTH_IDX"] = int.Parse(ddlLineWork.SelectedValue);
            drContacts["LineWorkTH"] = ddlLineWork.SelectedItem.Text;
            
            drContacts["DepartmentTH_IDX"] = int.Parse(ddlDepartment.SelectedValue);
            drContacts["DepartmentTH"] = ddlDepartment.SelectedItem.Text;
            
            drContacts["SectionTH_IDX"] = int.Parse(ddlSection.SelectedValue);
            drContacts["SectionTH"] = ddlSection.SelectedItem.Text;

            drContacts["PositionTH_IDX"] = int.Parse(ddlPosition.SelectedValue);
            drContacts["PositionTH"] = ddlPosition.SelectedItem.Text;

            

            dsContacts.Tables["dsInsertEditPositionCenAddTable"].Rows.Add(drContacts);
            ViewState["vs_dsInsertPositionCenAdd_Edit"] = dsContacts;


            setGridData(GvRePosition_Edit, dsContacts.Tables["dsInsertEditPositionCenAddTable"]);
            GvRePosition_Edit.Visible = true;
        }
    }

    protected void ClearDataInsertEditRePosition()
    {

        GridView GvRePosition_Edit = (GridView)FvEdit_Detail.FindControl("GvRePosition_Edit");

        //clear set equipment list
        ViewState["vs_dsInsertPositionCenAdd_Edit"] = null;
        GvRePosition_Edit.DataSource = ViewState["vs_dsInsertPositionCenAdd_Edit"];
        GvRePosition_Edit.DataBind();
        GvRePosition_Edit.Visible = false;

        DataTableInsertEditRePosition();

    }

    protected void DataTableInsertRePosition()
    {
        var ds_InsertPositionCenAdd = new DataSet();
        ds_InsertPositionCenAdd.Tables.Add("dsInsertPositionCenAddTable");
        
        
        ds_InsertPositionCenAdd.Tables[0].Columns.Add("OrganizationTH_IDX", typeof(int));
        ds_InsertPositionCenAdd.Tables[0].Columns.Add("OrganizationTH", typeof(String));
        ds_InsertPositionCenAdd.Tables[0].Columns.Add("WorkGroupTH_IDX", typeof(int));
        ds_InsertPositionCenAdd.Tables[0].Columns.Add("WorkGroupTH", typeof(String));
        ds_InsertPositionCenAdd.Tables[0].Columns.Add("LineWorkTH_IDX", typeof(int));
        ds_InsertPositionCenAdd.Tables[0].Columns.Add("LineWorkTH", typeof(String));
        ds_InsertPositionCenAdd.Tables[0].Columns.Add("DepartmentTH_IDX", typeof(int));
        ds_InsertPositionCenAdd.Tables[0].Columns.Add("DepartmentTH", typeof(String));
        ds_InsertPositionCenAdd.Tables[0].Columns.Add("SectionTH_IDX", typeof(int));
        ds_InsertPositionCenAdd.Tables[0].Columns.Add("SectionTH", typeof(String));
        ds_InsertPositionCenAdd.Tables[0].Columns.Add("PositionTH_IDX", typeof(int));
        ds_InsertPositionCenAdd.Tables[0].Columns.Add("PositionTH", typeof(String));
        ds_InsertPositionCenAdd.Tables[0].Columns.Add("ActionPositionTH_IDX", typeof(int));
        ds_InsertPositionCenAdd.Tables[0].Columns.Add("ActionPositionTH", typeof(String));
        
       
        ViewState["vs_dsInsertPositionCenAdd"] = ds_InsertPositionCenAdd;

    }

    protected void setInsertRePosition()
    {
        if (ViewState["vs_dsInsertPositionCenAdd"] != null)
        {
            var GvRePosition = (GridView)FvInsertEmp.FindControl("GvRePosition");
            var ddlOrganization = (DropDownList)FvInsertEmp.FindControl("ddlOrganizationAction");
            var ddlWorkGroup = (DropDownList)FvInsertEmp.FindControl("ddlWorkGroupAction");
            var ddlLineWork = (DropDownList)FvInsertEmp.FindControl("ddlLineWorkAction");
            var ddlDepartment = (DropDownList)FvInsertEmp.FindControl("ddlDepartmentAction");
            var ddlSection = (DropDownList)FvInsertEmp.FindControl("ddlSectionAction");
            var ddlPosition = (DropDownList)FvInsertEmp.FindControl("ddlPositionAction");
            //var ddlActionPosition = (DropDownList)FvInsertEmp.FindControl("ddlActionPosition");

            
            DataSet dsContacts = (DataSet)ViewState["vs_dsInsertPositionCenAdd"];

            foreach (DataRow dr in dsContacts.Tables["dsInsertPositionCenAddTable"].Rows)
            {
                if (dr["OrganizationTH"].ToString() == ddlOrganization.SelectedItem.Text && dr["PositionTH"].ToString() == ddlPosition.SelectedItem.Text)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- Check Data Again ---');", true);
                    return;
                }
            }
            DataRow drContacts = dsContacts.Tables["dsInsertPositionCenAddTable"].NewRow();
            drContacts["OrganizationTH_IDX"] = int.Parse(ddlOrganization.SelectedValue);
            drContacts["OrganizationTH"] = ddlOrganization.SelectedItem.Text;

            drContacts["WorkGroupTH_IDX"] = int.Parse(ddlWorkGroup.SelectedValue);
            drContacts["WorkGroupTH"] = ddlWorkGroup.SelectedItem.Text;
            
            drContacts["LineWorkTH_IDX"] = int.Parse(ddlLineWork.SelectedValue);
            drContacts["LineWorkTH"] = ddlLineWork.SelectedItem.Text;
            
            drContacts["DepartmentTH_IDX"] = int.Parse(ddlDepartment.SelectedValue);
            drContacts["DepartmentTH"] = ddlDepartment.SelectedItem.Text;
            
            drContacts["SectionTH_IDX"] = int.Parse(ddlSection.SelectedValue);
            drContacts["SectionTH"] = ddlSection.SelectedItem.Text;

            drContacts["PositionTH_IDX"] = int.Parse(ddlPosition.SelectedValue);
            drContacts["PositionTH"] = ddlPosition.SelectedItem.Text;

            

            dsContacts.Tables["dsInsertPositionCenAddTable"].Rows.Add(drContacts);
            ViewState["vs_dsInsertPositionCenAdd"] = dsContacts;


            setGridData(GvRePosition, dsContacts.Tables["dsInsertPositionCenAddTable"]);
            GvRePosition.Visible = true;
        }
    }

    protected void ClearDataInsertRePosition()
    {

        GridView GvRePosition = (GridView)FvInsertEmp.FindControl("GvRePosition");

        //clear set equipment list
        ViewState["vs_dsInsertPositionCenAdd"] = null;
        GvRePosition.DataSource = ViewState["vs_dsInsertPositionCenAdd"];
        GvRePosition.DataBind();
        GvRePosition.Visible = false;

        DataTableInsertRePosition();

    }
    #endregion

    protected void getWorkGroupCen(DropDownList ddlName, int _org_idx)
    {

        search_cen_master_detail _orgSelected = new search_cen_master_detail ();
        _orgSelected.s_org_idx = _org_idx.ToString();
        _data_cen_master = getMasterList ("4", _orgSelected);
        _funcTool.setDdlData (ddlName, _data_cen_master.cen_wg_list_m0, "wg_name_th", "wg_idx");

    }

    protected void getLineWorkCen(DropDownList ddlName, int _org_idx, int _wg_idx)
    {

        search_cen_master_detail _wgSelected = new search_cen_master_detail ();
        _wgSelected.s_org_idx = _org_idx.ToString();
        _wgSelected.s_wg_idx = _wg_idx.ToString();
        _data_cen_master = getMasterList ("5", _wgSelected);
        _funcTool.setDdlData (ddlName, _data_cen_master.cen_lw_list_m0, "lw_name_th", "lw_idx");

    }

    protected void getDepartmentCen(DropDownList ddlName, int _org_idx, int _wg_idx, int _lw_idx)
    {

        search_cen_master_detail _lgSelected = new search_cen_master_detail ();
        _lgSelected.s_org_idx = _org_idx.ToString();
        _lgSelected.s_wg_idx = _wg_idx.ToString();
        _lgSelected.s_lw_idx = _lw_idx.ToString();
        _data_cen_master = getMasterList ("6", _lgSelected);
        _funcTool.setDdlData (ddlName, _data_cen_master.cen_dept_list_m0, "dept_name_th", "dept_idx");

    }

    protected void getSectionCen(DropDownList ddlName, int _org_idx, int _wg_idx, int _lw_idx, int _dept_idx)
    {

        search_cen_master_detail _deptSelected = new search_cen_master_detail ();
        _deptSelected.s_org_idx = _org_idx.ToString();
        _deptSelected.s_wg_idx = _wg_idx.ToString();
        _deptSelected.s_lw_idx = _lw_idx.ToString();
        _deptSelected.s_dept_idx = _dept_idx.ToString();
        _data_cen_master = getMasterList ("7", _deptSelected);
        _funcTool.setDdlData (ddlName, _data_cen_master.cen_sec_list_m0, "sec_name_th", "sec_idx");

    }

    protected void getPositionCen(DropDownList ddlName, int _org_idx, int _wg_idx, int _lw_idx, int _dept_idx, int _sec_idx)
    {

        search_cen_master_detail _secSelected = new search_cen_master_detail ();
        _secSelected.s_org_idx = _org_idx.ToString();
        _secSelected.s_wg_idx = _wg_idx.ToString();
        _secSelected.s_lw_idx = _lw_idx.ToString();
        _secSelected.s_dept_idx = _dept_idx.ToString();
        _secSelected.s_sec_idx = _sec_idx.ToString();
        _data_cen_master = getMasterList ("8", _secSelected);
        _funcTool.setDdlData (ddlName, _data_cen_master.cen_pos_list_m0, "pos_name_th", "pos_idx");

    }



    #region callService



    protected data_cen_employee getEmployeeList (string _employeeMode, search_cen_employee_detail _searchData) {
        _data_cen_employee.search_cen_employee_list = new search_cen_employee_detail[1];
        _data_cen_employee.employee_mode = _employeeMode;
        _data_cen_employee.search_cen_employee_list[0] = _searchData;
        // litDebug.Text = _funcTool.convertObjectToJson(_data_cen_master);

        _data_cen_employee = callServicePostCenEmployee (_urlGetCenEmployeeList, _data_cen_employee);
        return _data_cen_employee;
    }


    protected data_cen_master getMasterList(string _masterMode, search_cen_master_detail _searchData)
    {
        _data_cen_master.search_cen_master_list = new search_cen_master_detail[1];
        _data_cen_master.master_mode = _masterMode;
        _data_cen_master.search_cen_master_list[0] = _searchData;
        //litDebug.Text = _funcTool.convertObjectToJson(_data_cen_master);

        _data_cen_master = callServicePostCenMaster(_urlCenGetCenMasterList, _data_cen_master);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_cen_master));
        return _data_cen_master;
    }

    protected data_cen_master callServicePostCenMaster(string _cmdUrl, data_cen_master _data_cen_master)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_cen_master);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_cen_master = (data_cen_master)_funcTool.convertJsonToObject(typeof(data_cen_master), _localJson);

        return _data_cen_master;
    }

    protected data_cen_employee callServicePostCenEmployee (string _cmdUrl, data_cen_employee _data_cen_employee) {
        // convert to json
        _localJson = _funcTool.convertObjectToJson (_data_cen_employee);
        //litDebug.Text = _local_json;

        // call services
        _localJson = _funcTool.callServicePost (_cmdUrl, _localJson);

        // convert json to object
        _data_cen_employee = (data_cen_employee) _funcTool.convertJsonToObject (typeof (data_cen_employee), _localJson);

        return _data_cen_employee;
    }


    protected data_cen_master callServiceMaster(string _cmdUrl, data_cen_master _data_cen_master)
    {
        _localJson = _funcTool.convertObjectToJson(_data_cen_master);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_cen_master = (data_cen_master)_funcTool.convertJsonToObject(typeof(data_cen_master), _localJson);


        return _data_cen_master;
    }


    protected data_employee callServiceEmpProfile(string _cmdUrl)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }

    protected data_employee callServiceEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //fss.Text = _localJson.ToString();

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //fsa.Text = _cmdUrl + _localJson;

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);


        return _dataEmployee;
    }

    protected data_employee callServiceEmployee_Check(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //fss.Text = _localJson.ToString();

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //fsa.Text = _cmdUrl + _localJson;
        //Label2.Text = _cmdUrl + _localJson;

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        ViewState["return_code"] = _dataEmployee.return_code;
        ViewState["return_sucess"] = _dataEmployee.return_msg;

        return _dataEmployee;
    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger1 = new PostBackTrigger();
        trigger1.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger1);
    }

    protected data_vacation callServiceVacation(string _cmdUrl, data_vacation _dataVacation)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataVacation);
        //fss.Text = _localJson.ToString();

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //fsa.Text = _cmdUrl + _localJson;

        // convert json to object
        _dataVacation = (data_vacation)_funcTool.convertJsonToObject(typeof(data_vacation), _localJson);


        return _dataVacation;
    }

    protected data_tpm_form callServicePostTPMForm(string _cmdUrl, data_tpm_form _dtpmform)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtpmform);
        // text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtpmform = (data_tpm_form)_funcTool.convertJsonToObject(typeof(data_tpm_form), _localJson);


        return _dtpmform;
    }

    #endregion

    #region custom data format and display
    protected string formatDate(string dateIN)
    {
        return DateTime.Parse(dateIN).ToString("dd/MM/yyyy");
    }
    #endregion

    #region BTN
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        DropDownList ddlemptype1 = (DropDownList)ViewProbation.FindControl("ddlemptype");

        switch (cmdName)
        {
            case "btnIndex":
                Select_Employee_index();
                Menu_Color(int.Parse(cmdArg));
                MvMaster.SetActiveView(ViewIndex);
                break;
            case "btnadd":
                Menu_Color(int.Parse(cmdArg));
                MvMaster.SetActiveView(ViewAdd);
                FvInsertEmp.ChangeMode(FormViewMode.Insert);
                FvInsertEmp.DataBind();
                Menu_Color_Add(1);
                linkFvTrigger(FvInsertEmp);
                break;
            case "btnsetapprove":
                Menu_Color(int.Parse(cmdArg));
                MvMaster.SetActiveView(ViewSetApprove);

                var dsodsp = new DataSet();
                dsodsp.Tables.Add("Temp_ODSP");
                dsodsp.Tables[0].Columns.Add("org_idx", typeof(int));
                dsodsp.Tables[0].Columns.Add("org_name_th", typeof(string));
                dsodsp.Tables[0].Columns.Add("rdept_idx", typeof(int));
                dsodsp.Tables[0].Columns.Add("dept_name_th", typeof(string));
                dsodsp.Tables[0].Columns.Add("rsec_idx", typeof(int));
                dsodsp.Tables[0].Columns.Add("sec_name_th", typeof(string));
                dsodsp.Tables[0].Columns.Add("rpos_idx", typeof(int));
                dsodsp.Tables[0].Columns.Add("pos_name_th", typeof(string));
                dsodsp.Tables[0].Columns.Add("emp_idx", typeof(int));
                ViewState["vsTemp_ODSP"] = dsodsp;
                break;
            case "btnreport":
                Menu_Color(int.Parse(cmdArg));
                MvMaster.SetActiveView(ViewReport);
                break;
            case "btnedit_emp":
                MvMaster.SetActiveView(ViewEdit);
                FvEdit_Detail.ChangeMode(FormViewMode.Edit);
                Select_FvEdit_Detail(int.Parse(cmdArg));
                ViewState["Gv_emp_idx"] = int.Parse(cmdArg);
                Menu_Color_Edit(1);
                linkFvTrigger(FvEdit_Detail);

                break;
            case "btnresetpassword_emp":
                _dataEmployee.employee_list = new employee_detail[1];
                employee_detail _dataEmployee_reset = new employee_detail();

                _dataEmployee_reset.emp_idx = int.Parse(cmdArg);

                _dataEmployee.employee_list[0] = _dataEmployee_reset;
                _dataEmployee = callServiceEmployee(_urlSetResetpass_employeeListList, _dataEmployee);
                Select_Employee_index();
                break;
            case "btndelete_emp":
                _dataEmployee.employee_list = new employee_detail[1];
                employee_detail _dataEmployee_emp = new employee_detail();

                _dataEmployee_emp.emp_idx = int.Parse(cmdArg);

                _dataEmployee.employee_list[0] = _dataEmployee_emp;
                _dataEmployee = callServiceEmployee(_urlSetDeleteEmployeeListList, _dataEmployee);
                Select_Employee_index();
                break;
            case "btn_search_index":
                Select_Employee_index_search();

                if (ViewState["Box_dataEmployee_index"] != null)
                {
                    employee_detail[] _templist_cheack = (employee_detail[])ViewState["Box_dataEmployee_index"];
                    var _linqCheck = (from dataMat in _templist_cheack

                                      select new
                                      {
                                          dataMat
                                      }).ToList();

                    lblsum_list_index.Text = "จำนวน : " + _linqCheck.Count().ToString() + " รายการ / ทั้งหมด : " + GvEmployee.PageCount.ToString() + " หน้า";
                }
                else
                {
                    lblsum_list_index.Text = "จำนวน : " + "0" + " รายการ / ทั้งหมด : " + "0" + " หน้า";
                }

                break;
            case "btn_search_report":
                DropDownList ddlreport_type_ = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlreport_type");
                DropDownList ddlchart_type_ = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlchart_type");
                CheckBoxList YrChkBox = (CheckBoxList)Fv_Search_Emp_Report.FindControl("YrChkBox");
                //Panel pn_chart_type_ = (Panel)Fv_Search_Emp_Report.FindControl("pn_chart_type");
                //Panel pn_chart_type_ = (Panel)Fv_Search_Emp_Report.FindControl("pn_chart_type");
                //Panel pn_chart_type_ = (Panel)Fv_Search_Emp_Report.FindControl("pn_chart_type");
                //Panel pn_chart_type_ = (Panel)Fv_Search_Emp_Report.FindControl("pn_chart_type");

                switch (ddlreport_type_.SelectedValue) //Report Type
                {
                    case "1": //Table
                        CheckBoxList YrChkBoxColumns = (CheckBoxList)Fv_Search_Emp_Report.FindControl("YrChkBoxColumns");

                        ViewState["Columns_Check_Search"] = null;
                        List<String> YrColumnsList = new List<string>();
                        foreach (ListItem item1 in YrChkBoxColumns.Items)
                        {
                            if (item1.Selected)
                            {
                                YrColumnsList.Add(item1.Value);
                            }
                            else
                            {
                                YrColumnsList.Add("0");
                            }
                        }
                        String YrColumnsList1 = String.Join(",", YrColumnsList.ToArray());
                        ViewState["Columns_Check_Search"] = YrColumnsList1;

                        Select_Employee_report_search();

                        if (ViewState["Box_dataEmployee_Report"] != null)
                        {
                            employee_detail[] _templist_cheackMat = (employee_detail[])ViewState["Box_dataEmployee_Report"];
                            var _linqCheckMat = (from dataMat in _templist_cheackMat

                                                 select new
                                                 {
                                                     dataMat
                                                 }).ToList();

                            lblsum_list.Text = "จำนวน : " + _linqCheckMat.Count().ToString() + " รายการ / ทั้งหมด : " + GvEmployee_Report.PageCount.ToString() + " หน้า";
                        }
                        else
                        {
                            lblsum_list.Text = "จำนวน : " + "0" + " รายการ / ทั้งหมด : " + "0" + " หน้า";
                        }

                        pn_gridview_list.Visible = true;

                        pn_education.Visible = false;
                        pn_sex.Visible = false;
                        pn_age.Visible = false;
                        pn_car.Visible = false;
                        pn_count_employee.Visible = false;
                        pn_leave_employee.Visible = false;
                        pn_new_employee.Visible = false;
                        pn_race.Visible = false;

                        btnexport.Visible = false;
                        btnexport_general.Visible = true;
                        btnexport_beplus.Visible = true;
                        btnexport_Visa.Visible = true;
                        btnexport_chart_education.Visible = false;
                        btnexport_chart_sex.Visible = false;
                        btnexport_chart_age.Visible = false;
                        btnexport_chart_car.Visible = false;
                        btnexport_chart_emp_by_dept.Visible = false;
                        btnexport_chart_leave_by_dept.Visible = false;
                        btnexport_chart_nationality.Visible = false;
                        btnexport_chart_new_emp.Visible = false;

                        break;

                    case "2":
                        pn_gridview_list.Visible = false;
                        btnexport.Visible = false;
                        btnexport_general.Visible = false;
                        btnexport_beplus.Visible = false;
                        btnexport_Visa.Visible = false;

                        switch (ddlchart_type_.SelectedValue) //Chrat Type
                        {
                            case "1": //Education
                                Select_Chart_Education_colum();

                                pn_education.Visible = true;
                                pn_sex.Visible = false;
                                pn_age.Visible = false;
                                pn_car.Visible = false;
                                pn_count_employee.Visible = false;
                                pn_leave_employee.Visible = false;
                                pn_race.Visible = false;
                                pn_new_employee.Visible = false;

                                btnexport_chart_education.Visible = true;
                                btnexport_chart_sex.Visible = false;
                                btnexport_chart_age.Visible = false;
                                btnexport_chart_car.Visible = false;
                                btnexport_chart_emp_by_dept.Visible = false;
                                btnexport_chart_leave_by_dept.Visible = false;
                                btnexport_chart_nationality.Visible = false;
                                btnexport_chart_new_emp.Visible = false;
                                break;
                            case "2": //Sex
                                Select_Chart_Sex();

                                pn_education.Visible = false;
                                pn_sex.Visible = true;
                                pn_age.Visible = false;
                                pn_car.Visible = false;
                                pn_count_employee.Visible = false;
                                pn_leave_employee.Visible = false;
                                pn_race.Visible = false;
                                pn_new_employee.Visible = false;

                                btnexport_chart_education.Visible = false;
                                btnexport_chart_sex.Visible = true;
                                btnexport_chart_age.Visible = false;
                                btnexport_chart_car.Visible = false;
                                btnexport_chart_emp_by_dept.Visible = false;
                                btnexport_chart_leave_by_dept.Visible = false;
                                btnexport_chart_nationality.Visible = false;
                                btnexport_chart_new_emp.Visible = false;
                                break;
                            case "3": //Age
                                Select_Chart_Age();

                                pn_education.Visible = false;
                                pn_sex.Visible = false;
                                pn_age.Visible = true;
                                pn_car.Visible = false;
                                pn_count_employee.Visible = false;
                                pn_leave_employee.Visible = false;
                                pn_race.Visible = false;
                                pn_new_employee.Visible = false;

                                btnexport_chart_education.Visible = false;
                                btnexport_chart_sex.Visible = false;
                                btnexport_chart_age.Visible = true;
                                btnexport_chart_car.Visible = false;
                                btnexport_chart_emp_by_dept.Visible = false;
                                btnexport_chart_leave_by_dept.Visible = false;
                                btnexport_chart_nationality.Visible = false;
                                btnexport_chart_new_emp.Visible = false;
                                break;
                            case "4": //Car
                                Select_Chart_Car_colum();

                                pn_education.Visible = false;
                                pn_sex.Visible = false;
                                pn_age.Visible = false;
                                pn_car.Visible = true;
                                pn_count_employee.Visible = false;
                                pn_leave_employee.Visible = false;
                                pn_race.Visible = false;
                                pn_new_employee.Visible = false;

                                btnexport_chart_education.Visible = false;
                                btnexport_chart_sex.Visible = false;
                                btnexport_chart_age.Visible = false;
                                btnexport_chart_car.Visible = true;
                                btnexport_chart_emp_by_dept.Visible = false;
                                btnexport_chart_leave_by_dept.Visible = false;
                                btnexport_chart_nationality.Visible = false;
                                btnexport_chart_new_emp.Visible = false;
                                break;
                            case "5": //Count Employee

                                ViewState["Columns_Check_dep_Search"] = null;
                                List<String> YrColumnsList_ = new List<string>();
                                foreach (ListItem item1 in YrChkBox.Items)
                                {
                                    if (item1.Selected)
                                    {
                                        YrColumnsList_.Add(item1.Value);
                                    }
                                    else
                                    {
                                        YrColumnsList_.Add("0");
                                    }
                                }
                                String YrColumnsList1_ = String.Join(",", YrColumnsList_.ToArray());
                                ViewState["Columns_Check_dep_Search"] = YrColumnsList1_;

                                Select_Chart_Count_Employee();

                                pn_education.Visible = false;
                                pn_sex.Visible = false;
                                pn_age.Visible = false;
                                pn_car.Visible = false;
                                pn_count_employee.Visible = true;
                                pn_leave_employee.Visible = false;
                                pn_race.Visible = false;
                                pn_new_employee.Visible = false;

                                btnexport_chart_education.Visible = false;
                                btnexport_chart_sex.Visible = false;
                                btnexport_chart_age.Visible = false;
                                btnexport_chart_car.Visible = false;
                                btnexport_chart_emp_by_dept.Visible = true;
                                btnexport_chart_leave_by_dept.Visible = false;
                                btnexport_chart_nationality.Visible = false;
                                btnexport_chart_new_emp.Visible = false;
                                break;
                            case "6": //Leave Employee
                                ViewState["Columns_Check_dep_leave_Search"] = null;
                                List<String> YrColumnsList_1 = new List<string>();
                                foreach (ListItem item1 in YrChkBox.Items)
                                {
                                    if (item1.Selected)
                                    {
                                        YrColumnsList_1.Add(item1.Value);
                                    }
                                    else
                                    {
                                        YrColumnsList_1.Add("0");
                                    }
                                }
                                String YrColumnsList1_1 = String.Join(",", YrColumnsList_1.ToArray());
                                ViewState["Columns_Check_dep_leave_Search"] = YrColumnsList1_1;

                                Select_Chart_Leave_Employee();

                                pn_education.Visible = false;
                                pn_sex.Visible = false;
                                pn_age.Visible = false;
                                pn_car.Visible = false;
                                pn_count_employee.Visible = false;
                                pn_leave_employee.Visible = true;
                                pn_race.Visible = false;
                                pn_new_employee.Visible = false;

                                btnexport_chart_education.Visible = false;
                                btnexport_chart_sex.Visible = false;
                                btnexport_chart_age.Visible = false;
                                btnexport_chart_car.Visible = false;
                                btnexport_chart_emp_by_dept.Visible = false;
                                btnexport_chart_leave_by_dept.Visible = true;
                                btnexport_chart_nationality.Visible = false;
                                btnexport_chart_new_emp.Visible = false;
                                break;
                            case "7": //Race
                                Select_Chart_Race();

                                pn_education.Visible = false;
                                pn_sex.Visible = false;
                                pn_age.Visible = false;
                                pn_car.Visible = false;
                                pn_count_employee.Visible = false;
                                pn_leave_employee.Visible = false;
                                pn_race.Visible = true;
                                pn_new_employee.Visible = false;

                                btnexport_chart_education.Visible = false;
                                btnexport_chart_sex.Visible = false;
                                btnexport_chart_age.Visible = false;
                                btnexport_chart_car.Visible = false;
                                btnexport_chart_emp_by_dept.Visible = false;
                                btnexport_chart_leave_by_dept.Visible = false;
                                btnexport_chart_nationality.Visible = true;
                                btnexport_chart_new_emp.Visible = false;
                                break;
                            case "8": //New employee
                                Select_Chart_New_Employee();

                                pn_education.Visible = false;
                                pn_sex.Visible = false;
                                pn_age.Visible = false;
                                pn_car.Visible = false;
                                pn_count_employee.Visible = false;
                                pn_leave_employee.Visible = false;
                                pn_race.Visible = false;
                                pn_new_employee.Visible = true;

                                btnexport_chart_education.Visible = false;
                                btnexport_chart_sex.Visible = false;
                                btnexport_chart_age.Visible = false;
                                btnexport_chart_car.Visible = false;
                                btnexport_chart_emp_by_dept.Visible = false;
                                btnexport_chart_leave_by_dept.Visible = false;
                                btnexport_chart_nationality.Visible = false;
                                btnexport_chart_new_emp.Visible = true;
                                break;
                        }

                        break;
                }

                break;
            case "Cmdsearch_clear":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btngeneral":
                Menu_Color_Add(int.Parse(cmdArg));
                linkFvTrigger(FvInsertEmp);
                break;
            case "btnposition":
                Menu_Color_Add(int.Parse(cmdArg));
                break;
            case "btnaddress":
                Menu_Color_Add(int.Parse(cmdArg));
                break;
            case "btnhealth":
                // DropDownList ddlnation = (DropDownList)FvInsertEmp.FindControl("ddlnation");
                // TextBox txtidcard = (TextBox)FvInsertEmp.FindControl("txtidcard");
                // TextBox txtsecurityid = (TextBox)FvInsertEmp.FindControl("txtsecurityid");

                // if (ddlnation.SelectedValue == "177")
                // {
                //     txtsecurityid.Text = txtidcard.Text;
                // }
                Menu_Color_Add(int.Parse(cmdArg));
                break;
            case "btnapprove":
                Menu_Color_Add(int.Parse(cmdArg));
                break;
            case "btnasset":
                Menu_Color_Add(int.Parse(cmdArg));
                DropDownList ddlassettype_add = (DropDownList)FvInsertEmp.FindControl("ddlassettype_add");
                DropDownList ddlunittype_add = (DropDownList)FvInsertEmp.FindControl("ddlunittype_add");
                Select_asset_type(ddlassettype_add);
                Select_unit_type(ddlunittype_add);

                var dsAsset_null = new DataSet();
                dsAsset_null.Tables.Add("AssetAdd");
                dsAsset_null.Tables[0].Columns.Add("asidx", typeof(int));
                dsAsset_null.Tables[0].Columns.Add("as_name", typeof(String));
                dsAsset_null.Tables[0].Columns.Add("m0_unidx", typeof(int));
                dsAsset_null.Tables[0].Columns.Add("m0_name", typeof(String));
                dsAsset_null.Tables[0].Columns.Add("u0_number", typeof(String));
                dsAsset_null.Tables[0].Columns.Add("u0_asset_no", typeof(String));
                dsAsset_null.Tables[0].Columns.Add("u0_holderdate", typeof(String));
                dsAsset_null.Tables[0].Columns.Add("u0_detail", typeof(String));
                ViewState["vsTemp_Asset"] = dsAsset_null;
                //630
                break;
            case "btngeneral_edit":
                Menu_Color_Edit(int.Parse(cmdArg));
                break;
            case "btnposition_edit":
                Menu_Color_Edit(int.Parse(cmdArg));
                break;
            case "btnaddress_edit":
                Menu_Color_Edit(int.Parse(cmdArg));
                break;
            case "btnhealth_edit":
                DropDownList ddlnation_edit = (DropDownList)FvEdit_Detail.FindControl("ddlnation_edit");
                TextBox txtidcard_edit = (TextBox)FvEdit_Detail.FindControl("txtidcard");
                TextBox txtsecurityid_edit = (TextBox)FvEdit_Detail.FindControl("txtsecurityid");

                if (ddlnation_edit.SelectedValue == "177")
                {
                    txtsecurityid_edit.Text = txtidcard_edit.Text;
                }
                else
                {
                    txtsecurityid_edit.Text = "";
                }
                Menu_Color_Edit(int.Parse(cmdArg));
                break;
            case "btnapprove_edit":
                Menu_Color_Edit(int.Parse(cmdArg));
                break;
            case "btnadd_asset_edit":
                Menu_Color_Edit(int.Parse(cmdArg));
                break;
            case "btnSave": // Save Data ----------------------------
                var txt_startdate = (TextBox)FvInsertEmp.FindControl("txt_startdate");
                var ddlemptype = (DropDownList)FvInsertEmp.FindControl("ddlemptype");
                var txt_name_th = (TextBox)FvInsertEmp.FindControl("txt_name_th");
                var txt_surname_th = (TextBox)FvInsertEmp.FindControl("txt_surname_th");
                var txt_name_en = (TextBox)FvInsertEmp.FindControl("txt_name_en");
                var txt_surname_en = (TextBox)FvInsertEmp.FindControl("txt_surname_en");

                var ddlorg = (DropDownList)FvInsertEmp.FindControl("ddlorg");
                var ddldep = (DropDownList)FvInsertEmp.FindControl("ddldep");
                var ddlsec = (DropDownList)FvInsertEmp.FindControl("ddlsec");
                var ddlpos = (DropDownList)FvInsertEmp.FindControl("ddlpos");

                if (ddlemptype.SelectedValue == "2") //รายเดือน
                {
                    if (txt_startdate.Text != "" && ddlemptype.SelectedValue != "0" && txt_name_th.Text != "" && txt_surname_th.Text != "" & txt_name_en.Text != "" && txt_surname_en.Text != ""
                    && ddlorg.SelectedValue != "0" && ddldep.SelectedValue != "0" && ddlsec.SelectedValue != "0" && ddlpos.SelectedValue != "0")
                    {
                        Insert_Employee();
                        Page.Response.Redirect(Page.Request.Url.ToString(), true);
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "UploadError", "alert('" + "คุณกรอกข้อมูลไม่ครบถ้วน" + "')", true);
                    }
                }
                else //รายวัน
                {
                    if (txt_startdate.Text != "" && ddlemptype.SelectedValue != "0" && txt_name_th.Text != "" && txt_surname_th.Text != "" & txt_name_en.Text != "" && txt_surname_en.Text != "")
                    {
                        Insert_Employee();
                        Page.Response.Redirect(Page.Request.Url.ToString(), true);
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "UploadError", "alert('" + "คุณกรอกข้อมูลไม่ครบถ้วน" + "')", true);
                    }
                }


                break;
            case "btnUpdate":
                Update_Employee();

                var GvAsset_View_update = (GridView)FvEdit_Detail.FindControl("GvAsset_View");

                Select_Refer_view(int.Parse(ViewState["fv_emp_idx"].ToString()));
                Select_Child_view(int.Parse(ViewState["fv_emp_idx"].ToString()));
                Select_Prior_view(int.Parse(ViewState["fv_emp_idx"].ToString()));
                Select_Education_view(int.Parse(ViewState["fv_emp_idx"].ToString()));
                Select_Train_view(int.Parse(ViewState["fv_emp_idx"].ToString()));
                Select_ODSP(int.Parse(ViewState["fv_emp_idx"].ToString()));
                Select_Gv_EmpShift(int.Parse(ViewState["fv_emp_idx"].ToString()));
                Select_Gv_Asset(GvAsset_View_update, int.Parse(ViewState["fv_emp_idx"].ToString()));
                Select_Gurantee_view(int.Parse(ViewState["fv_emp_idx"].ToString()));

                var GvReferenceEdit_null = (GridView)FvEdit_Detail.FindControl("GvReference_Edit");
                var GvChildAddEdit_null = (GridView)FvEdit_Detail.FindControl("GvChildAdd_Edit");
                var GvPriEdit_null = (GridView)FvEdit_Detail.FindControl("GvPri_Edit");
                var GvEducationEdit_null = (GridView)FvEdit_Detail.FindControl("GvEducation_Edit");
                var GvTrainEdit_null = (GridView)FvEdit_Detail.FindControl("GvTrain_Edit");
                var GvPosAddEdit_null = (GridView)FvEdit_Detail.FindControl("GvPosAdd_Edit");
                var GvEmpshiftTemp_null = (GridView)FvEdit_Detail.FindControl("GvEmpshiftTemp_edit");
                var GvAsset_null = (GridView)FvEdit_Detail.FindControl("GvAsset_edit");
                var GvGurantee_null = (GridView)FvEdit_Detail.FindControl("GvGurantee_Edit");

                var ckreference_null = (CheckBox)FvEdit_Detail.FindControl("ckreference_edit");
                var chkchild_null = (CheckBox)FvEdit_Detail.FindControl("chkchild_edit");
                var chkorg_null = (CheckBox)FvEdit_Detail.FindControl("chkorg_edit");
                var ckeducation_null = (CheckBox)FvEdit_Detail.FindControl("ckeducation_edit");
                var cktraining_null = (CheckBox)FvEdit_Detail.FindControl("cktraining_edit");
                var chkposition_null = (CheckBox)FvEdit_Detail.FindControl("chkposition_edit");
                var chkasset_null = (CheckBox)FvEdit_Detail.FindControl("chkasset_edit");
                var ckgurantee_null = (CheckBox)FvEdit_Detail.FindControl("ckgurantee_edit");

                var BoxReference_null = (Panel)FvEdit_Detail.FindControl("BoxReference");
                var BoxChild_null = (Panel)FvEdit_Detail.FindControl("BoxChild");
                var BoxOrgOld_null = (Panel)FvEdit_Detail.FindControl("BoxOrgOld");
                var BoxEducation_null = (Panel)FvEdit_Detail.FindControl("BoxEducation");
                var BoxTrain_null = (Panel)FvEdit_Detail.FindControl("BoxEducation");
                var BoxPosition_null = (Panel)FvEdit_Detail.FindControl("BoxPosition");
                var BoxAsset_null = (Panel)FvEdit_Detail.FindControl("BoxAsset_Edit");
                var BoxGurantee_null = (Panel)FvEdit_Detail.FindControl("BoxGurantee");

                GvReferenceEdit_null.DataSource = null;
                GvReferenceEdit_null.DataBind();
                GvChildAddEdit_null.DataSource = null;
                GvChildAddEdit_null.DataBind();
                GvPriEdit_null.DataSource = null;
                GvPriEdit_null.DataBind();
                GvEducationEdit_null.DataSource = null;
                GvEducationEdit_null.DataBind();
                GvTrainEdit_null.DataSource = null;
                GvTrainEdit_null.DataBind();
                GvPosAddEdit_null.DataSource = null;
                GvPosAddEdit_null.DataBind();
                GvEmpshiftTemp_null.DataSource = null;
                GvEmpshiftTemp_null.DataBind();
                GvAsset_null.DataSource = null;
                GvAsset_null.DataBind();
                GvGurantee_null.DataSource = null;
                GvGurantee_null.DataBind();

                BoxAsset_null.Visible = false;
                chkasset_null.Checked = false;

                ViewState["vsTemp_Refer_edit"] = null;
                ViewState["vsTemp_Child_edit"] = null;
                ViewState["vsTemp_Prior_edit"] = null;
                ViewState["vsTemp_Education_edit"] = null;
                ViewState["vsTemp_Train_edit"] = null;
                ViewState["vsTemp_PosAdd_edit"] = null;
                ViewState["vsTemp_Empshift_edit"] = null;
                ViewState["vsTemp_Asset_Edit"] = null;
                ViewState["vsTemp_Gurantee_edit"] = null;

                var dsRefer_null = new DataSet();
                dsRefer_null.Tables.Add("Refer_add");
                dsRefer_null.Tables[0].Columns.Add("ref_fullname", typeof(String));
                dsRefer_null.Tables[0].Columns.Add("ref_position", typeof(String));
                dsRefer_null.Tables[0].Columns.Add("ref_relation", typeof(String));
                dsRefer_null.Tables[0].Columns.Add("ref_tel", typeof(String));
                ViewState["vsTemp_Refer_edit"] = dsRefer_null;

                var dsEquipment_null = new DataSet();
                dsEquipment_null.Tables.Add("Equipment_");
                dsEquipment_null.Tables[0].Columns.Add("Child_name", typeof(String));
                dsEquipment_null.Tables[0].Columns.Add("Child_num", typeof(String));
                ViewState["vsTemp_Child_edit"] = dsEquipment_null;

                var dsEquipment_org = new DataSet();
                dsEquipment_org.Tables.Add("OrgOld_edit");
                dsEquipment_org.Tables[0].Columns.Add("Pri_Nameorg", typeof(String));
                dsEquipment_org.Tables[0].Columns.Add("Pri_pos", typeof(String));
                dsEquipment_org.Tables[0].Columns.Add("Pri_startdate", typeof(String));
                dsEquipment_org.Tables[0].Columns.Add("Pri_resigndate", typeof(String));
                ViewState["vsTemp_Prior_edit"] = dsEquipment_org;

                var dsEquipment_edu = new DataSet();
                dsEquipment_edu.Tables.Add("Edu_edit");
                dsEquipment_edu.Tables[0].Columns.Add("Edu_qualification_ID", typeof(int));
                dsEquipment_edu.Tables[0].Columns.Add("Edu_qualification", typeof(String));
                dsEquipment_edu.Tables[0].Columns.Add("Edu_name", typeof(String));
                dsEquipment_edu.Tables[0].Columns.Add("Edu_branch", typeof(String));
                dsEquipment_edu.Tables[0].Columns.Add("Edu_start", typeof(String));
                dsEquipment_edu.Tables[0].Columns.Add("Edu_end", typeof(String));
                ViewState["vsTemp_Education_edit"] = dsEquipment_edu;

                var dsEquipment_train = new DataSet();
                dsEquipment_train.Tables.Add("Train_edit");
                dsEquipment_train.Tables[0].Columns.Add("Train_courses", typeof(String));
                dsEquipment_train.Tables[0].Columns.Add("Train_date", typeof(String));
                dsEquipment_train.Tables[0].Columns.Add("Train_assessment", typeof(String));
                dsEquipment_train.Tables[0].Columns.Add("Train_enddate", typeof(String));
                ViewState["vsTemp_Train_edit"] = dsEquipment_train;

                var dsPosadd_null = new DataSet();
                dsPosadd_null.Tables.Add("PosAdd_edit");
                dsPosadd_null.Tables[0].Columns.Add("OrgIDX_S", typeof(int));
                dsPosadd_null.Tables[0].Columns.Add("OrgNameTH_S", typeof(String));
                dsPosadd_null.Tables[0].Columns.Add("RDeptIDX_S", typeof(int));
                dsPosadd_null.Tables[0].Columns.Add("DeptNameTH_S", typeof(String));
                dsPosadd_null.Tables[0].Columns.Add("RSecIDX_S", typeof(int));
                dsPosadd_null.Tables[0].Columns.Add("SecNameTH_S", typeof(String));
                dsPosadd_null.Tables[0].Columns.Add("RPosIDX_S", typeof(int));
                dsPosadd_null.Tables[0].Columns.Add("PosNameTH_S", typeof(String));
                ViewState["vsTemp_PosAdd_edit"] = dsPosadd_null;

                var dsshiftadd = new DataSet();
                dsshiftadd.Tables.Add("Empshift_edit");

                dsshiftadd.Tables[0].Columns.Add("OrgIDX_add", typeof(int));
                dsshiftadd.Tables[0].Columns.Add("OrgNameTH_S", typeof(String));
                dsshiftadd.Tables[0].Columns.Add("midx_add", typeof(int));
                dsshiftadd.Tables[0].Columns.Add("TypeWork_add", typeof(String));
                dsshiftadd.Tables[0].Columns.Add("emp_idx_create_add", typeof(int));
                ViewState["vsTemp_Empshift_edit"] = dsshiftadd;

                var dsAsset_ = new DataSet();
                dsAsset_.Tables.Add("AssetAdd_Edit");
                dsAsset_.Tables[0].Columns.Add("asidx", typeof(int));
                dsAsset_.Tables[0].Columns.Add("as_name", typeof(String));
                dsAsset_.Tables[0].Columns.Add("m0_unidx", typeof(int));
                dsAsset_.Tables[0].Columns.Add("m0_name", typeof(String));
                dsAsset_.Tables[0].Columns.Add("u0_number", typeof(String));
                dsAsset_.Tables[0].Columns.Add("u0_asset_no", typeof(String));
                dsAsset_.Tables[0].Columns.Add("u0_holderdate", typeof(String));
                dsAsset_.Tables[0].Columns.Add("u0_detail", typeof(String));
                ViewState["vsTemp_Asset_Edit"] = dsAsset_;

                var dsgur_ = new DataSet();
                dsgur_.Tables.Add("Guran_edit");
                dsgur_.Tables[0].Columns.Add("gu_fullname", typeof(String));
                dsgur_.Tables[0].Columns.Add("gu_startdate", typeof(String));
                dsgur_.Tables[0].Columns.Add("gu_relation", typeof(String));
                dsgur_.Tables[0].Columns.Add("gu_tel", typeof(String));
                ViewState["vsTemp_Gurantee_edit"] = dsgur_;

                break;
            case "btnExport":
                Select_Employee_Export();

                GvExport.AllowPaging = false;
                GvExport.DataSource = ViewState["Box_dataEmployee"];
                GvExport.DataBind();

                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=EmployeeExport.xls");
                // Response.Charset = ""; set character 
                Response.Charset = "utf-8";
                Response.ContentEncoding = System.Text.Encoding.GetEncoding("utf-8");
                Response.ContentType = "application/vnd.ms-excel";

                Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
                Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>");
                //Response.Write(@"<style>.number {mso-number-format:0\.00; } </style>");

                StringWriter sw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(sw);

                for (int i = 0; i < GvExport.Rows.Count; i++)
                {
                    //Apply text style to each Row
                    GvExport.Rows[i].Attributes.Add("class", "number2");
                }
                GvExport.RenderControl(hw);

                //Amount is displayed in number format with 2 decimals
                Response.Write(@"<style>.number2 {mso-number-format:0; } </style>");

                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();

                break;
            case "btnexport_general":
                //Select_Employee_Export();
                Select_Employee_report_search();

                GvEmployee_Report.AllowPaging = false;
                GvEmployee_Report.DataSource = ViewState["Box_dataEmployee_Report"];
                GvEmployee_Report.DataBind();

                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=EmployeeExport_v2.xls");
                // Response.Charset = ""; set character 
                Response.Charset = "utf-8";
                Response.ContentEncoding = System.Text.Encoding.GetEncoding("utf-8");
                Response.ContentType = "application/vnd.ms-excel";

                Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
                Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>");
                //Response.Write(@"<style>.number {mso-number-format:0\.00; } </style>");

                StringWriter sw_4 = new StringWriter();
                HtmlTextWriter hw_4 = new HtmlTextWriter(sw_4);

                for (int i = 0; i < GvEmployee_Report.Rows.Count; i++)
                {
                    //Apply text style to each Row
                    GvEmployee_Report.Rows[i].Attributes.Add("class", "number2");
                }
                GvEmployee_Report.RenderControl(hw_4);

                //Amount is displayed in number format with 2 decimals
                Response.Write(@"<style>.number2 {mso-number-format:0; } </style>");

                Response.Output.Write(sw_4.ToString());
                Response.Flush();
                Response.End();

                break;
            case "btnExport_bplus":
                Select_Employee_Export();

                GvExport_Bplus.AllowPaging = false;
                GvExport_Bplus.DataSource = ViewState["Box_dataEmployee"];
                GvExport_Bplus.DataBind();

                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=EmployeeExport.xls");
                // Response.Charset = ""; set character 
                Response.Charset = "utf-8";
                Response.ContentEncoding = System.Text.Encoding.GetEncoding("utf-8");
                Response.ContentType = "application/vnd.ms-excel";

                Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
                Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>");

                StringWriter sw1 = new StringWriter();
                HtmlTextWriter hw1 = new HtmlTextWriter(sw1);

                for (int i = 0; i < GvExport_Bplus.Rows.Count; i++)
                {
                    //Apply text style to each Row
                    GvExport_Bplus.Rows[i].Attributes.Add("class", "number2");
                    GvExport_Bplus.Rows[i].Cells[35].Attributes.Add("class", "textmode");
                    GvExport_Bplus.Rows[i].Cells[64].Attributes.Add("class", "textmode");
                    GvExport_Bplus.Rows[i].Cells[65].Attributes.Add("class", "textmode");
                    GvExport_Bplus.Rows[i].Cells[66].Attributes.Add("class", "textmode");
                    GvExport_Bplus.Rows[i].Cells[67].Attributes.Add("class", "textmode");
                    GvExport_Bplus.Rows[i].Cells[70].Attributes.Add("class", "textmode");
                    GvExport_Bplus.Rows[i].Cells[71].Attributes.Add("class", "textmode");
                    GvExport_Bplus.Rows[i].Cells[72].Attributes.Add("class", "textmode");

                    GvExport_Bplus.Rows[i].Cells[14].Attributes.Add("class", "numbertotext");
                    GvExport_Bplus.Rows[i].Cells[17].Attributes.Add("class", "numbertotext");
                    GvExport_Bplus.Rows[i].Cells[18].Attributes.Add("class", "numbertotext");
                    GvExport_Bplus.Rows[i].Cells[20].Attributes.Add("class", "numbertotext");
                    GvExport_Bplus.Rows[i].Cells[21].Attributes.Add("class", "numbertotext");
                    GvExport_Bplus.Rows[i].Cells[25].Attributes.Add("class", "numbertotext");

                    GvExport_Bplus.Rows[i].Cells[23].Attributes.Add("class", "number1");
                    GvExport_Bplus.Rows[i].Cells[39].Attributes.Add("class", "number1");
                    GvExport_Bplus.Rows[i].Cells[40].Attributes.Add("class", "number1");
                    GvExport_Bplus.Rows[i].Cells[56].Attributes.Add("class", "number1");
                }
                GvExport_Bplus.RenderControl(hw1);

                //Amount is displayed in number format with 2 decimals
                Response.Write(@"<style>.number2 {mso-number-format:00; } </style>");
                Response.Write(@"<style>.number1 {mso-number-format:0; } </style>");
                string style1 = @"<style> .numbertotext { mso-number-format:\@; font-family: myFirstFont; } </style>";
                string style3 = @"<style> .textmode{mso-number-format:\@; };.Nums{mso-number-format:_(* #,###.00_);};.unwrap{wrap:false}</style>";
                Response.Write(style1);
                Response.Write(style3);
                Response.Output.Write(sw1.ToString());
                Response.Flush();
                Response.End();
                break;
            case "btnexport_visa":
                Select_Employee_Export_Exp_visa();

                Gv_Export_Visa.AllowPaging = false;
                Gv_Export_Visa.DataSource = ViewState["Box_Visa_dataEmployee"];
                Gv_Export_Visa.DataBind();

                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=EmployeeExport_visa.xls");
                // Response.Charset = ""; set character 
                Response.Charset = "utf-8";
                Response.ContentEncoding = System.Text.Encoding.GetEncoding("utf-8");
                Response.ContentType = "application/vnd.ms-excel";

                Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
                Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>");
                //Response.Write(@"<style>.number {mso-number-format:0\.00; } </style>");

                StringWriter sw2 = new StringWriter();
                HtmlTextWriter hw2 = new HtmlTextWriter(sw2);

                for (int i = 0; i < Gv_Export_Visa.Rows.Count; i++)
                {
                    //Apply text style to each Row
                    Gv_Export_Visa.Rows[i].Attributes.Add("class", "number2");
                }
                Gv_Export_Visa.RenderControl(hw2);

                //Amount is displayed in number format with 2 decimals
                Response.Write(@"<style>.number2 {mso-number-format:0; } </style>");

                Response.Output.Write(sw2.ToString());
                Response.Flush();
                Response.End();

                break;
            case "btnsearchtranfer":
                var FvDetailUser = (FormView)ViewSetApprove.FindControl("FvDetailUser");
                var GvRsecApproveDatabase = (GridView)ViewSetApprove.FindControl("GvRsecApproveDatabase");
                var txt_search_empcode = (TextBox)ViewSetApprove.FindControl("txt_search_empcode");

                Select_Fv_Detail(FvDetailUser, txt_search_empcode.Text);
                Select_ApproveEmployee(GvRsecApproveDatabase, txt_search_empcode.Text);
                break;
            case "btnsearchtranfer_temp":
                var FvDetailUser_temp = (FormView)ViewSetApprove.FindControl("FvDetailUser_temp");
                var GvRsecApproveTemp = (GridView)ViewSetApprove.FindControl("GvRsecApproveTemp");
                var txt_search_empcode_temp = (TextBox)ViewSetApprove.FindControl("txt_search_empcode_temp");

                Select_Fv_Detail(FvDetailUser_temp, txt_search_empcode_temp.Text);
                break;
            case "AddChild":

                var GvChildAdd = (GridView)FvInsertEmp.FindControl("GvChildAdd");
                var txtchildname = (TextBox)FvInsertEmp.FindControl("txtchildname");
                var ddlchildnumber = (DropDownList)FvInsertEmp.FindControl("ddlchildnumber");

                var dsEquiment = (DataSet)ViewState["vsTemp_Child"];

                var drEquiment = dsEquiment.Tables[0].NewRow();
                drEquiment["Child_name"] = txtchildname.Text;
                drEquiment["Child_num"] = ddlchildnumber.SelectedValue;

                dsEquiment.Tables[0].Rows.Add(drEquiment);

                ViewState["vsTemp_Child"] = dsEquiment;

                GvChildAdd.DataSource = dsEquiment.Tables[0];
                GvChildAdd.DataBind();

                txtchildname.Text = null;
                ddlchildnumber.SelectedValue = null;

                break;
            case "AddReference":
                var GvReferenceAdd = (GridView)FvInsertEmp.FindControl("GvReference");
                var txtfullname_add = (TextBox)FvInsertEmp.FindControl("txtfullname_add");
                var txtposition_add = (TextBox)FvInsertEmp.FindControl("txtposition_add");
                var txtrelation_add = (TextBox)FvInsertEmp.FindControl("txtrelation_add");
                var txttel_add = (TextBox)FvInsertEmp.FindControl("txttel_add");

                var dsRefer = (DataSet)ViewState["vsTemp_Refer"];

                var drRefer = dsRefer.Tables[0].NewRow();
                drRefer["ref_fullname"] = txtfullname_add.Text;
                drRefer["ref_position"] = txtposition_add.Text;
                drRefer["ref_relation"] = txtrelation_add.Text;
                drRefer["ref_tel"] = txttel_add.Text;

                dsRefer.Tables[0].Rows.Add(drRefer);

                ViewState["vsTemp_Refer"] = dsRefer;
                GvReferenceAdd.DataSource = dsRefer.Tables[0];
                GvReferenceAdd.DataBind();

                txtfullname_add.Text = null;
                txtposition_add.Text = null;
                txtrelation_add.Text = null;
                txttel_add.Text = null;

                break;
            case "AddPrior":

                var GvPri = (GridView)FvInsertEmp.FindControl("GvPri");
                var txtorgold = (TextBox)FvInsertEmp.FindControl("txtorgold");
                var txtposold = (TextBox)FvInsertEmp.FindControl("txtposold");
                //var txtworktimeold = (TextBox)FvInsertEmp.FindControl("txtworktimeold");
                var txtstartdate_ex = (TextBox)FvInsertEmp.FindControl("txtstartdate_ex");
                var txtresigndate_ex = (TextBox)FvInsertEmp.FindControl("txtresigndate_ex");

                var dsOrg = (DataSet)ViewState["vsTemp_Prior"];

                var drOrg = dsOrg.Tables[0].NewRow();
                drOrg["Pri_Nameorg"] = txtorgold.Text;
                drOrg["Pri_pos"] = txtposold.Text;
                //drOrg["Pri_worktime"] = txtworktimeold.Text;
                drOrg["Pri_startdate"] = txtstartdate_ex.Text;
                drOrg["Pri_resigndate"] = txtresigndate_ex.Text;

                dsOrg.Tables[0].Rows.Add(drOrg);

                ViewState["vsTemp_Prior"] = dsOrg;

                GvPri.DataSource = dsOrg.Tables[0];
                GvPri.DataBind();

                txtorgold.Text = null;
                txtposold.Text = null;
                txtstartdate_ex.Text = null;
                txtresigndate_ex.Text = null;

                break;
            case "AddEducation":

                var GvEducation = (GridView)FvInsertEmp.FindControl("GvEducation");
                var ddleducationback = (DropDownList)FvInsertEmp.FindControl("ddleducationback");
                var txtschoolname = (TextBox)FvInsertEmp.FindControl("txtschoolname");
                var txtstarteducation = (TextBox)FvInsertEmp.FindControl("txtstarteducation");
                var txtendeducation = (TextBox)FvInsertEmp.FindControl("txtendeducation");
                var txtstudy = (TextBox)FvInsertEmp.FindControl("txtstudy");

                var dsEdu = (DataSet)ViewState["vsTemp_Education"];

                var drEdu = dsEdu.Tables[0].NewRow();
                drEdu["Edu_qualification_ID"] = ddleducationback.SelectedValue;
                drEdu["Edu_qualification"] = ddleducationback.SelectedItem.Text;
                drEdu["Edu_name"] = txtschoolname.Text;
                drEdu["Edu_branch"] = txtstudy.Text;
                drEdu["Edu_start"] = txtstarteducation.Text;
                drEdu["Edu_end"] = txtendeducation.Text;

                dsEdu.Tables[0].Rows.Add(drEdu);

                ViewState["vsTemp_Education"] = dsEdu;

                GvEducation.DataSource = dsEdu.Tables[0];
                GvEducation.DataBind();

                ddleducationback.SelectedValue = null;
                txtschoolname.Text = null;
                txtstarteducation.Text = null;
                txtendeducation.Text = null;
                txtstudy.Text = null;

                break;
            case "Addtraining":

                var GvTrain = (GridView)FvInsertEmp.FindControl("GvTrain");
                var txttraincourses = (TextBox)FvInsertEmp.FindControl("txttraincourses");
                var txttraindate = (TextBox)FvInsertEmp.FindControl("txttraindate");
                var txttrainassessment = (TextBox)FvInsertEmp.FindControl("txttrainassessment");
                var txttraindateend = (TextBox)FvInsertEmp.FindControl("txttraindateend");

                var dsTrain = (DataSet)ViewState["vsTemp_Train"];

                var drTrain = dsTrain.Tables[0].NewRow();
                drTrain["Train_courses"] = txttraincourses.Text;
                drTrain["Train_date"] = txttraindate.Text;
                drTrain["Train_assessment"] = txttrainassessment.Text;
                drTrain["Train_enddate"] = txttraindateend.Text;

                dsTrain.Tables[0].Rows.Add(drTrain);

                ViewState["vsTemp_Train"] = dsTrain;

                GvTrain.DataSource = dsTrain.Tables[0];
                GvTrain.DataBind();

                txttraincourses.Text = null;
                txttraindate.Text = null;
                txttrainassessment.Text = null;
                txttraindateend.Text = null;

                break;
            case "AddPosAdd":

                var GvPosAdd = (GridView)FvInsertEmp.FindControl("GvPosAdd");
                var ddlorg_add = (DropDownList)FvInsertEmp.FindControl("ddlorg_add");
                var ddldep_add = (DropDownList)FvInsertEmp.FindControl("ddldep_add");
                var ddlsec_add = (DropDownList)FvInsertEmp.FindControl("ddlsec_add");
                var ddlpos_add = (DropDownList)FvInsertEmp.FindControl("ddlpos_add");


                var dsPosAdd = (DataSet)ViewState["vsTemp_PosAdd"];

                var drPosAdd = dsPosAdd.Tables[0].NewRow();
                drPosAdd["OrgIDX_S"] = int.Parse(ddlorg_add.SelectedValue);
                drPosAdd["OrgNameTH_S"] = ddlorg_add.SelectedItem.Text;
                drPosAdd["RDeptIDX_S"] = int.Parse(ddldep_add.SelectedValue);
                drPosAdd["DeptNameTH_S"] = ddldep_add.SelectedItem.Text;
                drPosAdd["RSecIDX_S"] = int.Parse(ddlsec_add.SelectedValue);
                drPosAdd["SecNameTH_S"] = ddlsec_add.SelectedItem.Text;
                drPosAdd["RPosIDX_S"] = int.Parse(ddlpos_add.SelectedValue);
                drPosAdd["PosNameTH_S"] = ddlpos_add.SelectedItem.Text;

                dsPosAdd.Tables[0].Rows.Add(drPosAdd);

                ViewState["vsTemp_PosAdd"] = dsPosAdd;

                GvPosAdd.DataSource = dsPosAdd.Tables[0];
                GvPosAdd.DataBind();

                ddlorg_add.SelectedValue = null;
                ddldep_add.SelectedValue = null;
                ddlsec_add.SelectedValue = null;
                ddlpos_add.SelectedValue = null;

                break;
            case "cmdInsertPositionCenAction":

                setInsertRePosition();

                break;
            case "cmdInsertPositionCenAction_Edit":

                setInsertEditRePosition();

                break;
            case "AddAsset":
                //771
                var GvAsset_add = (GridView)FvInsertEmp.FindControl("GvAsset");
                var ddlassettype_add_ = (DropDownList)FvInsertEmp.FindControl("ddlassettype_add");
                var txtnum_add = (TextBox)FvInsertEmp.FindControl("txtnum_add");
                var ddlunittype_add_ = (DropDownList)FvInsertEmp.FindControl("ddlunittype_add");
                var txtassnumber_add = (TextBox)FvInsertEmp.FindControl("txtassnumber_add");
                var txtholderdate_add = (TextBox)FvInsertEmp.FindControl("txtholderdate_add");
                var txtdetail_add = (TextBox)FvInsertEmp.FindControl("txtdetail_add");

                var dsAssetAdd = (DataSet)ViewState["vsTemp_Asset"];

                var drAssetAdd = dsAssetAdd.Tables[0].NewRow();
                drAssetAdd["asidx"] = int.Parse(ddlassettype_add_.SelectedValue);
                drAssetAdd["as_name"] = ddlassettype_add_.SelectedItem.Text;
                drAssetAdd["m0_unidx"] = int.Parse(ddlunittype_add_.SelectedValue);
                drAssetAdd["m0_name"] = ddlunittype_add_.SelectedItem.Text;
                drAssetAdd["u0_number"] = txtnum_add.Text;
                drAssetAdd["u0_asset_no"] = txtassnumber_add.Text;
                drAssetAdd["u0_holderdate"] = txtholderdate_add.Text;
                drAssetAdd["u0_detail"] = txtdetail_add.Text;

                dsAssetAdd.Tables[0].Rows.Add(drAssetAdd);

                ViewState["vsTemp_Asset"] = dsAssetAdd;
                GvAsset_add.DataSource = dsAssetAdd.Tables[0];
                GvAsset_add.DataBind();

                txtholderdate_add.Text = null;
                txtnum_add.Text = null;
                txtassnumber_add.Text = null;
                txtdetail_add.Text = null;

                //631
                break;

            case "AddPosAdd_Edit":

                var GvPosAdd_Edit = (GridView)FvEdit_Detail.FindControl("GvPosAdd_Edit");
                var ddlorg_add_edit = (DropDownList)FvEdit_Detail.FindControl("ddlorg_add_edit");
                var ddldep_add_edit = (DropDownList)FvEdit_Detail.FindControl("ddldep_add_edit");
                var ddlsec_add_edit = (DropDownList)FvEdit_Detail.FindControl("ddlsec_add_edit");
                var ddlpos_add_edit = (DropDownList)FvEdit_Detail.FindControl("ddlpos_add_edit");

                var dsPosAdd_edit = (DataSet)ViewState["vsTemp_PosAdd_edit"];

                var drPosAdd_edit = dsPosAdd_edit.Tables[0].NewRow();
                drPosAdd_edit["OrgIDX_S"] = int.Parse(ddlorg_add_edit.SelectedValue);
                drPosAdd_edit["OrgNameTH_S"] = ddlorg_add_edit.SelectedItem.Text;
                drPosAdd_edit["RDeptIDX_S"] = int.Parse(ddldep_add_edit.SelectedValue);
                drPosAdd_edit["DeptNameTH_S"] = ddldep_add_edit.SelectedItem.Text;
                drPosAdd_edit["RSecIDX_S"] = int.Parse(ddlsec_add_edit.SelectedValue);
                drPosAdd_edit["SecNameTH_S"] = ddlsec_add_edit.SelectedItem.Text;
                drPosAdd_edit["RPosIDX_S"] = int.Parse(ddlpos_add_edit.SelectedValue);
                drPosAdd_edit["PosNameTH_S"] = ddlpos_add_edit.SelectedItem.Text;

                dsPosAdd_edit.Tables[0].Rows.Add(drPosAdd_edit);

                ViewState["vsTemp_PosAdd_edit"] = dsPosAdd_edit;

                GvPosAdd_Edit.DataSource = dsPosAdd_edit.Tables[0];
                GvPosAdd_Edit.DataBind();

                ddlorg_add_edit.SelectedValue = null;
                ddldep_add_edit.SelectedValue = null;
                ddlsec_add_edit.SelectedValue = null;
                ddlpos_add_edit.SelectedValue = null;

                break;

            case "AddShiftAdd_Edit":

                var GvEmpshiftTemp_edit = (GridView)FvEdit_Detail.FindControl("GvEmpshiftTemp_edit");
                var ddlorg_add_group = (DropDownList)FvEdit_Detail.FindControl("ddlorg_add_group");
                var ddlshift_add_group = (DropDownList)FvEdit_Detail.FindControl("ddlshift_add_group");

                var dsshiftAdd_edit = (DataSet)ViewState["vsTemp_Empshift_edit"];

                var drshiftAdd_edit = dsshiftAdd_edit.Tables[0].NewRow();
                drshiftAdd_edit["OrgIDX_add"] = int.Parse(ddlorg_add_group.SelectedValue);
                drshiftAdd_edit["OrgNameTH_S"] = ddlorg_add_group.SelectedItem.Text;
                drshiftAdd_edit["midx_add"] = int.Parse(ddlshift_add_group.SelectedValue);
                drshiftAdd_edit["TypeWork_add"] = ddlshift_add_group.SelectedItem.Text;
                drshiftAdd_edit["emp_idx_create_add"] = int.Parse(ViewState["EmpIDX"].ToString());

                dsshiftAdd_edit.Tables[0].Rows.Add(drshiftAdd_edit);

                ViewState["vsTemp_Empshift_edit"] = dsshiftAdd_edit;

                GvEmpshiftTemp_edit.DataSource = dsshiftAdd_edit.Tables[0];
                GvEmpshiftTemp_edit.DataBind();

                break;
            case "AddChild_Edit":

                var GvChildAdd_Edit = (GridView)FvEdit_Detail.FindControl("GvChildAdd_Edit");
                var txtchildname_edit = (TextBox)FvEdit_Detail.FindControl("txtchildname_edit");
                var ddlchildnumber_edit = (DropDownList)FvEdit_Detail.FindControl("ddlchildnumber_edit");

                var dsEquiment_ = (DataSet)ViewState["vsTemp_Child_edit"];

                var drEquiment_ = dsEquiment_.Tables[0].NewRow();
                drEquiment_["Child_name"] = txtchildname_edit.Text;
                drEquiment_["Child_num"] = ddlchildnumber_edit.SelectedValue;

                dsEquiment_.Tables[0].Rows.Add(drEquiment_);

                ViewState["vsTemp_Child_edit"] = dsEquiment_;

                GvChildAdd_Edit.DataSource = dsEquiment_.Tables[0];
                GvChildAdd_Edit.DataBind();

                txtchildname_edit.Text = null;
                ddlchildnumber_edit.SelectedValue = null;

                break;
            case "AddReference_Edit":
                var GvReferenceEdit = (GridView)FvEdit_Detail.FindControl("GvReference_Edit");
                var txtfullname_edit = (TextBox)FvEdit_Detail.FindControl("txtfullname_add");
                var txtposition_edit = (TextBox)FvEdit_Detail.FindControl("txtposition_add");
                var txtrelation_edit = (TextBox)FvEdit_Detail.FindControl("txtrelation_add");
                var txttel_edit = (TextBox)FvEdit_Detail.FindControl("txttel_add");

                var dsRefer_edit = (DataSet)ViewState["vsTemp_Refer_edit"];

                var drRefer_edit = dsRefer_edit.Tables[0].NewRow();
                drRefer_edit["ref_fullname"] = txtfullname_edit.Text;
                drRefer_edit["ref_position"] = txtposition_edit.Text;
                drRefer_edit["ref_relation"] = txtrelation_edit.Text;
                drRefer_edit["ref_tel"] = txttel_edit.Text;

                dsRefer_edit.Tables[0].Rows.Add(drRefer_edit);

                ViewState["vsTemp_Refer_edit"] = dsRefer_edit;
                GvReferenceEdit.DataSource = dsRefer_edit.Tables[0];
                GvReferenceEdit.DataBind();

                txtfullname_edit.Text = null;
                txtposition_edit.Text = null;
                txtrelation_edit.Text = null;
                txttel_edit.Text = null;

                break;

            case "AddPrior_Edit":

                var GvPri_Edit = (GridView)FvEdit_Detail.FindControl("GvPri_Edit");
                var txtorgold_edit = (TextBox)FvEdit_Detail.FindControl("txtorgold_edit");
                var txtposold_edit = (TextBox)FvEdit_Detail.FindControl("txtposold_edit");
                //var txtworktimeold_edit = (TextBox)FvEdit_Detail.FindControl("txtworktimeold_edit");
                var txtstartdate_edit = (TextBox)FvEdit_Detail.FindControl("txtstartdate_edit");
                var txtresigndate_edit = (TextBox)FvEdit_Detail.FindControl("txtresigndate_edit");

                var dsOrg_edit = (DataSet)ViewState["vsTemp_Prior_edit"];

                var drOrg_edit = dsOrg_edit.Tables[0].NewRow();
                drOrg_edit["Pri_Nameorg"] = txtorgold_edit.Text;
                drOrg_edit["Pri_pos"] = txtposold_edit.Text;
                //drOrg_edit["Pri_worktime"] = txtworktimeold_edit.Text;
                drOrg_edit["Pri_startdate"] = txtstartdate_edit.Text;
                drOrg_edit["Pri_resigndate"] = txtresigndate_edit.Text;

                dsOrg_edit.Tables[0].Rows.Add(drOrg_edit);

                ViewState["vsTemp_Prior_edit"] = dsOrg_edit;

                GvPri_Edit.DataSource = dsOrg_edit.Tables[0];
                GvPri_Edit.DataBind();

                txtorgold_edit.Text = null;
                txtposold_edit.Text = null;
                txtstartdate_edit.Text = null;
                txtresigndate_edit.Text = null;

                break;

            case "AddEducation_Edit":

                var GvEducation_Edit = (GridView)FvEdit_Detail.FindControl("GvEducation_Edit");
                var ddleducationback_edit = (DropDownList)FvEdit_Detail.FindControl("ddleducationback_edit");
                var txtschoolname_edit = (TextBox)FvEdit_Detail.FindControl("txtschoolname_edit");
                var txtstarteducation_edit = (TextBox)FvEdit_Detail.FindControl("txtstarteducation_edit");
                var txtendeducation_edit = (TextBox)FvEdit_Detail.FindControl("txtendeducation_edit");
                var txtstudy_edit = (TextBox)FvEdit_Detail.FindControl("txtstudy_edit");

                var dsEdu_edit = (DataSet)ViewState["vsTemp_Education_edit"];

                var drEdu_edit = dsEdu_edit.Tables[0].NewRow();
                drEdu_edit["Edu_qualification_ID"] = ddleducationback_edit.SelectedValue;
                drEdu_edit["Edu_qualification"] = ddleducationback_edit.SelectedItem.Text;
                drEdu_edit["Edu_name"] = txtschoolname_edit.Text;
                drEdu_edit["Edu_branch"] = txtstudy_edit.Text;
                drEdu_edit["Edu_start"] = txtstarteducation_edit.Text;
                drEdu_edit["Edu_end"] = txtendeducation_edit.Text;

                dsEdu_edit.Tables[0].Rows.Add(drEdu_edit);

                ViewState["vsTemp_Education_edit"] = dsEdu_edit;

                GvEducation_Edit.DataSource = dsEdu_edit.Tables[0];
                GvEducation_Edit.DataBind();

                ddleducationback_edit.SelectedValue = null;
                txtschoolname_edit.Text = null;
                txtstarteducation_edit.Text = null;
                txtendeducation_edit.Text = null;
                txtstudy_edit.Text = null;

                break;

            case "Addtraining_Edit":

                var GvTrain_Edit = (GridView)FvEdit_Detail.FindControl("GvTrain_Edit");
                var txttraincourses_edit = (TextBox)FvEdit_Detail.FindControl("txttraincourses_edit");
                var txttraindate_edit = (TextBox)FvEdit_Detail.FindControl("txttraindate_edit");
                var txttrainassessment_edit = (TextBox)FvEdit_Detail.FindControl("txttrainassessment_edit");
                var txttrainenddate_edit = (TextBox)FvEdit_Detail.FindControl("txttrainenddate_edit");

                var dsTrain_edit = (DataSet)ViewState["vsTemp_Train_edit"];

                var drTrain_edit = dsTrain_edit.Tables[0].NewRow();
                drTrain_edit["Train_courses"] = txttraincourses_edit.Text;
                drTrain_edit["Train_date"] = txttraindate_edit.Text;
                drTrain_edit["Train_assessment"] = txttrainassessment_edit.Text;
                drTrain_edit["Train_enddate"] = txttrainenddate_edit.Text;

                dsTrain_edit.Tables[0].Rows.Add(drTrain_edit);

                ViewState["vsTemp_Train_edit"] = dsTrain_edit;

                GvTrain_Edit.DataSource = dsTrain_edit.Tables[0];
                GvTrain_Edit.DataBind();

                txttraincourses_edit.Text = null;
                txttraindate_edit.Text = null;
                txttrainassessment_edit.Text = null;
                txttrainenddate_edit.Text = null;
                break;

            case "AddAsset_edit":

                var GvAsset_edit = (GridView)FvEdit_Detail.FindControl("GvAsset_edit");
                var ddlassettype_edit = (DropDownList)FvEdit_Detail.FindControl("ddlassettype_add");
                var txtnum_edit = (TextBox)FvEdit_Detail.FindControl("txtnum_add");
                var ddlunittype_edit = (DropDownList)FvEdit_Detail.FindControl("ddlunittype_add");
                var txtassnumber_edit = (TextBox)FvEdit_Detail.FindControl("txtassnumber_add");
                var txtholderdate_edit = (TextBox)FvEdit_Detail.FindControl("txtholderdate_add");
                var txtdetail_edit = (TextBox)FvEdit_Detail.FindControl("txtdetail_add");


                var dsAssetEdit = (DataSet)ViewState["vsTemp_Asset_Edit"];

                var drAssetEdit = dsAssetEdit.Tables[0].NewRow();
                drAssetEdit["asidx"] = int.Parse(ddlassettype_edit.SelectedValue);
                drAssetEdit["as_name"] = ddlassettype_edit.SelectedItem.Text;
                drAssetEdit["m0_unidx"] = int.Parse(ddlunittype_edit.SelectedValue);
                drAssetEdit["m0_name"] = ddlunittype_edit.SelectedItem.Text;
                drAssetEdit["u0_number"] = txtnum_edit.Text;
                drAssetEdit["u0_asset_no"] = txtassnumber_edit.Text;
                drAssetEdit["u0_holderdate"] = txtholderdate_edit.Text;
                drAssetEdit["u0_detail"] = txtdetail_edit.Text;

                dsAssetEdit.Tables[0].Rows.Add(drAssetEdit);

                ViewState["vsTemp_Asset_Edit"] = dsAssetEdit;
                GvAsset_edit.DataSource = dsAssetEdit.Tables[0];
                GvAsset_edit.DataBind();
                break;

            case "DeleteGvPos":

                string[] arg1 = new string[9];
                arg1 = e.CommandArgument.ToString().Split(';');
                int EODSPIDX = int.Parse(arg1[0]);
                int OrgIDX = int.Parse(arg1[1]);
                int RDeptIDX = int.Parse(arg1[3]);
                int RSecIDX = int.Parse(arg1[5]);
                int RPosIDX = int.Parse(arg1[8]);

                var GvPosAdd_View = (GridView)FvEdit_Detail.FindControl("GvPosAdd_View");

                _dataEmployee.ODSPRelation_list = new ODSP_List[1];
                ODSP_List _dataEmployee_ = new ODSP_List();

                _dataEmployee_.EODSPIDX = EODSPIDX;
                _dataEmployee_.EmpIDXUser = int.Parse(ViewState["EmpIDX"].ToString());

                _dataEmployee_.OrgIDX_S = OrgIDX;
                _dataEmployee_.RDeptIDX_S = RDeptIDX;
                _dataEmployee_.RSecIDX_S = RSecIDX;
                _dataEmployee_.RPosIDX_S = RPosIDX;

                _dataEmployee.ODSPRelation_list[0] = _dataEmployee_;
                _dataEmployee = callServiceEmployee_Check(_urlSetODSPList_Delete, _dataEmployee);

                Select_FvEdit_Detail(int.Parse(ViewState["fv_emp_idx"].ToString()));
                Menu_Color_Edit(2);
                txtfocus.Focus();

                if (int.Parse(_dataEmployee.return_code.ToString()) == 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('*** ไม่สามารถลบตำแหน่งหลักได้ กรุณาเปลี่ยนสถานะก่อน')", true);
                }

                GvPosAdd_View.DataSource = ViewState["BoxGvPosAdd_View"];
                GvPosAdd_View.DataBind();
                break;

            case "cmdDelGvPositionList":

                var GvPositionList = (GridView)FvEdit_Detail.FindControl("GvPositionList");

                _dataEmployee.cen_position_emp_r0_list = new cen_position_emp_r0_detail[1];
                cen_position_emp_r0_detail position_emp_r0_del = new cen_position_emp_r0_detail();

                position_emp_r0_del.pos_emp_idx = int.Parse(cmdArg);
                position_emp_r0_del.cemp_idx = int.Parse(ViewState["EmpIDX"].ToString());

                _dataEmployee.cen_position_emp_r0_list[0] = position_emp_r0_del;
                _dataEmployee = callServiceEmployee_Check(_urlSetDelPositionCen, _dataEmployee);

                
                //txtfocus.Focus();
                //litDebug.Text = _dataEmployee.return_code.ToString();

                if (int.Parse(_dataEmployee.return_code.ToString()) == 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('*** ไม่สามารถลบตำแหน่งหลักได้ กรุณาเปลี่ยนสถานะก่อน')", true);
                    break;
                }

                // Select_FvEdit_Detail(int.Parse(ViewState["fv_emp_idx"].ToString()));
                // Menu_Color_Edit(2);
                

                GvPositionList.DataSource = ViewState["vs_PositionList_View"];
                GvPositionList.DataBind();

                GvPositionList.Focus();

                
                break;

            case "DeleteGvRefer_View":
                string cmdArg_ref = e.CommandArgument.ToString();
                int ReIDX = int.Parse(cmdArg_ref);

                var GvReference_View = (GridView)FvEdit_Detail.FindControl("GvReference_View");

                _dataEmployee.BoxReferent_list = new Referent_List[1];
                Referent_List _dataEmployee_ref = new Referent_List();

                _dataEmployee_ref.ReIDX = ReIDX;

                _dataEmployee.BoxReferent_list[0] = _dataEmployee_ref;
                _dataEmployee = callServiceEmployee(_urlSetDeleteReferList, _dataEmployee);

                Select_FvEdit_Detail(int.Parse(ViewState["fv_emp_idx"].ToString()));
                Menu_Color_Edit(1);
                txtfocus.Focus();
                GvReference_View.EditIndex = -1;
                GvReference_View.DataSource = ViewState["BoxGvRefer_view"];
                GvReference_View.DataBind();
                break;

            case "DeleteGvChildAdd_View":
                string cmdArg2 = e.CommandArgument.ToString();
                int CHIDX = int.Parse(cmdArg2);

                var GvChildAdd_View = (GridView)FvEdit_Detail.FindControl("GvChildAdd_View");

                _dataEmployee.BoxChild_list = new Child_List[1];
                Child_List _dataEmployee_1 = new Child_List();

                _dataEmployee_1.CHIDX = CHIDX;

                _dataEmployee.BoxChild_list[0] = _dataEmployee_1;
                _dataEmployee = callServiceEmployee(_urlSetDeleteChildList, _dataEmployee);

                Select_FvEdit_Detail(int.Parse(ViewState["fv_emp_idx"].ToString()));
                Menu_Color_Edit(1);
                txtfocus.Focus();
                GvChildAdd_View.EditIndex = -1;
                GvChildAdd_View.DataSource = ViewState["BoxGvChild_view"];
                GvChildAdd_View.DataBind();
                break;

            case "DeleteGvPri_View":
                string cmdArg3 = e.CommandArgument.ToString();
                int ExperIDX = int.Parse(cmdArg3);

                var GvPri_View = (GridView)FvEdit_Detail.FindControl("GvPri_View");

                _dataEmployee.BoxPrior_list = new Prior_List[1];
                Prior_List _dataEmployee_2 = new Prior_List();

                _dataEmployee_2.ExperIDX = ExperIDX;

                _dataEmployee.BoxPrior_list[0] = _dataEmployee_2;
                _dataEmployee = callServiceEmployee(_urlSetDeletePriorList, _dataEmployee);

                Select_FvEdit_Detail(int.Parse(ViewState["fv_emp_idx"].ToString()));
                Menu_Color_Edit(1);
                txtfocus.Focus();
                GvPri_View.EditIndex = -1;
                GvPri_View.DataSource = ViewState["BoxGvPrior_view"];
                GvPri_View.DataBind();
                break;

            case "DeleteGvEducation_View":
                string cmdArg4 = e.CommandArgument.ToString();
                int EDUIDX = int.Parse(cmdArg4);

                var GvEducation_View = (GridView)FvEdit_Detail.FindControl("GvEducation_View");

                _dataEmployee.BoxEducation_list = new Education_List[1];
                Education_List _dataEmployee_3 = new Education_List();

                _dataEmployee_3.EDUIDX = EDUIDX;

                _dataEmployee.BoxEducation_list[0] = _dataEmployee_3;
                _dataEmployee = callServiceEmployee(_urlSetDeleteEducationList, _dataEmployee);

                Select_FvEdit_Detail(int.Parse(ViewState["fv_emp_idx"].ToString()));
                Menu_Color_Edit(1);
                txtfocus.Focus();
                GvEducation_View.EditIndex = -1;
                GvEducation_View.DataSource = ViewState["BoxGvEducation_view"];
                GvEducation_View.DataBind();
                break;

            case "DeleteGvTrain_View":
                string cmdArg5 = e.CommandArgument.ToString();
                int TNIDX = int.Parse(cmdArg5);

                var GvTrain_View = (GridView)FvEdit_Detail.FindControl("GvTrain_View");

                _dataEmployee.BoxTrain_list = new Train_List[1];
                Train_List _dataEmployee_4 = new Train_List();

                _dataEmployee_4.TNIDX = TNIDX;

                _dataEmployee.BoxTrain_list[0] = _dataEmployee_4;
                _dataEmployee = callServiceEmployee(_urlSetDeleteTrainList, _dataEmployee);

                Select_FvEdit_Detail(int.Parse(ViewState["fv_emp_idx"].ToString()));
                Menu_Color_Edit(1);
                txtfocus.Focus();
                GvTrain_View.EditIndex = -1;
                GvTrain_View.DataSource = ViewState["BoxGvTrain_view"];
                GvTrain_View.DataBind();
                break;
            case "btnselect":
                GridView GvRsecApprove_Temp = (GridView)ViewSetApprove.FindControl("GvRsecApproveTemp");
                GridView GvRsecApprove_Database = (GridView)ViewSetApprove.FindControl("GvRsecApproveDatabase");
                string[] arg_sw = new string[9];
                arg_sw = e.CommandArgument.ToString().Split(';');
                int org_idx = int.Parse(arg_sw[0]);
                string org_name_th = arg_sw[1];
                int rdept_idx = int.Parse(arg_sw[2]);
                string dept_name_th = arg_sw[3];
                int rsec_idx = int.Parse(arg_sw[4]);
                string sec_name_th = arg_sw[5];
                int rpos_idx = int.Parse(arg_sw[6]);
                string pos_name_th = arg_sw[7];
                int emp_idx = int.Parse(arg_sw[8]);

                var dsDevice_sw = (DataSet)ViewState["vsTemp_ODSP"];

                var drDevice_sw = dsDevice_sw.Tables[0].NewRow();
                drDevice_sw["org_idx"] = org_idx;
                drDevice_sw["org_name_th"] = org_name_th.ToString();
                drDevice_sw["rdept_idx"] = rdept_idx;
                drDevice_sw["dept_name_th"] = dept_name_th.ToString();
                drDevice_sw["rsec_idx"] = rsec_idx;
                drDevice_sw["sec_name_th"] = sec_name_th.ToString();
                drDevice_sw["rpos_idx"] = rpos_idx;
                drDevice_sw["pos_name_th"] = pos_name_th.ToString();
                drDevice_sw["emp_idx"] = emp_idx;

                dsDevice_sw.Tables[0].Rows.Add(drDevice_sw);
                ViewState["rsec_idx_1"] = rsec_idx.ToString();

                ViewState["vsTemp_ODSP"] = dsDevice_sw;
                GvRsecApprove_Temp.DataSource = dsDevice_sw.Tables[0];
                GvRsecApprove_Temp.DataBind();

                /* var dsDatabase = (DataSet)ViewState["vsTemp_ODSP_Database"]; 

                 for (int counter = 0; counter < dsDatabase.Tables[0].Rows.Count; counter--)
                 {
                     if (dsDatabase.Tables[0].Rows[counter]["rsec_idx"].ToString() == ViewState["rsec_idx_1"].ToString())
                     {
                         dsDatabase.Tables[0].Rows[counter].Delete();
                         break;
                     }
                 }
                 //ViewState["vsTemp_ODSP_Database"] = dsDatabase;
                 GvRsecApprove_Database.DataSource = (DataSet)ViewState["vsTemp_ODSP_Database"];//dsDatabase.Tables[0];
                 GvRsecApprove_Database.DataBind();*/
                break;

            case "btndel":
                string resc_idx_1 = cmdArg;
                GridView GvRsecApproveTemp_t = (GridView)ViewSetApprove.FindControl("GvRsecApproveTemp");
                var dsDevice_sw_1 = (DataSet)ViewState["vsTemp_ODSP"];
                ViewState["rsec_idx"] = resc_idx_1;

                for (int counter = 0; counter < dsDevice_sw_1.Tables[0].Rows.Count; counter++)
                {
                    if (dsDevice_sw_1.Tables[0].Rows[counter]["rsec_idx"].ToString() == ViewState["rsec_idx"].ToString())
                    {
                        dsDevice_sw_1.Tables[0].Rows[counter].Delete();
                        break;
                    }
                }

                GvRsecApproveTemp_t.DataSource = (DataSet)ViewState["vsTemp_ODSP"];
                GvRsecApproveTemp_t.DataBind();
                break;

            case "btnSave_setapprove":
                var FvDetailUser_temp_ = (FormView)ViewSetApprove.FindControl("FvDetailUser_temp");
                var FvDetailUser_ = (FormView)ViewSetApprove.FindControl("FvDetailUser");
                var lbl_emp_idx_temp = (Label)FvDetailUser_temp_.FindControl("lbl_emp_idx_temp");
                var lbl_emp_idx = (Label)FvDetailUser_.FindControl("lbl_emp_idx");

                if (lbl_emp_idx_temp.Text != "" || lbl_emp_idx.Text != "")
                {
                    Update_Employee_Setapprove();
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('*** กรุณากรอกข้อมูลให้ครบถ้วน')", true);
                }
                break;
            /*case "cmd_guidebook":
                Response.Write("<script>window.open('https://docs.google.com/document/d/11Ovt32jvajVIGD4-9iCJQr7MR7AdY3pnNc9o17S-Dvw/edit?usp=sharing','_blank');</script>");
                break;*/
            case "btnback_index":
                //Select_Employee_index();
                //Menu_Color(int.Parse(cmdArg));
                //MvMaster.SetActiveView(ViewIndex);
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "DeleteGvEmpshift":
                var sfs = (Label)FvEdit_Detail.FindControl("sfs");
                string[] arg2 = new string[1];
                arg1 = e.CommandArgument.ToString().Split(';');
                int shift_idx = int.Parse(arg1[0]);

                var GvEmpShift_ = (GridView)FvEdit_Detail.FindControl("GvEmpShift");

                _dataEmployee.ShiftTime_details = new ShiftTime[1];
                ShiftTime _orgList = new ShiftTime();
                _orgList.emp_shift_idx = int.Parse(ViewState["fv_emp_idx"].ToString());
                _orgList.type_selection = 4; //delete
                _orgList.shiftTime_idx = shift_idx;
                _orgList.shif_status = 9;
                _dataEmployee.ShiftTime_details[0] = _orgList;
                //sfs.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
                _dataEmployee = callServiceEmployee(_urlSetOrganizationList_shift, _dataEmployee);


                if (int.Parse(_dataEmployee.return_code) == 1)
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('*** ไม่สามารถลบตำแหน่งหลักได้ กรุณาเปลี่ยนสถานะก่อน')", true);
                }
                else
                {
                    //Select_FvEdit_Detail(int.Parse(ViewState["fv_emp_idx"].ToString()));
                    //Menu_Color_Edit(2);
                    //txtfocus.Focus();
                    //GvEmpShift_.DataSource = _dataEmployee.ShiftTime_details;
                    //GvEmpShift_.DataBind();
                    Select_Gv_EmpShift(int.Parse(ViewState["fv_emp_idx"].ToString()));
                }

                break;
            case "DeleteGvAsset":
                //var sfsa = (Label)FvEdit_Detail.FindControl("sfsa");
                string[] arg2d = new string[1];
                arg2d = e.CommandArgument.ToString().Split(';');
                int u0_asidx = int.Parse(arg2d[0]);

                var GvAsset_View = (GridView)FvEdit_Detail.FindControl("GvAsset_View");

                _dataEmployee.Holder_List = new Holder_Detail[1];
                Holder_Detail _AssetList = new Holder_Detail();
                _AssetList.Select_action = 7; //update
                _AssetList.u0_status = 9;
                _AssetList.u0_asidx = u0_asidx;
                _dataEmployee.Holder_List[0] = _AssetList;

                //sfsa.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
                _dataEmployee = callServiceEmployee(_urlSetOrganizationList_shift, _dataEmployee);
                Select_Gv_Asset(GvAsset_View, int.Parse(ViewState["fv_emp_idx"].ToString()));


                break;

            case "btnprobation":
                Menu_Color(int.Parse(cmdArg));
                MvMaster.SetActiveView(ViewProbation);
                select_org(ddlorgidx);
                Select_Location(ddllocation_probation);
                ddlorgidx.SelectedValue = "0";
                ddlrdeptidx.SelectedValue = "0";
                ddlsecidx.SelectedValue = "0";
                ddlemptype1.SelectedValue = "0";
                ddlprobation.SelectedValue = "9";
                ddlSearchDate.SelectedValue = "0";
                AddStartdate.Text = String.Empty;
                AddEndDate.Text = String.Empty;

                linkBtnTrigger(btnsearch_probation);
                gridViewTrigger(GvEmpProbationList);


                break;

            case "btnsearch_probation":
                gridViewTrigger(GvEmpProbationList);
                Select_Report_Probation();

                break;

            case "CmdDetail_Print":
                string[] arg_print = new string[4];
                arg_print = e.CommandArgument.ToString().Split(';');

                ViewState["EmpIDX_Print"] = int.Parse(arg_print[0]);
                ViewState["FullName_Print"] = arg_print[1].ToString();
                ViewState["JobGradeIDX_Print"] = arg_print[2].ToString();
                ViewState["EmpType_Print"] = arg_print[3].ToString();

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                lblfullname.Text = ViewState["FullName_Print"].ToString();

                FvTemplate_print.ChangeMode(FormViewMode.ReadOnly);
                Select_Report_Probation_Print(FvTemplate_print, int.Parse(arg_print[0]));

                if (int.Parse(ViewState["JobGradeIDX_Print"].ToString()) < 3 && ViewState["EmpType_Print"].ToString() == "1")
                //if (int.Parse(ViewState["JobGradeIDX_Print"].ToString()) < 3)
                {
                    btndaily.Visible = true;
                    btnofficer.Visible = false;
                    btnmanager.Visible = false;
                    linkBtnTrigger(btndaily);
                }
                else if (int.Parse(ViewState["JobGradeIDX_Print"].ToString()) < 8 && ViewState["EmpType_Print"].ToString() == "2")
                //  else if (int.Parse(ViewState["JobGradeIDX_Print"].ToString()) > 2 && int.Parse(ViewState["JobGradeIDX_Print"].ToString()) < 8)
                {
                    btndaily.Visible = false;
                    btnofficer.Visible = true;
                    btnmanager.Visible = false;
                    linkBtnTrigger(btnofficer);
                }
                else if (ViewState["EmpType_Print"].ToString() == "2")
                {
                    btndaily.Visible = false;
                    btnofficer.Visible = false;
                    btnmanager.Visible = true;
                    linkBtnTrigger(btnmanager);
                }
                break;



            //case "CmdPrint":
            //    //string[] arg_print = new string[3];
            //    //arg_print = e.CommandArgument.ToString().Split(';');

            //    //ViewState["EmpIDX_Print"] = int.Parse(arg_print[0]);
            //    //ViewState["FullName_Print"] = arg_print[1].ToString();
            //    //ViewState["EmpCode_Print"] = arg_print[2].ToString();


            //    //FvTemplate_print.ChangeMode(FormViewMode.ReadOnly);
            //    //Select_Report_Probation_Print(FvTemplate_print, int.Parse(arg_print[0]));

            //    //fss.Text = ViewState["FullName_Print"].ToString();
            //    break;

            case "btnrefresh":

                ddlorgidx.SelectedValue = "0";
                ddlrdeptidx.SelectedValue = "0";
                ddlsecidx.SelectedValue = "0";
                ddlemptype1.SelectedValue = "0";
                ddlprobation.SelectedValue = "9";
                ddlSearchDate.SelectedValue = "0";
                AddStartdate.Text = String.Empty;
                AddEndDate.Text = String.Empty;
                ddllenght_empcode.SelectedValue = "0";
                txtempcode.Text = String.Empty;
                txtempcode1.Text = String.Empty;
                txtempcode1.Enabled = false;
                ddllocation_probation.SelectedValue = "0";

                break;

            case "btnexport_probation":
                _dataEmployee.BoxEmployee_ProbationList = new Employee_Probation[1];
                Employee_Probation _dtreport = new Employee_Probation();

                _dtreport.OrgIDX = int.Parse(ddlorgidx.SelectedValue);
                _dtreport.RDeptID = int.Parse(ddlrdeptidx.SelectedValue);
                _dtreport.RSecID = int.Parse(ddlsecidx.SelectedValue);
                _dtreport.EmpProbation = int.Parse(ddlprobation.SelectedValue);
                _dtreport.EmpType = int.Parse(ddlemptype1.SelectedValue);
                _dtreport.IFSearchBetween = int.Parse(ddlSearchDate.SelectedValue);
                _dtreport.EmpProbationDate_Start = AddStartdate.Text;
                _dtreport.EmpProbationDate_End = AddEndDate.Text;
                _dtreport.LocIDX = int.Parse(ddllocation_probation.SelectedValue);
                _dtreport.IFLenght = int.Parse(ddllenght_empcode.SelectedValue);
                _dtreport.EmpCode = txtempcode.Text;
                _dtreport.EmpCode1 = txtempcode1.Text;
                _dtreport.Condition = 3;

                _dataEmployee.BoxEmployee_ProbationList[0] = _dtreport;
                //fss.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
                _dataEmployee = callServiceEmployee(_urlSelect_Report_Probation, _dataEmployee);
                setGridData(GvExport_Excel, _dataEmployee.BoxExport_ProbationList);

                GvExport_Excel.AllowSorting = false;
                GvExport_Excel.AllowPaging = false;

                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.xls");
                // Response.Charset = ""; set character 
                Response.Charset = "utf-8";
                Response.ContentEncoding = System.Text.Encoding.GetEncoding("utf-8");
                Response.ContentType = "application/vnd.ms-excel";

                Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
                Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>");


                StringWriter sw_export = new StringWriter();
                HtmlTextWriter hw_export = new HtmlTextWriter(sw_export);


                GvExport_Excel.Columns[0].Visible = true;
                GvExport_Excel.HeaderRow.BackColor = Color.White;

                foreach (TableCell cell in GvExport_Excel.HeaderRow.Cells)
                {
                    cell.BackColor = GvExport_Excel.HeaderStyle.BackColor;
                }

                foreach (GridViewRow row in GvExport_Excel.Rows)
                {
                    row.BackColor = Color.White;
                    foreach (TableCell cell in row.Cells)
                    {
                        if (row.RowIndex % 2 == 0)
                        {
                            cell.BackColor = GvExport_Excel.AlternatingRowStyle.BackColor;
                        }
                        else
                        {
                            cell.BackColor = GvExport_Excel.RowStyle.BackColor;
                        }
                        cell.CssClass = "textmode";
                    }
                }

                GvExport_Excel.RenderControl(hw_export);

                //style to format numbers to string
                string style = @"<style> .textmode { mso-number-format:\@; font-family: myFirstFont; } </style>";
                Response.Write(style);
                Response.Output.Write(sw_export.ToString());
                Response.Flush();
                Response.End();
                break;

            case "AddGurantee":
                var GvGurantee = (GridView)FvInsertEmp.FindControl("GvGurantee");
                var txtfullname_gun_add = (TextBox)FvInsertEmp.FindControl("txtfullname_gun_add");
                var txt_Gurantee_startdate_add = (TextBox)FvInsertEmp.FindControl("txt_Gurantee_startdate_add");
                var txt_relationgurantee_add = (TextBox)FvInsertEmp.FindControl("txt_relationgurantee_add");
                var txttel_guranbtee_add = (TextBox)FvInsertEmp.FindControl("txttel_guranbtee_add");

                var dsGurantee_add = (DataSet)ViewState["vsTemp_Gurantee"];

                var drGurantee_ = dsGurantee_add.Tables[0].NewRow();
                drGurantee_["gu_fullname"] = txtfullname_gun_add.Text;
                drGurantee_["gu_startdate"] = txt_Gurantee_startdate_add.Text;
                drGurantee_["gu_relation"] = txt_relationgurantee_add.Text;
                drGurantee_["gu_tel"] = txttel_guranbtee_add.Text;

                dsGurantee_add.Tables[0].Rows.Add(drGurantee_);

                ViewState["vsTemp_Gurantee"] = dsGurantee_add;
                GvGurantee.DataSource = dsGurantee_add.Tables[0];
                GvGurantee.DataBind();

                txtfullname_gun_add.Text = null;
                txt_Gurantee_startdate_add.Text = null;
                txt_relationgurantee_add.Text = null;
                txttel_guranbtee_add.Text = null;
                break;

            case "AddGurantee_edit":
                var GvGurantee_Edit = (GridView)FvEdit_Detail.FindControl("GvGurantee_Edit");
                var txtGu_fullname_add = (TextBox)FvEdit_Detail.FindControl("txtGu_fullname_add");
                var txtGu_startdate_add = (TextBox)FvEdit_Detail.FindControl("txtGu_startdate");
                var txtGu_relation_add = (TextBox)FvEdit_Detail.FindControl("txtGu_relation_add");
                var txtGu_tel_add = (TextBox)FvEdit_Detail.FindControl("txtGu_tel_add");

                var dsGurantee_edit = (DataSet)ViewState["vsTemp_Gurantee_edit"];

                var drGurantee__ = dsGurantee_edit.Tables[0].NewRow();
                drGurantee__["gu_fullname"] = txtGu_fullname_add.Text;
                drGurantee__["gu_startdate"] = txtGu_startdate_add.Text;
                drGurantee__["gu_relation"] = txtGu_relation_add.Text;
                drGurantee__["gu_tel"] = txtGu_tel_add.Text;

                dsGurantee_edit.Tables[0].Rows.Add(drGurantee__);

                ViewState["vsTemp_Gurantee_edit"] = dsGurantee_edit;
                GvGurantee_Edit.DataSource = dsGurantee_edit.Tables[0];
                GvGurantee_Edit.DataBind();

                txtGu_fullname_add.Text = null;
                txtGu_startdate_add.Text = null;
                txtGu_relation_add.Text = null;
                txtGu_tel_add.Text = null;
                break;

            case "DeleteGvGurantee_View":
                string cmdArg_Gur = e.CommandArgument.ToString();
                int GUAIDX = int.Parse(cmdArg_Gur);

                var GvGurantee_View = (GridView)FvEdit_Detail.FindControl("GvGurantee_View");

                _dataEmployee.BoxGurantee_list = new Gurantee_list[1];
                Gurantee_list _dataEmployee_gur = new Gurantee_list();

                _dataEmployee_gur.GUAIDX = GUAIDX;

                _dataEmployee.BoxGurantee_list[0] = _dataEmployee_gur;
                _dataEmployee = callServiceEmployee(_urlGetDeleteGurantee, _dataEmployee);

                Select_FvEdit_Detail(int.Parse(ViewState["fv_emp_idx"].ToString()));
                Menu_Color_Edit(1);
                txtfocus.Focus();
                GvGurantee_View.EditIndex = -1;
                GvGurantee_View.DataSource = ViewState["BoxGvRefer_view"];
                GvGurantee_View.DataBind();
                break;

            case "btnscroll_top":
                switch (int.Parse(cmdArg))
                {
                    case 1:
                        hf_top_up.Focus();
                        break;
                    case 2:

                        hf_top_down.Focus();
                        break;
                }
                break;
            case "btnscroll_top_add":
                var hf_top_down_add = (HyperLink)FvInsertEmp.FindControl("hf_top_down_add");
                var hf_top_up_add = (HyperLink)FvInsertEmp.FindControl("hf_top_up_add");
                switch (int.Parse(cmdArg))
                {
                    case 1:
                        hf_top_up_add.Focus();
                        break;
                    case 2:

                        hf_top_down_add.Focus();
                        break;
                }
                break;
            case "btnexport_chart_education":
                Gv_education.AllowPaging = false;
                Gv_education.DataSource = ViewState["Education_chart_list"];
                Gv_education.DataBind();

                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=EmployeeExport_education.xls");
                // Response.Charset = ""; set character 
                Response.Charset = "utf-8";
                Response.ContentEncoding = System.Text.Encoding.GetEncoding("utf-8");
                Response.ContentType = "application/vnd.ms-excel";

                Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
                Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>");
                //Response.Write(@"<style>.number {mso-number-format:0\.00; } </style>");

                StringWriter sw2_ = new StringWriter();
                HtmlTextWriter hw2_ = new HtmlTextWriter(sw2_);

                for (int i = 0; i < Gv_education.Rows.Count; i++)
                {
                    //Apply text style to each Row
                    Gv_education.Rows[i].Attributes.Add("class", "number2");
                }
                Gv_education.RenderControl(hw2_);

                //Amount is displayed in number format with 2 decimals
                Response.Write(@"<style>.number2 {mso-number-format:0; } </style>");

                Response.Output.Write(sw2_.ToString());
                Response.Flush();
                Response.End();

                break;
            case "btnexport_chart_sex":
                Gv_sex.AllowPaging = false;
                Gv_sex.DataSource = ViewState["sex_chart_list"];
                Gv_sex.DataBind();

                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=EmployeeExport_sex.xls");
                // Response.Charset = ""; set character 
                Response.Charset = "utf-8";
                Response.ContentEncoding = System.Text.Encoding.GetEncoding("utf-8");
                Response.ContentType = "application/vnd.ms-excel";

                Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
                Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>");
                //Response.Write(@"<style>.number {mso-number-format:0\.00; } </style>");

                StringWriter sw2_sex = new StringWriter();
                HtmlTextWriter hw2_sex = new HtmlTextWriter(sw2_sex);

                for (int i = 0; i < Gv_sex.Rows.Count; i++)
                {
                    //Apply text style to each Row
                    Gv_sex.Rows[i].Attributes.Add("class", "number2");
                }
                Gv_sex.RenderControl(hw2_sex);

                //Amount is displayed in number format with 2 decimals
                Response.Write(@"<style>.number2 {mso-number-format:0; } </style>");

                Response.Output.Write(sw2_sex.ToString());
                Response.Flush();
                Response.End();

                break;
            case "btnexport_chart_age":
                Gv_age.AllowPaging = false;
                Gv_age.DataSource = ViewState["age_chart_list"];
                Gv_age.DataBind();

                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=EmployeeExport_age.xls");
                // Response.Charset = ""; set character 
                Response.Charset = "utf-8";
                Response.ContentEncoding = System.Text.Encoding.GetEncoding("utf-8");
                Response.ContentType = "application/vnd.ms-excel";

                Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
                Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>");
                //Response.Write(@"<style>.number {mso-number-format:0\.00; } </style>");

                StringWriter sw2_age = new StringWriter();
                HtmlTextWriter hw2_age = new HtmlTextWriter(sw2_age);

                for (int i = 0; i < Gv_age.Rows.Count; i++)
                {
                    //Apply text style to each Row
                    Gv_age.Rows[i].Attributes.Add("class", "number2");
                }
                Gv_age.RenderControl(hw2_age);

                //Amount is displayed in number format with 2 decimals
                Response.Write(@"<style>.number2 {mso-number-format:0; } </style>");

                Response.Output.Write(sw2_age.ToString());
                Response.Flush();
                Response.End();

                break;
            case "btnexport_chart_car":
                Gv_car.AllowPaging = false;
                Gv_car.DataSource = ViewState["Temp_Gv_car"];
                Gv_car.DataBind();

                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=EmployeeExport_car.xls");
                // Response.Charset = ""; set character 
                Response.Charset = "utf-8";
                Response.ContentEncoding = System.Text.Encoding.GetEncoding("utf-8");
                Response.ContentType = "application/vnd.ms-excel";

                Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
                Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>");
                //Response.Write(@"<style>.number {mso-number-format:0\.00; } </style>");

                StringWriter sw2_car = new StringWriter();
                HtmlTextWriter hw2_car = new HtmlTextWriter(sw2_car);

                for (int i = 0; i < Gv_car.Rows.Count; i++)
                {
                    //Apply text style to each Row
                    Gv_car.Rows[i].Attributes.Add("class", "number2");
                }
                Gv_car.RenderControl(hw2_car);

                //Amount is displayed in number format with 2 decimals
                Response.Write(@"<style>.number2 {mso-number-format:0; } </style>");

                Response.Output.Write(sw2_car.ToString());
                Response.Flush();
                Response.End();

                break;
            case "btnexport_chart_emp_by_dept":
                Gv_CountEmployee.AllowPaging = false;
                Gv_CountEmployee.DataSource = ViewState["countemp_chart_list"];
                Gv_CountEmployee.DataBind();

                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=EmployeeExport_countemp.xls");
                // Response.Charset = ""; set character 
                Response.Charset = "utf-8";
                Response.ContentEncoding = System.Text.Encoding.GetEncoding("utf-8");
                Response.ContentType = "application/vnd.ms-excel";

                Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
                Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>");
                //Response.Write(@"<style>.number {mso-number-format:0\.00; } </style>");

                StringWriter sw2_countemp = new StringWriter();
                HtmlTextWriter hw2_countemp = new HtmlTextWriter(sw2_countemp);

                for (int i = 0; i < Gv_CountEmployee.Rows.Count; i++)
                {
                    //Apply text style to each Row
                    Gv_CountEmployee.Rows[i].Attributes.Add("class", "number2");
                }
                Gv_CountEmployee.RenderControl(hw2_countemp);

                //Amount is displayed in number format with 2 decimals
                Response.Write(@"<style>.number2 {mso-number-format:0; } </style>");

                Response.Output.Write(sw2_countemp.ToString());
                Response.Flush();
                Response.End();

                break;
            case "btnexport_chart_leave_by_dept":
                Gv_LeaveEmployee.AllowPaging = false;
                Gv_LeaveEmployee.DataSource = ViewState["leave_chart_list"];
                Gv_LeaveEmployee.DataBind();

                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=EmployeeExport_leave.xls");
                // Response.Charset = ""; set character 
                Response.Charset = "utf-8";
                Response.ContentEncoding = System.Text.Encoding.GetEncoding("utf-8");
                Response.ContentType = "application/vnd.ms-excel";

                Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
                Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>");
                //Response.Write(@"<style>.number {mso-number-format:0\.00; } </style>");

                StringWriter sw2_leave = new StringWriter();
                HtmlTextWriter hw2_leave = new HtmlTextWriter(sw2_leave);

                for (int i = 0; i < Gv_LeaveEmployee.Rows.Count; i++)
                {
                    //Apply text style to each Row
                    Gv_LeaveEmployee.Rows[i].Attributes.Add("class", "number2");
                }
                Gv_LeaveEmployee.RenderControl(hw2_leave);

                //Amount is displayed in number format with 2 decimals
                Response.Write(@"<style>.number2 {mso-number-format:0; } </style>");

                Response.Output.Write(sw2_leave.ToString());
                Response.Flush();
                Response.End();

                break;
            case "btnexport_chart_nationality":
                Gv_race.AllowPaging = false;
                Gv_race.DataSource = ViewState["nationlity_chart_list"];
                Gv_race.DataBind();

                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=EmployeeExport_nationality.xls");
                // Response.Charset = ""; set character 
                Response.Charset = "utf-8";
                Response.ContentEncoding = System.Text.Encoding.GetEncoding("utf-8");
                Response.ContentType = "application/vnd.ms-excel";

                Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
                Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>");
                //Response.Write(@"<style>.number {mso-number-format:0\.00; } </style>");

                StringWriter sw2_nationality = new StringWriter();
                HtmlTextWriter hw2_nationality = new HtmlTextWriter(sw2_nationality);

                for (int i = 0; i < Gv_race.Rows.Count; i++)
                {
                    //Apply text style to each Row
                    Gv_race.Rows[i].Attributes.Add("class", "number2");
                }
                Gv_race.RenderControl(hw2_nationality);

                //Amount is displayed in number format with 2 decimals
                Response.Write(@"<style>.number2 {mso-number-format:0; } </style>");

                Response.Output.Write(sw2_nationality.ToString());
                Response.Flush();
                Response.End();

                break;
            case "btnexport_chart_new_emp":
                Gv_new_employee.AllowPaging = false;
                Gv_new_employee.DataSource = ViewState["newemp_chart_list"];
                Gv_new_employee.DataBind();

                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=EmployeeExport_newemp.xls");
                // Response.Charset = ""; set character 
                Response.Charset = "utf-8";
                Response.ContentEncoding = System.Text.Encoding.GetEncoding("utf-8");
                Response.ContentType = "application/vnd.ms-excel";

                Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
                Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>");
                //Response.Write(@"<style>.number {mso-number-format:0\.00; } </style>");

                StringWriter sw2_newemp = new StringWriter();
                HtmlTextWriter hw2_newemp = new HtmlTextWriter(sw2_newemp);

                for (int i = 0; i < Gv_new_employee.Rows.Count; i++)
                {
                    //Apply text style to each Row
                    Gv_new_employee.Rows[i].Attributes.Add("class", "number2");
                }
                Gv_new_employee.RenderControl(hw2_newemp);

                //Amount is displayed in number format with 2 decimals
                Response.Write(@"<style>.number2 {mso-number-format:0; } </style>");

                Response.Output.Write(sw2_newemp.ToString());
                Response.Flush();
                Response.End();

                break;

            case "CmdUpload":
                Image image = (Image)FvEdit_Detail.FindControl("image");

                if (UploadFileEdit.HasFile)
                {
                    string getPathfile = ConfigurationManager.AppSettings["path_emp_profile"];
                    string fileName_upload = ViewState["EmpIDX_Pic"].ToString();
                    string filePath_upload = Server.MapPath(getPathfile + fileName_upload);

                    if (!Directory.Exists(filePath_upload))
                    {
                        Directory.CreateDirectory(filePath_upload);
                    }
                    string extension = Path.GetExtension(UploadFileEdit.FileName);

                    UploadFileEdit.SaveAs(Server.MapPath(getPathfile + fileName_upload) + "\\" + fileName_upload + extension);

                }
                //                GetPathPhoto(ViewState["txtidcard"].ToString(), image);

                Select_FvEdit_Detail(int.Parse(ViewState["EmpIDX_Pic"].ToString()));
                linkFvTrigger(FvEdit_Detail);

                break;
        }
    }
    #endregion


}
