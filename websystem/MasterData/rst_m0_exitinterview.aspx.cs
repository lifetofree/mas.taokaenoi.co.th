﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_rst_m0_exitinterview : System.Web.UI.Page
{
    #region Connect

    service_execute serviceexcute = new service_execute();
    data_employee _dtEmployee = new data_employee();
    data_resignation _dtresign = new data_resignation();
    function_tool _funcTool = new function_tool();

    string _localJson = String.Empty;
    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlInsert_Topic = _serviceUrl + ConfigurationManager.AppSettings["urlInsert_Topic"];
    static string _urlSelect_Topic = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Topic"];
    static string _urlSelect_TypeAnswer = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_TypeAnswer"];
    static string _urlSelect_TypeChoice = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_TypeChoice"];
    static string _urlSelect_TypeQuestion = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_TypeQuestion"];
    static string _urlInsert_Question_Resignation = _serviceUrl + ConfigurationManager.AppSettings["urlInsert_Question_Resignation"];
    static string _urlInsert_Question_ForWrite_Resignation = _serviceUrl + ConfigurationManager.AppSettings["urlInsert_Question_ForWrite_Resignation"];
    static string _urlSelect_TypeAnswer_Resignation = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_TypeAnswer_Resignation"];
    static string _urlSelect_Question_Resignation = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Question_Resignation"];
    static string _urlSelect_SubQuestion_Resignation = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_SubQuestion_Resignation"];
    static string _urlDelete_Topic_Resignation = _serviceUrl + ConfigurationManager.AppSettings["urlDelete_Topic_Resignation"];
    static string _urlSelect_EditSubQuestion_Resignation = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_EditSubQuestion_Resignation"];
    static string _urlUpdate_Resignationn = _serviceUrl + ConfigurationManager.AppSettings["urlUpdate_Resignationn"];
    static string _urlDelete_Subquestion_Resignation = _serviceUrl + ConfigurationManager.AppSettings["urlDelete_Subquestion_Resignation"];
    static string _urlUpdate_Topic_Resignation = _serviceUrl + ConfigurationManager.AppSettings["urlUpdate_Topic_Resignation"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];



    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ViewState["EmpIDX"] = int.Parse(Session["emp_idx"].ToString());
            select_empIdx_present();
            _divMenuLiToViewIndex.Attributes.Add("class", "active");
            SetDefaultpage(1);
        }

    }
    #endregion

    #region Select
    protected void select_empIdx_present()
    {

        _dtEmployee = callServiceEmpProfile(_urlGetMyProfile + ViewState["EmpIDX"].ToString());

        ViewState["rdept_name"] = _dtEmployee.employee_list[0].dept_name_th;
        ViewState["rdept_idx"] = _dtEmployee.employee_list[0].rdept_idx;
        ViewState["FullName"] = _dtEmployee.employee_list[0].emp_name_th;
        ViewState["Org_name"] = _dtEmployee.employee_list[0].org_name_th;
        ViewState["Org_idx"] = _dtEmployee.employee_list[0].org_idx;

        ViewState["EmpCode"] = _dtEmployee.employee_list[0].emp_code;
        ViewState["Positname"] = _dtEmployee.employee_list[0].pos_name_th;
        ViewState["Pos_idx"] = _dtEmployee.employee_list[0].rpos_idx;
        ViewState["Email"] = _dtEmployee.employee_list[0].emp_email;
        ViewState["Tel"] = _dtEmployee.employee_list[0].emp_mobile_no;
        ViewState["Secname"] = _dtEmployee.employee_list[0].sec_name_th;
        ViewState["Sec_idx"] = _dtEmployee.employee_list[0].rsec_idx;
        ViewState["CostIDX"] = _dtEmployee.employee_list[0].costcenter_idx;

    }

    protected void select_Topic(GridView gvName)
    {

        _dtresign = new data_resignation();

        _dtresign.Boxm0_ExitInterview = new M0_ExitInterview[1];
        M0_ExitInterview _selecttopic = new M0_ExitInterview();

        _selecttopic.condition = 1;
        _dtresign.Boxm0_ExitInterview[0] = _selecttopic;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

        _dtresign = callServicePostResignation(_urlSelect_Topic, _dtresign); //_urlSelectm0typequest
        setGridData(gvName, _dtresign.Boxm0_ExitInterview);

    }

    protected void selectDDl_Topic(DropDownList ddlName, int orgidx)
    {

        _dtresign = new data_resignation();

        _dtresign.Boxm0_ExitInterview = new M0_ExitInterview[1];
        M0_ExitInterview _selecttopic = new M0_ExitInterview();

        _selecttopic.orgidx = orgidx;
        _selecttopic.condition = 2;

        _dtresign.Boxm0_ExitInterview[0] = _selecttopic;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

        _dtresign = callServicePostResignation(_urlSelect_Topic, _dtresign); //_urlSelectm0typequest

        setDdlData(ddlName, _dtresign.Boxm0_ExitInterview, "topic_name", "m0_toidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกแบบฟอร์มลาออก...", "0"));

    }

    protected void Select_TypeQuestion(DropDownList ddlName)
    {

        _dtresign = new data_resignation();

        _dtresign.Boxm0_ExitInterview = new M0_ExitInterview[1];
        M0_ExitInterview _selecttopic = new M0_ExitInterview();

        _dtresign.Boxm0_ExitInterview[0] = _selecttopic;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

        _dtresign = callServicePostResignation(_urlSelect_TypeQuestion, _dtresign); //_urlSelectm0typequest

        setDdlData(ddlName, _dtresign.Boxm0_ExitInterview, "type_quest", "m0_tqidx");
        ddlName.Items.Insert(0, new ListItem("กรุณากำหนดคำตอบ...", "0"));

    }

    protected void gettypeanswer(DropDownList ddlName)
    {

        _dtresign = new data_resignation();

        _dtresign.Boxm0_ExitInterview = new M0_ExitInterview[1];
        M0_ExitInterview _selectanswer = new M0_ExitInterview();

        _dtresign.Boxm0_ExitInterview[0] = _selectanswer;
        //        txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtrcm));

        _dtresign = callServicePostResignation(_urlSelect_TypeAnswer, _dtresign);
        setDdlData(ddlName, _dtresign.Boxm0_ExitInterview, "type_ans", "m0_taidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกประเภทคำถาม...", "0"));
    }

    protected void gettypechoice(DropDownList ddlName)
    {

        _dtresign = new data_resignation();

        _dtresign.Boxm0_ExitInterview = new M0_ExitInterview[1];
        M0_ExitInterview _selectanswer = new M0_ExitInterview();

        _dtresign.Boxm0_ExitInterview[0] = _selectanswer;
        //        txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtrcm));

        _dtresign = callServicePostResignation(_urlSelect_TypeChoice, _dtresign);
        setDdlData(ddlName, _dtresign.Boxm0_ExitInterview, "type_choice", "m0_chidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกข้อมูลเพิ่มเติม...", "0"));
    }

    protected void select_gettypeanswer(GridView gvName, int m0_toidx)
    {

        _dtresign = new data_resignation();

        _dtresign.Boxm0_Question = new M0_Question[1];
        M0_Question _typeans = new M0_Question();

        _typeans.m0_toidx = m0_toidx;

        _dtresign.Boxm0_Question[0] = _typeans;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtrcm));

        _dtresign = callServicePostResignation(_urlSelect_TypeAnswer_Resignation, _dtresign);
        setGridData(gvName, _dtresign.Boxm0_Question);
    }

    protected void select_getquestion(GridView gvName, int m0_taidx, int m0_toidx)
    {

        _dtresign = new data_resignation();

        _dtresign.Boxm0_Question = new M0_Question[1];
        M0_Question _qustion = new M0_Question();

        _qustion.m0_toidx = m0_toidx;
        _qustion.m0_taidx = m0_taidx;

        _dtresign.Boxm0_Question[0] = _qustion;

        _dtresign = callServicePostResignation(_urlSelect_Question_Resignation, _dtresign);
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

        setGridData(gvName, _dtresign.Boxm0_Question);
    }

    protected void select_geteditsubquestion(GridView gvName, int m0_quidx, int m0_taidx)
    {

        _dtresign = new data_resignation();

        _dtresign.Boxm0_Question = new M0_Question[1];
        M0_Question _qustion = new M0_Question();

        _qustion.m0_quidx = m0_quidx;
        _qustion.m0_taidx = m0_taidx;

        _dtresign.Boxm0_Question[0] = _qustion;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

        _dtresign = callServicePostResignation(_urlSelect_SubQuestion_Resignation, _dtresign);

        setGridData(gvName, _dtresign.Boxm0_Question);
    }

    protected void select_geteditquestion(GridView gvName, int m0_quidx)
    {

        _dtresign = new data_resignation();

        _dtresign.Boxm0_Question = new M0_Question[1];
        M0_Question _qustion = new M0_Question();

        _qustion.m0_quidx = m0_quidx;

        _dtresign.Boxm0_Question[0] = _qustion;

        _dtresign = callServicePostResignation(_urlSelect_EditSubQuestion_Resignation, _dtresign);
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

        setGridData(gvName, _dtresign.Boxm0_Question);
    }

    protected void getOrganizationList(DropDownList ddlName)
    {

        _dtEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dtEmployee.organization_list[0] = _orgList;

        _dtEmployee = callServicePostEmp(_urlGetOrganizationList, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกแบบฟอร์มลาออก", "0"));
    }
    #endregion

    #region Insert & Delete
    protected void Insert_Topic(string txttopic, int cempidx, int orgidx)
    {
        _dtresign = new data_resignation();

        _dtresign.Boxm0_ExitInterview = new M0_ExitInterview[1];
        M0_ExitInterview _inserttopic = new M0_ExitInterview();

        _inserttopic.topic_name = txttopic;
        _inserttopic.CEmpIDX = cempidx;
        _inserttopic.orgidx = orgidx;

        _dtresign.Boxm0_ExitInterview[0] = _inserttopic;
        _dtresign = callServicePostResignation(_urlInsert_Topic, _dtresign);


    }

    protected void InsertQuestion()
    {

        _dtresign = new data_resignation();
        int i = 0;
        var ds_udoc0_insert = (DataSet)ViewState["vsBuyequipment"];
        var _document0 = new M0_ExitInterview[ds_udoc0_insert.Tables[0].Rows.Count];

        var ds_udoc1_insert = (DataSet)ViewState["vsBuyequipment"];
        var _document1 = new M0_Question[ds_udoc1_insert.Tables[0].Rows.Count];


        foreach (DataRow dtrow in ds_udoc1_insert.Tables[0].Rows)
        {


            _document0[i] = new M0_ExitInterview();
            _document0[i].m0_toidx = int.Parse(ddltopic.SelectedValue);
            _document0[i].m0_taidx = int.Parse(ddltypeans.SelectedValue);
            _document0[i].m0_tqidx = int.Parse(dtrow["m0_tqidx"].ToString());
            _document0[i].no_choice = int.Parse(dtrow["No_Quest"].ToString());
            _document0[i].quest_name = dtrow["Question"].ToString();
            _document0[i].CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());


            _document1[i] = new M0_Question();
            _document1[i].m0_chidx = int.Parse(dtrow["m0_chidx"].ToString());
            _document1[i].choice_name = dtrow["Choice"].ToString();
            _document1[i].quest_name = dtrow["Question"].ToString();
            _document1[i].CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

            i++;
            _dtresign.Boxm0_ExitInterview = _document0;
            _dtresign.Boxm0_Question = _document1;
        }

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));
        _dtresign = callServicePostResignation(_urlInsert_Question_Resignation, _dtresign);

    }

    protected void InsertQuestion_Writing()
    {
        _dtresign = new data_resignation();
        int i = 0;
        var ds_udoc0_insert = (DataSet)ViewState["vsBuyequipment"];
        var _document0 = new M0_Question[ds_udoc0_insert.Tables[0].Rows.Count];

        foreach (DataRow dtrow in ds_udoc0_insert.Tables[0].Rows)
        {
            _document0[i] = new M0_Question();
            _document0[i].m0_toidx = int.Parse(ddltopic.SelectedValue);
            _document0[i].m0_taidx = int.Parse(ddltypeans.SelectedValue);
            _document0[i].m0_tqidx = 0;
            _document0[i].no_choice = int.Parse(dtrow["No_Quest"].ToString());
            _document0[i].quest_name = dtrow["Question"].ToString();
            _document0[i].CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

            i++;
            _dtresign.Boxm0_Question = _document0;
        }
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));
        _dtresign = callServicePostResignation(_urlInsert_Question_ForWrite_Resignation, _dtresign);

    }

    protected void Delete_Topic(int m0_toidx, int cempidx)
    {
        _dtresign = new data_resignation();

        _dtresign.Boxm0_ExitInterview = new M0_ExitInterview[1];
        M0_ExitInterview _deletetopic = new M0_ExitInterview();

        _deletetopic.m0_toidx = m0_toidx;
        _deletetopic.CEmpIDX = cempidx;

        _dtresign.Boxm0_ExitInterview[0] = _deletetopic;
        _dtresign = callServicePostResignation(_urlDelete_Topic_Resignation, _dtresign);

    }

    protected void Delete_SubTopic(int m1_quidx, int cempidx)
    {
        _dtresign = new data_resignation();

        _dtresign.Boxm0_Question = new M0_Question[1];
        M0_Question _deletetopic = new M0_Question();

        _deletetopic.m1_quidx = m1_quidx;
        _deletetopic.CEmpIDX = cempidx;

        _dtresign.Boxm0_Question[0] = _deletetopic;
        _dtresign = callServicePostResignation(_urlDelete_Subquestion_Resignation, _dtresign);

    }

    protected void Update_Resignation(int m1_quidx, int m0_quidx, string quest_name, int m0_tqidx, string answer, int m0_chidx)
    {


        _dtresign = new data_resignation();

        _dtresign.Boxm0_Question = new M0_Question[1];
        M0_Question _update = new M0_Question();

        _update.m1_quidx = m1_quidx;
        _update.choice_name = answer;
        _update.m0_chidx = m0_chidx;
        _update.m0_quidx = m0_quidx;
        _update.m0_tqidx = m0_tqidx;
        _update.quest_name = quest_name;
        _update.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        _dtresign.Boxm0_Question[0] = _update;
        _dtresign = callServicePostResignation(_urlUpdate_Resignationn, _dtresign);


    }

    protected void Update_Topic(int m0_toidx, string topic_name)
    {
        _dtresign = new data_resignation();

        _dtresign.Boxm0_ExitInterview = new M0_ExitInterview[1];
        M0_ExitInterview _update = new M0_ExitInterview();

        _update.m0_toidx = m0_toidx;
        _update.topic_name = topic_name;
        _update.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        _dtresign.Boxm0_ExitInterview[0] = _update;
        _dtresign = callServicePostResignation(_urlUpdate_Topic_Resignation, _dtresign);
    }

    #endregion

    #region reuse
    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormViewData(FormView fvName, Object obj)
    {
        fvName.DataSource = obj;
        fvName.DataBind();
    }
    #endregion

    #region CallService

    protected data_employee callServiceEmpProfile(string _cmdUrl)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }

    protected data_resignation callServicePostResignation(string _cmdUrl, data_resignation _dtresign)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtresign);
        // text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtresign = (data_resignation)_funcTool.convertJsonToObject(typeof(data_resignation), _localJson);


        return _dtresign;
    }

    protected data_employee callServicePostEmp(string _cmdUrl, data_employee _dtEmployee)
    {
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);


        return _dtEmployee;
    }
    #endregion

    #region SetDefault
    protected void SetDefaultpage(int page)
    {

        switch (page)
        {

            case 1:
                _divMenuLiToViewIndex.Attributes.Add("class", "active");
                _divMenuLiToViewAdd.Attributes.Remove("class");
                MvMaster.SetActiveView(ViewIndex);
                select_Topic(GvTopic);
                break;

            case 2:

                _divMenuLiToViewIndex.Attributes.Remove("class");
                _divMenuLiToViewAdd.Attributes.Add("class", "active");
                MvMaster.SetActiveView(ViewInsert);

                FvDetailUser.ChangeMode(FormViewMode.Insert);
                FvDetailUser.DataBind();
                fv_insert.ChangeMode(FormViewMode.Insert);
                fv_insert.DataBind();


                CleardataSetFormList(GvReportAdd);
                GvReportAdd.Visible = false;

                CleardataSetFormList(GvWrite);
                GvWrite.Visible = false;
                break;

            case 3:
                _divMenuLiToViewIndex.Attributes.Add("class", "active");
                _divMenuLiToViewAdd.Attributes.Remove("class");
                MvMaster.SetActiveView(ViewDetail);
                select_gettypeanswer(GvTypeAns, int.Parse(ViewState["m0_toidx"].ToString()));

                break;


        }
    }


    #endregion

    #region SetDefault Form

    protected void SetViewState_Form()
    {

        DataSet dsFoodMaterialList = new DataSet();
        dsFoodMaterialList.Tables.Add("dsAddListTable");

        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("No_Quest", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("Question", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("type_quest", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("m0_tqidx", typeof(int));

        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("Choice", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("TypeChoice", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("m0_chidx", typeof(int));


        ViewState["vsBuyequipment"] = dsFoodMaterialList;

    }

    protected void setAddList_Form(TextBox no, TextBox Question, DropDownList ddltypequest, TextBox Choice, DropDownList ddltypechoice, GridView gvName)
    {

        if (ViewState["vsBuyequipment"] != null)
        {
            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts = (DataSet)ViewState["vsBuyequipment"];

            foreach (DataRow dr in dsContacts.Tables["dsAddListTable"].Rows)
            {
                //if (dr["No_Quest"].ToString() == no.Text)
                //{
                //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลลำดับนี้อยู่แล้ว!!!');", true);

                //    return;
                //}
                if (dr["No_Quest"].ToString() == no.Text &&
                   dr["Question"].ToString() == Question.Text &&
                   dr["Choice"].ToString() == Choice.Text)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว!!!');", true);

                    return;
                }
            }

            DataRow drContacts = dsContacts.Tables["dsAddListTable"].NewRow();

            drContacts["No_Quest"] = no.Text;
            drContacts["Question"] = Question.Text;
            drContacts["type_quest"] = ddltypequest.SelectedItem.Text;
            drContacts["m0_tqidx"] = ddltypequest.SelectedValue;

            drContacts["Choice"] = Choice.Text;
            drContacts["TypeChoice"] = ddltypechoice.SelectedItem.Text;
            drContacts["m0_chidx"] = ddltypechoice.SelectedValue;



            dsContacts.Tables["dsAddListTable"].Rows.Add(drContacts);
            ViewState["vsBuyequipment"] = dsContacts;
            setGridData(gvName, dsContacts.Tables["dsAddListTable"]);
            gvName.Visible = true;
            mergeCell(gvName);
        }

    }

    protected void CleardataSetFormList(GridView GvName)
    {
        ViewState["vsBuyequipment"] = null;
        GvName.DataSource = ViewState["vsBuyequipment"];
        GvName.DataBind();
        SetViewState_Form();
    }

    protected void SetViewState_FormWrite()
    {

        DataSet dsFoodMaterialList = new DataSet();
        dsFoodMaterialList.Tables.Add("dsAddListTable");

        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("No_Quest", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("Question", typeof(String));

        ViewState["vsBuyequipment"] = dsFoodMaterialList;

    }

    protected void setAddList_FormWrite(TextBox no, TextBox Question, GridView gvName)
    {

        if (ViewState["vsBuyequipment"] != null)
        {
            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts = (DataSet)ViewState["vsBuyequipment"];

            foreach (DataRow dr in dsContacts.Tables["dsAddListTable"].Rows)
            {
                if (dr["No_Quest"].ToString() == no.Text)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลลำดับนี้อยู่แล้ว!!!');", true);
                    return;
                }
                if (dr["No_Quest"].ToString() == no.Text &&
                   dr["Question"].ToString() == Question.Text)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว!!!');", true);
                    return;
                }

                setGridData(GvReportAdd, (DataSet)ViewState["vsBuyequipment"]);
                setGridData(GvWrite, (DataSet)ViewState["vsBuyequipment"]);

            }

            DataRow drContacts = dsContacts.Tables["dsAddListTable"].NewRow();


            drContacts["No_Quest"] = no.Text;
            drContacts["Question"] = Question.Text;

            dsContacts.Tables["dsAddListTable"].Rows.Add(drContacts);
            ViewState["vsBuyequipment"] = dsContacts;
            // GvReportAdd.Visible = true;
            setGridData(gvName, dsContacts.Tables["dsAddListTable"]);
            gvName.Visible = true;

        }

    }


    #endregion

    #region FormView

    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        var FvName = (FormView)sender;

        switch (FvName.ID)
        {

            case "FvDetailUser":
                FormView FvDetailUser = (FormView)ViewInsert.FindControl("FvDetailUser");

                if (FvDetailUser.CurrentMode == FormViewMode.Insert)
                {
                    var txtempcode = ((TextBox)FvDetailUser.FindControl("txtempcode"));
                    var txtrequesname = ((TextBox)FvDetailUser.FindControl("txtrequesname"));
                    var txtrequesdept = ((TextBox)FvDetailUser.FindControl("txtrequesdept"));
                    var txtsec = ((TextBox)FvDetailUser.FindControl("txtsec"));
                    var txtpos = ((TextBox)FvDetailUser.FindControl("txtpos"));
                    var txtemail = ((TextBox)FvDetailUser.FindControl("txtemail"));
                    var txttel = ((TextBox)FvDetailUser.FindControl("txttel"));
                    var txtorg = ((TextBox)FvDetailUser.FindControl("txtorg"));

                    txtempcode.Text = ViewState["EmpCode"].ToString();
                    txtrequesname.Text = ViewState["FullName"].ToString();
                    txtorg.Text = ViewState["Org_name"].ToString();
                    txtrequesdept.Text = ViewState["rdept_name"].ToString();
                    txtsec.Text = ViewState["Secname"].ToString();
                    txtpos.Text = ViewState["Positname"].ToString();
                    txttel.Text = ViewState["Tel"].ToString();
                    txtemail.Text = ViewState["Email"].ToString();
                }
                break;

        }
    }
    #endregion

    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        if (sender is DropDownList)
        {
            DropDownList ddName = (DropDownList)sender;

            Panel panel_quest = (Panel)fv_insert.FindControl("panel_quest");
            DropDownList ddlchooseqs = (DropDownList)fv_insert.FindControl("ddlchooseqs");
            Control div_save = (Control)fv_insert.FindControl("div_save");

            ////////////////////// Topic Question ///////////////////////////////
            Control divchoosetype = (Control)fv_insert.FindControl("divchoosetype");
            //Control div_save = (Control)fv_insert.FindControl("div_save");
            Control div_txttopic = (Control)fv_insert.FindControl("div_txttopic");
            ////////////////////// Sub Question ///////////////////////////////
            DropDownList ddltopic = (DropDownList)fv_insert.FindControl("ddltopic");
            Control div_choice = (Control)fv_insert.FindControl("div_choice");
            DropDownList ddltypechoice = (DropDownList)fv_insert.FindControl("ddltypechoice");
            DropDownList ddltypequest = (DropDownList)fv_insert.FindControl("ddltypequest");
            DropDownList ddlorgidx_form = (DropDownList)fv_insert.FindControl("ddlorgidx_form");
            DropDownList ddlorgidx = (DropDownList)fv_insert.FindControl("ddlorgidx");

            switch (ddName.ID)
            {
                case "ddlchooseqs":
                    if (ddlchooseqs.SelectedValue == "1")
                    {
                        panel_quest.Visible = false;
                        div_txttopic.Visible = true;
                        div_save.Visible = true;

                        getOrganizationList(ddlorgidx);
                    }
                    else if (ddlchooseqs.SelectedValue == "2")
                    {
                        div_save.Visible = false;
                        panel_quest.Visible = true;
                        div_txttopic.Visible = false;

                        gettypeanswer(ddltypeans);
                        gettypechoice(ddltypechoice);
                        Select_TypeQuestion(ddltypequest);
                        getOrganizationList(ddlorgidx_form);
                    }
                    else
                    {
                        panel_quest.Visible = false;
                        div_save.Visible = false;
                        div_txttopic.Visible = false;
                        ddltopic.SelectedValue = "0";
                        ddlorgidx.SelectedValue = "0";
                        ddlorgidx_form.SelectedValue = "0";

                    }
                    break;

                case "ddltypeans":
                    if (ddltypeans.SelectedValue == "1")
                    {
                        div_choice.Visible = true;

                    }
                    else
                    {
                        div_choice.Visible = false;
                    }
                    break;

                case "ddlorgidx_form":
                    selectDDl_Topic(ddltopic, int.Parse(ddlorgidx_form.SelectedValue));
                    break;
            }
        }
        if (sender is TextBox)
        {
            TextBox txtName = (TextBox)sender;
            TextBox txtcourse_item = (TextBox)fv_insert.FindControl("txtcourse_item");
            TextBox txtquest = (TextBox)fv_insert.FindControl("txtquest");

            switch (txtName.ID)
            {
                case "txtcourse_item":
                    txtquest.Text = String.Empty;
                    break;
            }
        }
    }
    #endregion

    #region RowDatabound & RowCommand
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvTopic":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.BackColor = System.Drawing.Color.GhostWhite;// "#e6e6e6";

                    if (GvTopic.EditIndex == e.Row.RowIndex)
                    {
                        var ddlorgidx_edit = ((DropDownList)e.Row.FindControl("ddlorgidx_edit"));
                        var lblorgidx_edit = ((Label)e.Row.FindControl("lblorgidx_edit"));

                        getOrganizationList(ddlorgidx_edit);
                        ddlorgidx_edit.SelectedValue = lblorgidx_edit.Text;
                    }

                }

                break;
            case "GvTypeAns":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvTypeAns.EditIndex != e.Row.RowIndex)
                    {
                        Label lblm0_taidx = (Label)e.Row.Cells[0].FindControl("lblm0_taidx");
                        GridView GvQuestion = (GridView)e.Row.Cells[0].FindControl("GvQuestion");
                        ViewState["m0_taidx_"] = lblm0_taidx.Text;
                        select_getquestion(GvQuestion, int.Parse(lblm0_taidx.Text), int.Parse(ViewState["m0_toidx"].ToString()));

                    }
                }
                break;

            case "GvQuestion":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    Label lblm0_taidx = (Label)e.Row.Cells[0].FindControl("lblm0_taidx");
                    Label lblm0_tqidx = (Label)e.Row.Cells[0].FindControl("lblm0_tqidx");
                    Label lblm0_quidx = (Label)e.Row.Cells[1].FindControl("lblm0_quidx");
                    Panel panel_choice_rdo = (Panel)e.Row.Cells[1].FindControl("panel_choice_rdo");
                    Panel panel_choice_chk = (Panel)e.Row.Cells[1].FindControl("panel_choice_chk");
                    Panel panel_comment = (Panel)e.Row.Cells[1].FindControl("panel_comment");
                    RadioButtonList rdochoice = (RadioButtonList)e.Row.Cells[1].FindControl("rdochoice");
                    CheckBoxList chkchoice = (CheckBoxList)e.Row.Cells[1].FindControl("chkchoice");
                    //Label lblm0_chidx = (Label)e.Row.Cells[1].FindControl("lblm0_chidx");
                    //TextBox txtrdo = (TextBox)e.Row.Cells[1].FindControl("txtrdo");
                    //TextBox txtchk = (TextBox)e.Row.Cells[1].FindControl("txtchk");
                    int b = 0;

                    _dtresign = new data_resignation();

                    _dtresign.Boxm0_Question = new M0_Question[1];
                    M0_Question _qustion = new M0_Question();

                    _qustion.m0_quidx = int.Parse(lblm0_quidx.Text);
                    _qustion.m0_taidx = int.Parse(ViewState["m0_taidx_"].ToString());

                    _dtresign.Boxm0_Question[0] = _qustion;
                    //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

                    _dtresign = callServicePostResignation(_urlSelect_SubQuestion_Resignation, _dtresign);

                    if (ViewState["m0_taidx_"].ToString() == "1")
                    {
                        //rdochoice.DataSource = _dtresign.Boxm0_Question;
                        //rdochoice.DataTextField = "choice_name";
                        //rdochoice.DataValueField = "m1_quidx";
                        //rdochoice.DataBind();

                        //chkchoice.DataSource = _dtresign.Boxm0_Question;
                        //chkchoice.DataTextField = "choice_name";
                        //chkchoice.DataValueField = "m1_quidx";
                        //chkchoice.DataBind();


                        for (int ex = 0; ex < _dtresign.Boxm0_Question.Count(); ex++)
                        {

                            //chkchoice.Items.Add(new ListItem(String.Format("{"+ ex + "'}<input id=\"txtchk{" +ex+"}\" name=\"txtchk{" + ex +"}\" / >" )+ _dtresign.Boxm0_Question[ex].choice_name, _dtresign.Boxm0_Question[ex].m1_quidx.ToString()));

                            if (_dtresign.Boxm0_Question[ex].m0_chidx.ToString() == "2")
                            {
                                chkchoice.Items.Add(new ListItem(_dtresign.Boxm0_Question[ex].choice_name + String.Format("<textarea  id =\"txtchk" + _dtresign.Boxm0_Question[ex].m1_quidx.ToString() + b + "\" name =\"txtchk" + _dtresign.Boxm0_Question[ex].m1_quidx.ToString() + b + "\" runat=\"server\"  class=\"form-control\" row=\"5\" cols=\"50\">" + "</textarea>"), _dtresign.Boxm0_Question[ex].m1_quidx.ToString()));

                                rdochoice.Items.Add(new ListItem(_dtresign.Boxm0_Question[ex].choice_name + String.Format("<textarea  id =\"txtrdo" + _dtresign.Boxm0_Question[ex].m1_quidx.ToString() + b + "\" name =\"txtrdo" + _dtresign.Boxm0_Question[ex].m1_quidx.ToString() + b + "\" runat=\"server\"  class=\"form-control\" row=\"5\" cols=\"50\">" + "</textarea>"), _dtresign.Boxm0_Question[ex].m1_quidx.ToString()));

                            }
                            else
                            {
                                //chkchoice_exit.Items.Add(new ListItem(_dtresign.Boxm0_Question[ex].choice_name + String.Format("<textarea  id =\"txtchk\" style=\"display:none\" name =\"txtchk\" runat=\"server\"  class=\"form-control\" row=\"5\" cols=\"50\">" + "</textarea>"), _dtresign.Boxm0_Question[ex].m1_quidx.ToString()));

                                chkchoice.Items.Add(new ListItem(_dtresign.Boxm0_Question[ex].choice_name + String.Format("<textarea  id =\"txtchk" + _dtresign.Boxm0_Question[ex].m1_quidx.ToString() + b + "\" name =\"txtchk" + _dtresign.Boxm0_Question[ex].m1_quidx.ToString() + b + "\" style=\"display:none\" runat=\"server\"  class=\"form-control\" row=\"5\" cols=\"50\">" + "</textarea>"), _dtresign.Boxm0_Question[ex].m1_quidx.ToString()));


                                rdochoice.Items.Add(new ListItem(_dtresign.Boxm0_Question[ex].choice_name + String.Format("<textarea  id =\"txtrdo" + _dtresign.Boxm0_Question[ex].m1_quidx.ToString() + b + "\" name =\"txtrdo" + _dtresign.Boxm0_Question[ex].m1_quidx.ToString() + b + "\" style=\"display:none\" runat=\"server\"  class=\"form-control\" row=\"5\" cols=\"50\">" + "</textarea>"), _dtresign.Boxm0_Question[ex].m1_quidx.ToString()));
                            }

                            if (lblm0_tqidx.Text == "1")
                            {
                                panel_choice_chk.Visible = true;
                                panel_choice_rdo.Visible = false;
                            }
                            else if (lblm0_tqidx.Text == "2")
                            {
                                panel_choice_chk.Visible = false;
                                panel_choice_rdo.Visible = true;
                            }
                        }

                        panel_comment.Visible = false;
                    }
                    else if (ViewState["m0_taidx_"].ToString() == "2")
                    {
                        panel_choice_rdo.Visible = false;
                        panel_choice_chk.Visible = false;
                        panel_comment.Visible = true;
                    }

                }
                break;

            case "GvEditQuestion":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvEditQuestion.EditIndex != e.Row.RowIndex)
                    {
                        Label lblm0_chidx = (Label)e.Row.Cells[0].FindControl("lblm0_chidx");
                        Label lblm0_tqidx = (Label)e.Row.Cells[0].FindControl("lblm0_tqidx");
                        DropDownList ddltypequest = (DropDownList)e.Row.Cells[1].FindControl("ddltypequest");
                        DropDownList ddltypechoice = (DropDownList)e.Row.Cells[1].FindControl("ddltypechoice");
                        Control id_typequest = (Control)e.Row.Cells[1].FindControl("id_typequest");
                        LinkButton Delete = (LinkButton)e.Row.Cells[3].FindControl("Delete");



                        e.Row.BackColor = System.Drawing.Color.GhostWhite;

                        if (ViewState["m0_taidx_edit"].ToString() == "1")
                        {
                            Select_TypeQuestion(ddltypequest);
                            ddltypequest.SelectedValue = lblm0_tqidx.Text;
                            gettypechoice(ddltypechoice);
                            ddltypechoice.SelectedValue = lblm0_chidx.Text;
                            id_typequest.Visible = true;
                            GvEditQuestion.Columns[2].Visible = true;
                            Delete.Visible = true;


                        }
                        else
                        {
                            GvEditQuestion.Columns[2].Visible = false;
                            id_typequest.Visible = false;
                            Delete.Visible = false;
                        }

                    }

                    if (GvEditQuestion.EditIndex == e.Row.RowIndex)
                    {
                        var ddltypequest_edit = (DropDownList)e.Row.FindControl("ddltypequest_edit");
                        var lblm0_tqidx_edit = (Label)e.Row.FindControl("lblm0_tqidx_edit");
                        var ddltypechoice_edit = (DropDownList)e.Row.FindControl("ddltypechoice_edit");
                        var lblm0_chidx_edit = (Label)e.Row.FindControl("lblm0_chidx_edit");
                        var id_typequest_edit = (Control)e.Row.FindControl("id_typequest_edit");

                        e.Row.BackColor = System.Drawing.Color.GhostWhite;
                        Select_TypeQuestion(ddltypequest_edit);
                        ddltypequest_edit.SelectedValue = lblm0_tqidx_edit.Text;
                        gettypechoice(ddltypechoice_edit);
                        ddltypechoice_edit.SelectedValue = lblm0_chidx_edit.Text;

                        if (ViewState["m0_taidx_edit"].ToString() == "1")
                        {

                            id_typequest_edit.Visible = true;


                        }
                        else
                        {
                            id_typequest_edit.Visible = false;
                        }

                        //for (int rowIndex = GvEditQuestion.Rows.Count - 2; rowIndex >= 0; rowIndex--)
                        //{
                        //    GridViewRow currentRow = GvEditQuestion.Rows[rowIndex];
                        //    GridViewRow previousRow = GvEditQuestion.Rows[rowIndex + 1];


                        //    if (((TextBox)currentRow.Cells[0].FindControl("txtquest_name")).Text == ((TextBox)previousRow.Cells[0].FindControl("txtquest_name")).Text)
                        //    {
                        //        if (previousRow.Cells[0].RowSpan < 2)
                        //        {
                        //            currentRow.Cells[0].RowSpan = 2;
                        //            //currentRow.Cells[1].RowSpan = 2;
                        //        }
                        //        else
                        //        {
                        //            currentRow.Cells[0].RowSpan = previousRow.Cells[0].RowSpan + 1;
                        //            //currentRow.Cells[1].RowSpan = previousRow.Cells[1].RowSpan + 1;
                        //        }
                        //        previousRow.Cells[0].Visible = false;
                        //        //previousRow.Cells[1].Visible = false;


                        //    }
                        //}
                    }
                }
                break;
        }
    }

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvTopic":
                GvTopic.EditIndex = e.NewEditIndex;
                select_Topic(GvTopic);
                break;
            case "GvEditQuestion":

                GvEditQuestion.EditIndex = e.NewEditIndex;
                if (ViewState["m0_taidx_edit"].ToString() == "1")
                {

                    select_geteditsubquestion(GvEditQuestion, int.Parse(ViewState["m0_quidx_edit"].ToString()), int.Parse(ViewState["m0_taidx_edit"].ToString()));
                }
                else if (ViewState["m0_taidx_edit"].ToString() == "2")
                {
                    select_geteditquestion(GvEditQuestion, int.Parse(ViewState["m0_quidx_edit"].ToString()));
                }
                break;



        }
    }

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvTopic":
                GvTopic.EditIndex = -1;
                select_Topic(GvTopic);
                break;
            case "GvEditQuestion":
                GvEditQuestion.EditIndex = -1;
                if (ViewState["m0_taidx_edit"].ToString() == "1")
                {

                    select_geteditsubquestion(GvEditQuestion, int.Parse(ViewState["m0_quidx_edit"].ToString()), int.Parse(ViewState["m0_taidx_edit"].ToString()));
                    mergeCell(GvEditQuestion);


                }
                else if (ViewState["m0_taidx_edit"].ToString() == "2")
                {
                    select_geteditquestion(GvEditQuestion, int.Parse(ViewState["m0_quidx_edit"].ToString()));
                }
                break;

        }
    }

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvTopic":
                int txtm0_toidx = Convert.ToInt32(GvTopic.DataKeys[e.RowIndex].Values[0].ToString());
                var txttopic = (TextBox)GvTopic.Rows[e.RowIndex].FindControl("txttopic");

                GvTopic.EditIndex = -1;
                Update_Topic(txtm0_toidx, txttopic.Text);
                select_Topic(GvTopic);
                break;

            case "GvEditQuestion":

                int txtm1_quidx = Convert.ToInt32(GvEditQuestion.DataKeys[e.RowIndex].Values[0].ToString());
                var txtm0_quidx = (TextBox)GvEditQuestion.Rows[e.RowIndex].FindControl("txtm0_quidx");
                var txtquest_name = (TextBox)GvEditQuestion.Rows[e.RowIndex].FindControl("txtquest_name");
                var txtanswer = (TextBox)GvEditQuestion.Rows[e.RowIndex].FindControl("txtanswer");
                var ddltypequest_edit = (DropDownList)GvEditQuestion.Rows[e.RowIndex].FindControl("ddltypequest_edit");
                var ddltypechoice_edit = (DropDownList)GvEditQuestion.Rows[e.RowIndex].FindControl("ddltypechoice_edit");

                GvEditQuestion.EditIndex = -1;

                Update_Resignation(txtm1_quidx, int.Parse(txtm0_quidx.Text), txtquest_name.Text, int.Parse(ddltypequest_edit.SelectedValue), txtanswer.Text, int.Parse(ddltypechoice_edit.SelectedValue));

                if (ViewState["m0_taidx_edit"].ToString() == "1")
                {
                    select_geteditsubquestion(GvEditQuestion, int.Parse(ViewState["m0_quidx_edit"].ToString()), int.Parse(ViewState["m0_taidx_edit"].ToString()));
                    mergeCell(GvEditQuestion);
                }
                else if (ViewState["m0_taidx_edit"].ToString() == "2")
                {
                    select_geteditquestion(GvEditQuestion, int.Parse(ViewState["m0_quidx_edit"].ToString()));
                }
                break;
        }
    }

    protected void onRowCommand(Object sender, GridViewCommandEventArgs e)
    {
        if (sender is GridView)
        {
            string cmdName = e.CommandName;
            switch (cmdName)
            {
                case "bnDeleteList":
                    GridView GvReportAdd = (GridView)fv_insert.FindControl("GvReportAdd");
                    GridViewRow rowSelect = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    DropDownList ddltypeans = (DropDownList)fv_insert.FindControl("ddltypeans");
                    Control div_save = (Control)fv_insert.FindControl("div_save");

                    int rowselect = rowSelect.RowIndex;

                    DataSet dsContacts = (DataSet)ViewState["vsBuyequipment"];
                    dsContacts.Tables["dsAddListTable"].Rows[rowselect].Delete();
                    dsContacts.AcceptChanges();
                    setGridData(GvReportAdd, dsContacts.Tables["dsAddListTable"]);


                    if (dsContacts.Tables["dsAddListTable"].Rows.Count < 1)
                    {
                        GvReportAdd.Visible = false;
                        div_save.Visible = false;
                    }


                    break;
            }
        }
    }

    protected void mergeCell(GridView GvName)
    {
        switch (GvName.ID)
        {
            case "GvReportAdd":
                for (int rowIndex = GvReportAdd.Rows.Count - 2; rowIndex >= 0; rowIndex--)
                {
                    GridViewRow currentRow = GvReportAdd.Rows[rowIndex];
                    GridViewRow previousRow = GvReportAdd.Rows[rowIndex + 1];

                    if (((Label)currentRow.Cells[0].FindControl("lbcourse_item")).Text == ((Label)previousRow.Cells[0].FindControl("lbcourse_item")).Text)
                    {
                        if (previousRow.Cells[0].RowSpan < 2)
                        {
                            currentRow.Cells[0].RowSpan = 2;

                        }
                        else
                        {
                            currentRow.Cells[0].RowSpan = previousRow.Cells[0].RowSpan + 1;

                        }
                        previousRow.Cells[0].Visible = false;
                    }

                    if (((Label)currentRow.Cells[1].FindControl("lbquest")).Text == ((Label)previousRow.Cells[1].FindControl("lbquest")).Text)
                    {
                        if (previousRow.Cells[1].RowSpan < 2)
                        {
                            currentRow.Cells[1].RowSpan = 2;
                            currentRow.Cells[2].RowSpan = 2;

                        }
                        else
                        {
                            currentRow.Cells[1].RowSpan = previousRow.Cells[1].RowSpan + 1;
                            currentRow.Cells[2].RowSpan = previousRow.Cells[2].RowSpan + 1;

                        }
                        previousRow.Cells[1].Visible = false;
                        previousRow.Cells[2].Visible = false;
                    }

                }
                break;

            case "GvEditQuestion":
                for (int rowIndex = GvEditQuestion.Rows.Count - 2; rowIndex >= 0; rowIndex--)
                {

                    GridViewRow currentRow = GvEditQuestion.Rows[rowIndex];
                    GridViewRow previousRow = GvEditQuestion.Rows[rowIndex + 1];


                    if (((Label)currentRow.Cells[0].FindControl("lblno_choice")).Text == ((Label)previousRow.Cells[0].FindControl("lblno_choice")).Text)
                    {
                        if (previousRow.Cells[1].RowSpan < 2)
                        {
                            currentRow.Cells[0].RowSpan = 2;

                        }
                        else
                        {
                            currentRow.Cells[0].RowSpan = previousRow.Cells[0].RowSpan + 1;

                        }
                        previousRow.Cells[0].Visible = false;
                    }

                    if (((Label)currentRow.Cells[1].FindControl("lblm0_quidx")).Text == ((Label)previousRow.Cells[1].FindControl("lblm0_quidx")).Text)
                    {
                        if (previousRow.Cells[1].RowSpan < 2)
                        {
                            currentRow.Cells[1].RowSpan = 2;

                        }
                        else
                        {
                            currentRow.Cells[1].RowSpan = previousRow.Cells[1].RowSpan + 1;

                        }
                        previousRow.Cells[1].Visible = false;
                    }
                }

                break;
        }
    }

    #endregion

    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        TextBox txttopic = (TextBox)fv_insert.FindControl("txttopic");
        TextBox txtcourse_item = (TextBox)fv_insert.FindControl("txtcourse_item");
        Control div_save = (Control)fv_insert.FindControl("div_save");
        TextBox txtquest = (TextBox)fv_insert.FindControl("txtquest");
        TextBox txtchoice = (TextBox)fv_insert.FindControl("txtchoice");
        DropDownList ddlchooseqs = (DropDownList)fv_insert.FindControl("ddlchooseqs");
        DropDownList ddltypechoice = (DropDownList)fv_insert.FindControl("ddltypechoice");
        DropDownList ddltypequest = (DropDownList)fv_insert.FindControl("ddltypequest");
        DropDownList ddlorgidx = (DropDownList)fv_insert.FindControl("ddlorgidx");


        switch (cmdName)
        {
            case "_divMenuBtnToDivIndex":
                SetDefaultpage(1);
                break;

            case "_divMenuBtnToDivAdd":

                SetDefaultpage(2);
                break;

            case "_divMenuBtnToDivManual":
                Response.Write("<script>window.open('https://docs.google.com/document/d/1IRTsf74ku7P_m7ms_8pjh059RO-DzM0P-utk5gTCV7Q/edit?usp=sharing','_blank');</script>");

                break;

            case "btnAdd":
                if (ddlchooseqs.SelectedValue == "1")
                {
                    Insert_Topic(txttopic.Text, int.Parse(ViewState["EmpIDX"].ToString()), int.Parse(ddlorgidx.SelectedValue));
                }
                else
                {
                    if (ddltypeans.SelectedValue == "1")
                    {
                        InsertQuestion();
                    }
                    else if (ddltypeans.SelectedValue == "2")
                    {
                        InsertQuestion_Writing();
                    }
                }
                SetDefaultpage(1);
                break;

            case "CmdAdddataset":
                if (int.Parse(txtcourse_item.Text) < 1)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาเลือกลำดับให้ถูกต้อง!!!');", true);
                    return;
                }
                else
                {
                    if (ddltypeans.SelectedValue == "1")
                    {
                        setAddList_Form(txtcourse_item, txtquest, ddltypequest, txtchoice, ddltypechoice, GvReportAdd);
                    }
                    else
                    {
                        setAddList_FormWrite(txtcourse_item, txtquest, GvWrite);
                    }
                    txtchoice.Text = String.Empty;

                    div_save.Visible = true;
                }
                break;

            case "CmdViewDetail":
                string[] arg1_ = new string[1];
                arg1_ = e.CommandArgument.ToString().Split(';');
                ViewState["m0_toidx"] = arg1_[0];
                SetDefaultpage(3);

                break;

            case "CmdViewEdit":
                string[] arg1_edit = new string[3];
                arg1_edit = e.CommandArgument.ToString().Split(';');

                ViewState["m0_quidx_edit"] = arg1_edit[1].ToString();
                ViewState["m0_taidx_edit"] = arg1_edit[2].ToString();
                MvMaster.SetActiveView(ViewEdit);
                if (arg1_edit[2].ToString() == "1")
                {

                    select_geteditsubquestion(GvEditQuestion, int.Parse(arg1_edit[1].ToString()), int.Parse(arg1_edit[2].ToString()));
                    mergeCell(GvEditQuestion);


                }
                else if (arg1_edit[2].ToString() == "2")
                {
                    select_geteditquestion(GvEditQuestion, int.Parse(arg1_edit[1].ToString()));
                }
                setOntop.Focus();
                break;

            case "btnCancel_Detail":
                SetDefaultpage(3);
                setOntop.Focus();
                break;

            case "CmdDelete":
                string[] arg1_delete = new string[1];
                arg1_delete = e.CommandArgument.ToString().Split(';');

                Delete_Topic(int.Parse(arg1_delete[0].ToString()), int.Parse(ViewState["EmpIDX"].ToString()));
                SetDefaultpage(1);

                break;

            case "btnCancel":
                SetDefaultpage(1);
                setOntop.Focus();
                break;

            case "CmdDel_subquest":
                string[] arg1_deletesub = new string[1];
                arg1_deletesub = e.CommandArgument.ToString().Split(';');
                Delete_SubTopic(int.Parse(arg1_deletesub[0].ToString()), int.Parse(ViewState["EmpIDX"].ToString()));

                if (ViewState["m0_taidx_edit"].ToString() == "1")
                {
                    select_geteditsubquestion(GvEditQuestion, int.Parse(ViewState["m0_quidx_edit"].ToString()), int.Parse(ViewState["m0_taidx_edit"].ToString()));
                    mergeCell(GvEditQuestion);
                }
                else if (ViewState["m0_taidx_edit"].ToString() == "2")
                {
                    select_geteditquestion(GvEditQuestion, int.Parse(ViewState["m0_quidx_edit"].ToString()));
                }
                break;
        }
    }
    #endregion


}