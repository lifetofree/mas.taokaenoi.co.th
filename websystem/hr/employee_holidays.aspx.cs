﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_hr_employee_holidays : System.Web.UI.Page
{
    #region initial function/data
    function_tool _funcTool = new function_tool();
    service_execute serviceexcute = new service_execute();
    data_employee _dataEmployee = new data_employee();
    data_holiday _data_holiday = new data_holiday();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];

    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    //-- employee --//

    //-- holiday --//
    static string _urlSetempsHoliday = _serviceUrl + ConfigurationManager.AppSettings["urlSetempsHoliday"];
    static string _urlGetempsDetailHoliday = _serviceUrl + ConfigurationManager.AppSettings["urlGetempsDetailHoliday"];
    static string _urlSetempsDeleteHoliday = _serviceUrl + ConfigurationManager.AppSettings["urlSetempsDeleteHoliday"];
    static string _urlGetempsYearsHoliday = _serviceUrl + ConfigurationManager.AppSettings["urlGetempsYearsHoliday"];
    static string _urlSetManageHoildayOrg = _serviceUrl + ConfigurationManager.AppSettings["urlSetManageHoildayOrg"];
    static string _urlGetDetailHolidayOrg = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailHolidayOrg"];
    static string _urlGetViewDetailHolidayOrg = _serviceUrl + ConfigurationManager.AppSettings["urlGetViewDetailHolidayOrg"];
    static string _urlGetDetailHolidayCreateOrg = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailHolidayCreateOrg"];
    static string _urlSetDeleteDtailHolidayOrg = _serviceUrl + ConfigurationManager.AppSettings["urlSetDeleteDtailHolidayOrg"];
    static string _urlGetDetailOrgNameHoliday = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailOrgNameHoliday"];
    //-- holiday --//

    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;

    int decision_hr_addusecar = 6; //hr add use car

    decimal tot_actual = 0;
    int returnResult = 0;
    int condition_ = 0;
    int _set_statusFilter = 0;


    int _condition_year_current = 0;
    int _condition_year = 0;


    //set rpos hr tab report carbooking
    //string set_rpos_idx_hr = "5911,5912,5883,5896,5897";
    string set_rsec_idx_hr = "436";


    //ViewState["vs_ViewWaitApproveRoomBooking"]

    #endregion initial function/data

    protected void Page_Init(object sender, EventArgs e)
    {

        _condition_year_current = DateTime.Today.Year;

        _emp_idx = int.Parse(Session["emp_idx"].ToString());

        getEmployeeProfile(_emp_idx);
        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
        //check permission
        //foreach (int item in rdept_qmr)
        //{
        //    if (_dataEmployee.employee_list[0].rdept_idx == item)
        //    {
        //        _flag_qmr = true;
        //        break;
        //    }
        //}
        ViewState["org_permission"] = _dataEmployee.employee_list[0].org_idx;
        ViewState["rsec_permission"] = _dataEmployee.employee_list[0].rsec_idx;
        ViewState["rdept_permission"] = _dataEmployee.employee_list[0].rdept_idx;
        ViewState["rpos_permission"] = _dataEmployee.employee_list[0].rpos_idx;
        ViewState["joblevel_permission"] = _dataEmployee.employee_list[0].jobgrade_level;

        Session["org_idx"] = int.Parse(ViewState["org_permission"].ToString());


        
        //Session["DetailCarUseIDXSearch"] = 0;
        //Session["DetailtypecarSearch"] = 0;
        //Session["ddlm0car_idxSearch"] = 0;


        //Set Permission Tab Report with HR
        string[] setTabReportHR = set_rsec_idx_hr.Split(',');
        for (int i = 0; i < setTabReportHR.Length; i++)
        {
            if (setTabReportHR[i] == ViewState["rsec_permission"].ToString() || _emp_idx == 1413 || _emp_idx == 23069 || _emp_idx == 32528 || _emp_idx == 32534)
            {
                li1.Visible = true;
                li2.Visible = true;
                li3.Visible = true;
                li4.Visible = true;

                break;
            }
            else
            {
                li1.Visible = false;
                li2.Visible = false;
                li3.Visible = false;
                li4.Visible = false;

            }

        }
        //Set Permission Tab Report with HR

    }

    protected void Page_Load(object sender, EventArgs e)
    {

        setPanelTitle();

        if (!IsPostBack)
        {
            initPage();
            initPageLoad();

            
        }

    }

    #region set/get bind data

    protected void getDetailOrganization(GridView gvName)
    {
        _dataEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dataEmployee.organization_list[0] = _orgList;

        _dataEmployee = callServicePostEmployee(_urlGetOrganizationList, _dataEmployee);
       // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        ViewState["Vs_DetailOrganization"] = _dataEmployee.organization_list;
        gvName.Visible = true;
        setGridData(gvName, ViewState["Vs_DetailOrganization"]);
    }

    protected void getOrganizationList(DropDownList ddlName)
    {
        _dataEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dataEmployee.organization_list[0] = _orgList;

        _dataEmployee = callServicePostEmployee(_urlGetOrganizationList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกองค์กร ---", "0"));
    }

    protected void getDepartmentList(DropDownList ddlName, int _org_idx)
    {
        _dataEmployee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dataEmployee.department_list[0] = _deptList;

        _dataEmployee = callServicePostEmployee(_urlGetDepartmentList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกฝ่าย ---", "0"));
    }

    protected void getddlYearHoliday(DropDownList ddlName, int _year_idx)
    {


        data_holiday data_holiday_year_detail = new data_holiday();
        emps_u0_holiday_detail u0_holiday_year_detail = new emps_u0_holiday_detail();
        data_holiday_year_detail.emps_u0_holiday_list = new emps_u0_holiday_detail[1];
        //u0_holiday_year_detail.org_idx = _org_idx;

        data_holiday_year_detail.emps_u0_holiday_list[0] = u0_holiday_year_detail;

        data_holiday_year_detail = callServicePostHoliday(_urlGetempsYearsHoliday, data_holiday_year_detail);
        setDdlData(ddlName, data_holiday_year_detail.emps_u0_holiday_list, "year_holiday", "year_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกปี ---", "0"));

        if (_year_idx != 0)
        {
            //litDebug.Text = _year_idx.ToString(); 
            ddlName.SelectedValue = _year_idx.ToString();
        }
        else
        {

            ddlName.SelectedValue = _condition_year_current.ToString();
            //litDebug.Text = "2";//ddlName.SelectedValue.ToString();
            //_condition_year = _condition_year_current;

            //litDebug.Text = ddlName.SelectedValue.ToString();
        }



    }

    protected void getddlYear(DropDownList ddlName)
    {

        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Clear();

        var currentYear = DateTime.Today.Year;
        for (int i = 5; i >= 0; i--)
        {
            ddlName.Items.Add((currentYear - i).ToString());
        }
        //ddlName.Items.Insert(0, new ListItem("--- เลือกปี ---", "0"));
        ddlName.Items.FindByValue(Convert.ToString(currentYear)).Selected = true;
    }

    protected void getDetailHoliday(GridView gvName, int _codition, string _year_holiday)
    {

        data_holiday data_holiday_detail = new data_holiday();
        emps_u0_holiday_detail u0_holiday_detail = new emps_u0_holiday_detail();
        data_holiday_detail.emps_u0_holiday_list = new emps_u0_holiday_detail[1];

        u0_holiday_detail.condition = _codition;
        u0_holiday_detail.year_holiday = _year_holiday;

        data_holiday_detail.emps_u0_holiday_list[0] = u0_holiday_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_roomdetail));        
        data_holiday_detail = callServicePostHoliday(_urlGetempsDetailHoliday, data_holiday_detail);


        ViewState["vs_DetailHoliday"] = data_holiday_detail.emps_u0_holiday_list;
        setGridData(gvName, ViewState["vs_DetailHoliday"]);

    }

    protected void getSearchDetailHolidayCreateOrg(GridView gvName, int _org, string _year_holiday)
    {

        data_holiday data_holiday_searchcreateorg = new data_holiday();
        emps_u0_holiday_manage_detail u0_holiday_searchcreateorg = new emps_u0_holiday_manage_detail();
        data_holiday_searchcreateorg.emps_u0_holiday_manage_list = new emps_u0_holiday_manage_detail[1];

        u0_holiday_searchcreateorg.org_idx = _org;
        u0_holiday_searchcreateorg.year_holiday = _year_holiday;

        data_holiday_searchcreateorg.emps_u0_holiday_manage_list[0] = u0_holiday_searchcreateorg;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_holiday_searchcreateorg));
        data_holiday_searchcreateorg = callServicePostHoliday(_urlGetDetailHolidayCreateOrg, data_holiday_searchcreateorg);

        if(data_holiday_searchcreateorg.return_code == 0)
        {
            Panel_DetailHolidaySearchCreateOrg.Visible = true;
            ViewState["vs_u0_manage_idx_createorg"] = data_holiday_searchcreateorg.emps_u0_holiday_manage_list[0].u0_manage_idx;
            ViewState["vs_DetailHolidaySearchCreateOrg"] = data_holiday_searchcreateorg.emps_u0_holiday_manage_list;
            setGridData(gvName, ViewState["vs_DetailHolidaySearchCreateOrg"]);
        }
       
    }

    protected void getDetailHolidayOrg(GridView gvName, int _codition, string _year_holiday)
    {

        data_holiday data_holiday_detailorg = new data_holiday();
        emps_u0_holiday_manage_detail u0_holiday_detailorg = new emps_u0_holiday_manage_detail();
        data_holiday_detailorg.emps_u0_holiday_manage_list = new emps_u0_holiday_manage_detail[1];

        u0_holiday_detailorg.condition = _codition;
        u0_holiday_detailorg.year_holiday = _year_holiday;

        data_holiday_detailorg.emps_u0_holiday_manage_list[0] = u0_holiday_detailorg;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_roomdetail));        
        data_holiday_detailorg = callServicePostHoliday(_urlGetDetailHolidayOrg, data_holiday_detailorg);


        ViewState["vs_DetailHolidayOrg"] = data_holiday_detailorg.emps_u0_holiday_manage_list;
        gvName.Visible = true;
        setGridData(gvName, ViewState["vs_DetailHolidayOrg"]);

    }

    protected void getViewDetailHolidayOrg(GridView gvName, int _u0_manage_idx, string _year_holiday, int _codition)
    {

        data_holiday data_holiday_viewdetailorg = new data_holiday();
        emps_u1_holiday_manage_detail u0_holiday_viewdetailorg = new emps_u1_holiday_manage_detail();
        data_holiday_viewdetailorg.emps_u1_holiday_manage_list = new emps_u1_holiday_manage_detail[1];

        u0_holiday_viewdetailorg.condition = _codition;
        u0_holiday_viewdetailorg.u0_manage_idx = _u0_manage_idx;
        u0_holiday_viewdetailorg.year_holiday = _year_holiday;

        data_holiday_viewdetailorg.emps_u1_holiday_manage_list[0] = u0_holiday_viewdetailorg;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_roomdetail));        
        data_holiday_viewdetailorg = callServicePostHoliday(_urlGetViewDetailHolidayOrg, data_holiday_viewdetailorg);


        ViewState["vs_ViewDetailHolidayOrg"] = data_holiday_viewdetailorg.emps_u1_holiday_manage_list;
        gvName.Visible = true;
        setGridData(gvName, ViewState["vs_ViewDetailHolidayOrg"]);

    }

    protected void getViewDetailOrgName(int _org_idx)
    {

        data_holiday data_org_detail = new data_holiday();
        emps_u0_holiday_manage_detail u0_org_detail = new emps_u0_holiday_manage_detail();
        data_org_detail.emps_u0_holiday_manage_list = new emps_u0_holiday_manage_detail[1];

        u0_org_detail.org_idx = _org_idx;

        data_org_detail.emps_u0_holiday_manage_list[0] = u0_org_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_roomdetail));        
        data_org_detail = callServicePostHoliday(_urlGetDetailOrgNameHoliday, data_org_detail);


        lbl_DetailHolidayOrgName.Text = data_org_detail.emps_u0_holiday_manage_list[0].org_name_th;
        //ViewState["vs_ViewDetailOrgName"] = data_org_detail.emps_u0_holiday_manage_list;
        //setGridData(gvName, ViewState["vs_DetailHoliday"]);

    }

    #endregion set/get bind data  

    #region set panel title
    protected void setPanelTitle()
    {
        string path = Request.Url.AbsolutePath;
        string module = path.Substring(path.LastIndexOf('/') + 1).ToLower();
        string moduleName;
        string path_file;

        //test Title Program
        string module_Value = path.Substring(path.IndexOf('/') + 1).ToLower();

        IFormatProvider culture = new CultureInfo("en-US", true);
        DateTime DateToday = DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy"), "dd/MM/yyyy", CultureInfo.InvariantCulture);

        //path_file = "~/masterpage/images/holiday-header/" + "room-booking" + ".png";

        //litDebug.Text = DateToday.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);//DateToday.ToString();

        
        string input = DateToday.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
        string[] result = input.Split(new string[] { "/" }, StringSplitOptions.None);

        //litDebug.Text = result[1].ToString();

        path_file = "~/masterpage/images/holiday-header/" + result[1].ToString() + ".jpg";
        //litDebug1.Text = path_file.ToString();
        moduleName = "<img src='" + ResolveUrl(path_file) + "' class='img-fluid' height='300px' width='100%' />";
        //moduleName = "<img src='" + ResolveUrl(path_file) + "' class='img-fluid' height='150px' width='100%' />";
       

        litPanelTitleCalendar.Text = moduleName;

    }
    #endregion set panel title

    #region event command
    protected void navCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        setActiveTab(cmdArg, 0, 0, 0, 0, 0, 0);

    }

    protected void ClickPrint(object sender, EventArgs e)
    {
        //  litDebug.Text = "lals";
    }

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        //linkBtnTrigger(lbDocSaveImport);

        switch (cmdName)
        {
            case "CmdSaveHoliday":

                TextBox txt_name_holidat = (TextBox)fvCreateHoliday.FindControl("txt_name_holidat");
                TextBox txt_date_holiday = (TextBox)fvCreateHoliday.FindControl("txt_date_holiday");

                //insert Holiday
                data_holiday _data_holiday_insert = new data_holiday();
                emps_u0_holiday_detail u0_holiday_detail = new emps_u0_holiday_detail();
                _data_holiday_insert.emps_u0_holiday_list = new emps_u0_holiday_detail[1];

                u0_holiday_detail.cemp_idx = _emp_idx;
                u0_holiday_detail.holiday_name = txt_name_holidat.Text;
                u0_holiday_detail.holiday_date = txt_date_holiday.Text;

                _data_holiday_insert.emps_u0_holiday_list[0] = u0_holiday_detail;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_holiday_insert));// vs_StatusRoomBooking

                _data_holiday_insert = callServicePostHoliday(_urlSetempsHoliday, _data_holiday_insert);
                if (_data_holiday_insert.return_code == 0)
                {
                    setActiveTab("docDetail", 0, 0, 0, 0, 0, 0);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- มีข้อมูลในระบบแล้ว ไม่สามารถเพิ่มได้ ---');", true);
                    break;
                }

                break;
            case "CmdSaveHolidayManage":

                DropDownList ddlorg = (DropDownList)fvManageHoliday.FindControl("ddlorg");
                //GridView GvCreateDetailHoliday = (GridView)fvManageHoliday.FindControl("GvCreateDetailHoliday");

                //insert u0 Holiday Manage
                data_holiday _data_holiday_manage = new data_holiday();
                emps_u0_holiday_manage_detail u0_holiday_manage_detail = new emps_u0_holiday_manage_detail();
                _data_holiday_manage.emps_u0_holiday_manage_list = new emps_u0_holiday_manage_detail[1];
                
                u0_holiday_manage_detail.u0_manage_idx = int.Parse(ViewState["vs_u0_manage_idx_createorg"].ToString());
                u0_holiday_manage_detail.cemp_idx = _emp_idx;
                u0_holiday_manage_detail.org_idx = int.Parse(ddlorg.SelectedValue);
               
                _data_holiday_manage.emps_u0_holiday_manage_list[0] = u0_holiday_manage_detail;

                //insert u1 manage
                //insert m1
                var _u1_holiday_detail = new emps_u1_holiday_manage_detail[GvCreateDetailHoliday.Rows.Count];
                int sumcheck_u1 = 0;
                int count_u1 = 0;
                //int sum_total_price = 0;

                foreach (GridViewRow gvrow in GvCreateDetailHoliday.Rows)
                {

                    _data_holiday_manage.emps_u1_holiday_manage_list = new emps_u1_holiday_manage_detail[1];

                    //CheckBox chk_date_ot = (CheckBox)gv_row.FindControl("chk_date_ot");
                    CheckBox chk_holiday = (CheckBox)gvrow.FindControl("chk_holiday");
                    Label lbl_holiday_idx_create = (Label)gvrow.FindControl("lbl_holiday_idx_create");
                    Label lbl_u0_manage_idx_create = (Label)gvrow.FindControl("lbl_u0_manage_idx_create");


                    if (chk_holiday.Checked == true)
                    {

                        _u1_holiday_detail[count_u1] = new emps_u1_holiday_manage_detail();
                        _u1_holiday_detail[count_u1].u0_manage_idx = int.Parse(lbl_u0_manage_idx_create.Text);
                        _u1_holiday_detail[count_u1].cemp_idx = _emp_idx;
                        _u1_holiday_detail[count_u1].holiday_idx = int.Parse(lbl_holiday_idx_create.Text);

                        sumcheck_u1 = sumcheck_u1 + 1;
                        count_u1++;

                    }

                }

                _data_holiday_manage.emps_u0_holiday_manage_list[0] = u0_holiday_manage_detail;
                _data_holiday_manage.emps_u1_holiday_manage_list = _u1_holiday_detail;
                //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_holiday_manage));

                _data_holiday_manage = callServicePostHoliday(_urlSetManageHoildayOrg, _data_holiday_manage);

                setActiveTab("docDetailOrg", 0, 0, 0, 0, 0, 0);

                break;
            case "CmdCancel":
                setActiveTab("docDetail", 0, 0, 0, 0, 0, 0);
                break;
            case "cmdBackToDetail":
                setActiveTab("docDetailOrg", 0, 0, 0, 0, 0, 0);
                break;

            case "cmdViewHolidayDetailOrg":
                string[] arg2 = new string[1];
                arg2 = e.CommandArgument.ToString().Split(';');
                int _u0_manage_idx_view = int.Parse(arg2[0]);
                int _org_idx_view = int.Parse(arg2[1]);

                setActiveTab("docDetailOrg", _u0_manage_idx_view, 0, 0, 0, 1, _org_idx_view);

                break;
            case "cmdRefreshViewDetailHolidayOrg":
                
                setActiveTab("docDetailOrg", int.Parse(lbl_ViewDetailHolidayOrg_u0idx.Text), 0, 0, 0, 1, int.Parse(lbl_ViewDetailHolidayOrg_orgidx.Text));

                break;

            case "cmdDeleteHolidayDetail":

                var holiday_idx = int.Parse(cmdArg);

                data_holiday _data_holiday_del = new data_holiday();
                emps_u0_holiday_detail u0_holiday_detail_del = new emps_u0_holiday_detail();
                _data_holiday_del.emps_u0_holiday_list = new emps_u0_holiday_detail[1];

                u0_holiday_detail_del.holiday_idx = holiday_idx;
                u0_holiday_detail_del.cemp_idx = _emp_idx;

                _data_holiday_del.emps_u0_holiday_list[0] = u0_holiday_detail_del;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_car_insert_tax));// vs_StatusRoomBooking ffff
                _data_holiday_del = callServicePostHoliday(_urlSetempsDeleteHoliday, _data_holiday_del);

                if (int.Parse(ddlyear.SelectedValue) != int.Parse(_condition_year_current.ToString()))
                {
                    getDetailHoliday(GvDetailHoliday, 1, ddlyear.SelectedValue.ToString());
                }
                else
                {
                    getDetailHoliday(GvDetailHoliday, 0, _condition_year_current.ToString());
                }

                //setActiveTab("docDetail", 0, 0, 0, 0, 0, 0);

                break;
            case "cmdRefresh":

                setActiveTab("docDetail", 0, 0, 0, 0, 0, 0);
                break;

            case "cmdExport":

                data_holiday _data_holiday_export = new data_holiday();
                emps_u0_holiday_detail u0_holiday_detail_export = new emps_u0_holiday_detail();
                _data_holiday_export.emps_u0_holiday_list = new emps_u0_holiday_detail[1];


                if(int.Parse(ddlyear.SelectedValue) != int.Parse(_condition_year_current.ToString()))
                {
                    u0_holiday_detail_export.condition = 1;
                }
                else
                {
                    u0_holiday_detail_export.condition = 0;
                }
                u0_holiday_detail_export.year_holiday = ddlyear.SelectedValue.ToString();
                u0_holiday_detail_export.cemp_idx = _emp_idx;
                

                _data_holiday_export.emps_u0_holiday_list[0] = u0_holiday_detail_export;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_report_roombooking_export));
                _data_holiday_export = callServicePostHoliday(_urlGetempsDetailHoliday, _data_holiday_export);
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_report_roombooking));

                //Update_PanelReportTable.Visible = true;
                ViewState["Vs_DetailHoliday_Export"] = _data_holiday_export.emps_u0_holiday_list;
                //setGridData(GvDetailTableReport, ViewState["Vs_DetailReportRoomTable"]);

                int count_ = 0;

                if (_data_holiday_export.return_code == 0)
                {
                    DataTable tableholiday = new DataTable();
                    tableholiday.Columns.Add("วันที่หยุด", typeof(String));
                    tableholiday.Columns.Add("ชื่อวันหยุด", typeof(String));

                    foreach (var row_report in _data_holiday_export.emps_u0_holiday_list)
                    {
                        DataRow addRowHoliday = tableholiday.NewRow();

                        addRowHoliday[0] = row_report.holiday_date.ToString();
                        addRowHoliday[1] = row_report.holiday_name.ToString();

                        tableholiday.Rows.InsertAt(addRowHoliday, count_++);
                    }

                    WriteExcelWithNPOI(tableholiday, "xls", "report-holiday");
                    
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- ไม่พบข้อมูล ---');", true);
                    break;
                }

                break;
            case "cmdDeleteViewHolidayDetailOrg":

                //var u1_manage_idx_del = int.Parse(cmdArg);

                string[] arg3 = new string[2];
                arg3 = e.CommandArgument.ToString().Split(';');
                int u1_manage_idx_del = int.Parse(arg3[0]);
                int _org_idx_del = int.Parse(arg3[1]);
                int u0_manage_idx_del = int.Parse(arg3[2]);

                data_holiday _data_holiday_manag_del = new data_holiday();
                emps_u1_holiday_manage_detail u0_holiday_manag_detail_del = new emps_u1_holiday_manage_detail();
                _data_holiday_manag_del.emps_u1_holiday_manage_list = new emps_u1_holiday_manage_detail[1];

                u0_holiday_manag_detail_del.u1_manage_idx = u1_manage_idx_del;
                u0_holiday_manag_detail_del.cemp_idx = _emp_idx;

                _data_holiday_manag_del.emps_u1_holiday_manage_list[0] = u0_holiday_manag_detail_del;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_car_insert_tax));// vs_StatusRoomBooking ffff
                _data_holiday_manag_del = callServicePostHoliday(_urlSetDeleteDtailHolidayOrg, _data_holiday_manag_del);

                setActiveTab("docDetailOrg", u0_manage_idx_del, 0, 0, 0, 1, _org_idx_del);

                break;

            case "cmdExportViewDetailHolidayOrg":

                data_holiday _data_holiday_exportorg = new data_holiday();
                emps_u1_holiday_manage_detail u0_holiday_detail_exportorg = new emps_u1_holiday_manage_detail();
                _data_holiday_exportorg.emps_u1_holiday_manage_list = new emps_u1_holiday_manage_detail[1];


                if (int.Parse(ddlYearViewDetailHolidayOrg.SelectedValue) != int.Parse(_condition_year_current.ToString()))
                {
                    u0_holiday_detail_exportorg.condition = 1;
                }
                else
                {
                    u0_holiday_detail_exportorg.condition = 0;
                }
                u0_holiday_detail_exportorg.year_holiday = ddlYearViewDetailHolidayOrg.SelectedValue.ToString();
                u0_holiday_detail_exportorg.cemp_idx = _emp_idx;
                u0_holiday_detail_exportorg.u0_manage_idx = int.Parse(lbl_ViewDetailHolidayOrg_u0idx.Text);

                _data_holiday_exportorg.emps_u1_holiday_manage_list[0] = u0_holiday_detail_exportorg;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_report_roombooking_export));
                _data_holiday_exportorg = callServicePostHoliday(_urlGetViewDetailHolidayOrg, _data_holiday_exportorg);
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_report_roombooking));

                //Update_PanelReportTable.Visible = true;
                ViewState["Vs_DetailHoliday_ExportOrg"] = _data_holiday_exportorg.emps_u1_holiday_manage_list;
                //setGridData(GvDetailTableReport, ViewState["Vs_DetailReportRoomTable"]);

                int count_export = 0;

                if (_data_holiday_exportorg.return_code == 0)
                {
                    DataTable tableholiday_org = new DataTable();
                    tableholiday_org.Columns.Add("ชื่อบริษัท", typeof(String));
                    tableholiday_org.Columns.Add("วันที่หยุด", typeof(String));
                    tableholiday_org.Columns.Add("ชื่อวันหยุด", typeof(String));

                    foreach (var row_report in _data_holiday_exportorg.emps_u1_holiday_manage_list)
                    {
                        DataRow addRowHoliday = tableholiday_org.NewRow();

                        addRowHoliday[0] = row_report.org_name_th.ToString();
                        addRowHoliday[1] = row_report.holiday_date.ToString();
                        addRowHoliday[2] = row_report.holiday_name.ToString();

                        tableholiday_org.Rows.InsertAt(addRowHoliday, count_export++);
                    }

                    WriteExcelWithNPOI(tableholiday_org, "xls", "report-holidayorg");

                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- ไม่พบข้อมูล ---');", true);
                    break;
                }

                break;

        }

    }
    //endbtn


    #endregion event command

    #region selected Changed

    protected void onSelectedCheckChanged(object sender, EventArgs e)
    {
        var cbName = (CheckBox)sender;

        //var chkName = (CheckBoxList)sender;

        switch (cbName.ID)
        {
            case "chk_date_ot":

                ////litDebug.Text = "5";
                //int count_ = 0;
                //linkBtnTrigger(btnSaveOTMonth);
                //foreach (GridViewRow row in GvCreateOTMonth.Rows)
                //{
                //    CheckBox chk_date_ot = (CheckBox)row.FindControl("chk_date_ot");
                //    UpdatePanel Update_Timestart = (UpdatePanel)row.FindControl("Update_Timestart");
                //    UpdatePanel Update_Timeend = (UpdatePanel)row.FindControl("Update_Timeend");
                //    TextBox txt_job = (TextBox)row.FindControl("txt_job");
                //    TextBox txt_timestart_job = (TextBox)row.FindControl("txt_timestart_job");
                //    TextBox txt_timeend_job = (TextBox)row.FindControl("txt_timeend_job");
                //    TextBox txt_sum_hour = (TextBox)row.FindControl("txt_sum_hour");
                //    TextBox txt_head_set = (TextBox)row.FindControl("txt_head_set");
                //    TextBox txt_remark = (TextBox)row.FindControl("txt_remark");

                //    decimal value = Convert.ToDecimal(txt_sum_hour.Text);

                //    if (chk_date_ot.Checked)
                //    {
                //        linkBtnTrigger(btnSaveOTMonth);

                //        txt_job.Visible = true;
                //        txt_head_set.Visible = true;
                //        txt_remark.Visible = true;

                //        tot_actual_otmonth += Convert.ToDecimal(value);

                //        Label lit_total_hoursmonth = (Label)GvCreateOTMonth.FooterRow.Cells[5].FindControl("lit_total_hoursmonth");

                //        lit_total_hoursmonth.Text = String.Format("{0:N2}", tot_actual_otmonth);
                //        // GvCreateOTMonth.FooterRow.Cells[5] = FontBoldWeight.Bold;

                //        count_++;
                //    }
                //    else
                //    {

                //        txt_job.Text = string.Empty;
                //        txt_head_set.Text = string.Empty;
                //        txt_remark.Text = string.Empty;


                //        txt_job.Visible = false;
                //        txt_head_set.Visible = false;
                //        txt_remark.Visible = false;

                //        ////Label lit_total_hoursmonth = (Label)GvCreateOTMonth.FooterRow.Cells[5].FindControl("lit_total_hoursmonth");

                //        ////lit_total_hoursmonth.Text = String.Format("{0:N2}", "0.00");

                //        //txt_price_total.Enabled = false;


                //    }

                //    //not check ot month
                //    if (count_ == 0)
                //    {
                //        Label lit_total_hoursmonth = (Label)GvCreateOTMonth.FooterRow.Cells[5].FindControl("lit_total_hoursmonth");

                //        lit_total_hoursmonth.Text = String.Format("{0:N2}", "0.00");
                //    }
                //    //litDebug.Text = count_.ToString();

                //}
                break;

        }
    }

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlName = (DropDownList)sender;

        switch (ddlName.ID)
        {
            case "ddlyear":

                if (int.Parse(ddlyear.SelectedValue) != int.Parse(_condition_year_current.ToString()))
                {
                    getDetailHoliday(GvDetailHoliday, 1, ddlyear.SelectedValue.ToString());
                }
                else
                {
                    getDetailHoliday(GvDetailHoliday, 0, ddlyear.SelectedValue.ToString());
                }
                break;
            case "ddlYearViewDetailHolidayOrg":

                if (int.Parse(ddlYearViewDetailHolidayOrg.SelectedValue) != int.Parse(_condition_year_current.ToString()))
                {
                    //getDetailHoliday(GvDetailHoliday, 1, ddlyear.SelectedValue.ToString());
                    getViewDetailHolidayOrg(GvViewDetailHolidayOrg, int.Parse(lbl_ViewDetailHolidayOrg_u0idx.Text), ddlYearViewDetailHolidayOrg.SelectedValue.ToString(), 1);
                }
                else
                {
                    //getDetailHoliday(GvDetailHoliday, 0, ddlyear.SelectedValue.ToString());
                    getViewDetailHolidayOrg(GvViewDetailHolidayOrg, int.Parse(lbl_ViewDetailHolidayOrg_u0idx.Text), ddlYearViewDetailHolidayOrg.SelectedValue.ToString(), 0);
                }
                break;
            case "ddlyear_manage":
                DropDownList ddlorg_search_ = (DropDownList)fvManageHoliday.FindControl("ddlorg");
                DropDownList ddlyear_manage_ = (DropDownList)fvManageHoliday.FindControl("ddlyear_manage");
                //GridView GvCreateDetailHoliday_ = (GridView)fvManageHoliday.FindControl("GvCreateDetailHoliday");

                //_urlGetDetailHolidayCreateOrg
                if (int.Parse(ddlorg_search_.SelectedValue.ToString()) != 0 && ddlyear_manage_.SelectedValue.ToString() != "0")
                {
                    //litDebug.Text = "3";
                    getSearchDetailHolidayCreateOrg(GvCreateDetailHoliday, int.Parse(ddlorg_search_.SelectedValue.ToString()), ddlyear_manage_.SelectedValue.ToString());
                }
                else
                {
                    Panel_DetailHolidaySearchCreateOrg.Visible = false;
                    //litDebug.Text = "4";
                    ViewState["vs_u0_manage_idx_createorg"] = 0;
                    ViewState["vs_DetailHolidaySearchCreateOrg"] = null;
                    setGridData(GvCreateDetailHoliday, ViewState["vs_DetailHolidaySearchCreateOrg"]);
                }

                ////if (int.Parse(ddlyear_manage_.SelectedValue) != int.Parse(_condition_year_current.ToString()))
                ////{
                ////    getDetailHoliday(GvCreateDetailHoliday_, 1, ddlyear_manage_.SelectedValue.ToString());
                ////}
                ////else
                ////{
                ////    getDetailHoliday(GvCreateDetailHoliday_, 0, ddlyear_manage_.SelectedValue.ToString());
                ////}

                break;
            case "ddlorg":
                DropDownList ddlorg_search = (DropDownList)fvManageHoliday.FindControl("ddlorg");
                DropDownList ddlyear_manage_search = (DropDownList)fvManageHoliday.FindControl("ddlyear_manage");
                //GridView GvCreateDetailHoliday_search = (GridView)fvManageHoliday.FindControl("GvCreateDetailHoliday");

                //_urlGetDetailHolidayCreateOrg
                if(int.Parse(ddlorg_search.SelectedValue.ToString()) != 0 && ddlyear_manage_search.SelectedValue.ToString() != "0")
                {
                    //litDebug.Text = "1";
                    getSearchDetailHolidayCreateOrg(GvCreateDetailHoliday, int.Parse(ddlorg_search.SelectedValue.ToString()), ddlyear_manage_search.SelectedValue.ToString());
                }
                else
                {
                    Panel_DetailHolidaySearchCreateOrg.Visible = false;
                    //litDebug.Text = "2";
                    ViewState["vs_u0_manage_idx_createorg"] = 0;
                    ViewState["vs_DetailHolidaySearchCreateOrg"] = null;
                    setGridData(GvCreateDetailHoliday, ViewState["vs_DetailHolidaySearchCreateOrg"]);
                }


                break;

        }
    }

    #endregion selected Changed

    #region gridview
    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView gvName = (GridView)sender;
        gvName.PageIndex = e.NewPageIndex;

        switch (gvName.ID)
        {

            case "GvDetailHoliday":

                setGridData(GvDetailHoliday, ViewState["vs_DetailHoliday"]);

                //setGridData(GvDetail, ViewState["Va_DetailCarbooking"]);
                //setOntop.Focus();

                break;
            case "GvViewDetailHolidayOrg":
                setGridData(gvName, ViewState["vs_ViewDetailHolidayOrg"]);

                break;


        }
    }

    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvDetailHoliday":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {

                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                    //btn_addplace.Visible = true;
                    //setFormData(Fv_Insert_Result, FormViewMode.ReadOnly, null);

                    //MultiView1.Visible = true;
                    //Gv_select_unit.Visible = false;
                    //btn_AddTopicMA.Visible = true;
                    //FvInsert.Visible = false;

                }

                break;


        }
    }

    protected void gvRowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvDetailHoliday":
                GvDetailHoliday.EditIndex = e.NewEditIndex;
                setGridData(GvDetailHoliday, ViewState["vs_DetailHoliday"]);
                //getDetailHoliday(0,"0");
                break;

        }
    }

    protected void gvRowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        //Label5.Text = GvName.ID;
        switch (GvName.ID)
        {
            case "GvDetailHoliday":
                GvDetailHoliday.EditIndex = -1;
                setGridData(GvDetailHoliday, ViewState["vs_DetailHoliday"]);
                //getDetailHoliday(0,"0");

                break;

        }
    }

    protected void gvRowUpdating(object sender, GridViewUpdateEventArgs e)
    {

        var GvName = (GridView)sender;
        //Label5.Text = GvName.ID;
        switch (GvName.ID)
        {
            case "GvDetailHoliday":

                //DropDownList ddlyear 
                DropDownList ddlyear_edit = (DropDownList)updatePanelInsertPer.FindControl("ddlyear");

                var txt_holiday_idx_edit = (TextBox)GvDetailHoliday.Rows[e.RowIndex].FindControl("txt_holiday_idx_edit");
                var txt_name_holidat_edit = (TextBox)GvDetailHoliday.Rows[e.RowIndex].FindControl("txt_name_holidat");
                var txt_date_holiday_edit = (TextBox)GvDetailHoliday.Rows[e.RowIndex].FindControl("txt_date_holiday");


                GvDetailHoliday.EditIndex = -1;


                data_holiday data_holiday_detail_edit = new data_holiday();
                emps_u0_holiday_detail u0_holiday_detail_edit = new emps_u0_holiday_detail();
                data_holiday_detail_edit.emps_u0_holiday_list = new emps_u0_holiday_detail[1];

                u0_holiday_detail_edit.holiday_idx = int.Parse(txt_holiday_idx_edit.Text);
                u0_holiday_detail_edit.holiday_name = txt_name_holidat_edit.Text;
                u0_holiday_detail_edit.holiday_date = txt_date_holiday_edit.Text;
                u0_holiday_detail_edit.cemp_idx = _emp_idx;

                data_holiday_detail_edit.emps_u0_holiday_list[0] = u0_holiday_detail_edit;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_holiday_detail_edit));
                data_holiday_detail_edit = callServicePostHoliday(_urlSetempsHoliday, data_holiday_detail_edit);


                //litDebug.Text = ddlyear.SelectedValue.ToString();

                if (data_holiday_detail_edit.return_code == 2)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ข้อมูลซ้ำ กรุณาแก้ไขใหม่อีกครั้ง!!!');", true);

                    //litDebug.Text = ddlyear_edit.SelectedValue.ToString();
                    //litDebug1.Text = _condition_year_current.ToString();

                    //setGridData(GvDetailHoliday, ViewState["vs_DetailHoliday"]);
                    if (int.Parse(ddlyear_edit.SelectedValue.ToString()) != int.Parse(_condition_year_current.ToString()))
                    {
                        //litDebug.Text = "9";
                        getDetailHoliday(GvDetailHoliday, 1, ddlyear_edit.SelectedValue.ToString());
                    }
                    else
                    {
                        //litDebug.Text = "10";
                        getDetailHoliday(GvDetailHoliday, 0, _condition_year_current.ToString());
                    }

                }
                else
                {

                    //litDebug.Text = ddlyear_edit.SelectedValue.ToString();
                    //litDebug1.Text = _condition_year_current.ToString();
                    if (int.Parse(ddlyear_edit.SelectedValue.ToString()) != int.Parse(_condition_year_current.ToString()))
                    {
                        //litDebug.Text = "11";
                        getDetailHoliday(GvDetailHoliday, 1, ddlyear_edit.SelectedValue.ToString());
                    }
                    else
                    {
                        //litDebug.Text = "12";
                        getDetailHoliday(GvDetailHoliday, 0, _condition_year_current.ToString());
                    }


                    //setGridData(GvDetailHoliday, ViewState["vs_DetailHoliday"]);
                    //getDetailHoliday(0,"0");
                }
                break;

        }
    }

    protected void onRowCommand(Object sender, GridViewCommandEventArgs e)
    {
        if (sender is GridView)
        {
            string cmdName = e.CommandName;

            switch (cmdName)
            {
                case "cmdRemoveCar":
                    //GridView gvAddCarBooking = (GridView)FvHrApprove.FindControl("gvAddCarBooking");
                    //GridViewRow rowSelect = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    //int rowIndex = rowSelect.RowIndex;
                    //DataSet dsContacts = (DataSet)ViewState["vsCarDetailList"];
                    //dsContacts.Tables["dsCarDetailTable"].Rows[rowIndex].Delete();
                    //dsContacts.AcceptChanges();
                    //setGridData(gvAddCarBooking, dsContacts.Tables["dsCarDetailTable"]);
                    //if (dsContacts.Tables["dsCarDetailTable"].Rows.Count < 1)
                    //{
                    //    gvAddCarBooking.Visible = false;
                    //}
                    break;


            }
        }
    }

    #endregion gridview

    #region Directories_File URL
    public void SearchDirectories(string _dir, String target)
    {

        string dirfiles = _dir.ToString();

        DataSet ds1 = new DataSet();
        DataTable dt1 = new DataTable();
        DataRow dr1 = dt1.NewRow();
        DataColumn dc1 = new DataColumn("FileName", typeof(string));
        dc1 = new DataColumn("Download", typeof(string));
        dt1.Columns.Add("FileName");
        dt1.Columns.Add("Download");

        //litDebug.Text = target.ToString();//files.Length.ToString();
        int i = 0;

        //litDebug.Text = Server.MapPath(_dir);
        if (Directory.Exists(Server.MapPath(_dir)))
        {
            string[] filesPath_view = Directory.GetFiles(Server.MapPath(_dir));
            List<ListItem> files = new List<ListItem>();
            foreach (string path in filesPath_view)
            {
                string getfiles = "";
                getfiles = Path.GetFileName(path);

                i++;

            }
        }
        else
        {

        }

    }

    public static bool UrlExists(string url)
    {
        try
        {
            var request = WebRequest.Create(url) as HttpWebRequest;
            if (request == null) return false;
            request.Method = "HEAD";
            using (var response = (HttpWebResponse)request.GetResponse())
            {
                return response.StatusCode == HttpStatusCode.OK;
            }
        }
        catch (UriFormatException)
        {
            //Invalid Url
            return false;
        }
        catch (WebException)
        {
            //Unable to access url
            return false;
        }
    }

    #endregion

    #region radiobutton
    protected void onSelectedIndexChanged(object sender, EventArgs e)
    {
        var rdName = (RadioButtonList)sender;
        //var chkName = (CheckBoxList)sender;

        switch (rdName.ID)
        {
            case "rdoStatusUse":
                //litDebug.Text = "666";

                //litDebug.Text = chk_selectdetail.ToString();
                //setUseCar_chk();
                break;

        }
    }
    #endregion radiobutton

    #region Formview Databind
    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        if (sender is FormView)
        {
            FormView FvName = (FormView)sender;

            switch (FvName.ID)
            {
                case "FvWaitApproveDetail":
                    if (FvName.CurrentMode == FormViewMode.ReadOnly)
                    {

                    }

                    if (FvName.CurrentMode == FormViewMode.Edit)
                    {


                    }
                    break;



            }
        }
    }
    #endregion Formview Databind

    #region reuse

    protected void initPageLoad()
    {

    }

    protected void initPage()
    {
        setActiveTab("docDetailHoliday", 0, 0, 0, 0, 0, 0);
    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    protected void setDir(string dirName)
    {
        if (!Directory.Exists(Server.MapPath(dirName)))
        {
            Directory.CreateDirectory(Server.MapPath(dirName));
        }
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void setRepeaterData(Repeater rptName, Object obj)
    {
        rptName.DataSource = obj;
        rptName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_carbooking callServicePostCarBooking(string _cmdUrl, data_carbooking _data_carbooking)
    {
        _localJson = _funcTool.convertObjectToJson(_data_carbooking);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_carbooking = (data_carbooking)_funcTool.convertJsonToObject(typeof(data_carbooking), _localJson);


        return _data_carbooking;
    }

    protected data_roombooking callServicePostRoomBooking(string _cmdUrl, data_roombooking _data_roombooking)
    {
        _localJson = _funcTool.convertObjectToJson(_data_roombooking);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_roombooking = (data_roombooking)_funcTool.convertJsonToObject(typeof(data_roombooking), _localJson);


        return _data_roombooking;
    }

    protected data_holiday callServicePostHoliday(string _cmdUrl, data_holiday _data_holiday)
    {
        _localJson = _funcTool.convertObjectToJson(_data_holiday);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_holiday = (data_holiday)_funcTool.convertJsonToObject(typeof(data_holiday), _localJson);


        return _data_holiday;
    }

    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }

    protected void setActiveView(string activeTab, int uidx, int nodeidx, int staidx, int actor_idx, int _chk_tab, int _orgidx)
    {
        mvSystem.SetActiveView((View)mvSystem.FindControl(activeTab));

        //set tab create
        setFormData(fvEmpDetail, FormViewMode.ReadOnly, null);
        setFormData(fvCreateHoliday, FormViewMode.ReadOnly, null);

        setGridData(GvDetailHoliday, null);
        //set tab manage
        setFormData(fvEmpDetailManage, FormViewMode.ReadOnly, null);
        setFormData(fvManageHoliday, FormViewMode.ReadOnly, null);

        GvDetailHolidayOrg.Visible = false;
        setGridData(GvDetailHolidayOrg, null);
        Div_BackToDetail.Visible = false;

        GvViewDetailHolidayOrg.Visible = false;
        setGridData(GvViewDetailHolidayOrg, null);
        Panel_ViewDetailHolidayOrg.Visible = false;
        Update_DetailHolidayOrgName.Visible = false;

        Panel_DetailHolidaySearchCreateOrg.Visible = false;

        switch (activeTab)
        {
            case "docDetailHoliday":

                switch (_chk_tab)
                {
                    case 0:
                        ////setFormData(FvDetailUseCarSearch, FormViewMode.Insert, null);
                        ////DropDownList ddlCarUseNameSearch = (DropDownList)FvDetailUseCarSearch.FindControl("ddlCarUseNameSearch");
                        ////DropDownList ddlDetailtypecarSearch = (DropDownList)FvDetailUseCarSearch.FindControl("ddlDetailtypecarSearch");

                        ////getDetailCarUse(ddlCarUseNameSearch, 0);

                        //////getDetailTypeCar(ddlDetailtypecarSearch, 0);

                        //////getShowDetailRoomSlider();

                        break;

                }
                setOntop.Focus();
                break;

            case "docDetail":

                switch (_chk_tab)
                {
                    case 0:

                        getddlYearHoliday(ddlyear, _condition_year_current);

                        if (int.Parse(ddlyear.SelectedValue) != int.Parse(_condition_year_current.ToString()))
                        {
                            getDetailHoliday(GvDetailHoliday, 1, ddlyear.SelectedValue.ToString());
                        }
                        else
                        {
                            getDetailHoliday(GvDetailHoliday, 0, _condition_year_current.ToString());
                        }

                        break;
                }
                setOntop.Focus();
                break;

            case "docCreate":

                setFormData(fvEmpDetail, FormViewMode.ReadOnly, ((data_employee)ViewState["vsEmpProfile"]).employee_list);
                setFormData(fvCreateHoliday, FormViewMode.Insert, null);
                //Update_PanelSearchReport.Visible = true;
                
                break;
            case "docManageHoliday":

                setFormData(fvEmpDetailManage, FormViewMode.ReadOnly, ((data_employee)ViewState["vsEmpProfile"]).employee_list);
                setFormData(fvManageHoliday, FormViewMode.Insert, null);

                DropDownList ddlyear_manage = (DropDownList)fvManageHoliday.FindControl("ddlyear_manage");
                DropDownList ddlorg = (DropDownList)fvManageHoliday.FindControl("ddlorg");
                GridView GvCreateDetailOrg = (GridView)fvManageHoliday.FindControl("GvCreateDetailOrg");
                //GridView GvCreateDetailHoliday = (GridView)fvManageHoliday.FindControl("GvCreateDetailHoliday");

                getddlYearHoliday(ddlyear_manage, _condition_year_current);
                getOrganizationList(ddlorg);
                ////getDetailOrganization(GvCreateDetailOrg);


                ////if (int.Parse(ddlyear_manage.SelectedValue) != int.Parse(_condition_year_current.ToString()))
                ////{
                ////    getDetailHoliday(GvCreateDetailHoliday, 1, ddlyear_manage.SelectedValue.ToString());
                ////}
                ////else
                ////{
                ////    getDetailHoliday(GvCreateDetailHoliday, 0, _condition_year_current.ToString());
                ////}

                //Update_PanelSearchReport.Visible = true;
                setOntop.Focus();

                break;
            case "docDetailOrg":
                switch (_chk_tab)
                {
                    case 0:
                        getDetailHolidayOrg(GvDetailHolidayOrg, 0, "0");
                        
                        break;
                    case 1: //View detail

                        Div_BackToDetail.Visible = true;
                        Panel_ViewDetailHolidayOrg.Visible = true;
                        Update_DetailHolidayOrgName.Visible = true;
                        getddlYearHoliday(ddlYearViewDetailHolidayOrg, _condition_year_current);

                        lbl_ViewDetailHolidayOrg_u0idx.Text = uidx.ToString();
                        lbl_ViewDetailHolidayOrg_orgidx.Text = _orgidx.ToString();

                        if (int.Parse(ddlYearViewDetailHolidayOrg.SelectedValue) != int.Parse(_condition_year_current.ToString()))
                        {
                            //getDetailHoliday(GvDetailHoliday, 1, ddlyear.SelectedValue.ToString());
                            getViewDetailHolidayOrg(GvViewDetailHolidayOrg, uidx, ddlYearViewDetailHolidayOrg.SelectedValue.ToString(), 1);
                        }
                        else
                        {
                            //getDetailHoliday(GvDetailHoliday, 0, _condition_year_current.ToString());
                            getViewDetailHolidayOrg(GvViewDetailHolidayOrg, uidx, ddlYearViewDetailHolidayOrg.SelectedValue.ToString(), 0);
                        }


                        
                        getViewDetailOrgName(_orgidx);
                        break;
                }
                setOntop.Focus();

                // getDetailHoliday(GvDetailHolidayOrg, 1, ddlyear.SelectedValue.ToString());
                break;

        }
    }


    protected void setActiveTab(string activeTab, int uidx, int nodeidx, int staidx, int actor_idx, int _chk_tab, int _car_use_idx)
    {
        setActiveView(activeTab, uidx, nodeidx, staidx, actor_idx, _chk_tab, _car_use_idx);
        switch (activeTab)
        {
            case "docDetailHoliday":
                li0.Attributes.Add("class", "active");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");

                break;

            case "docDetail":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "active");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");
                break;

            case "docCreate":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "active");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");
                break;
            case "docManageHoliday":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "active");
                li4.Attributes.Add("class", "");
                break;
            case "docDetailOrg":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "active");
                break;


        }
    }

    protected string getOnlyDate(string _dateIn)
    {
        return _funcTool.getOnlyDate(_dateIn);
    }

    protected string getStatus(int status)
    {
        if (status == 0)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Certificate'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='No Certificate'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        // Confirms that an HtmlForm control is rendered for the
        //specified ASP.NET server control at run time.
    }

    protected void rptOnRowDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        var rptName = (Repeater)Sender;
        switch (rptName.ID)
        {
            case "rp_place":

                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {

                    //var chk_coler = (Label)e.Item.FindControl("lbcheck_coler");
                    //var btnPlaceLab = (LinkButton)e.Item.FindControl("btnPlaceLab");

                    //for (int k = 0; k <= rp_place.Items.Count; k++)
                    //{
                    //    btnPlaceLab.CssClass = ConfigureColors(k);
                    //    //Console.WriteLine(i);
                    //}


                }

                break;
        }
    }

    #endregion reuse

    #region data excel
    protected void WriteExcelWithNPOI(DataTable dt, String extension, String fileName)
    {
        IWorkbook workbook;
        if (extension == "xlsx")
        {
            workbook = new XSSFWorkbook();
        }
        else if (extension == "xls")
        {
            workbook = new HSSFWorkbook();
        }
        else
        {
            throw new Exception("This format is not supported");
        }
        ISheet sheet1 = workbook.CreateSheet("Sheet 1");
        IRow row1 = sheet1.CreateRow(0);
        for (int j = 0; j < dt.Columns.Count; j++)
        {
            ICell cell = row1.CreateCell(j);
            String columnName = dt.Columns[j].ToString();
            cell.SetCellValue(columnName);
        }

        //set merge cell
        int max_row = dt.Rows.Count;


        for (int i = 0; i < dt.Rows.Count; i++)
        {
            IRow row = sheet1.CreateRow(i + 1);

            //set merge cell
            var cra = new NPOI.SS.Util.CellRangeAddress(1, max_row, 0, 0);
            sheet1.AddMergedRegion(cra);

            //var cra1 = new NPOI.SS.Util.CellRangeAddress(1, max_row, 1, 1);
            //sheet1.AddMergedRegion(cra1);


            for (int j = 0; j < dt.Columns.Count; j++)
            {
                ICell cell = row.CreateCell(j);
                String columnName = dt.Columns[j].ToString();
                cell.SetCellValue(dt.Rows[i][columnName].ToString());
            }
        }
        using (var exportData = new MemoryStream())
        {
            Response.Clear();
            workbook.Write(exportData);
            if (extension == "xlsx")
            {
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xlsx"));
                Response.BinaryWrite(exportData.ToArray());
            }
            else if (extension == "xls")
            {
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xls"));
                Response.BinaryWrite(exportData.GetBuffer());
            }
            Response.End();
        }
    }

    //protected void WriteExcelWithNPOI(DataTable dt, String extension, String fileName, int _set_value)
    //{
    //    IWorkbook workbook;


    //    if (extension == "xlsx")
    //    {
    //        workbook = new XSSFWorkbook();



    //    }
    //    else if (extension == "xls")
    //    {
    //        workbook = new HSSFWorkbook();


    //    }
    //    else
    //    {
    //        throw new Exception("This format is not supported");
    //    }

    //    ISheet sheet1 = workbook.CreateSheet("Sheet 1");
    //    IRow row1 = sheet1.CreateRow(0);

    //    ICellStyle testeStyle = workbook.CreateCellStyle();
    //    testeStyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.Medium;
    //    testeStyle.FillForegroundColor = IndexedColors.BrightGreen.Index;
    //    testeStyle.FillPattern = FillPattern.SolidForeground;

    //    //Create a Title row
    //    //var titleFont = workbook.CreateFont();
    //    //titleFont.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
    //    //titleFont.FontHeightInPoints = 11;
    //    //titleFont.Underline = NPOI.SS.UserModel.FontUnderlineType.Single;

    //    //var titleStyle = workbook.CreateCellStyle();
    //    //titleStyle.SetFont(titleFont);

    //    //HSSFCellStyle Style = workbook.CreateCellStyle() as HSSFCellStyle;

    //    for (int j = 0; j < dt.Columns.Count; j++)
    //    {
    //        ICell cell = row1.CreateCell(j);
    //        String columnName = dt.Columns[j].ToString();
    //        cell.SetCellValue(columnName);
    //    }

    //    //set merge cell
    //    int max_row = dt.Rows.Count - 1;

    //    for (int i = 0; i < dt.Rows.Count; i++)
    //    {


    //        IRow row = sheet1.CreateRow(i + 1);

    //        //set merge cell
    //        var cra = new NPOI.SS.Util.CellRangeAddress(1, max_row, 0, 0);
    //        sheet1.AddMergedRegion(cra);

    //        var cra1 = new NPOI.SS.Util.CellRangeAddress(1, max_row, 1, 1);
    //        sheet1.AddMergedRegion(cra1);

    //        for (int j = 0; j < dt.Columns.Count; j++)
    //        {


    //            ICell cell = row.CreateCell(j);
    //            String columnName = dt.Columns[j].ToString();
    //            cell.SetCellValue(dt.Rows[i][columnName].ToString());

    //            //Style.Style.Alignment = NPOI.SS.UserModel.HorizontalAlignment.Center;
    //            //Style.VerticalAlignment = VerticalAlignment.Center;
    //            //Style.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
    //            //set font
    //            //cell.CellStyle = titleStyle;

    //            //set export color total expenses green
    //            if (_set_value == 1)
    //            {
    //                if ((i + 1) == dt.Rows.Count && (dt.Rows[i][columnName].ToString() == "Total" || ((i + 1) == dt.Rows.Count && (j + 1) == dt.Columns.Count)))
    //                {
    //                    cell.CellStyle = testeStyle;
    //                    //cell.CellStyle.WrapText = true;
    //                }


    //            }

    //        }

    //    }

    //    using (var exportData = new MemoryStream())
    //    {
    //        Response.Clear();
    //        workbook.Write(exportData);

    //        if (extension == "xlsx")
    //        {
    //            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    //            Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xlsx"));

    //            Response.BinaryWrite(exportData.ToArray());
    //        }
    //        else if (extension == "xls")
    //        {
    //            Response.ContentType = "application/vnd.ms-excel";
    //            Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xls"));
    //            Response.BinaryWrite(exportData.GetBuffer());
    //        }
    //        Response.End();
    //    }


    //}
    #endregion data excel

}