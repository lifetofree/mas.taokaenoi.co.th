﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_qa_cims_m0_equipment_resolution : System.Web.UI.Page
{
    #region Connect
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();

    data_qa_cims _data_qa_cims = new data_qa_cims();


    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    
    //master
    static string _urlCimsGetUnit = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetUnit"];
    static string _urlSetM0_Resolution = _serviceUrl + ConfigurationManager.AppSettings["urlSetM0_Resolution"];
    static string _urlCimsGetM0_Resolution = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetM0_Resolution"];
    static string _urlCimsDelM0_Resolution = _serviceUrl + ConfigurationManager.AppSettings["urlCimsDelM0_Resolution"];

    ////static string _urlCimsSetEquipment_result = _serviceUrl + ConfigurationManager.AppSettings["urlCimsSetEquipment_result"];
    ////static string _urlCimsGetEquipment_result = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetEquipment_result"];
    ////static string _urlCimsDeleteEquipment_result = _serviceUrl + ConfigurationManager.AppSettings["urlCimsDeleteEquipment_result"];

    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    //-- employee --//

    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    bool _flag_qmr = false;
    #endregion Connect

    #region Page_Init
    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        ////getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
        //check permission
        //foreach (int item in rdept_qmr)
        //{
        //    if (_dataEmployee.employee_list[0].rdept_idx == item)
        //    {
        //        _flag_qmr = true;
        //        break;
        //    }
        //}
    }
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Select_Resolution();
            //ViewState["empIDX"] = Session["emp_idx"];
            ////Select_Equipment_result();


        }
    }
    #endregion Page Load

    protected void getM0Unit(DropDownList ddlName, string _unit_idx)
    {
        data_qa_cims _dataqacims = new data_qa_cims();
        _dataqacims.qa_cims_m0_unit_list = new qa_cims_m0_unit_detail[1];
        qa_cims_m0_unit_detail _m0Set = new qa_cims_m0_unit_detail();
        _dataqacims.qa_cims_m0_unit_list[0] = _m0Set;

        _dataqacims = callServicePostMasterQACIMS(_urlCimsGetUnit, _dataqacims);

        setDdlData(ddlName, _dataqacims.qa_cims_m0_unit_list, "unit_symbol_en", "unit_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือกหน่วยวัด --", "0"));
        
        ddlName.Items.Insert(1, new ListItem("µl", "29"));
        ddlName.Items.Insert(2, new ListItem("µm", "30"));
        ddlName.Items.Insert(3, new ListItem("°C", "31"));


        ddlName.SelectedValue = _unit_idx;
    }

    protected void Select_Resolution()
    {
        data_qa_cims data_m0_resulution = new data_qa_cims();
        qa_cims_m0_resulution_detail m0_resulution = new qa_cims_m0_resulution_detail();

        data_m0_resulution.qa_cims_m0_resolution_list = new qa_cims_m0_resulution_detail[1];

        data_m0_resulution.qa_cims_m0_resolution_list[0] = m0_resulution;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));
        data_m0_resulution = callServicePostMasterQACIMS(_urlCimsGetM0_Resolution, data_m0_resulution);

        setGridData(GvResolution, data_m0_resulution.qa_cims_m0_resolution_list);

    }

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        switch (cmdName)
        {

            case "cmdInsertResolution":

                MvMaster.SetActiveView(ViewInsert);
                getM0Unit((DropDownList)ViewInsert.FindControl("ddl_unit_idx"), "0");
               
                break;

            case "cmdSaveResolution":
                //litdebug.Text = _emp_idx.ToString();

                data_qa_cims data_resulution = new data_qa_cims();
                qa_cims_m0_resulution_detail m0_resolution_detail = new qa_cims_m0_resulution_detail();
                data_resulution.qa_cims_m0_resolution_list = new qa_cims_m0_resulution_detail[1];

                m0_resolution_detail.resolution_name = txt_resolution_name.Text;
                m0_resolution_detail.cemp_idx = _emp_idx;
                m0_resolution_detail.resolution_status = int.Parse(ddl_resolution_status.SelectedValue);
                m0_resolution_detail.unit_idx = int.Parse(ddl_unit_idx.SelectedValue);

                data_resulution.qa_cims_m0_resolution_list[0] = m0_resolution_detail;
                //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        

                data_resulution = callServicePostMasterQACIMS(_urlSetM0_Resolution, data_resulution);

                //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_resulution));
                if (data_resulution.return_code == 1)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);

                }
                else
                {
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                    //tex_equipment_result.Text = String.Empty;
                }



                break;

            case "cmdDelResolution":

                int resolution_idx_del= int.Parse(cmdArg);

                data_qa_cims data_resulution_del = new data_qa_cims();
                qa_cims_m0_resulution_detail m0_resolution_detail_del = new qa_cims_m0_resulution_detail();
                data_resulution_del.qa_cims_m0_resolution_list = new qa_cims_m0_resulution_detail[1];

                m0_resolution_detail_del.equipment_resolution_idx = resolution_idx_del;
               
                data_resulution_del.qa_cims_m0_resolution_list[0] = m0_resolution_detail_del;
                //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        

                data_resulution_del = callServicePostMasterQACIMS(_urlCimsDelM0_Resolution, data_resulution_del);

                Select_Resolution();

                break;

            case "cmdCancel":

                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;
            
              

               
        }
    }
    #endregion btnCommand

    #region bind data
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected string getStatus(int status)
    {
        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }
    #endregion bind data

    #region Gridview
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvResolution":
               

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                   

                    Label lbl_resolution_status = (Label)e.Row.FindControl("lbl_resolution_status");
                    Label lbl_resolution_statusOnline = (Label)e.Row.FindControl("lbl_resolution_statusOnline");
                    Label lbl_resolution_statusOffline = (Label)e.Row.FindControl("lbl_resolution_statusOffline");

                    ViewState["vs_status_resolution"] = lbl_resolution_status.Text;


                    if (ViewState["vs_status_resolution"].ToString() == "1")
                    {
                        lbl_resolution_statusOnline.Visible = true;
                    }
                    else if (ViewState["vs_status_resolution"].ToString() == "0")
                    {
                        lbl_resolution_statusOffline.Visible = true;
                    }
                    else
                    {

                    }



                    var _unit_idx_Resolution = (Label)e.Row.FindControl("lbequipment_unit_idx");
                    var _unit_name_Resolution = (Label)e.Row.FindControl("lbl_unit_symbol_en");

                    switch (_unit_idx_Resolution.Text)
                    {
                        case "29":
                            _unit_name_Resolution.Text = "µl";
                            break;

                        case "30":
                            _unit_name_Resolution.Text = "µm";
                            break;

                        case "31":
                            _unit_name_Resolution.Text = "°C";
                            break;
                    }


                }

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";


                    Label _lbl_unit_idx_edit = (Label)e.Row.FindControl("lbl_unit_idx_edit");
                    //getM0CaltypeList((DropDownList)e.Row.Cells[0].FindControl("DD_caltype"), txt_caltype.Text);

                    getM0Unit((DropDownList)e.Row.FindControl("ddlunit_idx_edit"), _lbl_unit_idx_edit.Text);


                    //btn_addplace.Visible = true;
                    //setFormData(Fv_Insert_Result, FormViewMode.ReadOnly, null);

                    //MultiView1.Visible = true;
                    //Gv_select_unit.Visible = false;
                    //btn_addequipment_result.Visible = true;
                    //Fv_Insert_Result.Visible = false;

                }

                break;
        }
    }

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvResolution":
                GvResolution.EditIndex = e.NewEditIndex;
                Select_Resolution();

                break;
        }
    }

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvResolution":

                var txt_equipment_resolution_idx_edit = (TextBox)GvResolution.Rows[e.RowIndex].FindControl("txt_equipment_resolution_idx_edit");
                var txt_resolution_name_edit = (TextBox)GvResolution.Rows[e.RowIndex].FindControl("txt_resolution_name_edit");
                var ddlunit_idx_edit = (DropDownList)GvResolution.Rows[e.RowIndex].FindControl("ddlunit_idx_edit");
                var ddl_resolution_status_edit = (DropDownList)GvResolution.Rows[e.RowIndex].FindControl("ddl_resolution_status_edit");


                GvResolution.EditIndex = -1;

                data_qa_cims data_resolution_edit = new data_qa_cims();
                data_resolution_edit.qa_cims_m0_resolution_list = new qa_cims_m0_resulution_detail[1];
                qa_cims_m0_resulution_detail m0_resolution_edit = new qa_cims_m0_resulution_detail();

                m0_resolution_edit.equipment_resolution_idx = int.Parse(txt_equipment_resolution_idx_edit.Text);
                m0_resolution_edit.resolution_name = txt_resolution_name_edit.Text;
                m0_resolution_edit.unit_idx = int.Parse(ddlunit_idx_edit.SelectedValue);
                m0_resolution_edit.resolution_status = int.Parse(ddl_resolution_status_edit.SelectedValue);

                data_resolution_edit.qa_cims_m0_resolution_list[0] = m0_resolution_edit;

                //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_resolution_edit));

                data_resolution_edit = callServicePostMasterQACIMS(_urlSetM0_Resolution, data_resolution_edit);

                if (data_resolution_edit.return_code == 1)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ข้อมูลซ้ำ กรุณาแก้ไขใหม่อีกครั้ง!!!');", true);
                    Select_Resolution();
                }
                else
                {
                    Select_Resolution();
                }
                break;
        }
    }

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvResolution":
                GvResolution.EditIndex = -1;
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
        }
    }

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvResolution":
                GvResolution.PageIndex = e.NewPageIndex;
                GvResolution.DataBind();
                Select_Resolution();
                break;
        }
    }

    #endregion Gridview

    #region event dropdownlist

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }
    #endregion dropdownlist

    #region callService 
    protected data_qa_cims callServicePostMasterQACIMS(string _cmdUrl, data_qa_cims _data_qa_cims)
    {
        _localJson = _funcTool.convertObjectToJson(_data_qa_cims);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_qa_cims = (data_qa_cims)_funcTool.convertJsonToObject(typeof(data_qa_cims), _localJson);


        return _data_qa_cims;
    }
    #endregion callService
}