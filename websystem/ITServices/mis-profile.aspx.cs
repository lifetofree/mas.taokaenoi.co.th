﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Data.SqlClient;


using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Text.RegularExpressions;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using NPOI.HSSF.UserModel;

public partial class websystem_ITServices_mis_profile : System.Web.UI.Page
{

    #region Connect

    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();
    data_employee _dtEmployee = new data_employee();
    data_adonline dataADOnline = new data_adonline();
    data_softwarelicense_devices _dtswl = new data_softwarelicense_devices();
    string _localJson = "";

    #region URL

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    static string _urlGetProfileMIS = _serviceUrl + ConfigurationManager.AppSettings["urlGetProfileMIS"];
    static string _urlGetCostcenterOld = _serviceUrl + ConfigurationManager.AppSettings["urlGetCostcenterOld"];
    static string _urlGetPositionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetPositionList"];
    static string _urlReadPermPol = _serviceUrl + ConfigurationManager.AppSettings["urlADReadPermPol"];
    static string _urlGetWHEREEmpIDX = _serviceUrl + ConfigurationManager.AppSettings["urlGetWHEREEmpIDX"];
    static string _urlGetWHEREADOnline = _serviceUrl + ConfigurationManager.AppSettings["urlGetWHEREADOnline"];
    static string _urlGetWHEREHolder = _serviceUrl + ConfigurationManager.AppSettings["urlGetWHEREHolder"];
    static string _urlGetDetailSoftwareProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailSoftwareProfile"];
    static string _urlSelectMainTel = _serviceUrl + ConfigurationManager.AppSettings["urlSelectMainTel"];
    static string _urlSelectLocate_Tel = _serviceUrl + ConfigurationManager.AppSettings["urlSelectLocate_Tel"];
    static string _urlGetPhoneWHERERsecIDX = _serviceUrl + ConfigurationManager.AppSettings["urlGetPhoneWHERERsecIDX"];
    static string _urlInsertPhoneWHEREEmpIDX = _serviceUrl + ConfigurationManager.AppSettings["urlInsertPhoneWHEREEmpIDX"];
    static string _urlUpdatePhoneWHEREEmpIDX = _serviceUrl + ConfigurationManager.AppSettings["urlUpdatePhoneWHEREEmpIDX"];
    static string _urlExportMISProfile = _serviceUrl + ConfigurationManager.AppSettings["urlExportMISProfile"];




    #endregion

    #endregion

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ViewState["EmpIDX"] = int.Parse(Session["emp_idx"].ToString());
            IndexList.Attributes.Add("class", "active");
            select_empIdx_present();
            getOrganizationList(ddlorgidx);
            SelectMasterList();
            SelectCostCenterList(ddlcostcenter);
            getSetPermPolToDDL(ddlad);

        }
    }
    #endregion

    #region Select

    protected void select_empIdx_present()
    {

        _dtEmployee = callServiceEmpProfile(urlGetMyProfile + ViewState["EmpIDX"].ToString());


        ViewState["rdept_name"] = _dtEmployee.employee_list[0].dept_name_th;
        ViewState["rdept_idx"] = _dtEmployee.employee_list[0].rdept_idx;
        ViewState["FullName"] = _dtEmployee.employee_list[0].emp_name_th;
        ViewState["Org_name"] = _dtEmployee.employee_list[0].org_name_th;
        ViewState["Org_idx"] = _dtEmployee.employee_list[0].org_idx;

        ViewState["EmpCode"] = _dtEmployee.employee_list[0].emp_code;
        ViewState["Positname"] = _dtEmployee.employee_list[0].pos_name_th;
        ViewState["Pos_idx"] = _dtEmployee.employee_list[0].rpos_idx;
        ViewState["Email"] = _dtEmployee.employee_list[0].emp_email;
        ViewState["Tel"] = _dtEmployee.employee_list[0].emp_mobile_no;
        ViewState["Secname"] = _dtEmployee.employee_list[0].sec_name_th;
        ViewState["Sec_idx"] = _dtEmployee.employee_list[0].rsec_idx;
        ViewState["CostIDX"] = _dtEmployee.employee_list[0].costcenter_idx;

    }

    protected void getOrganizationList(DropDownList ddlName)
    {
        _dtEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dtEmployee.organization_list[0] = _orgList;

        _dtEmployee = callServicePostEmp(_urlGetOrganizationList, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกองค์กร....", "0"));
    }

    protected void getDepartmentList(DropDownList ddlName, int _org_idx)
    {
        _dtEmployee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dtEmployee.department_list[0] = _deptList;

        _dtEmployee = callServicePostEmp(_urlGetDepartmentList, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกฝ่าย....", "0"));
    }

    protected void getSectionList(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {
        _dtEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _dtEmployee.section_list[0] = _secList;

        _dtEmployee = callServicePostEmp(_urlGetSectionList, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.section_list, "sec_name_th", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกแผนก....", "0"));
    }

    protected void getPositionList(DropDownList ddlName, int _org_idx, int _rdept_idx, int _rsec_idx)
    {
        _dtEmployee.position_list = new position_details[1];
        position_details _positionList = new position_details();
        _positionList.org_idx = _org_idx;
        _positionList.rdept_idx = _rdept_idx;
        _positionList.rsec_idx = _rsec_idx;
        _dtEmployee.position_list[0] = _positionList;
        //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtEmployee));
        _dtEmployee = callServicePostEmp(_urlGetPositionList, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.position_list, "pos_name_th", "rpos_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกตำแหน่ง....", "0"));
    }

    protected void SelectMasterList()
    {
        _dtEmployee = new data_employee();
        _dtEmployee.employee_list = new employee_detail[1];
        employee_detail _dtemp = new employee_detail();

        _dtemp.org_idx = int.Parse(ddlorgidx.SelectedValue);
        _dtemp.rdept_idx = int.Parse(ddlrdeptidx.SelectedValue);
        _dtemp.rsec_idx = int.Parse(ddlrsecidx.SelectedValue);
        _dtemp.rpos_idx = int.Parse(ddlposition.SelectedValue);
        _dtemp.costcenter_idx = int.Parse(ddlcostcenter.SelectedValue);
        _dtemp.perm_pol = int.Parse(ddlad.SelectedValue);
        _dtemp.emp_firstname_th = txtempidx.Text;
        if (ddlstatus.SelectedValue == "1" || ddlstatus.SelectedValue == "0")
        {
            _dtemp.empstatus = ddlstatus.SelectedValue;
        }
        else
        {
            _dtemp.empstatus = "1,0";
        }


        _dtemp.emp_email = txtemail.Text;
        _dtEmployee.employee_list[0] = _dtemp;
        //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtEmployee));

        _dtEmployee = callServicePostEmp(_urlGetProfileMIS, _dtEmployee);
        setGridData(GvMaster, _dtEmployee.employee_list);
    }

    //protected void getset()
    // {

    //     _dtswl = new data_softwarelicense_devices();
    //     _dtswl.bind_softwarelicense_list = new bind_softwarelicense_detail[1];
    //     bind_softwarelicense_detail _dtswldetail = new bind_softwarelicense_detail();
    //     _dtswldetail.u0_didx = 321;// int.Parse(lbu0_didx.Text);

    //     _dtswl.bind_softwarelicense_list[0] = _dtswldetail;

    //     _dtswl = callServiceSoftwareLicenseDevices(_urlGetDetailSoftwareProfile, _dtswl);
    //     text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtswl));

    //     if (_dtswl.return_code.ToString() == "0")
    //     {

    //         string tableStart = "<table class='table table-bordered'>";
    //         string tableTh = "<tr><th class='text-center'>เครื่องถือครอง</th><th class='text-center'>Software License</th></tr>";
    //         string tableEnd = "</table>";
    //         string tableTr = string.Empty;
    //         foreach (var rows in _dtswl.bind_softwarelicense_list)
    //         {
    //             tableTr += string.Format("<tr><td>{0}</td><td class='text-left'>{1}</td></tr>", rows.u0_code, rows.software_name);
    //         }
    //         btnsoftware.Attributes.Add("data-toggle", "tooltip");
    //         btnsoftware.Attributes.Add("data-placement", "top");
    //         btnsoftware.Attributes.Add("data-html", "true");
    //         btnsoftware.Attributes.Add("title", tableStart + tableTh + tableTr + tableEnd);

    //         btnsoftware.Visible = true;
    //     }
    //     else
    //     {
    //         //btnsoftware.Attributes.Add("data-toggle", "tooltip");
    //         //btnsoftware.Attributes.Add("data-placement", "top");
    //         //btnsoftware.Attributes.Add("data-html", "true");
    //         //btnsoftware.Attributes.Add("title", "ไม่ผูกอุปกรณ์ Software License");

    //         btnsoftware.Visible = false;
    //     }
    // }

    protected void SelectGetProfileList()
    {

        _dtEmployee = new data_employee();
        _dtEmployee.employee_list = new employee_detail[1];
        employee_detail _dtemp = new employee_detail();

        _dtemp.emp_idx = int.Parse(ViewState["showempidx"].ToString()); //
        _dtEmployee.employee_list[0] = _dtemp;

        _dtEmployee = callServicePostEmp(_urlGetWHEREEmpIDX, _dtEmployee);
        //  text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtEmployee));
        // ViewState["ShowEmpFV"] = _dtEmployee.employee_list;
        setFormViewData(FvProfile, _dtEmployee.employee_list_tel);


    }

    protected void SelectGetADOnlineList()
    {

        _dtEmployee = new data_employee();
        _dtEmployee.employee_list = new employee_detail[1];
        employee_detail _dtemp = new employee_detail();

        _dtemp.emp_idx = int.Parse(ViewState["showempidx"].ToString()); //1374;//
        _dtEmployee.employee_list[0] = _dtemp;
        //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtEmployee));

        _dtEmployee = callServicePostEmp(_urlGetWHEREADOnline, _dtEmployee);
        var rtCode_ad = _dtEmployee.return_ad;
        var rtCode_user = _dtEmployee.return_user;
        var rtCode_datead = _dtEmployee.return_datead;
        var rtCode_vpnsap = _dtEmployee.return_vpnsap;
        var rtCode_datevpnsap = _dtEmployee.return_datesap;
        var rtCode_vpnbi = _dtEmployee.return_vpnbi;
        var rtCode_datebi = _dtEmployee.return_datebi;
        var rtCode_vpnexpress = _dtEmployee.return_vpnexpress;
        var rtCode_dateexpress = _dtEmployee.return_dateexpress;
        var rtCode_sap = _dtEmployee.return_usersap;
        var rtCode_datesap = _dtEmployee.return_dateusersap;
        var rtCode_email = _dtEmployee.return_email;
        var rtCode_dateemail = _dtEmployee.return_dateemail;


        Lbpermpol.Text = rtCode_ad.ToString();
        Lbdatead.Text = rtCode_datead.ToString();
        Lbuserad.Text = rtCode_user.ToString();
        Lbusersap.Text = rtCode_sap.ToString();
        Lbdateusersap.Text = rtCode_datesap.ToString();
        Lbvpnsap.Text = getStatus(int.Parse(rtCode_vpnsap));
        Lbdatevpnsap.Text = rtCode_datevpnsap.ToString();
        Lbvpnbi.Text = getStatus(int.Parse(rtCode_vpnbi));
        Lbdatevpnbi.Text = rtCode_datebi.ToString();
        Lbvpnexpress.Text = getStatus(int.Parse(rtCode_vpnexpress));
        Lbdatevpnexpress.Text = rtCode_dateexpress.ToString();
        Lbemail.Text = rtCode_email.ToString();
        Lbdateemail.Text = rtCode_dateemail.ToString();



    }

    protected void SelectGetHolderList()
    {

        _dtEmployee = new data_employee();
        _dtEmployee.BoxHolder_list = new Holder_List[1];
        Holder_List _dtemp = new Holder_List();

        _dtemp.NHDEmpIDX = int.Parse(ViewState["showempidx"].ToString()); //1374;//
        _dtEmployee.BoxHolder_list[0] = _dtemp;
        ///  text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtEmployee));

        _dtEmployee = callServicePostEmp(_urlGetWHEREHolder, _dtEmployee);
        setGridData(GvHolder, _dtEmployee.BoxHolder_list);



    }

    protected void SelectCostCenterList(DropDownList ddlName)
    {
        _dtEmployee.CostCenterDetail = new CostCenter[1];
        CostCenter dtemployee = new CostCenter();

        _dtEmployee.CostCenterDetail[0] = dtemployee;

        _dtEmployee = callServicePostEmp(_urlGetCostcenterOld, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.CostCenterDetail, "CostNo", "CostIDX");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือก CostCenter....", "0"));
    }

    protected void getSetPermPolToDDL(DropDownList ddlName)
    {
        permission_policy objPermPol = new permission_policy();
        dataADOnline.ad_permission_policy_action = new permission_policy[1];
        dataADOnline.ad_permission_policy_action[0] = objPermPol;
        dataADOnline = callServiceADOnline(_urlReadPermPol, dataADOnline);
        setDdlData(ddlName, dataADOnline.ad_permission_policy_action, "perm_pol_name", "m0_perm_pol_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกสิทธิ์ AD....", "0"));


    }

    protected void SelectLocate()
    {
        DropDownList ddllocname = (DropDownList)FvProfile.FindControl("ddllocname");
        TextBox lblLocIDX = (TextBox)FvProfile.FindControl("lblLocIDX");

        _dtEmployee = new data_employee();
        _dtEmployee.employee_list = new employee_detail[1];
        employee_detail _dtemp = new employee_detail();

        _dtEmployee.employee_list[0] = _dtemp;

        _dtEmployee = callServicePostEmp(_urlSelectLocate_Tel, _dtEmployee);
        setDdlData(ddllocname, _dtEmployee.employee_list, "LocName", "LocIDX");
        ddllocname.Items.Insert(0, new ListItem("กรุณาเลือกสถานที่....", "0"));
        ddllocname.SelectedValue = lblLocIDX.Text;

    }

    protected void SelectMainTel()
    {
        DropDownList ddlmaintel = (DropDownList)FvProfile.FindControl("ddlmaintel");
        DropDownList ddllocname = (DropDownList)FvProfile.FindControl("ddllocname");
        TextBox lblm0telidx = (TextBox)FvProfile.FindControl("lblm0telidx");

        _dtEmployee = new data_employee();
        _dtEmployee.BoxTel_list = new Tel_List[1];
        Tel_List _dtemp = new Tel_List();
        _dtemp.LocIDX = int.Parse(ddllocname.SelectedValue);

        _dtEmployee.BoxTel_list[0] = _dtemp;
        //  text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtEmployee));

        _dtEmployee = callServicePostEmp(_urlSelectMainTel, _dtEmployee);
        setDdlData(ddlmaintel, _dtEmployee.BoxTel_list, "m0_tel", "m0telidx");
        ddlmaintel.Items.Insert(0, new ListItem("กรุณาเลือกเบอร์ติดต่อองค์กร....", "0"));
        ddlmaintel.SelectedValue = lblm0telidx.Text;

    }

    protected void SelectMainTel_Edit()
    {
        DropDownList ddlmaintel = (DropDownList)FvProfile.FindControl("ddlmaintel");
        DropDownList ddllocname = (DropDownList)FvProfile.FindControl("ddllocname");

        _dtEmployee = new data_employee();
        _dtEmployee.BoxTel_list = new Tel_List[1];
        Tel_List _dtemp = new Tel_List();
        _dtemp.LocIDX = int.Parse(ddllocname.SelectedValue);

        _dtEmployee.BoxTel_list[0] = _dtemp;
        //  text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtEmployee));

        _dtEmployee = callServicePostEmp(_urlSelectMainTel, _dtEmployee);
        setDdlData(ddlmaintel, _dtEmployee.BoxTel_list, "m0_tel", "m0telidx");
        ddlmaintel.Items.Insert(0, new ListItem("กรุณาเลือกเบอร์ติดต่อองค์กร....", "0"));

    }

    protected void InsertPhoneEmpIDX()
    {
        Label LbOrgidx = (Label)FvProfile.FindControl("LbOrgidx");
        Label LbDeptidx = (Label)FvProfile.FindControl("LbDeptidx");
        Label Lbrsecidx = (Label)FvProfile.FindControl("Lbrsecidx");
        Label LbEmpidx = (Label)FvProfile.FindControl("LbEmpidx");
        DropDownList ddlmaintel = (DropDownList)FvProfile.FindControl("ddlmaintel");
        TextBox lblu0tel = (TextBox)FvProfile.FindControl("lblu0tel");


        _dtEmployee = new data_employee();
        _dtEmployee.BoxTel_list = new Tel_List[1];
        Tel_List _dtemp = new Tel_List();

        _dtemp.OrgIDX = int.Parse(LbOrgidx.Text);
        _dtemp.RDeptIDX = int.Parse(LbDeptidx.Text);
        _dtemp.RSecIDX = int.Parse(Lbrsecidx.Text);
        _dtemp.m0telidx = int.Parse(ddlmaintel.SelectedValue);
        _dtemp.u0_tel = lblu0tel.Text;
        _dtemp.CEmpIDX = int.Parse(LbEmpidx.Text);
        _dtEmployee.BoxTel_list[0] = _dtemp;

        _dtEmployee = callServicePostEmp(_urlInsertPhoneWHEREEmpIDX, _dtEmployee);

    }

    protected void UpdatePhoneEmpIDX()
    {
        DropDownList ddlmaintel = (DropDownList)FvProfile.FindControl("ddlmaintel");
        TextBox lblu0tel = (TextBox)FvProfile.FindControl("lblu0tel");
        Label LbEmpidx = (Label)FvProfile.FindControl("LbEmpidx");

        _dtEmployee = new data_employee();
        _dtEmployee.BoxTel_list = new Tel_List[1];
        Tel_List _dtemp = new Tel_List();

        _dtemp.m0telidx = int.Parse(ddlmaintel.SelectedValue);
        _dtemp.u0_tel = lblu0tel.Text;
        _dtemp.CEmpIDX = int.Parse(LbEmpidx.Text);

        _dtEmployee.BoxTel_list[0] = _dtemp;
        //  text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtEmployee));

        _dtEmployee = callServicePostEmp(_urlUpdatePhoneWHEREEmpIDX, _dtEmployee);

    }


    #endregion

    #region Call Services
    protected data_employee callServiceEmpProfile(string _cmdUrl)
    {
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }

    protected data_employee callServicePostEmp(string _cmdUrl, data_employee _dtEmployee)
    {
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);


        return _dtEmployee;
    }
    protected data_adonline callServiceADOnline(string _cmdUrl, data_adonline _data_adonline)
    {
        _localJson = _funcTool.convertObjectToJson(_data_adonline);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        _data_adonline = (data_adonline)_funcTool.convertJsonToObject(typeof(data_adonline), _localJson);
        return _data_adonline;
    }
    protected data_softwarelicense_devices callServiceSoftwareLicenseDevices(string _cmdUrl, data_softwarelicense_devices _data_softwarelicense_devices)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_softwarelicense_devices);
        // litDebug.Text = _localJson;

        // call services
        //_localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_softwarelicense_devices = (data_softwarelicense_devices)_funcTool.convertJsonToObject(typeof(data_softwarelicense_devices), _localJson);

        return _data_softwarelicense_devices;
    }
    #endregion

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster.EditIndex != e.Row.RowIndex) //to overlook header row
                    {
                        var lbVeiw1 = ((LinkButton)e.Row.FindControl("lbVeiw1"));


                        if (ViewState["rdept_idx"].ToString() != "20" && ViewState["rdept_idx"].ToString() != "21")
                        {
                            GvMaster.Columns[8].Visible = false;
                            GvMaster.Columns[9].Visible = false;
                        }
                        else
                        {
                            GvMaster.Columns[8].Visible = true;
                            GvMaster.Columns[9].Visible = true;
                        }

                    }
                }

                break;


            case "GvHolder":


                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvHolder.EditIndex != e.Row.RowIndex) //to overlook header row
                    {
                        var lbu0_didx = ((Label)e.Row.FindControl("lbu0_didx"));
                        var btnsoftware = ((Label)e.Row.FindControl("btnsoftware"));
                        int i = 0;
                        string enter = "\n";

                        _dtswl = new data_softwarelicense_devices();
                        _dtswl.bind_softwarelicense_list = new bind_softwarelicense_detail[1];
                        bind_softwarelicense_detail _dtswldetail = new bind_softwarelicense_detail();
                        _dtswldetail.u0_didx = int.Parse(lbu0_didx.Text);

                        _dtswl.bind_softwarelicense_list[0] = _dtswldetail;
                        //  text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtswl));

                        _dtswl = callServiceSoftwareLicenseDevices(_urlGetDetailSoftwareProfile, _dtswl);

                        if (_dtswl.return_code.ToString() == "0")
                        {

                            //string tableStart = "<table class='table table-bordered'>";
                            //string tableTh = "<tr><th class='text-center'>เครื่องถือครอง</th><th class='text-center'>Software License</th></tr>";
                            //string tableEnd = "</table>";
                            //string tableTr = string.Empty;
                            foreach (var rows in _dtswl.bind_softwarelicense_list)
                            {
                                e.Row.ToolTip += _dtswl.bind_softwarelicense_list[i].software_name + " " + enter;
                                i++;

                                // tableTr += string.Format("<tr><td>{0}</td><td class='text-left'>{1}</td></tr>", rows.u0_code, rows.software_name);
                            }
                            //btnsoftware.Attributes.Add("data-toggle", "tooltip");
                            //btnsoftware.Attributes.Add("data-placement", "top");
                            //btnsoftware.Attributes.Add("data-html", "true");
                            //btnsoftware.Attributes.Add("title", tableStart + tableTh + tableTr + tableEnd);


                            btnsoftware.Visible = true;
                        }
                        else
                        {
                            //btnsoftware.Attributes.Add("data-toggle", "tooltip");
                            //btnsoftware.Attributes.Add("data-placement", "top");
                            //btnsoftware.Attributes.Add("data-html", "true");
                            //btnsoftware.Attributes.Add("title", "ไม่ผูกอุปกรณ์ Software License");

                            btnsoftware.Visible = false;
                        }

                    }
                }







                break;



        }

    }

    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();

                SelectMasterList();

                break;

        }
    }

    #endregion

    #region Trigger
    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger1 = new PostBackTrigger();
        trigger1.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger1);
    }



    #endregion

    #region reuse
    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    protected void setFormViewData(FormView fvName, Object obj)
    {
        fvName.DataSource = obj;
        fvName.DataBind();
    }
    protected string getStatus(int status)
    {
        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }
    #endregion

    #region FvDetail_DataBound

    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        var FvName = (FormView)sender;

        switch (FvName.ID)
        {

            case "FvProfile":
                FormView FvProfile = (FormView)ViewDetail.FindControl("FvProfile");

                if (FvProfile.CurrentMode == FormViewMode.ReadOnly)
                {


                }
                else if (FvProfile.CurrentMode == FormViewMode.Edit)
                {

                    /// ddllocname.SelectedValue = lbLocIDX.Text;


                }

                break;
        }
    }
    #endregion

    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (DropDownList)sender;
        FormView FvProfile1 = (FormView)ViewDetail.FindControl("FvProfile");

        switch (ddName.ID)
        {
            case "ddlorgidx":
                getDepartmentList(ddlrdeptidx, int.Parse(ddlorgidx.SelectedValue));
                break;

            case "ddlrdeptidx":
                getSectionList(ddlrsecidx, int.Parse(ddlorgidx.SelectedValue), int.Parse(ddlrdeptidx.SelectedValue));
                break;

            case "ddlrsecidx":
                getPositionList(ddlposition, int.Parse(ddlorgidx.SelectedValue), int.Parse(ddlrdeptidx.SelectedValue), int.Parse(ddlrsecidx.SelectedValue));
                break;
            case "ddllocname":

                SelectMainTel_Edit();

                break;



        }
    }
    #endregion

    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {

            case "btnsearch":
                SelectMasterList();
                break;

            case "btnrefresh":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "ViewProfile":
                int empidx = int.Parse(cmdArg);

                if (empidx == 0)
                {
                    ViewState["showempidx"] = int.Parse(ViewState["EmpIDX"].ToString());
                }
                else
                {
                    ViewState["showempidx"] = empidx;
                }

                IndexList.Attributes.Add("class", "non-active");
                ProfileList.Attributes.Add("class", "active");
                MvMaster.SetActiveView(ViewDetail);
                SelectGetProfileList();
                SelectGetADOnlineList();
                SelectGetHolderList();
                ImageButton btnedit = (System.Web.UI.WebControls.ImageButton)FvProfile.FindControl("btnedit");

                if (ViewState["showempidx"].ToString() != ViewState["EmpIDX"].ToString())
                {
                    btnedit.Visible = false;
                }
                else
                {
                    btnedit.Visible = true;
                }

                break;

            case "btnedit":

                FvProfile.ChangeMode(FormViewMode.Edit);
                FvProfile.DataBind();
                SelectGetProfileList();
                SelectLocate();
                SelectMainTel();

                break;

            case "btnfvread":
                FvProfile.ChangeMode(FormViewMode.ReadOnly);
                FvProfile.DataBind();
                SelectGetProfileList();
                break;

            case "btnsaveedit":
                TextBox lblu0telidx = (TextBox)FvProfile.FindControl("lblu0telidx");

                if (lblu0telidx.Text == "0")
                {
                    InsertPhoneEmpIDX();
                }
                else
                {
                    UpdatePhoneEmpIDX();
                }
                FvProfile.ChangeMode(FormViewMode.ReadOnly);
                FvProfile.DataBind();
                SelectGetProfileList();

                break;

            case "btnexport":

                //  GridView GridView2 = (GridView)ViewIndex.FindControl("GridView2");

                data_employee _dtEmployee_export = new data_employee();
                _dtEmployee_export.employee_list = new employee_detail[1];
                employee_detail _dtemp = new employee_detail();

                _dtemp.org_idx = int.Parse(ddlorgidx.SelectedValue);
                _dtemp.rdept_idx = int.Parse(ddlrdeptidx.SelectedValue);
                _dtemp.rsec_idx = int.Parse(ddlrsecidx.SelectedValue);
                _dtemp.rpos_idx = int.Parse(ddlposition.SelectedValue);
                _dtemp.costcenter_idx = int.Parse(ddlcostcenter.SelectedValue);
                _dtemp.perm_pol = int.Parse(ddlad.SelectedValue);
                _dtemp.emp_firstname_th = txtempidx.Text;
                if (ddlstatus.SelectedValue == "1" || ddlstatus.SelectedValue == "0")
                {
                    
                    _dtemp.empstatus = ddlstatus.SelectedValue;
                }
                else
                {
                    // text.Text = "2";
                    _dtemp.empstatus = "1,0";
                }


                _dtemp.emp_email = txtemail.Text;
                _dtEmployee_export.employee_list[0] = _dtemp;
                //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtEmployee_export));

                _dtEmployee_export = callServicePostEmp(_urlExportMISProfile, _dtEmployee_export);

                int countRowMaterialTotal = 0;
                if (_dtEmployee_export.employee_list != null)
                {
                    //text.Text = "1";
                    DataTable tableProfileTotal = new DataTable();
                    tableProfileTotal.Columns.Add("ชื่อสกุล EN", typeof(String));
                    tableProfileTotal.Columns.Add("ระดับสิทธิ์ AD", typeof(String));
                    tableProfileTotal.Columns.Add("User AD", typeof(String));
                    tableProfileTotal.Columns.Add("User SAP", typeof(String));
                    tableProfileTotal.Columns.Add("VPN SAP", typeof(String));
                    tableProfileTotal.Columns.Add("VPN BI", typeof(String));
                    tableProfileTotal.Columns.Add("VPN Express", typeof(String));
                    tableProfileTotal.Columns.Add("เบอร์ต่อภายใน", typeof(String));
                    tableProfileTotal.Columns.Add("Email", typeof(String));
                    tableProfileTotal.Columns.Add("บริษัท", typeof(String));
                    tableProfileTotal.Columns.Add("ฝ่าย", typeof(String));
                    tableProfileTotal.Columns.Add("แผนก", typeof(String));
                    tableProfileTotal.Columns.Add("Cost Center", typeof(String));
                    tableProfileTotal.Columns.Add("สถานที่", typeof(String));
                    tableProfileTotal.Columns.Add("สถานะการทำงาน", typeof(String));
                    tableProfileTotal.Columns.Add("Holder Computer", typeof(String));
                    tableProfileTotal.Columns.Add("Holder Monitor", typeof(String));
                    tableProfileTotal.Columns.Add("Holder Printer", typeof(String));

                    foreach (var employeeTotal in _dtEmployee_export.employee_list)
                    {
                        DataRow addProfile = tableProfileTotal.NewRow();
                        addProfile[0] = employeeTotal.emp_name_en;
                        addProfile[1] = employeeTotal.perm_pol_name;
                        addProfile[2] = employeeTotal.u0_perm_perm_ad;
                        addProfile[3] = employeeTotal.u0_perm_perm_sap;
                        addProfile[4] = employeeTotal.u0_perm_perm_ad;
                        addProfile[5] = employeeTotal.u0_perm_perm_ad;
                        addProfile[6] = employeeTotal.u0_perm_perm_ad;
                        addProfile[7] = employeeTotal.u0_tel;
                        addProfile[8] = employeeTotal.emp_email;
                        addProfile[9] = employeeTotal.org_name_th;
                        addProfile[10] = employeeTotal.dept_name_th;
                        addProfile[11] = employeeTotal.sec_name_th;
                        addProfile[12] = employeeTotal.costcenter_no;
                        addProfile[13] = employeeTotal.LocName;
                        addProfile[14] = employeeTotal.empstatus;
                        addProfile[15] = employeeTotal.u0_code;
                        addProfile[16] = employeeTotal.u0_monitor;
                        addProfile[17] = employeeTotal.u0_printer;


                        tableProfileTotal.Rows.InsertAt(addProfile, countRowMaterialTotal++);
                    }
                    WriteExcelWithNPOI(tableProfileTotal, "xls", "report-profile");
                }

                break;
        }
    }
    #endregion

    #region xml
    protected void WriteExcelWithNPOI(DataTable dt, String extension, String fileName)
    {
        IWorkbook workbook;
        if (extension == "xlsx")
        {
            workbook = new XSSFWorkbook();
        }
        else if (extension == "xls")
        {
            workbook = new HSSFWorkbook();
        }
        else
        {
            throw new Exception("This format is not supported");
        }
        ISheet sheet1 = workbook.CreateSheet("Sheet 1");
        IRow row1 = sheet1.CreateRow(0);
        for (int j = 0; j < dt.Columns.Count; j++)
        {
            ICell cell = row1.CreateCell(j);
            String columnName = dt.Columns[j].ToString();
            cell.SetCellValue(columnName);
        }
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            IRow row = sheet1.CreateRow(i + 1);
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                ICell cell = row.CreateCell(j);
                String columnName = dt.Columns[j].ToString();
                cell.SetCellValue(dt.Rows[i][columnName].ToString());
            }
        }
        using (var exportData = new MemoryStream())
        {
            Response.Clear();
            workbook.Write(exportData);
            if (extension == "xlsx")
            {
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xlsx"));
                Response.BinaryWrite(exportData.ToArray());
            }
            else if (extension == "xls")
            {
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xls"));
                Response.BinaryWrite(exportData.GetBuffer());
            }
            Response.End();
        }
    }


    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    #endregion
}