﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="nwd_m0_company.aspx.cs" Inherits="websystem_masterdata_nwd_m0_company" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" Runat="Server">
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>

    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">
         <asp:View ID="ViewIndex" runat="server">
             <div class="col-md-12">
                 <asp:LinkButton ID="btnToInsert" CssClass="btn btn-primary" runat="server" CommandName="btnToInsert" OnCommand="btnCommand"><i class="fa fa-plus-square"></i> ข้อมูลบริษัทที่ต่อประกัน</asp:LinkButton>

                 <asp:GridView ID="GvMaster"
                       runat="server"
                       AutoGenerateColumns="false"
                       DataKeyNames="company_idx"
                       CssClass="table table-striped table-bordered table-responsive col-md-12 m-t-10"
                       HeaderStyle-CssClass="info"
                       AllowPaging="true"
                       PageSize="10"
                       OnRowEditing="Master_RowEditing"
                       OnRowUpdating="Master_RowUpdating"
                       OnRowCancelingEdit="Master_RowCancelingEdit"
                       OnPageIndexChanging="Master_PageIndexChanging"
                       OnRowDataBound="Master_RowDataBound">
                       <PagerStyle CssClass="pageCustom" />
                       <PagerSettings Mode="NumericFirstLast" PageButtonCount="1" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                       <EmptyDataTemplate>
                          <div style="text-align: center">No result</div>
                       </EmptyDataTemplate>
                       <Columns>
                          <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center" 
                             HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
                             <ItemTemplate>
                                  <small>
                                <%# (Container.DataItemIndex +1) %>
                                   </small>
                             </ItemTemplate>
                             <EditItemTemplate>
                                <asp:TextBox ID="company_idx" runat="server" CssClass="form-control"
                                   Visible="False" Text='<%# Eval("company_idx")%>' />
                                <div class="col-md-6">
                                   <div class="form-group">
                                      <small>
                                      <label class="pull-left">ชื่อบริษัทที่ต่อประกัน</label>
                                      <asp:UpdatePanel ID="panelTypeUpdate" runat="server">
                                         <ContentTemplate>
                                            
                                            <asp:TextBox ID="txtCompanyNameUpdate" runat="server" CssClass="form-control"
                                               Text='<%# Eval("company_name")%>' />
                                            <asp:RequiredFieldValidator ID="requiredCompanyNameUpdate"
                                               ValidationGroup="saveCompanyNameUpdate" runat="server"
                                               Display="Dynamic"
                                               SetFocusOnError="true"
                                               ControlToValidate="txtCompanyNameUpdate"
                                               Font-Size="1em" ForeColor="Red"
                                               ErrorMessage="กรุณากรอกชื่อบริษัทที่ต่อประกัน" />
                                         </ContentTemplate>
                                      </asp:UpdatePanel>
                                      </small>
                                   </div>
                                </div>   
                                
                                <div class="col-md-6">
                                   <div class="form-group">
                                      <small>
                                      <label class="pull-left">สถานะ</label>
                                      <asp:DropDownList ID="ddlCompanyStatusUpdate" AutoPostBack="false" runat="server"
                                         CssClass="form-control" SelectedValue='<%# Eval("company_status") %>'>
                                         <asp:ListItem Value="1" Text="Online"></asp:ListItem>
                                         <asp:ListItem Value="0" Text="Offline"></asp:ListItem>
                                      </asp:DropDownList>
                                      </small>
                                   </div>
                                </div>
                                <div class="col-md-12">
                                   <div class="pull-left">
                                      <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server"
                                         ValidationGroup="saveCompanyNameUpdate" CommandName="Update"
                                         OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')">
                                         บันทึก
                                      </asp:LinkButton>
                                      <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server"
                                         CommandName="Cancel">ยกเลิก</asp:LinkButton>
                                   </div>
                                </div>
                             </EditItemTemplate>
                          </asp:TemplateField>

                           <asp:TemplateField HeaderText="ชื่อบริษัทที่ต่อประกัน" ItemStyle-HorizontalAlign="Left"
                             HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                             <ItemTemplate>
                                <small>
                                   <asp:Label ID="CompanyName" runat="server" Text='<%# Eval("company_name") %>' />
                                </small>
                             </ItemTemplate>
                             <EditItemTemplate />
                             <FooterTemplate />
                          </asp:TemplateField>

                           <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center"
                             HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                             <ItemTemplate>
                                <small>
                                   <asp:Label ID="Statuscompany" runat="server" Text='<%# getStatus((int)Eval("company_status")) %>' />
                                </small>
                             </ItemTemplate>
                             <EditItemTemplate />
                             <FooterTemplate />
                          </asp:TemplateField>

                           <asp:TemplateField HeaderText="Manage" ItemStyle-HorizontalAlign="center"
                             HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                             <ItemTemplate>
                                <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                   data-toggle="tooltip" title="แก้ไข"><i class="glyphicon glyphicon-pencil"></i></asp:LinkButton>
                                <asp:LinkButton ID="delete" CssClass="text-trash" runat="server" data-toggle="tooltip"
                                   title="ลบ" CommandName="btnDelete" OnCommand="btnCommand"
                                   CommandArgument='<%# Eval("company_idx") %>'
                                   OnClientClick="return confirm('คุณค้องการลบข้อมูลนี้ใช่หรือไม่')">
                                   <i class="glyphicon glyphicon-trash"></i>
                                </asp:LinkButton>
                             </ItemTemplate>
                             <EditItemTemplate />
                             <FooterTemplate />
                          </asp:TemplateField>
                            
                       </Columns>
                    </asp:GridView>

            </div>

        </asp:View>

      <!-- Start Insert Form -->
      <asp:View ID="ViewInsert" runat="server">
         <div class="row col-md-12">
            <div class="row">
               <div class="col-md-12">
                  <div class="form-group">
                     <asp:LinkButton CssClass="btn btn-danger" data-toggle="tooltip" title="Back" runat="server"
                        CommandName="btnCancel" OnCommand="btnCommand">ย้อนกลับ</asp:LinkButton>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <label>ชื่อบริษัทที่ต่อประกัน</label>
                     <asp:UpdatePanel ID="panelCompanyName" runat="server">
                        <ContentTemplate>
                           <asp:TextBox ID="txtCompanyName" runat="server" CssClass="form-control"
                              placeholder="ชื่อบริษัทที่ต่อประกัน..." />
                           <asp:RequiredFieldValidator ID="requiredTypeName"
                              ValidationGroup="saveCompany" runat="server"
                              Display="Dynamic"
                              SetFocusOnError="true"
                              ControlToValidate="txtCompanyName"
                              Font-Size="1em" ForeColor="Red"
                              ErrorMessage="กรุณากรอกชื่อบริษัทที่ต่อประกัน" />
                        </ContentTemplate>
                     </asp:UpdatePanel>
                  </div>
               </div>
               
               <div class="col-md-6">
                  <div class="form-group">
                     <label>สถานะ</label>
                     <asp:DropDownList ID="ddlCompanyStatus" runat="server" CssClass="form-control">
                        <asp:ListItem Value="1" Text="Online" />
                        <asp:ListItem Value="0" Text="Offline" />
                     </asp:DropDownList>
                  </div>
               </div>

               <div class="col-md-12">
                  <div class="form-group">
                     <asp:Button CssClass="btn btn-success" runat="server" CommandName="btnInsert" OnCommand="btnCommand" Text="บันทึก" ValidationGroup="saveCompany" />
                  </div>
               </div>
            </div>
         </div>
      </asp:View>
      <!-- End Insert Form -->



    </asp:MultiView>


</asp:Content>

