﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="hr_room_booking.aspx.cs" Inherits="websystem_hr_hr_room_booking" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:HyperLink ID="setOntop" runat="server"></asp:HyperLink>

    <!--tab menu-->
    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default" id="divBtnHead" runat="server" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-bar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="sub-navbar">
                        <a class="navbar-brand" href="#"><b>Menu</b></a>
                    </div>
                </div>

                <!--Collect the nav links, forms, and other content for toggling-->
                <div class="collapse navbar-collapse" id="menu-bar">
                    <ul class="nav navbar-nav" id="uiNav" runat="server">
                        <li id="li0" runat="server">
                            <asp:LinkButton ID="lbDetailRoom" runat="server" CommandName="cmdDetailRoom" OnCommand="navCommand" CommandArgument="docDetailRoom"> ห้องประชุม</asp:LinkButton>
                        </li>
                        <li id="li1" runat="server">
                            <asp:LinkButton ID="lbDetail" runat="server" CommandName="cmdDetail" OnCommand="navCommand" CommandArgument="docDetail"> รายการทั่วไป</asp:LinkButton>
                        </li>
                        <li id="li2" runat="server">
                            <asp:LinkButton ID="lbCreate" runat="server" CommandName="cmdCreate" OnCommand="navCommand" CommandArgument="docCreate"> สร้างรายการจองห้องประชุม</asp:LinkButton>
                        </li>

                        <% if (ViewState["rpos_permission"].ToString() == "5911" || ViewState["rpos_permission"].ToString() == "5912" || ViewState["rpos_permission"].ToString() == "5882" || ViewState["rpos_permission"].ToString() == "5883" || ViewState["rpos_permission"].ToString() == "5896" || ViewState["rpos_permission"].ToString() == "5897" || ViewState["rpos_permission"].ToString() == "886" || ViewState["rpos_permission"].ToString() == "13" || ViewState["rsec_permission"].ToString() == "210" || ViewState["rpos_permission"].ToString() == "3543" || ViewState["rpos_permission"].ToString() == "5945" || ViewState["rpos_permission"].ToString() == "12220" || ViewState["rpos_permission"].ToString() == "13630" || ViewState["rsec_permission"].ToString() == "80" || ViewState["rsec_permission"].ToString() == "79" || ViewState["rpos_permission"].ToString() == "949" || ViewState["rpos_permission"].ToString() == "7114")
                            { %>
                        <li id="li3" runat="server">
                            <asp:LinkButton ID="lbApprove" runat="server" CommandName="cmdApprove" OnCommand="navCommand" CommandArgument="docApprove" Visible="true"> รายการรออนุมัติ/ รอการเปลี่ยนแปลง</asp:LinkButton>
                        </li>


                        <% } %>

                        <% if (ViewState["rpos_permission"].ToString() == "5911" || ViewState["rpos_permission"].ToString() == "5912" || ViewState["rpos_permission"].ToString() == "5882" || ViewState["rpos_permission"].ToString() == "5883" || ViewState["rpos_permission"].ToString() == "5896" || ViewState["rpos_permission"].ToString() == "5897" || ViewState["rpos_permission"].ToString() == "886" || ViewState["rpos_permission"].ToString() == "13" || ViewState["rsec_permission"].ToString() == "210" || ViewState["rpos_permission"].ToString() == "3543" || ViewState["rpos_permission"].ToString() == "5945" || ViewState["rpos_permission"].ToString() == "12220" || ViewState["rpos_permission"].ToString() == "13630" || ViewState["rsec_permission"].ToString() == "80" || ViewState["rsec_permission"].ToString() == "79" || ViewState["rpos_permission"].ToString() == "949" || ViewState["rpos_permission"].ToString() == "7114" || ViewState["rpos_permission"].ToString() == "12499" || ViewState["rpos_permission"].ToString() == "780" || ViewState["rpos_permission"].ToString() == "7062")
                            { %>
                        <li id="li4" runat="server">
                            <asp:LinkButton ID="lbReport" runat="server" CommandName="cmdReport" OnCommand="navCommand" CommandArgument="docReport" Visible="true"> รายงาน</asp:LinkButton>
                        </li>

                        <% } %>
                    </ul>

                    <ul class="nav navbar-nav navbar-right" runat="server">

                        <li id="_divMenuLiToDocument" runat="server">
                            <asp:HyperLink ID="_divMenuBtnToDocument" runat="server"
                                NavigateUrl="https://docs.google.com/document/d/1JhPRLQm2gVCwARkpJhTF8Qrfv8zR03-qz1ZVzIGoqJo/edit?usp=sharing" Target="_blank" Text="คู่มือการใช้งาน" />
                        </li>
                    </ul>
                </div>
                <!--Collect the nav links, forms, and other content for toggling-->

            </div>
        </nav>
    </div>
    <!--tab menu-->

    <div class="col-md-12">
        <asp:Literal ID="litDebug" runat="server"></asp:Literal>
        <asp:Literal ID="litDebug1" runat="server"></asp:Literal>
    </div>

    <!--multiview-->
    <asp:MultiView ID="mvSystem" runat="server">

        <!--View Detail Room-->
        <asp:View ID="docDetailRoom" runat="server">
            <div class="col-md-12">

                <%--<div class="col-sm-12">
                    <div class="form-group">
                        <div id="myCarousel" runat="server" class="carousel slide m-b-20px" style="height: 200px;" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <asp:Literal ID="indicators" runat="server" />
                            </ol>
                            <div class="carousel-inner" role="listbox">
                                <asp:Literal ID="alertnews" runat="server" />

                                <asp:Literal ID="images" runat="server" />
                                <h5>&nbsp;</h5>
                                <div class="list-group">
                                    <asp:Literal ID="Literal3" runat="server" />
                                </div>
                                <asp:Label ID="lbidxnews" Visible="false" runat="server"></asp:Label>
                            </div>
                            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
                <script>
                    var prm = Sys.WebForms.PageRequestManager.getInstance();
                    prm.add_endRequest(function () {
                        $('.carousel').carousel()

                        $('.carousel-control.left').click(function (e) {
                            e.stopPropagation();
                            $('.carousel').carousel('prev');
                            return false;
                        });

                        $('.carousel-control.right').click(function (e) {
                            e.stopPropagation();
                            $('.carousel').carousel('next');
                            return false;
                        });
                    });


                    $(function () {
                        $('.carousel').carousel()

                        $('.carousel-control.left').click(function (e) {
                            e.stopPropagation();
                            $('.carousel').carousel('prev');
                            return false;
                        });

                        $('.carousel-control.right').click(function (e) {
                            e.stopPropagation();
                            $('.carousel').carousel('next');
                            return false;
                        });
                    });

                </script>--%>


                <div class="panel panel-info" id="Calendar_Selected" runat="server" visible="true">
                    <div class="panel-body">
                        <div class="row col-md-3">
                            <asp:FormView ID="FvDetailUseRoomSearch" runat="server" DefaultMode="Insert" Width="100%">
                                <InsertItemTemplate>

                                    <div class="panel panel-info">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">ค้นหารายการจองห้องประชุม</h3>
                                        </div>
                                        <div class="panel-body">
                                            <%--<div class="form-horizontal" role="form">--%>

                                            <asp:UpdatePanel ID="updateRoomInsert" runat="server">
                                                <ContentTemplate>

                                                    <%--  <div class="form-group">--%>
                                                    <%--<label class="col-sm-2 control-label">สถานที่</label>--%>
                                                    <div class="col-sm-12">
                                                        <asp:DropDownList ID="ddlPlaceSearch" runat="server" CssClass="form-control ddlPlaceSearch" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="Re_ddlPlaceSearch"
                                                            runat="server"
                                                            InitialValue="0"
                                                            ControlToValidate="ddlPlaceSearch" Display="None" SetFocusOnError="true"
                                                            ErrorMessage="*กรุณาเลือกสถานที่"
                                                            ValidationGroup="SearchDetail" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="Validator1" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlPlaceSearch" Width="200" PopupPosition="BottomLeft" />
                                                        <br />
                                                    </div>

                                                    <%--<label class="col-sm-2 control-label">ห้องประชุม</label>--%>
                                                    <%-- </div>--%>
                                                    <%--<br />--%>
                                                    <%-- <div class="form-group">--%>
                                                    <div class="col-sm-12">
                                                        <asp:DropDownList ID="ddlRoomSearch" runat="server" CssClass="form-control ddlRoomSearch" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                            <asp:ListItem Value="0">--- เลือกห้องประชุม ---</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <br />
                                                    </div>
                                                    <%-- </div>
                                                        <br />--%>
                                                    <%--<div class="form-group">--%>
                                                    <%--   <label class="col-sm-2 control-label"></label>--%>
                                                    <div class="col-sm-12" id="div_searchRoom" runat="server" visible="false">
                                                        <asp:LinkButton ID="btnSearchRoom" runat="server"
                                                            CssClass="btn btn-primary btnSearchRoom" data-original-title="ค้นหา" data-toggle="tooltip" ValidationGroup="SearchDetail"><span class="glyphicon glyphicon-search"></span> ค้นหา</asp:LinkButton>

                                                    </div>
                                                    <%--<label class="col-sm-6 control-label"></label>--%>


                                                    <%--  </div>--%>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <%--</div>--%>
                                        </div>
                                    </div>

                                </InsertItemTemplate>
                            </asp:FormView>
                        </div>

                        <div class="col-md-9">
                            <%-- <div id="calendar">
                            </div>--%>

                            <div id="calendar">
                                <div id="fullCalModal" class="modal fade">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">close</span></button>
                                                <h4 id="modalTitle" class="modal-title"></h4>
                                            </div>
                                            <div id="modalBody" class="modal-body">uteueuetuteu</div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                <%-- <button class="btn btn-primary"><a id="eventUrl" target="_blank">Event Page</a></button>--%>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>


                    </div>
                </div>

            </div>

        </asp:View>
        <!--View Detail Room-->

        <!--View Detail-->
        <asp:View ID="docDetail" runat="server">
            <div class="col-md-12">

                <div class="form-group" id="div_searchDetail" runat="server" visible="false">
                    <asp:LinkButton ID="btnSearchIndex" CssClass="btn btn-primary" runat="server" data-original-title="แสดงแถบเครื่องมือค้นหา" data-toggle="tooltip" CommandName="cmdSearchIndex" OnCommand="btnCommand"><span class="glyphicon glyphicon-search"></span> ค้นหา</asp:LinkButton>

                    <%-- <asp:LinkButton ID="btnResetSearchPage" CssClass="btn btn-default" runat="server" data-original-title="ล้างค่า" data-toggle="tooltip" CommandName="cmdResetSearchPage" OnCommand="btnCommand"><span class="glyphicon glyphicon-refresh"></span></asp:LinkButton>--%>
                </div>

                <!-- Back To Detail Room Booking -->
                <asp:UpdatePanel ID="Update_BackToDetailRoomBooking" runat="server" Visible="false">
                    <ContentTemplate>
                        <div class="form-group">
                            <asp:LinkButton ID="btnBackToDetail" CssClass="btn btn-danger" data-toggle="tooltip" title="" runat="server"
                                CommandName="cmdCancelToDetail" OnCommand="btnCommand">< ย้อนกลับ</asp:LinkButton>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- Back To Detail Room Booking -->

                <!-- Form Create -->
                <asp:FormView ID="FvSearchDetailRoom" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <InsertItemTemplate>

                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">ค้นหารายการจองห้องประชุม</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">เลือกประเภทการจอง</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlTypeReciveSearchDetail" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0" Text="ไม่เร่งด่วน" />
                                                <asp:ListItem Value="1" Text="เร่งด่วน" />
                                            </asp:DropDownList>

                                        </div>

                                        <label class="col-sm-2 control-label">เลือกสถานะเอกสาร</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlStatusSearchDetail" runat="server" CssClass="form-control">
                                                <%--<asp:ListItem Value="0" Text="ไม่เร่งด่วน" />
                                                <asp:ListItem Value="1" Text="เร่งด่วน" />--%>
                                            </asp:DropDownList>

                                        </div>

                                    </div>

                                    <asp:UpdatePanel ID="updateRoomInsert" runat="server">
                                        <ContentTemplate>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">สถานที่</label>
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddlPlaceSearchDetail" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="Re_ddlPlaceSearchDetail"
                                                        runat="server"
                                                        InitialValue="0"
                                                        ControlToValidate="ddlPlaceSearchDetail" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณาเลือกสถานที่"
                                                        ValidationGroup="SearchDetail" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8444" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlPlaceSearchDetail" Width="200" PopupPosition="BottomLeft" />
                                                </div>
                                                <label class="col-sm-2 control-label">ห้องประชุม</label>
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddlRoomSearchDetail" runat="server" CssClass="form-control">
                                                        <asp:ListItem Value="0">--- เลือกห้องประชุม ---</asp:ListItem>
                                                    </asp:DropDownList>

                                                </div>
                                            </div>


                                            <div class="form-group" id="panel_searchdate" runat="server">
                                                <%-- <asp:Label ID="Label9" runat="server" Text="ระบุวันค้นหา" CssClass="col-sm-2 control-label"></asp:Label>--%>
                                                <label class="col-sm-2 control-label">ระบุวันค้นหา</label>
                                                <div class="col-sm-2">
                                                    <asp:DropDownList ID="ddlSearchDate" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                        <asp:ListItem Value="00">เลือกเงื่อนไข</asp:ListItem>
                                                        <asp:ListItem Value="1">มากกว่า  ></asp:ListItem>
                                                        <asp:ListItem Value="2">น้อยกว่า <</asp:ListItem>
                                                        <asp:ListItem Value="3">ระหว่าง  <></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-sm-4 ">

                                                    <div class='input-group date'>
                                                        <asp:TextBox ID="AddStartdate" runat="server" CssClass="form-control from-date-datepicker" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                        <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="SearchDetail" runat="server" Display="None"
                                                            ControlToValidate="AddStartdate" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกวันที่ต้องการ"
                                                            ValidationExpression="กรุณาเลือกวันที่ต้องการ" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender51" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator3" Width="160" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class='input-group date'>
                                                        <asp:TextBox ID="AddEndDate" runat="server" CssClass="form-control from-date-datepicker" Enabled="false" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                        <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                    </div>
                                                </div>
                                                <%--<p class="help-block"><font color="red">**</font></p>--%>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label"></label>
                                                <div class="col-sm-4">
                                                    <asp:LinkButton ID="btnSearchRoomDetail" runat="server"
                                                        CssClass="btn btn-primary" OnCommand="btnCommand" CommandName="cmdSearchRoomDetail" data-original-title="ค้นหา" data-toggle="tooltip" ValidationGroup="SearchDetail"><span class="glyphicon glyphicon-search"></span> ค้นหา</asp:LinkButton>

                                                </div>
                                                <label class="col-sm-6 control-label"></label>
                                            </div>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>


                                </div>
                            </div>
                        </div>

                    </InsertItemTemplate>
                </asp:FormView>
                <!-- Form Create -->

                <asp:UpdatePanel ID="Panel_SearchDetail" runat="server" Visible="false">
                    <ContentTemplate>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ตั้งแต่วันที่</label>
                                    <div class="col-sm-4">
                                        <div class="input-group date">
                                            <%--<asp:HiddenField ID="txtSearchFromHidden" runat="server" />--%>
                                            <asp:TextBox ID="txtDateStart_search" runat="server" placeholder="วันที่เริ่มต้นจอง..."
                                                CssClass="form-control datetimepicker-from cursor-pointer" />
                                            <span class="input-group-addon show-from-onclick">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                            <%--<asp:RequiredFieldValidator ID="Re_txtDateStart_create"
                                        runat="server"
                                        ControlToValidate="txtDateStart_create" Display="None" SetFocusOnError="true"
                                        ErrorMessage="*กรุณาเลือกวันที่เริ่มต้นจอง"
                                        ValidationGroup="SaveIndex" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server"
                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txtDateStart_create" Width="200" PopupPosition="BottomLeft" />--%>
                                        </div>
                                    </div>

                                    <label class="col-sm-2 control-label">ถึงวันที่</label>
                                    <div class="col-sm-4">
                                        <div class="input-group date">
                                            <%--<asp:HiddenField ID="txtSearchToHidden" runat="server" />--%>
                                            <asp:TextBox ID="txtDateEnd_search" runat="server" placeholder="วันที่สิ้นสุดการจอง..."
                                                CssClass="form-control datetimepicker-to cursor-pointer" />
                                            <span class="input-group-addon show-to-onclick">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                            <%-- <asp:RequiredFieldValidator ID="Re_txtDateEnd_create"
                                        runat="server"
                                        ControlToValidate="txtDateEnd_create" Display="None" SetFocusOnError="true"
                                        ErrorMessage="*กรุณาเลือกวันที่สิ้นสุดการจอง"
                                        ValidationGroup="SaveIndex" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server"
                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txtDateEnd_create" Width="200" PopupPosition="BottomLeft" />--%>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>

                    </ContentTemplate>
                </asp:UpdatePanel>

                <asp:GridView ID="GvDetailRoomBooking" runat="server"
                    AutoGenerateColumns="False"
                    CssClass="table table-striped table-bordered table-hover table-responsive"
                    OnPageIndexChanging="gvPageIndexChanging"
                    OnRowDataBound="gvRowDataBound"
                    AllowPaging="True" PageSize="10">
                    <HeaderStyle CssClass="success" Height="40px" Font-Size="Small" />
                    <RowStyle Font-Size="Small" />
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">--- ไม่พบข้อมูล ---</div>
                    </EmptyDataTemplate>
                    <Columns>

                        <asp:TemplateField HeaderText="ประเภทการจอง" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                            <ItemTemplate>
                                <asp:Label ID="lbl_type_booking_idx_detail" runat="server" Visible="false" Text='<%# Eval("type_booking_idx") %>'></asp:Label>
                                <asp:Label ID="lbl_u0_document_idx_detail" runat="server" Visible="false" Text='<%# Eval("u0_document_idx") %>'></asp:Label>
                                <b>
                                    <asp:Label ID="type_booking_name_detail_on" runat="server" Text='<%# Eval("type_booking_name") %>' Visible="false" data-placement="top"
                                        CssClass="col-sm-12" Style="text-align: center; color: green;">
                                    </asp:Label>

                                    <asp:Label ID="type_booking_name_detail_off" runat="server" Text='<%# Eval("type_booking_name") %>' Visible="false"
                                        data-placement="top" CssClass="col-sm-12" Style="text-align: center; color: red;">
                                    </asp:Label>
                                </b>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="วันที่ทำรายการ" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                            <ItemTemplate>
                                <p>
                                    <asp:Label ID="lbl_create_date_detail" runat="server" Text='<%# Eval("create_date") %>'></asp:Label>
                                </p>
                                <p>
                                    <asp:Label ID="lbl_time_create_date_detail" runat="server" Text='<%# Eval("time_create_date") %>'></asp:Label>
                                </p>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="รายละเอียดการจอง" HeaderStyle-CssClass="text-center">
                            <ItemTemplate>
                                <p>
                                    <b>ชื่อ-สกุลผู้สร้าง:</b>
                                    <asp:Label ID="lbl_cemp_idx_detail" Visible="false" runat="server" Text='<%# Eval("cemp_idx") %>' />
                                    <asp:Label ID="lbl_emp_name_th_detail" runat="server" Text='<%# Eval("emp_name_th") %>' />
                                </p>
                                <p>
                                    <b>องค์กร:</b>
                                    <asp:Label ID="lbl_org_name_th_detail" runat="server" Text='<%# Eval("org_name_th") %>' />
                                </p>
                                <p>
                                    <b>ฝ่าย:</b>
                                    <asp:Label ID="lbl_dept_name_th_detail" runat="server" Text='<%# Eval("dept_name_th") %>' />
                                </p>

                                <p>
                                    <b>สถานที่:</b>
                                    <asp:Label ID="lbl_place_idx_detail" runat="server" Visible="false" Text='<%# Eval("place_idx") %>' />
                                    <asp:Label ID="lbl_place_name_detail" runat="server" Text='<%# Eval("place_name") %>' />
                                </p>
                                <p>
                                    <b>ชื่อห้อง:</b>
                                    <asp:Label ID="lbl_room_name_th_detail" runat="server" Text='<%# Eval("room_name_th") %>' />
                                </p>
                                <p>
                                    <b>หัวข้อการจอง:</b>
                                    <asp:Label ID="lbl_topic_booking_detail" runat="server" Text='<%# Eval("topic_booking") %>' />
                                </p>

                                <p>
                                    <b>ตั้งแต่วันที่:</b>
                                    <asp:Label ID="lbl_checkDateTime" runat="server" Text='<%# Eval("date_time_chekbutton") %>' Visible="false" />
                                    <asp:Label ID="lbl_date_start_detail" runat="server" Text='<%# Eval("date_start") %>' />
                                    <b>เวลา:</b>
                                    <asp:Label ID="lbl_time_start_detail" runat="server" Text='<%# Eval("time_start") %>' />
                                    <b>น.</b>
                                </p>
                                <p>
                                    <b>ถึงวันที่:</b>
                                    <asp:Label ID="lbl_date_end_detail" runat="server" Text='<%# Eval("date_end") %>' />
                                    <b>เวลา:</b>
                                    <asp:Label ID="lbl_time_end_detail" runat="server" Text='<%# Eval("time_end") %>' />
                                    <b>น.</b>
                                </p>
                                <p>
                                    <asp:UpdatePanel ID="PnAddGoogleCalendar" runat="server">
                                        <ContentTemplate>
                                            <asp:LinkButton ID="btnAddGoogleCalendar" CssClass="btn btn-default" runat="server" CommandName="cmdAddGoogleCalendar" OnCommand="btnCommand" CommandArgument='<%# Eval("u0_document_idx")+ ";" + Eval("m0_room_idx")+ ";" + Eval("type_booking_idx") + ";" + Eval("m0_node_idx") + ";" + Eval("m0_actor_idx") + ";" + Eval("staidx") + ";" + Eval("cemp_idx") + ";" + Eval("place_idx") + ";" + Eval("date_start") + ";" + Eval("time_start") + ";" + Eval("date_end") + ";" + Eval("time_end") + ";" + Eval("detail_booking") + ";" + Eval("room_name_th") + ";" + Eval("place_name") + ";" + Eval("topic_booking") + ";" + "1" %>' data-toggle="tooltip" title="เพิ่มลง GoogleCalendar"><span class="far fa-calendar-alt"></span> AddGoogleCalendar</asp:LinkButton>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnAddGoogleCalendar" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </p>
                                <p>
                                    <asp:UpdatePanel ID="PnViewGoogleCalendar" runat="server">
                                        <ContentTemplate>
                                            <asp:LinkButton ID="btnViewGoogleCalendar" CssClass="btn btn-primary" runat="server" CommandName="cmdAddGoogleCalendar" OnCommand="btnCommand" CommandArgument='<%# Eval("u0_document_idx")+ ";" + Eval("m0_room_idx")+ ";" + Eval("type_booking_idx") + ";" + Eval("m0_node_idx") + ";" + Eval("m0_actor_idx") + ";" + Eval("staidx") + ";" + Eval("cemp_idx") + ";" + Eval("place_idx") + ";" + Eval("date_start") + ";" + Eval("time_start") + ";" + Eval("date_end") + ";" + Eval("time_end") + ";" + Eval("detail_booking") + ";" + Eval("room_name_th") + ";" + Eval("place_name") + ";" + Eval("topic_booking") + ";" + "2" %>' data-toggle="tooltip" title="ดูข้อมูล GoogleCalendar"><span class="far fa-calendar-alt"></span> ViewGoogleCalendar</asp:LinkButton>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnViewGoogleCalendar" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </p>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สถานะรายการ" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                            <ItemTemplate>

                                <asp:Label ID="lbl_staidx_detail" runat="server" Text='<%# Eval("staidx") %>' Visible="false" />
                                <asp:Label ID="lbl_status_name_detail" runat="server" Text='<%# Eval("status_name") %>' />
                                <asp:Label ID="lbl_m0_node_idx_detail" runat="server" Text='<%# Eval("m0_node_idx") %>' Visible="false" />
                                <asp:Label ID="lbl_node_name_detail" runat="server" Text='<%# Eval("node_name") %>' />
                                <br />
                                โดย<br />
                                <asp:Label ID="lbl_m0_actor_idx_detail" runat="server" Text='<%# Eval("m0_actor_idx") %>' Visible="false" />
                                <asp:Label ID="lbl_actor_name_detail" runat="server" Text='<%# Eval("actor_name") %>' />

                                <%--  <asp:Label ID="lbl_current_status" runat="server" Text='<%# Eval("current_status") %>'></asp:Label>--%>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                            <ItemTemplate>

                                <asp:LinkButton ID="btnRoomDetail" CssClass="btn-sm btn-info" target="" runat="server" CommandName="cmdViewRoomDetail" OnCommand="btnCommand"
                                    CommandArgument='<%# Eval("u0_document_idx")+ ";" + Eval("m0_room_idx")+ ";" + Eval("type_booking_idx") + ";" + Eval("m0_node_idx") + ";" + Eval("m0_actor_idx") 
                                        + ";" + Eval("staidx") + ";" + Eval("cemp_idx") + ";" + Eval("place_idx")%>'
                                    data-toggle="tooltip" title="ดูข้อมูล"><i class="fa fa-file"></i></asp:LinkButton>

                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>

                <!-- Fv View Detail Room Booking -->
                <asp:FormView ID="fvDetailRoomBooking" runat="server" Width="100%"
                    DefaultMode="ReadOnly" OnDataBound="FvDetail_DataBound">
                    <ItemTemplate>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายละเอียดรายการจองห้องประชุม</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">ประเภทการจอง</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tb_m0_room_idx" runat="server" Visible="false" CssClass="form-control" Text='<%# Eval("m0_room_idx") %>'
                                                Enabled="true" />
                                            <asp:TextBox ID="tb_staidx" runat="server" Visible="false" CssClass="form-control" Text='<%# Eval("staidx") %>' Enabled="true" />
                                            <asp:TextBox ID="tb_u0_document_idx" runat="server" Visible="false" CssClass="form-control" Text='<%# Eval("u0_document_idx") %>' Enabled="false" />
                                            <asp:TextBox ID="tb_type_booking_idx" runat="server" Visible="false" Text='<%# Eval("type_booking_idx") %>' CssClass="form-control" Enabled="false" />
                                            <asp:TextBox ID="tb_type_booking_name" runat="server" CssClass="form-control" Text='<%# Eval("type_booking_name") %>' Enabled="false" />

                                        </div>

                                        <label class="col-sm-2 control-label">ผู้ทำรายการ</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpName" runat="server" CssClass="form-control" Text='<%# Eval("emp_name_th") %>' Enabled="false" />
                                            <asp:TextBox ID="tbActorCempIDX" runat="server" Visible="false" Text='<%# Eval("cemp_idx") %>' CssClass="form-control" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">องค์กร</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpOrg" runat="server" CssClass="form-control" Text='<%# Eval("org_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ฝ่าย</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpDept" runat="server" CssClass="form-control" Text='<%# Eval("dept_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">แผนก</label>
                                        <div class="col-sm-4">

                                            <asp:TextBox ID="tbEmpSec" runat="server" CssClass="form-control" Text='<%# Eval("sec_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ตำแหน่ง</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpPos" runat="server" CssClass="form-control" Text='<%# Eval("pos_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">เบอร์ติดต่อ</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpmobile" runat="server" CssClass="form-control" Text='<%# Eval("emp_mobile_no") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">Email</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpemail" runat="server" CssClass="form-control" Text='<%# Eval("emp_email") %>' Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">สถานที่</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbplace_idx" runat="server" CssClass="form-control" Text='<%# Eval("place_idx") %>' Enabled="false" Visible="false" />
                                            <asp:TextBox ID="tb_place_name" runat="server" CssClass="form-control" Text='<%# Eval("place_name") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ห้องประชุม</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tb_room_name_th" runat="server" CssClass="form-control" Text='<%# Eval("room_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">ตั้งแต่วันที่</label>
                                        <div class="col-sm-4">
                                            <asp:Label ID="tb_checkDateTime" runat="server" Text='<%# Eval("date_time_chekbutton") %>' Visible="false" />
                                            <asp:TextBox ID="tb_date_start" runat="server" CssClass="form-control" Text='<%# Eval("date_start") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">เวลา</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tb_time_start" runat="server" CssClass="form-control" Text='<%# Eval("time_start") %>' Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">ถึงวันที่</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tb_date_end" runat="server" CssClass="form-control" Text='<%# Eval("date_end") %>' Enabled="false" />

                                        </div>
                                        <label class="col-sm-2 control-label">เวลา</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tb_time_end" runat="server" CssClass="form-control" Text='<%# Eval("time_end") %>' Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">สถานะ</label>
                                        <div class="col-sm-4">

                                            <asp:TextBox ID="tb_status_booking" runat="server" CssClass="form-control" Text='<%# Eval("status_booking") %>' Enabled="false" />
                                        </div>

                                        <label class="col-sm-2 control-label">หัวข้อการประชุม</label>
                                        <div class="col-sm-4">

                                            <asp:TextBox ID="tb_topic_booking" runat="server" CssClass="form-control" Text='<%# Eval("topic_booking") %>' Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">เหตุผลการใช้ห้องประชุม</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tb_result_use_name" runat="server" CssClass="form-control" Text='<%# Eval("result_use_name") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">จำนวนผู้เข้าประชุม</label>
                                        <div class="col-sm-4">

                                            <asp:TextBox ID="tb_count_people" runat="server" CssClass="form-control" Text='<%# Eval("count_people") %>' Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">

                                        <label class="col-sm-2 control-label">รายละเอียดเพิ่มเติม</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="tb_detail_booking" runat="server" Text='<%# Eval("detail_booking") %>' TextMode="MultiLine" Enabled="false" CssClass="form-control" Rows="3">
                                            </asp:TextBox>
                                            <%-- <asp:TextBox ID="TextBox10" runat="server" CssClass="form-control" Text='<%# Eval("pos_name_th") %>' Enabled="false" />--%>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">รูปภาพห้องประชุม</label>
                                        <%--<label>รูปภาพห้องประชุม</label>--%>
                                        <div class="col-sm-10">
                                            <asp:HyperLink runat="server" ID="btnViewFileRoomDetail" CssClass="" Target="_blank"><i class="fa fa-file-text-o"></i> ดูรายละเอียด</asp:HyperLink>
                                            <asp:TextBox ID="txt_AlertDetailPictureView" runat="server" CssClass="form-control" Text="ไม่มีรูปภาพ" Visible="false" Font-Bold="true">
                                            </asp:TextBox>

                                            <asp:TextBox ID="txtm0_conidx" Visible="false" runat="server" Text='<%# Eval("m0_conidx") %>' CssClass="form-control">
                                            </asp:TextBox>
                                        </div>

                                    </div>

                                    <hr />

                                    <asp:GridView ID="GvConferenceInRoom_Detail" runat="server" Visible="true"
                                        AutoGenerateColumns="false"
                                        CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                                        HeaderStyle-CssClass="success"
                                        AllowPaging="false"
                                        OnRowDataBound="gvRowDataBound">

                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                                        <EmptyDataTemplate>
                                            <div style="text-align: center; color: red"><b>-- ไม่พบข้อมูล --</b> </div>
                                        </EmptyDataTemplate>
                                        <Columns>
                                            <asp:TemplateField HeaderStyle-CssClass="text-center"
                                                ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="5%" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:RadioButton ID="rdoconference_detail" Enabled="false" runat="server" />
                                                        <asp:Label ID="lbl_m0_conidx" runat="server" Visible="false" Text='<%# Eval("m0_conidx") %>' />

                                                    </small>

                                                </ItemTemplate>
                                            </asp:TemplateField>



                                            <asp:TemplateField HeaderText="ภาพอุปกรณ์" ItemStyle-HorizontalAlign="left"
                                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:UpdatePanel ID="updatepiccon" runat="server" UpdateMode="Always">
                                                            <ContentTemplate>
                                                                <div style="padding-top: 5px;">
                                                                    <asp:HyperLink runat="server" ID="btnViewFileConference_Detail" CssClass="" Target="_blank"><i class="fa fa-file-text-o"></i> ดูรายละเอียด</asp:HyperLink>
                                                                    <asp:TextBox ID="txt_AlertDetailPicture_ConDetail" Enabled="false" runat="server" CssClass="form-control" Text="ไม่มีรูปภาพ" Visible="false" Font-Bold="true">
                                                                    </asp:TextBox>

                                                                    <%--  <asp:Image ID="image" runat="server"></asp:Image>--%>
                                                                </div>

                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ชื่ออุปกรณ์" ItemStyle-HorizontalAlign="left"
                                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <div style="padding-top: 5px;">
                                                            <asp:Label ID="lbl_conference_name" runat="server"
                                                                Text='<%# Eval("conference_name") %>'></asp:Label>
                                                        </div>
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="อุปกรณ์สำหรับ" ItemStyle-HorizontalAlign="left"
                                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <div style="padding-top: 5px;">


                                                            <div class="col-sm-3">
                                                                <asp:Label ID="lbl_pic_check" runat="server" Visible="false"
                                                                    Text='<%# Eval("pic_check") %>'></asp:Label>
                                                                <asp:CheckBox ID="chkpic_show" runat="server" CssClass="checkbox-inline" Text="ภาพ" Enabled="false" />

                                                            </div>

                                                            <div class="col-sm-3">
                                                                <asp:Label ID="lbl_sound_check" runat="server" Visible="false"
                                                                    Text='<%# Eval("sound_check") %>'></asp:Label>

                                                                <asp:CheckBox ID="chksound_show" runat="server" CssClass="checkbox-inline" Text="เสียง" Enabled="false" />


                                                            </div>

                                                        </div>
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="จำนวนไมค์" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <div style="padding-top: 5px;">
                                                            <asp:Label ID="lbl_qty_mic" runat="server"
                                                                Text='<%# Eval("qty_mic") %>'></asp:Label>
                                                        </div>
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="จำนวนผู้เข้าประชุมที่เหมาะสม" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <div style="padding-top: 5px;">
                                                            <asp:Label ID="lbl_people_desc" runat="server"
                                                                Text='<%# Eval("people_desc") %>'></asp:Label>
                                                        </div>
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>



                                        </Columns>
                                    </asp:GridView>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">อาหารว่าง</label>
                                        <div class="col-sm-4 control-label textleft">
                                            <asp:CheckBoxList ID="chkFoodDetail_RoomDetail" runat="server" CellPadding="5" CellSpacing="5" RepeatColumns="2" Enabled="false"
                                                Width="100%" Font-Bold="false" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right" />
                                            <p class="help-block"><font color="red">** สามารถเลือกอาหารว่างได้(ต้องจองห้องล่วงหน้า 3 วัน) **</font></p>

                                        </div>

                                        <label class="col-sm-2 control-label">หมายเหตุอาหารว่าง</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txt_fooddetail_room_RoomDetail" runat="server" placeHolder="รายละเอียดอาหารว่างที่เพิ่มเติม" Text='<%# Eval("detail_foodbooking") %>' Enabled="false" TextMode="MultiLine" CssClass="form-control" Rows="3">
                                            </asp:TextBox>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </ItemTemplate>

                    <EditItemTemplate>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">แก้ไขรายละเอียดรายการจองห้องประชุม</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">ประเภทการจอง</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tb_u0_document_idx" runat="server" Visible="false" CssClass="form-control" Text='<%# Eval("u0_document_idx") %>' Enabled="false" />
                                            <asp:TextBox ID="tb_type_booking_name" runat="server" CssClass="form-control" Text='<%# Eval("type_booking_name") %>' Enabled="false" />
                                            <asp:TextBox ID="tb_type_booking_idx" runat="server" Visible="false" Text='<%# Eval("type_booking_idx") %>' CssClass="form-control" />
                                        </div>

                                        <label class="col-sm-2 control-label">ผู้ทำรายการ</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpName" runat="server" CssClass="form-control" Text='<%# Eval("emp_name_th") %>' Enabled="false" />
                                            <asp:TextBox ID="tbActorCempIDX" runat="server" Visible="false" Text='<%# Eval("cemp_idx") %>' CssClass="form-control" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">องค์กร</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpOrg" runat="server" CssClass="form-control" Text='<%# Eval("org_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ฝ่าย</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpDept" runat="server" CssClass="form-control" Text='<%# Eval("dept_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">แผนก</label>
                                        <div class="col-sm-4">

                                            <asp:TextBox ID="tbEmpSec" runat="server" CssClass="form-control" Text='<%# Eval("sec_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ตำแหน่ง</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpPos" runat="server" CssClass="form-control" Text='<%# Eval("pos_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">สถานที่</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tb_place_idx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("place_idx") %>' Enabled="true" />
                                            <asp:TextBox ID="tb_place_name" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("place_name") %>' Enabled="true" />

                                            <asp:DropDownList ID="ddlPlace_edit" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" Enabled="false">
                                            </asp:DropDownList>
                                        </div>
                                        <label class="col-sm-2 control-label">ห้องประชุม</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tb_m0_room_idx" runat="server" Visible="false" CssClass="form-control" Text='<%# Eval("m0_room_idx") %>' Enabled="true" />

                                            <asp:TextBox ID="tb_room_name_th" runat="server" Visible="false" CssClass="form-control" Text='<%# Eval("room_name_th") %>' Enabled="false" />

                                            <asp:DropDownList ID="ddlRoom_edit" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                <asp:ListItem Value="0">--- เลือกห้องประชุม ---</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">ตั้งแต่วันที่</label>
                                        <div class="col-sm-4">

                                            <asp:TextBox ID="tb_date_start" runat="server" CssClass="form-control" Text='<%# Eval("date_start") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">เวลา</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tb_time_start" runat="server" CssClass="form-control" Text='<%# Eval("time_start") %>' Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">ถึงวันที่</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tb_date_end" runat="server" CssClass="form-control" Text='<%# Eval("date_end") %>' Enabled="false" />

                                        </div>
                                        <label class="col-sm-2 control-label">เวลา</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tb_time_end" runat="server" CssClass="form-control" Text='<%# Eval("time_end") %>' Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">สถานะ</label>
                                        <div class="col-sm-4">

                                            <asp:TextBox ID="tb_status_booking" runat="server" CssClass="form-control" Text='<%# Eval("status_booking") %>' Enabled="false" />
                                        </div>

                                        <label class="col-sm-2 control-label">หัวข้อการประชุม</label>
                                        <div class="col-sm-4">

                                            <asp:TextBox ID="tb_topic_booking" runat="server" CssClass="form-control" Text='<%# Eval("topic_booking") %>' Enabled="true" />
                                        </div>
                                    </div>

                                    <div class="form-group">

                                        <label class="col-sm-2 control-label">เหตุผลการใช้ห้องประชุม</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tb_result_use_idx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("result_use_idx") %>' Enabled="true" />
                                            <asp:TextBox ID="tb_result_use_name" runat="server" Visible="false" CssClass="form-control" Text='<%# Eval("result_use_name") %>' Enabled="true" />

                                            <asp:DropDownList ID="ddlResultUse_edit" runat="server" CssClass="form-control">
                                            </asp:DropDownList>

                                        </div>

                                        <label class="col-sm-2 control-label">จำนวนผู้เข้าประชุม</label>
                                        <div class="col-sm-4">

                                            <asp:TextBox ID="tb_count_people" runat="server" CssClass="form-control" Text='<%# Eval("count_people") %>' Enabled="true" />
                                        </div>

                                    </div>

                                    <div class="form-group">

                                        <label class="col-sm-2 control-label">รายละเอียดเพิ่มเติม</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="tb_detail_booking" runat="server" Text='<%# Eval("detail_booking") %>' TextMode="MultiLine" Enabled="true" CssClass="form-control" Rows="3">
                                            </asp:TextBox>
                                            <%-- <asp:TextBox ID="TextBox10" runat="server" CssClass="form-control" Text='<%# Eval("pos_name_th") %>' Enabled="false" />--%>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">รูปภาพห้องประชุม</label>
                                        <%--<label>รูปภาพห้องประชุม</label>--%>
                                        <div class="col-sm-10">
                                            <asp:HyperLink runat="server" ID="btnViewFileRoomDetail" CssClass="" Target="_blank"><i class="fa fa-file-text-o"></i> ดูรายละเอียด</asp:HyperLink>
                                            <asp:TextBox ID="txt_AlertDetailPictureView" runat="server" CssClass="form-control" Text="ไม่มีรูปภาพ" Visible="false" Font-Bold="true">
                                            </asp:TextBox>
                                        </div>
                                        <asp:TextBox ID="txtm0_conidx_edit" Visible="false" runat="server" Text='<%# Eval("m0_conidx") %>' CssClass="form-control">
                                        </asp:TextBox>
                                    </div>

                                    <hr />

                                    <asp:GridView ID="GvConferenceInRoom_DetailEdit" runat="server" Visible="true"
                                        AutoGenerateColumns="false"
                                        CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                                        HeaderStyle-CssClass="success"
                                        AllowPaging="false"
                                        OnRowDataBound="gvRowDataBound">

                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                                        <EmptyDataTemplate>
                                            <div style="text-align: center; color: red"><b>-- ไม่พบข้อมูล --</b> </div>
                                        </EmptyDataTemplate>
                                        <Columns>
                                            <asp:TemplateField HeaderStyle-CssClass="text-center"
                                                ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="5%" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:RadioButton ID="rdoconference_edit" OnCheckedChanged="ddlSelectedIndexChanged" AutoPostBack="true" GroupName="rdoconference_edit" runat="server" />
                                                        <asp:Label ID="lbl_m0_conidx" runat="server" Visible="false" Text='<%# Eval("m0_conidx") %>' />

                                                    </small>

                                                </ItemTemplate>
                                            </asp:TemplateField>



                                            <asp:TemplateField HeaderText="ภาพอุปกรณ์" ItemStyle-HorizontalAlign="left"
                                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:UpdatePanel ID="updatepiccon" runat="server" UpdateMode="Always">
                                                            <ContentTemplate>
                                                                <div style="padding-top: 5px;">
                                                                    <asp:HyperLink runat="server" ID="btnViewFileConference_edit" CssClass="" Target="_blank"><i class="fa fa-file-text-o"></i> ดูรายละเอียด</asp:HyperLink>
                                                                    <asp:TextBox ID="txt_AlertDetailPicture_Conedit" Enabled="false" runat="server" CssClass="form-control" Text="ไม่มีรูปภาพ" Visible="false" Font-Bold="true">
                                                                    </asp:TextBox>

                                                                </div>

                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ชื่ออุปกรณ์" ItemStyle-HorizontalAlign="left"
                                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <div style="padding-top: 5px;">
                                                            <asp:Label ID="lbl_conference_name" runat="server"
                                                                Text='<%# Eval("conference_name") %>'></asp:Label>
                                                        </div>
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="อุปกรณ์สำหรับ" ItemStyle-HorizontalAlign="left"
                                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <div style="padding-top: 5px;">


                                                            <div class="col-sm-3">
                                                                <asp:Label ID="lbl_pic_check" runat="server" Visible="false"
                                                                    Text='<%# Eval("pic_check") %>'></asp:Label>
                                                                <asp:CheckBox ID="chkpic_show" runat="server" CssClass="checkbox-inline" Text="ภาพ" Enabled="false" />

                                                            </div>

                                                            <div class="col-sm-3">
                                                                <asp:Label ID="lbl_sound_check" runat="server" Visible="false"
                                                                    Text='<%# Eval("sound_check") %>'></asp:Label>

                                                                <asp:CheckBox ID="chksound_show" runat="server" CssClass="checkbox-inline" Text="เสียง" Enabled="false" />


                                                            </div>

                                                        </div>
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="จำนวนไมค์" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <div style="padding-top: 5px;">
                                                            <asp:Label ID="lbl_qty_mic" runat="server"
                                                                Text='<%# Eval("qty_mic") %>'></asp:Label>
                                                        </div>
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="จำนวนผู้เข้าประชุมที่เหมาะสม" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <div style="padding-top: 5px;">
                                                            <asp:Label ID="lbl_people_desc" runat="server"
                                                                Text='<%# Eval("people_desc") %>'></asp:Label>
                                                        </div>
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>



                                        </Columns>
                                    </asp:GridView>


                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">อาหารว่าง</label>
                                        <div class="col-sm-4 control-label textleft">
                                            <asp:CheckBoxList ID="chkFoodDetail_RoomDetail" runat="server" CellPadding="5" CellSpacing="5" RepeatColumns="2" Enabled="false"
                                                Width="100%" Font-Bold="false" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right" />
                                            <p class="help-block"><font color="red">** สามารถเลือกอาหารว่างได้(ต้องจองห้องล่วงหน้า 3 วัน) **</font></p>

                                        </div>

                                        <label class="col-sm-2 control-label">หมายเหตุอาหารว่าง</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txt_fooddetail_room_RoomDetail" runat="server" placeHolder="รายละเอียดอาหารว่างที่เพิ่มเติม" Text='<%# Eval("detail_foodbooking") %>' Enabled="false" TextMode="MultiLine" CssClass="form-control" Rows="3">
                                            </asp:TextBox>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-10">
                                            <asp:LinkButton ID="btnBackDetailBeforeEdit" CssClass="btn btn-default" data-toggle="tooltip" title="" runat="server"
                                                CommandName="cmdBackDetailBeforeEdit" OnCommand="btnCommand"><i class="fa fa-close"></i> ยกเลิกแก้ไข</asp:LinkButton>

                                            <asp:LinkButton ID="btnSaveEditDetail" CssClass="btn btn-success" runat="server" data-original-title="บันทึกการเปลี่ยนแปลง" data-toggle="tooltip"
                                                Text="บันทึกการเปลี่ยนแปลง" OnCommand="btnCommand" CommandName="cmdSaveEdit" ValidationGroup="SaveEdit" Visible="false"></asp:LinkButton>
                                        </div>
                                    </div>



                                    <!-- Gridview Search Detail Room -->
                                    <asp:UpdatePanel ID="Update_DetailRoomSearch_Edit" runat="server" Visible="false">
                                        <ContentTemplate>

                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">รายละเอียดห้องที่เปลี่ยนแปลง</h3>
                                                </div>
                                                <div class="panel-body">
                                                    <asp:GridView ID="GvDetailRoomSearch_Edit" runat="server"
                                                        AutoGenerateColumns="False"
                                                        CssClass="table table-striped table-bordered table-hover table-responsive"
                                                        OnPageIndexChanging="gvPageIndexChanging"
                                                        OnRowDataBound="gvRowDataBound"
                                                        AllowPaging="True" PageSize="10">
                                                        <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                                                        <RowStyle Font-Size="Small" />
                                                        <PagerStyle CssClass="pageCustom" />
                                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                                        <EmptyDataTemplate>
                                                            <div style="text-align: center">ไม่พบข้อมูล</div>
                                                        </EmptyDataTemplate>
                                                        <Columns>

                                                            <asp:TemplateField HeaderText="รูปภาพห้องประชุม" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                <ItemTemplate>
                                                                    <small>
                                                                        <asp:HyperLink runat="server" ID="btnViewFileRoomEdit" CssClass="pull-letf" data-toggle="tooltip"
                                                                            title="view" data-original-title="" Target="_blank">
                                                                                <i class="fa fa-file-text-o"></i> ดูรายละเอียด</asp:HyperLink>
                                                                    </small>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="ชื่อห้อง(TH)" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_m0_room_idx_edit" runat="server" Visible="false" Text='<%# Eval("m0_room_idx") %>'></asp:Label>
                                                                    <asp:Label ID="lbl_room_name_th" runat="server" Text='<%# Eval("room_name_th") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="ชื่อห้อง(EN)" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                <ItemTemplate>

                                                                    <asp:Label ID="lbl_room_name_en_edit" runat="server" Text='<%# Eval("room_name_en") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="รายละเอียดห้อง" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_room_detail_edit" runat="server" Text='<%# Eval("room_detail") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_room_detail_status_edit" runat="server" Visible="true"></asp:Label>
                                                                    <%--<asp:Label ID="lbl_room_detail_notbusy_edit" runat="server" Visible="false" Text="ไม่ว่าง"></asp:Label>
                                                                    <asp:Label ID="lbl_room_detail_busy_edit" runat="server" Visible="false" Text="ว่าง"></asp:Label>--%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <%--<label class="col-sm-10 control-label"></label>--%>
                                                <div class="col-sm-12">
                                                    <div class="pull-right">
                                                        <asp:LinkButton ID="btn_SaveEdit" CssClass="btn btn-success" runat="server" data-original-title="บันทึกการเปลี่ยนแปลง" data-toggle="tooltip"
                                                            Text="บันทึกการเปลี่ยนแปลง" OnCommand="btnCommand" CommandName="cmdSaveEdit" ValidationGroup="SaveEdit"></asp:LinkButton>

                                                        <asp:LinkButton ID="btn_CancelEdit" CssClass="btn btn-danger" runat="server" data-original-title="ยกเลิก" data-toggle="tooltip" Text="ยกเลิก" OnCommand="btnCommand" CommandName="cmdCancelToDetail"></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <!-- Gridview Search Detail Room cmdBackDetailBeforeEdit -->


                                </div>

                            </div>

                        </div>
                    </EditItemTemplate>

                </asp:FormView>
                <!-- Fv View Detail Room Booking -->

                <!-- Button Edit Detail RoomBooking -->
                <asp:UpdatePanel ID="Update_EditRoomBookingButton" runat="server" Visible="true">
                    <ContentTemplate>
                        <div class="form-group">
                            <%--    <asp:LinkButton ID="btnChangeRoomDetail" CssClass="btn-sm btn-warning" target="" runat="server" CommandName="cmdChangeRoomDetail" OnCommand="btnCommand"
                                    CommandArgument='<%# Eval("u0_document_idx")+ ";" + Eval("m0_room_idx")+ ";" + Eval("room_name_th")%>' data-toggle="tooltip" title="เปลี่ยนแปลง"><i class="fa fa-pencil-square-o"></i></asp:LinkButton>--%>
                            <asp:LinkButton ID="btnEditDetailBooking" CssClass="btn btn-warning" data-toggle="tooltip" title="" runat="server"
                                CommandName="cmdEditDetailBooking" OnCommand="btnCommand"><i class="fa fa-pencil-square-o"></i> แก้ไขการจองห้องประชุม</asp:LinkButton>

                            <asp:LinkButton ID="btnCancelBookingRoom" CssClass="btn btn-danger" data-toggle="tooltip" title="" runat="server"
                                CommandName="cmdCancelBookingRoom" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-close"></i> ยกเลิกการจองห้องประชุม</asp:LinkButton>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- Button Edit Detail RoomBooking -->

                <!-- Gridview Search Edit Detail Room -->
                <asp:UpdatePanel ID="Update_EditRoomDetail" runat="server" Visible="false">
                    <ContentTemplate>

                        <%--<div id="gvEmployeeList_scroll" style="overflow-x: scroll; width: 100%" runat="server">--%>
                        <asp:GridView ID="GridView1" runat="server"
                            AutoGenerateColumns="False"
                            CssClass="table table-striped table-bordered table-hover table-responsive"
                            OnPageIndexChanging="gvPageIndexChanging"
                            OnRowDataBound="gvRowDataBound"
                            AllowPaging="True" PageSize="10">
                            <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                            <RowStyle Font-Size="Small" />
                            <PagerStyle CssClass="pageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                            <EmptyDataTemplate>
                                <div style="text-align: center">ไม่พบข้อมูล</div>
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:UpdatePanel ID="Updatechecked" runat="server">
                                            <ContentTemplate>

                                                <%--  <asp:LinkButton ID="btn_AddRoomDetail" CssClass="btn btn-success btn-sm" runat="server" data-toggle="tooltip"
                                                    title="Insert" CommandName="cmdAddRoomDetail" OnCommand="btnCommand"
                                                    CommandArgument='<%# Eval("m0_room_idx") %>'><i class="fa fa-plus-square"></i>
                                                </asp:LinkButton>--%>

                                                <asp:LinkButton ID="btn_AddRoomDetailEdit" CssClass="btn-sm btn-success" target="" runat="server" CommandName="cmdAddRoomDetailEdit" OnCommand="btnCommand"
                                                    CommandArgument='<%# Eval("m0_room_idx")+ ";" + Eval("room_name_th")%>' data-toggle="tooltip" title="Insert"><i class="fa fa-plus-square"></i></asp:LinkButton>

                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="btn_AddRoomDetailEdit" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </ItemTemplate>

                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ชื่อห้อง(TH)">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_m0_room_idx" runat="server" Visible="false" Text='<%# Eval("m0_room_idx") %>'></asp:Label>
                                        <asp:Label ID="lbl_room_name_th" runat="server" Text='<%# Eval("room_name_th") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ชื่อห้อง(EN)">
                                    <ItemTemplate>

                                        <asp:Label ID="lbl_room_name_en" runat="server" Text='<%# Eval("room_name_en") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="รายละเอียดห้อง">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_room_detail" runat="server" Text='<%# Eval("room_detail") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="สถานะ">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_room_detail_notbusy" runat="server" Visible="false" Text="ไม่ว่าง"></asp:Label>
                                        <asp:Label ID="lbl_room_detail_busy" runat="server" Visible="false" Text="ว่าง"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>
                        <%--</div>--%>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- Gridview Search Edit Detail Room -->

                <!-- Log Detail Room Booking -->
                <div class="panel panel-info" id="div_LogViewDetail" runat="server" visible="false">
                    <div class="panel-heading">ประวัติการดำเนินการ</div>
                    <table class="table table-striped f-s-12 table-empshift-responsive">
                        <asp:Repeater ID="rptLogRoomBooking" runat="server">
                            <HeaderTemplate>
                                <tr>
                                    <th>วัน / เวลา</th>
                                    <th>ผู้ดำเนินการ</th>
                                    <th>ดำเนินการ</th>
                                    <%--<th>ผลการดำเนินการ</th>--%>
                                    <th>เหตุผล</th>
                                </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td data-th="วัน / เวลา"><%# Eval("create_date") %> <%# Eval("time_create") %></td>
                                    <td data-th="ผู้ดำเนินการ"><%# Eval("emp_name_th") %> ( <%# Eval("current_artor") %> )</td>
                                    <td data-th="ดำเนินการ"><%# Eval("current_decision") %></td>
                                    <td data-th="เหตุผล"><%# Eval("comment") %></td>
                                    <%--<td data-th="ผลการดำเนินการ"><%# Eval("current_status_name") %></td>--%>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                    <div class="m-t-10 m-b-10"></div>
                </div>
                <!-- Log Detail Room Booking -->
            </div>

        </asp:View>
        <!--View Detail-->

        <!--View Create-->
        <asp:View ID="docCreate" runat="server">
            <div class="col-md-12">
                <!-- Form Detail Employee Create -->
                <asp:FormView ID="fvEmpDetail" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <ItemTemplate>

                        <asp:HiddenField ID="hfEmpOrgIDX" runat="server" Value='<%# Eval("org_idx") %>' />
                        <asp:HiddenField ID="hfEmpRdeptIDX" runat="server" Value='<%# Eval("rdept_idx") %>' />
                        <asp:HiddenField ID="hfEmpRsecIDX" runat="server" Value='<%# Eval("rsec_idx") %>' />
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายละเอียดผู้ใช้งาน</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">รหัสพนักงาน</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpCode" runat="server" CssClass="form-control" Text='<%# Eval("emp_code") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ชื่อ - นามสกุล</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpName" runat="server" CssClass="form-control" Text='<%# Eval("emp_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">องค์กร</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpOrg" runat="server" CssClass="form-control" Text='<%# Eval("org_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ฝ่าย</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpDept" runat="server" CssClass="form-control" Text='<%# Eval("dept_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">แผนก</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpSec" runat="server" CssClass="form-control" Text='<%# Eval("sec_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ตำแหน่ง</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpPos" runat="server" CssClass="form-control" Text='<%# Eval("pos_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">เบอร์ติดต่อ</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" Text='<%# Eval("emp_mobile_no") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">Email</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="TextBox2" runat="server" CssClass="form-control" Text='<%# Eval("emp_email") %>' Enabled="false" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </ItemTemplate>
                </asp:FormView>
                <!-- Form Detail Employee Create -->

                <!-- Form Create -->
                <asp:FormView ID="FvInsertRoomBooking" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <InsertItemTemplate>

                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">ค้นหาห้องประชุม</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">เลือกประเภทการจอง</label>
                                        <div class="col-sm-10">
                                            <asp:DropDownList ID="ddlTypeRecive" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0" Text="ไม่เร่งด่วน" />
                                                <asp:ListItem Value="1" Text="เร่งด่วน" />
                                            </asp:DropDownList>

                                        </div>

                                    </div>

                                    <asp:UpdatePanel ID="updateRoomInsert" runat="server">
                                        <ContentTemplate>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">สถานที่</label>
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddlPlace" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="Re_ddlPlace"
                                                        runat="server"
                                                        InitialValue="0"
                                                        ControlToValidate="ddlPlace" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณาเลือกสถานที่"
                                                        ValidationGroup="SaveIndex" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlPlace" Width="200" PopupPosition="BottomLeft" />
                                                </div>
                                                <label class="col-sm-2 control-label">ห้องประชุม</label>
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddlRoom" runat="server" CssClass="form-control">
                                                        <asp:ListItem Value="0">--- เลือกห้องประชุม ---</asp:ListItem>
                                                    </asp:DropDownList>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">ตั้งแต่วันที่</label>
                                                <div class="col-sm-4">
                                                    <div class="input-group date">
                                                        <asp:HiddenField ID="txtSearchFromHidden" runat="server" />
                                                        <asp:TextBox ID="txtDateStart_create" runat="server" placeholder="วันที่เริ่มต้นจอง..."
                                                            CssClass="form-control datetimepicker-from cursor-pointer" />
                                                        <span class="input-group-addon show-from-onclick">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <asp:RequiredFieldValidator ID="Re_txtDateStart_create"
                                                            runat="server"
                                                            ControlToValidate="txtDateStart_create" Display="None" SetFocusOnError="true"
                                                            ErrorMessage="*กรุณาเลือกวันที่เริ่มต้นจอง"
                                                            ValidationGroup="SaveIndex" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txtDateStart_create" Width="200" PopupPosition="BottomLeft" />

                                                    </div>
                                                </div>
                                                <label class="col-sm-2 control-label">เวลา</label>
                                                <div class="col-sm-4">
                                                    <div class="input-group time">
                                                        <asp:HiddenField ID="HiddenTimeStartCreate" runat="server" />
                                                        <asp:TextBox ID="txt_timestart_create" placeholder="เวลา ..." runat="server" CssClass="form-control clockpicker cursor-pointer" value="" />
                                                        <span class="input-group-addon show-time-onclick cursor-pointer">
                                                            <span class="glyphicon glyphicon-time"></span>
                                                        </span>
                                                        <asp:RequiredFieldValidator ID="Re_txt_timestart_create"
                                                            runat="server"
                                                            ControlToValidate="txt_timestart_create" Display="None" SetFocusOnError="true"
                                                            ErrorMessage="*กรุณาเลือกเวลาที่เริ่มต้นจอง"
                                                            ValidationGroup="SaveIndex" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_timestart_create" Width="200" PopupPosition="BottomLeft" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">ถึงวันที่</label>
                                                <div class="col-sm-4">
                                                    <div class="input-group date">
                                                        <asp:HiddenField ID="txtSearchToHidden" runat="server" />
                                                        <asp:TextBox ID="txtDateEnd_create" runat="server" placeholder="วันที่สิ้นสุดการจอง..."
                                                            CssClass="form-control datetimepicker-to cursor-pointer" />
                                                        <span class="input-group-addon show-to-onclick">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <asp:RequiredFieldValidator ID="Re_txtDateEnd_create"
                                                            runat="server"
                                                            ControlToValidate="txtDateEnd_create" Display="None" SetFocusOnError="true"
                                                            ErrorMessage="*กรุณาเลือกวันที่สิ้นสุดการจอง"
                                                            ValidationGroup="SaveIndex" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txtDateEnd_create" Width="200" PopupPosition="BottomLeft" />
                                                    </div>
                                                </div>
                                                <label class="col-sm-2 control-label">เวลา</label>
                                                <div class="col-sm-4">
                                                    <div class="input-group time">
                                                        <asp:HiddenField ID="HiddenTimeEndCreate" runat="server" />
                                                        <asp:TextBox ID="txt_timeend_create" placeholder="เวลา ..." runat="server" CssClass="form-control clockpickerto cursor-pointer" value="" />
                                                        <span class="input-group-addon show-timeto-onclick cursor-pointer">
                                                            <span class="glyphicon glyphicon-time"></span>
                                                        </span>
                                                        <asp:RequiredFieldValidator ID="Re_txt_timeend_create"
                                                            runat="server"
                                                            ControlToValidate="txt_timeend_create" Display="None" SetFocusOnError="true"
                                                            ErrorMessage="*กรุณาเลือกเวลาที่สิ้นสุดการจอง"
                                                            ValidationGroup="SaveIndex" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_timeend_create" Width="200" PopupPosition="BottomLeft" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label"></label>
                                                <div class="col-sm-4">
                                                    <asp:LinkButton ID="btnSearchRoomCreate" runat="server"
                                                        CssClass="btn btn-primary" OnCommand="btnCommand" CommandName="cmdSearchRoomCreate" data-original-title="ค้นหา" data-toggle="tooltip" ValidationGroup="SaveIndex"><span class="glyphicon glyphicon-search"></span> ค้นหาห้องว่าง</asp:LinkButton>

                                                </div>
                                                <label class="col-sm-6 control-label"></label>
                                            </div>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>


                                </div>
                            </div>
                        </div>

                    </InsertItemTemplate>
                </asp:FormView>
                <!-- Form Create -->

                <!-- Gridview Search Detail Room -->
                <asp:UpdatePanel ID="Update_DetailRoomSearch" runat="server" Visible="false">
                    <ContentTemplate>

                        <%--<div id="gvEmployeeList_scroll" style="overflow-x: scroll; width: 100%" runat="server">--%>
                        <asp:GridView ID="GvDetailRoomSearch" runat="server"
                            AutoGenerateColumns="False"
                            CssClass="table table-striped table-bordered table-hover table-responsive"
                            OnPageIndexChanging="gvPageIndexChanging"
                            OnRowDataBound="gvRowDataBound"
                            AllowPaging="True" PageSize="10">
                            <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                            <RowStyle Font-Size="Small" />
                            <PagerStyle CssClass="pageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                            <EmptyDataTemplate>
                                <div style="text-align: center">ไม่พบข้อมูล</div>
                            </EmptyDataTemplate>
                            <Columns>

                                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:UpdatePanel ID="Updatechecked" runat="server">
                                            <ContentTemplate>
                                                <asp:LinkButton ID="btn_AddRoomDetail" CssClass="btn-sm btn-success" target="" runat="server" CommandName="cmdAddRoomDetail" OnCommand="btnCommand"
                                                    CommandArgument='<%# Eval("m0_room_idx")+ ";" + Eval("room_name_th")%>' data-toggle="tooltip" title="Insert"><i class="fa fa-plus-square"></i></asp:LinkButton>

                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="btn_AddRoomDetail" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </ItemTemplate>

                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="รูปภาพห้องประชุม" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>

                                            <asp:HyperLink runat="server" ID="btnViewFileRoom" CssClass="pull-letf" data-toggle="tooltip" title="view" data-original-title="" Target="_blank">
                                                    <i class="fa fa-file-text-o"></i> ดูรายละเอียด</asp:HyperLink>

                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ชื่อห้อง(TH)" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_m0_room_idx" runat="server" Visible="false" Text='<%# Eval("m0_room_idx") %>'></asp:Label>
                                        <asp:Label ID="lbl_room_name_th" runat="server" Text='<%# Eval("room_name_th") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ชื่อห้อง(EN)" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>

                                        <asp:Label ID="lbl_room_name_en" runat="server" Text='<%# Eval("room_name_en") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="รายละเอียดห้อง" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_room_detail" runat="server" Text='<%# Eval("room_detail") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>



                                <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_room_detail_statusroom" runat="server" Visible="true"></asp:Label>
                                        <%--<asp:Label ID="lbl_room_detail_notbusy" runat="server" Visible="false" Text="ไม่ว่าง"></asp:Label>--%>
                                        <%--<asp:Label ID="lbl_room_detail_busy" runat="server" Visible="false" Text="ว่าง"></asp:Label>--%>
                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>
                        <%--</div>--%>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- Gridview Search Detail Room -->

                <!-- Back To Search Room Create -->
                <asp:UpdatePanel ID="Update_BackToSearch" runat="server" Visible="false">
                    <ContentTemplate>
                        <div class="form-group">
                            <asp:LinkButton ID="btnCancelToSearch" CssClass="btn btn-danger" data-toggle="tooltip" title="" runat="server"
                                CommandName="cmdCancelToSerach" OnCommand="btnCommand">< ย้อนกลับไปค้นหา</asp:LinkButton>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- Back To Search Room Create -->

                <!-- Detail Create Room -->
                <asp:UpdatePanel ID="UpdatePanel_InsertDetailRoom" runat="server" Visible="false">
                    <ContentTemplate>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายละเอียดการสร้างรายการจองห้องประชุม</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">เลือกประเภทการจอง</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txt_StatusIDX_insert" runat="server" CssClass="form-control" Enabled="false" Visible="false">
                                            </asp:TextBox>
                                            <asp:TextBox ID="txt_StatusText_insert" runat="server" CssClass="form-control" Enabled="false">
                                            </asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">สถานที่</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txt_PlaceIDX_insert" runat="server" CssClass="form-control" Enabled="false" Visible="false">
                                            </asp:TextBox>
                                            <asp:TextBox ID="txt_PlaceText_insert" runat="server" CssClass="form-control" Enabled="false">
                                            </asp:TextBox>

                                        </div>
                                        <label class="col-sm-2 control-label">ห้องประชุม</label>
                                        <div class="col-sm-4">

                                            <asp:TextBox ID="txt_RoomIDX_insert" runat="server" CssClass="form-control" Enabled="false" Visible="false">
                                            </asp:TextBox>
                                            <asp:TextBox ID="txt_RoomText_insert" runat="server" CssClass="form-control" Enabled="false">
                                            </asp:TextBox>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">ตั้งแต่วันที่</label>
                                        <div class="col-sm-4">
                                            <div class="input-group date">

                                                <asp:TextBox ID="txtDateStart_insert" runat="server" placeholder="วันที่เริ่มต้นจอง..."
                                                    CssClass="form-control datetimepicker-from cursor-pointer" Enabled="false" />
                                                <span class="input-group-addon show-from-onclick">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>

                                            </div>
                                        </div>
                                        <label class="col-sm-2 control-label">เวลา</label>
                                        <div class="col-sm-4">
                                            <div class="input-group clockpicker">

                                                <asp:TextBox ID="txt_timestart_insert" placeholder="เวลา ..." runat="server" CssClass="form-control clockpicker cursor-pointer" value="" Enabled="false" />
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-time"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">ถึงวันที่</label>
                                        <div class="col-sm-4">
                                            <div class="input-group date">

                                                <asp:TextBox ID="txtDateEnd_insert" runat="server" placeholder="วันที่สิ้นสุดการจอง..."
                                                    CssClass="form-control datetimepicker-to cursor-pointer" Enabled="false" />
                                                <span class="input-group-addon show-to-onclick">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>

                                            </div>
                                        </div>
                                        <label class="col-sm-2 control-label">เวลา</label>
                                        <div class="col-sm-4">
                                            <div class="input-group clockpicker">

                                                <asp:TextBox ID="txt_timeend_insert" placeholder="เวลา ..." runat="server" CssClass="form-control clockpicker cursor-pointer" value="" Enabled="false" />
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-time"></span>
                                                </span>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">

                                        <label class="col-sm-2 control-label">สถานะห้องประชุม</label>
                                        <div class="col-sm-4">

                                            <asp:TextBox ID="txt_StatusRoomBooking" runat="server" CssClass="form-control" Enabled="false">
                                            </asp:TextBox>

                                        </div>

                                        <label class="col-sm-2 control-label">หัวข้อการประชุม</label>
                                        <div class="col-sm-4">

                                            <asp:TextBox ID="txt_TopicName" runat="server" placeHolder="กรอกหัวข้อการประชุม" CssClass="form-control">
                                            </asp:TextBox>

                                            <asp:RequiredFieldValidator ID="Re_txt_TopicName"
                                                runat="server"
                                                ControlToValidate="txt_TopicName" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกหัวข้อการประชุม"
                                                ValidationGroup="SaveCreate" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_TopicName" Width="200" PopupPosition="BottomLeft" />

                                        </div>
                                    </div>

                                    <div class="form-group">

                                        <label class="col-sm-2 control-label">เหตุผลการใช้ห้องประชุม</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlResultUse" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="Re_ddlResultUse"
                                                runat="server"
                                                InitialValue="0"
                                                ControlToValidate="ddlResultUse" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกเหตุผลการใช้ห้องประชุม"
                                                ValidationGroup="SaveCreate" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8333" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlResultUse" Width="200" PopupPosition="BottomLeft" />

                                        </div>

                                        <label class="col-sm-2 control-label">จำนวนผู้เข้าประชุม</label>
                                        <div class="col-sm-4">

                                            <asp:TextBox ID="txt_CountPeople" runat="server" placeHolder="กรอกจำนวนผู้เข้าประชุม" MaxLength="2" CssClass="form-control">
                                            </asp:TextBox>

                                            <asp:RequiredFieldValidator ID="Requiredtxt_CountPeople" ValidationGroup="SaveCreate"
                                                runat="server" Display="None"
                                                ControlToValidate="txt_CountPeople"
                                                ErrorMessage="กรุณากรอกจำนวนผู้เข้าประชุม"
                                                ValidationExpression="กรุณากรอกจำนวนผู้เข้าประชุม" />

                                            <asp:RegularExpressionValidator ID="Requiredtxt_CountPeople1" runat="server" ValidationGroup="SaveCreate" Display="None"
                                                ErrorMessage="กรุณากรอกเฉพาะตัวเลข"
                                                ControlToValidate="txt_CountPeople"
                                                ValidationExpression="^\d+" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender107" runat="Server" HighlightCssClass="validatorCalloutHighlight" Width="200" PopupPosition="BottomLeft" TargetControlID="Requiredtxt_CountPeople" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender117" runat="Server" HighlightCssClass="validatorCalloutHighlight" Width="200" PopupPosition="BottomLeft" TargetControlID="Requiredtxt_CountPeople1" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">รายละเอียดเพิ่มเติม</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txt_CommentCreate" runat="server" placeHolder="กรอกรายละเอียดเพิ่มเติม" TextMode="MultiLine" CssClass="form-control" Rows="3">
                                            </asp:TextBox>
                                            <p class="help-block"><font color="red">**กรุณางดการใส่อักขระพิเศษ เช่น <  > ' " [ ] </font></p>
                                        </div>
                                    </div>

                                    <%-- <div class="col-md-12">--%>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">รูปภาพห้องประชุม</label>
                                        <%--<label>รูปภาพห้องประชุม</label>--%>
                                        <div class="col-sm-10">
                                            <asp:HyperLink runat="server" ID="btnViewFileRoomDetailCreate" CssClass="" Target="_blank"><i class="fa fa-file-text-o"></i> ดูรายละเอียด</asp:HyperLink>
                                            <asp:TextBox ID="txt_AlertDetailPicture" runat="server" CssClass="form-control" Text="ไม่มีรูปภาพ" Visible="false" Font-Bold="true">
                                            </asp:TextBox>
                                        </div>

                                    </div>
                                    <%-- </div>--%>

                                    <hr />


                                    <asp:GridView ID="GvConferenceInRoom" runat="server" Visible="true"
                                        AutoGenerateColumns="false"
                                        CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                                        HeaderStyle-CssClass="success"
                                        AllowPaging="false"
                                        OnRowDataBound="gvRowDataBound">

                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                                        <EmptyDataTemplate>
                                            <div style="text-align: center; color: red"><b>-- ไม่พบข้อมูล --</b> </div>
                                        </EmptyDataTemplate>
                                        <Columns>
                                            <asp:TemplateField HeaderStyle-CssClass="text-center"
                                                ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="5%" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:RadioButton ID="rdoconference" OnCheckedChanged="ddlSelectedIndexChanged" AutoPostBack="true" GroupName="rdoconference" runat="server" />
                                                        <asp:Label ID="lbl_m0_conidx" runat="server" Visible="false" Text='<%# Eval("m0_conidx") %>' />

                                                    </small>

                                                </ItemTemplate>
                                            </asp:TemplateField>



                                            <asp:TemplateField HeaderText="ภาพอุปกรณ์" ItemStyle-HorizontalAlign="left"
                                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:UpdatePanel ID="updatepiccon" runat="server" UpdateMode="Always">
                                                            <ContentTemplate>
                                                                <div style="padding-top: 5px;">
                                                                    <asp:HyperLink runat="server" ID="btnViewFileConference" CssClass="" Target="_blank"><i class="fa fa-file-text-o"></i> ดูรายละเอียด</asp:HyperLink>
                                                                    <asp:TextBox ID="txt_AlertDetailPicture_Con" Enabled="false" runat="server" CssClass="form-control" Text="ไม่มีรูปภาพ" Visible="false" Font-Bold="true">
                                                                    </asp:TextBox>

                                                                    <%--  <asp:Image ID="image" runat="server"></asp:Image>--%>
                                                                </div>

                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ชื่ออุปกรณ์" ItemStyle-HorizontalAlign="left"
                                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <div style="padding-top: 5px;">
                                                            <asp:Label ID="lbl_conference_name" runat="server"
                                                                Text='<%# Eval("conference_name") %>'></asp:Label>
                                                        </div>
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="อุปกรณ์สำหรับ" ItemStyle-HorizontalAlign="left"
                                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <div style="padding-top: 5px;">


                                                            <div class="col-sm-3">
                                                                <asp:Label ID="lbl_pic_check" runat="server" Visible="false"
                                                                    Text='<%# Eval("pic_check") %>'></asp:Label>
                                                                <asp:CheckBox ID="chkpic_show" runat="server" CssClass="checkbox-inline" Text="ภาพ" Enabled="false" />

                                                            </div>

                                                            <div class="col-sm-3">
                                                                <asp:Label ID="lbl_sound_check" runat="server" Visible="false"
                                                                    Text='<%# Eval("sound_check") %>'></asp:Label>

                                                                <asp:CheckBox ID="chksound_show" runat="server" CssClass="checkbox-inline" Text="เสียง" Enabled="false" />


                                                            </div>

                                                        </div>
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="จำนวนไมค์" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <div style="padding-top: 5px;">
                                                            <asp:Label ID="lbl_qty_mic" runat="server"
                                                                Text='<%# Eval("qty_mic") %>'></asp:Label>
                                                        </div>
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="จำนวนผู้เข้าประชุมที่เหมาะสม" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <div style="padding-top: 5px;">
                                                            <asp:Label ID="lbl_people_desc" runat="server"
                                                                Text='<%# Eval("people_desc") %>'></asp:Label>
                                                        </div>
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>



                                        </Columns>
                                    </asp:GridView>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">อาหารว่าง</label>
                                        <div class="col-sm-4 control-label textleft">
                                            <asp:CheckBoxList ID="chkFoodDetail" runat="server" CellPadding="5" CellSpacing="5" RepeatColumns="2" Enabled="false"
                                                Width="100%" Font-Bold="false" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right" />
                                            <p class="help-block"><font color="red">** สามารถเลือกอาหารว่างได้(ต้องจองห้องล่วงหน้า 3 วัน) **</font></p>

                                        </div>

                                        <label class="col-sm-2 control-label">หมายเหตุอาหารว่าง</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txt_fooddetail_room" runat="server" placeHolder="กรอกรายละเอียดอาหารว่างที่เพิ่มเติม" Enabled="false" TextMode="MultiLine" CssClass="form-control" Rows="3">
                                            </asp:TextBox>

                                        </div>
                                    </div>

                                    <div class="form-group pull-right">
                                        <%--<label class="col-sm-2 control-label"></label>--%>
                                        <div class="col-sm-12">
                                            <asp:LinkButton ID="btn_SaveCreate" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="บันทึก" OnCommand="btnCommand" CommandName="cmdSave" ValidationGroup="SaveCreate"></asp:LinkButton>

                                            <asp:LinkButton ID="btn_CancelCreate" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="ยกเลิก" OnCommand="btnCommand" CommandName="cmdCancel"></asp:LinkButton>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- Detail Create Room -->

            </div>
        </asp:View>
        <!--View Create-->

        <!--View Approve-->
        <asp:View ID="docApprove" runat="server">
            <div class="col-md-12">

                <asp:FormView ID="FvSearch_Approve" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <InsertItemTemplate>

                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">ค้นหารายการ</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <asp:UpdatePanel ID="update_RoomApprove" runat="server">
                                        <ContentTemplate>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">สถานที่</label>
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddlPlaceSearchApprove" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="Re_ddlPlaceSearchApprove"
                                                        runat="server"
                                                        InitialValue="0"
                                                        ControlToValidate="ddlPlaceSearchApprove" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณาเลือกสถานที่"
                                                        ValidationGroup="SearchApprove" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender84444311" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlPlaceSearchApprove" Width="200" PopupPosition="BottomLeft" />
                                                </div>
                                                <label class="col-sm-2 control-label">ห้องประชุม</label>
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddlRoomSearchApprove" runat="server" CssClass="form-control">
                                                        <asp:ListItem Value="0">--- เลือกห้องประชุม ---</asp:ListItem>
                                                    </asp:DropDownList>

                                                </div>
                                            </div>

                                            <div class="form-group" id="panel_searchdateApprove" runat="server">
                                                <%-- <asp:Label ID="Label9" runat="server" Text="ระบุวันค้นหา" CssClass="col-sm-2 control-label"></asp:Label>--%>
                                                <label class="col-sm-2 control-label">ระบุวันค้นหา</label>
                                                <div class="col-sm-2">
                                                    <asp:DropDownList ID="ddlSearchDateApprove" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                        <asp:ListItem Value="00">เลือกเงื่อนไข</asp:ListItem>
                                                        <asp:ListItem Value="1">มากกว่า  ></asp:ListItem>
                                                        <asp:ListItem Value="2">น้อยกว่า <</asp:ListItem>
                                                        <asp:ListItem Value="3">ระหว่าง  <></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-sm-4 ">

                                                    <div class='input-group date'>
                                                        <asp:TextBox ID="AddStartdateApprove" runat="server" CssClass="form-control from-date-datepicker" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                        <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>

                                                        <asp:RequiredFieldValidator ID="Re_AddStartdateApprove"
                                                            ValidationGroup="SearchApprove" runat="server" Display="None"
                                                            ControlToValidate="AddStartdateApprove" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกวันที่ต้องการ"
                                                            ValidationExpression="กรุณาเลือกวันที่ต้องการ" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5122222" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_AddStartdateApprove" Width="160" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class='input-group date'>
                                                        <asp:TextBox ID="AddEndDateApprove" runat="server" CssClass="form-control from-date-datepicker" Enabled="false" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                        <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                    </div>
                                                </div>
                                                <%--<p class="help-block"><font color="red">**</font></p>--%>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label"></label>
                                                <div class="col-sm-4">
                                                    <asp:LinkButton ID="btnSearchRoomApprove" runat="server"
                                                        CssClass="btn btn-primary" OnCommand="btnCommand" CommandName="cmdSearchRoomApprove" data-original-title="ค้นหา" data-toggle="tooltip" ValidationGroup="SearchApprove"><span class="glyphicon glyphicon-search"></span> ค้นหา</asp:LinkButton>

                                                </div>
                                                <label class="col-sm-6 control-label"></label>
                                            </div>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                </div>
                            </div>
                        </div>

                    </InsertItemTemplate>
                </asp:FormView>


                <!-- Gridview Detail Show Approve With HR -->
                <asp:GridView ID="GvWaitApprove" runat="server"
                    AutoGenerateColumns="False"
                    CssClass="table table-striped table-bordered table-hover table-responsive"
                    OnPageIndexChanging="gvPageIndexChanging"
                    OnRowDataBound="gvRowDataBound"
                    AllowPaging="True" PageSize="10">
                    <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                    <RowStyle Font-Size="Small" />
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">--- ไม่พบข้อมูล ---</div>
                    </EmptyDataTemplate>
                    <Columns>

                        <asp:TemplateField HeaderText="ประเภทการจอง" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                            <ItemTemplate>
                                <asp:Label ID="lbl_type_booking_idx_approve" runat="server" Visible="false" Text='<%# Eval("type_booking_idx") %>'></asp:Label>
                                <asp:Label ID="lbl_u0_document_idx_approve" runat="server" Visible="false" Text='<%# Eval("u0_document_idx") %>'></asp:Label>
                                <asp:Label ID="lbl_cemp_idx_approve" runat="server" Visible="false" Text='<%# Eval("cemp_idx") %>'></asp:Label>
                                <b>
                                    <asp:Label ID="type_booking_name_detail_on_approve" runat="server" Text='<%# Eval("type_booking_name") %>' Visible="false" data-placement="top"
                                        CssClass="col-sm-12" Style="text-align: center; color: green;">
                                    </asp:Label>

                                    <asp:Label ID="type_booking_name_detail_off_approve" runat="server" Text='<%# Eval("type_booking_name") %>' Visible="false"
                                        data-placement="top" CssClass="col-sm-12" Style="text-align: center; color: red;">
                                    </asp:Label>
                                </b>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="วันที่ทำรายการ" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                            <ItemTemplate>
                                <p>
                                    <asp:Label ID="lbl_create_date_detail_approve" runat="server" Text='<%# Eval("create_date") %>'></asp:Label>
                                </p>
                                <p>
                                    <asp:Label ID="lbl_time_create_date_detail_approve" runat="server" Text='<%# Eval("time_create_date") %>'></asp:Label>
                                </p>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="รายละเอียดการจอง" HeaderStyle-CssClass="text-center">
                            <ItemTemplate>
                                <p>
                                    <b>ชื่อ-สกุลผู้สร้าง:</b>
                                    <asp:Label ID="lbl_emp_name_th_approve" runat="server" Text='<%# Eval("emp_name_th") %>' />
                                </p>
                                <p>
                                    <b>องค์กร:</b>
                                    <asp:Label ID="lbl_org_name_th_approve" runat="server" Text='<%# Eval("org_name_th") %>' />
                                </p>
                                <p>
                                    <b>ฝ่าย:</b>
                                    <asp:Label ID="lbl_dept_name_th_approve" runat="server" Text='<%# Eval("dept_name_th") %>' />
                                </p>

                                <p>
                                    <b>สถานที่:</b>
                                    <asp:Label ID="lbl_place_name_approve" runat="server" Text='<%# Eval("place_name") %>' />
                                </p>
                                <p>
                                    <b>ชื่อห้อง:</b>
                                    <asp:Label ID="lbl_room_name_th_approve" runat="server" Text='<%# Eval("room_name_th") %>' />
                                </p>
                                <p>
                                    <b>หัวข้อการจอง:</b>
                                    <asp:Label ID="lbl_topic_booking_approve" runat="server" Text='<%# Eval("topic_booking") %>' />
                                </p>

                                <p>
                                    <b>ตั้งแต่วันที่:</b>
                                    <asp:Label ID="lbl_date_start_approve" runat="server" Text='<%# Eval("date_start") %>' />
                                    <b>เวลา:</b>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("time_start") %>' />
                                    <b>น.</b>
                                </p>
                                <p>
                                    <b>ถึงวันที่:</b>
                                    <asp:Label ID="lbl_date_end_approve" runat="server" Text='<%# Eval("date_end") %>' />
                                    <b>เวลา:</b>
                                    <asp:Label ID="lbl_time_end_approve" runat="server" Text='<%# Eval("time_end") %>' />
                                    <b>น.</b>
                                </p>


                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สถานะรายการ" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                            <ItemTemplate>

                                <asp:Label ID="lbl_status_name_approve" runat="server" Text='<%# Eval("status_name") %>' />
                                <asp:Label ID="lbl_node_name_detail" runat="server" Text='<%# Eval("node_name") %>' />
                                <br />
                                โดย<br />
                                <asp:Label ID="lbl_actor_name_approve" runat="server" Text='<%# Eval("actor_name") %>' />

                                <%--  <asp:Label ID="lbl_current_status" runat="server" Text='<%# Eval("current_status") %>'></asp:Label>--%>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnRoomDetailWaitApprove" CssClass="btn-sm btn-info" target="" runat="server" CommandName="cmdViewWaitApprove" OnCommand="btnCommand"
                                    CommandArgument='<%# Eval("u0_document_idx")+ ";" + Eval("m0_room_idx")+ ";" + Eval("room_name_th") + ";" + Eval("m0_node_idx") + ";" + Eval("m0_actor_idx") 
                                        + ";" + Eval("staidx")%>'
                                    data-toggle="tooltip" title="ดูข้อมูล"><i class="fa fa-file"></i></asp:LinkButton>

                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>
                <!-- Gridview Detail Show Approve With HR -->

                <!-- Back To Wait Approve Detail Room Booking -->
                <asp:UpdatePanel ID="Panel_BackToWaitApprove" runat="server" Visible="false">
                    <ContentTemplate>
                        <div class="form-group">
                            <asp:LinkButton ID="btnCancelToWaitApprove" CssClass="btn btn-danger" data-toggle="tooltip" title="" runat="server"
                                CommandName="cmdCancelToWaitApprove" OnCommand="btnCommand">< ย้อนกลับ</asp:LinkButton>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- Back To Wait Approve Detail Room Booking -->

                <!-- Fv View Wait Approve Detail Room Booking -->
                <asp:FormView ID="FvWaitApproveDetail" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                    <ItemTemplate>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายละเอียดรายการจองห้องประชุม</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">ประเภทการจอง</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tb_u0_document_idx_approve" runat="server" Visible="false" CssClass="form-control" Text='<%# Eval("u0_document_idx") %>' Enabled="false" />
                                            <asp:TextBox ID="tb_type_booking_name_approve" runat="server" CssClass="form-control" Text='<%# Eval("type_booking_name") %>' Enabled="false" />
                                            <asp:TextBox ID="tb_type_booking_idx_approve" runat="server" Visible="false" Text='<%# Eval("type_booking_idx") %>' CssClass="form-control" />

                                        </div>

                                        <label class="col-sm-2 control-label">ผู้ทำรายการ</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpName_approve" runat="server" CssClass="form-control" Text='<%# Eval("emp_name_th") %>' Enabled="false" />
                                            <asp:TextBox ID="tbActorCempIDX_approve" runat="server" Visible="false" Text='<%# Eval("cemp_idx") %>' CssClass="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">องค์กร</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpOrg_approve" runat="server" CssClass="form-control" Text='<%# Eval("org_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ฝ่าย</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpDept_approve" runat="server" CssClass="form-control" Text='<%# Eval("dept_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">แผนก</label>
                                        <div class="col-sm-4">

                                            <asp:TextBox ID="tbEmpSec_approve" runat="server" CssClass="form-control" Text='<%# Eval("sec_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ตำแหน่ง</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpPos_approve" runat="server" CssClass="form-control" Text='<%# Eval("pos_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">เบอร์ติดต่อ</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpmobile_approve" runat="server" CssClass="form-control" Text='<%# Eval("emp_mobile_no") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">Email</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpemail_approve" runat="server" CssClass="form-control" Text='<%# Eval("emp_email") %>' Enabled="false" />
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">สถานที่</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tb_place_idx_approve" runat="server" CssClass="form-control" Text='<%# Eval("place_idx") %>' Enabled="false" Visible="false" />
                                            <asp:TextBox ID="tb_place_name_approve" runat="server" CssClass="form-control" Text='<%# Eval("place_name") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ห้องประชุม</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tb_m0_room_idx_approve" runat="server" CssClass="form-control" Text='<%# Eval("m0_room_idx") %>' Enabled="false" Visible="false" />
                                            <asp:TextBox ID="tb_room_name_th_approve" runat="server" CssClass="form-control" Text='<%# Eval("room_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">ตั้งแต่วันที่</label>
                                        <div class="col-sm-4">

                                            <asp:TextBox ID="tb_date_start_approve" runat="server" CssClass="form-control" Text='<%# Eval("date_start") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">เวลา</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tb_time_start_approve" runat="server" CssClass="form-control" Text='<%# Eval("time_start") %>' Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">ถึงวันที่</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tb_date_end_approve" runat="server" CssClass="form-control" Text='<%# Eval("date_end") %>' Enabled="false" />

                                        </div>
                                        <label class="col-sm-2 control-label">เวลา</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tb_time_end_approve" runat="server" CssClass="form-control" Text='<%# Eval("time_end") %>' Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">สถานะ</label>
                                        <div class="col-sm-4">

                                            <asp:TextBox ID="tb_status_booking" runat="server" CssClass="form-control" Text='<%# Eval("status_booking") %>' Enabled="false" />
                                        </div>

                                        <label class="col-sm-2 control-label">หัวข้อการประชุม</label>
                                        <div class="col-sm-4">

                                            <asp:TextBox ID="tb_topic_booking" runat="server" CssClass="form-control" Text='<%# Eval("topic_booking") %>' Enabled="false" />
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">เหตุผลการใช้ห้องประชุม</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tb_result_use_name" runat="server" CssClass="form-control" Text='<%# Eval("result_use_name") %>' Enabled="false" />
                                        </div>

                                        <label class="col-sm-2 control-label">จำนวนผู้เข้าประชุม</label>
                                        <div class="col-sm-4">

                                            <asp:TextBox ID="tb_count_people_approve" runat="server" CssClass="form-control" Text='<%# Eval("count_people") %>' Enabled="false" />
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">รายละเอียดเพิ่มเติม</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="tb_detail_booking_approve" runat="server" Text='<%# Eval("detail_booking") %>' TextMode="MultiLine" Enabled="false" CssClass="form-control" Rows="3">
                                            </asp:TextBox>
                                            <%-- <asp:TextBox ID="TextBox10" runat="server" CssClass="form-control" Text='<%# Eval("pos_name_th") %>' Enabled="false" />--%>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">รูปภาพห้องประชุม</label>
                                        <%--<label>รูปภาพห้องประชุม</label>--%>
                                        <div class="col-sm-10">
                                            <asp:HyperLink runat="server" ID="btnViewFileRoomDetailApprove" CssClass="" Target="_blank"><i class="fa fa-file-text-o"></i> ดูรายละเอียด</asp:HyperLink>
                                            <asp:TextBox ID="txt_AlertDetailPictureApprove" runat="server" CssClass="form-control" Text="ไม่มีรูปภาพ" Visible="false" Font-Bold="true">
                                            </asp:TextBox>
                                        </div>
                                        <asp:TextBox ID="txtm0_conidx_approve" Visible="false" runat="server" Text='<%# Eval("m0_conidx") %>' CssClass="form-control">
                                        </asp:TextBox>
                                    </div>

                                    <hr />

                                    <asp:GridView ID="GvConferenceInRoom_ApproveDetail" runat="server" Visible="true"
                                        AutoGenerateColumns="false"
                                        CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                                        HeaderStyle-CssClass="success"
                                        AllowPaging="false"
                                        OnRowDataBound="gvRowDataBound">

                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                                        <EmptyDataTemplate>
                                            <div style="text-align: center; color: red"><b>-- ไม่พบข้อมูล --</b> </div>
                                        </EmptyDataTemplate>
                                        <Columns>
                                            <asp:TemplateField HeaderStyle-CssClass="text-center"
                                                ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="5%" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:RadioButton ID="rdoconference_detailapprove" Enabled="false" runat="server" />
                                                        <asp:Label ID="lbl_m0_conidx" runat="server" Visible="false" Text='<%# Eval("m0_conidx") %>' />

                                                    </small>

                                                </ItemTemplate>
                                            </asp:TemplateField>



                                            <asp:TemplateField HeaderText="ภาพอุปกรณ์" ItemStyle-HorizontalAlign="left"
                                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:UpdatePanel ID="updatepiccon" runat="server" UpdateMode="Always">
                                                            <ContentTemplate>
                                                                <div style="padding-top: 5px;">
                                                                    <asp:HyperLink runat="server" ID="btnViewFileConference_Approve" CssClass="" Target="_blank"><i class="fa fa-file-text-o"></i> ดูรายละเอียด</asp:HyperLink>
                                                                    <asp:TextBox ID="txt_AlertDetailPicture_ConApprove" Enabled="false" runat="server" CssClass="form-control" Text="ไม่มีรูปภาพ" Visible="false" Font-Bold="true">
                                                                    </asp:TextBox>

                                                                </div>

                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ชื่ออุปกรณ์" ItemStyle-HorizontalAlign="left"
                                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <div style="padding-top: 5px;">
                                                            <asp:Label ID="lbl_conference_name" runat="server"
                                                                Text='<%# Eval("conference_name") %>'></asp:Label>
                                                        </div>
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="อุปกรณ์สำหรับ" ItemStyle-HorizontalAlign="left"
                                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <div style="padding-top: 5px;">


                                                            <div class="col-sm-3">
                                                                <asp:Label ID="lbl_pic_check" runat="server" Visible="false"
                                                                    Text='<%# Eval("pic_check") %>'></asp:Label>
                                                                <asp:CheckBox ID="chkpic_show" runat="server" CssClass="checkbox-inline" Text="ภาพ" Enabled="false" />

                                                            </div>

                                                            <div class="col-sm-3">
                                                                <asp:Label ID="lbl_sound_check" runat="server" Visible="false"
                                                                    Text='<%# Eval("sound_check") %>'></asp:Label>

                                                                <asp:CheckBox ID="chksound_show" runat="server" CssClass="checkbox-inline" Text="เสียง" Enabled="false" />


                                                            </div>

                                                        </div>
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="จำนวนไมค์" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <div style="padding-top: 5px;">
                                                            <asp:Label ID="lbl_qty_mic" runat="server"
                                                                Text='<%# Eval("qty_mic") %>'></asp:Label>
                                                        </div>
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="จำนวนผู้เข้าประชุมที่เหมาะสม" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <div style="padding-top: 5px;">
                                                            <asp:Label ID="lbl_people_desc" runat="server"
                                                                Text='<%# Eval("people_desc") %>'></asp:Label>
                                                        </div>
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>



                                        </Columns>
                                    </asp:GridView>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">อาหารว่าง</label>
                                        <div class="col-sm-4 control-label textleft">
                                            <asp:CheckBoxList ID="chkFoodDetail_approve" runat="server" CellPadding="5" CellSpacing="5" RepeatColumns="2" Enabled="false"
                                                Width="100%" Font-Bold="false" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right" />
                                            <p class="help-block"><font color="red">** สามารถเลือกอาหารว่างได้(ต้องจองห้องล่วงหน้า 3 วัน) **</font></p>

                                        </div>

                                        <label class="col-sm-2 control-label">หมายเหตุอาหารว่าง</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txt_fooddetail_room_approve" runat="server" placeHolder="รายละเอียดอาหารว่างที่เพิ่มเติม" Text='<%# Eval("detail_foodbooking") %>' Enabled="false" TextMode="MultiLine" CssClass="form-control" Rows="3">
                                            </asp:TextBox>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </ItemTemplate>

                    <EditItemTemplate>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">เปลี่บนแปลงรายการจองห้องประชุม (แทนที่)</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">ประเภทการจอง</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tb_u0_document_idx_approve" runat="server" Visible="false" CssClass="form-control" Text='<%# Eval("u0_document_idx") %>' Enabled="false" />
                                            <asp:TextBox ID="tb_type_booking_name_approve" runat="server" CssClass="form-control" Text='<%# Eval("type_booking_name") %>' Enabled="false" />
                                            <asp:TextBox ID="tb_type_booking_idx_approve" runat="server" Visible="false" Text='<%# Eval("type_booking_idx") %>' CssClass="form-control" />
                                        </div>

                                        <label class="col-sm-2 control-label">ผู้ทำรายการ</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpName_approve" runat="server" CssClass="form-control" Text='<%# Eval("emp_name_th") %>' Enabled="false" />
                                            <asp:TextBox ID="tbActorCempIDX_approve" runat="server" Visible="false" Text='<%# Eval("cemp_idx") %>' CssClass="form-control" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">องค์กร</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpOrg_approve" runat="server" CssClass="form-control" Text='<%# Eval("org_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ฝ่าย</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpDept_approve" runat="server" CssClass="form-control" Text='<%# Eval("dept_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">แผนก</label>
                                        <div class="col-sm-4">

                                            <asp:TextBox ID="tbEmpSec_approve" runat="server" CssClass="form-control" Text='<%# Eval("sec_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ตำแหน่ง</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpPos_approve" runat="server" CssClass="form-control" Text='<%# Eval("pos_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">สถานที่</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tb_place_idx_approve" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("place_idx") %>' Enabled="true" />
                                            <asp:TextBox ID="tb_place_name_approve" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("place_name") %>' Enabled="true" />

                                            <asp:DropDownList ID="ddlPlace_edit_approve" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" Enabled="false">
                                            </asp:DropDownList>
                                        </div>
                                        <label class="col-sm-2 control-label">ห้องประชุม</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tb_m0_room_idx_approve" runat="server" Visible="false" CssClass="form-control" Text='<%# Eval("m0_room_idx") %>' Enabled="true" />

                                            <asp:TextBox ID="tb_room_name_th_approve" runat="server" Visible="false" CssClass="form-control" Text='<%# Eval("room_name_th") %>' Enabled="false" />

                                            <asp:DropDownList ID="ddlRoom_edit_approve" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                <asp:ListItem Value="0">--- เลือกห้องประชุม ---</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">ตั้งแต่วันที่</label>
                                        <div class="col-sm-4">

                                            <asp:TextBox ID="tb_date_start_approve" runat="server" CssClass="form-control" Text='<%# Eval("date_start") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">เวลา</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tb_time_start_approve" runat="server" CssClass="form-control" Text='<%# Eval("time_start") %>' Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">ถึงวันที่</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tb_date_end_approve" runat="server" CssClass="form-control" Text='<%# Eval("date_end") %>' Enabled="false" />

                                        </div>
                                        <label class="col-sm-2 control-label">เวลา</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tb_time_end_approve" runat="server" CssClass="form-control" Text='<%# Eval("time_end") %>' Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">สถานะ</label>
                                        <div class="col-sm-4">

                                            <asp:TextBox ID="tb_status_booking_approve" runat="server" CssClass="form-control" Text='<%# Eval("status_booking") %>' Enabled="false" />
                                        </div>

                                        <label class="col-sm-2 control-label">หัวข้อการประชุม</label>
                                        <div class="col-sm-4">

                                            <asp:TextBox ID="tb_topic_booking_approve" runat="server" CssClass="form-control" Text='<%# Eval("topic_booking") %>' Enabled="true" />
                                        </div>
                                    </div>

                                    <div class="form-group">

                                        <label class="col-sm-2 control-label">เหตุผลการใช้ห้องประชุม</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tb_result_use_idx_approve" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("result_use_idx") %>' Enabled="true" />
                                            <asp:TextBox ID="tb_result_use_name_approve" runat="server" Visible="false" CssClass="form-control" Text='<%# Eval("result_use_name") %>' Enabled="true" />

                                            <asp:DropDownList ID="ddlResultUse_edit_approve" runat="server" CssClass="form-control">
                                            </asp:DropDownList>

                                        </div>

                                        <label class="col-sm-2 control-label">จำนวนผู้เข้าประชุม</label>
                                        <div class="col-sm-4">

                                            <asp:TextBox ID="tb_count_people_approve" runat="server" CssClass="form-control" Text='<%# Eval("count_people") %>' Enabled="true" />
                                        </div>

                                    </div>

                                    <div class="form-group">

                                        <label class="col-sm-2 control-label">รายละเอียดเพิ่มเติม</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="tb_detail_booking_approve" runat="server" Text='<%# Eval("detail_booking") %>' TextMode="MultiLine" Enabled="true" CssClass="form-control" Rows="3">
                                            </asp:TextBox>
                                            <%-- <asp:TextBox ID="TextBox10" runat="server" CssClass="form-control" Text='<%# Eval("pos_name_th") %>' Enabled="false" />--%>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">รูปภาพห้องประชุม</label>
                                        <%--<label>รูปภาพห้องประชุม</label>--%>
                                        <div class="col-sm-10">
                                            <asp:HyperLink runat="server" ID="btnViewFileRoomDetailApprove" CssClass="" Target="_blank"><i class="fa fa-file-text-o"></i> ดูรายละเอียด</asp:HyperLink>
                                            <asp:TextBox ID="txt_AlertDetailPictureApprove" runat="server" CssClass="form-control" Text="ไม่มีรูปภาพ" Visible="false" Font-Bold="true">
                                            </asp:TextBox>
                                        </div>
                                        <asp:TextBox ID="txtm0_conidx_appedit" Visible="false" runat="server" Text='<%# Eval("m0_conidx") %>' CssClass="form-control">
                                        </asp:TextBox>
                                    </div>


                                    <hr />

                                    <asp:GridView ID="GvConferenceInRoom_DetailApproveEdit" runat="server" Visible="true"
                                        AutoGenerateColumns="false"
                                        CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                                        HeaderStyle-CssClass="success"
                                        AllowPaging="false"
                                        OnRowDataBound="gvRowDataBound">

                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                                        <EmptyDataTemplate>
                                            <div style="text-align: center; color: red"><b>-- ไม่พบข้อมูล --</b> </div>
                                        </EmptyDataTemplate>
                                        <Columns>
                                            <asp:TemplateField HeaderStyle-CssClass="text-center"
                                                ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="5%" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:RadioButton ID="rdoconference_appedit" OnCheckedChanged="ddlSelectedIndexChanged" AutoPostBack="true" GroupName="rdoconference" runat="server" />
                                                        <asp:Label ID="lbl_m0_conidx" runat="server" Visible="false" Text='<%# Eval("m0_conidx") %>' />

                                                    </small>

                                                </ItemTemplate>
                                            </asp:TemplateField>



                                            <asp:TemplateField HeaderText="ภาพอุปกรณ์" ItemStyle-HorizontalAlign="left"
                                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:UpdatePanel ID="updatepiccon" runat="server" UpdateMode="Always">
                                                            <ContentTemplate>
                                                                <div style="padding-top: 5px;">
                                                                    <asp:HyperLink runat="server" ID="btnViewFileConference_Appedit" CssClass="" Target="_blank"><i class="fa fa-file-text-o"></i> ดูรายละเอียด</asp:HyperLink>
                                                                    <asp:TextBox ID="txt_AlertDetailPicture_ConAppEdit" Enabled="false" runat="server" CssClass="form-control" Text="ไม่มีรูปภาพ" Visible="false" Font-Bold="true">
                                                                    </asp:TextBox>

                                                                </div>

                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ชื่ออุปกรณ์" ItemStyle-HorizontalAlign="left"
                                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <div style="padding-top: 5px;">
                                                            <asp:Label ID="lbl_conference_name" runat="server"
                                                                Text='<%# Eval("conference_name") %>'></asp:Label>
                                                        </div>
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="อุปกรณ์สำหรับ" ItemStyle-HorizontalAlign="left"
                                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <div style="padding-top: 5px;">


                                                            <div class="col-sm-3">
                                                                <asp:Label ID="lbl_pic_check" runat="server" Visible="false"
                                                                    Text='<%# Eval("pic_check") %>'></asp:Label>
                                                                <asp:CheckBox ID="chkpic_show" runat="server" CssClass="checkbox-inline" Text="ภาพ" Enabled="false" />

                                                            </div>

                                                            <div class="col-sm-3">
                                                                <asp:Label ID="lbl_sound_check" runat="server" Visible="false"
                                                                    Text='<%# Eval("sound_check") %>'></asp:Label>

                                                                <asp:CheckBox ID="chksound_show" runat="server" CssClass="checkbox-inline" Text="เสียง" Enabled="false" />


                                                            </div>

                                                        </div>
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="จำนวนไมค์" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <div style="padding-top: 5px;">
                                                            <asp:Label ID="lbl_qty_mic" runat="server"
                                                                Text='<%# Eval("qty_mic") %>'></asp:Label>
                                                        </div>
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="จำนวนผู้เข้าประชุมที่เหมาะสม" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <div style="padding-top: 5px;">
                                                            <asp:Label ID="lbl_people_desc" runat="server"
                                                                Text='<%# Eval("people_desc") %>'></asp:Label>
                                                        </div>
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>



                                        </Columns>
                                    </asp:GridView>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">อาหารว่าง</label>
                                        <div class="col-sm-4 control-label textleft">
                                            <asp:CheckBoxList ID="chkFoodDetail_approve" runat="server" CellPadding="5" CellSpacing="5" RepeatColumns="2" Enabled="false"
                                                Width="100%" Font-Bold="false" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right" />
                                            <p class="help-block"><font color="red">** สามารถเลือกอาหารว่างได้(ต้องจองห้องล่วงหน้า 3 วัน) **</font></p>

                                        </div>

                                        <label class="col-sm-2 control-label">หมายเหตุอาหารว่าง</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txt_fooddetail_room_approve" runat="server" placeHolder="รายละเอียดอาหารว่างที่เพิ่มเติม" Text='<%# Eval("detail_foodbooking") %>' Enabled="false" TextMode="MultiLine" CssClass="form-control" Rows="3">
                                            </asp:TextBox>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-10">
                                            <asp:LinkButton ID="btnBackDetailBeforeEdit_approve" CssClass="btn btn-default" data-toggle="tooltip" title="" runat="server"
                                                CommandName="cmdBackDetailBeforeEdit_approve" OnCommand="btnCommand"><i class="fa fa-close"></i> ยกเลิกแก้ไข</asp:LinkButton>

                                            <asp:LinkButton ID="btnSaveEditDetail_approve" CssClass="btn btn-success" runat="server" data-original-title="บันทึกการเปลี่ยนแปลง" data-toggle="tooltip" Text="บันทึกการเปลี่ยนแปลง" OnCommand="btnCommand" CommandName="cmdSaveEdit_approve" ValidationGroup="SaveEdit" Visible="false"></asp:LinkButton>
                                        </div>
                                    </div>



                                    <!-- Gridview Search Detail Room -->
                                    <asp:UpdatePanel ID="Update_DetailRoomSearch_EditApprove" runat="server" Visible="false">
                                        <ContentTemplate>

                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">รายละเอียดห้องที่เปลี่ยนแปลง</h3>
                                                </div>
                                                <div class="panel-body">
                                                    <asp:GridView ID="GvDetailRoomSearch_EditApprove" runat="server"
                                                        AutoGenerateColumns="False"
                                                        CssClass="table table-striped table-bordered table-hover table-responsive"
                                                        OnPageIndexChanging="gvPageIndexChanging"
                                                        OnRowDataBound="gvRowDataBound"
                                                        AllowPaging="True" PageSize="10">
                                                        <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                                                        <RowStyle Font-Size="Small" />
                                                        <PagerStyle CssClass="pageCustom" />
                                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                                        <EmptyDataTemplate>
                                                            <div style="text-align: center">ไม่พบข้อมูล</div>
                                                        </EmptyDataTemplate>
                                                        <Columns>

                                                            <asp:TemplateField HeaderText="รูปภาพห้องประชุม" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                <ItemTemplate>
                                                                    <small>
                                                                        <asp:HyperLink runat="server" ID="btnViewFileRoomEditApprove" CssClass="pull-letf" data-toggle="tooltip"
                                                                            title="view" data-original-title="" Target="_blank">
                                                                                <i class="fa fa-file-text-o"></i> ดูรายละเอียด</asp:HyperLink>
                                                                    </small>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="ชื่อห้อง(TH)" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_m0_room_idx_edit_approve" runat="server" Visible="false" Text='<%# Eval("m0_room_idx") %>'></asp:Label>
                                                                    <asp:Label ID="lbl_room_name_th_approve" runat="server" Text='<%# Eval("room_name_th") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="ชื่อห้อง(EN)" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                <ItemTemplate>

                                                                    <asp:Label ID="lbl_room_name_en_edit_approve" runat="server" Text='<%# Eval("room_name_en") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="รายละเอียดห้อง" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_room_detail_edit_approve" runat="server" Text='<%# Eval("room_detail") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_room_detail_status_edit_approve" runat="server" Visible="true"></asp:Label>
                                                                    <%--<asp:Label ID="lbl_room_detail_notbusy_edit" runat="server" Visible="false" Text="ไม่ว่าง"></asp:Label>
                                                                    <asp:Label ID="lbl_room_detail_busy_edit" runat="server" Visible="false" Text="ว่าง"></asp:Label>--%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <%--<label class="col-sm-10 control-label"></label>--%>
                                                <div class="col-sm-12">
                                                    <div class="pull-right">
                                                        <asp:LinkButton ID="btn_SaveEditApprove" CssClass="btn btn-success" runat="server" data-original-title="บันทึกการเปลี่ยนแปลงห้อง" data-toggle="tooltip" Text="บันทึกการเปลี่ยนแปลงห้อง" OnCommand="btnCommand" CommandName="cmdSaveEditReplaceRoom" ValidationGroup="SaveEdit"></asp:LinkButton>

                                                        <asp:LinkButton ID="btn_CancelEditApprove" CssClass="btn btn-danger" runat="server" data-original-title="ยกเลิก" data-toggle="tooltip" Text="ยกเลิก" OnCommand="btnCommand" CommandName="cmdCancelToWaitApprove"></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <!-- Gridview Search Detail Room cmdBackDetailBeforeEdit -->

                                </div>

                            </div>

                        </div>
                    </EditItemTemplate>

                </asp:FormView>
                <!-- Fv View Wait Approve Detail Room Booking -->

                <!-- Button Edit Detail RoomBooking -->
                <asp:UpdatePanel ID="Update_EditRoomBookingButtonHR" runat="server" Visible="false">
                    <ContentTemplate>
                        <div class="form-group">
                            <%--    <asp:LinkButton ID="btnChangeRoomDetail" CssClass="btn-sm btn-warning" target="" runat="server" CommandName="cmdChangeRoomDetail" OnCommand="btnCommand"
                                    CommandArgument='<%# Eval("u0_document_idx")+ ";" + Eval("m0_room_idx")+ ";" + Eval("room_name_th")%>' data-toggle="tooltip" title="เปลี่ยนแปลง"><i class="fa fa-pencil-square-o"></i></asp:LinkButton>--%>
                            <asp:LinkButton ID="btnEditReplaceRoomHR" CssClass="btn btn-warning" data-toggle="tooltip" title="" runat="server"
                                CommandName="cmdEditDetailBookingHR" OnCommand="btnCommand"><i class="fa fa-pencil-square-o"></i> เปลี่ยนแปลงการจองห้องประชุม</asp:LinkButton>

                            <asp:LinkButton ID="btnECancelReplaceRoomHR" CssClass="btn btn-danger" data-toggle="tooltip" title="" runat="server"
                                CommandName="cmdCancelBookingRoomHR" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-close"></i> ยกเลิกการจองห้องประชุม</asp:LinkButton>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- Button Edit Detail RoomBooking -->

                <!-- Fv View Wait Approve Detail Room Booking -->
                <asp:FormView ID="FvDetailApproveHR" runat="server" Width="100%">
                    <InsertItemTemplate>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">พิจารณารายการของ (เจ้าหน้าที่ฝ่ายบุคคล) </h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <%--<div class="col-md-9 col-md-offset-3">--%>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">เลือกสถานะพิจารณา</label>
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddlDecisionApprove" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="Re_ddlDecisionApprove"
                                                runat="server"
                                                InitialValue="0"
                                                ControlToValidate="ddlDecisionApprove" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกสถานะพิจารณา"
                                                ValidationGroup="SaveApproveHR" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1233" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlDecisionApprove" Width="200" PopupPosition="BottomLeft" />
                                        </div>
                                        <label class="col-sm-3 control-label"></label>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">รายละเอียดเพิ่มเติม</label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txt_CommentApprove" runat="server" placeHolder="กรอกรายละเอียดเพิ่มเติม" TextMode="MultiLine" CssClass="form-control" Rows="3">
                                            </asp:TextBox>

                                        </div>
                                        <label class="col-sm-3 control-label"></label>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"></label>
                                        <div class="col-sm-9">
                                            <asp:LinkButton ID="btn_SaveApproveHR" CssClass="btn btn-success" runat="server" data-original-title="บันทึก" data-toggle="tooltip" Text="บันทึก" OnCommand="btnCommand"
                                                CommandName="cmdSaveApprove" ValidationGroup="SaveApproveHR"></asp:LinkButton>

                                            <asp:LinkButton ID="btn_CancelCreate" CssClass="btn btn-danger" runat="server" data-original-title="ยกเลิก" data-toggle="tooltip" Text="ยกเลิก" OnCommand="btnCommand" CommandName="cmdCancelToWaitApprove"></asp:LinkButton>
                                        </div>
                                    </div>
                                    <%-- </div>--%>
                                </div>
                            </div>
                        </div>
                    </InsertItemTemplate>
                </asp:FormView>

                <!-- Log WaitApprove Detail Room Booking -->
                <div class="panel panel-default" id="div_LogViewDetailWaitApprove" runat="server" visible="false">
                    <div class="panel-heading">ประวัติการดำเนินการ</div>
                    <table class="table table-striped f-s-12 table-empshift-responsive">
                        <asp:Repeater ID="rptLogRoomBookingWaitApprove" runat="server">
                            <HeaderTemplate>
                                <tr>
                                    <th>วัน / เวลา</th>
                                    <th>ผู้ดำเนินการ</th>
                                    <th>ดำเนินการ</th>
                                    <%--<th>ผลการดำเนินการ</th>--%>
                                    <th>Comment</th>
                                </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td data-th="วัน / เวลา"><%# Eval("create_date") %> <%# Eval("time_create") %></td>
                                    <td data-th="ผู้ดำเนินการ"><%# Eval("emp_name_th") %> ( <%# Eval("current_artor") %> )</td>
                                    <td data-th="ดำเนินการ"><%# Eval("current_decision") %></td>
                                    <td data-th="Comment"><%# Eval("comment") %></td>
                                    <%--<td data-th="ผลการดำเนินการ"><%# Eval("current_status_name") %></td>--%>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                    <div class="m-t-10 m-b-10"></div>
                </div>
                <!-- Log WaitApprove Detail Room Booking -->

            </div>

        </asp:View>
        <!--View Approve-->

        <!--View Report-->
        <asp:View ID="docReport" runat="server">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="col-md-12">

                        <!-- Panel Search Report -->
                        <asp:UpdatePanel ID="Update_PnSearchReport" runat="server">
                            <ContentTemplate>
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">ค้นหารายงาน </h3>
                                    </div>

                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">ประเภทรายงาน</label>
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddlTypeReport" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" CssClass="form-control">
                                                        <asp:ListItem Value="0">--- เลือกประเภทรายงาน ---</asp:ListItem>
                                                        <asp:ListItem Value="1">ตาราง</asp:ListItem>
                                                        <asp:ListItem Value="2">กราฟ</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="Re_ddlTypeReport"
                                                        runat="server"
                                                        InitialValue="0"
                                                        ControlToValidate="ddlTypeReport" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณาเลือกประเภทรายงาน"
                                                        ValidationGroup="SearchReportDetail" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1233" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlTypeReport" Width="200" PopupPosition="BottomLeft" />
                                                </div>
                                                <label class="col-sm-6 control-label"></label>
                                            </div>

                                            <!-- START Report Table -->
                                            <asp:UpdatePanel ID="Update_PnTableReportDetail" runat="server" Visible="false">
                                                <ContentTemplate>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">ค้นหาจากสถานะ</label>
                                                        <div class="col-sm-4">
                                                            <asp:DropDownList ID="ddlStatusRoomBooking" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" CssClass="form-control">
                                                                <asp:ListItem Value="0">--- เลือกประเภทการค้นหา ---</asp:ListItem>
                                                                <asp:ListItem Value="3">เสร็จสมบูรณ์</asp:ListItem>
                                                                <asp:ListItem Value="2">ถูกยกเลิก</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <label class="col-sm-6 control-label"></label>
                                                    </div>

                                                    <div class="form-group">
                                                        <%-- <asp:Label ID="Label9" runat="server" Text="ระบุวันค้นหา" CssClass="col-sm-2 control-label"></asp:Label>--%>
                                                        <label class="col-sm-2 control-label">ระบุวันค้นหา</label>
                                                        <div class="col-sm-2">
                                                            <asp:DropDownList ID="ddlSearchDateReport" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- เลือกเงื่อนไข ---</asp:ListItem>
                                                                <asp:ListItem Value="1">มากกว่า ></asp:ListItem>
                                                                <asp:ListItem Value="2">น้อยกว่า <</asp:ListItem>
                                                                <asp:ListItem Value="3">ระหว่าง <></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>

                                                        <div class="col-sm-4 ">

                                                            <div class='input-group date'>
                                                                <asp:TextBox ID="txtAddStartdateReport" runat="server" CssClass="form-control from-date-datepicker" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                                <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                <asp:RequiredFieldValidator ID="Required_txtAddStartdateReport" ValidationGroup="SearchReportDetail" runat="server" Display="None"
                                                                    ControlToValidate="txtAddStartdateReport" Font-Size="11"
                                                                    ErrorMessage="กรุณาเลือกวันที่ต้องการ"
                                                                    ValidationExpression="กรุณาเลือกวันที่ต้องการ" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5133" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Required_txtAddStartdateReport" Width="160" />
                                                            </div>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <div class='input-group date'>
                                                                <asp:TextBox ID="txtAddEndDateReport" runat="server" CssClass="form-control from-date-datepicker" Enabled="false" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                                <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">ค้นหาจากองค์กร</label>
                                                        <div class="col-sm-4">
                                                            <asp:DropDownList ID="ddlorgidx" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control">
                                                                <asp:ListItem Value="0">--- เลือกองค์กร ---</asp:ListItem>
                                                            </asp:DropDownList>

                                                        </div>
                                                        <label class="col-sm-2 control-label">ค้นหาจากฝ่าย</label>
                                                        <div class="col-sm-4">
                                                            <asp:DropDownList ID="ddlrdeptidx" runat="server" CssClass="form-control">
                                                                <asp:ListItem Value="0">--- เลือกฝ่าย ---</asp:ListItem>
                                                            </asp:DropDownList>

                                                        </div>

                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">สถานที่</label>
                                                        <div class="col-sm-4">
                                                            <asp:DropDownList ID="ddlPlaceReportSearchDetail" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                            <%--<asp:RequiredFieldValidator ID="Re_ddlPlaceSearchDetail"
                                                        runat="server"
                                                        InitialValue="0"
                                                        ControlToValidate="ddlPlaceSearchDetail" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณาเลือกสถานที่"
                                                        ValidationGroup="SearchDetail" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8444" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlPlaceSearchDetail" Width="200" PopupPosition="BottomLeft" />--%>
                                                        </div>
                                                        <label class="col-sm-2 control-label">ห้องประชุม</label>
                                                        <div class="col-sm-4">
                                                            <asp:DropDownList ID="ddlRoomReportSearchDetail" runat="server" CssClass="form-control">
                                                                <asp:ListItem Value="0">--- เลือกห้องประชุม ---</asp:ListItem>
                                                            </asp:DropDownList>

                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label"></label>
                                                        <div class="col-sm-4">
                                                            <asp:LinkButton ID="btnSearchReportRoomDetail" runat="server"
                                                                CssClass="btn btn-primary" OnCommand="btnCommand" CommandName="cmdSearchReportRoomDetail" data-original-title="ค้นหา" data-toggle="tooltip" ValidationGroup="SearchReportDetail"><span class="glyphicon glyphicon-search"></span> ค้นหา</asp:LinkButton>

                                                            <asp:LinkButton ID="btnExportReportRoomDetail" runat="server"
                                                                CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdExportReportRoomDetail" data-original-title="Export" data-toggle="tooltip" ValidationGroup="SearchReportDetail"><span class="glyphicon glyphicon-export"></span> Export</asp:LinkButton>

                                                        </div>
                                                        <label class="col-sm-6 control-label"></label>
                                                    </div>

                                                    <%--<p class="help-block"><font color="red">**</font></p>--%>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <%--<asp:PostBackTrigger ControlID="btnPrintSampleCode" />--%>
                                                    <asp:PostBackTrigger ControlID="btnExportReportRoomDetail" />
                                                </Triggers>

                                            </asp:UpdatePanel>
                                            <!-- START Report Table -->

                                            <!-- START Report Graph -->
                                            <asp:UpdatePanel ID="Update_PnGraphReportDetail" runat="server" Visible="false" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">ประเภทกราฟ</label>
                                                        <div class="col-sm-4">
                                                            <asp:DropDownList ID="ddlTypeGraph" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" CssClass="form-control">
                                                                <asp:ListItem Value="0">--- เลือกประเภทกราฟ ---</asp:ListItem>
                                                                <asp:ListItem Value="1">จำนวนการจองห้อง/หน่วยงาน</asp:ListItem>
                                                                <asp:ListItem Value="2">จำนวนการยกเลิก/หน่วยงาน</asp:ListItem>
                                                            </asp:DropDownList>

                                                            <asp:RequiredFieldValidator ID="Re_ddlTypeGraph"
                                                                runat="server"
                                                                InitialValue="0"
                                                                ControlToValidate="ddlTypeGraph" Display="None" SetFocusOnError="true"
                                                                ErrorMessage="*กรุณาเลือกประเภทกราฟ"
                                                                ValidationGroup="SearchReportDetailGraph" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender512212" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlTypeGraph" Width="200" PopupPosition="BottomLeft" />


                                                        </div>
                                                        <label class="col-sm-2 control-label" runat="server" id="status_report" visible="false">ค้นหาจากสถานะ</label>
                                                        <div class="col-sm-4">
                                                            <asp:DropDownList ID="ddlStatusBooking" runat="server" CssClass="form-control" Visible="false">
                                                                <asp:ListItem Value="0">--- เลือกประเภทการค้นหา ---</asp:ListItem>
                                                                <asp:ListItem Value="3">เสร็จสมบูรณ์</asp:ListItem>
                                                                <asp:ListItem Value="2">ถูกยกเลิก</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">เดือน</label>
                                                        <div class="col-sm-4">
                                                            <asp:DropDownList ID="ddlmonth" runat="server" CssClass="form-control">
                                                                <asp:ListItem Value="0">--- เลือกเดือน ---</asp:ListItem>
                                                                <asp:ListItem Value="1">มกราคม</asp:ListItem>
                                                                <asp:ListItem Value="2">กุมภาพันธ์</asp:ListItem>
                                                                <asp:ListItem Value="3">มีนาคม</asp:ListItem>
                                                                <asp:ListItem Value="4">เมษายน</asp:ListItem>
                                                                <asp:ListItem Value="5">พฤษภาคม</asp:ListItem>
                                                                <asp:ListItem Value="6">มิถุนายน</asp:ListItem>
                                                                <asp:ListItem Value="7">กรกฎาคม</asp:ListItem>
                                                                <asp:ListItem Value="8">สิงหาคม</asp:ListItem>
                                                                <asp:ListItem Value="9">กันยายน</asp:ListItem>
                                                                <asp:ListItem Value="10">ตุลาคม</asp:ListItem>
                                                                <asp:ListItem Value="11">พฤศจิกายน</asp:ListItem>
                                                                <asp:ListItem Value="12">ธันวาคม</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>

                                                        <label class="col-sm-2 control-label">ปี</label>
                                                        <div class="col-sm-4">

                                                            <asp:DropDownList ID="ddlyear" CssClass="form-control" runat="server">
                                                            </asp:DropDownList>
                                                        </div>

                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label"></label>
                                                        <div class="col-sm-4">
                                                            <asp:LinkButton ID="btnSearchGraph" runat="server"
                                                                CssClass="btn btn-primary" OnCommand="btnCommand" CommandName="cmdSearchReportGraph" data-original-title="ค้นหา" data-toggle="tooltip" ValidationGroup="SearchReportDetailGraph"><span class="glyphicon glyphicon-search"></span> ค้นหา</asp:LinkButton>
                                                        </div>
                                                        <label class="col-sm-6 control-label"></label>
                                                    </div>


                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="btnSearchGraph" />
                                                    <asp:AsyncPostBackTrigger ControlID="ddlTypeGraph" EventName="SelectedIndexChanged" />
                                                </Triggers>

                                            </asp:UpdatePanel>
                                            <!-- START Report Graph -->

                                        </div>
                                    </div>

                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <!-- Panel Search Report -->

                        <!-- START Report Table -->

                        <!-- Detail Search Room Report-->
                        <asp:UpdatePanel ID="Update_PanelReportTable" runat="server" Visible="false">
                            <ContentTemplate>
                                <asp:GridView ID="GvDetailTableReport" runat="server"
                                    AutoGenerateColumns="False"
                                    CssClass="table table-striped table-bordered table-hover table-responsive"
                                    OnPageIndexChanging="gvPageIndexChanging"
                                    OnRowDataBound="gvRowDataBound"
                                    AllowPaging="True" PageSize="10">
                                    <HeaderStyle CssClass="success" Height="40px" Font-Size="Small" />
                                    <RowStyle Font-Size="Small" />
                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                    <EmptyDataTemplate>
                                        <div style="text-align: center">ไม่พบข้อมูล</div>
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:TemplateField HeaderText="ชื่อห้อง" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_m0_room_idx_reporttable" runat="server" Visible="false" Text='<%# Eval("m0_room_idx") %>'></asp:Label>
                                                <asp:Label ID="lbl_room_name_th_reporttable" runat="server" Text='<%# Eval("room_name_th") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ฝ่าย" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_rdept_idx_reporttable" runat="server" Visible="false" Text='<%# Eval("rdept_idx") %>'></asp:Label>
                                                <asp:Label ID="lbl_dept_name_th_reporttable" runat="server" Text='<%# Eval("dept_name_th") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="จำนวนครั้ง" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_count_booking_reporttable" runat="server" Text='<%# Eval("count_booking") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ดูข้อมูล" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                            <ItemTemplate>

                                                <asp:LinkButton ID="btnViewDetailReportTable" CssClass="btn-sm btn-info" runat="server" CommandName="cmdViewDetailReportTable"
                                                    OnCommand="btnCommand" CommandArgument='<%# Eval("m0_room_idx") + ";" + Eval("rdept_idx")%>'
                                                    data-toggle="tooltip" title="ดูข้อมูล"><i class="fa fa-file"></i></asp:LinkButton>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <!-- Detail Search Room Report-->

                        <!-- Back To Search Report-->
                        <asp:UpdatePanel ID="Update_PanelBackToSearch" runat="server" Visible="false">
                            <ContentTemplate>
                                <div class="form-group">
                                    <asp:LinkButton ID="btnBackToSearchReport" CssClass="btn btn-danger" data-toggle="tooltip" title="" runat="server"
                                        CommandName="cmdBackToSearchReport" OnCommand="btnCommand">< ย้อนกลับ</asp:LinkButton>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <!-- Back To Search Report-->

                        <!-- View Detail Room Report-->
                        <asp:UpdatePanel ID="Update_PanelDetailReportTable" runat="server" Visible="false">
                            <ContentTemplate>
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">รายงานการจองห้องประชุม </h3>
                                    </div>
                                    <div class="panel-body">

                                        <div class="form-group">
                                            <asp:UpdatePanel ID="updatepanelbutton" runat="server">
                                                <ContentTemplate>
                                                    <asp:LinkButton ID="btnExportDetailReportRoomDetail" runat="server"
                                                        CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdExportDetailReportRoomDetail" data-original-title="Export" data-toggle="tooltip" ValidationGroup="SearchReportDetail"><span class="glyphicon glyphicon-export"></span> Export</asp:LinkButton>

                                                </ContentTemplate>
                                                <Triggers>
                                                    <%--<asp:PostBackTrigger ControlID="btnPrintSampleCode" />--%>
                                                    <asp:PostBackTrigger ControlID="btnExportDetailReportRoomDetail" />
                                                </Triggers>

                                            </asp:UpdatePanel>
                                        </div>

                                        <asp:GridView ID="GvViewDetailReport" runat="server"
                                            AutoGenerateColumns="False"
                                            CssClass="table table-striped table-bordered table-hover table-responsive"
                                            OnPageIndexChanging="gvPageIndexChanging"
                                            OnRowDataBound="gvRowDataBound"
                                            AllowPaging="True" PageSize="10">
                                            <HeaderStyle CssClass="success" Height="40px" Font-Size="Small" />
                                            <RowStyle Font-Size="Small" />
                                            <PagerStyle CssClass="pageCustom" />
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                            <EmptyDataTemplate>
                                                <div style="text-align: center">ไม่พบข้อมูล</div>
                                            </EmptyDataTemplate>
                                            <Columns>
                                                <asp:TemplateField HeaderText="สถานที่" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_place_idx_reportview" runat="server" Visible="false" Text='<%# Eval("place_idx") %>'></asp:Label>
                                                        <asp:Label ID="lbl_place_name_reportview" runat="server" Text='<%# Eval("place_name") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="ชื่อห้อง" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_m0_room_idx_reportview" runat="server" Visible="false" Text='<%# Eval("m0_room_idx") %>'></asp:Label>
                                                        <asp:Label ID="lbl_room_name_th_reportview" runat="server" Text='<%# Eval("room_name_th") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="ฝ่าย" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_rdept_idx_reportview" Visible="false" runat="server" Text='<%# Eval("rdept_idx") %>'></asp:Label>
                                                        <asp:Label ID="lbl_dept_name_th_reportview" runat="server" Text='<%# Eval("dept_name_th") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="รายละเอียดการจอง" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                    <ItemTemplate>

                                                        <p>
                                                            <b>ตั้งแต่วันที่:</b>
                                                            <asp:Label ID="lbl_date_start_reportview" runat="server" Text='<%# Eval("date_start") %>' />
                                                            <b>เวลา:</b>
                                                            <asp:Label ID="lbl_time_start_reportview" runat="server" Text='<%# Eval("time_start") %>' />
                                                        </p>
                                                        <p>
                                                            <b>ถึงวันที่:</b>
                                                            <asp:Label ID="lbl_date_end_reportview" runat="server" Text='<%# Eval("date_end") %>' />
                                                            <b>เวลา:</b>
                                                            <asp:Label ID="lbl_time_end_reportview" runat="server" Text='<%# Eval("time_end") %>' />
                                                        </p>


                                                        <%-- <asp:Label ID="lbl_rdept_idx_reportview" runat="server" Text='<%# Eval("rdept_idx") %>'></asp:Label>
                                            <asp:Label ID="lbl_dept_name_th_reportview" runat="server" Text='<%# Eval("dept_name_th") %>'></asp:Label>--%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                        </asp:GridView>

                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <!-- View Detail Room Report-->

                        <!-- END Report Table -->

                        <asp:UpdatePanel ID="Update_PanelGraph" runat="server" Visible="false">
                            <ContentTemplate>
                                <div class="panel panel-success ">
                                    <div class="panel-heading">
                                        <h3 class="panel-title"><i class="glyphicon glyphicon-stats"></i><strong>&nbsp; ข้อมูลรายงาน</strong></h3>
                                    </div>
                                    <div class="panel-body">
                                        <asp:Literal ID="litReportChart" runat="server" />
                                        <asp:Literal ID="litReportChart1" runat="server" />
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:View>
        <!--View Report-->

    </asp:MultiView>
    <!--multiview-->

    <script type="text/javascript">
        function confirmWithOutValidated(message, validateGroup) {
            if (validateGroup != '') {
                var validated = Page_ClientValidate(validateGroup);
                if (validated) {
                    return confirm(message);
                }
            } else {
                return confirm(message);
            }
        }

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            //var dateToday = new Date();
            //currentTime = new Date();
            $('.datetimepicker-from').datetimepicker({

                format: 'DD/MM/YYYY',
                minDate: moment().add(-1, 'day'),
                maxDate: moment().add(60, 'day'),
                //ignoreReadonly: true

            });
            $('.datetimepicker-from').on('dp.change', function (e) {
                var dateTo = $('.datetimepicker-to').data("DateTimePicker").date();
                if (e.date > dateTo) {
                    $('.datetimepicker-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                //else {
                //    $('.datetimepicker-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                //}

            });
            $('.show-from-onclick').click(function () {
                $('.datetimepicker-from').data("DateTimePicker").show();
            });
            $('.datetimepicker-to').datetimepicker({
                format: 'DD/MM/YYYY',
                minDate: moment().add(-1, 'day'),
                maxDate: moment().add(60, 'day'),
                //ignoreReadonly: true
            });
            $('.show-to-onclick').click(function () {
                $('.datetimepicker-to').data("DateTimePicker").show();
            });
            $('.datetimepicker-to').on('dp.change', function (e) {
                var dateFrom = $('.datetimepicker-from').data("DateTimePicker").date();
                if (e.date < dateFrom) {
                    $('.datetimepicker-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                //else {
                //    $('.datetimepicker-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));

                //}
                //$('.txtSearchToHidden').val($('.datetimepicker-to').val());
                <%--$('#<%= txtSearchToHidden.ClientID %>').val($('.datetimepicker-to').val());--%>
            });

            /* START Filter DatetimePicker Permission Temporary */
            $('.datetimepicker-filter-from').datetimepicker({
                format: 'DD/MM/YYYY',
                //ignoreReadonly: true
            });
            $('.datetimepicker-filter-from').on('dp.change', function (e) {
                var dateTo = $('.datetimepicker-filter-to').data("DateTimePicker").date();
                if (e.date > dateTo) {
                    $('.datetimepicker-filter-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
               <%-- $('#<%= txtFilterIndexPermTempFromHidden.ClientID %>').val($('.datetimepicker-filter-from').val());--%>
            });
            $('.show-filter-from-onclick').click(function () {
                $('.datetimepicker-filter-from').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-to').datetimepicker({
                format: 'DD/MM/YYYY',
                //ignoreReadonly: true
            });
            $('.show-filter-to-onclick').click(function () {
                $('.datetimepicker-filter-to').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-to').on('dp.change', function (e) {
                var dateFrom = $('.datetimepicker-filter-from').data("DateTimePicker").date();
                if (e.date < dateFrom) {
                    $('.datetimepicker-filter-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                <%--$('#<%= txtFilterIndexPermTempToHidden.ClientID %>').val($('.datetimepicker-filter-to').val());--%>
            });
            $('.datetimepicker-filter-only').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });
            $('.show-filter-only-onclick').click(function () {
                $('.datetimepicker-filter-only').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-only').on('dp.change', function (e) {
                <%--$('#<%= txtFilterIndexPermTempOnlyHidden.ClientID %>').val($('.datetimepicker-filter-only').val());--%>
            });
            /* END Filter Datetimepicker Permission Temporary */
            /* START Filter Datetimepicker Permission Permanent */
            $('.datetimepicker-filter-perm-from').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });
            $('.datetimepicker-filter-perm-from').on('dp.change', function (e) {
                var dateTo = $('.datetimepicker-filter-perm-to').data("DateTimePicker").date();
                if (e.date > dateTo) {
                    $('.datetimepicker-filter-perm-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                <%--$('#<%= txtFilterIndexPermPermFromHidden.ClientID %>').val($('.datetimepicker-filter-perm-from').val());--%>
            });
            $('.show-filter-perm-from-onclick').click(function () {
                $('.datetimepicker-filter-perm-from').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-perm-to').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });
            $('.show-filter-perm-to-onclick').click(function () {
                $('.datetimepicker-filter-perm-to').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-perm-to').on('dp.change', function (e) {
                var dateFrom = $('.datetimepicker-filter-perm-from').data("DateTimePicker").date();
                if (e.date < dateFrom) {
                    $('.datetimepicker-filter-perm-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                <%--$('#<%= txtFilterIndexPermPermToHidden.ClientID %>').val($('.datetimepicker-filter-perm-to').val());--%>
            });
            $('.datetimepicker-filter-perm-only').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });
            $('.show-filter-perm-only-onclick').click(function () {
                $('.datetimepicker-filter-perm-only').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-perm-only').on('dp.change', function (e) {
                <%--$('#<%= txtFilterIndexPermPermOnlyHidden.ClientID %>').val($('.datetimepicker-filter-perm-only').val());--%>
            });
            /* END Filter Datetimepicker Permission Permanent */
        });
    </script>

    <script type="text/javascript">


        //no post back
        $('.clockpicker').datetimepicker({
            format: 'HH:mm',
            stepping: 30,
            ignoreReadonly: true
        });
        $('.show-time-onclick').click(function () {
            $('.clockpicker').data("DateTimePicker").show();
        });

        $('.clockpickerto').datetimepicker({
            format: 'HH:mm',
            stepping: 30,
            ignoreReadonly: true
        });
        $('.show-timeto-onclick').click(function () {
            $('.clockpickerto').data("DateTimePicker").show();
        });

        //is
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {

            $('.loading-icon-approve').hide();

            $('.clockpicker').datetimepicker({
                format: 'HH:mm',
                stepping: 30,
                ignoreReadonly: true
            });
            $('.show-time-onclick').click(function () {
                $('.clockpicker').data("DateTimePicker").show();
            });

            $('.clockpickerto').datetimepicker({
                format: 'HH:mm',
                stepping: 30,
                ignoreReadonly: true
            });
            $('.show-timeto-onclick').click(function () {
                $('.clockpickerto').data("DateTimePicker").show();
            });
        });


        function getAlertSuccess() {
            $("#success_alert").show();
            $("#success_alert").fadeTo(500, 500).slideUp(500, function () {
                $("#success_alert").slideUp(500);
            });
        }

        function ValidatePage(toHide, toShow) {

            $(toHide).hide();
            $(toShow).show();

        }
    </script>

    <script type="text/javascript">
        $(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });

        //});

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            });
        });

    </script>

    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.datepickerMont').datetimepicker({
                    format: "MM/YYYY",
                    ignoreReadonly: true

                });
            });
        });
        $(function () {
            $('.datepickerMont').datetimepicker({
                format: "MM/YYYY",
                ignoreReadonly: true
            });
        });
    </script>

    <script type="text/javascript">

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('#calendar').fullCalendar({
                    theme: true,
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'month,agendaWeek,agendaDay,listWeek,listDay'
                    },
                    nowIndicator: true,

                    views: {
                        listDay: { buttonText: 'list day' },
                        listWeek: { buttonText: 'list week' }
                    },

                    defaultView: 'month',
                    timeFormat: 'h:mm a',
                    allDayText: 'Time', // set replace all-day
                    selectable: false,
                    selectHelper: false,
                    //select: selectDate,
                    editable: false,
                    events: '<%= ResolveUrl("~/websystem/hr/JsonResponse.ashx") %>',
                    eventDrop: eventDropped,
                    eventResize: eventResized,
                    eventLimit: true,

                    eventClick: function (event, jsEvent, view) {
                        $('#modalTitle').html(
                            '<strong>' + 'หัวข้อ' + ' : ' + event.topic_booking + '</strong>'
                        );
                        $('#modalBody').html(
                            //event.description
                            '<strong>' + 'สถานที่ : ' + '</strong>' + event.place_name + '<br>'
                            + '<strong>' + 'ชื่อห้อง : ' + '</strong>' + event.room_name_th + '<br>'
                            + '<strong>' + 'ชื่อผู้จอง : ' + '</strong>' + event.emp_name_th + '<br>'
                            + '<strong>' + 'ตั้งแต่วันที่ : ' + '</strong>' + event.start.format("DD/MM/YYYY h:mm a") + '<br>'
                            + '<strong>' + 'ถึงวันที่ : ' + '</strong>' + event.end.format("DD/MM/YYYY h:mm a") + '<br>'
                            + '<strong>' + 'รายละเอียดเพิ่มเติม : ' + '</strong>' + event.description + '<br>'
                        );
                        $('#eventUrl').attr('href', event.url);
                        $('#fullCalModal').modal();
                    },


                    eventRender: function (event, element) {

                        var today = moment(new Date()).format("YYYY-MM-DD")
                        //var start = moment(event.start).format("YYYY-MM-DD");

                        var date_start = moment(event.start, "YYYY-MM-DD");
                        var date_end = moment(event.end, "YYYY-MM-DD");

                        if (event != null) {
                            element.qtip({
                                content: {
                                    text: qTipText(event.topic_booking, event.place_name, event.room_name_th, event.start, event.end, event.description),//qTipText(event.start, event.end, event.description, event.topic_booking),
                                    title: '<strong>' + 'หัวข้อ' + ' : ' + event.topic_booking + '</strong>'
                                    //title: '<strong>' + 'หัวข้อ' + ' : ' + event.title + '</strong>'
                                },
                                position: {
                                    my: 'top left',
                                    at: 'center left'
                                    //my: 'bottom left',
                                    //at: 'left center'
                                },
                                hide: {
                                    event: 'mouseout'
                                },
                                show: {
                                    event: 'mouseover'
                                },
                                style: {
                                    //classes: 'qtip-shadow qtip-rounded qtip-youtube'
                                    classes: 'qtip-tipsy'
                                }

                            });

                            var now = moment().format('YYYY-MM-DD HH:mm:ss');//"04/09/2013 15:00:00";
                            //var then = "2018-09-28 14:00:00";
                            var then = moment(event.start).format('YYYY-MM-DD HH:mm');
                            var then_end = moment(event.end).format('YYYY-MM-DD HH:mm');

                            var ms = moment(now, "YYYY-MM-DD HH:mm:ss").diff(moment(then, "YYYY-MM-DD HH:mm:ss"));
                            var d = moment.duration(ms);
                            //var s = Math.floor(d.asHours()) + moment.utc(ms).format(":mm:ss");
                            var s_start = Math.floor(d.asHours());

                            var ms_end = moment(now, "YYYY-MM-DD HH:mm:ss").diff(moment(then_end, "YYYY-MM-DD HH:mm:ss"));
                            var d_end = moment.duration(ms_end);
                            //var s = Math.floor(d.asHours()) + moment.utc(ms).format(":mm:ss");
                            var s_end = Math.floor(d_end.asHours());

                            //alert(s_start + ',' + moment(event.start).format('YYYY-MM-DD HH:mm') + ',' + s_end + ',' + moment(event.end).format('YYYY-MM-DD HH:mm'));
                            if ((s_start > 24 || s_start >= 16) && s_end > 0 || (s_start >= 24 && (s_end > 0 && s_end < 24))) {
                                //alert("1");
                                element.css('background-color', '#D02090');//pink
                                element.find(".fc-event-dot").css('background-color', '#D02090')
                            }
                            else if ((s_start > 0 && s_start < 24) || ((s_start > 0 || s_start >= -8 || s_start > 24) && s_end <= 0)) {
                                //alert("2");
                                element.css('background-color', '#00CC00');
                                element.find(".fc-event-dot").css('background-color', '#00CC00')//green
                                //element.find(".fc-event-dot").after($("<span class=\"fc-event-icons\"></span>").html("Whatever you want the content of the span to be")); 
                            }
                            else if (s_start < 0) {
                                //alert("3");
                                element.css('background-color', '#1E90FF');
                                element.find(".fc-event-dot").css('background-color', '#1E90FF')
                            }


                        }
                    }
                });
            });
        });
        $(function () {
            $('#calendar').fullCalendar({
                theme: true,
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay,listWeek,listDay'
                },
                nowIndicator: true,

                views: {
                    listDay: { buttonText: 'list day' },
                    listWeek: { buttonText: 'list week' }
                },

                defaultView: 'month',
                timeFormat: 'h:mm a',
                allDayText: 'Time', // set replace all-day
                //eventClick: updateEvent,
                selectable: false,
                selectHelper: false,
                //select: selectDate,
                editable: false,
                events: '<%= ResolveUrl("~/websystem/hr/JsonResponse.ashx") %>',
                eventDrop: eventDropped,
                eventResize: eventResized,
                eventLimit: true,

                eventClick: function (event, jsEvent, view) {
                    $('#modalTitle').html(
                        '<strong>' + 'หัวข้อ' + ' : ' + event.topic_booking + '</strong>'
                    );
                    $('#modalBody').html(
                        //event.description
                        '<strong>' + 'สถานที่ : ' + '</strong>' + event.place_name + '<br>'
                        + '<strong>' + 'ชื่อห้อง : ' + '</strong>' + event.room_name_th + '<br>'
                        + '<strong>' + 'ชื่อผู้จอง : ' + '</strong>' + event.emp_name_th + '<br>'
                        + '<strong>' + 'ตั้งแต่วันที่ : ' + '</strong>' + event.start.format("DD/MM/YYYY h:mm a") + '<br>'
                        + '<strong>' + 'ถึงวันที่ : ' + '</strong>' + event.end.format("DD/MM/YYYY h:mm a") + '<br>'
                        + '<strong>' + 'รายละเอียดเพิ่มเติม : ' + '</strong>' + event.description + '<br>'
                    );
                    $('#eventUrl').attr('href', event.url);
                    $('#fullCalModal').modal();
                },
                eventRender: function (event, element) {

                    var today = moment(new Date()).format("YYYY-MM-DD")
                    //var start = moment(event.start).format("YYYY-MM-DD");

                    var date_start = moment(event.start, "YYYY-MM-DD");
                    var date_end = moment(event.end, "YYYY-MM-DD");


                    //alert(event);
                    if (event != null) {
                        element.qtip({
                            content: {
                                text: qTipText(event.topic_booking, event.place_name, event.room_name_th, event.start, event.end, event.description),//qTipText(event.start, event.end, event.description, event.topic_booking),
                                title: '<strong>' + 'หัวข้อ' + ' : ' + event.topic_booking + '</strong>'
                                //title: '<strong>' + 'หัวข้อ' + ' : ' + event.title + '</strong>'
                            },
                            position: {
                                my: 'top left',
                                at: 'center left'
                                //my: 'bottom left',
                                //at: 'left center'
                            },
                            hide: {
                                event: 'mouseout'
                            },
                            show: {
                                event: 'mouseover'
                            },
                            style: {
                                //classes: 'qtip-shadow  qtip-youtube'
                                classes: 'qtip-tipsy'
                            }

                        });

                        var now = moment().format('YYYY-MM-DD HH:mm:ss');//"04/09/2013 15:00:00";
                        //var then = "2018-09-28 14:00:00";
                        var then = moment(event.start).format('YYYY-MM-DD HH:mm');
                        var then_end = moment(event.end).format('YYYY-MM-DD HH:mm');

                        var ms = moment(now, "YYYY-MM-DD HH:mm:ss").diff(moment(then, "YYYY-MM-DD HH:mm:ss"));
                        var d = moment.duration(ms);
                        //var s = Math.floor(d.asHours()) + moment.utc(ms).format(":mm:ss");
                        var s_start = Math.floor(d.asHours());

                        var ms_end = moment(now, "YYYY-MM-DD HH:mm:ss").diff(moment(then_end, "YYYY-MM-DD HH:mm:ss"));
                        var d_end = moment.duration(ms_end);
                        //var s = Math.floor(d.asHours()) + moment.utc(ms).format(":mm:ss");
                        var s_end = Math.floor(d_end.asHours());

                        //alert(s_start + ',' + moment(event.start).format('YYYY-MM-DD HH:mm') + ',' + s_end + ',' + moment(event.end).format('YYYY-MM-DD HH:mm'));
                        if ((s_start > 24 || s_start >= 16) && s_end > 0 || (s_start >= 24 && (s_end > 0 && s_end < 24))) {
                            //alert("1");
                            element.css('background-color', '#D02090');//pink
                            element.find(".fc-event-dot").css('background-color', '#D02090')
                        }
                        else if ((s_start > 0 && s_start < 24) || ((s_start > 0 || s_start >= -8 || s_start > 24) && s_end <= 0)) {
                            //alert("2");
                            element.css('background-color', '#00CC00');
                            element.find(".fc-event-dot").css('background-color', '#00CC00')//green
                            //element.find(".fc-event-dot").after($("<span class=\"fc-event-icons\"></span>").html("Whatever you want the content of the span to be")); 
                        }
                        else if (s_start < 0) {
                            //alert("3");
                            element.css('background-color', '#1E90FF');
                            element.find(".fc-event-dot").css('background-color', '#1E90FF')
                        }

                    }

                }
            });
        });

    </script>

</asp:Content>

