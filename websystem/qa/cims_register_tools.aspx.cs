using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
//using System.Drawing;
using System.Collections;

using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using NPOI.HSSF.UserModel;
using System.Globalization;
using System.Threading;

public partial class websystem_qa_cims_register_tools : System.Web.UI.Page
{

    #region Connect
    function_tool _funcTool = new function_tool();
    data_employee _dataEmployee = new data_employee();
    data_qa_cims _data_qa_cims = new data_qa_cims();


    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];

    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    static string _urlGetPositionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetPositionList"];


    //-- employee --//

    //-- master registration device --//
    static string _urlCimsGetRegistrationDevice = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetRegistrationDevice"];
    static string _urlCimsSetRegistrationDevice = _serviceUrl + ConfigurationManager.AppSettings["urlCimsSetRegistrationDevice"];
    static string _urlCimsGetEquipment_name = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetEquipment_name"];
    static string _urlCimsGetBrand = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetBrand"];
    static string _urlCimsGetEquipment_type = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetEquipment_type"];
    static string _urlCimsGetUnit = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetUnit"];
    static string _urlCimsGetCal_type = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetCal_type"];
    static string _urlCimsGetM1RegistrationDevice = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetM1RegistrationDevice"];
    static string _urlCimsSetM1RegistrationDevice = _serviceUrl + ConfigurationManager.AppSettings["urlCimsSetM1RegistrationDevice"];
    static string _urlCimsDeleteM1RegistrationDevice = _serviceUrl + ConfigurationManager.AppSettings["urlCimsDeleteM1RegistrationDevice"];
    static string _urlCimsSetM0RegistrationDevice = _serviceUrl + ConfigurationManager.AppSettings["urlCimsSetM0RegistrationDevice"];
    static string _urlCimsSetDocumentTranfer = _serviceUrl + ConfigurationManager.AppSettings["urlCimsSetDocumentTranfer"];
    static string _urlCimsGetLogRegistrationDevice = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetLogRegistrationDevice"];
    static string _urlCimsSearchRegistrationDevice = _serviceUrl + ConfigurationManager.AppSettings["urlCimsSearchRegistrationDevice"];
    static string _urlCimsGetRangeUseRegis = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetRangeUseRegis"];
    static string _urlCimsSetRangeUseDevice = _serviceUrl + ConfigurationManager.AppSettings["urlCimsSetRangeUseDevice"];
    static string _urlCimsGetmeasuringRegis = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetmeasuringRegis"];
    static string _urlCimsGetAcceptanceRegis = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetAcceptanceRegis"];
    static string _urlCimsSetMeasuringDevice = _serviceUrl + ConfigurationManager.AppSettings["urlCimsSetMeasuringDevice"];
    static string _urlCimsSetAcceptanceDevice = _serviceUrl + ConfigurationManager.AppSettings["urlCimsSetAcceptanceDevice"];
    static string _urlCimsGetLocationName = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetLocationName"];
    static string _urlCimsGetLocationZoneName = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetLocationZoneName"];
    static string _urlCimsGetReportDevices = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetReportDevices"];
    static string _urlCimsGetReportDetailExportDevices = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetReportDetailExportDevices"];
    static string _urlCimsGetPerManageQA = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetPerManageQA"];
    static string _urlCimsSetPermissionManageQA = _serviceUrl + ConfigurationManager.AppSettings["urlCimsSetPermissionManageQA"];
    static string _urlCimsGetPermissionManageQA = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetPermissionManageQA"];
    static string _urlCimsGetPermissionManagePlaceQA = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetPermissionManagePlaceQA"];
    static string _urlCimsGetPermissionManagePerQA = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetPermissionManagePerQA"];
    static string _urlCimsSetDelPermissionManagePerQA = _serviceUrl + ConfigurationManager.AppSettings["urlCimsSetDelPermissionManagePerQA"];
    static string _urlCimsGetPermissionManagePerQAView = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetPermissionManagePerQAView"];
    static string _urlCimsGetCheckPermissionQA = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetCheckPermissionQA"];



    //--document 
    static string _urlCimsGetSelectDecisionNode1 = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetSelectDecisionNode1"];
    static string _urlCimsGetDecisionTranfer = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetDecisionTranfer"];
    static string _urlCimsGetEquipmentList = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetEquipmentList"];
    static string _urlCimsGetDecisionCutoff = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetDecisionCutoff"];
    static string _urlCimsGetDocEquipmentList = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetDocEquipmentList"];
    static string _urlCimsGetDecisionHeadSection = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetDecisionHeadSection"];
    static string _urlCimsGetPlace = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetPlace"];
    static string _urlCimsGetDecisionQACutoff = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetDecisionQACutoff"];
    static string _urlCimsSetDocumentUpdate = _serviceUrl + ConfigurationManager.AppSettings["urlCimsSetDocumentUpdate"];
    static string _urlCimsGetSelectSearchTool = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetSelectSearchTool"];
    static string _urlCimsSetDocumentCalibration = _serviceUrl + ConfigurationManager.AppSettings["urlCimsSetDocumentCalibration"];
    static string _urlCimsSetDocumentUpdateHeadSection = _serviceUrl + ConfigurationManager.AppSettings["urlCimsSetDocumentUpdateHeadSection"];
    static string _urlCimsGetDecisionHeadQA = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetDecisionHeadQA"];
    static string _urlCimsSetDocumentUpdateHeadQA = _serviceUrl + ConfigurationManager.AppSettings["urlCimsSetDocumentUpdateHeadQA"];
    static string _urlCimsGetDecisionQATranfer = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetDecisionQATranfer"];
    static string _urlCimsGetLogEquipmentList = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetLogEquipmentList"];
    static string _urlCimsGetDecisionReceiver = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetDecisionReceiver"];
    static string _urlCimsGetDetailCreator = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetDetailCreator"];
    static string _urlCimsGetDocumentCalibration = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetDocumentCalibration"];
    static string _urlCimsGetDocumentDetails = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetDocumentDetails"];
    static string _urlCimsGetCalibrationList = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetCalibrationList"];
    static string _urlCimsGetLab = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetLab"];
    static string _urlCimsSetDocumentUpdateReceiver = _serviceUrl + ConfigurationManager.AppSettings["urlCimsSetDocumentUpdateReceiver"];
    static string _urlSearchInDocumentDevices = _serviceUrl + ConfigurationManager.AppSettings["urlSearchInDocumentDevices"];
    static string _urlCimsGetM0_Resolution = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetM0_Resolution"];
    static string _urlCimsGetM0_Frequency = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetM0_Frequency"];
    static string _urlCimsSetM0_Resolution = _serviceUrl + ConfigurationManager.AppSettings["urlSetM0_Resolution"];
    static string _urlCimsGetStatusDevices = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetStatusDevices"];



    int _emp_idx = 0;
    int _default_int = 0;
    int _r1Regis = 0;
    int ExternalID = 2;
    int ExternalCondition = 6;

    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    bool _flag_qmr = false;
    List<GetValueToRepeater> lst = new List<GetValueToRepeater>();

    //datable departmentexcel
    string calibrationpoint_dataexcel = "";
    string Temp_calibrationpoint_dataexcel = "";

    string status_devices_online = "1"; //Online
    string status_devices_offline = "2"; // Offline
    string status_devices_obsolate = "3"; // Obsolate



    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];

        ViewState["org_permission"] = _dataEmployee.employee_list[0].org_idx;
        ViewState["rdept_permission"] = _dataEmployee.employee_list[0].rdept_idx;
        ViewState["rsec_permission"] = _dataEmployee.employee_list[0].rsec_idx;
        ViewState["rpos_permission"] = _dataEmployee.employee_list[0].rpos_idx;
        ViewState["jobgrade_permission"] = _dataEmployee.employee_list[0].jobgrade_level;

    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            initPage();
            // initPageLoad();
            getPlacePerList();
            getPlaceListUpdate();

            getPerManageList();
            getPerListUpdate();

        }

        linkBtnTrigger(lbCreate);
        linkBtnTrigger(btnaddregis);

    }

    #region class default 
    protected void initPage()
    {
        dataSetCalibrationPoint();
        //setActiveTab("docRegistration", 0, 0, 0);
        setActiveTab("docRegistration", 0, 0, 0, 0, 0, 0);
        DataTableCalEquipmentOld();
        DataTableCalEquipmentNew();
        DataTableCalibrationPoint();
        DataTableCalibrationPointInsert();

        //insert register tools 
        DataTableResolution();
        DataTableFrequency();
        DataTableRangeOfUse();
        DataTableMeasuring();
        DataTableAcceptance();
    }

    #endregion

    protected void ClearGvRegisterDevices(string v_place, string v_lab)
    {
        data_qa_cims data_Detail_Index = new data_qa_cims();
        data_Detail_Index.qa_cims_search_registration_device_list = new qa_cims_search_registration_device[1];
        qa_cims_search_registration_device m0_devics_Index = new qa_cims_search_registration_device();

        m0_devics_Index.condition = 2;
        m0_devics_Index.cal_type_idx = int.Parse(v_lab);
        m0_devics_Index.place_idx = int.Parse(v_place);
        m0_devics_Index.rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
        m0_devics_Index.rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());

        data_Detail_Index.qa_cims_search_registration_device_list[0] = m0_devics_Index;

        data_Detail_Index = callServicePostMasterQACIMS(_urlCimsSearchRegistrationDevice, data_Detail_Index);

        if (data_Detail_Index.return_code == 0)
        {
            // fv_SearchIndex.Visible = true;
            show_gvregistration.Visible = true;
            ViewState["vsRegistrationDevice"] = data_Detail_Index.qa_cims_search_registration_device_list;
            setGridData(gvRegistrationList, ViewState["vsRegistrationDevice"]);

        }
        else
        {

            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่พบข้อมูล!!!');", true);
            //fv_SearchIndex.Visible = false;
            show_gvregistration.Visible = true;
            ViewState["vsRegistrationDevice"] = null;//data_SearchDetail.qa_cims_search_registration_device_list;
            setGridData(gvRegistrationList, ViewState["vsRegistrationDevice"]);

        }
    }

    #region clear data set 
    protected void ClearDataCalEquipmentOld()
    {
        //clear set equipment list
        ViewState["vsDataEquipmentCalOld"] = null;
        gvListEquipmentCalOld.DataSource = ViewState["vsDataEquipmentCalOld"];
        gvListEquipmentCalOld.DataBind();
        gvListEquipmentCalOld.Visible = false;

        DataTableCalEquipmentOld();

    }

    protected void ClearDataTextbox()
    {
        tbIDNo.Text = String.Empty;
        tbResolution.Text = String.Empty;
        tbserailnumber.Text = String.Empty;
        tbRangeOfUseMin.Text = String.Empty;
        tbRangeOfUseMax.Text = String.Empty;
        tbdetails_equipment.Text = String.Empty;
        tbMeasuringRangeMin.Text = String.Empty;
        tbMeasuringRangeMax.Text = String.Empty;
        tbAcceptanceCriteria.Text = String.Empty;
        tbCalibrationFrequency.Text = String.Empty;

    }

    protected void ClearDataCalibrationPoint()
    {
        //clear set equipment list
        ViewState["vsDataCalibrationPoint"] = null;
        gvCalibrationPoint.DataSource = ViewState["vsDataCalibrationPoint"];
        gvCalibrationPoint.DataBind();
        // gvCalibrationPoint.Visible = false;

        DataTableCalibrationPoint();

    }
    #endregion

    #region data set 

    protected void DataTableResolution()
    {
        var ds_resolution_data = new DataSet();
        ds_resolution_data.Tables.Add("DataResolution");
        ds_resolution_data.Tables[0].Columns.Add("ResolutionIDX", typeof(int));
        ds_resolution_data.Tables[0].Columns.Add("Resolution", typeof(String));
        ViewState["vsDataResolution"] = ds_resolution_data;

    }

    protected void DataTableFrequency()
    {
        var ds_frequency_data = new DataSet();
        ds_frequency_data.Tables.Add("DataFrequency");
        ds_frequency_data.Tables[0].Columns.Add("FrequencyIDX", typeof(int));
        ds_frequency_data.Tables[0].Columns.Add("Frequency", typeof(String));
        ViewState["vsDataFrequency"] = ds_frequency_data;

    }

    protected void DataTableRangeOfUse()
    {
        var ds_range_of_use_data = new DataSet();
        ds_range_of_use_data.Tables.Add("DataRangeOfUse");
        ds_range_of_use_data.Tables[0].Columns.Add("RangeOfUseMin", typeof(String));
        ds_range_of_use_data.Tables[0].Columns.Add("RangeOfUseMax", typeof(String));
        ds_range_of_use_data.Tables[0].Columns.Add("RangeOfUseUnit", typeof(String));
        ds_range_of_use_data.Tables[0].Columns.Add("RangeOfUseUnitIDX", typeof(int));
        ViewState["vsDataRangeOfUse"] = ds_range_of_use_data;

    }

    protected void DataTableMeasuring()
    {
        var ds_range_measuring_data = new DataSet();
        ds_range_measuring_data.Tables.Add("DataMeasuring");
        ds_range_measuring_data.Tables[0].Columns.Add("MeasuringMin", typeof(String));
        ds_range_measuring_data.Tables[0].Columns.Add("MeasuringMax", typeof(String));
        ds_range_measuring_data.Tables[0].Columns.Add("MeasuringUnit", typeof(String));
        ds_range_measuring_data.Tables[0].Columns.Add("MeasuringUnitIDX", typeof(int));
        ViewState["vsDataMeasuring"] = ds_range_measuring_data;

    }

    protected void DataTableAcceptance()
    {
        var ds_acceptance_data = new DataSet();
        ds_acceptance_data.Tables.Add("DataAcceptance");
        ds_acceptance_data.Tables[0].Columns.Add("Acceptance", typeof(String));
        ds_acceptance_data.Tables[0].Columns.Add("AcceptanceUnit", typeof(String));
        ds_acceptance_data.Tables[0].Columns.Add("AcceptanceUnitIDX", typeof(int));
        ViewState["vsDataAcceptance"] = ds_acceptance_data;

    }

    protected void DataTableCalibrationPoint()
    {

        var ds_calibration_point_data = new DataSet();
        ds_calibration_point_data.Tables.Add("CalibrationPointData");
        ds_calibration_point_data.Tables[0].Columns.Add("CalibrationPointUnitIDX", typeof(int));
        ds_calibration_point_data.Tables[0].Columns.Add("IDNUMBER", typeof(String));
        ds_calibration_point_data.Tables[0].Columns.Add("CalibrationPointUnit", typeof(String));
        ds_calibration_point_data.Tables[0].Columns.Add("CalibrationPoint", typeof(String));
        ViewState["vsDataCalibrationPoint"] = ds_calibration_point_data;
        gvCalibrationPoint.DataBind();

    }

    protected void DataTableCalibrationPointInsert()
    {

        var ds_calibration_point_data = new DataSet();
        ds_calibration_point_data.Tables.Add("CalibrationPointData_insert");
        ds_calibration_point_data.Tables[0].Columns.Add("CalibrationPointUnitIDX_insert", typeof(Decimal));
        ds_calibration_point_data.Tables[0].Columns.Add("IDNUMBER_insert", typeof(String));
        ds_calibration_point_data.Tables[0].Columns.Add("CalibrationPointUnit_insert", typeof(String));
        ds_calibration_point_data.Tables[0].Columns.Add("CalibrationPoint_insert", typeof(String));
        ds_calibration_point_data.Tables[0].Columns.Add("Serial_no_insert", typeof(String));

        ViewState["vsDataCalibrationPoint_insert"] = ds_calibration_point_data;
        gvCalPointInsert.DataBind();

    }

    protected void DataTableCalEquipmentNew()
    {
        var ds_equipment_new_data = new DataSet();
        ds_equipment_new_data.Tables.Add("EquipmentNewData");
        ds_equipment_new_data.Tables[0].Columns.Add("M0DeviceIDX_New", typeof(int));
        ds_equipment_new_data.Tables[0].Columns.Add("M0BrandIDX_New", typeof(int));
        ds_equipment_new_data.Tables[0].Columns.Add("EquipmentTypeIDX_New", typeof(int));
        ds_equipment_new_data.Tables[0].Columns.Add("HolderRsecIDX_New", typeof(int));
        ds_equipment_new_data.Tables[0].Columns.Add("HolderOrgIDX_New", typeof(int));
        ds_equipment_new_data.Tables[0].Columns.Add("HolderRdeptIDX_New", typeof(int));
        ds_equipment_new_data.Tables[0].Columns.Add("RangeOfUseMin", typeof(decimal));
        ds_equipment_new_data.Tables[0].Columns.Add("RangeOfUseMax", typeof(decimal));
        ds_equipment_new_data.Tables[0].Columns.Add("RangeOfUseUnit", typeof(int));
        ds_equipment_new_data.Tables[0].Columns.Add("CalibrationType", typeof(int));
        ds_equipment_new_data.Tables[0].Columns.Add("Acceptance_Criteria", typeof(decimal));
        ds_equipment_new_data.Tables[0].Columns.Add("Acceptance_Criteria_Unit", typeof(int));
        ds_equipment_new_data.Tables[0].Columns.Add("Resolution", typeof(decimal));
        ds_equipment_new_data.Tables[0].Columns.Add("Resolution_Unit", typeof(int));
        ds_equipment_new_data.Tables[0].Columns.Add("Measuring_Range_Min", typeof(decimal));
        ds_equipment_new_data.Tables[0].Columns.Add("Measuring_Range_Max", typeof(decimal));
        ds_equipment_new_data.Tables[0].Columns.Add("Measuring_Range_Unit", typeof(int));
        ds_equipment_new_data.Tables[0].Columns.Add("CalibrationPoint", typeof(int));
        ds_equipment_new_data.Tables[0].Columns.Add("CalibrationPoint_Unit", typeof(int));
        ds_equipment_new_data.Tables[0].Columns.Add("Calibration_Frequency", typeof(decimal));
        ds_equipment_new_data.Tables[0].Columns.Add("Certificate_insert", typeof(int));

        ds_equipment_new_data.Tables[0].Columns.Add("IDCodeNumber_New", typeof(String));
        ds_equipment_new_data.Tables[0].Columns.Add("EquipmentName_New", typeof(String));
        ds_equipment_new_data.Tables[0].Columns.Add("EquipmentType_New", typeof(String));
        ds_equipment_new_data.Tables[0].Columns.Add("BrandName_New", typeof(String));
        ds_equipment_new_data.Tables[0].Columns.Add("Holder_New", typeof(String));
        ds_equipment_new_data.Tables[0].Columns.Add("SerialNumber_New", typeof(String));
        ds_equipment_new_data.Tables[0].Columns.Add("RangeOfUseUnitName", typeof(String));
        ds_equipment_new_data.Tables[0].Columns.Add("Details_Equipment", typeof(String));
        ds_equipment_new_data.Tables[0].Columns.Add("CalibrationTypeName", typeof(String));
        ds_equipment_new_data.Tables[0].Columns.Add("Acceptance_Criteria_UnitName", typeof(String));
        ds_equipment_new_data.Tables[0].Columns.Add("Resolution_UnitName", typeof(String));
        ds_equipment_new_data.Tables[0].Columns.Add("CalibrationPoint_UnitName", typeof(String));
        ds_equipment_new_data.Tables[0].Columns.Add("Measuring_Range_UnitName", typeof(String));
        ds_equipment_new_data.Tables[0].Columns.Add("CalibrationPointList", typeof(String));
        ds_equipment_new_data.Tables[0].Columns.Add("CalibrationPointList_IDX", typeof(String));


        ViewState["vsDataEquipmentCalNew"] = ds_equipment_new_data;
    }

    protected void DataTableCalEquipmentOld()
    {
        var ds_equipmentold_data = new DataSet();
        ds_equipmentold_data.Tables.Add("EquipmentOldData");
        ds_equipmentold_data.Tables[0].Columns.Add("M0DeviceIDX", typeof(int));
        ds_equipmentold_data.Tables[0].Columns.Add("IDCodeNumber", typeof(String));
        ds_equipmentold_data.Tables[0].Columns.Add("EquipmentName", typeof(String));
        ds_equipmentold_data.Tables[0].Columns.Add("EquipmentType", typeof(String));
        ds_equipmentold_data.Tables[0].Columns.Add("BrandName", typeof(String));
        ds_equipmentold_data.Tables[0].Columns.Add("Holder", typeof(String));
        ds_equipmentold_data.Tables[0].Columns.Add("calDate", typeof(String));
        ds_equipmentold_data.Tables[0].Columns.Add("DueDate", typeof(String));
        ds_equipmentold_data.Tables[0].Columns.Add("SerialNumber", typeof(String));
        ViewState["vsDataEquipmentCalOld"] = ds_equipmentold_data;

    }
    #endregion

    #region event command

    protected void setRepeaterData(Repeater rptName, Object obj)
    {
        rptName.DataSource = obj;
        rptName.DataBind();
    }

    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }


    protected void navCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        setActiveTab(cmdArg, 0, 0, 0, 0, 0, 0);

    }

    protected void setActiveTab(string activeTab, int uidx, int u1idx, int staidx, int place, int type_equipment, int certificate)
    {
        setActiveView(activeTab, uidx, u1idx, staidx, place, type_equipment, certificate);
        switch (activeTab)
        {
            case "docCreate":
                li0.Attributes.Add("class", "active");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");
                li5.Attributes.Add("class", "");

                break;
            case "docDetailList":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "active");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");
                li5.Attributes.Add("class", "");
                break;
            case "docRegistration":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "active");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");
                li5.Attributes.Add("class", "");

                break;
            case "docList":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "active");
                li4.Attributes.Add("class", "");
                li5.Attributes.Add("class", "");

                break;
            case "docReport":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "active");
                li5.Attributes.Add("class", "");

                break;
            case "docManagement":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");
                li5.Attributes.Add("class", "active");

                break;


        }
    }

    protected void setActiveView(string activeTab, int uidx, int u1idx, int staidx, int place, int type_equipment, int certificate)
    {
        mvSystem.SetActiveView((View)mvSystem.FindControl(activeTab));

        //document cal
        setFormData(fvShowDetailsSearchTool, FormViewMode.ReadOnly, null);
        setFormData(fvDetailsDocument, FormViewMode.ReadOnly, null);
        setFormData(fvCalDocumentList, FormViewMode.ReadOnly, null);
        divActionSaveCreateDocument.Visible = false;
        pnEquipmentNew.Visible = false;

        setFormData(fvDetailUserInPageRegistration, FormViewMode.ReadOnly, null);
        setFormData(fvInsertCIMS, FormViewMode.ReadOnly, null);
        viewRegistration.Visible = false;
        divTranferRegistrationList.Visible = false;
        divCutoffRegistrationList.Visible = false;
        setRepeaterData(rp_place_document, null);
        divdocumentList.Visible = true;
        divFVVQACutoff.Visible = false;
        divFVVQATranfer.Visible = false;
        divFVVAllCutoff.Visible = false;
        divFVVAllTranfer.Visible = false;
        CheckAllQACutoff.Checked = false;
        btnBackToDocList.Visible = false;
        divDocDetailUser.Visible = false;
        div_detailTranfer.Visible = false;
        div_LogTranfer_wait.Visible = false;
        div_successTranfer.Visible = false;
        div_detailwaitTranfer.Visible = false;
        div_Detail_CutSuccess.Visible = false;
        div_Log_DetailCut_Sucess.Visible = false;


        divAlertTranferSeacrh.Visible = false;
        divAlertCutoffSeacrh.Visible = false;
        setFormData(fvDetailSearchCutoff, FormViewMode.ReadOnly, null);
        setFormData(fvDetailSearchTranfer, FormViewMode.ReadOnly, null);

        //Report
        //div_SearchReport.Visible = false;

        //Mangement
        //div_Management.Visible = false;

        switch (activeTab)
        {
            case "docCreate":

                setFormData(fvEmpDetail, FormViewMode.ReadOnly, ((data_employee)ViewState["vsEmpProfile"]).employee_list);

                if (uidx == 0)
                {
                    //select node decision node 1
                    divSelectTypeEquipment.Visible = true;
                    data_qa_cims dataCimsDecisionNode1 = new data_qa_cims();
                    dataCimsDecisionNode1.qa_cims_bindnode_decision_list = new qa_cims_bindnode_decision_detail[1];
                    qa_cims_bindnode_decision_detail _selectDecisionNode1 = new qa_cims_bindnode_decision_detail();
                    _selectDecisionNode1.condition = 0;
                    dataCimsDecisionNode1.qa_cims_bindnode_decision_list[0] = _selectDecisionNode1;

                    dataCimsDecisionNode1 = callServicePostQaDocCIMS(_urlCimsGetSelectDecisionNode1, dataCimsDecisionNode1);

                    rblTypeEquipment.DataSource = dataCimsDecisionNode1.qa_cims_bindnode_decision_list;
                    rblTypeEquipment.DataTextField = "decision_name";
                    rblTypeEquipment.DataValueField = "decision_idx";
                    rblTypeEquipment.DataBind();
                    rblTypeEquipment.SelectedValue = "2";
                    getSelectPlace(ddlPlaceCreate, "0", "0");
                    tbsearchingToolID.Focus();
                    linkBtnTrigger(btnSearchToolID);
                    ClearDataCalEquipmentOld();
                    ClearDataCalibrationPoint();
                    ClearDataTextbox();

                }
                else if (uidx > 0)
                {

                    data_qa_cims _dataCims = new data_qa_cims();
                    divSelectTypeEquipment.Visible = false;
                    //details document
                    _dataCims.qa_cims_calibration_document_list = new qa_cims_calibration_document_details[1];
                    qa_cims_calibration_document_details _selectDetails = new qa_cims_calibration_document_details();
                    _selectDetails.u0_cal_idx = uidx;
                    _dataCims.qa_cims_calibration_document_list[0] = _selectDetails;

                    _dataCims = callServicePostQaDocCIMS(_urlCimsGetDocumentDetails, _dataCims);
                    setFormData(fvDetailsDocument, FormViewMode.ReadOnly, _dataCims.qa_cims_calibration_document_list);

                    //list equipment
                    data_qa_cims _dataCimslist = new data_qa_cims();
                    _dataCimslist.qa_cims_u1_calibration_document_list = new qa_cims_u1_calibration_document_details[1];
                    qa_cims_u1_calibration_document_details _selectList = new qa_cims_u1_calibration_document_details();
                    _selectList.u0_cal_idx = uidx;
                    _dataCimslist.qa_cims_u1_calibration_document_list[0] = _selectList;

                    _dataCimslist = callServicePostQaDocCIMS(_urlCimsGetCalibrationList, _dataCimslist);
                    if (_dataCimslist.qa_cims_u1_calibration_document_list != null)
                    {
                        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataCimslist));
                        setFormData(fvCalDocumentList, FormViewMode.ReadOnly, _dataCimslist.qa_cims_u1_calibration_document_list);
                        var gvCalibrationList = (GridView)fvCalDocumentList.FindControl("gvCalibrationList");
                        setGridData(gvCalibrationList, _dataCimslist.qa_cims_u1_calibration_document_list);
                    }
                    else
                    {
                        //litDebug.Text = "llll";
                    }

                    if (u1idx == 0)
                    {

                    }
                    else if (u1idx > 0)
                    {
                        //litDebug.Text = "Record Certificate";
                        setFormData(fvCalDocumentList, FormViewMode.ReadOnly, null);
                    }

                }

                break;

            case "docDetailList":

                data_qa_cims dataCimsList = new data_qa_cims();

                if (uidx == 0)
                {

                    dataCimsList.qa_cims_calibration_document_list = new qa_cims_calibration_document_details[1];
                    qa_cims_calibration_document_details _selectDocCal = new qa_cims_calibration_document_details();
                    dataCimsList.qa_cims_calibration_document_list[0] = _selectDocCal;

                    dataCimsList = callServicePostQaDocCIMS(_urlCimsGetDocumentCalibration, dataCimsList);
                    ViewState["vsGetDocumentCalibration"] = dataCimsList.qa_cims_calibration_document_list;
                    setGridData(gvCalibrationDocument, ViewState["vsGetDocumentCalibration"]);
                }

                break;

            case "docRegistration":


                div_SearchIndex_1.Visible = true;

                if (certificate == 0)
                {
                    divRegistrationList.Visible = true;
                    div_SearchIndex_1.Visible = true;
                    fv_SearchIndex.Visible = false;
                    show_gvregistration.Visible = false;


                    if (tbPlaceIDXInDocument.Text != "" && tbLabIDXInDocument.Text != "")
                    {

                        getM0CalType((DropDownList)docRegistration.FindControl("dllcaltype_search"), tbLabIDXInDocument.Text);
                        getSelectPlace((DropDownList)docRegistration.FindControl("ddlPlaceMachine_search"), tbPlaceIDXInDocument.Text, "0");

                    }
                    else
                    {
                        tbPlaceIDXInDocument.Text = "0";
                        tbPlaceNameInDocument.Text = "";

                        tbLabIDXInDocument.Text = "0";
                        tbLabNameInDocument.Text = "";

                        getM0CalType((DropDownList)docRegistration.FindControl("dllcaltype_search"), "0");
                        getSelectPlace((DropDownList)docRegistration.FindControl("ddlPlaceMachine_search"), "0", "0");


                    }


                    if ((tbPlaceIDXInDocument.Text != "" && tbLabIDXInDocument.Text != "" && tbPlaceIDXInDocument.Text != "0" && tbLabIDXInDocument.Text != "0"))
                    {

                        //litDebug.Text = "88888";
                        data_qa_cims data_SearchDetail = new data_qa_cims();
                        data_SearchDetail.qa_cims_search_registration_device_list = new qa_cims_search_registration_device[1];
                        qa_cims_search_registration_device m0_SearchRegistration1 = new qa_cims_search_registration_device();

                        m0_SearchRegistration1.condition = 2;
                        m0_SearchRegistration1.cal_type_idx = int.Parse(dllcaltype_search.SelectedValue);
                        m0_SearchRegistration1.place_idx = int.Parse(ddlPlaceMachine_search.SelectedValue);
                        m0_SearchRegistration1.rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
                        m0_SearchRegistration1.rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());

                        data_SearchDetail.qa_cims_search_registration_device_list[0] = m0_SearchRegistration1;

                        data_SearchDetail = callServicePostMasterQACIMS(_urlCimsSearchRegistrationDevice, data_SearchDetail);

                        if (data_SearchDetail.return_code == 0)
                        {
                            divRegistrationList.Visible = true;
                            div_SearchIndex.Visible = true;
                            fv_SearchIndex.Visible = true;
                            btnsearchshow.Visible = true;
                            show_gvregistration.Visible = true;
                            ViewState["vsRegistrationDevice"] = data_SearchDetail.qa_cims_search_registration_device_list;
                            setGridData(gvRegistrationList, ViewState["vsRegistrationDevice"]);

                        }
                        else
                        {

                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่พบข้อมูล!!!');", true);
                            fv_SearchIndex.Visible = false;
                            show_gvregistration.Visible = true;
                            ViewState["vsRegistrationDevice"] = null;//data_SearchDetail.qa_cims_search_registration_device_list;
                            setGridData(gvRegistrationList, ViewState["vsRegistrationDevice"]);

                        }
                    }

                    btnhiddensearch.Visible = false;
                    TabSearch.Visible = false;

                    divTranferRegistrationList.Visible = false;
                    divCutoffRegistrationList.Visible = false;
                    viewRegistration.Visible = false;

                }


                break;

            case "docList":
                div_SearchDoc.Visible = true;
                txtseach_samplecode_lab.Text = String.Empty;

                if (tbPlaceIDXInDocument.Text != "" && tbLabIDXInDocument.Text != "")
                {
                    //litDebug.Text = "3333";
                    getM0CalType((DropDownList)docList.FindControl("dllcaltype_searchDoc"), tbLabIDXInDocument.Text);
                    getSelectPlace((DropDownList)docList.FindControl("ddlPlaceMachine_searchDoc"), tbPlaceIDXInDocument.Text, "0");

                }
                else
                {
                    //litDebug.Text = "444";
                    tbPlaceIDXInDocument.Text = "0";
                    tbPlaceNameInDocument.Text = "";

                    tbLabIDXInDocument.Text = "0";
                    tbLabNameInDocument.Text = "";

                    getM0CalType((DropDownList)docList.FindControl("dllcaltype_searchDoc"), "0");
                    getSelectPlace((DropDownList)docList.FindControl("ddlPlaceMachine_searchDoc"), "0", "0");

                    divFVVQACutoff.Visible = false;
                    divDocDetailUser.Visible = false;
                    divFVVQACutoff.Visible = false;
                    divFVVQATranfer.Visible = false;
                    divFVVAllCutoff.Visible = false;
                    divFVVAllTranfer.Visible = false;
                    divPassU1.Visible = false;

                    div_SearchDoc.Visible = true;

                }

                //if (tbPlaceIDXInDocument.Text != "0" && tbLabIDXInDocument.Text != "0
                if ((tbPlaceIDXInDocument.Text != "" && tbLabIDXInDocument.Text != "" && tbPlaceIDXInDocument.Text != "0" && tbLabIDXInDocument.Text != "0"))
                {

                    data_qa_cims _dataSelectPlaceDoc_backpage = new data_qa_cims();

                    if (ViewState["rdept_permission"].ToString() == "27")
                    {
                        //litDebug.Text = "9999";
                        //litDebug.Text = "4444";
                        //data_qa_cims _dataSelectPlaceDoc_backpage = new data_qa_cims();
                        _dataSelectPlaceDoc_backpage.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
                        qa_cims_u1_document_device_detail _select_place_doc = new qa_cims_u1_document_device_detail();
                        _select_place_doc.cal_type_idx = int.Parse(tbLabIDXInDocument.Text);
                        _select_place_doc.place_idx = int.Parse(tbPlaceIDXInDocument.Text);
                        _select_place_doc.condition = 6;
                        _dataSelectPlaceDoc_backpage.qa_cims_u1_document_device_list[0] = _select_place_doc;
                        _dataSelectPlaceDoc_backpage = callServicePostQaDocCIMS(_urlCimsGetEquipmentList, _dataSelectPlaceDoc_backpage);

                        if (_dataSelectPlaceDoc_backpage.return_code == 0)
                        {
                            divSearchDocument.Visible = true;
                            divdocumentList.Visible = true;
                            div_SearchDoc.Visible = true;

                            gvDocumentList.PageIndex = 0;
                            ViewState["vsDocument"] = _dataSelectPlaceDoc_backpage.qa_cims_u0_document_device_list;
                            setGridData(gvDocumentList, ViewState["vsDocument"]);
                        }
                        else
                        {
                            //gvDocumentList.PageIndex = 0;


                            divSearchDocument.Visible = false;
                            divdocumentList.Visible = true;
                            div_SearchDoc.Visible = true;

                            //div_SearchDoc.Visible = false;
                            ViewState["vsDocument"] = null;//_dataSelectPlaceDoc_backpage.qa_cims_u0_document_device_list;
                            setGridData(gvDocumentList, ViewState["vsDocument"]);
                        }

                    }
                    else
                    {
                        //litDebug.Text = "666";
                        //data_qa_cims _dataSelectPlaceDoc_backpage = new data_qa_cims();
                        _dataSelectPlaceDoc_backpage.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
                        qa_cims_u1_document_device_detail _select_place_doc = new qa_cims_u1_document_device_detail();
                        _select_place_doc.cal_type_idx = int.Parse(tbLabIDXInDocument.Text);
                        _select_place_doc.place_idx = int.Parse(tbPlaceIDXInDocument.Text);
                        _select_place_doc.condition = 8;
                        _select_place_doc.rsec_idx_create = int.Parse(ViewState["rsec_permission"].ToString());
                        _dataSelectPlaceDoc_backpage.qa_cims_u1_document_device_list[0] = _select_place_doc;
                        _dataSelectPlaceDoc_backpage = callServicePostQaDocCIMS(_urlCimsGetEquipmentList, _dataSelectPlaceDoc_backpage);


                        if (_dataSelectPlaceDoc_backpage.return_code == 0)
                        {

                            divSearchDocument.Visible = true;
                            divdocumentList.Visible = true;
                            div_SearchDoc.Visible = true;

                            gvDocumentList.PageIndex = 0;
                            ViewState["vsDocument"] = _dataSelectPlaceDoc_backpage.qa_cims_u0_document_device_list;
                            setGridData(gvDocumentList, ViewState["vsDocument"]);
                        }
                        else
                        {
                            //gvDocumentList.PageIndex = 0;
                            //div_SearchDoc.Visible = false;

                            divSearchDocument.Visible = false;
                            divdocumentList.Visible = true;
                            div_SearchDoc.Visible = true;

                            ViewState["vsDocument"] = null;//_dataSelectPlaceDoc_backpage.qa_cims_u0_document_device_list;
                            setGridData(gvDocumentList, ViewState["vsDocument"]);
                        }


                    }

                    if (ViewState["rdept_permission"].ToString() == "20" && _emp_idx == 3760
                           || ViewState["rdept_permission"].ToString() == "27" && int.Parse(ViewState["jobgrade_permission"].ToString()) >= 6
                           || ViewState["rdept_permission"].ToString() == "20" && _emp_idx == 1413)
                    {
                      //  gvDocumentList.Columns[6].Visible = true;
                    }
                    else
                    {
                        //gvDocumentList.Columns[6].Visible = false;
                    }


                }
                else
                {
                    divSearchDocument.Visible = false;
                    divdocumentList.Visible = false;
                }

                break;

            case "docReport":
                div_SearchReportOnline.Visible = false;
                div_SearchReportOffline.Visible = false;
                div_SearchReportCutoff.Visible = false;

                div_ReportDetail_Online.Visible = false;
                div_ReportDetail_Offline.Visible = false;
                div_ReportDetail_Cutoff.Visible = false;


                switch (certificate)
                {
                    case 1: // Report Devices Online

                        div_SearchReportOnline.Visible = true;
                        getOrganizationList(ddl_orgidx_report);
                        getDepartmentList(ddl_rdeptidx_report, int.Parse(ddl_orgidx_report.SelectedValue));
                        getSectionList(ddl_rsecidx_report, int.Parse(ddl_orgidx_report.SelectedValue), int.Parse(ddl_rdeptidx_report.SelectedValue));


                        data_qa_cims _data_report_online = new data_qa_cims();
                        _data_report_online.qa_cims_reportdevices_list = new qa_cims_reportdevices_detail[1];
                        qa_cims_reportdevices_detail m0_report_list = new qa_cims_reportdevices_detail();

                        m0_report_list.condition = 1;

                        _data_report_online.qa_cims_reportdevices_list[0] = m0_report_list;
                        _data_report_online = callServicePostMasterQACIMS(_urlCimsGetReportDevices, _data_report_online);

                        ViewState["vs_ReportDevices_Online"] = _data_report_online.qa_cims_reportdevices_list;
                        setGridData(gvReportDevicesOnline, ViewState["vs_ReportDevices_Online"]);
                        //selectResolution(ViewState["vsM0DeviceIDX"].ToString());

                        break;


                    case 2: //Report Devices Offline
                        div_SearchReportOffline.Visible = true;

                        getOrganizationList(ddl_orgidx_report_offline);
                        getDepartmentList(ddl_rdeptidx_report_offline, int.Parse(ddl_orgidx_report_offline.SelectedValue));
                        getSectionList(ddl_rsecidx_report_offline, int.Parse(ddl_orgidx_report_offline.SelectedValue), int.Parse(ddl_rdeptidx_report_offline.SelectedValue));

                        data_qa_cims _data_report_offline = new data_qa_cims();
                        _data_report_offline.qa_cims_reportdevices_list = new qa_cims_reportdevices_detail[1];
                        qa_cims_reportdevices_detail m0_reportoffline_list = new qa_cims_reportdevices_detail();

                        m0_reportoffline_list.condition = 2;

                        _data_report_offline.qa_cims_reportdevices_list[0] = m0_reportoffline_list;
                        _data_report_offline = callServicePostMasterQACIMS(_urlCimsGetReportDevices, _data_report_offline);

                        ViewState["vs_ReportDevices_Offline"] = _data_report_offline.qa_cims_reportdevices_list;
                        setGridData(gvReportDevicesOffline, ViewState["vs_ReportDevices_Offline"]);


                        break;

                    case 3: //Report Devices Cut off
                        div_SearchReportCutoff.Visible = true;

                        getOrganizationList(ddl_orgidx_report_cutoff);
                        getDepartmentList(ddl_rdeptidx_report_cutoff, int.Parse(ddl_orgidx_report_cutoff.SelectedValue));
                        getSectionList(ddl_rsecidx_report_cutoff, int.Parse(ddl_orgidx_report_cutoff.SelectedValue), int.Parse(ddl_rdeptidx_report_cutoff.SelectedValue));

                        data_qa_cims _data_report_cutoff = new data_qa_cims();
                        _data_report_cutoff.qa_cims_reportdevices_list = new qa_cims_reportdevices_detail[1];
                        qa_cims_reportdevices_detail m0_reportcutoff_list = new qa_cims_reportdevices_detail();

                        m0_reportcutoff_list.condition = 3;

                        _data_report_cutoff.qa_cims_reportdevices_list[0] = m0_reportcutoff_list;
                        _data_report_cutoff = callServicePostMasterQACIMS(_urlCimsGetReportDevices, _data_report_cutoff);

                        ViewState["vs_ReportDevices_Cutoff"] = _data_report_cutoff.qa_cims_reportdevices_list;
                        setGridData(gvReportDevicesCutoff, ViewState["vs_ReportDevices_Cutoff"]);

                        break;
                }


                break;
            case "docManagement":

                GvPerManagement.Visible = true;
                PanelInertPerManageQA.Visible = true;
                div_btncancel.Visible = false;
                fvInsertPerQA.Visible = false;
                setFormData(fvInsertPerQA, FormViewMode.ReadOnly, null);

                actionPermissionManage();
                //if(ViewState["vsPlaceListUpdate"] != null)
                //{
                //    CleardataSetPlaceListUpdate();
                //}
                //else
                //{

                //}


                //fvInsertPerQA.ChangeMode(FormViewMode.ReadOnly);


                break;
        }
    }


    protected void SelectCalibration_point(string cmdArgUserForUpdate)
    {
        data_qa_cims _data_M1 = new data_qa_cims();
        _data_M1.qa_cims_m1_registration_device_list = new qa_cims_m1_registration_device[1];
        qa_cims_m1_registration_device _selectM1Registration = new qa_cims_m1_registration_device();

        _selectM1Registration.m0_device_idx = int.Parse(cmdArgUserForUpdate);

        _data_M1.qa_cims_m1_registration_device_list[0] = _selectM1Registration;

        _data_M1 = callServicePostMasterQACIMS(_urlCimsGetM1RegistrationDevice, _data_M1);

        GridView GvCalPointEdit = (GridView)fvInsertCIMS.FindControl("GvCalPointEdit");

        setGridData(GvCalPointEdit, _data_M1.qa_cims_m1_registration_device_list);
    }

    protected void selectResolution(string ResolutionForUpdate)
    {
        data_qa_cims _data_resolution = new data_qa_cims();
        _data_resolution.qa_cims_m0_resolution_list = new qa_cims_m0_resulution_detail[1];
        qa_cims_m0_resulution_detail _ResolutionIDX = new qa_cims_m0_resulution_detail();

        _ResolutionIDX.m0_device_idx = int.Parse(ResolutionForUpdate);
        _ResolutionIDX.condition = 2;

        _data_resolution.qa_cims_m0_resolution_list[0] = _ResolutionIDX;
        _data_resolution = callServicePostMasterQACIMS(_urlCimsGetM0_Resolution, _data_resolution);

        GridView gvResolutionUpdate = (GridView)fvInsertCIMS.FindControl("gvResolutionUpdate");
        setGridData(gvResolutionUpdate, _data_resolution.qa_cims_m0_resolution_list);
    }

    protected void selectgvRangeUpdate(string v_RangeUpdate)
    {
        data_qa_cims _data_rangeuse = new data_qa_cims();
        _data_rangeuse.qa_cims_m0_rangeuse_list = new qa_cims_m0_rangeuse_detail[1];
        qa_cims_m0_rangeuse_detail _m0_rangeuse_list = new qa_cims_m0_rangeuse_detail();

        _m0_rangeuse_list.m0_device_idx = int.Parse(v_RangeUpdate);
        //_ResolutionIDX.condition = 2;

        _data_rangeuse.qa_cims_m0_rangeuse_list[0] = _m0_rangeuse_list;
        _data_rangeuse = callServicePostMasterQACIMS(_urlCimsGetRangeUseRegis, _data_rangeuse);

        GridView gvRangeUpdate = (GridView)fvInsertCIMS.FindControl("gvRangeUpdate");
        setGridData(gvRangeUpdate, _data_rangeuse.qa_cims_m0_rangeuse_list);

    }

    protected void selectgvRangePeview(string Peview)
    {
        //Rangeuse Preview
        data_qa_cims _data_rangeuse_peview = new data_qa_cims();
        _data_rangeuse_peview.qa_cims_m0_rangeuse_list = new qa_cims_m0_rangeuse_detail[1];
        qa_cims_m0_rangeuse_detail _m0_rangeuse_peview = new qa_cims_m0_rangeuse_detail();

        _m0_rangeuse_peview.m0_device_idx = int.Parse(Peview);

        _data_rangeuse_peview.qa_cims_m0_rangeuse_list[0] = _m0_rangeuse_peview;
        _data_rangeuse_peview = callServicePostMasterQACIMS(_urlCimsGetRangeUseRegis, _data_rangeuse_peview);

        GridView gvRangePeview = (GridView)fvEquipmentDetail.FindControl("gvRangePeview");
        setGridData(gvRangePeview, _data_rangeuse_peview.qa_cims_m0_rangeuse_list);

        //Measuring Preview
        data_qa_cims _data_measuring_preview = new data_qa_cims();
        _data_measuring_preview.qa_cims_m0_measuring_list = new qa_cims_m0_measuring_detail[1];
        qa_cims_m0_measuring_detail _m0_measuring_preview = new qa_cims_m0_measuring_detail();

        _m0_measuring_preview.m0_device_idx = int.Parse(Peview);

        _data_measuring_preview.qa_cims_m0_measuring_list[0] = _m0_measuring_preview;
        _data_measuring_preview = callServicePostMasterQACIMS(_urlCimsGetmeasuringRegis, _data_measuring_preview);

        GridView gvMeasuringPreview = (GridView)fvEquipmentDetail.FindControl("gvMeasuringPreview");
        setGridData(gvMeasuringPreview, _data_measuring_preview.qa_cims_m0_measuring_list);

        //Acceptance Preview
        data_qa_cims _data_acceptance_preview = new data_qa_cims();
        _data_acceptance_preview.qa_cims_m0_acceptance_list = new qa_cims_m0_acceptance_detail[1];
        qa_cims_m0_acceptance_detail _m0_acceptance_preview = new qa_cims_m0_acceptance_detail();

        _m0_acceptance_preview.m0_device_idx = int.Parse(Peview);

        _data_acceptance_preview.qa_cims_m0_acceptance_list[0] = _m0_acceptance_preview;
        _data_acceptance_preview = callServicePostMasterQACIMS(_urlCimsGetAcceptanceRegis, _data_acceptance_preview);
        GridView gvAcceptancePreview = (GridView)fvEquipmentDetail.FindControl("gvAcceptancePreview");
        setGridData(gvAcceptancePreview, _data_acceptance_preview.qa_cims_m0_acceptance_list);

        //Resulution Preview
        data_qa_cims _data_resolution_preview = new data_qa_cims();
        _data_resolution_preview.qa_cims_m0_resolution_list = new qa_cims_m0_resulution_detail[1];
        qa_cims_m0_resulution_detail _ResolutionIDX_preview = new qa_cims_m0_resulution_detail();

        _ResolutionIDX_preview.m0_device_idx = int.Parse(Peview);
        _ResolutionIDX_preview.condition = 2;

        _data_resolution_preview.qa_cims_m0_resolution_list[0] = _ResolutionIDX_preview;
        _data_resolution_preview = callServicePostMasterQACIMS(_urlCimsGetM0_Resolution, _data_resolution_preview);

        GridView gvResolutionPreview = (GridView)fvEquipmentDetail.FindControl("gvResolutionPreview");
        setGridData(gvResolutionPreview, _data_resolution_preview.qa_cims_m0_resolution_list);

        //CalPoint Preview
        data_qa_cims _data_calpoint_preview = new data_qa_cims();
        _data_calpoint_preview.qa_cims_m1_registration_device_list = new qa_cims_m1_registration_device[1];
        qa_cims_m1_registration_device _calpoint_preview = new qa_cims_m1_registration_device();

        _calpoint_preview.m0_device_idx = int.Parse(Peview);

        _data_calpoint_preview.qa_cims_m1_registration_device_list[0] = _calpoint_preview;

        _data_calpoint_preview = callServicePostMasterQACIMS(_urlCimsGetM1RegistrationDevice, _data_calpoint_preview);

        GridView GvCalPointPreview = (GridView)fvEquipmentDetail.FindControl("GvCalPointPreview");

        setGridData(GvCalPointPreview, _data_calpoint_preview.qa_cims_m1_registration_device_list);

    }

    protected void selectgvMeasuringUpdate(string vs_MeasuringUpdate)
    {
        data_qa_cims _data_measuring = new data_qa_cims();
        _data_measuring.qa_cims_m0_measuring_list = new qa_cims_m0_measuring_detail[1];
        qa_cims_m0_measuring_detail _m0_measuring_list = new qa_cims_m0_measuring_detail();

        _m0_measuring_list.m0_device_idx = int.Parse(vs_MeasuringUpdate);
        //_ResolutionIDX.condition = 2;

        _data_measuring.qa_cims_m0_measuring_list[0] = _m0_measuring_list;
        _data_measuring = callServicePostMasterQACIMS(_urlCimsGetmeasuringRegis, _data_measuring);

        GridView gvMeasuringUpdate = (GridView)fvInsertCIMS.FindControl("gvMeasuringUpdate");
        setGridData(gvMeasuringUpdate, _data_measuring.qa_cims_m0_measuring_list);
    }

    protected void selectgvAcceptanceUpdate(string vs_AcceptanceUpdate)
    {
        data_qa_cims _data_acceptance = new data_qa_cims();
        _data_acceptance.qa_cims_m0_acceptance_list = new qa_cims_m0_acceptance_detail[1];
        qa_cims_m0_acceptance_detail _m0_acceptance_list = new qa_cims_m0_acceptance_detail();

        _m0_acceptance_list.m0_device_idx = int.Parse(vs_AcceptanceUpdate);
        //_ResolutionIDX.condition = 2;

        _data_acceptance.qa_cims_m0_acceptance_list[0] = _m0_acceptance_list;
        _data_acceptance = callServicePostMasterQACIMS(_urlCimsGetAcceptanceRegis, _data_acceptance);

        GridView gvAcceptanceUpdate = (GridView)fvInsertCIMS.FindControl("gvAcceptanceUpdate");
        setGridData(gvAcceptanceUpdate, _data_acceptance.qa_cims_m0_acceptance_list);
    }


    protected void selectLogRegistrationDevice(string cmdArgUserForUpdate)
    {
        data_qa_cims _data_Log = new data_qa_cims();
        _data_Log.qa_cims_log_registration_device_list = new qa_cims_log_registration_device_detail[1];
        qa_cims_log_registration_device_detail _logView = new qa_cims_log_registration_device_detail();

        _logView.m0_device_idx = int.Parse(cmdArgUserForUpdate);

        _data_Log.qa_cims_log_registration_device_list[0] = _logView;

        _data_Log = callServicePostMasterQACIMS(_urlCimsGetLogRegistrationDevice, _data_Log);


        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_Log.qa_cims_log_registration_device_list));

        history_registration_device_action.DataSource = _data_Log.qa_cims_log_registration_device_list;
        history_registration_device_action.DataBind();
    }


    protected void CheckboxChanged(object sender, EventArgs e)
    {
        var cb = (CheckBox)sender;
        var txtinput_machine_name = (TextBox)fvInsertCIMS.FindControl("txtinput_machine_name");
        var txtinput_brand = (TextBox)fvInsertCIMS.FindControl("txtinput_brand");
        var chk_other_name = (CheckBox)fvInsertCIMS.FindControl("chk_other_name");
        var chk_other_brand = (CheckBox)fvInsertCIMS.FindControl("chk_other_brand");
        var ddlEquipmentName = (DropDownList)fvInsertCIMS.FindControl("ddlEquipmentName");
        var ddlBrand = (DropDownList)fvInsertCIMS.FindControl("ddlBrand");


        switch (cb.ID)
        {
            case "chk_other_name":
                if (chk_other_name.Checked)
                {
                    txtinput_machine_name.Visible = true;
                    ddlEquipmentName.Visible = false;
                    txtinput_machine_name.Text = null;
                }
                else
                {
                    txtinput_machine_name.Visible = false;
                    ddlEquipmentName.Visible = true;
                    getM0EquipmentName((DropDownList)fvInsertCIMS.FindControl("ddlEquipmentName"), "0");
                }

                break;

            case "chk_other_brand":
                if (chk_other_brand.Checked)
                {
                    txtinput_brand.Visible = true;
                    ddlBrand.Visible = false;
                    txtinput_brand.Text = null;
                }
                else
                {
                    txtinput_brand.Visible = false;
                    ddlBrand.Visible = true;
                    getM0Brand((DropDownList)fvInsertCIMS.FindControl("ddlBrand"), "0");
                }
                break;
        }
    }

    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        if (sender is FormView)
        {
            FormView FvName = (FormView)sender;
            switch (FvName.ID)
            {
                case "fvInsertPerQA":
                    if (FvName.CurrentMode == FormViewMode.ReadOnly) { }
                    if (FvName.CurrentMode == FormViewMode.Edit)
                    {

                        CleardataSetPlaceListUpdate();
                        CleardataSetPerListUpdate();

                        Label lblm0_management_idx_edit = (Label)FvName.FindControl("lblm0_management_idx_edit");
                        Label lblorg_idx_edit = (Label)FvName.FindControl("lblorg_idx_edit");
                        Label lbl_rdept_idx_edit = (Label)FvName.FindControl("lbl_rdept_idx_edit");
                        Label lbl_rsec_idx_edit = (Label)FvName.FindControl("lbl_rsec_idx_edit");
                        Label lbl_rpos_idx_edit = (Label)FvName.FindControl("lbl_rpos_idx_edit");

                        GridView gvPlacePerListUpdate = (GridView)FvName.FindControl("gvPlacePerListUpdate");
                        GridView gvPerPerListUpdate = (GridView)FvName.FindControl("gvPerPerListUpdate");
                        DropDownList ddlorg_idxupdate = (DropDownList)FvName.FindControl("ddlorg_idxupdate");
                        DropDownList ddlrdept_idxupdate = (DropDownList)FvName.FindControl("ddlrdept_idxupdate");
                        DropDownList ddlrsec_idxupdate = (DropDownList)FvName.FindControl("ddlrsec_idxupdate");
                        DropDownList ddlrpos_idxupdate = (DropDownList)FvName.FindControl("ddlrpos_idxupdate");


                        getOrganizationPerList(ddlorg_idxupdate, int.Parse(lblorg_idx_edit.Text));
                        getDepartmentPerList(ddlrdept_idxupdate, int.Parse(ddlorg_idxupdate.SelectedValue), int.Parse(lbl_rdept_idx_edit.Text));

                        getSectionPerList(ddlrsec_idxupdate, int.Parse(ddlorg_idxupdate.SelectedValue), int.Parse(ddlrdept_idxupdate.SelectedValue), int.Parse(lbl_rsec_idx_edit.Text));
                        getPositionPerList(ddlrpos_idxupdate, int.Parse(ddlorg_idxupdate.SelectedValue), int.Parse(ddlrdept_idxupdate.SelectedValue), int.Parse(ddlrsec_idxupdate.SelectedValue), int.Parse(lbl_rpos_idx_edit.Text));

                        getSelectPlace((DropDownList)FvName.FindControl("ddl_PlaceidxUpdate"), "0", "0");
                        getPermissionManageList((DropDownList)FvName.FindControl("ddl_PerManageUpdate"));

                        //litDebug.Text = lblm0_management_idx_edit.Text;

                        data_qa_cims _data_place_edit = new data_qa_cims();
                        _data_place_edit.qa_cims_m1_management_list = new qa_cims_m1_management_detail[1];
                        qa_cims_m1_management_detail _m0_place_edit = new qa_cims_m1_management_detail();

                        _m0_place_edit.condition = 1;
                        _m0_place_edit.m0_management_idx = int.Parse(lblm0_management_idx_edit.Text);

                        _data_place_edit.qa_cims_m1_management_list[0] = _m0_place_edit;

                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_place_edit));
                        _data_place_edit = callServicePostMasterQACIMS(_urlCimsGetPermissionManagePlaceQA, _data_place_edit);

                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_place_edit));
                        foreach (var place_edit in _data_place_edit.qa_cims_m1_management_list)
                        {
                            if (ViewState["vsPlaceListUpdate"] != null)
                            {
                                CultureInfo culture = new CultureInfo("en-US");
                                Thread.CurrentThread.CurrentCulture = culture;
                                DataSet dsContacts = (DataSet)ViewState["vsPlaceListUpdate"];
                                DataRow drContacts = dsContacts.Tables["dsPlaceTableUpdate"].NewRow();

                                drContacts["drPlaceIdxUpdate"] = place_edit.place_idx;
                                drContacts["drPlaceTextUpdate"] = place_edit.place_name;

                                dsContacts.Tables["dsPlaceTableUpdate"].Rows.Add(drContacts);
                                ViewState["vsPlaceListUpdate"] = dsContacts;
                                setGridData(gvPlacePerListUpdate, dsContacts.Tables["dsPlaceTableUpdate"]);

                                gvPlacePerListUpdate.Visible = true;
                            }
                        }

                        //per per list
                        data_qa_cims _data_per_edit = new data_qa_cims();
                        _data_per_edit.qa_cims_m2_management_list = new qa_cims_m2_management_detail[1];
                        qa_cims_m2_management_detail _m0_per_edit = new qa_cims_m2_management_detail();

                        _m0_per_edit.condition = 1;
                        _m0_per_edit.m0_management_idx = int.Parse(lblm0_management_idx_edit.Text);


                        _data_per_edit.qa_cims_m2_management_list[0] = _m0_per_edit;

                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_place_edit));
                        _data_per_edit = callServicePostMasterQACIMS(_urlCimsGetPermissionManagePerQA, _data_per_edit);

                        foreach (var per_edit in _data_per_edit.qa_cims_m2_management_list)
                        {
                            if (ViewState["vsPerListUpdate"] != null)
                            {
                                CultureInfo culture = new CultureInfo("en-US");
                                Thread.CurrentThread.CurrentCulture = culture;
                                DataSet dsContacts = (DataSet)ViewState["vsPerListUpdate"];
                                DataRow drContacts = dsContacts.Tables["dsPerTableUpdate"].NewRow();

                                drContacts["drPerIdxUpdate"] = per_edit.m0_per_management_idx;
                                drContacts["drPerTextUpdate"] = per_edit.per_management_name;

                                dsContacts.Tables["dsPerTableUpdate"].Rows.Add(drContacts);
                                ViewState["vsPerListUpdate"] = dsContacts;
                                setGridData(gvPerPerListUpdate, dsContacts.Tables["dsPerTableUpdate"]);

                                gvPerPerListUpdate.Visible = true;
                            }
                        }




                    }
                    if (FvName.CurrentMode == FormViewMode.Insert)
                    {

                        CleardataSetPlaceList();
                        CleardataSetPerManagementList();

                        DropDownList ddlorg_idx = (DropDownList)fvInsertPerQA.FindControl("ddlorg_idx");
                        DropDownList ddlrdept_idx = (DropDownList)fvInsertPerQA.FindControl("ddlrdept_idx");
                        DropDownList ddlrsec_idx = (DropDownList)fvInsertPerQA.FindControl("ddlrsec_idx");
                        DropDownList ddlrpos_idx = (DropDownList)fvInsertPerQA.FindControl("ddlrpos_idx");

                        getOrganizationPerList(ddlorg_idx, 1);
                        getDepartmentPerList(ddlrdept_idx, int.Parse(ddlorg_idx.SelectedValue), 27);
                        getSectionPerList(ddlrsec_idx, int.Parse(ddlorg_idx.SelectedValue), int.Parse(ddlrdept_idx.SelectedValue), 0);
                        getPositionPerList(ddlrpos_idx, int.Parse(ddlorg_idx.SelectedValue), int.Parse(ddlrdept_idx.SelectedValue), int.Parse(ddlrsec_idx.SelectedValue), 0);

                        getSelectPlace((DropDownList)fvInsertPerQA.FindControl("ddl_Placeidx"), "0", "0");
                        getPermissionManageList((DropDownList)fvInsertPerQA.FindControl("ddl_Permanagement"));


                    }
                    break;
            }
        }
        else if (sender is RadioButtonList)
        {
            RadioButtonList rdoListName = (RadioButtonList)sender;
            switch (rdoListName.ID)
            {
                case "rdoListFoodMaterialHaving":
                    //Control panelFoodMaterial = (Control)fvCRUD.FindControl("panelFoodMaterial");
                    //if (rdoListName.SelectedValue == "1")
                    //{
                    //    panelFoodMaterial.Visible = true;
                    //}
                    //else
                    //{
                    //    panelFoodMaterial.Visible = false;
                    //}
                    break;
            }
        }
        else if (sender is DropDownList)
        {
            DropDownList ddlName = (DropDownList)sender;
            switch (ddlName.ID)
            {
                //case "ddlFoodMaterial":
                //    Label lblFoodMaterialUnit = (Label)fvCRUD.FindControl("lblFoodMaterialUnit");
                //    string unitMaterial = getUnitMaterial(int.Parse(ddlName.SelectedValue));
                //    lblFoodMaterialUnit.Text = unitMaterial;
                //    break;

                //case "ddlFoodMaterialUpdate":
                //    Label lblFoodMaterialUnitUpdate = (Label)fvCRUD.FindControl("lblFoodMaterialUnitUpdate");
                //    string unitMaterialUpdate = getUnitMaterial(int.Parse(ddlName.SelectedValue));
                //    lblFoodMaterialUnitUpdate.Text = unitMaterialUpdate;
                //    break;
            }
        }
    }


    public class GetValueToRepeater
    {
        public int m0_device_idx { get; set; }
        public string device_id_no { get; set; }
    }

    protected void GetSelectedRecords()
    {
        List<int> list = ViewState["SelectedRecords"] as List<int>;
        if (list != null)
        {
            foreach (int m0_device_idx in list)
            {
                //litDebug.Text += (m0_device_idx.ToString()+" ");
            }
        }
    }

    protected void selectLogCutoff(string cmdArgUserForUpdate)
    {
        data_qa_cims _data_Log_cutoff = new data_qa_cims();
        _data_Log_cutoff.qa_cims_u2_document_device_list = new qa_cims_u2_document_device_detail[1];
        qa_cims_u2_document_device_detail _logCutoff = new qa_cims_u2_document_device_detail();

        _logCutoff.u0_device_idx = int.Parse(cmdArgUserForUpdate);

        _data_Log_cutoff.qa_cims_u2_document_device_list[0] = _logCutoff;

        _data_Log_cutoff = callServicePostMasterQACIMS(_urlCimsGetLogEquipmentList, _data_Log_cutoff);


        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_Log_cutoff.qa_cims_u2_document_device_list));

        rptLogCutoff.DataSource = _data_Log_cutoff.qa_cims_u2_document_device_list;
        rptLogCutoff.DataBind();
    }

    protected void selectLogQACutoff(string cmdArgQACut)
    {
        data_qa_cims _data_Log_cutoff = new data_qa_cims();
        _data_Log_cutoff.qa_cims_u2_document_device_list = new qa_cims_u2_document_device_detail[1];
        qa_cims_u2_document_device_detail _logCutoff = new qa_cims_u2_document_device_detail();

        _logCutoff.u0_device_idx = int.Parse(cmdArgQACut);

        _data_Log_cutoff.qa_cims_u2_document_device_list[0] = _logCutoff;

        _data_Log_cutoff = callServicePostMasterQACIMS(_urlCimsGetLogEquipmentList, _data_Log_cutoff);


        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_Log_cutoff.qa_cims_u2_document_device_list));

        rpt_cutqa.DataSource = _data_Log_cutoff.qa_cims_u2_document_device_list;
        rpt_cutqa.DataBind();
    }

    protected void selectLogQACutoff_sucesscutoff(string cmdArgQACut)
    {
        data_qa_cims _data_Log_cutoff_success = new data_qa_cims();
        _data_Log_cutoff_success.qa_cims_u2_document_device_list = new qa_cims_u2_document_device_detail[1];
        qa_cims_u2_document_device_detail _logCutoff_success = new qa_cims_u2_document_device_detail();

        _logCutoff_success.u0_device_idx = int.Parse(cmdArgQACut);

        _data_Log_cutoff_success.qa_cims_u2_document_device_list[0] = _logCutoff_success;

        _data_Log_cutoff_success = callServicePostMasterQACIMS(_urlCimsGetLogEquipmentList, _data_Log_cutoff_success);


        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_Log_cutoff.qa_cims_u2_document_device_list));

        rpt_sucess_cutoff.DataSource = _data_Log_cutoff_success.qa_cims_u2_document_device_list;
        rpt_sucess_cutoff.DataBind();
    }

    protected void selectLogTranfer(string cmdArgUserForUpdate)
    {
        data_qa_cims _data_Log_tranfer = new data_qa_cims();
        _data_Log_tranfer.qa_cims_u2_document_device_list = new qa_cims_u2_document_device_detail[1];
        qa_cims_u2_document_device_detail _logTranfer = new qa_cims_u2_document_device_detail();

        _logTranfer.u0_device_idx = int.Parse(cmdArgUserForUpdate);

        _data_Log_tranfer.qa_cims_u2_document_device_list[0] = _logTranfer;

        _data_Log_tranfer = callServicePostMasterQACIMS(_urlCimsGetLogEquipmentList, _data_Log_tranfer);


        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_Log_cutoff.qa_cims_u2_document_device_list));

        rptLogTranfer.DataSource = _data_Log_tranfer.qa_cims_u2_document_device_list;
        rptLogTranfer.DataBind();
    }

    protected void selectLogQATranfer(string cmdArgQAForUpdate)
    {
        data_qa_cims _data_Log_tranfer = new data_qa_cims();
        _data_Log_tranfer.qa_cims_u2_document_device_list = new qa_cims_u2_document_device_detail[1];
        qa_cims_u2_document_device_detail _logTranfer = new qa_cims_u2_document_device_detail();

        _logTranfer.u0_device_idx = int.Parse(cmdArgQAForUpdate);

        _data_Log_tranfer.qa_cims_u2_document_device_list[0] = _logTranfer;

        _data_Log_tranfer = callServicePostMasterQACIMS(_urlCimsGetLogEquipmentList, _data_Log_tranfer);


        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_Log_cutoff.qa_cims_u2_document_device_list));

        rpt_qatranfer.DataSource = _data_Log_tranfer.qa_cims_u2_document_device_list;
        rpt_qatranfer.DataBind();
    }

    protected void BackToCurrentPageGvRegistrationList()
    {
        divRegistrationList.Visible = true;
        //btnaddregis.Visible = true;
        viewRegistration.Visible = false;
        divCutoffRegistrationList.Visible = false;
        divTranferRegistration.Visible = false;


        if (tbValuePlacecims.Text == "" || tbValuePlacecims.Text == "0")
        {
            tbValuePlacecims.Text = "0";
            data_qa_cims dataCims = new data_qa_cims();
            dataCims.qa_cims_m0_registration_device_list = new qa_cims_m0_registration_device[1];
            qa_cims_m0_registration_device _selectRegistration = new qa_cims_m0_registration_device();

            if (ViewState["rdept_permission"].ToString() == "27")
            {

                dataCims.qa_cims_m0_registration_device_list[0] = _selectRegistration;
                dataCims = callServicePostMasterQACIMS(_urlCimsGetRegistrationDevice, dataCims);

                ViewState["vsRegistrationDevice"] = dataCims.qa_cims_m0_registration_device_list;

                //ViewState["vsRegistrationDevice"] = dataCims.qa_cims_m0_registration_device_list;
                ViewState["vsCheckIDandSerial"] = dataCims.qa_cims_m0_registration_device_list;

            }
            else
            {
                _selectRegistration.condition = 4;
                _selectRegistration.device_rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
                dataCims.qa_cims_m0_registration_device_list[0] = _selectRegistration;
                dataCims = callServicePostMasterQACIMS(_urlCimsGetRegistrationDevice, dataCims);

                ViewState["vsRegistrationDevice"] = dataCims.qa_cims_m0_registration_device_list;
                ViewState["vsCheckIDandSerial"] = dataCims.qa_cims_m0_registration_device_list;
            }
            //gvRegistrationList.PageIndex = 0;
            setGridData(gvRegistrationList, ViewState["vsRegistrationDevice"]);

            //select Place Cims
            data_qa_cims dataCims_place = new data_qa_cims();
            dataCims_place.qa_cims_m0_place_list = new qa_cims_m0_place_detail[1];
            qa_cims_m0_place_detail _selectPlacecims = new qa_cims_m0_place_detail();

            _selectPlacecims.condition = 2;
            dataCims_place.qa_cims_m0_place_list[0] = _selectPlacecims;
            dataCims_place = callServicePostMasterQACIMS(_urlCimsGetPlace, dataCims_place);
            setRepeaterData(rptBindPlaceForSelectDoc_cims, dataCims_place.qa_cims_m0_place_list);
        }
        else
        {
            data_qa_cims datasertRegistration = new data_qa_cims();
            datasertRegistration.qa_cims_m0_registration_device_list = new qa_cims_m0_registration_device[1];
            qa_cims_m0_registration_device registrationsert = new qa_cims_m0_registration_device();
            registrationsert.rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
            registrationsert.rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
            registrationsert.device_place_idx = int.Parse(tbValuePlacecims.Text);
            registrationsert.condition = 2;

            datasertRegistration.qa_cims_m0_registration_device_list[0] = registrationsert;

            datasertRegistration = callServicePostQaDocCIMS(_urlCimsGetRegistrationDevice, datasertRegistration);


            setGridData(gvRegistrationList, ViewState["vsRegistrationDevice"]);
        }
    }

    protected void BackToCurrentPageGvDocumentList(int place)
    {
        divdocumentList.Visible = true;
        divDocDetailUser.Visible = false;
        divFVVQACutoff.Visible = false;
        divFVVQATranfer.Visible = false;
        divFVVAllCutoff.Visible = false;
        divFVVAllTranfer.Visible = false;
        divPassU1.Visible = false;


        if (place == 0)
        {
            if (int.Parse(ViewState["rdept_permission"].ToString()) == 27)
            {
                data_qa_cims dataSelectList_document = new data_qa_cims();
                dataSelectList_document.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
                qa_cims_u1_document_device_detail _selectu0DocList = new qa_cims_u1_document_device_detail();
                dataSelectList_document.qa_cims_u1_document_device_list[0] = _selectu0DocList;
                dataSelectList_document = callServicePostQaDocCIMS(_urlCimsGetEquipmentList, dataSelectList_document);

                //gvDocumentList.PageIndex = 0;
                ViewState["vsDocument"] = dataSelectList_document.qa_cims_u0_document_device_list;
                setGridData(gvDocumentList, ViewState["vsDocument"]);
            }
            else
            {
                data_qa_cims dataSelectList_document = new data_qa_cims();
                dataSelectList_document.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
                qa_cims_u1_document_device_detail _selectu0DocList = new qa_cims_u1_document_device_detail();
                _selectu0DocList.condition = 7;
                _selectu0DocList.rsec_idx_create = int.Parse(ViewState["rsec_permission"].ToString());
                dataSelectList_document.qa_cims_u1_document_device_list[0] = _selectu0DocList;
                dataSelectList_document = callServicePostQaDocCIMS(_urlCimsGetEquipmentList, dataSelectList_document);

                //gvDocumentList.PageIndex = 0;
                ViewState["vsDocument"] = dataSelectList_document.qa_cims_u0_document_device_list;
                setGridData(gvDocumentList, ViewState["vsDocument"]);
            }



        }
        else
        {

            if (int.Parse(ViewState["rdept_permission"].ToString()) == 27)
            {
                data_qa_cims _dataSelectPlaceDoc_backpage = new data_qa_cims();
                _dataSelectPlaceDoc_backpage.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
                qa_cims_u1_document_device_detail _select_place_doc = new qa_cims_u1_document_device_detail();
                _select_place_doc.place_idx = place;
                _select_place_doc.condition = 6;
                //_select_place_doc.rsec_idx_create = int.Parse(ViewState["rsec_permission"].ToString());
                _dataSelectPlaceDoc_backpage.qa_cims_u1_document_device_list[0] = _select_place_doc;
                _dataSelectPlaceDoc_backpage = callServicePostQaDocCIMS(_urlCimsGetEquipmentList, _dataSelectPlaceDoc_backpage);

                //gvDocumentList.PageIndex = 0;
                ViewState["vsDocument"] = _dataSelectPlaceDoc_backpage.qa_cims_u0_document_device_list;
                setGridData(gvDocumentList, ViewState["vsDocument"]);
            }

            else
            {
                data_qa_cims _dataSelectPlaceDoc_backpage = new data_qa_cims();
                _dataSelectPlaceDoc_backpage.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
                qa_cims_u1_document_device_detail _select_place_doc = new qa_cims_u1_document_device_detail();
                _select_place_doc.place_idx = place;
                _select_place_doc.condition = 8;
                _select_place_doc.rsec_idx_create = int.Parse(ViewState["rsec_permission"].ToString());
                _dataSelectPlaceDoc_backpage.qa_cims_u1_document_device_list[0] = _select_place_doc;
                _dataSelectPlaceDoc_backpage = callServicePostQaDocCIMS(_urlCimsGetEquipmentList, _dataSelectPlaceDoc_backpage);

                //gvDocumentList.PageIndex = 0;
                ViewState["vsDocument"] = _dataSelectPlaceDoc_backpage.qa_cims_u0_document_device_list;
                setGridData(gvDocumentList, ViewState["vsDocument"]);
            }

        }

        data_qa_cims _dataqa_cimsplace = new data_qa_cims();
        _dataqa_cimsplace.qa_cims_m0_place_list = new qa_cims_m0_place_detail[1];
        qa_cims_m0_place_detail _m0place = new qa_cims_m0_place_detail();
        _m0place.condition = 2;
        _dataqa_cimsplace.qa_cims_m0_place_list[0] = _m0place;

        _dataqa_cimsplace = callServicePostMasterQACIMS(_urlCimsGetPlace, _dataqa_cimsplace);
        setRepeaterData(rp_place_document, _dataqa_cimsplace.qa_cims_m0_place_list);

        divSearchDocument.Visible = true;
    }
    #endregion

    #region event gridview

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView gvName = (GridView)sender;
        gvName.PageIndex = e.NewPageIndex;

        switch (gvName.ID)
        {
            case "GvCalPointEdit":
                GridView GvCalPointEdit = (GridView)fvInsertCIMS.FindControl("GvCalPointEdit");
                GvCalPointEdit.PageIndex = e.NewPageIndex;
                GvCalPointEdit.DataBind();
                SelectCalibration_point(ViewState["vsM0DeviceIDX"].ToString());
                break;

            case "gvRegistrationList":
                gvRegistrationList.PageIndex = e.NewPageIndex;
                gvRegistrationList.DataBind();

                setGridData(gvRegistrationList, ViewState["vsRegistrationDevice"]);

                break;

            case "GvPerManagement":
                GvPerManagement.PageIndex = e.NewPageIndex;
                GvPerManagement.DataBind();

                setGridData(GvPerManagement, ViewState["vs_PermissionManage_Select"]);

                break;

            case "gvReportDevicesOnline":

                gvReportDevicesOnline.PageIndex = e.NewPageIndex;
                gvReportDevicesOnline.DataBind();

                setGridData(gvReportDevicesOnline, ViewState["vs_ReportDevices_Online"]);

                break;
            case "gvReportDetailOnline":

                gvReportDetailOnline.PageIndex = e.NewPageIndex;
                gvReportDetailOnline.DataBind();

                setGridData(gvReportDetailOnline, ViewState["vs_ReportDetailDevicesOnline"]);

                break;

            case "gvReportDevicesOffline":

                gvReportDevicesOffline.PageIndex = e.NewPageIndex;
                gvReportDevicesOffline.DataBind();

                setGridData(gvReportDevicesOffline, ViewState["vs_ReportDevices_Offline"]);

                break;
            case "gvReportDetailOffline":

                gvReportDetailOffline.PageIndex = e.NewPageIndex;
                gvReportDetailOffline.DataBind();

                setGridData(gvReportDetailOffline, ViewState["vs_ReportDetailDevicesOffline"]);

                break;

            case "gvReportDetailCutOff":

                gvReportDetailCutOff.PageIndex = e.NewPageIndex;
                gvReportDetailCutOff.DataBind();

                setGridData(gvReportDetailCutOff, ViewState["vs_ReportDetailDevicesCutoff"]);

                break;

            case "gvReportDevicesCutoff":

                gvReportDevicesCutoff.PageIndex = e.NewPageIndex;
                gvReportDevicesCutoff.DataBind();

                setGridData(gvReportDevicesCutoff, ViewState["vs_ReportDevices_Cutoff"]);

                break;

            case "gvListEquipment":

                // savecheckedvalus();
                setGridData(gvListEquipment, ViewState["vsEquipmentListHolder"]);
                // checkeddvaluesp();

                break;

            case "gvListEquipmentCutoff":
                //savecheckedvaluscutoff();
                setGridData(gvListEquipmentCutoff, ViewState["vsEquipmentListHolderCutoff"]);
                //checkeddvaluespcutoff();

                break;

            case "ChkEquipmentCutoff":

                foreach (GridViewRow rowItems in gvListEquipmentCutoff.Rows)
                {
                    CheckBox chk = (CheckBox)rowItems.FindControl("ChkEquipmentCutoff");
                    Label lblDeviceIDX = (Label)rowItems.FindControl("lblDeviceIDX");
                    Label lblEquipment_IDNO = (Label)rowItems.FindControl("lblEquipment_IDNO");

                    if (chk.Checked)
                    {
                        chk.Checked = true;
                        lst.Add(new GetValueToRepeater() { m0_device_idx = int.Parse(lblDeviceIDX.Text), device_id_no = lblEquipment_IDNO.Text });

                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(lst));
                        RepeaterCutoff.DataSource = lst;
                        RepeaterCutoff.DataBind();
                        // litDebug.Text = "0000";
                    }
                    else
                    {
                        //lst.Add(new GetValueToRepeater() { m0_device_idx = int.Parse(lblDeviceIDX.Text), device_id_no = lblEquipment_IDNO.Text });
                        lst = null;
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(lst));
                        RepeaterCutoff.DataSource = lst;
                        RepeaterCutoff.DataBind();
                        //litDebug.Text = "1111";
                    }
                }
                break;

            case "gvEquipmentQACutoff":
                savecheckedvalusQAcutoff();
                setGridData(gvEquipmentQACutoff, ViewState["DocEquipmentList"]);
                checkeddvaluespQAcutoff();
                break;

            case "gvDocumentList":
                //setActiveTab("docList", 0, 0, 0, int.Parse(tbPlaceIDXInDocument.Text), 0, 0);

                setGridData(gvDocumentList, ViewState["vsDocument"]);
                setOntop.Focus();
                break;

            case "gvEquipmentQATranfer":
                savecheckedvalusQAtranfer();
                setGridData(gvEquipmentQATranfer, ViewState["DocEquipmentList"]);
                checkeddvaluespQAtranfer();
                break;

            case "gvCalibrationDocument":
                setGridData(gvCalibrationDocument, ViewState["vsGetDocumentCalibration"]);

                break;
        }

    }

    protected void gvRowDeleted(object sender, GridViewDeleteEventArgs e)
    {
        var gvName = (GridView)sender;

        switch (gvName.ID)
        {
            case "GvDeptExcel":

                GridView GvDeptExcel = (GridView)fvInsertCIMS.FindControl("GvDeptExcel");
                var DeleteFormList = (DataSet)ViewState["vsCalibrationPoint_Excel"];
                var drDriving = DeleteFormList.Tables[0].Rows;

                drDriving.RemoveAt(e.RowIndex);

                ViewState["vsCalibrationPoint_Excel"] = DeleteFormList;
                GvDeptExcel.EditIndex = -1;

                setGridData(GvDeptExcel, ViewState["vsCalibrationPoint_Excel"]);

                break;

            case "gvResolution":

                GridView gvResolution = (GridView)fvInsertCIMS.FindControl("gvResolution");
                var DeleteResolution = (DataSet)ViewState["vsDataResolution"];
                var dr_Resolution = DeleteResolution.Tables[0].Rows;

                dr_Resolution.RemoveAt(e.RowIndex);

                ViewState["vsDataResolution"] = DeleteResolution;
                gvResolution.EditIndex = -1;

                setGridData(gvResolution, ViewState["vsDataResolution"]);

                break;

            case "gvRange":

                GridView gvRange = (GridView)fvInsertCIMS.FindControl("gvRange");
                var DeleteRange = (DataSet)ViewState["vsDataRangeOfUse"];
                var dr_Range = DeleteRange.Tables[0].Rows;

                dr_Range.RemoveAt(e.RowIndex);

                ViewState["vsDataRangeOfUse"] = DeleteRange;
                gvRange.EditIndex = -1;

                setGridData(gvRange, ViewState["vsDataRangeOfUse"]);

                break;

            case "gvMeasuring":

                GridView gvMeasuring = (GridView)fvInsertCIMS.FindControl("gvMeasuring");
                var DeleteMeasuring = (DataSet)ViewState["vsDataMeasuring"];
                var dr_Measuring = DeleteMeasuring.Tables[0].Rows;

                dr_Measuring.RemoveAt(e.RowIndex);

                ViewState["vsDataMeasuring"] = DeleteMeasuring;
                gvMeasuring.EditIndex = -1;

                setGridData(gvMeasuring, ViewState["vsDataMeasuring"]);

                break;

            case "gvAcceptance":

                GridView gvAcceptance = (GridView)fvInsertCIMS.FindControl("gvAcceptance");
                var DeleteAcceptance = (DataSet)ViewState["vsDataAcceptance"];
                var dr_Acceptance = DeleteAcceptance.Tables[0].Rows;

                dr_Acceptance.RemoveAt(e.RowIndex);

                ViewState["vsDataAcceptance"] = DeleteAcceptance;
                gvAcceptance.EditIndex = -1;

                setGridData(gvAcceptance, ViewState["vsDataAcceptance"]);

                break;

            case "gvFrequency":

                GridView gvFrequency = (GridView)fvInsertCIMS.FindControl("gvFrequency");
                //var DeleteFormList = (DataSet)ViewState["vsCalibrationPoint_Excel"];
                //var drDriving = DeleteFormList.Tables[0].Rows;

                //drDriving.RemoveAt(e.RowIndex);

                //ViewState["vsCalibrationPoint_Excel"] = DeleteFormList;
                //GvDeptExcel.EditIndex = -1;

                //setGridData(GvDeptExcel, ViewState["vsCalibrationPoint_Excel"]);
                break;

            case "gvListEquipmentCalOld":

                GridView gvListEquipmentCalOld = (GridView)docCreate.FindControl("gvListEquipmentCalOld");
                var DeleteListEquipmentCalOld = (DataSet)ViewState["vsDataEquipmentCalOld"];
                var drDelete = DeleteListEquipmentCalOld.Tables[0].Rows;

                drDelete.RemoveAt(e.RowIndex);

                ViewState["vsDataEquipmentCalOld"] = DeleteListEquipmentCalOld;
                gvListEquipmentCalOld.EditIndex = -1;

                setGridData(gvListEquipmentCalOld, ViewState["vsDataEquipmentCalOld"]);

                if (DeleteListEquipmentCalOld.Tables[0].Rows.Count == 0)
                {
                    divActionSaveCreateDocument.Visible = false;
                }
                else
                {
                    divActionSaveCreateDocument.Visible = true;
                }

                break;

            case "gvCalibrationPoint":

                GridView gvCalibrationPoint = (GridView)docCreate.FindControl("gvCalibrationPoint");
                var DeleteListCalibrationPoint = (DataSet)ViewState["vsDataCalibrationPoint"];
                var drDeleteCalibrationPoint = DeleteListCalibrationPoint.Tables[0].Rows;

                drDeleteCalibrationPoint.RemoveAt(e.RowIndex);

                ViewState["vsDataCalibrationPoint"] = DeleteListCalibrationPoint;
                gvCalibrationPoint.EditIndex = -1;

                setGridData(gvCalibrationPoint, ViewState["vsDataCalibrationPoint"]);

                if (DeleteListCalibrationPoint.Tables[0].Rows.Count == 0)
                {
                    btnInsertDetail.Visible = false;
                }
                else
                {
                    btnInsertDetail.Visible = true;
                }


                break;
        }

    }

    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "gvRegistrationList":

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    var _StatusDevice = (Label)e.Row.Cells[6].FindControl("lblDeviceStatus");
                    var _lbl_status_idx_detail = (Label)e.Row.FindControl("lbl_status_idx_detail");

                    var _StatusDeviceName = (Label)e.Row.Cells[6].FindControl("lblDeviceStatusName");
                    var _M0IDXDevice = (Label)e.Row.Cells[0].FindControl("lblM0DeviceIDX");
                    var _StatusDevice_Online = (Label)e.Row.Cells[6].FindControl("lblStatusOnLine");
                    var _StatusDevice_Offline = (Label)e.Row.Cells[6].FindControl("lblStatusOffLine");

                    var _lblDeviceRsecIDX = (Label)e.Row.Cells[5].FindControl("lblDeviceRsecIDX");

                    var _button_transfer = (LinkButton)e.Row.Cells[7].FindControl("btnTranfer");
                    var _button_cutOff = (LinkButton)e.Row.Cells[7].FindControl("btnCutOff");
                    var _button_edit = (LinkButton)e.Row.Cells[7].FindControl("btnEdit");

                    data_qa_cims _dataqaChecktool_index = new data_qa_cims();
                    _dataqaChecktool_index.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
                    qa_cims_u1_document_device_detail _selectDocument_index = new qa_cims_u1_document_device_detail();
                    _selectDocument_index.condition = 5;
                    _dataqaChecktool_index.qa_cims_u1_document_device_list[0] = _selectDocument_index;
                    _dataqaChecktool_index = callServicePostQaDocCIMS(_urlCimsGetEquipmentList, _dataqaChecktool_index);



                    //check permission edit
                    data_qa_cims _data_check_permission_qa = new data_qa_cims();
                    _data_check_permission_qa.qa_cims_m0_management_list = new qa_cims_m0_management_detail[1];
                    qa_cims_m0_management_detail _select_perQA_Check = new qa_cims_m0_management_detail();

                    _select_perQA_Check.condition = 3;
                    _select_perQA_Check.rpos_idx = int.Parse(ViewState["rpos_permission"].ToString());
                    _select_perQA_Check.place_idx = int.Parse(tbPlaceIDXInDocument.Text);


                    _data_check_permission_qa.qa_cims_m0_management_list[0] = _select_perQA_Check;
                    _data_check_permission_qa = callServicePostQaDocCIMS(_urlCimsGetCheckPermissionQA, _data_check_permission_qa);

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_check_permission_qa));
                    int per_place_idx_editregis = 0;
                    if (_data_check_permission_qa.return_code == 0)
                    {
                        per_place_idx_editregis = _data_check_permission_qa.qa_cims_m0_management_list[0].place_idx;
                        //litDebug.Text = "uuuuuuuuuuu";
                    }
                    else
                    {
                        per_place_idx_editregis = 0;
                       
                    }


                    if (_dataqaChecktool_index.qa_cims_u1_document_device_list != null)
                    {
                        var _linqCheckStatusTool_index = (from dt in _dataqaChecktool_index.qa_cims_u1_document_device_list
                                                          where dt.m0_device_idx == int.Parse(_M0IDXDevice.Text)
                                                         
                                                          select new
                                                          {
                                                              dt.m0_device_idx
                                                          }).ToList();

                        string _CheckStatusTool_index = "";



                        for (int CheckStatusTool = 0; CheckStatusTool < _linqCheckStatusTool_index.Count(); CheckStatusTool++)
                        {
                            _CheckStatusTool_index = _linqCheckStatusTool_index[CheckStatusTool].m0_device_idx.ToString();
                        }

                        if (_CheckStatusTool_index == _M0IDXDevice.Text)
                        {
                            _StatusDeviceName.Text = "<i class='fa fa-circle' style='color: Orange; font - size: 8px; ' aria-hidden='true'></i>  " + "Waiting";

                            _button_transfer.Visible = false;
                            _button_cutOff.Visible = false;
                            _button_edit.Visible = false;

                        }
                        else
                        {
                            if (_lbl_status_idx_detail.Text == "1")
                            {
                                _StatusDeviceName.Text = "<i class='fa fa-circle' style='color: lime; font - size: 8px; ' aria-hidden='true'></i>  " + "Online";

                                if (ViewState["rdept_permission"].ToString() == "27" && (ViewState["rsec_permission"].ToString() == _lblDeviceRsecIDX.Text))
                                {
                                    _button_transfer.Visible = true;
                                    _button_cutOff.Visible = true;

                                    if (per_place_idx_editregis.ToString() == tbPlaceIDXInDocument.Text && ViewState["rsec_permission"].ToString() == _lblDeviceRsecIDX.Text)
                                    {
                                       
                                        _button_edit.Visible = true;
                                    }
                                    else
                                    {
                                        
                                        _button_edit.Visible = false;
                                    }

                                }
                                else
                                {
                                    if (ViewState["rdept_permission"].ToString() == "27")
                                    {
                                        _button_transfer.Visible = false;
                                        _button_cutOff.Visible = true;
                                        _button_edit.Visible = true;
                                    }
                                    else
                                    {
                                        _button_transfer.Visible = true;
                                        _button_cutOff.Visible = false;
                                        _button_edit.Visible = false;
                                    }

                                }

                            }
                            else if(_lbl_status_idx_detail.Text == "2")
                            {
                                _StatusDeviceName.Text = "<i class='fa fa-circle' style='color: Gray; font - size: 8px; ' aria-hidden='true'></i>  " + "Offline";


                                if (ViewState["rdept_permission"].ToString() == "27")
                                {
                                    _button_transfer.Visible = false;
                                    _button_cutOff.Visible = false;
                                    _button_edit.Visible = true;
                                }
                                else
                                {
                                    _button_transfer.Visible = false;
                                    _button_cutOff.Visible = false;
                                    _button_edit.Visible = false;
                                }


                            }
                            else
                            {
                                _StatusDeviceName.Text = "<i class='fa fa-circle' style='color: Gray; font - size: 8px; ' aria-hidden='true'></i>  " + "Obsolate";

                                _button_transfer.Visible = false;
                                _button_cutOff.Visible = false;
                                _button_edit.Visible = false;
                            }
                        }
                    }
                    else
                    {

                       
                        if (_lbl_status_idx_detail.Text == "1")
                        {
                            //litDebug.Text = ViewState["rsec_permission"].ToString();
                            _StatusDeviceName.Text = "<i class='fa fa-circle' style='color: lime; font - size: 8px; ' aria-hidden='true'></i>  " + "Online";

                            if (ViewState["rdept_permission"].ToString() == "27" && (ViewState["rsec_permission"].ToString() == _lblDeviceRsecIDX.Text))
                            {
                                _button_transfer.Visible = true;
                                _button_cutOff.Visible = true;
                                //_button_edit.Visible = true;

                                if (per_place_idx_editregis.ToString() == tbPlaceIDXInDocument.Text && ViewState["rsec_permission"].ToString() == _lblDeviceRsecIDX.Text)
                                {
                                    //litDebug.Text = "33333";
                                    _button_edit.Visible = true;
                                }
                                else
                                {
                                   
                                    _button_edit.Visible = false;
                                }

                            }
                            else
                            {
                                if (ViewState["rdept_permission"].ToString() == "27")
                                {
                                    //litDebug.Text = "88888";
                                    _button_transfer.Visible = false;
                                    _button_cutOff.Visible = false;
                                    //_button_edit.Visible = true;

                                    if (per_place_idx_editregis.ToString() == tbPlaceIDXInDocument.Text)
                                    {
                                        //litDebug.Text = "33333";
                                        _button_edit.Visible = true;
                                    }
                                    else
                                    {
                                        //litDebug.Text = "44444";
                                        _button_edit.Visible = false;
                                    }

                                }
                                else
                                {
                                    _button_transfer.Visible = true;
                                    _button_cutOff.Visible = false;
                                    _button_edit.Visible = false;
                                }
                                                              
                            }

                        }
                        else if (_lbl_status_idx_detail.Text == "2")
                        {
                            _StatusDeviceName.Text = "<i class='fa fa-circle' style='color: Gray; font - size: 8px; ' aria-hidden='true'></i>  " + "Offline";


                            if (ViewState["rdept_permission"].ToString() == "27" && (ViewState["rsec_permission"].ToString() == _lblDeviceRsecIDX.Text))
                            {
                                _button_transfer.Visible = false;
                                _button_cutOff.Visible = false;
                                _button_edit.Visible = true;
                            }
                            else
                            {
                                if (ViewState["rdept_permission"].ToString() == "27")
                                {
                                    _button_transfer.Visible = false;
                                    _button_cutOff.Visible = false;
                                    _button_edit.Visible = true;
                                }
                                else
                                {
                                    _button_transfer.Visible = true;
                                    _button_cutOff.Visible = true;
                                    _button_edit.Visible = false;
                                }

                            }
                        }
                        else
                        {

                            _StatusDeviceName.Text = "<i class='fa fa-circle' style='color: Gray; font - size: 8px; ' aria-hidden='true'></i>  " + "Obsolate";

                             btnView.Visible = false;
                            _button_transfer.Visible = false;
                            _button_cutOff.Visible = false;
                            _button_edit.Visible = false;
                        }
                    }
                }

                break;
            case "GvPerManagement":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblm0_management_idx = (Label)e.Row.FindControl("lblm0_management_idx");
                    Repeater rptPlaceList = (Repeater)e.Row.FindControl("rptPlaceList");
                    Repeater rptPerList = (Repeater)e.Row.FindControl("rptPerList");

                    data_qa_cims _data_qa_select_place = new data_qa_cims();
                    _data_qa_select_place.qa_cims_m1_management_list = new qa_cims_m1_management_detail[1];
                    qa_cims_m1_management_detail _m0_select_perplace = new qa_cims_m1_management_detail();
                    _m0_select_perplace.m0_management_idx = int.Parse(lblm0_management_idx.Text);

                    _data_qa_select_place.qa_cims_m1_management_list[0] = _m0_select_perplace;

                    _data_qa_select_place = callServicePostMasterQACIMS(_urlCimsGetPermissionManagePlaceQA, _data_qa_select_place);

                    setRepeaterData(rptPlaceList, _data_qa_select_place.qa_cims_m1_management_list);

                    data_qa_cims _data_qa_select_per = new data_qa_cims();
                    _data_qa_select_per.qa_cims_m2_management_list = new qa_cims_m2_management_detail[1];
                    qa_cims_m2_management_detail _m0_select_per_per = new qa_cims_m2_management_detail();
                    _m0_select_per_per.m0_management_idx = int.Parse(lblm0_management_idx.Text);

                    _data_qa_select_per.qa_cims_m2_management_list[0] = _m0_select_per_per;

                    _data_qa_select_per = callServicePostMasterQACIMS(_urlCimsGetPermissionManagePerQA, _data_qa_select_per);

                    setRepeaterData(rptPerList, _data_qa_select_per.qa_cims_m2_management_list);



                }
                break;

            case "gvListEquipment":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    int index = e.Row.RowIndex;
                    CheckBox chk = (CheckBox)e.Row.FindControl("ChkEquipment");
                    Label lblDeviceIDX = (Label)e.Row.FindControl("lblDeviceIDX");
                    Label lblStatus = (Label)e.Row.FindControl("lblStatus");
                    Label lblStatusTool = (Label)e.Row.FindControl("lblStatusTool");

                    HiddenField hfSelected = (HiddenField)e.Row.FindControl("hfSelected");

                    if (ChkAll.Checked || bool.Parse(hfSelected.Value) == true)
                    {
                        chk.Checked = true;

                        if (ChkAll.Checked && chk.Checked)
                        {
                            chk.Enabled = false;
                        }
                    }

                    else
                    {
                        chk.Checked = false;
                        chk.Enabled = true;
                    }


                }
                break;

            case "gvListEquipmentCutoff":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    int index = e.Row.RowIndex;
                    CheckBox chk = (CheckBox)e.Row.FindControl("ChkEquipmentCutoff");
                    Label lblDeviceIDX = (Label)e.Row.FindControl("lblDeviceIDX");
                    Label lblStatus = (Label)e.Row.FindControl("lblStatus");
                    Label lblStatusTool = (Label)e.Row.FindControl("lblStatusTool");

                    HiddenField hfSelected = (HiddenField)e.Row.FindControl("hfSelected");

                    if (ChkAllCutoff.Checked || bool.Parse(hfSelected.Value) == true)
                    {
                        chk.Checked = true;

                        if (ChkAllCutoff.Checked && chk.Checked)
                        {
                            chk.Enabled = false;
                        }
                    }

                    else
                    {
                        chk.Checked = false;
                        chk.Enabled = true;
                    }

                    data_qa_cims _dataqaChecktool = new data_qa_cims();
                    _dataqaChecktool.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
                    qa_cims_u1_document_device_detail _selectDocument = new qa_cims_u1_document_device_detail();
                    _selectDocument.condition = 5;
                    _dataqaChecktool.qa_cims_u1_document_device_list[0] = _selectDocument;
                    _dataqaChecktool = callServicePostQaDocCIMS(_urlCimsGetEquipmentList, _dataqaChecktool);

                    if (_dataqaChecktool.qa_cims_u1_document_device_list != null)
                    {
                        var _linqCheckStatusTool = (from dt in _dataqaChecktool.qa_cims_u1_document_device_list
                                                    where dt.m0_device_idx == int.Parse(lblDeviceIDX.Text)
                                                    select new
                                                    {
                                                        dt.m0_device_idx
                                                    }).ToList();

                        string _CheckStatusTool = "";

                        for (int CheckStatusTool = 0; CheckStatusTool < _linqCheckStatusTool.Count(); CheckStatusTool++)
                        {
                            _CheckStatusTool = _linqCheckStatusTool[CheckStatusTool].m0_device_idx.ToString();
                        }

                        if (_CheckStatusTool == lblDeviceIDX.Text)
                        {
                            lblStatus.Text = "<i class='fa fa-circle' style='color: yellow; font - size: 8px; ' aria-hidden='true'></i>  " + "Waiting";
                            chk.Visible = true;
                            chk.Checked = false;
                            chk.Enabled = false;
                        }
                        else
                        {
                            if (lblStatusTool.Text == "1")
                            {
                                lblStatus.Text = "<i class='fa fa-circle' style='color: lime; font - size: 8px; ' aria-hidden='true'></i>  " + "Online";
                            }
                            else
                            {
                                lblStatus.Text = "<i class='fa fa-circle' style='color: red; font - size: 8px; ' aria-hidden='true'></i>  " + "Offline";
                            }
                        }
                    }

                }
                break;

            case "GvCalPointEdit":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    var _unit_idx_calpoint = (TextBox)e.Row.FindControl("Txbunit_idx_gv");
                    var _unit_name_calpoint = (Label)e.Row.FindControl("lbunit_symbol_en");

                    switch (_unit_idx_calpoint.Text)
                    {
                        case "29":
                            _unit_name_calpoint.Text = "µl";
                            break;

                        case "30":
                            _unit_name_calpoint.Text = "µm";
                            break;

                        case "31":
                            _unit_name_calpoint.Text = "°C";
                            break;
                    }
                }

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    var _IDXUnitCPView = (TextBox)e.Row.FindControl("Txbunit_idx");

                    DropDownList ddlUnitCP_Edit = (DropDownList)e.Row.FindControl("ddlUnitCP_Edit");
                    //litDebug.Text = _IDXUnitCPView.Text;
                    getUnitEdit((DropDownList)e.Row.FindControl("ddlUnitCP_Edit"), _IDXUnitCPView.Text);
                }

                break;

            case "gvDocumentList":

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    Label _U0DocIdx = (Label)e.Row.Cells[0].FindControl("lbU0DocIdx");
                    Label _lbrsec_idx_tranfer = (Label)e.Row.Cells[0].FindControl("lbrsec_idx_tranfer");

                    Label _lbl_rsec_idx_create = (Label)e.Row.Cells[1].FindControl("lbl_rsec_idx_create");

                    Label _U0DocType = (Label)e.Row.Cells[3].FindControl("lblDocumentTypeIDX");

                    Repeater _rptEquipmentList = (Repeater)e.Row.Cells[2].FindControl("rptEquipmentList");

                    Label _DecisionHead = (Label)e.Row.Cells[5].FindControl("lblDecisionHeadSection");
                    Label _DecisionIDXHead = (Label)e.Row.Cells[5].FindControl("lblDecisionIDXHeadSection");
                    Label _DecisionIDXHead2 = (Label)e.Row.Cells[5].FindControl("lblDecisionIDXHeadSection2");

                    Label _DecisionHeadQA = (Label)e.Row.Cells[5].FindControl("lblDecisionHeadQA");
                    Label _DecisionIDXHeadQA = (Label)e.Row.Cells[5].FindControl("lblDecisionIDXHeadQA");
                    Label _DecisionIDXHeadQA2 = (Label)e.Row.Cells[5].FindControl("lblDecisionIDXHeadQA2");

                    Label _DecisionReceiver = (Label)e.Row.Cells[5].FindControl("lblDecisionReceiver");
                    Label _DecisionIDXReceiver = (Label)e.Row.Cells[5].FindControl("lblDecisionIDXReceiver");
                    Label _DecisionIDXReceiver2 = (Label)e.Row.Cells[5].FindControl("lblDecisionIDXReceiver2");

                    Label _DecisionIDXQA = (Label)e.Row.Cells[5].FindControl("lblDecisionQA");
                    Label _DecisionIDXQA2 = (Label)e.Row.Cells[5].FindControl("lblDecisionQA2");

                    Label _DecisionIDXQACutoff = (Label)e.Row.Cells[5].FindControl("lblDecisionQACutoff");
                    Label _DecisionIDXQACutoff2 = (Label)e.Row.Cells[5].FindControl("lblDecisionQACutoff2");

                    Label lblstaidxQACutoff = (Label)e.Row.Cells[5].FindControl("lblstaidxQACutoff");
                    Label lblstaidxQACutoff2 = (Label)e.Row.Cells[5].FindControl("lblstaidxQACutoff2");

                    Label _lbDecision = (Label)e.Row.Cells[0].FindControl("lbDecision");
                    Label _lbActorIdx = (Label)e.Row.Cells[0].FindControl("lbActorIdx");
                    Label _lbStatusIDX = (Label)e.Row.Cells[4].FindControl("lblDocumentStatusIDX");

                    LinkButton btnHeadSectionApprove = (LinkButton)e.Row.Cells[5].FindControl("btnHeadSectionApprove");
                    LinkButton btnHeadSectionNotApprove = (LinkButton)e.Row.Cells[5].FindControl("btnHeadSectionNotApprove");

                    LinkButton btnHeadQAApprove = (LinkButton)e.Row.Cells[5].FindControl("btnHeadQAApprove");
                    LinkButton btnHeadQANotApprove = (LinkButton)e.Row.Cells[5].FindControl("btnHeadQANotApprove");

                    LinkButton btnReceiverApprove = (LinkButton)e.Row.Cells[5].FindControl("btnReceiverApprove");
                    LinkButton btnReceiverNotApprove = (LinkButton)e.Row.Cells[5].FindControl("btnReceiverNotApprove");

                    LinkButton btnViewDocList = (LinkButton)e.Row.Cells[6].FindControl("btnViewDocList");
                    LinkButton btnViewDocListTranfer = (LinkButton)e.Row.Cells[6].FindControl("btnViewDocListTranfer");

                    LinkButton btnViewDocListAllCutoff = (LinkButton)e.Row.Cells[6].FindControl("btnViewDocListAllCutoff");
                    LinkButton btnViewDocListAllTranfer = (LinkButton)e.Row.Cells[6].FindControl("btnViewDocListAllTranfer");


                    data_qa_cims _dtListDocument = new data_qa_cims();

                    if (_lbDecision.Text == "9" && _lbActorIdx.Text == "2" && _lbStatusIDX.Text == "30")
                    {
                        _dtListDocument.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
                        qa_cims_u1_document_device_detail _selectDocument = new qa_cims_u1_document_device_detail();
                        _selectDocument.u0_device_idx = int.Parse(_U0DocIdx.Text);
                        _selectDocument.condition = 3;
                        _dtListDocument.qa_cims_u1_document_device_list[0] = _selectDocument;
                        _dtListDocument = callServicePostQaDocCIMS(_urlCimsGetEquipmentList, _dtListDocument);

                        setRepeaterData(_rptEquipmentList, _dtListDocument.qa_cims_u1_document_device_list);
                    }
                    else if (_lbDecision.Text == "9" && _lbActorIdx.Text == "2" && _lbStatusIDX.Text == "29")
                    {
                        _dtListDocument.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
                        qa_cims_u1_document_device_detail _selectDocument = new qa_cims_u1_document_device_detail();
                        _selectDocument.u0_device_idx = int.Parse(_U0DocIdx.Text);
                        _selectDocument.condition = 4;
                        _dtListDocument.qa_cims_u1_document_device_list[0] = _selectDocument;
                        _dtListDocument = callServicePostQaDocCIMS(_urlCimsGetEquipmentList, _dtListDocument);

                        setRepeaterData(_rptEquipmentList, _dtListDocument.qa_cims_u1_document_device_list);
                    }
                    else
                    {
                        _dtListDocument.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
                        qa_cims_u1_document_device_detail _selectDocument = new qa_cims_u1_document_device_detail();
                        _selectDocument.u0_device_idx = int.Parse(_U0DocIdx.Text);
                        _selectDocument.condition = 1;
                        _dtListDocument.qa_cims_u1_document_device_list[0] = _selectDocument;
                        _dtListDocument = callServicePostQaDocCIMS(_urlCimsGetEquipmentList, _dtListDocument);

                        setRepeaterData(_rptEquipmentList, _dtListDocument.qa_cims_u1_document_device_list);
                    }
                    _dtListDocument.qa_cims_u0_document_device_list = new qa_cims_u0_document_device_detail[1];
                    qa_cims_u0_document_device_detail _selectU0Document = new qa_cims_u0_document_device_detail();
                    _selectU0Document.document_type_idx = int.Parse(_U0DocType.Text);
                    _dtListDocument.qa_cims_u0_document_device_list[0] = _selectU0Document;
                    _dtListDocument = callServicePostQaDocCIMS(_urlCimsGetDecisionHeadSection, _dtListDocument);

                    if (_dtListDocument.qa_cims_bindnode_decision_list[0].nodidx.ToString() != null)
                    {
                        _DecisionHead.Text = _dtListDocument.qa_cims_bindnode_decision_list[0].nodidx.ToString();
                        _DecisionIDXHead.Text = _dtListDocument.qa_cims_bindnode_decision_list[0].decision_idx.ToString();
                        _DecisionIDXHead2.Text = _dtListDocument.qa_cims_bindnode_decision_list[1].decision_idx.ToString();
                    }
                    else
                    {

                    }

                    data_qa_cims _dtListDocumentHeadQA = new data_qa_cims();
                    _dtListDocumentHeadQA.qa_cims_u0_document_device_list = new qa_cims_u0_document_device_detail[1];
                    qa_cims_u0_document_device_detail _selectU0DocumentHeadQA = new qa_cims_u0_document_device_detail();
                    _selectU0DocumentHeadQA.document_type_idx = int.Parse(_U0DocType.Text);
                    _dtListDocumentHeadQA.qa_cims_u0_document_device_list[0] = _selectU0DocumentHeadQA;
                    _dtListDocumentHeadQA = callServicePostQaDocCIMS(_urlCimsGetDecisionHeadQA, _dtListDocumentHeadQA);

                    //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtListDocumentHeadQA));

                    if (_dtListDocumentHeadQA.qa_cims_bindnode_decision_list[0].nodidx.ToString() != null)
                    {
                        _DecisionHeadQA.Text = _dtListDocumentHeadQA.qa_cims_bindnode_decision_list[0].nodidx.ToString();
                        _DecisionIDXHeadQA.Text = _dtListDocumentHeadQA.qa_cims_bindnode_decision_list[0].decision_idx.ToString();
                        _DecisionIDXHeadQA2.Text = _dtListDocumentHeadQA.qa_cims_bindnode_decision_list[1].decision_idx.ToString();
                    }

                    data_qa_cims _dtListDocumentReceiver = new data_qa_cims();
                    _dtListDocumentReceiver.qa_cims_u0_document_device_list = new qa_cims_u0_document_device_detail[1];
                    qa_cims_u0_document_device_detail _selectU0DocumentReceiver = new qa_cims_u0_document_device_detail();
                    //_selectU0DocumentReceiver.document_type_idx = int.Parse(_U0DocType.Text);
                    _dtListDocumentReceiver.qa_cims_u0_document_device_list[0] = _selectU0DocumentReceiver;
                    _dtListDocumentReceiver = callServicePostQaDocCIMS(_urlCimsGetDecisionReceiver, _dtListDocumentReceiver);

                    //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtListDocumentHeadQA));

                    if (_dtListDocumentReceiver.qa_cims_bindnode_decision_list[0].nodidx.ToString() != null)
                    {
                        _DecisionReceiver.Text = _dtListDocumentReceiver.qa_cims_bindnode_decision_list[0].nodidx.ToString();
                        _DecisionIDXReceiver.Text = _dtListDocumentReceiver.qa_cims_bindnode_decision_list[1].decision_idx.ToString();
                        _DecisionIDXReceiver2.Text = _dtListDocumentReceiver.qa_cims_bindnode_decision_list[0].decision_idx.ToString();
                    }

                    data_qa_cims _dataCimsDecisionNodeQATranfer = new data_qa_cims();
                    _dataCimsDecisionNodeQATranfer.qa_cims_bindnode_decision_list = new qa_cims_bindnode_decision_detail[1];
                    qa_cims_bindnode_decision_detail _selectDecisionNodeQATranfer = new qa_cims_bindnode_decision_detail();
                    _dataCimsDecisionNodeQATranfer.qa_cims_bindnode_decision_list[0] = _selectDecisionNodeQATranfer;

                    _dataCimsDecisionNodeQATranfer = callServicePostQaDocCIMS(_urlCimsGetDecisionQATranfer, _dataCimsDecisionNodeQATranfer);
                    if (_dataCimsDecisionNodeQATranfer.qa_cims_bindnode_decision_list[0].nodidx.ToString() != null)
                    {
                        //_DecisionReceiver.Text = _dataCimsDecisionNodeQATranfer.qa_cims_bindnode_decision_list[0].nodidx.ToString();
                        _DecisionIDXQA.Text = _dataCimsDecisionNodeQATranfer.qa_cims_bindnode_decision_list[1].decision_idx.ToString();
                        _DecisionIDXQA2.Text = _dataCimsDecisionNodeQATranfer.qa_cims_bindnode_decision_list[0].decision_idx.ToString();
                    }

                    data_qa_cims _dataCimsDecisionNodeQACutoff_ = new data_qa_cims();
                    _dataCimsDecisionNodeQACutoff_.qa_cims_bindnode_decision_list = new qa_cims_bindnode_decision_detail[1];
                    qa_cims_bindnode_decision_detail _selectDecisionNodeQACutoff_ = new qa_cims_bindnode_decision_detail();
                    _dataCimsDecisionNodeQACutoff_.qa_cims_bindnode_decision_list[0] = _selectDecisionNodeQACutoff_;

                    _dataCimsDecisionNodeQACutoff_ = callServicePostQaDocCIMS(_urlCimsGetDecisionQACutoff, _dataCimsDecisionNodeQACutoff_);
                    if (_dataCimsDecisionNodeQACutoff_.qa_cims_bindnode_decision_list[0].nodidx.ToString() != null)
                    {
                        //_DecisionReceiver.Text = _dataCimsDecisionNodeQATranfer.qa_cims_bindnode_decision_list[0].nodidx.ToString();
                        _DecisionIDXQACutoff.Text = _dataCimsDecisionNodeQACutoff_.qa_cims_bindnode_decision_list[1].decision_idx.ToString();
                        _DecisionIDXQACutoff2.Text = _dataCimsDecisionNodeQACutoff_.qa_cims_bindnode_decision_list[0].decision_idx.ToString();
                        lblstaidxQACutoff.Text = _dataCimsDecisionNodeQACutoff_.qa_cims_bindnode_decision_list[1].nodidx.ToString();
                        lblstaidxQACutoff2.Text = _dataCimsDecisionNodeQACutoff_.qa_cims_bindnode_decision_list[0].nodidx.ToString();
                    }


                    btnHeadSectionApprove.Visible = false;
                    btnHeadSectionNotApprove.Visible = false;
                    btnHeadQAApprove.Visible = false;
                    btnHeadQANotApprove.Visible = false;
                    btnReceiverApprove.Visible = false;
                    btnReceiverNotApprove.Visible = false;
                    btnViewDocListTranfer.Visible = false;
                    btnViewDocList.Visible = false;
                    btnViewDocListAllCutoff.Visible = false;
                    btnViewDocListAllTranfer.Visible = false;


                    switch (_lbStatusIDX.Text)
                    {
                        case "16":
                            btnViewDocListAllTranfer.Visible = true;
                            break;

                        case "17":
                            if ((ViewState["rsec_permission"].ToString() == _lbl_rsec_idx_create.Text) && (int.Parse(ViewState["jobgrade_permission"].ToString()) >= 5))
                            {
                                btnHeadSectionApprove.Visible = true;
                                btnHeadSectionNotApprove.Visible = true;
                                btnViewDocListAllTranfer.Visible = true;
                            }
                            else
                            {
                                btnViewDocListAllTranfer.Visible = true;
                            }
                            break;

                        case "18":

                            if (ViewState["rdept_permission"].ToString() == "27")
                            {
                                btnViewDocListTranfer.Visible = true;

                                btnViewDocList.Visible = false;

                                btnHeadSectionApprove.Visible = false;
                                btnHeadSectionNotApprove.Visible = false;
                                btnHeadQAApprove.Visible = false;
                                btnHeadQANotApprove.Visible = false;
                                btnReceiverApprove.Visible = false;
                                btnReceiverNotApprove.Visible = false;
                            }
                            else
                            {
                                btnViewDocListAllTranfer.Visible = true;
                            }
                            break;

                        case "19":
                            if (ViewState["rdept_permission"].ToString() == "27" && int.Parse(ViewState["jobgrade_permission"].ToString()) >= 5)
                            {
                                btnHeadQAApprove.Visible = true;
                                btnHeadQANotApprove.Visible = true;
                                btnViewDocListAllTranfer.Visible = true;
                            }
                            else
                            {
                                btnViewDocListAllTranfer.Visible = true;
                            }
                            break;

                        case "20":
                            if (ViewState["rsec_permission"].ToString() == _lbrsec_idx_tranfer.Text)
                            {
                                //litDebug.Text = "In Here";
                               // gvDocumentList.Columns[6].Visible = true;
                                btnReceiverApprove.Visible = true;
                                btnReceiverNotApprove.Visible = true;
                                btnViewDocListAllTranfer.Visible = true;
                            }
                            else
                            {
                                btnViewDocListAllTranfer.Visible = true;
                            }
                            break;

                        case "23":
                            if ((ViewState["rsec_permission"].ToString() == _lbl_rsec_idx_create.Text) && (int.Parse(ViewState["jobgrade_permission"].ToString()) >= 5))
                            {
                                btnHeadSectionApprove.Visible = true;
                                btnHeadSectionNotApprove.Visible = true;
                                btnViewDocListAllCutoff.Visible = true;
                            }
                            else
                            {
                                btnViewDocListAllCutoff.Visible = true;
                            }
                            break;

                        case "24":
                            if (ViewState["rdept_permission"].ToString() == "27")
                            {
                                btnViewDocList.Visible = true;

                                btnViewDocListTranfer.Visible = false;

                                btnHeadSectionApprove.Visible = false;
                                btnHeadSectionNotApprove.Visible = false;
                                btnHeadQAApprove.Visible = false;
                                btnHeadQANotApprove.Visible = false;
                                btnReceiverApprove.Visible = false;
                                btnReceiverNotApprove.Visible = false;
                            }
                            else
                            {
                                btnViewDocListAllCutoff.Visible = true;
                            }
                            break;

                        case "25":
                            if (ViewState["rdept_permission"].ToString() == "27" && int.Parse(ViewState["jobgrade_permission"].ToString()) >= 5)
                            {
                                btnHeadQAApprove.Visible = true;
                                btnHeadQANotApprove.Visible = true;
                                btnViewDocListAllCutoff.Visible = true;
                            }
                            else
                            {
                                btnViewDocListAllCutoff.Visible = true;
                            }
                            break;

                        case "27":
                            btnViewDocListAllCutoff.Visible = true;
                            break;

                        case "28":
                            btnViewDocListAllTranfer.Visible = true;
                            break;

                        case "29":
                            btnViewDocListAllTranfer.Visible = true;
                            break;

                        case "30":
                            btnViewDocListAllCutoff.Visible = true;
                            break;

                        case "31":
                            btnViewDocListAllTranfer.Visible = true;
                            break;
                        case "36":
                            btnViewDocListAllTranfer.Visible = true;
                            break;



                    }

                    //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtListDocumentHeadQA));

                }
                break;

            case "gvEquipmentQACutoff":

                List<int> listQA = ViewState["SelectedRecords"] as List<int>;
                if (e.Row.RowType == DataControlRowType.DataRow && listQA != null)
                {
                    var u1_device_idx = int.Parse(gvEquipmentQACutoff.DataKeys[e.Row.RowIndex].Value.ToString());
                    if (listQA.Contains(u1_device_idx))
                    {
                        CheckBox chk = (CheckBox)e.Row.FindControl("ChkEquipmentQACutoff");
                        chk.Checked = true;
                    }
                }


                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    var lblu0_device_idx_cut = (Label)e.Row.FindControl("lblu0_device_idx");
                    var lblEquipment_IDNO_cut = (Label)e.Row.FindControl("lblEquipment_IDNO");
                    //var status = (Label)e.Row.FindControl("lblstatus_idx");
                    //var status_name = (Label)e.Row.Cells[5].FindControl("lblstatusname");
                    var btnViewFile_Cutoff = (HyperLink)e.Row.FindControl("btnViewFile_Cutoff");

                    string filePath_Cutoff = ConfigurationManager.AppSettings["pathfile_document_cutoff"];
                    string directoryName = lblEquipment_IDNO_cut.Text.Trim() + lblu0_device_idx_cut.Text; //+ ViewState["vs_date_document_pahtfile"].ToString();

                    if (Directory.Exists(Server.MapPath(filePath_Cutoff + directoryName)))
                    {
                        string[] filesPath = Directory.GetFiles(Server.MapPath(filePath_Cutoff + directoryName));
                        List<ListItem> files = new List<ListItem>();
                        foreach (string path in filesPath)
                        {
                            string getfiles = "";
                            getfiles = Path.GetFileName(path);
                            btnViewFile_Cutoff.NavigateUrl = filePath_Cutoff + directoryName + "/" + getfiles;
                        }
                        btnViewFile_Cutoff.Visible = true;
                    }
                    else
                    {
                        btnViewFile_Cutoff.Visible = false;
                    }


                }
                break;

            case "Gv_CutOff_Success":

                //List<int> listQA = ViewState["SelectedRecords"] as List<int>;
                //if (e.Row.RowType == DataControlRowType.DataRow && listQA != null)
                //{
                //    var u1_device_idx = int.Parse(gvEquipmentQACutoff.DataKeys[e.Row.RowIndex].Value.ToString());
                //    if (listQA.Contains(u1_device_idx))
                //    {
                //        CheckBox chk = (CheckBox)e.Row.FindControl("ChkEquipmentQACutoff");
                //        chk.Checked = true;
                //    }
                //}


                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    var lbl_u0_device_idx_Success_cut = (Label)e.Row.FindControl("lbl_u0_device_idx_Success");
                    var lblEquipment_IDNO_Success_cut = (Label)e.Row.FindControl("lblEquipment_IDNO_Success");
                    //var status = (Label)e.Row.FindControl("lblstatus_idx");
                    //var status_name = (Label)e.Row.Cells[5].FindControl("lblstatusname");
                    var btnViewFile_Success_Cutoff = (HyperLink)e.Row.FindControl("btnViewFile_Success");

                    string filePath_Cutoff_ = ConfigurationManager.AppSettings["pathfile_document_cutoff"];
                    string directoryName_ = lblEquipment_IDNO_Success_cut.Text.Trim() + lbl_u0_device_idx_Success_cut.Text; //+ ViewState["vs_date_document_pahtfile"].ToString();

                    if (Directory.Exists(Server.MapPath(filePath_Cutoff_ + directoryName_)))
                    {
                        string[] filesPath_ = Directory.GetFiles(Server.MapPath(filePath_Cutoff_ + directoryName_));
                        List<ListItem> files = new List<ListItem>();
                        foreach (string path in filesPath_)
                        {
                            string getfiles = "";
                            getfiles = Path.GetFileName(path);
                            btnViewFile_Success_Cutoff.NavigateUrl = filesPath_ + directoryName_ + "/" + getfiles;
                        }
                        btnViewFile_Success_Cutoff.Visible = true;
                    }
                    else
                    {
                        btnViewFile_Success_Cutoff.Visible = false;
                    }


                }
                break;

            case "gvEquipmentQATranfer":

                List<int> listQATF = ViewState["SelectedRecords"] as List<int>;
                if (e.Row.RowType == DataControlRowType.DataRow && listQATF != null)
                {
                    var u1_device_idx = int.Parse(gvEquipmentQATranfer.DataKeys[e.Row.RowIndex].Value.ToString());
                    if (listQATF.Contains(u1_device_idx))
                    {

                        //litDebug.Text = "22222";

                        CheckBox chk = (CheckBox)e.Row.FindControl("ChkEquipmentQATranfer");
                        chk.Checked = true;


                    }
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    //litDebug.Text = "4444";

                    ////RadioButtonList rblQAApproveTranfer_ = (RadioButtonList)e.Row.FindControl("rblQAApproveTranfer");
                    ////Label lbl_status_ = (Label)e.Row.FindControl("lbl_status");
                    ////TextBox txtcomment_qacutoff = (TextBox)e.Row.FindControl("txtcomment_qacutoff");

                    ////switch (lbl_status_.Text)
                    ////{
                    ////    case "0":
                    ////        rblQAApproveTranfer_.Visible = false;
                    ////        txtcomment_qacutoff.Visible = false;
                    ////        //litDebug.Text = "22222";
                    ////        break;
                    ////    case "1":
                    ////        rblQAApproveTranfer_.Visible = true;
                    ////        txtcomment_qacutoff.Visible = true;
                    ////        //litDebug.Text = "3333";
                    ////        break;

                    ////}
                }



                break;

            case "gvCalibrationList":

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    var equipment_type = (TextBox)fvDetailsDocument.FindControl("tbtypeEquipment");
                    var placeidx = (TextBox)fvDetailsDocument.FindControl("tbplaceidx");
                    var gvCalibrationList = (GridView)fvCalDocumentList.FindControl("gvCalibrationList");
                    var pnlAction = (Panel)fvCalDocumentList.FindControl("pnlAction");

                    var status = (Label)e.Row.Cells[9].FindControl("lblstaidx");
                    var certificate = (Label)e.Row.Cells[8].FindControl("lblcertificate");
                    var record_certificate = (LinkButton)e.Row.Cells[10].FindControl("btnSaveCertificate");
                    var view_details = (LinkButton)e.Row.Cells[10].FindControl("btnViewDocCal");
                    var lblM0LabName = (Label)e.Row.Cells[7].FindControl("lblM0LabName");
                    var pnlChooseLocationCal = (Panel)e.Row.Cells[7].FindControl("pnlChooseLocationCal");


                    pnlAction.Visible = false;
                    pnlChooseLocationCal.Visible = false;

                    switch (equipment_type.Text)
                    {
                        case "1": // ประเภทเครื่องใหม่

                            gvCalibrationList.Columns[4].Visible = false;

                            switch (certificate.Text)
                            {
                                case "4":

                                    switch (status.Text)
                                    {
                                        case "1":
                                        case "8":
                                            //litDebug.Text = "ประเภทเครื่องใหม่ มีใบ cer";
                                            record_certificate.Visible = true;
                                            break;
                                    }


                                    break;

                                case "3":

                                    switch (status.Text)
                                    {
                                        case "1":
                                        case "2":
                                        case "3":
                                            //litDebug.Text = "ประเภทเครื่องใหม่ ไม่มีใบ cer";
                                            view_details.Visible = true;
                                            lblM0LabName.Visible = false;
                                            pnlChooseLocationCal.Visible = true;
                                            getLocationList((DropDownList)e.Row.Cells[7].FindControl("ddlChooseLocation"), "0", placeidx.Text);
                                            getChooseLabTypeCal((DropDownList)e.Row.Cells[7].FindControl("ddlChooseLabType"), "0", placeidx.Text);
                                            pnlAction.Visible = true;
                                            break;
                                    }


                                    break;
                            }

                            break;

                        case "2": // ประเภทเครื่องเก่า
                            //litDebug.Text = "ประเภทเครื่องเก่า";
                            gvCalibrationList.Columns[8].Visible = false;
                            switch (status.Text)
                            {
                                case "1":
                                    view_details.Visible = true;
                                    lblM0LabName.Visible = false;
                                    pnlChooseLocationCal.Visible = true;
                                    getLocationList((DropDownList)e.Row.Cells[7].FindControl("ddlChooseLocation"), "0", placeidx.Text);
                                    getChooseLabTypeCal((DropDownList)e.Row.Cells[7].FindControl("ddlChooseLabType"), "0", placeidx.Text);
                                    pnlAction.Visible = true;
                                    break;
                            }
                            break;
                    }

                }
                break;

            case "gvEquipmentAllCutoff":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    var lblEquipment_IDNumber = (Label)e.Row.Cells[0].FindControl("lblEquipment_IDNO");
                    var status = (Label)e.Row.Cells[5].FindControl("lblstatus_idx");
                    var status_name = (Label)e.Row.Cells[5].FindControl("lblstatusname");
                    var btnViewFile = (HyperLink)e.Row.Cells[4].FindControl("btnViewFile");



                    string filePath = ConfigurationManager.AppSettings["pathfile_document_cutoff"];
                    string directoryName = lblEquipment_IDNumber.Text.Trim() + ViewState["vs_date_document_pahtfile"].ToString();

                    if (Directory.Exists(Server.MapPath(filePath + directoryName)))
                    {
                        string[] filesPath = Directory.GetFiles(Server.MapPath(filePath + directoryName));
                        List<ListItem> files = new List<ListItem>();
                        foreach (string path in filesPath)
                        {
                            string getfiles = "";
                            getfiles = Path.GetFileName(path);
                            btnViewFile.NavigateUrl = filePath + directoryName + "/" + getfiles;
                        }
                        btnViewFile.Visible = true;
                    }
                    else
                    {
                        btnViewFile.Visible = false;
                    }
                    switch (status.Text)
                    {
                        case "29":
                        case "30":
                            status_name.Text = "รายการที่ไม่ถูกอนุมัติ";
                            status_name.ForeColor = System.Drawing.Color.Red;
                            break;
                    }





                }
                break;

            case "gvEquipmentAllCutoffNA":

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    var _lblEquipment_IDNumber = (Label)e.Row.Cells[0].FindControl("lblEquipmentIDNO");
                    var _btnViewFile = (HyperLink)e.Row.Cells[4].FindControl("btnViewFiles");
                    // Display the company name in italics.

                    if (_lblEquipment_IDNumber.Text != null)
                    {
                        string _filePath = ConfigurationManager.AppSettings["pathfile_document_cutoff"];
                        string _directoryName = _lblEquipment_IDNumber.Text.Trim() + ViewState["vs_date_document_pahtfile"].ToString();

                        if (Directory.Exists(Server.MapPath(_filePath + _directoryName)))
                        {
                            string[] _filesPath = Directory.GetFiles(Server.MapPath(_filePath + _directoryName));
                            List<ListItem> _files = new List<ListItem>();
                            foreach (string _path in _filesPath)
                            {
                                string _getfiles = "";
                                _getfiles = Path.GetFileName(_path);
                                _btnViewFile.NavigateUrl = _filePath + _directoryName + "/" + _getfiles;
                            }
                            _btnViewFile.Visible = true;
                        }
                        else
                        {
                            _btnViewFile.Visible = false;
                        }
                    }


                }
                break;

            case "gvEquipmentAllTranfer":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    var lblEquipment_IDNumber = (Label)e.Row.FindControl("lblEquipment_IDNO_headtranfer");
                    var status = (Label)e.Row.FindControl("lblstatus_idx_tran");
                    var status_name = (Label)e.Row.FindControl("lblstatusname");
                    var rbl_HeadApproveTranfer = (RadioButtonList)e.Row.FindControl("rbl_HeadApproveTranfer");
                    var txtcomment_headtranfer = (TextBox)e.Row.FindControl("txtcomment_headtranfer");


                    //var btnViewFile = (HyperLink)e.Row.Cells[4].FindControl("btnViewFile");

                    switch (status.Text)
                    {
                        case "17":
                            //gvEquipmentAllTranfer.Columns[0].Visible = true;
                            //gvEquipmentAllTranfer.Columns[2].Visible = true;
                            btnHeadUserSubmitQATranfer.Visible = true;
                            show_detail_devices_approve.Visible = true;
                            show_detail_devices_notapprove.Visible = true;
                            btnHeadAceptTranfer.Visible = false;
                            break;
                        case "18":
                            //gvEquipmentAllTranfer.Columns[0].Visible = false;
                            //gvEquipmentAllTranfer.Columns[2].Visible = false;
                            btnHeadUserSubmitQATranfer.Visible = false;

                            //show_detail_devices_approve.Visible = false;
                            //show_detail_devices_notapprove.Visible = false;
                            btnHeadAceptTranfer.Visible = false;
                            break;
                        case "29":
                        case "30":
                            rbl_HeadApproveTranfer.Visible = false;
                            txtcomment_headtranfer.Visible = false;
                            status_name.Text = "รายการที่ไม่ถูกอนุมัติ";
                            status_name.ForeColor = System.Drawing.Color.Red;

                            //gvEquipmentAllTranfer.Columns[0].Visible = false;
                            //gvEquipmentAllTranfer.Columns[2].Visible = false;
                            btnHeadUserSubmitQATranfer.Visible = false;
                            show_detail_devices_approve.Visible = false;
                            show_detail_devices_notapprove.Visible = false;
                            //e.Row.Cells[0].Visible = false;
                            //e.Row.Cells[2].Visible = false;


                            //btnHeadAceptTranfer.Visible = false;

                            break;

                        case "36":
                            //gvEquipmentAllTranfer.Columns[0].Visible = true;
                            // gvEquipmentAllTranfer.Columns[2].Visible = true;
                            rbl_HeadApproveTranfer.Visible = true;
                            txtcomment_headtranfer.Visible = true;
                            btnHeadUserSubmitQATranfer.Visible = false;
                            btnHeadAceptTranfer.Visible = true;
                            break;
                        case "28":

                            //gvEquipmentAllTranfer.Columns[0].Visible = false;
                            //gvEquipmentAllTranfer.Columns[2].Visible = false;
                            rbl_HeadApproveTranfer.Visible = false;
                            txtcomment_headtranfer.Visible = false;
                            btnHeadUserSubmitQATranfer.Visible = false;
                            btnHeadAceptTranfer.Visible = false;


                            break;
                    }
                }
                break;

            case "gvResolutionUpdate":

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    var _unit_idx_resolution_edit = (Label)e.Row.FindControl("lbl_unit_idx_resolution_edit");
                    var _unit_name_resolution_edit = (Label)e.Row.FindControl("lblunit_symbol_en_resolution_edit");

                    switch (_unit_idx_resolution_edit.Text)
                    {
                        case "29":
                            _unit_name_resolution_edit.Text = "µl";
                            break;

                        case "30":
                            _unit_name_resolution_edit.Text = "µm";
                            break;

                        case "31":
                            _unit_name_resolution_edit.Text = "°C";
                            break;
                    }
                }


                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    var _txtResolutionIDX = (Label)e.Row.FindControl("lblResolution_idx");
                    getResolution((DropDownList)e.Row.FindControl("ddlResolutionForUpdate"), _txtResolutionIDX.Text);
                }

                break;
            case "gvRangeUpdate":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    var _unit_idx_range = (Label)e.Row.FindControl("lbl_unit_idxupdate");
                    var _unit_name_range = (Label)e.Row.FindControl("lblunit_symbol_enupdate");

                    switch (_unit_idx_range.Text)
                    {
                        case "29":
                            _unit_name_range.Text = "µl";
                            break;

                        case "30":
                            _unit_name_range.Text = "µm";
                            break;

                        case "31":
                            _unit_name_range.Text = "°C";
                            break;
                    }
                }
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    var _txt_unit_idxeditrange = (TextBox)e.Row.FindControl("txt_unit_idxeditrange");

                    //DropDownList ddlUnitCP_Edit = (DropDownList)e.Row.FindControl("ddlUnitCP_Edit");
                    //litDebug.Text = _IDXUnitCPView.Text;
                    getUnitEdit((DropDownList)e.Row.FindControl("ddlrange_editrange"), _txt_unit_idxeditrange.Text);


                }

                break;
            case "gvMeasuringUpdate":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    var _unit_idx_measuring = (Label)e.Row.FindControl("lbl_unit_idxupdate_measuring");
                    var _unit_name_measuring = (Label)e.Row.FindControl("lblunit_symbol_enupdate_measuring");

                    switch (_unit_idx_measuring.Text)
                    {
                        case "29":
                            _unit_name_measuring.Text = "µl";
                            break;

                        case "30":
                            _unit_name_measuring.Text = "µm";
                            break;

                        case "31":
                            _unit_name_measuring.Text = "°C";
                            break;
                    }
                }
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    var _txt_unit_idxeditMeasuring = (Label)e.Row.FindControl("lbl_measuringunit_idxedit");

                    getUnitEdit((DropDownList)e.Row.FindControl("ddlmeasuring_editunit"), _txt_unit_idxeditMeasuring.Text);


                }

                break;
            case "gvAcceptanceUpdate":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    var _unit_idx_acceptance = (Label)e.Row.FindControl("lblunit_idxacceptanceupdate");
                    var _unit_name_acceptance = (Label)e.Row.FindControl("lblsymbol_acceptanceupdate");

                    switch (_unit_idx_acceptance.Text)
                    {
                        case "29":
                            _unit_name_acceptance.Text = "µl";
                            break;

                        case "30":
                            _unit_name_acceptance.Text = "µm";
                            break;

                        case "31":
                            _unit_name_acceptance.Text = "°C";
                            break;
                    }
                }

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    var _lbl_acceptance_criteriadit = (Label)e.Row.FindControl("lbl_acceptance_criteriadit");
                    getUnitEdit((DropDownList)e.Row.FindControl("ddlacceptance_criteriaedit"), _lbl_acceptance_criteriadit.Text);


                }

                break;

            case "gvRangePeview":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    var _unit_idx_range_preview = (Label)e.Row.FindControl("lblrange_idx_preview");
                    var _unit_name_range_preview = (Label)e.Row.FindControl("lblrange_preview_unit");

                    switch (_unit_idx_range_preview.Text)
                    {
                        case "29":
                            _unit_name_range_preview.Text = "µl";
                            break;

                        case "30":
                            _unit_name_range_preview.Text = "µm";
                            break;

                        case "31":
                            _unit_name_range_preview.Text = "°C";
                            break;
                    }
                }
                break;

            case "gvMeasuringPreview":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    var _unit_idx_measuring_preview = (Label)e.Row.FindControl("lbl_unit_idx_measuring_preview");
                    var _unit_name_measuring_preview = (Label)e.Row.FindControl("lblunit_symbol_en_measuring_preview");

                    switch (_unit_idx_measuring_preview.Text)
                    {
                        case "29":
                            _unit_name_measuring_preview.Text = "µl";
                            break;

                        case "30":
                            _unit_name_measuring_preview.Text = "µm";
                            break;

                        case "31":
                            _unit_name_measuring_preview.Text = "°C";
                            break;
                    }
                }
                break;

            case "gvAcceptancePreview":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    var _unit_idx_acceptance_preview = (Label)e.Row.FindControl("lblunit_idxacceptance_preview");
                    var _unit_name_acceptance_preview = (Label)e.Row.FindControl("lblsymbol_acceptance_preview");

                    switch (_unit_idx_acceptance_preview.Text)
                    {
                        case "29":
                            _unit_name_acceptance_preview.Text = "µl";
                            break;

                        case "30":
                            _unit_name_acceptance_preview.Text = "µm";
                            break;

                        case "31":
                            _unit_name_acceptance_preview.Text = "°C";
                            break;
                    }
                }
                break;
            case "gvResolutionPreview":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    var _unit_idx_resolution_preview = (Label)e.Row.FindControl("lbl_unit_idx_resolution_preview");
                    var _unit_name_resolution_preview = (Label)e.Row.FindControl("lblunit_symbol_en_resolution_preview");

                    switch (_unit_idx_resolution_preview.Text)
                    {
                        case "29":
                            _unit_name_resolution_preview.Text = "µl";
                            break;

                        case "30":
                            _unit_name_resolution_preview.Text = "µm";
                            break;

                        case "31":
                            _unit_name_resolution_preview.Text = "°C";
                            break;
                    }
                }
                break;
            case "GvCalPointPreview":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    var _unit_idx_calibration_point_preview = (Label)e.Row.FindControl("lbl_unit_idx_calibration_point_preview");
                    var _unit_name_calibration_point_preview = (Label)e.Row.FindControl("lblunit_symbol_en_calibration_point_preview");

                    switch (_unit_idx_calibration_point_preview.Text)
                    {
                        case "29":
                            _unit_name_calibration_point_preview.Text = "µl";
                            break;

                        case "30":
                            _unit_name_calibration_point_preview.Text = "µm";
                            break;

                        case "31":
                            _unit_name_calibration_point_preview.Text = "°C";
                            break;
                    }
                }
                break;
        }

    }

    protected void gvRowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvCalPointEdit":

                GridView GvCalPointEdit = (GridView)fvInsertCIMS.FindControl("GvCalPointEdit");
                GvCalPointEdit.EditIndex = e.NewEditIndex;
                SelectCalibration_point(ViewState["vsM0DeviceIDX"].ToString());

                break;

            case "gvResolutionUpdate":

                GridView gvResolutionUpdate = (GridView)fvInsertCIMS.FindControl("gvResolutionUpdate");
                gvResolutionUpdate.EditIndex = e.NewEditIndex;
                selectResolution(ViewState["vsM0DeviceIDX"].ToString());

                break;
            case "gvRangeUpdate":

                GridView gvRangeUpdate = (GridView)fvInsertCIMS.FindControl("gvRangeUpdate");
                gvRangeUpdate.EditIndex = e.NewEditIndex;

                selectgvRangeUpdate(ViewState["vsM0DeviceIDX"].ToString());

                break;
            case "gvMeasuringUpdate":

                GridView gvMeasuringUpdate = (GridView)fvInsertCIMS.FindControl("gvMeasuringUpdate");
                gvMeasuringUpdate.EditIndex = e.NewEditIndex;

                selectgvMeasuringUpdate(ViewState["vsM0DeviceIDX"].ToString());

                break;
            case "gvAcceptanceUpdate":

                GridView gvAcceptanceUpdate = (GridView)fvInsertCIMS.FindControl("gvAcceptanceUpdate");
                gvAcceptanceUpdate.EditIndex = e.NewEditIndex;

                selectgvAcceptanceUpdate(ViewState["vsM0DeviceIDX"].ToString());

                break;

        }
    }

    protected void gvRowUpdating(object sender, GridViewUpdateEventArgs e)
    {

        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvCalPointEdit":
                GridView GvCalPointEdit = (GridView)fvInsertCIMS.FindControl("GvCalPointEdit");
                var Txbcalibration_point = (TextBox)GvCalPointEdit.Rows[e.RowIndex].FindControl("Txbcalibration_point");
                var ID_M1 = (TextBox)GvCalPointEdit.Rows[e.RowIndex].FindControl("ID_M1");
                var ddlUnitCP_Edit = (DropDownList)GvCalPointEdit.Rows[e.RowIndex].FindControl("ddlUnitCP_Edit");

                GvCalPointEdit.EditIndex = -1;

                data_qa_cims r1_Update = new data_qa_cims();
                r1_Update.qa_cims_m1_registration_device_list = new qa_cims_m1_registration_device[1];
                qa_cims_m1_registration_device r1_s = new qa_cims_m1_registration_device();
                r1_s.m1_device_idx = int.Parse(ID_M1.Text);
                r1_s.m0_device_idx = int.Parse(ViewState["vsM0DeviceIDX"].ToString());
                r1_s.calibration_point = (Txbcalibration_point.Text);
                r1_s.unit_idx = int.Parse(ddlUnitCP_Edit.SelectedValue);
                r1_s.cemp_idx = _emp_idx;
                r1_s.status = 1;


                r1_Update.qa_cims_m1_registration_device_list[0] = r1_s;

                r1_Update = callServicePostMasterQACIMS(_urlCimsSetM1RegistrationDevice, r1_Update);

                if (r1_Update.return_code == 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('แก้ไขข้อมูลสำเร็จ !!!');", true);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว !!!');", true);
                }


                SelectCalibration_point(ViewState["vsM0DeviceIDX"].ToString());

                break;

            case "gvResolutionUpdate":

                GridView _gvResolutionUpdate = (GridView)fvInsertCIMS.FindControl("gvResolutionUpdate");
                var _ResolutionForUpdate = (DropDownList)_gvResolutionUpdate.Rows[e.RowIndex].FindControl("ddlResolutionForUpdate");
                var _m0_device_idx_update = (Label)_gvResolutionUpdate.Rows[e.RowIndex].FindControl("lbm0_device_idx_update");

                _gvResolutionUpdate.EditIndex = -1;

                data_qa_cims data_resolution = new data_qa_cims();
                data_resolution.qa_cims_m2_registration_device_list = new qa_cims_m2_registration_device[1];
                qa_cims_m2_registration_device update_resolution = new qa_cims_m2_registration_device();

                update_resolution.equipment_resolution_idx = int.Parse(_ResolutionForUpdate.SelectedValue);
                update_resolution.m0_device_idx = int.Parse(ViewState["vsM0DeviceIDX"].ToString());
                update_resolution.m2_device_resolution_idx = int.Parse(_m0_device_idx_update.Text);
                update_resolution.cemp_idx = _emp_idx;
                update_resolution.condition = 1;
                update_resolution.status = 1;

                data_resolution.qa_cims_m2_registration_device_list[0] = update_resolution;
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_resolution));
                data_resolution = callServicePostMasterQACIMS(_urlCimsSetM0_Resolution, data_resolution);

                if (data_resolution.return_code == 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('แก้ไขข้อมูลสำเร็จ !!!');", true);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว !!!');", true);
                }
                selectResolution(ViewState["vsM0DeviceIDX"].ToString());

                break;
            case "gvRangeUpdate":

                GridView _gvRangeUpdate = (GridView)fvInsertCIMS.FindControl("gvRangeUpdate");
                var _txt_range_start_update = (TextBox)_gvRangeUpdate.Rows[e.RowIndex].FindControl("txt_range_start_update");
                var _txt_range_endupdate = (TextBox)_gvRangeUpdate.Rows[e.RowIndex].FindControl("txt_range_endupdate");
                var _ddlrange_editrange = (DropDownList)_gvRangeUpdate.Rows[e.RowIndex].FindControl("ddlrange_editrange");
                var _lbm4_device_range_idx_update = (Label)_gvRangeUpdate.Rows[e.RowIndex].FindControl("lbm4_device_range_idx_update");

                _gvRangeUpdate.EditIndex = -1;

                data_qa_cims data_rangeuse_edit = new data_qa_cims();
                data_rangeuse_edit.qa_cims_m4_registration_device_list = new qa_cims_m4_registration_device[1];
                qa_cims_m4_registration_device update_rangeuse = new qa_cims_m4_registration_device();

                update_rangeuse.unit_idx = int.Parse(_ddlrange_editrange.SelectedValue);
                update_rangeuse.m0_device_idx = int.Parse(ViewState["vsM0DeviceIDX"].ToString());
                update_rangeuse.m4_device_range_idx = int.Parse(_lbm4_device_range_idx_update.Text);
                update_rangeuse.range_start = _txt_range_start_update.Text;
                update_rangeuse.range_end = _txt_range_endupdate.Text;
                update_rangeuse.cemp_idx = _emp_idx;
                //update_rangeuse.condition = 1;
                update_rangeuse.status = 1;

                data_rangeuse_edit.qa_cims_m4_registration_device_list[0] = update_rangeuse;
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_resolution));
                data_rangeuse_edit = callServicePostMasterQACIMS(_urlCimsSetRangeUseDevice, data_rangeuse_edit);

                if (data_rangeuse_edit.return_code == 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('แก้ไขข้อมูลสำเร็จ !!!');", true);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว !!!');", true);
                }

                selectgvRangeUpdate(ViewState["vsM0DeviceIDX"].ToString());

                break;
            case "gvMeasuringUpdate":

                GridView _gvMeasuringUpdate = (GridView)fvInsertCIMS.FindControl("gvMeasuringUpdate");
                var _txt_measuring_start_edit = (TextBox)_gvMeasuringUpdate.Rows[e.RowIndex].FindControl("txt_measuring_start_edit");
                var _txt_measuring_endedit = (TextBox)_gvMeasuringUpdate.Rows[e.RowIndex].FindControl("txt_measuring_endedit");
                var _ddlmeasuring_editunit = (DropDownList)_gvMeasuringUpdate.Rows[e.RowIndex].FindControl("ddlmeasuring_editunit");
                var _lbl_m5_device_measuring_idxedit = (Label)_gvMeasuringUpdate.Rows[e.RowIndex].FindControl("lbl_m5_device_measuring_idxedit");

                _gvMeasuringUpdate.EditIndex = -1;

                data_qa_cims data_measuring_edit = new data_qa_cims();
                data_measuring_edit.qa_cims_m5_registration_device_list = new qa_cims_m5_registration_device[1];
                qa_cims_m5_registration_device update_measuring = new qa_cims_m5_registration_device();

                update_measuring.unit_idx = int.Parse(_ddlmeasuring_editunit.SelectedValue);
                update_measuring.m0_device_idx = int.Parse(ViewState["vsM0DeviceIDX"].ToString());
                update_measuring.m5_device_measduring_ix = int.Parse(_lbl_m5_device_measuring_idxedit.Text);
                update_measuring.measuring_start = _txt_measuring_start_edit.Text;
                update_measuring.measuring_end = _txt_measuring_endedit.Text;
                update_measuring.cemp_idx = _emp_idx;
                //update_rangeuse.condition = 1;
                update_measuring.status = 1;

                data_measuring_edit.qa_cims_m5_registration_device_list[0] = update_measuring;
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_resolution));
                data_measuring_edit = callServicePostMasterQACIMS(_urlCimsSetMeasuringDevice, data_measuring_edit);

                if (data_measuring_edit.return_code == 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('แก้ไขข้อมูลสำเร็จ !!!');", true);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว !!!');", true);
                }

                selectgvMeasuringUpdate(ViewState["vsM0DeviceIDX"].ToString());

                break;
            case "gvAcceptanceUpdate":

                GridView _gvAcceptanceUpdate = (GridView)fvInsertCIMS.FindControl("gvAcceptanceUpdate");
                var _txt_acceptance_criteria_edit = (TextBox)_gvAcceptanceUpdate.Rows[e.RowIndex].FindControl("txt_acceptance_criteria_edit");

                var _ddlacceptance_criteriaedit = (DropDownList)_gvAcceptanceUpdate.Rows[e.RowIndex].FindControl("ddlacceptance_criteriaedit");
                var _lbl_m6_device_acceptance_idxdit = (Label)_gvAcceptanceUpdate.Rows[e.RowIndex].FindControl("lbl_m6_device_acceptance_idxdit");

                _gvAcceptanceUpdate.EditIndex = -1;

                data_qa_cims data_acceptance_edit = new data_qa_cims();
                data_acceptance_edit.qa_cims_m6_registration_device_list = new qa_cims_m6_registration_device[1];
                qa_cims_m6_registration_device update_acceptance = new qa_cims_m6_registration_device();

                update_acceptance.unit_idx = int.Parse(_ddlacceptance_criteriaedit.SelectedValue);
                update_acceptance.m0_device_idx = int.Parse(ViewState["vsM0DeviceIDX"].ToString());
                update_acceptance.m6_device_acceptance_idx = int.Parse(_lbl_m6_device_acceptance_idxdit.Text);
                update_acceptance.acceptance_criteria = _txt_acceptance_criteria_edit.Text;
                update_acceptance.cemp_idx = _emp_idx;
                //update_rangeuse.condition = 1;
                update_acceptance.status = 1;

                data_acceptance_edit.qa_cims_m6_registration_device_list[0] = update_acceptance;
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_resolution));
                data_acceptance_edit = callServicePostMasterQACIMS(_urlCimsSetAcceptanceDevice, data_acceptance_edit);

                if (data_acceptance_edit.return_code == 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('แก้ไขข้อมูลสำเร็จ !!!');", true);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว !!!');", true);
                }

                selectgvAcceptanceUpdate(ViewState["vsM0DeviceIDX"].ToString());

                break;

        }
    }

    protected void gvRowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        //Label5.Text = GvName.ID;
        switch (GvName.ID)
        {
            case "GvCalPointEdit":
                GridView GvCalPointEdit = (GridView)fvInsertCIMS.FindControl("GvCalPointEdit");
                GvCalPointEdit.EditIndex = -1;
                SelectCalibration_point(ViewState["vsM0DeviceIDX"].ToString());

                break;
            case "gvRangeUpdate":
                GridView gvRangeUpdate = (GridView)fvInsertCIMS.FindControl("gvRangeUpdate");
                gvRangeUpdate.EditIndex = -1;
                selectgvRangeUpdate(ViewState["vsM0DeviceIDX"].ToString());

                break;
            case "gvResolutionUpdate":
                GridView gvResolutionUpdate = (GridView)fvInsertCIMS.FindControl("gvResolutionUpdate");
                gvResolutionUpdate.EditIndex = -1;

                selectResolution(ViewState["vsM0DeviceIDX"].ToString());

                break;
            case "gvMeasuringUpdate":
                GridView gvMeasuringUpdate = (GridView)fvInsertCIMS.FindControl("gvMeasuringUpdate");
                gvMeasuringUpdate.EditIndex = -1;

                selectgvMeasuringUpdate(ViewState["vsM0DeviceIDX"].ToString());

                break;
            case "gvAcceptanceUpdate":
                GridView gvAcceptanceUpdate = (GridView)fvInsertCIMS.FindControl("gvAcceptanceUpdate");
                gvAcceptanceUpdate.EditIndex = -1;

                selectgvAcceptanceUpdate(ViewState["vsM0DeviceIDX"].ToString());

                break;

        }
    }

    protected void onRowCommand(Object sender, GridViewCommandEventArgs e)
    {
        if (sender is GridView)
        {
            string cmdName = e.CommandName;
            switch (cmdName)
            {
                case "btnRemovePlacePer":
                    GridView gvPlaceList = (GridView)fvInsertPerQA.FindControl("gvPlaceList");
                    GridViewRow rowSelect = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    int rowIndex = rowSelect.RowIndex;
                    DataSet dsContacts = (DataSet)ViewState["vsPlacePermissionList"];
                    dsContacts.Tables["dsPlacePermissionTable"].Rows[rowIndex].Delete();
                    dsContacts.AcceptChanges();
                    setGridData(gvPlaceList, dsContacts.Tables["dsPlacePermissionTable"]);
                    if (dsContacts.Tables["dsPlacePermissionTable"].Rows.Count < 1)
                    {
                        gvPlaceList.Visible = false;
                    }
                    break;
                case "btnRemovePerManage":
                    GridView gvPerManageList = (GridView)fvInsertPerQA.FindControl("gvPerManageList");
                    GridViewRow rowSelect_per = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    int rowIndex_per = rowSelect_per.RowIndex;
                    DataSet dsContacts_per = (DataSet)ViewState["vsPerManageList"];
                    dsContacts_per.Tables["dsPerManageTable"].Rows[rowIndex_per].Delete();
                    dsContacts_per.AcceptChanges();
                    setGridData(gvPerManageList, dsContacts_per.Tables["dsPerManageTable"]);
                    if (dsContacts_per.Tables["dsPerManageTable"].Rows.Count < 1)
                    {
                        gvPerManageList.Visible = false;
                    }
                    break;

                case "btnRemovePlacePerUpdate":
                    GridView gvPlacePerListUpdate = (GridView)fvInsertPerQA.FindControl("gvPlacePerListUpdate");
                    GridViewRow rowSelectUpdate_place = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    int rowIndexUpdate = rowSelectUpdate_place.RowIndex;

                    DataSet dsContactsUpdate = (DataSet)ViewState["vsPlaceListUpdate"];
                    dsContactsUpdate.Tables["dsPlaceTableUpdate"].Rows[rowIndexUpdate].Delete();
                    dsContactsUpdate.AcceptChanges();

                    setGridData(gvPlacePerListUpdate, dsContactsUpdate.Tables["dsPlaceTableUpdate"]);
                    if (dsContactsUpdate.Tables["dsPlaceTableUpdate"].Rows.Count < 1)
                    {
                        gvPlacePerListUpdate.Visible = false;
                    }
                    break;
                case "btnRemovePerPerUpdate":

                    GridView gvPerPerListUpdate = (GridView)fvInsertPerQA.FindControl("gvPerPerListUpdate");
                    GridViewRow rowSelectUpdate_per = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    int rowIndexUpdate_per = rowSelectUpdate_per.RowIndex;

                    DataSet dsContactsUpdate_per = (DataSet)ViewState["vsPerListUpdate"];
                    dsContactsUpdate_per.Tables["dsPerTableUpdate"].Rows[rowIndexUpdate_per].Delete();
                    dsContactsUpdate_per.AcceptChanges();

                    setGridData(gvPerPerListUpdate, dsContactsUpdate_per.Tables["dsPerTableUpdate"]);
                    if (dsContactsUpdate_per.Tables["dsPerTableUpdate"].Rows.Count < 1)
                    {
                        gvPerPerListUpdate.Visible = false;
                    }
                    break;
            }
        }
    }

    #endregion

    #region event formview

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    //protected void setFormData(FormView fvName, Object obj)
    //{
    //    fvName.DataSource = obj;
    //    fvName.DataBind();
    //}

    #endregion

    #region event checkbox


    private void savecheckedvalus()
    {
        ArrayList usercontent = new ArrayList();
        int index = -1;
        foreach (GridViewRow gvrow in gvListEquipment.Rows)
        {
            index = Convert.ToInt32(gvListEquipment.DataKeys[gvrow.RowIndex].Value);
            bool result = ((CheckBox)gvrow.FindControl("ChkEquipment")).Checked;

            // Check in the Session
            if (ViewState["chkditems"] != null)
                usercontent = (ArrayList)ViewState["chkditems"];
            if (result)
            {
                if (!usercontent.Contains(index))
                    usercontent.Add(index);
            }
            else
                usercontent.Remove(index);
        }
        if (usercontent != null && usercontent.Count > 0)
            ViewState["chkditems"] = usercontent;
    }

    private void savecheckedvaluscutoff()
    {
        ArrayList usercontent = new ArrayList();
        int index = -1;
        foreach (GridViewRow gvrow in gvListEquipmentCutoff.Rows)
        {
            index = Convert.ToInt32(gvListEquipmentCutoff.DataKeys[gvrow.RowIndex].Value);
            bool result = ((CheckBox)gvrow.FindControl("ChkEquipmentCutoff")).Checked;

            // Check in the Session
            if (ViewState["chkditems"] != null)
                usercontent = (ArrayList)ViewState["chkditems"];
            if (result)
            {
                if (!usercontent.Contains(index))
                    usercontent.Add(index);
            }
            else
                usercontent.Remove(index);
        }
        if (usercontent != null && usercontent.Count > 0)
            ViewState["chkditems"] = usercontent;
    }

    private void savecheckedvalusQAcutoff()
    {
        ArrayList usercontent = new ArrayList();
        int index = -1;
        foreach (GridViewRow gvrow in gvEquipmentQACutoff.Rows)
        {
            index = Convert.ToInt32(gvEquipmentQACutoff.DataKeys[gvrow.RowIndex].Value);
            bool result = ((CheckBox)gvrow.FindControl("ChkEquipmentQACutoff")).Checked;

            // Check in the Session
            if (ViewState["chkditems"] != null)
                usercontent = (ArrayList)ViewState["chkditems"];
            if (result)
            {
                if (!usercontent.Contains(index))
                    usercontent.Add(index);
            }
            else
                usercontent.Remove(index);
        }
        if (usercontent != null && usercontent.Count > 0)
            ViewState["chkditems"] = usercontent;
    }

    private void savecheckedvalusQAtranfer()
    {
        ArrayList usercontent = new ArrayList();
        int index = -1;
        foreach (GridViewRow gvrow in gvEquipmentQATranfer.Rows)
        {
            index = Convert.ToInt32(gvEquipmentQATranfer.DataKeys[gvrow.RowIndex].Value);
            bool result = ((CheckBox)gvrow.FindControl("ChkEquipmentQATranfer")).Checked;

            // Check in the Session
            if (ViewState["chkditems"] != null)
                usercontent = (ArrayList)ViewState["chkditems"];
            if (result)
            {
                if (!usercontent.Contains(index))
                    usercontent.Add(index);
            }
            else
                usercontent.Remove(index);
        }
        if (usercontent != null && usercontent.Count > 0)
            ViewState["chkditems"] = usercontent;
    }

    private void checkeddvaluesp()
    {
        ArrayList usercontent = (ArrayList)ViewState["chkditems"];
        if (usercontent != null && usercontent.Count > 0)
        {

            foreach (GridViewRow gvrow in gvListEquipment.Rows)
            {
                int index = Convert.ToInt32(gvListEquipment.DataKeys[gvrow.RowIndex].Value);
                if (usercontent.Contains(index))
                {
                    CheckBox myCheckBox = (CheckBox)gvrow.FindControl("ChkEquipment");
                    myCheckBox.Checked = true;
                }
            }
        }
    }

    private void checkeddvaluespcutoff()
    {
        ArrayList usercontent = (ArrayList)ViewState["chkditems"];
        if (usercontent != null && usercontent.Count > 0)
        {

            foreach (GridViewRow gvrow in gvListEquipmentCutoff.Rows)
            {
                int index = Convert.ToInt32(gvListEquipmentCutoff.DataKeys[gvrow.RowIndex].Value);
                if (usercontent.Contains(index))
                {
                    CheckBox myCheckBox = (CheckBox)gvrow.FindControl("ChkEquipmentCutoff");
                    myCheckBox.Checked = true;
                }
            }
        }
    }

    private void checkeddvaluespQAcutoff()
    {
        ArrayList usercontent = (ArrayList)ViewState["chkditems"];
        if (usercontent != null && usercontent.Count > 0)
        {

            foreach (GridViewRow gvrow in gvEquipmentQACutoff.Rows)
            {
                int index = Convert.ToInt32(gvEquipmentQACutoff.DataKeys[gvrow.RowIndex].Value);
                if (usercontent.Contains(index))
                {
                    CheckBox myCheckBox = (CheckBox)gvrow.FindControl("ChkEquipmentQACutoff");
                    myCheckBox.Checked = true;
                }
            }
        }
    }

    private void checkeddvaluespQAtranfer()
    {
        ArrayList usercontent = (ArrayList)ViewState["chkditems"];
        if (usercontent != null && usercontent.Count > 0)
        {

            foreach (GridViewRow gvrow in gvEquipmentQATranfer.Rows)
            {
                int index = Convert.ToInt32(gvEquipmentQATranfer.DataKeys[gvrow.RowIndex].Value);
                if (usercontent.Contains(index))
                {
                    CheckBox myCheckBox = (CheckBox)gvrow.FindControl("ChkEquipmentQATranfer");
                    myCheckBox.Checked = true;
                }
            }
        }
    }

    protected void checkindexchange(object sender, EventArgs e)
    {
        var chkName = (CheckBox)sender;

        divAlertTranferSeacrh.Visible = false;
        divAlertCutoffSeacrh.Visible = false;

        switch (chkName.ID)
        {
            case "ChkEquipmentAll":

                var _chkAll = (CheckBox)gvListEquipment.HeaderRow.FindControl("ChkEquipmentAll");


                foreach (GridViewRow row in gvListEquipment.Rows)
                {

                    CheckBox ChkBoxRows = (CheckBox)row.FindControl("ChkEquipment");

                    if (_chkAll.Checked == true)
                    {
                        qa_cims_m0_registration_device[] _templistEquipmentHolder = (qa_cims_m0_registration_device[])ViewState["vsEquipmentListHolder"];

                        ChkBoxRows.Checked = true;
                        ChkBoxRows.Enabled = false;

                        // vsEquipmentListHolder
                    }
                    else
                    {
                        ChkBoxRows.Checked = false;
                        ChkBoxRows.Enabled = true;
                    }
                }

                break;

            case "ChkAll":

                int count_check = 0;

                if (ChkAll.Checked == true)
                {

                    setGridData(gvListEquipment, ViewState["vsEquipmentListHolder"]);

                    foreach (GridViewRow row in gvListEquipment.Rows)
                    {
                        CheckBox ChkBoxRows = (CheckBox)row.FindControl("ChkEquipment");
                        Label lblDeviceIDX = (Label)row.FindControl("lblDeviceIDX");
                        //HiddenField hfSelected = (HiddenField)row.FindControl("hfSelected");

                        qa_cims_m0_registration_device[] _templistEquipmentHolder = (qa_cims_m0_registration_device[])ViewState["vsEquipmentListHolder"];

                        ChkBoxRows.Checked = true;
                        ChkBoxRows.Enabled = false;
                        tbSearchIDNoTranfer.Enabled = false;
                        _templistEquipmentHolder[count_check].selected = true;
                        count_check++;
                        //hfSelected.Value = true.ToString();
                    }

                }
                else if (ChkAll.Checked == false)
                {
                    foreach (GridViewRow row in gvListEquipment.Rows)
                    {
                        CheckBox ChkBoxRows = (CheckBox)row.FindControl("ChkEquipment");
                        Label lblDeviceIDX = (Label)row.FindControl("lblDeviceIDX");
                        //HiddenField hfSelected = (HiddenField)row.FindControl("hfSelected");
                        qa_cims_m0_registration_device[] _templistEquipmentHolder = (qa_cims_m0_registration_device[])ViewState["vsEquipmentListHolder"];

                        ChkBoxRows.Checked = false;
                        ChkBoxRows.Enabled = true;
                        tbSearchIDNoTranfer.Enabled = true;
                        _templistEquipmentHolder[count_check].selected = false;
                        count_check++;
                        //hfSelected.Value = false.ToString();

                    }
                }

                break;

            case "ChkEquipment":

                if (ViewState["vsEquipmentListHolder"] != null)
                {

                    qa_cims_m0_registration_device[] _templistEquipmentHolder = (qa_cims_m0_registration_device[])ViewState["vsEquipmentListHolder"];

                    int _checked = int.Parse(chkName.Text);
                    _templistEquipmentHolder[_checked].selected = chkName.Checked;

                }


                break;

            case "ChkAllCutoff":

                if (ChkAllCutoff.Checked == true)
                {

                    setGridData(gvListEquipmentCutoff, ViewState["vsEquipmentListHolderCutoff"]);

                    foreach (GridViewRow row in gvListEquipmentCutoff.Rows)
                    {
                        CheckBox ChkBoxRows = (CheckBox)row.FindControl("ChkEquipmentCutoff");
                        Label lblDeviceIDX = (Label)row.FindControl("lblDeviceIDX");
                        Label lblEquipment_IDNO = (Label)row.FindControl("lblEquipment_IDNO");

                        qa_cims_m0_registration_device[] _templistEquipmentHolder = (qa_cims_m0_registration_device[])ViewState["vsEquipmentListHolderCutoff"];

                        if (ViewState["vsEquipmentListHolderCutoff"] != null)
                        {
                            //lst.Add(new GetValueToRepeater() { m0_device_idx = int.Parse(lblDeviceIDX.Text), device_id_no = lblEquipment_IDNO.Text });

                            data_qa_cims _dataqaChecktool = new data_qa_cims();
                            _dataqaChecktool.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
                            qa_cims_u1_document_device_detail _selectDocument = new qa_cims_u1_document_device_detail();
                            _selectDocument.condition = 5;
                            _dataqaChecktool.qa_cims_u1_document_device_list[0] = _selectDocument;
                            _dataqaChecktool = callServicePostQaDocCIMS(_urlCimsGetEquipmentList, _dataqaChecktool);

                            lst.Add(new GetValueToRepeater() { m0_device_idx = int.Parse(lblDeviceIDX.Text), device_id_no = lblEquipment_IDNO.Text });
                            tbSearchIDNoCutoff.Enabled = false;
                        }
                    }

                    RepeaterCutoff.DataSource = lst;
                    RepeaterCutoff.DataBind();

                }
                else if (ChkAllCutoff.Checked == false)
                {
                    foreach (GridViewRow row in gvListEquipmentCutoff.Rows)
                    {
                        lst = null;

                        CheckBox ChkBoxRows = (CheckBox)row.FindControl("ChkEquipmentCutoff");
                        Label lblDeviceIDX = (Label)row.FindControl("lblDeviceIDX");
                        qa_cims_m0_registration_device[] _templistEquipmentHolder = (qa_cims_m0_registration_device[])ViewState["vsEquipmentListHolderCutoff"];

                        data_qa_cims _dataqaChecktool = new data_qa_cims();
                        _dataqaChecktool.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
                        qa_cims_u1_document_device_detail _selectDocument = new qa_cims_u1_document_device_detail();
                        _selectDocument.condition = 5;
                        _dataqaChecktool.qa_cims_u1_document_device_list[0] = _selectDocument;
                        _dataqaChecktool = callServicePostQaDocCIMS(_urlCimsGetEquipmentList, _dataqaChecktool);

                        ChkBoxRows.Checked = false;
                        ChkBoxRows.Enabled = true;
                        tbSearchIDNoCutoff.Enabled = true;
                    }

                    RepeaterCutoff.DataSource = lst;
                    RepeaterCutoff.DataBind();

                }
                break;

            case "ChkEquipmentCutoff":

                foreach (GridViewRow rowItems in gvListEquipmentCutoff.Rows)
                {
                    CheckBox chk = (CheckBox)rowItems.FindControl("ChkEquipmentCutoff");
                    Label lblDeviceIDX = (Label)rowItems.FindControl("lblDeviceIDX");
                    Label lblEquipment_IDNO = (Label)rowItems.FindControl("lblEquipment_IDNO");

                    if (chk.Checked && lst != null)
                    {
                        chk.Checked = true;
                        lst.Add(new GetValueToRepeater() { m0_device_idx = int.Parse(lblDeviceIDX.Text), device_id_no = lblEquipment_IDNO.Text });

                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(lst));
                        RepeaterCutoff.DataSource = lst;
                        RepeaterCutoff.DataBind();
                        // litDebug.Text = "0000";
                    }
                    else
                    {

                        RepeaterCutoff.DataSource = lst;
                        RepeaterCutoff.DataBind();
                    }

                }

                break;

            case "CheckAllQACutoff":

                if (CheckAllQACutoff.Checked == true)
                {
                    foreach (GridViewRow row in gvEquipmentQACutoff.Rows)
                    {
                        CheckBox ChkBoxRows = (CheckBox)row.FindControl("ChkEquipmentQACutoff");


                        ChkBoxRows.Checked = true;
                        ChkBoxRows.Enabled = false;

                    }
                }
                else if (CheckAllQACutoff.Checked == false)
                {
                    foreach (GridViewRow row in gvEquipmentQACutoff.Rows)
                    {
                        CheckBox ChkBoxRows = (CheckBox)row.FindControl("ChkEquipmentQACutoff");

                        ChkBoxRows.Checked = false;
                        ChkBoxRows.Enabled = true;
                    }
                }

                break;

            case "CheckAllQATranfer":

                if (CheckAllQATranfer.Checked == true)
                {
                    foreach (GridViewRow row in gvEquipmentQATranfer.Rows)
                    {
                        CheckBox ChkBoxRows = (CheckBox)row.FindControl("ChkEquipmentQATranfer");


                        ChkBoxRows.Checked = true;
                        ChkBoxRows.Enabled = false;

                    }
                }
                else if (CheckAllQATranfer.Checked == false)
                {
                    foreach (GridViewRow row in gvEquipmentQATranfer.Rows)
                    {
                        CheckBox ChkBoxRows = (CheckBox)row.FindControl("ChkEquipmentQATranfer");

                        ChkBoxRows.Checked = false;
                        ChkBoxRows.Enabled = true;
                    }
                }

                break;

            case "CheckAllQAApproveCutoff":
                if (CheckAllQAApproveCutoff.Checked == true)
                {
                    foreach (GridViewRow row in gvEquipmentQACutoff.Rows)
                    {
                        RadioButtonList rblQAApproveCutoff = (RadioButtonList)row.FindControl("rblQAApproveCutoff");

                        rblQAApproveCutoff.SelectedValue = "1";
                        //rblQAApproveCutoff.Enabled = false;

                    }
                    CheckAllQANotApproveCutoff.Checked = false;
                }
                else if (CheckAllQAApproveCutoff.Checked == false)
                {
                    foreach (GridViewRow row in gvEquipmentQACutoff.Rows)
                    {
                        RadioButtonList rblQAApproveCutoff = (RadioButtonList)row.FindControl("rblQAApproveCutoff");

                        rblQAApproveCutoff.SelectedValue = null;
                        //rblQAApproveCutoff.Enabled = true;
                    }
                    CheckAllQANotApproveCutoff.Checked = false;
                }
                break;

            case "CheckAllQANotApproveCutoff":
                if (CheckAllQANotApproveCutoff.Checked == true)
                {
                    foreach (GridViewRow row in gvEquipmentQACutoff.Rows)
                    {
                        RadioButtonList rblQAApproveCutoff = (RadioButtonList)row.FindControl("rblQAApproveCutoff");

                        rblQAApproveCutoff.SelectedValue = "2";
                        //rblQAApproveCutoff.Enabled = false;

                    }
                    CheckAllQAApproveCutoff.Checked = false;
                }
                else if (CheckAllQANotApproveCutoff.Checked == false)
                {
                    foreach (GridViewRow row in gvEquipmentQACutoff.Rows)
                    {
                        RadioButtonList rblQAApproveCutoff = (RadioButtonList)row.FindControl("rblQAApproveCutoff");

                        rblQAApproveCutoff.SelectedValue = null;
                        //rblQAApproveCutoff.Enabled = true;
                    }
                    CheckAllQAApproveCutoff.Checked = false;
                }
                break;

            case "CheckAllQAApproveTranfer":
                if (CheckAllQAApproveTranfer.Checked == true)
                {
                    foreach (GridViewRow row in gvEquipmentQATranfer.Rows)
                    {
                        RadioButtonList rblQAApproveTranfer = (RadioButtonList)row.FindControl("rblQAApproveTranfer");

                        rblQAApproveTranfer.SelectedValue = "1";
                        //rblQAApproveCutoff.Enabled = false;

                    }
                    CheckAllQANotApproveTranfer.Checked = false;
                }
                else if (CheckAllQAApproveTranfer.Checked == false)
                {
                    foreach (GridViewRow row in gvEquipmentQATranfer.Rows)
                    {
                        RadioButtonList rblQAApproveTranfer = (RadioButtonList)row.FindControl("rblQAApproveTranfer");

                        rblQAApproveTranfer.SelectedValue = null;
                        //rblQAApproveCutoff.Enabled = true;
                    }
                    CheckAllQANotApproveTranfer.Checked = false;
                }
                break;

            case "CheckAllQANotApproveTranfer":
                if (CheckAllQANotApproveTranfer.Checked == true)
                {
                    foreach (GridViewRow row in gvEquipmentQATranfer.Rows)
                    {
                        RadioButtonList rblQAApproveTranfer = (RadioButtonList)row.FindControl("rblQAApproveTranfer");

                        rblQAApproveTranfer.SelectedValue = "2";
                        //rblQAApproveCutoff.Enabled = false;

                    }
                    CheckAllQAApproveTranfer.Checked = false;
                }
                else if (CheckAllQANotApproveTranfer.Checked == false)
                {
                    foreach (GridViewRow row in gvEquipmentQATranfer.Rows)
                    {
                        RadioButtonList rblQAApproveTranfer = (RadioButtonList)row.FindControl("rblQAApproveTranfer");

                        rblQAApproveTranfer.SelectedValue = null;
                        //rblQAApproveCutoff.Enabled = true;
                    }
                    CheckAllQAApproveTranfer.Checked = false;
                }
                break;
            case "CheckAll_HeadApproveTranfer":
                if (CheckAll_HeadApproveTranfer.Checked == true)
                {
                    foreach (GridViewRow row_ in gvEquipmentAllTranfer.Rows)
                    {
                        RadioButtonList rbl_HeadApproveTranfer = (RadioButtonList)row_.FindControl("rbl_HeadApproveTranfer");

                        rbl_HeadApproveTranfer.SelectedValue = "1";
                        //CheckAll_HeadApproveTranfer.Checked = true;
                        //rblQAApproveCutoff.Enabled = false;

                    }
                    CheckAll_HeadNotApproveTranfer.Checked = false;
                }
                else if (CheckAll_HeadApproveTranfer.Checked == false)
                {
                    foreach (GridViewRow row_ in gvEquipmentAllTranfer.Rows)
                    {
                        RadioButtonList rbl_HeadApproveTranfer = (RadioButtonList)row_.FindControl("rbl_HeadApproveTranfer");

                        rbl_HeadApproveTranfer.SelectedValue = null;
                        //rblQAApproveCutoff.Enabled = true;
                    }
                    CheckAll_HeadNotApproveTranfer.Checked = false;
                }
                break;
            case "CheckAll_HeadNotApproveTranfer":
                if (CheckAll_HeadNotApproveTranfer.Checked == true)
                {
                    foreach (GridViewRow row_ in gvEquipmentAllTranfer.Rows)
                    {
                        RadioButtonList rbl_HeadApproveTranfer = (RadioButtonList)row_.FindControl("rbl_HeadApproveTranfer");

                        rbl_HeadApproveTranfer.SelectedValue = "2";
                        //CheckAll_HeadApproveTranfer.Checked = true;
                        //rblQAApproveCutoff.Enabled = false;

                    }
                    CheckAll_HeadApproveTranfer.Checked = false;
                }
                else if (CheckAll_HeadNotApproveTranfer.Checked == false)
                {
                    foreach (GridViewRow row_ in gvEquipmentAllTranfer.Rows)
                    {
                        RadioButtonList rbl_HeadApproveTranfer = (RadioButtonList)row_.FindControl("rbl_HeadApproveTranfer");

                        rbl_HeadApproveTranfer.SelectedValue = null;
                        //rblQAApproveCutoff.Enabled = true;
                    }
                    CheckAll_HeadApproveTranfer.Checked = false;
                }
                break;
        }
    }
    #endregion

    #region event radiobutton

    protected void radioCheckChange(object sender, EventArgs e)
    {
        var rdName = (RadioButtonList)sender;

        switch (rdName.ID)
        {
            case "rblTypeEquipment":
                divAlertSearchEquipmentOld.Visible = false;
                RadioButtonList rblTypeEquipment = (RadioButtonList)docCreate.FindControl("rblTypeEquipment");

                if (rblTypeEquipment.SelectedValue == "2")
                {
                    pnEquipmentOld.Visible = true;
                    tbsearchingToolID.Focus();
                    pnEquipmentNew.Visible = false;
                }
                else
                {
                    pnEquipmentOld.Visible = false;
                    gvListEquipmentCalOld.Visible = false;
                    divActionSaveCreateDocument.Visible = false;
                    setFormData(fvShowDetailsSearchTool, FormViewMode.ReadOnly, null);
                    pnEquipmentNew.Visible = true;
                    ClearDataCalEquipmentOld();
                    ClearDataCalibrationPoint();
                    ClearDataTextbox();

                    getM0EquipmentName((DropDownList)pnEquipmentNew.FindControl("ddl_machine_name"), "0");
                    getM0Brand((DropDownList)pnEquipmentNew.FindControl("ddl_machine_brand"), "0");
                    getM0EquipmentType((DropDownList)pnEquipmentNew.FindControl("ddlmachineType"), "0");
                    getM0Unit((DropDownList)pnEquipmentNew.FindControl("ddl_range_of_use_unit"), "0");
                    getM0Unit((DropDownList)pnEquipmentNew.FindControl("ddl_calibration_point_unit"), "0");
                    getM0Unit((DropDownList)pnEquipmentNew.FindControl("ddl_acceptance_criteria_unit"), "0");
                    getM0Unit((DropDownList)pnEquipmentNew.FindControl("ddl_measuring_range_unit"), "0");
                    getM0Unit((DropDownList)pnEquipmentNew.FindControl("ddl_resolution_unit"), "0");
                    getM0CalType((DropDownList)pnEquipmentNew.FindControl("ddlCalibrationType"), "0");
                }

                break;

            case "rblQAApproveCutoff":
                foreach (GridViewRow rowItems in gvEquipmentQACutoff.Rows)
                {
                    RadioButtonList chk = (RadioButtonList)rowItems.FindControl("rblQAApproveCutoff");

                    if (chk.SelectedValue == "1" && CheckAllQANotApproveCutoff.Checked)
                    {
                        CheckAllQANotApproveCutoff.Checked = false;
                    }
                    else if (chk.SelectedValue == "2" && CheckAllQAApproveCutoff.Checked)
                    {
                        CheckAllQAApproveCutoff.Checked = false;
                    }

                }
                break;

            case "rblQAApproveTranfer":
                foreach (GridViewRow rowItems in gvEquipmentQATranfer.Rows)
                {
                    RadioButtonList chk = (RadioButtonList)rowItems.FindControl("rblQAApproveTranfer");

                    if (chk.SelectedValue == "1" && CheckAllQANotApproveTranfer.Checked)
                    {
                        CheckAllQANotApproveTranfer.Checked = false;
                    }
                    else if (chk.SelectedValue == "2" && CheckAllQAApproveTranfer.Checked)
                    {
                        CheckAllQAApproveTranfer.Checked = false;
                    }

                }
                break;

            case "rbl_HeadApproveTranfer":
                foreach (GridViewRow rowItems_ in gvEquipmentAllTranfer.Rows)
                {
                    RadioButtonList chk_ = (RadioButtonList)rowItems_.FindControl("rbl_HeadApproveTranfer");

                    if (chk_.SelectedValue == "1" && CheckAll_HeadNotApproveTranfer.Checked)
                    {
                        CheckAll_HeadNotApproveTranfer.Checked = false;
                    }
                    else if (chk_.SelectedValue == "2" && CheckAll_HeadApproveTranfer.Checked)
                    {
                        CheckAll_HeadApproveTranfer.Checked = false;
                    }

                }
                break;

            case "rd_certificate":

                RadioButtonList rd_certificate = (RadioButtonList)fvInsertCIMS.FindControl("rd_certificate");
                Panel div_file_certificate = (Panel)fvInsertCIMS.FindControl("div_file_certificate");
                FileUpload File_certificate = (FileUpload)fvInsertCIMS.FindControl("File_certificate");

                if (rd_certificate.SelectedValue == "1")
                {
                    div_file_certificate.Visible = true;
                    File_certificate.Focus();
                }
                else
                {
                    div_file_certificate.Visible = false;
                    rd_certificate.Focus();
                }

                break;
        }
    }
    #endregion

    #region event dropdownlist

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }


    protected void getResolution(DropDownList ddlName, string _resolution)
    {
        data_qa_cims data_m0_resulution = new data_qa_cims();
        qa_cims_m0_resulution_detail m0_resulution = new qa_cims_m0_resulution_detail();
        data_m0_resulution.qa_cims_m0_resolution_list = new qa_cims_m0_resulution_detail[1];

        m0_resulution.condition = 1;
        data_m0_resulution.qa_cims_m0_resolution_list[0] = m0_resulution;

        data_m0_resulution = callServicePostMasterQACIMS(_urlCimsGetM0_Resolution, data_m0_resulution);

        if (data_m0_resulution.qa_cims_m0_resolution_list != null)
        {
            var _linqddl_resolution = (from dt in data_m0_resulution.qa_cims_m0_resolution_list

                                       where dt.unit_idx != 29 && dt.unit_idx != 30 && dt.unit_idx != 31

                                       select new
                                       {
                                           dt.unit_idx,
                                           dt.unit_symbol_en,
                                           dt.resolution_name,
                                           dt.equipment_resolution_idx
                                       }).ToList();


            var _linqddl_resolution_symbol = (from dt_synbol in data_m0_resulution.qa_cims_m0_resolution_list

                                              where dt_synbol.unit_idx == 29 || dt_synbol.unit_idx == 30 || dt_synbol.unit_idx == 31

                                              select new
                                              {
                                                  dt_synbol.unit_idx,
                                                  dt_synbol.unit_symbol_en,
                                                  dt_synbol.resolution_name,
                                                  dt_synbol.equipment_resolution_idx,

                                                  STATUS = dt_synbol.unit_idx == 29 ? "µl" : dt_synbol.unit_idx == 30 ? "µm" : dt_synbol.unit_idx == 31 ? "°C" : "-"
                                              }).ToList();



            ddlName.AppendDataBoundItems = true;
            ddlName.Items.Add(new ListItem("-- เลือกค่าอ่านละเอียด --", "0"));

            string ddl_resolution = "";
            string var_ddl_resolution = "";

            //litDebug.Text = _linqddl_resolution_symbol.Count().T;
            for (int idset = 0; idset < _linqddl_resolution_symbol.Count(); idset++)
            {

                ddl_resolution = _linqddl_resolution_symbol[idset].unit_idx.ToString();

                if (_linqddl_resolution_symbol[idset].unit_idx.ToString() == "29")
                {
                    var_ddl_resolution = "µl";
                }
                else if (_linqddl_resolution_symbol[idset].unit_idx.ToString() == "30")
                {
                    var_ddl_resolution = "µm";
                }
                else if (_linqddl_resolution_symbol[idset].unit_idx.ToString() == "31")
                {
                    var_ddl_resolution = "°C";
                }

                //var productQuery2 = _linqddl_resolution_symbol.Select(p1 => new { ProductId1 = p1.equipment_resolution_idx, DisplayText1 = p1.resolution_name.ToString() + " " + (var_ddl_resolution) });
                //ddlName.DataSource = productQuery2;
                //ddlName.DataValueField = "ProductId1";
                //ddlName.DataTextField = "DisplayText1";
                //ddlName.DataBind();


            }

            // litDebug.Text = _linqddl_resolution.ToString();

            if (_linqddl_resolution_symbol != null)
            {
                //var productQuery1 = _linqddl_resolution_symbol.Select(p => new { ProductId = p.equipment_resolution_idx, DisplayText = p.STATUS.ToString() + " " + (p.unit_symbol_en.ToString()) });
                var resolutionQuery1 = _linqddl_resolution_symbol.Select(p => new { Resolotion_idx = p.equipment_resolution_idx, Resolution_name = p.resolution_name + " " + p.STATUS.ToString() });
                ddlName.DataSource = resolutionQuery1;
                ddlName.DataValueField = "Resolotion_idx";
                ddlName.DataTextField = "Resolution_name";
                ddlName.DataBind();
                ddlName.SelectedValue = _resolution;
            }

            if (_linqddl_resolution != null)
            {
                var resolutionQuery2 = _linqddl_resolution.Select(p1 => new { Resolotion_idx = p1.equipment_resolution_idx, Resolution_name = p1.resolution_name.ToString() + " " + p1.unit_symbol_en });
                ddlName.DataSource = resolutionQuery2;
                ddlName.DataValueField = "Resolotion_idx";
                ddlName.DataTextField = "Resolution_name";
                ddlName.DataBind();
                ddlName.SelectedValue = _resolution;
            }



        }
        else
        {
            ddlName.Items.Insert(0, new ListItem("-- เลือกค่าอ่านละเอียด --", "0"));
        }


        //ddlName.Items.Insert(0, new ListItem("-- เลือกค่าอ่านละเอียด --", "0"));
        ////setDdlData(ddlName, _linqddl_resolution, "var_ddl_resolution", "equipment_resolution_idx");

        ////ddlName.Items.Insert(0, new ListItem("-- เลือกค่าอ่านละเอียด --", "0"));
        //ddlName.Items.Insert(1, new ListItem("µl", "29"));
        //ddlName.Items.Insert(2, new ListItem("µm", "30"));
        //ddlName.Items.Insert(3, new ListItem("°C", "31"));
        ////ddlName.SelectedValue = _resolution;


    }


    protected void getFrequency(DropDownList ddlName, string _frequency)
    {
        data_qa_cims data_m0_frequency = new data_qa_cims();
        qa_cims_m0_frequency_detail m0_frequency = new qa_cims_m0_frequency_detail();
        data_m0_frequency.qa_cims_m0_frequency_list = new qa_cims_m0_frequency_detail[1];

        m0_frequency.condition = 1;
        data_m0_frequency.qa_cims_m0_frequency_list[0] = m0_frequency;

        data_m0_frequency = callServicePostMasterQACIMS(_urlCimsGetM0_Frequency, data_m0_frequency);

        setDdlData(ddlName, data_m0_frequency.qa_cims_m0_frequency_list, "detail_frequency", "equipment_frequency_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือกความถี่ในการสอบเทียบ --", "0"));
        ddlName.SelectedValue = _frequency;
    }


    protected void getM0EquipmentName(DropDownList ddlName, string _equipment_idx)
    {
        data_qa_cims _dataqacims = new data_qa_cims();
        _dataqacims.qa_cims_m0_equipment_name_list = new qa_cims_m0_equipment_name_detail[1];
        qa_cims_m0_equipment_name_detail _m0Set = new qa_cims_m0_equipment_name_detail();
        _dataqacims.qa_cims_m0_equipment_name_list[0] = _m0Set;

        _dataqacims = callServicePostMasterQACIMS(_urlCimsGetEquipment_name, _dataqacims);

        setDdlData(ddlName, _dataqacims.qa_cims_m0_equipment_name_list, "equipment_name", "equipment_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือกชื่ออุปกรณ์/เครื่องมือ --", "0"));
        ddlName.SelectedValue = _equipment_idx;
    }

    protected void getM0Brand(DropDownList ddlName, string _brand_idx)
    {
        data_qa_cims _dataqacims = new data_qa_cims();
        _dataqacims.qa_cims_m0_brand_list = new qa_cims_m0_brand_detail[1];
        qa_cims_m0_brand_detail _m0Set = new qa_cims_m0_brand_detail();
        _dataqacims.qa_cims_m0_brand_list[0] = _m0Set;

        _dataqacims = callServicePostMasterQACIMS(_urlCimsGetBrand, _dataqacims);

        setDdlData(ddlName, _dataqacims.qa_cims_m0_brand_list, "brand_name", "brand_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือกยี่ห้อ/รุ่น --", "0"));
        ddlName.SelectedValue = _brand_idx;
    }

    protected void getM0EquipmentType(DropDownList ddlName, string _equipment_type_idx)
    {
        data_qa_cims _dataqacims = new data_qa_cims();
        _dataqacims.qa_cims_m0_equipment_type_list = new qa_cims_m0_equipment_type_detail[1];
        qa_cims_m0_equipment_type_detail _m0Set = new qa_cims_m0_equipment_type_detail();
        _dataqacims.qa_cims_m0_equipment_type_list[0] = _m0Set;

        _dataqacims = callServicePostMasterQACIMS(_urlCimsGetEquipment_type, _dataqacims);

        setDdlData(ddlName, _dataqacims.qa_cims_m0_equipment_type_list, "equipment_type_name", "equipment_type_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือกประเภทเครื่องมือ --", "0"));
        ddlName.SelectedValue = _equipment_type_idx;
    }

    protected void getM0Location(DropDownList ddlName, string _location_idx)
    {
        data_qa_cims _data_cims_location = new data_qa_cims();
        _data_cims_location.qa_cims_m0_location_list = new qa_cims_m0_location_detail[1];
        qa_cims_m0_location_detail _m0_location = new qa_cims_m0_location_detail();

        _m0_location.condition = 1;
        _data_cims_location.qa_cims_m0_location_list[0] = _m0_location;

        _data_cims_location = callServicePostMasterQACIMS(_urlCimsGetLocationName, _data_cims_location);

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_cims_location));

        setDdlData(ddlName, _data_cims_location.qa_cims_m0_location_list, "location_name", "m0_location_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือก Location --", "0"));
        ddlName.SelectedValue = _location_idx;
    }

    protected void getM0Unit(DropDownList ddlName, string _unit_idx)
    {
        data_qa_cims _dataqacims = new data_qa_cims();
        _dataqacims.qa_cims_m0_unit_list = new qa_cims_m0_unit_detail[1];
        qa_cims_m0_unit_detail _m0Set = new qa_cims_m0_unit_detail();
        _dataqacims.qa_cims_m0_unit_list[0] = _m0Set;

        _dataqacims = callServicePostMasterQACIMS(_urlCimsGetUnit, _dataqacims);

        var _linqddl_unit = (from dt in _dataqacims.qa_cims_m0_unit_list

                             select new
                             {
                                 dt.unit_symbol_en,
                                 dt.unit_idx
                             }).ToList();

        setDdlData(ddlName, _linqddl_unit, "unit_symbol_en", "unit_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือกหน่วยวัด --", "0"));
        ddlName.Items.Insert(1, new ListItem("µl", "29"));
        ddlName.Items.Insert(2, new ListItem("µm", "30"));
        ddlName.Items.Insert(3, new ListItem("°C", "31"));

        ddlName.SelectedValue = _unit_idx;
    }

    protected void getM0CalType(DropDownList ddlName, string _cal_type_idx)
    {
        data_qa_cims _dataqacims = new data_qa_cims();
        _dataqacims.qa_cims_m0_cal_type_list = new qa_cims_m0_cal_type_detail[1];
        qa_cims_m0_cal_type_detail _m0Set = new qa_cims_m0_cal_type_detail();
        _dataqacims.qa_cims_m0_cal_type_list[0] = _m0Set;

        _dataqacims = callServicePostMasterQACIMS(_urlCimsGetCal_type, _dataqacims);

        setDdlData(ddlName, _dataqacims.qa_cims_m0_cal_type_list, "cal_type_name", "cal_type_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือกประเภทการสอบเทียบ --", "0"));
        ddlName.SelectedValue = _cal_type_idx;
    }

    protected void getOrganizationList(DropDownList ddlName)
    {
        _dataEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dataEmployee.organization_list[0] = _orgList;

        _dataEmployee = callServicePostEmployee(_urlGetOrganizationList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือกองค์กร --", "0"));
    }

    protected void getDepartmentList(DropDownList ddlName, int _org_idx)
    {
        data_employee _dataEmployee_rdept = new data_employee();
        _dataEmployee_rdept.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dataEmployee_rdept.department_list[0] = _deptList;

        _dataEmployee_rdept = callServicePostEmployee(_urlGetDepartmentList, _dataEmployee_rdept);
        setDdlData(ddlName, _dataEmployee_rdept.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือกฝ่าย --", "0"));
    }

    protected void getSectionList(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {
        ddlName.Items.Clear();
        _dataEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _dataEmployee.section_list[0] = _secList;

        _dataEmployee = callServicePostEmployee(_urlGetSectionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.section_list, "sec_name_th", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือกแผนก --", "0"));
    }

    protected void getOrganizationPerList(DropDownList ddlName, int _org_idx)
    {
        _dataEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        //_orgList.org_idx = int.Parse(_org_idx);
        _dataEmployee.organization_list[0] = _orgList;

        _dataEmployee = callServicePostEmployee(_urlGetOrganizationList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือกองค์กร --", "0"));
        ddlName.SelectedValue = _org_idx.ToString();
    }

    protected void getDepartmentPerList(DropDownList ddlName, int _org_idx, int rdept_idx)
    {
        data_employee _dataEmployee_rdept = new data_employee();
        _dataEmployee_rdept.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dataEmployee_rdept.department_list[0] = _deptList;

        _dataEmployee_rdept = callServicePostEmployee(_urlGetDepartmentList, _dataEmployee_rdept);
        setDdlData(ddlName, _dataEmployee_rdept.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือกฝ่าย --", "0"));
        ddlName.SelectedValue = rdept_idx.ToString();
    }

    protected void getSectionPerList(DropDownList ddlName, int _org_idx, int _rdept_idx, int _rsec_idx)
    {
        ddlName.Items.Clear();
        _dataEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _dataEmployee.section_list[0] = _secList;

        _dataEmployee = callServicePostEmployee(_urlGetSectionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.section_list, "sec_name_th", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือกแผนก --", "0"));
        ddlName.SelectedValue = _rsec_idx.ToString();
    }

    protected void getPositionPerList(DropDownList ddlName, int _org_idx, int _rdept_idx, int _rsec_idx, int _rpos_idx)
    {
        ddlName.Items.Clear();
        _dataEmployee.position_list = new position_details[1];
        position_details _posList = new position_details();
        _posList.org_idx = _org_idx;
        _posList.rdept_idx = _rdept_idx;
        _posList.rsec_idx = _rsec_idx;
        _dataEmployee.position_list[0] = _posList;

        _dataEmployee = callServicePostEmployee(_urlGetPositionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.position_list, "pos_name_th", "rpos_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือกตำแหน่ง --", "0"));
        ddlName.SelectedValue = _rpos_idx.ToString();
    }

    protected void getPermissionManageList(DropDownList ddlName)
    {
        ddlName.Items.Clear();

        data_qa_cims _data_qa_permanage = new data_qa_cims();
        _data_qa_permanage.qa_cims_m0_per_management_list = new qa_cims_m0_per_management_detail[1];
        qa_cims_m0_per_management_detail _per_list = new qa_cims_m0_per_management_detail();


        _data_qa_permanage.qa_cims_m0_per_management_list[0] = _per_list;

        _data_qa_permanage = callServicePostMasterQACIMS(_urlCimsGetPerManageQA, _data_qa_permanage);
        setDdlData(ddlName, _data_qa_permanage.qa_cims_m0_per_management_list, "per_management_name", "m0_per_management_idx");

        ddlName.Items.Insert(0, new ListItem("-- เลือกสิทธิ์ --", "0"));
    }



    protected void getSectionListWhere(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {

        //litDebug.Text = ViewState["vsrsec_idx_from_tranfer"].ToString();

        HiddenField hfEmpRsecIDXTranfer = (HiddenField)fvDocDetailUser.FindControl("hfEmpRsecIDXTranfer");

        ddlName.Items.Clear();
        _dataEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _dataEmployee.section_list[0] = _secList;

        _dataEmployee = callServicePostEmployee(_urlGetSectionList, _dataEmployee);

        if (_dataEmployee.section_list != null)
        {
            var _linqlocationDefault = (from dt in _dataEmployee.section_list
                                        where dt.rsec_idx != int.Parse(ViewState["vsrsec_idx_from_tranfer"].ToString())
                                        select new
                                        {
                                            dt.sec_name_th,
                                            dt.rsec_idx
                                        }).ToList();
            setDdlData(ddlName, _linqlocationDefault, "sec_name_th", "rsec_idx");
        }


        ddlName.Items.Insert(0, new ListItem("-- เลือกแผนก --", "-1"));
    }

    protected void getChooseLabTypeCal(DropDownList ddlName, string _idxLabtype, string _defaultLocation)
    {

        data_qa_cims dataCimsDecisionLab = new data_qa_cims();
        dataCimsDecisionLab.qa_cims_bindnode_decision_list = new qa_cims_bindnode_decision_detail[1];
        qa_cims_bindnode_decision_detail _selectDecisionLab = new qa_cims_bindnode_decision_detail();
        _selectDecisionLab.condition = 1;
        dataCimsDecisionLab.qa_cims_bindnode_decision_list[0] = _selectDecisionLab;

        dataCimsDecisionLab = callServicePostQaDocCIMS(_urlCimsGetSelectDecisionNode1, dataCimsDecisionLab);

        qa_cims_m0_lab_detail[] _templist_default = (qa_cims_m0_lab_detail[])ViewState["vsCheckLabTypeDefault"];

        var _linqlocationDefault = (from dt in _templist_default
                                    where dt.place_idx == int.Parse(_defaultLocation)
                                    select new
                                    {
                                        dt.m0_lab_idx,
                                        dt.lab_name,
                                        dt.cal_type_idx
                                    }).ToList();

        string defaultLocation = "";

        for (int ex = 0; ex < _linqlocationDefault.Count(); ex++)
        {
            defaultLocation = _linqlocationDefault[ex].cal_type_idx.ToString();
        }

        if (defaultLocation == ExternalID.ToString())
        {
            setDdlData(ddlName, dataCimsDecisionLab.qa_cims_bindnode_decision_list, "decision_name", "decision_idx");
            ddlName.Items.Insert(0, new ListItem("- เลือกประเภทสอบเทียบ -", "0"));
            ddlName.SelectedValue = ExternalCondition.ToString();

        }
        else
        {
            var _linqlocationDefault_interanl = (from dt in dataCimsDecisionLab.qa_cims_bindnode_decision_list
                                                 where dt.decision_idx != (ExternalCondition)
                                                 select new
                                                 {
                                                     dt.decision_idx,
                                                     dt.decision_name
                                                 }).ToList();

            string defaultLocation_internal = "";

            for (int _internal = 0; _internal < _linqlocationDefault.Count(); _internal++)
            {
                defaultLocation_internal = _linqlocationDefault_interanl[_internal].decision_idx.ToString();
            }

            setDdlData(ddlName, dataCimsDecisionLab.qa_cims_bindnode_decision_list, "decision_name", "decision_idx");
            ddlName.Items.Insert(0, new ListItem("- เลือกประเภทสอบเทียบ -", "0"));
            ddlName.SelectedValue = defaultLocation_internal;

        }
    }

    protected void getLocationList(DropDownList ddlName, string _idxplace, string _defaultLocation)
    {
        data_qa_cims dataCimsDecisionLabPlace = new data_qa_cims();
        dataCimsDecisionLabPlace.qa_cims_m0_lab_list = new qa_cims_m0_lab_detail[1];
        qa_cims_m0_lab_detail _selectDecisionPlace = new qa_cims_m0_lab_detail();
        _selectDecisionPlace.condition = 1;
        dataCimsDecisionLabPlace.qa_cims_m0_lab_list[0] = _selectDecisionPlace;

        dataCimsDecisionLabPlace = callServicePostMasterQACIMS(_urlCimsGetLab, dataCimsDecisionLabPlace);

        ViewState["vsCheckLabTypeDefault"] = dataCimsDecisionLabPlace.qa_cims_m0_lab_list;
        var _linqlocationDefault = (from dt in dataCimsDecisionLabPlace.qa_cims_m0_lab_list
                                    where dt.place_idx == int.Parse(_defaultLocation)
                                    select new
                                    {
                                        dt.m0_lab_idx,
                                        dt.lab_name,
                                        dt.cal_type_idx
                                    }).ToList();

        string defaultLocation = "";
        string defaultLabtype = "";

        for (int ex = 0; ex < _linqlocationDefault.Count(); ex++)
        {
            defaultLocation = _linqlocationDefault[ex].m0_lab_idx.ToString();
            defaultLabtype = _linqlocationDefault[ex].cal_type_idx.ToString();
            ViewState["vsDefaultLocation"] = defaultLabtype;

            //litDebug.Text = _linqlocationDefault[ex].m0_lab_idx.ToString();
        }

        if (_defaultLocation != "0")
        {
            setDdlData(ddlName, dataCimsDecisionLabPlace.qa_cims_m0_lab_list, "lab_name", "m0_lab_idx");
            ddlName.Items.Insert(0, new ListItem("- เลือกสถานที่สอบเทียบ -", "0"));
            ddlName.SelectedValue = defaultLocation;

            if (defaultLabtype == ExternalID.ToString())
            {
                ddlName.Enabled = false;
            }
            else
            {

                if (_idxplace == "0")
                {

                    var linqInternal = (from dt in dataCimsDecisionLabPlace.qa_cims_m0_lab_list
                                        where dt.cal_type_idx != ExternalID
                                        select new
                                        {
                                            dt.m0_lab_idx,
                                            dt.lab_name
                                        }).ToList();


                    string extanalidx1 = "";
                    string placeDefault = "";
                    for (int ex = 0; ex < linqInternal.Count(); ex++)
                    {
                        placeDefault = linqInternal[ex].m0_lab_idx.ToString();
                        extanalidx1 = linqInternal[ex].m0_lab_idx.ToString();
                        setDdlData(ddlName, dataCimsDecisionLabPlace.qa_cims_m0_lab_list, "lab_name", "m0_lab_idx");
                        ddlName.Items.Insert(0, new ListItem("- เลือกสถานที่สอบเทียบ -", "0"));
                        ddlName.SelectedValue = defaultLocation;// "3";//placeDefault;

                    }
                }
                else
                {

                }
            }
        }
        else
        {
            if (_idxplace == ExternalCondition.ToString())
            {
                var linqExtanal = (from dt in dataCimsDecisionLabPlace.qa_cims_m0_lab_list
                                   where dt.cal_type_idx == ExternalID
                                   select new
                                   {
                                       dt.m0_lab_idx,
                                       dt.lab_name
                                   }).ToList();

                string extanalidx = "";

                for (int ex = 0; ex < linqExtanal.Count(); ex++)
                {
                    extanalidx = linqExtanal[ex].m0_lab_idx.ToString();

                    setDdlData(ddlName, dataCimsDecisionLabPlace.qa_cims_m0_lab_list, "lab_name", "m0_lab_idx");
                    ddlName.Items.Insert(0, new ListItem("- เลือกสถานที่สอบเทียบ -", "0"));
                    ddlName.SelectedValue = extanalidx;
                    ddlName.Enabled = false;
                }
            }
            else
            {
                setDdlData(ddlName, dataCimsDecisionLabPlace.qa_cims_m0_lab_list, "lab_name", "m0_lab_idx");
                ddlName.Items.Insert(0, new ListItem("- เลือกสถานที่สอบเทียบ -", "0"));
                ddlName.Enabled = true;

            }
        }
    }

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlName = (DropDownList)sender;

        DropDownList ddlOrg_excel = (DropDownList)fvInsertCIMS.FindControl("ddlOrg_excel");
        DropDownList ddlDept_excel = (DropDownList)fvInsertCIMS.FindControl("ddlDept_excel");
        DropDownList ddlSec_excel = (DropDownList)fvInsertCIMS.FindControl("ddlSec_excel");

        DropDownList dllcaltype_search = (DropDownList)docRegistration.FindControl("dllcaltype_search");
        DropDownList ddlPlaceMachine_search = (DropDownList)docRegistration.FindControl("ddlPlaceMachine_search");

        DropDownList dllcaltype_searchDoc = (DropDownList)docRegistration.FindControl("dllcaltype_searchDoc");
        DropDownList ddlPlaceMachine_searchDoc = (DropDownList)docRegistration.FindControl("ddlPlaceMachine_searchDoc");

        DropDownList ddl_location = (DropDownList)fvInsertCIMS.FindControl("ddl_location");

        // Report Devices
        DropDownList ddl_orgidx_report = (DropDownList)docReport.FindControl("ddl_orgidx_report");
        DropDownList ddl_rdeptidx_report = (DropDownList)docReport.FindControl("ddl_rdeptidx_report");
        DropDownList ddl_rsecidx_report = (DropDownList)docReport.FindControl("ddl_rsecidx_report");

        DropDownList ddl_orgidx_report_offline = (DropDownList)docReport.FindControl("ddl_orgidx_report_offline");
        DropDownList ddl_rdeptidx_report_offline = (DropDownList)docReport.FindControl("ddl_rdeptidx_report_offline");
        DropDownList ddl_rsecidx_report_offline = (DropDownList)docReport.FindControl("ddl_rsecidx_report_offline");

        DropDownList ddl_orgidx_report_cutoff = (DropDownList)docReport.FindControl("ddl_orgidx_report_cutoff");
        DropDownList ddl_rdeptidx_report_cutoff = (DropDownList)docReport.FindControl("ddl_rdeptidx_report_cutoff");
        DropDownList ddl_rsecidx_report_cutoff = (DropDownList)docReport.FindControl("ddl_rsecidx_report_cutoff");

        DropDownList ddlorg_idx = (DropDownList)fvInsertPerQA.FindControl("ddlorg_idx");
        DropDownList ddlrdept_idx = (DropDownList)fvInsertPerQA.FindControl("ddlrdept_idx");
        DropDownList ddlrsec_idx = (DropDownList)fvInsertPerQA.FindControl("ddlrsec_idx");
        DropDownList ddlrpos_idx = (DropDownList)fvInsertPerQA.FindControl("ddlrpos_idx");

        DropDownList ddlorg_idxupdate = (DropDownList)fvInsertPerQA.FindControl("ddlorg_idxupdate");
        DropDownList ddlrdept_idxupdate = (DropDownList)fvInsertPerQA.FindControl("ddlrdept_idxupdate");
        DropDownList ddlrsec_idxupdate = (DropDownList)fvInsertPerQA.FindControl("ddlrsec_idxupdate");
        DropDownList ddlrpos_idxupdate = (DropDownList)fvInsertPerQA.FindControl("ddlrpos_idxupdate");

        switch (ddlName.ID)
        {
            case "ddlOrgtranfer":
                getDepartmentList(ddlRdepttranfer, int.Parse(ddlOrgtranfer.SelectedItem.Value));
                ddlRsectranfer.Items.Clear();
                ddlRsectranfer.Items.Insert(0, new ListItem("-- เลือกแผนก --", "-1"));
                break;

            case "ddlRdepttranfer":
                getSectionListWhere(ddlRsectranfer, int.Parse(ddlOrgtranfer.SelectedItem.Value), int.Parse(ddlRdepttranfer.SelectedItem.Value));
                break;

            case "ddlOrg_excel":
                // ddlDocType.Focus();
                getDepartmentList(ddlDept_excel, int.Parse(ddlOrg_excel.SelectedItem.Value));
                ddlSec_excel.Items.Clear();
                ddlSec_excel.Items.Insert(0, new ListItem("-- เลือกแผนก --", "-1"));
                // ddlSec_create.Items.Clear();
                break;
            case "ddlDept_excel":
                //ddlDocType.Focus();
                getSectionList(ddlSec_excel, int.Parse(ddlOrg_excel.SelectedItem.Value), int.Parse(ddlDept_excel.SelectedItem.Value));

                break;

            case "ddl_orgidx_report":

                getDepartmentList(ddl_rdeptidx_report, int.Parse(ddl_orgidx_report.SelectedItem.Value));
                ddl_rsecidx_report.Items.Clear();
                ddl_rsecidx_report.Items.Insert(0, new ListItem("-- เลือกแผนก --", "0"));

                break;
            case "ddl_rdeptidx_report":
                //ddlDocType.Focus();
                getSectionList(ddl_rsecidx_report, int.Parse(ddl_orgidx_report.SelectedItem.Value), int.Parse(ddl_rdeptidx_report.SelectedItem.Value));
                break;

            case "ddlrsec_idx":
                //ddlDocType.Focus();
                getPositionPerList(ddlrpos_idx, int.Parse(ddlorg_idx.SelectedItem.Value), int.Parse(ddlrdept_idx.SelectedItem.Value), int.Parse(ddlrsec_idx.SelectedItem.Value), 0);
                break;

            case "ddlrsec_idxupdate":
                //ddlDocType.Focus();
                getPositionPerList(ddlrpos_idxupdate, int.Parse(ddlorg_idxupdate.SelectedItem.Value), int.Parse(ddlrdept_idxupdate.SelectedItem.Value), int.Parse(ddlrsec_idxupdate.SelectedItem.Value), 0);
                break;

            case "ddl_orgidx_report_offline":

                getDepartmentList(ddl_rdeptidx_report_offline, int.Parse(ddl_orgidx_report_offline.SelectedItem.Value));
                ddl_rsecidx_report_offline.Items.Clear();
                ddl_rsecidx_report_offline.Items.Insert(0, new ListItem("-- เลือกแผนก --", "0"));

                break;
            case "ddl_rdeptidx_report_offline":
                //ddlDocType.Focus();
                getSectionList(ddl_rsecidx_report_offline, int.Parse(ddl_orgidx_report_offline.SelectedItem.Value), int.Parse(ddl_rdeptidx_report_offline.SelectedItem.Value));
                break;

            case "ddl_orgidx_report_cutoff":

                getDepartmentList(ddl_rdeptidx_report_cutoff, int.Parse(ddl_orgidx_report_cutoff.SelectedItem.Value));
                ddl_rsecidx_report_cutoff.Items.Clear();
                ddl_rsecidx_report_cutoff.Items.Insert(0, new ListItem("-- เลือกแผนก --", "0"));

                break;
            case "ddl_rdeptidx_report_cutoff":
                //ddlDocType.Focus();
                getSectionList(ddl_rsecidx_report_cutoff, int.Parse(ddl_orgidx_report_cutoff.SelectedItem.Value), int.Parse(ddl_rdeptidx_report_cutoff.SelectedItem.Value));
                break;

            case "ddl_location":

                DropDownList ddl_Zone = (DropDownList)fvInsertCIMS.FindControl("ddl_Zone");
                getZoneList(ddl_Zone, ddl_location.SelectedItem.Value);
                //ddl_Zone.Items.Clear();
                //ddl_Zone.Items.Insert(0, new ListItem("-- เลือก Zone --", "-1"));

                break;

            case "ddlChooseLabType":

                var ddlID = (DropDownList)sender;
                var rowGrid = (GridViewRow)ddlID.NamingContainer;

                DropDownList _selectLabType = (DropDownList)rowGrid.FindControl("ddlChooseLabType");
                DropDownList _selectLocation = (DropDownList)rowGrid.FindControl("ddlChooseLocation");
                TextBox _commentBeforeExtanalLab = (TextBox)rowGrid.FindControl("txtCommentBeforeExtanalLab");
                Panel pnlAction = (Panel)fvCalDocumentList.FindControl("pnlAction");

                TextBox tbplaceidx = (TextBox)fvDetailsDocument.FindControl("tbplaceidx");

                ViewState["vs_labtype"] = _selectLabType.SelectedValue.ToString();

                //litDebug.Text = ViewState["vs_labtype"].ToString();

                if (int.Parse(_selectLabType.SelectedValue) == ExternalCondition)
                {
                    _commentBeforeExtanalLab.Visible = true;
                    _selectLocation.Visible = true;
                    getLocationList((DropDownList)rowGrid.FindControl("ddlChooseLocation"), ViewState["vs_labtype"].ToString(), "0");
                }
                else
                {
                    _selectLocation.Visible = true;
                    _commentBeforeExtanalLab.Visible = false;
                    getLocationList((DropDownList)rowGrid.FindControl("ddlChooseLocation"), ViewState["vs_labtype"].ToString(), tbplaceidx.Text);

                }
                pnlAction.Visible = true;

                break;
            case "ddOrg_seachregis":
                seachregisDepartmentList(ddDept_seachregis, int.Parse(ddOrg_seachregis.SelectedItem.Value));
                ddSec_seachregis.Items.Clear();
                ddSec_seachregis.Items.Insert(0, new ListItem("-- เลือกแผนก --", "-1"));
                break;

            case "ddDept_seachregis":
                seachregisSectionList(ddSec_seachregis, int.Parse(ddOrg_seachregis.SelectedItem.Value), int.Parse(ddDept_seachregis.SelectedItem.Value));
                break;

            case "dllcaltype_search":

                //check place
                if (int.Parse(dllcaltype_search.SelectedValue) != 0)
                {
                    tbLabNameInDocument.Text = dllcaltype_search.SelectedItem.ToString();
                    tbLabIDXInDocument.Text = dllcaltype_search.SelectedValue.ToString();
                }
                else
                {
                    tbLabIDXInDocument.Text = "";
                    tbLabNameInDocument.Text = dllcaltype_search.SelectedValue.ToString();
                }

                //
                if (int.Parse(dllcaltype_search.SelectedValue) != 0 && int.Parse(ddlPlaceMachine_search.SelectedValue) != 0)
                {


                    data_qa_cims data_SearchDetail = new data_qa_cims();
                    data_SearchDetail.qa_cims_search_registration_device_list = new qa_cims_search_registration_device[1];
                    qa_cims_search_registration_device m0_SearchRegistration1 = new qa_cims_search_registration_device();

                    m0_SearchRegistration1.condition = 2;
                    m0_SearchRegistration1.cal_type_idx = int.Parse(dllcaltype_search.SelectedValue);
                    m0_SearchRegistration1.place_idx = int.Parse(ddlPlaceMachine_search.SelectedValue);
                    m0_SearchRegistration1.rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
                    m0_SearchRegistration1.rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());

                    data_SearchDetail.qa_cims_search_registration_device_list[0] = m0_SearchRegistration1;

                    data_SearchDetail = callServicePostMasterQACIMS(_urlCimsSearchRegistrationDevice, data_SearchDetail);

                    if (data_SearchDetail.return_code == 0)
                    {
                        div_SearchIndex_1.Visible = true;
                        fv_SearchIndex.Visible = true;
                        show_gvregistration.Visible = true;
                        ViewState["vsRegistrationDevice"] = data_SearchDetail.qa_cims_search_registration_device_list;
                        setGridData(gvRegistrationList, ViewState["vsRegistrationDevice"]);

                    }
                    else
                    {

                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่พบข้อมูล!!!');", true);
                        fv_SearchIndex.Visible = false;
                        show_gvregistration.Visible = true;
                        ViewState["vsRegistrationDevice"] = null;//data_SearchDetail.qa_cims_search_registration_device_list;
                        setGridData(gvRegistrationList, ViewState["vsRegistrationDevice"]);

                    }

                }
                else
                {
                    show_gvregistration.Visible = false;
                    fv_SearchIndex.Visible = false;

                    ViewState["vsRegistrationDevice"] = null;//data_SearchDetail.qa_cims_search_registration_device_list;
                    setGridData(gvRegistrationList, ViewState["vsRegistrationDevice"]);


                }


                break;
            case "ddlPlaceMachine_search":

                //check place machine
                if (int.Parse(ddlPlaceMachine_search.SelectedValue) != 0)
                {
                    tbPlaceNameInDocument.Text = ddlPlaceMachine_search.SelectedItem.ToString();
                    tbPlaceIDXInDocument.Text = ddlPlaceMachine_search.SelectedValue.ToString();
                }
                else
                {
                    tbPlaceNameInDocument.Text = "";
                    tbPlaceIDXInDocument.Text = ddlPlaceMachine_search.SelectedValue.ToString();
                }

                //
                if (int.Parse(dllcaltype_search.SelectedValue) != 0 && int.Parse(ddlPlaceMachine_search.SelectedValue) != 0)
                {
                    fv_SearchIndex.Visible = true;

                    data_qa_cims data_SearchDetail = new data_qa_cims();
                    data_SearchDetail.qa_cims_search_registration_device_list = new qa_cims_search_registration_device[1];
                    qa_cims_search_registration_device m0_SearchRegistration1 = new qa_cims_search_registration_device();

                    m0_SearchRegistration1.condition = 2;
                    m0_SearchRegistration1.cal_type_idx = int.Parse(dllcaltype_search.SelectedValue);
                    m0_SearchRegistration1.place_idx = int.Parse(ddlPlaceMachine_search.SelectedValue);
                    m0_SearchRegistration1.rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
                    m0_SearchRegistration1.rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());

                    data_SearchDetail.qa_cims_search_registration_device_list[0] = m0_SearchRegistration1;

                    data_SearchDetail = callServicePostMasterQACIMS(_urlCimsSearchRegistrationDevice, data_SearchDetail);

                    if (data_SearchDetail.return_code == 0)
                    {
                        fv_SearchIndex.Visible = true;
                        show_gvregistration.Visible = true;
                        ViewState["vsRegistrationDevice"] = data_SearchDetail.qa_cims_search_registration_device_list;
                        setGridData(gvRegistrationList, ViewState["vsRegistrationDevice"]);

                    }
                    else
                    {

                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่พบข้อมูล!!!');", true);
                        fv_SearchIndex.Visible = false;
                        show_gvregistration.Visible = true;
                        ViewState["vsRegistrationDevice"] = null;//data_SearchDetail.qa_cims_search_registration_device_list;
                        setGridData(gvRegistrationList, ViewState["vsRegistrationDevice"]);

                    }
                }
                else
                {
                    show_gvregistration.Visible = false;
                    fv_SearchIndex.Visible = false;

                    ViewState["vsRegistrationDevice"] = null;//data_SearchDetail.qa_cims_search_registration_device_list;
                    setGridData(gvRegistrationList, ViewState["vsRegistrationDevice"]);
                }


                break;
            case "dllcaltype_searchDoc": // Document Tn Regis Devices

                //check place
                if (int.Parse(dllcaltype_searchDoc.SelectedValue) != 0)
                {
                    tbLabNameInDocument.Text = dllcaltype_searchDoc.SelectedItem.ToString();
                    tbLabIDXInDocument.Text = dllcaltype_searchDoc.SelectedValue.ToString();
                }
                else
                {
                    tbLabNameInDocument.Text = "";
                    tbLabIDXInDocument.Text = dllcaltype_searchDoc.SelectedValue.ToString();
                }

                //
                if (int.Parse(dllcaltype_searchDoc.SelectedValue) != 0 && int.Parse(ddlPlaceMachine_searchDoc.SelectedValue) != 0)
                {
                    divSearchDocument.Visible = true;
                    divdocumentList.Visible = true;
                    div_SearchDoc.Visible = true;

                    data_qa_cims _dataSelectPlaceDoc_backpage = new data_qa_cims();

                    if (ViewState["rdept_permission"].ToString() == "27")
                    {

                        //litDebug.Text = "9999";
                        //data_qa_cims _dataSelectPlaceDoc_backpage = new data_qa_cims();
                        _dataSelectPlaceDoc_backpage.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
                        qa_cims_u1_document_device_detail _select_place_doc = new qa_cims_u1_document_device_detail();
                        _select_place_doc.place_idx = int.Parse(ddlPlaceMachine_searchDoc.SelectedValue);
                        _select_place_doc.cal_type_idx = int.Parse(dllcaltype_searchDoc.SelectedValue);
                        _select_place_doc.condition = 6;
                        _dataSelectPlaceDoc_backpage.qa_cims_u1_document_device_list[0] = _select_place_doc;
                        _dataSelectPlaceDoc_backpage = callServicePostQaDocCIMS(_urlCimsGetEquipmentList, _dataSelectPlaceDoc_backpage);
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataSelectPlaceDoc_backpage));


                        if (_dataSelectPlaceDoc_backpage.return_code == 0)
                        {
                            gvDocumentList.PageIndex = 0;
                            divSearchDocument.Visible = true;
                            ViewState["vsDocument"] = _dataSelectPlaceDoc_backpage.qa_cims_u0_document_device_list;
                            setGridData(gvDocumentList, ViewState["vsDocument"]);
                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่พบข้อมูล!!!');", true);
                            divSearchDocument.Visible = false;

                            divdocumentList.Visible = true;
                            ViewState["vsDocument"] = null;//_dataSelectPlaceDoc_backpage.qa_cims_u0_document_device_list;
                            setGridData(gvDocumentList, ViewState["vsDocument"]);

                        }

                    }
                    else
                    {
                        // litDebug.Text = "9988";
                        //data_qa_cims _dataSelectPlaceDoc_backpage = new data_qa_cims();
                        _dataSelectPlaceDoc_backpage.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
                        qa_cims_u1_document_device_detail _select_place_doc = new qa_cims_u1_document_device_detail();
                        _select_place_doc.place_idx = int.Parse(ddlPlaceMachine_searchDoc.SelectedValue);
                        _select_place_doc.cal_type_idx = int.Parse(dllcaltype_searchDoc.SelectedValue);
                        _select_place_doc.condition = 8;
                        _select_place_doc.rsec_idx_create = int.Parse(ViewState["rsec_permission"].ToString());
                        _dataSelectPlaceDoc_backpage.qa_cims_u1_document_device_list[0] = _select_place_doc;
                        _dataSelectPlaceDoc_backpage = callServicePostQaDocCIMS(_urlCimsGetEquipmentList, _dataSelectPlaceDoc_backpage);

                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataSelectPlaceDoc_backpage));
                        if (_dataSelectPlaceDoc_backpage.return_code == 0)
                        {

                            gvDocumentList.PageIndex = 0;
                            divSearchDocument.Visible = true;
                            ViewState["vsDocument"] = _dataSelectPlaceDoc_backpage.qa_cims_u0_document_device_list;
                            setGridData(gvDocumentList, ViewState["vsDocument"]);
                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่พบข้อมูล!!!');", true);
                            divSearchDocument.Visible = false;

                            divdocumentList.Visible = true;
                            ViewState["vsDocument"] = null;//_dataSelectPlaceDoc_backpage.qa_cims_u0_document_device_list;
                            setGridData(gvDocumentList, ViewState["vsDocument"]);

                        }

                    }

                }
                else
                {
                    divdocumentList.Visible = false;
                    divSearchDocument.Visible = false;

                }
                break;
            case "ddlPlaceMachine_searchDoc":

                if (int.Parse(ddlPlaceMachine_searchDoc.SelectedValue) != 0)
                {
                    tbPlaceNameInDocument.Text = ddlPlaceMachine_searchDoc.SelectedItem.ToString();
                    tbPlaceIDXInDocument.Text = ddlPlaceMachine_searchDoc.SelectedValue.ToString();
                }
                else
                {
                    tbPlaceNameInDocument.Text = "";
                    tbPlaceIDXInDocument.Text = ddlPlaceMachine_searchDoc.SelectedValue.ToString();
                }

                //
                if (int.Parse(dllcaltype_searchDoc.SelectedValue) != 0 && int.Parse(ddlPlaceMachine_searchDoc.SelectedValue) != 0)
                {
                    divSearchDocument.Visible = true;
                    divdocumentList.Visible = true;
                    div_SearchDoc.Visible = true;

                    data_qa_cims _dataSelectPlaceDoc_backpage = new data_qa_cims();

                    if (ViewState["rdept_permission"].ToString() == "27")
                    {

                        //litDebug.Text = "9999";
                        //data_qa_cims _dataSelectPlaceDoc_backpage = new data_qa_cims();
                        _dataSelectPlaceDoc_backpage.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
                        qa_cims_u1_document_device_detail _select_place_doc = new qa_cims_u1_document_device_detail();
                        _select_place_doc.place_idx = int.Parse(ddlPlaceMachine_searchDoc.SelectedValue);
                        _select_place_doc.cal_type_idx = int.Parse(dllcaltype_searchDoc.SelectedValue);
                        _select_place_doc.condition = 6;
                        _dataSelectPlaceDoc_backpage.qa_cims_u1_document_device_list[0] = _select_place_doc;
                        _dataSelectPlaceDoc_backpage = callServicePostQaDocCIMS(_urlCimsGetEquipmentList, _dataSelectPlaceDoc_backpage);

                        if (_dataSelectPlaceDoc_backpage.return_code == 0)
                        {
                            gvDocumentList.PageIndex = 0;
                            ViewState["vsDocument"] = _dataSelectPlaceDoc_backpage.qa_cims_u0_document_device_list;
                            setGridData(gvDocumentList, ViewState["vsDocument"]);
                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่พบข้อมูล!!!');", true);
                            divSearchDocument.Visible = false;
                            divdocumentList.Visible = true;

                            ViewState["vsDocument"] = null;//_dataSelectPlaceDoc_backpage.qa_cims_u0_document_device_list;
                            setGridData(gvDocumentList, ViewState["vsDocument"]);
                        }


                    }
                    else
                    {
                        //litDebug.Text = "9988";
                        //data_qa_cims _dataSelectPlaceDoc_backpage = new data_qa_cims();
                        _dataSelectPlaceDoc_backpage.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
                        qa_cims_u1_document_device_detail _select_place_doc = new qa_cims_u1_document_device_detail();
                        _select_place_doc.place_idx = int.Parse(ddlPlaceMachine_searchDoc.SelectedValue);
                        _select_place_doc.cal_type_idx = int.Parse(dllcaltype_searchDoc.SelectedValue);
                        _select_place_doc.condition = 8;
                        _select_place_doc.rsec_idx_create = int.Parse(ViewState["rsec_permission"].ToString());
                        _dataSelectPlaceDoc_backpage.qa_cims_u1_document_device_list[0] = _select_place_doc;

                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataSelectPlaceDoc_backpage));

                        _dataSelectPlaceDoc_backpage = callServicePostQaDocCIMS(_urlCimsGetEquipmentList, _dataSelectPlaceDoc_backpage);
                        

                        if (_dataSelectPlaceDoc_backpage.return_code == 0)
                        {
                            gvDocumentList.PageIndex = 0;
                            ViewState["vsDocument"] = _dataSelectPlaceDoc_backpage.qa_cims_u0_document_device_list;
                            setGridData(gvDocumentList, ViewState["vsDocument"]);
                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่พบข้อมูล!!!');", true);
                            divSearchDocument.Visible = false;
                            divdocumentList.Visible = true;

                            ViewState["vsDocument"] = null;//_dataSelectPlaceDoc_backpage.qa_cims_u0_document_device_list;
                            setGridData(gvDocumentList, ViewState["vsDocument"]);
                        }
                    }

                }
                else
                {
                    divdocumentList.Visible = false;
                    divSearchDocument.Visible = false;

                }

                break;


        }
    }

    protected void getEquipmentNameEdit(DropDownList ddlName, string _GetEquipmentIdx)
    {
        data_qa_cims _data_qa_Edit = new data_qa_cims();
        _data_qa_Edit.qa_cims_m0_equipment_name_list = new qa_cims_m0_equipment_name_detail[1];
        qa_cims_m0_equipment_name_detail _equipmentnameList = new qa_cims_m0_equipment_name_detail();

        _data_qa_Edit.qa_cims_m0_equipment_name_list[0] = _equipmentnameList;

        _data_qa_Edit = callServicePostMasterQACIMS(_urlCimsGetEquipment_name, _data_qa_Edit);

        setDdlData(ddlName, _data_qa_Edit.qa_cims_m0_equipment_name_list, "equipment_name", "equipment_idx");
        ddlName.Items.Insert(0, new ListItem("-- กรุณาเลือกอุปกรณ์ --", "-1"));
        ddlName.SelectedValue = _GetEquipmentIdx;

    }

    protected void getBrandNameEdit(DropDownList ddlName, string _GetBrandIdx)
    {
        data_qa_cims _data_qa_Edit = new data_qa_cims();
        _data_qa_Edit.qa_cims_m0_brand_list = new qa_cims_m0_brand_detail[1];
        qa_cims_m0_brand_detail _brandList = new qa_cims_m0_brand_detail();

        _data_qa_Edit.qa_cims_m0_brand_list[0] = _brandList;

        _data_qa_Edit = callServicePostMasterQACIMS(_urlCimsGetBrand, _data_qa_Edit);

        setDdlData(ddlName, _data_qa_Edit.qa_cims_m0_brand_list, "brand_name", "brand_idx");
        ddlName.Items.Insert(0, new ListItem("-- กรุณาเลือกยี่ห้อ/รุ่น --", "-1"));
        ddlName.SelectedValue = _GetBrandIdx;

    }

    protected void getEquipmentTypeEdit(DropDownList ddlName, string _GetEquipmentTypeIdx)
    {
        data_qa_cims _data_qa_Edit = new data_qa_cims();
        _data_qa_Edit.qa_cims_m0_equipment_type_list = new qa_cims_m0_equipment_type_detail[1];
        qa_cims_m0_equipment_type_detail _equipment_typeList = new qa_cims_m0_equipment_type_detail();

        _data_qa_Edit.qa_cims_m0_equipment_type_list[0] = _equipment_typeList;

        _data_qa_Edit = callServicePostMasterQACIMS(_urlCimsGetEquipment_type, _data_qa_Edit);

        setDdlData(ddlName, _data_qa_Edit.qa_cims_m0_equipment_type_list, "equipment_type_name", "equipment_type_idx");
        ddlName.Items.Insert(0, new ListItem("-- กรุณาเลือกประเภทเครื่องมือ --", "-1"));
        ddlName.SelectedValue = _GetEquipmentTypeIdx;

    }

    protected void getUnitEdit(DropDownList ddlName, string _GetUnitIdx)
    {
        data_qa_cims _data_qa_Edit = new data_qa_cims();
        _data_qa_Edit.qa_cims_m0_unit_list = new qa_cims_m0_unit_detail[1];
        qa_cims_m0_unit_detail _unitList = new qa_cims_m0_unit_detail();

        _data_qa_Edit.qa_cims_m0_unit_list[0] = _unitList;

        _data_qa_Edit = callServicePostMasterQACIMS(_urlCimsGetUnit, _data_qa_Edit);

        setDdlData(ddlName, _data_qa_Edit.qa_cims_m0_unit_list, "unit_symbol_en", "unit_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือกหน่วยวัด --", "0"));
        ddlName.Items.Insert(1, new ListItem("µl", "29"));
        ddlName.Items.Insert(2, new ListItem("µm", "30"));
        ddlName.Items.Insert(3, new ListItem("°C", "31"));
        ddlName.SelectedValue = _GetUnitIdx;

    }

    protected void getCalTypeEdit(DropDownList ddlName, string _GetCalTypeIdx)
    {
        data_qa_cims _data_qa_Edit = new data_qa_cims();
        _data_qa_Edit.qa_cims_m0_cal_type_list = new qa_cims_m0_cal_type_detail[1];
        qa_cims_m0_cal_type_detail _caltypeList = new qa_cims_m0_cal_type_detail();

        _data_qa_Edit.qa_cims_m0_cal_type_list[0] = _caltypeList;

        _data_qa_Edit = callServicePostMasterQACIMS(_urlCimsGetCal_type, _data_qa_Edit);

        setDdlData(ddlName, _data_qa_Edit.qa_cims_m0_cal_type_list, "cal_type_name", "cal_type_idx");
        ddlName.Items.Insert(0, new ListItem("-- กรุณาเลือกประเภทการสอบเทียบ --", "-1"));
        ddlName.SelectedValue = _GetCalTypeIdx;

    }

    protected void getOrgEdit(DropDownList ddlName, string _GetOtgIdx)
    {
        //data_qa_cims _data_qa_Edit = new data_qa_cims();
        _dataEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();

        _dataEmployee.organization_list[0] = _orgList;

        _dataEmployee = callServicePostEmployee(_urlGetOrganizationList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือกองค์กร --", "-1"));
        ddlName.SelectedValue = _GetOtgIdx;

    }

    protected void getDeptEdit(DropDownList ddlName, string _GetOrgIdx, string _GetDeptIdx)
    {
        //data_qa_cims _data_qa_Edit = new data_qa_cims();
        data_employee _dataEmployee_rdept = new data_employee();
        _dataEmployee_rdept.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = int.Parse(_GetOrgIdx);

        _dataEmployee_rdept.department_list[0] = _deptList;

        _dataEmployee_rdept = callServicePostEmployee(_urlGetDepartmentList, _dataEmployee_rdept);
        setDdlData(ddlName, _dataEmployee_rdept.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือกฝ่าย --", "-1"));
        ddlName.SelectedValue = _GetDeptIdx;

    }

    protected void getSecEdit(DropDownList ddlName, string _GetOrgIdx, string _GetDeptIdx, string _GetSecIdx)
    {
        _dataEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = int.Parse(_GetOrgIdx);
        _secList.rdept_idx = int.Parse(_GetDeptIdx);
        _dataEmployee.section_list[0] = _secList;

        _dataEmployee = callServicePostEmployee(_urlGetSectionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.section_list, "sec_name_th", "rsec_idx");
        //setDdlData(ddlName, _dataEmployee_rdept.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือกแผนก --", "-1"));
        ddlName.SelectedValue = _GetSecIdx;

    }

    protected void getDeviceStatusEdit(DropDownList ddlName, string _GetStatusIdx)
    {
        //data_qa_cims _data_qa_Edit = new data_qa_cims();
        //_data_qa_Edit.qa_cims_m0_registration_device_list = new qa_cims_m0_registration_device[1];
        //qa_cims_m0_registration_device _statusList = new qa_cims_m0_registration_device();

        //_data_qa_Edit.qa_cims_m0_registration_device_list[0] = _statusList;

        //_data_qa_Edit = callServicePostMasterQACIMS(_urlCimsGetRegistrationDevice, _data_qa_Edit);

        //setDdlData(ddlName, _data_qa_Edit.qa_cims_m0_cal_type_list, "cal_type_name", "cal_type_idx");
        //ddlName.Items.Insert(0, new ListItem("-- กรุณาเลือกประเภทการสอบเทียบ --", "-1"));
        ddlName.SelectedValue = _GetStatusIdx;

    }

    protected void getZoneList(DropDownList ddlName, string _location_idx)
    {

        data_qa_cims _data_locationzone = new data_qa_cims();
        _data_locationzone.qa_cims_m0_location_list = new qa_cims_m0_location_detail[1];

        qa_cims_m0_location_detail _m0_locationzone = new qa_cims_m0_location_detail();
        _m0_locationzone.m0_location_idx = int.Parse(_location_idx);
        _m0_locationzone.condition = 1;

        _data_locationzone.qa_cims_m0_location_list[0] = _m0_locationzone;



        _data_locationzone = callServicePostMasterQACIMS(_urlCimsGetLocationZoneName, _data_locationzone);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_locationzone));

        setDdlData(ddlName, _data_locationzone.qa_cims_m0_location_list, "location_zone", "m1_location_idx");

        ddlName.Items.Insert(0, new ListItem("-- เลือก Zone --", "0"));
        //ddlName.SelectedValue = _location_idx;

    }

    protected void getZoneEditList(DropDownList ddlName, string _location_idx, string _location_zone)
    {

        data_qa_cims _data_locationzone = new data_qa_cims();
        _data_locationzone.qa_cims_m0_location_list = new qa_cims_m0_location_detail[1];

        qa_cims_m0_location_detail _m0_locationzone = new qa_cims_m0_location_detail();
        _m0_locationzone.m0_location_idx = int.Parse(_location_idx);
        _m0_locationzone.condition = 1;

        _data_locationzone.qa_cims_m0_location_list[0] = _m0_locationzone;

        _data_locationzone = callServicePostMasterQACIMS(_urlCimsGetLocationZoneName, _data_locationzone);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_locationzone));

        setDdlData(ddlName, _data_locationzone.qa_cims_m0_location_list, "location_zone", "m1_location_idx");

        ddlName.Items.Insert(0, new ListItem("-- เลือก Zone --", "0"));
        ddlName.SelectedValue = _location_zone;

    }

    protected void getStatusDevicesList(DropDownList ddlName, string _statusdevices_idx)
    {

        data_qa_cims _data_statusdevices = new data_qa_cims();
        _data_statusdevices.qa_cims_m0_statusdevices_list = new qa_cims_m0_statusdevices_detail[1];

        qa_cims_m0_statusdevices_detail _m0_statusdevices = new qa_cims_m0_statusdevices_detail();
        // _m0_statusdevices.m0_status_idx = int.Parse(_statusdevices_idx);       

        _data_statusdevices.qa_cims_m0_statusdevices_list[0] = _m0_statusdevices;

        _data_statusdevices = callServicePostMasterQACIMS(_urlCimsGetStatusDevices, _data_statusdevices);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_statusdevices));

        setDdlData(ddlName, _data_statusdevices.qa_cims_m0_statusdevices_list, "status_name", "m0_status_idx");

        ddlName.Items.Insert(0, new ListItem("-- เลือก Status --", "0"));
        ddlName.SelectedValue = _statusdevices_idx;

    }

    protected void getSelectPlace(DropDownList ddlName, string _placeidx, string place_idx_where)
    {
        data_qa_cims _dataqacimsplace = new data_qa_cims();
        _dataqacimsplace.qa_cims_m0_place_list = new qa_cims_m0_place_detail[1];
        qa_cims_m0_place_detail _m0place = new qa_cims_m0_place_detail();
        _m0place.condition = 2;
        //_m0place.place_idx_check_insert_regis = place_idx_where.ToString();
        _dataqacimsplace.qa_cims_m0_place_list[0] = _m0place;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataqacimsplace));
        _dataqacimsplace = callServicePostMasterQACIMS(_urlCimsGetPlace, _dataqacimsplace);

        setDdlData(ddlName, _dataqacimsplace.qa_cims_m0_place_list, "place_name", "place_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือกสถานที่ --", "0"));
        ddlName.SelectedValue = _placeidx;
    }

    protected void getSelectPlacePermission_Insertcheck(DropDownList ddlName, string _placeidx, string place_idx_where)
    {
        data_qa_cims _dataqacimsplace = new data_qa_cims();
        _dataqacimsplace.qa_cims_m0_place_list = new qa_cims_m0_place_detail[1];
        qa_cims_m0_place_detail _m0place = new qa_cims_m0_place_detail();
        _m0place.condition = 3;
        _m0place.place_idx_check_insert_regis = place_idx_where.ToString();
        _dataqacimsplace.qa_cims_m0_place_list[0] = _m0place;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataqacimsplace));
        _dataqacimsplace = callServicePostMasterQACIMS(_urlCimsGetPlace, _dataqacimsplace);

        setDdlData(ddlName, _dataqacimsplace.qa_cims_m0_place_list, "place_name", "place_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือกสถานที่ --", "0"));
        ddlName.SelectedValue = _placeidx;
    }

    protected void seachregisOrganizationList(DropDownList ddlName)
    {
        _dataEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dataEmployee.organization_list[0] = _orgList;

        _dataEmployee = callServicePostEmployee(_urlGetOrganizationList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือกองค์กร --", "-1"));
    }

    protected void seachregisDepartmentList(DropDownList ddlName, int _org_idx)
    {
        data_employee _dataEmployee_rdept = new data_employee();
        _dataEmployee_rdept.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dataEmployee_rdept.department_list[0] = _deptList;

        _dataEmployee_rdept = callServicePostEmployee(_urlGetDepartmentList, _dataEmployee_rdept);
        setDdlData(ddlName, _dataEmployee_rdept.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือกฝ่าย --", "-1"));
    }

    protected void seachregisSectionList(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {
        ddlName.Items.Clear();
        _dataEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _dataEmployee.section_list[0] = _secList;

        _dataEmployee = callServicePostEmployee(_urlGetSectionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.section_list, "sec_name_th", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือกแผนก --", "-1"));
    }

    #endregion

    #region event Repeater

    protected void rptOnRowDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        var rptName = (Repeater)Sender;
        switch (rptName.ID)
        {
            case "rptBindPlaceForSelectDoc_cims":

                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {

                    var _lbcheck_coler_place = (Label)e.Item.FindControl("lbcheck_coler_place");
                    var _btnDocumentPlaceLab_cims = (LinkButton)e.Item.FindControl("btnDocumentPlaceLab_cims");

                    for (int c_p = 0; c_p <= rptBindPlaceForSelectDoc_cims.Items.Count; c_p++)
                    {
                        _btnDocumentPlaceLab_cims.CssClass = ConfigureColorsPlace(c_p);
                        //_btnDocumentPlaceLab_cims.CssClass = "active";
                    }

                }
                break;

            case "rptEquipmentList":
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    var _lbpic_status = (Label)e.Item.FindControl("lbpic_status");
                    var _lblu1_staidx = (Label)e.Item.FindControl("lblu1_staidx");

                    if (_lblu1_staidx.Text == "29" || _lblu1_staidx.Text == "30")
                    {
                        _lbpic_status.Text = "<i class='fa fa-times' aria-hidden='true'></i>  ";
                        _lbpic_status.ForeColor = System.Drawing.Color.Red;
                        _lbpic_status.ToolTip = "ไม่อนุมัติ";
                    }
                    else if (_lblu1_staidx.Text == "16" || _lblu1_staidx.Text == "27" || _lblu1_staidx.Text == "28")
                    {
                        _lbpic_status.Text = "<i class='fa fa-check' aria-hidden='true'></i>  ";
                        _lbpic_status.ForeColor = System.Drawing.Color.Green;
                        _lbpic_status.ToolTip = "อนุมัติ";
                    }
                    else
                    {
                        _lbpic_status.Text = "<i class='fa fa-clock-o' aria-hidden='true'></i>  ";
                        //_lbpic_status.Text = "<i class='glyphicon glyphicon-time' aria-hidden='true'></i>  ";
                        _lbpic_status.ForeColor = System.Drawing.Color.LightSkyBlue;
                        _lbpic_status.ToolTip = "รอพิจารณา";
                    }
                }
                break;
        }

    }
    #region colore Place
    protected string ConfigureColorsPlace(int c_p)
    {
        string returnColor = "";
        if (c_p == 0)
        {
            returnColor = "btn btn-success";

        }
        else if (c_p == 1)
        {
            returnColor = "btn btn-primary";
        }
        else if (c_p == 2)
        {
            returnColor = "btn btn-warning";
        }
        else if (c_p == 3)
        {
            returnColor = "btn btn-default";
        }
        else if (c_p == 4)
        {
            returnColor = "btn btn-info";
        }
        else
        {
            returnColor = "btn btn-default";
        }

        return returnColor;
    }
    #endregion
    #endregion

    #region function get data

    protected void getToolIDList(TextBox txtName, string CheckID)
    {
        divAlertSearchEquipmentOld.Visible = false;
        data_qa_cims _dataDeviceIDNo = new data_qa_cims();
        _dataDeviceIDNo.qa_cims_m0_registration_device_list = new qa_cims_m0_registration_device[1];
        qa_cims_m0_registration_device _toolSearch = new qa_cims_m0_registration_device();
        _toolSearch.device_id_no = txtName.Text;
        _dataDeviceIDNo.qa_cims_m0_registration_device_list[0] = _toolSearch;

        _dataDeviceIDNo = callServicePostQaDocCIMS(_urlCimsGetSelectSearchTool, _dataDeviceIDNo);

        if (_dataDeviceIDNo.qa_cims_m0_registration_device_list != null)
        {
            setFormData(fvShowDetailsSearchTool, FormViewMode.ReadOnly, _dataDeviceIDNo.qa_cims_m0_registration_device_list);
            divAlertSearchEquipmentOld.Visible = false;
            //  litDebug.Text = "######";
        }
        else
        {
            divAlertSearchEquipmentOld.Visible = true;
            setFormData(fvShowDetailsSearchTool, FormViewMode.ReadOnly, null);
            // litDebug.Text = "1111";
        }



    }

    // เช็ครหัสเลขเครื่อง ว่าซ้ำไหม????
    protected void getCheckIDNoEquipment(TextBox txtName, string CheckID)
    {
        var lbltxtShowIDnumber = (Label)fvInsertCIMS.FindControl("lbltxtShowIDnumber");

        //litDebug.Text = txtName.Text.Trim();

        data_qa_cims dataCims = new data_qa_cims();
        dataCims.qa_cims_m0_registration_device_list = new qa_cims_m0_registration_device[1];
        qa_cims_m0_registration_device _selectRegistration = new qa_cims_m0_registration_device();

        dataCims.qa_cims_m0_registration_device_list[0] = _selectRegistration;
        dataCims = callServicePostMasterQACIMS(_urlCimsGetRegistrationDevice, dataCims);

        //ViewState["vsRegistrationDevice"] = dataCims.qa_cims_m0_registration_device_list;

        ViewState["vsCheckIDandSerial"] = dataCims.qa_cims_m0_registration_device_list;

        qa_cims_m0_registration_device[] _listEquipmentIDNumber = (qa_cims_m0_registration_device[])ViewState["vsCheckIDandSerial"];

        if (_listEquipmentIDNumber == null)
        {

            lbltxtShowIDnumber.Text = "<i class='fa fa-check' aria-hidden='true'></i>  " + "สามารถใช้เลขรหัสเครื่องมือหมายเลขนี้ได้";
            lbltxtShowIDnumber.ForeColor = System.Drawing.Color.Green;

        }
        else
        {

            var _linqCheckIDNumber = (from data_IDNumber in _listEquipmentIDNumber
                                      where data_IDNumber.device_id_no == txtName.Text //.Contains(txtName.Text.Trim())
                                      select new
                                      {
                                          data_IDNumber.device_id_no
                                      }).ToList();


            string _checkIDNumber = "";

            for (int idnumber = 0; idnumber < _linqCheckIDNumber.Count(); idnumber++)
            {
                _checkIDNumber = _linqCheckIDNumber[idnumber].device_id_no.ToString();

                //litDebug.Text = _checkIDNumber;
            }

            //litDebug.Text = _linqCheckIDNumber.Count().ToString();

            if (_checkIDNumber == txtName.Text)
            {

                //litDebug.Text = _checkIDNumber;
                lbltxtShowIDnumber.Text = "<i class='fa fa-times' aria-hidden='true'></i>  " + "มีรหัสเครื่อง  " + _checkIDNumber + " นี้แล้ว กรุณาทำการกรอกรหัสเครื่องใหม่";
                lbltxtShowIDnumber.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                lbltxtShowIDnumber.Text = "<i class='fa fa-check' aria-hidden='true'></i>  " + "สามารถใช้เลขรหัสเครื่องมือหมายเลขนี้ได้";
                lbltxtShowIDnumber.ForeColor = System.Drawing.Color.Green;
            }

        }



    }

    // เช็คหมายเลขเครื่อง ว่าซ้ำไหม????
    protected void getCheckSerialNumberEquipment(TextBox txtName, string CheckID)
    {
        var lbltxtShowSerialNumber = (Label)fvInsertCIMS.FindControl("lbltxtShowSerialNumber");


        data_qa_cims dataCims = new data_qa_cims();
        dataCims.qa_cims_m0_registration_device_list = new qa_cims_m0_registration_device[1];
        qa_cims_m0_registration_device _selectRegistration = new qa_cims_m0_registration_device();

        dataCims.qa_cims_m0_registration_device_list[0] = _selectRegistration;
        dataCims = callServicePostMasterQACIMS(_urlCimsGetRegistrationDevice, dataCims);

        //ViewState["vsRegistrationDevice"] = dataCims.qa_cims_m0_registration_device_list;

        ViewState["vsCheckIDandSerial"] = dataCims.qa_cims_m0_registration_device_list;


        qa_cims_m0_registration_device[] _listEquipmentSerialNumber = ((qa_cims_m0_registration_device[])ViewState["vsCheckIDandSerial"]);

        if (_listEquipmentSerialNumber == null)
        {
            lbltxtShowSerialNumber.Text = "<i class='fa fa-check' aria-hidden='true'></i>  " + "สามารถใช้เลขหมายเลขเครื่องนี้ได้";
            lbltxtShowSerialNumber.ForeColor = System.Drawing.Color.Green;
        }
        else
        {
            var _linqCheckSerialNumber = (from data_SerialNumber in _listEquipmentSerialNumber
                                              //  where data_SerialNumber.device_serial == txtName.Text
                                          where data_SerialNumber.device_serial == txtName.Text//.Contains(txtName.Text)
                                          select new
                                          {
                                              data_SerialNumber.device_serial
                                          }).ToList();

            string _checkSerialNumber = "";

            for (int idnumber = 0; idnumber < _linqCheckSerialNumber.Count(); idnumber++)
            {
                _checkSerialNumber = _linqCheckSerialNumber[idnumber].device_serial.ToString();
            }

            if (_checkSerialNumber == txtName.Text)
            {
                lbltxtShowSerialNumber.Text = "<i class='fa fa-times' aria-hidden='true'></i>  " + "มีหมายเลขเครื่อง  " + _checkSerialNumber + " นี้แล้ว กรุณาทำการกรอกหมายเลขเครื่องใหม่";
                lbltxtShowSerialNumber.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                lbltxtShowSerialNumber.Text = "<i class='fa fa-check' aria-hidden='true'></i>  " + "สามารถใช้เลขหมายเลขเครื่องนี้ได้";
                lbltxtShowSerialNumber.ForeColor = System.Drawing.Color.Green;
            }
        }

    }

    // เช็ครหัสเลขเครื่อง ว่าซ้ำไหม????
    protected void getCheckIDNoEquipmentEdit(TextBox txtName, string CheckID)
    {
        var lbltxtEditShowIDnumber = (Label)fvInsertCIMS.FindControl("lbltxtEditShowIDnumber");
        var lblOldIDNo = (Label)fvInsertCIMS.FindControl("lblOldIDNo");

        data_qa_cims dataCims = new data_qa_cims();
        dataCims.qa_cims_m0_registration_device_list = new qa_cims_m0_registration_device[1];
        qa_cims_m0_registration_device _selectRegistration = new qa_cims_m0_registration_device();

        dataCims.qa_cims_m0_registration_device_list[0] = _selectRegistration;
        dataCims = callServicePostMasterQACIMS(_urlCimsGetRegistrationDevice, dataCims);

        //ViewState["vsRegistrationDevice"] = dataCims.qa_cims_m0_registration_device_list;

        ViewState["vsCheckIDandSerial"] = dataCims.qa_cims_m0_registration_device_list;

        qa_cims_m0_registration_device[] _listEquipmentIDNumber = (qa_cims_m0_registration_device[])ViewState["vsCheckIDandSerial"];


        var _linqCheckIDNumber = (from data_IDNumber in _listEquipmentIDNumber
                                  where data_IDNumber.device_id_no.Contains(txtName.Text.Trim())
                                  && data_IDNumber.device_id_no != lblOldIDNo.Text.Trim()
                                  select new
                                  {
                                      data_IDNumber.device_id_no
                                  }).ToList();

        string _checkIDNumber = "";

        for (int idnumber = 0; idnumber < _linqCheckIDNumber.Count(); idnumber++)
        {
            _checkIDNumber = _linqCheckIDNumber[idnumber].device_id_no.ToString();
        }

        if (_checkIDNumber == txtName.Text)
        {
            lbltxtEditShowIDnumber.Text = "<i class='fa fa-times' aria-hidden='true'></i>  " + "มีรหัสเครื่อง  " + _checkIDNumber + " นี้แล้ว กรุณาทำการกรอกรหัสเครื่องใหม่!!!";
            lbltxtEditShowIDnumber.ForeColor = System.Drawing.Color.Red;
        }
        else
        {
            lbltxtEditShowIDnumber.Text = "<i class='fa fa-check' aria-hidden='true'></i>  " + "สามารถใช้เลขรหัสเครื่องมือหมายเลขนี้ได้!!!";
            lbltxtEditShowIDnumber.ForeColor = System.Drawing.Color.Green;
        }
    }

    // เช็คหมายเลขเครื่อง ว่าซ้ำไหม????
    protected void getCheckSerialNumberEquipmentEdit(TextBox txtName, string CheckID)
    {
        var lbltxtEditShowSerialnumber = (Label)fvInsertCIMS.FindControl("lbltxtEditShowSerialnumber");
        var lblOldSerialNo = (Label)fvInsertCIMS.FindControl("lblOldSerialNo");


        data_qa_cims dataCims = new data_qa_cims();
        dataCims.qa_cims_m0_registration_device_list = new qa_cims_m0_registration_device[1];
        qa_cims_m0_registration_device _selectRegistration = new qa_cims_m0_registration_device();

        dataCims.qa_cims_m0_registration_device_list[0] = _selectRegistration;
        dataCims = callServicePostMasterQACIMS(_urlCimsGetRegistrationDevice, dataCims);

        //ViewState["vsRegistrationDevice"] = dataCims.qa_cims_m0_registration_device_list;

        ViewState["vsCheckIDandSerial"] = dataCims.qa_cims_m0_registration_device_list;


        qa_cims_m0_registration_device[] _listEquipmentSerialNumber = ((qa_cims_m0_registration_device[])ViewState["vsCheckIDandSerial"]).ToArray();

        var _linqCheckSerialNumber = (from data_SerialNumber in _listEquipmentSerialNumber
                                          //  where data_SerialNumber.device_serial == txtName.Text
                                      where data_SerialNumber.device_serial.Contains(txtName.Text)
                                      && data_SerialNumber.device_serial != lblOldSerialNo.Text.Trim()
                                      select new
                                      {
                                          data_SerialNumber.device_serial
                                      }).ToList();

        string _checkSerialNumber = "";

        for (int idnumber = 0; idnumber < _linqCheckSerialNumber.Count(); idnumber++)
        {
            _checkSerialNumber = _linqCheckSerialNumber[idnumber].device_serial.ToString();
        }

        if (_checkSerialNumber == txtName.Text)
        {
            lbltxtEditShowSerialnumber.Text = "<i class='fa fa-times' aria-hidden='true'></i>  " + "มีหมายเลขเครื่อง  " + _checkSerialNumber + " นี้แล้ว กรุณาทำการกรอกหมายเลขเครื่องใหม่!!!";
            lbltxtEditShowSerialnumber.ForeColor = System.Drawing.Color.Red;
        }
        else
        {
            lbltxtEditShowSerialnumber.Text = "<i class='fa fa-check' aria-hidden='true'></i>  " + "สามารถใช้เลขหมายเลขเครื่องนี้ได้!!!";
            lbltxtEditShowSerialnumber.ForeColor = System.Drawing.Color.Green;
        }
    }



    #endregion

    #region data set
    protected void dataSetCalibrationPoint()
    {
        #region View-CalibrationPoint data table excel
        var ds_calibrationpoint_dataexcel = new DataSet();
        ds_calibrationpoint_dataexcel.Tables.Add("CalibrationPointData_Excel");
        ds_calibrationpoint_dataexcel.Tables[0].Columns.Add("calibration_point", typeof(Decimal));
        ds_calibrationpoint_dataexcel.Tables[0].Columns.Add("unit_idx", typeof(int));
        ds_calibrationpoint_dataexcel.Tables[0].Columns.Add("unit_symbol_en", typeof(string));
        ViewState["vsCalibrationPoint_Excel"] = ds_calibrationpoint_dataexcel;
        #endregion
        ViewState["CalPoint_calibration_point_Excel"] = "";
    }

    protected void dataSetResolution()
    {
        #region View-Resolution data table excel
        var ds_Resolution_dataexcel = new DataSet();
        ds_Resolution_dataexcel.Tables.Add("DataResolution");
        ds_Resolution_dataexcel.Tables[0].Columns.Add("ResolutionIDX", typeof(int));
        ds_Resolution_dataexcel.Tables[0].Columns.Add("Resolution", typeof(String));
        ViewState["vsDataResolution"] = ds_Resolution_dataexcel;
        #endregion
        ViewState["Resolution_Excel"] = "";
    }

    protected void dataSetRange()
    {
        #region View-Range Use data table excel
        var ds_Range_dataexcel = new DataSet();
        ds_Range_dataexcel.Tables.Add("DataRangeUse");

        ds_Range_dataexcel.Tables[0].Columns.Add("RangeOfUseMin", typeof(String));
        ds_Range_dataexcel.Tables[0].Columns.Add("RangeOfUseMax", typeof(String));
        ds_Range_dataexcel.Tables[0].Columns.Add("RangeOfUseUnit", typeof(String));
        ds_Range_dataexcel.Tables[0].Columns.Add("RangeOfUseUnitIDX", typeof(int));
        ViewState["vsDataRangeOfUse"] = ds_Range_dataexcel;
        #endregion
        ViewState["RangeUse_Excel"] = "";
    }

    protected void dataSetMeasuring()
    {
        #region View-Measuring data table excel
        var ds_Measuring_dataexcel = new DataSet();
        ds_Measuring_dataexcel.Tables.Add("DataMeasuring");

        ds_Measuring_dataexcel.Tables[0].Columns.Add("MeasuringMin", typeof(String));
        ds_Measuring_dataexcel.Tables[0].Columns.Add("MeasuringMax", typeof(String));
        ds_Measuring_dataexcel.Tables[0].Columns.Add("MeasuringUnit", typeof(String));
        ds_Measuring_dataexcel.Tables[0].Columns.Add("MeasuringUnitIDX", typeof(int));

        ViewState["vsDataMeasuring"] = ds_Measuring_dataexcel;
        #endregion
        ViewState["Measuring_Excel"] = "";
    }

    protected void dataSetAcceptance()
    {
        #region View-Acceptance data table excel
        var ds_Acceptance_dataexcel = new DataSet();
        ds_Acceptance_dataexcel.Tables.Add("DataAcceptance");

        ds_Acceptance_dataexcel.Tables[0].Columns.Add("Acceptance", typeof(String));
        ds_Acceptance_dataexcel.Tables[0].Columns.Add("AcceptanceUnit", typeof(String));
        ds_Acceptance_dataexcel.Tables[0].Columns.Add("AcceptanceUnitIDX", typeof(int));

        ViewState["vsDataAcceptance"] = ds_Acceptance_dataexcel;
        #endregion
        ViewState["Acceptance_Excel"] = "";
    }





    protected void CleardataSetCalibrationPoint()
    {
        var GvDeptExcel = (GridView)fvInsertCIMS.FindControl("GvDeptExcel");
        ViewState["vsCalibrationPoint_Excel"] = null;
        GvDeptExcel.DataSource = ViewState["vsCalibrationPoint_Excel"];
        GvDeptExcel.DataBind();
        dataSetCalibrationPoint();
    }

    protected void CleardataSetResolution()
    {
        var gvResolution = (GridView)fvInsertCIMS.FindControl("gvResolution");
        ViewState["vsDataResolution"] = null;
        gvResolution.DataSource = ViewState["vsDataResolution"];
        gvResolution.DataBind();
        dataSetResolution();
    }

    protected void CleardataSetRange()
    {
        var gvRange = (GridView)fvInsertCIMS.FindControl("gvRange");
        ViewState["vsDataRangeOfUse"] = null;
        gvRange.DataSource = ViewState["vsDataRangeOfUse"];
        gvRange.DataBind();
        dataSetRange();
    }

    protected void CleardataSetMeasuring()
    {
        var gvMeasuring = (GridView)fvInsertCIMS.FindControl("gvMeasuring");
        ViewState["vsDataMeasuring"] = null;
        gvMeasuring.DataSource = ViewState["vsDataMeasuring"];
        gvMeasuring.DataBind();
        dataSetMeasuring();
    }

    protected void CleardataSetAcceptance()
    {
        var gvAcceptance = (GridView)fvInsertCIMS.FindControl("gvAcceptance");
        ViewState["vsDataAcceptance"] = null;
        gvAcceptance.DataSource = ViewState["vsDataAcceptance"];
        gvAcceptance.DataBind();
        dataSetAcceptance();
    }



    #endregion

    #region event link button
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {
            case "cmdSearchTool":

                TextBox tbsearchingToolID = (TextBox)pnEquipmentOld.FindControl("tbsearchingToolID");

                if (tbsearchingToolID.Text != "")
                {
                    getToolIDList((TextBox)pnEquipmentOld.FindControl("tbsearchingToolID"), "0");
                    tbsearchingToolID.Text = string.Empty;
                    divAlertSearchEquipmentOld.Visible = false;
                }
                else
                {

                }
                break;

            case "cmdTranferEquipment":

                tbSearchIDNoTranfer.Text = String.Empty;
                tbSearchIDNoTranfer.Enabled = true;
                txtcoment_tranfer.Text = String.Empty;
                ChkAll.Checked = false;
                setFormData(fvDetailUserInPageRegistration, FormViewMode.ReadOnly, ((data_employee)ViewState["vsEmpProfile"]).employee_list);
                divTranferRegistration.Visible = true;
                divRegistrationList.Visible = false;
                divTranferRegistrationList.Visible = true;
                btnhiddensearch.Visible = false;
                div_SearchIndex_1.Visible = false;
                //btnResetSearchPage.Visible = true;

                divAlertTranferSeacrh.Visible = false;
                TabSearch.Visible = false;

                string[] cmdArgTranfer = cmdArg.Split(',');
                int rsec_idx_from_tranfer = int.TryParse(cmdArgTranfer[0].ToString(), out _default_int) ? int.Parse(cmdArgTranfer[0].ToString()) : _default_int;
                int place_idx_tranfer = int.TryParse(cmdArgTranfer[1].ToString(), out _default_int) ? int.Parse(cmdArgTranfer[1].ToString()) : _default_int;

                ViewState["vsPlaceIDX"] = place_idx_tranfer;
                ViewState["vsrsec_idx_from_tranfer"] = rsec_idx_from_tranfer;


                data_qa_cims _dataqaChecktool = new data_qa_cims();
                _dataqaChecktool.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
                qa_cims_u1_document_device_detail _selectDocument = new qa_cims_u1_document_device_detail();
                _selectDocument.condition = 5;
                _dataqaChecktool.qa_cims_u1_document_device_list[0] = _selectDocument;
                _dataqaChecktool = callServicePostQaDocCIMS(_urlCimsGetEquipmentList, _dataqaChecktool);

                string _listStatusTool_M0deviceIDX = "";

                if (_dataqaChecktool.qa_cims_u1_document_device_list != null)
                {
                    for (int CheckStatusTool = 0; CheckStatusTool < _dataqaChecktool.qa_cims_u1_document_device_list.Count(); CheckStatusTool++)
                    {
                        _listStatusTool_M0deviceIDX += _dataqaChecktool.qa_cims_u1_document_device_list[CheckStatusTool].m0_device_idx.ToString() + ",";
                    }

                }

                data_qa_cims _dataSelectTool = new data_qa_cims();
                _dataSelectTool.qa_cims_m0_registration_device_list = new qa_cims_m0_registration_device[1];
                qa_cims_m0_registration_device _selectranfer = new qa_cims_m0_registration_device();
                _selectranfer.device_rsec_idx = rsec_idx_from_tranfer;//int.Parse(ViewState["rsec_permission"].ToString());
                _selectranfer.list_m0_device = _listStatusTool_M0deviceIDX;
                _selectranfer.condition = 1;
                _selectranfer.place_idx = int.Parse(tbPlaceIDXInDocument.Text);
                _selectranfer.cal_type_idx = int.Parse(tbLabIDXInDocument.Text);

                _dataSelectTool.qa_cims_m0_registration_device_list[0] = _selectranfer;

                _dataSelectTool = callServicePostMasterQACIMS(_urlCimsGetRegistrationDevice, _dataSelectTool);

                ViewState["vsEquipmentListHolder"] = _dataSelectTool.qa_cims_m0_registration_device_list;
                setGridData(gvListEquipment, ViewState["vsEquipmentListHolder"]);

                gvListEquipment.Visible = true;
                getOrganizationList(ddlOrgtranfer);
                getDepartmentList(ddlRdepttranfer, int.Parse(ddlOrgtranfer.SelectedValue));
                getSectionListWhere(ddlRsectranfer, int.Parse(ddlOrgtranfer.SelectedValue), int.Parse(ddlRdepttranfer.SelectedValue));

                data_qa_cims _dataCimsDecisionNodeTranfer = new data_qa_cims();
                _dataCimsDecisionNodeTranfer.qa_cims_bindnode_decision_list = new qa_cims_bindnode_decision_detail[1];
                qa_cims_bindnode_decision_detail _selectDecisionNodeTranfer = new qa_cims_bindnode_decision_detail();
                _dataCimsDecisionNodeTranfer.qa_cims_bindnode_decision_list[0] = _selectDecisionNodeTranfer;



                _dataCimsDecisionNodeTranfer = callServicePostQaDocCIMS(_urlCimsGetDecisionTranfer, _dataCimsDecisionNodeTranfer);
                setRepeaterData(rptActionTranfer, _dataCimsDecisionNodeTranfer.qa_cims_bindnode_decision_list);

                getSelectPlace((DropDownList)divTranferRegistrationList.FindControl("ddlPlaceTranfer"), tbPlaceIDXInDocument.Text, "0");
              
                tbSearchIDNoTranfer.Focus();

                break;

            case "cmdCancelTranfer":
                //setActiveTab("docRegistration", 0, 0, 0, 0, 0, 0);
                divCutoffRegistrationList.Visible = false;
                divTranferRegistrationList.Visible = false;

                divRegistrationList.Visible = true;
                fv_SearchIndex.Visible = true;
                btnsearchshow.Visible = true;
                div_SearchIndex_1.Visible = true;

                viewRegistration.Visible = false;
                divTranferRegistration.Visible = false;
                btnhiddensearch.Visible = false;
                TabSearch.Visible = false;

                setFormData(fvOwner, FormViewMode.ReadOnly, null);
                setFormData(fvEquipmentDetail, FormViewMode.ReadOnly, null);


                getM0CalType((DropDownList)docRegistration.FindControl("dllcaltype_search"), tbLabIDXInDocument.Text);
                getSelectPlace((DropDownList)docRegistration.FindControl("ddlPlaceMachine_search"), tbPlaceIDXInDocument.Text, "0");

                ClearGvRegisterDevices(tbPlaceIDXInDocument.Text, tbLabIDXInDocument.Text);

                setOntop.Focus();
                break;

            case "cmdResetDocument":

                TextBox txtseach_samplecode_lab_reset = (TextBox)docList.FindControl("txtseach_samplecode_lab");

                txtseach_samplecode_lab_reset.Text = String.Empty;

                setActiveTab("docList", 0, 0, 0, 0, 0, 0);

                //setActiveTab("docList", 0, 0, 0, 0, 0, 0);
                //setOntop.Focus();
                break;

            case "cmdAddRegis":

                //linkBtnTrigger(Lbtn_submit_cal_type);
                setActiveTab("docRegistration", 0, 0, 0, 0, 0, int.Parse(cmdArg));
                string c_tab = cmdArg;

                if (c_tab == "1")
                {

                    div_SearchIndex_1.Visible = false;
                    // div_SearchIndex.Visible = false;
                    fv_SearchIndex.Visible = false;
                    TabSearch.Visible = false;
                    show_gvregistration.Visible = false;


                    //check permission insert
                    data_qa_cims _data_check_permission_qa = new data_qa_cims();
                    _data_check_permission_qa.qa_cims_m0_management_list = new qa_cims_m0_management_detail[1];
                    qa_cims_m0_management_detail _select_perQA_Check = new qa_cims_m0_management_detail();

                    _select_perQA_Check.condition = 2;
                    _select_perQA_Check.rpos_idx = int.Parse(ViewState["rpos_permission"].ToString()); 


                    _data_check_permission_qa.qa_cims_m0_management_list[0] = _select_perQA_Check;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_check_permission_qa));
                    _data_check_permission_qa = callServicePostQaDocCIMS(_urlCimsGetCheckPermissionQA, _data_check_permission_qa);

                    if(_data_check_permission_qa.return_code == 0)
                    {
                        string place_idx_insert_regis =  _data_check_permission_qa.qa_cims_m0_management_list[0].place_idx_check_insertregis;

                        //litDebug.Text = place_idx_insert_regis;

                        setFormData(fvDetailUserInPageRegistration, FormViewMode.ReadOnly, ((data_employee)ViewState["vsEmpProfile"]).employee_list);

                        DropDownList ddlOrg_excel = (DropDownList)fvInsertCIMS.FindControl("ddlOrg_excel");
                        DropDownList ddlDept_excel = (DropDownList)fvInsertCIMS.FindControl("ddlDept_excel");
                        DropDownList ddlSec_excel = (DropDownList)fvInsertCIMS.FindControl("ddlSec_excel");


                        setFormData(fvInsertCIMS, FormViewMode.Insert, null);
                        setFormData(fvOwner, FormViewMode.Insert, null);
                        setFormData(fvEquipmentDetail, FormViewMode.Insert, null);
                        log_register.Visible = false;

                        linkBtnTrigger(Lbtn_submit_cal_type);

                        getM0EquipmentName((DropDownList)fvInsertCIMS.FindControl("ddlEquipmentName"), "0");
                        getM0Brand((DropDownList)fvInsertCIMS.FindControl("ddlBrand"), "0");
                        getM0EquipmentType((DropDownList)fvInsertCIMS.FindControl("ddlEquipType"), "0");

                        getM0Location((DropDownList)fvInsertCIMS.FindControl("ddl_location"), "0");

                        getM0Unit((DropDownList)fvInsertCIMS.FindControl("ddlUnitROU"), "0");
                        getM0Unit((DropDownList)fvInsertCIMS.FindControl("ddlUnitMR"), "0");
                        getM0Unit((DropDownList)fvInsertCIMS.FindControl("ddlUnitAC"), "0");
                        getM0Unit((DropDownList)fvInsertCIMS.FindControl("ddlUnitCP"), "0");

                        getM0CalType((DropDownList)fvInsertCIMS.FindControl("dllcaltype"), "0"); 
                        getOrganizationList((DropDownList)fvInsertCIMS.FindControl("ddlOrg_excel"));
                        getDepartmentList((DropDownList)fvInsertCIMS.FindControl("ddlDept_excel"), -1);
                        getSectionList((DropDownList)fvInsertCIMS.FindControl("ddlSec_excel"), -1, -1);

                        getSelectPlacePermission_Insertcheck((DropDownList)fvInsertCIMS.FindControl("ddlPlaceMachine"), "0", place_idx_insert_regis.ToString());

                        getResolution((DropDownList)fvInsertCIMS.FindControl("ddlResolution"), "0");
                        getFrequency((DropDownList)fvInsertCIMS.FindControl("ddlCalFrequency"), "0");

                        var btnInsertDeptExcel = (LinkButton)fvInsertCIMS.FindControl("btnInsertDeptExcel");
                        var btnAddAcceptance = (LinkButton)fvInsertCIMS.FindControl("btnAddAcceptance");
                        var btnAddMeasuring = (LinkButton)fvInsertCIMS.FindControl("btnAddMeasuring");
                        var btnAddRangeOfUse = (LinkButton)fvInsertCIMS.FindControl("btnAddRangeOfUse");
                        var btnAddResolution = (LinkButton)fvInsertCIMS.FindControl("btnAddResolution");

                        CleardataSetCalibrationPoint();
                        CleardataSetResolution();
                        CleardataSetRange();
                        CleardataSetMeasuring();
                        CleardataSetAcceptance();

                        linkBtnTrigger(btnInsertDeptExcel);
                        linkBtnTrigger(btnAddAcceptance);
                        linkBtnTrigger(btnAddMeasuring);
                        linkBtnTrigger(btnAddRangeOfUse);
                        linkBtnTrigger(btnAddResolution);
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('คุณไม่มีสิทธิ์ เพิ่มทะเบียนเครื่องมือ!!!');", true);
                        setActiveTab("docRegistration", 0, 0, 0, 0, 0, 0);



                        return;
                    }



                    
                }


                break;

            case "cmdCancelInsertRegis":

                setActiveTab("docRegistration", 0, 0, 0, 0, 0, 0);

                setFormData(fvDetailUserInPageRegistration, FormViewMode.ReadOnly, null);
                setFormData(fvInsertCIMS, FormViewMode.ReadOnly, null);

                setOntop.Focus();


                break;

            case "cmdCancelEditRegis":

                setActiveTab("docRegistration", 0, 0, 0, 0, 0, 0);
                setFormData(fvInsertCIMS, FormViewMode.ReadOnly, null);

                viewRegistration.Visible = false;

                setFormData(fvOwner, FormViewMode.ReadOnly, null);
                setFormData(fvEquipmentDetail, FormViewMode.ReadOnly, null);


                setOntop.Focus();

                break;

            case "btnAddCalFrequency":

                var gvFrequency = (GridView)fvInsertCIMS.FindControl("gvFrequency");
                var ddlCalFrequency = (DropDownList)fvInsertCIMS.FindControl("ddlCalFrequency");

                var ds_add_frequency = (DataSet)ViewState["vsDataFrequency"];
                var dr_add_frequency = ds_add_frequency.Tables[0].NewRow();

                int numrow_frequency = ds_add_frequency.Tables[0].Rows.Count; //จำนวนแถว

                if (numrow_frequency > 0)
                {
                    dr_add_frequency["Frequency"] = (ddlCalFrequency.SelectedItem.Text);
                    dr_add_frequency["FrequencyIDX"] = int.Parse(ddlCalFrequency.SelectedValue);

                    ds_add_frequency.Tables[0].Rows.Add(dr_add_frequency);
                    ViewState["vsDataFrequency"] = ds_add_frequency;

                    setGridData(gvFrequency, ViewState["vsDataFrequency"]);
                }
                else if (numrow_frequency == 0)
                {
                    dr_add_frequency["Frequency"] = (ddlCalFrequency.SelectedItem.Text);
                    dr_add_frequency["FrequencyIDX"] = int.Parse(ddlCalFrequency.SelectedValue);

                    ds_add_frequency.Tables[0].Rows.Add(dr_add_frequency);
                    ViewState["vsDataFrequency"] = ds_add_frequency;

                    setGridData(gvFrequency, ViewState["vsDataFrequency"]);
                }


                break;

            case "btnAddResolution":

                var gvResolution = (GridView)fvInsertCIMS.FindControl("gvResolution");
                var ddlResolution = (DropDownList)fvInsertCIMS.FindControl("ddlResolution");

                var ds_add_resolution = (DataSet)ViewState["vsDataResolution"];
                var dr_add_resolution = ds_add_resolution.Tables[0].NewRow();

                int numrow_resolution = ds_add_resolution.Tables[0].Rows.Count; //จำนวนแถว


                ViewState["_vsResolution"] = ddlResolution.SelectedItem.ToString();

                if (ddlResolution.SelectedValue != "0")
                {

                    if (numrow_resolution > 0)
                    {

                        foreach (DataRow check_resolutionds in ds_add_resolution.Tables[0].Rows)
                        {
                            ViewState["check_Resolution"] = check_resolutionds["Resolution"];

                            if (ViewState["_vsResolution"].ToString() == ViewState["check_Resolution"].ToString())
                            {
                                ViewState["CheckDataset_vsResolution"] = "0"; //ซ้ำ
                                break;

                            }
                            else
                            {
                                ViewState["CheckDataset_vsResolution"] = "1";

                            }
                        }

                        //
                        if (ViewState["CheckDataset_vsResolution"].ToString() == "1")
                        {
                            //ข้อมูลที่ต้องการเพิ่ม
                            dr_add_resolution["Resolution"] = (ddlResolution.SelectedItem.Text);
                            dr_add_resolution["ResolutionIDX"] = int.Parse(ddlResolution.SelectedValue);

                            ds_add_resolution.Tables[0].Rows.Add(dr_add_resolution);
                            ViewState["vsDataResolution"] = ds_add_resolution;

                            setGridData(gvResolution, ViewState["vsDataResolution"]);

                        }
                        else if (ViewState["CheckDataset_vsResolution"].ToString() == "0")
                        {

                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีค่าอ่านละเอียดนี้แล้ว!!!');", true);

                        }




                    }
                    else
                    {
                        dr_add_resolution["Resolution"] = (ddlResolution.SelectedItem.Text);
                        dr_add_resolution["ResolutionIDX"] = int.Parse(ddlResolution.SelectedValue);

                        ds_add_resolution.Tables[0].Rows.Add(dr_add_resolution);
                        ViewState["vsDataResolution"] = ds_add_resolution;

                        setGridData(gvResolution, ViewState["vsDataResolution"]);
                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาเลือกค่าอ่านละเอียด!!!');", true);
                }


                break;

            case "btnAddRangeOfUse":

                var gvRange = (GridView)fvInsertCIMS.FindControl("gvRange");
                var txtRangeOfUseMax = (TextBox)fvInsertCIMS.FindControl("txtROUend");
                var txtRangeOfUseMin = (TextBox)fvInsertCIMS.FindControl("txtROUstart");
                var ddlUnitRangeOfUse = (DropDownList)fvInsertCIMS.FindControl("ddlUnitROU");

                var ds_add_range_of_use = (DataSet)ViewState["vsDataRangeOfUse"];
                var dr_add_range_of_use = ds_add_range_of_use.Tables[0].NewRow();

                int numrow_range = ds_add_range_of_use.Tables[0].Rows.Count; //จำนวนแถว

                ViewState["_vstxtRangeOfUseMax_ds"] = txtRangeOfUseMax.Text;
                ViewState["_vstxtRangeOfUseMin_ds"] = txtRangeOfUseMin.Text;
                ViewState["_vsddlUnitRangeOfUse_ds"] = ddlUnitRangeOfUse.SelectedItem.ToString();

                if (ddlUnitRangeOfUse.SelectedValue != "0" && txtRangeOfUseMax.Text != "" && txtRangeOfUseMin.Text != "")
                {

                    if (numrow_range > 0)
                    {

                        foreach (DataRow check_RangeOfUse in ds_add_range_of_use.Tables[0].Rows)
                        {
                            ViewState["check_RangeOfUseMin"] = check_RangeOfUse["RangeOfUseMin"];
                            ViewState["check_RangeOfUseMax"] = check_RangeOfUse["RangeOfUseMax"];
                            ViewState["check_RangeOfUseUnit"] = check_RangeOfUse["RangeOfUseUnit"];

                            if (ViewState["_vstxtRangeOfUseMin_ds"].ToString() == ViewState["check_RangeOfUseMin"].ToString() && ViewState["_vstxtRangeOfUseMax_ds"].ToString() == ViewState["check_RangeOfUseMax"].ToString()
                                && ViewState["_vsddlUnitRangeOfUse_ds"].ToString() == ViewState["check_RangeOfUseUnit"].ToString())
                            {
                                ViewState["CheckDataset_RangeOfUse"] = "0"; //ซ้ำ
                                break;

                            }
                            else
                            {
                                ViewState["CheckDataset_RangeOfUse"] = "1";

                            }

                        }

                        if (ViewState["CheckDataset_RangeOfUse"].ToString() == "1")
                        {
                            //ข้อมูลที่ต้องการเพิ่ม
                            dr_add_range_of_use["RangeOfUseUnit"] = (ddlUnitRangeOfUse.SelectedItem.Text);
                            dr_add_range_of_use["RangeOfUseUnitIDX"] = int.Parse(ddlUnitRangeOfUse.SelectedValue);
                            dr_add_range_of_use["RangeOfUseMax"] = txtRangeOfUseMax.Text;
                            dr_add_range_of_use["RangeOfUseMin"] = txtRangeOfUseMin.Text;

                            ds_add_range_of_use.Tables[0].Rows.Add(dr_add_range_of_use);
                            ViewState["vsDataRangeOfUse"] = ds_add_range_of_use;

                            setGridData(gvRange, ViewState["vsDataRangeOfUse"]);

                        }
                        else if (ViewState["CheckDataset_RangeOfUse"].ToString() == "0")
                        {

                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีค่าช่วงการใช้งานนี้แล้ว!!!');", true);

                        }



                    }
                    else
                    {
                        dr_add_range_of_use["RangeOfUseUnit"] = (ddlUnitRangeOfUse.SelectedItem.Text);
                        dr_add_range_of_use["RangeOfUseUnitIDX"] = int.Parse(ddlUnitRangeOfUse.SelectedValue);
                        dr_add_range_of_use["RangeOfUseMax"] = txtRangeOfUseMax.Text;
                        dr_add_range_of_use["RangeOfUseMin"] = txtRangeOfUseMin.Text;

                        ds_add_range_of_use.Tables[0].Rows.Add(dr_add_range_of_use);
                        ViewState["vsDataRangeOfUse"] = ds_add_range_of_use;

                        setGridData(gvRange, ViewState["vsDataRangeOfUse"]);
                    }
                }
                else
                {

                    //litDebug.Text = "666";
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาใส่ค่า และเลือกหน่วยของช่วงการใช้งานก่อน!!!');", true);
                }
                break;

            case "btnAddMeasuring":

                var gvMeasuring = (GridView)fvInsertCIMS.FindControl("gvMeasuring");
                var txtMeasuringMin = (TextBox)fvInsertCIMS.FindControl("txtMRstart");
                var txtMeasuringMax = (TextBox)fvInsertCIMS.FindControl("txtMRend");
                var ddlUnitMeasuring = (DropDownList)fvInsertCIMS.FindControl("ddlUnitMR");

                var ds_add_measuring = (DataSet)ViewState["vsDataMeasuring"];
                var dr_add_measuring = ds_add_measuring.Tables[0].NewRow();

                int numrow_measuring = ds_add_measuring.Tables[0].Rows.Count; //จำนวนแถว

                ViewState["_vstxtMeasuringMin"] = txtMeasuringMin.Text;
                ViewState["_vstxtMeasuringMax"] = txtMeasuringMax.Text;
                ViewState["_vsddlUnitMeasuring"] = ddlUnitMeasuring.SelectedItem.ToString();

                if (ddlUnitMeasuring.SelectedValue != "0" && txtMeasuringMin.Text != "" && txtMeasuringMax.Text != "")
                {
                    if (numrow_measuring > 0)
                    {
                        foreach (DataRow check_Measuring in ds_add_measuring.Tables[0].Rows)
                        {
                            ViewState["check_MeasuringMin"] = check_Measuring["MeasuringMin"];
                            ViewState["check_MeasuringMax"] = check_Measuring["MeasuringMax"];
                            ViewState["check_MeasuringUnit"] = check_Measuring["MeasuringUnit"];

                            if (ViewState["_vstxtMeasuringMin"].ToString() == ViewState["check_MeasuringMin"].ToString() && ViewState["_vstxtMeasuringMax"].ToString() == ViewState["check_MeasuringMax"].ToString()
                                && ViewState["_vsddlUnitMeasuring"].ToString() == ViewState["check_MeasuringUnit"].ToString())
                            {
                                ViewState["CheckDataset_Measuring"] = "0"; //ซ้ำ
                                break;

                            }
                            else
                            {
                                ViewState["CheckDataset_Measuring"] = "1";

                            }

                        }
                        //ค่าที่ไม่ซ้ำ
                        if (ViewState["CheckDataset_Measuring"].ToString() == "1")
                        {
                            //ข้อมูลที่ต้องการเพิ่ม
                            dr_add_measuring["MeasuringUnit"] = (ddlUnitMeasuring.SelectedItem.Text);
                            dr_add_measuring["MeasuringUnitIDX"] = int.Parse(ddlUnitMeasuring.SelectedValue);
                            dr_add_measuring["MeasuringMax"] = txtMeasuringMax.Text;
                            dr_add_measuring["MeasuringMin"] = txtMeasuringMin.Text;

                            ds_add_measuring.Tables[0].Rows.Add(dr_add_measuring);
                            ViewState["vsDataMeasuring"] = ds_add_measuring;

                            setGridData(gvMeasuring, ViewState["vsDataMeasuring"]);

                        }
                        else if (ViewState["CheckDataset_Measuring"].ToString() == "0")
                        {

                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีค่าช่วงการวัด/พิสัยนี้แล้ว!!!');", true);

                        }


                    }
                    else if (numrow_measuring == 0)
                    {
                        dr_add_measuring["MeasuringUnit"] = (ddlUnitMeasuring.SelectedItem.Text);
                        dr_add_measuring["MeasuringUnitIDX"] = int.Parse(ddlUnitMeasuring.SelectedValue);
                        dr_add_measuring["MeasuringMax"] = txtMeasuringMax.Text;
                        dr_add_measuring["MeasuringMin"] = txtMeasuringMin.Text;

                        ds_add_measuring.Tables[0].Rows.Add(dr_add_measuring);
                        ViewState["vsDataMeasuring"] = ds_add_measuring;

                        setGridData(gvMeasuring, ViewState["vsDataMeasuring"]);
                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาใส่ค่า และเลือกหน่วยของช่วงการวัด/พิสัยก่อน!!!');", true);
                }
                break;

            case "btnAddAcceptance":

                var gvAcceptance = (GridView)fvInsertCIMS.FindControl("gvAcceptance");
                var txtAcceptance = (TextBox)fvInsertCIMS.FindControl("txtAC");
                var ddlUnitAcceptance = (DropDownList)fvInsertCIMS.FindControl("ddlUnitAC");

                var ds_add_acceptance = (DataSet)ViewState["vsDataAcceptance"];
                var dr_add_acceptance = ds_add_acceptance.Tables[0].NewRow();

                int numrow_acceptance = ds_add_acceptance.Tables[0].Rows.Count; //จำนวนแถว

                ViewState["_vstxtAcceptance"] = txtAcceptance.Text;
                ViewState["_vsddlUnitAcceptance"] = ddlUnitAcceptance.SelectedItem.ToString();

                if (ddlUnitAcceptance.SelectedValue != "0" && txtAcceptance.Text != "")
                {

                    if (numrow_acceptance > 0)
                    {

                        foreach (DataRow check_Acceptance in ds_add_acceptance.Tables[0].Rows)
                        {
                            ViewState["check_Acceptance"] = check_Acceptance["Acceptance"];
                            ViewState["check_AcceptanceUnit"] = check_Acceptance["AcceptanceUnit"];

                            if (ViewState["_vstxtAcceptance"].ToString() == ViewState["check_Acceptance"].ToString() && ViewState["_vsddlUnitAcceptance"].ToString() == ViewState["check_AcceptanceUnit"].ToString())
                            {
                                ViewState["CheckDataset_Acceptance"] = "0"; //ซ้ำ
                                break;

                            }
                            else
                            {
                                ViewState["CheckDataset_Acceptance"] = "1";

                            }

                        }
                        //ค่าที่ไม่ซ้ำ
                        if (ViewState["CheckDataset_Acceptance"].ToString() == "1")
                        {
                            //ข้อมูลที่ต้องการเพิ่ม
                            dr_add_acceptance["AcceptanceUnit"] = (ddlUnitAcceptance.SelectedItem.Text);
                            dr_add_acceptance["AcceptanceUnitIDX"] = int.Parse(ddlUnitAcceptance.SelectedValue);
                            dr_add_acceptance["Acceptance"] = txtAcceptance.Text;

                            ds_add_acceptance.Tables[0].Rows.Add(dr_add_acceptance);
                            ViewState["vsDataAcceptance"] = ds_add_acceptance;

                            setGridData(gvAcceptance, ViewState["vsDataAcceptance"]);
                        }
                        else if (ViewState["CheckDataset_Acceptance"].ToString() == "0")
                        {

                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีค่าเกณฑ์การยอมรับนี้แล้ว!!!');", true);

                        }


                    }
                    else if (numrow_acceptance == 0)
                    {
                        dr_add_acceptance["AcceptanceUnit"] = (ddlUnitAcceptance.SelectedItem.Text);
                        dr_add_acceptance["AcceptanceUnitIDX"] = int.Parse(ddlUnitAcceptance.SelectedValue);
                        dr_add_acceptance["Acceptance"] = txtAcceptance.Text;

                        ds_add_acceptance.Tables[0].Rows.Add(dr_add_acceptance);
                        ViewState["vsDataAcceptance"] = ds_add_acceptance;

                        setGridData(gvAcceptance, ViewState["vsDataAcceptance"]);
                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาใส่ค่า และเลือกหน่วยของเกณฑ์การยอมรับก่อน!!!');", true);
                }

                break;


            case "btnInsertDeptExcel":


                TextBox txtcalpoint_excel_insert = (TextBox)fvInsertCIMS.FindControl("txtcalpoint");
                DropDownList ddlUnitCP_excel_insert = (DropDownList)fvInsertCIMS.FindControl("ddlUnitCP");

                GridView GvDeptExcel = (GridView)fvInsertCIMS.FindControl("GvDeptExcel");

                if (ddlUnitCP_excel_insert.SelectedValue != "0" && txtcalpoint_excel_insert.Text != "")
                {

                    //litDebug.Text = "555";
                    //Add-Department
                    var ds_Add_calibrationpoint_excel = (DataSet)ViewState["vsCalibrationPoint_Excel"];
                    var dr_Add_calibrationpoint_excel = ds_Add_calibrationpoint_excel.Tables[0].NewRow();

                    int numrow_calp_excel = ds_Add_calibrationpoint_excel.Tables[0].Rows.Count; //จำนวนแถว

                    ViewState["_vscalibration_point"] = txtcalpoint_excel_insert.Text;
                    ViewState["_vsddlUnitCP_excel_insert"] = ddlUnitCP_excel_insert.SelectedItem.ToString();

                    if (numrow_calp_excel > 0)
                    {
                        foreach (DataRow check_calpoint in ds_Add_calibrationpoint_excel.Tables[0].Rows)
                        {
                            ViewState["check_calibration_point"] = check_calpoint["calibration_point"];
                            ViewState["check_ddlUnitCP_excel_insert"] = check_calpoint["unit_symbol_en"];

                            if (ViewState["_vscalibration_point"].ToString() == ViewState["check_calibration_point"].ToString() && ViewState["_vsddlUnitCP_excel_insert"].ToString() == ViewState["check_ddlUnitCP_excel_insert"].ToString())
                            {
                                ViewState["CheckDataset_calibration_point"] = "0"; //ซ้ำ
                                break;

                            }
                            else
                            {
                                ViewState["CheckDataset_calibration_point"] = "1";

                            }

                        }
                        //ค่าที่ไม่ซ้ำ
                        if (ViewState["CheckDataset_calibration_point"].ToString() == "1")
                        {
                            //ข้อมูลที่ต้องการเพิ่ม
                            dr_Add_calibrationpoint_excel["calibration_point"] = decimal.Parse(txtcalpoint_excel_insert.Text);
                            dr_Add_calibrationpoint_excel["unit_idx"] = int.Parse(ddlUnitCP_excel_insert.SelectedValue);
                            dr_Add_calibrationpoint_excel["unit_symbol_en"] = ddlUnitCP_excel_insert.SelectedItem.Text;

                            ds_Add_calibrationpoint_excel.Tables[0].Rows.Add(dr_Add_calibrationpoint_excel);
                            ViewState["vsCalibrationPoint_Excel"] = ds_Add_calibrationpoint_excel;

                            //////  create data set show detail license  ////////
                            setGridData(GvDeptExcel, ViewState["vsCalibrationPoint_Excel"]);

                        }
                        else if (ViewState["CheckDataset_calibration_point"].ToString() == "0")
                        {

                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีค่าจุดสอบเทียบนี้แล้ว!!!');", true);

                        }
                    }

                    else
                    {
                        //ข้อมูลที่ต้องการเพิ่ม
                        dr_Add_calibrationpoint_excel["calibration_point"] = decimal.Parse(txtcalpoint_excel_insert.Text);
                        dr_Add_calibrationpoint_excel["unit_idx"] = int.Parse(ddlUnitCP_excel_insert.SelectedValue);
                        dr_Add_calibrationpoint_excel["unit_symbol_en"] = ddlUnitCP_excel_insert.SelectedItem.Text;

                        ds_Add_calibrationpoint_excel.Tables[0].Rows.Add(dr_Add_calibrationpoint_excel);
                        ViewState["vsCalibrationPoint_Excel"] = ds_Add_calibrationpoint_excel;

                        //////  create data set show detail license  ////////
                        setGridData(GvDeptExcel, ViewState["vsCalibrationPoint_Excel"]);

                    }
                }
                else
                {

                    //litDebug.Text = "666";
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาใส่ค่า และเลือกหน่วยของจุดสอบเทียบก่อน!!!');", true);
                }

                break;

            case "btnInsertCalpointEdit":
                TextBox txtcalpoint = (TextBox)fvInsertCIMS.FindControl("txtcalpointEdit");
                DropDownList ddlUnitCP = (DropDownList)fvInsertCIMS.FindControl("ddlUnitCPEdit");

                if (ddlUnitCP.SelectedValue != "0" && txtcalpoint.Text != String.Empty)
                {
                    _data_qa_cims.qa_cims_m1_registration_device_list = new qa_cims_m1_registration_device[1];
                    qa_cims_m1_registration_device _m1_Form = new qa_cims_m1_registration_device();
                    _m1_Form.m0_device_idx = int.Parse(ViewState["vsM0DeviceIDX"].ToString());
                    _m1_Form.calibration_point = (txtcalpoint.Text);
                    _m1_Form.unit_idx = int.Parse(ddlUnitCP.SelectedValue);
                    _m1_Form.cemp_idx = _emp_idx;
                    _m1_Form.status = 1;
                    _data_qa_cims.qa_cims_m1_registration_device_list[0] = _m1_Form;

                    //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_qa));
                    _data_qa_cims = callServicePostMasterQACIMS(_urlCimsSetM1RegistrationDevice, _data_qa_cims);
                    if (_data_qa_cims.return_code == 101)
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);

                    }

                    SelectCalibration_point(ViewState["vsM0DeviceIDX"].ToString());
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาใส่ค่า และเลือกหน่วยของจุดสอบเทียบก่อน!!!');", true);
                }


                break;

            case "btnAddResolutionUpdate":

                var _insert_forUpdateResolution = (DropDownList)fvInsertCIMS.FindControl("ddlResolutionUpdate");

                if (_insert_forUpdateResolution.SelectedValue != "0")
                {
                    data_qa_cims _data_resolution = new data_qa_cims();
                    _data_resolution.qa_cims_m2_registration_device_list = new qa_cims_m2_registration_device[1];
                    qa_cims_m2_registration_device _insert_resolution = new qa_cims_m2_registration_device();
                    _insert_resolution.m0_device_idx = int.Parse(ViewState["vsM0DeviceIDX"].ToString());
                    _insert_resolution.m2_device_resolution_idx = 0;
                    _insert_resolution.equipment_resolution_idx = int.Parse(_insert_forUpdateResolution.SelectedValue);
                    _insert_resolution.cemp_idx = _emp_idx;
                    _insert_resolution.status = 1;
                    _data_resolution.qa_cims_m2_registration_device_list[0] = _insert_resolution;
                    _data_resolution = callServicePostMasterQACIMS(_urlCimsSetM0_Resolution, _data_resolution);

                    if (_data_resolution.return_code == 101)
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);

                    }

                    selectResolution(ViewState["vsM0DeviceIDX"].ToString());

                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาเลือกค่าอ่านละเอียดก่อน!!!');", true);

                }

                break;
            case "btnAddRangeUpdate":

                var _tbRangeOfUseMinUpdate = (TextBox)fvInsertCIMS.FindControl("tbRangeOfUseMinUpdate");
                var _tbRangeOfUseMaxUpdate = (TextBox)fvInsertCIMS.FindControl("tbRangeOfUseMaxUpdate");
                var _ddl_range_of_use_unitUpdate = (DropDownList)fvInsertCIMS.FindControl("ddl_range_of_use_unitUpdate");

                if (_tbRangeOfUseMinUpdate.Text != "" && _tbRangeOfUseMaxUpdate.Text != "" && _ddl_range_of_use_unitUpdate.SelectedValue != "0")
                {
                    data_qa_cims _data_rangeinsert = new data_qa_cims();
                    _data_rangeinsert.qa_cims_m4_registration_device_list = new qa_cims_m4_registration_device[1];
                    qa_cims_m4_registration_device m4_rangeinsert = new qa_cims_m4_registration_device();

                    m4_rangeinsert.m0_device_idx = int.Parse(ViewState["vsM0DeviceIDX"].ToString());
                    m4_rangeinsert.m4_device_range_idx = 0;
                    m4_rangeinsert.range_start = _tbRangeOfUseMinUpdate.Text;
                    m4_rangeinsert.range_end = _tbRangeOfUseMaxUpdate.Text;
                    m4_rangeinsert.unit_idx = int.Parse(_ddl_range_of_use_unitUpdate.SelectedValue);
                    m4_rangeinsert.cemp_idx = _emp_idx;
                    m4_rangeinsert.status = 1;

                    _data_rangeinsert.qa_cims_m4_registration_device_list[0] = m4_rangeinsert;
                    _data_rangeinsert = callServicePostMasterQACIMS(_urlCimsSetRangeUseDevice, _data_rangeinsert);

                    if (_data_rangeinsert.return_code == 101)
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);

                    }

                    selectgvRangeUpdate(ViewState["vsM0DeviceIDX"].ToString());

                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาใส่ค่า และเลือกหน่วยของช่วงการใช้งานก่อน!!!');", true);

                }

                break;

            case "btnAddMeasuringUpdate":

                var _txt_Measuringstart_update = (TextBox)fvInsertCIMS.FindControl("txt_Measuringstart_update");
                var _txt_Measuringend_update = (TextBox)fvInsertCIMS.FindControl("txt_Measuringend_update");
                var _ddlMeasuring_Update = (DropDownList)fvInsertCIMS.FindControl("ddlMeasuring_Update");


                if (_txt_Measuringstart_update.Text != "" && _txt_Measuringend_update.Text != "" && _ddlMeasuring_Update.SelectedValue != "0")
                {
                    data_qa_cims _data_Measuring = new data_qa_cims();
                    _data_Measuring.qa_cims_m5_registration_device_list = new qa_cims_m5_registration_device[1];
                    qa_cims_m5_registration_device _insert_Measuring = new qa_cims_m5_registration_device();

                    _insert_Measuring.m0_device_idx = int.Parse(ViewState["vsM0DeviceIDX"].ToString());
                    _insert_Measuring.m5_device_measduring_ix = 0;
                    _insert_Measuring.measuring_start = _txt_Measuringstart_update.Text;
                    _insert_Measuring.measuring_end = _txt_Measuringend_update.Text; ;
                    _insert_Measuring.unit_idx = int.Parse(_ddlMeasuring_Update.SelectedValue);
                    _insert_Measuring.cemp_idx = _emp_idx;
                    _insert_Measuring.status = 1;

                    _data_Measuring.qa_cims_m5_registration_device_list[0] = _insert_Measuring;
                    _data_Measuring = callServicePostMasterQACIMS(_urlCimsSetMeasuringDevice, _data_Measuring);

                    if (_data_Measuring.return_code == 101)
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);

                    }


                    selectgvMeasuringUpdate(ViewState["vsM0DeviceIDX"].ToString());

                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาใส่ค่า และเลือกหน่วยของช่วงการวัด/พิสัยก่อน!!!');", true);

                }

                break;
            case "btnAddAcceptanceUpdate":

                var _txt_AcceptanceUpdate = (TextBox)fvInsertCIMS.FindControl("txt_AcceptanceUpdate");
                var _ddlAcceptance_Update = (DropDownList)fvInsertCIMS.FindControl("ddlAcceptance_Update");


                if (_txt_AcceptanceUpdate.Text != "" && _ddlAcceptance_Update.SelectedValue != "0")
                {
                    data_qa_cims _data_Acceptance = new data_qa_cims();
                    _data_Acceptance.qa_cims_m6_registration_device_list = new qa_cims_m6_registration_device[1];
                    qa_cims_m6_registration_device _insert_Acceptance = new qa_cims_m6_registration_device();

                    _insert_Acceptance.m0_device_idx = int.Parse(ViewState["vsM0DeviceIDX"].ToString());
                    _insert_Acceptance.m6_device_acceptance_idx = 0;
                    _insert_Acceptance.acceptance_criteria = _txt_AcceptanceUpdate.Text; ;
                    _insert_Acceptance.unit_idx = int.Parse(_ddlAcceptance_Update.SelectedValue);
                    _insert_Acceptance.cemp_idx = _emp_idx;
                    _insert_Acceptance.status = 1;

                    _data_Acceptance.qa_cims_m6_registration_device_list[0] = _insert_Acceptance;
                    _data_Acceptance = callServicePostMasterQACIMS(_urlCimsSetAcceptanceDevice, _data_Acceptance);

                    if (_data_Acceptance.return_code == 101)
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);

                    }

                    selectgvAcceptanceUpdate(ViewState["vsM0DeviceIDX"].ToString());

                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาใส่ค่า และเลือกหน่วยของเกณฑ์การยอมรับก่อน!!!');", true);

                }

                break;

            case "cmdSave":

                var ddlBrand = (DropDownList)fvInsertCIMS.FindControl("ddlBrand");
                var ddlEquipmentName = (DropDownList)fvInsertCIMS.FindControl("ddlEquipmentName");
                var ddlEquipType = (DropDownList)fvInsertCIMS.FindControl("ddlEquipType");
                var _ddlCalFrequency = (DropDownList)fvInsertCIMS.FindControl("ddlCalFrequency");
                var txtserialno = (TextBox)fvInsertCIMS.FindControl("txtserialno");
                var txtequipmentNo = (TextBox)fvInsertCIMS.FindControl("txtequipmentNo");
                var dllcaltype = (DropDownList)fvInsertCIMS.FindControl("dllcaltype");
                var txtequipmentdetail = (TextBox)fvInsertCIMS.FindControl("txtequipmentdetail");

                var HiddenCalDate = (HiddenField)fvInsertCIMS.FindControl("HiddenCalDate");
                var HiddenDueDate = (HiddenField)fvInsertCIMS.FindControl("HiddenDueDate");

                var txt_cal_date = (TextBox)fvInsertCIMS.FindControl("txt_cal_date");
                var txt_due_date = (TextBox)fvInsertCIMS.FindControl("txt_due_date");

                var ddlOrg = (DropDownList)fvInsertCIMS.FindControl("ddlOrg_excel");
                var ddlDept = (DropDownList)fvInsertCIMS.FindControl("ddlDept_excel");
                var ddlSec = (DropDownList)fvInsertCIMS.FindControl("ddlSec_excel");
                var ddlFormStatus = (DropDownList)fvInsertCIMS.FindControl("ddlFormStatus");
                var txtinput_machine_name = (TextBox)fvInsertCIMS.FindControl("txtinput_machine_name");
                var txtinput_brand = (TextBox)fvInsertCIMS.FindControl("txtinput_brand");
                var chk_other_name = (CheckBox)fvInsertCIMS.FindControl("chk_other_name");
                var chk_other_brand = (CheckBox)fvInsertCIMS.FindControl("chk_other_brand");
                var ddlPlaceMachine = (DropDownList)fvInsertCIMS.FindControl("ddlPlaceMachine");

                var lbltxtShowIDnumber = (Label)fvInsertCIMS.FindControl("lbltxtShowIDnumber");
                var lbltxtShowSerialNumber = (Label)fvInsertCIMS.FindControl("lbltxtShowSerialNumber");
                var GvDeptExcel_CP = (GridView)fvInsertCIMS.FindControl("GvDeptExcel");

                var txtReceiveDevices = (TextBox)fvInsertCIMS.FindControl("txtReceiveDevices");
                var rd_certificate = (RadioButtonList)fvInsertCIMS.FindControl("rd_certificate");
                var location_devices = (DropDownList)fvInsertCIMS.FindControl("ddl_location");
                var zonelocation_devices = (DropDownList)fvInsertCIMS.FindControl("ddl_Zone");


                tbPlaceIDXInDocument.Text = ddlPlaceMachine.SelectedValue.ToString();
                tbLabIDXInDocument.Text = dllcaltype.SelectedValue.ToString();
                linkBtnTrigger(Lbtn_submit_cal_type);
                if (GvDeptExcel_CP.Rows.Count > 0)
                {
                    _data_qa_cims.qa_cims_m0_registration_device_list = new qa_cims_m0_registration_device[1];
                    qa_cims_m0_registration_device _m0_Form = new qa_cims_m0_registration_device();

                    _data_qa_cims.qa_cims_m0_equipment_name_list = new qa_cims_m0_equipment_name_detail[1];
                    qa_cims_m0_equipment_name_detail _m0_EquipmentName = new qa_cims_m0_equipment_name_detail();

                    _data_qa_cims.qa_cims_m0_brand_list = new qa_cims_m0_brand_detail[1];
                    qa_cims_m0_brand_detail _m0_Brand = new qa_cims_m0_brand_detail();

                    _data_qa_cims.qa_cims_m3_registration_device_list = new qa_cims_m3_registration_device[1];
                    qa_cims_m3_registration_device _addFrequency = new qa_cims_m3_registration_device();

                    _m0_Form.device_serial = txtserialno.Text;
                    _m0_Form.device_org_idx = int.Parse(ddlOrg.SelectedValue);
                    _m0_Form.device_rdept_idx = int.Parse(ddlDept.SelectedValue);
                    _m0_Form.device_rsec_idx = int.Parse(ddlSec.SelectedValue);

                    _m0_Form.device_cal_date = txt_cal_date.Text;
                    _m0_Form.device_due_date = txt_due_date.Text;
                    _m0_Form.certificate = int.Parse(rd_certificate.Text);

                    _m0_Form.m0_location_idx = int.Parse(location_devices.SelectedValue);

                    if (zonelocation_devices.SelectedValue == "0")
                    {
                        _m0_Form.m1_location_idx = int.Parse(zonelocation_devices.SelectedValue);
                    }
                    else
                    {
                        _m0_Form.m1_location_idx = int.Parse(zonelocation_devices.SelectedValue);
                    }

                    //string status_devices_online = "1"; //Online
                    //string status_devices_offline = "2"; // Offline
                    //string status_devices_obsolate = "3"; // Obsolate

                    //status devices machine
                    if (rd_certificate.SelectedValue == "1") //have certificate
                    {
                        _m0_Form.m0_status_idx = int.Parse(status_devices_online);
                    }
                    else
                    {
                        _m0_Form.m0_status_idx = int.Parse(status_devices_offline);
                    }


                    _m0_Form.receive_devices = txtReceiveDevices.Text;
                    //_m0_Form.device_status = int.Parse(ddlFormStatus.SelectedValue);
                    _m0_Form.emp_idx_create = _emp_idx;
                    _m0_Form.org_idx_create = int.Parse(ViewState["org_permission"].ToString());
                    _m0_Form.rdept_idx_create = int.Parse(ViewState["rdept_permission"].ToString());
                    _m0_Form.rsec_idx_create = int.Parse(ViewState["rsec_permission"].ToString());
                    _m0_Form.equipment_idx = int.Parse(ddlEquipmentName.SelectedValue);
                    _m0_Form.brand_idx = int.Parse(ddlBrand.SelectedValue);
                    _m0_Form.place_idx = int.Parse(ddlPlaceMachine.SelectedValue);
                    _m0_Form.equipment_type_idx = int.Parse(ddlEquipType.SelectedValue);

                    _addFrequency.equipment_frequency_idx = int.Parse(_ddlCalFrequency.SelectedValue);

                    if (chk_other_brand.Checked || chk_other_name.Checked)
                    {
                        _m0_Form.condition = 1;
                        if (chk_other_name.Checked)
                        {
                            _m0_Form.equipment_idx = 0;
                            //_m0_Form.brand_idx = 0;
                            _m0_EquipmentName.equipment_name = txtinput_machine_name.Text;
                            _m0_EquipmentName.cemp_idx = _emp_idx;
                            _m0_EquipmentName.equipment_status = 1;

                            _data_qa_cims.qa_cims_m0_equipment_name_list[0] = _m0_EquipmentName;
                        }
                        else if (chk_other_brand.Checked)
                        {
                            //_m0_Form.equipment_idx = 0;
                            _m0_Form.brand_idx = 0;
                            _m0_Brand.brand_name = txtinput_brand.Text;
                            _m0_Brand.cemp_idx = _emp_idx;
                            _m0_Brand.brand_status = 1;

                            _data_qa_cims.qa_cims_m0_brand_list[0] = _m0_Brand;

                        }
                        else if (chk_other_brand.Checked && chk_other_name.Checked)
                        {
                            _m0_Form.equipment_idx = 0;
                            _m0_Form.brand_idx = 0;
                            _m0_EquipmentName.equipment_name = txtinput_machine_name.Text;
                            _m0_EquipmentName.cemp_idx = _emp_idx;
                            _m0_EquipmentName.equipment_status = 1;
                            _m0_Brand.brand_name = txtinput_brand.Text;
                            _m0_Brand.cemp_idx = _emp_idx;
                            _m0_Brand.brand_status = 1;

                            _data_qa_cims.qa_cims_m0_brand_list[0] = _m0_Brand;
                            _data_qa_cims.qa_cims_m0_equipment_name_list[0] = _m0_EquipmentName;
                        }
                    }
                    _m0_Form.device_id_no = txtequipmentNo.Text;
                    _m0_Form.device_details = txtequipmentdetail.Text;
                    _m0_Form.cal_type_idx = int.Parse(dllcaltype.SelectedValue);


                    if (ViewState["vsCalibrationPoint_Excel"] != null && ViewState["vsDataResolution"] != null && ViewState["vsDataRangeOfUse"] != null
                        && ViewState["vsDataMeasuring"] != null && ViewState["vsDataAcceptance"] != null)
                    {
                        var _datasetCreateForm = (DataSet)ViewState["vsCalibrationPoint_Excel"];
                        var _addFormList = new qa_cims_m1_registration_device[_datasetCreateForm.Tables[0].Rows.Count];
                        int i = 0;

                        foreach (DataRow datarowForm in _datasetCreateForm.Tables[0].Rows)
                        {
                            _addFormList[i] = new qa_cims_m1_registration_device();
                            _addFormList[i].calibration_point = (datarowForm["calibration_point"].ToString());
                            _addFormList[i].unit_idx = int.Parse(datarowForm["unit_idx"].ToString());
                            _addFormList[i].cemp_idx = _emp_idx;

                            i++;


                        }

                        //Data Resolution
                        var _datasetResolution = (DataSet)ViewState["vsDataResolution"];
                        var _addResolution = new qa_cims_m2_registration_device[_datasetResolution.Tables[0].Rows.Count];
                        int Resolution = 0;

                        foreach (DataRow dtrowResolution in _datasetResolution.Tables[0].Rows)
                        {
                            _addResolution[Resolution] = new qa_cims_m2_registration_device();
                            _addResolution[Resolution].equipment_resolution_idx = int.Parse(dtrowResolution["ResolutionIDX"].ToString());
                            _addResolution[Resolution].cemp_idx = _emp_idx;

                            Resolution++;

                        }

                        //Data Range Of Use
                        var _datasetRangeOfUse = (DataSet)ViewState["vsDataRangeOfUse"];
                        var _addRangeOfUse = new qa_cims_m4_registration_device[_datasetRangeOfUse.Tables[0].Rows.Count];
                        int RangeOfUse = 0;

                        foreach (DataRow dtrowRangeOfUse in _datasetRangeOfUse.Tables[0].Rows)
                        {
                            _addRangeOfUse[RangeOfUse] = new qa_cims_m4_registration_device();
                            _addRangeOfUse[RangeOfUse].unit_idx = int.Parse(dtrowRangeOfUse["RangeOfUseUnitIDX"].ToString());
                            _addRangeOfUse[RangeOfUse].range_start = (dtrowRangeOfUse["RangeOfUseMin"].ToString());
                            _addRangeOfUse[RangeOfUse].range_end = (dtrowRangeOfUse["RangeOfUseMax"].ToString());
                            _addRangeOfUse[RangeOfUse].cemp_idx = _emp_idx;

                            RangeOfUse++;

                        }

                        //Data Measuring
                        var _datasetMeasuring = (DataSet)ViewState["vsDataMeasuring"];
                        var _addMeasuring = new qa_cims_m5_registration_device[_datasetMeasuring.Tables[0].Rows.Count];
                        int Measuring = 0;

                        foreach (DataRow dtrowMeasuring in _datasetMeasuring.Tables[0].Rows)
                        {
                            _addMeasuring[Measuring] = new qa_cims_m5_registration_device();
                            _addMeasuring[Measuring].unit_idx = int.Parse(dtrowMeasuring["MeasuringUnitIDX"].ToString());
                            _addMeasuring[Measuring].measuring_start = (dtrowMeasuring["MeasuringMin"].ToString());
                            _addMeasuring[Measuring].measuring_end = (dtrowMeasuring["MeasuringMax"].ToString());
                            _addMeasuring[Measuring].cemp_idx = _emp_idx;

                            Measuring++;

                        }

                        //Data Acceptance
                        var _datasetAcceptance = (DataSet)ViewState["vsDataAcceptance"];
                        var _addAcceptance = new qa_cims_m6_registration_device[_datasetAcceptance.Tables[0].Rows.Count];
                        int Acceptance = 0;

                        foreach (DataRow dtrowAcceptance in _datasetAcceptance.Tables[0].Rows)
                        {
                            _addAcceptance[Acceptance] = new qa_cims_m6_registration_device();
                            _addAcceptance[Acceptance].unit_idx = int.Parse(dtrowAcceptance["AcceptanceUnitIDX"].ToString());
                            _addAcceptance[Acceptance].acceptance_criteria = (dtrowAcceptance["Acceptance"].ToString());
                            _addAcceptance[Acceptance].cemp_idx = _emp_idx;

                            Acceptance++;

                        }


                        if (_datasetCreateForm.Tables[0].Rows.Count == 0 && _datasetResolution.Tables[0].Rows.Count == 0 && _datasetRangeOfUse.Tables[0].Rows.Count == 0
                            && _datasetMeasuring.Tables[0].Rows.Count == 0 && _datasetAcceptance.Tables[0].Rows.Count == 0)
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณากรอกข้อมูลให้ครบถ้วน !!!');", true);
                        }
                        else
                        {

                            _data_qa_cims.qa_cims_m0_registration_device_list[0] = _m0_Form;
                            _data_qa_cims.qa_cims_m1_registration_device_list = _addFormList;
                            _data_qa_cims.qa_cims_m2_registration_device_list = _addResolution;
                            _data_qa_cims.qa_cims_m3_registration_device_list[0] = _addFrequency;
                            _data_qa_cims.qa_cims_m4_registration_device_list = _addRangeOfUse;
                            _data_qa_cims.qa_cims_m5_registration_device_list = _addMeasuring;
                            _data_qa_cims.qa_cims_m6_registration_device_list = _addAcceptance;

                        //    litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_qa_cims));

                            _data_qa_cims = callServicePostMasterQACIMS(_urlCimsSetRegistrationDevice, _data_qa_cims);

                            if (_data_qa_cims.return_code == 0)
                            {
                                //Panel div_file_certificate = (Panel)fvInsertCIMS.FindControl("div_file_certificate");
                                FileUpload File_certificate = (FileUpload)fvInsertCIMS.FindControl("File_certificate");

                                if (File_certificate.HasFile)
                                {
                                    //litDebug.Text = "333";

                                    string gtepathfile_devices_certificate = ConfigurationManager.AppSettings["pathfile_devices_certificate"];

                                    string id_equipment_code = txtequipmentNo.Text;//+ txtserialno.Tex;
                                    string fileName_upload = id_equipment_code;

                                    string filePath_upload = Server.MapPath(gtepathfile_devices_certificate + id_equipment_code);

                                    if (!Directory.Exists(filePath_upload))
                                    {
                                        Directory.CreateDirectory(filePath_upload);
                                    }
                                    string extension = Path.GetExtension(File_certificate.FileName);

                                    //litDebug.Text += ((getPathfile_equipmrnt + id_equipment_code) + "\\" + fileName_upload + extension + " //END// ");

                                    File_certificate.SaveAs(Server.MapPath(gtepathfile_devices_certificate + id_equipment_code) + "\\" + fileName_upload + extension);

                                }
                                else
                                {
                                    //litDebug.Text = "555";
                                }

                                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('เพิ่มข้อมูลทะเบียนเครื่องมือสำเร็จ !!!');", true);
                                setFormData(fvInsertCIMS, FormViewMode.ReadOnly, null);

                                setActiveTab("docRegistration", 0, 0, 0, 0, 0, 0);
                                //BackToCurrentPageGvRegistrationList();
                            }
                            setOntop.Focus();
                        }
                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณากรอกข้อมูลให้ครบถ้วน');", true);
                }

                break;

            case "cmdInsertPerManagement":

                DropDownList ddlorg_idx_insert = (DropDownList)fvInsertPerQA.FindControl("ddlorg_idx");
                DropDownList ddlrdept_idx_insert = (DropDownList)fvInsertPerQA.FindControl("ddlrdept_idx");
                DropDownList ddlrsec_idx_insert = (DropDownList)fvInsertPerQA.FindControl("ddlrsec_idx");
                DropDownList ddlrpos_idx_insert = (DropDownList)fvInsertPerQA.FindControl("ddlrpos_idx");

                data_qa_cims _data_per_insert = new data_qa_cims();

                _data_per_insert.qa_cims_m0_management_list = new qa_cims_m0_management_detail[1];
                qa_cims_m0_management_detail _m0_management_insert = new qa_cims_m0_management_detail();

                _m0_management_insert.cemp_idx = _emp_idx;
                _m0_management_insert.org_idx = int.Parse(ddlorg_idx_insert.SelectedValue);
                _m0_management_insert.rdept_idx = int.Parse(ddlrdept_idx_insert.SelectedValue);
                _m0_management_insert.rsec_idx = int.Parse(ddlrsec_idx_insert.SelectedValue);
                _m0_management_insert.rpos_idx = int.Parse(ddlrpos_idx_insert.SelectedValue);

                //place list vs
                var _dataset_PlaceList = (DataSet)ViewState["vsPlacePermissionList"];

                var _add_PlaceList = new qa_cims_m1_management_detail[_dataset_PlaceList.Tables[0].Rows.Count];
                int row_placelist = 0;

                foreach (DataRow dtrow_Placelist in _dataset_PlaceList.Tables[0].Rows)
                {
                    _add_PlaceList[row_placelist] = new qa_cims_m1_management_detail();

                    _add_PlaceList[row_placelist].place_idx = int.Parse(dtrow_Placelist["drPlacePerIdx"].ToString());
                    _add_PlaceList[row_placelist].cemp_idx = _emp_idx;

                    row_placelist++;

                }

                var _dataset_PermanageList = (DataSet)ViewState["vsPerManageList"];

                var _add_PermanageList = new qa_cims_m2_management_detail[_dataset_PermanageList.Tables[0].Rows.Count];
                int row_permanagelist = 0;

                foreach (DataRow dtrow_Permanagelist in _dataset_PermanageList.Tables[0].Rows)
                {
                    _add_PermanageList[row_permanagelist] = new qa_cims_m2_management_detail();

                    _add_PermanageList[row_permanagelist].m0_per_management_idx = int.Parse(dtrow_Permanagelist["drPerIdx"].ToString());
                    _add_PermanageList[row_permanagelist].cemp_idx = _emp_idx;

                    row_permanagelist++;

                }

                _data_per_insert.qa_cims_m0_management_list[0] = _m0_management_insert;
                _data_per_insert.qa_cims_m1_management_list = _add_PlaceList;
                _data_per_insert.qa_cims_m2_management_list = _add_PermanageList;

                //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_per_insert));
                _data_per_insert = callServicePostMasterQACIMS(_urlCimsSetPermissionManageQA, _data_per_insert);

                if (_data_per_insert.return_code == 0)
                {
                    setActiveTab("docManagement", 0, 0, 0, 0, 0, 0);
                    setOntop.Focus();
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีในระบบแล้ว ไม่สามารถเพิ่มได้');", true);
                    return;
                }




                break;

            case "cmdSaveUpdatePer":

                DropDownList ddlorg_idxupdate = (DropDownList)fvInsertPerQA.FindControl("ddlorg_idxupdate");
                DropDownList ddlrdept_idxupdate = (DropDownList)fvInsertPerQA.FindControl("ddlrdept_idxupdate");
                DropDownList ddlrsec_idxupdate = (DropDownList)fvInsertPerQA.FindControl("ddlrsec_idxupdate");
                DropDownList ddlrpos_idxupdate = (DropDownList)fvInsertPerQA.FindControl("ddlrpos_idxupdate");
                DropDownList ddlm0_management_statusUpdate = (DropDownList)fvInsertPerQA.FindControl("ddlm0_management_statusUpdate");

                data_qa_cims _data_per_update = new data_qa_cims();

                _data_per_update.qa_cims_m0_management_list = new qa_cims_m0_management_detail[1];
                qa_cims_m0_management_detail _m0_management_update = new qa_cims_m0_management_detail();

                _m0_management_update.m0_management_idx = int.Parse(cmdArg);
                _m0_management_update.cemp_idx = _emp_idx;
                _m0_management_update.m0_management_status = int.Parse(ddlm0_management_statusUpdate.SelectedValue);
                _m0_management_update.org_idx = int.Parse(ddlorg_idxupdate.SelectedValue);
                _m0_management_update.rdept_idx = int.Parse(ddlrdept_idxupdate.SelectedValue);
                _m0_management_update.rsec_idx = int.Parse(ddlrsec_idxupdate.SelectedValue);
                _m0_management_update.rpos_idx = int.Parse(ddlrpos_idxupdate.SelectedValue);

                //place list vs
                var _dataset_PlaceList_update = (DataSet)ViewState["vsPlaceListUpdate"];

                var _add_PlaceList_update = new qa_cims_m1_management_detail[_dataset_PlaceList_update.Tables[0].Rows.Count];
                int row_placelist_update = 0;

                foreach (DataRow dtrow_Placelist in _dataset_PlaceList_update.Tables[0].Rows)
                {
                    _add_PlaceList_update[row_placelist_update] = new qa_cims_m1_management_detail();

                    _add_PlaceList_update[row_placelist_update].place_idx = int.Parse(dtrow_Placelist["drPlaceIdxUpdate"].ToString());
                    _add_PlaceList_update[row_placelist_update].cemp_idx = _emp_idx;

                    row_placelist_update++;

                }

                var _dataset_PermanageList_update = (DataSet)ViewState["vsPerListUpdate"];

                var _add_PermanageList_update = new qa_cims_m2_management_detail[_dataset_PermanageList_update.Tables[0].Rows.Count];
                int row_permanagelist_update = 0;

                foreach (DataRow dtrow_Permanagelist in _dataset_PermanageList_update.Tables[0].Rows)
                {
                    _add_PermanageList_update[row_permanagelist_update] = new qa_cims_m2_management_detail();

                    _add_PermanageList_update[row_permanagelist_update].m0_per_management_idx = int.Parse(dtrow_Permanagelist["drPerIdxUpdate"].ToString());
                    _add_PermanageList_update[row_permanagelist_update].cemp_idx = _emp_idx;

                    row_permanagelist_update++;

                }

                _data_per_update.qa_cims_m0_management_list[0] = _m0_management_update;
                _data_per_update.qa_cims_m1_management_list = _add_PlaceList_update;
                _data_per_update.qa_cims_m2_management_list = _add_PermanageList_update;

                //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_per_update));
                _data_per_update = callServicePostMasterQACIMS(_urlCimsSetPermissionManageQA, _data_per_update);

                if (_data_per_update.return_code == 1)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีในระบบแล้ว ไม่สามารถเพิ่มได้');", true);
                    return;
                }
                else
                {
                    setActiveTab("docManagement", 0, 0, 0, 0, 0, 0);
                    setOntop.Focus();
                }

                break;

            case "cmdReportDevicesOnline":

                string[] report_idx = new string[2];
                report_idx = e.CommandArgument.ToString().Split(';');
                int place_idx = int.Parse(report_idx[0]);
                int condition_idx = int.Parse(report_idx[1]);

                div_SearchReportOnline.Visible = false;
                div_SearchReportOffline.Visible = false;
                div_SearchReportCutoff.Visible = false;


                switch (condition_idx)
                {
                    case 1: //Report Detail Devices Online

                        DropDownList ddl_orgidx_report_detail = (DropDownList)div_SearchReportOnline.FindControl("ddl_orgidx_report");
                        DropDownList ddl_rdeptidx_report_detail = (DropDownList)div_SearchReportOnline.FindControl("ddl_rdeptidx_report");
                        DropDownList ddl_rsecidx_report_detail = (DropDownList)div_SearchReportOnline.FindControl("ddl_rsecidx_report");


                        div_ReportDetail_Online.Visible = true;
                        data_qa_cims _data_report_online = new data_qa_cims();
                        _data_report_online.qa_cims_reportdevices_list = new qa_cims_reportdevices_detail[1];
                        qa_cims_reportdevices_detail _detail_report_online = new qa_cims_reportdevices_detail();

                        _detail_report_online.condition = condition_idx;
                        _detail_report_online.place_idx = place_idx;
                        _detail_report_online.device_org_idx = int.Parse(ddl_orgidx_report_detail.SelectedValue);
                        _detail_report_online.device_rdept_idx = int.Parse(ddl_rdeptidx_report_detail.SelectedValue);
                        _detail_report_online.device_rsec_idx = int.Parse(ddl_rsecidx_report_detail.SelectedValue);

                        _data_report_online.qa_cims_reportdevices_list[0] = _detail_report_online;
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_searchdetail));
                        _data_report_online = callServicePostQaDocCIMS(_urlCimsGetReportDetailExportDevices, _data_report_online);

                        ViewState["vs_ReportDetailDevicesOnline"] = _data_report_online.qa_cims_reportdevices_list;

                        setGridData(gvReportDetailOnline, ViewState["vs_ReportDetailDevicesOnline"]);



                        break;
                    case 2:

                        DropDownList ddl_orgidx_report_offline_detail = (DropDownList)div_SearchReportOffline.FindControl("ddl_orgidx_report_offline");
                        DropDownList ddl_rdeptidx_report_offline_detail = (DropDownList)div_SearchReportOffline.FindControl("ddl_rdeptidx_report_offline");
                        DropDownList ddl_rsecidx_report_offline_detail = (DropDownList)div_SearchReportOffline.FindControl("ddl_rsecidx_report_offline");


                        div_ReportDetail_Offline.Visible = true;

                        data_qa_cims _data_report_offilne = new data_qa_cims();
                        _data_report_offilne.qa_cims_reportdevices_list = new qa_cims_reportdevices_detail[1];
                        qa_cims_reportdevices_detail _detail_report_offline = new qa_cims_reportdevices_detail();

                        _detail_report_offline.condition = condition_idx;
                        _detail_report_offline.place_idx = place_idx;
                        _detail_report_offline.device_org_idx = int.Parse(ddl_orgidx_report_offline_detail.SelectedValue);
                        _detail_report_offline.device_rdept_idx = int.Parse(ddl_rdeptidx_report_offline_detail.SelectedValue);
                        _detail_report_offline.device_rsec_idx = int.Parse(ddl_rsecidx_report_offline_detail.SelectedValue);

                        _data_report_offilne.qa_cims_reportdevices_list[0] = _detail_report_offline;
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_searchdetail));
                        _data_report_offilne = callServicePostQaDocCIMS(_urlCimsGetReportDetailExportDevices, _data_report_offilne);

                        ViewState["vs_ReportDetailDevicesOffline"] = _data_report_offilne.qa_cims_reportdevices_list;

                        setGridData(gvReportDetailOffline, ViewState["vs_ReportDetailDevicesOffline"]);

                        break;
                    case 3:

                        DropDownList ddl_orgidx_report_cutoff_detail = (DropDownList)div_SearchReportCutoff.FindControl("ddl_orgidx_report_cutoff");
                        DropDownList ddl_rdeptidx_report_cutoff_detail = (DropDownList)div_SearchReportCutoff.FindControl("ddl_rdeptidx_report_cutoff");
                        DropDownList ddl_rsecidx_report_cutoff_detail = (DropDownList)div_SearchReportCutoff.FindControl("ddl_rsecidx_report_cutoff");


                        div_ReportDetail_Cutoff.Visible = true;

                        data_qa_cims _data_report_cutoff = new data_qa_cims();
                        _data_report_cutoff.qa_cims_reportdevices_list = new qa_cims_reportdevices_detail[1];
                        qa_cims_reportdevices_detail _detail_report_cutoff = new qa_cims_reportdevices_detail();

                        _detail_report_cutoff.condition = condition_idx;
                        _detail_report_cutoff.place_idx = place_idx;
                        _detail_report_cutoff.device_org_idx = int.Parse(ddl_orgidx_report_cutoff_detail.SelectedValue);
                        _detail_report_cutoff.device_rdept_idx = int.Parse(ddl_rdeptidx_report_cutoff_detail.SelectedValue);
                        _detail_report_cutoff.device_rsec_idx = int.Parse(ddl_rsecidx_report_cutoff_detail.SelectedValue);

                        _data_report_cutoff.qa_cims_reportdevices_list[0] = _detail_report_cutoff;
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_searchdetail));
                        _data_report_cutoff = callServicePostQaDocCIMS(_urlCimsGetReportDetailExportDevices, _data_report_cutoff);

                        ViewState["vs_ReportDetailDevicesCutoff"] = _data_report_cutoff.qa_cims_reportdevices_list;

                        setGridData(gvReportDetailCutOff, ViewState["vs_ReportDetailDevicesCutoff"]);

                        break;
                }

                break;

            case "cmdExportExcelOnline":

                qa_cims_reportdevices_detail[] _templist_exportdetail_online = (qa_cims_reportdevices_detail[])ViewState["vs_ReportDetailDevicesOnline"];

                int countRowDevicesonline = 0;
                if (_templist_exportdetail_online != null)
                {

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_templist_exportdetail_online));
                    DataTable tableDevicesOnline = new DataTable();

                    tableDevicesOnline.Columns.Add("ชื่อเครื่องมือ", typeof(String));
                    tableDevicesOnline.Columns.Add("รหัสเครื่องมือ", typeof(String));
                    tableDevicesOnline.Columns.Add("หมายเลขเครื่อง", typeof(String));
                    tableDevicesOnline.Columns.Add("ผู้ถือครอง", typeof(String));
                    tableDevicesOnline.Columns.Add("วันที่สอบเทียบครั้งล่าสุด", typeof(String));
                    tableDevicesOnline.Columns.Add("วันที่สอบเทียบครั้งถัดไป", typeof(String));

                    foreach (var devicesOnlineTotal in _templist_exportdetail_online)
                    {
                        DataRow addDevicesOnlineTotalRow = tableDevicesOnline.NewRow();
                        addDevicesOnlineTotalRow[0] = devicesOnlineTotal.equipment_name.ToString();
                        addDevicesOnlineTotalRow[1] = devicesOnlineTotal.device_id_no.ToString();
                        addDevicesOnlineTotalRow[2] = devicesOnlineTotal.device_serial.ToString();
                        addDevicesOnlineTotalRow[3] = devicesOnlineTotal.device_rsec.ToString();
                        addDevicesOnlineTotalRow[4] = devicesOnlineTotal.device_cal_date.ToString();
                        addDevicesOnlineTotalRow[5] = devicesOnlineTotal.device_due_date.ToString();


                        tableDevicesOnline.Rows.InsertAt(addDevicesOnlineTotalRow, countRowDevicesonline++);
                    }
                    //WriteExcelWithNPOI(tableMaterialTotal, "xls", "report-material-total");
                    WriteExcelWithNPOI(tableDevicesOnline, "xls", "report-devices-online");
                }
                else
                {
                    //litDebug.Text = "33333";
                }

                break;
            case "cmdExportExcelOffline":

                qa_cims_reportdevices_detail[] _templist_exportdetail_offline = (qa_cims_reportdevices_detail[])ViewState["vs_ReportDetailDevicesOffline"];

                int countRowDevicesoffline = 0;
                if (_templist_exportdetail_offline != null)
                {

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_templist_exportdetail_online));
                    DataTable tableDevicesOffline = new DataTable();

                    tableDevicesOffline.Columns.Add("ชื่อเครื่องมือ", typeof(String));
                    tableDevicesOffline.Columns.Add("รหัสเครื่องมือ", typeof(String));
                    tableDevicesOffline.Columns.Add("หมายเลขเครื่อง", typeof(String));
                    tableDevicesOffline.Columns.Add("ผู้ถือครอง", typeof(String));
                    tableDevicesOffline.Columns.Add("วันที่สอบเทียบครั้งล่าสุด", typeof(String));
                    tableDevicesOffline.Columns.Add("วันที่สอบเทียบครั้งถัดไป", typeof(String));

                    foreach (var devicesOfflineTotal in _templist_exportdetail_offline)
                    {
                        DataRow addDevicesOfflineTotalRow = tableDevicesOffline.NewRow();
                        addDevicesOfflineTotalRow[0] = devicesOfflineTotal.equipment_name.ToString();
                        addDevicesOfflineTotalRow[1] = devicesOfflineTotal.device_id_no.ToString();
                        addDevicesOfflineTotalRow[2] = devicesOfflineTotal.device_serial.ToString();
                        addDevicesOfflineTotalRow[3] = devicesOfflineTotal.device_rsec.ToString();
                        addDevicesOfflineTotalRow[4] = devicesOfflineTotal.device_cal_date.ToString();
                        addDevicesOfflineTotalRow[5] = devicesOfflineTotal.device_due_date.ToString();


                        tableDevicesOffline.Rows.InsertAt(addDevicesOfflineTotalRow, countRowDevicesoffline++);
                    }
                    //WriteExcelWithNPOI(tableMaterialTotal, "xls", "report-material-total");
                    WriteExcelWithNPOI(tableDevicesOffline, "xls", "report-devices-offline");
                }
                else
                {
                    //litDebug.Text = "33333";
                }
                break;
            case "cmdExportExcelCutoff":

                qa_cims_reportdevices_detail[] _templist_exportdetail_cutoff = (qa_cims_reportdevices_detail[])ViewState["vs_ReportDetailDevicesCutoff"];

                int countRowDevicescutoff = 0;
                if (_templist_exportdetail_cutoff != null)
                {

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_templist_exportdetail_online));
                    DataTable tableDevicesCutoff = new DataTable();

                    tableDevicesCutoff.Columns.Add("ชื่อเครื่องมือ", typeof(String));
                    tableDevicesCutoff.Columns.Add("รหัสเครื่องมือ", typeof(String));
                    tableDevicesCutoff.Columns.Add("หมายเลขเครื่อง", typeof(String));
                    tableDevicesCutoff.Columns.Add("ผู้ถือครอง", typeof(String));
                    tableDevicesCutoff.Columns.Add("วันที่สอบเทียบครั้งล่าสุด", typeof(String));
                    tableDevicesCutoff.Columns.Add("วันที่สอบเทียบครั้งถัดไป", typeof(String));

                    foreach (var devicesCutoffTotal in _templist_exportdetail_cutoff)
                    {
                        DataRow addDevicesCutoffTotalRow = tableDevicesCutoff.NewRow();
                        addDevicesCutoffTotalRow[0] = devicesCutoffTotal.equipment_name.ToString();
                        addDevicesCutoffTotalRow[1] = devicesCutoffTotal.device_id_no.ToString();
                        addDevicesCutoffTotalRow[2] = devicesCutoffTotal.device_serial.ToString();
                        addDevicesCutoffTotalRow[3] = devicesCutoffTotal.device_rsec.ToString();
                        addDevicesCutoffTotalRow[4] = devicesCutoffTotal.device_cal_date.ToString();
                        addDevicesCutoffTotalRow[5] = devicesCutoffTotal.device_due_date.ToString();


                        tableDevicesCutoff.Rows.InsertAt(addDevicesCutoffTotalRow, countRowDevicescutoff++);
                    }
                    //WriteExcelWithNPOI(tableMaterialTotal, "xls", "report-material-total");
                    WriteExcelWithNPOI(tableDevicesCutoff, "xls", "report-devices-cutoff");
                }
                else
                {
                    //litDebug.Text = "33333";
                }
                break;

            case "cmdDelPerList":

                actionDelPermission(int.Parse(cmdArg));

                break;

            case "cmdSaveTranfer":

                int STARTNODETRANFER = int.Parse(cmdArg);

                //litDebug.Text = STARTNODETRANFER.ToString();

                ViewState["CheckChkbox"] = -1;

                if (gvListEquipment.Rows.Count > 0)
                {
                    //var _chkAll = (CheckBox)gvListEquipment.HeaderRow.FindControl("ChkEquipmentAll");

                    data_qa_cims _dataTranfer = new data_qa_cims();

                    _dataTranfer.qa_cims_u0_document_device_list = new qa_cims_u0_document_device_detail[1];
                    _dataTranfer.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
                    _dataTranfer.qa_cims_u2_document_device_list = new qa_cims_u2_document_device_detail[1];

                    qa_cims_u0_document_device_detail _dataU0 = new qa_cims_u0_document_device_detail();
                    qa_cims_u1_document_device_detail _chkItemsTranfer = new qa_cims_u1_document_device_detail();
                    qa_cims_u2_document_device_detail _addLogTranfer = new qa_cims_u2_document_device_detail();

                    //  qa_cims_u2_document_device_detail

                    _dataU0.org_idx_create = int.Parse(ViewState["org_permission"].ToString());
                    _dataU0.rdept_idx_create = int.Parse(ViewState["rdept_permission"].ToString());
                    _dataU0.rsec_idx_create = int.Parse(ViewState["rsec_permission"].ToString());
                    _dataU0.emp_idx_create = _emp_idx;
                    _dataU0.org_idx_tranfer = int.Parse(ddlOrgtranfer.SelectedValue);
                    _dataU0.rdept_idx_tranfer = int.Parse(ddlRdepttranfer.SelectedValue);
                    _dataU0.rsec_idx_tranfer = int.Parse(ddlRsectranfer.SelectedValue);
                    _dataU0.comment = txtcoment_tranfer.Text;
                    _dataU0.m0_actor_idx = 1;
                    _dataU0.m0_node_idx = STARTNODETRANFER;
                    _dataU0.decision_idx = 19; // create tranfer form user holder
                    _dataU0.place_idx = int.Parse(tbPlaceIDXInDocument.Text);//int.Parse(ddlPlaceTranfer.SelectedValue);//int.Parse(ViewState["vsPlaceIDX"].ToString());
                    _dataU0.place_idx_tranfer = int.Parse(ddlPlaceTranfer.SelectedValue);//int.Parse(ViewState["vsPlaceIDX"].ToString());

                    _dataTranfer.qa_cims_u0_document_device_list[0] = _dataU0;

                    _addLogTranfer.cemp_idx = _emp_idx;

                    _dataTranfer.qa_cims_u2_document_device_list[0] = _addLogTranfer;

                    if (ChkAll.Checked == true) // Check All Devices Tranfer ridViewRow row in GvExcel_Show_excel.Rows
                    {
                        var count_alert_u1excel1 = gvListEquipment.Rows.Count;
                        var u1_doc_tranfer_someAll1 = new qa_cims_u1_document_device_detail[gvListEquipment.Rows.Count];
                        int s_insert1 = 0;
                        foreach (GridViewRow row_insert_tranfer in gvListEquipment.Rows)
                        {
                            CheckBox ChkBoxRows_inserttranfer = (CheckBox)row_insert_tranfer.FindControl("ChkEquipment");
                            Label lblDeviceIDX_inserttranfer = (Label)row_insert_tranfer.FindControl("lblDeviceIDX");
                            Label lblEquipment_IDNO_inserttranfer = (Label)row_insert_tranfer.FindControl("lblEquipment_IDNO");
                            TextBox txt_Range_of_use_inserttranfer = (TextBox)row_insert_tranfer.FindControl("txt_Range_of_use");
                            TextBox txt_Poin_of_use_inserttranfer = (TextBox)row_insert_tranfer.FindControl("txt_Poin_of_use");

                            if (txt_Range_of_use_inserttranfer.Text == "" || txt_Poin_of_use_inserttranfer.Text == "")
                            {
                                if (txt_Range_of_use_inserttranfer.Text == "")
                                {
                                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('คุณยังไม่ได้ทำการกรอกข้อมูล ช่วงการใช้งาน !!');", true);
                                    txt_Range_of_use_inserttranfer.Focus();
                                }
                                else if (txt_Poin_of_use_inserttranfer.Text == "")
                                {
                                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('คุณยังไม่ได้ทำการกรอกข้อมูล จุดการใช้งาน !!');", true);
                                    txt_Poin_of_use_inserttranfer.Focus();
                                }

                            }
                            else
                            {
                                u1_doc_tranfer_someAll1[s_insert1] = new qa_cims_u1_document_device_detail();
                                u1_doc_tranfer_someAll1[s_insert1].m0_device_idx = int.Parse(lblDeviceIDX_inserttranfer.Text);
                                u1_doc_tranfer_someAll1[s_insert1].cemp_idx = _emp_idx;
                                u1_doc_tranfer_someAll1[s_insert1].range_of_use = txt_Range_of_use_inserttranfer.Text;
                                u1_doc_tranfer_someAll1[s_insert1].point_of_use = txt_Poin_of_use_inserttranfer.Text;

                            }

                            s_insert1++;

                        }

                        _dataTranfer.qa_cims_u1_document_device_list = u1_doc_tranfer_someAll1;
                        // litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataTranfer));
                        _dataTranfer = callServicePostQaDocCIMS(_urlCimsSetDocumentTranfer, _dataTranfer);
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(ViewState["CheckChkbox"]));
                        if (_dataTranfer.return_code == 0)
                        {
                            setActiveTab("docList", 0, 0, 0, 0, 0, 0);
                            setOntop.Focus();

                        }

                    }
                    else if (ChkAll.Checked == false)
                    {
                        var count_alert_u1excel = gvListEquipment.Rows.Count;
                        var u1_doc_tranfer_someChk = new qa_cims_u1_document_device_detail[gvListEquipment.Rows.Count];
                        int s_insert = 0;
                        foreach (GridViewRow row_insert_tranfer in gvListEquipment.Rows)
                        {
                            CheckBox ChkBoxRows_inserttranfer = (CheckBox)row_insert_tranfer.FindControl("ChkEquipment");
                            Label lblDeviceIDX_inserttranfer = (Label)row_insert_tranfer.FindControl("lblDeviceIDX");
                            Label lblEquipment_IDNO_inserttranfer = (Label)row_insert_tranfer.FindControl("lblEquipment_IDNO");
                            TextBox txt_Range_of_use_inserttranfer = (TextBox)row_insert_tranfer.FindControl("txt_Range_of_use");
                            TextBox txt_Poin_of_use_inserttranfer = (TextBox)row_insert_tranfer.FindControl("txt_Poin_of_use");

                            if (ChkBoxRows_inserttranfer.Checked)
                            {
                                if (txt_Range_of_use_inserttranfer.Text == "" || txt_Poin_of_use_inserttranfer.Text == "")
                                {

                                    if (txt_Range_of_use_inserttranfer.Text == "")
                                    {
                                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('คุณยังไม่ได้ทำการกรอกข้อมูล ช่วงการใช้งาน !!');", true);
                                        txt_Range_of_use_inserttranfer.Focus();
                                    }
                                    else if (txt_Poin_of_use_inserttranfer.Text == "")
                                    {
                                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('คุณยังไม่ได้ทำการกรอกข้อมูล จุดการใช้งาน !!');", true);
                                        txt_Poin_of_use_inserttranfer.Focus();
                                    }

                                }
                                else
                                {
                                    u1_doc_tranfer_someChk[s_insert] = new qa_cims_u1_document_device_detail();
                                    u1_doc_tranfer_someChk[s_insert].m0_device_idx = int.Parse(lblDeviceIDX_inserttranfer.Text);
                                    u1_doc_tranfer_someChk[s_insert].cemp_idx = _emp_idx;
                                    u1_doc_tranfer_someChk[s_insert].range_of_use = txt_Range_of_use_inserttranfer.Text;
                                    u1_doc_tranfer_someChk[s_insert].point_of_use = txt_Poin_of_use_inserttranfer.Text;
                                    ViewState["CheckChkbox"] = s_insert;

                                }

                            }

                            s_insert++;

                        }


                        if (int.Parse(ViewState["CheckChkbox"].ToString()) >= 0)
                        {
                            _dataTranfer.qa_cims_u1_document_device_list = u1_doc_tranfer_someChk;
                            //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataTranfer));
                            _dataTranfer = callServicePostQaDocCIMS(_urlCimsSetDocumentTranfer, _dataTranfer);

                            if (_dataTranfer.return_code == 0)
                            {

                                setActiveTab("docList", 0, 0, 0, 0, 0, 0);
                                setOntop.Focus();


                            }
                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาเลือกเครื่องมือที่จะทำการโอนย้าย!!');", true);
                        }

                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่มีข้อมูลเครื่องมือที่ถือครอง');", true);
                }

                break;

            case "cmdEditEquipment":

                //string cmdArgUserForUpdate = cmdArg;

                string[] arg4_editdevices = new string[2];
                arg4_editdevices = e.CommandArgument.ToString().Split(',');
                string cmdArgUserForUpdate = arg4_editdevices[0];
                string devices_no_edit = arg4_editdevices[1];

                div_SearchIndex_1.Visible = false;
                fv_SearchIndex.Visible = false;
                show_gvregistration.Visible = false;
                btnhiddensearch.Visible = false;
                TabSearch.Visible = false;

                divTranferRegistration.Visible = false;

                //btnResetSearchPage.Visible = true;

                setFormData(fvDetailUserInPageRegistration, FormViewMode.ReadOnly, ((data_employee)ViewState["vsEmpProfile"]).employee_list);

                data_qa_cims _data_Edit = new data_qa_cims();
                _data_Edit.qa_cims_m0_registration_device_list = new qa_cims_m0_registration_device[1];
                qa_cims_m0_registration_device _M0Edit = new qa_cims_m0_registration_device();
                _data_Edit.qa_cims_m0_registration_device_list[0] = _M0Edit;

                _data_Edit = callServicePostMasterQACIMS(_urlCimsGetRegistrationDevice, _data_Edit);

                var _linqSet = from data_qa_cims in _data_Edit.qa_cims_m0_registration_device_list
                               where data_qa_cims.m0_device_idx == int.Parse(cmdArgUserForUpdate)
                               select data_qa_cims;

                setFormData(fvInsertCIMS, FormViewMode.Edit, _linqSet.ToList());

                TextBox txt_location_edit = (TextBox)fvInsertCIMS.FindControl("txt_location_edit");
                TextBox txt_zone_edit = (TextBox)fvInsertCIMS.FindControl("txt_zone_edit");
                //TextBox tbFormStatusidx_Edit = (TextBox)fvInsertCIMS.FindControl("tbFormStatusidx_Edit");


                getM0Location((DropDownList)fvInsertCIMS.FindControl("ddl_location"), txt_location_edit.Text);
                getZoneEditList((DropDownList)fvInsertCIMS.FindControl("ddl_Zone"), txt_location_edit.Text, txt_zone_edit.Text);
                //getStatusDevicesList((DropDownList)fvInsertCIMS.FindControl("ddlFormStatusEdit"), tbFormStatusidx_Edit.Text);


                ViewState["vsM0DeviceIDX"] = int.Parse(cmdArgUserForUpdate);

                SelectCalibration_point(cmdArgUserForUpdate);
                selectResolution(cmdArgUserForUpdate);
                selectgvRangeUpdate(cmdArgUserForUpdate);
                selectgvMeasuringUpdate(cmdArgUserForUpdate);
                selectgvAcceptanceUpdate(cmdArgUserForUpdate);


                getResolution((DropDownList)fvInsertCIMS.FindControl("ddlResolutionUpdate"), "0");

                //Range Of Use
                getM0Unit((DropDownList)fvInsertCIMS.FindControl("ddl_range_of_use_unitUpdate"), "0");


                //Measuring Range
                getM0Unit((DropDownList)fvInsertCIMS.FindControl("ddlMeasuring_Update"), "0");

                //Acceptance Criteria
                getM0Unit((DropDownList)fvInsertCIMS.FindControl("ddlAcceptance_Update"), "0");

                var _Frequencyidx = (Label)fvInsertCIMS.FindControl("lblFrequencyidx_update");
                getFrequency((DropDownList)fvInsertCIMS.FindControl("ddlFrequencyUpdate"), _Frequencyidx.Text);


                getM0Unit((DropDownList)fvInsertCIMS.FindControl("ddlUnitCPEdit"), "0");
                var _IDXEquipmentNameView = (TextBox)fvInsertCIMS.FindControl("tbEquipmentName_Edit");
                getEquipmentNameEdit((DropDownList)fvInsertCIMS.FindControl("ddlEquipmentNameEdit"), _IDXEquipmentNameView.Text);
                var _IDXBrandNameView = (TextBox)fvInsertCIMS.FindControl("tbBrand_Edit");
                getBrandNameEdit((DropDownList)fvInsertCIMS.FindControl("ddlBrandEdit"), _IDXBrandNameView.Text);
                var _IDXEquipmentTypeView = (TextBox)fvInsertCIMS.FindControl("tbEquipType_Edit");
                getEquipmentTypeEdit((DropDownList)fvInsertCIMS.FindControl("ddlEquipTypeEdit"), _IDXEquipmentTypeView.Text);
                var _IDXUnitCalTypeView = (TextBox)fvInsertCIMS.FindControl("tbcaltype_Edit");
                getCalTypeEdit((DropDownList)fvInsertCIMS.FindControl("dllcaltypeEdit"), _IDXUnitCalTypeView.Text);
                var _IDXOrgView = (TextBox)fvInsertCIMS.FindControl("tbOrg_excel_Edit");
                getOrgEdit((DropDownList)fvInsertCIMS.FindControl("ddlOrg_excelEdit"), _IDXOrgView.Text);
                var _IDXDeptView = (TextBox)fvInsertCIMS.FindControl("tbDept_excel_Edit");
                getDeptEdit((DropDownList)fvInsertCIMS.FindControl("ddlDept_excelEdit"), _IDXOrgView.Text, _IDXDeptView.Text);
                var _IDXSecView = (TextBox)fvInsertCIMS.FindControl("tbSec_excel_Edit");
                getSecEdit((DropDownList)fvInsertCIMS.FindControl("ddlSec_excelEdit"), _IDXOrgView.Text, _IDXDeptView.Text, _IDXSecView.Text);

                //status devices
                var _IDXStatusView = (TextBox)fvInsertCIMS.FindControl("tbFormStatusidx_Edit");
                getStatusDevicesList((DropDownList)fvInsertCIMS.FindControl("ddlFormStatusEdit"), _IDXStatusView.Text);


                var _IDXPlaceView = (TextBox)fvInsertCIMS.FindControl("tbplaceMachineEdit");
                getSelectPlace((DropDownList)fvInsertCIMS.FindControl("ddlPlaceMachineEdit"), _IDXPlaceView.Text, "0");


                HyperLink btnViewFileAttachment_edit = (HyperLink)fvInsertCIMS.FindControl("btnViewFileAttachment");
                TextBox txt_detailnotfile_edit = (TextBox)fvInsertCIMS.FindControl("txt_detailnotfile");

                string filePath_viewedit = ConfigurationManager.AppSettings["pathfile_devices_certificate"];
                string directoryName_edit = devices_no_edit; //+ ViewState["vs_date_document_pahtfile"].ToString();


                if (Directory.Exists(Server.MapPath(filePath_viewedit + directoryName_edit)))
                {

                    string[] filesPath_viewedit = Directory.GetFiles(Server.MapPath(filePath_viewedit + directoryName_edit));
                    List<ListItem> files = new List<ListItem>();
                    foreach (string path in filesPath_viewedit)
                    {
                        string getfiles = "";
                        getfiles = Path.GetFileName(path);

                        btnViewFileAttachment_edit.NavigateUrl = filePath_viewedit + directoryName_edit + "/" + getfiles;

                        //litDebug.Text = filePath_view + directoryName + "/" + getfiles;
                    }
                    btnViewFileAttachment_edit.Visible = true;
                    txt_detailnotfile_edit.Visible = false;
                }
                else
                {
                    btnViewFileAttachment_edit.Visible = false;
                    txt_detailnotfile_edit.Visible = true;
                }
                break;

            case "btnTodelete_CPUnit_Edit":
                // ViewState["DELETE_PLACE"] = int.Parse(cmdArg);

                int a = int.Parse(cmdArg);
                data_qa_cims m1_regis_de = new data_qa_cims();
                qa_cims_m1_registration_device m1_regis_sde = new qa_cims_m1_registration_device();

                m1_regis_de.qa_cims_m1_registration_device_list = new qa_cims_m1_registration_device[1];

                m1_regis_sde.m1_device_idx = int.Parse(a.ToString());

                m1_regis_de.qa_cims_m1_registration_device_list[0] = m1_regis_sde;
                //  test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_de));
                m1_regis_de = callServicePostMasterQACIMS(_urlCimsDeleteM1RegistrationDevice, m1_regis_de);

                SelectCalibration_point(ViewState["vsM0DeviceIDX"].ToString());

                break;

            case "cmdUpdate":

                DropDownList ddlEquipmentNameEdit = (DropDownList)fvInsertCIMS.FindControl("ddlEquipmentNameEdit");
                DropDownList ddlBrandEdit = (DropDownList)fvInsertCIMS.FindControl("ddlBrandEdit");
                DropDownList ddlEquipTypeEdit = (DropDownList)fvInsertCIMS.FindControl("ddlEquipTypeEdit");
                TextBox txtserialnoEdit = (TextBox)fvInsertCIMS.FindControl("txtserialnoEdit");
                TextBox txtROUstartEdit = (TextBox)fvInsertCIMS.FindControl("txtROUstartEdit");
                TextBox txtROUendEdit = (TextBox)fvInsertCIMS.FindControl("txtROUendEdit");
                DropDownList ddlUnitROUEdit = (DropDownList)fvInsertCIMS.FindControl("ddlUnitROUEdit");
                TextBox txtequipmentNoEdit = (TextBox)fvInsertCIMS.FindControl("txtequipmentNoEdit");
                TextBox txtresolutionEdit = (TextBox)fvInsertCIMS.FindControl("txtresolutionEdit");
                DropDownList ddlUnitResEdit = (DropDownList)fvInsertCIMS.FindControl("ddlUnitResEdit");
                TextBox txtMRstartEdit = (TextBox)fvInsertCIMS.FindControl("txtMRstartEdit");
                TextBox txtMRendEdit = (TextBox)fvInsertCIMS.FindControl("txtMRendEdit");
                DropDownList ddlUnitMREdit = (DropDownList)fvInsertCIMS.FindControl("ddlUnitMREdit");
                TextBox txtACEdit = (TextBox)fvInsertCIMS.FindControl("txtACEdit");
                DropDownList ddlUnitACEdit = (DropDownList)fvInsertCIMS.FindControl("ddlUnitACEdit");
                TextBox txtCFEdit = (TextBox)fvInsertCIMS.FindControl("txtCFEdit");
                DropDownList dllcaltypeEdit = (DropDownList)fvInsertCIMS.FindControl("dllcaltypeEdit");
                TextBox txtequipmentdetailEdit = (TextBox)fvInsertCIMS.FindControl("txtequipmentdetailEdit");

                TextBox txtreceive_devices_update = (TextBox)fvInsertCIMS.FindControl("txtreceive_devices_update");

                HiddenField HiddenCalDateEdit = (HiddenField)fvInsertCIMS.FindControl("HiddenCalDateEdit");
                HiddenField HiddenDueDateEdit = (HiddenField)fvInsertCIMS.FindControl("HiddenDueDateEdit");

                TextBox txt_cal_dateEdit = (TextBox)fvInsertCIMS.FindControl("txt_cal_date");
                TextBox txt_due_dateEdit = (TextBox)fvInsertCIMS.FindControl("txt_due_date");


                DropDownList ddlOrgEdit = (DropDownList)fvInsertCIMS.FindControl("ddlOrg_excelEdit");
                DropDownList ddlDeptEdit = (DropDownList)fvInsertCIMS.FindControl("ddlDept_excelEdit");
                DropDownList ddlSecEdit = (DropDownList)fvInsertCIMS.FindControl("ddlSec_excelEdit");
                DropDownList ddlFormStatusEdit = (DropDownList)fvInsertCIMS.FindControl("ddlFormStatusEdit");
                DropDownList ddlPlaceMachineEdit = (DropDownList)fvInsertCIMS.FindControl("ddlPlaceMachineEdit");

                Label lblOldIDNo = (Label)fvInsertCIMS.FindControl("lblOldIDNo");
                Label lbltxtEditShowIDnumber = (Label)fvInsertCIMS.FindControl("lbltxtEditShowIDnumber");
                Label lblOldSerialNo = (Label)fvInsertCIMS.FindControl("lblOldSerialNo");
                Label lbltxtEditShowSerialnumber = (Label)fvInsertCIMS.FindControl("lbltxtEditShowSerialnumber");

                DropDownList ddlFrequencyUpdate = (DropDownList)fvInsertCIMS.FindControl("ddlFrequencyUpdate");
                Label lbl_m3_device_frequency_idx = (Label)fvInsertCIMS.FindControl("lbl_m3_device_frequency_idx");
                GridView GvCalPointEdit = (GridView)fvInsertCIMS.FindControl("GvCalPointEdit");
                GridView gvResolutionUpdate = (GridView)fvInsertCIMS.FindControl("gvResolutionUpdate");
                GridView gvRangeUpdate = (GridView)fvInsertCIMS.FindControl("gvRangeUpdate");
                GridView gvMeasuringUpdate = (GridView)fvInsertCIMS.FindControl("gvMeasuringUpdate");
                GridView gvAcceptanceUpdate = (GridView)fvInsertCIMS.FindControl("gvAcceptanceUpdate");

                DropDownList ddl_location_edit = (DropDownList)fvInsertCIMS.FindControl("ddl_location");
                DropDownList ddl_Zone = (DropDownList)fvInsertCIMS.FindControl("ddl_Zone");
                DropDownList ddlFormStatus_edit = (DropDownList)fvInsertCIMS.FindControl("ddlFormStatusEdit");

                //litDebug.Text = gvAcceptanceUpdate.Rows.Count.ToString();

                //if ((lbltxtEditShowIDnumber.Text == "<i class='fa fa-check' aria-hidden='true'></i>  " + "สามารถใช้เลขรหัสเครื่องมือหมายเลขนี้ได้!!!" || lbltxtEditShowIDnumber.Text == "") &&
                //    (lbltxtEditShowSerialnumber.Text == "<i class='fa fa-check' aria-hidden='true'></i>  " + "สามารถใช้เลขหมายเลขเครื่องนี้ได้!!!" || lbltxtEditShowSerialnumber.Text == "") &&
                //    GvCalPointEdit.Rows.Count > 0 && gvResolutionUpdate.Rows.Count > 0 && gvRangeUpdate.Rows.Count > 0 && gvMeasuringUpdate.Rows.Count > 0 && gvAcceptanceUpdate.Rows.Count > 0)
                if ((lbltxtEditShowIDnumber.Text == "<i class='fa fa-check' aria-hidden='true'></i>  " + "สามารถใช้เลขรหัสเครื่องมือหมายเลขนี้ได้!!!" || lbltxtEditShowIDnumber.Text == "") &&
                    (lbltxtEditShowSerialnumber.Text == "<i class='fa fa-check' aria-hidden='true'></i>  " + "สามารถใช้เลขหมายเลขเครื่องนี้ได้!!!" || lbltxtEditShowSerialnumber.Text == "") &&
                    GvCalPointEdit.Rows.Count > 0 && gvResolutionUpdate.Rows.Count > 0 && gvRangeUpdate.Rows.Count > 0 && gvMeasuringUpdate.Rows.Count > 0 && gvAcceptanceUpdate.Rows.Count > 0)
                {

                    _data_qa_cims.qa_cims_m0_registration_device_list = new qa_cims_m0_registration_device[1];
                    qa_cims_m0_registration_device _m0_Form_Edit = new qa_cims_m0_registration_device();
                    _m0_Form_Edit.m0_device_idx = int.Parse(ViewState["vsM0DeviceIDX"].ToString());
                    _m0_Form_Edit.device_serial = txtserialnoEdit.Text;
                    _m0_Form_Edit.device_org_idx = int.Parse(ddlOrgEdit.SelectedValue);
                    _m0_Form_Edit.device_rdept_idx = int.Parse(ddlDeptEdit.SelectedValue);
                    _m0_Form_Edit.device_rsec_idx = int.Parse(ddlSecEdit.SelectedValue);

                    //_m0_Form_Edit.device_cal_date = HiddenCalDateEdit.Value;
                    //_m0_Form_Edit.device_due_date = HiddenDueDateEdit.Value;

                    _m0_Form_Edit.device_cal_date = txt_cal_dateEdit.Text;
                    _m0_Form_Edit.device_due_date = txt_due_dateEdit.Text;

                    _m0_Form_Edit.m0_location_idx = int.Parse(ddl_location_edit.SelectedValue);

                    if (ddl_Zone.SelectedValue == "0")
                    {
                        _m0_Form_Edit.m1_location_idx = 0;
                    }
                    else
                    {
                        _m0_Form_Edit.m1_location_idx = int.Parse(ddl_Zone.SelectedValue);
                    }

                    _m0_Form_Edit.m0_status_idx = int.Parse(ddlFormStatus_edit.SelectedValue);



                    _m0_Form_Edit.device_status = int.Parse(ddlFormStatusEdit.SelectedValue);
                    _m0_Form_Edit.emp_idx_create = _emp_idx;
                    _m0_Form_Edit.org_idx_create = int.Parse(ViewState["org_permission"].ToString());
                    _m0_Form_Edit.rdept_idx_create = int.Parse(ViewState["rdept_permission"].ToString());
                    _m0_Form_Edit.rsec_idx_create = int.Parse(ViewState["rsec_permission"].ToString());
                    _m0_Form_Edit.brand_idx = int.Parse(ddlBrandEdit.SelectedValue);
                    _m0_Form_Edit.equipment_idx = int.Parse(ddlEquipmentNameEdit.SelectedValue);
                    _m0_Form_Edit.device_id_no = txtequipmentNoEdit.Text;
                    _m0_Form_Edit.receive_devices = txtreceive_devices_update.Text;
                    _m0_Form_Edit.equipment_type_idx = int.Parse(ddlEquipTypeEdit.SelectedValue);
                    _m0_Form_Edit.equipment_frequency_idx = int.Parse(ddlFrequencyUpdate.SelectedValue);
                    _m0_Form_Edit.m3_device_frequency_idx = int.Parse(lbl_m3_device_frequency_idx.Text);
                    _m0_Form_Edit.device_details = txtequipmentdetailEdit.Text;
                    _m0_Form_Edit.cal_type_idx = int.Parse(dllcaltypeEdit.SelectedValue);
                    //_m0_Form_Edit.calibration_frequency = Convert.ToDecimal(txtCFEdit.Text);
                    _m0_Form_Edit.place_idx = int.Parse(ddlPlaceMachineEdit.SelectedValue);


                    _data_qa_cims.qa_cims_m0_registration_device_list[0] = _m0_Form_Edit;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_qa_cims));

                    _data_qa_cims = callServicePostMasterQACIMS(_urlCimsSetM0RegistrationDevice, _data_qa_cims);

                    //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('แก้ไขข้อมูลทะเบียนเครื่องมือสำเร็จ !!!');", true);

                    setFormData(fvInsertCIMS, FormViewMode.ReadOnly, null);


                    divRegistrationList.Visible = true;

                    div_SearchIndex_1.Visible = true;

                    viewRegistration.Visible = false;
                    divTranferRegistration.Visible = false;
                    btnhiddensearch.Visible = false;
                    TabSearch.Visible = false;
                    fv_SearchIndex.Visible = true;

                    setFormData(fvOwner, FormViewMode.ReadOnly, null);
                    setFormData(fvEquipmentDetail, FormViewMode.ReadOnly, null);

                    getM0CalType((DropDownList)docRegistration.FindControl("dllcaltype_search"), tbLabIDXInDocument.Text);
                    getSelectPlace((DropDownList)docRegistration.FindControl("ddlPlaceMachine_search"), tbPlaceIDXInDocument.Text, "0");

                    show_gvregistration.Visible = true;
                    //setGridData(gvRegistrationList, ViewState["vsRegistrationDevice"]);

                    setActiveTab("docRegistration", 0, 0, 0, 0, 0, 0);

                    setOntop.Focus();
                }
                else
                {
                    if (lbltxtEditShowIDnumber.Text == "<i class='fa fa-times' aria-hidden='true'></i>  " + "มีรหัสเครื่อง  " + txtequipmentNoEdit.Text + " นี้แล้ว กรุณาทำการกรอกรหัสเครื่องใหม่!!!")
                    {

                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาทำการกรอกรหัสเครื่องใหม่ที่ไม่ซ้ำก่อนทำการบันทึก!!!');", true);
                        break;
                    }
                    else if (lbltxtEditShowSerialnumber.Text == "<i class='fa fa-times' aria-hidden='true'></i>  " + "มีหมายเลขเครื่อง  " + txtserialnoEdit.Text + " นี้แล้ว กรุณาทำการกรอกหมายเลขเครื่องใหม่!!!")
                    {

                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาทำการกรอกหมายเลขเครื่องใหม่ที่ไม่ซ้ำก่อนทำการบันทึก!!!');", true);
                        break;
                    }
                    else if (GvCalPointEdit.Rows.Count == 0 || gvResolutionUpdate.Rows.Count == 0 || gvRangeUpdate.Rows.Count == 0 || gvMeasuringUpdate.Rows.Count == 0
                        || gvAcceptanceUpdate.Rows.Count == 0)
                    {


                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณากรอกข้อมูลให้ครบถ้วน!!!');", true);
                        break;
                    }
                }

                break;

            case "cmdViewEquipment":

                //string cmdArgUserForView = cmdArg;

                string[] arg1_file = new string[2];
                arg1_file = e.CommandArgument.ToString().Split(',');
                string cmdArgUserForView = arg1_file[0];
                string devices_no = arg1_file[1];

                //litDebug.Text = devices_no.ToString();

                setFormData(fvDetailUserInPageRegistration, FormViewMode.ReadOnly, ((data_employee)ViewState["vsEmpProfile"]).employee_list);

                divTranferRegistration.Visible = true;
                divRegistrationList.Visible = false;
                div_SearchIndex_1.Visible = false;
                viewRegistration.Visible = true;
                btnhiddensearch.Visible = false;

                TabSearch.Visible = false;
                var btnBack = (LinkButton)fvOwner.FindControl("btnBack");


                data_qa_cims _data_View = new data_qa_cims();
                _data_View.qa_cims_m0_registration_device_list = new qa_cims_m0_registration_device[1];
                qa_cims_m0_registration_device _M0View = new qa_cims_m0_registration_device();
                _data_View.qa_cims_m0_registration_device_list[0] = _M0View;

                _data_View = callServicePostMasterQACIMS(_urlCimsGetRegistrationDevice, _data_View);

                var _linqSetView = from data_qa_cims in _data_View.qa_cims_m0_registration_device_list
                                   where data_qa_cims.m0_device_idx == int.Parse(cmdArgUserForView)
                                   select data_qa_cims;

                log_register.Visible = true;
                selectLogRegistrationDevice(cmdArgUserForView);


                setFormData(fvOwner, FormViewMode.ReadOnly, _linqSetView.ToList());
                setFormData(fvEquipmentDetail, FormViewMode.ReadOnly, _linqSetView.ToList());

                //FormView fvEquipmentDetail = (FormView)docRegistration.FindControl("fvEquipmentDetail");
                HyperLink btnViewFileAttachment = (HyperLink)fvEquipmentDetail.FindControl("btnViewFileAttachment");
                TextBox txt_detailnotfile = (TextBox)fvEquipmentDetail.FindControl("txt_detailnotfile");

                selectgvRangePeview(cmdArgUserForView);
                ViewState["vsM0DeviceIDX"] = int.Parse(cmdArgUserForView);


                string filePath_view = ConfigurationManager.AppSettings["pathfile_devices_certificate"];

                string directoryName = devices_no; //+ ViewState["vs_date_document_pahtfile"].ToString();


                if (Directory.Exists(Server.MapPath(filePath_view + directoryName)))
                {

                    //litDebug.Text = "222";//filePath_view;

                    string[] filesPath_view = Directory.GetFiles(Server.MapPath(filePath_view + directoryName));
                    List<ListItem> files = new List<ListItem>();
                    foreach (string path in filesPath_view)
                    {
                        string getfiles = "";
                        getfiles = Path.GetFileName(path);

                        btnViewFileAttachment.NavigateUrl = filePath_view + directoryName + "/" + getfiles;

                        //litDebug.Text = filePath_view + directoryName + "/" + getfiles;
                    }
                    btnViewFileAttachment.Visible = true;
                    txt_detailnotfile.Visible = false;
                }
                else
                {
                    btnViewFileAttachment.Visible = false;
                    txt_detailnotfile.Visible = true;
                }


                break;

            case "cmdback":

                divRegistrationList.Visible = true;

                fv_SearchIndex.Visible = true;
                btnsearchshow.Visible = true;

                div_SearchIndex_1.Visible = true;

                viewRegistration.Visible = false;
                divTranferRegistration.Visible = false;
                btnhiddensearch.Visible = false;
                TabSearch.Visible = false;

                setFormData(fvOwner, FormViewMode.ReadOnly, null);
                setFormData(fvEquipmentDetail, FormViewMode.ReadOnly, null);

                //show_gvregistration.Visible = true;
                //ViewState["vsRegistrationDevice"] = data_SearchDetail.qa_cims_search_registration_device_list;

                getM0CalType((DropDownList)docRegistration.FindControl("dllcaltype_search"), tbLabIDXInDocument.Text);
                getSelectPlace((DropDownList)docRegistration.FindControl("ddlPlaceMachine_search"), tbPlaceIDXInDocument.Text, "0");

                ClearGvRegisterDevices(tbPlaceIDXInDocument.Text, tbLabIDXInDocument.Text);
                //setGridData(gvRegistrationList, ViewState["vsRegistrationDevice"]);

                setOntop.Focus();
                break;

            case "cmdCutOffEquipment":

                tbSearchIDNoCutoff.Text = String.Empty;
                tbSearchIDNoCutoff.Enabled = true;
                ChkAllCutoff.Checked = false;
                setFormData(fvDetailUserInPageRegistration, FormViewMode.ReadOnly, ((data_employee)ViewState["vsEmpProfile"]).employee_list);
                linkBtnTrigger(btnSearchTool);
                divTranferRegistration.Visible = true;
                divRegistrationList.Visible = false;
                divCutoffRegistrationList.Visible = true;
                ChkAllCutoff.Checked = false;
                btnhiddensearch.Visible = false;
                div_SearchIndex_1.Visible = false;

                divAlertCutoffSeacrh.Visible = false;

                //btnsearchshow.Visible = true;
                //btnResetSearchPage.Visible = true;
                TabSearch.Visible = false;
                lst = null;
                RepeaterCutoff.DataSource = lst;
                RepeaterCutoff.DataBind();

                string[] cmdArgCutoff = cmdArg.Split(',');
                int rsec_idx_from_Cutoff = int.TryParse(cmdArgCutoff[0].ToString(), out _default_int) ? int.Parse(cmdArgCutoff[0].ToString()) : _default_int;
                int place_idx_Cutoff = int.TryParse(cmdArgCutoff[1].ToString(), out _default_int) ? int.Parse(cmdArgCutoff[1].ToString()) : _default_int;

                ViewState["vsPlaceIDX_Cutoff"] = place_idx_Cutoff;

                data_qa_cims _dataqaChecktoolCutoff = new data_qa_cims();
                _dataqaChecktoolCutoff.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
                qa_cims_u1_document_device_detail _selectDocumentCutoff = new qa_cims_u1_document_device_detail();
                _selectDocumentCutoff.condition = 5;
                _dataqaChecktoolCutoff.qa_cims_u1_document_device_list[0] = _selectDocumentCutoff;
                _dataqaChecktoolCutoff = callServicePostQaDocCIMS(_urlCimsGetEquipmentList, _dataqaChecktoolCutoff);

                string _listStatusTool_M0deviceIDX_Cutoff = "";

                if (_dataqaChecktoolCutoff.qa_cims_u1_document_device_list != null)
                {
                    for (int CheckStatusTool = 0; CheckStatusTool < _dataqaChecktoolCutoff.qa_cims_u1_document_device_list.Count(); CheckStatusTool++)
                    {
                        _listStatusTool_M0deviceIDX_Cutoff += _dataqaChecktoolCutoff.qa_cims_u1_document_device_list[CheckStatusTool].m0_device_idx.ToString() + ",";
                    }

                }

                data_qa_cims _dataSelectToolCutoff = new data_qa_cims();
                _dataSelectToolCutoff.qa_cims_m0_registration_device_list = new qa_cims_m0_registration_device[1];
                qa_cims_m0_registration_device _selectCutoff = new qa_cims_m0_registration_device();
                _selectCutoff.device_rsec_idx = rsec_idx_from_Cutoff;//int.Parse(ViewState["rsec_permission"].ToString()); ;
                _selectCutoff.list_m0_device = _listStatusTool_M0deviceIDX_Cutoff;
                _selectCutoff.condition = 1;
                _selectCutoff.place_idx = int.Parse(tbPlaceIDXInDocument.Text);
                _selectCutoff.cal_type_idx = int.Parse(tbLabIDXInDocument.Text);

                _dataSelectToolCutoff.qa_cims_m0_registration_device_list[0] = _selectCutoff;

                _dataSelectToolCutoff = callServicePostMasterQACIMS(_urlCimsGetRegistrationDevice, _dataSelectToolCutoff);

                ViewState["vsEquipmentListHolderCutoff"] = _dataSelectToolCutoff.qa_cims_m0_registration_device_list;
                setGridData(gvListEquipmentCutoff, ViewState["vsEquipmentListHolderCutoff"]);

                gvListEquipmentCutoff.Visible = true;

                data_qa_cims _dataCimsDecisionNodeCutoff = new data_qa_cims();
                _dataCimsDecisionNodeCutoff.qa_cims_bindnode_decision_list = new qa_cims_bindnode_decision_detail[1];
                qa_cims_bindnode_decision_detail _selectDecisionNodeCutoff = new qa_cims_bindnode_decision_detail();
                _dataCimsDecisionNodeCutoff.qa_cims_bindnode_decision_list[0] = _selectDecisionNodeCutoff;

                _dataCimsDecisionNodeCutoff = callServicePostQaDocCIMS(_urlCimsGetDecisionCutoff, _dataCimsDecisionNodeCutoff);
                setRepeaterData(rptActionCutoff, _dataCimsDecisionNodeCutoff.qa_cims_bindnode_decision_list);

                tbSearchIDNoCutoff.Focus();
                break;

            case "cmdSaveCutoff":

                int STARTNODECUTOFF = int.Parse(cmdArg);

                //litDebug.Text += STARTNODECUTOFF;
                ViewState["CheckRpt"] = -1;

                if (gvListEquipmentCutoff.Rows.Count > 0)
                {
                    _data_qa_cims.qa_cims_u0_document_device_list = new qa_cims_u0_document_device_detail[1];
                    qa_cims_u0_document_device_detail _u0_Cutoff_Insert = new qa_cims_u0_document_device_detail();

                    _u0_Cutoff_Insert.org_idx_create = int.Parse(ViewState["org_permission"].ToString());
                    _u0_Cutoff_Insert.rdept_idx_create = int.Parse(ViewState["rdept_permission"].ToString());
                    _u0_Cutoff_Insert.rsec_idx_create = int.Parse(ViewState["rsec_permission"].ToString());
                    _u0_Cutoff_Insert.emp_idx_create = _emp_idx;
                    _u0_Cutoff_Insert.m0_actor_idx = 2;
                    _u0_Cutoff_Insert.m0_node_idx = STARTNODECUTOFF;
                    _u0_Cutoff_Insert.decision_idx = 29;
                    _u0_Cutoff_Insert.place_idx = int.Parse(ViewState["vsPlaceIDX_Cutoff"].ToString());
                    //_u0_Cutoff_Insert.comment = txtcomment_cutoff.Text;


                    qa_cims_m0_registration_device[] _u0tempListEquipmentHolderCutoff_Save = (qa_cims_m0_registration_device[])ViewState["vsEquipmentListHolderCutoff"];

                    var u1_doc_cutoff_someAll_Save = new qa_cims_u1_document_device_detail[_u0tempListEquipmentHolderCutoff_Save.Count()];
                    int count_someAll_cutoff = 0;

 
                    //in to data base
                    foreach (RepeaterItem item in RepeaterCutoff.Items)
                    {
                        int count = 0;

                        Label lbM0Cutoff = (Label)item.FindControl("lbM0Cutoff");
                        Label lbIDNOCutoff = (Label)item.FindControl("lbIDNOCutoff");
                        FileUpload uploadFileDocument = (FileUpload)item.FindControl("FileUpload2");
                        TextBox txtcomment_cutoff = (TextBox)item.FindControl("txtcomment_cutoff");


                        //if (uploadFileDocument.HasFile)
                        //{

                        //    string getPathfile_equipmrnt = ConfigurationManager.AppSettings["pathfile_document_cutoff"];
                        //    string id_equipment_code = lbIDNOCutoff.Text + docdate;
                        //    string fileName_upload = id_equipment_code;

                        //    string filePath_upload = Server.MapPath(getPathfile_equipmrnt + id_equipment_code);

                        //    if (!Directory.Exists(filePath_upload))
                        //    {
                        //        Directory.CreateDirectory(filePath_upload);
                        //    }
                        //    string extension = Path.GetExtension(uploadFileDocument.FileName);

                        //    //litDebug.Text += ((getPathfile_equipmrnt + id_equipment_code) + "\\" + fileName_upload + extension + " //END// ");

                        //    uploadFileDocument.SaveAs(Server.MapPath(getPathfile_equipmrnt + id_equipment_code) + "\\" + fileName_upload + extension);

                        //}

                        u1_doc_cutoff_someAll_Save[count_someAll_cutoff] = new qa_cims_u1_document_device_detail();
                        u1_doc_cutoff_someAll_Save[count_someAll_cutoff].m0_device_idx = int.Parse(lbM0Cutoff.Text);
                        u1_doc_cutoff_someAll_Save[count_someAll_cutoff].cemp_idx = _emp_idx;
                        if (txtcomment_cutoff.Text == "")
                        {
                            u1_doc_cutoff_someAll_Save[count_someAll_cutoff].comment = "-";
                        }
                        else
                        {
                            u1_doc_cutoff_someAll_Save[count_someAll_cutoff].comment = txtcomment_cutoff.Text;
                        }
                        ViewState["CheckRpt"] = count_someAll_cutoff;
                        count_someAll_cutoff++;

                        count++;
                    }

                    if (int.Parse(ViewState["CheckRpt"].ToString()) >= 0)
                    {
                        _data_qa_cims.qa_cims_u0_document_device_list[0] = _u0_Cutoff_Insert;
                        _data_qa_cims.qa_cims_u1_document_device_list = u1_doc_cutoff_someAll_Save;
                        _data_qa_cims = callServicePostQaDocCIMS(_urlCimsSetDocumentTranfer, _data_qa_cims);
                        //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_qa_cims));
                        if (_data_qa_cims.return_code == 0)
                        {
                            //have file
                            foreach (RepeaterItem item in RepeaterCutoff.Items)
                            {
                                int count_file = 0;

                                Label lbM0Cutoff = (Label)item.FindControl("lbM0Cutoff");
                                Label lbIDNOCutoff = (Label)item.FindControl("lbIDNOCutoff");
                                FileUpload uploadFileDocument = (FileUpload)item.FindControl("FileUpload2");
                                TextBox txtcomment_cutoff = (TextBox)item.FindControl("txtcomment_cutoff");


                                if (uploadFileDocument.HasFile)
                                {

                                    string getPathfile_equipmrnt = ConfigurationManager.AppSettings["pathfile_document_cutoff"];
                                    string id_equipment_code = lbIDNOCutoff.Text + _data_qa_cims.return_msg;
                                    string fileName_upload = id_equipment_code;

                                    string filePath_upload = Server.MapPath(getPathfile_equipmrnt + id_equipment_code);

                                    if (!Directory.Exists(filePath_upload))
                                    {
                                        Directory.CreateDirectory(filePath_upload);
                                    }
                                    string extension = Path.GetExtension(uploadFileDocument.FileName);

                                    //litDebug.Text += ((getPathfile_equipmrnt + id_equipment_code) + "\\" + fileName_upload + extension + " //END// ");

                                    uploadFileDocument.SaveAs(Server.MapPath(getPathfile_equipmrnt + id_equipment_code) + "\\" + fileName_upload + extension);

                                }

                                count_file++;
                            }


                            if (tbValuePlacecims.Text == "" || tbValuePlacecims.Text == "0")
                            {
                                setActiveTab("docList", 0, 0, 0, 0, 0, 0);
                                setOntop.Focus();
                            }
                            else
                            {
                                setActiveTab("docList", 0, 0, 0, int.Parse(tbValuePlacecims.Text), 0, 0);
                                setOntop.Focus();
                            }

                            //setActiveTab("docRegistration", 0, 0, 0, 0, 0, 0);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาเลือกเครื่องมือที่ต้องการตัดเสียก่อน!!!');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่มีข้อมูลเครื่องมือที่ถือครอง!!!');", true);
                }

                break;

            case "cmdHeadSectionApprove":

                string[] cmdArgDecisionHeadSection = cmdArg.Split(',');
                int DOCUMENTTYPE = int.TryParse(cmdArgDecisionHeadSection[0].ToString(), out _default_int) ? int.Parse(cmdArgDecisionHeadSection[0].ToString()) : _default_int;
                int SETTYPEDECISION = int.TryParse(cmdArgDecisionHeadSection[1].ToString(), out _default_int) ? int.Parse(cmdArgDecisionHeadSection[1].ToString()) : _default_int;
                int U0DOCHEADAPPROVE = int.TryParse(cmdArgDecisionHeadSection[2].ToString(), out _default_int) ? int.Parse(cmdArgDecisionHeadSection[2].ToString()) : _default_int;
                int U0NODE = int.TryParse(cmdArgDecisionHeadSection[3].ToString(), out _default_int) ? int.Parse(cmdArgDecisionHeadSection[3].ToString()) : _default_int;
                int U0ACTOR = int.TryParse(cmdArgDecisionHeadSection[4].ToString(), out _default_int) ? int.Parse(cmdArgDecisionHeadSection[4].ToString()) : _default_int;
                int RowCount = int.TryParse(cmdArgDecisionHeadSection[5].ToString(), out _default_int) ? int.Parse(cmdArgDecisionHeadSection[5].ToString()) : _default_int;

                //litDebug.Text = RowCount.ToString();

                var lbU0DocIdx = (gvDocumentList.Rows[RowCount].FindControl("lbU0DocIdx") as Label);
                var DecisionIDXHeadSection2 = (gvDocumentList.Rows[RowCount].FindControl("lblDecisionIDXHeadSection2") as Label);
                var DecisionIDXHeadSection = (gvDocumentList.Rows[RowCount].FindControl("lblDecisionIDXHeadSection") as Label);
                //var rptEquipmentList = (gvDocumentList.Rows[RowCount].FindControl("rptEquipmentList") as Repeater);

                //litDebug.Text = DecisionIDXHeadSection2.Text + DecisionIDXHeadSection.Text;


                switch (SETTYPEDECISION)
                {
                    case 0:
                        switch (DOCUMENTTYPE)
                        {
                            case 1:
                            case 2:
                            case 3:
                                data_qa_cims _data_tranfer = new data_qa_cims();
                                _data_tranfer.qa_cims_u0_document_device_list = new qa_cims_u0_document_device_detail[1];
                                qa_cims_u0_document_device_detail _headApproveTranfer = new qa_cims_u0_document_device_detail();

                                _headApproveTranfer.u0_device_idx = U0DOCHEADAPPROVE;
                                _headApproveTranfer.document_type_idx = DOCUMENTTYPE;
                                _headApproveTranfer.emp_idx_create = _emp_idx;
                                _headApproveTranfer.m0_node_idx = U0NODE;
                                _headApproveTranfer.m0_actor_idx = U0ACTOR;
                                _headApproveTranfer.decision_idx = int.Parse(DecisionIDXHeadSection2.Text);

                                _data_tranfer.qa_cims_u0_document_device_list[0] = _headApproveTranfer;

                                ////litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_tranfer));
                                ////_data_tranfer = callServicePostQaDocCIMS(_urlCimsSetDocumentUpdateHeadSection, _data_tranfer);

                                data_qa_cims _data_qa_tranfer_select = new data_qa_cims();
                                _data_qa_tranfer_select.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
                                qa_cims_u1_document_device_detail _selectEquipmentTranferToUpdate = new qa_cims_u1_document_device_detail();
                                _selectEquipmentTranferToUpdate.condition = 2;
                                _selectEquipmentTranferToUpdate.u0_device_idx = U0DOCHEADAPPROVE;
                                _data_qa_tranfer_select.qa_cims_u1_document_device_list[0] = _selectEquipmentTranferToUpdate;

                                _data_qa_tranfer_select = callServicePostQaDocCIMS(_urlCimsGetDocEquipmentList, _data_qa_tranfer_select);
                                ViewState["DocEquipmentListUpdate"] = "";
                                ViewState["DocEquipmentListUpdate"] = _data_qa_tranfer_select.qa_cims_u1_document_device_list;
                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_qa_cutoff.qa_cims_u1_document_device_list));

                                setGridData(gvPassU1, ViewState["DocEquipmentListUpdate"]);


                                qa_cims_u1_document_device_detail[] _u1tempListEquipmentHolderToUpdate = (qa_cims_u1_document_device_detail[])ViewState["DocEquipmentListUpdate"];
                                var u1_doc_tranfer_to_update = new qa_cims_u1_document_device_detail[_u1tempListEquipmentHolderToUpdate.Count()];
                                int count_someAll = 0;


                                foreach (GridViewRow rowItems in gvPassU1.Rows)
                                {
                                    Label lblEquipment_U1IDX = (Label)rowItems.FindControl("lblEquipment_U1IDX");

                                    u1_doc_tranfer_to_update[count_someAll] = new qa_cims_u1_document_device_detail();
                                    u1_doc_tranfer_to_update[count_someAll].u1_device_idx = int.Parse(lblEquipment_U1IDX.Text);
                                    u1_doc_tranfer_to_update[count_someAll].u0_device_idx = _u1tempListEquipmentHolderToUpdate[count_someAll].u0_device_idx;
                                    count_someAll++;
                                }

                                _data_tranfer.qa_cims_u1_document_device_list = u1_doc_tranfer_to_update;
                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_tranfer));

                                _data_tranfer = callServicePostQaDocCIMS(_urlCimsSetDocumentUpdateHeadSection, _data_tranfer);

                                setActiveTab("docList", 0, 0, 0, 0, 0, 0);
                                ////BackToCurrentPageGvDocumentList(int.Parse(tbPlaceIDXInDocument.Text));
                                //setActiveTab("docList", 0, 0, 0, int.Parse(tbPlaceIDXInDocument.Text), 0, 0);
                                break;

                            case 4:
                                data_qa_cims _data_cutoff = new data_qa_cims();
                                _data_cutoff.qa_cims_u0_document_device_list = new qa_cims_u0_document_device_detail[1];
                                qa_cims_u0_document_device_detail _headApprove = new qa_cims_u0_document_device_detail();

                                _headApprove.u0_device_idx = U0DOCHEADAPPROVE;
                                _headApprove.document_type_idx = DOCUMENTTYPE;
                                _headApprove.emp_idx_create = _emp_idx;
                                _headApprove.m0_node_idx = U0NODE;
                                _headApprove.m0_actor_idx = U0ACTOR;
                                _headApprove.decision_idx = int.Parse(DecisionIDXHeadSection2.Text);

                                _data_cutoff.qa_cims_u0_document_device_list[0] = _headApprove;

                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_cutoff));
                                //_data_qa_cims = callServicePostQaDocCIMS(_urlCimsSetDocumentUpdateHeadSection, _data_cutoff);

                                data_qa_cims _data_qa_cutoff_select = new data_qa_cims();
                                _data_qa_cutoff_select.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
                                qa_cims_u1_document_device_detail _selectEquipmentCutoffToUpdate = new qa_cims_u1_document_device_detail();
                                _selectEquipmentCutoffToUpdate.condition = 1;
                                _selectEquipmentCutoffToUpdate.u0_device_idx = U0DOCHEADAPPROVE;
                                _data_qa_cutoff_select.qa_cims_u1_document_device_list[0] = _selectEquipmentCutoffToUpdate;

                                _data_qa_cutoff_select = callServicePostQaDocCIMS(_urlCimsGetDocEquipmentList, _data_qa_cutoff_select);
                                ViewState["DocEquipmentListUpdate"] = "";
                                ViewState["DocEquipmentListUpdate"] = _data_qa_cutoff_select.qa_cims_u1_document_device_list;
                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_qa_cutoff_select.qa_cims_u1_document_device_list));

                                setGridData(gvPassU1, ViewState["DocEquipmentListUpdate"]);


                                qa_cims_u1_document_device_detail[] _u1tempListEquipmentHolderToUpdateHSCutoff = (qa_cims_u1_document_device_detail[])ViewState["DocEquipmentListUpdate"];
                                var u1_doc_tranfer_to_update_HS_Cutoff = new qa_cims_u1_document_device_detail[_u1tempListEquipmentHolderToUpdateHSCutoff.Count()];
                                int count_someAll_HS_cutoff = 0;


                                foreach (GridViewRow rowItems in gvPassU1.Rows)
                                {
                                    Label lblEquipment_U1IDX = (Label)rowItems.FindControl("lblEquipment_U1IDX");

                                    u1_doc_tranfer_to_update_HS_Cutoff[count_someAll_HS_cutoff] = new qa_cims_u1_document_device_detail();
                                    u1_doc_tranfer_to_update_HS_Cutoff[count_someAll_HS_cutoff].u1_device_idx = int.Parse(lblEquipment_U1IDX.Text);
                                    u1_doc_tranfer_to_update_HS_Cutoff[count_someAll_HS_cutoff].u0_device_idx = _u1tempListEquipmentHolderToUpdateHSCutoff[count_someAll_HS_cutoff].u0_device_idx;
                                    count_someAll_HS_cutoff++;
                                }

                                _data_cutoff.qa_cims_u1_document_device_list = u1_doc_tranfer_to_update_HS_Cutoff;
                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_cutoff));

                                _data_cutoff = callServicePostQaDocCIMS(_urlCimsSetDocumentUpdateHeadSection, _data_cutoff);


                                setActiveTab("docList", 0, 0, 0, 0, 0, 0);
                                ////BackToCurrentPageGvDocumentList(int.Parse(tbPlaceIDXInDocument.Text));
                                //setActiveTab("docList", 0, 0, 0, int.Parse(tbPlaceIDXInDocument.Text), 0, 0);
                                break;
                        }
                        break;

                    case 1:
                        switch (DOCUMENTTYPE)
                        {
                            case 0:
                            case 1:
                            case 2:
                            case 3:
                                data_qa_cims _data_tranfer = new data_qa_cims();
                                _data_tranfer.qa_cims_u0_document_device_list = new qa_cims_u0_document_device_detail[1];
                                qa_cims_u0_document_device_detail _headApprovetranfer = new qa_cims_u0_document_device_detail();

                                _headApprovetranfer.u0_device_idx = U0DOCHEADAPPROVE;
                                _headApprovetranfer.document_type_idx = DOCUMENTTYPE;
                                _headApprovetranfer.emp_idx_create = _emp_idx;
                                _headApprovetranfer.m0_node_idx = U0NODE;
                                _headApprovetranfer.m0_actor_idx = U0ACTOR;
                                _headApprovetranfer.decision_idx = int.Parse(DecisionIDXHeadSection.Text);

                                _data_tranfer.qa_cims_u0_document_device_list[0] = _headApprovetranfer;

                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_tranfer));
                                //_data_qa_cims = callServicePostQaDocCIMS(_urlCimsSetDocumentUpdateHeadSection, _data_tranfer);

                                data_qa_cims _data_qa_tranfer_select = new data_qa_cims();
                                _data_qa_tranfer_select.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
                                qa_cims_u1_document_device_detail _selectEquipmentTranferToUpdate = new qa_cims_u1_document_device_detail();
                                _selectEquipmentTranferToUpdate.condition = 2;
                                _selectEquipmentTranferToUpdate.u0_device_idx = U0DOCHEADAPPROVE;
                                _data_qa_tranfer_select.qa_cims_u1_document_device_list[0] = _selectEquipmentTranferToUpdate;

                                _data_qa_tranfer_select = callServicePostQaDocCIMS(_urlCimsGetDocEquipmentList, _data_qa_tranfer_select);
                                ViewState["DocEquipmentListUpdate"] = "";
                                ViewState["DocEquipmentListUpdate"] = _data_qa_tranfer_select.qa_cims_u1_document_device_list;
                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_qa_cutoff.qa_cims_u1_document_device_list));

                                setGridData(gvPassU1, ViewState["DocEquipmentListUpdate"]);


                                qa_cims_u1_document_device_detail[] _u1tempListEquipmentHolderToUpdate = (qa_cims_u1_document_device_detail[])ViewState["DocEquipmentListUpdate"];
                                var u1_doc_tranfer_to_update = new qa_cims_u1_document_device_detail[_u1tempListEquipmentHolderToUpdate.Count()];
                                int count_someAll = 0;


                                foreach (GridViewRow rowItems in gvPassU1.Rows)
                                {
                                    Label lblEquipment_U1IDX = (Label)rowItems.FindControl("lblEquipment_U1IDX");

                                    u1_doc_tranfer_to_update[count_someAll] = new qa_cims_u1_document_device_detail();
                                    u1_doc_tranfer_to_update[count_someAll].u1_device_idx = int.Parse(lblEquipment_U1IDX.Text);
                                    u1_doc_tranfer_to_update[count_someAll].u0_device_idx = _u1tempListEquipmentHolderToUpdate[count_someAll].u0_device_idx;
                                    count_someAll++;
                                }

                                _data_tranfer.qa_cims_u1_document_device_list = u1_doc_tranfer_to_update;
                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_tranfer));

                                _data_tranfer = callServicePostQaDocCIMS(_urlCimsSetDocumentUpdateHeadSection, _data_tranfer);

                                setActiveTab("docList", 0, 0, 0, 0, 0, 0);

                                ////BackToCurrentPageGvDocumentList(int.Parse(tbPlaceIDXInDocument.Text));
                                //setActiveTab("docList", 0, 0, 0, int.Parse(tbPlaceIDXInDocument.Text), 0, 0);
                                break;
                            case 4:

                                data_qa_cims _data_cutoff = new data_qa_cims();
                                _data_cutoff.qa_cims_u0_document_device_list = new qa_cims_u0_document_device_detail[1];
                                qa_cims_u0_document_device_detail _headApprove = new qa_cims_u0_document_device_detail();

                                _headApprove.u0_device_idx = U0DOCHEADAPPROVE;
                                _headApprove.document_type_idx = DOCUMENTTYPE;
                                _headApprove.emp_idx_create = _emp_idx;
                                _headApprove.m0_node_idx = U0NODE;
                                _headApprove.m0_actor_idx = U0ACTOR;
                                _headApprove.decision_idx = int.Parse(DecisionIDXHeadSection.Text);

                                _data_cutoff.qa_cims_u0_document_device_list[0] = _headApprove;

                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_cutoff));
                                //_data_qa_cims = callServicePostQaDocCIMS(_urlCimsSetDocumentUpdateHeadSection, _data_cutoff);

                                data_qa_cims _data_qa_cutoff_select = new data_qa_cims();
                                _data_qa_cutoff_select.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
                                qa_cims_u1_document_device_detail _selectEquipmentCutoffToUpdate = new qa_cims_u1_document_device_detail();
                                _selectEquipmentCutoffToUpdate.condition = 1;
                                _selectEquipmentCutoffToUpdate.u0_device_idx = U0DOCHEADAPPROVE;
                                _data_qa_cutoff_select.qa_cims_u1_document_device_list[0] = _selectEquipmentCutoffToUpdate;

                                _data_qa_cutoff_select = callServicePostQaDocCIMS(_urlCimsGetDocEquipmentList, _data_qa_cutoff_select);
                                ViewState["DocEquipmentListUpdate"] = "";
                                ViewState["DocEquipmentListUpdate"] = _data_qa_cutoff_select.qa_cims_u1_document_device_list;
                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_qa_cutoff_select.qa_cims_u1_document_device_list));

                                setGridData(gvPassU1, ViewState["DocEquipmentListUpdate"]);


                                qa_cims_u1_document_device_detail[] _u1tempListEquipmentHolderToUpdateHSCutoff = (qa_cims_u1_document_device_detail[])ViewState["DocEquipmentListUpdate"];
                                var u1_doc_tranfer_to_update_HS_Cutoff = new qa_cims_u1_document_device_detail[_u1tempListEquipmentHolderToUpdateHSCutoff.Count()];
                                int count_someAll_HS_cutoff = 0;


                                foreach (GridViewRow rowItems in gvPassU1.Rows)
                                {
                                    Label lblEquipment_U1IDX = (Label)rowItems.FindControl("lblEquipment_U1IDX");

                                    u1_doc_tranfer_to_update_HS_Cutoff[count_someAll_HS_cutoff] = new qa_cims_u1_document_device_detail();
                                    u1_doc_tranfer_to_update_HS_Cutoff[count_someAll_HS_cutoff].u1_device_idx = int.Parse(lblEquipment_U1IDX.Text);
                                    u1_doc_tranfer_to_update_HS_Cutoff[count_someAll_HS_cutoff].u0_device_idx = _u1tempListEquipmentHolderToUpdateHSCutoff[count_someAll_HS_cutoff].u0_device_idx;
                                    count_someAll_HS_cutoff++;
                                }

                                _data_cutoff.qa_cims_u1_document_device_list = u1_doc_tranfer_to_update_HS_Cutoff;
                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_cutoff));

                                _data_cutoff = callServicePostQaDocCIMS(_urlCimsSetDocumentUpdateHeadSection, _data_cutoff);

                                setActiveTab("docList", 0, 0, 0, 0, 0, 0);


                                ////BackToCurrentPageGvDocumentList(int.Parse(tbPlaceIDXInDocument.Text));
                                //setActiveTab("docList", 0, 0, 0, int.Parse(tbPlaceIDXInDocument.Text), 0, 0);
                                break;

                        }
                        break;
                }

                break;

            case "cmdInsertDetail":

                var _inputM0DeviceIDX = (TextBox)fvShowDetailsSearchTool.FindControl("tbM0DeviceIDX");
                var _inputIDCodeEquipment = (TextBox)fvShowDetailsSearchTool.FindControl("tbIDCodeEquipment");
                var _inputEquipmentType = (TextBox)fvShowDetailsSearchTool.FindControl("tbEquipmentType");
                var _inputEquipmentName = (TextBox)fvShowDetailsSearchTool.FindControl("tbEquipmentName");
                var _inputEquipmentBrandName = (TextBox)fvShowDetailsSearchTool.FindControl("tbEquipmentBrandName");
                var _inputSerailNumber = (TextBox)fvShowDetailsSearchTool.FindControl("tbSerailNumber");
                var _inputSectionHolder = (TextBox)fvShowDetailsSearchTool.FindControl("tbSectionHolder");
                var _inputDeviceCalDate = (TextBox)fvShowDetailsSearchTool.FindControl("tbDeviceCalDate");
                var _inputDeviceDueDate = (TextBox)fvShowDetailsSearchTool.FindControl("tbDeviceDueDate");

                var ds_Add_EquipmentList_old = (DataSet)ViewState["vsDataEquipmentCalOld"];
                var dr_Add_EquipmentList_old = ds_Add_EquipmentList_old.Tables[0].NewRow();

                int _numrow = ds_Add_EquipmentList_old.Tables[0].Rows.Count; //จำนวนแถว

                ViewState["CHECKDUPICATE_IDCODENUMBER_OLD"] = _inputIDCodeEquipment.Text;

                if (_numrow > 0)
                {
                    //foreach (DataRow checkIdCodeEquipment in ds_Add_EquipmentList_old.Tables[0].Rows)
                    //{
                    //    ViewState["_CHECKIDCODENUMBEROLD"] = checkIdCodeEquipment["IDCodeNumber"];

                    //    if (ViewState["CHECKDUPICATE_IDCODENUMBER_OLD"].ToString() == ViewState["_CHECKIDCODENUMBEROLD"].ToString())
                    //    {
                    //        ViewState["CheckDataset_Equipment"] = "1";
                    //        break;
                    //    }
                    //    else
                    //    {
                    //        ViewState["CheckDataset_Equipment"] = "0";
                    //    }

                    //}

                    //if (ViewState["CheckDataset_Equipment"].ToString() == "1")
                    //{
                    //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลรหัสเครื่อง " + _inputIDCodeEquipment.Text + " นี้แล้ว กรุณาทำการกรอกรหัสเครื่องใหม่ค่ะ!');", true);

                    //}

                    //   else if (ViewState["CheckDataset_Equipment"].ToString() == "0")
                    //   {
                    dr_Add_EquipmentList_old["IDCodeNumber"] = _inputIDCodeEquipment.Text;
                    dr_Add_EquipmentList_old["EquipmentType"] = _inputEquipmentType.Text;
                    dr_Add_EquipmentList_old["EquipmentName"] = _inputEquipmentName.Text;
                    dr_Add_EquipmentList_old["BrandName"] = _inputEquipmentBrandName.Text;
                    dr_Add_EquipmentList_old["SerialNumber"] = _inputSerailNumber.Text;
                    dr_Add_EquipmentList_old["Holder"] = _inputSectionHolder.Text;
                    dr_Add_EquipmentList_old["calDate"] = _inputDeviceCalDate.Text;
                    dr_Add_EquipmentList_old["DueDate"] = _inputDeviceCalDate.Text;
                    dr_Add_EquipmentList_old["M0DeviceIDX"] = _inputM0DeviceIDX.Text;


                    //     }
                }
                else
                {
                    dr_Add_EquipmentList_old["IDCodeNumber"] = _inputIDCodeEquipment.Text;
                    dr_Add_EquipmentList_old["EquipmentType"] = _inputEquipmentType.Text;
                    dr_Add_EquipmentList_old["EquipmentName"] = _inputEquipmentName.Text;
                    dr_Add_EquipmentList_old["BrandName"] = _inputEquipmentBrandName.Text;
                    dr_Add_EquipmentList_old["SerialNumber"] = _inputSerailNumber.Text;
                    dr_Add_EquipmentList_old["Holder"] = _inputSectionHolder.Text;
                    dr_Add_EquipmentList_old["calDate"] = _inputDeviceCalDate.Text;
                    dr_Add_EquipmentList_old["DueDate"] = _inputDeviceCalDate.Text;
                    dr_Add_EquipmentList_old["M0DeviceIDX"] = _inputM0DeviceIDX.Text;

                }

                ds_Add_EquipmentList_old.Tables[0].Rows.Add(dr_Add_EquipmentList_old);
                ViewState["vsDataEquipmentCalOld"] = ds_Add_EquipmentList_old;

                gvListEquipmentCalOld.Visible = true;
                setGridData(gvListEquipmentCalOld, (DataSet)ViewState["vsDataEquipmentCalOld"]);

                if (ViewState["vsDataEquipmentCalOld"] != null)
                {
                    divActionSaveCreateDocument.Visible = true;
                }
                else
                {
                    divActionSaveCreateDocument.Visible = false;
                }

                break;

            case "cmdSaveDocCal":

                ClearDataCalibrationPoint();


                data_qa_cims _qacalibration = new data_qa_cims();

                _qacalibration.qa_cims_u0_calibration_document_list = new qa_cims_u0_calibration_document_details[1];
                _qacalibration.qa_cims_u1_calibration_document_list = new qa_cims_u1_calibration_document_details[1];
                _qacalibration.qa_cims_u2_calibration_document_list = new qa_cims_u2_calibration_document_details[1];

                qa_cims_u0_calibration_document_details _u0calibration = new qa_cims_u0_calibration_document_details();
                qa_cims_u1_calibration_document_details _u1calibration = new qa_cims_u1_calibration_document_details();
                qa_cims_u2_calibration_document_details _u2calibration = new qa_cims_u2_calibration_document_details();

                RadioButtonList _node_decision = (RadioButtonList)docCreate.FindControl("rblTypeEquipment");

                _u0calibration.cemp_idx = _emp_idx;
                _u0calibration.m0_actor_idx = 1;
                _u0calibration.m0_node_idx = 1;
                _u0calibration.place_idx = int.Parse(ddlPlaceCreate.SelectedValue);
                _u0calibration.decision_idx = int.Parse(_node_decision.SelectedValue);

                _qacalibration.qa_cims_u0_calibration_document_list[0] = _u0calibration;

                switch (_node_decision.SelectedValue)
                {
                    case "1": // machine new

                        var ds_equipment_cal_new_insert = (DataSet)ViewState["vsDataEquipmentCalNew"];
                        var m0_doc_cal_new_insert = new qa_cims_m0_calibration_registration_details[ds_equipment_cal_new_insert.Tables[0].Rows.Count];

                        var ds_point = (DataSet)ViewState["vsDataCalibrationPoint_insert"];
                        var _point = new qa_cims_m1_registration_device[ds_point.Tables[0].Rows.Count];

                        var _checkRowCountNew = ds_equipment_cal_new_insert.Tables[0].Rows.Count;
                        var _checkpointNew = ds_point.Tables[0].Rows.Count;

                        int m = 0;
                        int p = 0;

                        foreach (DataRow dr_equipment_cal_new_insert in ds_equipment_cal_new_insert.Tables[0].Rows)
                        {
                            m0_doc_cal_new_insert[m] = new qa_cims_m0_calibration_registration_details();
                            m0_doc_cal_new_insert[m].m0_device_idx = int.Parse(dr_equipment_cal_new_insert["M0DeviceIDX_New"].ToString());
                            m0_doc_cal_new_insert[m].brand_idx = int.Parse(dr_equipment_cal_new_insert["M0BrandIDX_New"].ToString());
                            m0_doc_cal_new_insert[m].equipment_type_idx = int.Parse(dr_equipment_cal_new_insert["EquipmentTypeIDX_New"].ToString());
                            m0_doc_cal_new_insert[m].range_of_use_min = Convert.ToDecimal(dr_equipment_cal_new_insert["RangeOfUseMin"].ToString());
                            m0_doc_cal_new_insert[m].range_of_use_max = Convert.ToDecimal(dr_equipment_cal_new_insert["RangeOfUseMax"].ToString());
                            m0_doc_cal_new_insert[m].unit_idx_range_of_use = int.Parse(dr_equipment_cal_new_insert["RangeOfUseUnit"].ToString());
                            m0_doc_cal_new_insert[m].cal_type_idx = int.Parse(dr_equipment_cal_new_insert["CalibrationType"].ToString());
                            m0_doc_cal_new_insert[m].acceptance_criteria = Convert.ToDecimal(dr_equipment_cal_new_insert["Acceptance_Criteria"].ToString());
                            m0_doc_cal_new_insert[m].acceptance_criteria_unit_idx = int.Parse(dr_equipment_cal_new_insert["Acceptance_Criteria_Unit"].ToString());
                            m0_doc_cal_new_insert[m].resolution_detail = Convert.ToDecimal(dr_equipment_cal_new_insert["Resolution"].ToString());
                            m0_doc_cal_new_insert[m].unit_idx_resolution = int.Parse(dr_equipment_cal_new_insert["Resolution_Unit"].ToString());
                            m0_doc_cal_new_insert[m].measuring_range_min = Convert.ToDecimal(dr_equipment_cal_new_insert["Measuring_Range_Min"].ToString());
                            m0_doc_cal_new_insert[m].measuring_range_max = Convert.ToDecimal(dr_equipment_cal_new_insert["Measuring_Range_Max"].ToString());
                            m0_doc_cal_new_insert[m].unit_idx_measuring_range = int.Parse(dr_equipment_cal_new_insert["Measuring_Range_Unit"].ToString());
                            m0_doc_cal_new_insert[m].calibration_frequency = Convert.ToDecimal(dr_equipment_cal_new_insert["Calibration_Frequency"].ToString());
                            m0_doc_cal_new_insert[m].device_serial = (dr_equipment_cal_new_insert["SerialNumber_New"].ToString());
                            m0_doc_cal_new_insert[m].device_details = (dr_equipment_cal_new_insert["Details_Equipment"].ToString());
                            m0_doc_cal_new_insert[m].certificate = int.Parse(dr_equipment_cal_new_insert["Certificate_insert"].ToString());
                            m0_doc_cal_new_insert[m].rsec_idx_create = int.Parse(ViewState["rsec_permission"].ToString());
                            m0_doc_cal_new_insert[m].org_idx_create = int.Parse(ViewState["org_permission"].ToString());
                            m0_doc_cal_new_insert[m].rdept_idx_create = int.Parse(ViewState["rdept_permission"].ToString());
                            m0_doc_cal_new_insert[m].emp_idx_create = _emp_idx;
                            m0_doc_cal_new_insert[m].device_rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
                            m0_doc_cal_new_insert[m].device_org_idx = int.Parse(ViewState["org_permission"].ToString());
                            m0_doc_cal_new_insert[m].device_rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
                            m0_doc_cal_new_insert[m].calibration_point_list = (dr_equipment_cal_new_insert["CalibrationPointList"].ToString());
                            m0_doc_cal_new_insert[m].calibration_point_idx_list = (dr_equipment_cal_new_insert["CalibrationPointList_IDX"].ToString());
                            m0_doc_cal_new_insert[m].device_id_no = (dr_equipment_cal_new_insert["IDCodeNumber_New"].ToString());

                            m++;
                        }

                        foreach (DataRow dr_point in ds_point.Tables[0].Rows)
                        {
                            _point[p] = new qa_cims_m1_registration_device();
                            _point[p].calibration_point = (dr_point["CalibrationPoint_insert"].ToString());
                            _point[p].unit_idx = int.Parse(dr_point["CalibrationPointUnitIDX_insert"].ToString());
                            _point[p].device_id_no = (dr_point["IDNUMBER_insert"].ToString());
                            _point[p].device_serial_no = (dr_point["Serial_no_insert"].ToString());
                            _point[p].cemp_idx = _emp_idx;

                            p++;

                        }

                        _qacalibration.qa_cims_m0_calibration_registration_list = m0_doc_cal_new_insert;
                        _qacalibration.qa_cims_m1_registration_device_list = _point;
                        _qacalibration = callServicePostQaDocCIMS(_urlCimsSetDocumentCalibration, _qacalibration);

                        // litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_qacalibration));

                        break;

                    case "2": // machine old 

                        var ds_equipment_cal_old_insert = (DataSet)ViewState["vsDataEquipmentCalOld"];
                        var u1_doc_cal_insert = new qa_cims_u1_calibration_document_details[ds_equipment_cal_old_insert.Tables[0].Rows.Count];
                        var _checkRowCount = ds_equipment_cal_old_insert.Tables[0].Rows.Count;

                        int c = 0;

                        foreach (DataRow dr_equipment_cal_old_insert in ds_equipment_cal_old_insert.Tables[0].Rows)
                        {
                            u1_doc_cal_insert[c] = new qa_cims_u1_calibration_document_details();
                            u1_doc_cal_insert[c].m0_device_idx = int.Parse(dr_equipment_cal_old_insert["M0DeviceIDX"].ToString());
                            //  u1_doc_cal_insert[c].certificate = 1;

                            c++;
                        }

                        _qacalibration.qa_cims_u1_calibration_document_list = u1_doc_cal_insert;
                        //  _qacalibration = callServicePostQaDocCIMS(_urlCimsSetDocumentCalibration, _qacalibration);

                        //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_qacalibration));

                        break;

                }


                break;

            case "cmdAddCalibrationPoint":

                var _input_calibration_point = (TextBox)pnEquipmentNew.FindControl("tbcalibration_point");
                var _input_calibration_point_idx = (DropDownList)pnEquipmentNew.FindControl("ddl_calibration_point_unit");
                var _input_serailnumber_insert = (TextBox)pnEquipmentNew.FindControl("tbserailnumber");

                var ds_add_calibration_point = (DataSet)ViewState["vsDataCalibrationPoint"];
                var dr_add_calibration_point = ds_add_calibration_point.Tables[0].NewRow();

                int numrow_point = ds_add_calibration_point.Tables[0].Rows.Count; //จำนวนแถว

                ViewState["CHECK"] = _input_calibration_point.Text;

                if (numrow_point > 0)
                {
                    dr_add_calibration_point["CalibrationPoint"] = _input_calibration_point.Text;
                    dr_add_calibration_point["CalibrationPointUnitIDX"] = int.Parse(_input_calibration_point_idx.SelectedValue);
                    dr_add_calibration_point["CalibrationPointUnit"] = _input_calibration_point_idx.SelectedItem.Text;
                    dr_add_calibration_point["IDNUMBER"] = tbIDNo.Text;



                }
                else
                {
                    dr_add_calibration_point["CalibrationPoint"] = _input_calibration_point.Text;
                    dr_add_calibration_point["CalibrationPointUnitIDX"] = int.Parse(_input_calibration_point_idx.SelectedValue);
                    dr_add_calibration_point["CalibrationPointUnit"] = _input_calibration_point_idx.SelectedItem.Text;
                    dr_add_calibration_point["IDNUMBER"] = tbIDNo.Text;
                }


                var ds_add_calibration_point_insert = (DataSet)ViewState["vsDataCalibrationPoint_insert"];
                var dr_add_calibration_point_insert = ds_add_calibration_point_insert.Tables[0].NewRow();

                int numrow_point_insert = ds_add_calibration_point_insert.Tables[0].Rows.Count; //จำนวนแถว

                //  ViewState["CHECK"] = _input_calibration_point.Text;

                if (numrow_point_insert > 0)
                {
                    dr_add_calibration_point_insert["CalibrationPoint_insert"] = _input_calibration_point.Text;
                    dr_add_calibration_point_insert["CalibrationPointUnitIDX_insert"] = int.Parse(_input_calibration_point_idx.SelectedValue);
                    dr_add_calibration_point_insert["CalibrationPointUnit_insert"] = _input_calibration_point_idx.SelectedItem.Text;
                    dr_add_calibration_point_insert["IDNUMBER_insert"] = tbIDNo.Text;
                    dr_add_calibration_point_insert["Serial_no_insert"] = _input_serailnumber_insert.Text;



                }
                else
                {
                    dr_add_calibration_point_insert["CalibrationPoint_insert"] = _input_calibration_point.Text;
                    dr_add_calibration_point_insert["CalibrationPointUnitIDX_insert"] = int.Parse(_input_calibration_point_idx.SelectedValue);
                    dr_add_calibration_point_insert["CalibrationPointUnit_insert"] = _input_calibration_point_idx.SelectedItem.Text;
                    dr_add_calibration_point_insert["IDNUMBER_insert"] = tbIDNo.Text;
                    dr_add_calibration_point_insert["Serial_no_insert"] = _input_serailnumber_insert.Text;
                }

                ds_add_calibration_point_insert.Tables[0].Rows.Add(dr_add_calibration_point_insert);
                ViewState["vsDataCalibrationPoint_insert"] = ds_add_calibration_point_insert;

                ds_add_calibration_point.Tables[0].Rows.Add(dr_add_calibration_point);
                ViewState["vsDataCalibrationPoint"] = ds_add_calibration_point;


                gvCalibrationPoint.Visible = true;
                setGridData(gvCalibrationPoint, (DataSet)ViewState["vsDataCalibrationPoint"]);

                setGridData(gvCalPointInsert, (DataSet)ViewState["vsDataCalibrationPoint_insert"]);

                if (ViewState["vsDataCalibrationPoint"] != null)
                {
                    btnInsertDetail.Visible = true;
                }
                else
                {
                    btnInsertDetail.Visible = false;
                }

                break;

            case "cmdViewQACutoff":

                string[] cmdArgDecisionQACutoff = cmdArg.Split(',');
                int thorwU0_device_idx = int.TryParse(cmdArgDecisionQACutoff[0].ToString(), out _default_int) ? int.Parse(cmdArgDecisionQACutoff[0].ToString()) : _default_int;
                int RowCountQACutoff = int.TryParse(cmdArgDecisionQACutoff[1].ToString(), out _default_int) ? int.Parse(cmdArgDecisionQACutoff[1].ToString()) : _default_int;

                //litDebug.Text = thorwU0_device_idx.ToString(); 
                setRepeaterData(rp_place_document, null);
                var DecisionQACutoff2 = (gvDocumentList.Rows[RowCountQACutoff].FindControl("lblDecisionQACutoff2") as Label);
                var DecisionQACutoff = (gvDocumentList.Rows[RowCountQACutoff].FindControl("lblDecisionQACutoff") as Label);
                var lblstaidxQACutoff2 = (gvDocumentList.Rows[RowCountQACutoff].FindControl("lblstaidxQACutoff2") as Label);
                var lblstaidxQACutoff = (gvDocumentList.Rows[RowCountQACutoff].FindControl("lblstaidxQACutoff") as Label);
                //var rblQAApproveCutoff = (gvDocumentList.Rows[RowCountQACutoff].FindControl("rblQAApproveCutoff") as RadioButtonList);

                //RadioButtonList rblQAApproveCutoff = (RadioButtonList)gvEquipmentQACutoff.FindControl("rblQAApproveCutoff");

                //int thorwU0_device_idx = int.Parse(cmdArg);

                //litDebug.Text = thorwU0_device_idx.ToString(); 

                selectLogQACutoff(thorwU0_device_idx.ToString());

                divdocumentList.Visible = false;
                divRegistrationList.Visible = false;
                divFVVQACutoff.Visible = true;
                divTranferRegistration.Visible = true;
                divDocDetailUser.Visible = true;
                btnBackToDocList.Visible = true;
                divSearchDocument.Visible = false;

                div_SearchDoc.Visible = false;

                CheckAllQAApproveCutoff.Checked = false;
                CheckAllQANotApproveCutoff.Checked = false;

                setFormData(fvDocDetailUser, FormViewMode.ReadOnly, ((data_employee)ViewState["vsEmpProfile"]).employee_list);

                data_qa_cims data_select_Creator_details = new data_qa_cims();
                data_select_Creator_details.qa_cims_u0_document_device_list = new qa_cims_u0_document_device_detail[1];
                qa_cims_u0_document_device_detail _selectDetailsCutoff = new qa_cims_u0_document_device_detail();
                _selectDetailsCutoff.u0_device_idx = thorwU0_device_idx;
                data_select_Creator_details.qa_cims_u0_document_device_list[0] = _selectDetailsCutoff;

                data_select_Creator_details = callServicePostQaDocCIMS(_urlCimsGetDetailCreator, data_select_Creator_details);

                setFormData(fvDocDetailCreator, FormViewMode.ReadOnly, (data_select_Creator_details.qa_cims_u0_document_device_list));

                data_qa_cims data_qa_cutoff_ = new data_qa_cims();
                data_qa_cutoff_.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
                qa_cims_u1_document_device_detail _selectEquipmentCutoff = new qa_cims_u1_document_device_detail();
                _selectEquipmentCutoff.condition = 1;
                _selectEquipmentCutoff.u0_device_idx = thorwU0_device_idx;
                data_qa_cutoff_.qa_cims_u1_document_device_list[0] = _selectEquipmentCutoff;

                data_qa_cutoff_ = callServicePostQaDocCIMS(_urlCimsGetDocEquipmentList, data_qa_cutoff_);

                ViewState["DocEquipmentList"] = data_qa_cutoff_.qa_cims_u1_document_device_list;
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_qa_cutoff.qa_cims_u1_document_device_list));

                setGridData(gvEquipmentQACutoff, ViewState["DocEquipmentList"]);

                data_qa_cims _dataCimsDecisionNodeQACutoff_ = new data_qa_cims();
                _dataCimsDecisionNodeQACutoff_.qa_cims_bindnode_decision_list = new qa_cims_bindnode_decision_detail[1];
                qa_cims_bindnode_decision_detail _selectDecisionNodeQACutoff_ = new qa_cims_bindnode_decision_detail();
                _dataCimsDecisionNodeQACutoff_.qa_cims_bindnode_decision_list[0] = _selectDecisionNodeQACutoff_;

                _dataCimsDecisionNodeQACutoff_ = callServicePostQaDocCIMS(_urlCimsGetDecisionQACutoff, _dataCimsDecisionNodeQACutoff_);

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataCimsDecisionNodeQACutoff));

                var _linqSetDecision = from data_qa_cims in _dataCimsDecisionNodeQACutoff_.qa_cims_bindnode_decision_list
                                       where data_qa_cims.decision_idx == int.Parse(DecisionQACutoff.Text)
                                       select data_qa_cims;

                var _linqSetDecisionNA = from data_qa_cims in _dataCimsDecisionNodeQACutoff_.qa_cims_bindnode_decision_list
                                         where data_qa_cims.decision_idx == int.Parse(DecisionQACutoff2.Text)
                                         select data_qa_cims;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_linqSetDecisionNA.ToList()));
                //setRepeaterData(rptActionQACutoff, _linqSetDecision.ToList());
                //setRepeaterData(rptActionQACutoffNotAppove, _linqSetDecisionNA.ToList());

                lblDecisionQACutoffView2.Text = DecisionQACutoff2.Text;
                lblDecisionQACutoffView.Text = DecisionQACutoff.Text;
                lbstaidxQACutoffView2.Text = lblstaidxQACutoff2.Text;
                lbstaidxQACutoffView.Text = lblstaidxQACutoff.Text;

                break;

            case "cmdXmlCutoff":

                int STARTNODEQACUTOFF = int.Parse(cmdArg);

                //litDebug.Text = STARTNODEQACUTOFF.ToString();

                qa_cims_u1_document_device_detail[] _u1tempListEquipmentHolderCutoffHead = (qa_cims_u1_document_device_detail[])ViewState["DocEquipmentList"];


                if (CheckAllQACutoff.Checked == true)
                {
                    _data_qa_cims.qa_cims_u0_document_device_list = new qa_cims_u0_document_device_detail[1];
                    qa_cims_u0_document_device_detail _selectDicision = new qa_cims_u0_document_device_detail();
                    _selectDicision.decision_idx = STARTNODEQACUTOFF;
                    _selectDicision.m0_actor_idx = _u1tempListEquipmentHolderCutoffHead[0].m0_actor_idx;
                    _selectDicision.m0_node_idx = _u1tempListEquipmentHolderCutoffHead[0].m0_node_idx;
                    _selectDicision.u0_device_idx = _u1tempListEquipmentHolderCutoffHead[0].u0_device_idx;
                    _selectDicision.emp_idx_create = _emp_idx;
                    _data_qa_cims.qa_cims_u0_document_device_list[0] = _selectDicision;

                    qa_cims_u1_document_device_detail[] _u1tempListEquipmentHolderCutoff = (qa_cims_u1_document_device_detail[])ViewState["DocEquipmentList"];

                    var u1_doc_cutoff_someAll = new qa_cims_u1_document_device_detail[_u1tempListEquipmentHolderCutoff.Count()];
                    int count_someAll = 0;


                    foreach (GridViewRow rowItems in gvEquipmentQACutoff.Rows)
                    {
                        Label lblDeviceIDX = (Label)rowItems.FindControl("lblDeviceIDX");
                        TextBox txtcomment_qacutoff = (TextBox)rowItems.FindControl("txtcomment_qacutoff");

                        u1_doc_cutoff_someAll[count_someAll] = new qa_cims_u1_document_device_detail();
                        u1_doc_cutoff_someAll[count_someAll].u1_device_idx = int.Parse(lblDeviceIDX.Text);
                        u1_doc_cutoff_someAll[count_someAll].decision_idx = STARTNODEQACUTOFF;
                        u1_doc_cutoff_someAll[count_someAll].u0_device_idx = _u1tempListEquipmentHolderCutoff[count_someAll].u0_device_idx;
                        u1_doc_cutoff_someAll[count_someAll].m0_actor_idx = _u1tempListEquipmentHolderCutoff[count_someAll].m0_actor_idx;
                        u1_doc_cutoff_someAll[count_someAll].m0_node_idx = _u1tempListEquipmentHolderCutoff[count_someAll].m0_node_idx;
                        u1_doc_cutoff_someAll[count_someAll].condition = 2;
                        if (txtcomment_qacutoff.Text == "")
                        {
                            u1_doc_cutoff_someAll[count_someAll].comment = "-";
                        }
                        else
                        {
                            u1_doc_cutoff_someAll[count_someAll].comment = txtcomment_qacutoff.Text;
                        }
                        if (STARTNODEQACUTOFF == int.Parse(lblDecisionQACutoffView2.Text))
                        {
                            u1_doc_cutoff_someAll[count_someAll].status = 0;
                        }
                        else if (STARTNODEQACUTOFF == int.Parse(lblDecisionQACutoffView.Text))
                        {
                            u1_doc_cutoff_someAll[count_someAll].status = 1;
                        }

                        u1_doc_cutoff_someAll[count_someAll].cemp_idx = _u1tempListEquipmentHolderCutoff[count_someAll].cemp_idx;

                        count_someAll++;


                    }

                    _data_qa_cims.qa_cims_u1_document_device_list = u1_doc_cutoff_someAll;
                    //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_qa_cims));
                    _data_qa_cims = callServicePostQaDocCIMS(_urlCimsSetDocumentUpdate, _data_qa_cims);

                    divdocumentList.Visible = true;
                    divFVVQACutoff.Visible = false;

                    CheckAllQACutoff.Checked = false;
                    btnBackToDocList.Visible = false;
                    divDocDetailUser.Visible = false;

                    //litDebug.Text = "We Here 1";

                    //data_qa_cims renew = new data_qa_cims();
                    //renew.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
                    //qa_cims_u1_document_device_detail _selectu0DocList = new qa_cims_u1_document_device_detail();

                    //renew.qa_cims_u1_document_device_list[0] = _selectu0DocList;

                    //renew = callServicePostQaDocCIMS(_urlCimsGetEquipmentList, renew);

                    //setGridData(gvDocumentList, renew.qa_cims_u0_document_device_list);
                    //setActiveTab("docList", 0, 0, 0, 0, 0, 0);
                    setActiveTab("docList", 0, 0, 0, int.Parse(tbPlaceIDXInDocument.Text), 0, 0);

                }
                else
                {
                    GridView EquipmentQACutoff = (GridView)divFVVQACutoff.FindControl("gvEquipmentQACutoff");

                    qa_cims_u1_document_device_detail[] _u1tempListEquipmentHolderCutoff = (qa_cims_u1_document_device_detail[])ViewState["DocEquipmentList"];

                    var u1_doc_cutoff_someAll = new qa_cims_u1_document_device_detail[_u1tempListEquipmentHolderCutoff.Count()];
                    int count_someAll = 0;
                    int count_row_check = 0;
                    foreach (GridViewRow rowItems in gvEquipmentQACutoff.Rows)
                    {
                        CheckBox chk = (CheckBox)rowItems.FindControl("ChkEquipmentQACutoff");
                        Label lblDeviceIDX = (Label)rowItems.FindControl("lblDeviceIDX");
                        //Label lblEquipment_IDNO = (Label)rowItems.FindControl("lblEquipment_IDNO");
                        TextBox txtcomment_qacutoff = (TextBox)rowItems.FindControl("txtcomment_qacutoff");

                        if (chk.Checked)
                        {
                            u1_doc_cutoff_someAll[count_someAll] = new qa_cims_u1_document_device_detail();
                            u1_doc_cutoff_someAll[count_someAll].u1_device_idx = int.Parse(lblDeviceIDX.Text);
                            u1_doc_cutoff_someAll[count_someAll].decision_idx = STARTNODEQACUTOFF;
                            u1_doc_cutoff_someAll[count_someAll].u0_device_idx = _u1tempListEquipmentHolderCutoff[count_someAll].u0_device_idx;
                            u1_doc_cutoff_someAll[count_someAll].m0_actor_idx = _u1tempListEquipmentHolderCutoff[count_someAll].m0_actor_idx;
                            u1_doc_cutoff_someAll[count_someAll].m0_node_idx = _u1tempListEquipmentHolderCutoff[count_someAll].m0_node_idx;
                            u1_doc_cutoff_someAll[count_someAll].condition = 2;
                            if (txtcomment_qacutoff.Text == "")
                            {
                                u1_doc_cutoff_someAll[count_someAll].comment = "-";
                            }
                            else
                            {
                                u1_doc_cutoff_someAll[count_someAll].comment = txtcomment_qacutoff.Text;
                            }

                            if (STARTNODEQACUTOFF == int.Parse(lblDecisionQACutoffView2.Text))
                            {
                                u1_doc_cutoff_someAll[count_someAll].status = 0;
                            }
                            else if (STARTNODEQACUTOFF == int.Parse(lblDecisionQACutoffView.Text))
                            {
                                u1_doc_cutoff_someAll[count_someAll].status = 1;
                            }
                            u1_doc_cutoff_someAll[count_someAll].cemp_idx = _u1tempListEquipmentHolderCutoff[count_someAll].cemp_idx;


                            count_row_check++;
                            count_someAll++;

                            ViewState["count_row_check"] = count_row_check;

                        }
                        else
                        {
                            u1_doc_cutoff_someAll[count_someAll] = new qa_cims_u1_document_device_detail();
                            u1_doc_cutoff_someAll[count_someAll].u1_device_idx = int.Parse(lblDeviceIDX.Text);
                            if (STARTNODEQACUTOFF == int.Parse(lblDecisionQACutoffView2.Text))
                            {
                                u1_doc_cutoff_someAll[count_someAll].decision_idx = int.Parse(lblDecisionQACutoffView.Text);
                                u1_doc_cutoff_someAll[count_someAll].status = 1;
                            }
                            else if (STARTNODEQACUTOFF == int.Parse(lblDecisionQACutoffView.Text))
                            {
                                u1_doc_cutoff_someAll[count_someAll].decision_idx = int.Parse(lblDecisionQACutoffView2.Text);
                                u1_doc_cutoff_someAll[count_someAll].status = 0;
                            }
                            u1_doc_cutoff_someAll[count_someAll].condition = 2;
                            u1_doc_cutoff_someAll[count_someAll].u0_device_idx = _u1tempListEquipmentHolderCutoff[count_someAll].u0_device_idx;
                            u1_doc_cutoff_someAll[count_someAll].m0_actor_idx = _u1tempListEquipmentHolderCutoff[count_someAll].m0_actor_idx;
                            u1_doc_cutoff_someAll[count_someAll].m0_node_idx = _u1tempListEquipmentHolderCutoff[count_someAll].m0_node_idx;
                            if (txtcomment_qacutoff.Text == "")
                            {
                                u1_doc_cutoff_someAll[count_someAll].comment = "-";
                            }
                            else
                            {
                                u1_doc_cutoff_someAll[count_someAll].comment = txtcomment_qacutoff.Text;
                            }
                            //u1_doc_cutoff_someAll[count_someAll].status = 9;
                            u1_doc_cutoff_someAll[count_someAll].cemp_idx = _u1tempListEquipmentHolderCutoff[count_someAll].cemp_idx;

                            count_someAll++;
                        }
                    }
                    if (int.Parse(ViewState["count_row_check"].ToString()) == EquipmentQACutoff.Rows.Count)
                    {
                        _data_qa_cims.qa_cims_u0_document_device_list = new qa_cims_u0_document_device_detail[1];
                        qa_cims_u0_document_device_detail _selectDicision = new qa_cims_u0_document_device_detail();
                        _selectDicision.decision_idx = STARTNODEQACUTOFF;
                        _selectDicision.m0_actor_idx = _u1tempListEquipmentHolderCutoffHead[0].m0_actor_idx;
                        _selectDicision.m0_node_idx = _u1tempListEquipmentHolderCutoffHead[0].m0_node_idx;
                        _selectDicision.u0_device_idx = _u1tempListEquipmentHolderCutoffHead[0].u0_device_idx;
                        _selectDicision.emp_idx_create = _emp_idx;
                        _data_qa_cims.qa_cims_u0_document_device_list[0] = _selectDicision;
                    }
                    else
                    {
                        _data_qa_cims.qa_cims_u0_document_device_list = new qa_cims_u0_document_device_detail[1];
                        qa_cims_u0_document_device_detail _selectDicision = new qa_cims_u0_document_device_detail();
                        _selectDicision.decision_idx = int.Parse(lblDecisionQACutoffView.Text);
                        _selectDicision.m0_actor_idx = _u1tempListEquipmentHolderCutoffHead[0].m0_actor_idx;
                        _selectDicision.m0_node_idx = _u1tempListEquipmentHolderCutoffHead[0].m0_node_idx;
                        _selectDicision.u0_device_idx = _u1tempListEquipmentHolderCutoffHead[0].u0_device_idx;
                        _selectDicision.emp_idx_create = _emp_idx;
                        _data_qa_cims.qa_cims_u0_document_device_list[0] = _selectDicision;
                    }

                    _data_qa_cims.qa_cims_u1_document_device_list = u1_doc_cutoff_someAll;
                    _data_qa_cims = callServicePostQaDocCIMS(_urlCimsSetDocumentUpdate, _data_qa_cims);
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_qa_cims));

                    divdocumentList.Visible = true;
                    divFVVQACutoff.Visible = false;

                    CheckAllQACutoff.Checked = false;
                    btnBackToDocList.Visible = false;
                    divDocDetailUser.Visible = false;

                    //litDebug.Text = "We Here 2";

                    //data_qa_cims renew = new data_qa_cims();
                    //renew.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
                    //qa_cims_u1_document_device_detail _selectu0DocList = new qa_cims_u1_document_device_detail();

                    //renew.qa_cims_u1_document_device_list[0] = _selectu0DocList;

                    //renew = callServicePostQaDocCIMS(_urlCimsGetEquipmentList, renew);

                    //setGridData(gvDocumentList, renew.qa_cims_u0_document_device_list);
                    //setActiveTab("docList", 0, 0, 0, 0, 0, 0);
                    setActiveTab("docList", 0, 0, 0, int.Parse(tbPlaceIDXInDocument.Text), 0, 0);
                }

                //litDebug.Text += STARTNODEQACUTOFF;

                break;

            case "cmdBackToDocList":
                //BackToCurrentPageGvDocumentList(int.Parse(tbPlaceIDXInDocument.Text));
                //setActiveTab("docList", 0, 0, 0, int.Parse(tbPlaceIDXInDocument.Text), 0, 0);

                setActiveTab("docList", 0, 0, 0, 0, 0, 0);
                setOntop.Focus();

                break;

            case "cmdHeadQAApprove":

                string[] cmdArgDecisionHeadQA = cmdArg.Split(',');
                int DOCUMENTTYPEHeadQA = int.TryParse(cmdArgDecisionHeadQA[0].ToString(), out _default_int) ? int.Parse(cmdArgDecisionHeadQA[0].ToString()) : _default_int;
                int SETTYPEDECISIONHeadQA = int.TryParse(cmdArgDecisionHeadQA[1].ToString(), out _default_int) ? int.Parse(cmdArgDecisionHeadQA[1].ToString()) : _default_int;
                int U0DOCHEADAPPROVEHeadQA = int.TryParse(cmdArgDecisionHeadQA[2].ToString(), out _default_int) ? int.Parse(cmdArgDecisionHeadQA[2].ToString()) : _default_int;
                int U0NODEQA = int.TryParse(cmdArgDecisionHeadQA[3].ToString(), out _default_int) ? int.Parse(cmdArgDecisionHeadQA[3].ToString()) : _default_int;
                int U0ACTORQA = int.TryParse(cmdArgDecisionHeadQA[4].ToString(), out _default_int) ? int.Parse(cmdArgDecisionHeadQA[4].ToString()) : _default_int;
                int RowCountQA = int.TryParse(cmdArgDecisionHeadQA[5].ToString(), out _default_int) ? int.Parse(cmdArgDecisionHeadQA[5].ToString()) : _default_int;

                //litDebug.Text = RowCount.ToString();

                var DecisionIDXHeadQA2 = (gvDocumentList.Rows[RowCountQA].FindControl("lblDecisionIDXHeadQA2") as Label);
                var DecisionIDXHeadQA = (gvDocumentList.Rows[RowCountQA].FindControl("lblDecisionIDXHeadQA") as Label);


                switch (SETTYPEDECISIONHeadQA) //ใช้แยกปุ่มอนุมัติกับไม่อนุมัติ
                {
                    case 0: //NotApprove
                        switch (DOCUMENTTYPEHeadQA)
                        {
                            case 1:
                            case 2:
                            case 3:
                                data_qa_cims _data_tranfer = new data_qa_cims();
                                _data_tranfer.qa_cims_u0_document_device_list = new qa_cims_u0_document_device_detail[1];
                                qa_cims_u0_document_device_detail _headApprovetranfer = new qa_cims_u0_document_device_detail();

                                _headApprovetranfer.u0_device_idx = U0DOCHEADAPPROVEHeadQA;
                                _headApprovetranfer.document_type_idx = DOCUMENTTYPEHeadQA;
                                _headApprovetranfer.emp_idx_create = _emp_idx;
                                _headApprovetranfer.m0_node_idx = U0NODEQA;
                                _headApprovetranfer.m0_actor_idx = U0ACTORQA;
                                _headApprovetranfer.decision_idx = int.Parse(DecisionIDXHeadQA2.Text);

                                _data_tranfer.qa_cims_u0_document_device_list[0] = _headApprovetranfer;

                                //_data_qa_cims = callServicePostQaDocCIMS(_urlCimsSetDocumentUpdateHeadQA, _data_tranfer);

                                data_qa_cims _data_qa_tranfer_select = new data_qa_cims();
                                _data_qa_tranfer_select.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
                                qa_cims_u1_document_device_detail _selectEquipmentTranferToUpdate = new qa_cims_u1_document_device_detail();
                                _selectEquipmentTranferToUpdate.condition = 2;
                                _selectEquipmentTranferToUpdate.u0_device_idx = U0DOCHEADAPPROVEHeadQA;
                                _data_qa_tranfer_select.qa_cims_u1_document_device_list[0] = _selectEquipmentTranferToUpdate;

                                _data_qa_tranfer_select = callServicePostQaDocCIMS(_urlCimsGetDocEquipmentList, _data_qa_tranfer_select);
                                ViewState["DocEquipmentListUpdate"] = "";
                                ViewState["DocEquipmentListUpdate"] = _data_qa_tranfer_select.qa_cims_u1_document_device_list;
                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_qa_cutoff.qa_cims_u1_document_device_list));

                                setGridData(gvPassU1, ViewState["DocEquipmentListUpdate"]);


                                qa_cims_u1_document_device_detail[] _u1tempListEquipmentHolderToUpdate = (qa_cims_u1_document_device_detail[])ViewState["DocEquipmentListUpdate"];
                                var u1_doc_tranfer_to_update = new qa_cims_u1_document_device_detail[_u1tempListEquipmentHolderToUpdate.Count()];
                                int count_someAll = 0;


                                foreach (GridViewRow rowItems in gvPassU1.Rows)
                                {
                                    Label lblEquipment_U1IDX = (Label)rowItems.FindControl("lblEquipment_U1IDX");
                                    Label lblEquipment_Staidx = (Label)rowItems.FindControl("lblEquipment_Staidx");

                                    if (lblEquipment_Staidx.Text != "30")
                                    {
                                        u1_doc_tranfer_to_update[count_someAll] = new qa_cims_u1_document_device_detail();
                                        u1_doc_tranfer_to_update[count_someAll].u1_device_idx = int.Parse(lblEquipment_U1IDX.Text);
                                        u1_doc_tranfer_to_update[count_someAll].u0_device_idx = _u1tempListEquipmentHolderToUpdate[count_someAll].u0_device_idx;
                                    }
                                    count_someAll++;
                                }

                                _data_tranfer.qa_cims_u1_document_device_list = u1_doc_tranfer_to_update;
                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_tranfer));

                                _data_tranfer = callServicePostQaDocCIMS(_urlCimsSetDocumentUpdateHeadQA, _data_tranfer);

                                setActiveTab("docList", 0, 0, 0, 0, 0, 0);

                                ////BackToCurrentPageGvDocumentList(int.Parse(tbPlaceIDXInDocument.Text));
                                //setActiveTab("docList", 0, 0, 0, int.Parse(tbPlaceIDXInDocument.Text), 0, 0);
                                break;
                            case 4:
                                data_qa_cims _data_cutoff = new data_qa_cims();
                                _data_cutoff.qa_cims_u0_document_device_list = new qa_cims_u0_document_device_detail[1];
                                qa_cims_u0_document_device_detail _headApprove = new qa_cims_u0_document_device_detail();

                                _headApprove.u0_device_idx = U0DOCHEADAPPROVEHeadQA;
                                _headApprove.document_type_idx = DOCUMENTTYPEHeadQA;
                                _headApprove.emp_idx_create = _emp_idx;
                                _headApprove.m0_node_idx = U0NODEQA;
                                _headApprove.m0_actor_idx = U0ACTORQA;
                                _headApprove.decision_idx = int.Parse(DecisionIDXHeadQA2.Text);

                                _data_cutoff.qa_cims_u0_document_device_list[0] = _headApprove;

                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_cutoff));
                                //_data_qa_cims = callServicePostQaDocCIMS(_urlCimsSetDocumentUpdateHeadQA, _data_cutoff);

                                data_qa_cims _data_qa_cutoff_select = new data_qa_cims();
                                _data_qa_cutoff_select.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
                                qa_cims_u1_document_device_detail _selectEquipmentCutoffToUpdate = new qa_cims_u1_document_device_detail();
                                _selectEquipmentCutoffToUpdate.condition = 1;
                                _selectEquipmentCutoffToUpdate.u0_device_idx = U0DOCHEADAPPROVEHeadQA;
                                _data_qa_cutoff_select.qa_cims_u1_document_device_list[0] = _selectEquipmentCutoffToUpdate;

                                _data_qa_cutoff_select = callServicePostQaDocCIMS(_urlCimsGetDocEquipmentList, _data_qa_cutoff_select);
                                ViewState["DocEquipmentListUpdate"] = "";
                                ViewState["DocEquipmentListUpdate"] = _data_qa_cutoff_select.qa_cims_u1_document_device_list;
                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_qa_cutoff_select.qa_cims_u1_document_device_list));

                                setGridData(gvPassU1, ViewState["DocEquipmentListUpdate"]);


                                qa_cims_u1_document_device_detail[] _u1tempListEquipmentHolderToUpdateHSCutoff = (qa_cims_u1_document_device_detail[])ViewState["DocEquipmentListUpdate"];
                                var u1_doc_tranfer_to_update_HS_Cutoff = new qa_cims_u1_document_device_detail[_u1tempListEquipmentHolderToUpdateHSCutoff.Count()];
                                int count_someAll_HS_cutoff = 0;


                                foreach (GridViewRow rowItems in gvPassU1.Rows)
                                {
                                    Label lblEquipment_U1IDX = (Label)rowItems.FindControl("lblEquipment_U1IDX");
                                    Label lblEquipment_Staidx = (Label)rowItems.FindControl("lblEquipment_Staidx");

                                    if (lblEquipment_Staidx.Text != "30")
                                    {
                                        u1_doc_tranfer_to_update_HS_Cutoff[count_someAll_HS_cutoff] = new qa_cims_u1_document_device_detail();
                                        u1_doc_tranfer_to_update_HS_Cutoff[count_someAll_HS_cutoff].u1_device_idx = int.Parse(lblEquipment_U1IDX.Text);
                                        u1_doc_tranfer_to_update_HS_Cutoff[count_someAll_HS_cutoff].u0_device_idx = _u1tempListEquipmentHolderToUpdateHSCutoff[count_someAll_HS_cutoff].u0_device_idx;
                                    }
                                    else
                                    {

                                    }
                                    count_someAll_HS_cutoff++;
                                }

                                _data_cutoff.qa_cims_u1_document_device_list = u1_doc_tranfer_to_update_HS_Cutoff;
                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_cutoff));

                                _data_cutoff = callServicePostQaDocCIMS(_urlCimsSetDocumentUpdateHeadQA, _data_cutoff);

                                setActiveTab("docList", 0, 0, 0, 0, 0, 0);
                                //BackToCurrentPageGvDocumentList(int.Parse(tbPlaceIDXInDocument.Text));
                                //setActiveTab("docList", 0, 0, 0, int.Parse(tbPlaceIDXInDocument.Text), 0, 0);
                                break;
                        }
                        break;

                    case 1: //Approve
                        switch (DOCUMENTTYPEHeadQA)
                        {
                            case 0:
                            case 1:
                            case 2:
                            case 3:
                                data_qa_cims _data_tranfer = new data_qa_cims();
                                _data_tranfer.qa_cims_u0_document_device_list = new qa_cims_u0_document_device_detail[1];
                                qa_cims_u0_document_device_detail _headApprovetranfer = new qa_cims_u0_document_device_detail();

                                _headApprovetranfer.u0_device_idx = U0DOCHEADAPPROVEHeadQA;
                                _headApprovetranfer.document_type_idx = DOCUMENTTYPEHeadQA;
                                _headApprovetranfer.emp_idx_create = _emp_idx;
                                _headApprovetranfer.m0_node_idx = U0NODEQA;
                                _headApprovetranfer.m0_actor_idx = U0ACTORQA;
                                _headApprovetranfer.decision_idx = int.Parse(DecisionIDXHeadQA.Text);
                                _headApprovetranfer.rdept_idx_create = int.Parse(ViewState["rdept_permission"].ToString());
                                _headApprovetranfer.rsec_idx_create = int.Parse(ViewState["rsec_permission"].ToString());


                                //m0_devics_Index.rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
                                //m0_devics_Index.rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
                                _data_tranfer.qa_cims_u0_document_device_list[0] = _headApprovetranfer;

                                //_data_qa_cims = callServicePostQaDocCIMS(_urlCimsSetDocumentUpdateHeadQA, _data_tranfer);

                                data_qa_cims _data_qa_tranfer_select = new data_qa_cims();
                                _data_qa_tranfer_select.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
                                qa_cims_u1_document_device_detail _selectEquipmentTranferToUpdate = new qa_cims_u1_document_device_detail();
                                _selectEquipmentTranferToUpdate.condition = 2;
                                _selectEquipmentTranferToUpdate.u0_device_idx = U0DOCHEADAPPROVEHeadQA;
                                _data_qa_tranfer_select.qa_cims_u1_document_device_list[0] = _selectEquipmentTranferToUpdate;

                                _data_qa_tranfer_select = callServicePostQaDocCIMS(_urlCimsGetDocEquipmentList, _data_qa_tranfer_select);
                                ViewState["DocEquipmentListUpdate"] = "";
                                ViewState["DocEquipmentListUpdate"] = _data_qa_tranfer_select.qa_cims_u1_document_device_list;
                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_qa_cutoff.qa_cims_u1_document_device_list));

                                setGridData(gvPassU1, ViewState["DocEquipmentListUpdate"]);


                                qa_cims_u1_document_device_detail[] _u1tempListEquipmentHolderToUpdate = (qa_cims_u1_document_device_detail[])ViewState["DocEquipmentListUpdate"];
                                var u1_doc_tranfer_to_update = new qa_cims_u1_document_device_detail[_u1tempListEquipmentHolderToUpdate.Count()];
                                int count_someAll = 0;


                                foreach (GridViewRow rowItems in gvPassU1.Rows)
                                {
                                    Label lblEquipment_U1IDX = (Label)rowItems.FindControl("lblEquipment_U1IDX");
                                    Label lblEquipment_Staidx = (Label)rowItems.FindControl("lblEquipment_Staidx");

                                    if (lblEquipment_Staidx.Text != "29")
                                    {
                                        u1_doc_tranfer_to_update[count_someAll] = new qa_cims_u1_document_device_detail();
                                        u1_doc_tranfer_to_update[count_someAll].u1_device_idx = int.Parse(lblEquipment_U1IDX.Text);
                                        u1_doc_tranfer_to_update[count_someAll].u0_device_idx = _u1tempListEquipmentHolderToUpdate[count_someAll].u0_device_idx;
                                    }
                                    else
                                    {

                                    }
                                    count_someAll++;
                                }

                                //_data_tranfer.qa_cims_u0_document_device_list[0] = _headApprovetranfer;
                                _data_tranfer.qa_cims_u1_document_device_list = u1_doc_tranfer_to_update;
                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_tranfer));

                                _data_tranfer = callServicePostQaDocCIMS(_urlCimsSetDocumentUpdateHeadQA, _data_tranfer);

                                setActiveTab("docList", 0, 0, 0, 0, 0, 0);
                                //BackToCurrentPageGvDocumentList(int.Parse(tbPlaceIDXInDocument.Text));
                                //setActiveTab("docList", 0, 0, 0, int.Parse(tbPlaceIDXInDocument.Text), 0, 0);
                                break;
                            case 4:
                                data_qa_cims _data_cutoff = new data_qa_cims();
                                _data_cutoff.qa_cims_u0_document_device_list = new qa_cims_u0_document_device_detail[1];
                                qa_cims_u0_document_device_detail _headApprove = new qa_cims_u0_document_device_detail();

                                _headApprove.u0_device_idx = U0DOCHEADAPPROVEHeadQA;
                                _headApprove.document_type_idx = DOCUMENTTYPEHeadQA;
                                _headApprove.emp_idx_create = _emp_idx;
                                _headApprove.m0_node_idx = U0NODEQA;
                                _headApprove.m0_actor_idx = U0ACTORQA;
                                _headApprove.decision_idx = int.Parse(DecisionIDXHeadQA.Text);

                                _data_cutoff.qa_cims_u0_document_device_list[0] = _headApprove;

                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_cutoff));
                                //_data_qa_cims = callServicePostQaDocCIMS(_urlCimsSetDocumentUpdateHeadQA, _data_cutoff);

                                data_qa_cims _data_qa_cutoff_select = new data_qa_cims();
                                _data_qa_cutoff_select.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
                                qa_cims_u1_document_device_detail _selectEquipmentCutoffToUpdate = new qa_cims_u1_document_device_detail();
                                _selectEquipmentCutoffToUpdate.condition = 1;
                                _selectEquipmentCutoffToUpdate.u0_device_idx = U0DOCHEADAPPROVEHeadQA;
                                _data_qa_cutoff_select.qa_cims_u1_document_device_list[0] = _selectEquipmentCutoffToUpdate;

                                _data_qa_cutoff_select = callServicePostQaDocCIMS(_urlCimsGetDocEquipmentList, _data_qa_cutoff_select);
                                ViewState["DocEquipmentListUpdate"] = "";
                                ViewState["DocEquipmentListUpdate"] = _data_qa_cutoff_select.qa_cims_u1_document_device_list;
                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_qa_cutoff_select.qa_cims_u1_document_device_list));

                                setGridData(gvPassU1, ViewState["DocEquipmentListUpdate"]);


                                qa_cims_u1_document_device_detail[] _u1tempListEquipmentHolderToUpdateHSCutoff = (qa_cims_u1_document_device_detail[])ViewState["DocEquipmentListUpdate"];
                                var u1_doc_tranfer_to_update_HS_Cutoff = new qa_cims_u1_document_device_detail[_u1tempListEquipmentHolderToUpdateHSCutoff.Count()];
                                int count_someAll_HS_cutoff = 0;


                                foreach (GridViewRow rowItems in gvPassU1.Rows)
                                {
                                    Label lblEquipment_U1IDX = (Label)rowItems.FindControl("lblEquipment_U1IDX");
                                    Label lblEquipment_Staidx = (Label)rowItems.FindControl("lblEquipment_Staidx");

                                    if (lblEquipment_Staidx.Text != "30")
                                    {
                                        u1_doc_tranfer_to_update_HS_Cutoff[count_someAll_HS_cutoff] = new qa_cims_u1_document_device_detail();
                                        u1_doc_tranfer_to_update_HS_Cutoff[count_someAll_HS_cutoff].u1_device_idx = int.Parse(lblEquipment_U1IDX.Text);
                                        u1_doc_tranfer_to_update_HS_Cutoff[count_someAll_HS_cutoff].u0_device_idx = _u1tempListEquipmentHolderToUpdateHSCutoff[count_someAll_HS_cutoff].u0_device_idx;
                                    }
                                    else
                                    {

                                    }
                                    count_someAll_HS_cutoff++;
                                }

                                _data_cutoff.qa_cims_u1_document_device_list = u1_doc_tranfer_to_update_HS_Cutoff;
                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_cutoff));

                                _data_cutoff = callServicePostQaDocCIMS(_urlCimsSetDocumentUpdateHeadQA, _data_cutoff);

                                setActiveTab("docList", 0, 0, 0, 0, 0, 0);
                                //BackToCurrentPageGvDocumentList(int.Parse(tbPlaceIDXInDocument.Text));
                                //setActiveTab("docList", 0, 0, 0, int.Parse(tbPlaceIDXInDocument.Text), 0, 0);
                                break;

                        }
                        break;
                }

                break;

            case "cmdReceiverApprove":
                string[] cmdArgDecisionReceiver = cmdArg.Split(',');
                int DOCUMENTTYPEReceiver = int.TryParse(cmdArgDecisionReceiver[0].ToString(), out _default_int) ? int.Parse(cmdArgDecisionReceiver[0].ToString()) : _default_int;
                int SETTYPEDECISIONReceiver = int.TryParse(cmdArgDecisionReceiver[1].ToString(), out _default_int) ? int.Parse(cmdArgDecisionReceiver[1].ToString()) : _default_int;
                int U0DOCHEADAPPROVEReceiver = int.TryParse(cmdArgDecisionReceiver[2].ToString(), out _default_int) ? int.Parse(cmdArgDecisionReceiver[2].ToString()) : _default_int;
                int U0NODEReceiver = int.TryParse(cmdArgDecisionReceiver[3].ToString(), out _default_int) ? int.Parse(cmdArgDecisionReceiver[3].ToString()) : _default_int;
                int U0ACTORReceiver = int.TryParse(cmdArgDecisionReceiver[4].ToString(), out _default_int) ? int.Parse(cmdArgDecisionReceiver[4].ToString()) : _default_int;
                int RowCountReceiver = int.TryParse(cmdArgDecisionReceiver[5].ToString(), out _default_int) ? int.Parse(cmdArgDecisionReceiver[5].ToString()) : _default_int;

                //litDebug.Text = RowCount.ToString();

                var DecisionIDXReceiver2 = (gvDocumentList.Rows[RowCountReceiver].FindControl("lblDecisionIDXReceiver2") as Label);
                var DecisionIDXReceiver = (gvDocumentList.Rows[RowCountReceiver].FindControl("lblDecisionIDXReceiver") as Label);


                switch (SETTYPEDECISIONReceiver)
                {
                    case 0:
                        switch (DOCUMENTTYPEReceiver)
                        {
                            case 1:
                            case 2:
                            case 3:
                                data_qa_cims _data_tranfer = new data_qa_cims();
                                _data_tranfer.qa_cims_u0_document_device_list = new qa_cims_u0_document_device_detail[1];
                                qa_cims_u0_document_device_detail _headApprovetranfer = new qa_cims_u0_document_device_detail();

                                _headApprovetranfer.u0_device_idx = U0DOCHEADAPPROVEReceiver;
                                _headApprovetranfer.document_type_idx = DOCUMENTTYPEReceiver;
                                _headApprovetranfer.emp_idx_create = _emp_idx;
                                _headApprovetranfer.m0_node_idx = U0NODEReceiver;
                                _headApprovetranfer.m0_actor_idx = U0ACTORReceiver;
                                _headApprovetranfer.decision_idx = int.Parse(DecisionIDXReceiver2.Text);

                                _data_tranfer.qa_cims_u0_document_device_list[0] = _headApprovetranfer;

                                //_data_qa_cims = callServicePostQaDocCIMS(_urlCimsSetDocumentUpdateReceiver, _data_tranfer);

                                data_qa_cims _data_qa_tranfer_select = new data_qa_cims();
                                _data_qa_tranfer_select.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
                                qa_cims_u1_document_device_detail _selectEquipmentTranferToUpdate = new qa_cims_u1_document_device_detail();
                                _selectEquipmentTranferToUpdate.condition = 2;
                                _selectEquipmentTranferToUpdate.u0_device_idx = U0DOCHEADAPPROVEReceiver;
                                _data_qa_tranfer_select.qa_cims_u1_document_device_list[0] = _selectEquipmentTranferToUpdate;

                                _data_qa_tranfer_select = callServicePostQaDocCIMS(_urlCimsGetDocEquipmentList, _data_qa_tranfer_select);
                                ViewState["DocEquipmentListUpdate"] = "";
                                ViewState["DocEquipmentListUpdate"] = _data_qa_tranfer_select.qa_cims_u1_document_device_list;
                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_qa_cutoff.qa_cims_u1_document_device_list));

                                setGridData(gvPassU1, ViewState["DocEquipmentListUpdate"]);


                                qa_cims_u1_document_device_detail[] _u1tempListEquipmentHolderToUpdate = (qa_cims_u1_document_device_detail[])ViewState["DocEquipmentListUpdate"];
                                var u1_doc_tranfer_to_update = new qa_cims_u1_document_device_detail[_u1tempListEquipmentHolderToUpdate.Count()];
                                int count_someAll = 0;


                                foreach (GridViewRow rowItems in gvPassU1.Rows)
                                {
                                    Label lblEquipment_U1IDX = (Label)rowItems.FindControl("lblEquipment_U1IDX");

                                    u1_doc_tranfer_to_update[count_someAll] = new qa_cims_u1_document_device_detail();
                                    u1_doc_tranfer_to_update[count_someAll].u1_device_idx = int.Parse(lblEquipment_U1IDX.Text);
                                    u1_doc_tranfer_to_update[count_someAll].u0_device_idx = _u1tempListEquipmentHolderToUpdate[count_someAll].u0_device_idx;
                                    if (_u1tempListEquipmentHolderToUpdate[count_someAll].status == 1)
                                    {
                                        u1_doc_tranfer_to_update[count_someAll].decision_idx = int.Parse(DecisionIDXReceiver.Text);
                                    }
                                    else if (_u1tempListEquipmentHolderToUpdate[count_someAll].status == 0)
                                    {
                                        u1_doc_tranfer_to_update[count_someAll].decision_idx = int.Parse(DecisionIDXReceiver2.Text);
                                    }
                                    count_someAll++;
                                }

                                _data_tranfer.qa_cims_u1_document_device_list = u1_doc_tranfer_to_update;
                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_tranfer));
                                //litDebug.Text = U0DOCHEADAPPROVEReceiver.ToString();
                                _data_tranfer = callServicePostQaDocCIMS(_urlCimsSetDocumentUpdateHeadQA, _data_tranfer);

                                setActiveTab("docList", 0, 0, 0, 0, 0, 0);
                                //BackToCurrentPageGvDocumentList(int.Parse(tbPlaceIDXInDocument.Text));
                                //setActiveTab("docList", 0, 0, 0, int.Parse(tbPlaceIDXInDocument.Text), 0, 0);
                                break;
                        }
                        break;

                    case 1:
                        switch (DOCUMENTTYPEReceiver)
                        {
                            case 0:
                            case 1:
                            case 2:
                            case 3:
                                data_qa_cims _data_tranfer = new data_qa_cims();
                                _data_tranfer.qa_cims_u0_document_device_list = new qa_cims_u0_document_device_detail[1];
                                qa_cims_u0_document_device_detail _headApprovetranfer = new qa_cims_u0_document_device_detail();

                                _headApprovetranfer.u0_device_idx = U0DOCHEADAPPROVEReceiver;
                                _headApprovetranfer.document_type_idx = DOCUMENTTYPEReceiver;
                                _headApprovetranfer.emp_idx_create = _emp_idx;
                                _headApprovetranfer.m0_node_idx = U0NODEReceiver;
                                _headApprovetranfer.m0_actor_idx = U0ACTORReceiver;
                                _headApprovetranfer.decision_idx = int.Parse(DecisionIDXReceiver.Text);

                                _data_tranfer.qa_cims_u0_document_device_list[0] = _headApprovetranfer;

                                //_data_qa_cims = callServicePostQaDocCIMS(_urlCimsSetDocumentUpdateReceiver, _data_tranfer);

                                data_qa_cims _data_qa_tranfer_select = new data_qa_cims();
                                _data_qa_tranfer_select.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
                                qa_cims_u1_document_device_detail _selectEquipmentTranferToUpdate = new qa_cims_u1_document_device_detail();
                                _selectEquipmentTranferToUpdate.condition = 2;
                                _selectEquipmentTranferToUpdate.u0_device_idx = U0DOCHEADAPPROVEReceiver;
                                _data_qa_tranfer_select.qa_cims_u1_document_device_list[0] = _selectEquipmentTranferToUpdate;

                                _data_qa_tranfer_select = callServicePostQaDocCIMS(_urlCimsGetDocEquipmentList, _data_qa_tranfer_select);
                                ViewState["DocEquipmentListUpdate"] = "";
                                ViewState["DocEquipmentListUpdate"] = _data_qa_tranfer_select.qa_cims_u1_document_device_list;
                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_qa_cutoff.qa_cims_u1_document_device_list));

                                setGridData(gvPassU1, ViewState["DocEquipmentListUpdate"]);


                                qa_cims_u1_document_device_detail[] _u1tempListEquipmentHolderToUpdate = (qa_cims_u1_document_device_detail[])ViewState["DocEquipmentListUpdate"];
                                var u1_doc_tranfer_to_update = new qa_cims_u1_document_device_detail[_u1tempListEquipmentHolderToUpdate.Count()];
                                int count_someAll = 0;


                                foreach (GridViewRow rowItems in gvPassU1.Rows)
                                {
                                    Label lblEquipment_U1IDX = (Label)rowItems.FindControl("lblEquipment_U1IDX");

                                    u1_doc_tranfer_to_update[count_someAll] = new qa_cims_u1_document_device_detail();
                                    u1_doc_tranfer_to_update[count_someAll].u1_device_idx = int.Parse(lblEquipment_U1IDX.Text);
                                    u1_doc_tranfer_to_update[count_someAll].u0_device_idx = _u1tempListEquipmentHolderToUpdate[count_someAll].u0_device_idx;
                                    if (_u1tempListEquipmentHolderToUpdate[count_someAll].status == 1)
                                    {
                                        u1_doc_tranfer_to_update[count_someAll].decision_idx = int.Parse(DecisionIDXReceiver.Text);
                                    }
                                    else if (_u1tempListEquipmentHolderToUpdate[count_someAll].status == 0)
                                    {
                                        u1_doc_tranfer_to_update[count_someAll].decision_idx = int.Parse(DecisionIDXReceiver2.Text);
                                    }
                                    count_someAll++;
                                }

                                _data_tranfer.qa_cims_u1_document_device_list = u1_doc_tranfer_to_update;
                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_tranfer));

                                _data_tranfer = callServicePostQaDocCIMS(_urlCimsSetDocumentUpdateHeadQA, _data_tranfer);

                                setActiveTab("docList", 0, 0, 0, 0, 0, 0);
                                //BackToCurrentPageGvDocumentList(int.Parse(tbPlaceIDXInDocument.Text));
                                //setActiveTab("docList", 0, 0, 0, int.Parse(tbPlaceIDXInDocument.Text), 0, 0);
                                break;
                        }
                        break;
                }
                break;

            case "cmdViewQATranfer":

                string[] cmdArgDecisionQATranfer = cmdArg.Split(',');

                int thorwU0_device_idx_TF = int.TryParse(cmdArgDecisionQATranfer[0].ToString(), out _default_int) ? int.Parse(cmdArgDecisionQATranfer[0].ToString()) : _default_int;
                int RowCountQATranfer = int.TryParse(cmdArgDecisionQATranfer[1].ToString(), out _default_int) ? int.Parse(cmdArgDecisionQATranfer[1].ToString()) : _default_int;

                int RowStatusQATranfer = int.TryParse(cmdArgDecisionQATranfer[2].ToString(), out _default_int) ? int.Parse(cmdArgDecisionQATranfer[2].ToString()) : _default_int;

                //litDebug.Text = thorwU0_device_idx.ToString(); 

                var DecisionQA2 = (gvDocumentList.Rows[RowCountQATranfer].FindControl("lblDecisionQA2") as Label);
                var DecisionQA = (gvDocumentList.Rows[RowCountQATranfer].FindControl("lblDecisionQA") as Label);


                selectLogQATranfer(thorwU0_device_idx_TF.ToString());

                divTranferRegistration.Visible = true;
                divDocDetailUser.Visible = true;
                btnBackToDocList.Visible = true;
                divFVVQATranfer.Visible = true;
                div_detailTranfer.Visible = true;


                div_SearchDoc.Visible = false;
                divdocumentList.Visible = false;
                divRegistrationList.Visible = false;
                //divFVVQACutoff.Visible = true;                                                       
                divSearchDocument.Visible = false;

                CheckAllQAApproveTranfer.Checked = false;
                CheckAllQANotApproveTranfer.Checked = false;


                setFormData(fvDocDetailUser, FormViewMode.ReadOnly, ((data_employee)ViewState["vsEmpProfile"]).employee_list);

                data_qa_cims data_select_Creator_detailsTF = new data_qa_cims();
                data_select_Creator_detailsTF.qa_cims_u0_document_device_list = new qa_cims_u0_document_device_detail[1];
                qa_cims_u0_document_device_detail _selectDetailsCutoffTF = new qa_cims_u0_document_device_detail();
                _selectDetailsCutoffTF.u0_device_idx = thorwU0_device_idx_TF;
                data_select_Creator_detailsTF.qa_cims_u0_document_device_list[0] = _selectDetailsCutoffTF;

                data_select_Creator_detailsTF = callServicePostQaDocCIMS(_urlCimsGetDetailCreator, data_select_Creator_detailsTF);

                setFormData(fvDocDetailCreator, FormViewMode.ReadOnly, (data_select_Creator_detailsTF.qa_cims_u0_document_device_list));


                data_qa_cims data_qa_tranfer = new data_qa_cims();
                data_qa_tranfer.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
                qa_cims_u1_document_device_detail _selectEquipmentTranfer = new qa_cims_u1_document_device_detail();
                _selectEquipmentTranfer.condition = 2;
                _selectEquipmentTranfer.u0_device_idx = thorwU0_device_idx_TF;
                data_qa_tranfer.qa_cims_u1_document_device_list[0] = _selectEquipmentTranfer;

                data_qa_tranfer = callServicePostQaDocCIMS(_urlCimsGetDocEquipmentList, data_qa_tranfer);

                ViewState["DocEquipmentList"] = data_qa_tranfer.qa_cims_u1_document_device_list;
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_qa_cutoff.qa_cims_u1_document_device_list));

                qa_cims_u1_document_device_detail[] _temp_status18_qa_approve = ((qa_cims_u1_document_device_detail[])ViewState["DocEquipmentList"]);

                var _linq_qa_approve = from data_qa_cims_approve in _temp_status18_qa_approve
                                       where data_qa_cims_approve.status == 1
                                       select data_qa_cims_approve;


                ViewState["vs_qa_approve_tranfer"] = _linq_qa_approve.ToList();

                setGridData(gvEquipmentQATranfer, ViewState["vs_qa_approve_tranfer"]);

                var _linq_status_qa_detailnotapprove = (from data_qa_not_approve_tranfer in _temp_status18_qa_approve
                                                            //  where data_SerialNumber.device_serial == txtName.Text
                                                        where data_qa_not_approve_tranfer.status == 0//.Contains(txtName.Text)
                                                        select data_qa_not_approve_tranfer
                                                   );

                ViewState["vs_qa_not_approve_tranfer"] = _linq_status_qa_detailnotapprove.ToList();
                setGridData(gvDetailTranfer, ViewState["vs_qa_not_approve_tranfer"]);


                data_qa_cims _dataCimsDecisionNodeQATranfer = new data_qa_cims();
                _dataCimsDecisionNodeQATranfer.qa_cims_bindnode_decision_list = new qa_cims_bindnode_decision_detail[1];
                qa_cims_bindnode_decision_detail _selectDecisionNodeQATranfer = new qa_cims_bindnode_decision_detail();
                _dataCimsDecisionNodeQATranfer.qa_cims_bindnode_decision_list[0] = _selectDecisionNodeQATranfer;

                _dataCimsDecisionNodeQATranfer = callServicePostQaDocCIMS(_urlCimsGetDecisionQATranfer, _dataCimsDecisionNodeQATranfer);

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataCimsDecisionNodeQACutoff));

                var _linqSetDecisionTF = from data_qa_cims in _dataCimsDecisionNodeQATranfer.qa_cims_bindnode_decision_list
                                         where data_qa_cims.decision_idx == int.Parse(DecisionQA.Text)
                                         select data_qa_cims;

                var _linqSetDecisionTFNA = from data_qa_cims in _dataCimsDecisionNodeQATranfer.qa_cims_bindnode_decision_list
                                           where data_qa_cims.decision_idx == int.Parse(DecisionQA2.Text)
                                           select data_qa_cims;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_linqSetDecisionTFNA.ToList()));
                //setRepeaterData(rptActionQATranfer, _linqSetDecisionTF.ToList());
                //setRepeaterData(rptActionQATranferNotApprove, _linqSetDecisionTFNA.ToList());

                lblDecisionQATranferView2.Text = DecisionQA2.Text;
                lblDecisionQATranferView.Text = DecisionQA.Text;

                break;

            case "cmdViewOnlyCutoff":

                //int thorwU0_device_idx_View = int.Parse(cmdArg);
                ////litDebug.Text = thorwU0_device_idx_View.ToString();

                string[] cmdArgDecisionCutoff = cmdArg.Split(',');
                int thorwU0_device_idx_View = int.TryParse(cmdArgDecisionCutoff[0].ToString(), out _default_int) ? int.Parse(cmdArgDecisionCutoff[0].ToString()) : _default_int;
                int staidxCutoff = int.TryParse(cmdArgDecisionCutoff[1].ToString(), out _default_int) ? int.Parse(cmdArgDecisionCutoff[1].ToString()) : _default_int;
                int device_rsec_idx_Cutoff = int.TryParse(cmdArgDecisionCutoff[2].ToString(), out _default_int) ? int.Parse(cmdArgDecisionCutoff[2].ToString()) : _default_int;
                int RowCountQA_cut = int.TryParse(cmdArgDecisionCutoff[3].ToString(), out _default_int) ? int.Parse(cmdArgDecisionCutoff[3].ToString()) : _default_int;


                var lblDecisionIDXHeadUser_noapprove = (gvDocumentList.Rows[RowCountQA_cut].FindControl("lblDecisionIDXHeadSection2") as Label);
                var DecisionHeadUser_approve = (gvDocumentList.Rows[RowCountQA_cut].FindControl("lblDecisionIDXHeadSection") as Label);

                var lblDecisionIDXHeadQA2_noapprove = (gvDocumentList.Rows[RowCountQA_cut].FindControl("lblDecisionIDXHeadQA2") as Label);
                var lblDecisionIDXHeadQA_approve = (gvDocumentList.Rows[RowCountQA_cut].FindControl("lblDecisionIDXHeadQA") as Label);


                lbl_HeadUserNotapprove.Text = lblDecisionIDXHeadUser_noapprove.Text;
                lbl_HeadUserapprove.Text = DecisionHeadUser_approve.Text;

                lbl_HeadQANotapprove.Text = lblDecisionIDXHeadQA2_noapprove.Text;
                lbl_HeadQAapprove.Text = lblDecisionIDXHeadQA_approve.Text;

                selectLogCutoff(thorwU0_device_idx_View.ToString());

                divTranferRegistration.Visible = true;
                divDocDetailUser.Visible = true;
                btnBackToDocList.Visible = true;


                divFVVAllCutoff.Visible = false;
                divFVVAllTranfer.Visible = false;
                divdocumentList.Visible = false;
                divRegistrationList.Visible = false;
                divFVVQACutoff.Visible = false;
                divSearchDocument.Visible = false;

                div_SearchDoc.Visible = false;
                btnSubmitQACutoff.Visible = false;
                btnSubmitHeadUserCutoff.Visible = false;


                setFormData(fvDocDetailUser, FormViewMode.ReadOnly, ((data_employee)ViewState["vsEmpProfile"]).employee_list);

                data_qa_cims data_select_Creator_detailsOnlyCO = new data_qa_cims();
                data_select_Creator_detailsOnlyCO.qa_cims_u0_document_device_list = new qa_cims_u0_document_device_detail[1];
                qa_cims_u0_document_device_detail _selectDetailsCutoffOnlyCO = new qa_cims_u0_document_device_detail();
                _selectDetailsCutoffOnlyCO.u0_device_idx = thorwU0_device_idx_View;
                data_select_Creator_detailsOnlyCO.qa_cims_u0_document_device_list[0] = _selectDetailsCutoffOnlyCO;

                data_select_Creator_detailsOnlyCO = callServicePostQaDocCIMS(_urlCimsGetDetailCreator, data_select_Creator_detailsOnlyCO);

                setFormData(fvDocDetailCreator, FormViewMode.ReadOnly, (data_select_Creator_detailsOnlyCO.qa_cims_u0_document_device_list));

                data_qa_cims _data_qa_cutoff = new data_qa_cims();
                _data_qa_cutoff.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
                qa_cims_u1_document_device_detail _selectEquipmentCutoffV = new qa_cims_u1_document_device_detail();
                _selectEquipmentCutoffV.condition = 1;
                _selectEquipmentCutoffV.u0_device_idx = thorwU0_device_idx_View;
                _data_qa_cutoff.qa_cims_u1_document_device_list[0] = _selectEquipmentCutoffV;

                _data_qa_cutoff = callServicePostQaDocCIMS(_urlCimsGetDocEquipmentList, _data_qa_cutoff);

                string document_create_getpath = "";

                ViewState["DocEquipmentList"] = _data_qa_cutoff.qa_cims_u1_document_device_list;

                //var lblDecisionIDXHeadQA_approve = (gvDocumentList.Rows[RowCountQA_cut].FindControl("lblDecisionIDXHeadQA") as Label);

                //aaa;
                //litDebug.Text = staidxCutoff.ToString();

                switch (staidxCutoff)
                {
                    case 23:

                        if (device_rsec_idx_Cutoff == int.Parse(ViewState["rsec_permission"].ToString()) && int.Parse(ViewState["jobgrade_permission"].ToString()) >= 6)
                        {

                            //litDebug.Text = device_rsec_idx_Cutoff.ToString();
                            btnSubmitHeadUserCutoff.Visible = true;

                            div_Detail_CutSuccess.Visible = true;
                            Gv_CutOff_Success.Visible = true;

                            divFVVQACutoff.Visible = true;
                            gvEquipmentQACutoff.Visible = true;

                            qa_cims_u1_document_device_detail[] _temp_status23_node20 = ((qa_cims_u1_document_device_detail[])ViewState["DocEquipmentList"]);

                            var _linq_status23_create = (from data_not23_approve_tranfer in _temp_status23_node20
                                                                 //  where data_SerialNumber.device_serial == txtName.Text
                                                             where data_not23_approve_tranfer.status == 0//.Contains(txtName.Text)
                                                             && data_not23_approve_tranfer.staidx == 23
                                                             select data_not23_approve_tranfer
                                                        );

                            var _linq_status23_notapprove = (from data_not23_approve_tranfer in _temp_status23_node20
                                                             //  where data_SerialNumber.device_serial == txtName.Text
                                                             where data_not23_approve_tranfer.status == 0//.Contains(txtName.Text)                                                        
                                                             select data_not23_approve_tranfer
                                                            );


                            var _linq_status23_approve = (from data_approve25_tranfer in _temp_status23_node20
                                                              //  where data_SerialNumber.device_serial == txtName.Text
                                                          where data_approve25_tranfer.status == 1//.Contains(txtName.Text)
                                                          select data_approve25_tranfer
                                                        );

                            ViewState["vs_gvDetail_not_approve_node20"] = _linq_status23_notapprove.ToList();
                            ViewState["vs_gvDetail_approve_node20"] = _linq_status23_approve.ToList();


                            ViewState["vs_gvDetail_create_approve_node20"] = _linq_status23_create.ToList();

                            if(_temp_status23_node20.Count() == _linq_status23_create.Count())
                            {
                                div_Detail_CutSuccess.Visible = false;
                                setGridData(gvEquipmentQACutoff, ViewState["vs_gvDetail_create_approve_node20"]);
                               
                            }
                            else
                            {
                                div_Detail_CutSuccess.Visible = true;
                                setGridData(gvEquipmentQACutoff, ViewState["vs_gvDetail_approve_node20"]);
                                setGridData(Gv_CutOff_Success, ViewState["vs_gvDetail_not_approve_node20"]);
                            }
                                                      
                            selectLogQACutoff(thorwU0_device_idx_View.ToString());

                        }
                        else
                        {

                            //litDebug.Text = device_rsec_idx_Cutoff.ToString();

                            divFVVAllCutoff.Visible = true;
                            gvEquipmentAllCutoff.Visible = true;
                            
                            ViewState["vs_date_document_pahtfile"] = thorwU0_device_idx_View;
                            setGridData(gvEquipmentAllCutoff, ViewState["DocEquipmentList"]);

                        }
                        setOntop.Focus();

                        break;
                    case 25:
                    case 27:


                        if (int.Parse(ViewState["rdept_permission"].ToString()) == 27 && int.Parse(ViewState["jobgrade_permission"].ToString()) >= 6)
                        {
                          //  litDebug.Text = "666";
                            
                            
                            div_Detail_CutSuccess.Visible = true;
                            Gv_CutOff_Success.Visible = true;

                            //divFVVAllCutoff.Visible = true;
                            //gvEquipmentAllCutoff.Visible = true;

                            


                            qa_cims_u1_document_device_detail[] _temp_status25_node22 = ((qa_cims_u1_document_device_detail[])ViewState["DocEquipmentList"]);

                            var _linq_status25_notapprove = (from data_not25_approve_tranfer in _temp_status25_node22
                                                               //  where data_SerialNumber.device_serial == txtName.Text
                                                           where data_not25_approve_tranfer.status == 0//.Contains(txtName.Text)
                                                           select data_not25_approve_tranfer
                                                        );


                            var _linq_status25_approve = (from data_approve25_tranfer in _temp_status25_node22
                                                              //  where data_SerialNumber.device_serial == txtName.Text
                                                          where data_approve25_tranfer.status == 1//.Contains(txtName.Text)
                                                        select data_approve25_tranfer
                                                        );


                            ViewState["vs_gvDetail_not_approve_node22"] = _linq_status25_notapprove.ToList();
                            ViewState["vs_gvDetail_approve_node22"] = _linq_status25_approve.ToList();


                            ViewState["vs_gvDetail_not_approve_node22"] = _linq_status25_notapprove.ToList();
                            ViewState["vs_gvDetail_approve_node22"] = _linq_status25_approve.ToList();

                            if(_linq_status25_approve.Count() != 0)
                            {
                                litDebug.Text = "8888";
                                divFVVQACutoff.Visible = true;
                                btnSubmitQACutoff.Visible = true;
                                gvEquipmentQACutoff.Visible = true;
                                setGridData(gvEquipmentQACutoff, ViewState["vs_gvDetail_approve_node22"]);
                            }
                            else
                            {
                              //  litDebug.Text = "7777";
                                divFVVQACutoff.Visible = false;
                                btnSubmitQACutoff.Visible = false;
                                gvEquipmentQACutoff.Visible = false;

                                div_Log_DetailCut_Sucess.Visible = true;
                                selectLogQACutoff_sucesscutoff(thorwU0_device_idx_View.ToString());
                            }
                            
                            setGridData(Gv_CutOff_Success, ViewState["vs_gvDetail_not_approve_node22"]);
                            //setGridData(gvEquipmentAllCutoff, ViewState["vs_gvDetail_not_approve_node22"]);

                            selectLogQACutoff(thorwU0_device_idx_View.ToString()); 

                        }
                        else
                        {

                          //  litDebug.Text = "5555";
                            divFVVAllCutoff.Visible = true;
                            gvEquipmentAllCutoff.Visible = true;

                            //lbNotApprove.Visible = false;
                            //gvEquipmentAllCutoffNA.Visible = false;

                            ViewState["vs_date_document_pahtfile"] = thorwU0_device_idx_View;
                            setGridData(gvEquipmentAllCutoff, ViewState["DocEquipmentList"]);

                        }

                        break;
                    case 24:

                    case 26:
                   
                    case 28:
                    case 29:
                        break;
                    case 30:
                        //litDebug.Text = "Cutoff 30";

                        //lbWaiting.Visible = true;
                        //lbApprove.Visible = false;
                        gvEquipmentAllCutoff.Visible = true;
                        lbNotApprove.Visible = false;
                        gvEquipmentAllCutoffNA.Visible = false;




                        //_data_qa_cutoffNA.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
                        //qa_cims_u1_document_device_detail _selectEquipmentCutoffVNA = new qa_cims_u1_document_device_detail();
                        //_selectEquipmentCutoffVNA.condition = 3;
                        //_selectEquipmentCutoffVNA.u0_device_idx = thorwU0_device_idx_View;
                        //_data_qa_cutoffNA.qa_cims_u1_document_device_list[0] = _selectEquipmentCutoffVNA;

                        //_data_qa_cutoffNA = callServicePostQaDocCIMS(_urlCimsGetDocEquipmentList, _data_qa_cutoffNA);
                        ////if (_data_qa_cutoff.qa_cims_u1_document_device_list[0].create_date.ToString() != null)
                        ////{
                        ////    var _create_date = _data_qa_cutoff.qa_cims_u1_document_device_list[0].create_date.ToString();
                        ////    //setGridData(gvEquipmentAllCutoffNA, _data_qa_cutoffNA.qa_cims_u1_document_device_list);

                        ////    DateTime s1 = System.Convert.ToDateTime(_create_date.Trim());
                        ////    DateTime date = (s1);
                        ////    String frmdt = date.ToString("dd/MM/yyyy");

                        ////    //  litDebug.Text = frmdt;
                        ////    //{

                        ////    var list_date_document = frmdt.Split('/');
                        ////    //    var list_date_document = _create_date.Split('/');

                        ////    foreach (string document_create in list_date_document)
                        ////    {
                        ////        if (document_create != null)
                        ////        {
                        ////            document_create_getpath += document_create;
                        ////        }
                        ////    }
                        ////}

                        ////ViewState["vs_date_document_pahtfile"] = document_create_getpath;

                        setGridData(gvEquipmentAllCutoff, ViewState["DocEquipmentList"]);

                        ////////    Label lblEquipment_IDNO = (Label)row.FindControl("lblEquipment_IDNO");


                        break;

                }


                setOntop.Focus();

                break;

            case "cmdViewOnlyTranfer":

                //int thorwU0_device_idx_ViewTF = int.Parse(cmdArg);
                string[] cmdArgDecisionTranfer = cmdArg.Split(',');
                int thorwU0_device_idx_ViewTF = int.TryParse(cmdArgDecisionTranfer[0].ToString(), out _default_int) ? int.Parse(cmdArgDecisionTranfer[0].ToString()) : _default_int;
                int RowCountQATranfer_ = int.TryParse(cmdArgDecisionTranfer[1].ToString(), out _default_int) ? int.Parse(cmdArgDecisionTranfer[1].ToString()) : _default_int;
                int staidxTranfer = int.TryParse(cmdArgDecisionTranfer[2].ToString(), out _default_int) ? int.Parse(cmdArgDecisionTranfer[2].ToString()) : _default_int;
                int rsec_Tranfer = int.TryParse(cmdArgDecisionTranfer[3].ToString(), out _default_int) ? int.Parse(cmdArgDecisionTranfer[3].ToString()) : _default_int;
                int rsec_create_Tranfer = int.TryParse(cmdArgDecisionTranfer[4].ToString(), out _default_int) ? int.Parse(cmdArgDecisionTranfer[4].ToString()) : _default_int;


                var lblDecisionIDXHeadSection2 = (gvDocumentList.Rows[RowCountQATranfer_].FindControl("lblDecisionIDXHeadSection2") as Label);
                var lblDecisionIDXHeadSection = (gvDocumentList.Rows[RowCountQATranfer_].FindControl("lblDecisionIDXHeadSection") as Label);

                var lblDecisionIDXReceiver2 = (gvDocumentList.Rows[RowCountQATranfer_].FindControl("lblDecisionIDXReceiver2") as Label);
                var lblDecisionIDXReceiver = (gvDocumentList.Rows[RowCountQATranfer_].FindControl("lblDecisionIDXReceiver") as Label);

                //litDebug.Text = thorwU0_device_idx_ViewTF.ToString(); 

                lblDecisionHeadTranferView_not.Text = lblDecisionIDXHeadSection2.Text;
                lblDecisionHeadTranferView_approve.Text = lblDecisionIDXHeadSection.Text;

                lblDecisionHeadReceiveTranferView_not.Text = lblDecisionIDXReceiver2.Text;
                lblDecisionHeadReceiveTranferView_approve.Text = lblDecisionIDXReceiver.Text;


                selectLogTranfer(thorwU0_device_idx_ViewTF.ToString());

                divTranferRegistration.Visible = true;
                divDocDetailUser.Visible = true;
                btnBackToDocList.Visible = true;

                div_SearchDoc.Visible = false;
                divFVVAllCutoff.Visible = false;

                divFVVAllTranfer.Visible = false;
                divdocumentList.Visible = false;
                divRegistrationList.Visible = false;
                divFVVQACutoff.Visible = false;

                gvEquipmentAllTranferNA.Visible = false;
                divSearchDocument.Visible = false;
                div_detailTranfer.Visible = false;
                lbltranferNotApprove.Visible = false;
                div_detailwaitTranfer.Visible = false;

                div_LogTranfer_wait.Visible = false;
                div_successTranfer.Visible = false;

                lblDecisionHeadTranferView_not.Text = lblDecisionIDXHeadSection2.Text;

                setFormData(fvDocDetailUser, FormViewMode.ReadOnly, ((data_employee)ViewState["vsEmpProfile"]).employee_list);

                data_qa_cims data_select_Creator_detailsOnlyTF = new data_qa_cims();
                data_select_Creator_detailsOnlyTF.qa_cims_u0_document_device_list = new qa_cims_u0_document_device_detail[1];
                qa_cims_u0_document_device_detail _selectDetailsCutoffOnlyTF = new qa_cims_u0_document_device_detail();
                _selectDetailsCutoffOnlyTF.u0_device_idx = thorwU0_device_idx_ViewTF;
                data_select_Creator_detailsOnlyTF.qa_cims_u0_document_device_list[0] = _selectDetailsCutoffOnlyTF;

                data_select_Creator_detailsOnlyTF = callServicePostQaDocCIMS(_urlCimsGetDetailCreator, data_select_Creator_detailsOnlyTF);

                setFormData(fvDocDetailCreator, FormViewMode.ReadOnly, (data_select_Creator_detailsOnlyTF.qa_cims_u0_document_device_list));

                //litDebug.Text = staidxTranfer.ToString();

                data_qa_cims _data_qa_tranfer = new data_qa_cims();
                _data_qa_tranfer.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
                qa_cims_u1_document_device_detail _selectEquipmentTranferV = new qa_cims_u1_document_device_detail();
                _selectEquipmentTranferV.condition = 2;
                _selectEquipmentTranferV.u0_device_idx = thorwU0_device_idx_ViewTF;
                _data_qa_tranfer.qa_cims_u1_document_device_list[0] = _selectEquipmentTranferV;

                _data_qa_tranfer = callServicePostQaDocCIMS(_urlCimsGetDocEquipmentList, _data_qa_tranfer);

                string document_create_getpath1 = "";

                ViewState["DocEquipmentList"] = _data_qa_tranfer.qa_cims_u1_document_device_list;



                switch (staidxTranfer)
                {
                    case 16:
                    case 17:
                        //div_detailTranfer.Visible = true;
                        divFVVAllTranfer.Visible = true;
                        gvEquipmentAllTranfer.Visible = true;
                        div_LogTranfer_wait.Visible = true;

                        if (_data_qa_tranfer.qa_cims_u1_document_device_list[0].create_date.ToString() != null)
                        {
                            var _create_date = _data_qa_tranfer.qa_cims_u1_document_device_list[0].create_date.ToString();
                            //setGridData(gvEquipmentAllCutoffNA, _data_qa_cutoffNA.qa_cims_u1_document_device_list);

                            DateTime s1 = System.Convert.ToDateTime(_create_date.Trim());
                            DateTime date = (s1);
                            String frmdt = date.ToString("dd/MM/yyyy");

                            //  litDebug.Text = frmdt;
                            //{

                            var list_date_document = frmdt.Split('/');
                            //    var list_date_document = _create_date.Split('/');

                            foreach (string document_create in list_date_document)
                            {
                                if (document_create != null)
                                {
                                    document_create_getpath1 += document_create;
                                }
                            }
                        }

                        ViewState["vs_date_document_pahtfile"] = document_create_getpath1;


                        if (int.Parse(rsec_create_Tranfer.ToString()) == int.Parse(ViewState["rsec_permission"].ToString()) && (int.Parse(ViewState["jobgrade_permission"].ToString()) >= 6))
                        {

                            //litDebug.Text = rsec_Tranfer.ToString();

                            //div_detailwaitTranfer.Visible = false;
                            divFVVAllTranfer.Visible = true;
                            //div_detailTranfer.Visible = true;

                            //setGridData(gvDetailTranfer, ViewState["vs_gvDetail"]);
                            //setGridData(gvEquipmentAllTranfer, ViewState["vs_gvDetailwaittranfer"]);


                            setGridData(gvEquipmentAllTranfer, ViewState["DocEquipmentList"]);

                        }
                        else
                        {
                            divFVVAllTranfer.Visible = false;
                            div_successTranfer.Visible = true;
                            setGridData(gvSucessTranfer, ViewState["DocEquipmentList"]);

                            //setGridData(gvDetailWaitTranfer, ViewState["vs_gvDetailwaittranfer"]);
                            //setGridData(gvDetailTranfer, ViewState["vs_gvDetail"]);

                        }




                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_qa_tranfer.qa_cims_u1_document_device_list));


                        break;
                    case 18:

                        //18
                        div_detailTranfer.Visible = true;
                        div_detailwaitTranfer.Visible = true;
                        div_LogTranfer_wait.Visible = true;

                        qa_cims_u1_document_device_detail[] _temp_status18_notapprove = ((qa_cims_u1_document_device_detail[])ViewState["DocEquipmentList"]);

                        var _linq_status_notapprove = (from data_not_approve_tranfer in _temp_status18_notapprove
                                                           //  where data_SerialNumber.device_serial == txtName.Text
                                                       where data_not_approve_tranfer.status == 0//.Contains(txtName.Text)
                                                       select data_not_approve_tranfer
                                                    );


                        var _linq_status_approve = (from data_not_approve_tranfer in _temp_status18_notapprove
                                                        //  where data_SerialNumber.device_serial == txtName.Text
                                                    where data_not_approve_tranfer.status == 1//.Contains(txtName.Text)
                                                    select data_not_approve_tranfer
                                                    );

                        ViewState["vs_gvDetail"] = _linq_status_notapprove.ToList();
                        ViewState["vs_gvDetailwaittranfer"] = _linq_status_approve.ToList();
                        ViewState["DocEquipmentList"] = _linq_status_approve.ToList();

                        setGridData(gvDetailTranfer, ViewState["vs_gvDetail"]);
                        setGridData(gvDetailWaitTranfer, ViewState["vs_gvDetailwaittranfer"]);

                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_linq_status_notapprove));
                        //setGridData(gvEquipmentAllTranfer, ViewState["DocEquipmentList"]);



                        break;
                    case 36:
                        div_detailTranfer.Visible = true;
                        div_detailwaitTranfer.Visible = true;
                        div_LogTranfer_wait.Visible = true;



                        qa_cims_u1_document_device_detail[] _temp_status36_notapprove = ((qa_cims_u1_document_device_detail[])ViewState["DocEquipmentList"]);

                        var _linq_status36_notapprove = (from data_not_approve_tranfer in _temp_status36_notapprove
                                                             //  where data_SerialNumber.device_serial == txtName.Text
                                                         where data_not_approve_tranfer.status == 0//.Contains(txtName.Text)
                                                         select data_not_approve_tranfer
                                                    );


                        var _linq_status36_approve = (from data_not_approve_tranfer in _temp_status36_notapprove
                                                          //  where data_SerialNumber.device_serial == txtName.Text
                                                      where data_not_approve_tranfer.status == 1//.Contains(txtName.Text)
                                                      select data_not_approve_tranfer
                                                    );

                        ViewState["vs_gvDetail"] = _linq_status36_notapprove.ToList();
                        ViewState["vs_gvDetailwaittranfer"] = _linq_status36_approve.ToList();
                        //ViewState["DocEquipmentList"] = _linq_status36_approve.ToList();


                        //gvDetailTranfer
                        if (int.Parse(rsec_Tranfer.ToString()) == int.Parse(ViewState["rsec_permission"].ToString()) && (int.Parse(ViewState["jobgrade_permission"].ToString()) >= 6))
                        {

                           // litDebug.Text = rsec_Tranfer.ToString();

                            div_detailwaitTranfer.Visible = false;
                            divFVVAllTranfer.Visible = true;
                            div_detailTranfer.Visible = true;
                            //setGridData(gvDetailWaitTranfer, ViewState["vs_gvDetailwaittranfer"]);
                            setGridData(gvDetailTranfer, ViewState["vs_gvDetail"]);
                            setGridData(gvEquipmentAllTranfer, ViewState["vs_gvDetailwaittranfer"]);
                        }
                        else
                        {
                            divFVVAllTranfer.Visible = false;
                            setGridData(gvDetailWaitTranfer, ViewState["vs_gvDetailwaittranfer"]);
                            setGridData(gvDetailTranfer, ViewState["vs_gvDetail"]);

                        }
                        break;
                    case 28:


                        div_detailTranfer.Visible = true;
                        //div_detailwaitTranfer.Visible = true;
                        div_LogTranfer_wait.Visible = true;
                        //divFVVAllTranfer.Visible = true;
                        div_successTranfer.Visible = true;

                        qa_cims_u1_document_device_detail[] _temp_status28_notapprove = ((qa_cims_u1_document_device_detail[])ViewState["DocEquipmentList"]);

                        var _linq_status28_notapprove = (from data_not_approve_tranfer in _temp_status28_notapprove
                                                             //  where data_SerialNumber.device_serial == txtName.Text
                                                         where data_not_approve_tranfer.status == 0//.Contains(txtName.Text)
                                                         select data_not_approve_tranfer
                                                    );


                        var _linq_status28_approve = (from data_not_approve_tranfer in _temp_status28_notapprove
                                                          //  where data_SerialNumber.device_serial == txtName.Text
                                                      where data_not_approve_tranfer.status == 1//.Contains(txtName.Text)
                                                      select data_not_approve_tranfer
                                                    );

                        ViewState["vs_gvDetail_notapprove_n28"] = _linq_status28_notapprove.ToList();
                        ViewState["vs_gvDetailwaittranfer_approve_28"] = _linq_status28_approve.ToList();

                        setGridData(gvDetailTranfer, ViewState["vs_gvDetail_notapprove_n28"]);
                        setGridData(gvSucessTranfer, ViewState["vs_gvDetailwaittranfer_approve_28"]);
                        //setGridData(gvEquipmentAllTranfer, ViewState["vs_gvDetailwaittranfer_approve_28"]);


                        break;
                    case 29:
                        div_detailTranfer.Visible = true;
                        div_LogTranfer_wait.Visible = true;

                        qa_cims_u1_document_device_detail[] _temp_status29_notapprove = ((qa_cims_u1_document_device_detail[])ViewState["DocEquipmentList"]);

                        var _linq_status29_notapprove = (from data_not_approve_tranfer in _temp_status29_notapprove
                                                             //  where data_SerialNumber.device_serial == txtName.Text
                                                         where data_not_approve_tranfer.staidx == staidxTranfer//.Contains(txtName.Text)
                                                         select data_not_approve_tranfer
                                                    );


                        var _linq_status29_approve = (from data_not_approve_tranfer in _temp_status29_notapprove
                                                          //  where data_SerialNumber.device_serial == txtName.Text
                                                      where data_not_approve_tranfer.status == 1//.Contains(txtName.Text)
                                                      select data_not_approve_tranfer
                                                    );

                        ViewState["vs_gvDetail_notapprove_n29"] = _linq_status29_notapprove.ToList();
                        ViewState["vs_gvDetailwaittranfer_approve_29"] = _linq_status29_approve.ToList();

                        setGridData(gvDetailTranfer, ViewState["vs_gvDetail_notapprove_n29"]);
                        //setGridData(gvSucessTranfer, ViewState["vs_gvDetailwaittranfer_approve_28"]);


                        break;

                    case 19:
                    case 20:


                    case 31:


                        //18
                        ////divFVVAllTranfer.Visible = true;
                        ////gvEquipmentAllTranfer.Visible = true;                                            
                        ////div_detailTranfer.Visible = true;

                        ////qa_cims_u1_document_device_detail[] _temp_status18_notapprove = ((qa_cims_u1_document_device_detail[])ViewState["DocEquipmentList"]);

                        ////var _linq_status_notapprove = (from data_not_approve_tranfer in _temp_status18_notapprove
                        ////                                   //  where data_SerialNumber.device_serial == txtName.Text
                        ////                               where data_not_approve_tranfer.staidx == 29//.Contains(txtName.Text)
                        ////                               select data_not_approve_tranfer
                        ////                            );


                        ////var _linq_status_approve = (from data_not_approve_tranfer in _temp_status18_notapprove
                        ////                                   //  where data_SerialNumber.device_serial == txtName.Text
                        ////                               where data_not_approve_tranfer.staidx == 18//.Contains(txtName.Text)
                        ////                               select data_not_approve_tranfer
                        ////                            );

                        ////ViewState["vs_gvDetail"] = _linq_status_notapprove.ToList();
                        ////ViewState["DocEquipmentList"] = _linq_status_approve.ToList();

                        ////setGridData(gvDetailTranfer, ViewState["vs_gvDetail"]);
                        //////litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_linq_status_notapprove));
                        ////setGridData(gvEquipmentAllTranfer, ViewState["DocEquipmentList"]);

                        //data_qa_cims _data_qa_tranfer = new data_qa_cims();
                        //_data_qa_tranfer.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
                        //qa_cims_u1_document_device_detail _selectEquipmentTranferV = new qa_cims_u1_document_device_detail();
                        //_selectEquipmentTranferV.condition = 2;
                        //_selectEquipmentTranferV.u0_device_idx = thorwU0_device_idx_ViewTF;
                        //_data_qa_tranfer.qa_cims_u1_document_device_list[0] = _selectEquipmentTranferV;

                        //_data_qa_tranfer = callServicePostQaDocCIMS(_urlCimsGetDocEquipmentList, _data_qa_tranfer);

                        ////data_qa_cims _data_qa_tranfer = new data_qa_cims();
                        ////_data_qa_tranfer.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
                        ////qa_cims_u1_document_device_detail _selectEquipmentTranferV = new qa_cims_u1_document_device_detail();
                        ////_selectEquipmentTranferV.condition = 2;
                        ////_selectEquipmentTranferV.u0_device_idx = thorwU0_device_idx_ViewTF;
                        ////_data_qa_tranfer.qa_cims_u1_document_device_list[0] = _selectEquipmentTranferV;

                        ////_data_qa_tranfer = callServicePostQaDocCIMS(_urlCimsGetDocEquipmentList, _data_qa_tranfer);

                        ////string document_create_getpath = "";
                        //////_data_qa_cutoffNA.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
                        //////qa_cims_u1_document_device_detail _selectEquipmentCutoffVNA = new qa_cims_u1_document_device_detail();
                        //////_selectEquipmentCutoffVNA.condition = 3;
                        //////_selectEquipmentCutoffVNA.u0_device_idx = thorwU0_device_idx_View;
                        //////_data_qa_cutoffNA.qa_cims_u1_document_device_list[0] = _selectEquipmentCutoffVNA;

                        //////_data_qa_cutoffNA = callServicePostQaDocCIMS(_urlCimsGetDocEquipmentList, _data_qa_cutoffNA);
                        ////if (_data_qa_tranfer.qa_cims_u1_document_device_list[0].create_date.ToString() != null)
                        ////{
                        ////    var _create_date = _data_qa_tranfer.qa_cims_u1_document_device_list[0].create_date.ToString();
                        ////    //setGridData(gvEquipmentAllCutoffNA, _data_qa_cutoffNA.qa_cims_u1_document_device_list);

                        ////    DateTime s1 = System.Convert.ToDateTime(_create_date.Trim());
                        ////    DateTime date = (s1);
                        ////    String frmdt = date.ToString("dd/MM/yyyy");

                        ////    //  litDebug.Text = frmdt;
                        ////    //{

                        ////    var list_date_document = frmdt.Split('/');
                        ////    //    var list_date_document = _create_date.Split('/');

                        ////    foreach (string document_create in list_date_document)
                        ////    {
                        ////        if (document_create != null)
                        ////        {
                        ////            document_create_getpath += document_create;
                        ////        }
                        ////    }
                        ////}

                        ////ViewState["vs_date_document_pahtfile"] = document_create_getpath;
                        ////ViewState["DocEquipmentList"] = _data_qa_tranfer.qa_cims_u1_document_device_list;
                        //////litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_qa_tranfer.qa_cims_u1_document_device_list));
                        ////setGridData(gvEquipmentAllTranfer, ViewState["DocEquipmentList"]);

                        ////////    Label lblEquipment_IDNO = (Label)row.FindControl("lblEquipment_IDNO");


                        break;

                        //case 30:

                        //    //data_qa_cims _data_qa_cutoff1 = new data_qa_cims();
                        //    //_data_qa_cutoff1.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
                        //    //qa_cims_u1_document_device_detail _selectEquipmentCutoffV1 = new qa_cims_u1_document_device_detail();
                        //    //_selectEquipmentCutoffV1.condition = 3;
                        //    //_selectEquipmentCutoffV1.u0_device_idx = thorwU0_device_idx_View;
                        //    //_data_qa_cutoff1.qa_cims_u1_document_device_list[0] = _selectEquipmentCutoffV1;

                        //    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_qa_cutoff1));
                        //////    //    FileRef.ImageUrl = getPathShowimages + ViewState["vsGetDocument_code"].ToString() + lblEquipment_IDNO.Text + ".jpg";


                        //    //_data_qa_cutoff1 = callServicePostQaDocCIMS(_urlCimsGetDocEquipmentList, _data_qa_cutoff1);

                        //    //string document_create_getpath1 = "";

                        //    //if (_data_qa_cutoff1.qa_cims_u1_document_device_list[0].create_date.ToString() != null)
                        //    //{
                        //    //    var _create_date = _data_qa_cutoff1.qa_cims_u1_document_device_list[0].create_date.ToString();

                        //    //    DateTime s1 = System.Convert.ToDateTime(_create_date.Trim());
                        //    //    DateTime date = (s1);
                        //    //    String frmdt = date.ToString("dd/MM/yyyy");

                        //    //    //  litDebug.Text = frmdt;

                        //    //    var list_date_document = frmdt.Split('/');

                        //    //    foreach (string document_create in list_date_document)
                        //    //    {
                        //    //        if (document_create != null)
                        //    //        {
                        //    //            document_create_getpath1 += document_create;
                        //    //        }
                        //    //    }
                        //    //}

                        //    //ViewState["vs_date_document_pahtfile"] = document_create_getpath1;
                        //    ////setGridData(gvEquipmentAllCutoff, _data_qa_cutoff1.qa_cims_u1_document_device_list);



                        //    //data_qa_cims _data_qa_cutoffNA1 = new data_qa_cims();
                        //    //_data_qa_cutoffNA1.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
                        //    //qa_cims_u1_document_device_detail _selectEquipmentCutoffVNA1 = new qa_cims_u1_document_device_detail();
                        //    //_selectEquipmentCutoffVNA1.condition = 3;
                        //    //_selectEquipmentCutoffVNA1.u0_device_idx = thorwU0_device_idx_View;
                        //    //_data_qa_cutoffNA1.qa_cims_u1_document_device_list[0] = _selectEquipmentCutoffVNA1;

                        //    //_data_qa_cutoffNA1 = callServicePostQaDocCIMS(_urlCimsGetDocEquipmentList, _data_qa_cutoffNA1);



                        //    //setGridData(gvEquipmentAllCutoffNA, _data_qa_cutoffNA1.qa_cims_u1_document_device_list);

                        //    setGridData(gvEquipmentAllCutoff, ViewState["DocEquipmentList"]);
                        //    //gvEquipmentAllCutoff.Visible = false;
                        //    //gvEquipmentAllCutoffNA.Visible = true;
                        //    ////  
                        //    break;


                }

                //if (staidxTranfer == 17)
                //{
                //    lbtranferWaiting.Visible = true;
                //    lbltranferApprove.Visible = false;
                //    gvEquipmentAllTranfer.Visible = true;

                //    lbltranferNotApprove.Visible = false;
                //    gvEquipmentAllTranferNA.Visible = false;
                //}
                //else
                //{
                //    lbtranferWaiting.Visible = false;
                //    lbltranferApprove.Visible = true;
                //    gvEquipmentAllTranfer.Visible = true;

                //    lbltranferNotApprove.Visible = true;
                //    gvEquipmentAllTranferNA.Visible = true;
                //}

                //data_qa_cims _data_qa_tranfer = new data_qa_cims();
                //_data_qa_tranfer.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
                //qa_cims_u1_document_device_detail _selectEquipmentTranferV = new qa_cims_u1_document_device_detail();
                //_selectEquipmentTranferV.condition = 2;
                //_selectEquipmentTranferV.u0_device_idx = thorwU0_device_idx_ViewTF;
                //_data_qa_tranfer.qa_cims_u1_document_device_list[0] = _selectEquipmentTranferV;

                //_data_qa_tranfer = callServicePostQaDocCIMS(_urlCimsGetDocEquipmentList, _data_qa_tranfer);

                //data_qa_cims _data_qa_tranferNA = new data_qa_cims();
                //_data_qa_tranferNA.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
                //qa_cims_u1_document_device_detail _selectEquipmentTranferVNA = new qa_cims_u1_document_device_detail();
                //_selectEquipmentTranferVNA.condition = 4;
                //_selectEquipmentTranferVNA.u0_device_idx = thorwU0_device_idx_ViewTF;
                //_data_qa_tranferNA.qa_cims_u1_document_device_list[0] = _selectEquipmentTranferVNA;

                //_data_qa_tranferNA = callServicePostQaDocCIMS(_urlCimsGetDocEquipmentList, _data_qa_tranferNA);

                //ViewState["DocEquipmentList"] = _data_qa_tranfer.qa_cims_u1_document_device_list;
                ////litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_qa_cutoff.qa_cims_u1_document_device_list));

                //setGridData(gvEquipmentAllTranfer, ViewState["DocEquipmentList"]);
                //setGridData(gvEquipmentAllTranferNA, _data_qa_tranferNA.qa_cims_u1_document_device_list);

                setOntop.Focus();
                break;

            case "cmdXmlTranfer":

                int STARTNODEQATRANFER = int.Parse(cmdArg);

                //litDebug.Text = STARTNODEQACUTOFF.ToString();

                qa_cims_u1_document_device_detail[] _u1tempListEquipmentHolderTranferHead = (qa_cims_u1_document_device_detail[])ViewState["DocEquipmentList"];

                if (CheckAllQATranfer.Checked == true)
                {
                    _data_qa_cims.qa_cims_u0_document_device_list = new qa_cims_u0_document_device_detail[1];
                    qa_cims_u0_document_device_detail _selectDicision = new qa_cims_u0_document_device_detail();
                    _selectDicision.decision_idx = STARTNODEQATRANFER;
                    _selectDicision.m0_actor_idx = _u1tempListEquipmentHolderTranferHead[0].m0_actor_idx;
                    _selectDicision.m0_node_idx = _u1tempListEquipmentHolderTranferHead[0].m0_node_idx;
                    _selectDicision.u0_device_idx = _u1tempListEquipmentHolderTranferHead[0].u0_device_idx;
                    _selectDicision.emp_idx_create = _emp_idx;
                    _data_qa_cims.qa_cims_u0_document_device_list[0] = _selectDicision;

                    qa_cims_u1_document_device_detail[] _u1tempListEquipmentHolderTranfer = (qa_cims_u1_document_device_detail[])ViewState["DocEquipmentList"];

                    var u1_doc_cutoff_someAll = new qa_cims_u1_document_device_detail[_u1tempListEquipmentHolderTranfer.Count()];
                    int count_someAll = 0;


                    foreach (GridViewRow rowItems in gvEquipmentQATranfer.Rows)
                    {
                        Label lblDeviceIDX = (Label)rowItems.FindControl("lblDeviceIDX");
                        TextBox txtcomment_qacutoff = (TextBox)rowItems.FindControl("txtcomment_qacutoff");

                        u1_doc_cutoff_someAll[count_someAll] = new qa_cims_u1_document_device_detail();
                        u1_doc_cutoff_someAll[count_someAll].u1_device_idx = int.Parse(lblDeviceIDX.Text);
                        u1_doc_cutoff_someAll[count_someAll].decision_idx = STARTNODEQATRANFER;
                        u1_doc_cutoff_someAll[count_someAll].u0_device_idx = _u1tempListEquipmentHolderTranfer[count_someAll].u0_device_idx;
                        u1_doc_cutoff_someAll[count_someAll].m0_actor_idx = _u1tempListEquipmentHolderTranfer[count_someAll].m0_actor_idx;
                        u1_doc_cutoff_someAll[count_someAll].m0_node_idx = _u1tempListEquipmentHolderTranfer[count_someAll].m0_node_idx;
                        u1_doc_cutoff_someAll[count_someAll].condition = 1;
                        if (txtcomment_qacutoff.Text == "")
                        {
                            u1_doc_cutoff_someAll[count_someAll].comment = "-";
                        }
                        else
                        {
                            u1_doc_cutoff_someAll[count_someAll].comment = txtcomment_qacutoff.Text;
                        }
                        if (STARTNODEQATRANFER == int.Parse(lblDecisionQATranferView2.Text))
                        {
                            u1_doc_cutoff_someAll[count_someAll].status = 0;
                        }
                        else if (STARTNODEQATRANFER == int.Parse(lblDecisionQATranferView.Text))
                        {
                            u1_doc_cutoff_someAll[count_someAll].status = 1;
                        }

                        u1_doc_cutoff_someAll[count_someAll].cemp_idx = _u1tempListEquipmentHolderTranfer[count_someAll].cemp_idx;

                        count_someAll++;


                    }

                    _data_qa_cims.qa_cims_u1_document_device_list = u1_doc_cutoff_someAll;
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_qa_cims));
                    _data_qa_cims = callServicePostQaDocCIMS(_urlCimsSetDocumentUpdate, _data_qa_cims);

                    divdocumentList.Visible = true;
                    divFVVQATranfer.Visible = false;

                    CheckAllQATranfer.Checked = false;
                    btnBackToDocList.Visible = false;
                    divDocDetailUser.Visible = false;
                    divSearchDocument.Visible = false;
                    //litDebug.Text = "We Here 1";

                    data_qa_cims renew = new data_qa_cims();
                    renew.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
                    qa_cims_u1_document_device_detail _selectu0DocList = new qa_cims_u1_document_device_detail();

                    renew.qa_cims_u1_document_device_list[0] = _selectu0DocList;

                    renew = callServicePostQaDocCIMS(_urlCimsGetEquipmentList, renew);

                    setGridData(gvDocumentList, renew.qa_cims_u0_document_device_list);
                    setActiveTab("docRegistration", 0, 0, 0, 0, 0, 0);

                }
                else
                {
                    GridView EquipmentQATranfer = (GridView)divFVVQACutoff.FindControl("gvEquipmentQATranfer");

                    qa_cims_u1_document_device_detail[] _u1tempListEquipmentHolderTranfer = (qa_cims_u1_document_device_detail[])ViewState["DocEquipmentList"];

                    var u1_doc_cutoff_someAll = new qa_cims_u1_document_device_detail[_u1tempListEquipmentHolderTranfer.Count()];
                    int count_someAll = 0;
                    int count_row_check = 0;
                    foreach (GridViewRow rowItems in gvEquipmentQATranfer.Rows)
                    {
                        CheckBox chk = (CheckBox)rowItems.FindControl("ChkEquipmentQATranfer");
                        Label lblDeviceIDX = (Label)rowItems.FindControl("lblDeviceIDX");
                        //Label lblEquipment_IDNO = (Label)rowItems.FindControl("lblEquipment_IDNO");
                        TextBox txtcomment_qacutoff = (TextBox)rowItems.FindControl("txtcomment_qacutoff");

                        if (chk.Checked)
                        {
                            u1_doc_cutoff_someAll[count_someAll] = new qa_cims_u1_document_device_detail();
                            u1_doc_cutoff_someAll[count_someAll].u1_device_idx = int.Parse(lblDeviceIDX.Text);
                            u1_doc_cutoff_someAll[count_someAll].decision_idx = STARTNODEQATRANFER;
                            u1_doc_cutoff_someAll[count_someAll].u0_device_idx = _u1tempListEquipmentHolderTranfer[count_someAll].u0_device_idx;
                            u1_doc_cutoff_someAll[count_someAll].m0_actor_idx = _u1tempListEquipmentHolderTranfer[count_someAll].m0_actor_idx;
                            u1_doc_cutoff_someAll[count_someAll].m0_node_idx = _u1tempListEquipmentHolderTranfer[count_someAll].m0_node_idx;
                            u1_doc_cutoff_someAll[count_someAll].condition = 1;
                            if (txtcomment_qacutoff.Text == "")
                            {
                                u1_doc_cutoff_someAll[count_someAll].comment = "-";
                            }
                            else
                            {
                                u1_doc_cutoff_someAll[count_someAll].comment = txtcomment_qacutoff.Text;
                            }

                            if (STARTNODEQATRANFER == int.Parse(lblDecisionQATranferView2.Text))
                            {
                                u1_doc_cutoff_someAll[count_someAll].status = 0;
                            }
                            else if (STARTNODEQATRANFER == int.Parse(lblDecisionQATranferView.Text))
                            {
                                u1_doc_cutoff_someAll[count_someAll].status = 1;
                            }
                            u1_doc_cutoff_someAll[count_someAll].cemp_idx = _u1tempListEquipmentHolderTranfer[count_someAll].cemp_idx;


                            count_row_check++;
                            count_someAll++;

                            ViewState["count_row_check"] = count_row_check;

                        }
                        else
                        {
                            u1_doc_cutoff_someAll[count_someAll] = new qa_cims_u1_document_device_detail();
                            u1_doc_cutoff_someAll[count_someAll].u1_device_idx = int.Parse(lblDeviceIDX.Text);
                            if (STARTNODEQATRANFER == int.Parse(lblDecisionQATranferView2.Text))
                            {
                                u1_doc_cutoff_someAll[count_someAll].decision_idx = int.Parse(lblDecisionQATranferView.Text);
                                u1_doc_cutoff_someAll[count_someAll].status = 1;
                            }
                            else if (STARTNODEQATRANFER == int.Parse(lblDecisionQATranferView.Text))
                            {
                                u1_doc_cutoff_someAll[count_someAll].decision_idx = int.Parse(lblDecisionQATranferView2.Text);
                                u1_doc_cutoff_someAll[count_someAll].status = 0;
                            }
                            u1_doc_cutoff_someAll[count_someAll].condition = 1;
                            u1_doc_cutoff_someAll[count_someAll].u0_device_idx = _u1tempListEquipmentHolderTranfer[count_someAll].u0_device_idx;
                            u1_doc_cutoff_someAll[count_someAll].m0_actor_idx = _u1tempListEquipmentHolderTranfer[count_someAll].m0_actor_idx;
                            u1_doc_cutoff_someAll[count_someAll].m0_node_idx = _u1tempListEquipmentHolderTranfer[count_someAll].m0_node_idx;
                            if (txtcomment_qacutoff.Text == "")
                            {
                                u1_doc_cutoff_someAll[count_someAll].comment = "-";
                            }
                            else
                            {
                                u1_doc_cutoff_someAll[count_someAll].comment = txtcomment_qacutoff.Text;
                            }
                            //u1_doc_cutoff_someAll[count_someAll].status = 9;
                            u1_doc_cutoff_someAll[count_someAll].cemp_idx = _u1tempListEquipmentHolderTranfer[count_someAll].cemp_idx;

                            count_someAll++;
                        }
                    }
                    if (int.Parse(ViewState["count_row_check"].ToString()) == EquipmentQATranfer.Rows.Count)
                    {
                        _data_qa_cims.qa_cims_u0_document_device_list = new qa_cims_u0_document_device_detail[1];
                        qa_cims_u0_document_device_detail _selectDicision = new qa_cims_u0_document_device_detail();
                        _selectDicision.decision_idx = STARTNODEQATRANFER;
                        _selectDicision.m0_actor_idx = _u1tempListEquipmentHolderTranferHead[0].m0_actor_idx;
                        _selectDicision.m0_node_idx = _u1tempListEquipmentHolderTranferHead[0].m0_node_idx;
                        _selectDicision.u0_device_idx = _u1tempListEquipmentHolderTranferHead[0].u0_device_idx;
                        _selectDicision.emp_idx_create = _emp_idx;
                        _data_qa_cims.qa_cims_u0_document_device_list[0] = _selectDicision;
                    }
                    else
                    {
                        _data_qa_cims.qa_cims_u0_document_device_list = new qa_cims_u0_document_device_detail[1];
                        qa_cims_u0_document_device_detail _selectDicision = new qa_cims_u0_document_device_detail();
                        _selectDicision.decision_idx = int.Parse(lblDecisionQATranferView.Text);
                        _selectDicision.m0_actor_idx = _u1tempListEquipmentHolderTranferHead[0].m0_actor_idx;
                        _selectDicision.m0_node_idx = _u1tempListEquipmentHolderTranferHead[0].m0_node_idx;
                        _selectDicision.u0_device_idx = _u1tempListEquipmentHolderTranferHead[0].u0_device_idx;
                        _selectDicision.emp_idx_create = _emp_idx;
                        _data_qa_cims.qa_cims_u0_document_device_list[0] = _selectDicision;
                    }

                    _data_qa_cims.qa_cims_u1_document_device_list = u1_doc_cutoff_someAll;
                    //_data_qa_cims = callServicePostQaDocCIMS(_urlCimsSetDocumentUpdate, _data_qa_cims);
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_qa_cims));

                    //divdocumentList.Visible = true;
                    //divFVVQATranfer.Visible = false;

                    //CheckAllQATranfer.Checked = false;
                    //btnBackToDocList.Visible = false;
                    //divDocDetailUser.Visible = false;
                    //divSearchDocument.Visible = false;

                    ////litDebug.Text = "We Here 2";

                    //data_qa_cims renew = new data_qa_cims();
                    //renew.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
                    //qa_cims_u1_document_device_detail _selectu0DocList = new qa_cims_u1_document_device_detail();

                    //renew.qa_cims_u1_document_device_list[0] = _selectu0DocList;

                    //renew = callServicePostQaDocCIMS(_urlCimsGetEquipmentList, renew);

                    //setGridData(gvDocumentList, renew.qa_cims_u0_document_device_list);
                }

                //litDebug.Text += STARTNODEQACUTOFF;

                break;

            case "cmdInserEquipmentNew":

                var _input_serailnumber = (TextBox)pnEquipmentNew.FindControl("tbserailnumber");
                var _input_details_equipment = (TextBox)pnEquipmentNew.FindControl("tbdetails_equipment");
                var _input_measuring_range_min = (TextBox)pnEquipmentNew.FindControl("tbMeasuringRangeMin");
                var _input_measuring_range_max = (TextBox)pnEquipmentNew.FindControl("tbMeasuringRangeMax");
                var _input_resolution = (TextBox)pnEquipmentNew.FindControl("tbResolution");
                var _input_acceptance_criteria = (TextBox)pnEquipmentNew.FindControl("tbAcceptanceCriteria");
                var _input_calibration_frequency = (TextBox)pnEquipmentNew.FindControl("tbCalibrationFrequency");
                var _input_range_of_use_min = (TextBox)pnEquipmentNew.FindControl("tbRangeOfUseMin");
                var _input_range_of_use_max = (TextBox)pnEquipmentNew.FindControl("tbRangeOfUseMax");
                var _input_id_no = (TextBox)pnEquipmentNew.FindControl("tbIDNo");

                var _machine_name = (DropDownList)pnEquipmentNew.FindControl("ddl_machine_name");
                var _machine_brand = (DropDownList)pnEquipmentNew.FindControl("ddl_machine_brand");
                var _calibration_type = (DropDownList)pnEquipmentNew.FindControl("ddlCalibrationType");
                var _machine_type = (DropDownList)pnEquipmentNew.FindControl("ddlmachineType");
                var _range_of_use_unit = (DropDownList)pnEquipmentNew.FindControl("ddl_range_of_use_unit");
                var _measuring_range_unit = (DropDownList)pnEquipmentNew.FindControl("ddl_measuring_range_unit");
                var _resolution_unit = (DropDownList)pnEquipmentNew.FindControl("ddl_resolution_unit");
                var _acceptance_criteria_unit = (DropDownList)pnEquipmentNew.FindControl("ddl_acceptance_criteria_unit");

                var _input_certificate = (RadioButtonList)pnEquipmentNew.FindControl("rd_certificate");

                var ds_add_equipment_new = (DataSet)ViewState["vsDataEquipmentCalNew"];
                var dr_add_equipment_new = ds_add_equipment_new.Tables[0].NewRow();

                int numrow_new = ds_add_equipment_new.Tables[0].Rows.Count; //จำนวนแถว

                if (numrow_new > 0)
                {
                    dr_add_equipment_new["SerialNumber_New"] = _input_serailnumber.Text;
                    dr_add_equipment_new["Details_Equipment"] = _input_details_equipment.Text;
                    dr_add_equipment_new["BrandName_New"] = _machine_brand.SelectedItem.Text;
                    dr_add_equipment_new["CalibrationTypeName"] = _calibration_type.SelectedItem.Text;
                    dr_add_equipment_new["EquipmentType_New"] = _machine_type.SelectedItem.Text;
                    dr_add_equipment_new["RangeOfUseUnitName"] = _range_of_use_unit.SelectedItem.Text;
                    dr_add_equipment_new["Measuring_Range_UnitName"] = _measuring_range_unit.SelectedItem.Text;
                    dr_add_equipment_new["Resolution_UnitName"] = _resolution_unit.SelectedItem.Text;
                    dr_add_equipment_new["Acceptance_Criteria_UnitName"] = _acceptance_criteria_unit.SelectedItem.Text;
                    dr_add_equipment_new["EquipmentName_New"] = _machine_name.SelectedItem.Text;
                    dr_add_equipment_new["IDCodeNumber_New"] = _input_id_no.Text;

                    dr_add_equipment_new["Measuring_Range_Min"] = _input_measuring_range_min.Text;
                    dr_add_equipment_new["Measuring_Range_Max"] = _input_measuring_range_max.Text;
                    dr_add_equipment_new["Resolution"] = _input_resolution.Text;
                    dr_add_equipment_new["Acceptance_Criteria"] = _input_acceptance_criteria.Text;
                    dr_add_equipment_new["Calibration_Frequency"] = _input_calibration_frequency.Text;
                    dr_add_equipment_new["RangeOfUseMin"] = _input_range_of_use_min.Text;
                    dr_add_equipment_new["RangeOfUseMax"] = _input_range_of_use_max.Text;
                    dr_add_equipment_new["Certificate_insert"] = _input_certificate.SelectedValue;

                    dr_add_equipment_new["M0BrandIDX_New"] = int.Parse(_machine_brand.SelectedValue);
                    dr_add_equipment_new["CalibrationType"] = int.Parse(_calibration_type.SelectedValue);
                    dr_add_equipment_new["EquipmentTypeIDX_New"] = int.Parse(_machine_type.SelectedValue);
                    dr_add_equipment_new["Acceptance_Criteria_Unit"] = int.Parse(_acceptance_criteria_unit.SelectedValue);
                    dr_add_equipment_new["Resolution_Unit"] = int.Parse(_resolution_unit.SelectedValue);
                    dr_add_equipment_new["Measuring_Range_Unit"] = int.Parse(_measuring_range_unit.SelectedValue);
                    dr_add_equipment_new["RangeOfUseUnit"] = int.Parse(_range_of_use_unit.SelectedValue);
                    dr_add_equipment_new["M0DeviceIDX_New"] = int.Parse(_machine_name.SelectedValue);

                    var dsCalibrationPoint = (DataSet)ViewState["vsDataCalibrationPoint"];
                    string addListCalpoint = "";
                    string addListCalpointIDX = "";

                    foreach (DataRow dr in dsCalibrationPoint.Tables[0].Rows)
                    {

                        addListCalpoint += dr["CalibrationPoint"].ToString() + ",";
                        addListCalpointIDX += dr["CalibrationPointUnitIDX"].ToString() + ",";
                    }

                    dr_add_equipment_new["CalibrationPointList"] = addListCalpoint;
                    dr_add_equipment_new["CalibrationPointList_IDX"] = addListCalpointIDX;

                }
                else
                {
                    dr_add_equipment_new["SerialNumber_New"] = _input_serailnumber.Text;
                    dr_add_equipment_new["Details_Equipment"] = _input_details_equipment.Text;
                    dr_add_equipment_new["BrandName_New"] = _machine_brand.SelectedItem.Text;
                    dr_add_equipment_new["CalibrationTypeName"] = _calibration_type.SelectedItem.Text;
                    dr_add_equipment_new["EquipmentType_New"] = _machine_type.SelectedItem.Text;
                    dr_add_equipment_new["RangeOfUseUnitName"] = _range_of_use_unit.SelectedItem.Text;
                    dr_add_equipment_new["Measuring_Range_UnitName"] = _measuring_range_unit.SelectedItem.Text;
                    dr_add_equipment_new["Resolution_UnitName"] = _resolution_unit.SelectedItem.Text;
                    dr_add_equipment_new["Acceptance_Criteria_UnitName"] = _acceptance_criteria_unit.SelectedItem.Text;
                    dr_add_equipment_new["EquipmentName_New"] = _machine_name.SelectedItem.Text;
                    dr_add_equipment_new["IDCodeNumber_New"] = _input_id_no.Text;

                    dr_add_equipment_new["Measuring_Range_Min"] = _input_measuring_range_min.Text;
                    dr_add_equipment_new["Measuring_Range_Max"] = _input_measuring_range_max.Text;
                    dr_add_equipment_new["Resolution"] = _input_resolution.Text;
                    dr_add_equipment_new["Acceptance_Criteria"] = _input_acceptance_criteria.Text;
                    dr_add_equipment_new["Calibration_Frequency"] = _input_calibration_frequency.Text;
                    dr_add_equipment_new["RangeOfUseMin"] = _input_range_of_use_min.Text;
                    dr_add_equipment_new["RangeOfUseMax"] = _input_range_of_use_max.Text;
                    dr_add_equipment_new["Certificate_insert"] = _input_certificate.SelectedValue;


                    dr_add_equipment_new["M0BrandIDX_New"] = int.Parse(_machine_brand.SelectedValue);
                    dr_add_equipment_new["CalibrationType"] = int.Parse(_calibration_type.SelectedValue);
                    dr_add_equipment_new["EquipmentTypeIDX_New"] = int.Parse(_machine_type.SelectedValue);
                    dr_add_equipment_new["Acceptance_Criteria_Unit"] = int.Parse(_acceptance_criteria_unit.SelectedValue);
                    dr_add_equipment_new["Resolution_Unit"] = int.Parse(_resolution_unit.SelectedValue);
                    dr_add_equipment_new["Measuring_Range_Unit"] = int.Parse(_measuring_range_unit.SelectedValue);
                    dr_add_equipment_new["RangeOfUseUnit"] = int.Parse(_range_of_use_unit.SelectedValue);
                    dr_add_equipment_new["M0DeviceIDX_New"] = int.Parse(_machine_name.SelectedValue);


                    var dsCalibrationPoint = (DataSet)ViewState["vsDataCalibrationPoint"];
                    string addListCalpoint = "";
                    string addListCalpointIDX = "";

                    foreach (DataRow dr in dsCalibrationPoint.Tables[0].Rows)
                    {

                        addListCalpoint += dr["CalibrationPoint"].ToString() + ",";
                        addListCalpointIDX += dr["CalibrationPointUnitIDX"].ToString() + ",";
                    }

                    dr_add_equipment_new["CalibrationPointList"] = addListCalpoint;
                    dr_add_equipment_new["CalibrationPointList_IDX"] = addListCalpointIDX;


                }
                ds_add_equipment_new.Tables[0].Rows.Add(dr_add_equipment_new);
                ViewState["vsDataEquipmentCalNew"] = ds_add_equipment_new;

                gvListEquipmentCalNew.Visible = true;
                setGridData(gvListEquipmentCalNew, (DataSet)ViewState["vsDataEquipmentCalNew"]);

                if (ViewState["vsDataEquipmentCalNew"] != null)
                {
                    divActionSaveCreateDocument.Visible = true;
                }
                else
                {
                    divActionSaveCreateDocument.Visible = false;

                }

                ClearDataCalibrationPoint();

                setGridData(gvCalPointInsert, (DataSet)ViewState["vsDataCalibrationPoint_insert"]);
                break;

            case "DeletePoint":

                string[] arg1 = new string[3];
                arg1 = e.CommandArgument.ToString().Split(',');
                string idnumber = (arg1[0]);
                int condition = int.Parse(arg1[1]);
                string calpoint = (arg1[2]);

                switch (condition)
                {
                    case 0:
                        var ds_point_delete = (DataSet)ViewState["vsDataCalibrationPoint_insert"];
                        ViewState["idnumber"] = idnumber;
                        ViewState["point_cal"] = calpoint;

                        for (int counter_point = 0; counter_point < ds_point_delete.Tables[0].Rows.Count; counter_point++)
                        {
                            if (ds_point_delete.Tables[0].Rows[counter_point]["IDNUMBER_insert"].ToString() == ViewState["idnumber"].ToString() &&
                                ds_point_delete.Tables[0].Rows[counter_point]["CalibrationPoint_insert"].ToString() == ViewState["point_cal"].ToString())
                            {
                                ds_point_delete.Tables[0].Rows[counter_point].Delete();
                                break;
                            }
                        }

                        var ds_key_point_delete = (DataSet)ViewState["vsDataCalibrationPoint"];

                        for (int counter_keypoint = 0; counter_keypoint < ds_key_point_delete.Tables[0].Rows.Count; counter_keypoint++)
                        {
                            if (ds_key_point_delete.Tables[0].Rows[counter_keypoint]["IDNUMBER"].ToString() == ViewState["idnumber"].ToString() &&
                                ds_key_point_delete.Tables[0].Rows[counter_keypoint]["CalibrationPoint"].ToString() == ViewState["point_cal"].ToString())
                            {
                                ds_key_point_delete.Tables[0].Rows[counter_keypoint].Delete();
                                break;
                            }
                        }
                        setGridData(gvCalPointInsert, (DataSet)ViewState["vsDataCalibrationPoint_insert"]);
                        setGridData(gvCalibrationPoint, (DataSet)ViewState["vsDataCalibrationPoint"]);

                        break;
                    case 1:

                        var ds_point_delete_list = (DataSet)ViewState["vsDataCalibrationPoint_insert"];
                        ViewState["idnumber_list"] = idnumber;
                        // :: for loop multiple delete cal set point in gvCalPointInsert :: 
                        for (int counter_point_in_doc = gvCalPointInsert.Rows.Count - 1; counter_point_in_doc >= 0; counter_point_in_doc--)
                            if (ds_point_delete_list.Tables[0].Rows[counter_point_in_doc]["IDNUMBER_insert"].ToString() == ViewState["idnumber_list"].ToString())
                                ds_point_delete_list.Tables[0].Rows.RemoveAt(counter_point_in_doc);

                        setGridData(gvCalPointInsert, (DataSet)ViewState["vsDataCalibrationPoint_insert"]);

                        // :: delete one only list equipment new in gvListEquipmentCalNew ::
                        var ds_list_equipment_cal = (DataSet)ViewState["vsDataEquipmentCalNew"];
                        for (int counter_cal_equipment = 0; counter_cal_equipment < ds_list_equipment_cal.Tables[0].Rows.Count; counter_cal_equipment++)
                        {
                            if (ds_list_equipment_cal.Tables[0].Rows[counter_cal_equipment]["IDCodeNumber_New"].ToString() == ViewState["idnumber_list"].ToString())
                            {
                                ds_list_equipment_cal.Tables[0].Rows[counter_cal_equipment].Delete();
                                break;
                            }
                        }
                        setGridData(gvListEquipmentCalNew, (DataSet)ViewState["vsDataEquipmentCalNew"]);

                        break;
                }


                break;

            case "cmdView":

                string[] cmdArgView = cmdArg.Split(',');
                int _u0CalDocument = int.TryParse(cmdArgView[0].ToString(), out _default_int) ? int.Parse(cmdArgView[0].ToString()) : _default_int;
                int _typeDocument = int.TryParse(cmdArgView[1].ToString(), out _default_int) ? int.Parse(cmdArgView[1].ToString()) : _default_int;
                int _typeEquipment = int.TryParse(cmdArgView[2].ToString(), out _default_int) ? int.Parse(cmdArgView[2].ToString()) : _default_int;
                int _certificate = int.TryParse(cmdArgView[3].ToString(), out _default_int) ? int.Parse(cmdArgView[3].ToString()) : _default_int;
                int _u1CalDocument = int.TryParse(cmdArgView[4].ToString(), out _default_int) ? int.Parse(cmdArgView[4].ToString()) : _default_int;


               // litDebug.Text = _u0CalDocument.ToString();
                switch (_typeDocument)
                {
                    case 1: //ประเภทเอกสารหลัก
                        setActiveTab("docCreate", _u0CalDocument, 0, 0, 0, 0, 0);
                        break;
                    case 2: // ประเภทรายการของ แต่ละเครื่องมือที่สอบเทียบ
                        setActiveTab("docCreate", _u0CalDocument, _u1CalDocument, 0, 0, _typeDocument, _certificate);
                        break;
                }
                break;

            case "cmdDocCancel":
                int back_conddition = int.Parse(cmdArg);

                switch (back_conddition)
                {
                    case 0:
                        setActiveTab("docDetailList", 0, 0, 0, 0, 0, 0);
                        setOntop.Focus();
                        break;
                }



                break;

            case "cmdDocSave":

                int _node_idx = int.Parse(cmdArg);

                var gvCalibrationList = (GridView)fvCalDocumentList.FindControl("gvCalibrationList");

                switch (_node_idx)
                {
                    case 3:
                        foreach (GridViewRow gr in gvCalibrationList.Rows)
                        {
                            Label _status = (Label)gr.FindControl("lblstaidx");
                            DropDownList ddlChooseLabType = (DropDownList)gr.FindControl("ddlChooseLabType");
                            DropDownList ddlChooseLocation = (DropDownList)gr.FindControl("ddlChooseLocation");
                            TextBox txtCommentBeforeExtanalLab = (TextBox)gr.FindControl("txtCommentBeforeExtanalLab");

                            data_qa_cims _dtqaRecordLabCal = new data_qa_cims();
                            _dtqaRecordLabCal.qa_cims_u1_calibration_document_list = new qa_cims_u1_calibration_document_details[1];

                            var _u1doc_node3 = new qa_cims_u1_calibration_document_details[gvCalibrationList.Rows.Count];
                            int count_u1doc = 0;

                            Label lblDocIDX_Sample1 = (Label)gr.FindControl("lblDocIDX_Sample");

                            //_u1doc_node3[count_u1doc] = new qa_cims_u1_calibration_document_details();
                            //_u1doc_node3[count_u1doc].u1_cal_idx = int.Parse(lblDocIDX_Sample1.Text);
                            //_u1doc_node3[count_u1doc]. = int.Parse(ddlChooseLabType.SelectedValue);
                            //count_u1doc++;

                        }
                        break;
                }


                break;

            case "cmdSearchToolTranfer":

                if (tbSearchIDNoTranfer.Text != String.Empty)
                {
                    //getRegisterToolIDList(tbSearchIDNoTranfer, "0");
                    //tbSearchIDNoTranfer.Text = string.Empty;
                    //divAlertTranferSeacrh.Visible = false;

                    divAlertTranferSeacrh.Visible = false;

                    data_qa_cims _dataSearchTool = new data_qa_cims();
                    _dataSearchTool.qa_cims_m0_registration_device_list = new qa_cims_m0_registration_device[1];
                    qa_cims_m0_registration_device _selectRegistration = new qa_cims_m0_registration_device();
                    _selectRegistration.device_id_no = tbSearchIDNoTranfer.Text;
                    _selectRegistration.condition = 5;
                    _selectRegistration.place_idx = int.Parse(tbPlaceIDXInDocument.Text);
                    _selectRegistration.cal_type_idx = int.Parse(tbLabIDXInDocument.Text);
                    _selectRegistration.device_rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
                    _dataSearchTool.qa_cims_m0_registration_device_list[0] = _selectRegistration;

                    _dataSearchTool = callServicePostMasterQACIMS(_urlCimsGetRegistrationDevice, _dataSearchTool);

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataSearchTool.qa_cims_m0_registration_device_list));

                    if (_dataSearchTool.qa_cims_m0_registration_device_list != null)
                    {
                        setFormData(fvDetailSearchTranfer, FormViewMode.ReadOnly, _dataSearchTool.qa_cims_m0_registration_device_list);
                        divAlertTranferSeacrh.Visible = false;
                        //tbSearchIDNoTranfer.Text = String.Empty;
                        //  litDebug.Text = "######";
                    }
                    else
                    {
                        divAlertTranferSeacrh.Visible = true;
                        setFormData(fvDetailSearchTranfer, FormViewMode.ReadOnly, null);
                        //litDebug.Text = "1111";
                    }
                }
                else
                {
                    divAlertTranferSeacrh.Visible = true;
                    setFormData(fvDetailSearchTranfer, FormViewMode.ReadOnly, null);
                    //tbSearchIDNoTranfer.Text = String.Empty;
                    //litDebug.Text = "######";
                }
                //tbSearchIDNoTranfer.Text = String.Empty;

                break;

            case "cmdCheckFromSearch":

                TextBox tbIDCodeEquipment = (TextBox)fvDetailSearchTranfer.FindControl("tbIDCodeEquipment");

                foreach (GridViewRow rowItems in gvListEquipment.Rows)
                {
                    CheckBox chk = (CheckBox)rowItems.FindControl("ChkEquipment");
                    Label lblEquipment_IDNO = (Label)rowItems.FindControl("lblEquipment_IDNO");

                    if (tbIDCodeEquipment.Text == lblEquipment_IDNO.Text)
                    {
                        chk.Checked = true;
                        checkindexchange(chk, EventArgs.Empty);
                    }
                    else
                    {

                    }
                }

                break;

            case "cmdSearchToolCutoff":

                if (tbSearchIDNoCutoff.Text != String.Empty)
                {
                    //getRegisterToolIDList(tbSearchIDNoTranfer, "0");
                    //tbSearchIDNoTranfer.Text = string.Empty;
                    //divAlertTranferSeacrh.Visible = false;

                    divAlertCutoffSeacrh.Visible = false;

                    data_qa_cims _dataSearchTool = new data_qa_cims();
                    _dataSearchTool.qa_cims_m0_registration_device_list = new qa_cims_m0_registration_device[1];
                    qa_cims_m0_registration_device _selectRegistration = new qa_cims_m0_registration_device();
                    _selectRegistration.device_id_no = tbSearchIDNoCutoff.Text;
                    _selectRegistration.condition = 5;
                    _selectRegistration.place_idx = int.Parse(tbPlaceIDXInDocument.Text);
                    _selectRegistration.cal_type_idx = int.Parse(tbLabIDXInDocument.Text);
                    _selectRegistration.device_rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
                    _selectRegistration.device_rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
                    _dataSearchTool.qa_cims_m0_registration_device_list[0] = _selectRegistration;

                    _dataSearchTool = callServicePostMasterQACIMS(_urlCimsGetRegistrationDevice, _dataSearchTool);

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataSearchTool.qa_cims_m0_registration_device_list));

                    if (_dataSearchTool.qa_cims_m0_registration_device_list != null)
                    {
                        setFormData(fvDetailSearchCutoff, FormViewMode.ReadOnly, _dataSearchTool.qa_cims_m0_registration_device_list);
                        divAlertTranferSeacrh.Visible = false;
                        //litDebug.Text = "in";
                    }
                    else
                    {
                        divAlertCutoffSeacrh.Visible = true;
                        setFormData(fvDetailSearchCutoff, FormViewMode.ReadOnly, null);
                        //litDebug.Text = "1111";
                    }
                }
                else
                {
                    divAlertCutoffSeacrh.Visible = true;
                    setFormData(fvDetailSearchCutoff, FormViewMode.ReadOnly, null);
                    //litDebug.Text = "######";
                }

                //tbSearchIDNoCutoff.Text = String.Empty;

                break;

            case "cmdCheckFromSearchCutoff":

                TextBox tbIDCodeEquipmentCutoff = (TextBox)fvDetailSearchCutoff.FindControl("tbIDCodeEquipment");

                foreach (GridViewRow rowItems in gvListEquipmentCutoff.Rows)
                {
                    CheckBox chk = (CheckBox)rowItems.FindControl("ChkEquipmentCutoff");
                    Label lblDeviceIDX = (Label)rowItems.FindControl("lblDeviceIDX");
                    Label lblEquipment_IDNO = (Label)rowItems.FindControl("lblEquipment_IDNO");

                    //litDebug.Text += lblDeviceIDX.Text;

                    if (tbIDCodeEquipmentCutoff.Text == lblEquipment_IDNO.Text)
                    {
                        chk.Checked = true;

                        checkindexchange(chk, EventArgs.Empty);
                    }
                    else
                    {

                    }
                }

                break;

            case "cmdPlaceDocument":

                int cmdArgPlaceDocument = int.Parse(cmdArg);
                ViewState["vsArgPlaceDocument"] = cmdArgPlaceDocument;

                if (cmdArgPlaceDocument == 2)
                {
                    tbValuePlacecims.Text = "2";
                    tb_PlacecimsName.Text = "โรจนะ";
                }
                else if (cmdArgPlaceDocument == 3)
                {
                    tbValuePlacecims.Text = "3";
                    tb_PlacecimsName.Text = "นพวงศ์";
                }

                if (int.Parse(ViewState["rdept_permission"].ToString()) == 27)
                {
                    data_qa_cims _dataSelectPlaceDoc = new data_qa_cims();
                    _dataSelectPlaceDoc.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
                    qa_cims_u1_document_device_detail _select_place_doc = new qa_cims_u1_document_device_detail();
                    _select_place_doc.place_idx = cmdArgPlaceDocument;
                    //_select_place_doc.rsec_idx_create = int.Parse(ViewState["rsec_permission"].ToString());
                    _select_place_doc.condition = 6;
                    _dataSelectPlaceDoc.qa_cims_u1_document_device_list[0] = _select_place_doc;

                    _dataSelectPlaceDoc = callServicePostQaDocCIMS(_urlCimsGetEquipmentList, _dataSelectPlaceDoc);

                    if (_dataSelectPlaceDoc.return_code == 0)
                    {

                        //lblNamePlaceOnDocument.Text = "รายการเอกสารของสถานที่ : " + _dataSelectPlaceDoc.qa_cims_u0_document_device_list[0].place_name.ToString();

                        //lblNamePlaceOnDocument.Visible = true;
                        tbPlaceIDXInDocument.Text = cmdArgPlaceDocument.ToString();
                        tbPlaceNameInDocument.Text = _dataSelectPlaceDoc.qa_cims_u0_document_device_list[0].place_name.ToString();

                        ViewState["vsDocument"] = _dataSelectPlaceDoc.qa_cims_u0_document_device_list;

                    }
                    else
                    {

                        //lblNamePlaceOnDocument.Text = "รายการเอกสารของสถานที่ : " + _dataSelectPlaceDoc.return_msg.ToString();
                        //tbPlaceIDXInDocument.Text = _dataSelectPlaceDoc.qa_cims_u0_document_device_list[0].place_idx.ToString();
                        //tbPlaceNameInDocument.Text = _dataSelectPlaceDoc.return_msg.ToString();
                        //lblNamePlaceOnDocument.Visible = true;
                        ViewState["vsDocument"] = null;
                    }
                }
                else
                {
                    data_qa_cims _dataSelectPlaceDoc = new data_qa_cims();
                    _dataSelectPlaceDoc.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
                    qa_cims_u1_document_device_detail _select_place_doc = new qa_cims_u1_document_device_detail();
                    _select_place_doc.place_idx = cmdArgPlaceDocument;
                    _select_place_doc.rsec_idx_create = int.Parse(ViewState["rsec_permission"].ToString());
                    _select_place_doc.condition = 8;
                    _dataSelectPlaceDoc.qa_cims_u1_document_device_list[0] = _select_place_doc;

                    _dataSelectPlaceDoc = callServicePostQaDocCIMS(_urlCimsGetEquipmentList, _dataSelectPlaceDoc);

                    if (_dataSelectPlaceDoc.return_code == 0)
                    {

                        //lblNamePlaceOnDocument.Text = "รายการเอกสารของสถานที่ : " + _dataSelectPlaceDoc.qa_cims_u0_document_device_list[0].place_name.ToString();

                        //lblNamePlaceOnDocument.Visible = true;
                        tbPlaceIDXInDocument.Text = cmdArgPlaceDocument.ToString();
                        tbPlaceNameInDocument.Text = _dataSelectPlaceDoc.qa_cims_u0_document_device_list[0].place_name.ToString();

                        ViewState["vsDocument"] = _dataSelectPlaceDoc.qa_cims_u0_document_device_list;

                    }
                    else
                    {

                        //lblNamePlaceOnDocument.Text = "รายการเอกสารของสถานที่ : " + _dataSelectPlaceDoc.return_msg.ToString();
                        //tbPlaceIDXInDocument.Text = _dataSelectPlaceDoc.qa_cims_u0_document_device_list[0].place_idx.ToString();
                        //tbPlaceNameInDocument.Text = _dataSelectPlaceDoc.return_msg.ToString();
                        //lblNamePlaceOnDocument.Visible = true;
                        ViewState["vsDocument"] = null;
                    }
                }





                // show_search_lab.Visible = true;
                gvDocumentList.Visible = true;
                gvDocumentList.PageIndex = 0;

                setGridData(gvDocumentList, ViewState["vsDocument"]);

                break;
            case "cmdshowsearch_Registration":

                // linkBtnTrigger(btnSearchIndex1);
                seachregisOrganizationList(ddOrg_seachregis);
                seachregisDepartmentList(ddDept_seachregis, int.Parse(ddOrg_seachregis.SelectedItem.Value));
                seachregisSectionList(ddSec_seachregis, int.Parse(ddOrg_seachregis.SelectedItem.Value), int.Parse(ddDept_seachregis.SelectedItem.Value));

                btnsearchshow.Visible = false;
                btnhiddensearch.Visible = true;
                TabSearch.Visible = true;

                device_id_no_search.Text = string.Empty;
                equipment_name_search.Text = string.Empty;
                device_serial_search.Text = string.Empty;
                HiddenCalDateSearch.Value = string.Empty;
                HiddenDueDateSearch.Value = string.Empty;

                ddOrg_seachregis.SelectedValue = "-1";
                ddDept_seachregis.Items.Clear();
                ddDept_seachregis.Items.Insert(0, new ListItem("-- เลือกฝ่าย --", "-1"));
                ddSec_seachregis.Items.Clear();
                ddSec_seachregis.Items.Insert(0, new ListItem("-- เลือกแผนก --", "-1"));

                break;

            case "cmdSearchDetail":

                //DropDownList dllcaltype_search = (DropDownList)


                data_qa_cims data_SearchDetail = new data_qa_cims();
                data_SearchDetail.qa_cims_search_registration_device_list = new qa_cims_search_registration_device[1];
                qa_cims_search_registration_device m0_SearchRegistration1 = new qa_cims_search_registration_device();

                m0_SearchRegistration1.condition = 2;
                m0_SearchRegistration1.cal_type_idx = int.Parse(dllcaltype_search.SelectedValue);
                m0_SearchRegistration1.place_idx = int.Parse(ddlPlaceMachine_search.SelectedValue);
                m0_SearchRegistration1.rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
                m0_SearchRegistration1.rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());

                data_SearchDetail.qa_cims_search_registration_device_list[0] = m0_SearchRegistration1;

                data_SearchDetail = callServicePostMasterQACIMS(_urlCimsSearchRegistrationDevice, data_SearchDetail);

                if (int.Parse(ddlPlaceMachine_search.SelectedValue) != 0)
                {
                    tbPlaceNameInDocument.Text = ddlPlaceMachine_search.SelectedItem.ToString();
                    tbPlaceIDXInDocument.Text = ddlPlaceMachine_search.SelectedValue.ToString();
                }
                else
                {
                    tbPlaceNameInDocument.Text = "";
                    tbPlaceIDXInDocument.Text = ddlPlaceMachine_search.SelectedValue.ToString();
                }

                if (data_SearchDetail.return_code == 0)
                {
                    show_gvregistration.Visible = true;
                    ViewState["vsRegistrationDevice"] = data_SearchDetail.qa_cims_search_registration_device_list;
                    setGridData(gvRegistrationList, ViewState["vsRegistrationDevice"]);


                }
                else
                {

                    show_gvregistration.Visible = true;
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่พบข้อมูล!!!');", true);

                }

                break;

            case "cmdBackFirtIndex":

                tbPlaceIDXInDocument.Text = "0";
                tbPlaceNameInDocument.Text = "";

                getM0CalType((DropDownList)docRegistration.FindControl("dllcaltype_search"), "0");
                getSelectPlace((DropDownList)docRegistration.FindControl("ddlPlaceMachine_search"), "0", "0");

                ViewState["vsRegistrationDevice"] = null;
                setGridData(gvRegistrationList, ViewState["vsRegistrationDevice"]);


                break;
            case "cmdhiddensearch_Registration":

                btnsearchshow.Visible = true;
                btnhiddensearch.Visible = false;
                TabSearch.Visible = false;



                ClearGvRegisterDevices(tbPlaceIDXInDocument.Text, tbLabIDXInDocument.Text);


                //show_gvregistration.Visible = true;
                //ViewState["vsRegistrationDevice"] = data_SearchDetail.qa_cims_search_registration_device_list;
                //setGridData(gvRegistrationList, ViewState["vsRegistrationDevice"]);

                break;

            case "cmdSearchDocument":

                gvDocumentList.PageIndex = 0;
                TextBox txtseach_samplecode_lab = (TextBox)docList.FindControl("txtseach_samplecode_lab");


                if (txtseach_samplecode_lab.Text != String.Empty)
                {

                    data_qa_cims _dataSelectPlaceDoc_search = new data_qa_cims();

                    _dataSelectPlaceDoc_search.qa_cims_u1_search_document_device_list = new qa_cims_u1_search_document_device_detail[1];
                    qa_cims_u1_search_document_device_detail _select_place_docsearch = new qa_cims_u1_search_document_device_detail();

                    _select_place_docsearch.place_idx = int.Parse(tbPlaceIDXInDocument.Text);//int.Parse(ViewState["vsArgPlaceDocument"].ToString());
                    _select_place_docsearch.cal_type_idx = int.Parse(tbLabIDXInDocument.Text);//int.Parse(ViewState["vsArgPlaceDocument"].ToString());
                    _select_place_docsearch.rsec_idx_create = int.Parse(ViewState["rsec_permission"].ToString());
                    _select_place_docsearch.rdept_idx_create = int.Parse(ViewState["rdept_permission"].ToString());
                    _select_place_docsearch.device_id_no = txtseach_samplecode_lab.Text;
                    //_select_place_docsearch.condition = 6;

                    _dataSelectPlaceDoc_search.qa_cims_u1_search_document_device_list[0] = _select_place_docsearch;

                    //ViewState["rsec_permission"] = _dataEmployee.employee_list[0].rsec_idx;

                    _dataSelectPlaceDoc_search = callServicePostQaDocCIMS(_urlSearchInDocumentDevices, _dataSelectPlaceDoc_search);
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataSelectPlaceDoc_search));

                    if (_dataSelectPlaceDoc_search.return_code == 0)
                    {
                        ViewState["vsDocument"] = _dataSelectPlaceDoc_search.qa_cims_u1_search_document_device_list;

                        setGridData(gvDocumentList, ViewState["vsDocument"]);

                        txtseach_samplecode_lab.Text = string.Empty;
                    }
                    else
                    {
                        ViewState["vsDocument"] = null;//_dataSelectPlaceDoc_search.qa_cims_u1_search_document_device_list;

                        setGridData(gvDocumentList, ViewState["vsDocument"]);
                    }

                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณากรอกรหัสเครื่องมือก่อนทำการค้นหา!!!');", true);
                }


                break;

            case "cmdSearchReport":

                string search_idx = cmdArg;

                switch (search_idx)
                {
                    case "1": //Serach Devices Online in Report

                        DropDownList ddl_orgidx_report = (DropDownList)docReport.FindControl("ddl_orgidx_report");
                        DropDownList ddl_rdeptidx_report = (DropDownList)docReport.FindControl("ddl_rdeptidx_report");
                        DropDownList ddl_rsecidx_report = (DropDownList)docReport.FindControl("ddl_rsecidx_report");

                        data_qa_cims data_search_report = new data_qa_cims();
                        data_search_report.qa_cims_reportdevices_list = new qa_cims_reportdevices_detail[1];
                        qa_cims_reportdevices_detail m0_search_report = new qa_cims_reportdevices_detail();

                        m0_search_report.condition = 1;
                        m0_search_report.device_org_idx = int.Parse(ddl_orgidx_report.SelectedValue);
                        m0_search_report.device_rdept_idx = int.Parse(ddl_rdeptidx_report.SelectedValue);
                        m0_search_report.device_rsec_idx = int.Parse(ddl_rsecidx_report.SelectedValue);

                        data_search_report.qa_cims_reportdevices_list[0] = m0_search_report;


                        data_search_report = callServicePostQaDocCIMS(_urlCimsGetReportDevices, data_search_report);
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_search_report));

                        ViewState["vs_ReportDevices_Online"] = data_search_report.qa_cims_reportdevices_list;
                        setGridData(gvReportDevicesOnline, ViewState["vs_ReportDevices_Online"]);
                        break;
                    case "2":

                        DropDownList ddl_orgidx_report_offline = (DropDownList)docReport.FindControl("ddl_orgidx_report_offline");
                        DropDownList ddl_rdeptidx_report_offline = (DropDownList)docReport.FindControl("ddl_rdeptidx_report_offline");
                        DropDownList ddl_rsecidx_report_offline = (DropDownList)docReport.FindControl("ddl_rsecidx_report_offline");

                        data_qa_cims data_searchoffline_report = new data_qa_cims();
                        data_searchoffline_report.qa_cims_reportdevices_list = new qa_cims_reportdevices_detail[1];
                        qa_cims_reportdevices_detail m0_searchoffline_report = new qa_cims_reportdevices_detail();

                        m0_searchoffline_report.condition = 2;
                        m0_searchoffline_report.device_org_idx = int.Parse(ddl_orgidx_report_offline.SelectedValue);
                        m0_searchoffline_report.device_rdept_idx = int.Parse(ddl_rdeptidx_report_offline.SelectedValue);
                        m0_searchoffline_report.device_rsec_idx = int.Parse(ddl_rsecidx_report_offline.SelectedValue);

                        data_searchoffline_report.qa_cims_reportdevices_list[0] = m0_searchoffline_report;


                        data_searchoffline_report = callServicePostQaDocCIMS(_urlCimsGetReportDevices, data_searchoffline_report);
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_searchoffline_report));

                        ViewState["vs_ReportDevices_Offline"] = data_searchoffline_report.qa_cims_reportdevices_list;
                        setGridData(gvReportDevicesOffline, ViewState["vs_ReportDevices_Offline"]);


                        break;
                    case "3":

                        DropDownList ddl_orgidx_report_cutoff = (DropDownList)docReport.FindControl("ddl_orgidx_report_cutoff");
                        DropDownList ddl_rdeptidx_report_cutoff = (DropDownList)docReport.FindControl("ddl_rdeptidx_report_cutoff");
                        DropDownList ddl_rsecidx_report_cutoff = (DropDownList)docReport.FindControl("ddl_rsecidx_report_cutoff");

                        data_qa_cims data_searchcutoff_report = new data_qa_cims();
                        data_searchcutoff_report.qa_cims_reportdevices_list = new qa_cims_reportdevices_detail[1];
                        qa_cims_reportdevices_detail m0_searchcutoff_report = new qa_cims_reportdevices_detail();

                        m0_searchcutoff_report.condition = 3;
                        m0_searchcutoff_report.device_org_idx = int.Parse(ddl_orgidx_report_cutoff.SelectedValue);
                        m0_searchcutoff_report.device_rdept_idx = int.Parse(ddl_rdeptidx_report_cutoff.SelectedValue);
                        m0_searchcutoff_report.device_rsec_idx = int.Parse(ddl_rsecidx_report_cutoff.SelectedValue);

                        data_searchcutoff_report.qa_cims_reportdevices_list[0] = m0_searchcutoff_report;


                        data_searchcutoff_report = callServicePostQaDocCIMS(_urlCimsGetReportDevices, data_searchcutoff_report);
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_searchoffline_report));

                        ViewState["vs_ReportDevices_Cutoff"] = data_searchcutoff_report.qa_cims_reportdevices_list;
                        setGridData(gvReportDevicesCutoff, ViewState["vs_ReportDevices_Cutoff"]);


                        break;
                }
                break;


            case "cmdSearch_dataRegistration":

                TextBox _equipment_name_search = (TextBox)docRegistration.FindControl("equipment_name_search");
                TextBox _device_serial_search = (TextBox)docRegistration.FindControl("device_serial_search");
                TextBox _device_id_no_search = (TextBox)docRegistration.FindControl("device_id_no_search");

                HiddenField _HiddenCalDateSearch = (HiddenField)docRegistration.FindControl("HiddenCalDateSearch");
                HiddenField _HiddenDueDateSearch = (HiddenField)docRegistration.FindControl("HiddenDueDateSearch");

                DropDownList _ddlOrg = (DropDownList)docRegistration.FindControl("ddOrg_seachregis");
                DropDownList _ddlDept = (DropDownList)docRegistration.FindControl("ddDept_seachregis");
                DropDownList _ddlSec = (DropDownList)docRegistration.FindControl("ddSec_seachregis");

                data_qa_cims data_SearchRegistration1 = new data_qa_cims();
                data_SearchRegistration1.qa_cims_search_registration_device_list = new qa_cims_search_registration_device[1];
                qa_cims_search_registration_device SearchRegistration1 = new qa_cims_search_registration_device();
                SearchRegistration1.condition = 1;
                SearchRegistration1.place_idx = int.Parse(tbPlaceIDXInDocument.Text);
                SearchRegistration1.cal_type_idx = int.Parse(tbLabIDXInDocument.Text);
                SearchRegistration1.equipment_name = _equipment_name_search.Text;
                SearchRegistration1.device_id_no = _device_id_no_search.Text;
                SearchRegistration1.device_serial = _device_serial_search.Text;
                SearchRegistration1.device_cal_date = _HiddenCalDateSearch.Value.ToString();
                SearchRegistration1.device_due_date = _HiddenDueDateSearch.Value.ToString();
                SearchRegistration1.device_org_idx = int.Parse(_ddlOrg.SelectedValue);
                SearchRegistration1.device_rdept_idx = int.Parse(_ddlDept.SelectedValue);
                SearchRegistration1.device_rsec_idx = int.Parse(_ddlSec.SelectedValue);
                SearchRegistration1.rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
                SearchRegistration1.rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());


                data_SearchRegistration1.qa_cims_search_registration_device_list[0] = SearchRegistration1;

                data_SearchRegistration1 = callServicePostMasterQACIMS(_urlCimsSearchRegistrationDevice, data_SearchRegistration1);

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_SearchRegistration1));
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_SearchRegistration1));

                if (data_SearchRegistration1.return_code == 0)
                {
                    ViewState["vsRegistrationDevice"] = data_SearchRegistration1.qa_cims_search_registration_device_list;
                    setGridData(gvRegistrationList, ViewState["vsRegistrationDevice"]);

                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่พบข้อมูล!!!');", true);
                    setGridData(gvRegistrationList, null);
                    // break;
                }

                break;

            case "cmdReset_dataRegistration":
                equipment_name_search.Text = string.Empty;
                device_id_no_search.Text = string.Empty;
                device_serial_search.Text = string.Empty;

                HiddenCalDateSearch.Value = string.Empty;
                HiddenDueDateSearch.Value = string.Empty;

                ddOrg_seachregis.SelectedValue = "-1";
                ddDept_seachregis.Items.Clear();
                ddDept_seachregis.Items.Insert(0, new ListItem("-- เลือกฝ่าย --", "-1"));
                ddSec_seachregis.Items.Clear();
                ddSec_seachregis.Items.Insert(0, new ListItem("-- เลือกแผนก --", "-1"));


                break;
            case "cmdDocumentPlacecims":
                string[] arg3 = new string[2];
                arg3 = e.CommandArgument.ToString().Split(';');
                int SETSHOWDOCUMENTPLACECIMS = int.Parse(arg3[0]);
                string SET_PlaceName = arg3[1];


                //
                if (SETSHOWDOCUMENTPLACECIMS == 2)
                {
                    tbPlaceIDXInDocument.Text = "2";
                    tbPlaceNameInDocument.Text = "โรจนะ";


                }
                else if (SETSHOWDOCUMENTPLACECIMS == 3)
                {
                    tbPlaceIDXInDocument.Text = "3";
                    tbPlaceNameInDocument.Text = "นพวงศ์";
                }
                //



                ViewState["Showplacecims"] = SETSHOWDOCUMENTPLACECIMS;
                tbValuePlacecims.Text = (SETSHOWDOCUMENTPLACECIMS.ToString());
                tb_PlacecimsName.Text = SET_PlaceName;

                data_qa_cims datasertRegistration = new data_qa_cims();
                datasertRegistration.qa_cims_m0_registration_device_list = new qa_cims_m0_registration_device[1];
                qa_cims_m0_registration_device registrationsert = new qa_cims_m0_registration_device();
                registrationsert.rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
                registrationsert.rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
                registrationsert.device_place_idx = int.Parse(tbValuePlacecims.Text);
                registrationsert.condition = 2;

                datasertRegistration.qa_cims_m0_registration_device_list[0] = registrationsert;

                datasertRegistration = callServicePostQaDocCIMS(_urlCimsGetRegistrationDevice, datasertRegistration);

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(datasertRegistration));

                setGridData(gvRegistrationList, ViewState["vsRegistrationDevice"]);

                break;
            case "cmdResetSearchPage":

                data_qa_cims dataResetsertRegistration = new data_qa_cims();
                dataResetsertRegistration.qa_cims_m0_registration_device_list = new qa_cims_m0_registration_device[1];
                qa_cims_m0_registration_device resetregistrationsert = new qa_cims_m0_registration_device();

                if (ViewState["rdept_permission"].ToString() == "27")
                {

                    dataResetsertRegistration.qa_cims_m0_registration_device_list[0] = resetregistrationsert;
                    dataResetsertRegistration = callServicePostQaDocCIMS(_urlCimsGetRegistrationDevice, dataResetsertRegistration);
                    ViewState["vsRegistrationDevice"] = dataResetsertRegistration.qa_cims_m0_registration_device_list;

                }
                else
                {
                    resetregistrationsert.condition = 4;
                    resetregistrationsert.device_rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
                    dataResetsertRegistration.qa_cims_m0_registration_device_list[0] = resetregistrationsert;

                    dataResetsertRegistration = callServicePostQaDocCIMS(_urlCimsGetRegistrationDevice, dataResetsertRegistration);
                    ViewState["vsRegistrationDevice"] = dataResetsertRegistration.qa_cims_m0_registration_device_list;

                }
                resetregistrationsert.device_rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
                resetregistrationsert.device_rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
                setGridData(gvRegistrationList, ViewState["vsRegistrationDevice"]);

                tbPlaceIDXInDocument.Text = "0";
                tbValuePlacecims.Text = "0";
                tbPlaceNameInDocument.Text = "";
                tb_PlacecimsName.Text = "";

                break;

            case "QASubmitCutoff":
                GridView EquipmentQACutoffNew = (GridView)divFVVQACutoff.FindControl("gvEquipmentQACutoff");

                qa_cims_u1_document_device_detail[] _u1tempListEquipmentHolderCutoffNew = (qa_cims_u1_document_device_detail[])ViewState["DocEquipmentList"];

                var u1_doc_cutoff_someAllnew = new qa_cims_u1_document_device_detail[_u1tempListEquipmentHolderCutoffNew.Count()];
                int count_someAllnew = 0;
                int count_row_checknew = 0;
                ViewState["count_row_check"] = 0;

                ViewState["va_status_qa_approve_cut"] = 0;
                foreach (GridViewRow rowItems in gvEquipmentQACutoff.Rows)
                {
                    CheckBox chk = (CheckBox)rowItems.FindControl("ChkEquipmentQACutoff");
                    Label lblDeviceIDX = (Label)rowItems.FindControl("lblDeviceIDX");
                    RadioButtonList rblQAApproveCutoff = (RadioButtonList)rowItems.FindControl("rblQAApproveCutoff");
                    TextBox txtcomment_qacutoff = (TextBox)rowItems.FindControl("txtcomment_qacutoff");
                    
                    if (rblQAApproveCutoff.SelectedValue == "1") //approve
                    {
                        u1_doc_cutoff_someAllnew[count_someAllnew] = new qa_cims_u1_document_device_detail();
                        u1_doc_cutoff_someAllnew[count_someAllnew].u1_device_idx = int.Parse(lblDeviceIDX.Text);
                        u1_doc_cutoff_someAllnew[count_someAllnew].decision_idx = int.Parse(lbl_HeadQAapprove.Text);
                        u1_doc_cutoff_someAllnew[count_someAllnew].u0_device_idx = _u1tempListEquipmentHolderCutoffNew[count_someAllnew].u0_device_idx;
                        u1_doc_cutoff_someAllnew[count_someAllnew].m0_actor_idx = _u1tempListEquipmentHolderCutoffNew[count_someAllnew].m0_actor_idx;
                        u1_doc_cutoff_someAllnew[count_someAllnew].m0_node_idx = _u1tempListEquipmentHolderCutoffNew[count_someAllnew].m0_node_idx;
                        u1_doc_cutoff_someAllnew[count_someAllnew].condition = 2;
                        u1_doc_cutoff_someAllnew[count_someAllnew].status = 1;
                        if (txtcomment_qacutoff.Text == "")
                        {
                            u1_doc_cutoff_someAllnew[count_someAllnew].comment = "-";
                        }
                        else
                        {
                            u1_doc_cutoff_someAllnew[count_someAllnew].comment = txtcomment_qacutoff.Text;
                        }
                        u1_doc_cutoff_someAllnew[count_someAllnew].cemp_idx = _u1tempListEquipmentHolderCutoffNew[count_someAllnew].cemp_idx;


                        count_row_checknew++;
                        count_someAllnew++;

                        ViewState["count_row_check"] = count_row_checknew;

                    }
                    else if (rblQAApproveCutoff.SelectedValue == "2") // not approve
                    {
                        u1_doc_cutoff_someAllnew[count_someAllnew] = new qa_cims_u1_document_device_detail();
                        u1_doc_cutoff_someAllnew[count_someAllnew].u1_device_idx = int.Parse(lblDeviceIDX.Text);
                        u1_doc_cutoff_someAllnew[count_someAllnew].decision_idx = int.Parse(lbl_HeadQANotapprove.Text);
                        u1_doc_cutoff_someAllnew[count_someAllnew].u0_device_idx = _u1tempListEquipmentHolderCutoffNew[count_someAllnew].u0_device_idx;
                        u1_doc_cutoff_someAllnew[count_someAllnew].m0_actor_idx = _u1tempListEquipmentHolderCutoffNew[count_someAllnew].m0_actor_idx;
                        u1_doc_cutoff_someAllnew[count_someAllnew].m0_node_idx = _u1tempListEquipmentHolderCutoffNew[count_someAllnew].m0_node_idx;
                        u1_doc_cutoff_someAllnew[count_someAllnew].condition = 2;
                        u1_doc_cutoff_someAllnew[count_someAllnew].status = 1;
                        if (txtcomment_qacutoff.Text == "")
                        {
                            u1_doc_cutoff_someAllnew[count_someAllnew].comment = "-";
                        }
                        else
                        {
                            u1_doc_cutoff_someAllnew[count_someAllnew].comment = txtcomment_qacutoff.Text;
                        }
                        u1_doc_cutoff_someAllnew[count_someAllnew].cemp_idx = _u1tempListEquipmentHolderCutoffNew[count_someAllnew].cemp_idx;


                        count_row_checknew++;
                        count_someAllnew++;

                        ViewState["count_row_check"] = count_row_checknew;
                        ViewState["va_status_qa_approve_cut"] = 1;

                    }
                }

                //litDebug.Text = ViewState["count_row_check"].ToString();


                if (int.Parse(ViewState["count_row_check"].ToString()) != EquipmentQACutoffNew.Rows.Count)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาเลือกข้อมูลให้ครบถ้วน ก่อนทำการบันทึก!!!');", true);
                }
                else
                {

                    //if (int.Parse(ViewState["count_row_check"].ToString()) == EquipmentQACutoffNew.Rows.Count)
                    //{
                    _data_qa_cims.qa_cims_u0_document_device_list = new qa_cims_u0_document_device_detail[1];
                    qa_cims_u0_document_device_detail _selectDicision = new qa_cims_u0_document_device_detail();


                    if(ViewState["va_status_qa_approve_cut"].ToString() == "1")
                    {
                        _selectDicision.decision_idx = int.Parse(lbl_HeadQANotapprove.Text);
                    }
                    else
                    {
                        _selectDicision.decision_idx = int.Parse(lbl_HeadQAapprove.Text);
                    }
                    


                    _selectDicision.m0_actor_idx = _u1tempListEquipmentHolderCutoffNew[0].m0_actor_idx;
                    _selectDicision.m0_node_idx = _u1tempListEquipmentHolderCutoffNew[0].m0_node_idx;
                    _selectDicision.u0_device_idx = _u1tempListEquipmentHolderCutoffNew[0].u0_device_idx;
                    _selectDicision.emp_idx_create = _emp_idx;
                    _selectDicision.condition = 1;
                    _data_qa_cims.qa_cims_u0_document_device_list[0] = _selectDicision;


                    _data_qa_cims.qa_cims_u1_document_device_list = u1_doc_cutoff_someAllnew;
                    _data_qa_cims = callServicePostQaDocCIMS(_urlCimsSetDocumentUpdate, _data_qa_cims);
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_qa_cims));

                    //divdocumentList.Visible = true;
                    //divFVVQACutoff.Visible = false;

                    //CheckAllQACutoff.Checked = false;
                    //btnBackToDocList.Visible = false;
                    //divDocDetailUser.Visible = false;

                    setActiveTab("docList", 0, 0, 0, 0, 0, 0);
                    //BackToCurrentPageGvDocumentList(int.Parse(tbPlaceIDXInDocument.Text));
                    setOntop.Focus();
                    //setActiveTab("docList", 0, 0, 0, int.Parse(tbPlaceIDXInDocument.Text), 0, 0);

                }


                break;

            case "cmdHeadUserSubmitCutoff":
                GridView EquipmentHeadUser_Approve = (GridView)divFVVQACutoff.FindControl("gvEquipmentQACutoff");

                qa_cims_u1_document_device_detail[] _temp_status_23 = (qa_cims_u1_document_device_detail[])ViewState["DocEquipmentList"];

                var u1_node_20_ = new qa_cims_u1_document_device_detail[_temp_status_23.Count()];
                int count_someAllnew_20 = 0;
                int count_row_checknew_20 = 0;
                ViewState["count_row_check_20"] = 0;
                ViewState["vs_decision_20"] = 0;
                foreach (GridViewRow rowItems_20 in EquipmentHeadUser_Approve.Rows)
                {

                    Label lblDeviceIDX = (Label)rowItems_20.FindControl("lblDeviceIDX");
                    RadioButtonList rblQAApproveCutoff = (RadioButtonList)rowItems_20.FindControl("rblQAApproveCutoff");
                    TextBox txtcomment_qacutoff = (TextBox)rowItems_20.FindControl("txtcomment_qacutoff");

                    if (rblQAApproveCutoff.SelectedValue == "1") //approve
                    {
                        u1_node_20_[count_someAllnew_20] = new qa_cims_u1_document_device_detail();
                        u1_node_20_[count_someAllnew_20].u1_device_idx = int.Parse(lblDeviceIDX.Text);
                        u1_node_20_[count_someAllnew_20].decision_idx = int.Parse(lbl_HeadUserapprove.Text);
                        u1_node_20_[count_someAllnew_20].u0_device_idx = _temp_status_23[count_someAllnew_20].u0_device_idx;
                        u1_node_20_[count_someAllnew_20].m0_actor_idx = _temp_status_23[count_someAllnew_20].m0_actor_idx;
                        u1_node_20_[count_someAllnew_20].m0_node_idx = _temp_status_23[count_someAllnew_20].m0_node_idx;
                        u1_node_20_[count_someAllnew_20].condition = 2;
                        u1_node_20_[count_someAllnew_20].status = 0;
                        if (txtcomment_qacutoff.Text == "")
                        {
                            u1_node_20_[count_someAllnew_20].comment = "-";
                        }
                        else
                        {
                            u1_node_20_[count_someAllnew_20].comment = txtcomment_qacutoff.Text;
                        }
                        u1_node_20_[count_someAllnew_20].cemp_idx = _temp_status_23[count_someAllnew_20].cemp_idx;


                        count_row_checknew_20++;
                        count_someAllnew_20++;

                        ViewState["count_row_check_20"] = count_row_checknew_20;

                        //ViewState["vs_decision_20"] = 1;


                    }
                    else if (rblQAApproveCutoff.SelectedValue == "2")
                    {
                        u1_node_20_[count_someAllnew_20] = new qa_cims_u1_document_device_detail();
                        u1_node_20_[count_someAllnew_20].u1_device_idx = int.Parse(lblDeviceIDX.Text);
                        u1_node_20_[count_someAllnew_20].decision_idx = int.Parse(lbl_HeadUserNotapprove.Text);
                        u1_node_20_[count_someAllnew_20].u0_device_idx = _temp_status_23[count_someAllnew_20].u0_device_idx;
                        u1_node_20_[count_someAllnew_20].m0_actor_idx = _temp_status_23[count_someAllnew_20].m0_actor_idx;
                        u1_node_20_[count_someAllnew_20].m0_node_idx = _temp_status_23[count_someAllnew_20].m0_node_idx;
                        u1_node_20_[count_someAllnew_20].condition = 2;
                        u1_node_20_[count_someAllnew_20].status = 1;
                        if (txtcomment_qacutoff.Text == "")
                        {
                            u1_node_20_[count_someAllnew_20].comment = "-";
                        }
                        else
                        {
                            u1_node_20_[count_someAllnew_20].comment = txtcomment_qacutoff.Text;
                        }
                        u1_node_20_[count_someAllnew_20].cemp_idx = u1_node_20_[count_someAllnew_20].cemp_idx;


                        count_row_checknew_20++;
                        count_someAllnew_20++;

                        ViewState["count_row_check_20"] = count_row_checknew_20;

                        ViewState["vs_decision_20"] = 1;

                    }
                }

                //litDebug.Text = ViewState["count_row_check"].ToString();


                if (int.Parse(ViewState["count_row_check_20"].ToString()) != EquipmentHeadUser_Approve.Rows.Count)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาเลือกข้อมูลให้ครบถ้วน ก่อนทำการบันทึก!!!');", true);
                }
                else
                {

                    //if (int.Parse(ViewState["count_row_check"].ToString()) == EquipmentQACutoffNew.Rows.Count)
                    //{
                    _data_qa_cims.qa_cims_u0_document_device_list = new qa_cims_u0_document_device_detail[1];
                    qa_cims_u0_document_device_detail _selectDicision = new qa_cims_u0_document_device_detail();


                    if (ViewState["vs_decision_20"].ToString() == "1")
                    {
                        _selectDicision.decision_idx = int.Parse(lbl_HeadUserNotapprove.Text);
                    }
                    else
                    {
                        _selectDicision.decision_idx = int.Parse(lbl_HeadUserapprove.Text);
                    }

                    _selectDicision.m0_actor_idx = _temp_status_23[0].m0_actor_idx;
                    _selectDicision.m0_node_idx = _temp_status_23[0].m0_node_idx;
                    _selectDicision.u0_device_idx = _temp_status_23[0].u0_device_idx;
                    _selectDicision.emp_idx_create = _emp_idx;
                    _selectDicision.condition = 1;

                    _data_qa_cims.qa_cims_u0_document_device_list[0] = _selectDicision;
                    _data_qa_cims.qa_cims_u1_document_device_list = u1_node_20_;
                    _data_qa_cims = callServicePostQaDocCIMS(_urlCimsSetDocumentUpdate, _data_qa_cims);
                    //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_qa_cims));

                    ////divdocumentList.Visible = true;
                    ////divFVVQACutoff.Visible = false;

                    ////CheckAllQACutoff.Checked = false;
                    ////btnBackToDocList.Visible = false;
                    ////divDocDetailUser.Visible = false;

                    setActiveTab("docList", 0, 0, 0, 0, 0, 0);
                    //////BackToCurrentPageGvDocumentList(int.Parse(tbPlaceIDXInDocument.Text));
                    setOntop.Focus();
                    //////setActiveTab("docList", 0, 0, 0, int.Parse(tbPlaceIDXInDocument.Text), 0, 0);

                }


                break;

            case "QASubmitTranfer":
                GridView EquipmentQATranferNew = (GridView)divFVVQACutoff.FindControl("gvEquipmentQATranfer");

                qa_cims_u1_document_device_detail[] _u1tempListEquipmentHolderTranferNew = (qa_cims_u1_document_device_detail[])ViewState["DocEquipmentList"];

                var u1_doc_tranfer_someAllnew = new qa_cims_u1_document_device_detail[_u1tempListEquipmentHolderTranferNew.Count()];
                int count_someAllnew_TF = 0;
                int count_row_checknew_TF = 0;
                ViewState["count_row_check"] = 0;
                ViewState["vs_qa_tranfer_approve"] = 0;

                foreach (GridViewRow rowItems in gvEquipmentQATranfer.Rows)
                {
                    CheckBox chk = (CheckBox)rowItems.FindControl("ChkEquipmentQACutoff");
                    Label lblDeviceIDX = (Label)rowItems.FindControl("lblDeviceIDX");
                    RadioButtonList rblQAApproveTranfer = (RadioButtonList)rowItems.FindControl("rblQAApproveTranfer");
                    TextBox txtcomment_qacutoff = (TextBox)rowItems.FindControl("txtcomment_qacutoff");

                    if (rblQAApproveTranfer.SelectedValue == "1") //approve
                    {
                        u1_doc_tranfer_someAllnew[count_someAllnew_TF] = new qa_cims_u1_document_device_detail();
                        u1_doc_tranfer_someAllnew[count_someAllnew_TF].u1_device_idx = int.Parse(lblDeviceIDX.Text);
                        u1_doc_tranfer_someAllnew[count_someAllnew_TF].decision_idx = int.Parse(lblDecisionQATranferView.Text);
                        u1_doc_tranfer_someAllnew[count_someAllnew_TF].u0_device_idx = _u1tempListEquipmentHolderTranferNew[count_someAllnew_TF].u0_device_idx;
                        u1_doc_tranfer_someAllnew[count_someAllnew_TF].m0_actor_idx = _u1tempListEquipmentHolderTranferNew[count_someAllnew_TF].m0_actor_idx;
                        u1_doc_tranfer_someAllnew[count_someAllnew_TF].m0_node_idx = _u1tempListEquipmentHolderTranferNew[count_someAllnew_TF].m0_node_idx;
                        u1_doc_tranfer_someAllnew[count_someAllnew_TF].condition = 2;
                        u1_doc_tranfer_someAllnew[count_someAllnew_TF].status = 1;
                        if (txtcomment_qacutoff.Text == "")
                        {
                            u1_doc_tranfer_someAllnew[count_someAllnew_TF].comment = "-";
                        }
                        else
                        {
                            u1_doc_tranfer_someAllnew[count_someAllnew_TF].comment = txtcomment_qacutoff.Text;
                        }
                        u1_doc_tranfer_someAllnew[count_someAllnew_TF].cemp_idx = _u1tempListEquipmentHolderTranferNew[count_someAllnew_TF].cemp_idx;


                        count_row_checknew_TF++;
                        count_someAllnew_TF++;

                        ViewState["count_row_check"] = count_row_checknew_TF;
                        ViewState["vs_qa_tranfer_approve"] = 1;//lblDecisionQATranferView.Text;

                    }
                    else if (rblQAApproveTranfer.SelectedValue == "2") //not approve
                    {
                        u1_doc_tranfer_someAllnew[count_someAllnew_TF] = new qa_cims_u1_document_device_detail();
                        u1_doc_tranfer_someAllnew[count_someAllnew_TF].u1_device_idx = int.Parse(lblDeviceIDX.Text);
                        u1_doc_tranfer_someAllnew[count_someAllnew_TF].decision_idx = int.Parse(lblDecisionQATranferView2.Text);
                        u1_doc_tranfer_someAllnew[count_someAllnew_TF].u0_device_idx = _u1tempListEquipmentHolderTranferNew[count_someAllnew_TF].u0_device_idx;
                        u1_doc_tranfer_someAllnew[count_someAllnew_TF].m0_actor_idx = _u1tempListEquipmentHolderTranferNew[count_someAllnew_TF].m0_actor_idx;
                        u1_doc_tranfer_someAllnew[count_someAllnew_TF].m0_node_idx = _u1tempListEquipmentHolderTranferNew[count_someAllnew_TF].m0_node_idx;
                        u1_doc_tranfer_someAllnew[count_someAllnew_TF].condition = 2;
                        u1_doc_tranfer_someAllnew[count_someAllnew_TF].status = 0;
                        if (txtcomment_qacutoff.Text == "")
                        {
                            u1_doc_tranfer_someAllnew[count_someAllnew_TF].comment = "-";
                        }
                        else
                        {
                            u1_doc_tranfer_someAllnew[count_someAllnew_TF].comment = txtcomment_qacutoff.Text;
                        }
                        u1_doc_tranfer_someAllnew[count_someAllnew_TF].cemp_idx = _u1tempListEquipmentHolderTranferNew[count_someAllnew_TF].cemp_idx;

                        count_row_checknew_TF++;
                        count_someAllnew_TF++;

                        ViewState["count_row_check"] = count_row_checknew_TF;
                        //ViewState["vs_qa_tranfer_approve"] = 0;//lblDecisionQATranferView.Text;

                    }
                }
                //if (int.Parse(ViewState["count_row_check"].ToString()) == EquipmentQATranferNew.Rows.Count)
                //m0_devics_Index.rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
                //m0_devics_Index.rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
                if (int.Parse(ViewState["count_row_check"].ToString()) == EquipmentQATranferNew.Rows.Count)
                {
                    _data_qa_cims.qa_cims_u0_document_device_list = new qa_cims_u0_document_device_detail[1];
                    qa_cims_u0_document_device_detail _selectDicision = new qa_cims_u0_document_device_detail();

                    if (ViewState["vs_qa_tranfer_approve"].ToString() == "1")
                    {
                        _selectDicision.decision_idx = int.Parse(lblDecisionQATranferView.Text);
                    }
                    else
                    {
                        _selectDicision.decision_idx = int.Parse(lblDecisionQATranferView2.Text);
                    }

                    _selectDicision.m0_actor_idx = _u1tempListEquipmentHolderTranferNew[0].m0_actor_idx;
                    _selectDicision.m0_node_idx = _u1tempListEquipmentHolderTranferNew[0].m0_node_idx;
                    _selectDicision.u0_device_idx = _u1tempListEquipmentHolderTranferNew[0].u0_device_idx;
                    _selectDicision.emp_idx_create = _emp_idx;
                    _selectDicision.rdept_idx_create = int.Parse(ViewState["rdept_permission"].ToString()); ;
                    _selectDicision.rsec_idx_create = int.Parse(ViewState["rsec_permission"].ToString());
                    _selectDicision.condition = 1;
                    _data_qa_cims.qa_cims_u0_document_device_list[0] = _selectDicision;

                    _data_qa_cims.qa_cims_u1_document_device_list = u1_doc_tranfer_someAllnew;
                    _data_qa_cims = callServicePostQaDocCIMS(_urlCimsSetDocumentUpdate, _data_qa_cims);
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_qa_cims));

                    divdocumentList.Visible = true;
                    divFVVQATranfer.Visible = false;

                    CheckAllQATranfer.Checked = false;
                    btnBackToDocList.Visible = false;
                    divDocDetailUser.Visible = false;

                    setActiveTab("docList", 0, 0, 0, 0, 0, 0);
                    ////BackToCurrentPageGvDocumentList(int.Parse(tbPlaceIDXInDocument.Text));
                    setOntop.Focus();
                }

                else //if (int.Parse(ViewState["count_row_check"].ToString()) == 0)
                {

                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาเลือกข้อมูลให้ครบถ้วนก่อนทำการบันทึก!!!');", true);

                }

                break;

            case "cmdHeadSubmitTranfer":
                GridView gvEquipmentAllTranfer_node14 = (GridView)divFVVQACutoff.FindControl("gvEquipmentAllTranfer");

                qa_cims_u1_document_device_detail[] _temp_node14 = (qa_cims_u1_document_device_detail[])ViewState["DocEquipmentList"];

                var u1_doc_tranfer_headapprove_node14 = new qa_cims_u1_document_device_detail[_temp_node14.Count()];
                int count_someAllnew_node14 = 0;
                int count_row_checknew__node14 = 0;
                ViewState["count_row_check_node14"] = 0;
                ViewState["Vs_approve_Head"] = 0;


                foreach (GridViewRow rowItems in gvEquipmentAllTranfer.Rows)
                {

                    Label lblDeviceIDX_headtranfer = (Label)rowItems.FindControl("lblDeviceIDX_headtranfer");
                    RadioButtonList rbl_HeadApproveTranfer = (RadioButtonList)rowItems.FindControl("rbl_HeadApproveTranfer");
                    TextBox txtcomment_headtranfer = (TextBox)rowItems.FindControl("txtcomment_headtranfer");

                    if (rbl_HeadApproveTranfer.SelectedValue == "1") //approve
                    {
                        u1_doc_tranfer_headapprove_node14[count_someAllnew_node14] = new qa_cims_u1_document_device_detail();
                        u1_doc_tranfer_headapprove_node14[count_someAllnew_node14].u1_device_idx = int.Parse(lblDeviceIDX_headtranfer.Text);
                        u1_doc_tranfer_headapprove_node14[count_someAllnew_node14].decision_idx = int.Parse(lblDecisionHeadTranferView_approve.Text);
                        u1_doc_tranfer_headapprove_node14[count_someAllnew_node14].u0_device_idx = _temp_node14[count_someAllnew_node14].u0_device_idx;
                        u1_doc_tranfer_headapprove_node14[count_someAllnew_node14].m0_actor_idx = _temp_node14[count_someAllnew_node14].m0_actor_idx;
                        u1_doc_tranfer_headapprove_node14[count_someAllnew_node14].m0_node_idx = _temp_node14[count_someAllnew_node14].m0_node_idx;
                        u1_doc_tranfer_headapprove_node14[count_someAllnew_node14].condition = 2;
                        u1_doc_tranfer_headapprove_node14[count_someAllnew_node14].status = 1;
                        if (txtcomment_headtranfer.Text == "")
                        {
                            u1_doc_tranfer_headapprove_node14[count_someAllnew_node14].comment = "-";
                        }
                        else
                        {
                            u1_doc_tranfer_headapprove_node14[count_someAllnew_node14].comment = txtcomment_headtranfer.Text;
                        }
                        u1_doc_tranfer_headapprove_node14[count_someAllnew_node14].cemp_idx = u1_doc_tranfer_headapprove_node14[count_someAllnew_node14].cemp_idx;


                        count_row_checknew__node14++;
                        count_someAllnew_node14++;

                        ViewState["count_row_check_node14"] = count_row_checknew__node14;

                        ViewState["Vs_approve_Head"] = 1;

                    }
                    else if (rbl_HeadApproveTranfer.SelectedValue == "2") //not approve
                    {

                        //litDebug.Text = "33333";
                        u1_doc_tranfer_headapprove_node14[count_someAllnew_node14] = new qa_cims_u1_document_device_detail();
                        u1_doc_tranfer_headapprove_node14[count_someAllnew_node14].u1_device_idx = int.Parse(lblDeviceIDX_headtranfer.Text);
                        u1_doc_tranfer_headapprove_node14[count_someAllnew_node14].decision_idx = int.Parse(lblDecisionHeadTranferView_not.Text);
                        u1_doc_tranfer_headapprove_node14[count_someAllnew_node14].u0_device_idx = _temp_node14[count_someAllnew_node14].u0_device_idx;
                        u1_doc_tranfer_headapprove_node14[count_someAllnew_node14].m0_actor_idx = _temp_node14[count_someAllnew_node14].m0_actor_idx;
                        u1_doc_tranfer_headapprove_node14[count_someAllnew_node14].m0_node_idx = _temp_node14[count_someAllnew_node14].m0_node_idx;
                        u1_doc_tranfer_headapprove_node14[count_someAllnew_node14].condition = 2;
                        u1_doc_tranfer_headapprove_node14[count_someAllnew_node14].status = 0;
                        if (txtcomment_headtranfer.Text == "")
                        {
                            u1_doc_tranfer_headapprove_node14[count_someAllnew_node14].comment = "-";
                        }
                        else
                        {
                            u1_doc_tranfer_headapprove_node14[count_someAllnew_node14].comment = txtcomment_headtranfer.Text;
                        }
                        u1_doc_tranfer_headapprove_node14[count_someAllnew_node14].cemp_idx = u1_doc_tranfer_headapprove_node14[count_someAllnew_node14].cemp_idx;


                        count_someAllnew_node14++;
                        count_row_checknew__node14++;

                        ViewState["count_row_check_node14"] = count_row_checknew__node14;
                        //ViewState["Vs_approve_Head"] = 0;
                        //ViewState["Vs_notapprove_Head"] = lblDecisionHeadTranferView_not.Text;

                    }
                }
                //if (int.Parse(ViewState["count_row_check"].ToString()) == EquipmentQATranferNew.Rows.Count)
                //m0_devics_Index.rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
                //m0_devics_Index.rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
                if (int.Parse(ViewState["count_row_check_node14"].ToString()) > 0)
                {
                    _data_qa_cims.qa_cims_u0_document_device_list = new qa_cims_u0_document_device_detail[1];
                    qa_cims_u0_document_device_detail _selectDicision = new qa_cims_u0_document_device_detail();

                    if (ViewState["Vs_approve_Head"].ToString() == "1")
                    {
                        _selectDicision.decision_idx = int.Parse(lblDecisionHeadTranferView_approve.Text);
                    }
                    else
                    {
                        _selectDicision.decision_idx = int.Parse(lblDecisionHeadTranferView_not.Text);
                    }


                    _selectDicision.m0_actor_idx = _temp_node14[0].m0_actor_idx;
                    _selectDicision.m0_node_idx = _temp_node14[0].m0_node_idx;
                    _selectDicision.u0_device_idx = _temp_node14[0].u0_device_idx;
                    _selectDicision.emp_idx_create = _emp_idx;
                    _selectDicision.rdept_idx_create = int.Parse(ViewState["rdept_permission"].ToString()); ;
                    _selectDicision.rsec_idx_create = int.Parse(ViewState["rsec_permission"].ToString());
                    _selectDicision.condition = 1;

                    _data_qa_cims.qa_cims_u0_document_device_list[0] = _selectDicision;
                    _data_qa_cims.qa_cims_u1_document_device_list = u1_doc_tranfer_headapprove_node14;

                    _data_qa_cims = callServicePostQaDocCIMS(_urlCimsSetDocumentUpdate, _data_qa_cims);
                    //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_qa_cims));

                    ////divdocumentList.Visible = true;
                    ////divFVVQATranfer.Visible = false;

                    ////CheckAllQATranfer.Checked = false;
                    ////btnBackToDocList.Visible = false;
                    ////divDocDetailUser.Visible = false;

                    setActiveTab("docList", 0, 0, 0, 0, 0, 0);
                    ////////BackToCurrentPageGvDocumentList(int.Parse(tbPlaceIDXInDocument.Text));
                    setOntop.Focus();
                }
                else //if (int.Parse(ViewState["count_row_check"].ToString()) == 0)
                {

                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาพิจารณารายการโอนย้าย!!!');", true);

                }

                break;

            case "cmdHeadAceptSubmitTranfer":
                GridView gvEquipmentAllTranfer_node17 = (GridView)divFVVQACutoff.FindControl("gvEquipmentAllTranfer");

                qa_cims_u1_document_device_detail[] _temp_node17 = (qa_cims_u1_document_device_detail[])ViewState["DocEquipmentList"];

                var u1_doc_tranfer_headapprove_node17 = new qa_cims_u1_document_device_detail[_temp_node17.Count()];
                int count_someAllnew_node17 = 0;
                int count_row_checknew__node17 = 0;
                ViewState["count_row_check_node17"] = 0;
                ViewState["Vs_approve_HeadRecive"] = 0;//lblDecisionHeadReceiveTranferView_approve.Text;


                foreach (GridViewRow rowItems_n17 in gvEquipmentAllTranfer_node17.Rows)
                {

                    Label lblDeviceIDX_headtranfer_n17 = (Label)rowItems_n17.FindControl("lblDeviceIDX_headtranfer");
                    RadioButtonList rbl_HeadApproveTranfer_n17 = (RadioButtonList)rowItems_n17.FindControl("rbl_HeadApproveTranfer");
                    TextBox txtcomment_headtranfer_n17 = (TextBox)rowItems_n17.FindControl("txtcomment_headtranfer");

                    if (rbl_HeadApproveTranfer_n17.SelectedValue == "1") //approve
                    {
                        u1_doc_tranfer_headapprove_node17[count_someAllnew_node17] = new qa_cims_u1_document_device_detail();
                        u1_doc_tranfer_headapprove_node17[count_someAllnew_node17].u1_device_idx = int.Parse(lblDeviceIDX_headtranfer_n17.Text);
                        u1_doc_tranfer_headapprove_node17[count_someAllnew_node17].decision_idx = int.Parse(lblDecisionHeadReceiveTranferView_approve.Text);
                        u1_doc_tranfer_headapprove_node17[count_someAllnew_node17].u0_device_idx = _temp_node17[count_someAllnew_node17].u0_device_idx;
                        u1_doc_tranfer_headapprove_node17[count_someAllnew_node17].m0_actor_idx = _temp_node17[count_someAllnew_node17].m0_actor_idx;
                        u1_doc_tranfer_headapprove_node17[count_someAllnew_node17].m0_node_idx = _temp_node17[count_someAllnew_node17].m0_node_idx;
                        u1_doc_tranfer_headapprove_node17[count_someAllnew_node17].condition = 2;
                        u1_doc_tranfer_headapprove_node17[count_someAllnew_node17].status = 1;
                        if (txtcomment_headtranfer_n17.Text == "")
                        {
                            u1_doc_tranfer_headapprove_node17[count_someAllnew_node17].comment = "-";
                        }
                        else
                        {
                            u1_doc_tranfer_headapprove_node17[count_someAllnew_node17].comment = txtcomment_headtranfer_n17.Text;
                        }
                        u1_doc_tranfer_headapprove_node17[count_someAllnew_node17].cemp_idx = u1_doc_tranfer_headapprove_node17[count_someAllnew_node17].cemp_idx;


                        count_row_checknew__node17++;
                        count_someAllnew_node17++;

                        ViewState["count_row_check_node17"] = count_row_checknew__node17;

                        ViewState["Vs_approve_HeadRecive"] = 1;//lblDecisionHeadReceiveTranferView_approve.Text;

                    }
                    else if (rbl_HeadApproveTranfer_n17.SelectedValue == "2") //not approve
                    {
                        u1_doc_tranfer_headapprove_node17[count_someAllnew_node17] = new qa_cims_u1_document_device_detail();
                        u1_doc_tranfer_headapprove_node17[count_someAllnew_node17].u1_device_idx = int.Parse(lblDeviceIDX_headtranfer_n17.Text);
                        u1_doc_tranfer_headapprove_node17[count_someAllnew_node17].decision_idx = int.Parse(lblDecisionHeadReceiveTranferView_not.Text);
                        u1_doc_tranfer_headapprove_node17[count_someAllnew_node17].u0_device_idx = _temp_node17[count_someAllnew_node17].u0_device_idx;
                        u1_doc_tranfer_headapprove_node17[count_someAllnew_node17].m0_actor_idx = _temp_node17[count_someAllnew_node17].m0_actor_idx;
                        u1_doc_tranfer_headapprove_node17[count_someAllnew_node17].m0_node_idx = _temp_node17[count_someAllnew_node17].m0_node_idx;
                        u1_doc_tranfer_headapprove_node17[count_someAllnew_node17].condition = 2;
                        u1_doc_tranfer_headapprove_node17[count_someAllnew_node17].status = 0;
                        if (txtcomment_headtranfer_n17.Text == "")
                        {
                            u1_doc_tranfer_headapprove_node17[count_someAllnew_node17].comment = "-";
                        }
                        else
                        {
                            u1_doc_tranfer_headapprove_node17[count_someAllnew_node17].comment = txtcomment_headtranfer_n17.Text;
                        }
                        u1_doc_tranfer_headapprove_node17[count_someAllnew_node17].cemp_idx = u1_doc_tranfer_headapprove_node17[count_someAllnew_node17].cemp_idx;


                        count_someAllnew_node17++;
                        count_row_checknew__node17++;

                        ViewState["count_row_check_node17"] = count_row_checknew__node17;

                        //ViewState["Vs_approve_HeadRecive"] = 0;//lblDecisionHeadReceiveTranferView_approve.Text;

                    }
                }
                //if (int.Parse(ViewState["count_row_check"].ToString()) == EquipmentQATranferNew.Rows.Count)
                //m0_devics_Index.rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
                //m0_devics_Index.rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
                if (int.Parse(ViewState["count_row_check_node17"].ToString()) > 0)
                {
                    _data_qa_cims.qa_cims_u0_document_device_list = new qa_cims_u0_document_device_detail[1];
                    qa_cims_u0_document_device_detail _selectDicision = new qa_cims_u0_document_device_detail();

                    if (ViewState["Vs_approve_HeadRecive"].ToString() == "1")
                    {
                        _selectDicision.decision_idx = int.Parse(lblDecisionHeadReceiveTranferView_approve.Text);
                    }
                    else
                    {
                        _selectDicision.decision_idx = int.Parse(lblDecisionHeadReceiveTranferView_not.Text);
                    }


                    _selectDicision.m0_actor_idx = _temp_node17[0].m0_actor_idx;
                    _selectDicision.m0_node_idx = _temp_node17[0].m0_node_idx;
                    _selectDicision.u0_device_idx = _temp_node17[0].u0_device_idx;
                    _selectDicision.emp_idx_create = _emp_idx;
                    _selectDicision.rdept_idx_create = int.Parse(ViewState["rdept_permission"].ToString()); ;
                    _selectDicision.rsec_idx_create = int.Parse(ViewState["rsec_permission"].ToString());
                    _selectDicision.condition = 1;

                    _data_qa_cims.qa_cims_u0_document_device_list[0] = _selectDicision;
                    _data_qa_cims.qa_cims_u1_document_device_list = u1_doc_tranfer_headapprove_node17;

                    _data_qa_cims = callServicePostQaDocCIMS(_urlCimsSetDocumentUpdateHeadQA, _data_qa_cims);
                  //  litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_qa_cims));

                    ////divdocumentList.Visible = true;
                    ////divFVVQATranfer.Visible = false;

                    ////CheckAllQATranfer.Checked = false;
                    ////btnBackToDocList.Visible = false;
                    ////divDocDetailUser.Visible = false;

                    setActiveTab("docList", 0, 0, 0, 0, 0, 0);
                    ////////BackToCurrentPageGvDocumentList(int.Parse(tbPlaceIDXInDocument.Text));
                    setOntop.Focus();
                }
                else //if (int.Parse(ViewState["count_row_check"].ToString()) == 0)
                {

                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาพิจารณารายการโอนย้าย!!!');", true);

                }

                break;

            case "cmdDelete":

                string[] arg = new string[2];
                arg = e.CommandArgument.ToString().Split(',');
                string cmdArgDelete = (arg[0]);
                int m2_device_idx = int.Parse(arg[1]);

                switch (cmdArgDelete)
                {
                    case "Resolution":

                        data_qa_cims _data_resolution_del = new data_qa_cims();
                        _data_resolution_del.qa_cims_m2_registration_device_list = new qa_cims_m2_registration_device[1];
                        qa_cims_m2_registration_device del_resolution = new qa_cims_m2_registration_device();

                        del_resolution.m0_device_idx = int.Parse(ViewState["vsM0DeviceIDX"].ToString());
                        del_resolution.m2_device_resolution_idx = (m2_device_idx);
                        del_resolution.cemp_idx = _emp_idx;
                        del_resolution.condition = 1;
                        del_resolution.status = 9;

                        _data_resolution_del.qa_cims_m2_registration_device_list[0] = del_resolution;
                        _data_resolution_del = callServicePostMasterQACIMS(_urlCimsSetM0_Resolution, _data_resolution_del);

                        selectResolution(ViewState["vsM0DeviceIDX"].ToString());

                        break;
                }

                break;

            case "cmdReportDevices":

                //string[] arg = new string[2];
                //arg = e.CommandArgument.ToString().Split(',');
                //string cmdArgDelete = (arg[0]);
                //int m2_device_idx = int.Parse(arg[1]);
                string devices_no_report = cmdArg;

                setActiveTab("docReport", 0, 0, 0, 0, 0, int.Parse(cmdArg));

                break;

            case "cmdBackToReport":

                string devices_no_report_online = cmdArg;

                setActiveTab("docReport", 0, 0, 0, 0, 0, int.Parse(cmdArg));

                break;

            case "cmdSearchCancel":

                //DropDownList ddl_orgidx_reportcancel = (DropDownList)div_SearchReportOnline.FindControl("ddl_orgidx_report");
                //DropDownList ddl_rdeptidx_reportcancel = (DropDownList)div_SearchReportOnline.FindControl("ddl_rdeptidx_report");
                //DropDownList ddl_rsecidx_reportcancel = (DropDownList)div_SearchReportOnline.FindControl("ddl_rsecidx_report");

                setActiveTab("docReport", 0, 0, 0, 0, 0, int.Parse(cmdArg));

                //getOrganizationList(ddl_orgidx_report);
                //getDepartmentList(ddl_rdeptidx_report, int.Parse(ddl_orgidx_report.SelectedValue));
                //getSectionList(ddl_rsecidx_report, int.Parse(ddl_orgidx_report.SelectedValue), int.Parse(ddl_rdeptidx_report.SelectedValue));



                break;

            case "btnDelRangeUpdate":

                string m4_device_range_idx_edit = cmdArg;

                data_qa_cims _data_range_del = new data_qa_cims();
                _data_range_del.qa_cims_m4_registration_device_list = new qa_cims_m4_registration_device[1];
                qa_cims_m4_registration_device del_range = new qa_cims_m4_registration_device();

                del_range.m0_device_idx = int.Parse(ViewState["vsM0DeviceIDX"].ToString());
                del_range.m4_device_range_idx = int.Parse(m4_device_range_idx_edit);
                del_range.cemp_idx = _emp_idx;
                //del_range.condition = 1;
                del_range.status = 9;

                _data_range_del.qa_cims_m4_registration_device_list[0] = del_range;
                _data_range_del = callServicePostMasterQACIMS(_urlCimsSetRangeUseDevice, _data_range_del);

                selectgvRangeUpdate(ViewState["vsM0DeviceIDX"].ToString());

                break;
            case "btnDelmeasuringUpdate":

                string m5_device_measuring_idx_edit = cmdArg;

                data_qa_cims _data_measuring_del = new data_qa_cims();
                _data_measuring_del.qa_cims_m5_registration_device_list = new qa_cims_m5_registration_device[1];
                qa_cims_m5_registration_device m0_del_measuring = new qa_cims_m5_registration_device();

                m0_del_measuring.m0_device_idx = int.Parse(ViewState["vsM0DeviceIDX"].ToString());
                m0_del_measuring.m5_device_measduring_ix = int.Parse(m5_device_measuring_idx_edit);
                m0_del_measuring.cemp_idx = _emp_idx;
                //del_range.condition = 1;
                m0_del_measuring.status = 9;

                _data_measuring_del.qa_cims_m5_registration_device_list[0] = m0_del_measuring;
                _data_measuring_del = callServicePostMasterQACIMS(_urlCimsSetMeasuringDevice, _data_measuring_del);

                selectgvMeasuringUpdate(ViewState["vsM0DeviceIDX"].ToString());

                break;
            case "btnDelacceptanceUpdate":

                string m6_device_acceptance_idx_edit = cmdArg;

                data_qa_cims _data_acceptance_del = new data_qa_cims();
                _data_acceptance_del.qa_cims_m6_registration_device_list = new qa_cims_m6_registration_device[1];
                qa_cims_m6_registration_device m0_del_acceptance = new qa_cims_m6_registration_device();

                m0_del_acceptance.m0_device_idx = int.Parse(ViewState["vsM0DeviceIDX"].ToString());
                m0_del_acceptance.m6_device_acceptance_idx = int.Parse(m6_device_acceptance_idx_edit);
                m0_del_acceptance.cemp_idx = _emp_idx;
                //del_range.condition = 1;
                m0_del_acceptance.status = 9;

                _data_acceptance_del.qa_cims_m6_registration_device_list[0] = m0_del_acceptance;
                _data_acceptance_del = callServicePostMasterQACIMS(_urlCimsSetAcceptanceDevice, _data_acceptance_del);

                selectgvAcceptanceUpdate(ViewState["vsM0DeviceIDX"].ToString());

                break;

            case "cmdAddPermissionQA":

                setActiveTab("docManagement", 0, 0, 0, 0, 0, 0);
                GvPerManagement.Visible = false;
                PanelInertPerManageQA.Visible = false;
                //PanelShowInertPerManageQA.Visible = true;
                div_btncancel.Visible = true;

                fvInsertPerQA.Visible = true;
                fvInsertPerQA.ChangeMode(FormViewMode.Insert);




                break;

            case "cmdToUpdatePer":

                setActiveTab("docManagement", 0, 0, 0, 0, 0, 0);
                GvPerManagement.Visible = false;
                PanelInertPerManageQA.Visible = false;
                div_btncancel.Visible = true;


                fvInsertPerQA.Visible = true;
                fvInsertPerQA.ChangeMode(FormViewMode.Edit);

                actionReadPermission(int.Parse(cmdArg));
                setOntop.Focus();

                break;

            case "cmdAddPlacePerList":
                setPlacePermissionList();

                break;
            case "cmdAddPermissionManage":
                setPermanageList();
                break;

            case "cmdAddPlacePerUpdate":
                setPlaceListUpdate();
                break;
            case "cmdAddPerPerUpdate":
                setPerListUpdate();
                break;
            case "btnCancel":


                setActiveTab("docManagement", 0, 0, 0, 0, 0, 0);
                setOntop.Focus();
                break;

        }



    }

    #endregion   

    #region event textbox
    protected void TextBoxChanged(object sender, EventArgs e)
    {
        var txtName = (TextBox)sender;

        switch (txtName.ID)
        {
            case "tbsearchingToolID":
                TextBox tbsearchingToolID_txtChanged = (TextBox)pnEquipmentOld.FindControl("tbsearchingToolID");

                if (tbsearchingToolID_txtChanged.Text != "")
                {
                    getToolIDList((TextBox)pnEquipmentOld.FindControl("tbsearchingToolID"), "0");
                    tbsearchingToolID_txtChanged.Text = string.Empty;
                }
                else
                {

                }
                break;

            case "txtequipmentNo":
                TextBox txtequipmentNo = (TextBox)fvInsertCIMS.FindControl("txtequipmentNo");
                Label lbltxtShowIDnumber = (Label)fvInsertCIMS.FindControl("lbltxtShowIDnumber");

                if (txtequipmentNo.Text != "")
                {
                    getCheckIDNoEquipment((TextBox)fvInsertCIMS.FindControl("txtequipmentNo"), "0");
                }
                else
                {
                    lbltxtShowIDnumber.Text = String.Empty;
                }

                break;

            case "txtserialno":
                TextBox txtserialno = (TextBox)fvInsertCIMS.FindControl("txtserialno");
                Label lbltxtShowSerialNumber = (Label)fvInsertCIMS.FindControl("lbltxtShowSerialNumber");

                if (txtserialno.Text != "")
                {
                    getCheckSerialNumberEquipment((TextBox)fvInsertCIMS.FindControl("txtserialno"), "0");

                }
                else
                {
                    lbltxtShowSerialNumber.Text = String.Empty;
                }

                break;

            case "txtequipmentNoEdit":

                //litDebug.Text = "Here IDNO";

                TextBox txtequipmentNoEdit = (TextBox)fvInsertCIMS.FindControl("txtequipmentNoEdit");
                Label lbltxtEditShowIDnumber = (Label)fvInsertCIMS.FindControl("lbltxtEditShowIDnumber");

                if (txtequipmentNoEdit.Text != "")
                {
                    getCheckIDNoEquipmentEdit((TextBox)fvInsertCIMS.FindControl("txtequipmentNoEdit"), "0");
                }
                else
                {
                    lbltxtEditShowIDnumber.Text = String.Empty;
                }

                break;

            case "txtserialnoEdit":
                TextBox txtserialnoEdit = (TextBox)fvInsertCIMS.FindControl("txtserialnoEdit");
                Label lbltxtEditShowSerialnumber = (Label)fvInsertCIMS.FindControl("lbltxtEditShowSerialnumber");

                //litDebug.Text = "Here Serial";

                if (txtserialnoEdit.Text != "")
                {
                    getCheckSerialNumberEquipmentEdit((TextBox)fvInsertCIMS.FindControl("txtserialnoEdit"), "0");

                }
                else
                {
                    lbltxtEditShowSerialnumber.Text = String.Empty;
                }

                break;

        }
    }
    #endregion

    #region callService 
    protected data_qa_cims callServicePostMasterQACIMS(string _cmdUrl, data_qa_cims _data_qa_cims)
    {
        _localJson = _funcTool.convertObjectToJson(_data_qa_cims);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_qa_cims = (data_qa_cims)_funcTool.convertJsonToObject(typeof(data_qa_cims), _localJson);


        return _data_qa_cims;
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_qa_cims callServicePostQaDocCIMS(string _cmdUrl, data_qa_cims _data_qa_cims)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_qa_cims);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_qa_cims = (data_qa_cims)_funcTool.convertJsonToObject(typeof(data_qa_cims), _localJson);

        return _data_qa_cims;
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger = new PostBackTrigger();
        trigger.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger);
    }

    #endregion callService Functions

    protected void getPlacePerList()
    {
        //ViewState["vsFoodMaterialList"] = null;
        DataSet dsFoodMaterialList = new DataSet();
        dsFoodMaterialList.Tables.Add("dsPlacePermissionTable");
        dsFoodMaterialList.Tables["dsPlacePermissionTable"].Columns.Add("drPlacePerIdx", typeof(String));
        dsFoodMaterialList.Tables["dsPlacePermissionTable"].Columns.Add("drPlacePerText", typeof(String));
        //dsFoodMaterialList.Tables["dsFoodMaterialTable"].Columns.Add("drFoodMaterialAmount", typeof(String));
        //dsFoodMaterialList.Tables["dsFoodMaterialTable"].Columns.Add("drFoodMaterialPerUnit", typeof(String));
        ViewState["vsPlacePermissionList"] = dsFoodMaterialList;
    }

    protected void setPlacePermissionList()
    {
        if (ViewState["vsPlacePermissionList"] != null)
        {
            DropDownList ddl_Placeidx = (DropDownList)fvInsertPerQA.FindControl("ddl_Placeidx");
            GridView gvPlaceList = (GridView)fvInsertPerQA.FindControl("gvPlaceList");
            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts = (DataSet)ViewState["vsPlacePermissionList"];

            foreach (DataRow dr in dsContacts.Tables["dsPlacePermissionTable"].Rows)
            {
                if (dr["drPlacePerIdx"].ToString() == ddl_Placeidx.SelectedValue)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีสถานที่นี้แล้ว!!!');", true);
                    return;
                }
            }
            DataRow drContacts = dsContacts.Tables["dsPlacePermissionTable"].NewRow();
            drContacts["drPlacePerIdx"] = ddl_Placeidx.SelectedValue;
            drContacts["drPlacePerText"] = ddl_Placeidx.SelectedItem.Text;

            dsContacts.Tables["dsPlacePermissionTable"].Rows.Add(drContacts);
            ViewState["vsPlacePermissionList"] = dsContacts;
            setGridData(gvPlaceList, dsContacts.Tables["dsPlacePermissionTable"]);
            gvPlaceList.Visible = true;
        }
    }

    protected void getPlaceListUpdate()
    {
        //ViewState["vsFoodMaterialList"] = null;
        DataSet dsPlaceListUpdate = new DataSet();
        dsPlaceListUpdate.Tables.Add("dsPlaceTableUpdate");
        dsPlaceListUpdate.Tables["dsPlaceTableUpdate"].Columns.Add("drPlaceIdxUpdate", typeof(String));
        dsPlaceListUpdate.Tables["dsPlaceTableUpdate"].Columns.Add("drPlaceTextUpdate", typeof(String));

        ViewState["vsPlaceListUpdate"] = dsPlaceListUpdate;
    }

    protected void setPlaceListUpdate()
    {
        if (ViewState["vsPlaceListUpdate"] != null)
        {
            DropDownList ddl_PlaceidxUpdate = (DropDownList)fvInsertPerQA.FindControl("ddl_PlaceidxUpdate");
            GridView gvPlacePerListUpdate = (GridView)fvInsertPerQA.FindControl("gvPlacePerListUpdate");

            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;
            DataSet dsContacts = (DataSet)ViewState["vsPlaceListUpdate"];
            foreach (DataRow dr in dsContacts.Tables["dsPlaceTableUpdate"].Rows)
            {
                if (dr["drPlaceIdxUpdate"].ToString() == ddl_PlaceidxUpdate.SelectedValue)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('สถานที่มีในรายการแล้ว');", true);
                    return;
                }
            }
            DataRow drContacts = dsContacts.Tables["dsPlaceTableUpdate"].NewRow();
            drContacts["drPlaceIdxUpdate"] = ddl_PlaceidxUpdate.SelectedValue;
            drContacts["drPlaceTextUpdate"] = ddl_PlaceidxUpdate.SelectedItem.Text;

            dsContacts.Tables["dsPlaceTableUpdate"].Rows.Add(drContacts);
            ViewState["vsPlaceListUpdate"] = dsContacts;
            setGridData(gvPlacePerListUpdate, dsContacts.Tables["dsPlaceTableUpdate"]);
            gvPlacePerListUpdate.Visible = true;
        }
    }

    protected void getPerManageList()
    {
        //ViewState["vsFoodMaterialList"] = null;
        DataSet dsPerManageList = new DataSet();
        dsPerManageList.Tables.Add("dsPerManageTable");
        dsPerManageList.Tables["dsPerManageTable"].Columns.Add("drPerIdx", typeof(String));
        dsPerManageList.Tables["dsPerManageTable"].Columns.Add("drPerText", typeof(String));
        //dsFoodMaterialList.Tables["dsFoodMaterialTable"].Columns.Add("drFoodMaterialAmount", typeof(String));
        //dsFoodMaterialList.Tables["dsFoodMaterialTable"].Columns.Add("drFoodMaterialPerUnit", typeof(String));
        ViewState["vsPerManageList"] = dsPerManageList;
    }

    protected void setPermanageList()
    {
        if (ViewState["vsPerManageList"] != null)
        {
            DropDownList ddl_Permanagement = (DropDownList)fvInsertPerQA.FindControl("ddl_Permanagement");
            GridView gvPerManageList = (GridView)fvInsertPerQA.FindControl("gvPerManageList");
            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts = (DataSet)ViewState["vsPerManageList"];

            foreach (DataRow dr in dsContacts.Tables["dsPerManageTable"].Rows)
            {
                if (dr["drPerIdx"].ToString() == ddl_Permanagement.SelectedValue)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีสิทธิ์นี้แล้ว!!!');", true);
                    return;
                }
            }
            DataRow drContacts = dsContacts.Tables["dsPerManageTable"].NewRow();
            drContacts["drPerIdx"] = ddl_Permanagement.SelectedValue;
            drContacts["drPerText"] = ddl_Permanagement.SelectedItem.Text;

            dsContacts.Tables["dsPerManageTable"].Rows.Add(drContacts);
            ViewState["vsPerManageList"] = dsContacts;
            setGridData(gvPerManageList, dsContacts.Tables["dsPerManageTable"]);
            gvPerManageList.Visible = true;
        }
    }

    protected void getPerListUpdate()
    {
        //ViewState["vsFoodMaterialList"] = null;
        DataSet dsPerListUpdate = new DataSet();
        dsPerListUpdate.Tables.Add("dsPerTableUpdate");
        dsPerListUpdate.Tables["dsPerTableUpdate"].Columns.Add("drPerIdxUpdate", typeof(String));
        dsPerListUpdate.Tables["dsPerTableUpdate"].Columns.Add("drPerTextUpdate", typeof(String));

        ViewState["vsPerListUpdate"] = dsPerListUpdate;
    }

    protected void setPerListUpdate()
    {
        if (ViewState["vsPerListUpdate"] != null)
        {
            DropDownList ddl_PerManageUpdate = (DropDownList)fvInsertPerQA.FindControl("ddl_PerManageUpdate");
            GridView gvPerPerListUpdate = (GridView)fvInsertPerQA.FindControl("gvPerPerListUpdate");

            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;
            DataSet dsContacts = (DataSet)ViewState["vsPerListUpdate"];
            foreach (DataRow dr in dsContacts.Tables["dsPerTableUpdate"].Rows)
            {
                if (dr["drPerIdxUpdate"].ToString() == ddl_PerManageUpdate.SelectedValue)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('สิทธิ์นี้มีในรายการแล้ว');", true);
                    return;
                }
            }
            DataRow drContacts = dsContacts.Tables["dsPerTableUpdate"].NewRow();
            drContacts["drPerIdxUpdate"] = ddl_PerManageUpdate.SelectedValue;
            drContacts["drPerTextUpdate"] = ddl_PerManageUpdate.SelectedItem.Text;

            dsContacts.Tables["dsPerTableUpdate"].Rows.Add(drContacts);
            ViewState["vsPerListUpdate"] = dsContacts;

            setGridData(gvPerPerListUpdate, dsContacts.Tables["dsPerTableUpdate"]);
            gvPerPerListUpdate.Visible = true;
        }
    }

    protected void CleardataSetPlaceList()
    {
        var gvPlaceList = (GridView)fvInsertPerQA.FindControl("gvPlaceList");
        ViewState["vsPlacePermissionList"] = null;
        gvPlaceList.DataSource = ViewState["vsPlacePermissionList"];
        gvPlaceList.DataBind();
        getPlacePerList();
    }

    protected void CleardataSetPerManagementList()
    {
        var gvPerManageList = (GridView)fvInsertPerQA.FindControl("gvPerManageList");
        ViewState["vsPerManageList"] = null;
        gvPerManageList.DataSource = ViewState["vsPerManageList"];
        gvPerManageList.DataBind();
        getPerManageList();
    }

    protected void CleardataSetPlaceListUpdate()
    {
        var gvPlacePerListUpdate = (GridView)fvInsertPerQA.FindControl("gvPlacePerListUpdate");
        ViewState["vsPlaceListUpdate"] = null;
        gvPlacePerListUpdate.DataSource = ViewState["vsPlaceListUpdate"];
        gvPlacePerListUpdate.DataBind();
        getPlaceListUpdate();
    }

    protected void CleardataSetPerListUpdate()
    {
        var gvPerPerListUpdate = (GridView)fvInsertPerQA.FindControl("gvPerPerListUpdate");
        ViewState["vsPerListUpdate"] = null;
        gvPerPerListUpdate.DataSource = ViewState["vsPerListUpdate"];
        gvPerPerListUpdate.DataBind();

        getPerListUpdate();
    }

    protected string getStatus(int status)
    {
        if (status == 1)
        {
            return "<span style='color:#26A65B'>ใช้งาน</span>";
        }
        else if (status == 0)
        {
            return "<span style='color:#F22613'>ยกเลิกใช้งาน</span>";
        }
        else
        {
            return string.Empty;
        }
    }

    protected void actionPermissionManage()
    {
        data_qa_cims _data_qa_selectper = new data_qa_cims();
        _data_qa_selectper.qa_cims_m0_management_list = new qa_cims_m0_management_detail[1];
        qa_cims_m0_management_detail _m0_select_per = new qa_cims_m0_management_detail();
        //_m0place.condition = 2;
        _data_qa_selectper.qa_cims_m0_management_list[0] = _m0_select_per;

        _data_qa_selectper = callServicePostMasterQACIMS(_urlCimsGetPermissionManageQA, _data_qa_selectper);

        ViewState["vs_PermissionManage_Select"] = _data_qa_selectper.qa_cims_m0_management_list;
        setGridData(GvPerManagement, ViewState["vs_PermissionManage_Select"]);

    }

    protected void actionDelPermission(int id)
    {

        data_qa_cims _data_qa_delper = new data_qa_cims();
        _data_qa_delper.qa_cims_m0_management_list = new qa_cims_m0_management_detail[1];
        qa_cims_m0_management_detail _m0_del_per = new qa_cims_m0_management_detail();
        _m0_del_per.m0_management_idx = id;
        _m0_del_per.cemp_idx = _emp_idx;

        _data_qa_delper.qa_cims_m0_management_list[0] = _m0_del_per;

        _data_qa_delper = callServicePostMasterQACIMS(_urlCimsSetDelPermissionManagePerQA, _data_qa_delper);

        ViewState["vs_PermissionManage_Select"] = _data_qa_delper.qa_cims_m0_management_list;
        setGridData(GvPerManagement, ViewState["vs_PermissionManage_Select"]);

        setActiveTab("docManagement", 0, 0, 0, 0, 0, 0);

    }

    protected void actionReadPermission(int id)
    {

        data_qa_cims _data_qa_delper_edit = new data_qa_cims();
        _data_qa_delper_edit.qa_cims_m0_management_list = new qa_cims_m0_management_detail[1];
        qa_cims_m0_management_detail _m0_del_per_edit = new qa_cims_m0_management_detail();
        _m0_del_per_edit.m0_management_idx = id;

        _data_qa_delper_edit.qa_cims_m0_management_list[0] = _m0_del_per_edit;

        _data_qa_delper_edit = callServicePostMasterQACIMS(_urlCimsGetPermissionManagePerQAView, _data_qa_delper_edit);

        setFormData(fvInsertPerQA, FormViewMode.Edit, _data_qa_delper_edit.qa_cims_m0_management_list);




    }



    protected void WriteExcelWithNPOI(DataTable dt, String extension, String fileName)
    {
        IWorkbook workbook;
        if (extension == "xlsx")
        {
            workbook = new XSSFWorkbook();
        }
        else if (extension == "xls")
        {
            workbook = new HSSFWorkbook();
        }
        else
        {
            throw new Exception("This format is not supported");
        }
        ISheet sheet1 = workbook.CreateSheet("Sheet 1");
        IRow row1 = sheet1.CreateRow(0);
        for (int j = 0; j < dt.Columns.Count; j++)
        {
            ICell cell = row1.CreateCell(j);
            String columnName = dt.Columns[j].ToString();
            cell.SetCellValue(columnName);
        }
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            IRow row = sheet1.CreateRow(i + 1);
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                ICell cell = row.CreateCell(j);
                String columnName = dt.Columns[j].ToString();
                cell.SetCellValue(dt.Rows[i][columnName].ToString());
            }
        }
        using (var exportData = new MemoryStream())
        {
            Response.Clear();
            workbook.Write(exportData);
            if (extension == "xlsx")
            {
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xlsx"));
                Response.BinaryWrite(exportData.ToArray());
            }
            else if (extension == "xls")
            {
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xls"));
                Response.BinaryWrite(exportData.GetBuffer());
            }
            Response.End();
        }
    }

}