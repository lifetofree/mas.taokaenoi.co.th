<%@ Page Language="C#" AutoEventWireup="true" CodeFile="tknHny2018.aspx.cs" Inherits="webevent_tknHny2018" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>TKN : New Year Party 2018</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="./../Content/bootstrap.css" runat="server" rel="stylesheet" />
    <link href="./../Content/custom.css" runat="server" rel="stylesheet" />

    <style type="text/css">
        .btn-xl {
            padding: 35px 45px;
            font-size: 84px;
            border-radius: 20px;
            margin: 20px;
        }

        .font-xl {
            font-size: 64px;
            font-weight: bold;
        }

        .font-gv {
            font-size: 24px;
        }

        .font-gv-l {
            font-size: 20px;
        }
    </style>
</head>
<body>
    <form id="formMaster" runat="server">
		<asp:ScriptManager ID="tsmMaster" runat="server"></asp:ScriptManager>
		<script src='<%=ResolveUrl("~/Scripts/jquery-3.1.1.min.js") %>'></script>
		<script src='<%=ResolveUrl("~/Scripts/bootstrap.js") %>'></script>
        <script src='<%=ResolveUrl("~/Scripts/custom.js") %>'></script>

        <div class="container">
            <div class="col-lg-12">
                <div class="col-lg-2">
                    <img src='<%=ResolveUrl("~/masterpage/images/logo_tkn.png") %>' class="img-responsive" alt="Taokaenoi">
                </div>
                <div class="col-lg-10 font-xl">
                    TKN : New Year Party 2018 
                </div>
            </div>
            <asp:MultiView ID="mvSystem" runat="server">
                <asp:View ID="viewSelectPlant" runat="server">
                    <div class="text-center">
                        <asp:LinkButton ID="lbNPW" CssClass="btn btn-primary btn-xl" runat="server" data-original-title="นพวงศ์" data-toggle="tooltip" Text="นพวงศ์" CommandName="cmdSelectPlant" OnCommand="btnCommand" CommandArgument="npw" />
                        <asp:LinkButton ID="lbRJN" CssClass="btn btn-success btn-xl" runat="server" data-original-title="โรจนะ" data-toggle="tooltip" Text="โรจนะ" CommandName="cmdSelectPlant" OnCommand="btnCommand" CommandArgument="rjn" />
                    </div>
                </asp:View>
                <asp:View ID="viewPlant" runat="server">
                    <asp:Gridview ID="gvPrize" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-bordered font-gv" HeaderStyle-CssClass="table_headCenter font-gv" OnRowDataBound="gvRowDataBound" HeaderStyle-Height="40px">
                        <HeaderStyle CssClass="info" />
                        <EmptyDataTemplate>
                            ไม่พบข้อมูล
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="รางวัล">
                                <ItemTemplate>
                                    <asp:Literal ID="litPrizeName" runat="server" Text='<%# Eval("prize_name") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="จำนวน">
                                <ItemTemplate>
                                    <asp:Literal ID="litPrizeReward" runat="server" Text='<%# Eval("prize_reward") %>' />&nbsp;รางวัล
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="คงเหลือ">
                                <ItemTemplate>
                                    <asp:Literal ID="litPrizeRemain" runat="server" Text='<%# Eval("prize_remain") %>' />&nbsp;รางวัล
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbSelectPrize" CssClass="btn btn-primary btn-lg" runat="server" data-original-title="จับรางวัล" data-toggle="tooltip" Text="จับรางวัล" CommandName="cmdSelectPrize" OnCommand="btnCommand" CommandArgument='<%# Eval("midx") %>' />
                                    <asp:LinkButton ID="lbViewPrize" CssClass="btn btn-success btn-lg" runat="server" data-original-title="รายชื่อผู้โชคดี" data-toggle="tooltip" Text="รายชื่อผู้โชคดี" CommandName="cmdViewPrize" OnCommand="btnCommand" CommandArgument='<%# Eval("midx") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:Gridview>
                </asp:View>
                <asp:View ID="viewRandom" runat="server">
                    <h1>
                        รางวัล : <span class="text-danger"><asp:Literal ID="litPrizeName" runat="server" /></span>&nbsp;
                        จำนวน <span class="text-danger"><asp:Literal ID="litPrizeReward" runat="server" /></span> รางวัล
                        <div class="pull-right text-success">คงเหลือ <asp:Literal ID="litPrizeRemain" runat="server" /> รางวัล</div>
                    </h1>
                    <div ID="randomBox" runat="server">
                        <div class="row">
                            <div class="form-horizontal">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label class="col-lg-6 control-label"> <h3>สุ่มรางวัลจำนวน</h3></label>
                                        <div class="col-lg-6">
                                            <h3><asp:TextBox ID="tbRandPrize" runat="server" CssClass="input-lg" />&nbsp;รางวัล</h3>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-6 control-label"></label>
                                        <div class="col-lg-6">
                                            <asp:LinkButton ID="lbRandomPrize" CssClass="btn btn-info btn-lg" runat="server" data-original-title="สุ่มรางวัล" data-toggle="tooltip" Text="สุ่มรางวัล" CommandName="cmdRandomPrize" OnCommand="btnCommand" />
                                        </div>
                                    </div>
                                </div>

                                <asp:Gridview ID="gvResult" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-bordered font-gv" HeaderStyle-CssClass="table_headCenter font-gv" HeaderStyle-Height="40px">
                                    <HeaderStyle CssClass="info" />
                                    <%--<EmptyDataTemplate>
                                        ไม่พบข้อมูล
                                    </EmptyDataTemplate>--%>
                                    <Columns>
                                        <asp:TemplateField HeaderText="รหัสพนักงาน">
                                            <ItemTemplate>
                                                <asp:Literal ID="litEmpCode" runat="server" Text='<%# Eval("emp_code") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ชื่อ - นามสกุล">
                                            <ItemTemplate>
                                                <asp:Literal ID="litEmpName" runat="server" Text='<%# Eval("emp_name") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ตำแหน่ง">
                                            <ItemTemplate>
                                                <asp:Literal ID="litEmpPos" runat="server" Text='<%# Eval("emp_pos") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="แผนก">
                                            <ItemTemplate>
                                                <asp:Literal ID="litEmpSec" runat="server" Text='<%# Eval("emp_sec") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--<asp:TemplateField HeaderText="รับรางวัล">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbConfirm" CssClass="btn btn-info btn-lg" runat="server" data-original-title="รับรางวัล" data-toggle="tooltip" Text="รับรางวัล" CommandName="cmdConfirm" CommandArgument='<%# (String)Eval("emp_code") + "," + (String)Eval("emp_prize_idx").ToString() %>' OnCommand="btnCommand" CausesValidation="true" />
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                    </Columns>
                                </asp:Gridview>
                            </div>
                        </div>
                    </div>
                    <!-- <hr style="border: solid #eee 1px" /> -->
                    <div class="col-lg-12">
                        <div class="form-group">
                            <asp:LinkButton ID="lbBackToPrizeTop" CssClass="btn btn-default btn-lg" runat="server" data-original-title="Back" data-toggle="tooltip" Text="Back" CommandName="cmdBackToPrize" OnCommand="btnCommand" />
                        </div>

                        <!-- <h2>รายชื่อผู้โชคดี</h2> -->

                        <asp:GridView ID="gvHistory" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-bordered font-gv-l" HeaderStyle-CssClass="table_headCenter font-gv-l" HeaderStyle-Height="40px" OnRowDataBound="gvRowDataBound">
                            <Columns>
                                <asp:TemplateField HeaderText="รหัสพนักงาน">
                                    <ItemTemplate>
                                        <asp:Literal ID="litEmpCode" runat="server" Text='<%# Eval("emp_code") %>' />
                                        <asp:HiddenField ID="hfEmpPrizeStatus" runat="server" Value='<%# Eval("emp_prize_status") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ชื่อ - นามสกุล">
                                    <ItemTemplate>
                                        <asp:Literal ID="litEmpName" runat="server" Text='<%# Eval("emp_name") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ตำแหน่ง">
                                    <ItemTemplate>
                                        <asp:Literal ID="litEmpPos" runat="server" Text='<%# Eval("emp_pos") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="แผนก">
                                    <ItemTemplate>
                                        <asp:Literal ID="litEmpSec" runat="server" Text='<%# Eval("emp_sec") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="รับรางวัล">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbConfirm" CssClass="btn btn-info btn-md" runat="server" data-original-title="รับรางวัล" data-toggle="tooltip" Text="รับรางวัล" CommandName="cmdConfirm" CommandArgument='<%# (String)Eval("emp_code") + "," + (String)Eval("emp_prize_idx").ToString() %>' OnCommand="btnCommand" />
                                        <asp:LinkButton ID="lbCancel" CssClass="btn btn-danger btn-md" runat="server" data-original-title="ยกเลิก" data-toggle="tooltip" Text="ยกเลิก" CommandName="cmdCancel" CommandArgument='<%# (String)Eval("emp_code") + "," + (String)Eval("emp_prize_idx").ToString() %>' OnCommand="btnCommand" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>

                        <%--<asp:LinkButton ID="lbBackToPrizeBottom" CssClass="btn btn-default btn-lg" runat="server" data-original-title="Back" data-toggle="tooltip" Text="Back" CommandName="cmdBackToPrize" OnCommand="btnCommand" />--%>
                    </div>
                </asp:View>
            </asp:MultiView>
        </div>
    </form>
</body>