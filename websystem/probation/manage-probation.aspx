﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="manage-probation.aspx.cs" Inherits="websystem_probation_manage_probation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" Runat="Server">
    <!-- menu -->

    <asp:Literal ID="check_XML" runat="server"></asp:Literal>

    <div id="divMenubar" runat="server" class="col-md-12">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand">Menu</a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav ulActiveMenu" runat="server">
                    <li id="IndexList" runat="server">
                        <asp:LinkButton ID="listMenu0" runat="server"
                            CommandName="btnViewSelected"
                            OnCommand="btn_command" Text="ข้อมูลทั่วไป" />
                    </li>
                    <li id="listTabresultRate" runat="server">
                        <asp:LinkButton ID="listMenu1" runat="server"
                            CommandName="btnViewresultRate"
                            OnCommand="btn_command" Text="สรุปผลการประเมิน" />
                    </li>
                    <%--  <li id="listTab" runat="server">
                        <asp:LinkButton ID="listMenu2" runat="server"
                            CommandName="btnViewtwo"
                            OnCommand="btn_command" Text="สรุปผลการประเมิน" />
                    </li>--%>
                </ul>
            </div>
        </nav>
    </div>
    <!-- menu -->
    <asp:MultiView ID="mvMultiview" runat="server" ActiveViewIndex="0">
        <!-- view Home page -->
        <asp:View ID="viewHomepage" runat="server">
            <div class="col-sm-12" runat="server" id="content_Homepage">
                <label>รายชื่อประเมินทดลองงานของพนักงาน</label>
                <asp:GridView ID="gvListNameProbation" runat="server" AutoGenerateColumns="false" DataKeyNames="emp_probation_idx"
                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                    HeaderStyle-CssClass="info" HeaderStyle-Height="30px" AllowPaging="true" PageSize="5"
                    OnPageIndexChanging="gvPageIndexChanging" OnRowDataBound="gvRowDataBound">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                        FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่พบข้อมูลแบบฟอร์มการประเมิน</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <div style="text-align: center; padding-top: 70px;">
                                    <asp:Label ID="emp_probation_idx" runat="server" Visible="false" Text='<%# Eval("emp_probation_idx") %>' />
                                    <%# (Container.DataItemIndex +1) %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="รายละเอียดพนักงาน" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="left" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <asp:Label ID="lb_empCode_employee" runat="server" CssClass="col-sm-12">
                                  <small><p><b>รหัสพนักงาน:</b> &nbsp;<%# Eval("emps_code_probation") %></p></small>
                                </asp:Label>
                                <asp:Label ID="lb_fullName_employee" runat="server" CssClass="col-sm-12">
                                  <small><p><b>ชื่อ - นามสกุล:</b> &nbsp;<%# Eval("emps_fullname_probation") %></p></small>
                                </asp:Label>
                                <asp:Label ID="lb_orgName_probation" runat="server" CssClass="col-sm-12">
                                  <small><p><b>องค์กร:</b> &nbsp;<%# Eval("name_org_probation") %></p></small>
                                </asp:Label>
                                <asp:Label ID="lb_depart_probation" runat="server" CssClass="col-sm-12">
                                  <small><p><b>ฝ่าย:</b> &nbsp;<%# Eval("name_dept_probation") %></p></small>
                                </asp:Label>
                                <asp:Label ID="lb_section_probation" runat="server" CssClass="col-sm-12">
                                  <small><p><b>แผนก:</b> &nbsp;<%# Eval("name_sec_probation") %></p></small>
                                </asp:Label>
                                <asp:Label ID="lb_position_probation" runat="server" CssClass="col-sm-12">
                                  <small><p><b>ตำแหน่ง:</b> &nbsp;<%# Eval("name_pos_probation") %></p></small>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="วันเข้างาน" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="left" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <div style="text-align: center; padding-top: 60px;">
                                    <asp:Label ID="lb_date_start_probation" runat="server" CssClass="col-sm-12">
                                  <small><p><b>วันเข้างาน:</b> &nbsp;<%# Eval("date_start_probation") %></p></small>
                                    </asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="สถานะการดำเนินการ" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="left" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <div style="text-align: center; padding-top: 50px;">
                                    <asp:Label ID="lb_status_node" ForeColor="Green" runat="server" CssClass="col-sm-12">
                                    <small><span><%# Eval("node_probation_status") %>&nbsp;<%# Eval("node_probation_name") %>&nbsp;<b>โดย</b>&nbsp;<%# Eval("actor_probation_name") %></span></small>
                                    </asp:Label>
                                    <asp:Label ID="lb_status_node_defult" runat="server" CssClass="col-sm-12" Visible="false">
                                    <small><span>รอดำเนินการ&nbsp;สร้าง/แก้ไข&nbsp;<b>โดย</b>&nbsp;แผนกสรรหาว่าจ้าง</span></small>
                                    </asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="left" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <div style="text-align: center; padding-top: 75px;">
                                    <asp:LinkButton ID="btnManage_probation" CssClass="text-edit" runat="server" CommandName="Manage_probation"
                                        data-toggle="tooltip" OnCommand="btn_command" CommandArgument='<%#Eval("emp_probation_idx") %>' title="จัดการการประเมิน">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </asp:View>
        <asp:View ID="viewPage_Manage_Evaluation" runat="server">
            <asp:HyperLink ID="setFocus" runat="server"></asp:HyperLink>
            <div class="col-sm-12" runat="server" id="content_information_employee">
                <div class="form-group">
                    <asp:LinkButton ID="btnBackHomePage" CssClass="btn btn-info btn-sm"
                        runat="server" data-toggle="tooltip" data-placement="top"
                        title="กลับหน้าแรก"
                        CommandName="btnViewSelected" OnCommand="btn_command"><i class="glyphicon glyphicon-home"></i> &nbsp; กลับหน้าแรก</asp:LinkButton>
                    <br />
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <i class="glyphicon glyphicon-user"></i><strong>&nbsp; Information Employee</strong>&nbsp;[ข้อมูลพนักงาน]</h4>
                        </div>
                        <div class="panel-body">
                            <div class="post-content">
                                <asp:Repeater ID="Information_employee" runat="server">
                                    <ItemTemplate>
                                        <div style="margin-top: 15px;">
                                            <div class="row">
                                                <div class="col-sm-1"></div>
                                                <div class="col-sm-5">
                                                    <small class="list-group-item-heading text_left">
                                                        <b><font color="#1A5276">รหัสพนักงาน :</font>&nbsp;</b>
                                                        <span><%# Eval("emps_code_probation") %></span></small>
                                                </div>
                                                <div class="col-sm-6">
                                                    <small class="list-group-item-heading text_left">
                                                        <b><font color="#1A5276">ชื่อ - นามสกุล :</font>&nbsp;</b>
                                                        <span><%# Eval("emps_fullname_probation") %></span></small>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-1"></div>
                                                <div class="col-sm-5">
                                                    <small class="list-group-item-heading text_left">
                                                        <b><font color="#1A5276">องค์กร :</font>&nbsp;</b>
                                                        <span><%# Eval("name_org_probation") %></span></small>
                                                </div>
                                                <div class="col-sm-6">
                                                    <small class="list-group-item-heading text_left">
                                                        <b><font color="#1A5276">ฝ่าย :</font>&nbsp;</b>
                                                        <span><%# Eval("name_dept_probation") %></span></small>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-1"></div>
                                                <div class="col-sm-5">
                                                    <small class="list-group-item-heading text_left">
                                                        <b><font color="#1A5276">แผนก :</font>&nbsp;</b>
                                                        <span><%# Eval("name_sec_probation") %></span></small>
                                                </div>
                                                <div class="col-sm-6">
                                                    <small class="list-group-item-heading text_left">
                                                        <b><font color="#1A5276">ตำแหน่ง :</font>&nbsp;</b>
                                                        <span><%# Eval("name_pos_probation") %></span></small>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12" runat="server" id="content_set_assessor">
                <div class="form-group">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <i class="glyphicon glyphicon-edit"></i><strong>&nbsp; Set Assessor</strong>&nbsp;[กำหนดผู้ทำการประเมิน]</h4>
                        </div>
                        <div class="panel-body">
                            <div class="post-content">
                                <div class="form-group">
                                    <div style="margin-top: -10px;">
                                        <div class="col-sm-2">
                                            <font color="#1A5276"><small class="list-group-item-heading text_right">ผู้ประเมินคนที่ 1
                                              <br />
                                            <p>Set Assessor1 </p>
                                         </small></font>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlSetAssessor1"
                                                runat="server" CssClass="form-control fa-align-left"
                                                AutoPostBack="true" Enabled="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div style="margin-top: -10px;">
                                        <div class="col-sm-2">
                                            <font color="#1A5276"><small class="list-group-item-heading text_right">ผู้ประเมินคนที่ 2
                                              <br />
                                            <p>Set Assessor2</p>
                                         </small></font>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlSetAssessor2"
                                                runat="server" CssClass="form-control fa-align-left"
                                                AutoPostBack="true" Enabled="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <br />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <asp:CheckBox ID="check_haveNot_assessor" runat="server" OnCheckedChanged="checkboxIndexChange" /><font color="red"><b>** กรณีไม่มีผู้ประเมิน สามารถทำการส่งแบบประเมินไปที่ HR Director ได้ </b></font>
                <asp:LinkButton ID="btnsaveAssessor" CssClass="btn btn-primary btn-sm pull-right"
                    runat="server" data-toggle="tooltip" data-placement="top"
                    title="ส่งแบบประเมิน" OnClientClick="if(!confirm('คุณต้องการบันทึกและส่งแบบฟอร์มการประเมินใช่หรือไม่?')) return false;"
                    CommandName="saveAssessor" OnCommand="btn_command"><i class="glyphicon glyphicon-send"></i> &nbsp;ส่งแบบประเมิน</asp:LinkButton>
            </div>
        </asp:View>
    </asp:MultiView>

</asp:Content>

