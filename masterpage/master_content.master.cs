﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class masterpage_master_content : System.Web.UI.MasterPage
{

    #region Connect

    function_tool _funcTool = new function_tool();
    data_employee _data_employee = new data_employee();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];

    int emp_idx = 0;
    string _localJson = "";
    int _tempInt = 0;

    #endregion



    private void Page_Init(object sender, System.EventArgs e)
    {
        if (Request.Form["emp_idx"] != null)
        {
            bool _result = Int32.TryParse(Request.Form["emp_idx"], out _tempInt);
            if (_result && _tempInt > 0)
            {
                Session["emp_idx"] = Request.Form["emp_idx"];

                ViewState["rdept_idx"] = Request.Form["rdept_idx"];
                ViewState["rsec_idx"] = Request.Form["rsec_idx"];


            }
        }
        else if (Session["emp_idx"] == null || Session["reset_type"] != null)
        {
            string path = HttpContext.Current.Request.Url.AbsolutePath;
            Response.Redirect(ResolveUrl("~/warning") + "?url=" + path);
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        ViewState["emp_idx_check"] = int.Parse(Session["emp_idx"].ToString());

        actionIndex();

        //Response.Buffer = true;
        //Response.ExpiresAbsolute = DateTime.Now.AddSeconds(-1);
        //Response.Expires = 0;
        //Response.CacheControl = "no-cache";
    }


    #region selected    
    protected void actionIndex()
    {
        data_employee _data_employee = new data_employee();

        _data_employee.employee_list = new employee_detail[1];
        employee_detail _employee_detail = new employee_detail();

        _employee_detail.emp_idx = int.Parse(ViewState["emp_idx_check"].ToString());

        _data_employee.employee_list[0] = _employee_detail;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_system1));
        //_data_employee = callServiceEmpProfile(_urlGetEmpMenu, _data_employee);

        _data_employee = callServiceEmpProfile(_urlGetMyProfile + ViewState["emp_idx_check"].ToString());


        //ViewState["rdept_idx"] = _data_employee.empmenusystem_list[0].rdept_idx;
        //ViewState["rsec_idx"] = _data_employee.empmenusystem_list[0].rsec_idx;

        ViewState["rdept_idx"] = _data_employee.employee_list[0].rdept_idx;
        ViewState["rsec_idx"] = _data_employee.employee_list[0].rsec_idx;


    }

    #endregion selected

    #region CallService
    protected data_employee callServiceEmpProfile(string _cmdUrl)

    {
        //// convert to json
        // _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _data_employee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _data_employee;
    }

    #endregion

    #region BTN
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {

            case "cmdmanual":
                Response.Write("<script>window.open('https://docs.google.com/document/d/1xWzlwzjlvefER2Kg84ZfYuuAB3oWdcvVKJls3FygpbY/edit?usp=sharing','_blank');</script>");

                break;
        }
    }

    #endregion
}
