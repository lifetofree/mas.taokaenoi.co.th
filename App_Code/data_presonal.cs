using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Xml;
using System.Xml.Serialization;
/// <summary>
/// Summary description for data_presonal
/// </summary>

[Serializable]
[XmlRoot("data_presonal")]
public class data_presonal
{

    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    #region Presolnal
    [XmlElement("PrefixNameList")]
    public DetailPrefixName[] PrefixNameList { get; set; }
    [XmlElement("GenderList")]
    public DetailGender[] GenderList { get; set; }
    [XmlElement("BloodGroupList")]
    public DetailBloodGroup[] BloodGroupList { get; set; }
    [XmlElement("BloodRHList")]
    public DetailBloodRH[] BloodRHList { get; set; }
    [XmlElement("MilitaryList")]
    public DetailsMilitary[] MilitaryList { get; set; }
    [XmlElement("EduTypeList")]
    public DetailEduType[] EduTypeList { get; set; }
    [XmlElement("FamilyTypeList")]
    public DetailFamilyType[] FamilyTypeList { get; set; }
    [XmlElement("VisaTypeList")]
    public DetailVisaType[] VisaTypeList { get; set; }
    [XmlElement("LiveStatusList")]
    public DetailLiveStatus[] LiveStatusList { get; set; }
    [XmlElement("PersonalStatusList")]
    public DetailPersonalStatus[] PersonalStatusList { get; set; }
    [XmlElement("RegQuestionList")]
    public DetailRegQuestion[] RegQuestionList { get; set; }
    [XmlElement("RegChoiceList")]
    public DetailRegChoice[] RegChoiceList { get; set; }
    [XmlElement("RefTypeList")]
    public DetailRefType[] RefTypeList { get; set; }

    [XmlElement("BoxHospitalList")]
    public DetailsHospital[] BoxHospitalList { get; set; }
    //การศึกษา

    [XmlElement("boxFaculty")]
    public FacultyList[] boxFaculty { get; set; }

    [XmlElement("boxExperience")]
    public boxExperienceList[] boxExperience { get; set; }

    [XmlElement("boxExperienceSend_B")]
    public boxExperienceSend_BList[] boxExperienceSend_B { get; set; }

    #endregion

}


[Serializable]
public class boxExperienceList
{
    [XmlElement("emp_idx_exper")]
    public int emp_idx_exper { get; set; }
    [XmlElement("name_company")]
    public string name_company { get; set; }
    [XmlElement("position_old")]
    public string position_old { get; set; }
    [XmlElement("workIn_old")]
    public string workIn_old { get; set; }
    [XmlElement("resign_old")]
    public string resign_old { get; set; }
    [XmlElement("motive")]
    public string motive { get; set; }
    [XmlElement("salary_old")]
    public string salary_old { get; set; }
    [XmlElement("job_brief_details")]
    public string job_brief_details { get; set; }
    [XmlElement("status_exper")]
    public int status_exper { get; set; }

    [XmlElement("emp_idx_")]
    public int emp_idx_ { get; set; }
    [XmlElement("id_table_exper")]
    public int id_table_exper { get; set; }


}

[Serializable]
public class boxExperienceSend_BList
{
    [XmlElement("emp_idx_experS")]
    public int emp_idx_experS { get; set; }
    [XmlElement("name_companyS")]
    public string name_companyS { get; set; }
    [XmlElement("position_oldS")]
    public string position_oldS { get; set; }
    [XmlElement("workIn_oldS")]
    public string workIn_oldS { get; set; }
    [XmlElement("resign_oldS")]
    public string resign_oldS { get; set; }
    [XmlElement("motiveS")]
    public string motiveS { get; set; }
    [XmlElement("salary_oldS")]
    public string salary_oldS { get; set; }
    [XmlElement("job_brief_detailsS")]
    public string job_brief_detailsS { get; set; }
    [XmlElement("status_experS")]
    public int status_experS { get; set; }

    [XmlElement("emp_idx_S")]
    public int emp_idx_S { get; set; }
    [XmlElement("id_table_experS")]
    public int id_table_experS { get; set; }


}

[Serializable]
public class FacultyList
{
    [XmlElement("FacIDX")]
    public int FacIDX { get; set; }
    [XmlElement("Faculty_Name")]
    public string Faculty_Name { get; set; }


}



[Serializable]
public class DetailsHospital  /*  โรงพยาบาล  */
{
    [XmlElement("HosIDX")]
    public int HosIDX { get; set; }
    [XmlElement("Name")]
    private string _Name;
    public string Name { get; set; }
    [XmlElement("HStatus")]
    public int HStatus { get; set; }
    [XmlElement("HStatus1")]
    private string _HStatus1;
    public string HStatus1 { get; set; }
}




//[Serializable]
//public class DetailEducation
//{
    
//    [XmlElement("Eduidx")]
//    public int Eduidx { get; set; }
//    [XmlElement("Edu_name")]
//    public string Edu_name { get; set; }
//    [XmlElement("Major")]
//    public string Major { get; set; }
//    [XmlElement("University")]
//    public string University { get; set; }
//    [XmlElement("Faculty")]
//    public int Faculty { get; set; }

    


//}

#region Presolnal
[Serializable]
public class DetailPrefixName
{
    [XmlElement("PrefixIDX")]
    public int PrefixIDX { get; set; }
    [XmlElement("PrefixNameTH")]
    public string PrefixNameTH { get; set; }
    [XmlElement("PrefixNameEN")]
    public string PrefixNameEN { get; set; }
    [XmlElement("SexIDX")]
    public int SexIDX { get; set; }
    [XmlElement("PFStatus")]
    public int PFStatus { get; set; }
    [XmlElement("SexNameTH")]
    public string SexNameTH { get; set; }
}
[Serializable]
public class DetailGender
{
    [XmlElement("SexIDX")]
    public int SexIDX { get; set; }
    [XmlElement("SexName")]
    public string SexNameTH { get; set; }
    [XmlElement("SexNameEN")]
    public string SexNameEN { get; set; }
    [XmlElement("SStatus")]
    public int SStatus { get; set; }
}
[Serializable]
public class DetailBloodGroup
{
    [XmlElement("BGIDX")]
    public int BGIDX { get; set; }
    [XmlElement("BGName")]
    public string BGName { get; set; }
    [XmlElement("BGStatus")]
    public int BGStatus { get; set; }
}
[Serializable]
public class DetailBloodRH
{
    [XmlElement("BRHIDX")]
    public int BRHIDX { get; set; }
    [XmlElement("RHName")]
    public string RHName { get; set; }
    [XmlElement("RHStatus")]
    public int RHStatus { get; set; }
}
[Serializable]
public class DetailsMilitary
{
    [XmlElement("MilIDX")]
    public int MilIDX { get; set; }
    [XmlElement("MilName")]
    public string MilName { get; set; }
    [XmlElement("MSStatus")]
    public int MSStatus { get; set; }
}
[Serializable]
public class DetailEduType
{
    [XmlElement("EduTypeIDX")]
    public int EduTypeIDX { get; set; }
    [XmlElement("NameTH")]
    public string NameTH { get; set; }
    [XmlElement("NameEN")]
    public string NameEN { get; set; }
    [XmlElement("ETStatus")]
    public int ETStatus { get; set; }
}
[Serializable]
public class DetailFamilyType
{
    [XmlElement("FTIDX")]
    public int FTIDX { get; set; }
    [XmlElement("FTNameTH")]
    public string FTNameTH { get; set; }
    [XmlElement("FTNameEN")]
    public string FTNameEN { get; set; }
    [XmlElement("FTStatus")]
    public int FTStatus { get; set; }
}
[Serializable]
public class DetailVisaType
{
    [XmlElement("VisaIDX")]
    public int VisaIDX { get; set; }
    [XmlElement("NameTH")]
    public string NameTH { get; set; }
    [XmlElement("NameEN")]
    public string NameEN { get; set; }
    [XmlElement("VTStatus")]
    public int VTStatus { get; set; }
}
[Serializable]

public class DetailLiveStatus
{
    [XmlElement("LiveIDX")]
    public int LiveIDX { get; set; }
    [XmlElement("NameTH")]
    public string NameTH { get; set; }
    [XmlElement("NameEN")]
    public string NameEN { get; set; }
    [XmlElement("LiveStatus")]
    public int LiveStatus { get; set; }
}
[Serializable]
public class DetailPersonalStatus
{
    [XmlElement("PSIDX")]
    public int PSIDX { get; set; }
    [XmlElement("NameTH")]
    public string NameTH { get; set; }
    [XmlElement("NameEN")]
    public string NameEN { get; set; }
    [XmlElement("PSStatus")]
    public int PSStatus { get; set; }
}
[Serializable]
public class DetailRegQuestion
{
    [XmlElement("RQIDX")]
    public int RQIDX { get; set; }
    [XmlElement("TopicTH")]
    public string TopicTH { get; set; }
    [XmlElement("TopicEN")]
    public string TopicEN { get; set; }
    [XmlElement("RQStatus")]
    public int RQStatus { get; set; }
}
[Serializable]
public class DetailRegChoice
{
    [XmlElement("RCIDX")]
    public int RCIDX { get; set; }
    [XmlElement("RQIDX")]
    public int RQIDX { get; set; }
    [XmlElement("RQTopicTH")]
    public string RQTopicTH { get; set; }
    [XmlElement("RQTopicEN")]
    public string RQTopicEN { get; set; }
    [XmlElement("TopicTH")]
    public string TopicTH { get; set; }
    [XmlElement("TopicEN")]
    public string TopicEN { get; set; }
    [XmlElement("RCType")]
    public int RCType { get; set; }
    [XmlElement("RCStatus")]
    public int RCStatus { get; set; }
}
[Serializable]
public class DetailRefType
{
    [XmlElement("RTIDX")]
    public int RTIDX { get; set; }
    [XmlElement("NameTH")]
    public string NameTH { get; set; }
    [XmlElement("NameEN")]
    public string NameEN { get; set; }
    [XmlElement("RTStatus")]
    public int RTStatus { get; set; }
}



#endregion
