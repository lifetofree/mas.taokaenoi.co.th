﻿<%@ WebHandler Language="C#" Class="Handler" %>

using System;
using System.Web;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Globalization;
using System.Threading;

public class Handler : IHttpHandler
{
   public void ProcessRequest(HttpContext context)
   {
      int creator = 0;
      int announceDiaryIdxRef = 0;
      string callback = context.Request.QueryString["callback"];
      string changeAnnounceDiaryStartDate = String.Empty;
      string changeAnnounceDiaryEndDate = String.Empty;
      int.TryParse(context.Request.QueryString["announceDiaryIdxRef"], out announceDiaryIdxRef);
      int.TryParse(context.Request.QueryString["creator"], out creator);
      changeAnnounceDiaryStartDate = context.Request.QueryString["changeAnnounceDiaryStartDate"];
      changeAnnounceDiaryEndDate = context.Request.QueryString["changeAnnounceDiaryEndDate"];
      string json = this.createChangeAnnounceDiary(creator, announceDiaryIdxRef, changeAnnounceDiaryStartDate, changeAnnounceDiaryEndDate);
      if (!string.IsNullOrEmpty(callback))
      {
         json = string.Format("{0}({1});", callback, json);
      }
      context.Response.ContentType = "text/json";
      context.Response.Write(json);
   }

   private string createChangeAnnounceDiary(int creator, int announceDiaryIdxRef, string changeAnnounceDiaryStartDate, string changeAnnounceDiaryEndDate)
   {
      List<object> emps = new List<object>();
      using (SqlConnection conn = new SqlConnection())
      {
         conn.ConnectionString = ConfigurationManager.ConnectionStrings["conn_mas"].ConnectionString;
         using (SqlCommand cmd = new SqlCommand())
         {
            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;
            var now = DateTime.Now.ToString("yyyy-MM-dd H:m:s");
            int statusOnline = 1;
            cmd.Connection = conn;
            cmd.CommandText = "INSERT INTO emps_u0_change_announce_diary (u0_announce_diary_idx_ref, u0_node_idx_ref, change_announce_diary_date_start, change_announce_diary_date_end, change_announce_diary_status, change_announce_diary_created_at, change_announce_diary_created_by, change_announce_diary_updated_at, change_announce_diary_updated_by) OUTPUT Inserted.u0_change_announce_diary_idx VALUES (@announceDiaryIdxRef, 1, @changeAnnounceDiaryStartDate, @changeAnnounceDiaryEndDate, " + statusOnline + ", '" + now + "', @creator, '" + now + "', @creator)";
            cmd.Parameters.AddWithValue("@changeAnnounceDiaryStartDate", changeAnnounceDiaryStartDate);
            cmd.Parameters.AddWithValue("@changeAnnounceDiaryEndDate", changeAnnounceDiaryEndDate);
            cmd.Parameters.AddWithValue("@creator", creator);
            cmd.Parameters.AddWithValue("@announceDiaryIdxRef", announceDiaryIdxRef);
            conn.Open();
            var insertedId = cmd.ExecuteScalar();
            emps.Add(new
            {
               announceGroupDiaryIdx = insertedId
            });
            conn.Close();
         }
         return (new JavaScriptSerializer().Serialize(emps));
      }
   }

   public bool IsReusable
   {
      get
      {
         return false;
      }
   }
}
