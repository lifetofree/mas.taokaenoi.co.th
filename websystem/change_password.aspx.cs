using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_change_password : System.Web.UI.Page
{
    #region initial function/data
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlSetPassword = _serviceUrl + ConfigurationManager.AppSettings["urlSetPassword"];
    static string _urlurlGet_repasswordList = _serviceUrl + ConfigurationManager.AppSettings["urlGet_repasswordList"];

    string _localJson = "";
    string _initPass = "25d55ad283aa400af464c76d713c07ad";
    #endregion initial function/data

    private void Page_Init(object sender, System.EventArgs e)
    {
        if (Session["emp_idx"] == null)
        {
            string path = HttpContext.Current.Request.Url.AbsolutePath;
            Response.Redirect(ResolveUrl("~/warning") + "?url=" + path);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            // check Session["reset_type"]
            if(Session["reset_type"] != null)
            {
                if(int.Parse(Session["reset_type"].ToString()) == 1)
                {
                    tbEmpPass.Enabled = false;
                    rfvEmpPass.Enabled = false;
                    tbNewPassword.Focus();
                }
                else
                {
                    Session["reset_type"] = 2;
                    tbEmpPass.Focus();
                }
            }
            else
            {
                Session["reset_type"] = 2;
                tbEmpPass.Focus();
            }
        }
    }

    protected data_employee callServiceEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //fss.Text = _localJson.ToString();

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //fsa.Text = _cmdUrl + _localJson;

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);


        return _dataEmployee;
    }

    #region btn command
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {
            case "cmdChangePassword":
                if(Session["emp_idx"] != null && Session["reset_type"] != null)
                {
                    int _empIdx = int.Parse(Session["emp_idx"].ToString());
                    string _empPass = String.Empty;
                    if(int.Parse(Session["reset_type"].ToString()) == 1)
                    {
                        _empPass = _initPass;
                    }
                    else
                    {
                        _empPass = _funcTool.getMd5Sum(tbEmpPass.Text.Trim());
                    }
                    string _newPass = _funcTool.getMd5Sum(tbNewPassword.Text.Trim());
                    string _ipAddress = "";
                    int _resetType = int.Parse(Session["reset_type"].ToString()); // 1:change from initial; 2:user change; 3:manual reset password

                    if(_empPass != String.Empty)
                    {
                        // set data
                        _dataEmployee.employee_list = new employee_detail[1];
                        employee_detail _empDetail = new employee_detail();
                        _empDetail.emp_idx = _empIdx;
                        _empDetail.emp_password = _newPass; //_empPass;
                        _empDetail.new_password = _newPass;
                        _empDetail.ip_address = _ipAddress;
                        _empDetail.type_reset = _resetType;
                        _dataEmployee.employee_list[0] = _empDetail;

                        // convert to json
                        //_localJson = _funcTool.convertObjectToJson(_dataEmployee);

                        // call services
                        _dataEmployee = callServiceEmployee(_urlurlGet_repasswordList, _dataEmployee);

                        // convert json to object
                        //_dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);
                        
                        // check return_code
                        if(int.Parse(_dataEmployee.return_code) == 0)
                        {
                            // create session
                            Session["reset_type"] = null;

                            // go to page
                            string url = Request.QueryString["url"];
                            if(url == null)
                            {
                                Response.Redirect(ResolveUrl("~/success"));
                            }
                            else
                            {
                                Response.Redirect(ResolveUrl("~/success") + "?url=" + url);
                            }
                        }
                        else
                        {
                            divShowError.Visible = true;
                            litErrorCode.Text = _dataEmployee.return_code.ToString() + " : ข้อมูลไม่ถูกต้อง";
                            //litDebug.Text = "error : " + _dataEmployee.return_code.ToString() + " - " + _dataEmployee.return_msg;
                        }
                    }
                }
            break;
        }
    }
    #endregion btn command
}
