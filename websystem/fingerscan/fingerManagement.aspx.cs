﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Net;
using zkemkeeper;

public partial class websystem_fingerscan_fingerManagement : System.Web.UI.Page
{

    #region Connect
    //DBConn DBConn = new DBConn();
    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();
    //FunctionWeb funcWeb = new FunctionWeb();

    private string ODSP_Reletion = "ODSP_Reletion_Mas";
    private string ConCL = "conn_centralized";

    data_employee dtEmployee = new data_employee();
    dataODSP_Relation dtODSP = new dataODSP_Relation();
    data_fingermanagement dtfingermanagement= new data_fingermanagement();
    function_scan _funcScan = new function_scan();
    function_db _funcdb = new function_db();

    int emp_idx = 0;
    int defaultInt = 0;
    IPAddress defaultIP = new IPAddress(0);

    // public CZKEM axCZKEM1 = new CZKEM();
    public bool bIsConnected = false;//the boolean value identifies whether the device is connected
    public int iMachineNumber = 1;//the serial number of the device.After connecting

    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";

    public CZKEM axCZKEM1 = new CZKEM();

    private string sIp = "";
    private int iPort = 0;
    private int idwErrorCode = 0;
    private int idwValue = 0;
    private int idwStatus = 0;
    private int iFlag = 0;
    private string sTmpData = "";
    private int iTmpLength = 0;


    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

            #region ViewAddEmp

            var dsEmp = new DataSet();
            dsEmp.Tables.Add("TempEmp");

            dsEmp.Tables[0].Columns.Add("EmpCode", typeof(string));
            dsEmp.Tables[0].Columns.Add("FullNameTH", typeof(string));

            ViewState["vsTempEmp"] = dsEmp;

            #endregion

            ViewState["EmpIDX"] = Session["emp_idx"].ToString();

            ViewState["EmpDatabase"] = null;
            ViewState["DataTranfer"] = null;
            ViewState["Check_DataTranfer"] = null;
            ViewState["CheckSearch"] = 0;
            ViewState["OrgIDX"] = 0;
            ViewState["RDeptIDX"] = 0;
            ViewState["RSecIDX"] = 0;
            ViewState["RPosIDX"] = 0;

            MvMaster.SetActiveView(ViewIndex);
            Select_Employee();
            SelectMaster_Finger_List();
            Select_Checkbox_List();
            Select_FingerCenter_List();
            Select_FingerCheck_List();
            ddlOrg();
            Set_Defult();


            //Set Permission
            #region Permission
            
            if (int.Parse(ViewState["OrgIDX"].ToString()) == 1) // NPW
            {
                if (int.Parse(ViewState["RPosIDX"].ToString()) == 113 || 
                    int.Parse(ViewState["RPosIDX"].ToString()) == 650 || 
                    int.Parse(ViewState["RPosIDX"].ToString()) == 648 ||
                    int.Parse(ViewState["RPosIDX"].ToString()) == 3048 ||
                    int.Parse(ViewState["RPosIDX"].ToString()) == 3046 ||
                    int.Parse(ViewState["RPosIDX"].ToString()) == 63 ||
                    int.Parse(ViewState["RPosIDX"].ToString()) == 3047 ||
                    int.Parse(ViewState["RPosIDX"].ToString()) == 3045 ||
                    int.Parse(ViewState["RPosIDX"].ToString()) == 58)
                {
                    BoxPermission.Visible = true;
                    Admin_FingerCheck.Visible = true;
                    lbltranfer_view.Visible = true;
                }
                else
                {
                    BoxPermission.Visible = false;
                }
            }
            else //TKNL etc
            {
                if (int.Parse(ViewState["RPosIDX"].ToString()) == 411 ||
                    int.Parse(ViewState["RPosIDX"].ToString()) == 867 ||
                    int.Parse(ViewState["RPosIDX"].ToString()) == 3543 ||
                    int.Parse(ViewState["RPosIDX"].ToString()) == 377 ||
                    int.Parse(ViewState["RPosIDX"].ToString()) == 378)
                {
                    BoxPermission.Visible = true;
                    Admin_FingerCheck.Visible = true;
                    lbltranfer_view.Visible = true;
                }
                else
                {
                    BoxPermission.Visible = false;
                }
            }

            if (int.Parse(ViewState["EmpIDX"].ToString()) == 172 || int.Parse(ViewState["EmpIDX"].ToString()) == 173)
            {
                BoxPermission.Visible = true;
                Admin_FingerCheck.Visible = true;
                lbltranfer_view.Visible = true;
            }



            #endregion
        }
    }


    #region INSERT&SELECT&UPDATE

    protected void Select_Employee()
    {
        data_fingermanagement dtfingermanagement = new data_fingermanagement();
        u0_finger_detail select = new u0_finger_detail();
        dtfingermanagement.u0_finger_detail_list = new u0_finger_detail[1];

        select.EmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

        dtfingermanagement.u0_finger_detail_list[0] = select;

        _local_xml = serviceexcute.actionExec("conn_mas", "data_fingermanagement", "service_figermanagement", dtfingermanagement, 25);
        dtfingermanagement = (data_fingermanagement)_funcTool.convertXmlToObject(typeof(data_fingermanagement), _local_xml);

        ViewState["OrgIDX"] = dtfingermanagement.u0_finger_detail_list[0].OrgIDX;
        ViewState["RDeptIDX"] = dtfingermanagement.u0_finger_detail_list[0].RDepIDX;
        ViewState["RSecIDX"] = dtfingermanagement.u0_finger_detail_list[0].RSecIDX;
        ViewState["RPosIDX"] = dtfingermanagement.u0_finger_detail_list[0].RPosIDX;
    }

    protected void SelectMaster_Finger_List()
    {
        data_fingermanagement dtfingermanagement = new data_fingermanagement();
        u0_finger_detail select = new u0_finger_detail();

        select.EmpCode = "0";

        _local_xml = serviceexcute.actionExec("conn_mas", "data_fingermanagement", "service_figermanagement", dtfingermanagement, 22);
        dtfingermanagement = (data_fingermanagement)_funcTool.convertXmlToObject(typeof(data_fingermanagement), _local_xml);

        ViewState["EmpDatabase"] = dtfingermanagement;
        ViewState["Seach_EmpDatabase"] = dtfingermanagement;
        setGridData(GvEmpDatabase, dtfingermanagement.u0_finger_detail_list);
    }

    protected void Search_Finger_List()
    {
        ViewState["Search_OrgIDX"] = ddlOrganization.SelectedValue;
        ViewState["Search_DepIDX"] = ddlDepartment.SelectedValue;
        ViewState["Search_SecIDX"] = ddlSection.SelectedValue;
        ViewState["Search_EmpCode"] = txtempid.Text;
        string Check_IF = "";

        var Search = (data_fingermanagement)ViewState["Seach_EmpDatabase"];

        if (ViewState["Search_EmpCode"].ToString() != "")
        {
            Check_IF = "Check_Code";
            Search = Set_Linq(Search, Check_IF);
            GvEmpDatabase.DataSource = Search.u0_finger_detail_list;
            GvEmpDatabase.DataBind();
        }
        else if (int.Parse(ViewState["Search_OrgIDX"].ToString()) != 0)
        {
            if (int.Parse(ViewState["Search_DepIDX"].ToString()) != 0)
            {
                if (int.Parse(ViewState["Search_SecIDX"].ToString()) != 0)
                {
                    Check_IF = "Check_Org_Dep_Sec";
                    Search = Set_Linq(Search, Check_IF);
                    GvEmpDatabase.DataSource = Search.u0_finger_detail_list;
                    GvEmpDatabase.DataBind();
                }
                else
                {
                    Check_IF = "Check_Org_Dep";
                    Search = Set_Linq(Search,Check_IF);
                    setGridData(GvEmpDatabase, Search.u0_finger_detail_list);
                }
            }
            else
            {
                Check_IF = "Check_Org";
                Search = Set_Linq(Search, Check_IF);
                GvEmpDatabase.DataSource = Search.u0_finger_detail_list;
                GvEmpDatabase.DataBind();
            }
        }
        else
        {
            GvEmpDatabase.DataSource = null;
            GvEmpDatabase.DataBind();

        }
    }

    protected void Box_Select_Search()
    {
        var Search_1 = (data_fingermanagement)ViewState["EmpDatabase"];

        if (int.Parse(ViewState["CheckSearch"].ToString()) == 0)
        {
            var BoxDetaultGvReserva2 = from b in Search_1.u0_finger_detail_list where b.EmpStatus == 1 select b;
            GvEmpDatabase.DataSource = BoxDetaultGvReserva2.ToList();
            GvEmpDatabase.DataBind();
        }
        else
        {
            Search_Finger_List();
        }

        var BoxDetaultGvReserva1 = from a in Search_1.u0_finger_detail_list where a.EmpStatus == 2 select a;
        //ViewState["Tranfer_Check"] = BoxDetaultGvReserva1.ToList();
        GvEmpTemp.DataSource = BoxDetaultGvReserva1.ToList(); //ViewState["Tranfer_Check"];
        GvEmpTemp.DataBind();

    }

    protected void Box_Select_update_linq(data_fingermanagement finger, int statusupdate ,int empidx)
    {
        int i = 0;
        foreach (var ID in finger.u0_finger_detail_list)
        {
            if (ID.EmpIDX == empidx)
            {
                finger.u0_finger_detail_list[i].EmpStatus = int.Parse(statusupdate.ToString());
            }
            i++;
        }
    }

    protected void Select_Checkbox_List()
    {
        YrChkBox.Items.Clear();
        YrChkBox.AppendDataBoundItems = true;

        data_fingermanagement dtfingermanagement = new data_fingermanagement();
        u0_finger_detail select = new u0_finger_detail();

        _local_xml = serviceexcute.actionExec("conn_mas", "data_fingermanagement", "service_figermanagement", dtfingermanagement, 21);
        dtfingermanagement = (data_fingermanagement)_funcTool.convertXmlToObject(typeof(data_fingermanagement), _local_xml);

        YrChkBox.DataSource = dtfingermanagement.u0_finger_detail_list;
        YrChkBox.DataTextField = "fm_name";
        YrChkBox.DataValueField = "fm_ip";
        YrChkBox.DataBind(); 
    }

    protected void Select_FingerCenter_List()
    {
        ddlfinger_center.Items.Clear();
        ddlfinger_center.AppendDataBoundItems = true;
        ddlfinger_center.Items.Add(new ListItem("Select Finger Center....", "0"));

        data_fingermanagement dtfingermanagement = new data_fingermanagement();
        u0_finger_detail select = new u0_finger_detail();

        _local_xml = serviceexcute.actionExec("conn_mas", "data_fingermanagement", "service_figermanagement", dtfingermanagement, 23);
        dtfingermanagement = (data_fingermanagement)_funcTool.convertXmlToObject(typeof(data_fingermanagement), _local_xml);

        ddlfinger_center.DataSource = dtfingermanagement.u0_finger_detail_list;
        ddlfinger_center.DataTextField = "fm_name";
        ddlfinger_center.DataValueField = "fm_ip";
        ddlfinger_center.DataBind();

    }

    protected void Select_FingerCheck_List()
    {
        ddlfinger_center_check.Items.Clear();
        ddlfinger_center_check.AppendDataBoundItems = true;
        ddlfinger_center_check.Items.Add(new ListItem("Select Finger Check....", "0"));

        data_fingermanagement dtfingermanagement = new data_fingermanagement();
        u0_finger_detail select = new u0_finger_detail();

        _local_xml = serviceexcute.actionExec("conn_mas", "data_fingermanagement", "service_figermanagement", dtfingermanagement, 24);
        dtfingermanagement = (data_fingermanagement)_funcTool.convertXmlToObject(typeof(data_fingermanagement), _local_xml);

        ddlfinger_center_check.DataSource = dtfingermanagement.u0_finger_detail_list;
        ddlfinger_center_check.DataTextField = "fm_name";
        ddlfinger_center_check.DataValueField = "fm_ip";
        ddlfinger_center_check.DataBind();

    }

    protected data_fingermanagement Set_Linq(data_fingermanagement Search_,string CheckIF)
    {
        data_fingermanagement finger = new data_fingermanagement();
        int b = 0;
        int count = 0;
        ViewState["Search_linq"] = "";

        switch (CheckIF)
        {

            case "Check_Code":

                var linq_1 = from a in Search_.u0_finger_detail_list where a.EmpCode == ViewState["Search_EmpCode"].ToString() && a.EmpStatus == 1 select a;

                count = linq_1.Count();

                finger.u0_finger_detail_list = new u0_finger_detail[count];

                foreach (u0_finger_detail _Search in linq_1.ToList())
                {
                    u0_finger_detail dtfinger = new u0_finger_detail();
                    dtfinger.OrgIDX = _Search.OrgIDX;
                    dtfinger.RDepIDX = _Search.RDepIDX;
                    dtfinger.RSecIDX = _Search.RSecIDX;
                    dtfinger.EmpCode = _Search.EmpCode;
                    dtfinger.FullNameTH = _Search.FullNameTH;
                    dtfinger.fidx = _Search.fidx;
                    dtfinger.fm_ip = _Search.fm_ip;
                    dtfinger.fm_name = _Search.fm_name;
                    dtfinger.LocIDX = _Search.LocIDX;
                    dtfinger.LocName = _Search.LocName;
                    dtfinger.fmtidx = _Search.fmtidx;
                    dtfinger.fms_name = _Search.fms_name;
                    dtfinger.EmpIDX = _Search.EmpIDX;
                    dtfinger.EmpStatus = _Search.EmpStatus;

                    finger.u0_finger_detail_list[b] = dtfinger;
                    b++;
                }

                break;

            case "Check_Org":

                var linq_2 = from a in Search_.u0_finger_detail_list where (a.OrgIDX == int.Parse(ViewState["Search_OrgIDX"].ToString())) && a.EmpStatus == 1 select a;

                count = linq_2.Count();

                finger.u0_finger_detail_list = new u0_finger_detail[count];

                foreach (u0_finger_detail _Search in linq_2.ToList())
                {
                    u0_finger_detail dtfinger = new u0_finger_detail();
                    dtfinger.OrgIDX = _Search.OrgIDX;
                    dtfinger.RDepIDX = _Search.RDepIDX;
                    dtfinger.RSecIDX = _Search.RSecIDX;
                    dtfinger.EmpCode = _Search.EmpCode;
                    dtfinger.FullNameTH = _Search.FullNameTH;
                    dtfinger.fidx = _Search.fidx;
                    dtfinger.fm_ip = _Search.fm_ip;
                    dtfinger.fm_name = _Search.fm_name;
                    dtfinger.LocIDX = _Search.LocIDX;
                    dtfinger.LocName = _Search.LocName;
                    dtfinger.fmtidx = _Search.fmtidx;
                    dtfinger.fms_name = _Search.fms_name;
                    dtfinger.EmpIDX = _Search.EmpIDX;
                    dtfinger.EmpStatus = _Search.EmpStatus;

                    finger.u0_finger_detail_list[b] = dtfinger;
                    b++;
                }

                break;

            case "Check_Org_Dep":

                var linq_3 = from a in Search_.u0_finger_detail_list
                                  where
                                   (a.OrgIDX == int.Parse(ViewState["Search_OrgIDX"].ToString()) &&
                                   (a.RDepIDX == int.Parse(ViewState["Search_DepIDX"].ToString()))) && 
                                   a.EmpStatus == 1
                             select a;

                count = linq_3.Count();

                finger.u0_finger_detail_list = new u0_finger_detail[count];

                foreach (u0_finger_detail _Search in linq_3.ToList())
                {
                    u0_finger_detail dtfinger = new u0_finger_detail();
                    dtfinger.OrgIDX = _Search.OrgIDX;
                    dtfinger.RDepIDX = _Search.RDepIDX;
                    dtfinger.RSecIDX = _Search.RSecIDX;
                    dtfinger.EmpCode = _Search.EmpCode;
                    dtfinger.FullNameTH = _Search.FullNameTH;
                    dtfinger.fidx = _Search.fidx;
                    dtfinger.fm_ip = _Search.fm_ip;
                    dtfinger.fm_name = _Search.fm_name;
                    dtfinger.LocIDX = _Search.LocIDX;
                    dtfinger.LocName = _Search.LocName;
                    dtfinger.fmtidx = _Search.fmtidx;
                    dtfinger.fms_name = _Search.fms_name;
                    dtfinger.EmpIDX = _Search.EmpIDX;
                    dtfinger.EmpStatus = _Search.EmpStatus;

                    finger.u0_finger_detail_list[b] = dtfinger;
                    b++;
                }

                break;

            case "Check_Org_Dep_Sec":

                var linq_4 = from a in Search_.u0_finger_detail_list
                                  where
                                   (a.OrgIDX == int.Parse(ViewState["Search_OrgIDX"].ToString()) &&
                                   (a.RDepIDX == int.Parse(ViewState["Search_DepIDX"].ToString())) &&
                                   (a.RSecIDX == int.Parse(ViewState["Search_SecIDX"].ToString()))) && 
                                   a.EmpStatus == 1
                             select a;

                count = linq_4.Count();

                finger.u0_finger_detail_list = new u0_finger_detail[count];

                foreach (u0_finger_detail _Search in linq_4.ToList())
                {
                    u0_finger_detail dtfinger = new u0_finger_detail();
                    dtfinger.OrgIDX = _Search.OrgIDX;
                    dtfinger.RDepIDX = _Search.RDepIDX;
                    dtfinger.RSecIDX = _Search.RSecIDX;
                    dtfinger.EmpCode = _Search.EmpCode;
                    dtfinger.FullNameTH = _Search.FullNameTH;
                    dtfinger.fidx = _Search.fidx;
                    dtfinger.fm_ip = _Search.fm_ip;
                    dtfinger.fm_name = _Search.fm_name;
                    dtfinger.LocIDX = _Search.LocIDX;
                    dtfinger.LocName = _Search.LocName;
                    dtfinger.fmtidx = _Search.fmtidx;
                    dtfinger.fms_name = _Search.fms_name;
                    dtfinger.EmpIDX = _Search.EmpIDX;
                    dtfinger.EmpStatus = _Search.EmpStatus;

                    finger.u0_finger_detail_list[b] = dtfinger;
                    b++;
                }

                break;
        }

        return finger;
    }

    protected void Set_Defult()
    {
        ViewState["CheckSearch"] = 0;
        ViewState["Search_OrgIDX"] = 0;
        ViewState["Search_DepIDX"] = 0;
        ViewState["Search_SecIDX"] = 0;
        ViewState["Search_EmpCode"] = "";

        GvEmpDatabase.DataSource = null;
        GvEmpDatabase.DataBind();

        GvEmpTemp.DataSource = null;
        GvEmpTemp.DataBind();

        GvCheck_Finger.DataSource = null;
        GvCheck_Finger.DataBind();
    }

    #endregion

    #region Gridview

    #region SetGrid
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvEmpDatabase":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox CheckRow = (CheckBox)e.Row.FindControl("CheckRow");
                    CheckRow.Checked = true;

                    if (chkAll.Checked)
                    {
                        CheckRow.Checked = true;

                        if (chkAll.Checked && CheckRow.Checked)
                        {
                            CheckRow.Enabled = false;
                        }
                    }
                    else
                    {
                        CheckRow.Checked = false;
                        CheckRow.Enabled = true;
                    }

                }

                break;


        }

    }

    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvEmpDatabase":

                GvEmpDatabase.PageIndex = e.NewPageIndex;
                GvEmpDatabase.DataBind();

                Box_Select_Search();

                break;

            case "GvEmpTemp":

                GvEmpTemp.PageIndex = e.NewPageIndex;
                GvEmpTemp.DataBind();

                Box_Select_Search();

                break;

            case "GvCheck_Finger":

                GvCheck_Finger.PageIndex = e.NewPageIndex;
                GvCheck_Finger.DataBind();

                GvCheck_Finger.DataSource = (DataSet)ViewState["DataTranfer_Recheck"];
                GvCheck_Finger.DataBind();

                break;

            case "GvTemp":

                GvTemp.PageIndex = e.NewPageIndex;
                GvTemp.DataBind();

                GvTemp.DataSource = (DataSet)ViewState["DataTranfer_Report"];
                GvTemp.DataBind();

                break;
        }
    }

    #endregion

    #endregion

    #region chkSelectedIndexChanged

    protected void chkSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (CheckBox)sender;


        switch (ddName.ID)
        {

            case "chkAll":

                bool b = (sender as CheckBox).Checked;

                for (int i = 0; i < YrChkBox.Items.Count; i++)
                {
                    YrChkBox.Items[i].Selected = b;
                }

                break;

        }
    }

    #endregion

    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {
            case "Cmdsearch":

                Search_Finger_List();
                ViewState["CheckSearch"] = 1;

                break;

            case "btnselect":

                int EmpID = int.Parse(cmdArg);
                Box_Select_update_linq((data_fingermanagement)ViewState["EmpDatabase"],2, EmpID);
                Box_Select_update_linq((data_fingermanagement)ViewState["Seach_EmpDatabase"], 2, EmpID);
                Box_Select_Search();

                break;

            case "CmdDel":

                int EmpID_1 = int.Parse(cmdArg);
                Box_Select_update_linq((data_fingermanagement)ViewState["EmpDatabase"], 1, EmpID_1);
                Box_Select_update_linq((data_fingermanagement)ViewState["Seach_EmpDatabase"], 1, EmpID_1);
                Box_Select_Search();

                break;

            case "cmdtranfer":
                try
                {
                    GetFingerToViewState();
                    Box_Select_Search();
                    GetFingerScanToDatatable(); //ดึงข้อมูลจากเครื่อง Center
                    GetDatatableTranferFinger(); //โอนข้อมูลไปตามเครื่องที่เลือก
                }
                catch { }

                break;

            case "cmdtranfer_delete":
                try
                {
                    GetFingerToViewState();
                    GetFingerScanToDatatable_Dalete();
                    GetDatatableDeleteFinger();
                }
                catch { }

                break;

            case "cmdtranfer_view":

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);

                try
                {
                    GetFingerToViewState();
                    GetFingerScanToDatatable_Recheck();

                    GvCheck_Finger.DataSource = (DataSet)ViewState["DataTranfer_Recheck"];
                    GvCheck_Finger.DataBind();
                }
                catch { }

                break;

            case "Cmdsearch_clear":

                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "cmdcheck_tranfer":

                try
                {
                    GetFingerToViewState();
                    GetFingerScanToDatatable_Report();
                    GetDatatableTranferFinger_Report();

                    GvTemp.DataSource = (DataSet)ViewState["DataTranfer_Report"];
                    GvTemp.DataBind();
                }
                catch { }

                break;
        }

    }
    #endregion


    #region reuse

    private string setNewline()
    {
        return "<br />----------<br />";
    }

    private string convertHostToIp(string hostName)
    {
        IPHostEntry hostEntry = Dns.GetHostEntry(hostName);
        if (hostEntry.AddressList.Length > 0)
        {
            return hostEntry.AddressList[0].ToString();
        }
        else
        {
            return "0";
        }
    }

    protected void GetFingerToViewState()
    {
        ViewState["finger_ip_tranfer"] = null;
        ViewState["Finger_Center"] = 0;
        ViewState["finger_EmpCode_tranfer"] = null;
        var Search_1 = (data_fingermanagement)ViewState["EmpDatabase"];

        List<String> YrStrList = new List<string>();
        foreach (ListItem item in YrChkBox.Items)
        {
            if (item.Selected)
            {
                YrStrList.Add(item.Value);
            }
        }
        String YrStr = String.Join(",", YrStrList.ToArray());


        var linq = from a in Search_1.u0_finger_detail_list where a.EmpStatus == 2 select a.EmpCode;
        string temp = "";
        string[] names = linq.ToArray();

        for (int i = 0; i < names.Length; i++)
        {
            if (i + 1 == names.Length)
            {
                temp += names[i];
            }
            else
            {
                temp += names[i] + ",";
            }
        }

        ViewState["finger_ip_tranfer"] = YrStr; //IP Tranfer
        ViewState["finger_ip_tranfer_name"] = ddlfinger_center_check.SelectedItem.Text; //IP Name
        ViewState["Finger_Center"] = ddlfinger_center.SelectedValue; //IP Center - 1 เครื่อง
        ViewState["finger_EmpCode_tranfer"] = temp; //EmpCode Tranfer
    }

    protected void GetFingerScanToDatatable()
    {
        string sFromIp = ViewState["Finger_Center"].ToString();
        string ToIp = ViewState["finger_ip_tranfer"].ToString();
        string ip_EmpCode_tranfer = ViewState["finger_EmpCode_tranfer"].ToString();
        int iFromPort = 4370;
        string[] _sToIp = ToIp.Split(',');
        string[] Toempcode = ip_EmpCode_tranfer.Split(',');
        string temp = "";

        #region ViewLogScanner

        DataSet ds = new DataSet();

        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("empcode", typeof(string)));
        dt.Columns.Add(new DataColumn("finger", typeof(string)));
        dt.Columns.Add(new DataColumn("finger_index", typeof(int)));
        dt.Columns.Add(new DataColumn("finger_status_tranfer_Succec", typeof(int)));
        dt.Columns.Add(new DataColumn("ip_finger", typeof(string)));
        dt.Columns.Add(new DataColumn("finger_status_database", typeof(int)));

        #endregion

        // --------------------------------------------1. Loop Finger Ip Center -------------------------------//
        if (_funcScan.setMachineConnect(sFromIp, iFromPort))
            {
                _funcScan.setEnableDevice(iMachineNumber, false);

                // --------------------------------------------2. Loop EmpCode Tranfer -------------------------------//
                foreach (string sdwEnrollNumber in Toempcode)
                {
                    if (sdwEnrollNumber != String.Empty)
                    {
                        // --------------------------------------------3. Loop Finger Index[] -------------------------------//
                        for (int iFromFingerIndex = 0; iFromFingerIndex <= 9; iFromFingerIndex++)
                        {
                            //sTmpData = _funcScan.getUserTemplate(iMachineNumber, sdwEnrollNumber, iFromFingerIndex);// iFromFingerIndex);

                            if (sTmpData != "-2" && sTmpData != "0") // กรณี sTmpData Null จะมีค่าเป็น -2 และ 0
                            {
                                temp += " , " + sdwEnrollNumber + " , " + sTmpData;

                                DataRow dr = dt.NewRow();
                                dr["empcode"] = sdwEnrollNumber;
                                dr["finger"] = sTmpData;
                                dr["finger_index"] = iFromFingerIndex;
                                dr["finger_status_tranfer_Succec"] = 0;
                                dr["ip_finger"] = sFromIp;
                                dr["finger_status_database"] = _funcScan.getMachineError().ToString();
                                dt.Rows.Add(dr);

                            }
                            else
                            {
                                break;
                            }
                        }
                        // --------------------------------------------3. Loop Finger Index[] -------------------------------//
                    }
                }
                ds.Tables.Add(dt); //Datatable - EmpCode - Finger
                _funcScan.setEnableDevice(iMachineNumber, true);
                _funcScan.setMachineDisconnect();
                // --------------------------------------------2. End Loop Finger Ip Tranfer -------------------------------//
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Tranfer", "alert('" + "Tranfer Error" + "')", true);
            }
            // --------------------------------------------1. End Loop Finger Ip Center -------------------------------//

            ViewState["DataTranfer"] = ds;

    }

    protected void GetFingerScanToDatatable_Dalete()
    {
        //string sFromIp = ViewState["Finger_Center"].ToString();
        string ToIp = ViewState["finger_ip_tranfer"].ToString();
        string ip_EmpCode_tranfer = ViewState["finger_EmpCode_tranfer"].ToString();
        string[] _sToIp = ToIp.Split(',');
        string[] Toempcode = ip_EmpCode_tranfer.Split(',');
        //string temp = "";

        #region ViewLogScanner

        DataSet ds = new DataSet();

        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("empcode", typeof(string)));
        dt.Columns.Add(new DataColumn("finger", typeof(string)));
        dt.Columns.Add(new DataColumn("finger_index", typeof(int)));

        #endregion


        // --------------------------------------------2. Loop EmpCode Tranfer -------------------------------//
        foreach (string sdwEnrollNumber in Toempcode)
        {
            if (sdwEnrollNumber != String.Empty)
            {
                // --------------------------------------------3. Loop Finger Index[] -------------------------------//
                /*for (int iFromFingerIndex = 0; iFromFingerIndex <= 9; iFromFingerIndex++)
                {
                    sTmpData = _funcScan.getUserTemplate(iMachineNumber, sdwEnrollNumber, iFromFingerIndex);// iFromFingerIndex);
                    */
                    //if (sTmpData != "-2" && sTmpData != "0") // กรณี sTmpData Null จะมีค่าเป็น -2 และ 0
                    //{
                        //temp += " , " + sdwEnrollNumber + " , " + sTmpData;

                        DataRow dr = dt.NewRow();
                        dr["empcode"] = sdwEnrollNumber;
                        //dr["finger"] = sTmpData;
                        dr["finger_index"] = 0;//iFromFingerIndex;
                        dt.Rows.Add(dr);
                    /*}
                    else
                    {
                        break;
                    }*/
                //}
                // --------------------------------------------3. Loop Finger Index[] -------------------------------//
            }
        }
        ds.Tables.Add(dt); //Datatable - EmpCode - Finger
        _funcScan.setEnableDevice(iMachineNumber, true);
        _funcScan.setMachineDisconnect();
        // --------------------------------------------2. End Loop Finger Ip Tranfer -------------------------------//

        ViewState["DataTranfer_Delete"] = ds;
    }

    protected void GetFingerScanToDatatable_Recheck()
    {
        string sFromIp = ddlfinger_center_check.SelectedValue; //IP Name
        string sFromIp_name = ddlfinger_center_check.SelectedItem.Text; //IP Name
        string ToIp = ViewState["finger_ip_tranfer"].ToString();
        string ip_EmpCode_tranfer = ViewState["finger_EmpCode_tranfer"].ToString();
        int iFromPort = 4370;
        //string[] _sToIp = ToIp.Split(',');
        string[] Toempcode = ip_EmpCode_tranfer.Split(',');
        string temp = "";

        #region ViewLogScanner

        DataSet ds = new DataSet();

        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("ip_finger", typeof(string)));
        dt.Columns.Add(new DataColumn("empcode", typeof(string)));
        dt.Columns.Add(new DataColumn("finger", typeof(string)));
        dt.Columns.Add(new DataColumn("finger_index", typeof(int)));
        dt.Columns.Add(new DataColumn("finger_status_tranfer_Succec", typeof(int)));
        dt.Columns.Add(new DataColumn("finger_status_database", typeof(int)));

        #endregion


        // --------------------------------------------1. Loop Finger Ip Center -------------------------------//
        if (_funcScan.setMachineConnect(sFromIp, iFromPort))
        {
            _funcScan.setEnableDevice(iMachineNumber, false);
            // --------------------------------------------2. Loop EmpCode Tranfer -------------------------------//
            foreach (string sdwEnrollNumber in Toempcode)
            {
                if (sdwEnrollNumber != String.Empty)
                {
                    // --------------------------------------------3. Loop Finger Index[] -------------------------------//
                    for (int iFromFingerIndex = 0; iFromFingerIndex <= 9; iFromFingerIndex++)
                    {
                        sTmpData = _funcScan.getUserTemplate(iMachineNumber, sdwEnrollNumber, iFromFingerIndex);// iFromFingerIndex);

                        if (sTmpData != "-2" && sTmpData != "0") // กรณี sTmpData Null จะมีค่าเป็น -2 และ 0
                        {
                            //temp += " , " + sdwEnrollNumber + " , " + sTmpData;

                            DataRow dr = dt.NewRow();
                            dr["ip_finger"] = sFromIp_name;
                            dr["empcode"] = sdwEnrollNumber;
                            dr["finger"] = sTmpData;
                            dr["finger_index"] = iFromFingerIndex;
                            dr["finger_status_tranfer_Succec"] = 0;
                            dr["finger_status_database"] = _funcScan.getMachineError().ToString();
                            dt.Rows.Add(dr);
                        }
                        else
                        {
                            break;
                        }
                    }
                    // --------------------------------------------3. Loop Finger Index[] -------------------------------//
                }
            }
            ds.Tables.Add(dt); //Datatable - EmpCode - Finger
            _funcScan.setEnableDevice(iMachineNumber, true);
            _funcScan.setMachineDisconnect();
            // --------------------------------------------2. End Loop Finger Ip Tranfer -------------------------------//
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Tranfer", "alert('" + "Tranfer Error" + "')", true);
        }
        // --------------------------------------------1. End Loop Finger Ip Center -------------------------------//

        ViewState["DataTranfer_Recheck"] = ds;

        //GvTemp.DataSource = (DataSet)ViewState["DataTranfer_Recheck"];
        //GvTemp.DataBind();


    }

    protected void GetFingerScanToDatatable_Report()
    {
        string sFromIp = ViewState["Finger_Center"].ToString();
        string ToIp = ViewState["finger_ip_tranfer"].ToString();
        string ip_EmpCode_tranfer = ViewState["finger_EmpCode_tranfer"].ToString();
        int iFromPort = 4370;
        string[] _sToIp = ToIp.Split(',');
        string[] Toempcode = ip_EmpCode_tranfer.Split(',');
        string temp = "";

        #region ViewLogScanner

        DataSet ds = new DataSet();

        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("empcode", typeof(string)));
        dt.Columns.Add(new DataColumn("finger", typeof(string)));
        dt.Columns.Add(new DataColumn("finger_index", typeof(int)));
        dt.Columns.Add(new DataColumn("finger_status_tranfer_Succec", typeof(int)));
        dt.Columns.Add(new DataColumn("ip_finger", typeof(string)));
        dt.Columns.Add(new DataColumn("finger_status_database", typeof(int)));

        #endregion

        // --------------------------------------------1. Loop Finger Ip Center -------------------------------//
        if (_funcScan.setMachineConnect(sFromIp, iFromPort))
        {
            _funcScan.setEnableDevice(iMachineNumber, false);

            // --------------------------------------------2. Loop EmpCode Tranfer -------------------------------//
            foreach (string sdwEnrollNumber in Toempcode)
            {
                if (sdwEnrollNumber != String.Empty)
                {
                    // --------------------------------------------3. Loop Finger Index[] -------------------------------//
                    for (int iFromFingerIndex = 0; iFromFingerIndex <= 9; iFromFingerIndex++)
                    {
                        sTmpData = _funcScan.getUserTemplate(iMachineNumber, sdwEnrollNumber, iFromFingerIndex);// iFromFingerIndex);

                        if (sTmpData != "-2" && sTmpData != "0") // กรณี sTmpData Null จะมีค่าเป็น -2 และ 0
                        {
                            temp += " , " + sdwEnrollNumber + " , " + sTmpData;

                            DataRow dr = dt.NewRow();
                            dr["empcode"] = sdwEnrollNumber;
                            dr["finger"] = sTmpData;
                            dr["finger_index"] = iFromFingerIndex;
                            dr["finger_status_tranfer_Succec"] = 0;
                            dr["ip_finger"] = sFromIp;
                            dr["finger_status_database"] = _funcScan.getMachineError().ToString();
                            dt.Rows.Add(dr);
                        }
                        else
                        {
                            break;
                        }
                    }
                    // --------------------------------------------3. Loop Finger Index[] -------------------------------//
                }
            }
            ds.Tables.Add(dt); //Datatable - EmpCode - Finger
            _funcScan.setEnableDevice(iMachineNumber, true);
            _funcScan.setMachineDisconnect();
            // --------------------------------------------2. End Loop Finger Ip Tranfer -------------------------------//
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Tranfer", "alert('" + "Tranfer Error" + "')", true);
        }
        // --------------------------------------------1. End Loop Finger Ip Center -------------------------------//

        ViewState["DataTranfer_Report"] = ds;

    }

    protected void GetDatatableTranferFinger()
    {
        string ToIp = ViewState["finger_ip_tranfer"].ToString();
        var dsTranfer = (DataSet)ViewState["DataTranfer"];
        int iToPort = 4370;
        string[] _sToIp = ToIp.Split(',');
        int i = 0;

        foreach (string sToIP in _sToIp)
        {
            if (sToIP != "" && iToPort != 0)
            {
                if (_funcScan.setMachineConnect(sToIP, iToPort))
                {
                    _funcScan.setEnableDevice(iMachineNumber, false);

                    foreach (DataRow dr in dsTranfer.Tables[0].Rows)
                    {
                        litResult.Text += _funcScan.setUserTemplate(iMachineNumber, dr["empcode"].ToString(), int.Parse(dr["finger_index"].ToString()), 1, dr["finger"].ToString()) + setNewline();

                        i++;
                    }

                    _funcScan.setEnableDevice(iMachineNumber, true);
                    _funcScan.setMachineDisconnect();
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Tranfer", "alert('" +"Tranfer Succes"+ "')", true);
                }
                else
                {
                    //litResult.Text += "unable to connect the device, error code = " + _funcScan.getMachineError().ToString() + setNewline();
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Tranfer", "alert('" + "unable to connect the device, error code" + "')", true);
                }
                
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Tranfer", "alert('" + "input error" + "')", true);
                //litResult.Text += "input error" + setNewline();
            }
        }

    }

    protected void GetDatatableTranferFinger_Report()
    {
        string ToIp = ViewState["finger_ip_tranfer"].ToString();
        var dsTranfer = (DataSet)ViewState["DataTranfer_Report"];
        int iToPort = 4370;
        string[] _sToIp = ToIp.Split(',');
        int i = 0;

        #region ViewLogScanner

        DataSet _ds = new DataSet();

        DataTable _dt = new DataTable();
        _dt.Columns.Add(new DataColumn("empcode", typeof(string)));
        _dt.Columns.Add(new DataColumn("finger", typeof(string)));
        _dt.Columns.Add(new DataColumn("finger_index", typeof(int)));
        _dt.Columns.Add(new DataColumn("finger_status_tranfer_Succec", typeof(int)));
        _dt.Columns.Add(new DataColumn("ip_finger", typeof(string)));
        _dt.Columns.Add(new DataColumn("finger_status_database", typeof(int)));

        #endregion

        foreach (string sToIP in _sToIp)
        {
            if (sToIP != "" && iToPort != 0)
            {
                if (_funcScan.setMachineConnect(sToIP, iToPort))
                {
                    _funcScan.setEnableDevice(iMachineNumber, false);

                    foreach (DataRow dr in dsTranfer.Tables[0].Rows)
                    {
                        litResult.Text += _funcScan.setUserTemplate(iMachineNumber, dr["empcode"].ToString(), int.Parse(dr["finger_index"].ToString()), 1, dr["finger"].ToString()) + setNewline();

                        DataRow _dr = _dt.NewRow();
                        _dr["empcode"] = dr["empcode"].ToString();
                        _dr["finger"] = dr["finger"].ToString();
                        _dr["finger_index"] = int.Parse(dr["finger_index"].ToString());
                        _dr["finger_status_tranfer_Succec"] = 0;
                        _dr["ip_finger"] = sToIP;
                        _dr["finger_status_database"] = _funcScan.getMachineError().ToString();
                        _dt.Rows.Add(_dr);

                        i++;
                    }

                    _funcScan.setEnableDevice(iMachineNumber, true);
                    _funcScan.setMachineDisconnect();
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Tranfer", "alert('" + "Tranfer Succes" + "')", true);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Tranfer", "alert('" + "unable to connect the device, error code" + "')", true);
                }

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Tranfer", "alert('" + "input error" + "')", true);
            }
        }

        _ds.Tables.Add(_dt); //Datatable - EmpCode - Finger
        ViewState["DataTranfer_Report"] = _ds;

    }

    protected void GetDatatableDeleteFinger()
    {
        string ToIp = ViewState["finger_ip_tranfer"].ToString();
        var _dsTranfer = (DataSet)ViewState["DataTranfer_Delete"];
        int iToPort = 4370;
        string[] _sToIp = ToIp.Split(',');
        int i = 0;
        string _temp_1 = "";
        string _temp_2 = "";

        foreach (string sToIP in _sToIp)
        {
            if (sToIP != "" && iToPort != 0)
            {
                if (_funcScan.setMachineConnect(sToIP, iToPort))
                {
                    _funcScan.setEnableDevice(iMachineNumber, false);

                    foreach (DataRow _dr in _dsTranfer.Tables[0].Rows)
                    {
                        _temp_1 = _dr["empcode"].ToString();

                        if (_temp_1 != _temp_2)
                        {
                            litResult.Text += _funcScan.delUserTemplate(iMachineNumber, _dr["empcode"].ToString(), int.Parse(_dr["finger_index"].ToString())) + setNewline();
                            _temp_2 = _dr["empcode"].ToString();
                            i++;
                        }
                    }

                    _funcScan.setEnableDevice(iMachineNumber, true);
                    _funcScan.setMachineDisconnect();
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Tranfer", "alert('" + "Tranfer-Delete Succes" + "')", true);
                }
                else
                {
                    //litResult.Text += "unable to connect the device, error code = " + _funcScan.getMachineError().ToString() + setNewline();
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Tranfer", "alert('" + "unable to connect the device, error code" + "')", true);
                }
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Tranfer", "alert('" + "input error" + "')", true);
                //litResult.Text += "input error" + setNewline();
            }
        }

    }

    protected void ddlOrg()
    {
        ddlOrganization.Items.Clear();
        ddlOrganization.AppendDataBoundItems = true;
        ddlOrganization.Items.Add(new ListItem("Select Organization....", "0"));

        var Mac_Master = new Relation_ODSP[1];
        Mac_Master[0] = new Relation_ODSP();

        dataODSP_Relation Box_EN_Master = new dataODSP_Relation();
        Box_EN_Master.detailRelation_ODSP = Mac_Master;
        var xmlData = _funcTool.convertObjectToXml(Box_EN_Master);
        var BoxXML = _funcdb.execSPXml_old(ConCL, ODSP_Reletion, xmlData, 200);
        //fs.Text = HttpUtility.HtmlEncode(xmlData);
        Box_EN_Master = (dataODSP_Relation)_funcTool.convertXmlToObject(typeof(dataODSP_Relation), BoxXML);

        ddlOrganization.DataSource = Box_EN_Master.detailRelation_ODSP;
        ddlOrganization.DataTextField = "OrgNameTH";
        ddlOrganization.DataValueField = "OrgIDX";
        ddlOrganization.DataBind();

        ddlDepartment.Items.Clear();
        ddlDepartment.Items.Insert(0, new ListItem("Select Department....", "0"));
        ddlDepartment.SelectedValue = "0";

        ddlSection.Items.Clear();
        ddlSection.Items.Insert(0, new ListItem("Select Section....", "0"));
        ddlSection.SelectedValue = "0";
    }

    #endregion


    #region Checkbox เชคแสดง
    protected void Checkbox(object sender, EventArgs e)
    {
        var cb = (CheckBox)sender;
        switch (cb.ID)
        {

            case "CheckRow":

                foreach (GridViewRow row in GvEmpDatabase.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        //CheckBox chkRow1 = (CheckBox)row.Cells[0].FindControl("CheckRow");

                        CheckBox chk = new CheckBox();

                        chk = (CheckBox)(row.FindControl("CheckRow"));

                        if (chk.Checked == true)
                        {



                            Label lbfm_empcode = (Label)GvEmpDatabase.FindControl("lbfm_empcode");
                            Label lbfm_name = (Label)GvEmpDatabase.FindControl("lbfm_name");

                            //fs.Text = lbfm_empcode.ToString();
                            fs.Text = lbfm_empcode.ToString();//row.Cells[1].Text;

                            /*var dsEmp = (DataSet)ViewState["vsTempEmp"];
                            var drEmp = dsEmp.Tables[0].NewRow();

                            drEmp["EmpCode"] = lbfm_empcode;
                            drEmp["FullNameTH"] = lbfm_name;

                            dsEmp.Tables[0].Rows.Add(drEmp);
                            ViewState["vsTempEmp"] = dsEmp;*/

                        }
                    }
                }

                setGridData(GvEmpTemp, ViewState["vsTempEmp"]);

                /*#region ViewAddEmp

                var dsEmp = new DataSet();
                dsEmp.Tables.Add("TempEmp");

                dsEmp.Tables[0].Columns.Add("EmpCode", typeof(string));
                dsEmp.Tables[0].Columns.Add("FullNameTH", typeof(string));

                ViewState["vsTempEmp"] = dsEmp;

                #endregion*/

                /*string[] arg = new string[2];
                arg = e.CommandArgument.ToString().Split(';');
                int ResID = int.Parse(arg[0]);
                string IDRow = arg[1];

                var aaw = (DataReservation)ViewState["GvCreateApprove"];
                int i = 0;
                foreach (var Name in aaw.ReservationViewDetail)
                {

                    if (Name.ResIDX == ResID)
                    {
                        aaw.ReservationViewDetail[i].STAction = int.Parse("20");
                    }

                    i++;
                }

                ViewState["PageCheckCreate"] = 1;
                if (int.Parse(ViewState["RowUpdate"].ToString()) == 1)
                {
                    Box_CreatePO_ViewStateNo_Search();
                }
                else if (int.Parse(ViewState["RowUpdate"].ToString()) == 2)
                {
                    if (int.Parse(ViewState["ReturnCodeCreatePO"].ToString()) == 0)
                    {
                        Box_Search_Createapprove_Database();
                    }
                    else
                    {
                        var aaw5 = (DataReservation)ViewState["GvCreateApprove_Search"];
                        int i11 = 0;
                        foreach (var Name1 in aaw5.ReservationViewDetail)
                        {
                            if (Name1.ResIDX == ResID)
                            {
                                aaw5.ReservationViewDetail[i11].STAction = int.Parse("20");
                            }

                            i11++;
                        }

                        Box_Search_Createapprove_Database();
                    }
                }*/

                //if (ViewState["EmpDatabase"] != null)
                //{
                /*u0_finger_detail[] _tempList = (u0_finger_detail[])ViewState["EmpDatabase"];
                int _checked = int.Parse(cb.Text);
                _tempList[_checked].checkbox_row = cb.Checked;*/
                //}

                break;
        }
    }
    #endregion


    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (DropDownList)sender;

        switch (ddName.ID)
        {


            case "ddlOrganization":


                if (ddlOrganization.SelectedValue == "0")
                {

                    ddlOrganization.AppendDataBoundItems = true;
                    ddlOrganization.Items.Clear();
                    ddlOrganization.Items.Insert(0, new ListItem("Select Organization....", "0"));

                    var Mac_Master = new Relation_ODSP[1];
                    Mac_Master[0] = new Relation_ODSP();

                    dataODSP_Relation Box_EN_Master = new dataODSP_Relation();
                    Box_EN_Master.detailRelation_ODSP = Mac_Master;
                    var xmlData = _funcTool.convertObjectToXml(Box_EN_Master);
                    var BoxXML = _funcdb.execSPXml_old(ConCL, ODSP_Reletion, xmlData, 200);
                    Box_EN_Master = (dataODSP_Relation)_funcTool.convertXmlToObject(typeof(dataODSP_Relation), BoxXML);

                    ddlOrganization.DataSource = Box_EN_Master.detailRelation_ODSP;
                    ddlOrganization.DataTextField = "OrgNameTH";
                    ddlOrganization.DataValueField = "OrgIDX";
                    ddlOrganization.DataBind();
                    ddlOrganization.SelectedValue = "0";

                    ddlDepartment.Items.Clear();
                    ddlDepartment.Items.Insert(0, new ListItem("Select Department....", "0"));
                    ddlDepartment.SelectedValue = "0";


                    ddlSection.Items.Clear();
                    ddlSection.Items.Insert(0, new ListItem("Select Section....", "0"));
                    ddlSection.SelectedValue = "0";

                }
                else
                {
                    ddlDepartment.AppendDataBoundItems = true;
                    ddlDepartment.Items.Clear();
                    ddlDepartment.Items.Add(new ListItem("Select Department....", "0"));

                    var Mac_Master = new Relation_ODSP[1];
                    Mac_Master[0] = new Relation_ODSP();

                    Mac_Master[0].OrgIDX = int.Parse(ddlOrganization.SelectedValue);

                    dataODSP_Relation Box_EN_Master = new dataODSP_Relation();
                    Box_EN_Master.detailRelation_ODSP = Mac_Master;
                    var xmlData = _funcTool.convertObjectToXml(Box_EN_Master);
                    var BoxXML = _funcdb.execSPXml_old(ConCL, ODSP_Reletion, xmlData, 200);
                    Box_EN_Master = (dataODSP_Relation)_funcTool.convertXmlToObject(typeof(dataODSP_Relation), BoxXML);

                    ddlDepartment.DataSource = Box_EN_Master.detailRelation_ODSP;
                    ddlDepartment.DataTextField = "DeptNameTH";
                    ddlDepartment.DataValueField = "RDeptIDX";
                    ddlDepartment.DataBind();

                }

                break;

            case "ddlDepartment":

                if (ddlDepartment.SelectedValue == "0")
                {
                    ddlDepartment.AppendDataBoundItems = true;
                    ddlDepartment.Items.Clear();
                    ddlDepartment.Items.Insert(0, new ListItem("Select Department....", "0"));

                    var Mac_Master = new Relation_ODSP[1];
                    Mac_Master[0] = new Relation_ODSP();

                    Mac_Master[0].OrgIDX = int.Parse(ddlOrganization.SelectedValue);
                    Mac_Master[0].RDeptIDX = int.Parse(ddlDepartment.SelectedValue);

                    dataODSP_Relation Box_EN_Master = new dataODSP_Relation();
                    Box_EN_Master.detailRelation_ODSP = Mac_Master;
                    var xmlData = _funcTool.convertObjectToXml(Box_EN_Master);
                    var BoxXML = _funcdb.execSPXml_old(ConCL, ODSP_Reletion, xmlData, 200);
                    Box_EN_Master = (dataODSP_Relation)_funcTool.convertXmlToObject(typeof(dataODSP_Relation), BoxXML);

                    ddlDepartment.DataSource = Box_EN_Master.detailRelation_ODSP;
                    ddlDepartment.DataTextField = "DeptNameTH";
                    ddlDepartment.DataValueField = "RDeptIDX";
                    ddlDepartment.DataBind();
                    ddlDepartment.SelectedValue = "0";

                    ddlSection.Items.Clear();
                    ddlSection.Items.Insert(0, new ListItem("Select Section....", "0"));
                    ddlSection.SelectedValue = "0";

                }
                else
                {
                    ddlSection.AppendDataBoundItems = true;
                    ddlSection.Items.Clear();
                    ddlSection.Items.Add(new ListItem("Select Section....", "0"));

                    var Mac_Master = new Relation_ODSP[1];
                    Mac_Master[0] = new Relation_ODSP();

                    Mac_Master[0].OrgIDX = int.Parse(ddlOrganization.SelectedValue);
                    Mac_Master[0].RDeptIDX = int.Parse(ddlDepartment.SelectedValue);

                    dataODSP_Relation Box_EN_Master = new dataODSP_Relation();
                    Box_EN_Master.detailRelation_ODSP = Mac_Master;
                    var xmlData = _funcTool.convertObjectToXml(Box_EN_Master);
                    var BoxXML = _funcdb.execSPXml_old(ConCL, ODSP_Reletion, xmlData, 200);
                    Box_EN_Master = (dataODSP_Relation)_funcTool.convertXmlToObject(typeof(dataODSP_Relation), BoxXML);

                    ddlSection.DataSource = Box_EN_Master.detailRelation_ODSP;
                    ddlSection.DataTextField = "SecNameTH";
                    ddlSection.DataValueField = "RSecIDX";
                    ddlSection.DataBind();

                }

                break;

            case "ddlTranfer_Type":

                int type_Tranfer = 0;
                type_Tranfer = int.Parse(ddlTranfer_Type.SelectedValue);

                if (type_Tranfer == 1) //โอนย้าย
                {
                    Admin_FingerCenter.Visible = true;
                    lbltranfer.Visible = true;
                    lbltranfer_delete.Visible = false;
                }
                else if(type_Tranfer == 2) //ลบ
                {
                    Admin_FingerCenter.Visible = false;
                    lbltranfer.Visible = false;
                    lbltranfer_delete.Visible = true;
                }
                else
                {
                    Admin_FingerCenter.Visible = false;
                    lbltranfer.Visible = false;
                    lbltranfer_delete.Visible = false;
                }


                break;
        }
    }

    #endregion

}