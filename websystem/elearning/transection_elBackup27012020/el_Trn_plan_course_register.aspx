﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="el_Trn_plan_course_register.aspx.cs" Inherits="websystem_el_Trn_plan_course_register" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">



    <asp:HyperLink ID="SETFOCUS" runat="server"></asp:HyperLink>
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    <asp:Panel ID="Panel3" runat="server" CssClass="m-t-10">
        <asp:Panel ID="panelMenu" runat="server">
            <nav class="navbar navbar-default">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <%-- <a class="navbar-brand">Menu</a>--%>
                </div>
                <%--<asp:UpdatePanel ID="updatepanel4" runat="server">
                    <ContentTemplate>--%>
                <div class="collapse navbar-collapse p-l-10" style="overflow-x: hidden;" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-left" runat="server">
                        <li id="liListData" runat="server" visible="false">
                            <asp:LinkButton ID="btnListData" runat="server"
                                OnCommand="btnCommand" CommandName="btnListData" Text="ข้อมูลทั่วไป" />
                        </li>
                        <li id="liInsert" runat="server" visible="false">
                            <asp:LinkButton ID="btnInsert" runat="server"
                                OnCommand="btnCommand" CommandName="btnInsert" Text="คอร์สอบรม" />
                        </li>

                        <li id="lischeduler" runat="server" visible="true">
                            <asp:Label ID="lbHR_scheduler" runat="server" Text="ตารางแผนการอบรม" Visible="false"></asp:Label>
                            <asp:LinkButton ID="btnscheduler" runat="server"
                                OnCommand="btnCommand" CommandName="btnscheduler" Text="ตารางแผนการอบรม" />
                        </li>


                        <li id="liHR_Wait" runat="server" visible="false">
                            <asp:Label ID="lbHR_Wait" runat="server" Text="รายการที่รอลงทะเบียน" Visible="false"></asp:Label>
                            <asp:LinkButton ID="btnHR_Wait" runat="server"
                                OnCommand="btnCommand" CommandName="btnHR_Wait" Text="รายการที่รอลงทะเบียน" />
                        </li>

                        <li id="liMD_Wait" runat="server" visible="false">
                            <asp:Label ID="lbMD_Wait" runat="server" Text="รายการที่รอ MD อนุมัติ" Visible="false"></asp:Label>
                            <asp:LinkButton ID="btnMD_Wait" runat="server"
                                OnCommand="btnCommand" CommandName="btnMD_Wait" Text="รายการที่รอ MD อนุมัติ" />
                        </li>

                        <li id="linoregister" runat="server" visible="false">
                            <asp:LinkButton ID="btnnoregister" runat="server"
                                OnCommand="btnCommand" CommandName="btnnoregister" Text="รายการที่ลงทะเบียนไม่ทันเวลา" />
                        </li>
                        <li id="liopen_course" runat="server">
                            <asp:LinkButton ID="btnopen_course" runat="server"
                                OnCommand="btnCommand" CommandName="btnopen_course" Text="คอร์สอบรมที่เปิด" />
                        </li>
                        <li id="liHR_App" runat="server" visible="true">
                            <asp:Label ID="lbHR_App" runat="server" Text="คอร์สอบรมที่มีสิทธิ์เรียน" Visible="false"></asp:Label>
                            <asp:LinkButton ID="btnHR_App" runat="server"
                                OnCommand="btnCommand" CommandName="btnHR_App" Text="คอร์สอบรมที่มีสิทธิ์เรียน" />
                        </li>
                        <li id="lisummary_course" runat="server" visible="false">
                            <asp:Label ID="lbsummary_course" runat="server" Text="ผลการฝึกอบรม" Visible="false"></asp:Label>
                            <asp:LinkButton ID="btnsummary_course" runat="server"
                                OnCommand="btnCommand" CommandName="btnsummary_course" Text="ผลการฝึกอบรม" />
                        </li>

                        <li id="liMD_App" runat="server" visible="true">
                            <asp:Label ID="lbMD_App" runat="server" Text="ผลการฝึกอบรม" Visible="false"></asp:Label>
                            <asp:LinkButton ID="btnMD_App" runat="server"
                                OnCommand="btnCommand" CommandName="btnMD_App" Text="ผลการฝึกอบรม" />
                        </li>

                        <li id="lisyllabus" runat="server" visible="false">
                            <asp:LinkButton ID="btnsyllabus" runat="server"
                                OnCommand="btnCommand" CommandName="btnsyllabus" Text="หลักสูตรที่จะต้องเรียน" />
                        </li>

                        <%-- <li id="li_elearning" runat="server" visible="true">
                            <asp:Label ID="lb_elearning" runat="server" Text="E-Learning" Visible="false"></asp:Label>
                            <asp:LinkButton ID="_divbtn_elearning" runat="server"
                                OnCommand="btnCommand" CommandName="_divbtn_elearning" Text="E-Learning" />
                        </li>--%>
                    </ul>
                </div>
                <%--</ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnListData" />
                        <asp:PostBackTrigger ControlID="btnInsert" />
                        <asp:PostBackTrigger ControlID="btnscheduler" />
                        <asp:PostBackTrigger ControlID="btnHR_Wait" />
                        <asp:PostBackTrigger ControlID="btnMD_Wait" />
                        <asp:PostBackTrigger ControlID="btnnoregister" />
                        <asp:PostBackTrigger ControlID="btnopen_course" />
                        <asp:PostBackTrigger ControlID="btnHR_App" />
                        <asp:PostBackTrigger ControlID="btnsummary_course" />
                        <asp:PostBackTrigger ControlID="btnMD_App" />
                        <asp:PostBackTrigger ControlID="btnsyllabus" />
                    </Triggers>
                </asp:UpdatePanel>--%>
            </nav>

            <div class="clearfix"></div>

        </asp:Panel>

    </asp:Panel>

    <div class="form-horizontal" role="form">

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ข้อมูลส่วนตัว</strong></h3>
            </div>
            <div class="panel-body">

                <asp:FormView ID="FvDetailUser" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                    <ItemTemplate>
                        <%-- <div class="form-horizontal" role="form">--%>

                        <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                        <div class="form-group">
                            <div class="col-md-12">
                                <asp:Label ID="Label7" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="รหัสผู้ทำรายการ : " />
                                <div class="col-md-3">
                                    <asp:TextBox ID="txtEmpCode" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("EmpCode") %>' />
                                    <asp:Label ID="LbEmpIDX" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("EmpIDX")%>' />

                                </div>
                                <asp:Label ID="Label9" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="ชื่อผู้ทำรายการ : " />
                                <div class="col-md-4">
                                    <asp:TextBox ID="txtEmpName" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("EmpName") %>' />
                                </div>

                                <%--<asp:Label ID="lbdate" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="วันที่สร้างรายการ : " />
                                <div class="col-md-4">
                                    <div class='input-group date'>
                                        <asp:TextBox ID="txtu0_training_req_date" runat="server" CssClass="form-control filter-order-from" />
                                        <span class="input-group-addon show-order-sale-log-from-onclick">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>--%>
                            </div>
                        </div>


                        <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                        <div class="form-group">
                            <div class="col-md-12">
                                <%--<asp:Label ID="Label4" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="บริษัท : " />
                                    <div class="col-md-3">
                                        <asp:TextBox ID="txtOrgNameTH" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("OrgNameTH")%>' />
                                        <asp:Label ID="LbOrgIDX" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("OrgIDX")%>' />
                                    </div>--%>
                                <asp:Label ID="Labedl1" class="col-md-2 control-labelnotop text_right" runat="server" Text="แผนก : " />
                                <div class="col-md-3">
                                    <asp:TextBox ID="txtSecNameTH" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("SecNameTH") %>' />
                                    <asp:Label ID="LbSecIDX" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("RSecID")%>' />

                                </div>

                                <asp:Label ID="Label34" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="ฝ่าย : " />
                                <div class="col-md-4">
                                    <asp:TextBox ID="txtDeptNameTH" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("RDeptName")%>' />
                                    <asp:Label ID="LbRDeptIDX" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("RDeptID")%>' />
                                </div>
                            </div>
                        </div>


                        <%-------------- แผนก,ตำแหน่ง --------------%>
                        <div class="form-group">
                            <div class="col-md-12">
                                <%--<asp:Label ID="Labedl1" class="col-md-2 control-labelnotop text_right" runat="server" Text="แผนก : " />
                                    <div class="col-md-3">
                                        <asp:TextBox ID="txtSecNameTH" Enabled="false" CssClass="form-control"  runat="server" Text='<%# Eval("SecNameTH") %>' />
                                        <asp:Label ID="LbSecIDX" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("RSecID")%>' />

                                    </div>--%>

                                <asp:Label ID="Label8" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="ตำแหน่ง : " />
                                <div class="col-md-3">
                                    <asp:TextBox ID="txtPosNameTH" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("PosNameTH")%>' />
                                    <asp:Label ID="LbPosIDX" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("RPosIDX_J")%>' />
                                </div>

                                <asp:Label ID="Label6" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="E-mail : " />
                                <div class="col-md-4">
                                    <asp:TextBox ID="txtEmail" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("Email") %>' />

                                </div>
                            </div>
                        </div>

                        <%-------------- เบอร์ติดต่อ,E-mail --------------%>
                        <%-- <div class="form-group">
                                <div class="col-md-12">

                                    <asp:Label ID="Label5" class="col-md-2 control-labelnotop text_right" runat="server" Text="เบอร์ติดต่อ : " />
                                    <div class="col-md-3">
                                        <asp:TextBox ID="txtMobileNo"  Enabled="false" CssClass="form-control"   runat="server" Text='<%# Eval("MobileNo") %>' />
                                    </div>

                                    
                                </div>
                            </div>--%>


                        <%-------------- Cost Center --------------%>
                        <div class="form-group">
                            <div class="col-md-12">

                                <asp:Label ID="Label20" class="col-md-2 control-labelnotop text_right" runat="server" Text="Cost Center : " />
                                <div class="col-md-3">
                                    <asp:TextBox ID="txtCostNo" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("CostNo")%>' />
                                    <asp:Label ID="LoCostIDX" runat="server" Visible="false" CssClass="control-label" Text='<%# Eval("CostIDX")%>' />
                                </div>
                                <asp:Label ID="Label5" class="col-md-2 control-labelnotop text_right" runat="server" Text="เบอร์ติดต่อ : " />
                                <div class="col-md-4">
                                    <asp:TextBox ID="txtMobileNo" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("MobileNo") %>' />
                                </div>
                            </div>
                        </div>

                        <%-- </div>--%>
                    </ItemTemplate>
                </asp:FormView>

            </div>
        </div>
    </div>

    <asp:MultiView ID="MultiViewBody" runat="server" ActiveViewIndex="0">

        <asp:View ID="View_HR_WList" runat="server">

            <asp:Panel ID="Panel1" runat="server" Visible="true">
                <div class="panel panel-primary m-t-10">
                    <div class="panel-heading f-bold">ค้นหา</div>
                    <div class="panel-body">

                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>ปี</label>
                                <asp:DropDownList ID="ddlYearSearch_HR_W" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>เลขที่เอกสาร / ชื่อหลักสูตร</label>
                                <asp:TextBox ID="txtFilterKeyword_HR_W" runat="server"
                                    CssClass="form-control"
                                    placeholder="เลขที่เอกสาร / ชื่อหลักสูตร..." />
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <label>
                                &nbsp;
                            </label>
                            <div class="form-group">
                                <asp:LinkButton ID="btnS_HR_W" runat="server" CssClass="btn btn-primary" Text="<i class='fa fa-search'></i> ค้นหา"
                                    OnCommand="btnCommand" CommandName="btnS_HR_W" />
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <div class="row">

                <asp:GridView ID="GvHR_WList"
                    runat="server"
                    AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered table-responsive col-md-12 word-wrap"
                    HeaderStyle-CssClass="info"
                    AllowPaging="true"
                    PageSize="10"
                    OnRowCommand="onRowCommand"
                    OnRowDataBound="onRowDataBound"
                    OnPageIndexChanging="gvPageIndexChanging"
                    DataKeyNames="u0_training_course_idx"
                    ShowFooter="false"
                    Font-Size="Small">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />

                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่พบข้อมูล</div>
                    </EmptyDataTemplate>
                    <Columns>


                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                            <ItemTemplate>
                                <div class="word-wrap">
                                    <%# (Container.DataItemIndex + 1) %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="เลขที่เอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                            <ItemTemplate>
                                <div class="word-wrap">
                                    <%# Eval("training_course_no") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="วันที่สร้างเอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                            <ItemTemplate>
                                <div class="word-wrap">
                                    <%# Eval("zdate") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ประเภทหลักสูตร" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="3%">
                            <ItemTemplate>
                                <div class="word-wrap">
                                    <%--<%# course_plan_status_name((string)Eval("course_plan_status")) %>--%>
                                    <img src='<%# getImageIO((string)Eval("course_plan_status"),1) %>'
                                        width="50" height="50"
                                        title='<%# getImageIO((string)Eval("course_plan_status"),2) %>'
                                        id="imgicon_inplan" runat="server" visible="true" />
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="รายละเอียดหลักสูตร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="35%">
                            <ItemTemplate>
                                <div class="word-wrap">
                                    <strong>
                                        <asp:Label ID="Label60" runat="server"> หลักสูตร: </asp:Label></strong>
                                    <asp:Literal ID="Literal6" runat="server" Text='<%# Eval("course_name") %>' />
                                    <br />
                                    <strong>
                                        <asp:Label ID="Label61" runat="server"> กลุ่มวิชา: </asp:Label></strong>
                                    <asp:Literal ID="Literal8" runat="server" Text='<%# Eval("training_group_name") %>' />
                                    <br />
                                    <strong>
                                        <asp:Label ID="Label22" runat="server"> สาขาวิชา: </asp:Label></strong>
                                    <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("training_branch_name") %>' />
                                    <br />
                                    <strong>
                                        <asp:Label ID="Label11" runat="server"> ประเภท: </asp:Label></strong>
                                    <asp:Literal ID="Literal4" runat="server" Text='<%# Eval("zcourse_type_etraining_name") %>' />

                                    <br />
                                    <strong>
                                        <asp:Label ID="Label13" runat="server"> สถานที่อบรม: </asp:Label></strong>
                                    <asp:Literal ID="Literal5" runat="server" Text='<%# getplace_type((int)Eval("place_type")) %>' />

                                    <br>
                                    <table class="table table-striped f-s-12 table-empshift-responsive">
                                        <asp:Repeater ID="rptu8trncoursedate" runat="server">
                                            <HeaderTemplate>
                                                <tr>
                                                    <th>วันที่อบรม</th>
                                                    <th>เวลาที่เริ่ม-เวลาที่สิ้นสุด</th>
                                                    <th>รวมชม.ที่เรียน</th>
                                                </tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%# Eval("zdate") %></td>
                                                    <td><%# Eval("ztime_start")+" - "+Eval("ztime_end") %></td>
                                                    <td align="center"><%# string.Format("{0:n2}",Eval("training_course_date_qty")) %></td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </table>
                                    <%# getstar_level((int)Eval("zcount_Lv")) %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ต้องผ่านการเรียนหลักสูตร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                            <ItemTemplate>
                                <table class="table table-striped f-s-12 table-empshift-responsive">
                                    <asp:Repeater ID="rptcourse" runat="server">
                                        <HeaderTemplate>
                                            <tr>
                                                <th>หลักสูตร</th>
                                                <th>สถานะ</th>
                                            </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Eval("zName") %></td>
                                                <td align="center"><%# getStatusLevel((int)Eval("score")) %></td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                            <ItemTemplate>
                               
                                 <span>

                                     <%# getTextDoc((int)Eval("register_status"),(string)Eval("register_date"),
                                                   (int)Eval("EmpIDX"),0 ,
                                                   (int)Eval("signup_status"),(string)Eval("signup_date"),
                                                   (int)Eval("test_scores_status"),(string)Eval("test_scores_date"),
                                                   (int)Eval("summ_std_status"),(string)Eval("summ_std_date"),
                                                   (int)Eval("summ_super_status"),(string)Eval("summ_super_date"),
                                                   (string)Eval("zregister_date"),
                                                   (int)Eval("zresulte_app_status"),
                                                   (int)Eval("zresulte_count"),
                                                   (int)Eval("zcourse_count"),
                                                   (int)Eval("course_type_elearning"),
                                                   (int)Eval("course_type_etraining")
                                                   ) %>

                                 </span>

                                <br />
                                <asp:Panel runat="server" ID="pnlicon_yes">
                                    <center>
                                         <img src='<%# ResolveUrl("~/images/elearning/icon_yes.png") %>' width="30" height="30" title="ผ่าน" id="imgicon_yes" runat="server" visible="true" />
                                    </center>
                                </asp:Panel>
                                <asp:Panel runat="server" ID="pnlicon_no">
                                    <center>
                                         <img src='<%# ResolveUrl("~/images/elearning/icon_no.png") %>' width="30" height="30" title="ไม่ผ่าน" id="imgicon_no" runat="server" visible="true" />
                                    </center>
                                </asp:Panel>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                            <ItemTemplate>
                                <div class="word-wrap">

                                    <asp:Label ID="lbu0_training_course_idx" runat="server"
                                        Visible="false" Text='<%# Eval("u0_training_course_idx") %>' />

                                    <asp:Label ID="lbu0_course_idx" runat="server"
                                        Visible="false" Text='<%# Eval("u0_course_idx") %>' />

                                    <asp:Label ID="lbzcourse_count" runat="server"
                                        Visible="false" Text='<%# Eval("zcourse_count") %>' />

                                    <asp:Label ID="lbgrade_status" runat="server"
                                        Visible="false" Text='<%# Eval("grade_status") %>' />

                                     <asp:Label ID="lbsumm_super_status" runat="server"
                                        Visible="false" Text='<%# Eval("summ_super_status") %>' />


                                    <asp:LinkButton ID="btnDetail_GvHR_WList"
                                        CssClass="btn btn-info btn-sm" runat="server"
                                        data-original-title="รายละเอียด" data-toggle="tooltip" Text="รายละเอียด"
                                        OnCommand="btnCommand" CommandName="btnDetail_GvHR_WList"
                                        CommandArgument='<%# 
                                        Convert.ToString(Eval("u0_training_course_idx")) + "|" +  
                                        Convert.ToString(Eval("EmpIDX"))+ "|" +  
                                        Convert.ToString(Eval("zresulte_app_status"))+ "|" +  
                                        Convert.ToString(Eval("zresulte_count"))
                                        %>'>
                                    <i class="fa fa-file-alt"></i></asp:LinkButton>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>


                    </Columns>
                </asp:GridView>

            </div>

        </asp:View>



        <asp:View ID="View_HR_AList" runat="server">

            <asp:Panel ID="Panel8" runat="server" Visible="true">
                <div class="panel panel-primary m-t-10">
                    <div class="panel-heading f-bold">ค้นหา</div>
                    <div class="panel-body">

                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>ปี</label>
                                <asp:DropDownList ID="ddlYearSearch_HR_A" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>เลขที่เอกสาร / ชื่อหลักสูตร</label>
                                <asp:TextBox ID="txtFilterKeyword_HR_A" runat="server"
                                    CssClass="form-control"
                                    placeholder="เลขที่เอกสาร / ชื่อหลักสูตร..." />
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <label>
                                &nbsp;
                            </label>
                            <div class="form-group">
                                <asp:LinkButton ID="btnS_HR_A" runat="server" CssClass="btn btn-primary" Text="<i class='fa fa-search'></i> ค้นหา"
                                    OnCommand="btnCommand" CommandName="btnS_HR_A" />
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <div class="row">

                <asp:GridView ID="GvHR_AList"
                    runat="server"
                    AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered table-responsive col-md-12 word-wrap"
                    HeaderStyle-CssClass="info"
                    AllowPaging="true"
                    PageSize="10"
                    OnRowCommand="onRowCommand"
                    OnRowDataBound="onRowDataBound"
                    OnPageIndexChanging="gvPageIndexChanging"
                    DataKeyNames="u0_training_course_idx"
                    ShowFooter="false"
                    Font-Size="Small">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />

                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่พบข้อมูล</div>
                    </EmptyDataTemplate>
                    <Columns>


                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                            <ItemTemplate>
                                <div class="word-wrap">
                                    <%# (Container.DataItemIndex + 1) %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="เลขที่เอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                            <ItemTemplate>
                                <div class="word-wrap">
                                    <%# Eval("training_course_no") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="วันที่สร้างเอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                            <ItemTemplate>
                                <div class="word-wrap">
                                    <%# Eval("zdate") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ประเภทหลักสูตร" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="3%">
                            <ItemTemplate>
                                <div class="word-wrap">
                                    <%--<%# course_plan_status_name((string)Eval("course_plan_status")) %>--%>
                                    <img src='<%# getImageIO((string)Eval("course_plan_status"),1) %>'
                                        width="50" height="50"
                                        title='<%# getImageIO((string)Eval("course_plan_status"),2) %>'
                                        id="imgicon_inplan" runat="server" visible="true" />
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="รายละเอียดหลักสูตร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="35%">
                            <ItemTemplate>
                                <div class="word-wrap">
                                    <strong>
                                        <asp:Label ID="Label60" runat="server"> หลักสูตร: </asp:Label></strong>
                                    <asp:Literal ID="Literal6" runat="server" Text='<%# Eval("course_name") %>' />
                                    <br />
                                    <strong>
                                        <asp:Label ID="Label61" runat="server"> กลุ่มวิชา: </asp:Label></strong>
                                    <asp:Literal ID="Literal8" runat="server" Text='<%# Eval("training_group_name") %>' />
                                    <br />
                                    <strong>
                                        <asp:Label ID="Label22" runat="server"> สาขาวิชา: </asp:Label></strong>
                                    <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("training_branch_name") %>' />
                                    <br />
                                    <strong>
                                        <asp:Label ID="Label11" runat="server"> ประเภท: </asp:Label></strong>
                                    <asp:Literal ID="Literal4" runat="server" Text='<%# Eval("zcourse_type_etraining_name") %>' />
                                     <br />
                                    <strong>
                                        <asp:Label ID="Label13" runat="server"> สถานที่อบรม: </asp:Label></strong>
                                    <asp:Literal ID="Literal5" runat="server" Text='<%# getplace_type((int)Eval("place_type")) %>' />

                                    <%--   
                                    <br />
                                    <%# Eval("u0_training_course_idx") %>
                                    <%# Eval("place_type") %>
                                     
                                    <br />--%>

                                    <br>
                                    <table class="table table-striped f-s-12 table-empshift-responsive">
                                        <asp:Repeater ID="rptu8trncoursedate" runat="server">
                                            <HeaderTemplate>
                                                <tr>
                                                    <th>วันที่อบรม</th>
                                                    <th>เวลาที่เริ่ม-เวลาที่สิ้นสุด</th>
                                                    <th>รวมชม.ที่เรียน</th>
                                                </tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%# Eval("zdate") %></td>
                                                    <td><%# Eval("ztime_start")+" - "+Eval("ztime_end") %></td>
                                                    <td align="center"><%# string.Format("{0:n2}",Eval("training_course_date_qty")) %></td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </table>

                                    <%# getstar_level((int)Eval("zcount_Lv")) %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ต้องผ่านการเรียนหลักสูตร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                            <ItemTemplate>
                                <table class="table table-striped f-s-12 table-empshift-responsive">
                                    <asp:Repeater ID="rptcourse" runat="server">
                                        <HeaderTemplate>
                                            <tr>
                                                <th>หลักสูตร</th>
                                                <th>สถานะ</th>
                                            </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Eval("zName") %></td>
                                                <td align="center"><%# getStatusLevel((int)Eval("score")) %></td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%">
                            <ItemTemplate>

                                <span>
                                    <%# getTextDoc((int)Eval("register_status"),(string)Eval("register_date"),
                                                   (int)Eval("EmpIDX"),0 ,
                                                   (int)Eval("signup_status"),(string)Eval("signup_date"),
                                                   (int)Eval("test_scores_status"),(string)Eval("test_scores_date"),
                                                   (int)Eval("summ_std_status"),(string)Eval("summ_std_date"),
                                                   (int)Eval("summ_super_status"),(string)Eval("summ_super_date"),
                                                   (string)Eval("zregister_date"),
                                                   (int)Eval("zresulte_app_status"),
                                                   (int)Eval("zresulte_count"),
                                                   (int)Eval("zcourse_count"),
                                                   (int)Eval("course_type_elearning"),
                                                   (int)Eval("course_type_etraining")
                                                   ) %>
                                </span>
                                <br />
                                <asp:Panel runat="server" ID="pnlicon_yes">
                                    <center>
                                         <img src='<%# ResolveUrl("~/images/elearning/icon_yes.png") %>' width="30" height="30" title="ผ่าน" id="imgicon_yes" runat="server" visible="true" />
                                    </center>
                                </asp:Panel>
                                <asp:Panel runat="server" ID="pnlicon_no">
                                    <center>
                                         <img src='<%# ResolveUrl("~/images/elearning/icon_no.png") %>' width="30" height="30" title="ไม่ผ่าน" id="imgicon_no" runat="server" visible="true" />
                                    </center>
                                </asp:Panel>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                            <ItemTemplate>
                                <div class="word-wrap">

                                    <asp:Label ID="lbu0_training_course_idx" runat="server"
                                        Visible="false" Text='<%# Eval("u0_training_course_idx") %>' />

                                    <asp:TextBox ID="txtGvtraining_course_no" runat="server"
                                        Visible="false" Text='<%# Eval("training_course_no") %>' />

                                    <asp:Label ID="lbu0_course_idx" runat="server"
                                        Visible="false" Text='<%# Eval("u0_course_idx") %>' />

                                    <asp:Label ID="lbzcourse_count" runat="server"
                                        Visible="false" Text='<%# Eval("zcourse_count") %>' />
                                    <asp:Label ID="lbgrade_status" runat="server"
                                        Visible="false" Text='<%# Eval("grade_status") %>' />

                                    <asp:Label ID="lbsumm_super_status" runat="server"
                                        Visible="false" Text='<%# Eval("summ_super_status") %>' />

                                    <asp:HyperLink runat="server" ID="btnDownloadFile"
                                        CssClass="btn btn-default btn-sm" data-original-title="Download"
                                        data-toggle="tooltip" Text="Download"
                                        Target="_blank"
                                        CommandArgument='<%# Eval("training_course_no") %>'><i class="fa fa-download"></i>

                                    </asp:HyperLink>

                                    <asp:LinkButton ID="btnDetail_GvHR_AList"
                                        CssClass="btn btn-info btn-sm" runat="server"
                                        data-original-title="รายละเอียด" data-toggle="tooltip" Text="รายละเอียด"
                                        OnCommand="btnCommand" CommandName="btnDetail_GvHR_AList"
                                        CommandArgument='<%# 
                                        Convert.ToString(Eval("u0_training_course_idx")) + "|" +  
                                        Convert.ToString(Eval("zcourse_count"))+ "|" + 
                                        Convert.ToString(Eval("course_type_elearning"))+ "|" + 
                                        Convert.ToString(Eval("course_type_etraining"))+ "|" 
                                        %>'>
                                    <i class="fa fa-file-alt"></i></asp:LinkButton>
                                    <asp:LinkButton ID="btnDetail_sumcourseList"
                                        CssClass="btn btn-success btn-sm" runat="server"
                                        data-original-title="ผลการฝึกอบรม" data-toggle="tooltip" Text="ผลการฝึกอบรม"
                                        OnCommand="btnCommand" CommandName="btnDetail_sumcourseList"
                                        CommandArgument='<%# Eval("u0_training_course_idx") %>'
                                        Visible='<%# getVisible_btnsumm((int)Eval("register_status"),
                                                   (int)Eval("signup_status"),
                                                   (int)Eval("summ_std_status"),
                                                   (int)Eval("summ_super_status"),
                                                   (int)Eval("course_type_elearning"),
                                                   (int)Eval("course_type_etraining"),
                                                   (int)Eval("place_type")
                                                   ) %>'>
                                    <i class="fa fa-file-alt"></i></asp:LinkButton>
                                    <asp:LinkButton ID="btnDetail_sumcoursepreview"
                                        CssClass="btn btn-primary btn-sm" runat="server"
                                        data-original-title="ผลการฝึกอบรม" data-toggle="tooltip" Text="ผลการฝึกอบรม"
                                        OnCommand="btnCommand" CommandName="btnDetail_sumcoursepreview"
                                        CommandArgument='<%# Eval("u0_training_course_idx") %>'
                                        Visible='<%# getVisible_btnsumm_prview((int)Eval("summ_super_status")) %>'>
                                    <i class="fa fa-file-archive-o"></i></asp:LinkButton>

                                    <asp:LinkButton ID="btn_elearning"
                                        CssClass="btn btn-primary btn-sm" runat="server"
                                        data-original-title="e-Learning" data-toggle="tooltip" Text="e-Learning"
                                        OnCommand="btnCommand" CommandName="_divbtn_elearning"
                                        Visible='<%# getvisible_btn_elearning((int)Eval("EmpIDX"),
                                                                              (int)Eval("zcourse_count"),
                                                                              (int)Eval("course_type_elearning") ) %>'
                                        CommandArgument='<%# 
                                        Convert.ToString(Eval("u0_training_course_idx")) + "|" +  
                                        Convert.ToString(Eval("u3_training_course_idx"))
                                        %>'>
                                    <i class="fa fa-play"></i></asp:LinkButton>

                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>


                    </Columns>
                </asp:GridView>

            </div>

        </asp:View>


        <%-- Start MD --%>


        <asp:View ID="View_MD_WList" runat="server">

            <asp:Panel ID="Panel9" runat="server" Visible="true">
                <div class="panel panel-primary m-t-10">
                    <div class="panel-heading f-bold">ค้นหา</div>
                    <div class="panel-body">

                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>ปี</label>
                                <asp:DropDownList ID="ddlYearSearch_MD_W" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>เลขที่เอกสาร / ชื่อหลักสูตร</label>
                                <asp:TextBox ID="txtFilterKeyword_MD_W" runat="server"
                                    CssClass="form-control"
                                    placeholder="เลขที่เอกสาร / ชื่อหลักสูตร..." />
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <label>
                                &nbsp;
                            </label>
                            <div class="form-group">
                                <asp:LinkButton ID="btnS_MD_W" runat="server" CssClass="btn btn-primary" Text="<i class='fa fa-search'></i> ค้นหา"
                                    OnCommand="btnCommand" CommandName="btnS_MD_W" />
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <div class="row">

                <asp:GridView ID="GvMD_WList"
                    runat="server"
                    AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered table-responsive col-md-12 word-wrap"
                    HeaderStyle-CssClass="info"
                    AllowPaging="true"
                    PageSize="10"
                    OnRowCommand="onRowCommand"
                    OnRowDataBound="onRowDataBound"
                    OnPageIndexChanging="gvPageIndexChanging"
                    DataKeyNames="u0_training_course_idx"
                    ShowFooter="false"
                    Font-Size="Small">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />

                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่พบข้อมูล</div>
                    </EmptyDataTemplate>
                    <Columns>


                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                            <ItemTemplate>
                                <div class="word-wrap">
                                    <%# (Container.DataItemIndex + 1) %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="เลขที่เอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                            <ItemTemplate>
                                <div class="word-wrap">
                                    <%# Eval("training_course_no") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="วันที่สร้างเอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                            <ItemTemplate>
                                <div class="word-wrap">
                                    <%# Eval("zdate") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ประเภทหลักสูตร" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="3%">
                            <ItemTemplate>
                                <div class="word-wrap">
                                    <%-- <%# course_plan_status_name((string)Eval("course_plan_status")) %>--%>
                                    <img src='<%# getImageIO((string)Eval("course_plan_status"),1) %>'
                                        width="50" height="50"
                                        title='<%# getImageIO((string)Eval("course_plan_status"),2) %>'
                                        id="imgicon_inplan" runat="server" visible="true" />
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ชื่อหลักสูตร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                            <ItemTemplate>
                                <div class="word-wrap">
                                    <%# Eval("course_name") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="กลุ่มวิชา" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                            <ItemTemplate>
                                <div class="word-wrap">
                                    <%# Eval("training_group_name") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สาขาวิชา" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                            <ItemTemplate>
                                <div class="word-wrap">
                                    <%# Eval("training_branch_name") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สถานะของเอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                            <ItemTemplate>

                                <%# getTextDoc((int)Eval("register_status"),(string)Eval("register_date"),
                                                   (int)Eval("EmpIDX"),0 ,
                                                   (int)Eval("signup_status"),(string)Eval("signup_date"),
                                                   (int)Eval("test_scores_status"),(string)Eval("test_scores_date"),
                                                   (int)Eval("summ_std_status"),(string)Eval("summ_std_date"),
                                                   (int)Eval("summ_super_status"),(string)Eval("summ_super_date"),
                                                   (string)Eval("zregister_date"),
                                                   (int)Eval("zresulte_app_status"),
                                                   (int)Eval("zresulte_count"),
                                                   (int)Eval("zcourse_count"),
                                                   (int)Eval("course_type_elearning"),
                                                   (int)Eval("course_type_etraining")
                                                   ) %>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                            <ItemTemplate>
                                <div class="word-wrap">
                                    <asp:LinkButton ID="btnDetail_GvMD_WList"
                                        CssClass="btn btn-info btn-sm" runat="server"
                                        data-original-title="รายละเอียด" data-toggle="tooltip" Text="รายละเอียด"
                                        OnCommand="btnCommand" CommandName="btnDetail_GvMD_WList"
                                        CommandArgument='<%# Eval("u0_training_course_idx") %>'>
                                    <i class="fa fa-file-alt"></i></asp:LinkButton>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>


                    </Columns>
                </asp:GridView>

            </div>

        </asp:View>


        <asp:View ID="View_MD_AList" runat="server">

            <asp:Panel ID="Panel10" runat="server" Visible="true">
                <div class="panel panel-primary m-t-10">
                    <div class="panel-heading f-bold">ค้นหา</div>
                    <div class="panel-body">

                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>ปี</label>
                                <asp:DropDownList ID="ddlYearSearch_MD_A" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>เลขที่เอกสาร / ชื่อหลักสูตร</label>
                                <asp:TextBox ID="txtFilterKeyword_MD_A" runat="server"
                                    CssClass="form-control"
                                    placeholder="เลขที่เอกสาร / ชื่อหลักสูตร..." />
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>ผลการฝึกอบรม</label>
                                <asp:DropDownList ID="ddlSummary" runat="server"
                                    CssClass="form-control">
                                    <asp:ListItem Selected="True" Value="1">ยังไม่ให้ผล</asp:ListItem>
                                    <asp:ListItem Value="2">ให้ผลแล้ว</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <label>
                                &nbsp;
                            </label>
                            <div class="form-group">
                                <asp:LinkButton ID="btnS_MD_A" runat="server" CssClass="btn btn-primary" Text="<i class='fa fa-search'></i> ค้นหา"
                                    OnCommand="btnCommand" CommandName="btnS_MD_A" />
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <div class="row">

                <asp:GridView ID="GvMD_AList"
                    runat="server"
                    AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered table-responsive col-md-12 word-wrap"
                    HeaderStyle-CssClass="info"
                    AllowPaging="true"
                    PageSize="10"
                    OnRowCommand="onRowCommand"
                    OnRowDataBound="onRowDataBound"
                    OnPageIndexChanging="gvPageIndexChanging"
                    DataKeyNames="u6_training_course_idx"
                    ShowFooter="false"
                    Font-Size="Small">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />

                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่พบข้อมูล</div>
                    </EmptyDataTemplate>
                    <Columns>


                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                            <ItemTemplate>
                                <div class="word-wrap">
                                    <%# (Container.DataItemIndex + 1) %>

                                    <asp:Label ID="lbu0_training_course_idx" runat="server"
                                        Visible="false" Text='<%# Eval("u0_training_course_idx") %>' />

                                    <asp:Label ID="lbu0_course_idx" runat="server"
                                        Visible="false" Text='<%# Eval("u0_course_idx") %>' />
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="เลขที่เอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                            <ItemTemplate>
                                <div class="word-wrap">
                                    <%# Eval("training_course_no") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="วันที่สร้างเอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                            <ItemTemplate>
                                <div class="word-wrap">
                                    <%# Eval("zdate") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="หลักสูตร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                            <ItemTemplate>
                                <div class="word-wrap">
                                    <%# Eval("course_name") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="วันที่อบรม" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="25%">
                            <ItemTemplate>
                                <div class="word-wrap">
                                    <table class="table table-striped f-s-12 table-empshift-responsive">
                                        <asp:Repeater ID="rptu8trncoursedate" runat="server">
                                            <HeaderTemplate>
                                                <tr>
                                                    <th>วันที่อบรม</th>
                                                    <th>เวลาที่เริ่ม-เวลาที่สิ้นสุด</th>
                                                    <th>รวมชม.ที่เรียน</th>
                                                </tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%# Eval("zdate") %></td>
                                                    <td><%# Eval("ztime_start")+" - "+Eval("ztime_end") %></td>
                                                    <td align="center"><%# string.Format("{0:n2}",Eval("training_course_date_qty")) %></td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </table>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ผู้เข้าอบรม" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%">
                            <ItemTemplate>
                                <div class="word-wrap">
                                    <%# Eval("EmpCode")+" - "+Eval("FullNameTH") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                            <ItemTemplate>
                                <div class="word-wrap">
                                    <asp:TextBox ID="txtGvtraining_course_no" runat="server"
                                        Visible="false" Text='<%# Eval("training_course_no") %>' />
                                    <asp:TextBox ID="txtGvemp_idx_ref" runat="server"
                                        Visible="false" Text='<%# Eval("emp_idx_ref") %>' />

                                    <asp:HyperLink runat="server" ID="btnDownloadFile"
                                        CssClass="btn btn-default btn-sm" data-original-title="Download"
                                        data-toggle="tooltip" Text="Download"
                                        Target="_blank"
                                        CommandArgument='<%# Eval("training_course_no") %>'><i class="fa fa-download"></i>

                                    </asp:HyperLink>

                                    <asp:LinkButton ID="btnDetail_GvMD_AList"
                                        CssClass="btn btn-success btn-sm" runat="server"
                                        data-original-title="ผลการฝึกอบรม" data-toggle="tooltip" Text="ผลการฝึกอบรม"
                                        OnCommand="btnCommand" CommandName="btnDetail_GvMD_AList"
                                        CommandArgument='<%#
                                        Convert.ToString(Eval("u0_training_course_idx_ref")) + "|" +  
                                        Convert.ToString(Eval("u6_training_course_idx"))
                                        %>'>
                                    <i class="fa fa-file-alt"></i></asp:LinkButton>
                                    <%--  --%>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>


                    </Columns>
                </asp:GridView>

            </div>

        </asp:View>

        <%-- End MD --%>


        <%--  OnDataBound="FvDetail_DataBound" --%>
        <asp:View ID="View_HR_WDetail" runat="server">
            <asp:FormView ID="fv_preview" runat="server" Width="100%">
                <EditItemTemplate>


                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-file"></i><strong>&nbsp; 
                                        <asp:Label ID="lbtitle_preview" runat="server" Text="ข้อมูลแผนการฝึกอบรม"></asp:Label>
                            </strong></h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <asp:Panel ID="Panel5" runat="server" Visible="false">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:Label ID="Label20" class="col-md-2 control-labelnotop text_right" runat="server" Text="รหัสหลักสูตร :" />
                                                <div class="col-md-4">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>


                                <div class="form-group">

                                    <asp:Label ID="Label3" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="วันที่เอกสาร :" />
                                    <div class="col-md-4">
                                        <div class='input-group date'>
                                            <asp:Label ID="txttraining_course_date" runat="server"
                                                CssClass="f-s-13 control-label"
                                                Text='<%# Eval("zdate") %>' />
                                        </div>
                                    </div>


                                    <asp:Label ID="Label5" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="เลขที่เอกสาร : " />
                                    <div class="col-md-3">
                                        <asp:TextBox ID="txtu3_training_course_idx" runat="server"
                                            Visible="false"
                                            Text='<%# Eval("u3_training_course_idx") %>'>
                                        </asp:TextBox>
                                        <asp:Label ID="txttraining_course_no" runat="server"
                                            CssClass="f-s-13 control-label"
                                            Text='<%# Eval("training_course_no") %>' />

                                    </div>

                                </div>

                                <div class="form-group">

                                    <asp:Label ID="Label14" class="col-md-2 control-labelnotop text_right" runat="server" Text="ประเภทหลักสูตร :" />
                                    <div class="col-md-4">
                                        <asp:Label ID="Label1" runat="server"
                                            CssClass="f-s-13 control-label"
                                            Text='<%# getValue_plan_status((string)Eval("course_plan_status")) %>' />

                                    </div>

                                    <div class="col-md-6">
                                    </div>

                                </div>

                                <div class="form-group">

                                    <asp:Label ID="Label12" class="col-md-2 control-labelnotop text_right" runat="server" Text="ชื่อหลักสูตร :" />
                                    <div class="col-md-9">
                                        <asp:Label ID="Label2" runat="server"
                                            CssClass="f-s-13 control-label"
                                            Text='<%# Eval("course_name") %>' />
                                    </div>

                                </div>




                                <div class="form-group">

                                    <asp:Label ID="Label15" class="col-md-2 control-labelnotop text_right" runat="server" Text="กลุ่มวิชา :" />
                                    <div class="col-md-4">
                                        <asp:Label ID="txttraining_group_name" runat="server"
                                            Text='<%# Eval("training_group_name") %>'
                                            CssClass="f-s-13 control-label">
                                        </asp:Label>
                                    </div>

                                    <asp:Label ID="Label25" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="สาขาวิชา : " />
                                    <div class="col-md-3">
                                        <asp:Label ID="txttraining_branch_name" runat="server"
                                            Text='<%# Eval("training_branch_name") %>'
                                            CssClass="f-s-13 control-label">
                                        </asp:Label>
                                    </div>

                                </div>



                                <div class="form-group">

                                    <asp:Label ID="Label26" class="col-md-2 control-labelnotop text_right" runat="server" Text="รายละเอียดหลักสูตร :" />
                                    <div class="col-md-9">
                                        <asp:Label ID="txttraining_course_description" runat="server"
                                            Text='<%# Eval("training_course_description") %>'
                                            CssClass="f-s-13 control-label"
                                            TextMode="MultiLine"
                                            Rows="4">
                                        </asp:Label>
                                    </div>

                                </div>

                                <div class="form-group">

                                    <asp:Label ID="Label86" class="col-md-2 control-labelnotop text_right" runat="server" Text="คอร์สอบรม :" />
                                    <div class="col-md-9">
                                        <asp:Label ID="Label4" runat="server"
                                            Text='<%# getValue_course_type((int)Eval("training_course_type")) %>'
                                            CssClass="f-s-13 control-label">
                                        </asp:Label>

                                    </div>

                                </div>

                                <div class="form-group">

                                    <asp:Label ID="Label87" class="col-md-2 control-labelnotop text_right" runat="server" Text="สถานที่อบรม :" />
                                    <div class="col-md-4">
                                        <asp:Label ID="Label6" runat="server"
                                            Text='<%# getValue_place((int)Eval("place_idx_ref")) %>'
                                            CssClass="f-s-13 control-label">
                                        </asp:Label>

                                    </div>

                                </div>

                                <div class="form-group">

                                    <asp:Label ID="Label10" class="col-md-2 control-labelnotop text_right" runat="server" Text="วิทยากร :" />
                                    <div class="col-md-9">
                                        <asp:Label ID="Label7" runat="server"
                                            Text='<%# getValue_lecturer_type((int)Eval("lecturer_type")) %>'
                                            CssClass="f-s-13 control-label">
                                        </asp:Label>


                                    </div>

                                </div>

                                <asp:Panel ID="Panel2" runat="server" Visible="false">
                                    <div class="form-group">

                                        <asp:Label ID="Label28" class="col-md-2 control-labelnotop text_right" runat="server" Text=" " />
                                        <div class="col-md-9">
                                            <div>

                                                <div class="panel-info">
                                                    <div class="panel-heading f-bold">รายละเอียดวิทยากร</div>
                                                </div>
                                                <asp:GridView ID="Gvinstitution" runat="server"
                                                    CssClass="table table-bordered word-wrap"
                                                    GridLines="None" OnRowCommand="onRowCommand"
                                                    AutoGenerateColumns="false"
                                                    Font-Size="Small">
                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">ไม่พบข้อมูล</div>
                                                    </EmptyDataTemplate>
                                                    <Columns>

                                                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                                            HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center"
                                                            HeaderStyle-Width="5%">
                                                            <ItemTemplate>
                                                                <%# (Container.DataItemIndex +1) %>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField
                                                            HeaderText="ชื่อ - สกุล"
                                                            ItemStyle-HorizontalAlign="Left"
                                                            HeaderStyle-CssClass="text-center"
                                                            ItemStyle-Font-Size="Small">
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtinstitution_name_edit_L" runat="server" Text='<%# Bind("zName") %>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="txtinstitution_name_item_L" runat="server" Text='<%# Eval("zName") %>'
                                                                    Width="100%"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>

                                    </div>
                                </asp:Panel>

                                <div class="form-group">
                                    <asp:Label ID="Label29" class="col-md-2 control-labelnotop text_right" runat="server" Text="วันที่อบรม :" />
                                    <div class="col-sm-4">
                                        <table class="table table-striped f-s-12 table-empshift-responsive">
                                            <asp:Repeater ID="rptu8trncoursedate" runat="server">
                                                <HeaderTemplate>
                                                    <tr>
                                                        <th>วันที่อบรม</th>
                                                        <th>เวลาที่เริ่ม-เวลาที่สิ้นสุด</th>
                                                        <th>รวมชม.ที่เรียน</th>
                                                    </tr>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%# Eval("zdate") %></td>
                                                        <td><%# Eval("ztime_start")+" - "+Eval("ztime_end") %></td>
                                                        <td align="center"><%# string.Format("{0:n2}",Eval("training_course_date_qty")) %></td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </table>
                                    </div>

                                </div>


                                <%-- start วัตถุประสงค์ --%>
                                <asp:Panel ID="Panel4" runat="server" Visible="false">
                                    <div class="form-group">

                                        <asp:Label ID="Label40" class="col-md-2 control-labelnotop text_right" runat="server" Text="วัตถุประสงค์ :" />
                                        <div class="col-md-9">
                                            <div>

                                                <div class="panel-info">
                                                    <div class="panel-heading f-bold">รายละเอียดวัตถุประสงค์</div>
                                                </div>
                                                <%-- table table-striped f-s-12 table-empshift-responsive--%>
                                                <asp:GridView ID="Gvobjective" runat="server"
                                                    CssClass="table table-bordered word-wrap"
                                                    GridLines="None" OnRowCommand="onRowCommand"
                                                    AutoGenerateColumns="false"
                                                    Font-Size="Small">
                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">ไม่พบข้อมูล</div>
                                                    </EmptyDataTemplate>
                                                    <Columns>

                                                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                                            HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center"
                                                            HeaderStyle-Width="5%">
                                                            <ItemTemplate>
                                                                <%# (Container.DataItemIndex +1) %>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField
                                                            HeaderText="วัตถุประสงค์"
                                                            ItemStyle-HorizontalAlign="Left"
                                                            HeaderStyle-CssClass="text-center"
                                                            ItemStyle-Font-Size="Small">
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtobjective_name_edit_L" runat="server" Text='<%# Bind("zName") %>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbm0_objective_idx_ref" runat="server" Text='<%# Eval("m0_objective_idx_ref") %>'
                                                                    Visible="false"></asp:Label>
                                                                <asp:Label ID="txtobjective_name_item_L" runat="server" Text='<%# Eval("zName") %>'
                                                                    Width="100%"></asp:Label>
                                                                <hr />
                                                                <%-- start --%>

                                                                <asp:Label ID="Label78" class="col-md-12 control-labelnotop pull-left" runat="server"
                                                                    Text="การวัด ประเมิน และติดตามผล" Font-Bold="true" />

                                                                <div class="form-group">

                                                                    <asp:Label ID="Label79" class="col-md-1 control-labelnotop text_right" runat="server" Text="" />

                                                                    <div class="col-md-5 form-inline">
                                                                        <asp:CheckBox ID="cbpass_test_flag" runat="server"
                                                                            CssClass="checkbox"
                                                                            Enabled="false"
                                                                            Checked='<%# getValue((string)Eval("pass_test_flag")) %>'
                                                                            Text='<%# Eval("pass_test_per").ToString() == "0.00" ? "ผ่านการทดสอบ - %" : "ผ่านการทดสอบ "+Eval("pass_test_per")+" % " %>' />

                                                                    </div>


                                                                    <div class="col-md-6 form-inline">
                                                                        <asp:CheckBox ID="cbhour_training_flag" runat="server"
                                                                            CssClass="checkbox"
                                                                            Enabled="false"
                                                                            Checked='<%# getValue((string)Eval("hour_training_flag")) %>'
                                                                            Text='<%# Eval("hour_training_per").ToString() == "0.00" ? "ชั่วโมงเข้าอบรมครบ - %" : "ชั่วโมงเข้าอบรมครบ "+Eval("hour_training_per")+" % " %>' />

                                                                    </div>

                                                                </div>

                                                                <div class="form-group">

                                                                    <asp:Label ID="Label82" class="col-md-1 control-labelnotop text_right" runat="server" Text="" />

                                                                    <div class="col-md-5 form-inline">
                                                                        <asp:CheckBox ID="cbwrite_report_training_flag" runat="server"
                                                                            CssClass="checkbox"
                                                                            Enabled="false"
                                                                            Checked='<%# getValue((string)Eval("write_report_training_flag")) %>'
                                                                            Text="เขียนรายงานการอบรม" />
                                                                    </div>


                                                                    <div class="col-md-6 form-inline">
                                                                        <asp:CheckBox ID="cbpublish_training_flag" runat="server"
                                                                            CssClass="checkbox"
                                                                            Enabled="false"
                                                                            Checked='<%# getValue((string)Eval("publish_training_flag")) %>'
                                                                            Text='<%# "เผยแพร่ต่อผู้เกี่ยวข้องในรูปแบบ : " +Eval("publish_training_description") %>' />

                                                                    </div>

                                                                </div>

                                                                <div class="form-group">

                                                                    <asp:Label ID="Label83" class="col-md-1 control-labelnotop text_right" runat="server" Text="" />

                                                                    <div class="col-md-5 form-inline">
                                                                        <asp:CheckBox ID="cbcourse_lecturer_flag" runat="server"
                                                                            CssClass="checkbox"
                                                                            Enabled="false"
                                                                            Checked='<%# getValue((string)Eval("course_lecturer_flag")) %>'
                                                                            Text="จัดทำหลักสูตรและเป็นวิทยากรภายใน" />
                                                                    </div>


                                                                    <div class="col-md-6 form-inline">
                                                                        <asp:CheckBox ID="cbother_flag" runat="server"
                                                                            CssClass="checkbox"
                                                                            Enabled="false"
                                                                            Checked='<%# getValue((string)Eval("other_flag")) %>'
                                                                            Text='<%# "อื่นๆ ระบุ : "+ Eval("other_description") %>' />

                                                                    </div>

                                                                </div>
                                                                <asp:Label ID="Label88" class="col-md-12 control-labelnotop pull-left" runat="server"
                                                                    Text="การติดตามผลโดย HRD :" />
                                                                <div class="form-group">

                                                                    <asp:Label ID="Label84" class="col-md-1 control-labelnotop text_right" runat="server" Text="" />

                                                                    <div class="col-md-5 form-inline">
                                                                        <asp:CheckBox ID="cbhrd_nofollow_flag" runat="server"
                                                                            CssClass="checkbox"
                                                                            Enabled="false"
                                                                            Checked='<%# getValue((string)Eval("hrd_nofollow_flag")) %>'
                                                                            Text="ไม่ติดตามผล" />
                                                                    </div>


                                                                    <div class="col-md-6 form-inline">
                                                                        <asp:CheckBox ID="cbhrd_follow_flag" runat="server"
                                                                            CssClass="checkbox"
                                                                            Enabled="false"
                                                                            Checked='<%# getValue((string)Eval("hrd_follow_flag")) %>'
                                                                            Text='<%# Eval("hrd_follow_day").ToString() == "0" ? "ติดตามภายใน - วัน" : "ติดตามภายใน "+Eval("hrd_follow_day")+" วัน " %>' />

                                                                    </div>

                                                                </div>
                                                                <%-- end --%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>

                                    </div>
                                </asp:Panel>

                                <%-- end วัตถุประสงค์ --%>
                                <asp:Panel ID="Panel7" runat="server" Visible="false">
                                    <div class="form-group">

                                        <asp:Label ID="Label41" class="col-md-2 control-labelnotop text_right" runat="server" Text="รายละเอียดวัตถุประสงค์ :" />
                                        <div class="col-md-9">
                                            <asp:Label ID="txttraining_course_remark" runat="server"
                                                Text='<%# Eval("training_course_remark") %>'
                                                CssClass="f-s-13 control-label"
                                                TextMode="MultiLine"
                                                Rows="4">
                                            </asp:Label>
                                        </div>

                                    </div>

                                    <%-- start รายละเอียดผู้เข้าร่วมอบรม  : --%>

                                    <hr />

                                    <asp:Label ID="Label56" class="col-md-12 control-labelnotop pull-left h4" runat="server" Text="รายละเอียดผู้เข้าร่วมอบรม" />

                                    <div class="form-group">

                                        <asp:Label ID="Label44" class="col-md-2 control-labelnotop text_right" runat="server" Text=" " />
                                        <div class="col-md-9">
                                            <div>

                                                <div class="panel-info">
                                                    <div class="panel-heading f-bold">รายละเอียดพนักงาน</div>
                                                </div>
                                                <asp:GridView ID="Gvemp" runat="server"
                                                    CssClass="table table-striped f-s-12 table-empshift-responsive word-wrap"
                                                    GridLines="None" OnRowCommand="onRowCommand"
                                                    AutoGenerateColumns="false"
                                                    Font-Size="Small">
                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">ไม่พบข้อมูล</div>
                                                    </EmptyDataTemplate>
                                                    <Columns>

                                                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                                            HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center"
                                                            HeaderStyle-Width="5%">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <%# (Container.DataItemIndex +1) %>
                                                                </small>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField
                                                            HeaderText="ชื่อ - สกุล"
                                                            ItemStyle-HorizontalAlign="Left"
                                                            HeaderStyle-CssClass="text-center"
                                                            ItemStyle-Font-Size="Small">
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtemp_zName_edit_L" runat="server" Text='<%# Bind("zName") %>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="txtemp_zName_item_L" runat="server" Text='<%# Eval("zName") %>'
                                                                    Width="100%"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField
                                                            HeaderText="ตำแหน่ง"
                                                            ItemStyle-HorizontalAlign="Left"
                                                            HeaderStyle-CssClass="text-center"
                                                            ItemStyle-Font-Size="Small">
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtemp_zPostName_edit_L" runat="server" Text='<%# Bind("zPostName") %>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="txtemp_zPostName_item_L" runat="server" Text='<%# Eval("zPostName") %>'
                                                                    Width="100%"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField
                                                            HeaderText="Cost Center"
                                                            ItemStyle-HorizontalAlign="Left"
                                                            HeaderStyle-CssClass="text-center"
                                                            ItemStyle-Font-Size="Small">
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtemp_zCostCenter_edit_L" runat="server" Text='<%# Bind("zCostCenter") %>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="txtemp_zCostCenter_item_L" runat="server" Text='<%# Eval("zCostCenter") %>'
                                                                    Width="100%"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField
                                                            HeaderText="เบอร์ติดต่อ"
                                                            ItemStyle-HorizontalAlign="Left"
                                                            HeaderStyle-CssClass="text-center"
                                                            ItemStyle-Font-Size="Small">
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtemp_zTel_edit_L" runat="server" Text='<%# Bind("zTel") %>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="txtemp_zTel_item_L" runat="server" Text='<%# Eval("zTel") %>'
                                                                    Width="100%"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>

                                    </div>
                                    <hr />
                                </asp:Panel>
                                <%-- end รายละเอียดผู้เข้าร่วมอบรม  : --%>

                                <div class="form-group">

                                    <asp:Label ID="Label21" class="col-md-2 control-labelnotop text_right" runat="server" Text="จำนวนผู้เข้าร่วมอบรม  :" />
                                    <div class="col-md-3">

                                        <asp:Label ID="Label24" runat="server"
                                            CssClass="f-s-13 control-label"
                                            Text='<%# getformatfloat(((int)Eval("zcount_emp")).ToString(),0)+"    คน" %>' />

                                    </div>

                                </div>
                                <asp:Panel ID="Panel6" runat="server" Visible="false">
                                    <div class="form-group">

                                        <asp:Label ID="Label45" class="col-md-2 control-labelnotop text_right" runat="server" Text="รายละเอียดแผน และงบประมาณ  :" />
                                        <div class="col-md-9">

                                            <asp:Label ID="Label16" runat="server"
                                                CssClass="f-s-13 control-label"
                                                Text='<%# getValue_planbudget_type((int)Eval("training_course_planbudget_type")) %>' />

                                        </div>

                                    </div>

                                    <hr />

                                    <asp:Label ID="Label42" class="col-md-12 control-labelnotop pull-left h4" runat="server" Text="ค่าใช้จ่ายฝึกอบรม" />

                                    <div class="form-group">

                                        <asp:Label ID="Label55" class="col-md-2 control-labelnotop text_right" runat="server" Text=" " />
                                        <div class="col-md-9">
                                            <div>

                                                <div class="panel-info">
                                                    <div class="panel-heading f-bold">รายละเอียดค่าใช้จ่ายฝึกอบรม</div>
                                                </div>
                                                <asp:GridView ID="Gvtrn_expenses" runat="server"
                                                    CssClass="table table-bordered word-wrap"
                                                    GridLines="None" OnRowCommand="onRowCommand"
                                                    AutoGenerateColumns="false"
                                                    Font-Size="Small">
                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">ไม่พบข้อมูล</div>
                                                    </EmptyDataTemplate>
                                                    <Columns>

                                                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                                            HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center"
                                                            HeaderStyle-Width="5%">
                                                            <ItemTemplate>

                                                                <%# (Container.DataItemIndex +1) %>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField
                                                            HeaderText="รายการ"
                                                            ItemStyle-HorizontalAlign="Left"
                                                            HeaderStyle-CssClass="text-center"
                                                            ItemStyle-Font-Size="Small">
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtemp_expenses_description_edit_L" runat="server" Text='<%# Bind("expenses_description") %>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="txtemp_expenses_description_item_L" runat="server" Text='<%# Eval("expenses_description") %>'
                                                                    Width="100%"></asp:Label>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField
                                                            HeaderText="มูลค่า"
                                                            ItemStyle-HorizontalAlign="Right"
                                                            HeaderStyle-CssClass="text-center"
                                                            HeaderStyle-Width="15%"
                                                            ItemStyle-Font-Size="Small">
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtemp_amount_edit_L" runat="server" Text='<%# Bind("amount") %>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="txtemp_amount_item_L" runat="server" Text='<%# getStrformate((string)Eval("amount"),2) %>'
                                                                    Width="100%"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField
                                                            HeaderText="Vat"
                                                            ItemStyle-HorizontalAlign="Right"
                                                            HeaderStyle-CssClass="text-center"
                                                            ItemStyle-Font-Size="Small"
                                                            HeaderStyle-Width="10%">
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtemp_vat_edit_L" runat="server" Text='<%# Bind("vat") %>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="txtemp_vat_item_L" runat="server" Text='<%# getStrformate((string)Eval("vat"),2) %>'
                                                                    Width="100%"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField
                                                            HeaderText="หัก ณ ที่จ่าย"
                                                            ItemStyle-HorizontalAlign="Right"
                                                            HeaderStyle-CssClass="text-center"
                                                            ItemStyle-Font-Size="Small"
                                                            HeaderStyle-Width="15%">
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtemp_withholding_tax_edit_L" runat="server" Text='<%# Bind("withholding_tax") %>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="txtemp_withholding_tax_item_L" runat="server" Text='<%# getStrformate((string)Eval("withholding_tax"),2) %>'
                                                                    Width="100%"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label46" class="col-md-2 control-labelnotop text_right" runat="server" Text=" " />
                                        <div class="col-md-9">

                                            <div class="panel panel-default">
                                                <div class="panel-body">

                                                    <div class="form-group">

                                                        <asp:Label ID="Label57" class="col-md-2 control-labelnotop text_right" runat="server" Text="รวม :" />
                                                        <div class="col-md-2">
                                                            <asp:Label ID="txttraining_course_total" runat="server"
                                                                Text='<%# getformatfloat(((decimal)Eval("training_course_total")).ToString(),2) %>'
                                                                CssClass="f-s-13 control-label">
                                                            </asp:Label>
                                                        </div>
                                                        <asp:Label ID="Label60" CssClass="col-md-1 control-labelnotop textleft" runat="server" Text="บาท" />

                                                        <asp:Label ID="Label58" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="เฉลี่ย :" />
                                                        <div class="col-md-2">
                                                            <asp:Label ID="txttraining_course_total_avg" runat="server"
                                                                Text='<%# getformatfloat(((decimal)Eval("training_course_total_avg")).ToString(),2) %>'
                                                                CssClass="f-s-13 control-label">
                                                            </asp:Label>

                                                        </div>
                                                        <asp:Label ID="Label59" CssClass="col-md-2 control-labelnotop textleft" runat="server" Text="บาท/ท่าน" />
                                                    </div>

                                                    <div class="form-group">

                                                        <asp:Label ID="Label61" class="col-md-2 control-labelnotop text_right" runat="server" Text="ลดหย่อนภาษี :" />
                                                        <div class="col-md-2">
                                                            <asp:Label ID="txttraining_course_reduce_tax" runat="server"
                                                                Text='<%# getformatfloat(((decimal)Eval("training_course_reduce_tax")).ToString(),2) %>'
                                                                CssClass="f-s-13 control-label">
                                                            </asp:Label>
                                                        </div>
                                                        <asp:Label ID="Label62" CssClass="col-md-1 control-labelnotop textleft" runat="server" Text="%" />

                                                        <div class="col-md-2">
                                                        </div>

                                                    </div>

                                                    <div class="form-group">

                                                        <asp:Label ID="Label63" class="col-md-2 control-labelnotop text_right" runat="server" Text="ค่าใช้จ่ายสุทธิ :" />
                                                        <div class="col-md-2">
                                                            <asp:Label ID="txttraining_course_net_charge" runat="server"
                                                                Text='<%# getformatfloat(((decimal)Eval("training_course_net_charge")).ToString(),2) %>'
                                                                CssClass="f-s-13 control-label">
                                                            </asp:Label>
                                                        </div>
                                                        <asp:Label ID="Label64" CssClass="col-md-1 control-labelnotop textleft" runat="server" Text="บาท" />

                                                        <asp:Label ID="Label65" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="เฉลี่ย :" />
                                                        <div class="col-md-2">
                                                            <asp:Label ID="txttraining_course_net_charge_tax" runat="server"
                                                                Text='<%# getformatfloat(((decimal)Eval("training_course_net_charge_tax")).ToString(),2) %>'
                                                                CssClass="f-s-13 control-label">
                                                            </asp:Label>

                                                        </div>
                                                        <asp:Label ID="Label66" CssClass="col-md-2 control-labelnotop textleft" runat="server" Text="บาท/ท่าน" />
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label67" class="col-md-2 control-labelnotop text_right" runat="server" Text=" " />
                                        <div class="col-md-9">

                                            <div class="panel panel-default">
                                                <div class="panel-body">

                                                    <div class="form-group">

                                                        <asp:Label ID="Label68" class="col-md-3 control-labelnotop text_right" runat="server" Text="งบประมาณลง Cost Center :" />
                                                        <div class="col-md-3">
                                                            <asp:Label ID="Label18" runat="server"
                                                                Text='<%# Eval("CostNo") %>'
                                                                CssClass="f-s-13 control-label">
                                                            </asp:Label>
                                                        </div>

                                                        <asp:Label ID="Label70" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="หน่วยงาน :" />
                                                        <div class="col-md-4">
                                                            <asp:Label ID="Label19" runat="server"
                                                                Text='<%# Eval("dept_name_th") %>'
                                                                CssClass="f-s-13 control-label">
                                                            </asp:Label>

                                                        </div>

                                                    </div>

                                                    <div class="form-group">

                                                        <asp:Label ID="Label72" class="col-md-3 control-labelnotop text_right" runat="server" Text="แผนงบประมาณที่วางไว้ รวม :" />
                                                        <div class="col-md-3">
                                                            <asp:Label ID="txttraining_course_planbudget_total" runat="server"
                                                                Text='<%# getformatfloat(((decimal)Eval("training_course_planbudget_total")).ToString(),2) %>'
                                                                CssClass="f-s-13 control-label">
                                                            </asp:Label>

                                                        </div>
                                                        <asp:Label ID="Label73" CssClass="col-md-1 control-labelnotop textleft" runat="server" Text="บาท" />

                                                        <div class="col-md-2">
                                                        </div>

                                                    </div>

                                                    <div class="form-group">

                                                        <asp:Label ID="Label69" class="col-md-3 control-labelnotop text_right" runat="server" Text="งบประมาณที่ใช้รวมครั้งนี้ :" />
                                                        <div class="col-md-3">
                                                            <asp:Label ID="txttraining_course_budget_total" runat="server"
                                                                Text='<%#   getformatfloat(((decimal)Eval("training_course_budget_total")).ToString(),2) %>'
                                                                CssClass="f-s-13 control-label">
                                                            </asp:Label>
                                                        </div>
                                                        <asp:Label ID="Label71" CssClass="col-md-1 control-labelnotop textleft" runat="server" Text="บาท" />

                                                        <div class="col-md-2">
                                                        </div>

                                                    </div>

                                                    <div class="form-group">

                                                        <asp:Label ID="Label74" class="col-md-3 control-labelnotop text_right" runat="server" Text="งบประมาณคงเหลือ :" />
                                                        <div class="col-md-3">
                                                            <asp:Label ID="txttraining_course_budget_balance" runat="server"
                                                                Text='<%# getformatfloat(((decimal)Eval("training_course_budget_balance")).ToString(),2) %>'
                                                                CssClass="f-s-13 control-label">
                                                            </asp:Label>
                                                        </div>
                                                        <asp:Label ID="Label75" CssClass="col-md-1 control-labelnotop textleft" runat="server" Text="บาท" />

                                                        <div class="col-md-2">
                                                        </div>

                                                    </div>

                                                    <div class="form-group">

                                                        <asp:Label ID="Label76" class="col-md-3 control-labelnotop text_right" runat="server" Text="" />
                                                        <div class="col-md-2">
                                                            <asp:Label ID="txttraining_course_budget_total_per" runat="server"
                                                                Text='<%# getformatfloat(((decimal)Eval("training_course_budget_total_per")).ToString(),2) %>'
                                                                CssClass="f-s-13 control-label">
                                                            </asp:Label>
                                                        </div>
                                                        <asp:Label ID="Label77" CssClass="col-md-7 control-labelnotop textleft-red" runat="server" Text="%(ค่าใช้จ่ายที่ลงทั้งหมดเป็นค่าใช้จ่ายก่อน Vat เสมอ )" />



                                                    </div>


                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <hr />

                                </asp:Panel>
                                <%-- End  --%>
                            </div>
                        </div>
                    </div>

                    <asp:Panel ID="pnlDetailApp" runat="server">

                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h2 class="panel-title">รายละเอียดการลงทะเบียน
                                </h2>
                            </div>
                            <div class="panel-body">

                                <%-- register --%>
                                <asp:Panel ID="pnlregister" runat="server">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="form-group col-lg-12">
                                                <asp:Label ID="Label126" CssClass="col-sm-3 control-label" runat="server" Text="ผลการลงทะเบียน" />
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddlStatusapprove_register" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="" Text="....." />
                                                        <asp:ListItem Value="1" Text="ขอสิทธิ์เข้าร่วมคอร์สอบรม" />
                                                        <%--<asp:ListItem Value="2" Text="ไม่ลงทะเบียน" />--%>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredddlStatusapprove_register"
                                                        ValidationGroup="btnupdate_register" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="ddlStatusapprove_register"
                                                        Font-Size="1em" ForeColor="Red"
                                                        CssClass="pull-left"
                                                        ErrorMessage="กรุณากรอกผลการขอสิทธิ์เข้าร่วมคอร์สอบรม" />
                                                </div>
                                            </div>

                                            <div class="form-group col-lg-12" runat="server" visible="false">
                                                <asp:Label ID="Label127" CssClass="col-sm-3 control-label" runat="server" Text="ความคิดเห็นเพิ่มเติม" />
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="txtdetailApprove_register" CssClass="form-control" runat="server"
                                                        placeholder="ความคิดเห็นเพิ่มเติม ....." MaxLength="150" TextMode="multiline" Rows="4"></asp:TextBox>
                                                </div>
                                            </div>

                                            <asp:Panel ID="Panel15" runat="server">
                                                <div class="form-group col-lg-12">
                                                    <div class="col-sm-3 col-sm-offset-9">
                                                        <asp:LinkButton ID="btnupdate_register" CssClass="btn btn-success" runat="server"
                                                            ValidationGroup="btnupdate_register"
                                                            OnCommand="btnCommand" CommandName="btnupdate_register"
                                                            CommandArgument='<%# Eval("u0_training_course_idx") %>'
                                                            data-toggle="tooltip" title="Save"
                                                            OnClientClick="return confirm('คุณต้องการให้ผลการขอสิทธิ์เข้าร่วมคอร์สอบรมนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>

                                                    </div>
                                                </div>
                                            </asp:Panel>


                                        </div>
                                    </div>

                                </asp:Panel>

                                <asp:Panel ID="pnlapprove_hr" runat="server">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="form-group col-lg-12">
                                                <asp:Label ID="Label34" CssClass="col-sm-3 control-label" runat="server" Text="ผลการลงทะเบียน" />
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddlStatusapprove" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="" Text="ผลการลงทะเบียน....." />
                                                        <asp:ListItem Value="1" Text="ยืนยันการลงทะเบียน" />
                                                        <%--<asp:ListItem Value="2" Text="ไม่ลงทะเบียน" />--%>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredddlStatusapprove"
                                                        ValidationGroup="btnupdateApprove_HR" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="ddlStatusapprove"
                                                        Font-Size="1em" ForeColor="Red"
                                                        CssClass="pull-left"
                                                        ErrorMessage="กรุณากรอกผลการลงทะเบียน" />
                                                </div>
                                            </div>

                                            <div class="form-group col-lg-12" runat="server" visible="false">
                                                <asp:Label ID="Label35" CssClass="col-sm-3 control-label" runat="server" Text="ความคิดเห็นเพิ่มเติม" />
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="txtdetailApprove" CssClass="form-control" runat="server"
                                                        placeholder="ความคิดเห็นเพิ่มเติม ....." MaxLength="150" TextMode="multiline" Rows="4"></asp:TextBox>
                                                </div>
                                            </div>

                                            <asp:Panel ID="pnlApprove_HR_W" runat="server">
                                                <div class="form-group col-lg-12">
                                                    <div class="col-sm-3 col-sm-offset-9">
                                                        <asp:LinkButton ID="btnupdateApprove_HR" CssClass="btn btn-success" runat="server"
                                                            ValidationGroup="btnupdateApprove_HR"
                                                            OnCommand="btnCommand" CommandName="btnupdateApprove_HR"
                                                            CommandArgument='<%# Eval("u0_training_course_idx") %>'
                                                            data-toggle="tooltip" title="Save"
                                                            OnClientClick="return confirm('คุณต้องการให้ผลรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="btncancelApprove_HR"
                                                            CssClass="btn btn-default" runat="server" Text="Cancel"
                                                            OnCommand="btnCommand" CommandName="btnHR_Wait"
                                                            data-toggle="tooltip"
                                                            Visible="false"
                                                            title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </asp:Panel>

                                            <asp:Panel ID="pnlApprove_HR_A" runat="server">
                                                <div class="form-group col-lg-12">
                                                    <div class="col-sm-3 col-sm-offset-9">
                                                        <asp:LinkButton ID="btnupdateApprove_HR_A" CssClass="btn btn-success" runat="server"
                                                            ValidationGroup="btnupdateApprove_HR_A"
                                                            OnCommand="btnCommand" CommandName="btnupdateApprove_HR_A"
                                                            CommandArgument='<%# Eval("u0_training_course_idx") %>'
                                                            data-toggle="tooltip" title="Save"
                                                            OnClientClick="return confirm('คุณต้องการให้ผลรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="btncancelApprove_HR_A"
                                                            CssClass="btn btn-default" runat="server" Text="Cancel"
                                                            OnCommand="btnCommand" CommandName="btnHR_App"
                                                            data-toggle="tooltip"
                                                            Visible="false"
                                                            title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </asp:Panel>


                                        </div>
                                    </div>

                                </asp:Panel>

                                <asp:Panel ID="pnlapprove_md" runat="server">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="form-group col-lg-12">
                                                <asp:Label ID="Label38" CssClass="col-sm-3 control-label" runat="server" Text="ผลอนุมัติ" />
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddlStatusapprove_md" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="" Text="ผลการอนุมัติ....." />
                                                        <asp:ListItem Value="4" Text="อนุมัติ" />
                                                        <asp:ListItem Value="5" Text="กลับไปแก้ไข" />
                                                        <asp:ListItem Value="6" Text="ไม่อนุมัติ" />
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredddlStatusapprove_md"
                                                        ValidationGroup="btnupdateApprove_md" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="ddlStatusapprove_md"
                                                        Font-Size="1em" ForeColor="Red"
                                                        CssClass="pull-left"
                                                        ErrorMessage="กรุณากรอกผลอนุมัติ" />
                                                </div>
                                            </div>

                                            <div class="form-group col-lg-12">
                                                <asp:Label ID="Label39" CssClass="col-sm-3 control-label" runat="server" Text="ความคิดเห็นเพิ่มเติม" />
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="txtdetailApprove_md" CssClass="form-control" runat="server"
                                                        placeholder="ความคิดเห็นเพิ่มเติม ....." MaxLength="150" TextMode="multiline" Rows="4"></asp:TextBox>
                                                </div>
                                            </div>

                                            <asp:Panel ID="pnlApprove_MD_W" runat="server">
                                                <div class="form-group col-lg-12">
                                                    <div class="col-sm-3 col-sm-offset-9">
                                                        <asp:LinkButton ID="btnupdateApprove_MD" CssClass="btn btn-success" runat="server"
                                                            ValidationGroup="btnupdateApprove_md"
                                                            OnCommand="btnCommand" CommandName="btnupdateApprove_MD"
                                                            CommandArgument='<%# Eval("u0_training_course_idx") %>'
                                                            data-toggle="tooltip" title="Save"
                                                            OnClientClick="return confirm('คุณต้องการให้ผลรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="btncancelApprove_MD"
                                                            CssClass="btn btn-default" runat="server" Text="Cancel"
                                                            OnCommand="btnCommand" CommandName="btnMD_Wait"
                                                            data-toggle="tooltip"
                                                            Visible="false"
                                                            title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </asp:Panel>

                                            <asp:Panel ID="pnlApprove_MD_A" runat="server">
                                                <div class="form-group col-lg-12">
                                                    <div class="col-sm-3 col-sm-offset-9">
                                                        <asp:LinkButton ID="btnupdateApprove_MD_A" CssClass="btn btn-success" runat="server"
                                                            ValidationGroup="btnupdateApprove_md"
                                                            OnCommand="btnCommand" CommandName="btnupdateApprove_MD_A"
                                                            CommandArgument='<%# Eval("u0_training_course_idx") %>'
                                                            data-toggle="tooltip" title="Save"
                                                            OnClientClick="return confirm('คุณต้องการให้ผลรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="btncancelApprove_MD_A"
                                                            CssClass="btn btn-default" runat="server" Text="Cancel"
                                                            OnCommand="btnCommand" CommandName="btnMD_App"
                                                            data-toggle="tooltip"
                                                            Visible="false"
                                                            title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </asp:Panel>

                            </div>
                        </div>
                    </asp:Panel>
                </EditItemTemplate>


            </asp:FormView>

            <div class="row">
                <div class="form-group">
                    <div class="col-md-12">

                        <div class="col-md-2">
                        </div>
                        <div class="col-md-10">

                            <asp:LinkButton CssClass="btn btn-danger"
                                data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                                Text="<i class='fa fa-angle-left fa-lg'></i> ย้อนกลับ"
                                ID="btncancelApp_HR_W"
                                CommandName="btnHR_Wait_back" OnCommand="btnCommand" />

                            <asp:LinkButton CssClass="btn btn-danger"
                                data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                                Text="<i class='fa fa-angle-left fa-lg'></i> ย้อนกลับ"
                                ID="btncancelApp_HR"
                                CommandName="btnHR_App" OnCommand="btnCommand" />

                            <asp:LinkButton CssClass="btn btn-danger"
                                data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                                Text="<i class='fa fa-angle-left fa-lg'></i> ย้อนกลับ"
                                ID="btncancelApp_MD_W"
                                CommandName="btnMD_Wait" OnCommand="btnCommand" />

                            <asp:LinkButton CssClass="btn btn-danger"
                                data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                                Text="<i class='fa fa-angle-left fa-lg'></i> ย้อนกลับ"
                                ID="btncancelApp_MD"
                                CommandName="btnMD_App" OnCommand="btnCommand" />

                        </div>

                    </div>
                </div>
            </div>
            <br />

        </asp:View>


        <asp:View ID="View_syllabusList" runat="server">

            <asp:Panel ID="Panel11" runat="server" Visible="true">
                <div class="panel panel-primary m-t-10">
                    <div class="panel-heading f-bold">ค้นหา</div>
                    <div class="panel-body">

                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>ปี</label>
                                <asp:DropDownList ID="ddlYearSearch_syllabus" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>เลขที่เอกสาร / ชื่อหลักสูตร</label>
                                <asp:TextBox ID="txtFilterKeyword_syllabus" runat="server"
                                    CssClass="form-control"
                                    placeholder="เลขที่เอกสาร / ชื่อหลักสูตร..." />
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <label>
                                &nbsp;
                            </label>
                            <div class="form-group">
                                <asp:LinkButton ID="btnS_syllabus" runat="server" CssClass="btn btn-primary" Text="<i class='fa fa-search'></i> ค้นหา"
                                    OnCommand="btnCommand" CommandName="btnS_syllabus" />
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <div class="row">

                <asp:GridView ID="GvsyllabusList"
                    runat="server"
                    AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered table-responsive col-md-12 word-wrap"
                    HeaderStyle-CssClass="info"
                    AllowPaging="true"
                    PageSize="10"
                    OnRowCommand="onRowCommand"
                    OnRowDataBound="onRowDataBound"
                    OnPageIndexChanging="gvPageIndexChanging"
                    DataKeyNames="u0_course_idx"
                    ShowFooter="false"
                    Font-Size="Small">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />

                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่พบข้อมูล</div>
                    </EmptyDataTemplate>
                    <Columns>


                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                            <ItemTemplate>
                                <div class="word-wrap">
                                    <%# (Container.DataItemIndex + 1) %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="เลขที่เอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="9%">
                            <ItemTemplate>
                                <div class="word-wrap">
                                    <%# Eval("course_no") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="วันที่สร้างเอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                            <ItemTemplate>
                                <div class="word-wrap">
                                    <%# Eval("course_date") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="รายละเอียดหลักสูตร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="35%">
                            <ItemTemplate>
                                <div class="word-wrap">
                                    <strong>
                                        <asp:Label ID="Label60" runat="server"> หลักสูตร: </asp:Label></strong>
                                    <asp:Literal ID="Literal6" runat="server" Text='<%# Eval("course_name") %>' />
                                    <br />
                                    <strong>
                                        <asp:Label ID="Label61" runat="server"> กลุ่มวิชา: </asp:Label></strong>
                                    <asp:Literal ID="Literal8" runat="server" Text='<%# Eval("training_group_name") %>' />
                                    <br />
                                    <strong>
                                        <asp:Label ID="Label22" runat="server"> สาขาวิชา: </asp:Label></strong>
                                    <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("training_branch_name") %>' />
                                    <br />
                                    <strong>
                                        <asp:Label ID="Label23" runat="server"> ประเภท: </asp:Label></strong>
                                    <asp:Literal ID="Literal2" runat="server"
                                        Text='<%# getCourseType((int)Eval("course_type_etraining"),
                                                        (int)Eval("course_type_elearning")
                                                        ) %>' />

                                    <br />
                                    <strong>
                                        <asp:Label ID="Label13" runat="server"> สถานที่อบรม: </asp:Label></strong>
                                    <asp:Literal ID="Literal5" runat="server" Text='<%# getplace_type((int)Eval("place_type")) %>' />


                                    <br />

                                    <strong>
                                        <asp:Label ID="Label27" runat="server"> ระดับความสำคัญ: </asp:Label></strong>
                                    <asp:Literal ID="Literal3" runat="server"
                                        Text='<%# getpriority(int.Parse((string)Eval("course_priority"))) %>' />

                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="แบบทดสอบ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                            <ItemTemplate>
                                <div class="word-wrap">
                                    <%#  getStatustest((int)Eval("u2_course_test_item")) %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="แบบประเมิน" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                            <ItemTemplate>
                                <div class="word-wrap">
                                    <%# getStatusEvalue((int)Eval("u3_course_evaluation_item")) %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="คะแนน" ItemStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                            <ItemTemplate>
                                <%# string.Format("{0:n2}",Eval("course_score")) %>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                            <ItemTemplate>
                                <div class="word-wrap">
                                    <asp:LinkButton ID="btnDetail_GvsyllabusList"
                                        CssClass="btn btn-info btn-sm" runat="server"
                                        data-original-title="รายละเอียด" data-toggle="tooltip" Text="รายละเอียด"
                                        OnCommand="btnCommand" CommandName="btnDetail_GvsyllabusList"
                                        CommandArgument='<%# Eval("u0_course_idx") %>'>
                                    <i class="fa fa-file-alt"></i></asp:LinkButton>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>

            </div>

        </asp:View>


        <asp:View ID="View_SCHEDList" runat="server">

            <asp:Panel ID="Panel12" runat="server" Visible="true">
                <div class="panel panel-primary m-t-10">
                    <div class="panel-heading f-bold">ค้นหา</div>
                    <div class="panel-body">

                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>ปี</label>
                                <asp:DropDownList ID="ddlYearSearch_SCHED" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>เลขที่เอกสาร / ชื่อหลักสูตร</label>
                                <asp:TextBox ID="txtFilterKeyword_SCHED" runat="server"
                                    CssClass="form-control"
                                    placeholder="เลขที่เอกสาร / ชื่อหลักสูตร..." />
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>กลุ่มวิชา</label>
                                <asp:DropDownList ID="ddltrn_groupSearch_SCHED" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <label>
                                &nbsp;
                            </label>
                            <div class="form-group">
                                <asp:LinkButton ID="btnS_SCHED" runat="server" CssClass="btn btn-primary" Text="<i class='fa fa-search'></i> ค้นหา"
                                    OnCommand="btnCommand" CommandName="btnS_SCHED" />
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <div class="col-md-11 col-md-offset-1">
                <asp:Panel ID="panel13" runat="server" CssClass="text-right m-b-10 hidden-print">
                    <%--<button
                        onclick="tableToExcel('Tableprint_sched', 'ตารางแผนการอบรม')"
                        class="btn btn-primary">
                        <i class='fa fa-file-excel'></i>
                        Export Excel
                    </button>--%>

                    <%--<button id="btnExport" onclick="fnExcelReport();"> EXPORT </button>--%>

                    <asp:LinkButton ID="btnprint_sched" CssClass="btn btn-primary" runat="server"
                        Text="<i class='fa fa-print'></i> พิมพ์" OnClientClick="return printDiv('print_sched');" />
                </asp:Panel>
            </div>
            <div id="print_sched" class="row">

                <table id="Tableprint_sched"
                    class="table table-bordered">
                    <thead>
                        <tr style="background-color: #e7e7e7; padding: 5px 0px;">

                            <% for (int iWeek = 1; iWeek <= 12; iWeek++)
                                {
                            %>
                            <th style="width: 80px;">
                                <center>
                                <%= MonthTH(iWeek) %>
                                    </center>
                            </th>
                            <% 
                                } %>
                        </tr>
                    </thead>
                    <tbody>

                        <%= getHtmlSched() %>
                    </tbody>

                </table>


            </div>

            <br />

        </asp:View>

        <asp:View ID="View_summarycourse" runat="server">

            <asp:FormView ID="fv_summarycourse" runat="server" DefaultMode="Edit" Width="100%">
                <EditItemTemplate>


                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-file"></i><strong>&nbsp; 
                                        <asp:Label ID="lbtitle_preview" runat="server" Text="รายละเอียดหลักสูตร"></asp:Label>
                            </strong></h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <asp:Panel ID="Panel5" runat="server" Visible="false">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:Label ID="Label20" class="col-md-2 control-labelnotop text_right" runat="server" Text="รหัสหลักสูตร :" />
                                                <div class="col-md-4">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>

                                <div class="form-group" runat="server" visible="true">
                                    <asp:Label ID="Label123" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="เลขที่เอกสาร : " />
                                    <div class="col-md-10">
                                        <asp:TextBox ID="txtu3_training_course_idx" runat="server"
                                            Visible="false"
                                            Text='<%# Eval("u3_training_course_idx") %>'>
                                        </asp:TextBox>
                                        <asp:TextBox ID="txtu0_training_course_idx_ref" runat="server"
                                            Visible="false"
                                            Text='<%# Eval("u0_training_course_idx_ref") %>'>
                                        </asp:TextBox>
                                        <asp:Label ID="txttraining_course_no" runat="server"
                                            CssClass="f-s-13 control-label"
                                            Text='<%# Eval("training_course_no") %>' />

                                    </div>
                                </div>

                                <div class="form-group" runat="server" visible="true">

                                    <asp:Label ID="Label3" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="วันที่เอกสาร :" />
                                    <div class="col-md-10">
                                        <div class='input-group date'>
                                            <asp:Label ID="txttraining_course_date" runat="server"
                                                CssClass="f-s-13 control-label"
                                                Text='<%# Eval("zdate") %>' />
                                        </div>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label29" class="col-md-2 control-labelnotop text_right" runat="server" Text="วันที่เริ่มอบรม :" />
                                    <div class="col-sm-10">
                                        <asp:Label ID="Label8" runat="server"
                                            CssClass="f-s-13 control-label"
                                            Text='<%# Eval("zdate_start")+"   ถึง   "+Eval("zdate_end")
                                                      +"   เวลา :   "+Eval("ztime_start")+"   ถึง   "+Eval("ztime_end")
                                                    
                                                      %>' />


                                    </div>

                                </div>

                                <div class="form-group">

                                    <asp:Label ID="Label12" class="col-md-2 control-labelnotop text_right" runat="server" Text="ชื่อหลักสูตร :" />
                                    <div class="col-md-9">
                                        <asp:Label ID="Label2" runat="server"
                                            CssClass="f-s-13 control-label"
                                            Text='<%# Eval("course_name") %>' />
                                    </div>

                                </div>

                                <div class="form-group">

                                    <asp:Label ID="Label87" class="col-md-2 control-labelnotop text_right" runat="server" Text="สถานที่อบรม :" />
                                    <div class="col-md-4">
                                        <asp:Label ID="Label6" runat="server"
                                            Text='<%# getValue_place((int)Eval("place_idx_ref")) %>'
                                            CssClass="f-s-13 control-label">
                                        </asp:Label>

                                    </div>

                                </div>

                                <div class="form-group">

                                    <asp:Label ID="Label10" class="col-md-2 control-labelnotop text_right" runat="server" Text="วิทยากร :" />
                                    <div class="col-md-9">
                                        <asp:Label ID="Label7" runat="server"
                                            Text='<%# getValue_lecturer_type((int)Eval("lecturer_type")) %>'
                                            CssClass="f-s-13 control-label">
                                        </asp:Label>


                                    </div>

                                </div>

                                <asp:Panel ID="Panel2" runat="server" Visible="false">
                                    <div class="form-group">

                                        <asp:Label ID="Label28" class="col-md-2 control-labelnotop text_right" runat="server" Text=" " />
                                        <div class="col-md-9">
                                            <div>

                                                <div class="panel-info">
                                                    <div class="panel-heading f-bold">รายละเอียดวิทยากร</div>
                                                </div>
                                                <asp:GridView ID="Gvinstitution" runat="server"
                                                    CssClass="table table-bordered word-wrap"
                                                    GridLines="None" OnRowCommand="onRowCommand"
                                                    AutoGenerateColumns="false"
                                                    Font-Size="Small">
                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">ไม่พบข้อมูล</div>
                                                    </EmptyDataTemplate>
                                                    <Columns>

                                                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                                            HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center"
                                                            HeaderStyle-Width="5%">
                                                            <ItemTemplate>
                                                                <%# (Container.DataItemIndex +1) %>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField
                                                            HeaderText="ชื่อ - สกุล"
                                                            ItemStyle-HorizontalAlign="Left"
                                                            HeaderStyle-CssClass="text-center"
                                                            ItemStyle-Font-Size="Small">
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtinstitution_name_edit_L" runat="server" Text='<%# Bind("zName") %>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="txtinstitution_name_item_L" runat="server" Text='<%# Eval("zName") %>'
                                                                    Width="100%"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>

                                    </div>
                                </asp:Panel>

                            </div>
                        </div>
                    </div>

                </EditItemTemplate>
            </asp:FormView>

            <asp:FormView ID="fv_summaryLearner" runat="server" DefaultMode="Edit" Width="100%">
                <EditItemTemplate>
                    <asp:TextBox ID="txtu6_training_course_idx" runat="server"
                        Visible="false" Text='<%# Eval("u6_training_course_idx") %>' />
                    <asp:TextBox ID="txtu3_training_course_idx" runat="server"
                        Visible="false" Text='<%# Eval("u3_training_course_idx_ref") %>' />
                    <asp:TextBox ID="txtemp_idx_ref" runat="server"
                        Visible="false" Text='<%# Eval("emp_idx_ref") %>' />
                    <asp:TextBox ID="txtu0_training_course_idx_ref" runat="server"
                        Visible="false"
                        Text='<%# Eval("u0_training_course_idx_ref") %>'>
                    </asp:TextBox>
                    <asp:TextBox ID="txtsumm_no" runat="server"
                        Visible="false"
                        Text='<%# Eval("summ_no") %>'>
                    </asp:TextBox>

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-file"></i><strong>&nbsp; 
                                        <asp:Label ID="lbtitle_preview" runat="server" Text="ผลการฝึกอบรม"></asp:Label>
                            </strong></h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">

                                <div class="form-group">
                                    <asp:Label ID="Label29" class="col-md-2 control-labelnotop text_right" runat="server" Text="รายงานสรุปเนื้อหาการฝึกอบรม :" />
                                    <div class="col-sm-9">
                                        <asp:Literal ID="littraining_summary_report_remark" runat="server"
                                            Text='<%# Eval("training_summary_report_remark") %>'
                                            Visible="false">
                                        </asp:Literal>
                                        <asp:TextBox ID="txttraining_summary_report_remark" runat="server"
                                            CssClass="f-s-13 form-control"
                                            Rows="13"
                                            TextMode="MultiLine"
                                            Text='<%# Eval("training_summary_report_remark") %>' />

                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-2">
                                        <asp:Label ID="Label90" class="control-labelnotop pull-right" runat="server" Text="ไฟล์เอกสาร :" />
                                    </div>

                                    <div class="col-md-2">
                                        <asp:Panel ID="pnl_file" runat="server">
                                            <input type="file" id="txtfileimport"
                                                onchange="imageproposUpload(this)">

                                            <asp:TextBox ID="txttraining_course_file_name" runat="server"
                                                Visible="false"
                                                Text='<%# Eval("training_course_file_name") %>'>
                                            </asp:TextBox>

                                        </asp:Panel>
                                        <img alt="" id="imgfile_data" class="pull-left"
                                            style="height: 30px; width: 30px;"
                                            src='<%= getImg() %>' />
                                        <label><%= getImgFileType() %></label>

                                    </div>
                                    <div class="col-md-4">
                                        <asp:Panel ID="pnl_file_btn" runat="server">
                                            <asp:LinkButton ID="btninvestigatefile"
                                                CssClass="btn-primary btn-sm" runat="server"
                                                data-original-title="ตรวจสอบไฟล์" data-toggle="tooltip" Text="ตรวจสอบไฟล์"
                                                OnCommand="btnCommand" CommandName="">
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="btnClearfile"
                                                CssClass="btn-warning btn-sm" runat="server"
                                                data-original-title="เคลียร์ไฟล์" data-toggle="tooltip" Text="เคลียร์ไฟล์"
                                                OnCommand="btnCommand" CommandName="btnClearfile">
                                            </asp:LinkButton>

                                            <asp:LinkButton ID="btnDeletfileMAS"
                                                CssClass="btn-danger btn-sm" runat="server"
                                                data-original-title="ลบไฟล์" data-toggle="tooltip" Text="ลบไฟล์"
                                                OnCommand="btnCommand" CommandName="btnDeletfileMAS">
                                            </asp:LinkButton>

                                        </asp:Panel>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label124" class="col-md-2 control-labelnotop text_right" runat="server" Text="ประโยชน์ที่ได้รับจากการฝึกอบรม :" />
                                    <div class="col-sm-9">
                                        <asp:Literal ID="littraining_benefits_remark" runat="server"
                                            Text='<%# Eval("training_benefits_remark") %>'
                                            Visible="false">
                                        </asp:Literal>
                                        <asp:TextBox ID="txttraining_benefits_remark" runat="server"
                                            CssClass="f-s-13 form-control"
                                            Rows="13"
                                            TextMode="MultiLine"
                                            Text='<%# Eval("training_benefits_remark") %>' />
                                    </div>
                                </div>
                                <asp:Panel ID="pnl_super" runat="server">
                                    <hr />

                                    <div class="form-group">
                                        <asp:Label ID="Label125" class="col-md-2 control-labelnotop text_right" runat="server" Text="ความเห็นเพิ่มเติมของผู้บังคับบัญชาตามสายงาน :" />
                                        <div class="col-sm-9">
                                            <asp:Literal ID="litsupervisors_additional" runat="server"
                                                Text='<%# Eval("supervisors_additional") %>'
                                                Visible="false">
                                            </asp:Literal>
                                            <asp:TextBox ID="txtsupervisors_additional" runat="server"
                                                CssClass="f-s-13 form-control"
                                                Rows="13"
                                                TextMode="MultiLine"
                                                Text='<%# Eval("supervisors_additional") %>' />
                                        </div>
                                    </div>
                                </asp:Panel>

                            </div>
                        </div>
                    </div>

                </EditItemTemplate>
            </asp:FormView>

            <%--<asp:UpdatePanel ID="updatepanel3" runat="server">
                <ContentTemplate>
            --%>
            <div class="row">
                <div class="form-group">
                    <div class="col-md-12">

                        <div class="col-md-2">
                        </div>
                        <div class="col-md-10">
                            <asp:LinkButton CssClass="btn btn-success" runat="server"
                                CommandName="btnSave_summary" OnCommand="btnCommand"
                                data-toggle="tooltip" title="บันทึก"
                                ID="btnSave_summary"
                                ValidationGroup="btnSaveInsert">
                                   <i class="fa fa-save fa-lg"></i> <%= getlabelSave() %>
                            </asp:LinkButton>
                            <asp:LinkButton CssClass="btn btn-danger"
                                data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                                Text="<i class='fa fa-angle-left fa-lg'></i> ย้อนกลับ"
                                ID="btnsummary_back"
                                CommandName="btnsummary_back" OnCommand="btnCommand" />


                        </div>

                    </div>
                </div>
            </div>
            <%-- </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnSave_summary" />
                    <asp:PostBackTrigger ControlID="btnsummary_back" />
                    <asp:PostBackTrigger ControlID="LinkButton3" />
                </Triggers>
            </asp:UpdatePanel>--%>



            <br />

        </asp:View>

        <asp:View ID="View_elearning" runat="server">

            <asp:FormView ID="FV_elearning" runat="server" DefaultMode="Insert" Width="100%">
                <InsertItemTemplate>


                    <div class="panel panel-default">

                        <div class="panel-body">
                            <div class="form-horizontal" role="form">

                                <%-- start video --%>

                                <div class="row">
                                    <div class="col-md-12">

                                        <section class="video-player">
                                            <p>
                                                <b>
                                                    <i class="fa fa-list-ul" aria-hidden="true"></i>&nbsp;
                                                <asp:Label ID="lb_title_video" runat="server" Text="title" Style="font-size: medium;"></asp:Label>
                                                </b>
                                            </p>
                                            <%-- --%>
                                            <video id="myVideo" onmousedown="DisableRightClick(event)"
                                                poster='<%= url_video_image %>'
                                                style="height: auto; width: 100%; margin: 0 0; display: block; border: 3px solid #D9B4B4;">
                                                <source src='<%= url_video %>' type="video/mp4" />
                                            </video>

                                        </section>

                                    </div>
                                </div>

                                <hr />

                                <div class="row" id="dv_detail">

                                    <div class="col-md-12">
                                        <p>
                                            V <span id="video_item"><%= getvideo_item %></span> / 
                                    <span id="item_count"><%= getitem_count %></span>
                                            จำนวนที่ดู <span id="item_count_std"><%= getitem_count_std %></span>
                                            หัวข้อ <span id="video_title"><%= getvideo_title %></span>
                                        </p>
                                    </div>
                                    <div class="col-md-12">
                                        <p>รายละเอียด : <span id="video_description"><%= getvideo_description %></span></p>
                                    </div>

                                </div>

                                <%-- <hr />--%>
                                <div id="dv_crontroll">
                                    <div class="row">
                                        <div class="col-md-12">

                                            <%--<p>
                                                ระยะเวลา <span id="current_time">0:00</span> / <span id="duration">0:00</span>

                                            </p>--%>
                                            <%--<p>
                                                เวลาที่ดูล่าสุด <span id="current_time_std"><%= getcurrent_time_std %></span>
                                                <span id="current_time_std_int" style="visibility: hidden"><%= getcurrent_time_std_int %></span>
                                            </p>--%>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-inline">
                                            <div class="col-md-6">


                                                <asp:UpdatePanel ID="updatepanel3" runat="server">
                                                    <ContentTemplate>

                                                        <button id="btn_restart" onclick="restart()" class="btn btn-default" type="button"><i class="glyphicon glyphicon-refresh" title="refresh" data-toggle="tooltip"></i></button>

                                                        <button id="btn_play" onclick="playVid()" class="btn btn-default" type="button"><i class="glyphicon glyphicon-play" title="play" data-toggle="tooltip"></i></button>

                                                        <button id="btn_pause" onclick="pauseVid()" class="btn btn-default" type="button"><i class="glyphicon glyphicon-pause" title="Pause" data-toggle="tooltip"></i></button>

                                                        <button id="btn_stepbackward" onclick="stepbackward()" class="btn btn-default" type="button"><i class="glyphicon glyphicon-step-backward" title="Previous" data-toggle="tooltip"></i></button>

                                                        <input id="txt_steptime" type="number" style="width: 100px" value="30" class="form-control" />

                                                        <button id="btn_stepforward" onclick="stepforward()" class="btn btn-default" type="button"><i class="glyphicon glyphicon-step-forward" title="Next" data-toggle="tooltip"></i></button>

                                                        <button id="btn_timelast" onclick="timelast()" class="btn btn-default" type="button"><i class="glyphicon glyphicon-menu-hamburger" title="ไปเวลาที่ดูล่าสุด" data-toggle="tooltip"></i></button>


                                                        <asp:LinkButton runat="server" ID="btn_menuleft"
                                                            CssClass="btn btn-default"
                                                            CommandName="btn_menuleft" OnCommand="btnCommand"><i class="glyphicon glyphicon-menu-left" title="กลับไปวิดีโอก่อนหน้านี้" data-toggle="tooltip"></i></asp:LinkButton>

                                                        <asp:LinkButton runat="server" ID="btn_menuright"
                                                            CommandName="btn_menuright" OnCommand="btnCommand"
                                                            CssClass="btn btn-default"><i class="glyphicon glyphicon-menu-right" title="กลับไปวิดีโอก่อนหน้านี้" data-toggle="tooltip"></i></asp:LinkButton>

                                                        <button id="btn_fullscreen" onclick="func_fullscreen()" class="btn btn-default" type="button"><i class="glyphicon glyphicon-fullscreen" title="Fullscreen" data-toggle="tooltip"></i></button>

                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="btn_menuleft" />
                                                        <asp:PostBackTrigger ControlID="btn_menuright" />
                                                    </Triggers>
                                                </asp:UpdatePanel>

                                            </div>
                                            <div class="col-md-6 ">
                                                <%--<p>--%>
                                                <i class="fa fa-clock-o"></i>
                                                            ระยะเวลา <span id="current_time">0:00</span> / <span id="duration">0:00</span>

                                                <%--</p>--%>
                                                <%--<p>--%>
                                                
                                                            เวลาที่ดูล่าสุด <span id="current_time_std"><%= getcurrent_time_std %></span>
                                                
                                                <%--</p>--%>
                                                <i class="fa fa-clock-o"></i>
                                                <span id="current_time_std_int" style="visibility: hidden"><%= getcurrent_time_std_int %></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr />

                                <div id="dv_elearning_back">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:UpdatePanel ID="updatepanel5" runat="server">
                                                    <ContentTemplate>

                                                        <asp:LinkButton CssClass="btn btn-danger"
                                                            data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                                                            Text="<i class='fa fa-angle-left fa-lg'></i> ย้อนกลับ"
                                                            ID="btncancel_elearning"
                                                            CommandName="btnHR_App" OnCommand="btnCommand" />

                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="btncancel_elearning" />
                                                    </Triggers>
                                                </asp:UpdatePanel>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </InsertItemTemplate>
            </asp:FormView>


            <br />



        </asp:View>



    </asp:MultiView>




    <asp:HiddenField ID="Hddfld_status" runat="server" />
    <asp:HiddenField ID="Hddfld_training_course_no" runat="server" />
    <asp:HiddenField ID="Hddfld_u0_training_course_idx" runat="server" />
    <asp:HiddenField ID="Hddfld_folder" runat="server" />
    <asp:HiddenField ID="Hddfld_emp_idx" runat="server" />
    <asp:HiddenField ID="Hddfld_myVideo" runat="server" Value="" />
    <asp:TextBox ID="txtemp_idx" runat="server" Visible="false"></asp:TextBox>


    <%-- start show modal --%>


    <div class="col-lg-12" runat="server" id="DivEdit_IT">
        <div id="dv_Detailsyllabus" class="modal open" role="dialog">
            <div class="modal-dialog" style="width: 70%;">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">หลักสูตรที่จะต้องเรียน
                        </h4>
                    </div>
                    <div class="modal-body">
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="panel-heading">

                                    <asp:FormView ID="FvDetailsyllabus" runat="server" DefaultMode="Edit" Width="100%">
                                        <EditItemTemplate>

                                            <%--<div class="form-group">
                                                <div class="col-sm-8">
                                                   
                                                </div>
                                            </div>--%>

                                            <div class="form-group">
                                                <asp:Label ID="Label1" CssClass="col-sm-2 control-labelnotop text_right f-bold" runat="server" Text="เลขที่เอกสาร :" />
                                                <div class="col-sm-4">
                                                    <asp:Label ID="txteveldevice" CssClass="ff-s-13 control-label" runat="server" Text='<%# Eval("course_no") %>'></asp:Label>

                                                </div>
                                                <asp:Label ID="Label58" CssClass="col-md-2 control-labelnotop text_right f-bold" runat="server" Text="วันที่สร้างเอกสาร :" />
                                                <div class="col-sm-4">
                                                    <asp:Label ID="TextBox1" CssClass="f-s-13 control-label" runat="server" Text='<%# Eval("course_date") %>'></asp:Label>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label5" CssClass="col-md-2 control-labelnotop text_right f-bold" runat="server" Text="หลักสูตร :" />
                                                <div class="col-sm-8">
                                                    <asp:Label ID="txtname" CssClass="f-s-13 control-label" runat="server" Text='<%# Eval("course_name") %>'></asp:Label>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label33" CssClass="col-sm-2 control-labelnotop text_right f-bold" runat="server" Text="กลุ่มวิชา :" />
                                                <div class="col-sm-4">
                                                    <asp:Label ID="Label36" CssClass="ff-s-13 control-label" runat="server" Text='<%# Eval("training_group_name") %>'></asp:Label>

                                                </div>
                                                <asp:Label ID="Label37" CssClass="col-md-2 control-labelnotop text_right f-bold" runat="server" Text="สาขาวิชา :" />
                                                <div class="col-sm-4">
                                                    <asp:Label ID="Label43" CssClass="f-s-13 control-label" runat="server" Text='<%# Eval("training_branch_name") %>'></asp:Label>

                                                </div>
                                            </div>

                                            <div class="form-group">

                                                <asp:Label ID="Label41" class="col-sm-2 control-labelnotop text_right f-bold" runat="server" Text="รายละเอียด :" />
                                                <div class="col-sm-8">
                                                    <asp:Label ID="txttraining_course_remark" runat="server"
                                                        Text='<%# Eval("course_remark") %>'
                                                        CssClass="f-s-13 control-label"
                                                        TextMode="MultiLine"
                                                        Rows="4">
                                                    </asp:Label>
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label47" CssClass="col-sm-2 control-labelnotop text_right f-bold" runat="server" Text="ระดับความยากของหลักสูตร :" />
                                                <div class="col-sm-4">
                                                    <asp:Label ID="Label48" CssClass="ff-s-13 control-label" runat="server" Text='<%# Eval("level_name") %>'></asp:Label>

                                                </div>
                                                <asp:Label ID="Label49" CssClass="col-md-2 control-labelnotop text_right f-bold" runat="server" Text="คะแนน :" />
                                                <div class="col-sm-4">
                                                    <asp:Label ID="Label50" CssClass="f-s-13 control-label" runat="server"
                                                        Text='<%# string.Format("{0:n2}",Eval("course_score")) %>'></asp:Label>

                                                </div>
                                            </div>


                                            <div class="form-group">

                                                <asp:Label ID="Label51" class="col-sm-2 control-labelnotop text_right f-bold" runat="server" Text="ต้องผ่านการเรียนหลักสูตร :" />
                                                <div class="col-sm-8">
                                                    <%--<div class="panel-info">
                                                    <div class="panel-heading f-bold">ต้องผ่านการเรียนหลักสูตร</div>
                                                </div>--%>
                                                    <asp:GridView ID="GvCourse" runat="server"
                                                        CssClass="table table-striped f-s-12 table-empshift-responsive word-wrap"
                                                        GridLines="None" OnRowCommand="onRowCommand"
                                                        AutoGenerateColumns="false"
                                                        Font-Size="Small">
                                                        <EmptyDataTemplate>
                                                            <div style="text-align: center">ไม่พบข้อมูล</div>
                                                        </EmptyDataTemplate>
                                                        <Columns>

                                                            <asp:TemplateField HeaderText="หลักสูตร" ItemStyle-HorizontalAlign="Left"
                                                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                <ItemTemplate>

                                                                    <asp:Label CssClass="control-label" ID="GvCourse_zName" runat="server" Text='<%# Eval("zName") %>' />

                                                                </ItemTemplate>
                                                                <EditItemTemplate />
                                                                <FooterTemplate />
                                                            </asp:TemplateField>

                                                        </Columns>
                                                    </asp:GridView>
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label52" CssClass="col-sm-2 control-labelnotop text_right f-bold" runat="server" Text="ประเภท :" />
                                                <div class="col-sm-4">
                                                    <asp:Label ID="Label53" CssClass="ff-s-13 control-label" runat="server"
                                                        Text='<%# getCourseType((int)Eval("course_type_etraining"),
                                                        (int)Eval("course_type_elearning")
                                                        ) %>'></asp:Label>

                                                </div>
                                                <asp:Label ID="Label54" CssClass="col-md-2 control-labelnotop text_right f-bold" runat="server" Text="ระดับความสำคัญ :" />
                                                <div class="col-sm-4">
                                                    <asp:Label ID="Label80" CssClass="f-s-13 control-label" runat="server"
                                                        Text='<%# Eval("priority_name")+" : "+Eval("priority_remark") %>'></asp:Label>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label81" CssClass="col-sm-2 control-labelnotop text_right f-bold" runat="server" Text="การประเมิน :" />
                                                <div class="col-sm-4">
                                                    <asp:Label ID="Label85" CssClass="ff-s-13 control-label" runat="server" Text='<%# getcourse_assessment((int)Eval("course_assessment")) %>'></asp:Label>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label89" CssClass="col-sm-2 control-labelnotop text_right f-bold" runat="server" Text="แบบทดสอบ :" />
                                                <div class="col-sm-4">
                                                    <asp:Label ID="Label90" CssClass="ff-s-13 control-label" runat="server"
                                                        Text='<%# Eval("u2_course_test_item").ToString() == "0" ? "-" : string.Format("{0:n0}",Eval("u2_course_test_item")) +" ข้อ " %>'></asp:Label>

                                                </div>
                                                <asp:Label ID="Label91" CssClass="col-md-2 control-labelnotop text_right f-bold" runat="server" Text="แบบประเมิน" />
                                                <div class="col-sm-4">
                                                    <asp:Label ID="Label92" CssClass="f-s-13 control-label" runat="server"
                                                        Text='<%# Eval("u3_course_evaluation_item").ToString() == "0" ? "-" : string.Format("{0:n0}",Eval("u3_course_evaluation_item")) +" ข้อ " %>'></asp:Label>

                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-10">
                                                    <asp:LinkButton ID="lbCmdCancel" class="btn btn-default btn-sm" data-toggle="tooltip" title="Close" runat="server" data-dismiss="modal" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>

                                                </div>
                                            </div>


                                        </EditItemTemplate>
                                    </asp:FormView>
                                </div>

                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>


    <div class="col-lg-12" runat="server" id="Div2">
        <div id="DvRemarkRePrint" class="modal open" role="dialog">
            <div class="modal-dialog" style="width: 70%;">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">ช่องแสดงเหตุผลในการ Reprint</h4>
                    </div>
                    <div class="modal-body">
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="row">
                                    <div class="form-group">
                                        <asp:Label ID="Label22" runat="server" Text="เหตุผล" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtRemarkReprint" TextMode="multiline" Rows="5" runat="server" CssClass="form-control embed-responsive-item" PlaceHoldaer="........" />

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-10">
                                            <asp:Label ID="Label23" runat="server" Text="*** ถ้าไม่ใส่เหตุผลจะไม่สามารถ Reprint ได้ !"
                                                ForeColor="Red"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-10">

                                            <asp:LinkButton ID="Cancel" class="btn btn-default btn-sm" data-toggle="tooltip" title="Close" runat="server" data-dismiss="modal" CommandName="Cancel">&nbsp;Close</asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%-- schedule show --%>
    <div class="col-lg-12" runat="server" id="Div1">
        <div id="dv_Detailschedule_show" class="modal open" role="dialog">
            <div class="modal-dialog" style="width: 70%;">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">รายละเอียดหลักสูตร
                        </h4>
                    </div>
                    <div class="modal-body">
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="panel-heading">

                                    <input type="text" style="visibility: hidden" name="dlgSch_txtemp_id" id="dlgSch_txtu0_training_plan_idx" />
                                    <input type="text" style="visibility: hidden" name="dlgSch_txtemp_id" id="dlgSch_txtemp_id" />
                                    <input type="text" style="visibility: hidden" name="dlgSch_txtu0_training_course_idx" id="dlgSch_txtu0_training_course_idx" />
                                    <input type="text" style="visibility: hidden" name="dlgSch_txtu3_training_course_idx" id="dlgSch_txtu3_training_course_idx" />
                                    <div class="form-group">
                                        <asp:Label ID="Label1" CssClass="col-sm-2 control-labelnotop text_right f-bold" runat="server" Text="เลขที่เอกสาร :" />
                                        <div class="col-sm-4">
                                            <label id="dlgSch_txtcourse_no" />
                                        </div>
                                        <asp:Label ID="Label58" CssClass="col-md-2 control-labelnotop text_right f-bold" runat="server" Text="วันที่สร้างเอกสาร :" />
                                        <div class="col-sm-4">
                                            <label id="dlgSch_course_date" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label5" CssClass="col-md-2 control-labelnotop text_right f-bold" runat="server" Text="หลักสูตร :" />
                                        <div class="col-sm-8">
                                            <label id="dlgSch_course_name" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label33" CssClass="col-sm-2 control-labelnotop text_right f-bold" runat="server" Text="กลุ่มวิชา :" />
                                        <div class="col-sm-4">
                                            <label id="dlgSch_training_group_name" />
                                        </div>
                                        <asp:Label ID="Label37" CssClass="col-md-2 control-labelnotop text_right f-bold" runat="server" Text="สาขาวิชา :" />
                                        <div class="col-sm-4">
                                            <label id="dlgSch_training_branch_name" />
                                        </div>
                                    </div>

                                    <div class="form-group">

                                        <asp:Label ID="Label41" class="col-sm-2 control-labelnotop text_right f-bold" runat="server" Text="รายละเอียด :" />
                                        <div class="col-sm-8">
                                            <label id="dlgSch_course_remark" />
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label47" CssClass="col-sm-2 control-labelnotop text_right f-bold" runat="server" Text="ระดับความยากของหลักสูตร :" />
                                        <div class="col-sm-4">
                                            <label id="dlgSch_level_name" />
                                        </div>
                                        <asp:Label ID="Label49" CssClass="col-md-2 control-labelnotop text_right f-bold" runat="server" Text="คะแนน :" />
                                        <div class="col-sm-4">
                                            <label id="dlgSch_course_score" />

                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <asp:Label ID="Label52" CssClass="col-sm-2 control-labelnotop text_right f-bold" runat="server" Text="ประเภท :" />
                                        <div class="col-sm-4">
                                            <label id="dlgSch_course_type" />

                                        </div>
                                        <asp:Label ID="Label54" CssClass="col-md-2 control-labelnotop text_right f-bold" runat="server" Text="ระดับความสำคัญ :" />
                                        <div class="col-sm-4">
                                            <label id="dlgSch_priority_name"></label>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label81" CssClass="col-sm-2 control-labelnotop text_right f-bold" runat="server" Text="การประเมิน :" />
                                        <div class="col-sm-4">
                                            <label id="dlgSch_course_assessment"></label>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label89" CssClass="col-sm-2 control-labelnotop text_right f-bold" runat="server" Text="แบบทดสอบ :" />
                                        <div class="col-sm-4">
                                            <label id="dlgSch_u2_course_test_item"></label>

                                        </div>
                                        <asp:Label ID="Label91" CssClass="col-md-2 control-labelnotop text_right f-bold" runat="server" Text="แบบประเมิน" />
                                        <div class="col-sm-4">
                                            <label id="dlgSch_u3_course_evaluation_item"></label>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label122" CssClass="col-sm-2 control-labelnotop text_right f-bold" runat="server" Text="สถานะ :" />
                                        <div class="col-sm-10">
                                            <label id="dlgSch_status_name" style='color: #26A65B;'></label>

                                        </div>
                                    </div>

                                    <asp:UpdatePanel ID="updatepanel" runat="server">
                                        <ContentTemplate>
                                            <div class="form-group">
                                                <div class="col-sm-4 col-sm-offset-8">

                                                    <asp:LinkButton ID="lbCmdregister_cancel" class="btn btn-default btn-sm"
                                                        data-toggle="tooltip" title="Close" runat="server"
                                                        data-dismiss="modal" CommandName="Cancel"
                                                        Text='<i class="glyphicon glyphicon-remove"></i>'>
                                                    </asp:LinkButton>

                                                </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="lbCmdregister_ok" />
                                        </Triggers>
                                    </asp:UpdatePanel>




                                </div>

                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>

    <%-- schedule register --%>
    <div class="col-lg-12" runat="server" id="Div3">
        <div id="dv_Detailschedule_register" class="modal open" role="dialog">
            <div class="modal-dialog" style="width: 70%;">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">รายละเอียดหลักสูตร
                        </h4>
                    </div>
                    <div class="modal-body">
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="panel-heading">

                                    <input type="text" style="visibility: hidden" name="reg_dlgSch_txtu0_training_plan_idx" id="reg_dlgSch_txtu0_training_plan_idx" />
                                    <input type="text" style="visibility: hidden" name="reg_dlgSch_txtemp_id" id="reg_dlgSch_txtemp_id" />
                                    <input type="text" style="visibility: hidden" name="reg_dlgSch_txtu0_training_course_idx" id="reg_dlgSch_txtu0_training_course_idx" />
                                    <input type="text" style="visibility: hidden" name="reg_dlgSch_txtu3_training_course_idx" id="reg_dlgSch_txtu3_training_course_idx" />
                                    <div class="form-group">
                                        <asp:Label ID="Label96" CssClass="col-sm-2 control-labelnotop text_right f-bold" runat="server" Text="เลขที่เอกสาร :" />
                                        <div class="col-sm-4">
                                            <label id="reg_dlgSch_txtcourse_no" />
                                        </div>
                                        <asp:Label ID="Label97" CssClass="col-md-2 control-labelnotop text_right f-bold" runat="server" Text="วันที่สร้างเอกสาร :" />
                                        <div class="col-sm-4">
                                            <label id="reg_dlgSch_course_date" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label98" CssClass="col-md-2 control-labelnotop text_right f-bold" runat="server" Text="หลักสูตร :" />
                                        <div class="col-sm-8">
                                            <label id="reg_dlgSch_course_name" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label99" CssClass="col-sm-2 control-labelnotop text_right f-bold" runat="server" Text="กลุ่มวิชา :" />
                                        <div class="col-sm-4">
                                            <label id="reg_dlgSch_training_group_name" />
                                        </div>
                                        <asp:Label ID="Label100" CssClass="col-md-2 control-labelnotop text_right f-bold" runat="server" Text="สาขาวิชา :" />
                                        <div class="col-sm-4">
                                            <label id="reg_dlgSch_training_branch_name" />
                                        </div>
                                    </div>

                                    <div class="form-group">

                                        <asp:Label ID="Label101" class="col-sm-2 control-labelnotop text_right f-bold" runat="server" Text="รายละเอียด :" />
                                        <div class="col-sm-8">
                                            <label id="reg_dlgSch_course_remark" />
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label102" CssClass="col-sm-2 control-labelnotop text_right f-bold" runat="server" Text="ระดับความยากของหลักสูตร :" />
                                        <div class="col-sm-4">
                                            <label id="reg_dlgSch_level_name" />
                                        </div>
                                        <asp:Label ID="Label103" CssClass="col-md-2 control-labelnotop text_right f-bold" runat="server" Text="คะแนน :" />
                                        <div class="col-sm-4">
                                            <label id="reg_dlgSch_course_score" />

                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <asp:Label ID="Label104" CssClass="col-sm-2 control-labelnotop text_right f-bold" runat="server" Text="ประเภท :" />
                                        <div class="col-sm-4">
                                            <label id="reg_dlgSch_course_type" />

                                        </div>
                                        <asp:Label ID="Label105" CssClass="col-md-2 control-labelnotop text_right f-bold" runat="server" Text="ระดับความสำคัญ :" />
                                        <div class="col-sm-4">
                                            <label id="reg_dlgSch_priority_name"></label>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label106" CssClass="col-sm-2 control-labelnotop text_right f-bold" runat="server" Text="การประเมิน :" />
                                        <div class="col-sm-4">
                                            <label id="reg_dlgSch_course_assessment"></label>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label107" CssClass="col-sm-2 control-labelnotop text_right f-bold" runat="server" Text="แบบทดสอบ :" />
                                        <div class="col-sm-4">
                                            <label id="reg_dlgSch_u2_course_test_item"></label>

                                        </div>
                                        <asp:Label ID="Label108" CssClass="col-md-2 control-labelnotop text_right f-bold" runat="server" Text="แบบประเมิน" />
                                        <div class="col-sm-4">
                                            <label id="reg_dlgSch_u3_course_evaluation_item"></label>

                                        </div>
                                    </div>

                                    <asp:UpdatePanel ID="updatepanel1" runat="server">
                                        <ContentTemplate>
                                            <div class="form-group">
                                                <div class="col-sm-4 col-sm-offset-8">

                                                    <asp:LinkButton ID="lbCmdregister_ok" class="btn btn-success btn-sm"
                                                        data-toggle="tooltip" title="Register" runat="server"
                                                        Text='<i class="glyphicon glyphicon-ok"></i> ลงทะเบียนเข้าร่วมคอร์สอบรม'
                                                        OnClick="Button1_Click">
                                                    </asp:LinkButton>

                                                    <asp:LinkButton ID="LinkButton2" class="btn btn-default btn-sm"
                                                        data-toggle="tooltip" title="Close" runat="server"
                                                        data-dismiss="modal" CommandName="Cancel"
                                                        Text='<i class="glyphicon glyphicon-remove"></i>'>
                                                    </asp:LinkButton>

                                                </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="lbCmdregister_ok" />
                                        </Triggers>
                                    </asp:UpdatePanel>




                                </div>

                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>

    <%-- schedule Ask permission --%>
    <div class="col-lg-12" runat="server" id="Div4">
        <div id="dv_Detailschedule_ask_permission" class="modal open" role="dialog">
            <div class="modal-dialog" style="width: 70%;">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">รายละเอียดหลักสูตร
                        </h4>
                    </div>
                    <div class="modal-body">
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="panel-heading">

                                    <input type="text" style="visibility: hidden" name="ask_perm_dlgSch_txtu0_training_plan_idx" id="ask_perm_dlgSch_txtu0_training_plan_idx" />
                                    <input type="text" style="visibility: hidden" name="ask_perm_dlgSch_txtemp_id" id="ask_perm_dlgSch_txtemp_id" />
                                    <input type="text" style="visibility: hidden" name="ask_perm_dlgSch_txtu0_training_course_idx" id="ask_perm_dlgSch_txtu0_training_course_idx" />
                                    <input type="text" style="visibility: hidden" name="ask_perm_dlgSch_txtu3_training_course_idx" id="ask_perm_dlgSch_txtu3_training_course_idx" />
                                    <div class="form-group">
                                        <asp:Label ID="Label109" CssClass="col-sm-2 control-labelnotop text_right f-bold" runat="server" Text="เลขที่เอกสาร :" />
                                        <div class="col-sm-4">
                                            <label id="ask_perm_dlgSch_txtcourse_no" />
                                        </div>
                                        <asp:Label ID="Label110" CssClass="col-md-2 control-labelnotop text_right f-bold" runat="server" Text="วันที่สร้างเอกสาร :" />
                                        <div class="col-sm-4">
                                            <label id="ask_perm_dlgSch_course_date" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label111" CssClass="col-md-2 control-labelnotop text_right f-bold" runat="server" Text="หลักสูตร :" />
                                        <div class="col-sm-8">
                                            <label id="ask_perm_dlgSch_course_name" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label112" CssClass="col-sm-2 control-labelnotop text_right f-bold" runat="server" Text="กลุ่มวิชา :" />
                                        <div class="col-sm-4">
                                            <label id="ask_perm_dlgSch_training_group_name" />
                                        </div>
                                        <asp:Label ID="Label113" CssClass="col-md-2 control-labelnotop text_right f-bold" runat="server" Text="สาขาวิชา :" />
                                        <div class="col-sm-4">
                                            <label id="ask_perm_dlgSch_training_branch_name" />
                                        </div>
                                    </div>

                                    <div class="form-group">

                                        <asp:Label ID="Label114" class="col-sm-2 control-labelnotop text_right f-bold" runat="server" Text="รายละเอียด :" />
                                        <div class="col-sm-8">
                                            <label id="ask_perm_dlgSch_course_remark" />
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label115" CssClass="col-sm-2 control-labelnotop text_right f-bold" runat="server" Text="ระดับความยากของหลักสูตร :" />
                                        <div class="col-sm-4">
                                            <label id="ask_perm_dlgSch_level_name" />
                                        </div>
                                        <asp:Label ID="Label116" CssClass="col-md-2 control-labelnotop text_right f-bold" runat="server" Text="คะแนน :" />
                                        <div class="col-sm-4">
                                            <label id="ask_perm_dlgSch_course_score" />

                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <asp:Label ID="Label117" CssClass="col-sm-2 control-labelnotop text_right f-bold" runat="server" Text="ประเภท :" />
                                        <div class="col-sm-4">
                                            <label id="ask_perm_dlgSch_course_type" />

                                        </div>
                                        <asp:Label ID="Label118" CssClass="col-md-2 control-labelnotop text_right f-bold" runat="server" Text="ระดับความสำคัญ :" />
                                        <div class="col-sm-4">
                                            <label id="ask_perm_dlgSch_priority_name"></label>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label119" CssClass="col-sm-2 control-labelnotop text_right f-bold" runat="server" Text="การประเมิน :" />
                                        <div class="col-sm-4">
                                            <label id="ask_perm_dlgSch_course_assessment"></label>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label120" CssClass="col-sm-2 control-labelnotop text_right f-bold" runat="server" Text="แบบทดสอบ :" />
                                        <div class="col-sm-4">
                                            <label id="ask_perm_dlgSch_u2_course_test_item"></label>

                                        </div>
                                        <asp:Label ID="Label121" CssClass="col-md-2 control-labelnotop text_right f-bold" runat="server" Text="แบบประเมิน" />
                                        <div class="col-sm-4">
                                            <label id="ask_perm_dlgSch_u3_course_evaluation_item"></label>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <%--<asp:Label ID="Label126" CssClass="col-sm-2 control-labelnotop text_right f-bold" runat="server" Text="สถานะ :" />--%>
                                        <label id="ask_perm_dlgSch_status_name_lb" class="col-sm-2 control-labelnotop text_right f-bold"></label>
                                        <div class="col-sm-10">
                                            <label id="ask_perm_dlgSch_status_name" style='color: #26A65B;'></label>

                                        </div>
                                    </div>

                                    <asp:UpdatePanel ID="updatepanel2" runat="server">
                                        <ContentTemplate>
                                            <div class="form-group">
                                                <div class="col-sm-4 col-sm-offset-8">

                                                    <asp:LinkButton ID="btnCmdask_perm_ok" class="btn btn-info btn-sm"
                                                        data-toggle="tooltip" title="Register" runat="server"
                                                        CommandName="btnCmdask_perm_ok" OnCommand="btnCommand"
                                                        Text='<i class="glyphicon glyphicon-plus"></i> ขอสิทธิ์เข้าร่วมคอร์สอบรม'>
                                                    </asp:LinkButton>

                                                    <asp:LinkButton ID="LinkButton1" class="btn btn-default btn-sm"
                                                        data-toggle="tooltip" title="Close" runat="server"
                                                        data-dismiss="modal" CommandName="Cancel"
                                                        Text='<i class="glyphicon glyphicon-remove"></i>'>
                                                    </asp:LinkButton>

                                                </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnCmdask_perm_ok" />
                                        </Triggers>
                                    </asp:UpdatePanel>




                                </div>

                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>


    <%-- schedule show --%>

    <div class="col-lg-12" runat="server" id="Div55">
        <div id="dv_massage_show" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">e-Learning </h4>

                    </div>
                    <div class="modal-body">

                        <h3>
                            <label id="lb_massage" />
                        </h3>
                        <input type="text" style="visibility: hidden;" name="txt_hidden_massage" id="txt_hidden_massage" />
                    </div>
                    <div class="modal-footer">
                        <asp:UpdatePanel ID="updatepanel3" runat="server">
                            <ContentTemplate>
                                <%--<div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-8">--%>

                                <asp:LinkButton ID="btn_msok" class="btn btn-success btn-sm"
                                    data-toggle="tooltip" title="ตกลง" runat="server"
                                    CommandName="btn_msok" OnCommand="btnCommand"
                                    Text='<i class="glyphicon glyphicon-ok"></i>'>
                                </asp:LinkButton>

                                <asp:LinkButton ID="btn_mscancel" class="btn btn-default btn-sm"
                                    data-toggle="tooltip" title="ยกเลิก" runat="server"
                                    data-dismiss="modal" CommandName="Cancel"
                                    Text='<i class="glyphicon glyphicon-remove"></i>'>
                                </asp:LinkButton>

                                <%--</div>
                                </div>--%>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btn_msok" />
                                <%--  <asp:PostBackTrigger ControlID="btn_mscancel" />--%>
                            </Triggers>
                        </asp:UpdatePanel>

                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="col-lg-12" runat="server" id="Div5">
        <div id="dv_massage_confrim" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">e-Learning </h4>

                    </div>
                    <div class="modal-body">

                        <h4>
                            <label id="lb_massage_conf">
                                คุณดูวิดีโอครบแล้ว.
                            </label>
                        </h4>
                    </div>
                    <div class="modal-footer">
                        <asp:UpdatePanel ID="updatepanel4" runat="server">
                            <ContentTemplate>
                                <%--<div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-8">--%>

                                <asp:LinkButton ID="btn_msresualt_conf" class="btn btn-success btn-sm"
                                    runat="server"
                                    CommandName="btn_msresualt_conf" OnCommand="btnCommand"
                                    Text='<i class="glyphicon glyphicon-ok">ดูวีดีโอ</i>'>
                                </asp:LinkButton>

                                <asp:LinkButton ID="btn_msok_conf" class="btn btn-primary btn-sm"
                                    runat="server"
                                    CommandName="btn_msok_conf" OnCommand="btnCommand"
                                    Text='<i class="glyphicon glyphicon-file">ทำข้อสอบ</i>'>
                                </asp:LinkButton>

                                <asp:LinkButton ID="btn_mscancel_conf" class="btn btn-default btn-sm"
                                    data-toggle="tooltip" title="ยกเลิก" runat="server"
                                    CommandName="btn_mscancel" OnCommand="btnCommand"
                                    Text='<i class="glyphicon glyphicon-remove"></i>'>
                                </asp:LinkButton>

                                <%--</div>
                                </div>--%>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btn_msresualt_conf" />
                                <asp:PostBackTrigger ControlID="btn_msok_conf" />
                                <asp:PostBackTrigger ControlID="btn_mscancel_conf" />
                            </Triggers>
                        </asp:UpdatePanel>

                    </div>
                </div>

            </div>
        </div>
    </div>

    <asp:HiddenField ID="hdd_statusvideo" runat="server" />

    <script type="text/javascript">
        function openModal_dv_massage_confrim() {
            $('#dv_massage_confrim').modal('show');

        }
    </script>
    <script type="text/javascript">
        function openModal_Detailsyllabus() {
            $('#dv_Detailsyllabus').modal('show');

        }
    </script>
    <script type="text/javascript">
        function openModal_DvRemarkRePrint() {
            $('#DvRemarkRePrint').modal('show');

        }
    </script>
    <script type="text/javascript">
        function openModal_Detailschedule() {
            $('#dv_Detailschedule_register').modal('show');

        }
    </script>



    <script type="text/javascript">
        function show_register(value) {

            $.ajax({
                type: "POST",
                url: '<%=ResolveUrl("el_Trn_plan_course_register.aspx") %>/get_register',
                data: '{id: "' + value + '" , emp_id: "' + $("#<%=Hddfld_emp_idx.ClientID%>")[0].value + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    // document.getElementById('dlgSch_txtcourse_no').value = data.d[0].ContactName;

                    if (
                        (data.d[0].ztem_trn_plan == "0")
                        && (data.d[0].register_status == "0")
                        && (data.d[0].u0_training_course_idx != "0")
                        && (data.d[0].zresulte_count == "0")
                        || (data.d[0].zresulte_app_status == "6")
                        && (data.d[0].zcourse_count == 0)

                    ) {
                        document.getElementById('ask_perm_dlgSch_txtu0_training_plan_idx').value = data.d[0].u0_training_plan_idx;
                        document.getElementById('ask_perm_dlgSch_txtcourse_no').innerHTML = data.d[0].training_plan_no;
                        document.getElementById('ask_perm_dlgSch_course_date').innerHTML = data.d[0].training_plan_date;
                        document.getElementById('ask_perm_dlgSch_course_name').innerHTML = data.d[0].course_name;
                        document.getElementById('ask_perm_dlgSch_training_group_name').innerHTML = data.d[0].training_group_name;
                        document.getElementById('ask_perm_dlgSch_training_branch_name').innerHTML = data.d[0].training_branch_name;
                        document.getElementById('ask_perm_dlgSch_course_remark').innerHTML = data.d[0].training_course_remark;
                        document.getElementById('ask_perm_dlgSch_level_name').innerHTML = data.d[0].level_name;
                        document.getElementById('ask_perm_dlgSch_course_score').innerHTML = data.d[0].course_score_name;
                        document.getElementById('ask_perm_dlgSch_course_type').innerHTML = data.d[0].course_type;
                        document.getElementById('ask_perm_dlgSch_course_assessment').innerHTML = data.d[0].course_assessment;
                        document.getElementById('ask_perm_dlgSch_priority_name').innerHTML = data.d[0].priority_name;
                        document.getElementById('ask_perm_dlgSch_u2_course_test_item').innerHTML = data.d[0].u2_course_test_name;
                        document.getElementById('ask_perm_dlgSch_u3_course_evaluation_item').innerHTML = data.d[0].u3_course_evaluation_name;
                        document.getElementById('ask_perm_dlgSch_txtemp_id').value = data.d[0].emp_id;
                        document.getElementById('ask_perm_dlgSch_txtu3_training_course_idx').value = data.d[0].u3_training_course_idx;
                        document.getElementById('ask_perm_dlgSch_txtu0_training_course_idx').value = data.d[0].u0_training_course_idx;
                        if ((data.d[0].zresulte_app_status == "6")) {
                            document.getElementById('ask_perm_dlgSch_status_name_lb').innerHTML = "สถานะ :";
                            document.getElementById('ask_perm_dlgSch_status_name').innerHTML = data.d[0].status_name;
                        }
                        else {
                            document.getElementById('ask_perm_dlgSch_status_name_lb').innerHTML = "";
                            document.getElementById('ask_perm_dlgSch_status_name').innerHTML = "";
                        }

                        $('#dv_Detailschedule_ask_permission').modal('show');
                    }
                    else if ((data.d[0].ztem_trn_plan == "1")
                        && (data.d[0].register_status == "0")
                        && (data.d[0].zcourse_count == 0)
                        && (data.d[0].course_type_etraining > 0)
                    ) {

                        document.getElementById('reg_dlgSch_txtu0_training_plan_idx').value = data.d[0].u0_training_plan_idx;
                        document.getElementById('reg_dlgSch_txtcourse_no').innerHTML = data.d[0].training_plan_no + "-xxx-2";
                        document.getElementById('reg_dlgSch_course_date').innerHTML = data.d[0].training_plan_date;
                        document.getElementById('reg_dlgSch_course_name').innerHTML = data.d[0].course_name;
                        document.getElementById('reg_dlgSch_training_group_name').innerHTML = data.d[0].training_group_name;
                        document.getElementById('reg_dlgSch_training_branch_name').innerHTML = data.d[0].training_branch_name;
                        document.getElementById('reg_dlgSch_course_remark').innerHTML = data.d[0].training_course_remark;
                        document.getElementById('reg_dlgSch_level_name').innerHTML = data.d[0].level_name;
                        document.getElementById('reg_dlgSch_course_score').innerHTML = data.d[0].course_score_name;
                        document.getElementById('reg_dlgSch_course_type').innerHTML = data.d[0].course_type;
                        document.getElementById('reg_dlgSch_course_assessment').innerHTML = data.d[0].course_assessment;
                        document.getElementById('reg_dlgSch_priority_name').innerHTML = data.d[0].priority_name;
                        document.getElementById('reg_dlgSch_u2_course_test_item').innerHTML = data.d[0].u2_course_test_name;
                        document.getElementById('reg_dlgSch_u3_course_evaluation_item').innerHTML = data.d[0].u3_course_evaluation_name;
                        document.getElementById('reg_dlgSch_txtemp_id').value = data.d[0].emp_id;
                        document.getElementById('reg_dlgSch_txtu3_training_course_idx').value = data.d[0].u3_training_course_idx;
                        document.getElementById('reg_dlgSch_txtu0_training_course_idx').value = data.d[0].u0_training_course_idx;

                        $('#dv_Detailschedule_register').modal('show');
                    }
                    else {
                        document.getElementById('dlgSch_txtu0_training_plan_idx').value = data.d[0].u0_training_plan_idx;
                        document.getElementById('dlgSch_txtcourse_no').innerHTML = data.d[0].training_plan_no;
                        document.getElementById('dlgSch_course_date').innerHTML = data.d[0].training_plan_date;
                        document.getElementById('dlgSch_course_name').innerHTML = data.d[0].course_name;
                        document.getElementById('dlgSch_training_group_name').innerHTML = data.d[0].training_group_name;
                        document.getElementById('dlgSch_training_branch_name').innerHTML = data.d[0].training_branch_name;
                        document.getElementById('dlgSch_course_remark').innerHTML = data.d[0].training_course_remark;
                        document.getElementById('dlgSch_level_name').innerHTML = data.d[0].level_name;
                        document.getElementById('dlgSch_course_score').innerHTML = data.d[0].course_score_name;
                        document.getElementById('dlgSch_course_type').innerHTML = data.d[0].course_type;
                        document.getElementById('dlgSch_course_assessment').innerHTML = data.d[0].course_assessment;
                        document.getElementById('dlgSch_priority_name').innerHTML = data.d[0].priority_name;
                        document.getElementById('dlgSch_u2_course_test_item').innerHTML = data.d[0].u2_course_test_name;
                        document.getElementById('dlgSch_u3_course_evaluation_item').innerHTML = data.d[0].u3_course_evaluation_name;
                        document.getElementById('dlgSch_txtemp_id').value = data.d[0].emp_id;
                        document.getElementById('dlgSch_txtu3_training_course_idx').value = data.d[0].u3_training_course_idx;
                        document.getElementById('dlgSch_txtu0_training_course_idx').value = data.d[0].u0_training_course_idx;
                        document.getElementById('dlgSch_status_name').innerHTML = data.d[0].status_name;

                        $('#dv_Detailschedule_show').modal('show');
                    }


                },
                failure: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    //alert("Message: " + r.Message);
                    //alert("StackTrace: " + r.StackTrace);
                    //alert("ExceptionType: " + r.ExceptionType);
                }
            });
        }

    </script>
    <script type="text/javascript">
        function dotim() {
            tinyMCE.triggerSave();
        }

    </script>

    <script type="text/javascript">



        tinymce.init({
            mode: "specific_textareas",
            editor_selector: "tinymce",
            encoding: "xml",
            theme: "modern",
            menubar: false,
            resize: false,
            statusbar: false,
            plugins: ["advlist autolink lists charmap preview hr anchor",
                "pagebreak code nonbreaking table contextmenu directionality paste"],
            toolbar1: "styleselect | bold italic underline | undo redo",
            toolbar2: "alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
            setup: function (editor) {
                editor.on('change', function () { tinymce.triggerSave(); });
            }
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            tinymce.remove(".tinymce");

            tinymce.init({
                mode: "specific_textareas",
                editor_selector: "tinymce",
                encoding: "xml",
                theme: "modern",
                menubar: false,
                resize: false,
                statusbar: false,
                plugins: ["advlist autolink lists charmap preview hr anchor",
                    "pagebreak code nonbreaking table contextmenu directionality paste"],
                toolbar1: "styleselect | bold italic underline | undo redo",
                toolbar2: "alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
                setup: function (editor) {
                    editor.on('change', function () { tinymce.triggerSave(); });
                }
            });

            $(".multi").MultiFile();
        })

    </script>


    <script type="text/javascript">
        function printDiv(divName) {
            var panel = document.getElementById(divName);
            var printWindow = window.open('', '', 'height=800,width=1000');
            printWindow.document.write('<html><head><title></title></head>');
            printWindow.document.write('<body style="font-size:12px;">');
            printWindow.document.write('<style>table{width:100%;color:#000000;font-size:10px;};</style>');
            printWindow.document.write(panel.innerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            setTimeout(function () {
                printWindow.print();
                //printWindow.resizeTo(100,100);
                //printWindow.focus();
                //printwindow.window.close();
            }, 500);
            return false;

        }

        function confirmWithOutValidated(message, validateGroup) {
            if (validateGroup != '') {
                var validated = Page_ClientValidate(validateGroup);
                if (validated) {
                    return confirm(message);
                }
            } else {
                return confirm(message);
            }
        }

        function dtPickerInput(inputClassFrom, inputClassTo) {
            $(inputClassFrom).datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $(inputClassTo).datetimepicker({
                format: 'DD/MM/YYYY'
            });
        }

        function dtPickerBtn(inputClassFrom, inputClassTo, btnClassFrom, btnClassTo) {
            $(btnClassFrom).click(function () {
                $(inputClassFrom).data("DateTimePicker").show();
            });
            $(btnClassTo).click(function () {
                $(inputClassTo).data("DateTimePicker").show();
            });
        }

        $(function () {
            $('.header-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-manual-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-import-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.show-from-onclick').click(function () {
                $('.cutstock-manual-date').data("DateTimePicker").show();
            });

            $('.show-order-sale-log-from-createdocdate-onclick').click(function () {
                $('.filter-order-from-createdocdate').data("DateTimePicker").show();
            });
            $('.filter-order-from-createdocdate').datetimepicker({
                format: 'DD/MM/YYYY'
            });

            $('.show-order-sale-log-from-createstart-onclick').click(function () {
                $('.filter-order-from-createstart').data("DateTimePicker").show();
            });
            $('.show-order-sale-log-to-createend-onclick').click(function () {
                $('.filter-order-to-createend').data("DateTimePicker").show();
            });
            $('.filter-order-from-createstart').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-to-createend').datetimepicker({
                format: 'DD/MM/YYYY'
            });


            $('.show-order-sale-log-from-createstart-onclick').click(function () {
                $('.filter-order-from').data("DateTimePicker").show();
            });
            $('.show-order-sale-log-to-createend-onclick').click(function () {
                $('.filter-order-to-createend').data("DateTimePicker").show();
            });
            $('.filter-order-from-createstart').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-to-createend').datetimepicker({
                format: 'DD/MM/YYYY'
            });

        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $('.header-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-manual-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-import-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.show-from-onclick').click(function () {
                $('.cutstock-manual-date').data("DateTimePicker").show();
            });

            $('.show-order-sale-log-from-createdocdate-onclick').click(function () {
                $('.filter-order-from-createdocdate').data("DateTimePicker").show();
            });
            $('.filter-order-from-createdocdate').datetimepicker({
                format: 'DD/MM/YYYY'
            });

            $('.show-order-sale-log-from-createstart-onclick').click(function () {
                $('.filter-order-from').data("DateTimePicker").show();
            });
            $('.show-order-sale-log-to-createend-onclick').click(function () {
                $('.filter-order-to-createend').data("DateTimePicker").show();
            });
            $('.filter-order-from-createstart').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-to-createend').datetimepicker({
                format: 'DD/MM/YYYY'
            });


        });
    </script>


    <script type="text/javascript">


        //no post back
        $('.clockpicker').datetimepicker({
            format: 'HH:mm',
            //  stepping: 30,
            ignoreReadonly: true
        });
        $('.show-time-onclick').click(function () {
            $('.clockpicker').data("DateTimePicker").show();
        });

        $('.clockpickerto').datetimepicker({
            format: 'HH:mm',
            //   stepping: 30,
            ignoreReadonly: true
        });
        $('.show-timeto-onclick').click(function () {
            $('.clockpickerto').data("DateTimePicker").show();
        });

        //is
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {

            $('.loading-icon-approve').hide();

            $('.clockpicker').datetimepicker({
                format: 'HH:mm',
                // stepping: 30,
                ignoreReadonly: true
            });
            $('.show-time-onclick').click(function () {
                $('.clockpicker').data("DateTimePicker").show();
            });

            $('.clockpickerto').datetimepicker({
                format: 'HH:mm',
                // stepping: 30,
                ignoreReadonly: true
            });
            $('.show-timeto-onclick').click(function () {
                $('.clockpickerto').data("DateTimePicker").show();
            });
        });


        function getAlertSuccess() {
            $("#success_alert").show();
            $("#success_alert").fadeTo(500, 500).slideUp(500, function () {
                $("#success_alert").slideUp(500);
            });
        }

        function ValidatePage(toHide, toShow) {

            $(toHide).hide();
            $(toShow).show();

        }
    </script>

    <script type="text/javascript">
        function imageproposUpload(input) {
            if (input.files && input.files[0]) {

                var filerdr = new FileReader();
                filerdr.onload = function (e) {
                    //$('#img_1').attr('src', e.target.result);
                }
                filerdr.readAsDataURL(input.files[0]);
                var data_proposition = new FormData();
                var filename = $("#txtfileimport").val();
                var extension = filename.replace(/^.*\./, '');
                if (extension == filename) {
                    extension = '';
                } else {
                    extension = extension.toLowerCase();
                }
                if (
                    (extension != "jpg") &&
                    (extension != "gif") &&
                    (extension != "png") &&
                    (extension != "jpeg") &&
                    (extension != "xls") &&
                    (extension != "xlsx") &&
                    (extension != "doc") &&
                    (extension != "docx") &&
                    (extension != "pdf")
                ) {
                    alert('กรุณาเลือกไฟล์เอกสาร .jpg,.gif,.png,.jpeg,.xls,.xlsx,.doc,.docx,.pdf ' + "เท่านั้น");
                    $("#txtfileimport").val("");
                }
                else {

                    var files = $("#txtfileimport").get(0).files;
                    if (files.length > 0) {
                        data_proposition.append("Uploaded_txtfileimport", files[0]);
                    }

                    var ajaxRequest = $.ajax({
                        type: "POST",
                        url: '<%=ResolveUrl("el_Trn_plan_course_register.aspx") %>',
                        contentType: false,
                        processData: false,
                        data: data_proposition,
                        async: false,
                        success: function (response) {
                            alert('ไฟล์เอกสารพร้อมสำหรับตรวจสอบไฟล์');
                        },
                        error: function (error) {
                            alert('ไฟล์เอกสารไม่พร้อม.!');
                        }
                    });

                    ajaxRequest.done(function (xhr, textStatus) {
                        // Do other operation
                    });
                }
            }

        }


        function LoadProgressBar(result) {
            var progressbar = $("#progressbar-5");
            var progressLabel = $(".progress-label");
            progressbar.show();
            $("#progressbar-5").progressbar({
                //value: false,
                change: function () {
                    progressLabel.text(
                        progressbar.progressbar("value") + "%");
                },
                complete: function () {
                    progressLabel.text("Loading Completed!");
                    progressbar.progressbar("value", 0);
                    progressLabel.text("");
                    progressbar.hide();
                    var markup = "<tr><td>" + result + "</td><td><a href='#' onclick='DeleteFile(\"" + result + "\")'><span class='glyphicon glyphicon-remove red'></span></a></td></tr>";
                    $("#ListofFiles tbody").append(markup);
                    $('#Files').val('');
                    $('#FileBrowse').find("*").prop("disabled", false);
                }
            });
            function progress() {
                var val = progressbar.progressbar("value") || 0;
                progressbar.progressbar("value", val + 1);
                if (val < 99) {
                    setTimeout(progress, 25);
                }
            }
            setTimeout(progress, 100);
        }

    </script>

    <%-- play video e-learning --%>

    <span id="u6_course_idx" style="visibility: hidden;"><%= getu6_course_idx %></span>
    <span id="u3_training_course_idx" style="visibility: hidden;"><%= getu3_training_course_idx %></span>
    <input type="text" style="visibility: hidden;" name="txt_video_item" id="txt_video_item" value="<%= getvideo_item %>" />
    <span id="u13_training_course_idx" style="visibility: hidden;"><%= getu13_training_course_idx %></span>
    <span id="item_count_h" style="visibility: hidden;"><%= getitem_count_h %></span>
    <span id="item_total" style="visibility: hidden;"><%= getitem_total %></span>
    <span id="result_flag" style="visibility: hidden;"><%= getresult_flag %></span>
    <input type="text" name="_fullscreen" id="_fullscreen" style="visibility: hidden;" value="<%= get_fullscreen %>" />

    <script type="text/javascript">
        function pos_datavideo(value) {
            $.ajax({
                type: "POST",
                url: '<%=ResolveUrl("el_Trn_plan_course_register.aspx") %>/pos_datavideo',
                data: '{currenttime: "' + document.getElementById("current_time").innerHTML +
                    '" , duration: "' + document.getElementById("duration").innerHTML +
                    '" , emp_id: "' + $("#<%=Hddfld_emp_idx.ClientID%>")[0].value +
                    '" , u6_course_idx: "' + document.getElementById("u6_course_idx").innerHTML +
                    '" , u3_training_course_idx: "' + document.getElementById("u3_training_course_idx").innerHTML +
                    '" , u13_training_course_idx: "' + document.getElementById("u13_training_course_idx").innerHTML +
                    '" , item_count_h: "' + document.getElementById("item_count_h").innerHTML +
                    '" , item_total: "' + document.getElementById("item_total").innerHTML +
                    '" , result_flag: "' + document.getElementById("result_flag").innerHTML +
                    '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    document.getElementById("item_count_std").innerHTML = data.d[0].zresulte_count;
                    document.getElementById("current_time_std").innerHTML = data.d[0].currenttime;
                    document.getElementById("current_time_std_int").innerHTML = data.d[0].currenttime_int;
                    document.getElementById("u13_training_course_idx").innerHTML = data.d[0].u13_training_course_idx;

                    if (document.getElementById("current_time").innerHTML
                        == document.getElementById("duration").innerHTML) {

                        if ((document.getElementById("item_count_std").innerHTML ==
                            document.getElementById("item_count").innerHTML)
                            &&
                            (data.d[0].zresulte_count != "0")
                        ) {
                            document.getElementById('lb_massage').innerHTML = "คุณดูวิดีโอครบทั้ง " + document.getElementById("item_count").innerHTML + " วิดีโอแล้วไปทำข้อสอบได้ .!!!!";
                            document.getElementById('txt_hidden_massage').value = "1";
                            $('#dv_massage_show').modal('show');
                        }
                        else {
                            document.getElementById('lb_massage').innerHTML = "ดูวิดีโอถัดไป.!!!!";
                            document.getElementById('txt_hidden_massage').value = "0";
                            $('#dv_massage_show').modal('show');
                        }


                    }
                },
                failure: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                }
            });
        }

    </script>

    <script type="text/javascript">

        function DisableRightClick(event) {

            //For mouse right click

            if (event.button == 2) {

                alert("ไม่สามารถบันทึกไฟล์วิดีโอได้ !!!!");

            }

        }

        function DisableCtrlKey(e) {

            var code = (document.all) ? event.keyCode : e.which;

            var message = "Ctrl key functionality is disabled!";

            // look for CTRL key press

            if (parseInt(code) == 17) {

                alert(message);

                window.event.returnValue = false;

            }

        }

    </script>

    <style>
        .myVideo_active_style {
            /*
  background-position : 150px 8px;
  background-color: black;
  */
            height: auto;
            width: 100%;
            margin: 0 0;
            display: block;
            border: 3px solid #D9B4B4;
            position: fixed;
            right: 0;
            bottom: 0;
            min-width: 100%;
            min-height: 100%;
        }

        .myVideo_normal_style {
            /*
  background-position : 4px 8px; 
  background-color: green;
  */
            height: auto;
            width: 100%;
            margin: 0 0;
            display: block;
            border: 3px solid #D9B4B4;
        }

        .dv_elearning_back_active_style {
            visibility: hidden;
        }

        .dv_elearning_back_normal_style {
            visibility: visible;
        }

        .detail_active_style {
            visibility: hidden;
        }

        .detail_normal_style {
            visibility: visible;
        }

        .dv_crontroll_active_style {
            position: fixed;
            bottom: 0;
            /*background: rgba(0, 0, 0, 0.5);*/
            color: #f1f1f1;
            width: 100%;
            padding: 2px;
        }
    </style>

    <asp:Literal ID="txt" runat="server"></asp:Literal>

    <div>
    </div>

</asp:Content>

