﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="pms_performance.aspx.cs" Inherits="websystem_hr_pms_performance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    
    <asp:HyperLink ID="setOntop" runat="server"></asp:HyperLink>
    <!--tab menu-->
    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default" id="divBtnHead" runat="server" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-bar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="sub-navbar">
                        <a class="navbar-brand" href="#"><b>Menu</b></a>
                    </div>
                </div>

                <!--Collect the nav links, forms, and other content for toggling-->
                <div class="collapse navbar-collapse" id="menu-bar" >
                    <ul class="nav navbar-nav" id="uiNav" runat="server">

                        <li id="li0" runat="server">
                            <asp:LinkButton ID="lbDetail" runat="server" CommandName="cmdDetail" OnCommand="navCommand"
                                CommandArgument="docDetail"> รายการทั่วไป</asp:LinkButton>
                        </li>
                        <li id="li1" runat="server">
                            <asp:LinkButton ID="lbCreate" runat="server" CommandName="cmdCreate" OnCommand="navCommand"
                                CommandArgument="docCreate"> สร้างรายการประเมิน</asp:LinkButton>
                        </li>
                        <li id="li2" runat="server">
                            <asp:LinkButton ID="lbApprove" runat="server" CommandName="cmdApprove" OnCommand="navCommand"
                                CommandArgument="docApprove"> รายการรออนุมัติ</asp:LinkButton>
                        </li>
                        <li id="li4" runat="server">
                            <asp:LinkButton ID="lbApprove1" runat="server" CommandName="cmdApprove1" OnCommand="navCommand"
                                CommandArgument="docApprove1"> รออนุมัติ Approve1</asp:LinkButton>
                        </li>
                        <li id="li5" runat="server">
                            <asp:LinkButton ID="lbApprove2" runat="server" CommandName="cmdApprove2" OnCommand="navCommand"
                                CommandArgument="docApprove2"> รออนุมัติ Approve2</asp:LinkButton>
                        </li>
                        <li id="li6" runat="server">
                            <asp:LinkButton ID="lbApprove3" runat="server" CommandName="cmdApprove3" OnCommand="navCommand"
                                CommandArgument="docApprove3"> รออนุมัติ Approve3</asp:LinkButton>
                        </li>
                        <li id="li3" runat="server">
                            <asp:LinkButton ID="lbCheckPerformance" runat="server" CommandName="cmdCheckPerformance" OnCommand="navCommand"
                                CommandArgument="docCheckPerformance"> ตรวจสอบการประเมิน</asp:LinkButton>
                        </li>

                    </ul>

                    <ul class="nav navbar-nav navbar-right" runat="server">

                        <li id="_divFlowLiToDocument" runat="server">

                            <asp:HyperLink ID="hpFlowProcess" NavigateUrl="https://drive.google.com/file/d/1oH774PL3oXRYwB0pKOcZuMP4hMcuhfVh/view?usp=sharing" Target="_blank" runat="server" CommandName="cmdFlowProcess" OnCommand="btnCommand"><i class="fas fa-book"></i> Flow การทำงาน</asp:HyperLink>
                        </li>

                    </ul>

                </div>
            </div>
        </nav>
    </div>
    <!--tab menu-->
   
    <div class="col-md-12">
        <asp:Literal ID="litDebug" runat="server"></asp:Literal>
        <asp:PlaceHolder ID="plBarCode" runat="server" />
    </div>

    <!--multiview-->
    <asp:MultiView ID="mvSystem" runat="server">

        <!--View Detail-->
        <asp:View ID="docDetail" runat="server">
            <div class="col-md-12">

                
                <div id ="_DivHead" runat="server">
                    <asp:Image ID="imghead" runat="server" class="img-responsive img-fluid" ImageUrl="~/images/pms-formresult/performance_system.png" Style="height: 100%; width: 100%;" />

                </div>
                <br>
                <div class="clearfix"></div>
                

                <asp:FormView ID="fvSearch" runat="server" Width="100%" DefaultMode="Insert">
                    <InsertItemTemplate>

                        <div class="panel panel-danger">
                            <div class="panel-heading">
                                <h3 class="panel-title">ค้นหาข้อมูล</h3>
                            </div>

                            <div class="panel-body">
                                
                                <div class="col-md-10 col-md-offset-1">
                                    <div class="form-horizontal">
                                        
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">องค์กร</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlOrganization" runat="server"
                                                    Cssclass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                    AutoPostBack="true"></asp:DropDownList>
                                            </div>
                                            <label class="col-md-2 control-label">กลุ่มงาน</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlWorkGroup" runat="server"
                                                    Cssclass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">สายงาน</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlLineWork" runat="server"
                                                    Cssclass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                            <label class="col-md-2 control-label">ฝ่าย</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlDepartment" runat="server"
                                                    Cssclass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">แผนก</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlSection" runat="server" Cssclass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                            <label class="col-md-2 control-label">ตำแหน่ง</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlPosition" runat="server"
                                                    Cssclass="form-control">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2 control-label">รหัสพนักงาน</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="txt_empcode" runat="server" TextMode="MultiLine" Rows="2" Style="overflow: auto" placeholder="กรอกรหัสพนักงาน Ex.(58000106,58000107)..." CssClass="form-control" />
                                            </div>
                                            <label class="col-md-2 control-label"></label>
                                            <div class="col-md-4">
                                               
                                            </div>
                                        </div>



                                        <div class="form-group">
                                            <label class="col-md-2"></label>
                                            <div class="col-md-4">
                                                <asp:LinkButton ID="lbSearchData" runat="server"
                                                    CssClass="btn btn-md btn-primary" OnCommand="btnCommand"
                                                    CommandName="cmdSearch"><i class="fas fa-search"></i>&nbsp;ค้นหา
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="lbResetData" runat="server"
                                                    CssClass="btn btn-md btn-default" OnCommand="btnCommand"
                                                    CommandName="cmdReset"><i class="fas fa-redo"></i>&nbsp;เคลียร์ค่า
                                                </asp:LinkButton>
                                            </div>
                                            <label class="col-md-2"></label>
                                            <label class="col-md-4"></label>
                                        </div>
                                    </div>
                                </div>
                                <label class="col-md-1"></label>

                            </div>

                        </div>

                    </InsertItemTemplate>
                </asp:FormView>
                    
                <asp:GridView ID="GvDetail" runat="server"
                    AutoGenerateColumns="False"
                    CssClass="table table-striped table-bordered table-hover table-responsive"
                    OnPageIndexChanging="gvPageIndexChanging"
                    OnRowDataBound="gvRowDataBound"
                    ShowHeaderWhenEmpty="false"
                    AllowPaging="True"
                    PageSize="10"
                    BorderStyle="None"
                    DataKeyNames="u0_docidx"
                    CellSpacing="2">
                    <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                    <RowStyle Font-Size="Small" />
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">Data Cannot Be Found</div>
                    </EmptyDataTemplate>

                    <Columns>

                       
                        <asp:TemplateField HeaderText="ข้อมูลผู้ประเมิน" ItemStyle-Width="30%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="left" ItemStyle-Font-Size="Small">
                            <ItemTemplate>
                                <asp:Label ID="lblu0_docidx" runat="server" Visible="false" Text='<%# Eval("u0_docidx") %>'></asp:Label>
                                <asp:Label ID="lblunidx" runat="server" Visible="false" Text='<%# Eval("unidx") %>'></asp:Label>
                                <asp:Label ID="lblacidx" runat="server" Visible="false" Text='<%# Eval("acidx") %>'></asp:Label>
                                <asp:Label ID="lblstaidx" runat="server" Visible="false" Text='<%# Eval("staidx") %>'></asp:Label>
                                <asp:Label ID="lblsolid" runat="server" Visible="false" Text='<%# Eval("solid") %>'></asp:Label>
                                <asp:Label ID="lbldotted" runat="server" Visible="false" Text='<%# Eval("dotted") %>'></asp:Label>
                                
                                <p>
                                    <b>องค์กร:</b>
                                    <asp:Label ID="lbl_org_idx" Visible="false" runat="server" Text='<%# Eval("org_idx") %>' />
                                    <asp:Label ID="lbl_org_name_th" Visible="true" runat="server" Text='<%# Eval("org_name_th") %>' />
                                </p>
                                <p>
                                    <b>ฝ่าย:</b>
                                    <asp:Label ID="lbl_rdept_idx" Visible="false" runat="server" Text='<%# Eval("rdept_idx") %>' />
                                    <asp:Label ID="lbl_dept_name_th" Visible="true" runat="server" Text='<%# Eval("dept_name_th") %>' />
                                </p>
                                <p>
                                    <b>แผนก:</b>
                                    <asp:Label ID="lbl_rsec_idx" Visible="false" runat="server" Text='<%# Eval("rsec_idx") %>' />
                                    <asp:Label ID="lbl_sec_name_th" Visible="true" runat="server" Text='<%# Eval("sec_name_th") %>' />
                                </p>
                                <p>
                                    <b>ตำแหน่ง:</b>
                                    <asp:Label ID="lbl_rpos_idx" Visible="false" runat="server" Text='<%# Eval("rpos_idx") %>' />
                                    <asp:Label ID="lbl_pos_name_th" Visible="true" runat="server" Text='<%# Eval("pos_name_th") %>' />
                                </p>
                                <p>
                                    <b>ชื่อ-สกุล:</b>
                                    <asp:Label ID="lbl_cemp_idx" Visible="false" runat="server" Text='<%# Eval("cemp_idx") %>' />
                                    <asp:Label ID="lbl_emp_name_th" Visible="true" runat="server" Text='<%# Eval("emp_name_th") %>' />
                                </p>

                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="คะแนนประเมินตนเอง" ItemStyle-Width="20%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="left" ItemStyle-Font-Size="Small">
                            <ItemTemplate>

                                <p>
                                    <b>KPIs:</b>
                                    <asp:Label ID="lbl_sum_kpis_value" Visible="true" runat="server" Text='<%# getConvertString((string)Eval("sum_kpis_value")) %>'  />
                                    
                                </p>

                                <p>
                                    <b>Core Value:</b>
                                    <asp:Label ID="lbl_sum_mine_core_value" Visible="true" runat="server" Text='<%# getConvertString((string)Eval("sum_mine_core_value")) %>'  />
                                </p>
                                <p>
                                    <b>Competencies:</b>
                                    <asp:Label ID="lbl_sum_mine_competency" Visible="true" runat="server" Text='<%# getConvertString((string)Eval("sum_mine_competency")) %>'  />
                                </p>

                              

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="คะแนนผู้มีสิทธิ์ประเมิน" ItemStyle-Width="25%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="left" >
                            <ItemTemplate>

                                <asp:Panel ID="_DivDottedDetail" runat="server">

                                    <p>
                                        <b>Dotted KPIs:</b>
                                        <asp:Label ID="lbl_sum_dotted_kpis_value" Visible="true" runat="server" Text='<%# getConvertString((string)Eval("sum_kpis_value")) %>'  />
                                        
                                    </p>

                                    <p>
                                        <b>Dotted Core Value:</b>
                                        <asp:Label ID="lbl_sum_dotted_core_value" Visible="true" runat="server" Text='<%# getConvertString((string)Eval("sum_dotted_core_value")) %>'  />
                                    </p>
                                    <p>
                                        <b>Dotted Competencies:</b>
                                        <asp:Label ID="lbl_sum_dotted_competency" Visible="true" runat="server" Text='<%# getConvertString((string)Eval("sum_dotted_competency")) %>'  />
                                    </p>
                                </asp:Panel>

                                <asp:Panel ID="_DivSolidDetail" runat="server">
                                    <p>
                                        <b>Solid KPIs:</b>
                                        <asp:Label ID="lbl_sum_solid_kpis_value" Visible="true" runat="server" Text='<%# getConvertString((string)Eval("sum_kpis_value")) %>'  />
                                        
                                    </p>
                                
                                    <p>
                                        <b>Solid Core Value:</b>
                                        <asp:Label ID="lbl_sum_solid_core_value" Visible="true" runat="server" Text='<%# getConvertString((string)Eval("sum_solid_core_value")) %>'  />
                                    </p>
                                    <p>
                                        <b>Solid Competencies:</b>
                                        <asp:Label ID="lbl_sum_solid_competency" Visible="true" runat="server" Text='<%# getConvertString((string)Eval("sum_solid_competency")) %>'  />
                                    </p>

                                </asp:Panel>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สถานะรายการ" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center" >
                            <ItemTemplate>
                                
                                    <b><asp:Label ID="lblcurrent_status" runat="server" Text='<%# Eval("current_status") %>' /></b>
                               
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnViewDetail" CssClass="btn-sm btn-warning" target="" runat="server" CommandName="cmdViewDetail" OnCommand="btnCommand" CommandArgument='<%# Eval("u0_docidx")+ ";" + Eval("unidx") + ";" + Eval("acidx") + ";" + Eval("staidx") + ";" + Eval("cemp_idx") + ";" + Eval("form_name") + ";" + Eval("sum_solid_core_value") + ";" + Eval("sum_solid_competency") + ";" + Eval("u0_typeidx")+ ";" + Eval("solid")+ ";" + Eval("dotted") + ";" + Eval("org_idx")%>' data-toggle="tooltip" title="ดูข้อมูล"><i class="fa fa-book"></i></asp:LinkButton>

                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>

                <div id ="_DivFormName" runat="server">
                    <div class="alert alert-danger f-s-16" style="text-align: center" role="alert"><b>
                            <asp:Label ID="lblFormNameView" runat="server"></asp:Label>
                        </b></div>
                </div>

                <asp:FormView ID="fvViewEmpDetail" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <ItemTemplate>
                        <asp:HiddenField ID="hfEmpOrgIDX" runat="server" Value='<%# Eval("org_idx") %>' />
                        <asp:HiddenField ID="hfEmpRdeptIDX" runat="server" Value='<%# Eval("dept_idx") %>' />
                        <asp:HiddenField ID="hfEmpRsecIDX" runat="server" Value='<%# Eval("sec_idx") %>' />
                        

                        <div class="alert alert-warning f-s-14" role="alert"><b>ส่วนที่ 1:
                                ข้อมูลทั่วไปเกี่ยวกับพนักงาน</b>
                        </div>

                        <div class="panel panel-default">

                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp;
                                    ข้อมูลส่วนตัวผู้ทำรายการ</strong></h3>
                            </div>

                            <div class="panel-body">
                                <div class="col-md-10 col-md-offset-1">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="f-s-13">รหัสผู้ทำรายการ (Employee ID)</label>
                                            <asp:TextBox ID="tbEmpCode" runat="server" Text='<%# Eval("emp_code") %>' CssClass="form-control" Enabled="false" />
                                            

                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="f-s-13">ชื่อ-นามสกุล (Name - Lastname)</label>
                                            <asp:TextBox ID="tbEmpName" runat="server" Text='<%# Eval("emp_name_th") %>' CssClass="form-control" Enabled="false" />
                                            

                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="f-s-13">บริษัท (Organization)</label>
                                            <asp:TextBox ID="tbEmpOrg" runat="server" Text='<%# Eval("org_name_th") %>' CssClass="form-control" Enabled="false" />
                                            

                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="f-s-13">กลุ่มงาน (Workgroup)</label>
                                            <asp:TextBox ID="tbWorkGroup" runat="server" Text='<%# Eval("wg_name_th") %>' CssClass="form-control" Enabled="false" />
                                            

                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="f-s-13">สายงาน (Linework)</label>
                                            <asp:TextBox ID="tbLineWork" runat="server" Text='<%# Eval("lw_name_th") %>' CssClass="form-control" Enabled="false" />
                                            

                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="f-s-13">ฝ่าย (Division)</label>
                                            <asp:TextBox ID="tbEmpDept" runat="server" Text='<%# Eval("dept_name_th") %>' CssClass="form-control" Enabled="false" />
                                            

                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="f-s-13">แผนก / ส่วน (Department/ Section)</label>
                                            <asp:TextBox ID="tbEmpSec" runat="server" Text='<%# Eval("sec_name_th") %>' CssClass="form-control" Enabled="false" />
                                            

                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="f-s-13">ตำแหน่ง (Position)</label>
                                            <asp:TextBox ID="tbEmpPos" runat="server" Text='<%# Eval("pos_name_th") %>' CssClass="form-control" Enabled="false" />
                                            

                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="f-s-13">วันที่เริ่มงาน (Start date)</label>
                                            <asp:TextBox ID="txtstartdate" runat="server" Text='<%# Eval("emp_start_date") %>' CssClass="form-control" Enabled="false" />
                                            

                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="f-s-13">อีเมล์ (E-mail)</label>
                                            <asp:TextBox ID="txtemail" runat="server" Text='<%# Eval("emp_org_mail") %>' CssClass="form-control" Enabled="false" />
                                            

                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="f-s-13">Joblevel (Joblevel)</label>
                                            <asp:TextBox ID="txt_joblevel" runat="server" Text='<%# String.Format("{0}{1}", Eval("jobgrade_name"), Eval("joblevel_name")) %>'  CssClass="form-control" Enabled="false" />
                                            

                                        </div>
                                    </div>

                                    <div class="col-md-6"></div>

                                </div>

                            </div>

                        </div>
                        
                    </ItemTemplate>
                </asp:FormView>

                <div id ="_DivNameKPI" runat="server">

                    <div class="alert alert-warning f-s-14" role="alert"><b>ส่วนที่ 2:
                            การประเมินผลตามดัชนีชี้วัดผลการปฏิบัติงาน (KPIs)</b></div>
                </div>
                
                
                <asp:UpdatePanel ID="_PanelViewGvKPIs" runat="server">
                    <ContentTemplate>

                        <div id="GvKPIs_viewscroll" style="overflow-x: auto; width: 100%" runat="server">

                            <asp:GridView ID="GvViewKPIs" runat="server" AutoGenerateColumns="false"
                                CssClass="table table-bordered table-hover table-responsive"
                                HeaderStyle-CssClass="danger" 
                                AllowPaging="false" DataKeyNames="m0_kpi_idx"
                                OnRowDataBound="gvRowDataBound" ShowFooter="True">
                                <HeaderStyle CssClass="danger" Height="40px" Font-Size="Small" />
                                <RowStyle Font-Size="Small" />
                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First"
                                    LastPageText="Last" />
                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>

                                <Columns>

                                    <asp:TemplateField ItemStyle-Width="25%" HeaderStyle-CssClass="text-center"
                                        ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <b>
                                                <asp:Label ID="lblm1_kpi_idx_view" Visible="false" runat="server"
                                                    Text='<%# Eval("m1_kpi_idx") %>'></asp:Label>
                                                <asp:Label ID="lblperf_indicators_view" runat="server"
                                                    Text='<%# Eval("perf_indicators") %>'></asp:Label>
                                            </b>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-Width="10%" HeaderStyle-CssClass="text-center"
                                        ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <b>
                                                <p>
                                                    <asp:Label ID="lblkpi_unit_view" runat="server"
                                                        Text='<%# Eval("kpi_unit") %>'></asp:Label>
                                                </p>
                                            </b>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-Width="5%" HeaderStyle-CssClass="text-center"
                                        ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <b>
                                                <p>
                                                    <asp:Label ID="lblkpi_weight_view" runat="server" Text='<%# getConvertString((string)Eval("kpi_weight")) %>' ></asp:Label>
                                                </p>
                                            </b>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-Width="10%" HeaderStyle-CssClass="text-center"
                                        ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <b>
                                                <p>
                                                    <asp:Label ID="lblkpi_target_view" runat="server"
                                                        Text='<%# Eval("kpi_target") %>'></asp:Label>
                                                </p>
                                            </b>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-Width="20%" HeaderStyle-CssClass="text-center"
                                        ItemStyle-HorizontalAlign="left">
                                        <ItemTemplate>
                                            <asp:UpdatePanel ID="Update_Viewperformance_point_view" runat="server">
                                                <ContentTemplate>

                                                    <asp:Label ID="lblpref_selected_view" Visible="false" runat="server"
                                                        Text='<%# Eval("pref_selected") %>'></asp:Label>

                                                    <asp:TextBox ID="txtkpi_view" TextMode="MultiLine" Rows="3" Style="overflow: auto" placeholder="กรอกผลลัพธ์..." 
                                                        Columns="3" CssClass="form-control" Text='<%# Eval("comment") %>' runat="server">
                                                    </asp:TextBox>
                                                    

                                                    <asp:RequiredFieldValidator ID="Re_txtkpi_view"
                                                        runat="server"
                                                        ControlToValidate="txtkpi_view" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณากรอกผลลัพธ์"
                                                        ValidationGroup="SavePerformance" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtendere13" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txtkpi_view" Width="160" PopupPosition="BottomLeft" />



                                                    <asp:Label ID="lblpoint_view" runat="server"></asp:Label>
                                                    
                                                    <asp:RadioButtonList ID="rdoperformance_point_view" Enabled="false"
                                                        OnSelectedIndexChanged="SelectedIndexChanged"
                                                        AutoPostBack="true" runat="server" visible="false"
                                                        CssClass="radio-list-inline-emps" RepeatDirection="Vertical">
                                                    </asp:RadioButtonList>
                                                    <asp:RequiredFieldValidator ID="Req_rdoperformance_point_view"
                                                        ValidationGroup="SavePerformance" runat="server" 
                                                        ErrorMessage="กรุณาเลือกคำตอบให้ครบถ้วน"
                                                        Display="None" SetFocusOnError="true"
                                                        ControlToValidate="rdoperformance_point_view" />
                                                    <ajaxToolkit:ValidatorCalloutExtender
                                                        ID="ValidatorCaslloutExtenxder711" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight"
                                                        TargetControlID="Req_rdoperformance_point_view" Width="160"
                                                        PopupPosition="BottomLeft" />

                                                </ContentTemplate>

                                            </asp:UpdatePanel>
                                        </ItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-Width="20%" HeaderStyle-CssClass="text-center"
                                        ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="Medium">
                                        <ItemTemplate>
                                            <asp:UpdatePanel ID="Update_Viewperformance_level" runat="server">
                                                <ContentTemplate>
                                                    <asp:Label ID="lblm2_kpi_idx_view" Visible="false" runat="server" Text='<%# Eval("m2_kpi_idx") %>'></asp:Label>

                                                    <asp:RadioButtonList ID="rdoperformance_level_view"
                                                        OnSelectedIndexChanged="SelectedIndexChanged"
                                                        AutoPostBack="true" runat="server"
                                                        CssClass="radio-list-inline-emps" RepeatDirection="Vertical">
                                                    </asp:RadioButtonList>
                                                    <asp:RequiredFieldValidator ID="Req_rdoperformance_level_view"
                                                        ValidationGroup="SavePerformance" runat="server" 
                                                        ErrorMessage="กรุณาเลือกคำตอบให้ครบถ้วน"
                                                        Display="None" SetFocusOnError="true"
                                                        ControlToValidate="rdoperformance_level_view" />
                                                    <ajaxToolkit:ValidatorCalloutExtender
                                                        ID="ValidatorCaslloutExtenxder7221" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight"
                                                        TargetControlID="Req_rdoperformance_level_view" Width="160"
                                                        PopupPosition="BottomLeft" />

                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="rdoperformance_level_view" EventName="SelectedIndexChanged" />
                                                    
                                                </Triggers>

                                            </asp:UpdatePanel>
                                        </ItemTemplate>

                                        <FooterTemplate>

                                            <div style="text-align: right;">
                                                <asp:Literal ID="lit_sumpoint_name_view" runat="server" Text="คะแนนที่ได้ :">
                                                </asp:Literal>
                                            </div>

                                        </FooterTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-Width="10%" HeaderStyle-CssClass="text-center"
                                        ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <b>
                                                <p>
                                                    <asp:Label ID="lblweighted_score_view" runat="server"></asp:Label>
                                                </p>
                                            </b>
                                        </ItemTemplate>

                                        <FooterTemplate>
                                            <div style="text-align: right; background-color: chartreuse;">
                                                <asp:Label ID="lit_total_weighted_score_view" runat="server"
                                                    Font-Bold="true"></asp:Label>

                                            </div>
                                        </FooterTemplate>

                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>

                        </div>

                    </ContentTemplate>

                </asp:UpdatePanel>

                <div id ="_DivPoint" runat="server">

                    <div class="alert alert-warning f-s-14" role="alert"><b>ส่วนที่ 3:
                            การประเมินผลการปฏิบัติงานและการให้ข้อมูลป้อนกลับ</b></div>

                    <h5 style="font-size: small;"><b>คำชี้แจง: ในการประเมินผลปลายปี เราจะใช้เกณฑ์คะแนนประเมินพฤติกรรม 1
                            - 4
                            โดยแต่ละคะแนนจะมีความหมายของการประเมินพฤติกรรมเพื่อการพิจารณาตามตารางด้านล่างนี้</b>
                    </h5>

                </div>

                <asp:GridView ID="GvViewMasterPoint" runat="server" AutoGenerateColumns="false"
                    CssClass="table table-bordered table-hover table-responsive"
                    HeaderStyle-CssClass="danger" AllowPaging="false" DataKeyNames="m0_point"
                    OnRowDataBound="gvRowDataBound">
                    <HeaderStyle CssClass="danger" Height="40px" Font-Size="Small" />
                    <RowStyle Font-Size="Small" />
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First"
                        LastPageText="Last" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">Data Cannot Be Found</div>
                    </EmptyDataTemplate>

                    <Columns>

                        <asp:TemplateField ItemStyle-Width="10%" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <b>
                                    <asp:Label ID="lblScore_view" runat="server" Text='<%# Eval("point_name") %>'>
                                    </asp:Label>
                                </b>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-Width="30%" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <b>
                                    <p>
                                        <asp:Label ID="lblRatth_view" runat="server" Text='<%# Eval("rat_nameth") %>'>
                                        </asp:Label>
                                    </p>
                                    <p>
                                        <asp:Label ID="lblRaten_view" runat="server" Text='<%# Eval("rat_nameen") %>'>
                                        </asp:Label>
                                    </p>

                                </b>
                            </ItemTemplate>


                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-Width="60%" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="left">
                            <ItemTemplate>
                                <p>
                                    <asp:Label ID="lblDefinitionth_view" runat="server" Text='<%# Eval("definition_th") %>'>
                                    </asp:Label>
                                </p>
                                <p>
                                    <asp:Label ID="lblDefinitionen_view" runat="server" Text='<%# Eval("definition_en") %>'>
                                    </asp:Label>
                                </p>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>

                <asp:UpdatePanel ID="_PanelViewGvViewCoreValue" runat="server">
                    <ContentTemplate>

                        <div id="GvViewCoreValue_viewscroll" style="overflow-x: auto; width: 100%" runat="server">
                
   
                            <asp:GridView ID="GvViewCoreValue" runat="server"
                                CssClass="table table-bordered table-hover table-responsive"
                                AutoGenerateColumns="false"
                                HeaderStyle-CssClass="danger" 
                                AllowPaging="false" 
                                DataKeyNames="m1_coreidx"
                                ShowFooter="true"
                                OnRowDataBound="gvRowDataBound">
                                <HeaderStyle CssClass="danger" Height="40px" Font-Size="Small" />
                                <RowStyle Font-Size="Small" />
                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First"
                                    LastPageText="Last" />
                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>
                            
                            
                                <Columns>

                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="2%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Medium">
                                        <ItemTemplate>
                                            <b>
                                                <asp:Label ID="lblCorevalue" runat="server" Text="Core Values"></asp:Label>

                                                <asp:Label ID="lblm1_coreidx" Visible="false" runat="server" Text='<%# Eval("m1_coreidx") %>'></asp:Label>
                                                <asp:Label ID="lblu1_docidx" Visible="false" runat="server" Text='<%# Eval("u1_docidx") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-Width="3%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Medium">
                                        <ItemTemplate>

                                            <b><%# (Container.DataItemIndex +1) %></b>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Medium">
                                        <ItemTemplate>
                                            <%--<asp:Label ID="lbltype_core" runat="server" Text='<%# Eval("type_core") %>'></asp:Label></b>
                                            <asp:Label ID="lblcore_name" runat="server" Text='<%# Eval("core_name") %>'></asp:Label>--%>
                                            <%--  <b>
                                                <asp:Label ID="lbltype_core" runat="server" Text='<%# Eval("type_core") %>'></asp:Label>
                                                <br />
                                                <asp:Label ID="lblcore_name" runat="server" Text='<%# Eval("core_name") %>'></asp:Label></b>--%>
                                            <b>
                                                <asp:Label ID="lblcore_name" runat="server" Text='<%# Eval("core_name") %>'></asp:Label>

                                                <br />
                                                <asp:Label ID="lbltype_core" runat="server" Text='<%# Eval("type_core") %>'></asp:Label>
                                            </b>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-Width="26%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="smaller">
                                        <ItemTemplate>
                                            <asp:GridView ID="gvsubcorevalue_view" runat="server"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-borderedtable-responsive"
                                                HeaderStyle-CssClass="Medium"
                                                GridLines="None"
                                                ShowHeader="false"
                                                HeaderStyle-Height="40px"
                                                AllowPaging="false"
                                                DataKeyNames="m2_coreidx">


                                                <PagerStyle CssClass="pageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                </EmptyDataTemplate>
                                                <Columns>

                                                    <asp:TemplateField ItemStyle-CssClass="text-left" ItemStyle-Width="100%" Visible="true" ItemStyle-Font-Size="Medium">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblm2_coreidx" Visible="false" runat="server" Text='<%# Eval("m2_coreidx") %>' />

                                                            <small>
                                                                <p>
                                                                    <asp:Label ID="lblcore_nameen" runat="server" Text='<%# Eval("core_nameen") %>'></asp:Label>
                                                                </p>
                                                                <p>
                                                                    <asp:Label ID="lblcore_nameth" runat="server" Text='<%# Eval("core_nameth") %>'></asp:Label>
                                                                </p>
                                                            </small>

                                                        </ItemTemplate>

                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>
                                        </ItemTemplate>

                                        <FooterTemplate>

                                            <div style="text-align: right;">
                                                <asp:Literal ID="lit_sumcore_value_name_detail" runat="server" Text="รวม :">
                                                </asp:Literal>
                                            </div>

                                        </FooterTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-Width="32%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="Medium">
                                        <ItemTemplate>

                                            <asp:UpdatePanel ID="_PnSelfAssessmentcor" runat="server">
                                                <ContentTemplate>
                                                    
                                                    <div class="panel-body">

                                                        <div class="form-group">
                                                            <asp:Label ID="Ladabel733" CssClass="control-label" runat="server" Text="ประเมินตนเอง Self Assessment (1-4) : " />
                                                        </div>

                                                        <asp:RadioButtonList ID="rdochoice_mine_detail" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="radio-list-inline-emps emps-word-wrap" RepeatDirection="Vertical">
                                                        </asp:RadioButtonList>
                                                        <asp:RequiredFieldValidator ID="Req_rdochoice_mine_detail"
                                                            ValidationGroup="SavePerformance" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="กรุณาเลือกคำตอบให้ครบถ้วน"
                                                            Display="None" SetFocusOnError="true" ControlToValidate="rdochoice_mine_detail" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExxtenxder7" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_rdochoice_mine_detail" Width="160"
                                                            PopupPosition="BottomLeft" />


                                                            <asp:Label ID="lblanswer" runat="server" Visible="false" Text='<%# Eval("m0_point_mine") %>' />

                                                            <asp:Panel ID="panel_update_comment_coremine" runat="server">
                                                                <div class="form-group">
                                                                    <asp:Label ID="Ladabel7" CssClass="control-label" runat="server" Text="กรุณาระบุเหตุผล : " />
                                                                </div>
                                                                <div class="form-group">
                                                                    <asp:TextBox ID="txtremarkmine_core" Text='<%# Eval("remark_mine") %>' Style="overflow: auto" placeholder="กรอกเหตุผล..." 
                                                                        TextMode="MultiLine" Rows="3" Columns="3" CssClass="form-control" runat="server"></asp:TextBox>
                                                                
                                                                    <asp:Label ID="lblremark_coremine" Visible="false" Text='<%# Eval("remark_mine") %>' CssClass="control-label" runat="server" />
                                                                </div>
                            
                                                            </asp:Panel>
                                                    </div>

                                                </ContentTemplate>
                                            </asp:UpdatePanel>

                                        </ItemTemplate>

                                        <FooterTemplate>
                                            <div style="text-align: right; background-color: chartreuse;">
                                                <asp:Label ID="lit_total_core_value_detail" runat="server" Font-Bold="true">
                                                </asp:Label>

                                            </div>
                                        </FooterTemplate>


                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-Width="32%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="Medium">
                                        <ItemTemplate>

                                            <asp:UpdatePanel ID="_PnSolidSuperiorcor" runat="server">
                                                <ContentTemplate>
                                                
                                                
                                                    <div class="panel-body" >

                                                        <div class="form-group">
                                                            <asp:Label ID="Ladabel111" CssClass="control-label" runat="server" Text="solid ประเมิน Superior's Rating(1-4) : " />
                                                        </div>
                                                     

                                                        <asp:RadioButtonList ID="rdochoice_head_detail" runat="server" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true"
                                                        CssClass="radio-list-inline-emps" RepeatLayout="Table" TextAlign="Right" RepeatDirection="Vertical">
                                                        </asp:RadioButtonList>
                        
                                                        <asp:RequiredFieldValidator ID="Req_rdochoice_head_detail"
                                                            ValidationGroup="SavePerformance" runat="server" ErrorMessage="กรุณาเลือกคำตอบให้ครบถ้วน"
                                                            Display="None" SetFocusOnError="true" ControlToValidate="rdochoice_head_detail" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExetenxder7" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_rdochoice_head_detail" Width="160"
                                                            PopupPosition="BottomLeft" />

                                                            <asp:Panel ID="panel_update_comment_corehead" runat="server">
                                                                <div class="form-group">
                                                                    <asp:Label ID="Ladbeyl7" CssClass="control-label" runat="server" Text="กรุณาระบุเหตุผล : " />
                                                                </div>
                                                                <div class="form-group">
                                                                    <asp:TextBox ID="txtremarkhead_core" Text='<%# Eval("remark_solid") %>' Style="overflow: auto" placeholder="กรอกเหตุผล..." TextMode="MultiLine" Rows="3" Columns="3" CssClass="form-control" runat="server"></asp:TextBox>
                                                                
                            
                                                                </div>
                            
                                                            </asp:Panel>
                                                            <asp:Label ID="lblanswer_head" runat="server" Visible="false" Text='<%# Eval("m0_point_solid") %>' />
                                                            <asp:Label ID="lblremark_corehead" Visible="false" Text='<%# Eval("remark_solid") %>' CssClass="control-label" runat="server" />

                                                    </div>

                                                </ContentTemplate>
                                            </asp:UpdatePanel>

                                            <asp:UpdatePanel ID="_PnDottedSuperiorcor" runat="server">
                                                <ContentTemplate>

                                                    <div class="panel-body" >
                                                
                                                        <div class="form-group">
                                                            <asp:Label ID="lbl_DottedSuperiorcor" CssClass="control-label" runat="server" Text="dotted ประเมิน Superior's Rating(1-4) : " />
                                                        </div>
                                                        <asp:RadioButtonList ID="rdochoice_dotted_detail" runat="server" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" CssClass="radio-list-inline-emps" RepeatDirection="Vertical">
                                                        </asp:RadioButtonList>

                                                        <asp:RequiredFieldValidator ID="Req_rdochoice_dotted_detail"
                                                            ValidationGroup="SavePerformance" runat="server" ErrorMessage="กรุณาเลือกคำตอบให้ครบถ้วน"
                                                            Display="None" SetFocusOnError="true" ControlToValidate="rdochoice_dotted_detail" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExetenxder7655" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_rdochoice_dotted_detail" Width="160"
                                                        PopupPosition="BottomLeft" />
                        

                                                            <asp:Panel ID="panel_update_comment_coredotted" runat="server">
                                                                <div class="form-group">
                                                                    <asp:Label ID="lbl_cpmment_dotted_" CssClass="control-label" runat="server" Text="กรุณาระบุเหตุผล : " />
                                                                </div>
                                                                <div class="form-group">
                                                                    <asp:TextBox ID="txtremarkdotted_core" Text='<%# Eval("remark_dotted") %>' Style="overflow: auto" placeholder="กรอกเหตุผล..." TextMode="MultiLine" Rows="3" Columns="3" CssClass="form-control" runat="server"></asp:TextBox>
                                                                
                            
                                                                </div>
                            
                                                            </asp:Panel>
                                                            <asp:Label ID="lblanswer_dotted" runat="server" Visible="false" Text='<%# Eval("m0_point_dotted") %>' />
                                                            <asp:Label ID="lblremark_coredotted" Visible="false" Text='<%# Eval("remark_dotted") %>' CssClass="control-label" runat="server" />

                                                        </div>
                    

                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </ItemTemplate>

                                        <FooterTemplate>
                                            <asp:Panel style="text-align: right; background-color: chartreuse;" runat="server" id="div_dotted_core_value">
                                                <p>
                                                    <b>Dotted :</b>
                                                <asp:Label ID="lit_totaldotted_core_value_detail" runat="server" Font-Bold="true">
                                                </asp:Label>
                                                </p>
                                            </asp:Panel>
                                            <asp:Panel style="text-align: right; background-color: chartreuse;" runat="server" id="div_solid_core_value">
                                                <p>
                                                    <b>Solid :</b>
                                                <asp:Label ID="lit_totalsolid_core_value_detail" runat="server" Font-Bold="true">
                                                </asp:Label>
                                                </p>
                                            </asp:Panel>
                                        
                                        </FooterTemplate>
                                    
                                        
                                    
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                            

                        </div>

                    </ContentTemplate>
                </asp:UpdatePanel>

                <asp:UpdatePanel ID="_PanelViewGvViewCompetencies" runat="server">
                    <ContentTemplate>

                        <div id="GvViewCompetencies_viewscroll" style="overflow-x: auto; width: 100%" runat="server">

                            <asp:GridView ID="GvViewCompetencies" runat="server"
                                CssClass="table table-bordered table-hover table-responsive"
                                AutoGenerateColumns="false"
                                HeaderStyle-CssClass="danger" 
                                AllowPaging="false" 
                                ShowFooter="true"
                                DataKeyNames="m1_typeidx"
                                OnRowDataBound="gvRowDataBound">
                                <HeaderStyle CssClass="danger" Height="40px" Font-Size="Small" />
                                <RowStyle Font-Size="Small" />
                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First"
                                    LastPageText="Last" />
                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>

                                <Columns>

                                    <asp:TemplateField ItemStyle-Width="2%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Medium">
                                        <ItemTemplate>
                                            <b>
                                                <asp:Label ID="lblCompetencies" runat="server" Text="Competencies"></asp:Label>
                                                <asp:Label ID="lblu0_typeidx" Visible="false" runat="server" Text='<%# Eval("u0_typeidx") %>'></asp:Label></b>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-Width="2%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Medium">
                                        <ItemTemplate>

                                            <b>
                                                <asp:Label ID="lblno" runat="server"></asp:Label></b>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-Width="2%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Medium">
                                        <ItemTemplate>
                                            <b>
                                                <asp:Label ID="lblm1_typeidx" Visible="false" runat="server" Text='<%# Eval("m1_typeidx") %>'></asp:Label>
                                                <asp:Label ID="lblm1_type_name" runat="server" Text='<%# Eval("m1_type_name") %>'></asp:Label>
                                                <asp:Label ID="lblu1_docidx" Visible="false" runat="server" Text='<%# Eval("u1_docidx") %>'></asp:Label></b>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-Width="24%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="smaller">
                                        <ItemTemplate>
                                            <asp:GridView ID="gvsubcompetencies_view" runat="server"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-borderedtable-responsive"
                                                HeaderStyle-CssClass="Medium"
                                                GridLines="None"
                                                ShowHeader="false"
                                                HeaderStyle-Height="40px"
                                                AllowPaging="false"
                                                DataKeyNames="m2_typeidx">


                                                <PagerStyle CssClass="pageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                </EmptyDataTemplate>
                                                <Columns>

                                                    <asp:TemplateField ItemStyle-CssClass="text-left" ItemStyle-Width="100%" Visible="true" ItemStyle-Font-Size="Medium">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblm2_coreidx" Visible="false" runat="server" Text='<%# Eval("m2_typeidx") %>' />
                                                            <small>
                                                                <asp:Label ID="lblm2_type_name" runat="server" Text='<%# Eval("m2_type_name") %>'></asp:Label>
                                                            </small>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>
                                        </ItemTemplate>
                                        <FooterTemplate>

                                            <div style="text-align: right;">
                                                <asp:Literal ID="lit_sumCompetencies_name_view" runat="server" Text="รวม :">
                                                </asp:Literal>
                                            </div>

                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-Width="35%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="Medium">
                                        <ItemTemplate>

                                            <asp:UpdatePanel ID="_PnSelfAssessmentcom" runat="server">
                                                <ContentTemplate>

                                                    <div class="panel-body">

                                                        <div class="form-group">
                                                        
                                                            <asp:Label ID="Ladbsel7gw" CssClass="control-label" runat="server" Text="ประเมินตนเอง Self Assessment (1-4) : " />
                                                            
                                                        </div>
                                                        
                                                        <asp:RadioButtonList ID="rdochoicecom_mine_detail" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="radio-list-inline-emps" RepeatDirection="Vertical">
                                                        </asp:RadioButtonList>
                                                        <asp:RequiredFieldValidator ID="Req_rdochoicecom_mine_detail"
                                                            ValidationGroup="SavePerformance" runat="server" ErrorMessage="กรุณาเลือกคำตอบให้ครบถ้วน"
                                                            Display="None" SetFocusOnError="true" ControlToValidate="rdochoicecom_mine_detail" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalsloutExtenxder7d22" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_rdochoicecom_mine_detail" Width="160"
                                                            PopupPosition="BottomLeft" />
                        
                        
                                                        <asp:Panel ID="panel_update_comment_competenciesmine" runat="server" >
                                                            <%--  <hr />--%>
                                                            <div class="form-group">
                                                                
                                                                <asp:Label ID="Ladbsel7" CssClass="control-label" runat="server" Text="กรุณาระบุเหตุผล : " />
                                                                
                                                            </div>
                                                            <div class="form-group">
                                                            
                                                                <asp:TextBox ID="txtremark_detail" Text='<%# Eval("remark_mine") %>' Style="overflow: auto" placeholder="กรอกเหตุผล..." TextMode="MultiLine" Rows="3" Columns="3" CssClass="form-control" runat="server"></asp:TextBox>
                                                                
                        
                        
                                                            
                                                            </div>
                        
                                                        </asp:Panel>
                                                        <asp:Label ID="lblanswer_com" runat="server" Visible="false" Text='<%# Eval("m0_point_mine") %>' />
                                                        <asp:Label ID="lblremark_commine" Visible="false" Text='<%# Eval("remark_mine") %>' CssClass="control-label" runat="server" />

                                                    </div>


                                                </ContentTemplate>
                                            </asp:UpdatePanel>

                                        </ItemTemplate>

                                        <FooterTemplate>
                                            <div style="text-align: right; background-color: chartreuse;">
                                                <asp:Label ID="lit_total_Competencies_view" runat="server" Font-Bold="true">
                                                </asp:Label>

                                            </div>
                                        </FooterTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-Width="35%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="Medium">
                                        <ItemTemplate>

                                            <asp:UpdatePanel ID="_PnSuperiorcom" runat="server">
                                                <ContentTemplate>
                                                
                                                    <!-- style="background-color:#99e699;" -->
                                                    <div class="panel-body" >
                                                    
                                                        <div class="form-group" >
                                                            <asp:Label ID="Ladbsel7g" CssClass="control-label" runat="server" Text="solid ประเมิน Superior's Rating(1-4) : " />
                                                            
                                                        </div>

                                                        <asp:RadioButtonList ID="rdochoicecom_head_detail" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="radio-list-inline-emps" RepeatDirection="Vertical">
                                                        </asp:RadioButtonList>
                                                        <asp:RequiredFieldValidator ID="Req_rdochoicecom_head_detail"
                                                            ValidationGroup="SavePerformance" runat="server" ErrorMessage="กรุณาเลือกคำตอบให้ครบถ้วน"
                                                            Display="None" SetFocusOnError="true" ControlToValidate="rdochoicecom_head_detail" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatofrCalloutExtenxder7wee" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_rdochoicecom_head_detail" Width="160"
                                                            PopupPosition="BottomLeft" />

                                                            <asp:Panel ID="panel_update_comment_competencieshead" runat="server">
                                                                <%-- <hr />--%>
                                                                <div class="form-group">
                                                                
                                                                    <asp:Label ID="Ladabel7xx" CssClass="control-label" runat="server" Text="กรุณาระบุเหตุผล : " />
                                                                
                                                                </div>
                                                                <div class="form-group">
                                                                    
                                                                    <asp:TextBox ID="txtremark_head" Text='<%# Eval("remark_solid") %>' Style="overflow: auto" placeholder="กรอกเหตุผล..." TextMode="MultiLine" Rows="3" Columns="3" CssClass="form-control" runat="server"></asp:TextBox>
                                                                    
                                                                    
                            
                                                                    
                                                                </div>
                            
                                                            </asp:Panel>
                                                            <asp:Label ID="lblanswer_headcom" runat="server" Visible="false" Text='<%# Eval("m0_point_solid") %>' />
                                                            <asp:Label ID="lblremark_comhead" Visible="false" Text='<%# Eval("remark_solid") %>' CssClass="control-label" runat="server" />
                                                    </div>


                                                </ContentTemplate>
                                            </asp:UpdatePanel>

                                            <asp:UpdatePanel ID="_PnDottedSuperiorcom" runat="server">
                                                <ContentTemplate>
                                                    <div class="clearfix"></div>
                                                    
                                                    <div class="panel-body" >
                                                        <div class="form-group">
                                                            <asp:Label ID="lbl_DottedSuperiorcom_" CssClass="control-label" runat="server" Text="dotted ประเมิน Superior's Rating(1-4) : " />
                                                        </div>

                                                        <asp:RadioButtonList ID="rdochoicecom_dotted_detail" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="radio-list-inline-emps" RepeatDirection="Vertical">
                                                        </asp:RadioButtonList>
                                                        <asp:RequiredFieldValidator ID="Req_rdochoicecom_dotted_detail"
                                                            ValidationGroup="SavePerformance" runat="server" ErrorMessage="กรุณาเลือกคำตอบให้ครบถ้วน"
                                                            Display="None" SetFocusOnError="true" ControlToValidate="rdochoicecom_dotted_detail" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatofrCalloutExtenxder7wee3" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_rdochoicecom_dotted_detail" Width="160"
                                                            PopupPosition="BottomLeft" />
                                                    

                                                            <asp:Panel ID="panel_update_comment_competenciesdotted" runat="server">
                                                                <%-- <hr />--%>
                                                                <div class="form-group">
                                                                
                                                                    <asp:Label ID="lbl_comment_competencies_" CssClass="control-label" runat="server" Text="กรุณาระบุเหตุผล : " />
                                                                
                                                                </div>
                                                                <div class="form-group">
                                                                    
                                                                    <asp:TextBox ID="txtremark_dotted" Text='<%# Eval("remark_dotted") %>' Style="overflow: auto" placeholder="กรอกเหตุผล..." TextMode="MultiLine" Rows="3" Columns="3" CssClass="form-control" runat="server"></asp:TextBox>
                                                                    
                                                                    
                            
                                                                </div>
                            
                                                            </asp:Panel>
                                                            <asp:Label ID="lblanswer_dottedcom" runat="server" Visible="false" Text='<%# Eval("m0_point_dotted") %>' />
                                                            <asp:Label ID="lblremark_comdotted" Visible="false" Text='<%# Eval("remark_dotted") %>' CssClass="control-label" runat="server" />

                                                    </div>


                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </ItemTemplate>

                                        <FooterTemplate>
                                            <asp:Panel style="text-align: right; background-color: chartreuse;" id="div_dotted_Competencies" runat="server">
                                                <p>
                                                    <b>Dotted :</b>
                                                <asp:Label ID="lit_totaldotted_Competencies_view" runat="server" Font-Bold="true">
                                                </asp:Label>
                                                </p>
                                            </asp:Panel>
                                            <asp:Panel style="text-align: right; background-color: chartreuse;" id="div_solid_Competencies" runat="server">
                                                <p>
                                                    <b>Solid :</b>
                                                <asp:Label ID="lit_totalsolid_Competencies_view" runat="server" Font-Bold="true">
                                                </asp:Label>
                                                </p>
                                            </asp:Panel>
                                        
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>

                            

                        </div>

                    </ContentTemplate>
                </asp:UpdatePanel>
                
                

                <div class="clearfix"></div>
                <div id ="_Divworkdetail" runat="server">

                    <div class="alert alert-warning f-s-14" role="alert"><b>ส่วนที่ 4:
                            สรุปการประเมินผลการปฏิบัติงานโดยรวม</b></div>

                </div>
                <asp:GridView ID="GvViewFactor" 
                    runat="server" AutoGenerateColumns="false"
                    CssClass="table table-bordered table-hover table-responsive"
                    HeaderStyle-CssClass="danger" 
                    AllowPaging="false" 
                    OnRowDataBound="gvRowDataBound"
                    ShowFooter="true">
                    <HeaderStyle CssClass="danger" Height="40px" Font-Size="Small" />
                    <RowStyle Font-Size="Small" />
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First"
                        LastPageText="Last" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">Data Cannot Be Found</div>
                    </EmptyDataTemplate>

                    <Columns>

                        <asp:TemplateField HeaderText="หัวข้อ/ปัจจัยประเมิน" ItemStyle-Width="40%" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="left">
                            <ItemTemplate>

                                <b>
                                    <asp:Label ID="lblfactor_name_detail" runat="server" Text='<%# Eval("factor_name") %>'>
                                    </asp:Label>
                                </b>

                                <div id="GvViewSub_Factor_scroll" style="overflow-x: auto; width: 100%" runat="server">
                                                
                                    <asp:GridView ID="GvViewSub_Factor" runat="server"
                                        AutoGenerateColumns="false"
                                        CssClass="table table-bordered table-hover table-responsive"
                                        HeaderStyle-CssClass="small" 
                                        GridLines="None" ShowHeader="true"
                                        HeaderStyle-Height="40px" AllowPaging="false">


                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                                            FirstPageText="First" LastPageText="Last" />

                                        <EmptyDataTemplate>
                                            <div style="text-align: center">Data Cannot Be Found</div>
                                        </EmptyDataTemplate>
                                        <Columns>

                                            <asp:TemplateField ItemStyle-CssClass="text-left" HeaderText="รายการ" HeaderStyle-CssClass="text-center"
                                            ItemStyle-HorizontalAlign="left">
                                                <ItemTemplate>
 
                                                    <small>
                                                        <p>
                                                            <asp:Label ID="lbl_time_performance_name_view" runat="server" Text='<%# Eval("time_performance_name") %>'>
                                                            </asp:Label>
                                                        </p>
                                                        
                                                    </small>

                                                </ItemTemplate>

                                            </asp:TemplateField>

                                            <asp:TemplateField ItemStyle-CssClass="text-right" HeaderText="จำนวน" HeaderStyle-CssClass="text-center"
                                            ItemStyle-HorizontalAlign="left">
                                                <ItemTemplate>
 
                                                    <small>
                                                        <p>
                                                            <asp:Label ID="lbl_time_score_detail_view" runat="server" Text='<%# getConvertString((string)Eval("time_score_detail")) %>' >
                                                            </asp:Label>
                                                        </p>
                                                        
                                                    </small>

                                                </ItemTemplate>

                                            </asp:TemplateField>

                                            <asp:TemplateField ItemStyle-CssClass="text-right" HeaderText="คะแนน" HeaderStyle-CssClass="text-center"
                                            ItemStyle-HorizontalAlign="left">
                                                <ItemTemplate>
 
                                                    <small>
                                                        <p>
                                                            <asp:Label ID="lbl_score_detail_view" runat="server" Text='<%# getConvertString((string)Eval("score_detail")) %>' >
                                                            </asp:Label>
                                                        </p>
                                                        
                                                    </small>

                                                </ItemTemplate>

                                            </asp:TemplateField>

                                        </Columns>
                                    </asp:GridView>

                                </div>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="น้ำหนัก" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>

                                <b>
                                    <asp:Label ID="lblcal_detail" runat="server" Text='<%# getConvertString((string)Eval("cal_detail")) %>' >
                                    </asp:Label>
                                </b>
                               
                            </ItemTemplate>


                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="คะแนน ผู้สร้าง (ก่อนคิดน้ำหนัก)" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="right">
                            <ItemTemplate>

                                <b>
                                    <asp:Label ID="lblscore_before_detail" runat="server" Text='<%# getConvertString((string)Eval("score_before")) %>' >
                                    </asp:Label>
                                   
                                </b>
                               
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="คะแนน dotted (ก่อนคิดน้ำหนัก)" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="right">
                            <ItemTemplate>

                                <b>
                                    <asp:Label ID="lbldottedscore_before_detail" runat="server" Text='<%# getConvertString((string)Eval("score_dotted_before")) %>' >
                                    </asp:Label>
                                
                                </b>
                            
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="คะแนน solid (ก่อนคิดน้ำหนัก)" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="right">
                            <ItemTemplate>

                                <b>
                                    <asp:Label ID="lblsolidscore_before_detail" runat="server" Text='<%# getConvertString((string)Eval("score_solid_before")) %>' >
                                    </asp:Label>
                                
                                </b>
                            
                            </ItemTemplate>
                            <FooterTemplate>
                                <div style="text-align: right;">
                                    <asp:Literal ID="lit_total_score_name" runat="server" Text="สรุปคะแนน :">
                                    </asp:Literal>
                                </div>
                            </FooterTemplate>
                        </asp:TemplateField>
                        

                        <asp:TemplateField HeaderText="คะแนนที่ได้(หลังคิดน้ำหนัก)" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="right">
                            <ItemTemplate>

                                <b>
                                    <asp:Label ID="lblscore_after_detail" runat="server" Text='<%# getConvertString((string)Eval("score_after")) %>' >
                                    </asp:Label>
                                </b>
                               
                            </ItemTemplate>
                            <FooterTemplate>
                                <div style="text-align: right; background-color: chartreuse;">
                                    <asp:Label ID="lit_total_score" runat="server"
                                        Font-Bold="true"></asp:Label>

                                </div>
                            </FooterTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สรุปคะแนนประเมินผล" ItemStyle-Width="10%" Visible="false" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="center">
                            <ItemTemplate>

                                <b>
                                    <asp:Label ID="lbltotal_score_detail" runat="server">
                                    </asp:Label>
                                </b>
                               
                            </ItemTemplate>
                           
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>

                <div class="clearfix"></div>
                <div id ="_Divdev" runat="server">

                    <div class="alert alert-warning f-s-14" role="alert"><b>ส่วนที่ 5:
                            ความสามารถที่โดดเด่นและด้านที่ต้องพัฒนาเพิ่มเติม</b></div>

                    <h5 style="font-size: small;"><b>คำชี้แจง: </b></h5>
                    <h5 style="font-size: medium;"><b>5.1 ด้านที่ต้องพัฒนาเพิ่มเติม</b></h5>

                </div>

                <div  id ="_DivComment" runat="server" class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-pencil"></i><strong>&nbsp;
                                ด้านที่ต้องพัฒนาเพิ่มเติม</strong></h3>
                    </div>

                    <div class="panel-body">
                        <div class="form-horizontal" role="form">

                            <asp:FormView ID="FvViewComment" runat="server" DefaultMode="ReadOnly" Width="100%">
                                <ItemTemplate>
                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">
        
                                            <div class="form-group">
        
                                                <label class="col-md-3 control-label">
                                                    <h2 class="panel-title">
                                                        <asp:Label ID="Label4" runat="server"
                                                            Text="ข้อคิดเห็น & แผนพัฒนา (โปรดระบุขั้นตอนกิจกรรม กำหนดเวลา และผู้รับผิดชอบ)" />
                                                        <br />
                                                        <small><b>
                                                                <asp:Label ID="Label5" runat="server"
                                                                    Text="Comment & Development Actions Plan (please identify action, timeline and person in charge)" />
                                                                </b></small>
                                                    </h2>
                                                </label>
        
                                                <div class="col-md-9">
                                                    <asp:Label ID="lbl_u2idx_view" visible="false" Text='<%# Eval("u2_docidx") %>' runat="server"></asp:Label>
                                                    <asp:TextBox ID="txtcomment_view" Text='<%# Eval("comment_name") %>' Style="overflow: auto" placeholder="กรอกข้อมูล..." TextMode="MultiLine" Rows="3"
                                                        CssClass="form-control" runat="server"></asp:TextBox>

                                                    <asp:RequiredFieldValidator ID="Req_txtcomment_view"
                                                    ValidationGroup="SavePerformance" runat="server" ErrorMessage="กรุณาข้อมุล"
                                                    Display="None" SetFocusOnError="true" ControlToValidate="txtcomment_view" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatofrCalloutExtenxder3rr" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_txtcomment_view" Width="160"
                                                        PopupPosition="BottomLeft" />

                                                </div>
        
                                            </div>
        
        
                                        </div>
                                    </div>
                                </ItemTemplate>

                                <InsertItemTemplate>
                                    
                                    <div class="form-group">

                                        <label class="col-md-3 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label4" runat="server"
                                                    Text="ข้อคิดเห็น & แผนพัฒนา (โปรดระบุขั้นตอนกิจกรรม กำหนดเวลา และผู้รับผิดชอบ)" />
                                                <br />
                                                <small><b>
                                                        <asp:Label ID="Label5" runat="server"
                                                            Text="Comment & Development Actions Plan (please identify action, timeline and person in charge)" />
                                                        </b></small>
                                            </h2>
                                        </label>

                                        <div class="col-md-9">
                                            <asp:Label ID="lbl_u2idx_view" visible="false" runat="server"></asp:Label>
                                            <asp:TextBox ID="txtcomment_view" Style="overflow: auto" placeholder="กรอกข้อมูล..." TextMode="MultiLine" Rows="5"
                                                CssClass="form-control" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="Req_txtcomment_view"
                                                ValidationGroup="SavePerformance" runat="server" ErrorMessage="กรุณาข้อมุล"
                                                Display="None" SetFocusOnError="true" ControlToValidate="txtcomment_view" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatofrCalloutExtenxder3rr1" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_txtcomment_view" Width="160"
                                                    PopupPosition="BottomLeft" />

                                            

                                        </div>

                                    </div>

                                </InsertItemTemplate>
                           
                            </asp:FormView>
                            
                            <asp:GridView ID="GvViewComment" runat="server"
                                CssClass="table table-bordered table-hover table-responsive"
                                AutoGenerateColumns="false"
                                HeaderStyle-CssClass="danger" 
                                AllowPaging="false" 
                                DataKeyNames="u2_docidx"
                                OnRowDataBound="gvRowDataBound">
                                <HeaderStyle CssClass="danger" Height="40px" Font-Size="Small" />
                                <RowStyle Font-Size="Small" />
                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First"
                                    LastPageText="Last" />
                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>
                                <Columns>

                                    <asp:TemplateField ItemStyle-Width="40%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="Label11" runat="server" Text='<%# Eval("FullNameTH") %>'></asp:Label>
                                            <br />
                                            <asp:Label ID="lblu2_docidx" Visible="false" runat="server" Text='<%# Eval("u2_docidx") %>'></asp:Label>
                                            <asp:Label ID="lblcemp_idx" Visible="false" runat="server" Text='<%# Eval("cemp_idx") %>'></asp:Label>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Eval("typename") %>'></asp:Label>
                                            <asp:Label ID="lblm0_typeidx_choose" Visible="false" runat="server" Text='<%# Eval("m0_typeidx_choose") %>'></asp:Label>

                                        </ItemTemplate>

                                        <EditItemTemplate>
                                            <div class="form-horizontal" role="form">
                                                <div class="panel-heading">
                                                    <div class="form-group">
                                                        <div class="col-md-2">
                                                            <asp:TextBox ID="txtu2_docidx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("u2_docidx")%>' />
                                                        </div>
                                                    </div>


                                                    <div class="form-group">

                                                        <label class="col-md-3 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label4" runat="server" Text="ข้อคิดเห็น & แผนพัฒนา (โปรดระบุขั้นตอนกิจกรรม กำหนดเวลา และผู้รับผิดชอบ)" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label5" runat="server" Text="Comment & Development Actions Plan (please identify action, timeline and person in charge)" /></b></small>
                                                            </h2>
                                                        </label>

                                                        <div class="col-md-9">
                                                            <asp:TextBox ID="txtcomment_edit" TextMode="MultiLine" Rows="3" runat="server" CssClass="form-control" Text='<%# Eval("comment_name")%>' />

                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="Save_edit" runat="server" Display="None" ControlToValidate="txtcomment_edit" Font-Size="11"
                                                                ErrorMessage="กรุณาใส่สถานะรายการ"
                                                                ValidationExpression="กรุณาใส่สถานะรายการ"
                                                                SetFocusOnError="true" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                                                ValidationGroup="Save_edit" Display="None"
                                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                                ControlToValidate="txtcomment_edit"
                                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                                SetFocusOnError="true" />

                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator2" Width="160" />

                                                        </div>
                                                    </div>

                                            

                                                    <div class="form-group">
                                                        <div class="col-md-2 col-md-offset-10">
                                                            <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success btn-sm" runat="server" ValidationGroup="Save_edit" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>


                                        </EditItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-Width="50%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>

                                            <asp:Label ID="Ldabel3" runat="server" Text='<%# Eval("comment_name") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="จัดการข้อมูล" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="Edit_improve" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>

                        </div>
                    </div>
                
                </div>

                <div id ="_DivStar" runat="server">
                    <h5 style="font-size: medium;"><b>5.2 ความสามารถที่โดดเด่น</b></h5>
                </div>

                <div id ="_DivStarDetail" runat="server" class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-star"></i><strong>&nbsp;
                                ความสามารถที่โดดเด่น</strong></h3>
                    </div>

                    <div class="panel-body">
                        <div class="form-horizontal" role="form">

                            <asp:FormView ID="FvViewStar" runat="server" DefaultMode="ReadOnly" Width="100%">

                                <ItemTemplate>

                                    <div class="form-group">

                                        <label class="col-md-3 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label44444" runat="server"
                                                    Text="ข้อคิดเห็น & แผนพัฒนา (โปรดระบุขั้นตอนกิจกรรม กำหนดเวลา และผู้รับผิดชอบ)" />
                                                <br />
                                                <small><b>
                                                        <asp:Label ID="Label44" runat="server"
                                                            Text="Comment & Development Actions Plan (please identify action, timeline and person in charge)" />
                                                        </b></small>
                                            </h2>
                                        </label>

                                        <div class="col-md-9">
                                            <asp:Label ID="lbl_u2idx_star_view" visible="false" Text='<%# Eval("u2_docidx") %>' runat="server"></asp:Label>
                                            <asp:TextBox ID="txtcomment_star_view" Text='<%# Eval("comment_name") %>' Style="overflow: auto" placeholder="กรอกข้อมูล..." TextMode="MultiLine" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="Req_txtcomment_star_view"
                                                ValidationGroup="SavePerformance" runat="server" ErrorMessage="กรุณาข้อมุล"
                                                Display="None" SetFocusOnError="true" ControlToValidate="txtcomment_star_view" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatofrCalloutExtenxder3rr12" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_txtcomment_star_view" Width="160"
                                                    PopupPosition="BottomLeft" />


                                            
                                        </div>

                                    </div>
        
                                       
                                </ItemTemplate>

                                <InsertItemTemplate>
                                    <div class="form-group">

                                        <label class="col-md-3 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label4222" runat="server"
                                                    Text="ข้อคิดเห็น & แผนพัฒนา (โปรดระบุขั้นตอนกิจกรรม กำหนดเวลา และผู้รับผิดชอบ)" />
                                                <br />
                                                <small><b>
                                                        <asp:Label ID="Label5" runat="server"
                                                            Text="Comment & Development Actions Plan (please identify action, timeline and person in charge)" />
                                                        </b></small>
                                            </h2>
                                        </label>
                                        <asp:Label ID="lbl_u2idx_star_view" visible="false" runat="server"></asp:Label>

                                        <div class="col-md-9">
                                            <asp:TextBox ID="txtcomment_star_view" Style="overflow: auto" placeholder="กรอกข้อมูล..." TextMode="MultiLine" Rows="5"
                                                CssClass="form-control" runat="server"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="Req_txtcomment_star_view"
                                            ValidationGroup="SavePerformance" runat="server" ErrorMessage="กรุณาข้อมุล"
                                            Display="None" SetFocusOnError="true" ControlToValidate="txtcomment_star_view" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatofrCalloutExtenxder3rr121" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_txtcomment_star_view" Width="160"
                                                PopupPosition="BottomLeft" />
                                            

                                        </div>

                                    </div>
  
                                </InsertItemTemplate>

                            </asp:FormView>
                            
                            
                            <asp:GridView ID="GvViewStar" runat="server"
                                CssClass="table table-bordered table-hover table-responsive"
                                AutoGenerateColumns="false"
                                HeaderStyle-CssClass="danger" 
                                AllowPaging="false" 
                                DataKeyNames="u2_docidx"
                                OnRowDataBound="gvRowDataBound">
                                <HeaderStyle CssClass="danger" Height="40px" Font-Size="Small" />
                                <RowStyle Font-Size="Small" />
                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First"
                                    LastPageText="Last" />
                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>

                                <Columns>

                                    <asp:TemplateField HeaderText="ชื่อพฤติกรรม/ปัจจัย(Behavior/Factor Name)" ItemStyle-Width="40%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="Label11" runat="server" Text='<%# Eval("FullNameTH") %>'></asp:Label>
                                            <br />
                                            <asp:Label ID="lblu2_docidx" Visible="false" runat="server" Text='<%# Eval("u2_docidx") %>'></asp:Label>
                                            <asp:Label ID="lblcemp_idx" Visible="false" runat="server" Text='<%# Eval("cemp_idx") %>'></asp:Label>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Eval("typename") %>'></asp:Label>
                                            <asp:Label ID="lblm0_typeidx_choose" Visible="false" runat="server" Text='<%# Eval("m0_typeidx_choose") %>'></asp:Label>

                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <div class="form-horizontal" role="form">
                                                <div class="panel-heading">
                                                    <div class="form-group">
                                                        <div class="col-md-2">
                                                            <asp:TextBox ID="txtu2_docidx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("u2_docidx")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">

                                                        <label class="col-md-3 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label4" runat="server" Text="ข้อคิดเห็น & แผนพัฒนา (โปรดระบุขั้นตอนกิจกรรม กำหนดเวลา และผู้รับผิดชอบ)" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label5" runat="server" Text="Comment & Development Actions Plan (please identify action, timeline and person in charge)" /></b></small>
                                                            </h2>
                                                        </label>

                                                        <div class="col-md-9">
                                                            <asp:TextBox ID="txtcomment_edit" TextMode="MultiLine" Rows="3" runat="server" CssClass="form-control" Text='<%# Eval("comment_name")%>' />

                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="Save_edit" runat="server" Display="None" ControlToValidate="txtcomment_edit" Font-Size="11"
                                                                ErrorMessage="กรุณาใส่สถานะรายการ"
                                                                ValidationExpression="กรุณาใส่สถานะรายการ"
                                                                SetFocusOnError="true" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                                                ValidationGroup="Save_edit" Display="None"
                                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                                ControlToValidate="txtcomment_edit"
                                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                                SetFocusOnError="true" />

                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator2" Width="160" />

                                                        </div>
                                                    </div>

                                                   

                                                    <div class="form-group">
                                                        <div class="col-md-2 col-md-offset-10">
                                                            <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success btn-sm" runat="server" ValidationGroup="Save_edit" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>


                                        </EditItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ข้อคิดเห็น & แผนพัฒนา (โปรดระบุขั้นตอนกิจกรรม กำหนดเวลา และผู้รับผิดชอบ)Comment & Development Actions Plan (please identify action, timeline and person in charge)" ItemStyle-Width="50%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>

                                            <asp:Label ID="Labesl3" runat="server" Text='<%# Eval("comment_name") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="จัดการข้อมูล" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="Edit_star" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                          

                                        </ItemTemplate>
                                    </asp:TemplateField>


                                </Columns>
                            </asp:GridView>

                        </div>
                    </div>

                </div>

                <div id="div_ViewNode1" runat="server">
                    <!-- <label>&nbsp;</label>
                    <div class="clearfix"></div> -->
                    <div class="form-group pull-right">

                     
                        <asp:Repeater ID="rptViewBindbtnNode1" OnItemDataBound="rptOnRowDataBound" runat="server">
                            <ItemTemplate>
                                <asp:Label ID="lbcheck_colerNode1_view" runat="server" Visible="false"
                                    Text='<%# Eval("decision_name") %>'></asp:Label>
                                <asp:LinkButton ID="btn_ViewNode1" CssClass="btn btn-primary" runat="server" ValidationGroup="SavePerformance"
                                    data-original-title='<%# Eval("decision_name") %>' data-toggle="tooltip"
                                    CommandArgument='<%# Eval("decision_idx")%>' OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')"
                                    Text='<%# Eval("decision_name") %>' CommandName="cmdSave"></asp:LinkButton>


                            </ItemTemplate>
                        </asp:Repeater>

                        <asp:LinkButton ID="btnHRCheckPermission" CssClass="btn btn-warning" data-toggle="tooltip" title="ข้อมูลไม่เรียบร้อย"
                        runat="server" CommandArgument="12" CommandName="cmdHRCheck" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')"> <i class="glyphicon glyphicon-envelope" aria-hidden="true"></i> ทำการแก้ไข</asp:LinkButton>

                        
                        <asp:LinkButton ID="btnBackView" CssClass="btn btn-default" data-toggle="tooltip" title="ย้อนกลับ"
                        runat="server" CommandName="cmdCancel" OnCommand="btnCommand"><i class="fa fa-reply"
                            aria-hidden="true"></i> ย้อนกลับ</asp:LinkButton>

                    </div>

                </div>
                

                
                <div class="clearfix"></div>

                 <!-- Log Detail OT Mont -->
                 <div class="panel panel-info" id="div_LogViewDetail" runat="server" visible="false">
                    <div class="panel-heading">ประวัติการดำเนินการ</div>
                    <table class="table table-striped f-s-12 table-empshift-responsive">
                        <asp:Repeater ID="rptLogViewDetail" runat="server">
                            <HeaderTemplate>
                                <tr>
                                    <th>วัน / เวลา</th>
                                    <th>ผู้ดำเนินการ</th>
                                    <th>ดำเนินการ</th>
                                
                                
                                </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td data-th="วัน / เวลา"><%# Eval("create_date") %> <%# Eval("time_create") %></td>
                                    <td data-th="ผู้ดำเนินการ"><%# Eval("emp_name_th") %> ( <%# Eval("current_actor") %> )</td>
                                    <td data-th="ดำเนินการ"><%# Eval("current_decision") %></td>
                              
                                  
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                    <div class="m-t-10 m-b-10"></div>
                </div>
                <!-- Log Detail OT Mont -->


            </div>
        </asp:View>
        <!--View Detail-->

        <!--View Create-->
        <asp:View ID="docCreate" runat="server">
            <div class="col-md-12">

                <div id ="_DivCreate" runat="server">
                    <asp:Image ID="imgCreate" runat="server" class="img-responsive img-fluid" ImageUrl="~/images/pms-formresult/performance_system.png" Style="height: 100%; width: 100%;" />

                </div>
                <br>
                <div class="clearfix"></div>

                <div>
                    <div class="alert alert-danger f-s-16" style="text-align: center" role="alert"><b>
                            <asp:Label ID="lblFormName" runat="server"></asp:Label>
                        </b></div>
                </div>

                <asp:FormView ID="fvEmpDetail" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <ItemTemplate>
                        <asp:HiddenField ID="hfEmpOrgIDX" runat="server" Value='<%# Eval("org_idx") %>' />
                        <asp:HiddenField ID="hfEmpRdeptIDX" runat="server" Value='<%# Eval("dept_idx") %>' />
                        <asp:HiddenField ID="hfEmpRsecIDX" runat="server" Value='<%# Eval("sec_idx") %>' />

                        <div class="alert alert-warning f-s-14" role="alert"><b>ส่วนที่ 1:
                                ข้อมูลทั่วไปเกี่ยวกับพนักงาน</b>
                        </div>

                        <div class="panel panel-default">

                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp;
                                    ข้อมูลส่วนตัวผู้ทำรายการ</strong></h3>
                            </div>

                            <div class="panel-body">
                                <div class="col-md-10 col-md-offset-1">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="f-s-13">รหัสผู้ทำรายการ (Employee ID)</label>
                                            <asp:TextBox ID="tbEmpCode" runat="server" Text='<%# Eval("emp_code") %>' CssClass="form-control" Enabled="false" />
                                            

                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="f-s-13">ชื่อ-นามสกุล (Name - Lastname)</label>
                                            <asp:TextBox ID="tbEmpName" runat="server" Text='<%# Eval("emp_name_th") %>' CssClass="form-control" Enabled="false" />
                                            

                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="f-s-13">บริษัท (Organization)</label>
                                            <asp:TextBox ID="tbEmpOrg" runat="server" Text='<%# Eval("org_name_th") %>' CssClass="form-control" Enabled="false" />
                                            

                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="f-s-13">กลุ่มงาน (Workgroup)</label>
                                            <asp:TextBox ID="tbWorkGroup" runat="server" Text='<%# Eval("wg_name_th") %>' CssClass="form-control" Enabled="false" />
                                            

                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="f-s-13">สายงาน (Linework)</label>
                                            <asp:TextBox ID="tbLineWork" runat="server" Text='<%# Eval("lw_name_th") %>' CssClass="form-control" Enabled="false" />
                                            

                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="f-s-13">ฝ่าย (Division)</label>
                                            <asp:TextBox ID="tbEmpDept" runat="server" Text='<%# Eval("dept_name_th") %>' CssClass="form-control" Enabled="false" />
                                            

                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="f-s-13">แผนก / ส่วน (Department/ Section)</label>
                                            <asp:TextBox ID="tbEmpSec" runat="server" Text='<%# Eval("sec_name_th") %>' CssClass="form-control" Enabled="false" />
                                            

                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="f-s-13">ตำแหน่ง (Position)</label>
                                            <asp:TextBox ID="tbEmpPos" runat="server" Text='<%# Eval("pos_name_th") %>' CssClass="form-control" Enabled="false" />
                                            

                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="f-s-13">วันที่เริ่มงาน (Start date)</label>
                                            <asp:TextBox ID="txtstartdate" runat="server" Text='<%# Eval("emp_start_date") %>' CssClass="form-control" Enabled="false" />
                                            

                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="f-s-13">อีเมล์ (E-mail)</label>
                                            <asp:TextBox ID="txtemail" runat="server" Text='<%# Eval("emp_org_mail") %>' CssClass="form-control" Enabled="false" />
                                            

                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="f-s-13">Joblevel (Joblevel)</label>
                                            <asp:TextBox ID="txt_joblevel" runat="server" Text='<%# String.Format("{0}{1}", Eval("jobgrade_name"), Eval("joblevel_name")) %>'  CssClass="form-control" Enabled="false" />
                                            

                                        </div>
                                    </div>

                                    <div class="col-md-6"></div>

                                </div>

                            </div>

                        </div>


                    </ItemTemplate>
                </asp:FormView>

                <div>

                    <div class="alert alert-warning f-s-14" role="alert"><b>ส่วนที่ 2:
                            การประเมินผลตามดัชนีชี้วัดผลการปฏิบัติงาน (KPIs)</b></div>
                </div>

                <asp:UpdatePanel ID="_PanelGvKPIs" runat="server">
                    <ContentTemplate>

                        <div id="GvKPIs_scroll" style="overflow-x: auto; width: 100%" runat="server">

                            <asp:GridView ID="GvKPIs" runat="server" 
                                AutoGenerateColumns="false"
                                CssClass="table table-bordered table-hover table-responsive"
                                HeaderStyle-CssClass="danger" 
                                AllowPaging="false" 
                                DataKeyNames="m0_kpi_idx"
                                OnRowDataBound="gvRowDataBound" 
                                ShowFooter="True">
                                <HeaderStyle CssClass="danger" Height="40px" Font-Size="Small" />
                                <RowStyle Font-Size="Small" />
                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First"
                                    LastPageText="Last" />
                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>

                                <Columns>

                                   
                                    <asp:TemplateField ItemStyle-Width="25%" HeaderStyle-CssClass="text-center"
                                    ItemStyle-HorizontalAlign="left" >
                                        <ItemTemplate>
                                            <b>
                                                <asp:Label ID="lblm1_kpi_idx" Visible="false" runat="server"
                                                    Text='<%# Eval("m1_kpi_idx") %>'></asp:Label>
                                                <asp:Label ID="lblperf_indicators" runat="server"
                                                    Text='<%# Eval("perf_indicators") %>'></asp:Label>
                                            </b>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-Width="10%" HeaderStyle-CssClass="text-center"
                                        ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <b>
                                                <p>
                                                    <asp:Label ID="lblkpi_unit" runat="server"
                                                        Text='<%# Eval("kpi_unit") %>'></asp:Label>
                                                </p>
                                            </b>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-Width="5%" HeaderStyle-CssClass="text-center"
                                        ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <b>
                                                <p>
                                                    <asp:Label ID="lblkpi_weight" runat="server" Text='<%# getConvertString((string)Eval("kpi_weight")) %>' ></asp:Label>
                                                </p>
                                            </b>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-Width="10%" HeaderStyle-CssClass="text-center"
                                        ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <b>
                                                <p>
                                                    <asp:Label ID="lblkpi_target" runat="server"
                                                        Text='<%# Eval("kpi_target") %>'></asp:Label>
                                                </p>
                                            </b>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-Width="20%" HeaderStyle-CssClass="text-center"
                                        ItemStyle-HorizontalAlign="left">
                                        <ItemTemplate>
                                            <asp:UpdatePanel ID="Update_performance_point" runat="server">
                                                <ContentTemplate>

                                                    <asp:TextBox ID="txtkpi_insert" TextMode="MultiLine" Rows="3" Style="overflow: auto" ValidationGroup="SavePerformance"
                                                        Columns="3" CssClass="form-control" runat="server" placeholder="กรอกผลลัพธ์...">
                                                    </asp:TextBox>
                                                    <asp:Label ID="lbl_comment_kpi" runat="server"><span class="text-danger">*</span></asp:Label>

                                                    <asp:RequiredFieldValidator ID="Re_txtkpi_insert"
                                                        runat="server"
                                                        ControlToValidate="txtkpi_insert" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณากรอกผลลัพธ์"
                                                        ValidationGroup="SavePerformance" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender61111" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txtkpi_insert" Width="160" PopupPosition="BottomLeft" />

                                                    <asp:Label ID="lblpoint" runat="server"></asp:Label>
                                                    <asp:RadioButtonList ID="rdoperformance_point" Enabled="false"
                                                        OnSelectedIndexChanged="SelectedIndexChanged"
                                                        AutoPostBack="true" runat="server" visible="false"
                                                        CssClass="radio-list-inline-emps" RepeatDirection="Vertical">
                                                    </asp:RadioButtonList>
                                                    <asp:RequiredFieldValidator ID="Req_rdoperformance_point"
                                                        ValidationGroup="SavePerformance" runat="server" Font-Size="13px"
                                                        ForeColor="Red" ErrorMessage="กรุณาเลือกคำตอบให้ครบถ้วน"
                                                        Display="None" SetFocusOnError="true"
                                                        ControlToValidate="rdoperformance_point" />
                                                    <ajaxToolkit:ValidatorCalloutExtender
                                                        ID="ValidatorCaslloutExtenxder71" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight"
                                                        TargetControlID="Req_rdoperformance_point" Width="160"
                                                        PopupPosition="BottomLeft" />

                                                </ContentTemplate>

                                            </asp:UpdatePanel>
                                        </ItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-Width="20%" HeaderStyle-CssClass="text-center"
                                        ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="Medium">
                                        <ItemTemplate>
                                            <asp:UpdatePanel ID="Update_performance_level" runat="server">
                                                <ContentTemplate>

                                                    <asp:RadioButtonList ID="rdoperformance_level"
                                                        OnSelectedIndexChanged="SelectedIndexChanged"
                                                        AutoPostBack="true" runat="server"
                                                        CssClass="radio-list-inline-emps" RepeatDirection="Vertical">
                                                    </asp:RadioButtonList>

                                                    <asp:RequiredFieldValidator ID="Req_rdoperformance_level"
                                                        ValidationGroup="SavePerformance" runat="server" 
                                                        ErrorMessage="กรุณาเลือกคำตอบให้ครบถ้วน"
                                                        Display="None" SetFocusOnError="true"
                                                        ControlToValidate="rdoperformance_level" />
                                                    <ajaxToolkit:ValidatorCalloutExtender
                                                        ID="ValidatorCaslloutExtenxder722" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight"
                                                        TargetControlID="Req_rdoperformance_level" Width="160"
                                                        PopupPosition="BottomLeft" />

                                                </ContentTemplate>

                                            </asp:UpdatePanel>
                                        </ItemTemplate>

                                        <FooterTemplate>

                                            <div style="text-align: right;">
                                                <asp:Literal ID="lit_sumpoint_name" runat="server" Text="คะแนนที่ได้ :">
                                                </asp:Literal>
                                            </div>

                                        </FooterTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-Width="10%" HeaderStyle-CssClass="text-center"
                                        ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <b>
                                                <p>
                                                    <asp:Label ID="lblweighted_score" runat="server"></asp:Label>
                                                </p>
                                            </b>
                                        </ItemTemplate>

                                        <FooterTemplate>
                                            <div style="text-align: right; background-color: chartreuse;">
                                                <asp:Label ID="lit_total_weighted_score" runat="server"
                                                    Font-Bold="true"></asp:Label>

                                            </div>
                                        </FooterTemplate>

                                    </asp:TemplateField>



                                </Columns>
                            </asp:GridView>

                        </div>

                    </ContentTemplate>
                </asp:UpdatePanel>


                <div>

                    <div class="alert alert-warning f-s-14" role="alert"><b>ส่วนที่ 3:
                            การประเมินผลการปฏิบัติงานและการให้ข้อมูลป้อนกลับ</b></div>

                    <h5 style="font-size: small;"><b>คำชี้แจง: ในการประเมินผลปลายปี เราจะใช้เกณฑ์คะแนนประเมินพฤติกรรม 1
                            - 4
                            โดยแต่ละคะแนนจะมีความหมายของการประเมินพฤติกรรมเพื่อการพิจารณาตามตารางด้านล่างนี้</b>
                    </h5>

                </div>

                <asp:GridView ID="GvMasterPoint" runat="server" AutoGenerateColumns="false"
                    CssClass="table table-bordered table-hover table-responsive"
                    HeaderStyle-CssClass="danger" AllowPaging="false" DataKeyNames="m0_point"
                    OnRowDataBound="gvRowDataBound">
                    <HeaderStyle CssClass="danger" Height="40px" Font-Size="Small" />
                    <RowStyle Font-Size="Small" />
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First"
                        LastPageText="Last" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">Data Cannot Be Found</div>
                    </EmptyDataTemplate>

                    <Columns>

                        <asp:TemplateField ItemStyle-Width="10%" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <b>
                                    <asp:Label ID="lblScore" runat="server" Text='<%# Eval("point_name") %>'>
                                    </asp:Label>
                                </b>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-Width="90%" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <b>
                                    <p>
                                        <asp:Label ID="lblRatth" runat="server" Text='<%# Eval("rat_nameth") %>'>
                                        </asp:Label>
                                    </p>
                                    <p>
                                        <asp:Label ID="lblRaten" runat="server" Text='<%# Eval("rat_nameen") %>'>
                                        </asp:Label>
                                    </p>

                                </b>
                            </ItemTemplate>


                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-Width="60%" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="left" Visible = "false">
                            <ItemTemplate>
                                <p>
                                    <asp:Label ID="lblDefinitionth" runat="server" Text='<%# Eval("definition_th") %>'>
                                    </asp:Label>
                                </p>
                                <p>
                                    <asp:Label ID="lblDefinitionen" runat="server" Text='<%# Eval("definition_en") %>'>
                                    </asp:Label>
                                </p>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>

                <asp:UpdatePanel ID="_PanelCoreValue" runat="server">
                    <ContentTemplate>

                        <div id="GvCoreValue_scroll" style="overflow-x: auto; width: 100%" runat="server">

                            <asp:GridView ID="GvCoreValue" runat="server" AutoGenerateColumns="false"
                                CssClass="table table-bordered table-hover table-responsive"
                                HeaderStyle-CssClass="danger" AllowPaging="false" DataKeyNames="m1_coreidx"
                                OnRowDataBound="gvRowDataBound" ShowFooter="True">
                                <HeaderStyle CssClass="danger" Height="40px" Font-Size="Small" />
                                <RowStyle Font-Size="Small" />
                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First"
                                    LastPageText="Last" />
                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>
                                <Columns>

                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="5%"
                                        HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center"
                                        ItemStyle-Font-Size="Medium">
                                        <ItemTemplate>
                                            <b>
                                                <asp:Label ID="lblCorevalue" runat="server" Text="Core Values">
                                                </asp:Label>
                                                <asp:Label ID="lblm1_coreidx" Visible="false" runat="server"
                                                    Text='<%# Eval("m1_coreidx") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-Width="5%" HeaderStyle-CssClass="text-center"
                                        ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Medium">
                                        <ItemTemplate>

                                            <%--<b><%# (Container.DataItemIndex +1) %></b>--%>
                                            <b>
                                                <asp:Label ID="lblno" runat="server"
                                                    Text='<%# (Container.DataItemIndex +1) %>'></asp:Label>
                                            </b>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-Width="5%" HeaderStyle-CssClass="text-center"
                                        ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Medium">
                                        <ItemTemplate>
                                            <b>
                                                <asp:Label ID="lblcore_name" runat="server"
                                                    Text='<%# Eval("core_name") %>'></asp:Label>

                                                <br />
                                                <asp:Label ID="lbltype_core" runat="server"
                                                    Text='<%# Eval("type_core") %>'></asp:Label>
                                            </b>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-Width="40%" HeaderStyle-CssClass="text-center"
                                        ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="Medium">
                                        <ItemTemplate>
                                            
                                            <div id="GvSub_CoreValue_scroll" style="overflow-x: auto; width: 100%"
                                                runat="server">
                                                
                                                <asp:GridView ID="GvSub_CoreValue" runat="server"
                                                    AutoGenerateColumns="false"
                                                    CssClass="table table-bordered table-hover table-responsive"
                                                    HeaderStyle-CssClass="small" GridLines="None" ShowHeader="false"
                                                    HeaderStyle-Height="40px" AllowPaging="false"
                                                    DataKeyNames="m2_coreidx">


                                                    <PagerStyle CssClass="pageCustom" />
                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                                                        FirstPageText="First" LastPageText="Last" />

                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">Data Cannot Be Found</div>
                                                    </EmptyDataTemplate>
                                                    <Columns>

                                                        <asp:TemplateField ItemStyle-CssClass="text-left"
                                                            ItemStyle-Width="100%" Visible="true"
                                                            ItemStyle-Font-Size="Medium">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblm2_coreidx" Visible="false"
                                                                    runat="server" Text='<%# Eval("m2_coreidx") %>' />

                                                                <small>
                                                                    <p>
                                                                        <asp:Label ID="lblcore_nameen" runat="server"
                                                                            Text='<%# Eval("core_nameen") %>'>
                                                                        </asp:Label>
                                                                    </p>
                                                                    <p>
                                                                        <asp:Label ID="lblcore_nameth" runat="server"
                                                                            Text='<%# Eval("core_nameth") %>'>
                                                                        </asp:Label>
                                                                    </p>
                                                                </small>

                                                            </ItemTemplate>

                                                        </asp:TemplateField>

                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </ItemTemplate>

                                        <FooterTemplate>

                                            <div style="text-align: right;">
                                                <asp:Literal ID="lit_sumcore_value_name" runat="server" Text="รวม :">
                                                </asp:Literal>
                                            </div>

                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-Width="40%" HeaderStyle-CssClass="text-center"
                                        ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="Medium">
                                        <ItemTemplate>
                                            
                                            
                                            <asp:UpdatePanel ID="update_insert" runat="server">
                                                <ContentTemplate>

                                                    <asp:Label ID="lblcore_reason" runat="server"></asp:Label>
                                                    <asp:RadioButtonList ID="rdochoice_mine"
                                                        OnSelectedIndexChanged="SelectedIndexChanged"
                                                        AutoPostBack="true" runat="server"
                                                        CssClass="radio-list-inline-emps" RepeatDirection="Vertical">
                                                    </asp:RadioButtonList>

                                                    <asp:RequiredFieldValidator ID="Req_rdochoice_mine"
                                                        ValidationGroup="SavePerformance" runat="server" 
                                                        ErrorMessage="กรุณาเลือกคำตอบให้ครบถ้วน"
                                                        Display="None" SetFocusOnError="true"
                                                        ControlToValidate="rdochoice_mine" />
                                                    <ajaxToolkit:ValidatorCalloutExtender
                                                        ID="ValidatorCaslloutExtenxder7" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight"
                                                        TargetControlID="Req_rdochoice_mine" Width="160"
                                                        PopupPosition="BottomLeft" />

                                                    <asp:Panel ID="panel_add_comment_core" runat="server">
                                                        <%--   <hr />--%>
                                                        <div class="form-group">
                                                            <div class="col-md-12">
                                                                <asp:Label ID="Ladbeel7" CssClass="control-label"
                                                                    runat="server" Text="กรุณาระบุเหตุผล : " />
                                                            </div>
                                                        </div>
                                                        <div class="form-group">

                                                            <div class="col-md-12">
                                                                <asp:TextBox ID="txtcore_insert" Style="overflow: auto" TextMode="MultiLine" placeholder="กรอกเหตุผล..."
                                                                    Rows="3" Columns="3" CssClass="form-control"
                                                                    runat="server"></asp:TextBox>

                                                                   

                                                            </div>
                                                        </div>

                                                    </asp:Panel>

                                                </ContentTemplate>

                                            </asp:UpdatePanel>
                                        </ItemTemplate>

                                        <FooterTemplate>
                                            <div style="text-align: right; background-color: chartreuse;">
                                                <asp:Label ID="lit_total_core_value" runat="server" Font-Bold="true">
                                                </asp:Label>

                                            </div>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </div>

                    </ContentTemplate>
                </asp:UpdatePanel>

                <asp:UpdatePanel ID="_PanelCompetencies" runat="server">
                    <ContentTemplate>

                        <div id="GvCompetencies_scroll" style="overflow-x: auto; width: 100%" runat="server">
                            <asp:GridView ID="GvCompetencies" runat="server" AutoGenerateColumns="false"
                                CssClass="table  table-bordered table-hover table-responsive"
                                HeaderStyle-CssClass="danger" AllowPaging="false" DataKeyNames="m1_typeidx"
                                OnRowDataBound="gvRowDataBound" ShowFooter="True">
                                <HeaderStyle CssClass="danger" Height="40px" Font-Size="Small" />
                                <RowStyle Font-Size="Small" />
                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First"
                                    LastPageText="Last" />
                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>

                                <Columns>

                                    <asp:TemplateField HeaderText="ปัจจัยประเมิน Competency Factor" ItemStyle-Width="5%"
                                        HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center"
                                        ItemStyle-Font-Size="Medium">
                                        <ItemTemplate>
                                            <b>
                                                <asp:Label ID="lblCompetencies" runat="server" Text="Competencies">
                                                </asp:Label>
                                                <asp:Label ID="lblu0_typeidx" Visible="false" runat="server"
                                                    Text='<%# Eval("u0_typeidx") %>'></asp:Label>
                                            </b>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ปัจจัยประเมิน Competency Factor" ItemStyle-Width="5%"
                                        HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center"
                                        ItemStyle-Font-Size="Medium">
                                        <ItemTemplate>

                                            <b>
                                                <asp:Label ID="lblno" runat="server"></asp:Label>
                                            </b>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ปัจจัยประเมิน Competency Factor"
                                        ItemStyle-Width="10%" HeaderStyle-CssClass="text-center"
                                        ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Medium">
                                        <ItemTemplate>
                                            <b>
                                                <asp:Label ID="lblm1_typeidx" Visible="false" runat="server"
                                                    Text='<%# Eval("m1_typeidx") %>'></asp:Label>
                                                <asp:Label ID="lblm1_type_name" runat="server"
                                                    Text='<%# Eval("m1_type_name") %>'></asp:Label>
                                                <asp:Label ID="lblform_name" runat="server" Visible="false"
                                                    Text='<%# Eval("form_name") %>'></asp:Label>
                                            </b>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField
                                        HeaderText="พฤติกรรมการทำงานที่บริษัทคาดหวัง Behavioral Indicators"
                                        ItemStyle-Width="35%" HeaderStyle-CssClass="text-center"
                                        ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="smaller">
                                        <ItemTemplate>

                                            <div id="GvSub_Competencies_scroll" style="overflow-x: auto; width: 100%"
                                                runat="server">
                                                <asp:GridView ID="GvSub_Competencies" runat="server"
                                                    AutoGenerateColumns="false"
                                                    CssClass="table table-bordered table-responsive col-md-12"
                                                    HeaderStyle-CssClass="small" GridLines="None" ShowHeader="false"
                                                    HeaderStyle-Height="40px" AllowPaging="false"
                                                    DataKeyNames="m2_typeidx">


                                                    <PagerStyle CssClass="pageCustom" />
                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                                                        FirstPageText="First" LastPageText="Last" />

                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">Data Cannot Be Found</div>
                                                    </EmptyDataTemplate>
                                                    <Columns>

                                                        <asp:TemplateField ItemStyle-CssClass="text-left"
                                                            ItemStyle-Width="100%" Visible="true"
                                                            ItemStyle-Font-Size="Medium">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblm2_coreidx" Visible="false"
                                                                    runat="server" Text='<%# Eval("m2_typeidx") %>' />
                                                                <small>
                                                                    <asp:Label ID="lblm2_type_name" runat="server"
                                                                        Text='<%# Eval("m2_type_name") %>'></asp:Label>
                                                                </small>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                    </Columns>
                                                </asp:GridView>
                                            </div>

                                        </ItemTemplate>

                                        <FooterTemplate>

                                            <div style="text-align: right;">
                                                <asp:Literal ID="lit_sumCompetencies_name" runat="server" Text="รวม :">
                                                </asp:Literal>
                                            </div>

                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ประเมินตนเอง (1-4) Self Assessment"
                                        ItemStyle-Width="40%" HeaderStyle-CssClass="text-center"
                                        ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="Medium">
                                        <ItemTemplate>

                                            <asp:RadioButtonList ID="rdochoicecom_mine"
                                                OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true"
                                                runat="server" CssClass="radio-list-inline-emps"
                                                RepeatDirection="Vertical">
                                            </asp:RadioButtonList>

                                            <asp:RequiredFieldValidator ID="Req_rdochoicecom_mine"
                                                ValidationGroup="SavePerformance" runat="server" 
                                                ErrorMessage="กรุณาเลือกคำตอบให้ครบถ้วน"
                                                Display="None" SetFocusOnError="true"
                                                ControlToValidate="rdochoicecom_mine" />
                                            <ajaxToolkit:ValidatorCalloutExtender
                                                ID="ValidatorCaslloutExtenxder8" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                TargetControlID="Req_rdochoicecom_mine" Width="160"
                                                PopupPosition="BottomLeft" />


                                            <asp:Panel ID="panel_add_comment_competencies" runat="server">
                                                <%-- <hr />--%>
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Lacdbel7" CssClass="control-label" runat="server"
                                                            Text="กรุณาระบุเหตุผล : " />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <asp:TextBox ID="txtcom_insert" Style="overflow: auto" TextMode="MultiLine" Rows="3" placeholder="กรอกเหตุผล..."
                                                            Columns="3" CssClass="form-control" runat="server">
                                                        </asp:TextBox>
                                                       

                                                    </div>
                                                </div>

                                            </asp:Panel>
                                        </ItemTemplate>

                                        <FooterTemplate>
                                            <div style="text-align: right; background-color: chartreuse;">
                                                <asp:Label ID="lit_total_Competencies" runat="server" Font-Bold="true">
                                                </asp:Label>

                                            </div>
                                        </FooterTemplate>

                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>

                <div class="clearfix"></div>
                <div>

                    <div class="alert alert-warning f-s-14" role="alert"><b>ส่วนที่ 4:
                            สรุปการประเมินผลการปฏิบัติงานโดยรวม</b></div>

                </div>

                <asp:GridView ID="GvFactor" 
                    runat="server" AutoGenerateColumns="false"
                    CssClass="table table-bordered table-hover table-responsive"
                    HeaderStyle-CssClass="danger" 
                    AllowPaging="false" 
                    OnRowDataBound="gvRowDataBound">
                    <HeaderStyle CssClass="danger" Height="40px" Font-Size="Small" />
                    <RowStyle Font-Size="Small" />
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First"
                        LastPageText="Last" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">Data Cannot Be Found</div>
                    </EmptyDataTemplate>

                    <Columns>

                        <asp:TemplateField HeaderText="หัวข้อ/ปัจจัยประเมิน" ItemStyle-Width="40%" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="left">
                            <ItemTemplate>

                                <b>
                                    <asp:Label ID="lblfactor_name_detail" runat="server" Text='<%# Eval("factor_name") %>'>
                                    </asp:Label>
                                </b>

                                <div id="GvSub_Factor_scroll" style="overflow-x: auto; width: 100%" runat="server">
                                                
                                    <asp:GridView ID="GvSub_Factor" runat="server"
                                        AutoGenerateColumns="false"
                                        CssClass="table table-bordered table-hover table-responsive"
                                        HeaderStyle-CssClass="small" 
                                        GridLines="None" ShowHeader="true"
                                        HeaderStyle-Height="40px" AllowPaging="false">


                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                                            FirstPageText="First" LastPageText="Last" />

                                        <EmptyDataTemplate>
                                            <div style="text-align: center">Data Cannot Be Found</div>
                                        </EmptyDataTemplate>
                                        <Columns>

                                            <asp:TemplateField ItemStyle-CssClass="text-left" HeaderText="รายการ" HeaderStyle-CssClass="text-center"
                                            ItemStyle-HorizontalAlign="left">
                                                <ItemTemplate>
 
                                                    <small>
                                                        <p>
                                                            <asp:Label ID="lbl_time_performance_name" runat="server" Text='<%# Eval("time_performance_name") %>'>
                                                            </asp:Label>
                                                        </p>
                                                        
                                                    </small>

                                                </ItemTemplate>

                                            </asp:TemplateField>

                                            <asp:TemplateField ItemStyle-CssClass="text-right" HeaderText="จำนวน" HeaderStyle-CssClass="text-center"
                                            ItemStyle-HorizontalAlign="left">
                                                <ItemTemplate>
 
                                                    <small>
                                                        <p>
                                                            <asp:Label ID="lbl_time_score_detail" runat="server" Text='<%# getConvertString((string)Eval("time_score_detail")) %>' >
                                                            </asp:Label>
                                                        </p>
                                                        
                                                    </small>

                                                </ItemTemplate>

                                            </asp:TemplateField>

                                            <asp:TemplateField ItemStyle-CssClass="text-right" HeaderText="คะแนน" HeaderStyle-CssClass="text-center"
                                            ItemStyle-HorizontalAlign="left">
                                                <ItemTemplate>
 
                                                    <small>
                                                        <p>
                                                            <asp:Label ID="lbl_score_detail" runat="server" Text='<%# getConvertString((string)Eval("score_detail")) %>' >
                                                            </asp:Label>
                                                        </p>
                                                        
                                                    </small>

                                                </ItemTemplate>

                                            </asp:TemplateField>

                                        </Columns>
                                    </asp:GridView>

                                </div>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="น้ำหนัก" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>

                                <b>
                                    <asp:Label ID="lblcal_detail" runat="server" Text='<%# getConvertString((string)Eval("cal_detail")) %>' >
                                    </asp:Label>
                                </b>
                               
                            </ItemTemplate>


                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="คะแนนที่ได้(ก่อนคิดน้ำหนัก)" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="right">
                            <ItemTemplate>

                                <b>
                                    <asp:Label ID="lblscore_before_detail" runat="server" Text='<%# getConvertString((string)Eval("score_before")) %>' >
                                    </asp:Label>
                                   
                                </b>
                               
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="คะแนนที่ได้(หลังคิดน้ำหนัก)" ItemStyle-Width="15%" Visible="false" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="right">
                            <ItemTemplate>

                                <b>
                                    <asp:Label ID="lblscore_after_detail" runat="server" Text='<%# getConvertString((string)Eval("score_after")) %>' >
                                    </asp:Label>
                                </b>
                               
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สรุปคะแนนประเมินผล" ItemStyle-Width="15%" Visible="false" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="center">
                            <ItemTemplate>

                                <b>
                                    
                                    <asp:Label ID="lbltotal_score_detail" runat="server" Text='<%# getConvertString((string)Eval("total_score")) %>' >
                                    </asp:Label>
                                </b>
                               
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>

                <div class="clearfix"></div>
                <div>

                    <div class="alert alert-warning f-s-14" role="alert"><b>ส่วนที่ 5:
                            ความสามารถที่โดดเด่นและด้านที่ต้องพัฒนาเพิ่มเติม</b></div>

                    <h5 style="font-size: small;"><b>คำชี้แจง: </b></h5>
                    <h5 style="font-size: medium;"><b>5.1 ด้านที่ต้องพัฒนาเพิ่มเติม</b></h5>

                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-pencil"></i><strong>&nbsp;
                                ด้านที่ต้องพัฒนาเพิ่มเติม</strong></h3>
                    </div>
                    <asp:FormView ID="FvComment" runat="server" DefaultMode="ReadOnly" Width="100%">
                        <InsertItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">

                                        <label class="col-md-3 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label4" runat="server" Text="ข้อคิดเห็น & แผนพัฒนา (โปรดระบุขั้นตอนกิจกรรม กำหนดเวลา และผู้รับผิดชอบ)" />
                                                <asp:Label ID="lbl_comment_insert" runat="server"><span class="text-danger">*</span></asp:Label>
                                                <br />
                                                <small><b>
                                                        <asp:Label ID="Label5" runat="server"
                                                            Text="Comment & Development Actions Plan (please identify action, timeline and person in charge)" />
                                                        </b></small>
                                            </h2>
                                        </label>

                                        <div class="col-md-9">
                                            <asp:TextBox ID="txtcomment" TextMode="MultiLine" Rows="3" Style="overflow: auto" placeholder="กรอกข้อมูล..."
                                                CssClass="form-control" runat="server"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="Req_txtcomment"
                                                ValidationGroup="SavePerformance" runat="server" 
                                                ErrorMessage="กรุณากรอกข้อมูล"
                                                Display="None" SetFocusOnError="true"
                                                ControlToValidate="txtcomment" />
                                            <ajaxToolkit:ValidatorCalloutExtender
                                                ID="ValidatorCaslloutExtenxder9" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                TargetControlID="Req_txtcomment" Width="160"
                                                PopupPosition="BottomLeft" />

                                        </div>

                                    </div>


                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>

                </div>

                <h5 style="font-size: medium;"><b>5.2 ความสามารถที่โดดเด่น</b></h5>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-star"></i><strong>&nbsp;
                                ความสามารถที่โดดเด่น</strong></h3>
                    </div>

                    <asp:FormView ID="FvStar" runat="server" DefaultMode="ReadOnly" Width="100%">
                        <InsertItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">


                                    <div class="form-group">

                                        <label class="col-md-3 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label4" runat="server" Text="ข้อคิดเห็น & แผนพัฒนา (โปรดระบุขั้นตอนกิจกรรม กำหนดเวลา และผู้รับผิดชอบ)" />
                                                <asp:Label ID="lbl_commentstar_insert" runat="server"><span class="text-danger">*</span></asp:Label>
                                                <br />
                                                <small><b>
                                                        <asp:Label ID="Label5" runat="server"
                                                            Text="Comment & Development Actions Plan (please identify action, timeline and person in charge)" />
                                                        </b></small>
                                            </h2>
                                        </label>

                                        <div class="col-md-9">
                                            <asp:TextBox ID="txtcomment_star" TextMode="MultiLine" Rows="3" Style="overflow: auto" placeholder="กรอกข้อมูล..."
                                                CssClass="form-control" runat="server"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="Req_txtcomment_star"
                                                ValidationGroup="SavePerformance" runat="server" 
                                                ErrorMessage="กรุณากรอกข้อมูล"
                                                Display="None" SetFocusOnError="true"
                                                ControlToValidate="txtcomment_star" />
                                            <ajaxToolkit:ValidatorCalloutExtender
                                                ID="ValidatorCaslloutExtenxder10" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                TargetControlID="Req_txtcomment_star" Width="160"
                                                PopupPosition="BottomLeft" />


                                            
                                          

                                        </div>

                                    </div>

                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>

                </div>

                <div id="div_Node1" runat="server">
                    <label>&nbsp;</label>
                    <div class="clearfix"></div>
                    <div class="form-group pull-right">

                        <asp:Repeater ID="rptBindbtnNode1" OnItemDataBound="rptOnRowDataBound" runat="server">
                            <ItemTemplate>
                                <asp:Label ID="lbcheck_colerNode1" runat="server" Visible="false"
                                    Text='<%# Eval("decision_name") %>'></asp:Label>
                                <asp:LinkButton ID="btn_Node1" CssClass="btn btn-primary" ValidationGroup="SavePerformance" runat="server"
                                    data-original-title='<%# Eval("decision_name") %>' data-toggle="tooltip" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')"
                                    CommandArgument='<%# Eval("decision_idx")%>' OnCommand="btnCommand"
                                    Text='<%# Eval("decision_name") %>' CommandName="cmdSave"></asp:LinkButton>


                            </ItemTemplate>
                        </asp:Repeater>

                        <asp:LinkButton ID="btnBack" CssClass="btn btn-default" data-toggle="tooltip" title="ย้อนกลับ"
                            runat="server" CommandName="cmdCancel" OnCommand="btnCommand"><i class="fa fa-reply"
                                aria-hidden="true"></i> ย้อนกลับ</asp:LinkButton>

                    </div>

                </div>

            </div>

        </asp:View>
        <!--View Create-->

        <!--View Approve-->
        <asp:View ID="docApprove" runat="server">
            <div class="col-md-12">

                <div id ="_DivApprove" runat="server">
                    <asp:Image ID="imghead_approve" runat="server" class="img-responsive img-fluid" ImageUrl="~/images/pms-formresult/performance_system.png" Style="height: 100%; width: 100%;" />

                </div>
                <br>
                <div class="clearfix"></div>

                <asp:FormView ID="fvSearchApprove" runat="server" Width="100%" DefaultMode="Insert">
                    <InsertItemTemplate>

                        <div class="panel panel-danger">
                            <div class="panel-heading">
                                <h3 class="panel-title">ค้นหาข้อมูล</h3>
                            </div>

                            <div class="panel-body">
                                
                                <div class="col-md-10 col-md-offset-1">
                                    <div class="form-horizontal">
                                        
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">องค์กร</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlOrganization" runat="server"
                                                    Cssclass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChangedApprove"
                                                    AutoPostBack="true"></asp:DropDownList>
                                            </div>
                                            <label class="col-md-2 control-label">กลุ่มงาน</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlWorkGroup" runat="server"
                                                    Cssclass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChangedApprove"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">สายงาน</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlLineWork" runat="server"
                                                    Cssclass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChangedApprove"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                            <label class="col-md-2 control-label">ฝ่าย</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlDepartment" runat="server"
                                                    Cssclass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChangedApprove"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">แผนก</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlSection" runat="server" Cssclass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChangedApprove"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                            <label class="col-md-2 control-label">ตำแหน่ง</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlPosition" runat="server"
                                                    Cssclass="form-control">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2 control-label">รหัสพนักงาน</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="txt_empcode" runat="server" TextMode="MultiLine" Rows="2" Style="overflow: auto" placeholder="กรอกรหัสพนักงาน Ex.(58000106,58000107)..." CssClass="form-control" />
                                            </div>
                                            <label class="col-md-2 control-label"></label>
                                            <div class="col-md-4">
                                               
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2"></label>
                                            <div class="col-md-4">
                                                <asp:LinkButton ID="lbSearchData" runat="server"
                                                    CssClass="btn btn-md btn-primary" OnCommand="btnCommand"
                                                    CommandName="cmdSearchWaitApprove"><i class="fas fa-search"></i>&nbsp;ค้นหา
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="lbResetData" runat="server"
                                                    CssClass="btn btn-md btn-default" OnCommand="btnCommand"
                                                    CommandName="cmdResetWaitApprove"><i class="fas fa-redo"></i>&nbsp;เคลียร์ค่า
                                                </asp:LinkButton>
                                            </div>
                                            <label class="col-md-2"></label>
                                            <label class="col-md-4"></label>
                                        </div>
                                    </div>
                                </div>
                                <label class="col-md-1"></label>

                            </div>

                        </div>

                    </InsertItemTemplate>
                </asp:FormView>

                <asp:GridView ID="GvApprove" runat="server"
                    AutoGenerateColumns="False"
                    CssClass="table table-striped table-bordered table-hover table-responsive"
                    OnPageIndexChanging="gvPageIndexChanging"
                    OnRowDataBound="gvRowDataBound"
                    ShowHeaderWhenEmpty="false"
                    AllowPaging="false"
                    PageSize="10"
                    BorderStyle="None"
                    DataKeyNames="u0_docidx"
                    CellSpacing="2">
                    <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                    <RowStyle Font-Size="Small" />
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">Data Cannot Be Found</div>
                    </EmptyDataTemplate>

                    <Columns>

                    
                        <asp:TemplateField HeaderText="ข้อมูลผู้ประเมิน" ItemStyle-Width="30%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="left" ItemStyle-Font-Size="Small">
                            <ItemTemplate>
                                <asp:Label ID="lblu0_docidx" runat="server" Visible="false" Text='<%# Eval("u0_docidx") %>'></asp:Label>
                                <asp:Label ID="lblunidx" runat="server" Visible="false" Text='<%# Eval("unidx") %>'></asp:Label>
                                <asp:Label ID="lblacidx" runat="server" Visible="false" Text='<%# Eval("acidx") %>'></asp:Label>
                                <asp:Label ID="lblstaidx" runat="server" Visible="false" Text='<%# Eval("staidx") %>'></asp:Label>
                                <asp:Label ID="lblsolid" runat="server" Visible="false" Text='<%# Eval("solid") %>'></asp:Label>
                                <asp:Label ID="lbldotted" runat="server" Visible="false" Text='<%# Eval("dotted") %>'></asp:Label>
                                
                                <p>
                                    <b>องค์กร:</b>
                                    <asp:Label ID="lbl_org_idx" Visible="false" runat="server" Text='<%# Eval("org_idx") %>' />
                                    <asp:Label ID="lbl_org_name_th" Visible="true" runat="server" Text='<%# Eval("org_name_th") %>' />
                                </p>
                                <p>
                                    <b>ฝ่าย:</b>
                                    <asp:Label ID="lbl_rdept_idx" Visible="false" runat="server" Text='<%# Eval("rdept_idx") %>' />
                                    <asp:Label ID="lbl_dept_name_th" Visible="true" runat="server" Text='<%# Eval("dept_name_th") %>' />
                                </p>
                                <p>
                                    <b>แผนก:</b>
                                    <asp:Label ID="lbl_rsec_idx" Visible="false" runat="server" Text='<%# Eval("rsec_idx") %>' />
                                    <asp:Label ID="lbl_sec_name_th" Visible="true" runat="server" Text='<%# Eval("sec_name_th") %>' />
                                </p>
                                <p>
                                    <b>ตำแหน่ง:</b>
                                    <asp:Label ID="lbl_rpos_idx" Visible="false" runat="server" Text='<%# Eval("rpos_idx") %>' />
                                    <asp:Label ID="lbl_pos_name_th" Visible="true" runat="server" Text='<%# Eval("pos_name_th") %>' />
                                </p>
                                <p>
                                    <b>ชื่อ-สกุล:</b>
                                    <asp:Label ID="lbl_cemp_idx" Visible="false" runat="server" Text='<%# Eval("cemp_idx") %>' />
                                    <asp:Label ID="lbl_emp_name_th" Visible="true" runat="server" Text='<%# Eval("emp_name_th") %>' />
                                </p>

                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="คะแนนประเมินตนเอง" ItemStyle-Width="20%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="left" ItemStyle-Font-Size="Small">
                            <ItemTemplate>

                                <p>
                                    <b>KPIs:</b>
                                    <asp:Label ID="lbl_sum_kpis_value" Visible="true" runat="server" Text='<%# getConvertString((string)Eval("sum_kpis_value")) %>'  />
                                    
                                </p>

                                <p>
                                    <b>Core Value:</b>
                                    <asp:Label ID="lbl_sum_mine_core_value" Visible="true" runat="server" Text='<%# getConvertString((string)Eval("sum_mine_core_value")) %>' />
                                </p>
                                <p>
                                    <b>Competencies:</b>
                                    <asp:Label ID="lbl_sum_mine_competency" Visible="true" runat="server" Text='<%# getConvertString((string)Eval("sum_mine_competency")) %>'/>
                                </p>

                            

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="คะแนนผู้มีสิทธิ์ประเมิน" ItemStyle-Width="25%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="left" >
                            <ItemTemplate>

                                <p>
                                    <b>Dotted KPIs:</b>
                                    <asp:Label ID="lbl_sum_dotted_kpis_value" Visible="true" runat="server" Text='<%# getConvertString((string)Eval("sum_kpis_value")) %>'  />
                                    
                                </p>
                                
                                <p>
                                    <b>Dotted Core Value:</b>
                                    <asp:Label ID="lbl_sum_dotted_core_value" Visible="true" runat="server" Text='<%# getConvertString((string)Eval("sum_dotted_core_value")) %>'  />
                                </p>
                                <p>
                                    <b>Dotted Competencies:</b>
                                    <asp:Label ID="lbl_sum_dotted_competency" Visible="true" runat="server" Text='<%# getConvertString((string)Eval("sum_dotted_competency")) %>'  />
                                </p>

                                <p>
                                    <b>Solid KPIs:</b>
                                    <asp:Label ID="lbl_sum_solid_kpis_value" Visible="true" runat="server" Text='<%# getConvertString((string)Eval("sum_kpis_value")) %>'  />
                                    
                                </p>
                                
                                <p>
                                    <b>Solid Core Value:</b>
                                    <asp:Label ID="lbl_sum_solid_core_value" Visible="true" runat="server" Text='<%# getConvertString((string)Eval("sum_solid_core_value")) %>' />
                                </p>
                                <p>
                                    <b>Solid Competencies:</b>
                                    <asp:Label ID="lbl_sum_solid_competency" Visible="true" runat="server" Text='<%# getConvertString((string)Eval("sum_solid_competency")) %>' />
                                </p>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สถานะรายการ" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center" >
                            <ItemTemplate>
                                
                                    <b><asp:Label ID="lblcurrent_status" runat="server" Text='<%# Eval("current_status") %>' /></b>
                            
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnViewDetail" CssClass="btn-sm btn-warning" target="" runat="server" CommandName="cmdViewDetail" OnCommand="btnCommand" CommandArgument='<%# Eval("u0_docidx")+ ";" + Eval("unidx") + ";" + Eval("acidx") + ";" + Eval("staidx") + ";" + Eval("cemp_idx") + ";" + Eval("form_name") + ";" + Eval("sum_solid_core_value") + ";" + Eval("sum_solid_competency") + ";" + Eval("u0_typeidx") + ";" + Eval("solid")+ ";" + Eval("dotted")+ ";" + Eval("org_idx")%>' data-toggle="tooltip" title="ดูข้อมูล"><i class="fa fa-book"></i></asp:LinkButton>

                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>

            </div>
        </asp:View>
        <!--View Approve-->

        <!--View Approve 1-->
        <asp:View ID="docApprove1" runat="server">
            <div class="col-md-12">

                <div id ="_DivApprove1" runat="server">
                    <asp:Image ID="imghead_approve1" runat="server" class="img-responsive img-fluid" ImageUrl="~/images/pms-formresult/performance_system.png" Style="height: 100%; width: 100%;" />

                </div>
                <br>
                <div class="clearfix"></div>

                <asp:FormView ID="fvSearchApprove1" runat="server" Width="100%" DefaultMode="Insert">
                    <InsertItemTemplate>

                        <div class="panel panel-danger">
                            <div class="panel-heading">
                                <h3 class="panel-title">ค้นหาข้อมูล</h3>
                            </div>

                            <div class="panel-body">
                                
                                <div class="col-md-10 col-md-offset-1">
                                    <div class="form-horizontal">
                                        
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">องค์กร</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlOrganization" runat="server"
                                                    Cssclass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChangedApprove1"
                                                    AutoPostBack="true"></asp:DropDownList>
                                            </div>
                                            <label class="col-md-2 control-label">กลุ่มงาน</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlWorkGroup" runat="server"
                                                    Cssclass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChangedApprove1"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">สายงาน</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlLineWork" runat="server"
                                                    Cssclass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChangedApprove1"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                            <label class="col-md-2 control-label">ฝ่าย</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlDepartment" runat="server"
                                                    Cssclass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChangedApprove1"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">แผนก</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlSection" runat="server" Cssclass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChangedApprove1"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                            <label class="col-md-2 control-label">ตำแหน่ง</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlPosition" runat="server"
                                                    Cssclass="form-control">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2 control-label">รหัสพนักงาน</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="txt_empcode" runat="server" TextMode="MultiLine" Rows="2" Style="overflow: auto" placeholder="กรอกรหัสพนักงาน Ex.(58000106,58000107)..." CssClass="form-control" />
                                            </div>
                                            <label class="col-md-2 control-label"></label>
                                            <div class="col-md-4">
                                               
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2"></label>
                                            <div class="col-md-4">
                                                <asp:LinkButton ID="lbSearchData1" runat="server"
                                                    CssClass="btn btn-md btn-primary" OnCommand="btnCommand"
                                                    CommandName="cmdSearchWaitApprove1"><i class="fas fa-search"></i>&nbsp;ค้นหา
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="lbResetData1" runat="server"
                                                    CssClass="btn btn-md btn-default" OnCommand="btnCommand"
                                                    CommandName="cmdResetWaitApprove1"><i class="fas fa-redo"></i>&nbsp;เคลียร์ค่า
                                                </asp:LinkButton>
                                            </div>
                                            <label class="col-md-2"></label>
                                            <label class="col-md-4"></label>
                                        </div>
                                    </div>
                                </div>
                                <label class="col-md-1"></label>

                            </div>

                        </div>

                    </InsertItemTemplate>
                </asp:FormView>

                <div id="div_Approve1" runat="server">
                    <!-- <label>&nbsp;</label>
                    <div class="clearfix"></div> -->
                    <div class="form-group pull-right">

                     
                        <asp:Repeater ID="rptViewBindbtnNode4" OnItemDataBound="rptOnRowDataBound" runat="server">
                            <ItemTemplate>
                                <asp:Label ID="lbcheck_colerNode4_view" runat="server" Visible="false"
                                    Text='<%# Eval("decision_name") %>'></asp:Label>
                                <asp:LinkButton ID="btn_ViewNode4" CssClass="btn btn-primary" runat="server" ValidationGroup="SaveApprove1"
                                    data-original-title='<%# Eval("decision_name") %>' data-toggle="tooltip"
                                    CommandArgument='<%# Eval("decision_idx")%>' OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')"
                                    Text='<%# Eval("decision_name") %>' CommandName="cmdSaveApprove1"></asp:LinkButton>


                            </ItemTemplate>
                        </asp:Repeater>

                    </div>

                </div>

                <div id="GvApprove1_scroll" style="overflow-x: auto; width: 100%" runat="server">
                    <asp:GridView ID="GvApprove1" runat="server"
                        AutoGenerateColumns="False"
                        CssClass="table table-striped table-bordered table-hover table-responsive"
                        OnPageIndexChanging="gvPageIndexChanging"
                        OnRowDataBound="gvRowDataBound"
                        ShowHeaderWhenEmpty="false"
                        AllowPaging="false"
                        PageSize="10"
                        BorderStyle="None"
                        DataKeyNames="u0_docidx"
                        CellSpacing="2">
                        <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                        <RowStyle Font-Size="Small" />
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">Data Cannot Be Found</div>
                        </EmptyDataTemplate>

                        <Columns>

                            <asp:TemplateField HeaderText="ข้อมูลผู้ประเมิน" ItemStyle-Width="30%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="left" ItemStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <asp:Label ID="lblu0_docidx" runat="server" Visible="false" Text='<%# Eval("u0_docidx") %>'></asp:Label>
                                    <asp:Label ID="lblunidx" runat="server" Visible="false" Text='<%# Eval("unidx") %>'></asp:Label>
                                    <asp:Label ID="lblacidx" runat="server" Visible="false" Text='<%# Eval("acidx") %>'></asp:Label>
                                    <asp:Label ID="lblstaidx" runat="server" Visible="false" Text='<%# Eval("staidx") %>'></asp:Label>
                                    <asp:Label ID="lblsolid" runat="server" Visible="false" Text='<%# Eval("solid") %>'></asp:Label>
                                    <asp:Label ID="lbldotted" runat="server" Visible="false" Text='<%# Eval("dotted") %>'></asp:Label>
                                    
                                    
                                    <p>
                                        <b>รหัสพนักงาน:</b>
                                        <asp:Label ID="lbl_emp_code" Visible="true" runat="server" Text='<%# Eval("emp_code") %>' />
                                    </p>

                                    <p>
                                        <b>ชื่อ-สกุล:</b>
                                        <asp:Label ID="lbl_cemp_idx" Visible="false" runat="server" Text='<%# Eval("cemp_idx") %>' />
                                        <asp:Label ID="lbl_emp_name_th" Visible="true" runat="server" Text='<%# Eval("emp_name_th") %>' />
                                    </p>

                                    <p>
                                        <b>ตำแหน่ง:</b>
                                        <asp:Label ID="lbl_rpos_idx" Visible="false" runat="server" Text='<%# Eval("rpos_idx") %>' />
                                        <asp:Label ID="lbl_pos_name_th" Visible="true" runat="server" Text='<%# Eval("pos_name_th") %>' />
                                    </p>

                                    <p>
                                        <b>แผนก:</b>
                                        <asp:Label ID="lbl_rsec_idx" Visible="false" runat="server" Text='<%# Eval("rsec_idx") %>' />
                                        <asp:Label ID="lbl_sec_name_th" Visible="true" runat="server" Text='<%# Eval("sec_name_th") %>' />
                                    </p>

                                    <p>
                                        <b>ฝ่าย:</b>
                                        <asp:Label ID="lbl_rdept_idx" Visible="false" runat="server" Text='<%# Eval("rdept_idx") %>' />
                                        <asp:Label ID="lbl_dept_name_th" Visible="true" runat="server" Text='<%# Eval("dept_name_th") %>' />
                                    </p>

                                    <p>
                                        <b>สายงาน:</b>
                                        
                                        <asp:Label ID="lbl_lw_name_th" Visible="true" runat="server" Text='<%# Eval("lw_name_th") %>' />
                                    </p>

                                    <p>
                                        <b>กลุ่มงาน:</b>
                                        
                                        <asp:Label ID="lbl_wg_name_th" Visible="true" runat="server" Text='<%# Eval("wg_name_th") %>' />
                                    </p>

                                    <p>
                                        <b>กลุมพนักงาน:</b>
                                        
                                        <asp:Label ID="lbl_empgroup_name_th" Visible="true" runat="server" Text='<%# Eval("pos_name") %>' />
                                    </p>
                                    
                                    
                                    <!-- <p>
                                        <b>องค์กร:</b>
                                        <asp:Label ID="lbl_org_idx" Visible="false" runat="server" Text='<%# Eval("org_idx") %>' />
                                        <asp:Label ID="lbl_org_name_th" Visible="true" runat="server" Text='<%# Eval("org_name_th") %>' />
                                    </p> -->
                                    
        
                                </ItemTemplate>

                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="คะแนน KPIs" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="right" ItemStyle-Font-Size="Small">
                                <ItemTemplate>

                                    <p>
                                        <asp:Label ID="lbl_sum_kpis_value" Visible="true" runat="server" Text='<%# getConvertString((string)Eval("total_sum_kpis_value")) %>'  />
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="คะแนน TKN Spirit" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="right" >
                                <ItemTemplate>

                                    <p>
                                        <asp:Label ID="lbl_total_corevalue" Visible="true" runat="server" Text='<%# getConvertString((string)Eval("total_corevalue")) %>'  />
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="คะแนน Competency" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="right" >
                                <ItemTemplate>

                                    <p>
                                        <asp:Label ID="lbl_total_competencies" Visible="true" runat="server" Text='<%# getConvertString((string)Eval("total_competencies")) %>'  />
                                    </p>


                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="คะแนน Time Attendance" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="right" >
                                <ItemTemplate>

                                    <p>
                                        <asp:Label ID="lbl_total_score" Visible="true" runat="server" Text='<%# getConvertString((string)Eval("total_score")) %>'  />
                                    </p>
                                    
                                

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="คะแนนรวม(เต็ม100)" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="right" >
                                <ItemTemplate>

                                    <p>
                                        <asp:Label ID="lbl_sum_total" runat="server" />
                                    </p>
                                    
                                

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="เลือก" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">

                                <HeaderTemplate>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <asp:UpdatePanel ID="_PanelApproveAll1" runat="server">
                                                <ContentTemplate>
                                                    <small>
                                                        <asp:CheckBox ID="chkallApprove1" CssClass="radio-list-inline-emps" RepeatDirection="Vertical" runat="server" Text=" เลือก" Font-Size="Small" OnCheckedChanged="onSelectedCheckChanged" AutoPostBack="true">

                                                        </asp:CheckBox>

                                                    </small>

                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>

                                </HeaderTemplate>

                                <ItemTemplate>

                                    <asp:CheckBox ID="chk_approve1" runat="server" OnCheckedChanged="onSelectedCheckChanged" AutoPostBack="true"></asp:CheckBox>
                                    

                                </ItemTemplate>

                            </asp:TemplateField>



                        </Columns>
                    </asp:GridView>
                </div>

            </div>
        </asp:View>
        <!--View Approve 1-->

        <!--View Approve 2-->
        <asp:View ID="docApprove2" runat="server">
            <div class="col-md-12">

                <div id ="_DivApprove2" runat="server">
                    <asp:Image ID="imghead_approve2" runat="server" class="img-responsive img-fluid" ImageUrl="~/images/pms-formresult/performance_system.png" Style="height: 100%; width: 100%;" />

                </div>
                <br>
                <div class="clearfix"></div>

                <asp:FormView ID="fvSearchApprove2" runat="server" Width="100%" DefaultMode="Insert">
                    <InsertItemTemplate>

                        <div class="panel panel-danger">
                            <div class="panel-heading">
                                <h3 class="panel-title">ค้นหาข้อมูล</h3>
                            </div>

                            <div class="panel-body">
                                
                                <div class="col-md-10 col-md-offset-1">
                                    <div class="form-horizontal">
                                        
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">องค์กร</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlOrganization" runat="server"
                                                    Cssclass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChangedApprove2"
                                                    AutoPostBack="true"></asp:DropDownList>
                                            </div>
                                            <label class="col-md-2 control-label">กลุ่มงาน</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlWorkGroup" runat="server"
                                                    Cssclass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChangedApprove2"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">สายงาน</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlLineWork" runat="server"
                                                    Cssclass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChangedApprove2"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                            <label class="col-md-2 control-label">ฝ่าย</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlDepartment" runat="server"
                                                    Cssclass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChangedApprove2"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">แผนก</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlSection" runat="server" Cssclass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChangedApprove2"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                            <label class="col-md-2 control-label">ตำแหน่ง</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlPosition" runat="server"
                                                    Cssclass="form-control">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2 control-label">รหัสพนักงาน</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="txt_empcode" runat="server" TextMode="MultiLine" Rows="2" Style="overflow: auto" placeholder="กรอกรหัสพนักงาน Ex.(58000106,58000107)..." CssClass="form-control" />
                                            </div>
                                            <label class="col-md-2 control-label"></label>
                                            <div class="col-md-4">
                                               
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2"></label>
                                            <div class="col-md-4">
                                                <asp:LinkButton ID="lbSearchData2" runat="server"
                                                    CssClass="btn btn-md btn-primary" OnCommand="btnCommand"
                                                    CommandName="cmdSearchWaitApprove2"><i class="fas fa-search"></i>&nbsp;ค้นหา
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="lbResetData2" runat="server"
                                                    CssClass="btn btn-md btn-default" OnCommand="btnCommand"
                                                    CommandName="cmdResetWaitApprove2"><i class="fas fa-redo"></i>&nbsp;เคลียร์ค่า
                                                </asp:LinkButton>
                                            </div>
                                            <label class="col-md-2"></label>
                                            <label class="col-md-4"></label>
                                        </div>
                                    </div>
                                </div>
                                <label class="col-md-1"></label>

                            </div>

                        </div>

                    </InsertItemTemplate>
                </asp:FormView>

                <div id="div_Approve2" runat="server">
                    <!-- <label>&nbsp;</label>
                    <div class="clearfix"></div> -->
                    <div class="form-group pull-right">

                     
                        <asp:Repeater ID="rptViewBindbtnNode5" OnItemDataBound="rptOnRowDataBound" runat="server">
                            <ItemTemplate>
                                <asp:Label ID="lbcheck_colerNode5_view" runat="server" Visible="false"
                                    Text='<%# Eval("decision_name") %>'></asp:Label>
                                <asp:LinkButton ID="btn_ViewNode5" CssClass="btn btn-primary" runat="server" ValidationGroup="SaveApprove2"
                                    data-original-title='<%# Eval("decision_name") %>' data-toggle="tooltip"
                                    CommandArgument='<%# Eval("decision_idx")%>' OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')"
                                    Text='<%# Eval("decision_name") %>' CommandName="cmdSaveApprove2"></asp:LinkButton>


                            </ItemTemplate>
                        </asp:Repeater>

                    </div>

                </div>
                <div id="GvApprove2_scroll" style="overflow-x: auto; width: 100%" runat="server">

                    <asp:GridView ID="GvApprove2" runat="server"
                        AutoGenerateColumns="False"
                        CssClass="table table-striped table-bordered table-hover table-responsive"
                        OnPageIndexChanging="gvPageIndexChanging"
                        OnRowDataBound="gvRowDataBound"
                        ShowHeaderWhenEmpty="false"
                        AllowPaging="false"
                        PageSize="10"
                        BorderStyle="None"
                        DataKeyNames="u0_docidx"
                        CellSpacing="2">
                        <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                        <RowStyle Font-Size="Small" />
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">Data Cannot Be Found</div>
                        </EmptyDataTemplate>

                        <Columns>

                            <asp:TemplateField HeaderText="ข้อมูลผู้ประเมิน" ItemStyle-Width="30%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="left" ItemStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <asp:Label ID="lblu0_docidx" runat="server" Visible="false" Text='<%# Eval("u0_docidx") %>'></asp:Label>
                                    <asp:Label ID="lblunidx" runat="server" Visible="false" Text='<%# Eval("unidx") %>'></asp:Label>
                                    <asp:Label ID="lblacidx" runat="server" Visible="false" Text='<%# Eval("acidx") %>'></asp:Label>
                                    <asp:Label ID="lblstaidx" runat="server" Visible="false" Text='<%# Eval("staidx") %>'></asp:Label>
                                    <asp:Label ID="lblsolid" runat="server" Visible="false" Text='<%# Eval("solid") %>'></asp:Label>
                                    <asp:Label ID="lbldotted" runat="server" Visible="false" Text='<%# Eval("dotted") %>'></asp:Label>
                                    
                                    
                                    <p>
                                        <b>รหัสพนักงาน:</b>
                                        <asp:Label ID="lbl_emp_code" Visible="true" runat="server" Text='<%# Eval("emp_code") %>' />
                                    </p>

                                    <p>
                                        <b>ชื่อ-สกุล:</b>
                                        <asp:Label ID="lbl_cemp_idx" Visible="false" runat="server" Text='<%# Eval("cemp_idx") %>' />
                                        <asp:Label ID="lbl_emp_name_th" Visible="true" runat="server" Text='<%# Eval("emp_name_th") %>' />
                                    </p>

                                    <p>
                                        <b>ตำแหน่ง:</b>
                                        <asp:Label ID="lbl_rpos_idx" Visible="false" runat="server" Text='<%# Eval("rpos_idx") %>' />
                                        <asp:Label ID="lbl_pos_name_th" Visible="true" runat="server" Text='<%# Eval("pos_name_th") %>' />
                                    </p>

                                    <p>
                                        <b>แผนก:</b>
                                        <asp:Label ID="lbl_rsec_idx" Visible="false" runat="server" Text='<%# Eval("rsec_idx") %>' />
                                        <asp:Label ID="lbl_sec_name_th" Visible="true" runat="server" Text='<%# Eval("sec_name_th") %>' />
                                    </p>

                                    <p>
                                        <b>ฝ่าย:</b>
                                        <asp:Label ID="lbl_rdept_idx" Visible="false" runat="server" Text='<%# Eval("rdept_idx") %>' />
                                        <asp:Label ID="lbl_dept_name_th" Visible="true" runat="server" Text='<%# Eval("dept_name_th") %>' />
                                    </p>

                                    <p>
                                        <b>สายงาน:</b>
                                        
                                        <asp:Label ID="lbl_lw_name_th" Visible="true" runat="server" Text='<%# Eval("lw_name_th") %>' />
                                    </p>

                                    <p>
                                        <b>กลุ่มงาน:</b>
                                        
                                        <asp:Label ID="lbl_wg_name_th" Visible="true" runat="server" Text='<%# Eval("wg_name_th") %>' />
                                    </p>

                                    <p>
                                        <b>กลุมพนักงาน:</b>
                                        
                                        <asp:Label ID="lbl_empgroup_name_th" Visible="true" runat="server" Text='<%# Eval("pos_name") %>' />
                                    </p>
                                    
                                    
                                    <!-- <p>
                                        <b>องค์กร:</b>
                                        <asp:Label ID="lbl_org_idx" Visible="false" runat="server" Text='<%# Eval("org_idx") %>' />
                                        <asp:Label ID="lbl_org_name_th" Visible="true" runat="server" Text='<%# Eval("org_name_th") %>' />
                                    </p> -->
                                    
        
                                </ItemTemplate>

                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="คะแนน KPIs" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="right" ItemStyle-Font-Size="Small">
                                <ItemTemplate>

                                    <p>
                                        <asp:Label ID="lbl_sum_kpis_value" Visible="true" runat="server" Text='<%# getConvertString((string)Eval("total_sum_kpis_value")) %>'  />
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="คะแนน TKN Spirit" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="right" >
                                <ItemTemplate>

                                    <p>
                                        <asp:Label ID="lbl_total_corevalue" Visible="true" runat="server" Text='<%# getConvertString((string)Eval("total_corevalue")) %>'  />
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="คะแนน Competency" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="right" >
                                <ItemTemplate>

                                    <p>
                                        <asp:Label ID="lbl_total_competencies" Visible="true" runat="server" Text='<%# getConvertString((string)Eval("total_competencies")) %>'  />
                                    </p>


                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="คะแนน Time Attendance" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="right" >
                                <ItemTemplate>

                                    <p>
                                        <asp:Label ID="lbl_total_score" Visible="true" runat="server" Text='<%# getConvertString((string)Eval("total_score")) %>'  />
                                    </p>
                                    
                                

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="คะแนนรวม(เต็ม100)" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="right" >
                                <ItemTemplate>

                                    <p>
                                        <asp:Label ID="lbl_sum_total" runat="server" />
                                    </p>
                                    
                                

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="เลือก" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">

                                <HeaderTemplate>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <asp:UpdatePanel ID="_PanelApproveAll2" runat="server">
                                                <ContentTemplate>
                                                    <small>
                                                        <asp:CheckBox ID="chkallApprove2" CssClass="radio-list-inline-emps" RepeatDirection="Vertical" runat="server" Text=" เลือก" Font-Size="Small" OnCheckedChanged="onSelectedCheckChanged" AutoPostBack="true">

                                                        </asp:CheckBox>

                                                    </small>

                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>

                                </HeaderTemplate>

                                <ItemTemplate>

                                    <asp:CheckBox ID="chk_approve2" runat="server" OnCheckedChanged="onSelectedCheckChanged" AutoPostBack="true"></asp:CheckBox>
                                    

                                </ItemTemplate>

                            </asp:TemplateField>



                        </Columns>
                    </asp:GridView>
                </div>


            </div>
        </asp:View>
        <!--View Approve 2-->

        <!--View Approve 3-->
        <asp:View ID="docApprove3" runat="server">
            <div class="col-md-12">

                <div id ="_DivApprove3" runat="server">
                    <asp:Image ID="imghead_approve3" runat="server" class="img-responsive img-fluid" ImageUrl="~/images/pms-formresult/performance_system.png" Style="height: 100%; width: 100%;" />

                </div>
                <br>
                <div class="clearfix"></div>

                <asp:FormView ID="fvSearchApprove3" runat="server" Width="100%" DefaultMode="Insert">
                    <InsertItemTemplate>

                        <div class="panel panel-danger">
                            <div class="panel-heading">
                                <h3 class="panel-title">ค้นหาข้อมูล</h3>
                            </div>

                            <div class="panel-body">
                                
                                <div class="col-md-10 col-md-offset-1">
                                    <div class="form-horizontal">
                                        
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">องค์กร</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlOrganization" runat="server"
                                                    Cssclass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChangedApprove3"
                                                    AutoPostBack="true"></asp:DropDownList>
                                            </div>
                                            <label class="col-md-2 control-label">กลุ่มงาน</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlWorkGroup" runat="server"
                                                    Cssclass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChangedApprove3"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">สายงาน</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlLineWork" runat="server"
                                                    Cssclass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChangedApprove3"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                            <label class="col-md-2 control-label">ฝ่าย</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlDepartment" runat="server"
                                                    Cssclass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChangedApprove3"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">แผนก</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlSection" runat="server" Cssclass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChangedApprove3"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                            <label class="col-md-2 control-label">ตำแหน่ง</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlPosition" runat="server"
                                                    Cssclass="form-control">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2 control-label">รหัสพนักงาน</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="txt_empcode" runat="server" TextMode="MultiLine" Rows="2" Style="overflow: auto" placeholder="กรอกรหัสพนักงาน Ex.(58000106,58000107)..." CssClass="form-control" />
                                            </div>
                                            <label class="col-md-2 control-label"></label>
                                            <div class="col-md-4">
                                               
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2"></label>
                                            <div class="col-md-4">
                                                <asp:LinkButton ID="lbSearchData3" runat="server"
                                                    CssClass="btn btn-md btn-primary" OnCommand="btnCommand"
                                                    CommandName="cmdSearchWaitApprove3"><i class="fas fa-search"></i>&nbsp;ค้นหา
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="lbResetData3" runat="server"
                                                    CssClass="btn btn-md btn-default" OnCommand="btnCommand"
                                                    CommandName="cmdResetWaitApprove3"><i class="fas fa-redo"></i>&nbsp;เคลียร์ค่า
                                                </asp:LinkButton>
                                            </div>
                                            <label class="col-md-2"></label>
                                            <label class="col-md-4"></label>
                                        </div>
                                    </div>
                                </div>
                                <label class="col-md-1"></label>

                            </div>

                        </div>

                    </InsertItemTemplate>
                </asp:FormView>



                <div id="div_Approve3" runat="server">
                    <!-- <label>&nbsp;</label>
                    <div class="clearfix"></div> -->
                    <div class="form-group pull-right">

                     
                        <asp:Repeater ID="rptViewBindbtnNode6" OnItemDataBound="rptOnRowDataBound" runat="server">
                            <ItemTemplate>
                                <asp:Label ID="lbcheck_colerNode6_view" runat="server" Visible="false"
                                    Text='<%# Eval("decision_name") %>'></asp:Label>
                                <asp:LinkButton ID="btn_ViewNode6" CssClass="btn btn-primary" runat="server" ValidationGroup="SaveApprove3"
                                    data-original-title='<%# Eval("decision_name") %>' data-toggle="tooltip"
                                    CommandArgument='<%# Eval("decision_idx")%>' OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')"
                                    Text='<%# Eval("decision_name") %>' CommandName="cmdSaveApprove3"></asp:LinkButton>


                            </ItemTemplate>
                        </asp:Repeater>

                    </div>

                </div>

                <div id="GvApprove3_scroll" style="overflow-x: auto; width: 100%" runat="server">
                    <asp:GridView ID="GvApprove3" runat="server"
                        AutoGenerateColumns="False"
                        CssClass="table table-striped table-bordered table-hover table-responsive"
                        OnPageIndexChanging="gvPageIndexChanging"
                        OnRowDataBound="gvRowDataBound"
                        ShowHeaderWhenEmpty="false"
                        AllowPaging="false"
                        PageSize="10"
                        BorderStyle="None"
                        DataKeyNames="u0_docidx"
                        CellSpacing="2">
                        <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                        <RowStyle Font-Size="Small" />
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">Data Cannot Be Found</div>
                        </EmptyDataTemplate>

                        <Columns>

                            <asp:TemplateField HeaderText="ข้อมูลผู้ประเมิน" ItemStyle-Width="30%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="left" ItemStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <asp:Label ID="lblu0_docidx" runat="server" Visible="false" Text='<%# Eval("u0_docidx") %>'></asp:Label>
                                    <asp:Label ID="lblunidx" runat="server" Visible="false" Text='<%# Eval("unidx") %>'></asp:Label>
                                    <asp:Label ID="lblacidx" runat="server" Visible="false" Text='<%# Eval("acidx") %>'></asp:Label>
                                    <asp:Label ID="lblstaidx" runat="server" Visible="false" Text='<%# Eval("staidx") %>'></asp:Label>
                                    <asp:Label ID="lblsolid" runat="server" Visible="false" Text='<%# Eval("solid") %>'></asp:Label>
                                    <asp:Label ID="lbldotted" runat="server" Visible="false" Text='<%# Eval("dotted") %>'></asp:Label>
                                    
                                    
                                    <p>
                                        <b>รหัสพนักงาน:</b>
                                        <asp:Label ID="lbl_emp_code" Visible="true" runat="server" Text='<%# Eval("emp_code") %>' />
                                    </p>

                                    <p>
                                        <b>ชื่อ-สกุล:</b>
                                        <asp:Label ID="lbl_cemp_idx" Visible="false" runat="server" Text='<%# Eval("cemp_idx") %>' />
                                        <asp:Label ID="lbl_emp_name_th" Visible="true" runat="server" Text='<%# Eval("emp_name_th") %>' />
                                    </p>

                                    <p>
                                        <b>ตำแหน่ง:</b>
                                        <asp:Label ID="lbl_rpos_idx" Visible="false" runat="server" Text='<%# Eval("rpos_idx") %>' />
                                        <asp:Label ID="lbl_pos_name_th" Visible="true" runat="server" Text='<%# Eval("pos_name_th") %>' />
                                    </p>

                                    <p>
                                        <b>แผนก:</b>
                                        <asp:Label ID="lbl_rsec_idx" Visible="false" runat="server" Text='<%# Eval("rsec_idx") %>' />
                                        <asp:Label ID="lbl_sec_name_th" Visible="true" runat="server" Text='<%# Eval("sec_name_th") %>' />
                                    </p>

                                    <p>
                                        <b>ฝ่าย:</b>
                                        <asp:Label ID="lbl_rdept_idx" Visible="false" runat="server" Text='<%# Eval("rdept_idx") %>' />
                                        <asp:Label ID="lbl_dept_name_th" Visible="true" runat="server" Text='<%# Eval("dept_name_th") %>' />
                                    </p>

                                    <p>
                                        <b>สายงาน:</b>
                                        
                                        <asp:Label ID="lbl_lw_name_th" Visible="true" runat="server" Text='<%# Eval("lw_name_th") %>' />
                                    </p>

                                    <p>
                                        <b>กลุ่มงาน:</b>
                                        
                                        <asp:Label ID="lbl_wg_name_th" Visible="true" runat="server" Text='<%# Eval("wg_name_th") %>' />
                                    </p>

                                    <p>
                                        <b>กลุมพนักงาน:</b>
                                        
                                        <asp:Label ID="lbl_empgroup_name_th" Visible="true" runat="server" Text='<%# Eval("pos_name") %>' />
                                    </p>
                                    
                                    
                                    <!-- <p>
                                        <b>องค์กร:</b>
                                        <asp:Label ID="lbl_org_idx" Visible="false" runat="server" Text='<%# Eval("org_idx") %>' />
                                        <asp:Label ID="lbl_org_name_th" Visible="true" runat="server" Text='<%# Eval("org_name_th") %>' />
                                    </p> -->
                                    
        
                                </ItemTemplate>

                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="คะแนน KPIs" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="right" ItemStyle-Font-Size="Small">
                                <ItemTemplate>

                                    <p>
                                        <asp:Label ID="lbl_sum_kpis_value" Visible="true" runat="server" Text='<%# getConvertString((string)Eval("total_sum_kpis_value")) %>'  />
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="คะแนน TKN Spirit" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="right" >
                                <ItemTemplate>

                                    <p>
                                        <asp:Label ID="lbl_total_corevalue" Visible="true" runat="server" Text='<%# getConvertString((string)Eval("total_corevalue")) %>'  />
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="คะแนน Competency" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="right" >
                                <ItemTemplate>

                                    <p>
                                        <asp:Label ID="lbl_total_competencies" Visible="true" runat="server" Text='<%# getConvertString((string)Eval("total_competencies")) %>'  />
                                    </p>


                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="คะแนน Time Attendance" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="right" >
                                <ItemTemplate>

                                    <p>
                                        <asp:Label ID="lbl_total_score" Visible="true" runat="server" Text='<%# getConvertString((string)Eval("total_score")) %>'  />
                                    </p>
                                    
                                

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="คะแนนรวม(เต็ม100)" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="right" >
                                <ItemTemplate>

                                    <p>
                                        <asp:Label ID="lbl_sum_total" runat="server" />
                                    </p>
                                    
                                

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="เลือก" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">

                                <HeaderTemplate>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <asp:UpdatePanel ID="_PanelApproveAll3" runat="server">
                                                <ContentTemplate>
                                                    <small>
                                                        <asp:CheckBox ID="chkallApprove3" CssClass="radio-list-inline-emps" RepeatDirection="Vertical" runat="server" Text=" เลือก" Font-Size="Small" OnCheckedChanged="onSelectedCheckChanged" AutoPostBack="true">

                                                        </asp:CheckBox>

                                                    </small>

                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>

                                </HeaderTemplate>

                                <ItemTemplate>

                                    <asp:CheckBox ID="chk_approve3" runat="server" OnCheckedChanged="onSelectedCheckChanged" AutoPostBack="true"></asp:CheckBox>
                                    

                                </ItemTemplate>

                            </asp:TemplateField>



                        </Columns>
                    </asp:GridView>
                </div>

               

            </div>
        </asp:View>
        <!--View Approve 3-->

        <!--View CheckPerformance-->
        <asp:View ID="docCheckPerformance" runat="server">
            <div class="col-md-12">

                <div id ="_DivCheckPerformance" runat="server">
                    <asp:Image ID="imghead_checkperformance" runat="server" class="img-responsive img-fluid" ImageUrl="~/images/pms-formresult/performance_system.png" Style="height: 100%; width: 100%;" />

                </div>
                <br>

                <asp:FormView ID="fvSearchCheckPerformance" runat="server" Width="100%" DefaultMode="Insert">
                    <InsertItemTemplate>

                        <div class="panel panel-danger">
                            <div class="panel-heading">
                                <h3 class="panel-title">ค้นหาข้อมูล</h3>
                            </div>

                            <div class="panel-body">
                                
                                <div class="col-md-10 col-md-offset-1">
                                    <div class="form-horizontal">
                                        
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">รหัสพนักงาน</label>
                                            <div class="col-md-10">
                                                <asp:TextBox ID="txt_empcode" runat="server" TextMode="MultiLine" Rows="3" Style="overflow: auto" placeholder="กรอกรหัสพนักงาน Ex.(58000106,58000107)..." CssClass="form-control" />
                                            </div>
                                            <!-- <label class="col-md-2 control-label"></label>
                                            <div class="col-md-4"> -->
                                               
                                           
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2"></label>
                                            <div class="col-md-4">
                                                <asp:LinkButton ID="lbSearchData" runat="server"
                                                    CssClass="btn btn-md btn-primary" OnCommand="btnCommand"
                                                    CommandName="cmdSearchCheckPerformance"><i class="fas fa-search"></i>&nbsp;ค้นหา
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="lbResetData" runat="server"
                                                    CssClass="btn btn-md btn-default" OnCommand="btnCommand"
                                                    CommandName="cmdResetCheckPerformance"><i class="fas fa-redo"></i>&nbsp;เคลียร์ค่า
                                                </asp:LinkButton>
                                            </div>
                                            <label class="col-md-2"></label>
                                            <label class="col-md-4"></label>
                                        </div>
                                    </div>
                                </div>
                                <label class="col-md-1"></label>

                            </div>

                        </div>

                    </InsertItemTemplate>
                </asp:FormView>
                
                <asp:GridView ID="GvCheckPerformance" runat="server"
                    AutoGenerateColumns="False"
                    CssClass="table table-striped table-bordered table-hover table-responsive"
                    OnPageIndexChanging="gvPageIndexChanging"
                    OnRowDataBound="gvRowDataBound"
                    ShowHeaderWhenEmpty="false"
                    AllowPaging="false"
                    PageSize="10"
                    BorderStyle="None"
                    DataKeyNames="u0_docidx"
                    CellSpacing="2">
                    <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                    <RowStyle Font-Size="Small" />
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">Data Cannot Be Found</div>
                    </EmptyDataTemplate>

                    <Columns>

                    
                        <asp:TemplateField HeaderText="ข้อมูลผู้ประเมิน" ItemStyle-Width="30%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="left" ItemStyle-Font-Size="Small">
                            <ItemTemplate>
                                <asp:Label ID="lblu0_docidx" runat="server" Visible="false" Text='<%# Eval("u0_docidx") %>'></asp:Label>
                                <asp:Label ID="lblunidx" runat="server" Visible="false" Text='<%# Eval("unidx") %>'></asp:Label>
                                <asp:Label ID="lblacidx" runat="server" Visible="false" Text='<%# Eval("acidx") %>'></asp:Label>
                                <asp:Label ID="lblstaidx" runat="server" Visible="false" Text='<%# Eval("staidx") %>'></asp:Label>
                                
                                <p>
                                    <b>องค์กร:</b>
                                    <asp:Label ID="lbl_org_idx" Visible="false" runat="server" Text='<%# Eval("org_idx") %>' />
                                    <asp:Label ID="lbl_org_name_th" Visible="true" runat="server" Text='<%# Eval("org_name_th") %>' />
                                </p>
                                <p>
                                    <b>ฝ่าย:</b>
                                    <asp:Label ID="lbl_rdept_idx" Visible="false" runat="server" Text='<%# Eval("rdept_idx") %>' />
                                    <asp:Label ID="lbl_dept_name_th" Visible="true" runat="server" Text='<%# Eval("dept_name_th") %>' />
                                </p>
                                <p>
                                    <b>แผนก:</b>
                                    <asp:Label ID="lbl_rsec_idx" Visible="false" runat="server" Text='<%# Eval("rsec_idx") %>' />
                                    <asp:Label ID="lbl_sec_name_th" Visible="true" runat="server" Text='<%# Eval("sec_name_th") %>' />
                                </p>
                                <p>
                                    <b>ตำแหน่ง:</b>
                                    <asp:Label ID="lbl_rpos_idx" Visible="false" runat="server" Text='<%# Eval("rpos_idx") %>' />
                                    <asp:Label ID="lbl_pos_name_th" Visible="true" runat="server" Text='<%# Eval("pos_name_th") %>' />
                                </p>
                                <p>
                                    <b>ชื่อ-สกุล:</b>
                                    <asp:Label ID="lbl_cemp_idx" Visible="false" runat="server" Text='<%# Eval("cemp_idx") %>' />
                                    <asp:Label ID="lbl_emp_name_th" Visible="true" runat="server" Text='<%# Eval("emp_name_th") %>' />
                                </p>

                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="คะแนนประเมินตนเอง" ItemStyle-Width="20%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="left" ItemStyle-Font-Size="Small">
                            <ItemTemplate>

                                <p>
                                    <b>KPIs:</b>
                                    <asp:Label ID="lbl_sum_kpis_value" Visible="true" runat="server" Text='<%# getConvertString((string)Eval("sum_kpis_value")) %>'  />
                                    
                                </p>

                                <p>
                                    <b>Core Value:</b>
                                    <asp:Label ID="lbl_sum_mine_core_value" Visible="true" runat="server" Text='<%# getConvertString((string)Eval("sum_mine_core_value")) %>' />
                                </p>
                                <p>
                                    <b>Competencies:</b>
                                    <asp:Label ID="lbl_sum_mine_competency" Visible="true" runat="server" Text='<%# getConvertString((string)Eval("sum_mine_competency")) %>' />
                                </p>

                            

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="คะแนนผู้มีสิทธิ์ประเมิน" ItemStyle-Width="25%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="left" >
                            <ItemTemplate>

                                <p>
                                    <b>Dotted KPIs:</b>
                                    <asp:Label ID="lbl_sum_dotted_kpis_value" Visible="true" runat="server" Text='<%# getConvertString((string)Eval("sum_kpis_value")) %>'  />
                                    
                                </p>
                                
                                <p>
                                    <b>Dotted Core Value:</b>
                                    <asp:Label ID="lbl_sum_dotted_core_value" Visible="true" runat="server" Text='<%# getConvertString((string)Eval("sum_dotted_core_value")) %>'  />
                                </p>
                                <p>
                                    <b>Dotted Competencies:</b>
                                    <asp:Label ID="lbl_sum_dotted_competency" Visible="true" runat="server" Text='<%# getConvertString((string)Eval("sum_dotted_competency")) %>'  />
                                </p>

                                <p>
                                    <b>Solid KPIs:</b>
                                    <asp:Label ID="lbl_sum_solid_kpis_value" Visible="true" runat="server" Text='<%# getConvertString((string)Eval("sum_kpis_value")) %>'  />
                                    
                                </p>
                                
                                <p>
                                    <b>Solid Core Value:</b>
                                    <asp:Label ID="lbl_sum_solid_core_value" Visible="true" runat="server" Text='<%# getConvertString((string)Eval("sum_solid_core_value")) %>'  />
                                </p>
                                <p>
                                    <b>Solid Competencies:</b>
                                    <asp:Label ID="lbl_sum_solid_competency" Visible="true" runat="server" Text='<%# getConvertString((string)Eval("sum_solid_competency")) %>'  />
                                </p>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สถานะรายการ" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center" >
                            <ItemTemplate>
                                
                                    <b><asp:Label ID="lblcurrent_status" runat="server" Text='<%# Eval("current_status") %>' /></b>
                            
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnViewDetail" CssClass="btn-sm btn-warning" target="" runat="server" CommandName="cmdViewDetail" OnCommand="btnCommand" CommandArgument='<%# Eval("u0_docidx")+ ";" + Eval("unidx") + ";" + Eval("acidx") + ";" + Eval("staidx") + ";" + Eval("cemp_idx") + ";" + Eval("form_name") + ";" + Eval("sum_solid_core_value") + ";" + Eval("sum_solid_competency") + ";" + Eval("u0_typeidx")+ ";" + Eval("solid")+ ";" + Eval("dotted")+ ";" + Eval("org_idx")%>' data-toggle="tooltip" title="ดูข้อมูล"><i class="fa fa-book"></i></asp:LinkButton>

                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>

            </div>
        </asp:View>
         <!--View CheckPerformance-->

    </asp:MultiView>
    <!--multiview-->

</asp:Content>