﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.IO;
using System.Data;
using System.Data.SqlClient;

using System.Text;
using System.Web.UI.HtmlControls;




public partial class websystem_ITServices_it_news : System.Web.UI.Page
{

    #region initial function/data

    function_tool _funcTool = new function_tool();
    data_it_news _data_it_news = new data_it_news();


    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetITNews = _serviceUrl + ConfigurationManager.AppSettings["urlGetITNews"];
    static string _urlGetShowITNews = _serviceUrl + ConfigurationManager.AppSettings["urlGetShowITNews"];
    static string _urlGetSearchITNews = _serviceUrl + ConfigurationManager.AppSettings["urlGetSearchITNews"];
    static string _urlGetnewsdefault = _serviceUrl + ConfigurationManager.AppSettings["urlGetnewsdefault"];
    static string _urlGetShowITNewsMenu = _serviceUrl + ConfigurationManager.AppSettings["urlGetShowITNewsMenu"];
    
    string _localJson = "";
    int _tempInt = 0;



    #endregion


    //public String str, Indicators;
    //public int counter = 0;

    protected void Page_Load(object sender, EventArgs e)
    {


        //  classmyCarousel();

        if (!IsPostBack)
        {
            SELECT_IT_NEWS_GENARAL();
            // newsdefails();
            SELECTListNews();
            SELECT_IT_NEWSPR();
            SELECTNews();
            lbtitleSearchNews.Text = "";
            // classmyCarousel();
        }
 

    }

    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "gvShowNews":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {



                    HtmlImage ImagShowQRCode = (HtmlImage)e.Row.Cells[0].FindControl("ImagShowNews");
                   // HtmlImage imgListRoom = (HtmlImage)e.Item.FindControl("imgListRoom");

                    //count select button in node11
                    Label lblSampleCode = (Label)e.Row.Cells[0].FindControl("lbIdxU0News");

                    string getPath = ConfigurationManager.AppSettings["pathfile_itnews"];
                    string filePath = Server.MapPath(getPath + lblSampleCode.Text);//lblListRoomIdx.Text);
                    DirectoryInfo dirListRoom = new DirectoryInfo(filePath);

                    if (Directory.Exists(Server.MapPath(getPath + lblSampleCode.Text)))
                    {
                        HttpFileCollection hfcit3 = Request.Files;
                        for (int i = 0; i < 1; i++)
                        {

                            foreach (FileInfo file in dirListRoom.GetFiles())
                            {
                                if (Directory.Exists(filePath))
                                {
                                ImagShowQRCode.Attributes.Add("src", getPath + lblSampleCode.Text +
                                            "/" + file.Name);
                                }
                                else if (!Directory.Exists(filePath))
                                {

                                ImagShowQRCode.Attributes.Add("src", getPath + "default" +
                                     "/" + "default" + ".jpg");
                                }

                            }

                        }

                    }
                    else
                    {


                        ImagShowQRCode.Attributes.Add("src", getPath + "default" +
                                    "/" + "default" + ".jpg");

                    }

                //    //Show ViewNetwork
                //    string getPathShowimages = ConfigurationManager.AppSettings["pathfile_itnews"];

                //    string fileimageShowPath = Server.MapPath(getPathShowimages + lblSampleCode.Text + ".jpg");
                //    //// Show รูป
                //    if (!File.Exists(Server.MapPath(getPathShowimages + lblSampleCode.Text + ".jpg")))
                //    {
                //        ImagShowQRCode.Visible = false;
                //    }
                //    else
                //    {
                //        ImagShowQRCode.ImageUrl = getPathShowimages + lblSampleCode.Text + ".jpg";
                //    }
                //}
               }
                    break;
        }
    }

    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView gvName = (GridView)sender;
        gvName.PageIndex = e.NewPageIndex;

        switch (gvName.ID)
        {
            case "gvShowNews":
                newsdefails();
                break;
            case "gvLab":
               // setGridData(gvLab, ViewState["vs_gvLab_Detail"]);
               // setOntop.Focus();
                break;
        }
    }

    #region SELECT_IT_NEWSPR

    //ข่าวประชาสัมพันธ์
    protected void SELECT_IT_NEWSPR()
    {

        data_it_news _dataNews = new data_it_news();
        _dataNews.u0_it_news_list = new u0_it_news_details[1];
        u0_it_news_details select_Newspr = new u0_it_news_details();

        select_Newspr.m0_news_idx = 1;
        _dataNews.u0_it_news_list[0] = select_Newspr;

        _dataNews = callServiceITNews(_urlGetShowITNewsMenu, _dataNews);

        var _linq_newspr = from data_newspr in _dataNews.u0_it_news_list.Take(5)
                           select data_newspr;

        rplistnewsPR.DataSource = _linq_newspr.ToList();//_dataNews.u0_it_news_list;
        rplistnewsPR.DataBind();

    }

    protected void SELECTListNews()
    {

        data_it_news _dataNews = new data_it_news();
        _dataNews.u0_it_news_list = new u0_it_news_details[1];
        u0_it_news_details select_Newspr = new u0_it_news_details();

        select_Newspr.m0_news_idx = 1;
        _dataNews.u0_it_news_list[0] = select_Newspr;

        _dataNews = callServiceITNews(_urlGetShowITNews, _dataNews);

        var _linq_newspr = from data_newspr in _dataNews.u0_it_news_list.Take(4)
                           select data_newspr;

        ViewState["vsListNews"] = _linq_newspr.ToList();

        setRepeaterData(rptNewsIT, ViewState["vsListNews"]);

    }

    protected void SELECTNews()
    {

        data_it_news _dataNews = new data_it_news();
        _dataNews.u0_it_news_list = new u0_it_news_details[1];
        u0_it_news_details select_Newspr = new u0_it_news_details();

        select_Newspr.m0_news_idx = 2;
        _dataNews.u0_it_news_list[0] = select_Newspr;

        _dataNews = callServiceITNews(_urlGetShowITNews, _dataNews);

        var _linq_newspr = from data_newspr in _dataNews.u0_it_news_list.Take(8)
                           select data_newspr;

        ViewState["vsNews"] = _linq_newspr.ToList();

        setRepeaterData(rtpNews, ViewState["vsNews"]);

    }



    #endregion

    #region SELECT_IT_NEWS_GENARAL

    protected void SELECT_IT_NEWS_GENARAL()
    {

        data_it_news _dataNews = new data_it_news();
        _dataNews.u0_it_news_list = new u0_it_news_details[1];
        u0_it_news_details select_News = new u0_it_news_details();

        select_News.m0_news_idx = 2;
        _dataNews.u0_it_news_list[0] = select_News;

        _dataNews = callServiceITNews(_urlGetShowITNewsMenu, _dataNews);

        var _linq_news = from data_news in _dataNews.u0_it_news_list.Take(5)
                         select data_news;



        rplistnews.DataSource = _linq_news;//_dataNews.u0_it_news_list;
        rplistnews.DataBind();

    }

    #endregion

    #region SELECT_IT_NEWS_GENARAL

    #region select news defails

    protected void newsdefails()
    {

        data_it_news _datanewsdefault = new data_it_news();
        _datanewsdefault.u0_it_news_list = new u0_it_news_details[1];
        u0_it_news_details show_newsdefault = new u0_it_news_details();
        _datanewsdefault.u0_it_news_list[0] = show_newsdefault;

        _datanewsdefault = callServiceITNews(_urlGetnewsdefault, _datanewsdefault);

        if (_datanewsdefault.u0_it_news_list != null)
        {
            //ViewState["idnews"] = _datanewsdefault.u0_it_news_list[0].u0_news_idx.ToString();
            //rpListRoomIndex.DataSource = _datanewsdefault.u0_it_news_list;
            //rpListRoomIndex.DataBind();

            ViewState["idnews"] = _datanewsdefault.u0_it_news_list[0].u0_news_idx.ToString();
        //    gvShowNews.DataSource = _datanewsdefault.u0_it_news_list;
        //    gvShowNews.DataBind();


        }
        else
        {


        }




    }



    #endregion



    protected void rpDataBound(object sender, RepeaterItemEventArgs e)
    {
        var rpName = (Repeater)sender;
        switch (rpName.ID)
        {
            case "rptNewsIT":
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    data_it_news _dataNews = new data_it_news();
                    _dataNews.u0_it_news_list = new u0_it_news_details[1];
                    u0_it_news_details select_Newspr = new u0_it_news_details();

                    select_Newspr.m0_news_idx = 1;
                    _dataNews.u0_it_news_list[0] = select_Newspr;

                    _dataNews = callServiceITNews(_urlGetShowITNews, _dataNews);

                    //var _linq_newspr = from data_newspr in _dataNews.u0_it_news_list.Take(4)
                    //                   select data_newspr;
                    for (int j = 0; j < _dataNews.u0_it_news_list.Count(); j++)
                    {
                        HtmlImage imgListRoom = (HtmlImage)e.Item.FindControl("imgListRoom");
                        string getPath = ConfigurationManager.AppSettings["pathfile_itnews"];
                        string filePath = Server.MapPath(getPath + _dataNews.u0_it_news_list[j].u0_news_idx.ToString());//lblListRoomIdx.Text);
                        DirectoryInfo dirListRoom = new DirectoryInfo(filePath);

                        if (Directory.Exists(Server.MapPath(getPath + _dataNews.u0_it_news_list[j].u0_news_idx.ToString())))
                        {
                            HttpFileCollection hfcit3 = Request.Files;
                            for (int i = 0; i < _dataNews.u0_it_news_list.Count(); i++)
                            {

                                foreach (FileInfo file in dirListRoom.GetFiles())
                                {
                                    if (Directory.Exists(filePath))
                                    {
                                        imgListRoom.Attributes.Add("src", getPath + _dataNews.u0_it_news_list[i].u0_news_idx.ToString() +
                                               //imgListRoom.Attributes.Add("src", getPath + ViewState["idnews"] +
                                               "/" + file.Name);
                                    }
                                    else if (!Directory.Exists(filePath))
                                    {
                                        imgListRoom.Attributes.Add("src", getPath + "default" +
                                         "/" + "default" + ".jpg");

                                    }

                                }

                            }

                        }

                        else
                        {

                            imgListRoom.Attributes.Add("src", getPath + "default" +
                                        "/" + "default" + ".jpg");

                        }
                       // Label lblListRoomIdx = (Label)e.Item.FindControl("lblListRoomIdx");




                    }

                    break;

                }

                break;

            case "rptNewsShowDetailsAll":
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    data_it_news _dataNews = new data_it_news();
                    _dataNews.u0_it_news_list = new u0_it_news_details[1];
                    u0_it_news_details select_Newspr = new u0_it_news_details();

                    select_Newspr.m0_news_idx = 1;
                    _dataNews.u0_it_news_list[0] = select_Newspr;

                    _dataNews = callServiceITNews(_urlGetShowITNews, _dataNews);

                    //var _linq_newspr = from data_newspr in _dataNews.u0_it_news_list.Take(4)
                    //                   select data_newspr;
                    for (int j = 0; j < _dataNews.u0_it_news_list.Count(); j++)
                    {
                        HtmlImage imgListRoom = (HtmlImage)e.Item.FindControl("imgListRoom");
                        string getPath = ConfigurationManager.AppSettings["pathfile_itnews"];
                        string filePath = Server.MapPath(getPath + ViewState["idnews"]);//lblListRoomIdx.Text);
                        DirectoryInfo dirListRoom = new DirectoryInfo(filePath);

                        if (Directory.Exists(Server.MapPath(getPath + ViewState["idnews"])))
                        {
                            HttpFileCollection hfcit3 = Request.Files;
                            for (int i = 0; i < _dataNews.u0_it_news_list.Count(); i++)
                            {

                                foreach (FileInfo file in dirListRoom.GetFiles())
                                {
                                    if (Directory.Exists(filePath))
                                    {
                                        //imgListRoom.Attributes.Add("src", getPath + _dataNews.u0_it_news_list[i].u0_news_idx.ToString() +
                                               imgListRoom.Attributes.Add("src", getPath + ViewState["idnews"] +
                                               "/" + file.Name);
                                    }
                                    else if (!Directory.Exists(filePath))
                                    {
                                        imgListRoom.Attributes.Add("src", getPath + "default" +
                                         "/" + "default" + ".jpg");

                                    }

                                }

                            }

                        }

                        else
                        {

                            imgListRoom.Attributes.Add("src", getPath + "default" +
                                        "/" + "default" + ".jpg");

                        }
                        // Label lblListRoomIdx = (Label)e.Item.FindControl("lblListRoomIdx");




                    }

                    break;

                }
                break;

            case "rtpNews":
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    data_it_news _dataNews = new data_it_news();
                    _dataNews.u0_it_news_list = new u0_it_news_details[1];
                    u0_it_news_details select_Newspr = new u0_it_news_details();

                    select_Newspr.m0_news_idx = 2;
                    _dataNews.u0_it_news_list[0] = select_Newspr;

                    _dataNews = callServiceITNews(_urlGetShowITNews, _dataNews);

                    //var _linq_newspr = from data_newspr in _dataNews.u0_it_news_list.Take(4)
                    //                   select data_newspr;
                    for (int j = 0; j < _dataNews.u0_it_news_list.Count(); j++)
                    {
                        HtmlImage imgListRoom = (HtmlImage)e.Item.FindControl("imgListRoom");
                        string getPath = ConfigurationManager.AppSettings["pathfile_itnews"];
                        string filePath = Server.MapPath(getPath + _dataNews.u0_it_news_list[j].u0_news_idx.ToString());//lblListRoomIdx.Text);
                        DirectoryInfo dirListRoom = new DirectoryInfo(filePath);

                        if (Directory.Exists(Server.MapPath(getPath + _dataNews.u0_it_news_list[j].u0_news_idx.ToString())))
                        {
                            HttpFileCollection hfcit3 = Request.Files;
                            for (int i = 0; i < _dataNews.u0_it_news_list.Count(); i++)
                            {

                                foreach (FileInfo file in dirListRoom.GetFiles())
                                {
                                    if (Directory.Exists(filePath))
                                    {
                                        imgListRoom.Attributes.Add("src", getPath + _dataNews.u0_it_news_list[i].u0_news_idx.ToString() +
                                               //imgListRoom.Attributes.Add("src", getPath + ViewState["idnews"] +
                                               "/" + file.Name);
                                    }
                                    else if (!Directory.Exists(filePath))
                                    {
                                        imgListRoom.Attributes.Add("src", getPath + "default" +
                                         "/" + "default" + ".jpg");

                                    }

                                }

                            }

                        }

                        else
                        {

                            imgListRoom.Attributes.Add("src", getPath + "default" +
                                        "/" + "default" + ".jpg");

                        }
                        // Label lblListRoomIdx = (Label)e.Item.FindControl("lblListRoomIdx");




                    }

                    break;

                }

                break;
        }
    }




    #region myCarousel

    //protected void classmyCarousel()
    //{

    //    data_it_news _dataNews = new data_it_news();
    //    _dataNews.u0_it_news_list = new u0_it_news_details[1];
    //    u0_it_news_details select_News_pr = new u0_it_news_details();
    //    select_News_pr.m0_news_idx = 1;
    //    _dataNews.u0_it_news_list[0] = select_News_pr;
    //    _dataNews = callServiceITNews(_urlGetShowITNews, _dataNews);

    //    //  ViewState["classmyCarousel"] = _dataNews.u0_it_news_list;

    //    //var c = _dataNews.u0_it_news_list;

    //    //var linq = from news in c
    //    //           select news.u0_news_idx;
    //    Literal indicators = (Literal)myCarousel.FindControl("indicators");
    //    Literal images = (Literal)myCarousel.FindControl("images");
    //    indicators.Text = string.Empty;
    //    images.Text = string.Empty;

    //    if (_dataNews.return_code == "1")
    //    {
    //        int i = 0;


    //        //   if (images.Text != null)
    //        // {
    //        string getPath = ConfigurationManager.AppSettings["pathfile_itnews"];
    //        string filePath = Server.MapPath(getPath + "default" + "/" + "default" + ".jpg");
    //        DirectoryInfo dirListRoom = new DirectoryInfo(filePath);


    //        foreach (var file in _dataNews.u0_it_news_list)
    //        {

    //            // HtmlImage Bgpicture = (HtmlImage)myCarousel.FindControl("Bgpicture");
    //            HtmlImage Bgpicture = (HtmlImage)myCarousel.FindControl("Bgpicture");
    //            string imagePath = ResolveUrl(getPath + filePath);

    //            //Bgpicture.Style["background-image"] = "url(uploadfiles/it_news/default/default.jpg)";
    //            //Bgpicture.Attributes.Add("body", "background-repeat: no-repeat");


    //            indicators.Text += i == 0
    //                ? "<li data-target='#myCarousel' data-slide-to='" + i + "' class='active'></li>"
    //                : "<li data-target='#myCarousel' data-slide-to='" + i + "' class=''></li>";
    //            images.Text += i == 0 ? "<div class='item active'>" : "<div class='item'>";
    //            alertnews.Text = "<div style='margin:10px;' ></div>" + "&nbsp;&nbsp; " + "<i class=" + "'glyphicon glyphicon-globe'" + "></i>" + "<b>" + " แจ้งข่าว" + "</b>";

    //            images.Text += "<br />" + "&nbsp;&nbsp; " + "<b>" + _dataNews.u0_it_news_list[i].title_it_news + "</b>";
    //            images.Text += "</div>";


    //            i++;


    //            //< a href = "http://www.google.com" target = "_parent" >< button > Click me !</ button ></ a >
    //        }

    //        //    }



    //        //  }
    //        //else
    //        //{

    //        //    int i = 0;
    //        //    Literal indicators = (Literal)myCarousel.FindControl("indicators");
    //        //    Literal images = (Literal)myCarousel.FindControl("images");

    //        //    foreach (var file in _dataNews.u0_it_news_list)
    //        //    {

    //        //        // string imagePath = _dataNews.u0_it_news_list[0].u0_news_idx;//ResolveUrl(getPath + "room-" + dataIBK.ibk_u0_reserve_action[0].u0_room_idx_ref + "/" + file.Name);
    //        //        indicators.Text += i == 0
    //        //        ? "<li data-target='#myCarousel' data-slide-to='" + i + "' class='active'></li>"
    //        //        : "<li data-target='#myCarousel' data-slide-to='" + i + "' class=''></li>";
    //        //        images.Text += i == 0 ? "<div class='item active'>" : "<div class='item'>";
    //        //        alertnews.Text = "<div style='margin:10px;' ></div>" + "&nbsp;&nbsp; " + "<i class=" + "'glyphicon glyphicon-globe'" + "></i>" + "<b>" + " แจ้งข่าว" + "</b>";

    //        //        i++;

    //        //    }

    //    }
    //}


    #endregion




    #endregion

    public static string ShortDescription(string Description)
    {
        string result = Description;
        if (result.Length > 50)
        {
            result = result.Substring(0, 50);
            result += "....";
        }
        return result;
    }


    //protected void LnkEdit_Click(object sender, EventArgs e)
    //{
    //    var btnmenuNewsPR = (LinkButton)rpListRoomIndex.FindControl("btnmenuNewsPR");
    //    // multiUserdis.SetActiveView(vwViewProfile);
    //    btnmenuNewsPR.BackColor = System.Drawing.Color.LightGray;
    //}

    #region btnCommand

    protected void btncommand(object sender, CommandEventArgs e)
    {
        string cmd_name = e.CommandName.ToString();
        string cmd_arg = e.CommandArgument.ToString();

        switch (cmd_name)
        {
            case "cmdReadMore":

                int _u0newsReadMore = int.Parse(cmd_arg);
                ViewState["idnews"] = _u0newsReadMore;
                data_it_news _dataNewsReadMore = new data_it_news();
                _dataNewsReadMore.u0_it_news_list = new u0_it_news_details[1];
                u0_it_news_details selectReadMore = new u0_it_news_details();

                selectReadMore.u0_news_idx = _u0newsReadMore;
                _dataNewsReadMore.u0_it_news_list[0] = selectReadMore;

                _dataNewsReadMore = callServiceITNews(_urlGetShowITNews, _dataNewsReadMore);

                rptNewsShowDetailsAll.DataSource = _dataNewsReadMore.u0_it_news_list;
                rptNewsShowDetailsAll.DataBind();
                ViewState["vsListNewsReadMore"] = _dataNewsReadMore.u0_it_news_list;
                setRepeaterData(rptNewsShowDetailsAll, ViewState["vsListNewsReadMore"]);
                setRepeaterData(rptNewsIT, null);
                setFocus.Focus();
                break;

            case "cmdHiddenReadMore":

                SELECTListNews();
                setRepeaterData(rptNewsShowDetailsAll, null);
                setRepeaterData(rptNewsIT, ViewState["vsListNews"]);
                setFocus.Focus();
                break;

            case "menuNews":


                int _u0news = int.Parse(cmd_arg);
                ViewState["onClickActive"] = _u0news;

                data_it_news _dataNews = new data_it_news();
                _dataNews.u0_it_news_list = new u0_it_news_details[1];
                u0_it_news_details show_News = new u0_it_news_details();
                show_News.u0_news_idx = _u0news;
                _dataNews.u0_it_news_list[0] = show_News;
                _dataNews = callServiceITNews(_urlGetShowITNews, _dataNews);
                ViewState["idnews"] = _dataNews.u0_it_news_list[0].u0_news_idx.ToString();
                //rpListRoomIndex.DataSource = _dataNews.u0_it_news_list;
                //rpListRoomIndex.DataBind();

                //if (int.Parse(ViewState["onClickActive"].ToString()) == _u0news)
                //{
                //    int loop = 0;
                //    foreach()
                //    LinkButton test = (LinkButton)rplistnews.FindControl("test");
                //    test.BackColor = System.Drawing.Color.LightGray;
                //}


                break;

            case "menuNewsPR":

                int _u0newspr = int.Parse(cmd_arg);
                data_it_news _dataNewspr = new data_it_news();
                _dataNewspr.u0_it_news_list = new u0_it_news_details[1];
                u0_it_news_details show_Newspr = new u0_it_news_details();
                show_Newspr.u0_news_idx = _u0newspr;
                _dataNewspr.u0_it_news_list[0] = show_Newspr;
                _dataNewspr = callServiceITNews(_urlGetShowITNews, _dataNewspr);
                ViewState["idnews"] = _dataNewspr.u0_it_news_list[0].u0_news_idx.ToString();
                //rpListRoomIndex.DataSource = _dataNewspr.u0_it_news_list;
                //rpListRoomIndex.DataBind();
                ViewState["classmyCarousel"] = null;

                break;

            case "menuNewswarning":

                //int _idu2 = int.Parse(cmd_arg);

                //Literal1.Text = _idu2.ToString();
                //Literal2.Text = "hhhh";

                break;

            case "btnsearch":


                data_it_news _dataitnews = new data_it_news();
                _dataitnews.u0_it_news_list = new u0_it_news_details[1];
                u0_it_news_details search = new u0_it_news_details();
                search.search = txtsearch.Text;
                _dataitnews.u0_it_news_list[0] = search;
                _dataitnews = callServiceITNews(_urlGetSearchITNews, _dataitnews);

                if (_dataitnews.u0_it_news_list != null)
                {

                    ViewState["idnews"] = _dataitnews.u0_it_news_list[0].u0_news_idx.ToString();
                    rptNewsIT.DataSource = _dataitnews.u0_it_news_list;
                    rptNewsIT.DataBind();
                    txtsearch.Text = String.Empty;
                    lbtitleSearchNews.Text = "ผลลัพธ์การค้นหา";
                  //  setRepeaterData(rtpNews, null);
                }

                else
                {

                    lbtitleSearchNews.Text = "ไม่พบผลลัพธ์การค้นหา";
                    setRepeaterData(rptNewsIT, null);
                }

              
                //   classmyCarousel();

                break;

            case "btnHiddenmenuall_newspr":
                SELECT_IT_NEWSPR();
                btn_allnewspr.Visible = true;
                btnHiddenMenuPr.Visible = false;
                setFocus.Focus();
                break;

            case "btnmenuall_newspr":

                data_it_news _dataNews_prall = new data_it_news();
                _dataNews_prall.u0_it_news_list = new u0_it_news_details[1];
                u0_it_news_details select_Newsprall = new u0_it_news_details();

                select_Newsprall.m0_news_idx = 1;
                _dataNews_prall.u0_it_news_list[0] = select_Newsprall;

                _dataNews_prall = callServiceITNews(_urlGetShowITNews, _dataNews_prall);

                rplistnewsPR.DataSource = _dataNews_prall.u0_it_news_list;
                rplistnewsPR.DataBind();
                btn_allnewspr.Visible = false;
                btnHiddenMenuPr.Visible = true;
                setBelow.Focus();

                break;

            case "btnmenuall_news":

                data_it_news _dataNews_genr = new data_it_news();
                _dataNews_genr.u0_it_news_list = new u0_it_news_details[1];
                u0_it_news_details select_News_genr = new u0_it_news_details();

                select_News_genr.m0_news_idx = 2;
                _dataNews_genr.u0_it_news_list[0] = select_News_genr;

                _dataNews_genr = callServiceITNews(_urlGetShowITNews, _dataNews_genr);

                rplistnews.DataSource = _dataNews_genr.u0_it_news_list;
                rplistnews.DataBind();
                btnmenuall_news_genr.Visible = false;
                btnmenuall_HiddenGenr.Visible = true;
                

                break;
            case "btnHiddenmenuall_newsGenr":
                SELECT_IT_NEWS_GENARAL();
                btnmenuall_news_genr.Visible = true;
                btnmenuall_HiddenGenr.Visible = false;
                setFocus.Focus();
                break;
            case "btnHome":
                SELECTListNews();
                SELECT_IT_NEWSPR();
                SELECTNews();
                lbtitleSearchNews.Text = "";
                txtsearch.Text = String.Empty;
                break;


        }


    }

    #endregion

    #region call service
    protected data_it_news callServiceITNews(string _cmdUrl, data_it_news _data_it_news)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_it_news);
        // litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);

        // convert json to object
        _data_it_news = (data_it_news)_funcTool.convertJsonToObject(typeof(data_it_news), _localJson);

        return _data_it_news;
    }

    #endregion

    protected void setRepeaterData(Repeater rptName, Object obj)
    {
        rptName.DataSource = obj;
        rptName.DataBind();
    }

    protected void myFunction(object sender, EventArgs e)
    {
        //LinkButton btn = sender as LinkButton;
        //This will give you the text of the button.

        Literal1.Text += "gml";



    }



}