﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CalendarHolidayEvent
/// </summary>
public class CalendarHolidayEvent
{
    public int id { get; set; }
    public int holiday_idx { get; set; }
    public int org_idx { get; set; }
    public string holiday_name { get; set; }
    public DateTime start { get; set; }
    public DateTime end { get; set; }
    public bool allDay { get; set; }
    public string title { get; set; }
    public string description { get; set; }

}