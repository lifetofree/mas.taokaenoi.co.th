﻿using AjaxControlToolkit;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class websystem_el_TrnCourse : System.Web.UI.Page
{
    #region Init
    function_tool _funcTool = new function_tool();
    data_elearning _data_elearning = new data_elearning();
    function_dmu _func_dmu = new function_dmu();


    service_execute serviceexcute = new service_execute();
    data_employee _dataEmployee = new data_employee();
    data_tpm_form _dtpmform = new data_tpm_form();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];

    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    static string urlGetAll = _serviceUrl + ConfigurationManager.AppSettings["urlGetAll"];

    static string _urlSelect_Location = _serviceUrl + ConfigurationManager.AppSettings["urlGetPlace"];
    static string _urlSelectMaster = _serviceUrl + ConfigurationManager.AppSettings["urlSelectMaster_FormResult"];
    //-- employee --//

    //-- Elearning Toei--//
    static string _urlGetELM0Group = _serviceUrl + ConfigurationManager.AppSettings["urlGetELM0Group"];
    static string _urlGetELM0branch = _serviceUrl + ConfigurationManager.AppSettings["urlGetELM0branch"];
    static string _urlGetELM0Level = _serviceUrl + ConfigurationManager.AppSettings["urlGetELM0Level"];
    static string _urlGetELPassCouse = _serviceUrl + ConfigurationManager.AppSettings["urlGetELPassCouse"];
    static string _urlSetELU0Course = _serviceUrl + ConfigurationManager.AppSettings["urlSetELU0Course"];
    static string _urlGetELGetDetailCouse = _serviceUrl + ConfigurationManager.AppSettings["urlGetELGetDetailCouse"];
    static string _urlGetELGetDetailCousePerSec = _serviceUrl + ConfigurationManager.AppSettings["urlGetELGetDetailCousePerSec"];
    static string _urlSetDelELU0Course = _serviceUrl + ConfigurationManager.AppSettings["urlSetDelELU0Course"];

    static string _path_file_logocouse = ConfigurationManager.AppSettings["path_file_logocouse"];
    //-- Elearning --//


    static string _urlGetel_employee = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_employee"];

    static string _urlGetel_m0_training = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_m0_training"];

    static string _urlGetErrorel_m0_training = _serviceUrl + ConfigurationManager.AppSettings["urlGetErrorel_m0_training"];
    static string _urlGetel_lu_position = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_lu_position"];
    //
    static string _urlGetel_u_course = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_u_course"];
    static string _urlSetInsel_u_course = _serviceUrl + ConfigurationManager.AppSettings["urlSetInsel_u_course"];
    static string _urlDelel_u_course = _serviceUrl + ConfigurationManager.AppSettings["urlDelel_u_course"];
    static string _urlSetUpdel_u_course = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdel_u_course"];

    static string _urlGetel_u1_training_req = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_u1_training_req"];
    static string _urlGetel_u1_training_req_Dept = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_u1_training_req_Dept"];
    string _PathFileimage = ConfigurationSettings.AppSettings["path_flieimage_elearning"];

    // string _localJson = "";
    int _tempInt = 0;

    int emp_idx = 0;
    string _gPathFile = ConfigurationSettings.AppSettings["path_flie_elearning"];


    string _FromcourseRunNo = "u_course";

    string _Folder_coursetestimg = "course_test_img";

    string _Folder_courseBin = "course_test_img_bin";

    //set defualt
    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;


    //object
    FormView _FormView;
    DropDownList _ddlcourse_assessment;
    Image _img_proposition;
    HttpPostedFile file_delImage;
    #endregion Init

    #region Constant
    public static class Constants
    {
        public const int SELECT_ALL = 20;
        public const int SELECT_WHERE = 21;
        public const int NUMBER_NULL = -999;
    }
    #endregion Constant

    #region Connect Config Quiz
    static string _urlCreateQuiz = _serviceUrl + ConfigurationManager.AppSettings["urlInsertMaster_Quiz"];
    static string _urlGetMasterQuiz = _serviceUrl + ConfigurationManager.AppSettings["urlSelectMaster_Quiz"];
    static string _urlDeleteQuiz = _serviceUrl + ConfigurationManager.AppSettings["urlDeleteMaster_Quiz"];
    static string _urlUpdateQuiz = _serviceUrl + ConfigurationManager.AppSettings["urlUpdateMaster_Quiz"];

    string extension_q_el;
    string extension_1_el;
    string extension_2_el;
    string extension_3_el;
    string extension_4_el;
    string extension_ans_match;

    int checkcount_el;

    HttpPostedFile UploadedImage_q_el;
    HttpPostedFile UploadedImage_a_el;
    HttpPostedFile UploadedImage_b_el;
    HttpPostedFile UploadedImage_c_el;
    HttpPostedFile UploadedImage_d_el;
    HttpPostedFile UploadedImage_ansmatch_el;

    HttpPostedFile file_delImage_el;
    #endregion


    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());

        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];

        ViewState["org_permission"] = _dataEmployee.employee_list[0].org_idx;
        ViewState["rsec_permission"] = _dataEmployee.employee_list[0].rsec_idx;
        ViewState["rdept_permission"] = _dataEmployee.employee_list[0].rdept_idx;
        ViewState["rpos_permission"] = _dataEmployee.employee_list[0].rpos_idx;
        ViewState["joblevel_permission"] = _dataEmployee.employee_list[0].jobgrade_level;
        ViewState["emp_type_permission"] = _dataEmployee.employee_list[0].emp_type_idx;
        ViewState["time_idx_permission"] = _dataEmployee.employee_list[0].ACIDX; // shift type

        ViewState["Vs_check_tab_noidx"] = 0;

    }


    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        emp_idx = int.Parse(Session["emp_idx"].ToString());
        Hddfld_folderImage.Value = "fd_emp" + emp_idx.ToString() + DateTime.Now.ToString("yyyyMMdd");

        if (!IsPostBack)
        {

            initPage();
            initPageLoad();



            ViewState["path_q"] = "0";
            ViewState["path_1"] = "0";
            ViewState["path_2"] = "0";
            ViewState["path_3"] = "0";
            ViewState["path_4"] = "0";
            ViewState["path_ans_match"] = "0";
            ViewState["TypeInsert_addon"] = "1";
            ViewState["u2_main_course_idx_addon"] = "0";
            ViewState["check_edit"] = "0";
            //  HttpPostedFile file = Request.Files["UploadedImage"];
            HttpPostedFile file_UploadedImage_proposition = Request.Files["UploadedImage_proposition"];
            if (file_UploadedImage_proposition != null && file_UploadedImage_proposition.ContentLength > 0)
            {
                imageProcessRequest(file_UploadedImage_proposition, 1);
            }
            HttpPostedFile file_UploadedImage_a = Request.Files["UploadedImage_a"];
            if (file_UploadedImage_a != null && file_UploadedImage_a.ContentLength > 0)
            {
                imageProcessRequest(file_UploadedImage_a, 2);
            }
            HttpPostedFile file_UploadedImage_b = Request.Files["UploadedImage_b"];
            if (file_UploadedImage_b != null && file_UploadedImage_b.ContentLength > 0)
            {
                imageProcessRequest(file_UploadedImage_b, 3);
            }
            HttpPostedFile file_UploadedImage_c = Request.Files["UploadedImage_c"];
            if (file_UploadedImage_c != null && file_UploadedImage_c.ContentLength > 0)
            {
                imageProcessRequest(file_UploadedImage_c, 4);
            }
            HttpPostedFile file_UploadedImage_d = Request.Files["UploadedImage_d"];
            if (file_UploadedImage_d != null && file_UploadedImage_d.ContentLength > 0)
            {
                imageProcessRequest(file_UploadedImage_d, 5);
            }

            file_delImage = Request.Files["delImage_1"];
            if (file_delImage != null && file_delImage.ContentLength > 0)
            {
                imageupload_del("1");
            }
            file_delImage = Request.Files["delImage_2"];
            if (file_delImage != null && file_delImage.ContentLength > 0)
            {
                imageupload_del("2");
            }
            file_delImage = Request.Files["delImage_3"];
            if (file_delImage != null && file_delImage.ContentLength > 0)
            {
                imageupload_del("3");
            }
            file_delImage = Request.Files["delImage_4"];
            if (file_delImage != null && file_delImage.ContentLength > 0)
            {
                imageupload_del("4");
            }
            file_delImage = Request.Files["delImage_5"];
            if (file_delImage != null && file_delImage.ContentLength > 0)
            {
                imageupload_del("5");
            }

            _func_dmu.zSetDdlMonth(ddlMonthSearch_L);
            ddlMonthSearch_L.SelectedValue = (int.Parse(DateTime.Now.ToString("MM", CultureInfo.InstalledUICulture))).ToString();
            _func_dmu.zDropDownList(ddlYearSearch_L,
                                "-",
                                "zyear",
                                "zyear",
                                "0",
                                "trainingLoolup",
                                DateTime.Now.ToString("yyyy", CultureInfo.InstalledUICulture),
                                "COURSE-YEAR"
                                );

            _func_dmu.zSetDdlMonth(ddlMonthSearch_TrnNSur);
            ddlMonthSearch_TrnNSur.SelectedValue = (int.Parse(DateTime.Now.ToString("MM", CultureInfo.InstalledUICulture))).ToString();
            _func_dmu.zDropDownList(ddlYearSearch_TrnNSur,
                                "-",
                                "zyear",
                                "zyear",
                                "0",
                                "trainingLoolup",
                                DateTime.Now.ToString("yyyy", CultureInfo.InstalledUICulture),
                                "TRN-YEAR"
                                );

            _func_dmu.zSetDdlMonth(ddlMonthSearch_TrnNSurDept);
            ddlMonthSearch_TrnNSurDept.SelectedValue = (int.Parse(DateTime.Now.ToString("MM", CultureInfo.InstalledUICulture))).ToString();
            _func_dmu.zDropDownList(ddlYearSearch_TrnNSurDept,
                                "-",
                                "zyear",
                                "zyear",
                                "0",
                                "trainingLoolup",
                                DateTime.Now.ToString("yyyy", CultureInfo.InstalledUICulture),
                                "TRN-YEAR"
                                );

            getDatasetEmpty();
            DataSet ds = (DataSet)ViewState["vsel_u2_course"];
            _func_dmu.zSetGridData(GvTest, ds.Tables["dsel_u2_course"]);
            //_func_dmu.zSetGridData(GridView1, ds.Tables["dsel_u2_course"]);
            ////zSetMode(2);


            //get dataset
            getDeptCouseList();
            getPerDetailCouseListUpdate();
            //dataset



        }

        setTrigger();



    }

    private void setTrigger()
    {
        linkBtnTrigger(btnListData);
        linkBtnTrigger(btnInsert);
        linkBtnTrigger(btnSearchTrnNeedsSurvey);
        linkBtnTrigger(btnSearchTrnNeedsSurveyDEPT);
        linkBtnTrigger(btncourse);
        linkBtnTrigger(btntest);
        linkBtnTrigger(btnFilter);
        linkBtnTrigger(btnVideoAdd);

        linkBtnTrigger(btnSaveInsert);
        linkBtnTrigger(btnClear);
        linkBtnTrigger(btnevaluationform);
        linkBtnTrigger(btntestAdd);
        linkBtnTrigger(btntestClear);
        linkBtnTrigger(btnAdd);
        linkBtnTrigger(btnsearch_TrnNSur);
        linkBtnTrigger(btnsearch_TrnNSurDept);
        linkBtnTrigger(btnSaveUpdate);
        linkBtnTrigger(btnCancel);
        linkBtnTrigger(btnVideoAdd);
        linkBtnTrigger(btnexport1);
        linkBtnTrigger(btnexport2);

        gridViewTrigger(GvTest);
        gridViewTrigger(GvListData);
        gridViewTrigger(GvDeptTraningList);

        linkBtnTrigger(btnCreatecourse);
        linkBtnTrigger(btncreatequiz);
        linkBtnTrigger(btnviewquiz);


    }

    #endregion Page Load

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }


    /* Start Std*/
    #region Std

    #region zSetMode
    public void zSetMode(int AiMode)
    {
        pnlSave.Visible = false;
        switch (AiMode)
        {
            case 0:  //insert mode
                {

                    pnlListData.Visible = false;
                    pnlmenu_detail.Visible = true;
                    MultiViewBody.SetActiveView(View_trainingPage);
                    fvCRUD.Visible = true;
                    fv_Update.Visible = false;
                    fvCRUD.ChangeMode(FormViewMode.Insert);
                    setActiveTab("I");
                    SetViewStateEmpty();
                    actionIndex();
                    Select_Page1showdata();

                }
                break;
            case 1:  //update mode
                {

                    pnlListData.Visible = false;
                    pnlmenu_detail.Visible = true;
                    MultiViewBody.SetActiveView(View_trainingPage);
                    fvCRUD.Visible = false;
                    fv_Update.Visible = true;
                    fv_Update.ChangeMode(FormViewMode.Edit);
                    setActiveTab("I");
                    SetViewStateEmpty();
                    actionIndex();
                }
                break;
            case 2://preview mode
                {

                    pnlListData.Visible = true;
                    //pnlselect.Visible = false;
                    //pnlSearchTrnNSur.Visible = false;
                    //pnlSearchTrnNSurDept.Visible = false;
                    pnlmenu_detail.Visible = false;
                    //pnladdvideo.Visible = false;
                    setActiveTab("P");
                    CreateDs_dsListData();
                    ShowListData();
                    MultiViewBody.SetActiveView(View_ListDataPag);

                }
                break;
            case 3://preview mode
                {

                    pnlListData.Visible = false;
                    //pnlselect.Visible = false;
                    //pnlSearchTrnNSur.Visible = true;
                    //pnlSearchTrnNSurDept.Visible = false;
                    setActiveTab("SNS");
                    SetViewStateEmpty();
                    MultiViewBody.SetActiveView(View_RptTraining);

                }
                break;
            case 4://preview mode
                {

                    pnlListData.Visible = false;
                    setActiveTab("SNSDEPT");
                    SetViewStateEmpty();
                    MultiViewBody.SetActiveView(View_RptTrainingByDept);

                }
                break;
        }
    }
    #endregion zSetMode

    private void ShowListDataSearch(string sName = "")
    {

        _data_elearning.el_traning_req_action = new traning_req[1];
        traning_req obj = new traning_req();
        obj.filter_keyword = sName;
        obj.zmonth = int.Parse(ddlMonthSearch_TrnNSur.SelectedValue);
        obj.zyear = int.Parse(ddlYearSearch_TrnNSur.SelectedValue);
        _data_elearning.el_traning_req_action[0] = obj;
        _data_elearning = _func_dmu.zCallServiceNetwork(_urlGetel_u1_training_req, _data_elearning);

        _func_dmu.zSetGridData(GvListData_TrnNSur, _data_elearning.el_traning_req_action);

        ViewState["GvListData_TrnNSur"] = _data_elearning.el_traning_req_action;

    }

    private void ShowListDataSearchDept(string sName = "")
    {

        _data_elearning.el_traning_req_action = new traning_req[1];
        traning_req obj = new traning_req();
        obj.filter_keyword = sName;
        obj.zmonth = int.Parse(ddlMonthSearch_TrnNSurDept.SelectedValue);
        obj.zyear = int.Parse(ddlYearSearch_TrnNSurDept.SelectedValue);
        _data_elearning.el_traning_req_action[0] = obj;
        _data_elearning = _func_dmu.zCallServiceNetwork(_urlGetel_u1_training_req_Dept, _data_elearning);

        _func_dmu.zSetGridData(GvListData_TrnNSurDept, _data_elearning.el_traning_req_action);
        ViewState["GvListData_TrnNSurDept"] = _data_elearning.el_traning_req_action;

    }

    #region setActiveTab
    protected void setActiveTab(string activeTab)
    {
        liInsert.Attributes.Add("class", "");
        liListData.Attributes.Add("class", "");
        liSearchTrnNeedsSurvey.Attributes.Add("class", "");
        liSearchTrnNeedsSurveyDEPT.Attributes.Add("class", "");
        litraining.Attributes.Add("class", "");
        litest.Attributes.Add("class", "");
        lievaluationform.Attributes.Add("class", "");
        switch (activeTab)
        {
            case "P":
                liListData.Attributes.Add("class", "active");

                break;
            case "I":
                liInsert.Attributes.Add("class", "active");
                litraining.Attributes.Add("class", "active");
                break;
            case "SNS":
                liSearchTrnNeedsSurvey.Attributes.Add("class", "active");
                break;
            case "SNSDEPT":
                liSearchTrnNeedsSurveyDEPT.Attributes.Add("class", "active");
                break;
            case "evaluationform":
                lievaluationform.Attributes.Add("class", "active");
                break;
            case "test":
                litest.Attributes.Add("class", "active");
                break;
        }
    }
    #endregion setActiveTab

    #endregion Std

    #region Action
    protected void actionIndex()
    {
        Select_DetailList();
    }
    protected void Select_DetailList()
    {
        _data_elearning.employeeM_action = new employeeM[1];
        employeeM obj = new employeeM();
        obj.EmpIDX = emp_idx;
        _data_elearning.employeeM_action[0] = obj;
        //litDebug.Text = _funcTool.convertObjectToXml(_data_elearning);
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
        _data_elearning = _func_dmu.zCallServiceNetwork(_urlGetel_employee, _data_elearning);

        if (_data_elearning.employeeM_action != null)
        {
            foreach (var item in _data_elearning.employeeM_action)
            {
                ViewState["DocCode"] = item.EmpCode;
                ViewState["CEmpIDX_Create"] = item.EmpIDX;
                ViewState["RPosIDX_J"] = item.RPosIDX_J;
                ViewState["RDeptID"] = item.RDeptID;
            }
        }


    }

    private void SetViewStateEmpty()
    {
        ViewState["DocCode"] = "";
        ViewState["CEmpIDX_Create"] = "";
        ViewState["RPosIDX_J"] = "0";
        ViewState["RDeptID"] = "0";
        getDatasetEmpty();
        DataSet dsEmpTraning = (DataSet)ViewState["vsEmpTraningList"];


    }
    public Boolean getDataCheckbox(string id)
    {
        int i = 0;
        if ((id != "") && (id != null))
        {
            i = int.Parse(id);
        }
        if (i == 0)
        {
            return false;
        }
        else
        {
            return true;
        }

    }
    protected void Select_Page1showdata()
    {
        data_elearning dataelearning = new data_elearning();
        dataelearning.el_course_action = new course[1];
        course obj = new course();

        obj.operation_status_id = "U0-EMPTY";
        dataelearning.el_course_action[0] = obj;
        dataelearning = _func_dmu.zCallServiceNetwork(_urlGetel_u_course, dataelearning);
        _func_dmu.zSetFormViewData(fvCRUD, dataelearning.el_course_action);

        zModeData("E");
        Hddfld_course_no.Value = "";
        Hddfld_status.Value = "I";
        Hddfld_u0_course_idx.Value = "";
        TextBox txtcourse_no = (TextBox)fvCRUD.FindControl("txtcourse_no");
        DropDownList ddlm0_training_group_idx_ref = (DropDownList)fvCRUD.FindControl("ddlm0_training_group_idx_ref");
        DropDownList ddm0_training_branch_idx = (DropDownList)fvCRUD.FindControl("ddm0_training_branch_idx");
        TextBox txtcourse_name = (TextBox)fvCRUD.FindControl("txtcourse_name");
        TextBox txtcourse_date = (TextBox)fvCRUD.FindControl("txtcourse_date");
        TextBox txtcourse_remark = (TextBox)fvCRUD.FindControl("txtcourse_remark");
        DropDownList ddllevel_code = (DropDownList)fvCRUD.FindControl("ddllevel_code");
        TextBox txtcourse_score = (TextBox)fvCRUD.FindControl("txtcourse_score");
        GridView GvCourse = (GridView)fvCRUD.FindControl("GvCourse");
        GridView GvM0_EmpTarget = (GridView)fvCRUD.FindControl("GvM0_EmpTarget");

        txtcourse_date.Text = DateTime.Now.ToString("dd/MM/yyyy", CultureInfo.InstalledUICulture);
        _func_dmu.zDropDownList(ddllevel_code,
                                "",
                                "level_code",
                                "level_code",
                                "0",
                                "trainingLoolup",
                                "",
                                "LEVEL"

                                );
        _func_dmu.zClearDataDropDownList(ddm0_training_branch_idx);
        _func_dmu.zDropDownList(ddlm0_training_group_idx_ref,
                                "",
                                "m0_training_group_idx",
                                "training_group_name",
                                "0",
                                "trainingLoolup",
                                "",
                                "TRN-GROUP"
                                );

        _func_dmu.zGetOrganizationList(ddlOrg_search);
        _func_dmu.zClearDataDropDownList(ddlDept_search);
        _func_dmu.zClearDataDropDownList(ddlSec_search);

        _func_dmu.zGridLookUp(GvCourse, "trainingLoolup", "TRN-COURSE-LIST");
        _func_dmu.zGridLookUp(GvM0_EmpTarget, "trainingLoolup", "EMP-TARGET");

        DataSet dsu1 = (DataSet)ViewState["vsel_u1_course"];
        _func_dmu.zSetGridData(GvDeptTraningList, dsu1.Tables["dsel_u1_course"]);
        DataSet Dsu2 = (DataSet)ViewState["vsel_u2_course"];
        _func_dmu.zSetGridData(GvTest, Dsu2.Tables["dsel_u2_course"]);
        setGvEmpTarget_searchList();

    }

    protected void getDatasetEmpty() // สร้าง dataset
    {
        CreateDs_dsEmployeeTable();
        CreateDs_dsEmpTraningTable();
        CreateDs_dsEmpTraningTable1();
        CreateDs_dsAddVedio();
        CreateDs_dsevaluation();
        CreateDs_dstest();
        CreateDs_el_u1_course();
        CreateDs_el_u5_course();
    }
    private void CreateDs_dsEmployeeTable()
    {
        DataSet ds = new DataSet();
        ds.Tables.Add("dsEmployee");
        ds.Tables["dsEmployee"].Columns.Add("EmpIDX", typeof(String));
        ds.Tables["dsEmployee"].Columns.Add("DocCode", typeof(String));
        ds.Tables["dsEmployee"].Columns.Add("EmpCode", typeof(String));
        ds.Tables["dsEmployee"].Columns.Add("FullNameTH", typeof(String));
        ds.Tables["dsEmployee"].Columns.Add("RDeptID", typeof(String));
        ds.Tables["dsEmployee"].Columns.Add("RPosIDX_J", typeof(String));
        ViewState["vsEmployeeList"] = ds;
    }
    private void CreateDs_dsEmpTraningTable()
    {
        DataSet ds = new DataSet();
        ds.Tables.Add("dsEmpTraning");
        ds.Tables["dsEmpTraning"].Columns.Add("EmpIDX", typeof(String));
        ds.Tables["dsEmpTraning"].Columns.Add("DocCode", typeof(String));
        ds.Tables["dsEmpTraning"].Columns.Add("EmpCode", typeof(String));
        ds.Tables["dsEmpTraning"].Columns.Add("FullNameTH", typeof(String));
        ds.Tables["dsEmpTraning"].Columns.Add("RDeptID", typeof(String));
        ds.Tables["dsEmpTraning"].Columns.Add("RPosIDX_J", typeof(String));
        ds.Tables["dsEmpTraning"].Columns.Add("m0_training_idx", typeof(String));
        ds.Tables["dsEmpTraning"].Columns.Add("training_name", typeof(String));
        ds.Tables["dsEmpTraning"].Columns.Add("Training_typeid", typeof(String));
        ds.Tables["dsEmpTraning"].Columns.Add("Training_type", typeof(String));
        ds.Tables["dsEmpTraning"].Columns.Add("Training_want", typeof(String));
        ds.Tables["dsEmpTraning"].Columns.Add("Position_id", typeof(String));
        ds.Tables["dsEmpTraning"].Columns.Add("Position", typeof(String));
        ds.Tables["dsEmpTraning"].Columns.Add("Amount", typeof(String));
        ds.Tables["dsEmpTraning"].Columns.Add("Qty", typeof(String));
        ds.Tables["dsEmpTraning"].Columns.Add("Date", typeof(String));
        ViewState["vsEmpTraningList"] = ds;
    }

    private void CreateDs_dsEmpTraningTable1()
    {
        DataSet ds = new DataSet();
        ds.Tables.Add("dsEmpTraning1");
        ds.Tables["dsEmpTraning1"].Columns.Add("EmpIDX", typeof(String));
        ds.Tables["dsEmpTraning1"].Columns.Add("DocCode", typeof(String));
        ds.Tables["dsEmpTraning1"].Columns.Add("EmpCode", typeof(String));
        ds.Tables["dsEmpTraning1"].Columns.Add("FullNameTH", typeof(String));
        ds.Tables["dsEmpTraning1"].Columns.Add("RDeptID", typeof(String));
        ds.Tables["dsEmpTraning1"].Columns.Add("RPosIDX_J", typeof(String));
        ds.Tables["dsEmpTraning1"].Columns.Add("m0_training_idx", typeof(String));
        ds.Tables["dsEmpTraning1"].Columns.Add("training_name", typeof(String));
        ds.Tables["dsEmpTraning1"].Columns.Add("Training_typeid", typeof(String));
        ds.Tables["dsEmpTraning1"].Columns.Add("Training_type", typeof(String));
        ds.Tables["dsEmpTraning1"].Columns.Add("Training_want", typeof(String));
        ds.Tables["dsEmpTraning1"].Columns.Add("Position_id", typeof(String));
        ds.Tables["dsEmpTraning1"].Columns.Add("Position", typeof(String));
        ds.Tables["dsEmpTraning1"].Columns.Add("Amount", typeof(String));
        ds.Tables["dsEmpTraning1"].Columns.Add("Qty", typeof(String));
        ds.Tables["dsEmpTraning1"].Columns.Add("Date", typeof(String));
        ViewState["vsEmpTraning1List"] = ds;
    }

    private void CreateDs_dsListData()
    {
        DataSet ds = new DataSet();
        ds.Tables.Add("dsListData");
        ds.Tables["dsListData"].Columns.Add("EmpIDX", typeof(String));
        ds.Tables["dsListData"].Columns.Add("DocCode", typeof(String));
        ds.Tables["dsListData"].Columns.Add("EmpCode", typeof(String));
        ds.Tables["dsListData"].Columns.Add("FullNameTH", typeof(String));
        ds.Tables["dsListData"].Columns.Add("RDeptID", typeof(String));
        ds.Tables["dsListData"].Columns.Add("RPosIDX_J", typeof(String));
        ds.Tables["dsListData"].Columns.Add("m0_training_idx", typeof(String));
        ds.Tables["dsListData"].Columns.Add("training_name", typeof(String));
        ds.Tables["dsListData"].Columns.Add("Training_typeid", typeof(String));
        ds.Tables["dsListData"].Columns.Add("Training_type", typeof(String));
        ds.Tables["dsListData"].Columns.Add("Training_want", typeof(String));
        ds.Tables["dsListData"].Columns.Add("Position_id", typeof(String));
        ds.Tables["dsListData"].Columns.Add("Position", typeof(String));
        ds.Tables["dsListData"].Columns.Add("Amount", typeof(String));
        ds.Tables["dsListData"].Columns.Add("Qty", typeof(String));
        ds.Tables["dsListData"].Columns.Add("Date", typeof(String));
        ds.Tables["dsListData"].Columns.Add("Date_From", typeof(DateTime));
        ds.Tables["dsListData"].Columns.Add("Date_To", typeof(DateTime));
        ds.Tables["dsListData"].Columns.Add("Qty_HH", typeof(float));
        ds.Tables["dsListData"].Columns.Add("Amount_trn", typeof(float));
        ds.Tables["dsListData"].Columns.Add("status", typeof(int));
        ViewState["vsListData"] = ds;
    }

    private void ShowListData()
    {

        _data_elearning.el_course_action = new course[1];
        course obj = new course();
        obj.filter_keyword = txtFilterKeyword_L.Text;
        obj.zmonth = int.Parse(ddlMonthSearch_L.SelectedValue);
        obj.zyear = int.Parse(ddlYearSearch_L.SelectedValue);
        obj.operation_status_id = "U0";
        _data_elearning.el_course_action[0] = obj;
        _data_elearning = _func_dmu.zCallServiceNetwork(_urlGetel_u_course, _data_elearning);
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
        _func_dmu.zSetGridData(GvListData, _data_elearning.el_course_action);

    }


    protected void onSelectedIndexChanged(object sender, EventArgs e)
    {

        if (sender is CheckBox)
        {
            var rdName = (CheckBox)sender;
            switch (rdName.ID)
            {
                case "GvPosition_cb_RPosIDX_J":

                    break;

            }
        }

    }
    protected void onRowDataBound(Object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        string sGvName = GvName.ID;

        if (sGvName == "GvListData")
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton btnUpdate_GvListData = (LinkButton)e.Row.Cells[11].FindControl("btnUpdate_GvListData");
                LinkButton btnDelete_GvListData = (LinkButton)e.Row.Cells[11].FindControl("btnDelete_GvListData");
                TextBox txtapprove_status = (TextBox)e.Row.Cells[11].FindControl("txttrn_plan_flag_GvListData");
                LinkButton btnDetail = (LinkButton)e.Row.Cells[11].FindControl("btnDetail");
                linkBtnTrigger(btnUpdate_GvListData);
                linkBtnTrigger(btnDelete_GvListData);
                linkBtnTrigger(btnDetail);
                Boolean bBl = true;
                if (txtapprove_status.Text == "1") // 4	อนุมัติ
                {
                    bBl = false;
                }
                // btnUpdate_GvListData.Visible = bBl;
                btnDelete_GvListData.Visible = bBl;
            }
        }
        else if (sGvName == "GvTest")
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label btncourse_proposition_img_GvTestx = (Label)e.Row.Cells[1].FindControl("btncourse_proposition_img_GvTest");
                Label btncourse_propos_a_img_GvTest = (Label)e.Row.Cells[2].FindControl("btncourse_propos_a_img_GvTest");
                Label btncourse_propos_b_img_GvTest = (Label)e.Row.Cells[3].FindControl("btncourse_propos_b_img_GvTest");
                Label btncourse_propos_c_img_GvTest = (Label)e.Row.Cells[4].FindControl("btncourse_propos_c_img_GvTest");
                Label btncourse_propos_d_img_GvTest = (Label)e.Row.Cells[5].FindControl("btncourse_propos_d_img_GvTest");
                LinkButton btnRemove_GvTest = (LinkButton)e.Row.Cells[8].FindControl("btnRemove_GvTest");
                //linkBtnTrigger(btncourse_proposition_img_GvTestx);
                //linkBtnTrigger(btncourse_propos_a_img_GvTest);
                //linkBtnTrigger(btncourse_propos_b_img_GvTest);
                //linkBtnTrigger(btncourse_propos_c_img_GvTest);
                //linkBtnTrigger(btncourse_propos_d_img_GvTest);
                linkBtnTrigger(btnRemove_GvTest);


                Label lbcourse_proposition_img_GvTest = (Label)e.Row.Cells[1].FindControl("lbcourse_proposition_img_GvTest");
                Label lbcourse_propos_a_img_GvTest = (Label)e.Row.Cells[2].FindControl("lbcourse_propos_a_img_GvTest");
                Label lbcourse_propos_b_img_GvTest = (Label)e.Row.Cells[3].FindControl("lbcourse_propos_b_img_GvTest");
                Label lbcourse_propos_c_img_GvTest = (Label)e.Row.Cells[4].FindControl("lbcourse_propos_c_img_GvTest");
                Label lbcourse_propos_d_img_GvTest = (Label)e.Row.Cells[5].FindControl("lbcourse_propos_d_img_GvTest");

                setBtnImage(btncourse_proposition_img_GvTestx, lbcourse_proposition_img_GvTest);
                setBtnImage(btncourse_propos_a_img_GvTest, lbcourse_propos_a_img_GvTest);
                setBtnImage(btncourse_propos_b_img_GvTest, lbcourse_propos_b_img_GvTest);
                setBtnImage(btncourse_propos_c_img_GvTest, lbcourse_propos_c_img_GvTest);
                setBtnImage(btncourse_propos_d_img_GvTest, lbcourse_propos_d_img_GvTest);

            }
        }
        else if (sGvName == "GvDeptTraningList")
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton btnRemove_GvDeptTraningList = (LinkButton)e.Row.Cells[5].FindControl("btnRemove_GvDeptTraningList");
                linkBtnTrigger(btnRemove_GvDeptTraningList);
            }
        }


        switch (GvName.ID)
        {

            case "GvDetailCouse":

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    Label lblu0_course_idx_u2 = (Label)e.Row.FindControl("lblu0_course_idx_u2");
                    LinkButton btnViewQuiz = (LinkButton)e.Row.FindControl("btnViewQuiz");

                    Label lbl_course_date_detail = (Label)e.Row.FindControl("lbl_course_date_detail");
                    LinkButton btnEdittDetailView = (LinkButton)e.Row.FindControl("btnEdittDetailView");
                    LinkButton btnDeleteDetail = (LinkButton)e.Row.FindControl("btnDeleteDetail");

                    if (lblu0_course_idx_u2.Text != "0" && lblu0_course_idx_u2.Text != null)
                    {
                        btnViewQuiz.Visible = true;
                    }
                    else
                    {
                        btnViewQuiz.Visible = false;
                    }



                    IFormatProvider culture_ = new CultureInfo("en-US", true);
                    //set can edit couse

                    if (DateTime.ParseExact((lbl_course_date_detail.Text), "dd/MM/yyyy", culture_) > DateTime.ParseExact((DateTime.Now.ToString("dd/MM/yyyy")), "dd/MM/yyyy", culture_))
                    {
                        btnEdittDetailView.Visible = true;
                        btnDeleteDetail.Visible = true;
                    }
                    else
                    {
                        btnEdittDetailView.Visible = false;
                        btnDeleteDetail.Visible = false;
                    }

                    //set can edit couse

                    //litDebug.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    //DateTime.ParseExact((lbl_date_start.Text + " " + lbl_work_finish.Text), "dd/MM/yyyy HH:mm", culture_);
                }
                break;


        }
    }
    private void setBtnImage(Label _linkButton, Label _Label)
    {
        if (_Label.Text == "")
        {
            _linkButton.Visible = false;
        }
        else
        {
            _linkButton.Visible = true;
        }
        _linkButton.Visible = false;
    }
    public string getImgUrl(string ADocno = "", string AItem = "", string AImage = "")
    {
        string sPathImage = "";

        if (AImage == "")
        {
            sPathImage = "";
        }
        else
        {
            if (ADocno != "")
            {
                sPathImage = _gPathFile + _Folder_courseBin + "/" + ADocno + "/" + AItem + "/" + AImage;
            }
            else
            {
                sPathImage = _gPathFile + _Folder_coursetestimg + "/" + Hddfld_course_no.Value + "/" + AItem + "/" + AImage;
            }
        }
        sPathImage = ResolveUrl(sPathImage);

        return sPathImage;
    }
    public static bool UrlExists(string url)
    {
        try
        {
            var request = WebRequest.Create(url) as HttpWebRequest;
            if (request == null) return false;
            request.Method = "HEAD";
            using (var response = (HttpWebResponse)request.GetResponse())
            {
                return response.StatusCode == HttpStatusCode.OK;
            }
        }
        catch (UriFormatException)
        {
            //Invalid Url
            return false;
        }
        catch (WebException)
        {
            //Unable to access url
            return false;
        }
    }
    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView GvName = (GridView)sender;
        GvName.PageIndex = e.NewPageIndex;

        switch (GvName.ID)
        {
            case "GvTraningList":
                GvName.PageIndex = e.NewPageIndex;
                GvName.DataBind();
                // showmodal_traning(txtsearch_modal_training.Text);
                break;
            case "gvModalTraning":
                GvName.PageIndex = e.NewPageIndex;
                GvName.DataBind();
                //showmodal_traning_app(txtsearch_modal_training_app.Text);
                break;
            case "GvListData":
                GvName.PageIndex = e.NewPageIndex;
                GvName.DataBind();
                ShowListData();
                SETFOCUS.Focus();
                break;
            case "GvListData_TrnNSur":
                GvName.PageIndex = e.NewPageIndex;
                GvName.DataBind();
                ShowListDataSearch(txtFilterKeyword_TrnNSur.Text);
                SETFOCUS.Focus();
                break;
            case "GvListData_TrnNSurDept":
                GvName.PageIndex = e.NewPageIndex;
                GvName.DataBind();
                ShowListDataSearchDept(txtFilterKeyword_TrnNSurDept.Text);
                SETFOCUS.Focus();
                break;
            case "GvDetailCouse":

                setGridData(GvDetailCouse, ViewState["vs_DetailCouse"]);

                break;

        }
    }
    protected string getStatus(int status)
    {

        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }
    protected string getStatusPlan(int status)
    {
        if (status == 1)
        {

            return "<span class='' data-toggle='tooltip' title='แผนการฝึกอบรม'><i class='glyphicon glyphicon-file'></i></span>";
        }
        else
        {
            return "";
        }
    }
    protected string getCourseType(int course_type_etraining, int course_type_elearning)
    {
        string a = "";
        if (course_type_etraining > 0)
        {
            a = "ClassRoom";
        }
        string b = "";
        if (course_type_elearning > 0)
        {
            b = "E-Learning";
        }
        string sValue = "";
        if ((a != "") && (b != ""))
        {
            sValue = a + " / " + b;
        }
        else if (a != "")
        {
            sValue = a;
        }
        else if (b != "")
        {
            sValue = b;
        }
        return sValue;
    }
    protected string getpriority(int status)
    {

        if (status == 1)
        {
            return "<span>Must</span>";
        }
        else if (status == 2)
        {
            return "<span>Need</span>";
        }
        else if (status == 3)
        {
            return "<span>Want</span>";
        }
        else
        {
            return "<span style='font-weight:700;'>-</span>";
        }
    }
    protected string getStatustest(int status)
    {
        if (status > 0)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    protected string getStatusEvalue(int status)
    {

        if (status > 0)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    #endregion Action

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        int _m0_training_idx;
        int _cemp_idx;

        m0_training objM0_ProductType = new m0_training();




        switch (cmdName)
        {
            case "btnListData":

                li0.Attributes.Add("class", "");
                _divMenuLiToViewQuiz_Master.Attributes.Add("class", "");
                _divMenuLiToCreateQuiz.Attributes.Add("class", "");
                _divMenuLiToViewQuiz.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                liListData.Attributes.Add("class", "");
                liInsert.Attributes.Add("class", "");
                liSearchTrnNeedsSurvey.Attributes.Add("class", "");
                liSearchTrnNeedsSurveyDEPT.Attributes.Add("class", "");


                zSetMode(2);
                pnlmenu_detail.Visible = false;
                deleteFileFDImage();
                break;
            case "btncourse":
                pnlmenu_detail.Visible = true;

                litest.Attributes.Add("class", "");
                lievaluationform.Attributes.Add("class", "");
                litraining.Attributes.Add("class", "active");
                MultiViewBody.SetActiveView(View_trainingPage);
                pnlSave.Visible = true;
                setActiveTab("I");
                deleteFileFDImage();
                break;
            case "btnInsert":
                li0.Attributes.Add("class", "");
                _divMenuLiToViewQuiz_Master.Attributes.Add("class", "");
                _divMenuLiToCreateQuiz.Attributes.Add("class", "");
                _divMenuLiToViewQuiz.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                liListData.Attributes.Add("class", "");
                liInsert.Attributes.Add("class", "");
                liSearchTrnNeedsSurvey.Attributes.Add("class", "");
                liSearchTrnNeedsSurveyDEPT.Attributes.Add("class", "");

                getDatasetEmpty();
                zSetMode(0);
                pnlmenu_detail.Visible = true;
                evaluation_Select();
                MultiViewBody.SetActiveView(View_trainingPage);
                btnSaveInsert.Visible = true;
                btnSaveUpdate.Visible = false;
                pnlSave.Visible = true;
                deleteFileFDImage();



                break;
            case "btnCancel":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btnSaveInsert":
                if (checkError() == false)
                {
                    if (zSaveInsert() == true)
                    {
                        Page.Response.Redirect(Page.Request.Url.ToString(), true);
                    }
                }
                break;

            case "btnSaveCourse": //toei

                TextBox txtcourse_name = (TextBox)fvCreateCourse.FindControl("txtcourse_name");
                DropDownList ddlm0_training_group_idx_ref = (DropDownList)fvCreateCourse.FindControl("ddlm0_training_group_idx_ref");
                DropDownList ddm0_training_branch_idx = (DropDownList)fvCreateCourse.FindControl("ddm0_training_branch_idx");
                TextBox txtcourse_remark = (TextBox)fvCreateCourse.FindControl("txtcourse_remark");
                TextBox txtcourse_date = (TextBox)fvCreateCourse.FindControl("txtcourse_date");

                DropDownList ddllevel_code = (DropDownList)fvCreateCourse.FindControl("ddllevel_code");
                TextBox txtcourse_score = (TextBox)fvCreateCourse.FindControl("txtcourse_score");
                TextBox txtscore_through_per = (TextBox)fvCreateCourse.FindControl("txtscore_through_per");
                CheckBoxList chkPositionEmpTarget = (CheckBoxList)fvCreateCourse.FindControl("chkPositionEmpTarget");

                CheckBox cbcourse_type_etraining = (CheckBox)fvCreateCourse.FindControl("cbcourse_type_etraining");
                CheckBox cbcourse_type_elearning = (CheckBox)fvCreateCourse.FindControl("cbcourse_type_elearning");

                DropDownList ddlcourse_assessment = (DropDownList)fvCreateCourse.FindControl("ddlcourse_assessment");
                DropDownList ddlcourse_status = (DropDownList)fvCreateCourse.FindControl("ddlcourse_status");

                CheckBoxList chkCoursePass = (CheckBoxList)fvCreateCourse.FindControl("chkCoursePass");
                FileUpload UploadFileLogo = (FileUpload)fvCreateCourse.FindControl("UploadFileLogo");

                data_elearning _data_elearning_insert = new data_elearning();

                //open insert u0 //
                el_u0_course_detail u0_course_insert = new el_u0_course_detail();
                _data_elearning_insert.el_u0_course_list = new el_u0_course_detail[1];
                u0_course_insert.u0_course_idx = 0;
                u0_course_insert.course_date = txtcourse_date.Text;
                u0_course_insert.m0_training_group_idx_ref = int.Parse(ddlm0_training_group_idx_ref.SelectedValue);
                u0_course_insert.m0_training_branch_idx_ref = int.Parse(ddm0_training_branch_idx.SelectedValue);
                u0_course_insert.course_name = txtcourse_name.Text;
                u0_course_insert.course_remark = txtcourse_remark.Text;
                u0_course_insert.level_code = int.Parse(ddllevel_code.SelectedItem.Text);
                ////u0_course_insert.course_priority = int.Parse(ddlcourse_priority.SelectedValue);
                u0_course_insert.course_status = int.Parse(ddlcourse_status.SelectedValue);
                u0_course_insert.course_assessment = int.Parse(ddlcourse_assessment.SelectedValue); ;
                u0_course_insert.score_through_per = decimal.Parse(txtscore_through_per.Text);

                ////if (txtcourse_score.Text != "")
                ////{
                ////    icourse_score = int.Parse(txtcourse_score.Text);
                ////}
                u0_course_insert.course_score = int.Parse(txtcourse_score.Text);
                u0_course_insert.course_created_by = _emp_idx;
                //u0_course_insert.course_updated_by = _emp_idx;

                //set check type classroom/ e-learning
                if (cbcourse_type_etraining.Checked) //ClassRoom
                {
                    u0_course_insert.course_type_etraining = 1;
                }
                else
                {
                    u0_course_insert.course_type_etraining = 0;
                }

                if (cbcourse_type_elearning.Checked) //E-Learning
                {
                    u0_course_insert.course_type_elearning = 1;
                }
                else
                {
                    u0_course_insert.course_type_elearning = 0;
                }

                //set check type classroom/ e-learning

                //_func_dmu.zBooleanToInt(cbcourse_type_etraining.Checked);
                //u0_course_insert.course_type_elearning = int.Parse(cbcourse_type_elearning.Checked.ToString());//_func_dmu.zBooleanToInt(cbcourse_type_elearning.Checked);
                u0_course_insert.course_plan_status = "I";

                //node
                u0_course_insert.approve_status = 0;
                u0_course_insert.u0_idx = 5;
                u0_course_insert.node_idx = 9;
                u0_course_insert.actor_idx = 1;
                u0_course_insert.app_flag = 0;
                u0_course_insert.app_user = 0;

                u0_course_insert.operation_status_id = "U0";
                _data_elearning_insert.el_u0_course_list[0] = u0_course_insert;


                //_data_elearning_insert = //_func_dmu.zCallServicePostNetwork(_urlSetInsel_u_course, _data_elearning_insert);

                //end insert u0 //


                //open insert u5 //
                var u5_insert = new el_u5_course_detail[chkPositionEmpTarget.Items.Count];
                int sum_u5 = 0;
                int i_u5 = 0;

                List<String> AddoingList_u5 = new List<string>();
                foreach (ListItem chkList_u5insert in chkPositionEmpTarget.Items)
                {
                    if (chkList_u5insert.Selected)
                    {
                        u5_insert[i_u5] = new el_u5_course_detail();
                        ////u5_insert[i_u5].u0_course_idx_ref = u0_course_idx_ref;
                        u5_insert[i_u5].TIDX_ref = int.Parse(chkList_u5insert.Value);//_func_dmu.zStringToInt(item["TIDX_ref"].ToString());
                        //u5_insert[i_u5].TIDX_flag = _func_dmu.zStringToInt(item["TIDX_flag"].ToString());
                        u5_insert[i_u5].course_status = 1;
                        u5_insert[i_u5].course_updated_by = emp_idx;
                        u5_insert[i_u5].operation_status_id = "U5";

                        sum_u5 = sum_u5 + 1;

                        i_u5++;
                    }
                }
                _data_elearning_insert.el_u5_course_list = u5_insert;
                //end insert u5 //

                //open insert u1 //

                var ds_u1_insert = (DataSet)ViewState["vsDepartment"];
                var u1_doc_insert = new el_u1_course_detail[ds_u1_insert.Tables[0].Rows.Count];
                var SumT_check = ds_u1_insert.Tables[0].Rows.Count;

                //course[] objcourse1 = new course[dsU1.Tables["dsel_u1_course"].Rows.Count];

                //var u1_doc_insert_ = new List<string>();
                int s = 0;
                //int s_ = 0;
                //course[] u1_doc_insert_ = new course[s];

                //Check องค์กร ฝ่าย ตอนแบ่ง License

                foreach (DataRow dr_u1 in ds_u1_insert.Tables[0].Rows)
                {

                    u1_doc_insert[s] = new el_u1_course_detail();
                    u1_doc_insert[s].RSecID_ref = int.Parse(dr_u1["drRSecIDX"].ToString());
                    u1_doc_insert[s].RDeptID_ref = int.Parse(dr_u1["drRDeptIDX"].ToString());
                    u1_doc_insert[s].org_idx_ref = int.Parse(dr_u1["drOrgIDX"].ToString());
                    u1_doc_insert[s].TIDX_ref = int.Parse(dr_u1["drPosIDX"].ToString());
                    u1_doc_insert[s].TIDX_value = dr_u1["drPositionEmpIDX"].ToString();//dr_u1["drPositionEmpIDX"].ToString();

                    //u1_doc_insert[s].TIDX_ref = dr_u1["drPositionEmpIDX"].ToString();
                    u1_doc_insert[s].course_status = 1;
                    u1_doc_insert[s].course_updated_by = _emp_idx;
                    u1_doc_insert[s].operation_status_id = "U1";

                    s++;

                }


                _data_elearning_insert.el_u1_course_list = u1_doc_insert;
                //end insert u1 //

                //open insert u4 //
                var u4_insert = new el_u4_course_detail[chkCoursePass.Items.Count];
                int sum_u4 = 0;
                int i_u4 = 0;

                List<String> AddoingList_u4 = new List<string>();
                foreach (ListItem chkList_u4insert in chkCoursePass.Items)
                {
                    if (chkList_u4insert.Selected)
                    {
                        u4_insert[i_u4] = new el_u4_course_detail();
                        ////u5_insert[i_u5].u0_course_idx_ref = u0_course_idx_ref;
                        u4_insert[i_u4].u0_course_idx_ref_pass = int.Parse(chkList_u4insert.Value);//_func_dmu.zStringToInt(item["TIDX_ref"].ToString());
                        //u5_insert[i_u5].TIDX_flag = _func_dmu.zStringToInt(item["TIDX_flag"].ToString());
                        u4_insert[i_u4].course_status = 1;
                        u4_insert[i_u4].course_updated_by = _emp_idx;
                        u4_insert[i_u4].operation_status_id = "U4";

                        sum_u4 = sum_u4 + 1;

                        i_u4++;
                    }
                }
                _data_elearning_insert.el_u4_course_list = u4_insert;
                //end insert u4 //


                _data_elearning_insert = CallServicePostElearning(_urlSetELU0Course, _data_elearning_insert);
                //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning_insert));

                if (UploadFileLogo.HasFile)
                {

                    //litDebug.Text = "have file";
                    string filepath = Server.MapPath(_path_file_logocouse);

                    //litDebug.Text = filepath.ToString();
                    //HttpFileCollection uploadedFiles = Request.Files;

                    //for (int i = 0; i < uploadedFiles.Count; i++)
                    //{
                    //    HttpPostedFile userPostedFileMemo = uploadedFiles[i];

                    //    try
                    //    {
                    //        if (userPostedFileMemo.ContentLength > 0)
                    //        {

                    string filePath2 = _data_elearning_insert.el_u0_course_list[0].course_no.ToString();
                    string filePath1 = Server.MapPath(_path_file_logocouse + filePath2);

                    string _filepathExtension = Path.GetExtension(UploadFileLogo.FileName);

                    if (!Directory.Exists(filePath1))
                    {
                        Directory.CreateDirectory(filePath1);
                    }
                    UploadFileLogo.SaveAs(filepath + filePath2 + "\\" + _data_elearning_insert.el_u0_course_list[0].course_no.ToString() + _filepathExtension.ToLower());

                    //UploadFileLogo.SaveAs(filepath + filePath2 + "\\" + _data_elearning_insert.el_u0_course_list[0].course_no.ToString() + Path.GetFileName((i).ToString()) + _filepathExtension.ToLower());
                    //litDebug.Text += "Location where saved: " + filepath + "\\" + Path.GetFileName(userPostedFile.FileName) + "<p>";
                    //        }
                    //    }
                    //    catch (Exception Ex)
                    //    {
                    //        //litDebug.Text += "Error: <br>" + Ex.Message;
                    //    }
                    //}
                }
                else
                {
                    //litDebug.Text = "no file";
                }
                setActiveTab("docDetailCouse", 0, 0, 0, 0, 0, 0, 0);

                break;

            case "btnEditSaveCourse": // edit couse

                HiddenField hfu0_course_idx = (HiddenField)fvCreateCourse.FindControl("hfu0_course_idx");
                TextBox txtcourse_name_ = (TextBox)fvCreateCourse.FindControl("txtcourse_name");
                DropDownList ddlm0_training_group_idx_ref_ = (DropDownList)fvCreateCourse.FindControl("ddlm0_training_group_idx_ref");
                DropDownList ddm0_training_branch_idx_ = (DropDownList)fvCreateCourse.FindControl("ddm0_training_branch_idx");
                TextBox txtcourse_remark_ = (TextBox)fvCreateCourse.FindControl("txtcourse_remark");
                TextBox txtcourse_date_ = (TextBox)fvCreateCourse.FindControl("txtcourse_date");

                DropDownList ddllevel_code_ = (DropDownList)fvCreateCourse.FindControl("ddllevel_code");
                TextBox txtcourse_score_ = (TextBox)fvCreateCourse.FindControl("txtcourse_score");
                TextBox txtscore_through_per_ = (TextBox)fvCreateCourse.FindControl("txtscore_through_per");
                CheckBoxList chkPositionEmpTarget_ = (CheckBoxList)fvCreateCourse.FindControl("chkPositionEmpTarget");

                CheckBox cbcourse_type_etraining_ = (CheckBox)fvCreateCourse.FindControl("cbcourse_type_etraining");
                CheckBox cbcourse_type_elearning_ = (CheckBox)fvCreateCourse.FindControl("cbcourse_type_elearning");

                DropDownList ddlcourse_assessment_ = (DropDownList)fvCreateCourse.FindControl("ddlcourse_assessment");
                DropDownList ddlcourse_status_ = (DropDownList)fvCreateCourse.FindControl("ddlcourse_status");

                CheckBoxList chkCoursePass_ = (CheckBoxList)fvCreateCourse.FindControl("chkCoursePass");

                Label lbl_u0_course_idx_ref_pass_value_ = (Label)fvCreateCourse.FindControl("lbl_u0_course_idx_ref_pass_value");
                Label lbl_TIDX_ref_value_ = (Label)fvCreateCourse.FindControl("lbl_TIDX_ref_value");

                TextBox txt_course_no = (TextBox)fvCreateCourse.FindControl("txt_course_no");

                data_elearning _data_elearning_update = new data_elearning();

                //open insert u0 //
                el_u0_course_detail u0_course_update = new el_u0_course_detail();
                _data_elearning_update.el_u0_course_list = new el_u0_course_detail[1];
                u0_course_update.u0_course_idx = int.Parse(hfu0_course_idx.Value);
                u0_course_update.course_date = txtcourse_date_.Text;
                u0_course_update.m0_training_group_idx_ref = int.Parse(ddlm0_training_group_idx_ref_.SelectedValue);
                u0_course_update.m0_training_branch_idx_ref = int.Parse(ddm0_training_branch_idx_.SelectedValue);
                u0_course_update.course_name = txtcourse_name_.Text;
                u0_course_update.course_remark = txtcourse_remark_.Text;
                u0_course_update.level_code = int.Parse(ddllevel_code_.SelectedItem.Text);
                ////u0_course_insert.course_priority = int.Parse(ddlcourse_priority.SelectedValue);
                u0_course_update.course_status = int.Parse(ddlcourse_status_.SelectedValue);
                u0_course_update.course_assessment = int.Parse(ddlcourse_assessment_.SelectedValue); ;
                u0_course_update.score_through_per = decimal.Parse(txtscore_through_per_.Text);

                ////if (txtcourse_score.Text != "")
                ////{
                ////    icourse_score = int.Parse(txtcourse_score.Text);
                ////}
                u0_course_update.course_score = int.Parse(txtcourse_score_.Text);
                u0_course_update.course_updated_by = _emp_idx;
                //u0_course_insert.course_updated_by = _emp_idx;

                //set check type classroom/ e-learning
                if (cbcourse_type_etraining_.Checked) //ClassRoom
                {
                    u0_course_update.course_type_etraining = 1;
                }
                else
                {
                    u0_course_update.course_type_etraining = 0;
                }

                if (cbcourse_type_elearning_.Checked) //E-Learning
                {
                    u0_course_update.course_type_elearning = 1;
                }
                else
                {
                    u0_course_update.course_type_elearning = 0;
                }

                //set check type classroom/ e-learning

                u0_course_update.course_plan_status = "I";

                u0_course_update.operation_status_id = "U0";
                _data_elearning_update.el_u0_course_list[0] = u0_course_update;

                //end insert u0 //


                //open insert u5 //
                var u5_insert_edit = new el_u5_course_detail[chkPositionEmpTarget_.Items.Count];
                int sum_u5_edit = 0;
                int i_u5_edit = 0;

                string[] setcheck_TIDX_ref_ = lbl_TIDX_ref_value_.Text.Split(',');
                List<String> AddoingList_u5_edit = new List<string>();
                foreach (ListItem chkList_u5insert in chkPositionEmpTarget_.Items)
                {
                    if (chkList_u5insert.Selected && !(Array.IndexOf(setcheck_TIDX_ref_, chkList_u5insert.Value) > -1))
                    {
                        u5_insert_edit[i_u5_edit] = new el_u5_course_detail();
                        ////u5_insert[i_u5].u0_course_idx_ref = u0_course_idx_ref;
                        u5_insert_edit[i_u5_edit].TIDX_ref = int.Parse(chkList_u5insert.Value);
                        //u5_insert[i_u5].TIDX_flag = _func_dmu.zStringToInt(item["TIDX_flag"].ToString());
                        u5_insert_edit[i_u5_edit].course_status = 1;
                        u5_insert_edit[i_u5_edit].course_updated_by = emp_idx;
                        u5_insert_edit[i_u5_edit].operation_status_id = "U5";

                        sum_u5_edit = sum_u5_edit + 1;

                        i_u5_edit++;
                    }
                }
                _data_elearning_update.el_u5_course_list = u5_insert_edit;
                //end insert u5 //


                //open insert u1 //

                var ds_u1_insert_edit = (DataSet)ViewState["vsDetailCousePerListUpdate"];
                var u1_doc_insert_edit = new el_u1_course_detail[ds_u1_insert_edit.Tables[0].Rows.Count];
                var Sum_check_edit = ds_u1_insert_edit.Tables[0].Rows.Count;

                //course[] objcourse1 = new course[dsU1.Tables["dsel_u1_course"].Rows.Count];

                //var u1_doc_insert_ = new List<string>();
                int s_edit = 0;
                //int s_ = 0;
                //course[] u1_doc_insert_ = new course[s];

                //Check องค์กร ฝ่าย ตอนแบ่ง License

                foreach (DataRow dr_u1 in ds_u1_insert_edit.Tables[0].Rows)
                {

                    u1_doc_insert_edit[s_edit] = new el_u1_course_detail();

                    u1_doc_insert_edit[s_edit].RSecID_ref = int.Parse(dr_u1["drRSecID_refIDXUpdate"].ToString());
                    u1_doc_insert_edit[s_edit].RDeptID_ref = int.Parse(dr_u1["drRDeptID_refIDXUpdate"].ToString());
                    u1_doc_insert_edit[s_edit].org_idx_ref = int.Parse(dr_u1["drorg_idx_refIDXUpdate"].ToString());
                    u1_doc_insert_edit[s_edit].TIDX_ref = int.Parse(dr_u1["drTIDX_refUpdate"].ToString());
                    //u1_doc_insert_edit[s_edit].TIDX_value = dr_u1["drPositionEmpIDX"].ToString();//dr_u1["drPositionEmpIDX"].ToString();

                    //u1_doc_insert[s].TIDX_ref = dr_u1["drPositionEmpIDX"].ToString();
                    u1_doc_insert_edit[s_edit].course_status = 1;
                    u1_doc_insert_edit[s_edit].course_updated_by = _emp_idx;
                    u1_doc_insert_edit[s_edit].operation_status_id = "U1";

                    s_edit++;

                }


                _data_elearning_update.el_u1_course_list = u1_doc_insert_edit;
                //////end insert u1 //

                //open insert u4 //
                var u4_insert_edit = new el_u4_course_detail[chkCoursePass_.Items.Count];
                int sum_u4_edit = 0;
                int i_u4_edit = 0;


                string[] setu0_course_idx_ref_ = lbl_u0_course_idx_ref_pass_value_.Text.Split(',');
                List<String> AddoingList_u4_edit = new List<string>();
                foreach (ListItem chkList_u4insert_edit in chkCoursePass_.Items)
                {
                    if (chkList_u4insert_edit.Selected && !(Array.IndexOf(setu0_course_idx_ref_, chkList_u4insert_edit.Value) > -1))
                    {
                        u4_insert_edit[i_u4_edit] = new el_u4_course_detail();
                        u4_insert_edit[i_u4_edit].u0_course_idx_ref_pass = int.Parse(chkList_u4insert_edit.Value);
                        u4_insert_edit[i_u4_edit].course_status = 1;
                        u4_insert_edit[i_u4_edit].course_updated_by = _emp_idx;
                        u4_insert_edit[i_u4_edit].operation_status_id = "U4";

                        sum_u4_edit = sum_u4_edit + 1;

                        i_u4_edit++;
                    }
                }
                _data_elearning_update.el_u4_course_list = u4_insert_edit;
                //end insert u4 //

                //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning_update));

                _data_elearning_insert = CallServicePostElearning(_urlSetELU0Course, _data_elearning_update);

                FileUpload UploadFileLogoEdit = (FileUpload)fvCreateCourse.FindControl("UploadFileLogoEdit");


                if (UploadFileLogoEdit.HasFile)
                {

                    string getPathfile_edit = ConfigurationManager.AppSettings["path_file_logocouse"];
                    string directoryName_edit_old = txt_course_no.Text;
                    //lbl_place_name_edit_old.Text.Trim() + lbl_room_name_en_edit_old.Text + lblm0_room_idx_edit_old.Text;

                    string directoryName_edit_new = txt_course_no.Text;// ddlPlaceUpdate.SelectedItem + txt_roomname_en_Update.Text + lblm0_room_idx_edit_old.Text;
                                                                       // string document_code = ViewState["_no_invoice"].ToString();

                    string filePath_old = Server.MapPath(getPathfile_edit + directoryName_edit_old);

                    if (!Directory.Exists(filePath_old))
                    {
                        //litDebug.Text = "have file";
                        string filePath_new = Server.MapPath(getPathfile_edit + directoryName_edit_new);

                        if (!Directory.Exists(filePath_new))
                        {
                            Directory.CreateDirectory(filePath_new);
                        }
                        string extension = Path.GetExtension(UploadFileLogoEdit.FileName);

                        UploadFileLogoEdit.SaveAs(Server.MapPath(getPathfile_edit + directoryName_edit_new) + "\\" + directoryName_edit_new + extension);

                    }
                    else
                    {

                        string path_old = string.Empty;
                        List<string> files = new List<string>();
                        DirectoryInfo dirList = new DirectoryInfo(filePath_old);
                        foreach (FileInfo file in dirList.GetFiles())
                        {
                            path_old = ResolveUrl(filePath_old + "\\" + file.Name);

                            //litDebug.Text = path_old.ToString();

                        }
                        File.Delete(path_old);

                        string extension = Path.GetExtension(UploadFileLogoEdit.FileName);
                        UploadFileLogoEdit.SaveAs(Server.MapPath(getPathfile_edit + directoryName_edit_old) + "\\" + directoryName_edit_old + extension);

                    }
                }
                else
                {

                }



                setActiveTab("docDetailCouse", 0, 0, 0, 0, 0, 0, 0);

                break;

            case "cmdSearchIndex":

                TextBox txt_document_code = (TextBox)_PanelSearchDetail.FindControl("txt_document_code");
                DropDownList ddlSearchMonth = (DropDownList)_PanelSearchDetail.FindControl("ddlSearchMonth");
                DropDownList ddlSearchYear = (DropDownList)_PanelSearchDetail.FindControl("ddlSearchYear");

                // el_u0_course_detail[] _search_Detail = (el_u0_course_detail[])ViewState["vs_DetailCouse_DataSearch"];

                if (ViewState["vs_DetailCouse_DataSearch"] != null)
                {
                    el_u0_course_detail[] _value_datasearch = (el_u0_course_detail[])ViewState["vs_DetailCouse_DataSearch"];

                    var _linq_datasearch = (from data in _value_datasearch
                                            where
                                            (int.Parse(ddlSearchMonth.SelectedValue) == 0 || data.course_date_month == int.Parse(ddlSearchMonth.SelectedValue))
                                            && (int.Parse(ddlSearchYear.SelectedValue) == 0 || data.course_date_year == int.Parse(ddlSearchYear.SelectedValue))
                                            && (txt_document_code.Text == "" || data.course_no.Contains(txt_document_code.Text))


                                            select data).Distinct().ToList();

                    ViewState["vs_DetailCouse"] = _linq_datasearch.ToList();
                    setGridData(GvDetailCouse, ViewState["vs_DetailCouse"]);
                }
                else
                {
                    ViewState["vs_DetailCouse"] = null;
                    setGridData(GvDetailCouse, ViewState["vs_DetailCouse"]);
                }

                break;

            case "cmdResetSearchIndex":

                setActiveTab("docDetailCouse", 0, 0, 0, 0, 0, 0, 0);
                break;

            case "cmdDocCancel":
                setActiveTab("docDetailCouse", 0, 0, 0, 0, 0, 0, 0);
                break;

            case "btnDelete":
                _m0_training_idx = int.Parse(cmdArg);
                _cemp_idx = emp_idx;
                zDelete(_m0_training_idx);
                ShowListData();
                break;
            case "btnFilter":
                ShowListData();
                break;

            case "btnVideoAdd":
                addTraning();
                break;


            case "btnSearchTrnNeedsSurvey":

                li0.Attributes.Add("class", "");
                _divMenuLiToViewQuiz_Master.Attributes.Add("class", "");
                _divMenuLiToCreateQuiz.Attributes.Add("class", "");
                _divMenuLiToViewQuiz.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                liListData.Attributes.Add("class", "");
                liInsert.Attributes.Add("class", "");
                liSearchTrnNeedsSurvey.Attributes.Add("class", "");
                liSearchTrnNeedsSurveyDEPT.Attributes.Add("class", "");

                zSetMode(3);
                txtFilterKeyword_TrnNSur.Text = "";
                ShowListDataSearch("");
                pnlmenu_detail.Visible = false;
                deleteFileFDImage();
                break;
            case "btnsearch_TrnNSur":
                ShowListDataSearch(txtFilterKeyword_TrnNSur.Text);
                break;
            case "btnselGvListData_TrnNSur":
                _m0_training_idx = int.Parse(cmdArg);
                pnlListData.Visible = false;
                setActiveTab("I");
                Select_Page1showdata();
                break;
            case "btnSearchTrnNeedsSurveyDEPT":

                li0.Attributes.Add("class", "");
                _divMenuLiToViewQuiz_Master.Attributes.Add("class", "");
                _divMenuLiToCreateQuiz.Attributes.Add("class", "");
                _divMenuLiToViewQuiz.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                liListData.Attributes.Add("class", "");
                liInsert.Attributes.Add("class", "");
                liSearchTrnNeedsSurvey.Attributes.Add("class", "");
                liSearchTrnNeedsSurveyDEPT.Attributes.Add("class", "");

                zSetMode(4);
                txtFilterKeyword_TrnNSurDept.Text = "";
                ShowListDataSearchDept("");
                pnlmenu_detail.Visible = false;
                deleteFileFDImage();
                break;
            case "btntest":

                litraining.Attributes.Add("class", "");
                lievaluationform.Attributes.Add("class", "");
                litest.Attributes.Add("class", "active");
                MultiViewBody.SetActiveView(View_testform);
                //linkBtnTrigger(btntestAdd);
                //  linkBtnTrigger(btntestClear);
                // gridViewTrigger(GvTest);
                cleartest();
                break;
            case "btnevaluationform":

                litraining.Attributes.Add("class", "");
                litest.Attributes.Add("class", "");
                lievaluationform.Attributes.Add("class", "active");

                MultiViewBody.SetActiveView(View_evaluationform);
                evaluationList();
                deleteFileFDImage();
                break;
            case "btnImport":

                break;
            case "btntestAdd":
                int item = 0;
                // TextBox txtcourse_item = (TextBox)fvDetail.FindControl("txtcourse_item");
                if (txtcourse_item.Text != "")
                {
                    try
                    {
                        item = int.Parse(txtcourse_item.Text);
                    }
                    catch { }
                }
                if (item <= 0)
                {
                    showAlert("กรุณากรอกลำดับให้ถูกต้อง");

                }
                else
                {
                    addtest();
                }
                break;
            case "btntestClear":
                cleartest();

                break;

            case "btnAdd":
                addDeptTraning();

                break;
            case "btnInsertDept":


                setDeptCouseList();

                break;
            case "btnInsertDeptEdit":
                setPerDetailCouseListUpdate();

                break;

            case "btnUpdate_GvListData":
                _m0_training_idx = int.Parse(cmdArg);
                getDatasetEmpty();
                if (_m0_training_idx > 0)
                {

                    zSetMode(1);

                    zShowdataUpdate(_m0_training_idx, "E");
                    pnlSave.Visible = true;
                    btnSaveInsert.Visible = false;
                    btnSaveUpdate.Visible = true;
                }
                if (Hddfld_course_no.Value == "")
                {
                    zSetMode(2);
                }
                break;
            case "btnSaveUpdate":

                if (checkErrorUpdate() == false)
                {
                    if (Hddfld_course_no.Value == "")
                    {
                        zSetMode(2);
                    }
                    else
                    {
                        if (zSaveUpdate(int.Parse(Hddfld_u0_course_idx.Value)) == true)
                        {
                            Page.Response.Redirect(Page.Request.Url.ToString(), true);
                        }

                    }
                }

                break;
            case "btnDetail":
                _m0_training_idx = int.Parse(cmdArg);
                getDatasetEmpty();
                if (_m0_training_idx > 0)
                {

                    zSetMode(1);
                    zShowdataUpdate(_m0_training_idx, "P");
                    pnlSave.Visible = true;
                    btnSaveInsert.Visible = false;
                    btnSaveUpdate.Visible = false;
                }
                if (Hddfld_course_no.Value == "")
                {
                    zSetMode(2);
                }


                break;
            case "cmdDetailView":

                setActiveTab("docCreatecourse", int.Parse(cmdArg), 0, 0, 0, 0, 0, 1);

                break;
            case "cmdEditDetailView":

                setActiveTab("docCreatecourse", int.Parse(cmdArg), 0, 0, 0, 0, 0, 2);


                break;
            case "cmdDeleteDetail":

                data_elearning _data_detailcouse_del = new data_elearning();

                _data_detailcouse_del.el_u0_course_list = new el_u0_course_detail[1];
                el_u0_course_detail datael_u0_course_del = new el_u0_course_detail();

                datael_u0_course_del.u0_course_idx = int.Parse(cmdArg);
                datael_u0_course_del.course_updated_by = _emp_idx;

                _data_detailcouse_del.el_u0_course_list[0] = datael_u0_course_del;

                _data_detailcouse_del = CallServicePostElearning(_urlSetDelELU0Course, _data_detailcouse_del);

                setActiveTab("docDetailCouse", 0, 0, 0, 0, 0, 0, 0);

                break;
            case "btnsearch_TrnNSurDept":
                ShowListDataSearchDept(txtFilterKeyword_TrnNSurDept.Text);
                break;
            case "btnsearch_GvCourse":
                searchCOURSEList();
                break;
            case "btnexport1":

                GvListData_TrnNSur_Excel.DataSource = ViewState["GvListData_TrnNSur"];
                GvListData_TrnNSur_Excel.DataBind();
                ExportGridToExcel(GvListData_TrnNSur_Excel, "รายงานสรุปหลักสูตรที่ขอ", "รายงานสรุปหลักสูตรที่ขอ");
                break;
            case "btnexport2":
                GvListData_TrnNSurDept_Excel.DataSource = ViewState["GvListData_TrnNSurDept"];
                GvListData_TrnNSurDept_Excel.DataBind();
                ExportGridToExcel(GvListData_TrnNSurDept_Excel, "รายงานสรุปหลักสูตรที่ขอของแต่ละแผนก", "รายงานสรุปหลักสูตรที่ขอของแต่ละแผนก");
                break;



        }
    }
    #endregion btnCommand

    #region event command
    protected void navCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        setActiveTab(cmdArg, 0, 0, 0, 0, 0, 0, 0);

    }

    #endregion event command

    #region reuse
    protected void initPageLoad()
    {

    }

    protected void initPage()
    {
        setActiveTab("docDetailCouse", 0, 0, 0, 0, 0, 0, 0);

    }

    protected void setDir(string dirName)
    {
        if (!Directory.Exists(Server.MapPath(dirName)))
        {
            Directory.CreateDirectory(Server.MapPath(dirName));
        }
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    //protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    //{
    //    fvName.ChangeMode(fvMode);
    //    fvName.DataSource = obj;
    //    fvName.DataBind();
    //}

    protected void setRepeaterData(Repeater rptName, Object obj)
    {
        rptName.DataSource = obj;
        rptName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setChkData(CheckBoxList chkName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        chkName.Items.Clear();
        // bind items
        chkName.DataSource = obj;
        chkName.DataTextField = _data_text;
        chkName.DataValueField = _data_value;
        chkName.DataBind();
    }

    protected void setRdoData(RadioButtonList rdoName, Object obj, string _data_text, string _data_value)
    {
        rdoName.Items.Clear();
        rdoName.DataSource = obj;
        rdoName.DataTextField = _data_text;
        rdoName.DataValueField = _data_value;
        rdoName.DataBind();
    }

    protected data_employee callServiceEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //fss.Text = _localJson.ToString();

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //fsa.Text = _cmdUrl + _localJson;

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);


        return _dataEmployee;
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    public data_elearning CallServicePostElearning(string _cmdUrl, data_elearning _data_elearning)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_elearning);
        // litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_elearning = (data_elearning)_funcTool.convertJsonToObject(typeof(data_elearning), _localJson);

        return _data_elearning;
    }

    protected data_tpm_form callServicePostTPMForm(string _cmdUrl, data_tpm_form _dtpmform)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtpmform);
        // text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtpmform = (data_tpm_form)_funcTool.convertJsonToObject(typeof(data_tpm_form), _localJson);


        return _dtpmform;
    }

    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }

    protected void getEmployeeProfile_U2Create(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["U2_CEmpIDX_Create"] = _dataEmployee;
    }

    protected void setActiveView(string activeTab, int uidx, int nodeidx, int staidx, int actor_idx, int _chk_tab, int _cempidx, int u1idx)
    {
        MultiViewBody.SetActiveView((View)MultiViewBody.FindControl(activeTab));
        setFormData(fvEmpDetail, FormViewMode.ReadOnly, null);

        pnlmenu_detail.Visible = false;

        //tab docCreatecourse
        setFormData(fvCreateCourse, FormViewMode.ReadOnly, null);


        //set tab detail
        GvDetailCouse.Visible = false;
        setGridData(GvDetailCouse, null);

        switch (activeTab)
        {
            case "docCreatecourse":

                if (uidx == 0) //create
                {
                    setFormData(fvEmpDetail, FormViewMode.ReadOnly, ((data_employee)ViewState["vsEmpProfile"]).employee_list);
                    setFormData(fvCreateCourse, FormViewMode.Insert, null);

                    DropDownList ddlm0_training_group_idx_ref = (DropDownList)fvCreateCourse.FindControl("ddlm0_training_group_idx_ref");
                    DropDownList ddm0_training_branch_idx = (DropDownList)fvCreateCourse.FindControl("ddm0_training_branch_idx");
                    DropDownList ddllevel_code = (DropDownList)fvCreateCourse.FindControl("ddllevel_code");
                    GridView GvM0_EmpTarget = (GridView)fvCreateCourse.FindControl("GvM0_EmpTarget");
                    CheckBoxList chkPositionEmpTarget = (CheckBoxList)fvCreateCourse.FindControl("chkPositionEmpTarget");

                    DropDownList ddlOrg_search = (DropDownList)fvCreateCourse.FindControl("ddlOrg_search");
                    DropDownList ddlDept_search = (DropDownList)fvCreateCourse.FindControl("ddlDept_search");
                    DropDownList ddlSec_search = (DropDownList)fvCreateCourse.FindControl("ddlSec_search");

                    CheckBoxList chkPositionEmp = (CheckBoxList)fvCreateCourse.FindControl("chkPositionEmp");
                    DropDownList ddlPositionEmp_search = (DropDownList)fvCreateCourse.FindControl("ddlPositionEmp_search");

                    getGettraininggroup(ddlm0_training_group_idx_ref);
                    getGettrainingbranch(ddm0_training_branch_idx, int.Parse(ddlm0_training_group_idx_ref.SelectedValue));
                    getGettraininglevel(ddllevel_code);
                    select_positiongroup(chkPositionEmpTarget);

                    getOrganizationList(ddlOrg_search);
                    getDepartmentList(ddlDept_search, int.Parse(ddlOrg_search.SelectedValue));
                    getSectionList(ddlSec_search, int.Parse(ddlOrg_search.SelectedValue), int.Parse(ddlDept_search.SelectedValue));

                    select_positiongroup(chkPositionEmp);
                    getpositiongroup(ddlPositionEmp_search);


                }
                else //view
                {

                    data_elearning _data_detailcouse_view = new data_elearning();

                    _data_detailcouse_view.el_u0_course_list = new el_u0_course_detail[1];
                    el_u0_course_detail datael_u0_course_view = new el_u0_course_detail();

                    datael_u0_course_view.u0_course_idx = uidx;

                    _data_detailcouse_view.el_u0_course_list[0] = datael_u0_course_view;

                    _data_detailcouse_view = CallServicePostElearning(_urlGetELGetDetailCouse, _data_detailcouse_view);

                    switch (u1idx)
                    {
                        case 1: //view detail


                            setFormData(fvCreateCourse, FormViewMode.ReadOnly, _data_detailcouse_view.el_u0_course_list);

                            DropDownList ddlm0_training_group_idx_ref = (DropDownList)fvCreateCourse.FindControl("ddlm0_training_group_idx_ref");
                            Label lbl_m0_training_group_idx_ref = (Label)fvCreateCourse.FindControl("lbl_m0_training_group_idx_ref");
                            DropDownList ddm0_training_branch_idx = (DropDownList)fvCreateCourse.FindControl("ddm0_training_branch_idx");
                            Label lbl_m0_training_branch_idx_ref = (Label)fvCreateCourse.FindControl("lbl_m0_training_branch_idx_ref");

                            DropDownList ddllevel_code = (DropDownList)fvCreateCourse.FindControl("ddllevel_code");
                            GridView GvM0_EmpTarget = (GridView)fvCreateCourse.FindControl("GvM0_EmpTarget");
                            CheckBoxList chkPositionEmpTarget = (CheckBoxList)fvCreateCourse.FindControl("chkPositionEmpTarget");

                            DropDownList ddlOrg_search = (DropDownList)fvCreateCourse.FindControl("ddlOrg_search");
                            DropDownList ddlDept_search = (DropDownList)fvCreateCourse.FindControl("ddlDept_search");
                            DropDownList ddlSec_search = (DropDownList)fvCreateCourse.FindControl("ddlSec_search");


                            CheckBoxList chkPositionEmp = (CheckBoxList)fvCreateCourse.FindControl("chkPositionEmpTarget");
                            CheckBoxList chkCoursePass = (CheckBoxList)fvCreateCourse.FindControl("chkCoursePass");

                            CheckBox cbcourse_type_etraining = (CheckBox)fvCreateCourse.FindControl("cbcourse_type_etraining");
                            CheckBox cbcourse_type_elearning = (CheckBox)fvCreateCourse.FindControl("cbcourse_type_elearning");

                            Label lbl_course_type_etraining = (Label)fvCreateCourse.FindControl("lbl_course_type_etraining");
                            Label lbl_course_type_elearning = (Label)fvCreateCourse.FindControl("lbl_course_type_elearning");

                            Label lbl_u0_course_idx_ref_pass_value = (Label)fvCreateCourse.FindControl("lbl_u0_course_idx_ref_pass_value");
                            Label lbl_TIDX_ref_value = (Label)fvCreateCourse.FindControl("lbl_TIDX_ref_value");
                            GridView GvDeptTraningList = (GridView)fvCreateCourse.FindControl("GvDeptTraningList");

                            Label lbl_level_code = (Label)fvCreateCourse.FindControl("lbl_level_code");

                            TextBox txt_course_no = (TextBox)fvCreateCourse.FindControl("txt_course_no");
                            //View File In Detail Car
                            var btnViewFileLogo = (HyperLink)fvCreateCourse.FindControl("btnViewFileLogo");
                            //var btnViewFileRoom_ = (Image)e.Row.FindControl("btnViewFileRoom");

                            //select view Logo
                            string filePath_View = ConfigurationManager.AppSettings["path_file_logocouse"];
                            string directoryName = txt_course_no.Text;//lbl_place_name_file.Text.Trim() + lbl_room_name_en_view_file.Text + lbl_m0_room_idx_file.Text;

                            if (Directory.Exists(Server.MapPath(filePath_View + directoryName)))
                            {
                                string[] filesPath = Directory.GetFiles(Server.MapPath(filePath_View + directoryName));
                                List<ListItem> files = new List<ListItem>();
                                foreach (string path in filesPath)
                                {
                                    string getfiles = "";
                                    getfiles = Path.GetFileName(path);
                                    btnViewFileLogo.NavigateUrl = filePath_View + directoryName + "/" + getfiles;

                                    HtmlImage img = new HtmlImage();
                                    img.Src = filePath_View + directoryName + "/" + getfiles;
                                    img.Height = 50;


                                    btnViewFileLogo.Controls.Add(img);



                                }
                                btnViewFileLogo.Visible = true;
                            }
                            else
                            {
                                btnViewFileLogo.Visible = false;

                            }
                            //select view Logo

                            getGettraininggroup(ddlm0_training_group_idx_ref);
                            ddlm0_training_group_idx_ref.SelectedValue = lbl_m0_training_group_idx_ref.Text;

                            getGettrainingbranch(ddm0_training_branch_idx, int.Parse(ddlm0_training_group_idx_ref.SelectedValue));
                            ddm0_training_branch_idx.SelectedValue = lbl_m0_training_branch_idx_ref.Text;

                            getGettraininglevel(ddllevel_code);
                            ddllevel_code.SelectedItem.Text = lbl_level_code.Text;

                            select_positiongroup(chkPositionEmpTarget);
                            getCoursePass(chkCoursePass, int.Parse(ddlm0_training_group_idx_ref.SelectedValue), int.Parse(ddm0_training_branch_idx.SelectedValue), uidx);

                            if (lbl_course_type_etraining.Text == "1")//check ClassRoom
                            {
                                cbcourse_type_etraining.Checked = true;
                            }

                            if (lbl_course_type_elearning.Text == "1")//check E-Learning
                            {
                                cbcourse_type_elearning.Checked = true;
                            }

                            //check pass couse
                            string[] setcheck_passcouse = lbl_u0_course_idx_ref_pass_value.Text.Split(',');
                            foreach (ListItem chkListSet in chkCoursePass.Items)
                            {

                                if (Array.IndexOf(setcheck_passcouse, chkListSet.Value) > -1)
                                {
                                    chkListSet.Selected = true;
                                }
                                else
                                {
                                    chkListSet.Selected = false;
                                    //item.Value = "0";

                                }

                            }
                            //check pass couse

                            //check level emp
                            string[] setcheck_levelemp = lbl_TIDX_ref_value.Text.Split(',');
                            foreach (ListItem chkList_level in chkPositionEmpTarget.Items)
                            {

                                if (Array.IndexOf(setcheck_levelemp, chkList_level.Value) > -1)
                                {
                                    chkList_level.Selected = true;
                                }
                                else
                                {
                                    chkList_level.Selected = false;
                                    //item.Value = "0";

                                }

                            }

                            //check level emp

                            //detail sec can in couse
                            getDetailCousePerSec(GvDeptTraningList, uidx, "viewdata");


                            break;

                        case 2: //edit detail

                            setFormData(fvCreateCourse, FormViewMode.Edit, _data_detailcouse_view.el_u0_course_list);

                            DropDownList ddlm0_training_group_idx_ref_ = (DropDownList)fvCreateCourse.FindControl("ddlm0_training_group_idx_ref");
                            Label lbl_m0_training_group_idx_ref_ = (Label)fvCreateCourse.FindControl("lbl_m0_training_group_idx_ref");
                            DropDownList ddm0_training_branch_idx_ = (DropDownList)fvCreateCourse.FindControl("ddm0_training_branch_idx");
                            Label lbl_m0_training_branch_idx_ref_ = (Label)fvCreateCourse.FindControl("lbl_m0_training_branch_idx_ref");

                            DropDownList ddllevel_code_ = (DropDownList)fvCreateCourse.FindControl("ddllevel_code");
                            GridView GvM0_EmpTarget_ = (GridView)fvCreateCourse.FindControl("GvM0_EmpTarget");
                            CheckBoxList chkPositionEmpTarget_ = (CheckBoxList)fvCreateCourse.FindControl("chkPositionEmpTarget");

                            DropDownList ddlOrg_search_ = (DropDownList)fvCreateCourse.FindControl("ddlOrg_search");
                            DropDownList ddlDept_search_ = (DropDownList)fvCreateCourse.FindControl("ddlDept_search");
                            DropDownList ddlSec_search_ = (DropDownList)fvCreateCourse.FindControl("ddlSec_search");
                            DropDownList ddlPositionEmp_search_ = (DropDownList)fvCreateCourse.FindControl("ddlPositionEmp_search");


                            CheckBoxList chkPositionEmp_ = (CheckBoxList)fvCreateCourse.FindControl("chkPositionEmpTarget");
                            CheckBoxList chkCoursePass_ = (CheckBoxList)fvCreateCourse.FindControl("chkCoursePass");

                            CheckBox cbcourse_type_etraining_ = (CheckBox)fvCreateCourse.FindControl("cbcourse_type_etraining");
                            CheckBox cbcourse_type_elearning_ = (CheckBox)fvCreateCourse.FindControl("cbcourse_type_elearning");

                            Label lbl_course_type_etraining_ = (Label)fvCreateCourse.FindControl("lbl_course_type_etraining");
                            Label lbl_course_type_elearning_ = (Label)fvCreateCourse.FindControl("lbl_course_type_elearning");

                            Label lbl_u0_course_idx_ref_pass_value_ = (Label)fvCreateCourse.FindControl("lbl_u0_course_idx_ref_pass_value");
                            Label lbl_TIDX_ref_value_ = (Label)fvCreateCourse.FindControl("lbl_TIDX_ref_value");
                            GridView GvDeptTraningList_ = (GridView)fvCreateCourse.FindControl("GvDeptTraningList");
                            Label lbl_level_code_ = (Label)fvCreateCourse.FindControl("lbl_level_code");

                            TextBox txt_course_no_ = (TextBox)fvCreateCourse.FindControl("txt_course_no");
                            //View File In Detail 
                            var btnViewFileLogo_ = (HyperLink)fvCreateCourse.FindControl("btnViewFileLogo");

                            //select view Logo
                            string filePath_View_ = ConfigurationManager.AppSettings["path_file_logocouse"];
                            string directoryName_ = txt_course_no_.Text;//lbl_place_name_file.Text.Trim() + lbl_room_name_en_view_file.Text + lbl_m0_room_idx_file.Text;

                            if (Directory.Exists(Server.MapPath(filePath_View_ + directoryName_)))
                            {
                                string[] filesPath_ = Directory.GetFiles(Server.MapPath(filePath_View_ + directoryName_));
                                List<ListItem> files = new List<ListItem>();
                                foreach (string path in filesPath_)
                                {
                                    string getfiles = "";
                                    getfiles = Path.GetFileName(path);
                                    btnViewFileLogo_.NavigateUrl = filePath_View_ + directoryName_ + "/" + getfiles;

                                    HtmlImage img = new HtmlImage();
                                    img.Src = filePath_View_ + directoryName_ + "/" + getfiles;
                                    img.Height = 50;


                                    btnViewFileLogo_.Controls.Add(img);



                                }
                                btnViewFileLogo_.Visible = true;
                            }
                            else
                            {
                                btnViewFileLogo_.Visible = false;

                            }
                            //select view Logo


                            //search 
                            getOrganizationList(ddlOrg_search_);
                            getDepartmentList(ddlDept_search_, int.Parse(ddlOrg_search_.SelectedValue));
                            getSectionList(ddlSec_search_, int.Parse(ddlOrg_search_.SelectedValue), int.Parse(ddlDept_search_.SelectedValue));
                            getpositiongroup(ddlPositionEmp_search_);
                            //search



                            getGettraininggroup(ddlm0_training_group_idx_ref_);
                            ddlm0_training_group_idx_ref_.SelectedValue = lbl_m0_training_group_idx_ref_.Text;

                            getGettrainingbranch(ddm0_training_branch_idx_, int.Parse(ddlm0_training_group_idx_ref_.SelectedValue));
                            ddm0_training_branch_idx_.SelectedValue = lbl_m0_training_branch_idx_ref_.Text;

                            getGettraininglevel(ddllevel_code_);
                            ddllevel_code_.SelectedItem.Text = lbl_level_code_.Text;

                            select_positiongroup(chkPositionEmpTarget_);
                            getCoursePass(chkCoursePass_, int.Parse(ddlm0_training_group_idx_ref_.SelectedValue), int.Parse(ddm0_training_branch_idx_.SelectedValue), uidx);

                            if (lbl_course_type_etraining_.Text == "1")//check ClassRoom
                            {
                                cbcourse_type_etraining_.Checked = true;
                            }

                            if (lbl_course_type_elearning_.Text == "1")//check E-Learning
                            {
                                cbcourse_type_elearning_.Checked = true;
                            }

                            //check pass couse
                            string[] setcheck_passcouse_ = lbl_u0_course_idx_ref_pass_value_.Text.Split(',');
                            foreach (ListItem chkListSet in chkCoursePass_.Items)
                            {

                                if (Array.IndexOf(setcheck_passcouse_, chkListSet.Value) > -1)
                                {
                                    chkListSet.Selected = true;
                                }
                                else
                                {
                                    chkListSet.Selected = false;
                                    //item.Value = "0";

                                }

                            }
                            //check pass couse

                            //check level emp
                            string[] setcheck_levelemp_ = lbl_TIDX_ref_value_.Text.Split(',');
                            foreach (ListItem chkList_level in chkPositionEmpTarget_.Items)
                            {

                                if (Array.IndexOf(setcheck_levelemp_, chkList_level.Value) > -1)
                                {
                                    chkList_level.Selected = true;
                                }
                                else
                                {
                                    chkList_level.Selected = false;
                                    //item.Value = "0";

                                }

                            }

                            //check level emp

                            //detail sec can in couse
                            getDetailCousePerSec(GvDeptTraningList_, uidx, "editdata");





                            break;
                    }
                }



                break;
            case "docCreateQuiz":


                setFormData(fvdetail_createquiz, FormViewMode.ReadOnly, ((data_employee)ViewState["vsEmpProfile"]).employee_list);
                setFormData(fvcreate_quiz, FormViewMode.Insert, null);

                LinkButton btnAdddataset_createquiz = (LinkButton)fvcreate_quiz.FindControl("btnAdddataset_createquiz");

                linkBtnTrigger(btnAdddataset_createquiz);


                break;

            case "docViewQuiz":
                select_getquiz(GvQuiz);

                break;

            case "docViewDetailQuiz":
                getEmployeeProfile_U2Create(int.Parse(ViewState["CEmpIDX_U2"].ToString()));
                setFormData(fvdetailcreateuser, FormViewMode.ReadOnly, ((data_employee)ViewState["U2_CEmpIDX_Create"]).employee_list);
                select_typequiz_list_Head(GvHeadTypeAns, int.Parse(ViewState["u0_course_idx_U2"].ToString()));
                select_typequiz_list_Show(GvTypeAns, int.Parse(ViewState["u0_course_idx_U2"].ToString()));
                lblcourse_name_head.Text = ViewState["course_name_Head"].ToString();

                break;


            case "docCreateAssessmentForm":


                break;
            case "docDetailCouse":

                //detail index
                //detail index

                getddlYear(ddlSearchYear);

                ddlSearchMonth.SelectedValue = DateTime.Now.Month.ToString();
                GvDetailCouse.Visible = true;
                getDetailCouse(GvDetailCouse, 0);
                // litDebug.Text = DateTime.Now.Month.ToString();

                break;


        }
    }

    protected void setActiveTab(string activeTab, int uidx, int nodeidx, int staidx, int actor_idx, int _chk_tab, int _cemp_idx, int u1idx)
    {
        setActiveView(activeTab, uidx, nodeidx, staidx, actor_idx, _chk_tab, _cemp_idx, u1idx);
        switch (activeTab)
        {


            case "docCreatecourse":
                li0.Attributes.Add("class", "active");
                _divMenuLiToViewQuiz_Master.Attributes.Add("class", "");
                _divMenuLiToCreateQuiz.Attributes.Add("class", "");
                _divMenuLiToViewQuiz.Attributes.Add("class", "");

                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                liListData.Attributes.Add("class", "");
                liInsert.Attributes.Add("class", "");
                liSearchTrnNeedsSurvey.Attributes.Add("class", "");
                liSearchTrnNeedsSurveyDEPT.Attributes.Add("class", "");

                break;
            case "docCreateQuiz":
                li0.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                _divMenuLiToViewQuiz_Master.Attributes.Add("class", "active");
                _divMenuLiToCreateQuiz.Attributes.Add("class", "active");
                _divMenuLiToViewQuiz.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                liListData.Attributes.Add("class", "");
                liInsert.Attributes.Add("class", "");
                liSearchTrnNeedsSurvey.Attributes.Add("class", "");
                liSearchTrnNeedsSurveyDEPT.Attributes.Add("class", "");

                CleardataSetFormList(GvDatasetCreate_Quiz);
                GvDatasetCreate_Quiz.Visible = false;
                LinkButton btnAdddataset_createquiz = (LinkButton)fvcreate_quiz.FindControl("btnAdddataset_createquiz");
                linkBtnTrigger(btnAdddataset_createquiz);

                ViewState["TypeInsert_addon"] = "1";


                break;

            case "docViewQuiz":
            case "docViewDetailQuiz":

                li0.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                _divMenuLiToViewQuiz_Master.Attributes.Add("class", "active");
                _divMenuLiToCreateQuiz.Attributes.Add("class", "");
                _divMenuLiToViewQuiz.Attributes.Add("class", "active");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                liListData.Attributes.Add("class", "");
                liInsert.Attributes.Add("class", "");
                liSearchTrnNeedsSurvey.Attributes.Add("class", "");
                liSearchTrnNeedsSurveyDEPT.Attributes.Add("class", "");

                break;

            case "docCreateAssessmentForm":
                li0.Attributes.Add("class", "");
                _divMenuLiToViewQuiz_Master.Attributes.Add("class", "");
                _divMenuLiToCreateQuiz.Attributes.Add("class", "");
                _divMenuLiToViewQuiz.Attributes.Add("class", "");
                ////li2.Attributes.Add("class", "active");
                li2.Attributes.Add("class", "active");
                li3.Attributes.Add("class", "");

                liListData.Attributes.Add("class", "");
                liInsert.Attributes.Add("class", "");
                liSearchTrnNeedsSurvey.Attributes.Add("class", "");
                liSearchTrnNeedsSurveyDEPT.Attributes.Add("class", "");

                break;
            case "docDetailCouse":
                li0.Attributes.Add("class", "");
                _divMenuLiToViewQuiz_Master.Attributes.Add("class", "");
                _divMenuLiToCreateQuiz.Attributes.Add("class", "");
                _divMenuLiToViewQuiz.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "active");
                liListData.Attributes.Add("class", "");
                liInsert.Attributes.Add("class", "");
                liSearchTrnNeedsSurvey.Attributes.Add("class", "");
                liSearchTrnNeedsSurveyDEPT.Attributes.Add("class", "");

                break;

        }
    }


    #endregion reuse


    /// /////////////////////////// TOEI
    #region bind data

    protected void getGettraininggroup(DropDownList ddlName)
    {
        data_elearning _data_elearning = new data_elearning();
        _data_elearning.trainingLoolup_action = new trainingLoolup[1];
        trainingLoolup _training_group = new trainingLoolup();

        _training_group.m0_training_group_idx = 0;

        _data_elearning.trainingLoolup_action[0] = _training_group;

        _data_elearning = CallServicePostElearning(_urlGetELM0Group, _data_elearning);
        setDdlData(ddlName, _data_elearning.trainingLoolup_action, "training_group_name", "m0_training_group_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือก --", "0"));
    }

    protected void getGettrainingbranch(DropDownList ddlName, int group_idx)
    {
        data_elearning _data_elearning = new data_elearning();
        _data_elearning.trainingLoolup_action = new trainingLoolup[1];
        trainingLoolup _training_group = new trainingLoolup();

        _training_group.m0_training_group_idx = group_idx;

        _data_elearning.trainingLoolup_action[0] = _training_group;

        _data_elearning = CallServicePostElearning(_urlGetELM0branch, _data_elearning);
        setDdlData(ddlName, _data_elearning.trainingLoolup_action, "training_branch_name", "m0_training_branch_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือก --", "0"));
    }

    protected void getCoursePass(CheckBoxList chkName, int group_idx, int branch_idx, int _u0idx)
    {
        data_elearning _data_elearning = new data_elearning();
        _data_elearning.el_u0_course_list = new el_u0_course_detail[1];
        el_u0_course_detail _training_group = new el_u0_course_detail();

        _training_group.m0_training_group_idx_ref = group_idx;
        _training_group.m0_training_branch_idx_ref = branch_idx;
        _training_group.u0_course_idx = _u0idx;

        _data_elearning.el_u0_course_list[0] = _training_group;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
        _data_elearning = CallServicePostElearning(_urlGetELPassCouse, _data_elearning);

        setChkData(chkName, _data_elearning.el_u0_course_list, "course_name", "u0_course_idx");
        //setDdlData(ddlName, _data_elearning.trainingLoolup_action, "training_branch_name", "m0_training_branch_idx");
        //ddlName.Items.Insert(0, new ListItem("-- เลือก --", "0"));



    }

    protected void getGettraininglevel(DropDownList ddlName)
    {
        data_elearning _data_elearning = new data_elearning();
        _data_elearning.trainingLoolup_action = new trainingLoolup[1];
        trainingLoolup _training_group = new trainingLoolup();

        _data_elearning.trainingLoolup_action[0] = _training_group;

        _data_elearning = CallServicePostElearning(_urlGetELM0Level, _data_elearning);
        setDdlData(ddlName, _data_elearning.trainingLoolup_action, "level_code", "m0_level_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือก --", "0"));
    }

    protected void getOrganizationList(DropDownList ddlName)
    {
        _dataEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dataEmployee.organization_list[0] = _orgList;

        _dataEmployee = callServicePostEmployee(_urlGetOrganizationList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือกองค์กร --", "0"));
    }

    protected void getDepartmentList(DropDownList ddlName, int _org_idx)
    {
        data_employee _dataEmployee_rdept = new data_employee();
        _dataEmployee_rdept.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dataEmployee_rdept.department_list[0] = _deptList;

        _dataEmployee_rdept = callServicePostEmployee(_urlGetDepartmentList, _dataEmployee_rdept);
        setDdlData(ddlName, _dataEmployee_rdept.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือกฝ่าย --", "0"));
    }

    protected void getSectionList(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {
        ddlName.Items.Clear();
        _dataEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _dataEmployee.section_list[0] = _secList;

        _dataEmployee = callServicePostEmployee(_urlGetSectionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.section_list, "sec_name_th", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือกแผนก --", "0"));
        //ddlName.SelectedValue = _rdept_idx.ToString();

    }

    protected void select_positiongroup(CheckBoxList chkName)
    {
        _dtpmform = new data_tpm_form();
        _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
        tpmu0_DocFormDetail dataselect = new tpmu0_DocFormDetail();

        dataselect.condition = 5;

        _dtpmform.Boxtpmu0_DocFormDetail[0] = dataselect;

        _dtpmform = callServicePostTPMForm(_urlSelectMaster, _dtpmform);

        //setGridData(gvName, _dtpmform.Boxtpmm0_DocFormDetail);

        setChkData(chkName, _dtpmform.Boxtpmm0_DocFormDetail, "pos_name", "posidx");
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));
        ////setDdlData(ddlName, _dtpmform.Boxtpmm0_DocFormDetail, "pos_name", "posidx");
        ////ddlName.Items.Insert(0, new ListItem("เลือกประเภทกลุ่มพนักงาน...", "0"));
    }

    protected void getpositiongroup(DropDownList ddlName)
    {
        _dtpmform = new data_tpm_form();
        _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
        tpmu0_DocFormDetail dataselect = new tpmu0_DocFormDetail();

        dataselect.condition = 5;

        _dtpmform.Boxtpmu0_DocFormDetail[0] = dataselect;

        _dtpmform = callServicePostTPMForm(_urlSelectMaster, _dtpmform);

        //setGridData(gvName, _dtpmform.Boxtpmm0_DocFormDetail);

        //setChkData(chkName, _dtpmform.Boxtpmm0_DocFormDetail, "pos_name", "posidx");
        setDdlData(ddlName, _dtpmform.Boxtpmm0_DocFormDetail, "pos_name", "posidx");
        ddlName.Items.Insert(0, new ListItem("-- เลือกระดับพนักงาน --", "0"));


        //
        ////setDdlData(ddlName, _dtpmform.Boxtpmm0_DocFormDetail, "pos_name", "posidx");
        ////ddlName.Items.Insert(0, new ListItem("เลือกประเภทกลุ่มพนักงาน...", "0"));
    }

    protected void getDetailCouse(GridView gvName, int _u0_course_idx)
    {

        DropDownList ddlSearchMonth = (DropDownList)_PanelSearchDetail.FindControl("ddlSearchMonth");
        DropDownList ddlSearchYear = (DropDownList)_PanelSearchDetail.FindControl("ddlSearchYear");

        data_elearning _data_detailcouse = new data_elearning();

        _data_detailcouse.el_u0_course_list = new el_u0_course_detail[1];
        el_u0_course_detail datael_u0_course = new el_u0_course_detail();

        datael_u0_course.u0_course_idx = _u0_course_idx;

        _data_detailcouse.el_u0_course_list[0] = datael_u0_course;

        _data_detailcouse = CallServicePostElearning(_urlGetELGetDetailCouse, _data_detailcouse);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_detailcouse));
        ViewState["vs_DetailCouse_DataSearch"] = _data_detailcouse.el_u0_course_list;
        //ViewState["vs_DetailCouse"] = _data_detailcouse.el_u0_course_list;


        el_u0_course_detail[] _value_datasearch = (el_u0_course_detail[])ViewState["vs_DetailCouse_DataSearch"];

        var _linq_datasearch = (from data in _value_datasearch
                                where
                                data.course_date_month == int.Parse(ddlSearchMonth.SelectedValue)
                                && data.course_date_year == int.Parse(ddlSearchYear.SelectedValue)

                                select data).Distinct().ToList();

        ViewState["vs_DetailCouse"] = _linq_datasearch.ToList();
        setGridData(gvName, ViewState["vs_DetailCouse"]);





    }

    protected void getDetailCousePerSec(GridView gvName, int _u0_course_idx, string _condition_data)
    {

        data_elearning _data_detailcouse_per = new data_elearning();

        _data_detailcouse_per.el_u0_course_list = new el_u0_course_detail[1];
        el_u0_course_detail datael_u0_course_persec = new el_u0_course_detail();

        datael_u0_course_persec.u0_course_idx = _u0_course_idx;

        _data_detailcouse_per.el_u0_course_list[0] = datael_u0_course_persec;

        _data_detailcouse_per = CallServicePostElearning(_urlGetELGetDetailCousePerSec, _data_detailcouse_per);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_detailcouse_per));

        if (_data_detailcouse_per.return_code == 0)
        {
            if (_condition_data == "editdata")
            {
                foreach (var per_edit in _data_detailcouse_per.el_u0_course_list)
                {
                    if (ViewState["vsDetailCousePerListUpdate"] != null)
                    {
                        CultureInfo culture = new CultureInfo("en-US");
                        Thread.CurrentThread.CurrentCulture = culture;
                        DataSet dsContacts = (DataSet)ViewState["vsDetailCousePerListUpdate"];
                        DataRow drContacts = dsContacts.Tables["dsDetailCousePerTableUpdate"].NewRow();

                        drContacts["drorg_name_thUpdate"] = per_edit.org_name_th;
                        drContacts["drorg_idx_refIDXUpdate"] = per_edit.org_idx_ref;
                        drContacts["drdept_name_thUpdate"] = per_edit.dept_name_th;
                        drContacts["drRDeptID_refIDXUpdate"] = per_edit.RDeptID_ref;
                        drContacts["drsec_name_thUpdate"] = per_edit.sec_name_th;
                        drContacts["drRSecID_refIDXUpdate"] = per_edit.RSecID_ref;
                        drContacts["drpos_nameUpdate"] = per_edit.pos_name;
                        drContacts["drTIDX_refUpdate"] = per_edit.TIDX_ref;


                        dsContacts.Tables["dsDetailCousePerTableUpdate"].Rows.Add(drContacts);
                        ViewState["vsDetailCousePerListUpdate"] = dsContacts;
                        setGridData(gvName, dsContacts.Tables["dsDetailCousePerTableUpdate"]);

                        gvName.Visible = true;
                    }
                }
            }
            else
            {
                setGridData(gvName, _data_detailcouse_per.el_u0_course_list);
            }
        }

    }

    protected void getddlYear(DropDownList ddlName)
    {

        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Clear();

        var currentYear = DateTime.Today.Year;
        for (int i = 5; i >= 0; i--)
        {
            ddlName.Items.Add((currentYear - i).ToString());
        }
        ddlName.Items.Insert(0, new ListItem("-- เลือก --", "0"));
        ddlName.Items.FindByValue(Convert.ToString(currentYear)).Selected = true;
    }


    #endregion bind data

    #region clear data set 
    protected void ClearDataDeptCouseList()
    {

        GridView GvDeptTraningList = (GridView)fvCreateCourse.FindControl("GvDeptTraningList");

        //clear set equipment list
        ViewState["vsDepartment"] = null;
        GvDeptTraningList.DataSource = ViewState["vsDepartment"];
        GvDeptTraningList.DataBind();
        GvDeptTraningList.Visible = false;

        getDeptCouseList();

    }

    protected void CleardataSetPlaceListUpdate()
    {
        var GvDeptTraningList = (GridView)fvCreateCourse.FindControl("GvDeptTraningList");
        ViewState["vsDetailCousePerListUpdate"] = null;
        GvDeptTraningList.DataSource = ViewState["vsDetailCousePerListUpdate"];
        GvDeptTraningList.DataBind();
        getPerDetailCouseListUpdate();
    }

    #endregion clear data set

    #region set data set 
    protected void setDeptCouseList()
    {
        if (ViewState["vsDepartment"] != null)
        {

            DropDownList ddlOrg_search = (DropDownList)fvCreateCourse.FindControl("ddlOrg_search");
            DropDownList ddlDept_search = (DropDownList)fvCreateCourse.FindControl("ddlDept_search");
            DropDownList ddlSec_search = (DropDownList)fvCreateCourse.FindControl("ddlSec_search");
            DropDownList ddlPositionEmp_search = (DropDownList)fvCreateCourse.FindControl("ddlPositionEmp_search");


            CheckBoxList chkPositionEmp = (CheckBoxList)fvCreateCourse.FindControl("chkPositionEmp");

            GridView GvDeptTraningList = (GridView)fvCreateCourse.FindControl("GvDeptTraningList");

            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts = (DataSet)ViewState["vsDepartment"];


            // check box selected
            int count_ = 0;
            string Action_detail = "";
            string Action_detail_idx = "";

            List<String> AddoingList_ = new List<string>();
            foreach (ListItem chkList_ in chkPositionEmp.Items)
            {
                if (chkList_.Selected)
                {

                    Action_detail += chkList_.Text + ',';
                    Action_detail_idx += chkList_.Value + ',';

                    count_ = count_ + 1;

                    count_++;
                }
            }


            foreach (DataRow dr in dsContacts.Tables["dsDepartmentListTable"].Rows)
            {
                if (dr["drOrgIDX"].ToString() == ddlOrg_search.SelectedValue
                    && dr["drRDeptIDX"].ToString() == ddlDept_search.SelectedValue
                    && dr["drRSecIDX"].ToString() == ddlSec_search.SelectedValue
                    && dr["drPosIDX"].ToString() == ddlPositionEmp_search.SelectedValue)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- Check Data Again ---');", true);
                    return;
                }
            }



            DataRow drContacts = dsContacts.Tables["dsDepartmentListTable"].NewRow();

            drContacts["drOrgIDX"] = ddlOrg_search.SelectedValue;
            drContacts["drOrgNameENText"] = ddlOrg_search.SelectedItem.Text;
            drContacts["drRDeptIDX"] = ddlDept_search.SelectedValue;
            drContacts["drDeptNameENText"] = ddlDept_search.SelectedItem.Text;
            drContacts["drRSecIDX"] = ddlSec_search.SelectedValue;
            drContacts["drSecNameENText"] = ddlSec_search.SelectedItem.Text;
            drContacts["drPositionEmpIDX"] = Action_detail_idx.ToString();
            drContacts["drPositionEmpText"] = Action_detail.ToString();

            drContacts["drPosIDX"] = ddlPositionEmp_search.SelectedValue;
            drContacts["drPosNameText"] = ddlPositionEmp_search.SelectedItem.Text;

            dsContacts.Tables["dsDepartmentListTable"].Rows.Add(drContacts);
            ViewState["vsDepartment"] = dsContacts;


            setGridData(GvDeptTraningList, dsContacts.Tables["dsDepartmentListTable"]);
            GvDeptTraningList.Visible = true;
        }
    }

    protected void setPerDetailCouseListUpdate()
    {
        if (ViewState["vsDetailCousePerListUpdate"] != null)
        {
            DropDownList ddlOrg_search = (DropDownList)fvCreateCourse.FindControl("ddlOrg_search");
            DropDownList ddlDept_search = (DropDownList)fvCreateCourse.FindControl("ddlDept_search");
            DropDownList ddlSec_search = (DropDownList)fvCreateCourse.FindControl("ddlSec_search");
            DropDownList ddlPositionEmp_search = (DropDownList)fvCreateCourse.FindControl("ddlPositionEmp_search");
            GridView GvDeptTraningList = (GridView)fvCreateCourse.FindControl("GvDeptTraningList");

            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;
            DataSet dsContacts = (DataSet)ViewState["vsDetailCousePerListUpdate"];
            foreach (DataRow dr in dsContacts.Tables["dsDetailCousePerTableUpdate"].Rows)
            {
                if (dr["drorg_idx_refIDXUpdate"].ToString() == ddlOrg_search.SelectedValue &&
                    dr["drRDeptID_refIDXUpdate"].ToString() == ddlDept_search.SelectedValue &&
                    dr["drRSecID_refIDXUpdate"].ToString() == ddlSec_search.SelectedValue &&
                    dr["drTIDX_refUpdate"].ToString() == ddlPositionEmp_search.SelectedValue
                    )
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--Check Data--');", true);
                    return;
                }
            }
            DataRow drContacts = dsContacts.Tables["dsDetailCousePerTableUpdate"].NewRow();
            //drContacts["drPlaceIdxUpdate"] = ddl_PlaceidxUpdate.SelectedValue;
            drContacts["drorg_name_thUpdate"] = ddlOrg_search.SelectedItem.Text;
            drContacts["drorg_idx_refIDXUpdate"] = ddlOrg_search.SelectedValue;
            drContacts["drdept_name_thUpdate"] = ddlDept_search.SelectedItem.Text;
            drContacts["drRDeptID_refIDXUpdate"] = ddlDept_search.SelectedValue;
            drContacts["drsec_name_thUpdate"] = ddlSec_search.SelectedItem.Text;
            drContacts["drRSecID_refIDXUpdate"] = ddlSec_search.SelectedValue;
            drContacts["drpos_nameUpdate"] = ddlPositionEmp_search.SelectedItem.Text;
            drContacts["drTIDX_refUpdate"] = ddlPositionEmp_search.SelectedValue;

            dsContacts.Tables["dsDetailCousePerTableUpdate"].Rows.Add(drContacts);
            ViewState["vsDetailCousePerListUpdate"] = dsContacts;
            setGridData(GvDeptTraningList, dsContacts.Tables["dsDetailCousePerTableUpdate"]);
            GvDeptTraningList.Visible = true;
        }
    }

    #endregion set data set


    #region get dataset


    protected void getPerDetailCouseListUpdate()
    {
        //ViewState["vsFoodMaterialList"] = null;
        DataSet dsPerDetailCouseListUpdate = new DataSet();
        dsPerDetailCouseListUpdate.Tables.Add("dsDetailCousePerTableUpdate");
        dsPerDetailCouseListUpdate.Tables["dsDetailCousePerTableUpdate"].Columns.Add("drorg_name_thUpdate", typeof(String));
        dsPerDetailCouseListUpdate.Tables["dsDetailCousePerTableUpdate"].Columns.Add("drorg_idx_refIDXUpdate", typeof(String));
        dsPerDetailCouseListUpdate.Tables["dsDetailCousePerTableUpdate"].Columns.Add("drdept_name_thUpdate", typeof(String));
        dsPerDetailCouseListUpdate.Tables["dsDetailCousePerTableUpdate"].Columns.Add("drRDeptID_refIDXUpdate", typeof(String));
        dsPerDetailCouseListUpdate.Tables["dsDetailCousePerTableUpdate"].Columns.Add("drsec_name_thUpdate", typeof(String));
        dsPerDetailCouseListUpdate.Tables["dsDetailCousePerTableUpdate"].Columns.Add("drRSecID_refIDXUpdate", typeof(String));
        dsPerDetailCouseListUpdate.Tables["dsDetailCousePerTableUpdate"].Columns.Add("drpos_nameUpdate", typeof(String));
        dsPerDetailCouseListUpdate.Tables["dsDetailCousePerTableUpdate"].Columns.Add("drTIDX_refUpdate", typeof(String));

        ViewState["vsDetailCousePerListUpdate"] = dsPerDetailCouseListUpdate;
    }

    protected void getDeptCouseList()
    {
        #region View-Department data table
        var ds_department_data = new DataSet();
        ds_department_data.Tables.Add("dsDepartmentListTable");
        ds_department_data.Tables[0].Columns.Add("drOrgIDX", typeof(int));
        ds_department_data.Tables[0].Columns.Add("drOrgNameENText", typeof(String));
        ds_department_data.Tables[0].Columns.Add("drRDeptIDX", typeof(int));
        ds_department_data.Tables[0].Columns.Add("drDeptNameENText", typeof(String));
        ds_department_data.Tables[0].Columns.Add("drRSecIDX", typeof(int));
        ds_department_data.Tables[0].Columns.Add("drSecNameENText", typeof(String));
        ds_department_data.Tables[0].Columns.Add("drPositionEmpIDX", typeof(String));
        ds_department_data.Tables[0].Columns.Add("drPositionEmpText", typeof(String));
        ds_department_data.Tables[0].Columns.Add("drPosIDX", typeof(int));
        ds_department_data.Tables[0].Columns.Add("drPosNameText", typeof(String));
        ViewState["vsDepartment"] = ds_department_data;

        ViewState["Depart_RSecIDX"] = "";
        #endregion
    }

    #endregion get dataset


    #region DropDownList ddl
    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddltypeanswer_createquiz = (DropDownList)fvcreate_quiz.FindControl("ddltypeanswer_createquiz");
        Control div_choice = (Control)fvcreate_quiz.FindControl("div_choice");
        Control div_match = (Control)fvcreate_quiz.FindControl("div_match");
        LinkButton btnAdddataset_createquiz = (LinkButton)fvcreate_quiz.FindControl("btnAdddataset_createquiz");
        TextBox txtweight = ((TextBox)fvcreate_quiz.FindControl("txtweight"));
        TextBox txtcourseitem_createquiz = (TextBox)fvcreate_quiz.FindControl("txtcourseitem_createquiz");


        if (sender is DropDownList)
        {
            DropDownList ddlName = (DropDownList)sender;

            DropDownList ddlOrg_search = (DropDownList)fvCreateCourse.FindControl("ddlOrg_search");
            DropDownList ddlDept_search = (DropDownList)fvCreateCourse.FindControl("ddlDept_search");
            DropDownList ddlSec_search = (DropDownList)fvCreateCourse.FindControl("ddlSec_search");

            DropDownList ddlm0_training_group_idx_ref = (DropDownList)fvCreateCourse.FindControl("ddlm0_training_group_idx_ref");
            DropDownList ddm0_training_branch_idx = (DropDownList)fvCreateCourse.FindControl("ddm0_training_branch_idx");
            CheckBoxList chkCoursePass = (CheckBoxList)fvCreateCourse.FindControl("chkCoursePass");

            HiddenField hfu0_course_idx = (HiddenField)fvCreateCourse.FindControl("hfu0_course_idx");

            switch (ddlName.ID)
            {

                case "ddlm0_training_group_idx_ref":

                    getGettrainingbranch(ddm0_training_branch_idx, int.Parse(ddlm0_training_group_idx_ref.SelectedValue));

                    break;
                case "ddm0_training_branch_idx":
                    getCoursePass(chkCoursePass, int.Parse(ddlm0_training_group_idx_ref.SelectedValue), int.Parse(ddm0_training_branch_idx.SelectedValue), int.Parse(hfu0_course_idx.Value));

                    break;

                case "ddlOrg_search":

                    getDepartmentList(ddlDept_search, int.Parse(ddlOrg_search.SelectedValue));

                    ddlSec_search.Items.Clear();
                    ddlSec_search.Items.Insert(0, new ListItem("-- เลือกแผนก --", "0"));

                    break;
                case "ddlDept_search":

                    getSectionList(ddlSec_search, int.Parse(ddlOrg_search.SelectedItem.Value), int.Parse(ddlDept_search.SelectedItem.Value));

                    //ddlrposCreate.Items.Clear();
                    //ddlrposCreate.Items.Insert(0, new ListItem("-- เลือกตำแหน่ง --", "0"));

                    break;

                case "ddltypeanswer_createquiz":
                    if (ddltypeanswer_createquiz.SelectedValue == "1")
                    {
                        div_choice.Visible = true;
                        div_match.Visible = false;
                    }
                    else if (ddltypeanswer_createquiz.SelectedValue == "2")
                    {
                        div_choice.Visible = false;
                        div_match.Visible = false;

                    }
                    else if (ddltypeanswer_createquiz.SelectedValue == "3")
                    {
                        div_choice.Visible = false;
                        div_match.Visible = true;

                    }
                    else
                    {
                        div_choice.Visible = false;
                        div_match.Visible = false;

                    }
                    txtweight.Enabled = true;

                    //linkBtnTrigger(btnAdddataset_choice);
                    //linkBtnTrigger(btnAdddataset_write);

                    break;

            }

        }

        if (sender is TextBox)
        {
            TextBox txtName = (TextBox)sender;



            switch (txtName.ID)
            {
                case "txtcourseitem_createquiz":

                    data_elearning _dtelerning = new data_elearning();

                    _dtelerning.Boxu0_QuizDocument = new U0_QuizDocument[1];
                    U0_QuizDocument _select = new U0_QuizDocument();

                    _select.condition = 7;
                    _select.u0_course_idx = int.Parse(ddlcourse_createquiz.SelectedValue);
                    _select.m0_tqidx = int.Parse(ddltypeanswer_createquiz.SelectedValue);
                    _select.course_item = int.Parse(txtcourseitem_createquiz.Text);

                    _dtelerning.Boxu0_QuizDocument[0] = _select;
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtelerning));

                    _dtelerning = CallServicePostElearning(_urlGetMasterQuiz, _dtelerning);
                    //litDebug.Text = _dtelerning.return_code.ToString();

                    if (_dtelerning.return_code == 0)
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีลำดับคำถามนี้อยู่แล้ว กรุณาเลือกลำดับอื่น!!!');", true);
                        txtcourseitem_createquiz.Focus();
                        txtcourseitem_createquiz.Text = String.Empty;
                    }


                    linkBtnTrigger(btnAdddataset_createquiz);
                    setGridData(GvDatasetCreate_Quiz, (DataSet)ViewState["vsBuyequipment"]);



                    break;

            }
        }
    }
    #endregion DropDownList


    /// ///////////////////////////
    /// 

    #region BONUS
    #region select
    protected void selectcourse_quiz(DropDownList ddlName)
    {
        data_elearning _dtelerning = new data_elearning();

        _dtelerning.Boxu0_QuizDocument = new U0_QuizDocument[1];
        U0_QuizDocument _select = new U0_QuizDocument();

        _select.condition = 1;

        _dtelerning.Boxu0_QuizDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtelerning = CallServicePostElearning(_urlGetMasterQuiz, _dtelerning);

        setDdlData(ddlName, _dtelerning.Boxu0_QuizDocument, "course_name", "u0_course_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกคอร์สอบรม...", "0"));

    }

    protected void selecttypeanswer_quiz(DropDownList ddlName)
    {
        data_elearning _dtelerning = new data_elearning();

        _dtelerning.Boxu0_QuizDocument = new U0_QuizDocument[1];
        U0_QuizDocument _select = new U0_QuizDocument();

        _select.condition = 2;

        _dtelerning.Boxu0_QuizDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtelerning = CallServicePostElearning(_urlGetMasterQuiz, _dtelerning);

        setDdlData(ddlName, _dtelerning.Boxu0_QuizDocument, "type_quiz", "m0_tqidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกประเภทคำตอบ...", "0"));

    }

    protected void select_typequiz_list_Head(GridView gvName, int u0_course_idx)
    {
        data_elearning _dtelerning = new data_elearning();

        _dtelerning.Boxu0_QuizDocument = new U0_QuizDocument[1];
        U0_QuizDocument _select = new U0_QuizDocument();

        _select.condition = 4;
        _select.u0_course_idx = u0_course_idx;
        //_select.u2_main_course_idx = u2_main_course_idx;

        _dtelerning.Boxu0_QuizDocument[0] = _select;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtelerning));
        _dtelerning = CallServicePostElearning(_urlGetMasterQuiz, _dtelerning);


        ViewState["u2_main_course_idx_Addon"] = _dtelerning.Boxm0_QuizDocument[0].u2_main_course_idx.ToString();
        setGridData(gvName, _dtelerning.Boxm0_QuizDocument);
        ViewState["course_name_Head"] = _dtelerning.Boxm0_QuizDocument[0].course_name.ToString();
    }

    protected void select_typequiz_list_Show(GridView gvName, int u0_course_idx)
    {
        data_elearning _dtelerning = new data_elearning();

        _dtelerning.Boxu0_QuizDocument = new U0_QuizDocument[1];
        U0_QuizDocument _select = new U0_QuizDocument();

        _select.condition = 4;
        _select.u0_course_idx = u0_course_idx;

        _dtelerning.Boxu0_QuizDocument[0] = _select;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtelerning));
        _dtelerning = CallServicePostElearning(_urlGetMasterQuiz, _dtelerning);


        //ViewState["u2_main_course_idx_Addon"] = _dtelerning.Boxm0_QuizDocument[0].u2_main_course_idx.ToString();
        setGridData(gvName, _dtelerning.Boxm0_QuizDocument);
        //ViewState["course_name_Head"] = _dtelerning.Boxm0_QuizDocument[0].course_name.ToString();
    }



    protected void select_getquestion(GridView gvName, int m0_tqidx, int u2_main_course_idx, int u0_course_idx)
    {
        data_elearning _dtelerning = new data_elearning();

        _dtelerning.Boxu0_QuizDocument = new U0_QuizDocument[1];
        U0_QuizDocument _select = new U0_QuizDocument();

        _select.condition = 5;
        _select.m0_tqidx = m0_tqidx;
        _select.u2_main_course_idx = u2_main_course_idx;
        _select.u0_course_idx = u0_course_idx;

        _dtelerning.Boxu0_QuizDocument[0] = _select;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtelerning));

        _dtelerning = CallServicePostElearning(_urlGetMasterQuiz, _dtelerning);
        setGridData(gvName, _dtelerning.Boxm1_QuizDocument);
    }

    protected void select_getquiz(GridView gvName)
    {
        data_elearning _dtelerning = new data_elearning();

        _dtelerning.Boxu0_QuizDocument = new U0_QuizDocument[1];
        U0_QuizDocument _select = new U0_QuizDocument();

        _select.condition = 6;

        _dtelerning.Boxu0_QuizDocument[0] = _select;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtelerning));

        _dtelerning = CallServicePostElearning(_urlGetMasterQuiz, _dtelerning);
        setGridData(gvName, _dtelerning.el_u0_course_list);
    }
    #endregion

    #region Insert
    protected void InsertQuestion()
    {

        HyperLink images_q = (HyperLink)fvcreate_quiz.FindControl("images_q");
        Label lbchoice1 = (Label)fvcreate_quiz.FindControl("lbchoice1");
        HyperLink images_1 = (HyperLink)fvcreate_quiz.FindControl("images_1");
        Label lbchoice2 = (Label)fvcreate_quiz.FindControl("lbchoice2");
        HyperLink images_2 = (HyperLink)fvcreate_quiz.FindControl("images_2");
        Label lbchoice3 = (Label)fvcreate_quiz.FindControl("lbchoice3");
        HyperLink images_3 = (HyperLink)fvcreate_quiz.FindControl("images_3");
        Label lbchoice4 = (Label)fvcreate_quiz.FindControl("lbchoice4");
        HyperLink images_4 = (HyperLink)fvcreate_quiz.FindControl("images_4");
        Label lbchoice_ansidx = (Label)fvcreate_quiz.FindControl("lbchoice_ansidx");
        HyperLink images_ans_match = (HyperLink)fvcreate_quiz.FindControl("images_ans_match");
        Label lbchoice_ans = (Label)fvcreate_quiz.FindControl("lbchoice_ans");


        _data_elearning = new data_elearning();
        int i = 0;
        int m0_tqidx = 0;
        var ds_udoc2_insert = (DataSet)ViewState["vsBuyequipment"];
        var _document2 = new M1_QuizDocument[ds_udoc2_insert.Tables[0].Rows.Count];
        var _document2_main = new M0_QuizDocument[ds_udoc2_insert.Tables[0].Rows.Count];

        _data_elearning.Boxu0_QuizDocument = new U0_QuizDocument[1];
        U0_QuizDocument _insert = new U0_QuizDocument();

        _insert.condition = 1;
        _insert.CEmpIDX = emp_idx;
        _insert.u2_main_course_idx = int.Parse(ViewState["u2_main_course_idx_addon"].ToString());

        _data_elearning.Boxu0_QuizDocument[0] = _insert;


        foreach (DataRow dtrow in ds_udoc2_insert.Tables[0].Rows)
        {

            if (m0_tqidx != int.Parse(dtrow["m0_tqidx"].ToString()) && int.Parse(dtrow["m0_tqidx"].ToString()) != 0)
            {
                _document2_main[i] = new M0_QuizDocument();

                _document2_main[i].u0_course_idx = int.Parse(ddlcourse_createquiz.SelectedValue);
                _document2_main[i].m0_tqidx = int.Parse(dtrow["m0_tqidx"].ToString());
                _document2_main[i].weight_score = dtrow["weight_score"].ToString();

                _data_elearning.Boxm0_QuizDocument = _document2_main;

                m0_tqidx = int.Parse(dtrow["m0_tqidx"].ToString());


            }

            _document2[i] = new M1_QuizDocument();

            _document2[i].u0_course_idx = int.Parse(ddlcourse_createquiz.SelectedValue);
            _document2[i].m0_tqidx = int.Parse(dtrow["m0_tqidx"].ToString());
            _document2[i].no_choice = int.Parse(dtrow["No_Quest"].ToString());
            _document2[i].quest_name = dtrow["Question"].ToString();
            _document2[i].choice_a = dtrow["Choice_a"].ToString();
            _document2[i].choice_b = dtrow["Choice_b"].ToString();
            _document2[i].choice_c = dtrow["Choice_c"].ToString();
            _document2[i].choice_d = dtrow["Choice_d"].ToString();




            if (dtrow["Choice_ansIDX"].ToString() == "" && dtrow["m0_tqidx"].ToString() != "3" || dtrow["m0_tqidx"].ToString() == "3" && dtrow["Choice_ans"].ToString() == "")
            {
                _document2[i].choice_ans = "0";

            }
            else if (dtrow["m0_tqidx"].ToString() == "3" && dtrow["Choice_ans"].ToString() != "")
            {
                _document2[i].choice_ans = dtrow["Choice_ans"].ToString();

            }
            else
            {
                _document2[i].choice_ans = dtrow["Choice_ansIDX"].ToString();

            }

            if (images_q != null)
            {
                _document2[i].pic_quest = 1;
            }


            if (images_1 != null)
            {
                _document2[i].pic_a = 1;
            }

            if (images_2 != null)
            {
                _document2[i].pic_b = 1;
            }


            if (images_3 != null)
            {
                _document2[i].pic_c = 1;
            }

            if (images_4 != null)
            {
                _document2[i].pic_d = 1;
            }
            if (images_ans_match != null)
            {
                _document2[i].pic_ans = 1;
            }

            i++;
            _data_elearning.Boxm1_QuizDocument = _document2;
        }

        //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
        _data_elearning = CallServicePostElearning(_urlCreateQuiz, _data_elearning);
    }

    protected void UpdateMasterQuiz(string weight_score, int u2_mainidx)
    {
        _data_elearning.Boxu0_QuizDocument = new U0_QuizDocument[1];
        U0_QuizDocument _insert = new U0_QuizDocument();

        _insert.condition = 1;
        _insert.CEmpIDX = emp_idx;

        _data_elearning.Boxu0_QuizDocument[0] = _insert;

        _data_elearning.Boxm0_QuizDocument = new M0_QuizDocument[1];
        M0_QuizDocument _insertm0 = new M0_QuizDocument();

        _insertm0.weight_score = weight_score;
        _insertm0.u2_main_course_idx = u2_mainidx;

        _data_elearning.Boxm0_QuizDocument[0] = _insertm0;


        //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
        _data_elearning = CallServicePostElearning(_urlUpdateQuiz, _data_elearning);

    }

    protected void DeleteMasterQuiz(int u2_mainidx, int status)
    {
        _data_elearning.Boxu0_QuizDocument = new U0_QuizDocument[1];
        U0_QuizDocument _insert = new U0_QuizDocument();

        _insert.condition = 1;
        _insert.CEmpIDX = emp_idx;
        _insert.u2_main_course_idx = u2_mainidx;
        _insert.status = status;
        _data_elearning.Boxu0_QuizDocument[0] = _insert;


        //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
        _data_elearning = CallServicePostElearning(_urlDeleteQuiz, _data_elearning);

    }

    protected void DeleteMasterDetailQuiz(int u2_course_idx, int status)
    {
        _data_elearning.Boxu0_QuizDocument = new U0_QuizDocument[1];
        U0_QuizDocument _insert = new U0_QuizDocument();

        _insert.condition = 2;
        _insert.CEmpIDX = emp_idx;
        _insert.u2_course_idx = u2_course_idx;
        _insert.status = status;
        _data_elearning.Boxu0_QuizDocument[0] = _insert;


        //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
        _data_elearning = CallServicePostElearning(_urlDeleteQuiz, _data_elearning);

    }

    #endregion

    #region SetDefault Form

    protected void SetViewState_Form()
    {

        DataSet dsFoodMaterialList = new DataSet();
        dsFoodMaterialList.Tables.Add("dsAddListTable");

        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("No_Quest", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("m0_tqidx", typeof(int));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("TypeAnswer", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("weight_score", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("Question", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("Choice_a", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("Choice_b", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("Choice_c", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("Choice_d", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("Choice_ans", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("Choice_ansIDX", typeof(int));


        ViewState["vsBuyequipment"] = dsFoodMaterialList;

    }

    protected void setAddList_Form(TextBox no, DropDownList TypeAnswer, TextBox weight_score, TextBox Question, TextBox Choice_a, TextBox Choice_b, TextBox Choice_c, TextBox Choice_d, DropDownList Choice_ans, GridView gvName, TextBox Answer_match)
    {

        if (ViewState["vsBuyequipment"] != null)
        {
            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts = (DataSet)ViewState["vsBuyequipment"];

            foreach (DataRow dr in dsContacts.Tables["dsAddListTable"].Rows)
            {
                if (dr["No_Quest"].ToString() == no.Text && int.Parse(dr["m0_tqidx"].ToString()) == int.Parse(TypeAnswer.SelectedValue))
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลลำดับนี้อยู่แล้ว!!!');", true);

                    return;
                }
                if (dr["No_Quest"].ToString() == no.Text &&
                     int.Parse(dr["m0_tqidx"].ToString()) == int.Parse(TypeAnswer.SelectedValue) &&
                   dr["Question"].ToString() == Question.Text &&
                   dr["Choice_a"].ToString() == Choice_a.Text &&
                   dr["Choice_b"].ToString() == Choice_b.Text &&
                   dr["Choice_c"].ToString() == Choice_c.Text &&
                   dr["Choice_d"].ToString() == Choice_d.Text &&
                   int.Parse(dr["Choice_ansIDX"].ToString()) == int.Parse(Choice_ans.SelectedValue))
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว!!!');", true);

                    return;
                }
            }

            DataRow drContacts = dsContacts.Tables["dsAddListTable"].NewRow();

            drContacts["No_Quest"] = no.Text;
            drContacts["m0_tqidx"] = int.Parse(TypeAnswer.SelectedValue);
            drContacts["TypeAnswer"] = TypeAnswer.SelectedItem.Text;
            drContacts["weight_score"] = weight_score.Text;
            drContacts["Question"] = Question.Text;
            drContacts["Choice_a"] = Choice_a.Text;
            drContacts["Choice_b"] = Choice_b.Text;
            drContacts["Choice_c"] = Choice_c.Text;
            drContacts["Choice_d"] = Choice_d.Text;

            if (TypeAnswer.SelectedValue == "1")
            {
                drContacts["Choice_ans"] = Choice_ans.SelectedItem.Text;
                drContacts["Choice_ansIDX"] = int.Parse(Choice_ans.SelectedValue);
            }
            else if (TypeAnswer.SelectedValue == "2")
            {
                drContacts["Choice_ans"] = "";
                drContacts["Choice_ansIDX"] = int.Parse(Choice_ans.SelectedValue);
            }
            else if (TypeAnswer.SelectedValue == "3")
            {
                drContacts["Choice_ans"] = Answer_match.Text;
            }


            dsContacts.Tables["dsAddListTable"].Rows.Add(drContacts);
            ViewState["vsBuyequipment"] = dsContacts;
            setGridData(gvName, dsContacts.Tables["dsAddListTable"]);
            gvName.Visible = true;

        }

    }

    protected void CleardataSetFormList(GridView GvName)
    {
        ViewState["vsBuyequipment"] = null;
        GvName.DataSource = ViewState["vsBuyequipment"];
        GvName.DataBind();
        SetViewState_Form();
    }

    public void SearchDirectories(DirectoryInfo dir, String target, String no_quest)
    {

        string dirfiles = dir.ToString();

        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        DataRow dr = dt.NewRow();
        DataColumn dc = new DataColumn("FileName", typeof(string));
        dc = new DataColumn("Download", typeof(string));
        dt.Columns.Add("FileName");
        dt.Columns.Add("Download");

        try
        {
            FileInfo[] files = dir.GetFiles();
            int i = 0;
            foreach (FileInfo file in files)
            {
                if (file.Name == "q.jpg" && target == "q")
                {

                    string[] f = Directory.GetFiles(dirfiles, "q.jpg", SearchOption.AllDirectories);
                    dt.Rows.Add(file.Name);
                    dt.Rows[i][1] = f[0];
                    i++;
                    ViewState["path_q"] = "1";
                }
                else if (file.Name == "1.jpg" && target == "1")
                {

                    string[] f = Directory.GetFiles(dirfiles, "1.jpg", SearchOption.AllDirectories);
                    dt.Rows.Add(file.Name);
                    dt.Rows[i][1] = f[0];
                    i++;
                    ViewState["path_1"] = "1";
                }
                else if (file.Name == "2.jpg" && target == "2")
                {

                    string[] f = Directory.GetFiles(dirfiles, "2.jpg", SearchOption.AllDirectories);
                    dt.Rows.Add(file.Name);
                    dt.Rows[i][1] = f[0];
                    i++;
                    ViewState["path_2"] = "1";
                }
                else if (file.Name == "3.jpg" && target == "3")
                {

                    string[] f = Directory.GetFiles(dirfiles, "3.jpg", SearchOption.AllDirectories);
                    dt.Rows.Add(file.Name);
                    dt.Rows[i][1] = f[0];
                    i++;
                    ViewState["path_3"] = "1";
                }
                else if (file.Name == "4.jpg" && target == "4")
                {

                    string[] f = Directory.GetFiles(dirfiles, "4.jpg", SearchOption.AllDirectories);
                    dt.Rows.Add(file.Name);
                    dt.Rows[i][1] = f[0];
                    i++;
                    ViewState["path_4"] = "1";
                }
                else if (file.Name == "ans_" + no_quest + ".jpg" && target == "ans_" + no_quest)
                {

                    string[] f = Directory.GetFiles(dirfiles, "ans_" + no_quest + ".jpg", SearchOption.AllDirectories);
                    dt.Rows.Add(file.Name);
                    dt.Rows[i][1] = f[0];
                    i++;
                    ViewState["path_ans_match"] = "1";
                }

            }


        }
        catch (Exception ex)
        {
            // txt.Text = ex.ToString();
            ViewState["path"] = "0";
        }
    }

    #endregion

    #region GridView
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvDatasetCreate_Quiz":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbcourse_item = (Label)e.Row.Cells[0].FindControl("lbcourse_item");
                    Label lbm0_tqidx = (Label)e.Row.Cells[0].FindControl("lbm0_tqidx");


                    TextBox txtcourseitem_createquiz = ((TextBox)fvcreate_quiz.FindControl("txtcourseitem_createquiz"));
                    DropDownList ddltypeanswer_createquiz = ((DropDownList)fvcreate_quiz.FindControl("ddltypeanswer_createquiz"));
                    DropDownList ddlcourse_createquiz = ((DropDownList)fvcreate_quiz.FindControl("ddlcourse_createquiz"));


                    var images_q = (HyperLink)e.Row.FindControl("images_q");
                    var images_1 = (HyperLink)e.Row.FindControl("images_1");
                    var images_2 = (HyperLink)e.Row.FindControl("images_2");
                    var images_3 = (HyperLink)e.Row.FindControl("images_3");
                    var images_4 = (HyperLink)e.Row.FindControl("images_4");
                    var images_ans_match = (HyperLink)e.Row.FindControl("images_ans_match");


                    string getPathfile = ConfigurationManager.AppSettings["path_flie_elearning"];
                    string fileName_upload = ddlcourse_createquiz.SelectedValue + "/" + lbm0_tqidx.Text + "/" + lbcourse_item.Text;// + "/" + "q.jpg"; 
                    string filePath_upload = Server.MapPath(getPathfile + fileName_upload);
                    string pic = getPathfile + fileName_upload + "/q.jpg";

                    DirectoryInfo myDirLotus = new DirectoryInfo(filePath_upload);
                    SearchDirectories(myDirLotus, "q", lbcourse_item.Text);

                    if (ViewState["path_q"].ToString() != "0")
                    {
                        images_q.Visible = true;
                        images_q.NavigateUrl = pic;

                        HtmlImage img = new HtmlImage();
                        img.Src = pic;
                        img.Height = 100;
                        images_q.Controls.Add(img);
                    }
                    else
                    {
                        images_q.Visible = false;
                    }
                    ViewState["path_q"] = "0";


                    string pic1 = getPathfile + fileName_upload + "/1.jpg";
                    DirectoryInfo myDirLotus1 = new DirectoryInfo(filePath_upload);
                    SearchDirectories(myDirLotus1, "1", lbcourse_item.Text);

                    if (ViewState["path_1"].ToString() != "0")
                    {
                        images_1.Visible = true;
                        images_1.NavigateUrl = pic1;

                        HtmlImage img1 = new HtmlImage();
                        img1.Src = pic1;
                        img1.Height = 100;
                        images_1.Controls.Add(img1);
                    }
                    else
                    {
                        images_1.Visible = false;
                    }

                    ViewState["path_1"] = "0";

                    string pic2 = getPathfile + fileName_upload + "/2.jpg";
                    DirectoryInfo myDirLotus2 = new DirectoryInfo(filePath_upload);
                    SearchDirectories(myDirLotus2, "2", lbcourse_item.Text);
                    if (ViewState["path_2"].ToString() != "0")
                    {
                        images_2.Visible = true;
                        images_2.NavigateUrl = pic2;

                        HtmlImage img2 = new HtmlImage();
                        img2.Src = pic2;
                        img2.Height = 100;
                        images_2.Controls.Add(img2);
                    }
                    else
                    {
                        images_2.Visible = false;
                    }
                    ViewState["path_2"] = "0";

                    string pic3 = getPathfile + fileName_upload + "/3.jpg";
                    DirectoryInfo myDirLotus3 = new DirectoryInfo(filePath_upload);
                    SearchDirectories(myDirLotus3, "3", lbcourse_item.Text);

                    if (ViewState["path_3"].ToString() != "0")
                    {
                        images_3.Visible = true;
                        images_3.NavigateUrl = pic3;

                        HtmlImage img3 = new HtmlImage();
                        img3.Src = pic3;
                        img3.Height = 100;
                        images_3.Controls.Add(img3);
                    }
                    else
                    {
                        images_3.Visible = false;
                    }
                    ViewState["path_3"] = "0";

                    string pic4 = getPathfile + fileName_upload + "/4.jpg";
                    DirectoryInfo myDirLotus4 = new DirectoryInfo(filePath_upload);
                    SearchDirectories(myDirLotus4, "4", lbcourse_item.Text);

                    if (ViewState["path_4"].ToString() != "0")
                    {
                        images_4.Visible = true;
                        images_4.NavigateUrl = pic4;

                        HtmlImage img4 = new HtmlImage();
                        img4.Src = pic4;
                        img4.Height = 100;
                        images_4.Controls.Add(img4);
                    }
                    else
                    {
                        images_4.Visible = false;
                    }
                    ViewState["path_4"] = "0";

                    string pic_ansmatch = getPathfile + fileName_upload + "/ans_" + lbcourse_item.Text + ".jpg";
                    DirectoryInfo myDirLotus_ans_match = new DirectoryInfo(filePath_upload);
                    SearchDirectories(myDirLotus_ans_match, "ans_" + lbcourse_item.Text, lbcourse_item.Text);

                    if (ViewState["path_ans_match"].ToString() != "0")
                    {
                        images_ans_match.Visible = true;
                        images_ans_match.NavigateUrl = pic_ansmatch;

                        HtmlImage img_ans_match = new HtmlImage();
                        img_ans_match.Src = pic_ansmatch;
                        img_ans_match.Height = 100;
                        images_ans_match.Controls.Add(img_ans_match);
                    }
                    else
                    {
                        images_ans_match.Visible = false;
                    }
                    ViewState["path_ans_match"] = "0";

                }

                break;

            case "GvTypeAns":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblm0_tqidx = (Label)e.Row.Cells[0].FindControl("lblm0_tqidx");
                    Label lblu2_main_course_idx = (Label)e.Row.Cells[0].FindControl("lblu2_main_course_idx");
                    Label lbu0_course_idx_type = (Label)e.Row.Cells[0].FindControl("lbu0_course_idx_type");
                    GridView GvQuestion = (GridView)e.Row.Cells[0].FindControl("GvQuestion");

                    select_getquestion(GvQuestion, int.Parse(lblm0_tqidx.Text), int.Parse(lblu2_main_course_idx.Text), int.Parse(lbu0_course_idx_type.Text));

                    if (lblm0_tqidx.Text == "2")
                    {
                        GvQuestion.Columns[2].Visible = false;
                        GvQuestion.Columns[3].Visible = false;
                        GvQuestion.Columns[4].Visible = false;
                        GvQuestion.Columns[5].Visible = false;
                        GvQuestion.Columns[6].Visible = false;
                    }
                    else if (lblm0_tqidx.Text == "3")
                    {
                        GvQuestion.Columns[2].Visible = false;
                        GvQuestion.Columns[3].Visible = false;
                        GvQuestion.Columns[4].Visible = false;
                        GvQuestion.Columns[5].Visible = false;
                    }

                    if (ViewState["set_edit"].ToString() == "1")
                    {
                        GvQuestion.Columns[7].Visible = false;
                    }
                    else
                    {
                        GvQuestion.Columns[7].Visible = true;

                    }
                }
                break;

            case "GvQuestion":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbm0_tqidx = (Label)e.Row.Cells[0].FindControl("lbm0_tqidx");
                    Label lblu2_main_course_idx = (Label)e.Row.Cells[0].FindControl("lblu2_main_course_idx");
                    Label lbu0_course_idx = (Label)e.Row.Cells[0].FindControl("lbu0_course_idx");
                    Label lbcourse_item = (Label)e.Row.Cells[0].FindControl("lbcourse_item");
                    Label lblchoice_ans = (Label)e.Row.Cells[6].FindControl("lblchoice_ans");

                    var images_q = (HyperLink)e.Row.FindControl("images_q");
                    var images_1 = (HyperLink)e.Row.FindControl("images_1");
                    var images_2 = (HyperLink)e.Row.FindControl("images_2");
                    var images_3 = (HyperLink)e.Row.FindControl("images_3");
                    var images_4 = (HyperLink)e.Row.FindControl("images_4");
                    var images_ans_match = (HyperLink)e.Row.FindControl("images_ans_match");




                    if (lbm0_tqidx.Text == "1")
                    {
                        if (lblchoice_ans.Text == "1")
                        {
                            lblchoice_ans.Text = "ก";
                        }
                        else if (lblchoice_ans.Text == "2")
                        {
                            lblchoice_ans.Text = "ข";
                        }
                        else if (lblchoice_ans.Text == "3")
                        {
                            lblchoice_ans.Text = "ค";
                        }
                        else if (lblchoice_ans.Text == "4")
                        {
                            lblchoice_ans.Text = "ง";
                        }
                    }

                    string getPathfile = ConfigurationManager.AppSettings["path_flie_elearning"];
                    string fileName_upload = lbu0_course_idx.Text + "/" + lbm0_tqidx.Text + "/" + lbcourse_item.Text;// + "/" + "q.jpg"; 
                    string filePath_upload = Server.MapPath(getPathfile + fileName_upload);
                    string pic = getPathfile + fileName_upload + "/q.jpg";

                    DirectoryInfo myDirLotus = new DirectoryInfo(filePath_upload);
                    SearchDirectories(myDirLotus, "q", lbcourse_item.Text);

                    if (ViewState["path_q"].ToString() != "0")
                    {
                        images_q.Visible = true;
                        images_q.NavigateUrl = pic;

                        HtmlImage img = new HtmlImage();
                        img.Src = pic;
                        img.Height = 100;
                        images_q.Controls.Add(img);
                    }
                    else
                    {
                        images_q.Visible = false;
                    }
                    ViewState["path_q"] = "0";


                    string pic1 = getPathfile + fileName_upload + "/1.jpg";
                    DirectoryInfo myDirLotus1 = new DirectoryInfo(filePath_upload);
                    SearchDirectories(myDirLotus1, "1", lbcourse_item.Text);

                    if (ViewState["path_1"].ToString() != "0")
                    {
                        images_1.Visible = true;
                        images_1.NavigateUrl = pic1;

                        HtmlImage img1 = new HtmlImage();
                        img1.Src = pic1;
                        img1.Height = 100;
                        images_1.Controls.Add(img1);
                    }
                    else
                    {
                        images_1.Visible = false;
                    }

                    ViewState["path_1"] = "0";

                    string pic2 = getPathfile + fileName_upload + "/2.jpg";
                    DirectoryInfo myDirLotus2 = new DirectoryInfo(filePath_upload);
                    SearchDirectories(myDirLotus2, "2", lbcourse_item.Text);
                    if (ViewState["path_2"].ToString() != "0")
                    {
                        images_2.Visible = true;
                        images_2.NavigateUrl = pic2;

                        HtmlImage img2 = new HtmlImage();
                        img2.Src = pic2;
                        img2.Height = 100;
                        images_2.Controls.Add(img2);
                    }
                    else
                    {
                        images_2.Visible = false;
                    }
                    ViewState["path_2"] = "0";

                    string pic3 = getPathfile + fileName_upload + "/3.jpg";
                    DirectoryInfo myDirLotus3 = new DirectoryInfo(filePath_upload);
                    SearchDirectories(myDirLotus3, "3", lbcourse_item.Text);

                    if (ViewState["path_3"].ToString() != "0")
                    {
                        images_3.Visible = true;
                        images_3.NavigateUrl = pic3;

                        HtmlImage img3 = new HtmlImage();
                        img3.Src = pic3;
                        img3.Height = 100;
                        images_3.Controls.Add(img3);
                    }
                    else
                    {
                        images_3.Visible = false;
                    }
                    ViewState["path_3"] = "0";

                    string pic4 = getPathfile + fileName_upload + "/4.jpg";
                    DirectoryInfo myDirLotus4 = new DirectoryInfo(filePath_upload);
                    SearchDirectories(myDirLotus4, "4", lbcourse_item.Text);

                    if (ViewState["path_4"].ToString() != "0")
                    {
                        images_4.Visible = true;
                        images_4.NavigateUrl = pic4;

                        HtmlImage img4 = new HtmlImage();
                        img4.Src = pic4;
                        img4.Height = 100;
                        images_4.Controls.Add(img4);
                    }
                    else
                    {
                        images_4.Visible = false;
                    }
                    ViewState["path_4"] = "0";

                    string pic_ansmatch = getPathfile + fileName_upload + "/ans_" + lbcourse_item.Text + ".jpg";
                    DirectoryInfo myDirLotus_ans_match = new DirectoryInfo(filePath_upload);
                    SearchDirectories(myDirLotus_ans_match, "ans_" + lbcourse_item.Text, lbcourse_item.Text);

                    if (ViewState["path_ans_match"].ToString() != "0")
                    {
                        images_ans_match.Visible = true;
                        images_ans_match.NavigateUrl = pic_ansmatch;

                        HtmlImage img_ans_match = new HtmlImage();
                        img_ans_match.Src = pic_ansmatch;
                        img_ans_match.Height = 100;
                        images_ans_match.Controls.Add(img_ans_match);
                    }
                    else
                    {
                        images_ans_match.Visible = false;
                    }
                    ViewState["path_ans_match"] = "0";


                }
                break;

            case "GvHeadTypeAns":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvHeadTypeAns.EditIndex != e.Row.RowIndex)
                    {
                        LinkButton BtnAddQuiz = (LinkButton)e.Row.Cells[3].FindControl("BtnAddQuiz");

                        linkBtnTrigger(BtnAddQuiz);

                        if (ViewState["set_edit"].ToString() == "1")
                        {
                            GvHeadTypeAns.Columns[3].Visible = false;
                        }
                        else
                        {
                            GvHeadTypeAns.Columns[3].Visible = true;

                        }
                    }
                }
                break;
            case "GvQuiz":


                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton btnViewQuiz = (LinkButton)e.Row.Cells[8].FindControl("btnViewQuiz");

                    linkBtnTrigger(btnViewQuiz);
                }
                break;
        }
    }

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvHeadTypeAns":
                GvHeadTypeAns.EditIndex = e.NewEditIndex;
                select_typequiz_list_Head(GvHeadTypeAns, int.Parse(ViewState["u0_course_idx_U2"].ToString()));//, int.Parse(ViewState["u2_main_course_idx_Addon"].ToString()));

                break;
        }
    }

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvHeadTypeAns":
                GvHeadTypeAns.EditIndex = -1;
                select_typequiz_list_Head(GvHeadTypeAns, int.Parse(ViewState["u0_course_idx_U2"].ToString()));
                break;

        }
    }

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvHeadTypeAns":
                string u2_main_course_idx = GvHeadTypeAns.DataKeys[e.RowIndex].Values[0].ToString();
                var txtweight_score = (TextBox)GvHeadTypeAns.Rows[e.RowIndex].FindControl("txtweight_score");

                GvHeadTypeAns.EditIndex = -1;
                UpdateMasterQuiz(txtweight_score.Text, int.Parse(u2_main_course_idx));

                select_typequiz_list_Head(GvHeadTypeAns, int.Parse(ViewState["u0_course_idx_U2"].ToString()));

                break;

        }
    }

    protected void onRowCommand_Quiz(Object sender, GridViewCommandEventArgs e)
    {
        if (sender is GridView)
        {
            string cmdName = e.CommandName;
            switch (cmdName)
            {
                case "bnDeleteList":
                    GridView GvDatasetCreate_Quiz = (GridView)fvcreate_quiz.FindControl("GvDatasetCreate_Quiz");
                    GridViewRow rowSelect = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    DropDownList ddlcourse_createquiz = (DropDownList)fvcreate_quiz.FindControl("ddlcourse_createquiz");
                    DropDownList ddltypeanswer_createquiz = (DropDownList)fvcreate_quiz.FindControl("ddltypeanswer_createquiz");
                    Control div_save = (Control)fvcreate_quiz.FindControl("div_save");

                    int rowselect = rowSelect.RowIndex;
                    int rowIndex;


                    foreach (GridViewRow row in GvDatasetCreate_Quiz.Rows)
                    {
                        rowIndex = row.RowIndex;
                        Label lbcourse_item = (Label)row.Cells[0].FindControl("lbcourse_item");

                        if (rowIndex == rowselect)
                        {

                            string getPathfile = ConfigurationManager.AppSettings["path_flie_elearning"];
                            string fileName_upload = ddlcourse_createquiz.SelectedValue + "/" + ddltypeanswer_createquiz.SelectedValue + "/" + lbcourse_item.Text; //rowIndex.ToString();
                            string filePath_upload = Server.MapPath(getPathfile + fileName_upload);


                            if (Directory.Exists(filePath_upload))
                            {

                                foreach (string file in Directory.GetFiles(filePath_upload))
                                {
                                    File.Delete(file);
                                }
                                Directory.Delete(filePath_upload);

                            }
                        }
                        rowIndex++;
                    }

                    DataSet dsContacts = (DataSet)ViewState["vsBuyequipment"];
                    dsContacts.Tables["dsAddListTable"].Rows[rowselect].Delete();
                    dsContacts.AcceptChanges();
                    setGridData(GvDatasetCreate_Quiz, dsContacts.Tables["dsAddListTable"]);


                    if (dsContacts.Tables["dsAddListTable"].Rows.Count < 1)
                    {
                        GvDatasetCreate_Quiz.Visible = false;
                        div_save.Visible = false;
                    }


                    break;


            }
        }
    }
    #endregion

    #region btnCommand_Quiz
    protected void btnCommand_Quiz(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        DropDownList ddlcourse_createquiz = ((DropDownList)fvcreate_quiz.FindControl("ddlcourse_createquiz"));
        DropDownList ddltypeanswer_createquiz = ((DropDownList)fvcreate_quiz.FindControl("ddltypeanswer_createquiz"));
        TextBox txtweight = ((TextBox)fvcreate_quiz.FindControl("txtweight"));
        TextBox txtcourseitem_createquiz = ((TextBox)fvcreate_quiz.FindControl("txtcourseitem_createquiz"));
        TextBox txtquest_createquiz = ((TextBox)fvcreate_quiz.FindControl("txtquest_createquiz"));
        TextBox txtchoice1 = ((TextBox)fvcreate_quiz.FindControl("txtchoice1"));
        TextBox txtchoice2 = ((TextBox)fvcreate_quiz.FindControl("txtchoice2"));
        TextBox txtchoice3 = ((TextBox)fvcreate_quiz.FindControl("txtchoice3"));
        TextBox txtchoice4 = ((TextBox)fvcreate_quiz.FindControl("txtchoice4"));
        DropDownList ddl_answer_createquiz = ((DropDownList)fvcreate_quiz.FindControl("ddl_answer_createquiz"));
        TextBox txtanswer = ((TextBox)fvcreate_quiz.FindControl("txtanswer"));
        Control div_save = ((Control)fvcreate_quiz.FindControl("div_save"));

        switch (cmdName)
        {

            case "CmdAdddataset":

                if (int.Parse(txtcourseitem_createquiz.Text) < 1)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาเลือกลำดับให้ถูกต้อง!!!');", true);
                    return;
                }
                else
                {
                    txtweight.Enabled = false;

                    if (imgupload_q.HasFile)
                    {

                        extension_q_el = Path.GetExtension(imgupload_q.FileName);
                        if (extension_q_el.ToLower() == ".jpg")
                        {
                            string getPathfile = ConfigurationManager.AppSettings["path_flie_elearning"];
                            string fileName_upload = ddlcourse_createquiz.SelectedValue + "/" + ddltypeanswer_createquiz.SelectedValue + "/" + txtcourseitem_createquiz.Text;
                            string filePath_upload = Server.MapPath(getPathfile + fileName_upload);

                            if (!Directory.Exists(filePath_upload))
                            {
                                Directory.CreateDirectory(filePath_upload);
                            }

                            imgupload_q.SaveAs(Server.MapPath(getPathfile + fileName_upload + "\\" + "q" + extension_q_el));
                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('เฉพาะรูปภาพนามสกุลไฟล์.jpg เท่านั้น');", true);
                        }

                    }

                    if (ddltypeanswer_createquiz.SelectedValue == "1")
                    {
                        if (imgupload_1.HasFile)
                        {
                            extension_1_el = Path.GetExtension(imgupload_1.FileName);

                            if (extension_1_el.ToLower() == ".jpg")
                            {
                                string getPathfile = ConfigurationManager.AppSettings["path_flie_elearning"];
                                string fileName_upload = ddlcourse_createquiz.SelectedValue + "/" + ddltypeanswer_createquiz.SelectedValue + "/" + txtcourseitem_createquiz.Text;
                                string filePath_upload = Server.MapPath(getPathfile + fileName_upload);

                                if (!Directory.Exists(filePath_upload))
                                {
                                    Directory.CreateDirectory(filePath_upload);
                                }

                                imgupload_1.SaveAs(Server.MapPath(getPathfile + fileName_upload + "\\" + "1" + extension_1_el));
                            }
                            else
                            {
                                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('เฉพาะรูปภาพนามสกุลไฟล์.jpg เท่านั้น');", true);
                            }
                        }

                        if (imgupload_2.HasFile)
                        {
                            extension_2_el = Path.GetExtension(imgupload_2.FileName);

                            if (extension_2_el.ToLower() == ".jpg")
                            {
                                string getPathfile = ConfigurationManager.AppSettings["path_flie_elearning"];
                                string fileName_upload = ddlcourse_createquiz.SelectedValue + "/" + ddltypeanswer_createquiz.SelectedValue + "/" + txtcourseitem_createquiz.Text;
                                string filePath_upload = Server.MapPath(getPathfile + fileName_upload);

                                if (!Directory.Exists(filePath_upload))
                                {
                                    Directory.CreateDirectory(filePath_upload);
                                }

                                imgupload_2.SaveAs(Server.MapPath(getPathfile + fileName_upload + "\\" + "2" + extension_2_el));
                            }
                            else
                            {
                                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('เฉพาะรูปภาพนามสกุลไฟล์.jpg เท่านั้น');", true);
                            }
                        }


                        if (imgupload_3.HasFile)
                        {
                            extension_3_el = Path.GetExtension(imgupload_3.FileName);

                            if (extension_3_el.ToLower() == ".jpg")
                            {
                                string getPathfile = ConfigurationManager.AppSettings["path_flie_elearning"];
                                string fileName_upload = ddlcourse_createquiz.SelectedValue + "/" + ddltypeanswer_createquiz.SelectedValue + "/" + txtcourseitem_createquiz.Text;
                                string filePath_upload = Server.MapPath(getPathfile + fileName_upload);

                                if (!Directory.Exists(filePath_upload))
                                {
                                    Directory.CreateDirectory(filePath_upload);
                                }

                                imgupload_3.SaveAs(Server.MapPath(getPathfile + fileName_upload + "\\" + "3" + extension_3_el));
                            }
                            else
                            {
                                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('เฉพาะรูปภาพนามสกุลไฟล์.jpg เท่านั้น');", true);
                            }
                        }


                        if (imgupload_4.HasFile)
                        {
                            extension_4_el = Path.GetExtension(imgupload_4.FileName);

                            if (extension_4_el.ToLower() == ".jpg")
                            {
                                string getPathfile = ConfigurationManager.AppSettings["path_flie_elearning"];
                                string fileName_upload = ddlcourse_createquiz.SelectedValue + "/" + ddltypeanswer_createquiz.SelectedValue + "/" + txtcourseitem_createquiz.Text;
                                string filePath_upload = Server.MapPath(getPathfile + fileName_upload);

                                if (!Directory.Exists(filePath_upload))
                                {
                                    Directory.CreateDirectory(filePath_upload);
                                }

                                imgupload_4.SaveAs(Server.MapPath(getPathfile + fileName_upload + "\\" + "4" + extension_4_el));
                            }
                            else
                            {
                                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('เฉพาะรูปภาพนามสกุลไฟล์.jpg เท่านั้น');", true);
                            }
                        }
                    }
                    else if (ddltypeanswer_createquiz.SelectedValue == "3")
                    {
                        if (imgupload_match.HasFile)
                        {
                            extension_ans_match = Path.GetExtension(imgupload_match.FileName);

                            if (extension_ans_match.ToLower() == ".jpg")
                            {
                                string getPathfile = ConfigurationManager.AppSettings["path_flie_elearning"];
                                string fileName_upload = ddlcourse_createquiz.SelectedValue + "/" + ddltypeanswer_createquiz.SelectedValue + "/" + txtcourseitem_createquiz.Text;
                                string filePath_upload = Server.MapPath(getPathfile + fileName_upload);

                                if (!Directory.Exists(filePath_upload))
                                {
                                    Directory.CreateDirectory(filePath_upload);
                                }

                                imgupload_match.SaveAs(Server.MapPath(getPathfile + fileName_upload + "\\" + "ans_" + txtcourseitem_createquiz.Text + extension_ans_match));
                            }
                            else
                            {
                                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('เฉพาะรูปภาพนามสกุลไฟล์.jpg เท่านั้น');", true);
                            }
                        }

                    }
                }
                setAddList_Form(txtcourseitem_createquiz, ddltypeanswer_createquiz, txtweight, txtquest_createquiz, txtchoice1, txtchoice2, txtchoice3, txtchoice4, ddl_answer_createquiz, GvDatasetCreate_Quiz, txtanswer);
                txtquest_createquiz.Text = String.Empty;
                txtchoice1.Text = String.Empty;
                txtchoice2.Text = String.Empty;
                txtchoice3.Text = String.Empty;
                txtchoice4.Text = String.Empty;
                ddl_answer_createquiz.SelectedValue = "0";
                div_save.Visible = true;
                ddlcourse_createquiz.Enabled = false;
                break;

            case "btnAdd":
                InsertQuestion();
                setActiveTab("docDetailCouse", 0, 0, 0, 0, 0, 0, 0);

                break;

            case "cmdViewDetailQuiz":
                string[] arg1_view = new string[5];
                arg1_view = e.CommandArgument.ToString().Split(';');

                ViewState["u0_main_course_idx_ref"] = arg1_view[0].ToString();
                ViewState["CEmpIDX_U2"] = arg1_view[1].ToString();
                ViewState["u0_course_idx_U2"] = arg1_view[2].ToString();
                ViewState["ViewActive_Quiz"] = arg1_view[3].ToString();
                ViewState["set_edit"] = arg1_view[4].ToString();

                setActiveTab("docViewDetailQuiz", 0, 0, 0, 0, 0, 0, 0);


                break;

            case "CmdDelMaster_Quiz":
                string u2_mainidx_delete_quiz = e.CommandArgument.ToString();

                DeleteMasterQuiz(int.Parse(u2_mainidx_delete_quiz), 9);
                select_typequiz_list_Head(GvHeadTypeAns, int.Parse(ViewState["u0_course_idx_U2"].ToString()));
                select_typequiz_list_Show(GvTypeAns, int.Parse(ViewState["u0_course_idx_U2"].ToString()));

                break;

            case "BtnBack":
                setActiveTab(ViewState["ViewActive_Quiz"].ToString(), 0, 0, 0, 0, 0, 0, 0);
                SETFOCUS.Focus();
                break;

            case "CmdDelDetailQuest":
                string u2detail_delete = e.CommandArgument.ToString();

                DeleteMasterDetailQuiz(int.Parse(u2detail_delete), 9);
                select_typequiz_list_Show(GvTypeAns, int.Parse(ViewState["u0_course_idx_U2"].ToString()));

                break;

            case "CmdInsertQuiz_More":
                string[] arg_detailaddon = new string[5];
                arg_detailaddon = e.CommandArgument.ToString().Split(';');

                ViewState["m0_tqidx_addon"] = arg_detailaddon[0].ToString();
                ViewState["u0_course_idx_addon"] = arg_detailaddon[1].ToString();
                ViewState["TypeInsert_addon"] = arg_detailaddon[2].ToString();
                ViewState["weight_score_addon"] = arg_detailaddon[3].ToString();
                ViewState["u2_main_course_idx_addon"] = arg_detailaddon[4].ToString();

                //litDebug.Text = ViewState["m0_tqidx_addon"].ToString() + " ,, " + ViewState["u0_course_idx_addon"].ToString() + " ,, " + ViewState["TypeInsert_addon"].ToString() + " ,, " + ViewState["weight_score_addon"].ToString() + ",,," + ViewState["u2_main_course_idx_addon"].ToString();

                setActiveTab("docCreateQuiz", 0, 0, 0, 0, 0, 0, 0);
                SETFOCUS.Focus();

                break;

            case "CmdBack_DetailQuiz":
                ViewState["TypeInsert_addon"] = "1";
                setActiveTab("docViewDetailQuiz", 0, 0, 0, 0, 0, 0, 0);

                break;
        }
    }

    #endregion


    #endregion

    private FormView getFv(string _sMode)
    {
        if (_sMode == "I")
        {
            return fvCRUD;
        }
        else
        {
            return fv_Update;
        }
    }

    #region FvDetail_DataBound

    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        _FormView = getFv(_func_dmu.zGetMode(Hddfld_course_no.Value));
        if (sender is FormView)
        {
            var FvName = (FormView)sender;
            if (FvName.ID == "fvCRUD")
            {
                if (FvName.CurrentMode == FormViewMode.Insert)
                {
                    LinkButton btnsearch_GvCourse = (LinkButton)fvCRUD.FindControl("btnsearch_GvCourse");
                    linkBtnTrigger(btnsearch_GvCourse);
                    // setTrigger();
                }
            }
            else if (FvName.ID == "fv_Update")
            {
                if (FvName.CurrentMode == FormViewMode.Edit)
                {
                    LinkButton btnsearch_GvCourse = (LinkButton)fv_Update.FindControl("btnsearch_GvCourse");
                    linkBtnTrigger(btnsearch_GvCourse);
                    //setTrigger();
                }
            }

            //new check with case toei
            switch (FvName.ID)
            {
                case "fvCreateCourse":
                    if (FvName.CurrentMode == FormViewMode.Insert)
                    {
                        ClearDataDeptCouseList();


                    }

                    if (FvName.CurrentMode == FormViewMode.ReadOnly)
                    {


                    }

                    if (FvName.CurrentMode == FormViewMode.Edit)
                    {
                        CleardataSetPlaceListUpdate();

                    }
                    break;
                case "fvcreate_quiz":
                    if (FvName.CurrentMode == FormViewMode.Insert)
                    {
                        var ddlcourse_createquiz = ((DropDownList)fvcreate_quiz.FindControl("ddlcourse_createquiz"));
                        var ddltypeanswer_createquiz = ((DropDownList)fvcreate_quiz.FindControl("ddltypeanswer_createquiz"));
                        var txtweight = ((TextBox)fvcreate_quiz.FindControl("txtweight"));
                        var btnback_detailquiz = ((LinkButton)fvcreate_quiz.FindControl("btnback_detailquiz"));
                        var div_choice = ((Control)fvcreate_quiz.FindControl("div_choice"));
                        var div_match = ((Control)fvcreate_quiz.FindControl("div_match"));

                        selectcourse_quiz(ddlcourse_createquiz);
                        selecttypeanswer_quiz(ddltypeanswer_createquiz);

                        if (ViewState["TypeInsert_addon"].ToString() == "2")
                        {
                            ddlcourse_createquiz.SelectedValue = ViewState["u0_course_idx_addon"].ToString();
                            ddltypeanswer_createquiz.SelectedValue = ViewState["m0_tqidx_addon"].ToString();
                            txtweight.Text = ViewState["weight_score_addon"].ToString();

                            ddlcourse_createquiz.Enabled = false;
                            ddltypeanswer_createquiz.Enabled = false;
                            txtweight.Enabled = false;
                            btnback_detailquiz.Visible = true;

                            if (ddltypeanswer_createquiz.SelectedValue == "1")
                            {
                                div_choice.Visible = true;
                                div_match.Visible = false;
                            }
                            else if (ddltypeanswer_createquiz.SelectedValue == "2")
                            {
                                div_choice.Visible = false;
                                div_match.Visible = false;
                            }
                            else if (ddltypeanswer_createquiz.SelectedValue == "3")
                            {
                                div_choice.Visible = false;
                                div_match.Visible = true;
                            }

                            else
                            {
                                div_choice.Visible = false;
                                div_match.Visible = false;
                            }
                        }
                        else
                        {
                            btnback_detailquiz.Visible = false;
                            ddlcourse_createquiz.SelectedValue = "0";
                            ddltypeanswer_createquiz.SelectedValue = "0";
                            txtweight.Text = String.Empty;
                            ddlcourse_createquiz.Enabled = true;
                            ddltypeanswer_createquiz.Enabled = true;
                            txtweight.Enabled = true;
                        }

                    }

                    break;
            }
            //new check with case toei

        }
        else if (sender is DropDownList)
        {

            var FvName = (DropDownList)sender;
            switch (FvName.ID)
            {
                case "ddlm0_training_group_idx_ref":

                    DropDownList ddlm0_training_group_idx_ref = (DropDownList)_FormView.FindControl("ddlm0_training_group_idx_ref");
                    DropDownList ddm0_training_branch_idx = (DropDownList)_FormView.FindControl("ddm0_training_branch_idx");

                    _func_dmu.zDropDownListWhereID(ddm0_training_branch_idx,
                                                "",
                                                "m0_training_branch_idx",
                                                "training_branch_name",
                                                "0",
                                                "trainingLoolup",
                                                "",
                                                "TRN-BRANCH",
                                                "m0_training_group_idx_ref",
                                                _func_dmu.zStringToInt(ddlm0_training_group_idx_ref.SelectedValue)
                                                );
                    searchCOURSEList();
                    //setTrigger();

                    break;
                case "ddm0_training_branch_idx":

                    searchCOURSEList();
                    //setTrigger();

                    break;

                case "ddlOrg_search":
                    _func_dmu.zGetDepartmentList(ddlDept_search, int.Parse(ddlOrg_search.SelectedItem.Value));
                    setGvEmpTarget_search();
                    //setTrigger();
                    break;
                case "ddlDept_search":
                    _func_dmu.zGetSectionList(ddlSec_search, int.Parse(ddlOrg_search.SelectedItem.Value), int.Parse(ddlDept_search.SelectedItem.Value));
                    setGvEmpTarget_search();
                    //  setTrigger();
                    break;
            }
        }
    }


    #endregion FvDetail_DataBound


    public void showmodal_traning(string sSearch = "")
    {
        trainingLoolup obj_trainingLoolup = new trainingLoolup();
        _data_elearning.trainingLoolup_action = new trainingLoolup[1];
        obj_trainingLoolup.operation_status_id = "TRN";
        obj_trainingLoolup.fil_Search = sSearch;
        _data_elearning.trainingLoolup_action[0] = obj_trainingLoolup;
        _data_elearning = _func_dmu.zCallServiceNetwork(_func_dmu._urlGetel_lu_TraningAll, _data_elearning);
        //  _func_dmu.zSetGridData(gvModalTraning, _data_elearning.trainingLoolup_action);
    }

    public void showmodal_traning_app(string sSearch = "")
    {
        trainingLoolup obj_trainingLoolup = new trainingLoolup();
        _data_elearning.trainingLoolup_action = new trainingLoolup[1];
        obj_trainingLoolup.operation_status_id = "TRN";
        obj_trainingLoolup.fil_Search = sSearch;
        _data_elearning.trainingLoolup_action[0] = obj_trainingLoolup;
        _data_elearning = _func_dmu.zCallServiceNetwork(_func_dmu._urlGetel_lu_TraningAll, _data_elearning);
        // _func_dmu.zSetGridData(gvModalTraning_app, _data_elearning.trainingLoolup_action);
    }



    #region Check Session Redirect
    protected void checkSessionRedirect()
    {
        //if (Session["sesLoginIdxBackEnd"] == null)
        //{
        //   Response.Redirect(ResolveUrl("~/login"));
        //}
    }
    #endregion Check Session Redirect

    protected void btnuser(object sender, CommandEventArgs e)
    {

    }
    public string gettraining_req_type(int id)
    {
        string sRetrun = "";
        if (id == 0)
        {
            sRetrun = "ฝึกอบรมภายใน";
        }
        else if (id == 1)
        {
            sRetrun = "	ฝึกอบรมภายนอก";
        }
        else
        {
            sRetrun = "ฝึกอบรมภายใน/ฝึกอบรมภายนอก";
        }
        return sRetrun;
    }

    private void CreateDs_dsAddVedio()
    {
        DataSet ds = new DataSet();
        ds.Tables.Add("dsAddVedio");
        ds.Tables["dsAddVedio"].Columns.Add("id", typeof(String));
        ds.Tables["dsAddVedio"].Columns.Add("item", typeof(String));
        ds.Tables["dsAddVedio"].Columns.Add("title", typeof(String));
        ds.Tables["dsAddVedio"].Columns.Add("description", typeof(String));
        ds.Tables["dsAddVedio"].Columns.Add("file", typeof(String));
        ds.Tables["dsAddVedio"].Columns.Add("file_path", typeof(String));
        ds.Tables["dsAddVedio"].Columns.Add("fileimage", typeof(String));
        ds.Tables["dsAddVedio"].Columns.Add("fileimage_path", typeof(String));
        ViewState["vsAddVedioList"] = ds;

    }
    protected void BtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = this.Page.FindControl("UpdatePanel1") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }
    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    protected void FileUploadTrigger(FileUpload _FileUpload)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = _FileUpload.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    private void ClearTraning()
    {
        Select_Page1showdata();
    }

    private void CreateDs_dsevaluation()
    {
        string sDs = "dsEvaluation";
        string sVs = "vsEvaluation";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);
        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("evaluation_group_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("evaluation_group_name", typeof(String));
        ds.Tables[sDs].Columns.Add("evaluation_f_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("evaluation_f_name", typeof(String));
        ViewState[sVs] = ds;

    }
    private void evaluationList()
    {
        _func_dmu.zDropDownList(ddlm0_evaluation_group_idx_refsel,
                                 "",
                                 "zId",
                                 "zName",
                                 "0",
                                 "trainingLoolup",
                                 "",
                                 "EVALUA-GRP"

                                 );

        txtevaluation_f_name_sel.Text = "";

    }
    private void evaluation_Select()
    {

        data_elearning dataelearning = new data_elearning();
        trainingLoolup obj = new trainingLoolup();
        dataelearning.trainingLoolup_action = new trainingLoolup[1];
        obj.operation_status_id = "EVALUA-GRP-LIST";
        dataelearning.trainingLoolup_action[0] = obj;
        dataelearning = _func_dmu.zCallServiceNetwork(_func_dmu._urlGetel_lu_TraningAll, dataelearning);

        CreateDs_dsevaluation();
        DataSet Ds = (DataSet)ViewState["vsEvaluation"];
        DataRow dr;
        if (dataelearning.trainingLoolup_action != null)
        {

            foreach (var item in dataelearning.trainingLoolup_action)
            {
                dr = Ds.Tables["dsEvaluation"].NewRow();
                dr["id"] = "1";
                dr["evaluation_group_idx"] = item.zId_G.ToString();
                dr["evaluation_group_name"] = item.zName_G;
                dr["evaluation_f_idx"] = item.zId.ToString();
                dr["evaluation_f_name"] = item.zName;
                Ds.Tables["dsEvaluation"].Rows.Add(dr);
            }

        }
        ViewState["vsEvaluation"] = Ds;
        _func_dmu.zSetGridData(GvEvaluation, Ds.Tables["dsEvaluation"]);
    }

    public Boolean getValue(string id = "0")
    {
        Boolean rBoolean = false;

        if (id == "1")
        {
            rBoolean = true;
        }

        return rBoolean;
    }

    private void addTraning()
    {
        //******************  start U3 *********************//
        CreateDs_dsevaluation();
        DataSet Ds = (DataSet)ViewState["vsEvaluation"];
        DataRow dr;
        int ic = 0;
        foreach (var item in GvEvaluation.Rows)
        {
            CheckBox GvEvaluation_cb_sel = (CheckBox)GvEvaluation.Rows[ic].FindControl("GvEvaluation_cb_sel");
            Label lbevaluation_group_idx_GvEvalu = (Label)GvEvaluation.Rows[ic].FindControl("lbevaluation_group_idx_GvEvalu");
            Label lbevaluation_f_idx_GvEvalu = (Label)GvEvaluation.Rows[ic].FindControl("lbevaluation_f_idx_GvEvalu");
            Label lbevaluation_f_name_GvEvalu = (Label)GvEvaluation.Rows[ic].FindControl("lbevaluation_f_name_GvEvalu");
            Label lbevaluation_group_name_GvEvalu = (Label)GvEvaluation.Rows[ic].FindControl("lbevaluation_group_name_GvEvalu");

            dr = Ds.Tables["dsEvaluation"].NewRow();
            if (GvEvaluation_cb_sel.Checked == true)
            {
                dr["id"] = "1";
            }
            else
            {
                dr["id"] = "0";
            }

            dr["evaluation_group_idx"] = lbevaluation_group_idx_GvEvalu.Text;
            dr["evaluation_group_name"] = lbevaluation_group_name_GvEvalu.Text;
            dr["evaluation_f_idx"] = lbevaluation_f_idx_GvEvalu.Text;
            dr["evaluation_f_name"] = lbevaluation_f_name_GvEvalu.Text;
            Ds.Tables["dsEvaluation"].Rows.Add(dr);

            ic++;
        }

        ViewState["vsEvaluation"] = Ds;
        //******************  end U3 *********************//

        DataSet dsAddVedio = (DataSet)ViewState["vsEvaluation"];
        DataRow drAddVedio;
        drAddVedio = dsAddVedio.Tables["dsEvaluation"].NewRow();
        drAddVedio["id"] = "1";
        drAddVedio["evaluation_group_idx"] = ddlm0_evaluation_group_idx_refsel.SelectedValue;
        drAddVedio["evaluation_group_name"] = ddlm0_evaluation_group_idx_refsel.Items[ddlm0_evaluation_group_idx_refsel.SelectedIndex].ToString();
        drAddVedio["evaluation_f_name"] = txtevaluation_f_name_sel.Text;
        dsAddVedio.Tables["dsEvaluation"].Rows.Add(drAddVedio);
        ViewState["vsEvaluation"] = dsAddVedio;
        _func_dmu.zSetGridData(GvEvaluation, dsAddVedio.Tables["dsEvaluation"]);
        ddlm0_evaluation_group_idx_refsel.SelectedIndex = 0;
        txtevaluation_f_name_sel.Text = "";

    }

    protected void gridViewTrigger(GridView linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    protected void onRowCommand(Object sender, GridViewCommandEventArgs e)
    {
        if (sender is GridView)
        {
            string cmdName = e.CommandName;
            switch (cmdName)
            {
                case "btnRemove_GvTest":

                    GridViewRow rowSelect = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    int rowIndex = rowSelect.RowIndex;
                    DataSet dsAddVedio = (DataSet)ViewState["vsel_u2_course"];
                    dsAddVedio.Tables["dsel_u2_course"].Rows[rowIndex].Delete();
                    dsAddVedio.AcceptChanges();
                    _func_dmu.zSetGridData(GvTest, dsAddVedio.Tables["dsel_u2_course"]);
                    deleteFileFDImage();

                    break;
                case "btnRemove_GvDeptTraningList":
                    GridViewRow rowSelectDept = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    int rowIndexDept = rowSelectDept.RowIndex;
                    DataSet dsu1 = (DataSet)ViewState["vsel_u1_course"];
                    dsu1.Tables["dsel_u1_course"].Rows[rowIndexDept].Delete();
                    dsu1.AcceptChanges();
                    _func_dmu.zSetGridData(GvDeptTraningList, dsu1.Tables["dsel_u1_course"]);
                    break;

                case "cmdRemoveDeptEmp":
                    GridView GvDeptTraningList_ = (GridView)fvCreateCourse.FindControl("GvDeptTraningList");
                    GridViewRow rowSelect_ = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    int rowIndex_ = rowSelect_.RowIndex;
                    DataSet dsContacts = (DataSet)ViewState["vsDepartment"];
                    dsContacts.Tables["dsDepartmentListTable"].Rows[rowIndex_].Delete();
                    dsContacts.AcceptChanges();
                    setGridData(GvDeptTraningList_, dsContacts.Tables["dsDepartmentListTable"]);
                    if (dsContacts.Tables["dsDepartmentListTable"].Rows.Count < 1)
                    {
                        GvDeptTraningList_.Visible = false;
                    }
                    break;
                case "cmdRemovePerDeptCouse":
                    GridView GvDeptTraningList_edit = (GridView)fvCreateCourse.FindControl("GvDeptTraningList");
                    GridViewRow rowSelect_edit = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    int rowIndex_edit = rowSelect_edit.RowIndex;
                    DataSet dsContacts_edit = (DataSet)ViewState["vsDetailCousePerListUpdate"];
                    dsContacts_edit.Tables["dsDetailCousePerTableUpdate"].Rows[rowIndex_edit].Delete();
                    dsContacts_edit.AcceptChanges();
                    setGridData(GvDeptTraningList_edit, dsContacts_edit.Tables["dsDetailCousePerTableUpdate"]);
                    if (dsContacts_edit.Tables["dsDetailCousePerTableUpdate"].Rows.Count < 1)
                    {
                        GvDeptTraningList_edit.Visible = false;
                    }
                    break;

            }
        }
    }


    private void CreateDs_dstest()
    {
        string sDs = "dsel_u2_course";
        string sVs = "vsel_u2_course";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);
        ds.Tables[sDs].Columns.Add("course_item", typeof(String));
        ds.Tables[sDs].Columns.Add("course_proposition", typeof(String));
        ds.Tables[sDs].Columns.Add("course_propos_a", typeof(String));
        ds.Tables[sDs].Columns.Add("course_propos_b", typeof(String));
        ds.Tables[sDs].Columns.Add("course_propos_c", typeof(String));
        ds.Tables[sDs].Columns.Add("course_propos_d", typeof(String));
        ds.Tables[sDs].Columns.Add("course_propos_answer", typeof(String));
        ds.Tables[sDs].Columns.Add("course_propos_score", typeof(String));
        ds.Tables[sDs].Columns.Add("docno_bin", typeof(String));
        ds.Tables[sDs].Columns.Add("course_proposition_img", typeof(String));
        ds.Tables[sDs].Columns.Add("course_propos_a_img", typeof(String));
        ds.Tables[sDs].Columns.Add("course_propos_b_img", typeof(String));
        ds.Tables[sDs].Columns.Add("course_propos_c_img", typeof(String));
        ds.Tables[sDs].Columns.Add("course_propos_d_img", typeof(String));
        ViewState[sVs] = ds;

    }
    public void deleteFileFDImage()
    {
        string _PathFile1 = ConfigurationSettings.AppSettings["path_flie_elearning"];
        string _PathFile2 = "";
        if ((Hddfld_folderImage.Value == null) || (Hddfld_folderImage.Value == ""))
        {

        }
        else
        {
            for (int i = 1; i <= 5; i++)
            {
                try
                {
                    _PathFile2 = _PathFile1 + _Folder_courseBin + "/" + Hddfld_folderImage.Value + "/" + i.ToString() + ".png";
                    File.Delete(Server.MapPath(_PathFile2));
                }
                catch { }
            }
            try
            {
                _PathFile2 = _PathFile1 + _Folder_courseBin + "/" + Hddfld_folderImage.Value;
                Directory.Delete(Server.MapPath(_PathFile2));
            }
            catch { }
        }
    }
    private void addtest()
    {

        //TextBox txtcourse_item = (TextBox)fvDetail.FindControl("txtcourse_item");
        //TextBox txtcourse_proposition = (TextBox)fvDetail.FindControl("txtcourse_proposition");
        //TextBox txtcourse_propos_a = (TextBox)fvDetail.FindControl("txtcourse_propos_a");
        //TextBox txtcourse_propos_b = (TextBox)fvDetail.FindControl("txtcourse_propos_b");
        //TextBox txtcourse_propos_c = (TextBox)fvDetail.FindControl("txtcourse_propos_c");
        //TextBox txtcourse_propos_d = (TextBox)fvDetail.FindControl("txtcourse_propos_d");
        //DropDownList ddlcourse_propos_answer = (DropDownList)fvDetail.FindControl("ddlcourse_propos_answer");
        //FileUpload UploadImages_proposition = (FileUpload)fvDetail.FindControl("UploadImages_proposition");
        //FileUpload UploadImages_a = (FileUpload)fvDetail.FindControl("UploadImages_a");
        //FileUpload UploadImages_b = (FileUpload)fvDetail.FindControl("UploadImages_b");
        //FileUpload UploadImages_c = (FileUpload)fvDetail.FindControl("UploadImages_c");
        //FileUpload UploadImages_d = (FileUpload)fvDetail.FindControl("UploadImages_d");


        string _sDocNo = "";
        int i = 0, idata = 0;
        idata = int.Parse(txtcourse_item.Text);
        DataSet ds = (DataSet)ViewState["vsel_u2_course"];
        foreach (DataRow item in ds.Tables["dsel_u2_course"].Rows)
        {
            if (item["docno_bin"].ToString() != "")
            {
                _sDocNo = item["docno_bin"].ToString();
            }

            if (_func_dmu.zStringToInt(item["course_item"].ToString()) == idata)
            {
                i++;
            }
        }
        if (i > 0)
        {
            showAlert("ลำดับนี้มีอยู่แล้วกรุณากรอกใหม่");
            deleteFileFDImage();
            txtcourse_item.Focus();
        }
        else
        {
            DataRow dr;

            dr = ds.Tables["dsel_u2_course"].NewRow();
            dr["course_item"] = txtcourse_item.Text;
            dr["course_proposition"] = txtcourse_proposition.Text;
            dr["course_propos_a"] = txtcourse_propos_a.Text;
            dr["course_propos_b"] = txtcourse_propos_b.Text;
            dr["course_propos_c"] = txtcourse_propos_c.Text;
            dr["course_propos_d"] = txtcourse_propos_d.Text;
            dr["course_propos_answer"] = ddlcourse_propos_answer.SelectedValue;

            // _func_dmu.zSetGridData(GridView1, ds.Tables["dsel_u2_course"]);
            if (_sDocNo == "")
            {
                _sDocNo = "emp_idx" + emp_idx.ToString() + DateTime.Now.ToString("yyyyMMdd_HHmmss_ffff");

            }

            int _int = 0;

            if (zUploadImagesBin(UploadImages_proposition, _sDocNo, int.Parse(txtcourse_item.Text), 1) == 1)
            {
                _int++;
                dr["course_proposition_img"] = "1.png";
            }
            else
            {
                dr["course_proposition_img"] = "";
            }

            if (zUploadImagesBin(UploadImages_a, _sDocNo, int.Parse(txtcourse_item.Text), 2) == 1)
            {
                _int++;
                dr["course_propos_a_img"] = "2.png";
            }
            else
            {
                dr["course_propos_a_img"] = "";
            }

            if (zUploadImagesBin(UploadImages_b, _sDocNo, int.Parse(txtcourse_item.Text), 3) == 1)
            {
                _int++;
                dr["course_propos_b_img"] = "3.png";
            }
            else
            {
                dr["course_propos_b_img"] = "";
            }

            if (zUploadImagesBin(UploadImages_c, _sDocNo, int.Parse(txtcourse_item.Text), 4) == 1)
            {
                _int++;
                dr["course_propos_c_img"] = "4.png";
            }
            else
            {
                dr["course_propos_c_img"] = "";
            }

            if (zUploadImagesBin(UploadImages_d, _sDocNo, int.Parse(txtcourse_item.Text), 5) == 1)
            {
                _int++;
                dr["course_propos_d_img"] = "5.png";
            }
            else
            {
                dr["course_propos_d_img"] = "";
            }
            if (_int > 0)
            {
                dr["docno_bin"] = _sDocNo;
            }
            else
            {
                dr["docno_bin"] = "";
            }

            ds.Tables["dsel_u2_course"].Rows.Add(dr);
            ViewState["vsel_u2_course"] = ds;
            _func_dmu.zSetGridData(GvTest, ds.Tables["dsel_u2_course"]);

            cleartest();
        }
    }
    private void cleartest()
    {
        //TextBox txtcourse_item = (TextBox)fvDetail.FindControl("txtcourse_item");
        //TextBox txtcourse_proposition = (TextBox)fvDetail.FindControl("txtcourse_proposition");
        //TextBox txtcourse_propos_a = (TextBox)fvDetail.FindControl("txtcourse_propos_a");
        //TextBox txtcourse_propos_b = (TextBox)fvDetail.FindControl("txtcourse_propos_b");
        //TextBox txtcourse_propos_c = (TextBox)fvDetail.FindControl("txtcourse_propos_c");
        //TextBox txtcourse_propos_d = (TextBox)fvDetail.FindControl("txtcourse_propos_d");
        //DropDownList ddlcourse_propos_answer = (DropDownList)fvDetail.FindControl("ddlcourse_propos_answer");

        deleteFileFDImage();

        txtcourse_item.Text = "";
        txtcourse_proposition.Text = "";
        txtcourse_propos_a.Text = "";
        txtcourse_propos_b.Text = "";
        txtcourse_propos_c.Text = "";
        txtcourse_propos_d.Text = "";
        ddlcourse_propos_answer.SelectedIndex = 0;

    }

    public string gettest(string id)
    {
        string sNane = "";

        if (id == "1")
        {
            sNane = "ก";
        }
        else if (id == "2")
        {
            sNane = "ข";
        }
        else if (id == "3")
        {
            sNane = "ค";
        }
        else if (id == "4")
        {
            sNane = "ง";
        }

        return sNane;
    }
    private string getbranchcode(int id)
    {
        string sreturn = "";
        _data_elearning.trainingLoolup_action = new trainingLoolup[1];
        trainingLoolup obj = new trainingLoolup();
        obj.idx = id;
        obj.operation_status_id = "TRN-GROUP";
        _data_elearning.trainingLoolup_action[0] = obj;
        _data_elearning = _func_dmu.zCallServicePostNetwork(_func_dmu._urlGetel_lu_TraningAll, _data_elearning);
        if (_data_elearning.trainingLoolup_action != null)
        {
            obj = _data_elearning.trainingLoolup_action[0];
            sreturn = obj.doc_no;
        }

        return sreturn;
    }

    private Boolean zSaveInsert()
    {
        Boolean _Boolean = false;
        // U0
        TextBox txtcourse_no = (TextBox)fvCRUD.FindControl("txtcourse_no");
        DropDownList ddlm0_training_group_idx_ref = (DropDownList)fvCRUD.FindControl("ddlm0_training_group_idx_ref");
        DropDownList ddm0_training_branch_idx = (DropDownList)fvCRUD.FindControl("ddm0_training_branch_idx");
        TextBox txtcourse_name = (TextBox)fvCRUD.FindControl("txtcourse_name");
        TextBox txtcourse_date = (TextBox)fvCRUD.FindControl("txtcourse_date");
        TextBox txtcourse_remark = (TextBox)fvCRUD.FindControl("txtcourse_remark");
        DropDownList ddllevel_code = (DropDownList)fvCRUD.FindControl("ddllevel_code");
        TextBox txtcourse_score = (TextBox)fvCRUD.FindControl("txtcourse_score");
        CheckBox cbcourse_type_etraining = (CheckBox)fvCRUD.FindControl("cbcourse_type_etraining");
        CheckBox cbcourse_type_elearning = (CheckBox)fvCRUD.FindControl("cbcourse_type_elearning");
        DropDownList ddlcourse_priority = (DropDownList)fvCRUD.FindControl("ddlcourse_priority");
        DropDownList ddlcourse_status = (DropDownList)fvCRUD.FindControl("ddlcourse_status");
        GridView GvCourse = (GridView)fvCRUD.FindControl("GvCourse");
        GridView GvM0_EmpTarget = (GridView)fvCRUD.FindControl("GvM0_EmpTarget");
        _ddlcourse_assessment = (DropDownList)fvCRUD.FindControl("ddlcourse_assessment");
        TextBox txtscore_through_per = (TextBox)fvCRUD.FindControl("txtscore_through_per");

        string m0_prefix = "", _docno = "";

        m0_prefix = getbranchcode(int.Parse(ddlm0_training_group_idx_ref.SelectedValue));
        if (m0_prefix == "")
        {
            showAlert("รหัสกลุ่มวิชาไม่ถูกต้อง");

        }
        else
        {
            int idx = 0, icourse_score = 0, u0_course_idx_ref = 0;
            course obj_course = new course();
            _data_elearning.el_course_action = new course[1];
            // obj_course.course_no = _func_dmu.zRun_Number(_FromcourseRunNo, m0_prefix, "YYYY-COURSE", "N", "0000");
            //_docno = obj_course.course_no;

            obj_course.course_date = _func_dmu.zDateToDB(txtcourse_date.Text);
            obj_course.m0_training_group_idx_ref = int.Parse(ddlm0_training_group_idx_ref.SelectedValue);
            obj_course.m0_training_branch_idx_ref = int.Parse(ddm0_training_branch_idx.SelectedValue);
            obj_course.course_name = txtcourse_name.Text;
            obj_course.course_remark = txtcourse_remark.Text;
            obj_course.level_code = int.Parse(ddllevel_code.SelectedItem.Text);
            obj_course.course_priority = int.Parse(ddlcourse_priority.SelectedValue);
            obj_course.course_status = int.Parse(ddlcourse_status.SelectedValue);
            obj_course.course_assessment = _func_dmu.zStringToInt(_ddlcourse_assessment.SelectedValue);
            obj_course.score_through_per = _func_dmu.zStringToDecimal(txtscore_through_per.Text);

            if (txtcourse_score.Text != "")
            {
                icourse_score = int.Parse(txtcourse_score.Text);
            }
            obj_course.course_score = icourse_score;
            obj_course.course_created_by = emp_idx;
            obj_course.course_updated_by = emp_idx;
            obj_course.course_type_etraining = _func_dmu.zBooleanToInt(cbcourse_type_etraining.Checked);
            obj_course.course_type_elearning = _func_dmu.zBooleanToInt(cbcourse_type_elearning.Checked);
            obj_course.course_plan_status = "I";

            //node
            obj_course.approve_status = 0;
            obj_course.u0_idx = 5;
            obj_course.node_idx = 9;
            obj_course.actor_idx = 1;
            obj_course.app_flag = 0;
            obj_course.app_user = 0;

            obj_course.operation_status_id = "U0";
            _data_elearning.el_course_action[0] = obj_course;
            _data_elearning = _func_dmu.zCallServicePostNetwork(_urlSetInsel_u_course, _data_elearning);
            u0_course_idx_ref = _data_elearning.return_code;

            if (u0_course_idx_ref > 0)
            {
                _docno = _data_elearning.el_course_action[0].course_no;
                zSaveDetail(u0_course_idx_ref, _docno, "I");

            }
            _Boolean = true;
        }
        return _Boolean;

    }

    private void zSaveDetail(int _id, string _sDocNo, string _sMode)
    {
        if (_id <= 0)
        {
            return;
        }
        TextBox txtcourse_no;
        DropDownList ddlm0_training_group_idx_ref;
        DropDownList ddm0_training_branch_idx;
        TextBox txtcourse_name;
        TextBox txtcourse_date;
        TextBox txtcourse_remark;
        DropDownList ddllevel_code;
        TextBox txtcourse_score;
        CheckBox cbcourse_type_etraining;
        CheckBox cbcourse_type_elearning;
        DropDownList ddlcourse_priority;
        DropDownList ddlcourse_status;
        GridView GvCourse;
        GridView GvM0_EmpTarget;

        string _docno = "";

        // U0
        if (_sMode == "I")
        {
            txtcourse_no = (TextBox)fvCRUD.FindControl("txtcourse_no");
            ddlm0_training_group_idx_ref = (DropDownList)fvCRUD.FindControl("ddlm0_training_group_idx_ref");
            ddm0_training_branch_idx = (DropDownList)fvCRUD.FindControl("ddm0_training_branch_idx");
            txtcourse_name = (TextBox)fvCRUD.FindControl("txtcourse_name");
            txtcourse_date = (TextBox)fvCRUD.FindControl("txtcourse_date");
            txtcourse_remark = (TextBox)fvCRUD.FindControl("txtcourse_remark");
            ddllevel_code = (DropDownList)fvCRUD.FindControl("ddllevel_code");
            txtcourse_score = (TextBox)fvCRUD.FindControl("txtcourse_score");
            cbcourse_type_etraining = (CheckBox)fvCRUD.FindControl("cbcourse_type_etraining");
            cbcourse_type_elearning = (CheckBox)fvCRUD.FindControl("cbcourse_type_elearning");
            ddlcourse_priority = (DropDownList)fvCRUD.FindControl("ddlcourse_priority");
            ddlcourse_status = (DropDownList)fvCRUD.FindControl("ddlcourse_status");
            GvCourse = (GridView)fvCRUD.FindControl("GvCourse");
            GvM0_EmpTarget = (GridView)fvCRUD.FindControl("GvM0_EmpTarget");
        }
        else
        {
            txtcourse_no = (TextBox)fv_Update.FindControl("txtcourse_no");
            ddlm0_training_group_idx_ref = (DropDownList)fv_Update.FindControl("ddlm0_training_group_idx_ref");
            ddm0_training_branch_idx = (DropDownList)fv_Update.FindControl("ddm0_training_branch_idx");
            txtcourse_name = (TextBox)fv_Update.FindControl("txtcourse_name");
            txtcourse_date = (TextBox)fv_Update.FindControl("txtcourse_date");
            txtcourse_remark = (TextBox)fv_Update.FindControl("txtcourse_remark");
            ddllevel_code = (DropDownList)fv_Update.FindControl("ddllevel_code");
            txtcourse_score = (TextBox)fv_Update.FindControl("txtcourse_score");
            cbcourse_type_etraining = (CheckBox)fv_Update.FindControl("cbcourse_type_etraining");
            cbcourse_type_elearning = (CheckBox)fv_Update.FindControl("cbcourse_type_elearning");
            ddlcourse_priority = (DropDownList)fv_Update.FindControl("ddlcourse_priority");
            ddlcourse_status = (DropDownList)fv_Update.FindControl("ddlcourse_status");
            GvCourse = (GridView)fv_Update.FindControl("GvCourse");
            GvM0_EmpTarget = (GridView)fv_Update.FindControl("GvM0_EmpTarget");
        }
        _docno = txtcourse_no.Text;
        //showAlert(_sDocNo);
        int ic = 0, icount = 0, irow = 0, u0_course_idx_ref = 0;
        int itemObj = 0;

        u0_course_idx_ref = _id;
        data_elearning dataelearning = new data_elearning();
        //******************  start U1 *********************//
        itemObj = 0;
        DataSet dsU1 = (DataSet)ViewState["vsel_u1_course"];
        course[] objcourse1 = new course[dsU1.Tables["dsel_u1_course"].Rows.Count];
        foreach (DataRow item in dsU1.Tables["dsel_u1_course"].Rows)
        {
            objcourse1[itemObj] = new course();
            objcourse1[itemObj].u0_course_idx_ref = u0_course_idx_ref;
            objcourse1[itemObj].RSecID_ref = _func_dmu.zStringToInt(item["RSecID_ref"].ToString());
            objcourse1[itemObj].RDeptID_ref = _func_dmu.zStringToInt(item["RDeptID_ref"].ToString());
            objcourse1[itemObj].org_idx_ref = _func_dmu.zStringToInt(item["org_idx_ref"].ToString());
            objcourse1[itemObj].TIDX_ref = _func_dmu.zStringToInt(item["TIDX_ref"].ToString());
            objcourse1[itemObj].course_status = 1;
            objcourse1[itemObj].course_updated_by = emp_idx;
            objcourse1[itemObj].operation_status_id = "U1";
            itemObj++;

        }
        if (itemObj == 0)
        {
            objcourse1 = new course[1];
            objcourse1[itemObj] = new course();
            objcourse1[itemObj].u0_course_idx_ref = u0_course_idx_ref;
            objcourse1[itemObj].operation_status_id = "U1-DEL";
        }
        dataelearning.el_course_action = objcourse1;
        _func_dmu.zCallServicePostNetwork(_urlSetInsel_u_course, dataelearning);
        //******************  end U1 *********************//

        //******************  start U2 *********************//
        itemObj = 0;
        ic = 0;
        icount = 0;
        irow = 0;
        ic = 0;
        DataSet dsU2 = (DataSet)ViewState["vsel_u2_course"];
        course[] objcourse2 = new course[dsU2.Tables["dsel_u2_course"].Rows.Count];
        foreach (DataRow item in dsU2.Tables["dsel_u2_course"].Rows)
        {
            objcourse2[itemObj] = new course();
            objcourse2[itemObj].u0_course_idx_ref = u0_course_idx_ref;
            objcourse2[itemObj].course_item = _func_dmu.zStringToInt(item["course_item"].ToString());
            objcourse2[itemObj].course_proposition = item["course_proposition"].ToString();
            objcourse2[itemObj].course_propos_a = item["course_propos_a"].ToString();
            objcourse2[itemObj].course_propos_b = item["course_propos_b"].ToString();
            objcourse2[itemObj].course_propos_c = item["course_propos_c"].ToString();
            objcourse2[itemObj].course_propos_d = item["course_propos_d"].ToString();
            objcourse2[itemObj].course_propos_answer = item["course_propos_answer"].ToString();

            //if (item["docno_bin"].ToString() != "")
            //{
            objcourse2[itemObj].course_proposition_img = item["course_proposition_img"].ToString();
            objcourse2[itemObj].course_propos_a_img = item["course_propos_a_img"].ToString();
            objcourse2[itemObj].course_propos_b_img = item["course_propos_b_img"].ToString();
            objcourse2[itemObj].course_propos_c_img = item["course_propos_c_img"].ToString();
            objcourse2[itemObj].course_propos_d_img = item["course_propos_d_img"].ToString();
            //}

            objcourse2[itemObj].course_status = 1;
            objcourse2[itemObj].course_created_by = emp_idx;
            objcourse2[itemObj].course_updated_by = emp_idx;
            objcourse2[itemObj].operation_status_id = "U2";
            itemObj++;

        }
        if (itemObj == 0)
        {
            objcourse2 = new course[1];
            objcourse2[itemObj] = new course();
            objcourse2[itemObj].u0_course_idx_ref = u0_course_idx_ref;
            objcourse2[itemObj].operation_status_id = "U2-DEL";
        }
        dataelearning.el_course_action = objcourse2;
        _func_dmu.zCallServicePostNetwork(_urlSetInsel_u_course, dataelearning);
        if (itemObj > 0)
        {
            string docno_bin = "";
            foreach (DataRow item in dsU2.Tables["dsel_u2_course"].Rows)
            {
                if (item["docno_bin"].ToString() != "")
                {
                    docno_bin = item["docno_bin"].ToString();
                    zUploadImagesElearning(docno_bin,
                                             _sDocNo,
                                            _func_dmu.zStringToInt(item["course_item"].ToString())
                                             );
                }
            }
            zDelImagesBin(docno_bin);
        }
        else
        {
            zDelImagesBin(_sDocNo);
        }
        //******************  end U2 *********************//

        //******************  start U3 *********************//
        itemObj = 0;
        ic = 0;
        icount = 0;
        irow = 0;
        ic = 0;
        foreach (var item in GvEvaluation.Rows)
        {
            CheckBox GvEvaluation_cb_sel = (CheckBox)GvEvaluation.Rows[ic].FindControl("GvEvaluation_cb_sel");
            if (GvEvaluation_cb_sel.Checked == true)
            {
                irow++;

            }
            ic++;

        }
        course[] objcourse3 = new course[irow];
        if (irow > 0)
        {

            itemObj = 0;
            ic = 0;
            icount = 0;
            irow = 0;
            ic = 0;
            foreach (var item in GvEvaluation.Rows)
            {
                CheckBox GvEvaluation_cb_sel = (CheckBox)GvEvaluation.Rows[ic].FindControl("GvEvaluation_cb_sel");
                if (GvEvaluation_cb_sel.Checked == true)
                {
                    irow++;
                    Label lbevaluation_group_idx_GvEvalu = (Label)GvEvaluation.Rows[ic].FindControl("lbevaluation_group_idx_GvEvalu");
                    Label lbevaluation_f_idx_GvEvalu = (Label)GvEvaluation.Rows[ic].FindControl("lbevaluation_f_idx_GvEvalu");
                    Label lbevaluation_f_name_GvEvalu = (Label)GvEvaluation.Rows[ic].FindControl("lbevaluation_f_name_GvEvalu");

                    objcourse3[itemObj] = new course();
                    objcourse3[itemObj].u0_course_idx_ref = u0_course_idx_ref;
                    objcourse3[itemObj].m0_evaluation_f_idx_ref = _func_dmu.zStringToInt(lbevaluation_f_idx_GvEvalu.Text);
                    objcourse3[itemObj].evaluation_f_item = 0;
                    objcourse3[itemObj].m0_evaluation_group_idx_ref = _func_dmu.zStringToInt(lbevaluation_group_idx_GvEvalu.Text);
                    objcourse3[itemObj].course_evaluation_f_name_th = lbevaluation_f_name_GvEvalu.Text;
                    objcourse3[itemObj].course_item = irow;
                    objcourse3[itemObj].course_status = 1;
                    objcourse3[itemObj].course_created_by = emp_idx;
                    objcourse3[itemObj].course_updated_by = emp_idx;
                    objcourse3[itemObj].operation_status_id = "U3";
                    itemObj++;
                }
                ic++;
            }
        }
        else
        {
            objcourse3 = new course[1];
            objcourse3[itemObj] = new course();
            objcourse3[itemObj].u0_course_idx_ref = u0_course_idx_ref;
            objcourse3[itemObj].operation_status_id = "U3-DEL";
        }

        dataelearning.el_course_action = objcourse3;
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataelearning));
        _func_dmu.zCallServicePostNetwork(_urlSetInsel_u_course, dataelearning);
        //******************  end U3 *********************//

        //******************  start U4 *********************//
        ic = 0;
        icount = 0;
        irow = 0;
        ic = 0;
        foreach (var item in GvCourse.Rows)
        {
            CheckBox GvCourse_cb_zId = (CheckBox)GvCourse.Rows[ic].FindControl("GvCourse_cb_zId");
            if (GvCourse_cb_zId.Checked == true)
            {
                irow++;
            }
            ic++;
        }
        itemObj = 0;
        icount = 0;
        course[] objcourse4 = new course[irow];
        ic = 0;
        if (irow > 0)
        {

            foreach (var item in GvCourse.Rows)
            {
                CheckBox GvCourse_cb_zId = (CheckBox)GvCourse.Rows[ic].FindControl("GvCourse_cb_zId");
                if (GvCourse_cb_zId.Checked == true)
                {
                    irow++;
                    Label GvCourse_zId = (Label)GvCourse.Rows[ic].FindControl("GvCourse_zId");
                    //itemObj = 0;
                    objcourse4[itemObj] = new course();
                    objcourse4[itemObj].u0_course_idx_ref = u0_course_idx_ref;
                    objcourse4[itemObj].u0_course_idx_ref_pass = int.Parse(GvCourse_zId.Text);
                    objcourse4[itemObj].course_updated_by = emp_idx;
                    objcourse4[itemObj].operation_status_id = "U4";
                    itemObj++;
                }
                ic++;
            }

        }
        if (itemObj == 0)
        {
            objcourse4 = new course[1];
            objcourse4[itemObj] = new course();
            objcourse4[itemObj].u0_course_idx_ref = u0_course_idx_ref;
            objcourse4[itemObj].operation_status_id = "U4-DEL";
        }
        dataelearning.el_course_action = objcourse4;
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(dataelearning));
        // litDebug.Text = _func_dmu.zJson(_urlSetInsel_u_course, dataelearning);
        _func_dmu.zCallServicePostNetwork(_urlSetInsel_u_course, dataelearning);

        //******************  end U4 *********************//

        //******************  start U5 *********************//


        ic = 0;
        icount = 0;
        //foreach (var item in GvM0_EmpTarget.Rows)
        //{
        //    CheckBox GvM0_EmpTarget_cb_zId = (CheckBox)GvM0_EmpTarget.Rows[ic].FindControl("GvM0_EmpTarget_cb_zId");
        //    if (GvM0_EmpTarget_cb_zId.Checked == true)
        //    {
        //        icount++;
        //    }

        //    ic++;
        //}
        CreateDs_el_u5_course();

        DataSet dsu5 = (DataSet)ViewState["vsel_u5_course"];
        DataRow dru5;
        ic = 0;
        icount = 0;
        foreach (var item in GvM0_EmpTarget.Rows)
        {
            dru5 = dsu5.Tables["dsel_u5_course"].NewRow();
            CheckBox GvM0_EmpTarget_cb_zId = (CheckBox)GvM0_EmpTarget.Rows[ic].FindControl("GvM0_EmpTarget_cb_zId");
            if (GvM0_EmpTarget_cb_zId.Checked == true)
            {

                dru5["TIDX_flag"] = "1";

            }
            else
            {
                dru5["TIDX_flag"] = "0";
            }
            Label GvM0_EmpTarget_zId = (Label)GvM0_EmpTarget.Rows[ic].FindControl("GvM0_EmpTarget_zId");
            dru5["TIDX_ref"] = GvM0_EmpTarget_zId.Text;
            dsu5.Tables["dsel_u5_course"].Rows.Add(dru5);

            ic++;
        }
        ViewState["vsel_u5_course"] = dsu5;

        itemObj = 0;
        dsu5 = (DataSet)ViewState["vsel_u5_course"];
        course[] objcourse5 = new course[dsu5.Tables["dsel_u5_course"].Rows.Count];
        foreach (DataRow item in dsu5.Tables["dsel_u5_course"].Rows)
        {
            objcourse5[itemObj] = new course();
            objcourse5[itemObj].u0_course_idx_ref = u0_course_idx_ref;
            objcourse5[itemObj].TIDX_ref = _func_dmu.zStringToInt(item["TIDX_ref"].ToString());
            objcourse5[itemObj].TIDX_flag = _func_dmu.zStringToInt(item["TIDX_flag"].ToString());
            objcourse5[itemObj].course_status = 1;
            objcourse5[itemObj].course_updated_by = emp_idx;
            objcourse5[itemObj].operation_status_id = "U5";
            itemObj++;

        }
        if (itemObj == 0)
        {
            objcourse5 = new course[1];
            objcourse5[itemObj] = new course();
            objcourse5[itemObj].u0_course_idx_ref = u0_course_idx_ref;
            objcourse5[itemObj].operation_status_id = "U5-DEL";
        }
        dataelearning.el_course_action = objcourse5;
        _func_dmu.zCallServicePostNetwork(_urlSetInsel_u_course, dataelearning);
        //******************  end U5 *********************//



    }

    private void CreateDs_el_u1_course()
    {
        string sDs = "dsel_u1_course";
        string sVs = "vsel_u1_course";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);
        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("u1_course_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_course_idx_ref", typeof(String));
        ds.Tables[sDs].Columns.Add("RSecID_ref", typeof(String));
        ds.Tables[sDs].Columns.Add("RDeptID_ref", typeof(String));
        ds.Tables[sDs].Columns.Add("RPosIDX_ref", typeof(String));
        ds.Tables[sDs].Columns.Add("org_idx_ref", typeof(String));
        ds.Tables[sDs].Columns.Add("course_qty", typeof(String));
        ds.Tables[sDs].Columns.Add("course_updated_by", typeof(String));
        ds.Tables[sDs].Columns.Add("TIDX_ref", typeof(String));

        ds.Tables[sDs].Columns.Add("org_name_th", typeof(String));
        ds.Tables[sDs].Columns.Add("dept_name_th", typeof(String));
        ds.Tables[sDs].Columns.Add("sec_name_th", typeof(String));
        ds.Tables[sDs].Columns.Add("target_name", typeof(String));
        ViewState[sVs] = ds;

    }

    private void addDeptTraning()
    {
        GridView GvM0_EmpTarget = GvM0_EmpTarget_search;  //(GridView)fvCRUD.FindControl("GvM0_EmpTarget");
        int ic = 0, icount = 0;
        foreach (var item in GvM0_EmpTarget.Rows)
        {
            CheckBox GvM0_EmpTarget_cb_zId = (CheckBox)GvM0_EmpTarget.Rows[ic].FindControl("GvM0_EmpTarget_cb_zId");
            if (GvM0_EmpTarget_cb_zId.Checked == true)
            {
                icount++;
            }

            ic++;
        }
        if ((icount > 0) && (ddlOrg_search.SelectedValue != "0") && (ddlDept_search.SelectedValue != "0") && (ddlSec_search.SelectedValue != "0"))
        {
            DataSet dsu1 = (DataSet)ViewState["vsel_u1_course"];
            DataRow dru1;
            ic = 0;
            icount = 0;
            foreach (var item in GvM0_EmpTarget.Rows)
            {
                CheckBox GvM0_EmpTarget_cb_zId = (CheckBox)GvM0_EmpTarget.Rows[ic].FindControl("GvM0_EmpTarget_cb_zId");
                if (GvM0_EmpTarget_cb_zId.Checked == true)
                {
                    dru1 = dsu1.Tables["dsel_u1_course"].NewRow();
                    dru1["id"] = "1";
                    dru1["org_idx_ref"] = ddlOrg_search.SelectedValue;
                    dru1["org_name_th"] = _func_dmu.zGetDataDropDownList(ddlOrg_search);
                    dru1["RDeptID_ref"] = ddlDept_search.SelectedValue;
                    dru1["dept_name_th"] = _func_dmu.zGetDataDropDownList(ddlDept_search);
                    dru1["RSecID_ref"] = ddlSec_search.SelectedValue;
                    dru1["sec_name_th"] = _func_dmu.zGetDataDropDownList(ddlSec_search);
                    Label GvM0_EmpTarget_zId = (Label)GvM0_EmpTarget.Rows[ic].FindControl("GvM0_EmpTarget_zId");
                    Label GvM0_EmpTarget_zName = (Label)GvM0_EmpTarget.Rows[ic].FindControl("GvM0_EmpTarget_zName");
                    dru1["TIDX_ref"] = GvM0_EmpTarget_zId.Text;
                    dru1["target_name"] = GvM0_EmpTarget_zName.Text;
                    dsu1.Tables["dsel_u1_course"].Rows.Add(dru1);

                    icount++;
                }

                ic++;
            }
            ViewState["vsel_u1_course"] = dsu1;
            _func_dmu.zSetGridData(GvDeptTraningList, dsu1.Tables["dsel_u1_course"]);
            ddlOrg_search.SelectedIndex = 0;
            ddlDept_search.SelectedIndex = 0;
            ddlSec_search.SelectedIndex = 0;
            setGvEmpTarget_searchList();
        }



    }
    public Boolean checkError()
    {
        Boolean _Boolean = false;

        CheckBox cbcourse_type_etraining = (CheckBox)fvCRUD.FindControl("cbcourse_type_etraining");
        CheckBox cbcourse_type_elearning = (CheckBox)fvCRUD.FindControl("cbcourse_type_elearning");
        TextBox txtscore_through_per = (TextBox)fvCRUD.FindControl("txtscore_through_per");
        if ((cbcourse_type_etraining.Checked == false) &&
            (cbcourse_type_elearning.Checked == false))
        {
            _Boolean = true;
            showAlert("กรุณาเลือกประเภท ClassRoom / E-Learning");
        }
        else if ((_func_dmu.zStringToDecimal(txtscore_through_per.Text) < 0) ||
            (_func_dmu.zStringToDecimal(txtscore_through_per.Text) > 100)
            )
        {
            _Boolean = true;
            showAlert("กรุณากรอกคะแนนผ่าน % ให้ถูกต้อง");
        }

        return _Boolean;
    }

    public void showAlert(string Str)
    {
        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('" + Str + "');", true);
    }
    private void zDelete(int id)
    {
        if (id == 0)
        {
            return;
        }
        course obj_course = new course();
        _data_elearning.el_course_action = new course[1];
        obj_course.u0_course_idx_ref = id;
        obj_course.course_updated_by = emp_idx;
        _data_elearning.el_course_action[0] = obj_course;
        _data_elearning = _func_dmu.zCallServiceNetwork(_urlDelel_u_course, _data_elearning);
    }

    protected void zSelect_Page1showdata(int id)
    {



    }

    private void CreateDs_Clone()
    {
        string sDs = "dsclone";
        string sVs = "vsclone";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);
        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("zId_G", typeof(String));
        ds.Tables[sDs].Columns.Add("zId", typeof(String));
        ds.Tables[sDs].Columns.Add("zName", typeof(String));
        ViewState[sVs] = ds;

    }
    private void zModeData(string _Mode)
    {
        _func_dmu.zModePanel(pnlAddDeptTraningList, _Mode);
        _func_dmu.zModePanel(pnlAddtest, _Mode);
        _func_dmu.zModePanel(pnlAddEvaluation, _Mode);
        _func_dmu.zModeGridViewCol(GvDeptTraningList, _Mode, 5);
        //_func_dmu.zModeGridView(GvTest, _Mode, 7);
        _func_dmu.zModeGridViewCol(GvEvaluation, _Mode, 1);
    }
    private void zShowdataUpdate(int id, string _Mode)
    {
        fv_Update.ChangeMode(FormViewMode.Edit);
        data_elearning dataelearning = new data_elearning();
        dataelearning.el_course_action = new course[1];
        course obj = new course();
        obj.u0_course_idx_ref = id;
        obj.operation_status_id = "U0-FULL";
        dataelearning.el_course_action[0] = obj;
        // litDebug.Text = _func_dmu.zJson(_urlGetel_u_course, dataelearning);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(dataelearning));

        dataelearning = _func_dmu.zCallServiceNetwork(_urlGetel_u_course, dataelearning);


        _func_dmu.zSetFormViewData(fv_Update, dataelearning.el_course_action);


        TextBox txtcourse_no = (TextBox)fv_Update.FindControl("txtcourse_no");
        DropDownList ddlm0_training_group_idx_ref = (DropDownList)fv_Update.FindControl("ddlm0_training_group_idx_ref");
        DropDownList ddm0_training_branch_idx = (DropDownList)fv_Update.FindControl("ddm0_training_branch_idx");
        TextBox txtcourse_name = (TextBox)fv_Update.FindControl("txtcourse_name");
        TextBox txtcourse_date = (TextBox)fv_Update.FindControl("txtcourse_date");
        TextBox txtcourse_remark = (TextBox)fv_Update.FindControl("txtcourse_remark");
        DropDownList ddllevel_code = (DropDownList)fv_Update.FindControl("ddllevel_code");
        TextBox txtcourse_score = (TextBox)fv_Update.FindControl("txtcourse_score");
        GridView GvCourse = (GridView)fv_Update.FindControl("GvCourse");
        GridView GvM0_EmpTarget = (GridView)fv_Update.FindControl("GvM0_EmpTarget");
        CheckBox cbcourse_type_etraining = (CheckBox)fv_Update.FindControl("cbcourse_type_etraining");
        CheckBox cbcourse_type_elearning = (CheckBox)fv_Update.FindControl("cbcourse_type_elearning");
        DropDownList ddlcourse_priority = (DropDownList)fv_Update.FindControl("ddlcourse_priority");
        DropDownList ddlcourse_status = (DropDownList)fv_Update.FindControl("ddlcourse_status");
        _ddlcourse_assessment = (DropDownList)fv_Update.FindControl("ddlcourse_assessment");

        //Start SetMode

        _func_dmu.zModeTextBox(txtcourse_name, _Mode);
        _func_dmu.zModeTextBox(txtcourse_date, _Mode);
        _func_dmu.zModeTextBox(txtcourse_remark, _Mode);
        _func_dmu.zModeTextBox(txtcourse_score, _Mode);
        _func_dmu.zModeDropDownList(ddlm0_training_group_idx_ref, _Mode);
        _func_dmu.zModeDropDownList(ddm0_training_branch_idx, _Mode);
        _func_dmu.zModeDropDownList(ddllevel_code, _Mode);
        _func_dmu.zModeDropDownList(ddlcourse_priority, _Mode);
        _func_dmu.zModeDropDownList(ddlcourse_status, _Mode);
        _func_dmu.zModeDropDownList(_ddlcourse_assessment, _Mode);

        _func_dmu.zModeCheckBox(cbcourse_type_etraining, _Mode);
        _func_dmu.zModeCheckBox(cbcourse_type_elearning, _Mode);

        _func_dmu.zModeGridViewCol(GvM0_EmpTarget, _Mode, 0);
        _func_dmu.zModeGridViewCol(GvTest, _Mode, 8);

        //_func_dmu.zModeLinkButton(btnSaveUpdate, _Mode);

        _func_dmu.zModeGridViewCol(GvCourse, _Mode, 0);
        zModeData(_Mode);
        //End SetMode

        Hddfld_course_no.Value = "";
        Hddfld_u0_course_idx.Value = "";
        if (dataelearning.el_course_action != null)
        {

            obj = dataelearning.el_course_action[0];
            Hddfld_course_no.Value = obj.course_no;
            Hddfld_u0_course_idx.Value = obj.u0_course_idx.ToString();
            cbcourse_type_etraining.Checked = _func_dmu.zIntToBoolean(obj.course_type_etraining);
            cbcourse_type_elearning.Checked = _func_dmu.zIntToBoolean(obj.course_type_elearning);

            _func_dmu.zDropDownList(ddllevel_code,
                                "",
                                "level_code",
                                "level_code",
                                "0",
                                "trainingLoolup",
                                obj.level_code.ToString(),
                                "LEVEL"

                                );

            _func_dmu.zDropDownList(ddlm0_training_group_idx_ref,
                                    "",
                                    "m0_training_group_idx",
                                    "training_group_name",
                                    "0",
                                    "trainingLoolup",
                                    obj.m0_training_group_idx_ref.ToString(),
                                    "TRN-GROUP"
                                    );
            _func_dmu.zDropDownListWhereID(ddm0_training_branch_idx,
                                                "",
                                                "m0_training_branch_idx",
                                                "training_branch_name",
                                                "0",
                                                "trainingLoolup",
                                                obj.m0_training_branch_idx_ref.ToString(),
                                                "TRN-BRANCH",
                                                "m0_training_group_idx_ref",
                                                _func_dmu.zStringToInt(ddlm0_training_group_idx_ref.SelectedValue)
                                                );

            _func_dmu.zGetOrganizationList(ddlOrg_search);
            _func_dmu.zClearDataDropDownList(ddlDept_search);
            _func_dmu.zClearDataDropDownList(ddlSec_search);

            data_elearning dataelearning_u4 = new data_elearning();
            trainingLoolup obj_u4 = new trainingLoolup();
            dataelearning_u4.trainingLoolup_action = new trainingLoolup[1];
            obj_u4.idx = int.Parse(Hddfld_u0_course_idx.Value);
            if (_Mode == "P")
            {
                obj_u4.operation_status_id = "TRN-COURSE";
            }
            else
            {
                obj_u4.operation_status_id = "TRN-COURSE-WHEREID";
                obj_u4.zId_G = _func_dmu.zStringToInt(ddlm0_training_group_idx_ref.SelectedValue);
                obj_u4.idx1 = _func_dmu.zStringToInt(ddm0_training_branch_idx.SelectedValue);
            }

            dataelearning_u4.trainingLoolup_action[0] = obj_u4;
            dataelearning_u4 = _func_dmu.zCallServiceNetwork(_func_dmu._urlGetel_lu_TraningAll, dataelearning_u4);
            CreateDs_Clone();
            DataSet Ds = (DataSet)ViewState["vsclone"];
            DataRow dr;
            int ic = 0;
            if (dataelearning_u4.trainingLoolup_action != null)
            {
                foreach (var item in dataelearning_u4.trainingLoolup_action)
                {
                    dr = Ds.Tables["dsclone"].NewRow();
                    dr["id"] = item.zId_G.ToString();
                    dr["zId"] = item.zId.ToString();
                    dr["zName"] = item.zName;
                    Ds.Tables["dsclone"].Rows.Add(dr);
                    ic++;
                }
            }

            ViewState["vsclone"] = Ds;
            _func_dmu.zSetGridData(GvCourse, Ds.Tables["dsclone"]);

            data_elearning dataelearning_u5 = new data_elearning();
            trainingLoolup obj_u5 = new trainingLoolup();
            dataelearning_u5.trainingLoolup_action = new trainingLoolup[1];
            obj_u5.idx = int.Parse(Hddfld_u0_course_idx.Value);
            if (_Mode == "P")
            {
                obj_u5.operation_status_id = "TRN-COURSE-U5-P";
            }
            else
            {
                obj_u5.operation_status_id = "TRN-COURSE-U5";
            }

            dataelearning_u5.trainingLoolup_action[0] = obj_u5;
            dataelearning_u5 = _func_dmu.zCallServiceNetwork(_func_dmu._urlGetel_lu_TraningAll, dataelearning_u5);

            CreateDs_Clone();
            Ds = (DataSet)ViewState["vsclone"];
            ic = 0;
            if (dataelearning_u5.trainingLoolup_action != null)
            {
                foreach (var item in dataelearning_u5.trainingLoolup_action)
                {
                    dr = Ds.Tables["dsclone"].NewRow();
                    dr["id"] = item.zId_G.ToString();
                    dr["zId"] = item.zId.ToString();
                    dr["zName"] = item.zName;
                    Ds.Tables["dsclone"].Rows.Add(dr);
                    ic++;
                }
            }

            ViewState["vsclone"] = Ds;
            _func_dmu.zSetGridData(GvM0_EmpTarget, Ds.Tables["dsclone"]);


            setGvEmpTarget_searchList();


            data_elearning dataelearning_u1 = new data_elearning();
            course obj_u1 = new course();
            dataelearning_u1.el_course_action = new course[1];
            obj_u1.u0_course_idx_ref = id;
            obj_u1.operation_status_id = "U1";
            dataelearning_u1.el_course_action[0] = obj_u1;
            dataelearning_u1 = _func_dmu.zCallServiceNetwork(_urlGetel_u_course, dataelearning_u1);

            CreateDs_el_u1_course();
            DataSet dsu1 = (DataSet)ViewState["vsel_u1_course"];
            DataRow dru1;
            ic = 0;
            int icount = 0;
            if (dataelearning_u1.el_course_action != null)
            {
                foreach (var item in dataelearning_u1.el_course_action)
                {

                    dru1 = dsu1.Tables["dsel_u1_course"].NewRow();
                    dru1["id"] = "1";
                    dru1["org_idx_ref"] = item.org_idx_ref.ToString();
                    dru1["org_name_th"] = item.org_name_th;
                    dru1["RDeptID_ref"] = item.RDeptID_ref.ToString();
                    dru1["dept_name_th"] = item.dept_name_th;
                    dru1["RSecID_ref"] = item.RSecID_ref.ToString();
                    dru1["sec_name_th"] = item.sec_name_th;
                    dru1["TIDX_ref"] = item.TIDX_ref.ToString();
                    dru1["target_name"] = item.target_name;
                    dsu1.Tables["dsel_u1_course"].Rows.Add(dru1);

                    icount++;

                    ic++;
                }
            }

            ViewState["vsel_u1_course"] = dsu1;
            _func_dmu.zSetGridData(GvDeptTraningList, dsu1.Tables["dsel_u1_course"]);

            //แบบสดสอบ
            data_elearning dataelearning_u2 = new data_elearning();
            course obj_u2 = new course();
            dataelearning_u2.el_course_action = new course[1];
            obj_u2.u0_course_idx_ref = int.Parse(Hddfld_u0_course_idx.Value);
            obj_u2.operation_status_id = "U2";
            dataelearning_u2.el_course_action[0] = obj_u2;
            dataelearning_u2 = _func_dmu.zCallServiceNetwork(_urlGetel_u_course, dataelearning_u2);
            // litDebug.Text = _func_dmu.zJson(_urlGetel_u_course, dataelearning_u2);
            CreateDs_dstest();
            DataSet Dsu2 = (DataSet)ViewState["vsel_u2_course"];
            DataRow dru2;
            ic = 0;
            if (dataelearning_u2.el_course_action != null)
            {
                foreach (var item in dataelearning_u2.el_course_action)
                {
                    dru2 = Dsu2.Tables["dsel_u2_course"].NewRow();
                    dru2["docno_bin"] = "";
                    dru2["course_item"] = item.course_item.ToString();
                    dru2["course_proposition"] = item.course_proposition;
                    dru2["course_propos_a"] = item.course_propos_a;
                    dru2["course_propos_b"] = item.course_propos_b;
                    dru2["course_propos_c"] = item.course_propos_c;
                    dru2["course_propos_d"] = item.course_propos_d;
                    dru2["course_propos_answer"] = item.course_propos_answer;
                    dru2["course_proposition_img"] = item.course_proposition_img;
                    dru2["course_propos_a_img"] = item.course_propos_a_img;
                    dru2["course_propos_b_img"] = item.course_propos_b_img;
                    dru2["course_propos_c_img"] = item.course_propos_c_img;
                    dru2["course_propos_d_img"] = item.course_propos_d_img;
                    Dsu2.Tables["dsel_u2_course"].Rows.Add(dru2);
                    ic++;
                }
            }

            ViewState["vsel_u2_course"] = Dsu2;
            _func_dmu.zSetGridData(GvTest, Dsu2.Tables["dsel_u2_course"]);

            //แบบประเมิน

            data_elearning dataelearning_u3 = new data_elearning();
            course obj_u3 = new course();
            dataelearning_u3.el_course_action = new course[1];
            obj_u3.u0_course_idx_ref = int.Parse(Hddfld_u0_course_idx.Value);
            obj_u3.operation_status_id = "U3";
            dataelearning_u3.el_course_action[0] = obj_u3;
            dataelearning_u3 = _func_dmu.zCallServiceNetwork(_urlGetel_u_course, dataelearning_u3);

            CreateDs_dsevaluation();
            DataSet Dsu3 = (DataSet)ViewState["vsEvaluation"];
            DataRow dru3;
            if (dataelearning_u3.el_course_action != null)
            {

                foreach (var item in dataelearning_u3.el_course_action)
                {
                    dru3 = Dsu3.Tables["dsEvaluation"].NewRow();
                    dru3["id"] = "1";
                    dru3["evaluation_group_idx"] = item.m0_evaluation_group_idx_ref.ToString();
                    dru3["evaluation_group_name"] = item.evaluation_group_name_th;
                    dru3["evaluation_f_idx"] = item.m0_evaluation_f_idx_ref.ToString();
                    dru3["evaluation_f_name"] = item.course_evaluation_f_name_th;
                    Dsu3.Tables["dsEvaluation"].Rows.Add(dru3);
                }

            }
            ViewState["vsEvaluation"] = Dsu3;
            _func_dmu.zSetGridData(GvEvaluation, Dsu3.Tables["dsEvaluation"]);

        }

    }

    private Boolean zSaveUpdate(int id)
    {
        Boolean _Boolean = false;
        // U0
        TextBox txtcourse_no = (TextBox)fv_Update.FindControl("txtcourse_no");
        DropDownList ddlm0_training_group_idx_ref = (DropDownList)fv_Update.FindControl("ddlm0_training_group_idx_ref");
        DropDownList ddm0_training_branch_idx = (DropDownList)fv_Update.FindControl("ddm0_training_branch_idx");
        TextBox txtcourse_name = (TextBox)fv_Update.FindControl("txtcourse_name");
        TextBox txtcourse_date = (TextBox)fv_Update.FindControl("txtcourse_date");
        TextBox txtcourse_remark = (TextBox)fv_Update.FindControl("txtcourse_remark");
        DropDownList ddllevel_code = (DropDownList)fv_Update.FindControl("ddllevel_code");
        TextBox txtcourse_score = (TextBox)fv_Update.FindControl("txtcourse_score");
        CheckBox cbcourse_type_etraining = (CheckBox)fv_Update.FindControl("cbcourse_type_etraining");
        CheckBox cbcourse_type_elearning = (CheckBox)fv_Update.FindControl("cbcourse_type_elearning");
        DropDownList ddlcourse_priority = (DropDownList)fv_Update.FindControl("ddlcourse_priority");
        DropDownList ddlcourse_status = (DropDownList)fv_Update.FindControl("ddlcourse_status");
        GridView GvCourse = (GridView)fv_Update.FindControl("GvCourse");
        GridView GvM0_EmpTarget = (GridView)fv_Update.FindControl("GvM0_EmpTarget");
        _ddlcourse_assessment = (DropDownList)fv_Update.FindControl("ddlcourse_assessment");
        TextBox txtscore_through_per = (TextBox)fv_Update.FindControl("txtscore_through_per");

        int idx = 0, icourse_score = 0, u0_course_idx_ref = 0;
        u0_course_idx_ref = id;
        course obj_course = new course();
        _data_elearning.el_course_action = new course[1];
        obj_course.u0_course_idx_ref = id;
        obj_course.course_date = _func_dmu.zDateToDB(txtcourse_date.Text);
        obj_course.m0_training_group_idx_ref = int.Parse(ddlm0_training_group_idx_ref.SelectedValue);
        obj_course.m0_training_branch_idx_ref = int.Parse(ddm0_training_branch_idx.SelectedValue);
        obj_course.course_name = txtcourse_name.Text;
        obj_course.course_remark = txtcourse_remark.Text;
        obj_course.level_code = int.Parse(ddllevel_code.SelectedValue);
        obj_course.course_priority = int.Parse(ddlcourse_priority.SelectedValue);
        obj_course.course_status = int.Parse(ddlcourse_status.SelectedValue);
        obj_course.course_assessment = _func_dmu.zStringToInt(_ddlcourse_assessment.SelectedValue);
        obj_course.score_through_per = _func_dmu.zStringToDecimal(txtscore_through_per.Text);

        if (txtcourse_score.Text != "")
        {
            icourse_score = int.Parse(txtcourse_score.Text);
        }
        obj_course.course_score = icourse_score;
        obj_course.course_updated_by = emp_idx;
        obj_course.course_type_etraining = _func_dmu.zBooleanToInt(cbcourse_type_etraining.Checked);
        obj_course.course_type_elearning = _func_dmu.zBooleanToInt(cbcourse_type_elearning.Checked);
        obj_course.course_plan_status = "I";

        //node
        obj_course.approve_status = 0;
        obj_course.u0_idx = 5;
        obj_course.node_idx = 9;
        obj_course.actor_idx = 1;
        obj_course.app_flag = 0;
        obj_course.app_user = 0;

        obj_course.operation_status_id = "U0";
        _data_elearning.el_course_action[0] = obj_course;
        _data_elearning = _func_dmu.zCallServicePostNetwork(_urlSetUpdel_u_course, _data_elearning);

        if (u0_course_idx_ref > 0)
        {

            zSaveDetail(u0_course_idx_ref, txtcourse_no.Text, "E");

        }
        _Boolean = true;
        return _Boolean;

    }
    public Boolean checkErrorUpdate()
    {
        Boolean _Boolean = false;

        CheckBox cbcourse_type_etraining = (CheckBox)fv_Update.FindControl("cbcourse_type_etraining");
        CheckBox cbcourse_type_elearning = (CheckBox)fv_Update.FindControl("cbcourse_type_elearning");
        TextBox txtscore_through_per = (TextBox)fv_Update.FindControl("txtscore_through_per");
        if ((cbcourse_type_etraining.Checked == false) &&
            (cbcourse_type_elearning.Checked == false))
        {
            _Boolean = true;
            showAlert("กรุณาเลือกประเภท ClassRoom / E-Learning");
        }
        else if ((_func_dmu.zStringToDecimal(txtscore_through_per.Text) < 0) ||
            (_func_dmu.zStringToDecimal(txtscore_through_per.Text) > 100)
            )
        {
            _Boolean = true;
            showAlert("กรุณากรอกคะแนนผ่าน % ให้ถูกต้อง");
        }
        return _Boolean;
    }

    private void CreateDs_el_u5_course()
    {
        string sDs = "dsel_u5_course";
        string sVs = "vsel_u5_course";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);
        ds.Tables[sDs].Columns.Add("u5_course_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_course_idx_ref", typeof(String));
        ds.Tables[sDs].Columns.Add("TIDX_ref", typeof(String));
        ds.Tables[sDs].Columns.Add("TIDX_flag", typeof(String));
        ds.Tables[sDs].Columns.Add("course_updated_by", typeof(String));
        ViewState[sVs] = ds;

    }
    private void setGvEmpTarget_search()
    {
        GridView GvM0_EmpTarget;
        if ((Hddfld_course_no.Value == "") || (Hddfld_course_no.Value == null))
        {
            GvM0_EmpTarget = (GridView)fvCRUD.FindControl("GvM0_EmpTarget");
        }
        else
        {
            GvM0_EmpTarget = (GridView)fv_Update.FindControl("GvM0_EmpTarget");
        }
        int ic = 0, id = 0;
        CreateDs_Clone();
        DataSet Ds = (DataSet)ViewState["vsclone"];
        DataRow dr;
        foreach (var item in GvM0_EmpTarget.Rows)
        {
            CheckBox GvM0_EmpTarget_cb_zId = (CheckBox)GvM0_EmpTarget.Rows[ic].FindControl("GvM0_EmpTarget_cb_zId");
            if (GvM0_EmpTarget_cb_zId.Checked == true)
            {
                id = 1;
            }
            else
            {
                id = 0;
            }
            dr = Ds.Tables["dsclone"].NewRow();
            dr["id"] = id.ToString();
            Label GvM0_EmpTarget_zId = (Label)GvM0_EmpTarget.Rows[ic].FindControl("GvM0_EmpTarget_zId");
            Label GvM0_EmpTarget_zName = (Label)GvM0_EmpTarget.Rows[ic].FindControl("GvM0_EmpTarget_zName");
            dr["zId"] = GvM0_EmpTarget_zId.Text;
            dr["zName"] = GvM0_EmpTarget_zName.Text;
            Ds.Tables["dsclone"].Rows.Add(dr);
            ic++;
        }
        ViewState["vsclone"] = Ds;
        _func_dmu.zSetGridData(GvM0_EmpTarget_search, Ds.Tables["dsclone"]);
    }
    private void setGvEmpTarget_searchList()
    {
        data_elearning dataelearning_u4 = new data_elearning();
        trainingLoolup obj_u4 = new trainingLoolup();
        dataelearning_u4.trainingLoolup_action = new trainingLoolup[1];
        obj_u4.idx = 0;
        obj_u4.operation_status_id = "EMP-TARGET";
        dataelearning_u4.trainingLoolup_action[0] = obj_u4;
        dataelearning_u4 = _func_dmu.zCallServiceNetwork(_func_dmu._urlGetel_lu_TraningAll, dataelearning_u4);
        CreateDs_Clone();
        DataSet Ds = (DataSet)ViewState["vsclone"];
        DataRow dr;
        int ic = 0;
        if (dataelearning_u4.trainingLoolup_action != null)
        {
            foreach (var item in dataelearning_u4.trainingLoolup_action)
            {
                dr = Ds.Tables["dsclone"].NewRow();
                dr["id"] = "0";
                dr["zId"] = item.zId.ToString();
                dr["zName"] = item.zName;
                Ds.Tables["dsclone"].Rows.Add(dr);
                ic++;
            }
        }

        ViewState["vsclone"] = Ds;
        _func_dmu.zSetGridData(GvM0_EmpTarget_search, Ds.Tables["dsclone"]);
    }
    private void searchCOURSEList()
    {
        GridView GvCourse;
        TextBox txtsearch_GvCourse;
        DropDownList ddlm0_training_group_idx_ref;
        DropDownList ddm0_training_branch_idx;

        data_elearning dataelearning_u4 = new data_elearning();
        trainingLoolup obj_u4 = new trainingLoolup();
        dataelearning_u4.trainingLoolup_action = new trainingLoolup[1];
        if ((Hddfld_course_no.Value == "") || (Hddfld_course_no.Value == null))
        {
            GvCourse = (GridView)fvCRUD.FindControl("GvCourse");
            txtsearch_GvCourse = (TextBox)fvCRUD.FindControl("txtsearch_GvCourse");
            ddlm0_training_group_idx_ref = (DropDownList)fvCRUD.FindControl("ddlm0_training_group_idx_ref");
            ddm0_training_branch_idx = (DropDownList)fvCRUD.FindControl("ddm0_training_branch_idx");

            obj_u4.operation_status_id = "TRN-COURSE-LIST";
            obj_u4.fil_Search = txtsearch_GvCourse.Text;
            obj_u4.zId_G = _func_dmu.zStringToInt(ddlm0_training_group_idx_ref.SelectedValue);
            obj_u4.idx1 = _func_dmu.zStringToInt(ddm0_training_branch_idx.SelectedValue);
        }
        else
        {
            GvCourse = (GridView)fv_Update.FindControl("GvCourse");
            txtsearch_GvCourse = (TextBox)fv_Update.FindControl("txtsearch_GvCourse");
            ddlm0_training_group_idx_ref = (DropDownList)fv_Update.FindControl("ddlm0_training_group_idx_ref");
            ddm0_training_branch_idx = (DropDownList)fv_Update.FindControl("ddm0_training_branch_idx");

            obj_u4.operation_status_id = "TRN-COURSE-WHEREID";
            obj_u4.idx = int.Parse(Hddfld_u0_course_idx.Value);
            obj_u4.fil_Search = txtsearch_GvCourse.Text;
            obj_u4.zId_G = _func_dmu.zStringToInt(ddlm0_training_group_idx_ref.SelectedValue);
            obj_u4.idx1 = _func_dmu.zStringToInt(ddm0_training_branch_idx.SelectedValue);

        }

        dataelearning_u4.trainingLoolup_action[0] = obj_u4;
        dataelearning_u4 = _func_dmu.zCallServiceNetwork(_func_dmu._urlGetel_lu_TraningAll, dataelearning_u4);
        CreateDs_Clone();
        DataSet Ds = (DataSet)ViewState["vsclone"];
        DataRow dr;
        int ic = 0;
        if (dataelearning_u4.trainingLoolup_action != null)
        {
            foreach (var item in dataelearning_u4.trainingLoolup_action)
            {
                dr = Ds.Tables["dsclone"].NewRow();
                dr["id"] = item.zId_G.ToString();
                dr["zId"] = item.zId.ToString();
                dr["zName"] = item.zName;
                Ds.Tables["dsclone"].Rows.Add(dr);
                ic++;
            }
        }

        ViewState["vsclone"] = Ds;
        _func_dmu.zSetGridData(GvCourse, Ds.Tables["dsclone"]);
    }
    private int zUploadImagesBin(FileUpload _FileUpload, string sDocNo, int id, int item)
    {
        string _itemExtension = "";
        string _itemNameNew = "";
        string _itemFilePath = "";
        string newFilePath = "";
        string OldFilePath = "";
        int _int = 0;
        string _PathFile = ConfigurationSettings.AppSettings["path_flie_elearning"];

        OldFilePath = _PathFile + _Folder_courseBin + "/" + Hddfld_folderImage.Value;

        //if (_FileUpload.HasFile)
        //{
        if (Directory.Exists(Server.MapPath(_PathFile)))
        {
            _PathFile = _PathFile + _Folder_courseBin + "/" + sDocNo + "/" + id.ToString() + "/";
            newFilePath = _PathFile;

            if ((id > 0) && (item > 0) && (sDocNo != ""))
            {

                _itemExtension = ".png";

                //if (_FileUpload.HasFile)
                //{
                _itemNameNew = item.ToString() + _itemExtension.ToLower();
                //}

                //newFilePath = _PathFile +
                //                    _itemNameNew;

                OldFilePath = OldFilePath + "/" + _itemNameNew;
                newFilePath = newFilePath + "/" + _itemNameNew;
                Directory.CreateDirectory(Server.MapPath(_PathFile));
                try
                {
                    File.Move(Server.MapPath(OldFilePath), Server.MapPath(newFilePath));
                    _int = 1;
                }
                catch { }


                //if (_FileUpload.HasFile)
                //{
                //    try
                //    {
                //        Directory.Delete(Server.MapPath(newFilePath));
                //    }
                //    catch { }
                //    _itemFilePath = Server.MapPath(newFilePath);
                //    _FileUpload.SaveAs(_itemFilePath);
                //    _int = 1;
                //}

            }
        }
        //}


        return _int;

    }

    private void zUploadImagesElearning(string sDocNoBin, string sDocNoEl, int id)
    {

        if ((sDocNoBin != "") && (sDocNoEl != "") && (id > 0))
        {
            string _PathFile = ConfigurationSettings.AppSettings["path_flie_elearning"];
            string _itemExtension = "";
            string _itemNameNew = "";
            string _itemFilePath = "";
            string OldFilePath = "";
            string newFilePath = "";
            _itemExtension = ".png";

            OldFilePath = _PathFile + _Folder_courseBin + "/" + sDocNoBin + "/" + id.ToString() + "/";
            //OldFilePath = OldFilePath + _itemNameNew;

            newFilePath = _PathFile + _Folder_coursetestimg + "/" + sDocNoEl + "/" + id.ToString() + "/";
            //newFilePath = newFilePath + _itemNameNew;
            try
            {
                Directory.CreateDirectory(Server.MapPath(newFilePath));
            }
            catch { }
            string _folder = OldFilePath;
            for (int i = 1; i <= 5; i++)
            {
                _itemNameNew = i.ToString() + _itemExtension.ToLower();
                string _OldFilePath, _newFilePath;
                _OldFilePath = OldFilePath + _itemNameNew;
                _newFilePath = newFilePath + _itemNameNew;


                try
                {
                    Directory.Delete(Server.MapPath(_newFilePath));
                }
                catch { }

                try
                {
                    File.Move(Server.MapPath(_OldFilePath), Server.MapPath(_newFilePath));
                }
                catch { }
            }
            try
            {
                Directory.Delete(Server.MapPath(_folder));
            }
            catch { }
        }
    }

    private void zDelImagesBin(string sDocNoBin)
    {
        if (sDocNoBin != "")
        {
            string _PathFile = ConfigurationSettings.AppSettings["path_flie_elearning"];
            string OldFilePath = "";
            OldFilePath = _PathFile + _Folder_courseBin + "/" + sDocNoBin + "/";
            try
            {
                Directory.Delete(Server.MapPath(OldFilePath));
            }
            catch { }
            try
            {
                Directory.Delete(OldFilePath);
            }
            catch { }
        }

    }

    private void ExportGridToExcel(GridView _gv, string sfilename = "ExportToExcel", string ATitle = "Export To Excel")
    {
        if (_gv.Rows.Count == 0)
        {
            return;
        }
        //_gv.AllowPaging = false;
        //_gv.DataBind();
        var totalCols = _gv.Rows[0].Cells.Count;
        var totalRows = _gv.Rows.Count;
        var headerRow = _gv.HeaderRow;

        DataTable tableMaterialLog = new DataTable();

        for (var i = 1; i <= totalCols; i++)
        {
            tableMaterialLog.Columns.Add(headerRow.Cells[i - 1].Text, typeof(String));
        }
        for (var j = 1; j <= totalRows; j++)
        {
            DataRow addMaterialLogRow = tableMaterialLog.NewRow();
            for (var i = 1; i <= totalCols; i++)
            {
                string sdata = ((Label)_gv.Rows[j - 1].Cells[i - 1].Controls[1]).Text;
                addMaterialLogRow[i - 1] = sdata;
            }
            tableMaterialLog.Rows.Add(addMaterialLogRow);
        }
        GridView gv = new GridView();
        gv.DataSource = tableMaterialLog;
        gv.DataBind();
        WriteExcelWithNPOI(tableMaterialLog, "xls", sfilename, ATitle);

    }
    public void WriteExcelWithNPOI(DataTable dt, String extension, String fileName, string ATitle)
    {
        IWorkbook workbook;
        if (extension == "xlsx")
        {
            workbook = new XSSFWorkbook();
        }
        else if (extension == "xls")
        {
            workbook = new HSSFWorkbook();
        }
        else
        {
            throw new Exception("This format is not supported");
        }
        ISheet sheet1 = workbook.CreateSheet("Sheet 1");

        IRow row1 = sheet1.CreateRow(0);

        ICell cellTitle = row1.CreateCell(0);
        String columnNameTitle = ATitle;
        cellTitle.SetCellValue(columnNameTitle);


        row1 = sheet1.CreateRow(1);
        cellTitle = row1.CreateCell(0);
        columnNameTitle = "Print Date : " + DateTime.Now.ToString("dd/MM/yyyy", CultureInfo.InstalledUICulture);
        cellTitle.SetCellValue(columnNameTitle);

        row1 = sheet1.CreateRow(3);
        for (int j = 0; j < dt.Columns.Count; j++)
        {
            ICell cell = row1.CreateCell(j);
            String columnName = dt.Columns[j].ToString();
            cell.SetCellValue(columnName);
        }
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            IRow row = sheet1.CreateRow(i + 4);
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                ICell cell = row.CreateCell(j);
                String columnName = dt.Columns[j].ToString();
                cell.SetCellValue(dt.Rows[i][columnName].ToString());
            }
        }
        using (var exportData = new MemoryStream())
        {
            Response.Clear();
            workbook.Write(exportData);
            if (extension == "xlsx")
            {
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xlsx"));
                Response.BinaryWrite(exportData.ToArray());
            }
            else if (extension == "xls")
            {
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xls"));
                Response.BinaryWrite(exportData.GetBuffer());
            }
            Response.End();
        }
    }

    public void imageProcessRequest(HttpPostedFile _HttpPostedFile, int item)
    {

        string _PathFile = ConfigurationSettings.AppSettings["path_flie_elearning"];
        //if ((lbfolderImage.Text == null) || (lbfolderImage.Text == ""))
        //{
        // Hddfld_folderImage.Value = "fd_emp" + emp_idx.ToString() + DateTime.Now.ToString("yyyyMMdd");
        //}
        if (Directory.Exists(Server.MapPath(_PathFile)))
        {

            _PathFile = _PathFile + _Folder_courseBin + "/" + Hddfld_folderImage.Value;


            if (_HttpPostedFile != null && _HttpPostedFile.ContentLength > 0)
            {
                Directory.CreateDirectory(Server.MapPath(_PathFile));
                string _itemExtension = ".png";
                string _itemNameNew = item.ToString() + _itemExtension.ToLower();
                string _itemFilePath = "";
                string newFilePath = _PathFile + "/" + _itemNameNew;
                try
                {
                    Directory.Delete(Server.MapPath(newFilePath));
                }
                catch { }
                _itemFilePath = Server.MapPath(newFilePath);
                _HttpPostedFile.SaveAs(_itemFilePath);
                if (item == 1)
                {
                    // _img_proposition = (Image)fvCRUD.FindControl("img_proposition");
                    //img_proposition.ImageUrl = getImgUrlFDBin(newFilePath);
                    //  ImageButton1.ImageUrl = getImgUrlFDBin(newFilePath);
                }
            }
        }

    }
    public string getImgUrlFDBin(string AImage = "")
    {
        string sPathImage = "";
        if (AImage == "")
        {
            sPathImage = _PathFileimage + _func_dmu._IconnoImage;
        }
        else
        {
            sPathImage = AImage;
        }
        sPathImage = ResolveUrl(sPathImage);

        return sPathImage;
    }


    public string imageupload_del(string name)
    {
        string _PathFile = ConfigurationSettings.AppSettings["path_flie_elearning"];
        string _itemNameNew = "";
        if (name != "")
        {
            _itemNameNew = name + ".png";
        }
        else
        {
            name = "";
        }
        if (name != "")
        {
            //  Hddfld_folderImage.Value = "fd_emp" + emp_idx.ToString() + DateTime.Now.ToString("yyyyMMdd");
            //}
            if (Directory.Exists(Server.MapPath(_PathFile)))
            {

                _PathFile = _PathFile + _Folder_courseBin + "/" + Hddfld_folderImage.Value;

                string newFilePath = _PathFile + "/" + _itemNameNew;
                try
                {
                    Directory.Delete(Server.MapPath(newFilePath));
                }
                catch { }
                try
                {
                    File.Delete(Server.MapPath(newFilePath));
                }
                catch { }
            }
        }
        return name;
    }

}