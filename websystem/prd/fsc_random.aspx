<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="fsc_random.aspx.cs" Inherits="websystem_prd_fsc_random" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:HyperLink ID="hlSetTotop" runat="server"></asp:HyperLink>
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    <asp:MultiView ID="mvSystem" runat="server">
        <asp:View ID="viewList" runat="server">
            <div class="col-md-12">
                <div class="panel panel-success" ID="divRandom" runat="server">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            สุ่มรายชื่อในการทำแบบประเมิน
                        </h3>
                    </div>
                    <div class="panel-body">
                        <asp:FormView ID="fvRandom" runat="server" Width="100%" DefaultMode="Insert">
                            <InsertItemTemplate>
                                <div class="form-horizontal" role="form">
                                    <%-- <div class="form-group">
                                        <label class="col-md-3 control-label">ประเภทพนักงาน :</label>
                                        <div class="col-md-3 control-label">
                                            <asp:CheckBoxList ID="cblEmpTypeIdx" runat="server" RepeatLayout="Table"
                                                RepeatDirection="Horizontal">
                                                <asp:ListItem Value="1">&nbsp;รายวัน&nbsp;&nbsp;</asp:ListItem>
                                                <asp:ListItem Value="2">&nbsp;รายเดือน&nbsp;&nbsp;</asp:ListItem>
                                            </asp:CheckBoxList>
                                        </div>
                                        <label class="col-md-6"></label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Plant :</label>
                                        <div class="col-md-3 control-label">
                                            <asp:CheckBoxList ID="cblPlantIdx" runat="server" RepeatLayout="Table"
                                                RepeatDirection="Horizontal">
                                                <asp:ListItem Value="1">&nbsp;MTT&nbsp;&nbsp;</asp:ListItem>
                                                <asp:ListItem Value="2">&nbsp;NPW&nbsp;&nbsp;</asp:ListItem>
                                                <asp:ListItem Value="14">&nbsp;RJN&nbsp;&nbsp;</asp:ListItem>
                                            </asp:CheckBoxList>
                                        </div>
                                        <label class="col-md-6"></label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">สัญชาติ<span class="text-danger">
                                                :</label>
                                        <div class="col-md-3 control-label">
                                            <asp:CheckBoxList ID="cblNatIdx" runat="server" RepeatLayout="Table"
                                                RepeatDirection="Horizontal">
                                                <asp:ListItem Value="177">&nbsp;ไทย&nbsp;&nbsp;</asp:ListItem>
                                                <asp:ListItem Value="31">&nbsp;พม่า&nbsp;&nbsp;</asp:ListItem>
                                            </asp:CheckBoxList>
                                        </div>
                                        <label class="col-md-6"></label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">ฝ่าย<span class="text-danger">*</span>
                                            :</label>
                                        <div class="col-md-9 control-label textleft">
                                            <asp:CheckBoxList ID="cblRdeptIdx" runat="server" RepeatLayout="Table"
                                                RepeatDirection="Horizontal" RepeatColumns="2">
                                                <asp:ListItem>&nbsp;xxx&nbsp;&nbsp;</asp:ListItem>
                                                <asp:ListItem>&nbsp;xxx&nbsp;&nbsp;</asp:ListItem>
                                                <asp:ListItem>&nbsp;xxx&nbsp;&nbsp;</asp:ListItem>
                                                <asp:ListItem>&nbsp;xxx&nbsp;&nbsp;</asp:ListItem>
                                                <asp:ListItem>&nbsp;xxx&nbsp;&nbsp;</asp:ListItem>
                                                <asp:ListItem>&nbsp;xxx&nbsp;&nbsp;</asp:ListItem>
                                            </asp:CheckBoxList>
                                        </div>
                                    </div> --%>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"></label>
                                        <div class="col-md-3">
                                            <asp:LinkButton ID="lbRandom" runat="server" CssClass="btn btn-sm btn-primary"
                                                OnCommand="btnCommand" CommandName="cmdRandom">
                                                <i class="fas fa-random"></i>&nbsp;สุ่มรายชื่อ</asp:LinkButton>
                                            <asp:LinkButton ID="lbClear" runat="server" CssClass="btn btn-sm btn-info"
                                                OnCommand="btnCommand" CommandName="cmdClear" CommandArgument="0">
                                                <i class="fas fa-redo"></i>&nbsp;เคลียร์ค่า</asp:LinkButton>
                                        </div>
                                        <label class="col-md-6"></label>
                                    </div>
                                </div>
                            </InsertItemTemplate>
                        </asp:FormView>
                    </div>
                </div>

                <div class="panel panel-info" ID="divSearch" runat="server">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            ค้นหารายชื่อผู้ทำแบบประเมิน
                        </h3>
                    </div>
                    <div class="panel-body">
                        <asp:FormView ID="fvSearch" runat="server" Width="100%" DefaultMode="Insert">
                            <InsertItemTemplate>
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">รหัสพนักงาน :</label>
                                        <div class="col-md-3">
                                            <asp:TextBox ID="tbItemSearch" runat="server" CssClass="form-control"
                                                placeholder="ค้นหาโดยรหัสพนักงาน" MaxLength="8"></asp:TextBox>
                                        </div>
                                        <label class="col-md-6 control-label"></label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">ฝ่าย :</label>
                                        <div class="col-md-9 control-label textleft">
                                            <asp:CheckBoxList ID="cblRdeptIdxSearch" runat="server" RepeatLayout="Table"
                                                RepeatDirection="Horizontal" RepeatColumns="2">
                                                <asp:ListItem>&nbsp;xxx&nbsp;&nbsp;</asp:ListItem>
                                                <asp:ListItem>&nbsp;xxx&nbsp;&nbsp;</asp:ListItem>
                                                <asp:ListItem>&nbsp;xxx&nbsp;&nbsp;</asp:ListItem>
                                                <asp:ListItem>&nbsp;xxx&nbsp;&nbsp;</asp:ListItem>
                                                <asp:ListItem>&nbsp;xxx&nbsp;&nbsp;</asp:ListItem>
                                                <asp:ListItem>&nbsp;xxx&nbsp;&nbsp;</asp:ListItem>
                                            </asp:CheckBoxList>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"></label>
                                        <div class="col-md-6">
                                            <asp:LinkButton ID="lbItemSearch" runat="server" CssClass="btn btn-sm btn-primary"
                                                OnCommand="btnCommand" CommandName="cmdItemSearch"><i
                                                    class="fas fa-search"></i>&nbsp;ค้นหา</asp:LinkButton>
                                            <asp:LinkButton ID="lbItemClear" runat="server" CssClass="btn btn-sm btn-info"
                                                OnCommand="btnCommand" CommandName="cmdItemClear" CommandArgument="0"><i
                                                    class="fas fa-redo"></i>&nbsp;เคลียร์ค่า</asp:LinkButton>
                                            <asp:LinkButton ID="lbExport" runat="server" CssClass="btn btn-sm btn-success"
                                                OnCommand="btnCommand" CommandName="cmdItemExport" CommandArgument="0"><i
                                                    class="far fa-file-excel"></i>&nbsp;Export</asp:LinkButton>
                                        </div>
                                        <label class="col-md-3"></label>
                                    </div>
                                </div>
                            </InsertItemTemplate>
                        </asp:FormView>
                    </div>
                </div>

                <asp:GridView ID="gvDeptList" runat="server" AutoGenerateColumns="True"></asp:GridView>

                <asp:GridView ID="gvRandomList" runat="server" AutoGenerateColumns="False"
                    CssClass="table table-striped table-bordered table-responsive">
                    <HeaderStyle CssClass="info" Font-Size="Small" />
                    <RowStyle Font-Size="Small" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่พบข้อมูล</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="#">
                            <ItemTemplate>
                                <asp:Label ID="lblNo" runat="server" Text='<%# (Container.DataItemIndex +1) %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="รหัสพนักงาน">
                            <ItemTemplate>
                                <asp:HiddenField ID="hfEmpIdx" runat="server" Value='<%# Eval("emp_idx") %>'>
                                </asp:HiddenField>
                                <asp:Label ID="lbEmpCode" runat="server" Text='<%# Eval("emp_code") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ชื่อ">
                            <ItemTemplate>
                                <asp:Label ID="lblEmpNameTh" runat="server" Text='<%# Eval("emp_name_th") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ฝ่าย">
                            <ItemTemplate>
                                <asp:Label ID="lblDeptNameTh" runat="server" Text='<%# Eval("dept_name_th") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="แผนก">
                            <ItemTemplate>
                                <asp:Label ID="lblSecNameTh" runat="server" Text='<%# Eval("sec_name_th") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="สถานะแบบประเมิน">
                            <ItemTemplate>
                                <asp:Label ID="lblSurveyStatus" runat="server"
                                    Text='<%# ((int)Eval("survey_status") == 0) ? "รอดำเนินการ" : "ดำเนินการเรียบร้อย" %>'
                                    CssClass='<%# ((int)Eval("survey_status") == 0) ? "text-warning" : "text-success" %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="วันที่อัพเดท">
                            <ItemTemplate>
                                <asp:Label ID="lblUpdateDate" runat="server" Text='<%# Eval("update_date") %>'
                                    Visible='<%# ((int)Eval("survey_status") == 0) ? false : true %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <asp:LinkButton ID="lbSave" runat="server" CssClass="btn btn-sm btn-success" OnCommand="btnCommand"
                    CommandName="cmdSave" OnClientClick="return confirm('คุณต้องการบันทึกข้อมูลนี้ใช่หรือไม่')"
                    Visible="False"><i class="fas fa-save"></i>&nbsp;บันทึก</asp:LinkButton>
            </div>
        </asp:View>
    </asp:MultiView>
</asp:Content>