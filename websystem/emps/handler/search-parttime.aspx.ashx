﻿<%@ WebHandler Language="C#" Class="Handler" %>

using System;
using System.Web;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Globalization;
using System.Threading;

public class Handler : IHttpHandler
{

   public void ProcessRequest(HttpContext context)
   {
      string callback = context.Request.QueryString["callback"];
      string keyword = String.Empty;
      keyword = context.Request.QueryString["keyword"];
      string json = this.getParttime(keyword);
      if (!string.IsNullOrEmpty(callback))
      {
         json = string.Format("{0}({1});", callback, json);
      }
      context.Response.ContentType = "text/json";
      context.Response.Write(json);
   }

   private string getParttime(string keyword)
   {
      List<object> pt = new List<object>();
      using (SqlConnection conn = new SqlConnection())
      {
         conn.ConnectionString = ConfigurationManager.ConnectionStrings["conn_mas"].ConnectionString;
         using (SqlCommand cmd = new SqlCommand())
         {
            int statusDelete = 9;
            cmd.CommandText = "SELECT TOP 5 m0_parttime_idx, parttime_code, parttime_name_th, parttime_bg_color, parttime_status FROM emps_m0_parttime WHERE ";
            if (keyword != "")
            {
               cmd.CommandText += "(parttime_code LIKE '%' + @keyword + '%' OR parttime_name_th LIKE '%' +  @keyword + '%') AND parttime_status <> " + statusDelete;
               cmd.Parameters.AddWithValue("@keyword", keyword);
            }
            else if (keyword == "")
            {
               cmd.CommandText += "parttime_status <> " + statusDelete;
            }
            cmd.Connection = conn;
            conn.Open();
            using (SqlDataReader sdr = cmd.ExecuteReader())
            {
               while (sdr.Read())
               {
                  pt.Add(new
                  {
                     m0_parttime_idx = sdr["m0_parttime_idx"],
                     parttime_code = sdr["parttime_code"],
                     parttime_name_th = sdr["parttime_name_th"],
                     parttime_bg_color = sdr["parttime_bg_color"]
                  });
               }
            }
            conn.Close();
         }
         return (new JavaScriptSerializer().Serialize(pt));
      }
   }

   public bool IsReusable
   {
      get
      {
         return false;
      }
   }
}
