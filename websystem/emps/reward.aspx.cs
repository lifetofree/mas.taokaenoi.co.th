﻿using System;
using System.Globalization;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_emps_reward : System.Web.UI.Page
{
   #region Init
   data_emps dataEmps = new data_emps();
   service_execute servExec = new service_execute();
   function_tool _funcTool = new function_tool();
   string centralizedConn = "conn_centralized";
   string masConn = "conn_mas";
   string empsRewardService = "empsRewardService";
   string empsodspeService = "empsodspeService";
   string _local_xml = null;
   #endregion Init

   #region Constant
   public static class Constants
   {
      #region for region Action
      public const int SELECT_ALL = 20;
      public const int CREATE = 10;
      public const int UPDATE = 11;
      public const int SELECT_WHERE = 21;
      public const int BAN = 30;
      public const int UNBAN = 31;
      #endregion for region Action
   }
   #endregion Constant

   #region Page Load
   protected void Page_Load(object sender, EventArgs e)
   {
      if (!IsPostBack)
      {
         getEmpLogin(int.Parse(Session["emp_idx"].ToString()));
         actionIndex();
      }
   }
   #endregion Page Load

   #region Action
   protected void actionIndex()
   {
      rewards objReward = new rewards();
      dataEmps.emps_reward_action = new rewards[1];
      dataEmps.emps_reward_action[0] = objReward;
      _local_xml = servExec.actionExec(masConn, "data_emps", empsRewardService, dataEmps, Constants.SELECT_ALL);
      dataEmps = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml);
      setGridData(GvMaster, dataEmps.emps_reward_action);
   }

   protected void actionCreate()
   {
      rewards objReward = new rewards();
      dataEmps.emps_reward_action = new rewards[1];
      objReward.reward_name = txtRewardName.Text;
      objReward.reward_value = float.Parse(txtRewardValue.Text);
      objReward.reward_status = int.Parse(ddlRewardStatus.SelectedValue);
      objReward.reward_created_by = int.Parse(ViewState["EmpIDX"].ToString());
      objReward.reward_updated_by = int.Parse(ViewState["EmpIDX"].ToString());
      dataEmps.emps_reward_action[0] = objReward;
      servExec.actionExec(masConn, "data_emps", empsRewardService, dataEmps, Constants.CREATE);
   }

   protected void actionUpdate(int m0_reward_idx, string reward_name, string reward_value, int reward_status)
   {
      rewards objReward = new rewards();
      dataEmps.emps_reward_action = new rewards[1];
      objReward.m0_reward_idx = m0_reward_idx;
      objReward.reward_name = reward_name;
      objReward.reward_value = float.Parse(reward_value);
      objReward.reward_status = reward_status;
      objReward.reward_updated_by = int.Parse(ViewState["EmpIDX"].ToString());
      dataEmps.emps_reward_action[0] = objReward;
      servExec.actionExec(masConn, "data_emps", empsRewardService, dataEmps, Constants.UPDATE);
   }

   protected void actionBan(int id)
   {
      rewards objReward = new rewards();
      dataEmps.emps_reward_action = new rewards[1];
      objReward.m0_reward_idx = id;
      dataEmps.emps_reward_action[0] = objReward;
      servExec.actionExec(masConn, "data_emps", empsRewardService, dataEmps, Constants.BAN);
   }

   protected void actionUnban(int id)
   {
      rewards objReward = new rewards();
      dataEmps.emps_reward_action = new rewards[1];
      objReward.m0_reward_idx = id;
      dataEmps.emps_reward_action[0] = objReward;
      servExec.actionExec(masConn, "data_emps", empsRewardService, dataEmps, Constants.UNBAN);
   }
   #endregion Action

   #region btnCommand
   protected void btnCommand(object sender, CommandEventArgs e)
   {
      string cmdName = e.CommandName.ToString();
      string cmdArg = e.CommandArgument.ToString();
      switch (cmdName)
      {
         case "btnToInsert":
            MvMaster.SetActiveView(ViewInsert);
            break;

         case "btnInsert":
            actionCreate();
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
            break;

         case "btnBan":
            actionBan(int.Parse(cmdArg));
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
            break;

         case "btnUnBan":
            actionUnban(int.Parse(cmdArg));
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
            break;

         case "btnCancel":
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
            break;
      }
   }
   #endregion btnCommand

   #region Get data from DB
   protected void getEmpLogin(int empId)
   {
      odspe objODSPE = new odspe();
      dataEmps.emps_odspe_action = new odspe[1];
      objODSPE.emp_idx = empId;
      dataEmps.emps_odspe_action[0] = objODSPE;
      _local_xml = servExec.actionExec(masConn, "data_emps", empsodspeService, dataEmps, Constants.SELECT_WHERE);
      dataEmps = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml);
      ViewState["EmpIDX"] = empId;
   }
   #endregion

   #region Custom Functions
   protected void setGridData(GridView gvName, Object obj)
   {
      gvName.DataSource = obj;
      gvName.DataBind();
   }

   protected string getStatus(int status)
   {
      if (status == 1)
      {
         return "<span class='status-online' data-toggle='tooltip' title='ONLINE'><i class='fa fa-check-circle fa-lg'></i></span>";
      }
      else
      {
         return "<span class='status-offline' data-toggle='tooltip' title='OFFLINE'><i class='fa fa-times-circle fa-lg'></i></span>";
      }
   }

   protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
   {
      var GvNameTEST = (GridView)sender;
      switch (GvNameTEST.ID)
      {
         case "GvMaster":
            GvMaster.PageIndex = e.NewPageIndex;
            GvMaster.DataBind();
            actionIndex();
            break;
      }
   }

   protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
   {
      var GvNameTEST = (GridView)sender;
      switch (GvNameTEST.ID)
      {
         case "GvMaster":
            if (e.Row.RowState.ToString().Contains("Edit"))
            {
               GridView editGrid = sender as GridView;
               int colSpan = editGrid.Columns.Count;
               for (int i = 1; i < colSpan; i++)
               {
                  e.Row.Cells[i].Visible = false;
                  e.Row.Cells[i].Controls.Clear();
               }
               e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
               e.Row.Cells[0].CssClass = "";
            }
            break;
      }
   }

   protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
   {
      var GvNameTEST = (GridView)sender;
      switch (GvNameTEST.ID)
      {
         case "GvMaster":
            GvMaster.EditIndex = -1;
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
            break;
      }
   }

   protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
   {
      var gvNameTest = (GridView)sender;
      switch (gvNameTest.ID)
      {
         case "GvMaster":
            int dataKeyId = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
            var txtRewardNameUpdate = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtRewardNameUpdate");
            var txtRewardValueUpdate = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtRewardValueUpdate");
            var ddlRewardStatusUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlRewardStatusUpdate");
            GvMaster.EditIndex = -1;
            actionUpdate(dataKeyId, txtRewardNameUpdate.Text, txtRewardValueUpdate.Text, int.Parse(ddlRewardStatusUpdate.SelectedValue));
            actionIndex();
            break;
      }
   }

   protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
   {
      var GvNameTEST = (GridView)sender;
      switch (GvNameTEST.ID)
      {
         case "GvMaster":
            GvMaster.EditIndex = e.NewEditIndex;
            actionIndex();
            break;
      }
   }
   #endregion Custom Functions
}
