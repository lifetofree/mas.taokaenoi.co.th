﻿using DotNet.Highcharts;
using DotNet.Highcharts.Helpers;
using DotNet.Highcharts.Options;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Text;
using System.Net;
using System.Data.SqlClient;

using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using System.Globalization;

public partial class websystem_ITServices_Network_Layout : System.Web.UI.Page
{
    #region initial function/data

    function_tool _funcTool = new function_tool();
    service_execute serviceexcute = new service_execute();
    data_networkdevices _data_networkdevices = new data_networkdevices();
    data_networklayout _dtnwl = new data_networklayout();
    data_employee _dtEmployee = new data_employee();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetddlplace = _serviceUrl + ConfigurationManager.AppSettings["urlGetddlplace"];
    static string _urlGetm0Room = _serviceUrl + ConfigurationManager.AppSettings["urlGetm0Room"];
    static string urlSelectFloor = _serviceUrl + ConfigurationManager.AppSettings["urlSelectFloor"];
    static string urlSelectChamber = _serviceUrl + ConfigurationManager.AppSettings["urlSelectChamber"];
    static string _urlGetm0Category = _serviceUrl + ConfigurationManager.AppSettings["urlGetm0Category"];
    static string _urlGetm0Type = _serviceUrl + ConfigurationManager.AppSettings["urlGetm0Type"];
    static string urlSelectNWL = _serviceUrl + ConfigurationManager.AppSettings["urlSelectNWL"];
    static string urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string urlSelectTableNPW = _serviceUrl + ConfigurationManager.AppSettings["urlSelectTableNPW"];
    static string urlSelectTableIDC = _serviceUrl + ConfigurationManager.AppSettings["urlSelectTableIDC"];
    static string urlSelectTableRJN = _serviceUrl + ConfigurationManager.AppSettings["urlSelectTableRJN"];
    static string urlSelectTableMTT = _serviceUrl + ConfigurationManager.AppSettings["urlSelectTableMTT"];
    static string urlSelect_LogLayout = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_LogLayout"];
    static string urlSelectPlan_nwl = _serviceUrl + ConfigurationManager.AppSettings["urlSelectPlan_nwl"];
    static string urlSelect_Device_Cat = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Device_Cat"];
    static string urlSelect_Image_Layout = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Image_Layout"];
    static string urlSelect_Device_FV = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Device_FV"];
    static string urlSelect_Log_BindFV = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Log_BindFV"];
    static string urlSelect_Report = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Report"];
    static string urlSelect_Export = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Export"];
    static string urlSelect_Floor_Plan = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Floor_Plan"];


    
    string cell = "";
    string row = "";
    string register_number = "";
    string no_regis = "";
    string ip_address = "";
    string serial_number = "";
    string type_name = "";
    string category_name = "";
    string Locname = "";
    string room_name = "";
    string Floor_name = "";
    string img_name = "";
    int status_active = 0;

    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";
    string _ret_val = "";
    string _localJson = "";
    int _tempInt = 0;

    string _defaultDdlText;
    string _defaultDdlValue;


    int emp_idx = 0;

    #endregion initial function/data

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ViewState["EmpIDX"] = int.Parse(Session["emp_idx"].ToString());
            SelectCategory();
            SelectType();
            select_empIdx_present();
            Select_ChartNPW_Pie();
            Select_ChartNPW_Colum();
            Select_ChartIDC_Pie();
            Select_ChartIDC_Colum();
            Select_ChartRJN_Pie();
            Select_ChartRJN_Colum();
            Select_ChartMTT_Pie();
            Select_ChartMTT_Colum();
            BoxSearch.Visible = false;
            BoxButtonSearchShow.Visible = true;
            BoxButtonSearchHide.Visible = false;
            lbindex.BackColor = System.Drawing.Color.LightGray;
            lbreport.BackColor = System.Drawing.Color.Transparent;

        }

        linkBtnTrigger(lbindex);
        linkBtnTrigger(lbreport);

    }
    #endregion

    #region Select

    protected void SelectPlace()
    {
        //ddlplace.Items.Clear();
        //ddlplace.AppendDataBoundItems = true;
        //ddlplace.Items.Add(new ListItem("กรุณาเลือกชื่อสถานที่ติดตั้งอุปกรณ์...", "0"));

        _data_networkdevices.m0room_list = new m0room_detail[1];
        m0room_detail _m0roomDetail = new m0room_detail();

        _data_networkdevices.m0room_list[0] = _m0roomDetail;

        _data_networkdevices = callServiceNetwork(_urlGetddlplace, _data_networkdevices);
        //setDdlData(ddlplace, _data_networkdevices.m0room_list, "place_name", "place_idx");
        //ddlplace.Items.Insert(0, new ListItem("กรุณาเลือกชื่อสถานที่ติดตั้งอุปกรณ์...", "0"));

        setDdlData(ddlre_place, _data_networkdevices.m0room_list, "place_name", "place_idx");
        ddlre_place.Items.Insert(0, new ListItem("กรุณาเลือกชื่อสถานที่ติดตั้งอุปกรณ์...", "0"));
    }

    protected void SelectBuilding()
    {
        data_networkdevices _data_networkdevicesindex = new data_networkdevices();
        _data_networkdevicesindex.m0room_list = new m0room_detail[1];

        m0room_detail _m0room_detailindex = new m0room_detail();

        _m0room_detailindex.room_idx = 0;

        _data_networkdevicesindex.m0room_list[0] = _m0room_detailindex;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_networkdevicesindex));
        _data_networkdevicesindex = callServiceNetwork(_urlGetm0Room, _data_networkdevicesindex);
        //setDdlData(ddlbuild, _data_networkdevicesindex.m0room_list, "room_name", "room_idx");
        //ddlbuild.Items.Insert(0, new ListItem("กรุณาเลือกชื่ออาคารติดตั้งอุปกรณ์...", "0"));

        setDdlData(ddlre_build, _data_networkdevicesindex.m0room_list, "room_name", "room_idx");
        ddlre_build.Items.Insert(0, new ListItem("กรุณาเลือกชื่ออาคารติดตั้งอุปกรณ์...", "0"));

    }

    protected void SelectFloor()
    {
        data_networkdevices _dtnetde = new data_networkdevices();
        _dtnetde.m0floor_list = new m0floor_detail[1];

        m0floor_detail Floor_add = new m0floor_detail();

        Floor_add.FLIDX = 0;

        _dtnetde.m0floor_list[0] = Floor_add;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_system1));
        _dtnetde = callServiceNetwork(urlSelectFloor, _dtnetde);

        //setDdlData(ddlfloor, _dtnetde.m0floor_list, "Floor_name", "FLIDX");
        //ddlfloor.Items.Insert(0, new ListItem("กรุณาเลือกชั้นที่ตั้งอุปกรณ์...", "0"));

        setDdlData(ddlre_floor, _dtnetde.m0floor_list, "Floor_name", "FLIDX");
        ddlre_floor.Items.Insert(0, new ListItem("กรุณาเลือกชั้นที่ตั้งอุปกรณ์...", "0"));
    }

    //protected void SelectChamber()
    //{
    //    data_networkdevices _dtnetde = new data_networkdevices();
    //    _dtnetde.m0chamber_list = new m0chamber_detail[1];
    //    m0chamber_detail chamber_add = new m0chamber_detail();

    //    chamber_add.CHIDX = 0;

    //    _dtnetde.m0chamber_list[0] = chamber_add;

    //    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_system1));
    //    _dtnetde = callServiceNetwork(urlSelectChamber, _dtnetde);

    //    setDdlData(ddlroom, _dtnetde.m0chamber_list, "Chamber_name", "CHIDX");
    //    ddlroom.Items.Insert(0, new ListItem("กรุณาเลือกห้องที่ตั้งอุปกรณ์...", "0"));
    //}

    protected void SelectCategory()
    {
        data_networkdevices _data_networkdevicesindex = new data_networkdevices();
        _data_networkdevicesindex.m0category_list = new m0category_detail[1];

        m0category_detail _m0category_detailindex = new m0category_detail();

        _m0category_detailindex.category_idx = 0;

        _data_networkdevicesindex.m0category_list[0] = _m0category_detailindex;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_system1));
        _data_networkdevicesindex = callServiceNetwork(_urlGetm0Category, _data_networkdevicesindex);

        setDdlData(ddlcate, _data_networkdevicesindex.m0category_list, "category_name", "category_idx");
        ddlcate.Items.Insert(0, new ListItem("กรุณาเลือกชนิดตั้งอุปกรณ์...", "0"));

        setDdlData(ddlre_cate, _data_networkdevicesindex.m0category_list, "category_name", "category_idx");
        ddlre_cate.Items.Insert(0, new ListItem("กรุณาเลือกชนิดตั้งอุปกรณ์...", "0"));
    }

    protected void SelectType()
    {
        data_networkdevices _data_networkdevicesindex = new data_networkdevices();
        _data_networkdevicesindex.m0type_list = new m0type_detail[1];

        m0type_detail _m0type_detailindex = new m0type_detail();

        _m0type_detailindex.type_idx = 0;

        _data_networkdevicesindex.m0type_list[0] = _m0type_detailindex;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_system1));
        _data_networkdevicesindex = callServiceNetwork(_urlGetm0Type, _data_networkdevicesindex);

        setDdlData(ddltype, _data_networkdevicesindex.m0type_list, "type_name", "type_idx");
        ddltype.Items.Insert(0, new ListItem("กรุณาเลือกประเภทอุปกรณ์...", "0"));

        //setDdlData(ddlre_type, _data_networkdevicesindex.m0type_list, "type_name", "type_idx");
        //ddlre_type.Items.Insert(0, new ListItem("กรุณาเลือกประเภทอุปกรณ์...", "0"));

    }

    protected void select_empIdx_present()
    {

        _dtEmployee = callServiceEmpProfile(urlGetMyProfile + ViewState["EmpIDX"].ToString());


        ViewState["rdept_name"] = _dtEmployee.employee_list[0].dept_name_th;
        ViewState["rdept_idx"] = _dtEmployee.employee_list[0].rdept_idx;
        ViewState["FullName"] = _dtEmployee.employee_list[0].emp_name_th;
        ViewState["Org_name"] = _dtEmployee.employee_list[0].org_name_th;
        ViewState["Org_idx"] = _dtEmployee.employee_list[0].org_idx;

        ViewState["EmpCode"] = _dtEmployee.employee_list[0].emp_code;
        ViewState["Positname"] = _dtEmployee.employee_list[0].pos_name_th;
        ViewState["Pos_idx"] = _dtEmployee.employee_list[0].rpos_idx;
        ViewState["Email"] = _dtEmployee.employee_list[0].emp_email;
        ViewState["Tel"] = _dtEmployee.employee_list[0].emp_mobile_no;
        ViewState["Secname"] = _dtEmployee.employee_list[0].sec_name_th;
        ViewState["Sec_idx"] = _dtEmployee.employee_list[0].rsec_idx;
        ViewState["CostIDX"] = _dtEmployee.employee_list[0].costcenter_idx;

    }

    public void Select_ChartNPW_Pie()
    {

        data_networklayout _dtnwl = new data_networklayout();
        _dtnwl.BoxnwlList = new NWLList[1];
        NWLList searchlv1 = new NWLList();
        _dtnwl.BoxnwlList[0] = searchlv1;

        //_local_xml = serviceexcute.actionExec("conn_mis", "data_networklayout", "services_networklayout", _dtnwl, 204);
        //_dtnwl = (data_networklayout)_funcTool.convertXmlToObject(typeof(data_networklayout), _local_xml);
        //setGridData(GvNPW, _dtnwl.BoxnwlList);

        //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtnwl));
        _dtnwl = callServiceLayout(urlSelectTableNPW, _dtnwl);
        setGridData(GvNPW, _dtnwl.BoxnwlList);

        NWLList searchlv2 = new NWLList();
        _dtnwl.BoxnwlList = new NWLList[1];

        _dtnwl.BoxnwlList[0] = searchlv2;
        //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtnwl));
        _local_xml = serviceexcute.actionExec("conn_mis", "data_networklayout", "services_networklayout", _dtnwl, 208);
        _dtnwl = (data_networklayout)_funcTool.convertXmlToObject(typeof(data_networklayout), _local_xml);


        if (_dtnwl.ReturnCode == "0")
        {
            var caseclose = new List<object>();

            foreach (var data in _dtnwl.BoxnwlList)
            {
                caseclose.Add(new object[] { data.name_npw.ToString(), data.countdown_npw.ToString() });
            }

            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new Title { Text = " " });
            chart.InitChart(new Chart { DefaultSeriesType = DotNet.Highcharts.Enums.ChartTypes.Pie });

            chart.SetPlotOptions(new PlotOptions
            {
                Pie = new PlotOptionsPie
                {
                    ShowInLegend = true,
                    AllowPointSelect = true,
                    DataLabels = new PlotOptionsPieDataLabels
                    {
                        Enabled = true,
                        // Formatter = "function() { return '" + this. + ' : '" + lv1count + "';}" //  this.percentage.toFixed(2)+' % '; }"
                        Formatter = "function() { return this.point.name +' : '+  this.y.toFixed(0)+' '; }"
                    }
                }
            });

            chart.SetTooltip(new Tooltip
            {
                Enabled = true,
                Formatter = "function() { return this.point.name +' : '+  this.percentage.toFixed(2)+' % '; }"

            });
            chart.SetSeries(new Series[]
                 {
                        new Series()
                        {
                            Data = new Data (caseclose.ToArray())
                        }
                 });


            litReportChart.Text = chart.ToHtmlString();
        }
        else
        {
            litReportChart.Text = "<p class='bg-danger'>No result.</p>";
        }

    }

    public void Select_ChartNPW_Colum()
    {
        NWLList searchlv1 = new NWLList();
        _dtnwl.BoxnwlList = new NWLList[1];

        _dtnwl.BoxnwlList[0] = searchlv1;
        //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtnwl));
        _local_xml = serviceexcute.actionExec("conn_mis", "data_networklayout", "services_networklayout", _dtnwl, 208);
        _dtnwl = (data_networklayout)_funcTool.convertXmlToObject(typeof(data_networklayout), _local_xml);

        if (_dtnwl.ReturnCode == "0")
        {
            int count = _dtnwl.BoxnwlList.Length;
            string[] lv1code = new string[count];
            object[] lv1count = new object[count];
            int i = 0;
            foreach (var data in _dtnwl.BoxnwlList)
            {
                lv1code[i] = data.ip_address.ToString();
                lv1count[i] = data.countdown_npw.ToString();
                i++;
            }
            Highcharts chart1 = new Highcharts("chart1");
            chart1.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart1.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = " " } } });
            chart1.SetXAxis(new XAxis { Categories = lv1code });
            chart1.SetSeries(
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "IP Address",
                    Data = new Data(lv1count)
                }
            );
            litReportChart1.Text = chart1.ToHtmlString();
        }
        else
        {
            litReportChart1.Text = "<p class='bg-danger'>No result.</p>";
        }
    }

    public void Select_ChartIDC_Pie()
    {

        data_networklayout _dtnwl = new data_networklayout();
        _dtnwl.BoxnwlList = new NWLList[1];
        NWLList searchlv1 = new NWLList();
        _dtnwl.BoxnwlList[0] = searchlv1;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_system1));
        _dtnwl = callServiceLayout(urlSelectTableIDC, _dtnwl);
        setGridData(GVIDC, _dtnwl.BoxnwlList);


        NWLList searchlv2 = new NWLList();
        _dtnwl.BoxnwlList = new NWLList[1];

        _dtnwl.BoxnwlList[0] = searchlv2;
        //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtnwl));
        _local_xml = serviceexcute.actionExec("conn_mis", "data_networklayout", "services_networklayout", _dtnwl, 209);
        _dtnwl = (data_networklayout)_funcTool.convertXmlToObject(typeof(data_networklayout), _local_xml);


        if (_dtnwl.ReturnCode == "0")
        {
            var caseclose = new List<object>();

            foreach (var data in _dtnwl.BoxnwlList)
            {
                caseclose.Add(new object[] { data.name_idc.ToString(), data.countdown_idc.ToString() });
            }

            Highcharts chart2 = new Highcharts("chart2");
            chart2.SetTitle(new Title { Text = " " });
            chart2.InitChart(new Chart { DefaultSeriesType = DotNet.Highcharts.Enums.ChartTypes.Pie });

            chart2.SetPlotOptions(new PlotOptions
            {
                Pie = new PlotOptionsPie
                {
                    ShowInLegend = true,
                    AllowPointSelect = true,
                    DataLabels = new PlotOptionsPieDataLabels
                    {
                        Enabled = true,
                        // Formatter = "function() { return '" + this. + ' : '" + lv1count + "';}" //  this.percentage.toFixed(2)+' % '; }"
                        Formatter = "function() { return this.point.name +' : '+  this.y.toFixed(0)+' '; }"
                    }
                }
            });

            chart2.SetTooltip(new Tooltip
            {
                Enabled = true,
                Formatter = "function() { return this.point.name +' : '+  this.percentage.toFixed(2)+' % '; }"

            });
            chart2.SetSeries(new Series[]
                 {
                        new Series()
                        {
                            Data = new Data (caseclose.ToArray())
                        }
                 });


            litReportChart2.Text = chart2.ToHtmlString();
        }
        else
        {
            litReportChart2.Text = "<p class='bg-danger'>No result.</p>";
        }

    }

    public void Select_ChartIDC_Colum()
    {
        NWLList searchlv1 = new NWLList();
        _dtnwl.BoxnwlList = new NWLList[1];

        _dtnwl.BoxnwlList[0] = searchlv1;
        //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtnwl));
        _local_xml = serviceexcute.actionExec("conn_mis", "data_networklayout", "services_networklayout", _dtnwl, 209);
        _dtnwl = (data_networklayout)_funcTool.convertXmlToObject(typeof(data_networklayout), _local_xml);

        if (_dtnwl.ReturnCode == "0")
        {
            int count = _dtnwl.BoxnwlList.Length;
            string[] lv1code = new string[count];
            object[] lv1count = new object[count];
            int i = 0;
            foreach (var data in _dtnwl.BoxnwlList)
            {
                lv1code[i] = data.ip_address.ToString();
                lv1count[i] = data.countdown_idc.ToString();
                i++;
            }
            Highcharts chart3 = new Highcharts("chart3");
            chart3.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart3.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = " " } } });
            chart3.SetXAxis(new XAxis { Categories = lv1code });
            chart3.SetSeries(
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "IP Address",
                    Data = new Data(lv1count)
                }
            );
            litReportChart3.Text = chart3.ToHtmlString();
        }
        else
        {
            litReportChart3.Text = "<p class='bg-danger'>No result.</p>";
        }
    }

    public void Select_ChartRJN_Pie()
    {

        data_networklayout _dtnwl = new data_networklayout();
        _dtnwl.BoxnwlList = new NWLList[1];
        NWLList searchlv1 = new NWLList();
        _dtnwl.BoxnwlList[0] = searchlv1;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_system1));
        _dtnwl = callServiceLayout(urlSelectTableRJN, _dtnwl);
        setGridData(GvRJN, _dtnwl.BoxnwlList);


        NWLList searchlv2 = new NWLList();
        _dtnwl.BoxnwlList = new NWLList[1];

        _dtnwl.BoxnwlList[0] = searchlv2;
        //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtnwl));
        _local_xml = serviceexcute.actionExec("conn_mis", "data_networklayout", "services_networklayout", _dtnwl, 210);
        _dtnwl = (data_networklayout)_funcTool.convertXmlToObject(typeof(data_networklayout), _local_xml);


        if (_dtnwl.ReturnCode == "0")
        {
            var caseclose = new List<object>();

            foreach (var data in _dtnwl.BoxnwlList)
            {
                caseclose.Add(new object[] { data.name_rjn.ToString(), data.countdown_rjn.ToString() });
            }

            Highcharts chart4 = new Highcharts("chart4");
            chart4.SetTitle(new Title { Text = " " });
            chart4.InitChart(new Chart { DefaultSeriesType = DotNet.Highcharts.Enums.ChartTypes.Pie });

            chart4.SetPlotOptions(new PlotOptions
            {
                Pie = new PlotOptionsPie
                {
                    ShowInLegend = true,
                    AllowPointSelect = true,
                    DataLabels = new PlotOptionsPieDataLabels
                    {
                        Enabled = true,
                        // Formatter = "function() { return '" + this. + ' : '" + lv1count + "';}" //  this.percentage.toFixed(2)+' % '; }"
                        Formatter = "function() { return this.point.name +' : '+  this.y.toFixed(0)+' '; }"
                    }
                }
            });

            chart4.SetTooltip(new Tooltip
            {
                Enabled = true,
                Formatter = "function() { return this.point.name +' : '+  this.percentage.toFixed(2)+' % '; }"

            });
            chart4.SetSeries(new Series[]
                 {
                        new Series()
                        {
                            Data = new Data (caseclose.ToArray())
                        }
                 });


            litReportChart4.Text = chart4.ToHtmlString();
        }
        else
        {
            litReportChart4.Text = "<p class='bg-danger'>No result.</p>";
        }

    }

    public void Select_ChartRJN_Colum()
    {
        NWLList searchlv1 = new NWLList();
        _dtnwl.BoxnwlList = new NWLList[1];

        _dtnwl.BoxnwlList[0] = searchlv1;
        //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtnwl));
        _local_xml = serviceexcute.actionExec("conn_mis", "data_networklayout", "services_networklayout", _dtnwl, 210);
        _dtnwl = (data_networklayout)_funcTool.convertXmlToObject(typeof(data_networklayout), _local_xml);

        if (_dtnwl.ReturnCode == "0")
        {
            int count = _dtnwl.BoxnwlList.Length;
            string[] lv1code = new string[count];
            object[] lv1count = new object[count];
            int i = 0;
            foreach (var data in _dtnwl.BoxnwlList)
            {
                lv1code[i] = data.ip_address.ToString();
                lv1count[i] = data.countdown_rjn.ToString();
                i++;
            }
            Highcharts chart5 = new Highcharts("chart5");
            chart5.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart5.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = " " } } });
            chart5.SetXAxis(new XAxis { Categories = lv1code });
            chart5.SetSeries(
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "IP Address",
                    Data = new Data(lv1count)
                }
            );
            litReportChart5.Text = chart5.ToHtmlString();
        }
        else
        {
            litReportChart5.Text = "<p class='bg-danger'>No result.</p>";
        }
    }

    public void Select_ChartMTT_Pie()
    {

        data_networklayout _dtnwl = new data_networklayout();
        _dtnwl.BoxnwlList = new NWLList[1];
        NWLList searchlv1 = new NWLList();

        _dtnwl.BoxnwlList[0] = searchlv1;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_system1));
        _dtnwl = callServiceLayout(urlSelectTableMTT, _dtnwl);
        setGridData(GvMTT, _dtnwl.BoxnwlList);


        NWLList searchlv2 = new NWLList();
        _dtnwl.BoxnwlList = new NWLList[1];

        _dtnwl.BoxnwlList[0] = searchlv2;
        //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtnwl));
        _local_xml = serviceexcute.actionExec("conn_mis", "data_networklayout", "services_networklayout", _dtnwl, 211);
        _dtnwl = (data_networklayout)_funcTool.convertXmlToObject(typeof(data_networklayout), _local_xml);


        if (_dtnwl.ReturnCode == "0")
        {
            var caseclose = new List<object>();

            foreach (var data in _dtnwl.BoxnwlList)
            {
                caseclose.Add(new object[] { data.name_mtt.ToString(), data.countdown_mtt.ToString() });
            }

            Highcharts chart6 = new Highcharts("chart6");
            chart6.SetTitle(new Title { Text = " " });
            chart6.InitChart(new Chart { DefaultSeriesType = DotNet.Highcharts.Enums.ChartTypes.Pie });

            chart6.SetPlotOptions(new PlotOptions
            {
                Pie = new PlotOptionsPie
                {
                    ShowInLegend = true,
                    AllowPointSelect = true,
                    DataLabels = new PlotOptionsPieDataLabels
                    {
                        Enabled = true,
                        // Formatter = "function() { return '" + this. + ' : '" + lv1count + "';}" //  this.percentage.toFixed(2)+' % '; }"
                        Formatter = "function() { return this.point.name +' : '+  this.y.toFixed(0)+' '; }"
                    }
                }
            });

            chart6.SetTooltip(new Tooltip
            {
                Enabled = true,
                Formatter = "function() { return this.point.name +' : '+  this.percentage.toFixed(2)+' % '; }"

            });
            chart6.SetSeries(new Series[]
                 {
                        new Series()
                        {
                            Data = new Data (caseclose.ToArray())
                        }
                 });


            litReportChart6.Text = chart6.ToHtmlString();
        }
        else
        {
            litReportChart6.Text = "<p class='bg-danger'>No result.</p>";
        }

    }

    public void Select_ChartMTT_Colum()
    {
        NWLList searchlv1 = new NWLList();
        _dtnwl.BoxnwlList = new NWLList[1];

        _dtnwl.BoxnwlList[0] = searchlv1;
        //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtnwl));
        _local_xml = serviceexcute.actionExec("conn_mis", "data_networklayout", "services_networklayout", _dtnwl, 211);
        _dtnwl = (data_networklayout)_funcTool.convertXmlToObject(typeof(data_networklayout), _local_xml);

        if (_dtnwl.ReturnCode == "0")
        {
            int count = _dtnwl.BoxnwlList.Length;
            string[] lv1code = new string[count];
            object[] lv1count = new object[count];
            int i = 0;
            foreach (var data in _dtnwl.BoxnwlList)
            {
                lv1code[i] = data.ip_address.ToString();
                lv1count[i] = data.countdown_mtt.ToString();
                i++;
            }
            Highcharts chart7 = new Highcharts("chart7");
            chart7.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart7.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = " " } } });
            chart7.SetXAxis(new XAxis { Categories = lv1code });
            chart7.SetSeries(
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "IP Address",
                    Data = new Data(lv1count)
                }
            );
            litReportChart7.Text = chart7.ToHtmlString();
        }
        else
        {
            litReportChart7.Text = "<p class='bg-danger'>No result.</p>";
        }
    }

    public void Select_Device()
    {
        _dtnwl = new data_networklayout();

        _dtnwl.BoxnwlList = new NWLList[1];
        NWLList search = new NWLList();

        search.type_idx = int.Parse(ddltype.SelectedValue);
        search.category_idx = int.Parse(ddlcate.SelectedValue);

        _dtnwl.BoxnwlList[0] = search;
        //  text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtnwl));

        _dtnwl = callServiceLayout(urlSelect_Device_Cat, _dtnwl);

        setGridData(GvViewDevices, _dtnwl.BoxnwlList);

    }

    public void Select_Report_Table()
    {

        _dtnwl = new data_networklayout();

        _dtnwl.BoxnwlList = new NWLList[1];
        NWLList search = new NWLList();

        search.category_idx = int.Parse(ddlre_cate.SelectedValue);
        search.Search = txtdevices.Text;
        search.LocIDX = int.Parse(ddlre_place.SelectedValue);
        search.BUIDX = int.Parse(ddlre_build.SelectedValue);
        search.FLIDX = int.Parse(ddlre_floor.SelectedValue);
        search.create_log = AddStartdate.Text;
        if (AddEndDate.Text == "")
        {
            search.end_log = " ";
        }
        else
        {
            search.end_log = AddEndDate.Text;
        }
        search.IFSearchbetween = int.Parse(ddlSearchDate.SelectedValue);

        _dtnwl.BoxnwlList[0] = search;
        //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtnwl));

        _dtnwl = callServiceLayout(urlSelect_Report, _dtnwl);

        setGridData(GvReport, _dtnwl.BoxnwlList);

    }

    public void Select_Report_Chart_noregis()
    {
        _dtnwl = new data_networklayout();

        _dtnwl.BoxnwlList = new NWLList[1];
        NWLList search = new NWLList();

        //   search.category_idx = int.Parse(ddlre_cate.SelectedValue);
        search.Search = txtdevices.Text;
        //search.LocIDX = int.Parse(ddlre_place.SelectedValue);
        //search.BUIDX = int.Parse(ddlre_build.SelectedValue);
        //search.FLIDX = int.Parse(ddlre_floor.SelectedValue);
        search.create_log = AddStartdate.Text;
        search.end_log = AddEndDate.Text;
        search.IFSearchbetween = int.Parse(ddlSearchDate.SelectedValue);

        _dtnwl.BoxnwlList[0] = search;
        // text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtnwl));

        _local_xml = serviceexcute.actionExec("conn_mis", "data_networklayout", "services_networklayout", _dtnwl, 218);
        _dtnwl = (data_networklayout)_funcTool.convertXmlToObject(typeof(data_networklayout), _local_xml);

        if (_dtnwl.ReturnCode == "0")
        {
            int count = _dtnwl.BoxnwlList.Length;
            string[] lv1code = new string[count];
            object[] lv1count = new object[count];
            int i = 0;
            foreach (var data in _dtnwl.BoxnwlList)
            {
                lv1code[i] = data.no_regis.ToString();
                lv1count[i] = data.countu0idx.ToString();
                i++;
            }
            Highcharts chart8 = new Highcharts("chart8");
            chart8.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart8.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "ชื่ออุปกรณ์ / จำนวนครั้ง" } } });
            chart8.SetXAxis(new XAxis { Categories = lv1code });
            chart8.SetSeries(
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "ชื่ออุปกรณ์",
                    Data = new Data(lv1count)
                }
            );
            litReportChart8.Text = chart8.ToHtmlString();
        }
        else
        {
            litReportChart8.Text = "<p class='bg-danger'>No result.</p>";
        }
    }

    public void Select_Report_Chart_ipaddress()
    {
        _dtnwl = new data_networklayout();

        _dtnwl.BoxnwlList = new NWLList[1];
        NWLList search = new NWLList();

        //  search.category_idx = int.Parse(ddlre_cate.SelectedValue);
        search.Search = txtdevices.Text;
        //search.LocIDX = int.Parse(ddlre_place.SelectedValue);
        //search.BUIDX = int.Parse(ddlre_build.SelectedValue);
        //search.FLIDX = int.Parse(ddlre_floor.SelectedValue);
        search.create_log = AddStartdate.Text;
        search.end_log = AddEndDate.Text;
        search.IFSearchbetween = int.Parse(ddlSearchDate.SelectedValue);

        _dtnwl.BoxnwlList[0] = search;
        //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtnwl));

        _local_xml = serviceexcute.actionExec("conn_mis", "data_networklayout", "services_networklayout", _dtnwl, 219);
        _dtnwl = (data_networklayout)_funcTool.convertXmlToObject(typeof(data_networklayout), _local_xml);

        if (_dtnwl.ReturnCode == "0")
        {
            int count = _dtnwl.BoxnwlList.Length;
            string[] lv1code = new string[count];
            object[] lv1count = new object[count];
            int i = 0;
            foreach (var data in _dtnwl.BoxnwlList)
            {
                lv1code[i] = data.ip_address.ToString();
                lv1count[i] = data.countu0idx.ToString();
                i++;
            }
            Highcharts chart9 = new Highcharts("chart9");
            chart9.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart9.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "IP Address / จำนวนครั้ง" } } });
            chart9.SetXAxis(new XAxis { Categories = lv1code });
            chart9.SetSeries(
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "IP Address",
                    Data = new Data(lv1count)
                }
            );
            litReportChart8.Text = chart9.ToHtmlString();
        }
        else
        {
            litReportChart8.Text = "<p class='bg-danger'>No result.</p>";
        }
    }

    public void Select_Report_Chart_regis()
    {
        _dtnwl = new data_networklayout();

        _dtnwl.BoxnwlList = new NWLList[1];
        NWLList search = new NWLList();

        // search.category_idx = int.Parse(ddlre_cate.SelectedValue);
        search.Search = txtdevices.Text;
        //search.LocIDX = int.Parse(ddlre_place.SelectedValue);
        //search.BUIDX = int.Parse(ddlre_build.SelectedValue);
        //search.FLIDX = int.Parse(ddlre_floor.SelectedValue);
        search.create_log = AddStartdate.Text;
        search.end_log = AddEndDate.Text;
        search.IFSearchbetween = int.Parse(ddlSearchDate.SelectedValue);

        _dtnwl.BoxnwlList[0] = search;
        //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtnwl));

        _local_xml = serviceexcute.actionExec("conn_mis", "data_networklayout", "services_networklayout", _dtnwl, 220);
        _dtnwl = (data_networklayout)_funcTool.convertXmlToObject(typeof(data_networklayout), _local_xml);

        if (_dtnwl.ReturnCode == "0")
        {
            int count = _dtnwl.BoxnwlList.Length;
            string[] lv1code = new string[count];
            object[] lv1count = new object[count];
            int i = 0;
            foreach (var data in _dtnwl.BoxnwlList)
            {
                lv1code[i] = data.register_number.ToString();
                lv1count[i] = data.countu0idx.ToString();
                i++;
            }
            Highcharts chart10 = new Highcharts("chart10");
            chart10.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart10.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "เลขทะเบียนอุปกรณ์ / จำนวนครั้ง" } } });
            chart10.SetXAxis(new XAxis { Categories = lv1code });
            chart10.SetSeries(
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "เลขทะเบียนอุปกรณ์",
                    Data = new Data(lv1count)
                }
            );
            litReportChart8.Text = chart10.ToHtmlString();
        }
        else
        {
            litReportChart8.Text = "<p class='bg-danger'>No result.</p>";
        }
    }

    public void Select_Report_Chart_category()
    {
        _dtnwl = new data_networklayout();

        _dtnwl.BoxnwlList = new NWLList[1];
        NWLList search = new NWLList();

        search.category_idx = int.Parse(ddlre_cate.SelectedValue);
        search.create_log = AddStartdate.Text;
        search.end_log = AddEndDate.Text;
        search.IFSearchbetween = int.Parse(ddlSearchDate.SelectedValue);

        _dtnwl.BoxnwlList[0] = search;
        //  text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtnwl));

        _local_xml = serviceexcute.actionExec("conn_mis", "data_networklayout", "services_networklayout", _dtnwl, 221);
        _dtnwl = (data_networklayout)_funcTool.convertXmlToObject(typeof(data_networklayout), _local_xml);

        if (_dtnwl.ReturnCode == "0")
        {
            int count = _dtnwl.BoxnwlList.Length;
            string[] lv1code = new string[count];
            object[] lv1count = new object[count];
            int i = 0;
            foreach (var data in _dtnwl.BoxnwlList)
            {
                lv1code[i] = data.category_name.ToString();
                lv1count[i] = data.countu0idx.ToString();
                i++;
            }
            Highcharts chart10 = new Highcharts("chart10");
            chart10.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart10.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "ชนิดทะเบียนอุปกรณ์ / จำนวนครั้ง" } } });
            chart10.SetXAxis(new XAxis { Categories = lv1code });
            chart10.SetSeries(
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "ชนิดทะเบียนอุปกรณ์",
                    Data = new Data(lv1count)
                }
            );
            litReportChart8.Text = chart10.ToHtmlString();
        }
        else
        {
            litReportChart8.Text = "<p class='bg-danger'>No result.</p>";
        }
    }

    public void Select_Report_Chart_place()
    {
        _dtnwl = new data_networklayout();

        _dtnwl.BoxnwlList = new NWLList[1];
        NWLList search = new NWLList();

        // search.LocIDX = int.Parse(ddlre_place.SelectedValue);
        search.create_log = AddStartdate.Text;
        search.end_log = AddEndDate.Text;
        search.IFSearchbetween = int.Parse(ddlSearchDate.SelectedValue);

        _dtnwl.BoxnwlList[0] = search;
        // text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtnwl));

        _local_xml = serviceexcute.actionExec("conn_mis", "data_networklayout", "services_networklayout", _dtnwl, 222);
        _dtnwl = (data_networklayout)_funcTool.convertXmlToObject(typeof(data_networklayout), _local_xml);

        if (_dtnwl.ReturnCode == "0")
        {
            int count = _dtnwl.BoxnwlList.Length;
            string[] lv1code = new string[count];
            object[] lv1count = new object[count];
            int i = 0;
            foreach (var data in _dtnwl.BoxnwlList)
            {
                lv1code[i] = data.place_name.ToString();
                lv1count[i] = data.countu0idx.ToString();
                i++;
            }
            Highcharts chart10 = new Highcharts("chart10");
            chart10.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart10.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "สถานที่ / จำนวนครั้ง" } } });
            chart10.SetXAxis(new XAxis { Categories = lv1code });
            chart10.SetSeries(
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "สถานที่",
                    Data = new Data(lv1count)
                }
            );
            litReportChart8.Text = chart10.ToHtmlString();
        }
        else
        {
            litReportChart8.Text = "<p class='bg-danger'>No result.</p>";
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

   

    /*
    protected void select_plan()
    {
        _dtnwl = new data_networklayout();

        _dtnwl.BoxnwlList = new NWLList[1];
        NWLList search = new NWLList();

        search.BUIDX = int.Parse(ddlbuild.SelectedValue);
        search.LocIDX = int.Parse(ddlplace.SelectedValue);
        search.FLIDX = int.Parse(ddlfloor.SelectedValue);

        _dtnwl.BoxnwlList[0] = search;
        _dtnwl = callServiceLayout(urlSelectNWL, _dtnwl);
        //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtnwl));

        ViewState["_dtnwl"] = _dtnwl.BoxnwlList;

        ViewState["register_number"] = "";
        ViewState["no_regis"] = "";
        ViewState["row_"] = "";
        ViewState["cell_"] = "";
        ViewState["ip_address"] = "";
        ViewState["serial_number"] = "";
        ViewState["type_name"] = "";
        ViewState["category_name"] = "";
        ViewState["Locname"] = "";
        ViewState["room_name"] = "";
        ViewState["Floor_name"] = "";
        ViewState["img_name"] = "";
        if (_dtnwl.ReturnCode == "0")
        {

            foreach (var data in _dtnwl.BoxnwlList)
            {
                ViewState["register_number"] += data.register_number.ToString() + ",";
                ViewState["no_regis"] += data.no_regis.ToString() + ",";
                //  ViewState["row_"] += data.row_.ToString();
                row += data.row_.ToString() + ",";
                cell += data.cell_.ToString() + ",";
                ViewState["ip_address"] += data.ip_address.ToString() + ",";
                ViewState["serial_number"] += data.serial_number.ToString() + ",";
                ViewState["type_name"] += data.type_name.ToString() + ",";
                ViewState["category_name"] += data.category_name.ToString() + ",";
            }
            // ViewState["row_"] = point;
            text.Text = row;

            ViewState["Locname"] = _dtnwl.BoxnwlList[0].Locname;
            ViewState["room_name"] = _dtnwl.BoxnwlList[0].room_name;
            ViewState["Floor_name"] = _dtnwl.BoxnwlList[0].Floor_name;
            ViewState["img_name"] = _dtnwl.BoxnwlList[0].img_name;

        }








    }
    */

    #endregion

    #region ddlselectedchange
    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (DropDownList)sender;

        switch (ddName.ID)
        {

            case "ddltypereport":
                if (int.Parse(ddltypereport.SelectedValue) == 1)
                {
                    div_table.Visible = true;
                    div_report.Visible = false;
                    ddlsearch.SelectedValue = "0";
                    btnexport.Visible = true;
                }
                else if (int.Parse(ddltypereport.SelectedValue) == 2)
                {
                    div_table.Visible = false;
                    div_report.Visible = true;
                    panel_only.Visible = false;
                    panel_searchposition.Visible = false;
                    ddl_chart.SelectedValue = "0";
                    btnexport.Visible = false;
                }
                else
                {
                    div_table.Visible = false;
                    div_report.Visible = false;
                    ddlsearch.SelectedValue = "0";
                    ddl_chart.SelectedValue = "0";
                    txtdevices.Text = String.Empty;
                    ddlre_place.SelectedValue = "0";
                    ddlre_build.SelectedValue = "0";
                    ddlre_floor.SelectedValue = "0";
                    ddlre_cate.SelectedValue = "0";
                    btnexport.Visible = true;

                }
                break;

            case "ddlSearchDate":

                if (int.Parse(ddlSearchDate.SelectedValue) == 3)
                {
                    AddEndDate.Enabled = true;
                }
                else
                {
                    AddEndDate.Enabled = false;
                    AddEndDate.Text = string.Empty;
                }

                break;

            case "ddlsearch":

                SelectBuilding();
                SelectFloor();
                SelectPlace();
                ddl_chart.SelectedValue = "0";

                if (ddlsearch.SelectedValue == "1")
                {
                    panel_searchposition.Visible = true;

                    panel_only.Visible = false;
                }
                else if (ddlsearch.SelectedValue == "2")
                {

                    panel_only.Visible = true;
                    div_ddlre_cate.Visible = true;
                    div_txtdevices.Visible = true;
                    panel_searchposition.Visible = false;

                }


                //else if (ddlsearch.SelectedValue == "3")
                //{
                //    panel_only.Visible = false;
                //    panel_searchposition.Visible = false;
                //}
                //else if (ddlsearch.SelectedValue == "0")
                //{
                //    panel_only.Visible = false;
                //    panel_searchposition.Visible = false;
                //}

                break;

            case "ddlbuildsearch":

                _dtnwl = new data_networklayout();
                _dtnwl.BoxnwlList = new NWLList[1];
                NWLList search_img1 = new NWLList();

                search_img1.LocIDX = int.Parse(ViewState["placeidx_plan"].ToString());
                search_img1.BUIDX = int.Parse(ddlbuildsearch.SelectedValue);

                _dtnwl.BoxnwlList[0] = search_img1;
                _dtnwl = callServiceLayout(urlSelect_Floor_Plan, _dtnwl);


                setDdlData(ddlfloorsearch, _dtnwl.BoxnwlList, "Floor_name", "FLIDX");
                ddlfloorsearch.Items.Insert(0, new ListItem("กรุณาเลือกชั้นตั้งอุปกรณ์...", "0"));

                break;

            case "ddlfloorsearch":

                div_plan.Visible = true;
                SetData();

                _dtnwl = new data_networklayout();
                _dtnwl.BoxnwlList = new NWLList[1];
                NWLList search_img = new NWLList();

                search_img.LocIDX = int.Parse(ViewState["placeidx_plan"].ToString());
                search_img.BUIDX = int.Parse(ddlbuildsearch.SelectedValue);
                search_img.FLIDX = int.Parse(ddlfloorsearch.SelectedValue);

                _dtnwl.BoxnwlList[0] = search_img;
                //  text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtnwl));
                _dtnwl = callServiceLayout(urlSelect_Image_Layout, _dtnwl);

                if (_dtnwl.ReturnCode == "0")
                {

                    string path = _dtnwl.BoxnwlList[0].img_name + "/";
                    string namefile = _dtnwl.BoxnwlList[0].img_name + ".jpg";

                    string getPathLotus = ConfigurationSettings.AppSettings["pathfile_networklayout"];

                    //string a = getPathLotus + path + namefile;
                    //text.Text = a;
                    images.Visible = true;
                    GvMaster.Visible = true;
                    images.ImageUrl = (getPathLotus + path + namefile);
                }
                else
                {
                    images.Visible = false;
                    GvMaster.Visible = false;
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่พบข้อมูลภาพแผนผัง');", true);
                }
                break;

            case "ddl_chart":

                ddlsearch.SelectedValue = "0";
                if (ddl_chart.SelectedValue == "1" || ddl_chart.SelectedValue == "2" || ddl_chart.SelectedValue == "3")
                {
                    panel_only.Visible = true;
                    lblcate.Visible = false;
                    div_ddlre_cate.Visible = false;
                    //  lbldevice.Visible = true;
                    div_txtdevices.Visible = true;
                }
                else if (ddl_chart.SelectedValue == "4")
                {
                    panel_only.Visible = true;
                    lblcate.Visible = true;
                    div_ddlre_cate.Visible = true;
                    //  lbldevice.Visible = false;
                    div_txtdevices.Visible = false;
                }
                else if (ddl_chart.SelectedValue == "5")
                {
                    panel_only.Visible = false;
                    panel_only.Visible = false;
                    panel_searchposition.Visible = false;

                }



                break;

        }
    }
    #endregion

    #region SetFunction

    protected void SetData() //Bind Gridview
    {
        DataTable dt = new DataTable();

        if (dt.Columns.Count == 0)
        {
            for (int j = 0; j < 50; j++)
            {
                for (int b = j; b < 50; b++)
                {
                    dt.Columns.Add(Convert.ToString(b), typeof(string));
                    DataRow dr = dt.NewRow();
                    dr[j] = "";
                    dt.Rows.Add(dr);

                    j++;
                }
            }
        }


        GvMaster.DataSource = dt;// files;
        GvMaster.DataBind();
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormViewData(FormView fvName, Object obj)
    {
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        ddlName.Items.Clear();
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected data_networkdevices callServiceNetwork(string _cmdUrl, data_networkdevices _data_networkdevices)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_networkdevices);
        // litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);

        // convert json to object
        _data_networkdevices = (data_networkdevices)_funcTool.convertJsonToObject(typeof(data_networkdevices), _localJson);

        return _data_networkdevices;
    }

    protected data_networklayout callServiceLayout(string _cmdUrl, data_networklayout _dtnwl)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dtnwl);
        // litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);

        // convert json to object
        _dtnwl = (data_networklayout)_funcTool.convertJsonToObject(typeof(data_networklayout), _localJson);

        return _dtnwl;
    }

    protected data_employee callServiceEmpProfile(string _cmdUrl)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger1 = new PostBackTrigger();
        trigger1.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger1);
    }

    /* protected void SetDefault()
     {
         SelectCategory();
         SelectType();
         select_empIdx_present();
         Select_ChartNPW_Pie();
         Select_ChartNPW_Colum();
         Select_ChartIDC_Pie();
         Select_ChartIDC_Colum();
         Select_ChartRJN_Pie();
         Select_ChartRJN_Colum();
         Select_ChartMTT_Pie();
         Select_ChartMTT_Colum();
         BoxSearch.Visible = false;
         BoxButtonSearchShow.Visible = true;
         BoxButtonSearchHide.Visible = false;
         lbindex.BackColor = System.Drawing.Color.LightGray;
         lbreport.BackColor = System.Drawing.Color.Transparent;
         linkBtnTrigger(lbindex);
         linkBtnTrigger(lbreport);
     }
     */


    #endregion

    #region Gridview

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    cell = "";
                    row = "";
                    register_number = "";
                    no_regis = "";
                    ip_address = "";
                    serial_number = "";
                    type_name = "";
                    category_name = "";
                 
                    e.Row.Cells[0].ForeColor = Color.White;
                    int index = e.Row.RowIndex;

                    _dtnwl = new data_networklayout();

                    _dtnwl.BoxnwlList = new NWLList[1];
                    NWLList search = new NWLList();

                    search.LocIDX = int.Parse(ViewState["placeidx_plan"].ToString());
                    search.FLIDX = int.Parse(ddlfloorsearch.SelectedValue);

                    _dtnwl.BoxnwlList[0] = search;
                    _dtnwl = callServiceLayout(urlSelectNWL, _dtnwl);

                    if (_dtnwl.ReturnCode == "0")
                    {
                        foreach (var data in _dtnwl.BoxnwlList)
                        {
                            cell = data.cell_.ToString();
                            row = data.row_.ToString();
                            register_number = data.register_number.ToString();
                            no_regis = data.no_regis.ToString();
                            ip_address = data.ip_address.ToString();
                            serial_number = data.serial_number.ToString();
                            type_name = data.type_name.ToString();
                            category_name = data.category_name.ToString();
                            Locname = data.place_name.ToString();
                            status_active = data.status_active;
                            //int u0idx = data.u0idx;
                            //text.Text += Convert.ToString(u0idx) + ",";

                      
                            if (index == int.Parse(row))
                            {
                                //  e.Row.Cells[int.Parse(cell)].Text = "444";
                                e.Row.Cells[int.Parse(cell)].ToolTip = "รหัสทะเบียนอุปกรณ์ : " + register_number + "\n" +
                                    " ชนิดอุปกรณ์ : " + category_name + "\n" +
                                   " ประเภทอุปกรณ์ : " + type_name + "\n" +
                                   " ไอพี : " + ip_address + "\n" +
                                   " เลขอุปกรณ์ :" + no_regis + "\n" +
                                   " Serial Number :" + serial_number;


                                Button btnstatus = new Button();
                                btnstatus.ID = "btnstatus";
                              
                                btnstatus.Enabled = false;
                                btnstatus.Style.Add("Height", "10px");


                                btnstatus.OnClientClick = (e.Row.DataItem as DataRowView).Row[index].ToString();
                                e.Row.Cells[int.Parse(cell)].Controls.Add(btnstatus);

                                if (status_active == 1)
                                {
                                    btnstatus.BackColor = Color.Green;
                                    btnstatus.BorderColor = Color.Green;
                                }
                                else if (status_active == 2)
                                {
                                    btnstatus.BackColor = Color.Red;
                                    btnstatus.BorderColor = Color.Red;
                                }
                                else if (status_active == 0)
                                {
                                    btnstatus.BackColor = Color.Yellow;
                                    btnstatus.BorderColor = Color.Yellow;
                                }

                            }

                        }

                    }
                }


                break;

            case "GvNPW":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                break;

            case "GVIDC":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                break;

            case "GvViewDevices":

                var lblregister_number = ((Label)e.Row.FindControl("lblregister_number"));
                var image = ((System.Web.UI.WebControls.Image)e.Row.FindControl("image"));
                var Statusactive = ((Label)e.Row.FindControl("Statusactive"));
                var lbstate_ok = ((Label)e.Row.FindControl("lbstate_ok"));
                var lbstate_where = ((Label)e.Row.FindControl("lbstate_where"));
                var lbstate_no = ((Label)e.Row.FindControl("lbstate_no"));

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    //if (GvViewDevices.EditIndex != e.Row.RowIndex) //to overlook header row
                    //{
                    //  int index_ = e.Row.RowIndex;
                    //ViewState["brandname"] = brandname;
                    //ViewState["generation"] = generation;

                    _dtnwl = new data_networklayout();

                    _dtnwl.BoxnwlList = new NWLList[1];
                    NWLList search = new NWLList();

                    search.type_idx = int.Parse(ddltype.SelectedValue);
                    search.category_idx = int.Parse(ddlcate.SelectedValue);

                    _dtnwl.BoxnwlList[0] = search;
                    //  text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtnwl));

                    _dtnwl = callServiceLayout(urlSelect_Device_Cat, _dtnwl);



                    string getPathLotus = ConfigurationSettings.AppSettings["upload_networkdevices_file"];
                    string path = lblregister_number.Text + "/";
                    string namefile = lblregister_number.Text + ".jpg";

                    image.ImageUrl = getPathLotus + path + namefile;


                    if (Statusactive.Text == "0") // Not Found
                    {
                        lbstate_no.Visible = false;
                        lbstate_where.Visible = true;
                        lbstate_ok.Visible = false;
                        lbstate_where.ForeColor = System.Drawing.ColorTranslator.FromHtml("#FFCC00");
                        lbstate_where.Style["font-weight"] = "bold";
                        lbstate_where.Visible = true;
                    }
                    else if (Statusactive.Text == "1") // Active
                    {
                        lbstate_no.Visible = false;
                        lbstate_ok.Visible = true;
                        lbstate_where.Visible = false;
                        lbstate_ok.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC00");
                        lbstate_ok.Style["font-weight"] = "bold";
                        lbstate_ok.Visible = true;
                    }
                    else if (Statusactive.Text == "2") // Inactive
                    {
                        lbstate_no.Visible = true;
                        lbstate_ok.Visible = false;
                        lbstate_where.Visible = false;
                        lbstate_no.ForeColor = System.Drawing.ColorTranslator.FromHtml("#FF0000");
                        lbstate_ok.Style["font-weight"] = "bold";
                        lbstate_no.Visible = true;
                    }


                    //}

                }

                break;

        }

    }


    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvViewDevices":

                GvViewDevices.PageIndex = e.NewPageIndex;
                GvViewDevices.DataBind();

                Select_Device();
                mergeCell();
                break;

            case "GvReport":
                GvReport.PageIndex = e.NewPageIndex;
                GvReport.DataBind();


                Select_Report_Table();
                break;

            case "GvNPW":
                GvNPW.PageIndex = e.NewPageIndex;
                GvNPW.DataBind();


                Select_ChartNPW_Pie();
                break;

            case "GvRJN":
                GvRJN.PageIndex = e.NewPageIndex;
                GvRJN.DataBind();


                Select_ChartRJN_Pie();
                break;

            case "GVIDC":
                GVIDC.PageIndex = e.NewPageIndex;
                GVIDC.DataBind();


                Select_ChartIDC_Pie();
                break;

            case "GvMTT":
                GvMTT.PageIndex = e.NewPageIndex;
                GvMTT.DataBind();

                Select_ChartMTT_Pie();

                break;


        }
    }

    #endregion

    #endregion

    #region MergeCell
    protected void mergeCell()
    {
        for (int rowIndex = GvViewDevices.Rows.Count - 2; rowIndex >= 0; rowIndex--)
        {
            GridViewRow currentRow = GvViewDevices.Rows[rowIndex];
            GridViewRow previousRow = GvViewDevices.Rows[rowIndex + 1];

            if (((Literal)currentRow.Cells[6].FindControl("Literal7")).Text == ((Literal)previousRow.Cells[6].FindControl("Literal7")).Text &&
                ((Label)currentRow.Cells[7].FindControl("lbStatusComment")).Text == ((Label)previousRow.Cells[7].FindControl("lbStatusComment")).Text)
            {
                if (previousRow.Cells[0].RowSpan < 2)
                {
                    currentRow.Cells[0].RowSpan = 2;
                    currentRow.Cells[6].RowSpan = 2;
                    currentRow.Cells[7].RowSpan = 2;
                }
                else
                {
                    currentRow.Cells[0].RowSpan = previousRow.Cells[0].RowSpan + 1;
                    currentRow.Cells[6].RowSpan = previousRow.Cells[6].RowSpan + 1;
                    currentRow.Cells[7].RowSpan = previousRow.Cells[7].RowSpan + 1;
                }
                previousRow.Cells[0].Visible = false;
                previousRow.Cells[6].Visible = false;
                previousRow.Cells[7].Visible = false;
            }
            if (((Label)currentRow.Cells[2].FindControl("Literal5")).Text == ((Label)previousRow.Cells[2].FindControl("Literal5")).Text)
            {
                if (previousRow.Cells[2].RowSpan < 2)
                {
                    currentRow.Cells[2].RowSpan = 2;
                }
                else
                {
                    currentRow.Cells[2].RowSpan = previousRow.Cells[2].RowSpan + 1;
                }
                previousRow.Cells[2].Visible = false;

            }
            if (((Literal)currentRow.Cells[6].FindControl("Literal7")).Text == ((Literal)previousRow.Cells[6].FindControl("Literal7")).Text)
            {
                if (previousRow.Cells[6].RowSpan < 2)
                {
                    currentRow.Cells[6].RowSpan = 2;
                }
                else
                {
                    currentRow.Cells[6].RowSpan = previousRow.Cells[6].RowSpan + 1;
                }
                previousRow.Cells[6].Visible = false;

            }

            if (((Label)currentRow.Cells[7].FindControl("lbStatusComment")).Text == ((Label)previousRow.Cells[7].FindControl("lbStatusComment")).Text)
            {
                if (previousRow.Cells[7].RowSpan < 2)
                {
                    currentRow.Cells[7].RowSpan = 2;
                }
                else
                {
                    currentRow.Cells[7].RowSpan = previousRow.Cells[7].RowSpan + 1;
                }
                previousRow.Cells[7].Visible = false;

            }
            if (((Label)currentRow.Cells[3].FindControl("ltSystemName")).Text == ((Label)previousRow.Cells[3].FindControl("ltSystemName")).Text)
            {
                if (previousRow.Cells[3].RowSpan < 2)
                {
                    currentRow.Cells[3].RowSpan = 2;
                }
                else
                {
                    currentRow.Cells[3].RowSpan = previousRow.Cells[3].RowSpan + 1;
                }
                previousRow.Cells[3].Visible = false;

            }
        }
    }

    #endregion

    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {

            case "btnIndex":
                lbindex.BackColor = System.Drawing.Color.LightGray;
                lbreport.BackColor = System.Drawing.Color.Transparent;


                //MvMaster.SetActiveView(ViewIndex);
                //SetDefault();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "btnReport":
                lbindex.BackColor = System.Drawing.Color.Transparent;
                lbreport.BackColor = System.Drawing.Color.LightGray;
                MvMaster.SetActiveView(ViewReport);

                panel_chart.Visible = false;
                SelectBuilding();
                SelectFloor();
                SelectPlace();
                break;

            case "btnsearch":

                MvMaster.SetActiveView(ViewDevices);
                Select_Device();
                mergeCell();

                break;

            case "BtnBack":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                //linkBtnTrigger(btnback_position);
                //linkBtnTrigger(btnback_viewdevice);

                //MvMaster.SetActiveView(ViewIndex);

                //SetDefault();


                break;

            case "BtnHideSETBoxAllSearchShow":

                BoxSearch.Visible = true;
                BoxButtonSearchShow.Visible = false;
                BoxButtonSearchHide.Visible = true;
                break;

            case "BtnHideSETBoxAllSearchHide":

                BoxSearch.Visible = false;
                BoxButtonSearchShow.Visible = true;
                BoxButtonSearchHide.Visible = false;

                break;

            case "ViewLog":
                string[] arg1_Edit = new string[0];
                arg1_Edit = e.CommandArgument.ToString().Split(';');
                int U0IDX_Edit = int.Parse(arg1_Edit[0]);



                _dtnwl = new data_networklayout();
                _dtnwl.BoxnwlList = new NWLList[1];
                NWLList searchlv1 = new NWLList();
                searchlv1.u0idx = U0IDX_Edit;

                _dtnwl.BoxnwlList[0] = searchlv1;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_system1));
                _dtnwl = callServiceLayout(urlSelect_LogLayout, _dtnwl);

                rptLog.DataSource = _dtnwl.BoxnwlList;
                rptLog.DataBind();

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);

                break;

            case "btnhide":
                ViewState["placeidx"] = null;

                //string[] arg1 = new string[1];
                //arg1 = e.CommandArgument.ToString().Split(';');
                //ViewState["placeidx_plan"] = int.Parse(arg1[0]);
               

                string arg1;
                arg1 = e.CommandArgument.ToString();
                ViewState["placeidx_plan"] = int.Parse(arg1);



          
                _dtnwl = new data_networklayout();
                _dtnwl.BoxnwlList = new NWLList[1];
                NWLList search_img = new NWLList();

                search_img.LocIDX = int.Parse(ViewState["placeidx_plan"].ToString());

                _dtnwl.BoxnwlList[0] = search_img;
                _dtnwl = callServiceLayout(urlSelectPlan_nwl, _dtnwl);
                

                ddlbuildsearch.Items.Clear();
                ddlbuildsearch.AppendDataBoundItems = true;
                setDdlData(ddlbuildsearch, _dtnwl.BoxnwlList, "room_name", "BUIDX");
                ddlbuildsearch.Items.Insert(0, new ListItem("กรุณาเลือกอาคารตั้งอุปกรณ์...", "0"));

                MvMaster.SetActiveView(ViewPosition);

              
                break;

            case "btndevice":

                string[] arg1_u0idx = new string[0];
                arg1_u0idx = e.CommandArgument.ToString().Split(';');
                int u0idx = int.Parse(arg1_u0idx[0]);

                MvMaster.SetActiveView(ViewFVDevice);
                FormView FvDetailDevices = (FormView)ViewFVDevice.FindControl("FvDetailDevices");


                _dtnwl = new data_networklayout();
                _dtnwl.BoxnwlList = new NWLList[1];
                NWLList search_device = new NWLList();

                search_device.u0idx = u0idx;

                _dtnwl.BoxnwlList[0] = search_device;
                //   text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtnwl));
                _dtnwl = callServiceLayout(urlSelect_Device_FV, _dtnwl);

                setFormViewData(FvDetailDevices, _dtnwl.BoxnwlList);

                //  string path = _dtnwl.BoxnwlList[0].register_number + "/";
                //  string namefile = _dtnwl.BoxnwlList[0].register_number + ".jpg";

                Label lbl_status = (Label)FvDetailDevices.FindControl("lbl_status");
                Label lbstate_ok = (Label)FvDetailDevices.FindControl("lbstate_ok");
                Label lbstate_where = (Label)FvDetailDevices.FindControl("lbstate_where");
                Label lbstate_no = (Label)FvDetailDevices.FindControl("lbstate_no");


                if (lbl_status.Text == "0")
                {
                    lbstate_where.ForeColor = System.Drawing.ColorTranslator.FromHtml("#FFCC00");
                    lbstate_where.Style["font-weight"] = "bold";

                    lbstate_ok.Visible = false;
                    lbstate_where.Visible = true;
                    lbstate_no.Visible = false;
                }
                else if (lbl_status.Text == "1")
                {
                    lbstate_ok.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC00");
                    lbstate_ok.Style["font-weight"] = "bold";

                    lbstate_ok.Visible = true;
                    lbstate_where.Visible = false;
                    lbstate_no.Visible = false;
                }

                else if (lbl_status.Text == "2")
                {
                    lbstate_no.ForeColor = System.Drawing.ColorTranslator.FromHtml("#FF0000");
                    lbstate_ok.Style["font-weight"] = "bold";

                    lbstate_ok.Visible = false;
                    lbstate_where.Visible = false;
                    lbstate_no.Visible = true;
                }

                string getPathLotus = ConfigurationSettings.AppSettings["upload_networkdevices_file"];
                string path = _dtnwl.BoxnwlList[0].register_number + "/";
                string namefile = _dtnwl.BoxnwlList[0].register_number + ".jpg";


                img_devices.ImageUrl = getPathLotus + path + namefile;

                _dtnwl = new data_networklayout();
                _dtnwl.BoxnwlList = new NWLList[1];
                NWLList search_device_log = new NWLList();

                search_device_log.u0idx = u0idx;

                _dtnwl.BoxnwlList[0] = search_device_log;
                //   text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtnwl));
                _dtnwl = callServiceLayout(urlSelect_Log_BindFV, _dtnwl);

                Repeater_Log.DataSource = _dtnwl.BoxnwlList;
                Repeater_Log.DataBind();
                //   setFormViewData(FvDetailDevices, _dtnwl.BoxnwlList);

                break;

            case "btnsearch_report":
                div_datagrid.Visible = true;


                switch (ddltypereport.SelectedValue)
                {
                    case "1":

                        if (ddlsearch.SelectedValue == "1")
                        {
                            ddlre_cate.SelectedValue = "0";
                            txtdevices.Text = String.Empty;
                        }
                        else if (ddlsearch.SelectedValue == "2")
                        {
                            ddlre_place.SelectedValue = "0";
                            ddlre_build.SelectedValue = "0";
                            ddlre_floor.SelectedValue = "0";

                        }
                        else
                        {
                            ddlre_place.SelectedValue = "0";
                            ddlre_build.SelectedValue = "0";
                            ddlre_floor.SelectedValue = "0";
                            ddlre_cate.SelectedValue = "0";
                            txtdevices.Text = String.Empty;
                        }
                        div_gridreport.Visible = true;
                        div_chartreport.Visible = false;
                        Select_Report_Table();

                        break;
                    case "2":
                        div_gridreport.Visible = false;
                        div_chartreport.Visible = true;

                        if (ddl_chart.SelectedValue == "1")
                        {
                            Select_Report_Chart_noregis();
                        }
                        else if (ddl_chart.SelectedValue == "2")
                        {
                            Select_Report_Chart_ipaddress();
                        }
                        else if (ddl_chart.SelectedValue == "3")
                        {
                            Select_Report_Chart_regis();
                        }
                        else if (ddl_chart.SelectedValue == "4")
                        {
                            Select_Report_Chart_category();
                        }
                        else if (ddl_chart.SelectedValue == "5")
                        {
                            Select_Report_Chart_place();
                        }




                        break;
                }

                break;


            case "btnexport":
                _dtnwl = new data_networklayout();

                _dtnwl.BoxnwlList = new NWLList[1];
                NWLList search = new NWLList();

                search.category_idx = int.Parse(ddlre_cate.SelectedValue);
                search.Search = txtdevices.Text;
                search.LocIDX = int.Parse(ddlre_place.SelectedValue);
                search.BUIDX = int.Parse(ddlre_build.SelectedValue);
                search.FLIDX = int.Parse(ddlre_floor.SelectedValue);
                search.create_log = AddStartdate.Text;
                search.end_log = AddEndDate.Text;
                search.IFSearchbetween = int.Parse(ddlSearchDate.SelectedValue);

                _dtnwl.BoxnwlList[0] = search;
                text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtnwl));

                _dtnwl = callServiceLayout(urlSelect_Export, _dtnwl);

                GridView2.DataSource = _dtnwl.Export_BoxNWL;
                GridView2.DataBind();

                GridView2.AllowSorting = false;
                GridView2.AllowPaging = false;

                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.xls");
                // Response.Charset = ""; set character 
                Response.Charset = "utf-8";
                Response.ContentEncoding = System.Text.Encoding.GetEncoding("utf-8");
                Response.ContentType = "application/vnd.ms-excel";

                Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
                Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>");


                StringWriter sw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(sw);


                GridView2.Columns[0].Visible = true;
                GridView2.HeaderRow.BackColor = Color.White;

                foreach (TableCell cell in GridView2.HeaderRow.Cells)
                {
                    cell.BackColor = GridView2.HeaderStyle.BackColor;
                }

                foreach (GridViewRow row in GridView2.Rows)
                {
                    row.BackColor = Color.White;
                    foreach (TableCell cell in row.Cells)
                    {
                        if (row.RowIndex % 2 == 0)
                        {
                            cell.BackColor = GridView2.AlternatingRowStyle.BackColor;
                        }
                        else
                        {
                            cell.BackColor = GridView2.RowStyle.BackColor;
                        }
                        cell.CssClass = "textmode";
                    }
                }

                GridView2.RenderControl(hw);

                //style to format numbers to string
                string style = @"<style> .textmode { mso-number-format:\@; font-family: myFirstFont; } </style>";
                Response.Write(style);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();

                break;

            case "BtnRefresh":

                ddltypereport.SelectedValue = "0";
                ddlsearch.SelectedValue = "0";
                ddl_chart.SelectedValue = "0";
                ddlSearchDate.SelectedValue = "0";
                AddStartdate.Text = String.Empty;
                AddEndDate.Text = String.Empty;
                ddlre_cate.SelectedValue = "0";
                txtdevices.Text = String.Empty;
                ddlre_place.SelectedValue = "0";
                ddlre_build.SelectedValue = "0";
                ddlre_floor.SelectedValue = "0";

                break;

        }
    }

    #endregion
}

