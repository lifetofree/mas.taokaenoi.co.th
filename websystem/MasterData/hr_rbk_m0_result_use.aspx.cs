﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_hr_rbk_m0_result_use : System.Web.UI.Page
{

    #region Connect
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();
    data_roombooking _data_roombooking = new data_roombooking();

    data_qa_cims _data_qa_cims = new data_qa_cims();


    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    //-- employee --//

    //-- roombooking --//
    static string _urlSetRbkm0ResultUse = _serviceUrl + ConfigurationManager.AppSettings["urlSetRbkm0ResultUse"];
    static string _urlGetRbkm0ResultUse = _serviceUrl + ConfigurationManager.AppSettings["urlGetRbkm0ResultUse"];
    static string _urlSetRbkm0ResultUseDel = _serviceUrl + ConfigurationManager.AppSettings["urlSetRbkm0ResultUseDel"];


    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    bool _flag_qmr = false;
    #endregion Connect

    #region Page_Init
    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
        //check permission
        //foreach (int item in rdept_qmr)
        //{
        //    if (_dataEmployee.employee_list[0].rdept_idx == item)
        //    {
        //        _flag_qmr = true;
        //        break;
        //    }
        //}
    }
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //ViewState["empIDX"] = Session["emp_idx"];
            SelectResultUse();

        }
    }
    #endregion Page Load

    #region initPage
    protected void initPage()
    {
        MvSystem.SetActiveView(ViewIndex);
        SelectResultUse();
    }
    #endregion

    #region getEmployeeProfile
    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }
    #endregion

    #region setformdata
    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    #endregion setformdata

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        switch (cmdName)
        {
            case "cmdAddResultUse":

                GvResultUse.EditIndex = -1;
                SelectResultUse();
                btnAddResultUse.Visible = false;
                setFormData(FvInsert, FormViewMode.Insert, null);
                FvInsert.Visible = true;

                break;

            case "CmdSave":
                InsertResultUse();
                SelectResultUse();
                btnAddResultUse.Visible = true;
                FvInsert.Visible = false;
                break;

            case "CmdCancel":
                btnAddResultUse.Visible = true;
                FvInsert.Visible = false;

                break;

            case "CmdDelete":

                int resultuse_del = int.Parse(cmdArg);

                data_roombooking data_m0_result_del = new data_roombooking();
                rbk_m0_result_use_detail m0_result_del = new rbk_m0_result_use_detail();
                data_m0_result_del.rbk_m0_result_use_list = new rbk_m0_result_use_detail[1];

                m0_result_del.result_use_idx = resultuse_del;
                m0_result_del.cemp_idx = _emp_idx;

                data_m0_result_del.rbk_m0_result_use_list[0] = m0_result_del;
                data_m0_result_del = callServicePostRoomBooking(_urlSetRbkm0ResultUseDel, data_m0_result_del);

                SelectResultUse();
                //Gv_select_unit.Visible = false;
                btnAddResultUse.Visible = true;
                FvInsert.Visible = false;
                break;
        }
    }
    #endregion btnCommand

    #region Custom Functions
    protected void InsertResultUse()
    {

        TextBox txtresult_use_name = (TextBox)FvInsert.FindControl("txtresult_use_name");
        DropDownList ddlResultUseStatus = (DropDownList)FvInsert.FindControl("ddlResultUseStatus");

        data_roombooking data_m0_result_use = new data_roombooking();
        rbk_m0_result_use_detail m0_result_use_insert = new rbk_m0_result_use_detail();
        data_m0_result_use.rbk_m0_result_use_list = new rbk_m0_result_use_detail[1];

        m0_result_use_insert.result_use_idx = 0;
        m0_result_use_insert.result_use_name = txtresult_use_name.Text;
        m0_result_use_insert.cemp_idx = _emp_idx;
        m0_result_use_insert.result_use_status = int.Parse(ddlResultUseStatus.SelectedValue);

        data_m0_result_use.rbk_m0_result_use_list[0] = m0_result_use_insert;
        data_m0_result_use = callServicePostRoomBooking(_urlSetRbkm0ResultUse, data_m0_result_use);

        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Unit_b.return_code));
        if (data_m0_result_use.return_code == 1)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);

        }
        else
        {
            txtresult_use_name.Text = String.Empty;
            //tex_place_code.Text = String.Empty;
        }
    }

    protected void SelectResultUse()
    {

        data_roombooking data_m0_resultuse_detail = new data_roombooking();
        rbk_m0_result_use_detail m0_resultuse_detail = new rbk_m0_result_use_detail();
        data_m0_resultuse_detail.rbk_m0_result_use_list = new rbk_m0_result_use_detail[1];

        m0_resultuse_detail.condition = 0;

        data_m0_resultuse_detail.rbk_m0_result_use_list[0] = m0_resultuse_detail;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_m0_resultuse_detail = callServicePostRoomBooking(_urlGetRbkm0ResultUse, data_m0_resultuse_detail);

        setGridData(GvResultUse, data_m0_resultuse_detail.rbk_m0_result_use_list);
      
    }
    #endregion Custom Functions

    #region setGridData
    private void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region Masterbound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvResultUse":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbl_result_use_status = (Label)e.Row.Cells[3].FindControl("lbl_result_use_status");
                    Label result_use_statusOnline = (Label)e.Row.Cells[3].FindControl("result_use_statusOnline");
                    Label result_use_statusOffline = (Label)e.Row.Cells[3].FindControl("result_use_statusOffline");

                    ViewState["vs_resultuse_status"] = lbl_result_use_status.Text;


                    if (ViewState["vs_resultuse_status"].ToString() == "1")
                    {
                        result_use_statusOnline.Visible = true;
                    }
                    else if (ViewState["vs_resultuse_status"].ToString() == "0")
                    {
                        result_use_statusOffline.Visible = true;
                    }

                }


                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    btnAddResultUse.Visible = true;
                    FvInsert.Visible = false;

                }



                break;


        }
    }
    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvResultUse":
                GvResultUse.EditIndex = e.NewEditIndex;
                SelectResultUse();
                break;

        }
    }
    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

        var GvName = (GridView)sender;
        //Label5.Text = GvName.ID;
        switch (GvName.ID)
        {
            case "GvResultUse":

                var txt_result_use_idx_edit = (TextBox)GvResultUse.Rows[e.RowIndex].FindControl("txt_result_use_idx_edit");
                var txt_result_use_name_edit = (TextBox)GvResultUse.Rows[e.RowIndex].FindControl("txt_result_use_name_edit");
                var ddlresult_use_status_edit = (DropDownList)GvResultUse.Rows[e.RowIndex].FindControl("ddlresult_use_status_edit");


                GvResultUse.EditIndex = -1;


                data_roombooking data_m0_resultuse_edit = new data_roombooking();
                rbk_m0_result_use_detail m0_resultuse_edit = new rbk_m0_result_use_detail();
                data_m0_resultuse_edit.rbk_m0_result_use_list = new rbk_m0_result_use_detail[1];

                m0_resultuse_edit.result_use_idx = int.Parse(txt_result_use_idx_edit.Text);
                m0_resultuse_edit.result_use_name = txt_result_use_name_edit.Text;
                m0_resultuse_edit.cemp_idx = _emp_idx;
                m0_resultuse_edit.result_use_status = int.Parse(ddlresult_use_status_edit.SelectedValue);

                data_m0_resultuse_edit.rbk_m0_result_use_list[0] = m0_resultuse_edit;

                //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_resultuse_edit));

                data_m0_resultuse_edit = callServicePostRoomBooking(_urlSetRbkm0ResultUse, data_m0_resultuse_edit);

                if (data_m0_resultuse_edit.return_code == 1)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ข้อมูลซ้ำ กรุณาแก้ไขใหม่อีกครั้ง!!!');", true);
                    SelectResultUse();
                }
                else
                {
                    SelectResultUse();
                }
                break;

        }
    }
    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        //Label5.Text = GvName.ID;
        switch (GvName.ID)
        {
            case "GvResultUse":
                GvResultUse.EditIndex = -1;
                SelectResultUse();

                btnAddResultUse.Visible = true;
                FvInsert.Visible = false;
                break;

        }
    }
    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvResultUse":
                GvResultUse.PageIndex = e.NewPageIndex;
                GvResultUse.DataBind();
                SelectResultUse();
                break;

        }
    }
    #endregion

    #region callService 
    protected data_qa_cims callServicePostMasterQACIMS(string _cmdUrl, data_qa_cims _data_qa_cims)
    {
        _localJson = _funcTool.convertObjectToJson(_data_qa_cims);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_qa_cims = (data_qa_cims)_funcTool.convertJsonToObject(typeof(data_qa_cims), _localJson);


        return _data_qa_cims;
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_roombooking callServicePostRoomBooking(string _cmdUrl, data_roombooking _data_roombooking)
    {
        _localJson = _funcTool.convertObjectToJson(_data_roombooking);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_roombooking = (data_roombooking)_funcTool.convertJsonToObject(typeof(data_roombooking), _localJson);


        return _data_roombooking;
    }
    #endregion callService Functions
}
