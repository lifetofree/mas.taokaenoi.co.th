﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="el_TrnNeedsSurvey.aspx.cs" Inherits="websystem_el_TrnNeedsSurvey" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:HyperLink ID="SETFOCUS" runat="server"></asp:HyperLink>
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    <asp:Panel ID="pnlrightError" runat="server" Visible="false">
        <div class="container">
            <div class="row">

                <%-- <div class="col-md-12" id="panelview" runat="server" >
                    <div class="panel panel-info">
                        <div id="divshownamedetail" runat="server"  class="panel-heading f-bold">--%>

                <asp:Label ID="Litright" runat="server" Text="ยังไม่เปิดให้ขอฝึกอบรม"
                    CssClass="text-center" ForeColor="Red" Font-Size="Medium"></asp:Label>
                <%--</div>
                    </div>
                </div>--%>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlright" runat="server">



        <asp:Panel ID="panelMenu" runat="server">
            <nav class="navbar navbar-default">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand">Menu</a>
                </div>
                <div class="collapse navbar-collapse p-l-10" style="overflow-x: hidden;" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-left" runat="server">
                        <li id="liListData" runat="server">
                            <asp:LinkButton ID="btnListData" runat="server"
                                OnCommand="btnCommand" CommandName="btnListData" Text="ข้อมูลทั่วไป" />
                        </li>
                        <li id="liInsert" runat="server">
                            <asp:LinkButton ID="btnInsert" runat="server"
                                OnCommand="btnCommand" CommandName="btnInsert" Text="ขอฝึกอบรม" />
                        </li>
                        <li id="liApproveList" runat="server">
                            <asp:LinkButton ID="btnApproveList" runat="server"
                                OnCommand="btnCommand" CommandName="btnApproveList" Text="รายการที่รออนุมัติ" />
                        </li>
                        <li id="liApproveCom" runat="server">
                            <asp:LinkButton ID="btnApproveCom" runat="server"
                                OnCommand="btnCommand" CommandName="btnApproveCom" Text="รายการที่อนุมัติเสร็จแล้ว" />
                        </li>
                    </ul>
                </div>
            </nav>
            <div class="clearfix"></div>
        </asp:Panel>
        <asp:Panel ID="pnlListData" runat="server">

            <div class="row">
                <asp:Panel ID="pnlsearch" runat="server">
                    <div class="panel panel-primary">
                        <div class="panel-heading f-bold">ค้นหา</div>
                        <div class="panel-body">
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label>เดือน</label>
                                    <asp:DropDownList ID="ddlMonthSearch_L" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label>ปี</label>
                                    <asp:DropDownList ID="ddlYearSearch_L" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>เลขที่เอกสาร</label>
                                    <asp:TextBox ID="txtFilterKeyword" runat="server"
                                        CssClass="form-control"
                                        placeholder="เลขที่เอกสาร..." />
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label>สถานะของเอกสาร</label>
                                    <asp:DropDownList ID="ddlStatusapprove_L" CssClass="form-control" runat="server">
                                        <asp:ListItem Value="999" Text="-" Selected="True" />
                                        <asp:ListItem Value="0" Text="ดำเนินการ" />
                                        <asp:ListItem Value="4" Text="อนุมัติ" />
                                        <asp:ListItem Value="5" Text="กลับไปแก้ไข" />
                                        <asp:ListItem Value="6" Text="ไม่อนุมัติ" />
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <label>
                                    &nbsp;
                                </label>
                                <div class="form-group">
                                    <asp:LinkButton ID="btnFilter" runat="server" CssClass="btn btn-primary" Text="<i class='fa fa-search'></i> ค้นหา"
                                        OnCommand="btnCommand" CommandName="btnFilter" />
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>

            <div class="row">

                <asp:GridView ID="GvListData"
                    runat="server"
                    AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered table-responsive col-md-12"
                    HeaderStyle-CssClass="info"
                    AllowPaging="true"
                    PageSize="10"
                    OnRowCommand="onRowCommand"
                    OnRowDataBound="onRowDataBound"
                    OnPageIndexChanging="gvPageIndexChanging"
                    DataKeyNames="u0_training_req_idx"
                    ShowFooter="false">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />

                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่พบข้อมูล</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                            <ItemTemplate>
                                <%# (Container.DataItemIndex + 1) %>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="เลขที่เอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                            <ItemTemplate>
                                <%# Eval("training_req_no") %>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="วันที่สร้างรายการ" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                            <ItemTemplate>
                                <%# Eval("u0_training_req_date") %>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="จำนวนหลักสูตร" ItemStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                            <ItemTemplate>
                                <%# Eval("training_qty") %>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="จำนวนผู้เข้าอบรม" ItemStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                            <ItemTemplate>
                                <%# Eval("training_emp_qty") %>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ชื่อผู้มีสิทธิ์อนุมัติ" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%"
                            Visible="false">
                            <ItemTemplate>
                                <small>
                                    <strong>
                                        <asp:Label ID="Label13" runat="server">ผู้อนุมัติที่ 1 : </asp:Label></strong>
                                    <asp:Literal ID="Litertal1" runat="server" Text='<%# Eval("NameApprover1") %>' />
                                    </p>
                                    <strong>
                                        <asp:Label ID="Label15" runat="server">ผู้อนุมัติที่ 2 : </asp:Label></strong>
                                    <asp:Literal ID="Literal3" runat="server" Text='<%# Eval("NameApprover2") %>' />
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สถานะของเอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                            <ItemTemplate>

                                <span><%# getTextDoc((int)Eval("approve_status") , (string)Eval("decision_name")) %>
                                    &nbsp;<%# getTextDoc((int)Eval("approve_status") ,(string)Eval("node_name")) %>
                                    &nbsp;<%# getTextDoc((int)Eval("approve_status") ,"โดย") %>
                                    &nbsp;<%# getTextDoc((int)Eval("approve_status") ,(string)Eval("actor_name")) %></span>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="email_status" runat="server" Text='<%# getStatus((int)Eval("training_req_status")) %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-HorizontalAlign="center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                            <ItemTemplate>

                                <asp:TextBox ID="txtapprove_status_GvListData" runat="server" Visible="false" Text='<%# Eval("approve_status") %>' />
                                <asp:TextBox ID="txttraining_req_created_by_GvListData" runat="server"
                                    Visible="false" Text='<%# Eval("training_req_created_by") %>' />


                                <asp:LinkButton ID="btnDetail"
                                    CssClass="btn btn-info btn-sm" runat="server"
                                    data-original-title="รายละเอียด" data-toggle="tooltip" Text="รายละเอียด"
                                    OnCommand="btnCommand" CommandName="btnDetail" CommandArgument='<%# Eval("u0_training_req_idx") %>'>
                                    <i class="fa fa-file-alt"></i></asp:LinkButton>
                                <asp:LinkButton CssClass="btn btn-warning btn-sm" runat="server"
                                    ID="btnUpdate_GvListData"
                                    CommandName="btnUpdate_GvListData" OnCommand="btnCommand"
                                    data-original-title="แก้ไข" data-toggle="tooltip" Text="แก้ไข"
                                    CommandArgument='<%# Eval("u0_training_req_idx") %>'>
                           <i class="fa fa-pencil-alt"></i>
                                </asp:LinkButton>
                                <asp:LinkButton CssClass="btn btn-danger btn-sm" runat="server"
                                    data-original-title="ลบ" data-toggle="tooltip" Text="ลบ"
                                    ID="btnDelete_GvListData"
                                    CommandName="btnDelete" OnCommand="btnCommand" CommandArgument='<%# Eval("u0_training_req_idx") %>'
                                    OnClientClick="return confirm('คุณต้องการลบข้อมูลนี้ใช่หรือไม่')">
                           <i class="fa fa-trash"></i>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>

            </div>


        </asp:Panel>
        <%-- Start Select --%>
        <asp:Panel ID="pnlselect" runat="server">
            <%-- Start  --%>
            <div class="form-horizontal" role="form">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ข้อมูลส่วนตัว</strong></h3>
                    </div>
                    <div class="panel-body">

                        <asp:FormView ID="FvDetailUser" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                            <ItemTemplate>
                                <%-- <div class="form-horizontal" role="form">--%>

                                <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <%--<asp:Label ID="Label7" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="รหัสผู้ทำรายการ : " />
                                    <div class="col-md-3">
                                        <asp:TextBox ID="txtEmpCode" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("EmpCode") %>' />
                                        <asp:Label ID="LbEmpIDX" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("EmpIDX")%>' />

                                    </div>--%>
                                        <asp:Label ID="Label9" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="ชื่อผู้ทำรายการ : " />
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtEmpName" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("EmpName") %>' />
                                        </div>

                                        <asp:Label ID="lbdate" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="วันที่สร้างรายการ : " />
                                        <div class="col-md-4">
                                            <div class='input-group date'>
                                                <asp:TextBox ID="txtu0_training_req_date" runat="server" CssClass="form-control filter-order-from" />
                                                <span class="input-group-addon show-order-sale-log-from-onclick">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <%--<asp:Label ID="Label4" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="บริษัท : " />
                                    <div class="col-md-3">
                                        <asp:TextBox ID="txtOrgNameTH" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("OrgNameTH")%>' />
                                        <asp:Label ID="LbOrgIDX" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("OrgIDX")%>' />
                                    </div>--%>
                                        <asp:Label ID="Labedl1" class="col-md-2 control-labelnotop text_right" runat="server" Text="แผนก : " />
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtSecNameTH" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("SecNameTH") %>' />
                                            <asp:Label ID="LbSecIDX" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("RSecID")%>' />

                                        </div>

                                        <asp:Label ID="Label34" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="ฝ่าย : " />
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txtDeptNameTH" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("RDeptName")%>' />
                                            <asp:Label ID="LbRDeptIDX" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("RDeptID")%>' />
                                        </div>
                                    </div>
                                </div>


                                <%-------------- แผนก,ตำแหน่ง --------------%>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <%--<asp:Label ID="Labedl1" class="col-md-2 control-labelnotop text_right" runat="server" Text="แผนก : " />
                                    <div class="col-md-3">
                                        <asp:TextBox ID="txtSecNameTH" Enabled="false" CssClass="form-control"  runat="server" Text='<%# Eval("SecNameTH") %>' />
                                        <asp:Label ID="LbSecIDX" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("RSecID")%>' />

                                    </div>--%>

                                        <asp:Label ID="Label8" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="ตำแหน่ง : " />
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtPosNameTH" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("PosNameTH")%>' />
                                            <asp:Label ID="LbPosIDX" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("RPosIDX_J")%>' />
                                        </div>

                                        <asp:Label ID="Label6" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="E-mail : " />
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txtEmail" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("Email") %>' />

                                        </div>
                                    </div>
                                </div>

                                <%-------------- เบอร์ติดต่อ,E-mail --------------%>
                                <%-- <div class="form-group">
                                <div class="col-md-12">

                                    <asp:Label ID="Label5" class="col-md-2 control-labelnotop text_right" runat="server" Text="เบอร์ติดต่อ : " />
                                    <div class="col-md-3">
                                        <asp:TextBox ID="txtMobileNo"  Enabled="false" CssClass="form-control"   runat="server" Text='<%# Eval("MobileNo") %>' />
                                    </div>

                                    
                                </div>
                            </div>--%>


                                <%-------------- Cost Center --------------%>
                                <div class="form-group">
                                    <div class="col-md-12">

                                        <asp:Label ID="Label20" class="col-md-2 control-labelnotop text_right" runat="server" Text="Cost Center : " />
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtCostNo" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("CostNo")%>' />
                                            <asp:Label ID="LoCostIDX" runat="server" Visible="false" CssClass="control-label" Text='<%# Eval("CostIDX")%>' />
                                        </div>
                                        <asp:Panel ID="plndocno" runat="server">
                                            <asp:Label ID="Label1" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="เลขที่เอกสาร : " />
                                            <div class="col-md-4">
                                                <asp:TextBox ID="txttraining_req_no" Enabled="false" CssClass="form-control" runat="server" Text='' />
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </div>

                                <%-- </div>--%>
                            </ItemTemplate>
                        </asp:FormView>

                    </div>
                </div>
                <%-- Start Training Needs Survey --%>

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-pencil"></i><strong>&nbsp; รายละเอียดการขอฝึกอบรม</strong></h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <asp:Label ID="Label20" class="col-md-2 control-labelnotop text_right" runat="server" Text="หลักสูตร : " />
                                    <div class="col-md-3">
                                        <asp:DropDownList ID="ddltranning" runat="server" CssClass="form-control"
                                            AutoPostBack="true" OnSelectedIndexChanged="FvDetail_DataBound" />

                                    </div>

                                    <asp:Label ID="Label1" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="ประเภทการอบรม : " />
                                    <div class="col-md-4">
                                        <asp:RadioButtonList ID="rdltranning_type" runat="server"
                                            CssClass="radio-list-inline-emps" RepeatDirection="Horizontal">
                                            <asp:ListItem Value="0" Text="ฝึกอบรมภายใน" Selected="True" />
                                            <asp:ListItem Value="1" Text="ฝึกอบรมภายนอก" />
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <asp:Panel ID="pnltraning_other" runat="server">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <asp:Label ID="Label12" class="col-md-2 control-labelnotop text_right" runat="server" Text="หลักสูตรอื่นๆ : " />
                                        <div class="col-md-9">
                                            <asp:TextBox ID="txttraning_other" runat="server" CssClass="form-control" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>

                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <asp:Label ID="Label2" class="col-md-2 control-labelnotop text_right" runat="server" Text="ความต้องการ/ความจำเป็นในการฝึกอบรม " />
                                    <div class="col-md-9">
                                        <asp:TextBox ID="txtremark" runat="server" CssClass="form-control"
                                            TextMode="MultiLine" MaxLength="150"
                                            Rows="4" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <asp:Label ID="Label3" class="col-md-2 control-labelnotop text_right" runat="server" Text="งบประมาณต่อรุ่น : " />
                                    <div class="col-md-3">
                                        <asp:TextBox ID="txtAmount" runat="server" CssClass="form-control" TextMode="Number" />
                                    </div>

                                    <asp:Label ID="Label4" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="เดือนที่ต้องการเรียน : " />
                                    <div class="col-md-4">
                                        <div class='input-group date'>
                                            <asp:TextBox ID="txttraning_date" runat="server" CssClass="form-control filter-order-from" />
                                            <span class="input-group-addon show-order-sale-log-from-onclick">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                        <asp:RequiredFieldValidator ID="requiredtxttraning_date"
                                            ValidationGroup="btnSaveInsert" runat="server"
                                            Display="Dynamic"
                                            SetFocusOnError="true"
                                            ControlToValidate="txttraning_date"
                                            Font-Size="1em" ForeColor="Red"
                                            CssClass="pull-left"
                                            ErrorMessage="กรุณากรอกเดือนที่ต้องการเรียน" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <asp:Label ID="Label5" class="col-md-2 control-labelnotop text_right" runat="server" Text="ตำแหน่งที่ต้องการอบรม : " />
                                    <div class="col-md-4">
                                        <div class="panel panel-primary">
                                            <div class="panel-body" style="height: 200px; overflow-x: scroll; overflow-y: scroll;">
                                                <%--<asp:CheckBoxList ID="rdoposition" runat="server" Font-Bold="False" Font-Size="Smaller"
                                                OnSelectedIndexChanged="onSelectedIndexChanged" AutoPostBack="true"
                                                >
                                            </asp:CheckBoxList>--%>
                                                <asp:GridView ID="GvPosition"
                                                    runat="server"
                                                    AutoGenerateColumns="false"
                                                    CssClass="col-md-12"
                                                    HeaderStyle-CssClass="default"
                                                    ShowFooter="false"
                                                    AllowPaging="false"
                                                    ShowHeader="false"
                                                    GridLines="None"
                                                    DataKeyNames="RPosIDX_J">
                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">ไม่พบข้อมูล</div>
                                                    </EmptyDataTemplate>
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                                            HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <asp:CheckBox ID="GvPosition_cb_RPosIDX_J" runat="server"
                                                                        AutoPostBack="True" OnCheckedChanged="onSelectedIndexChanged" />
                                                                </small>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="ชื่อพนักงาน" ItemStyle-HorizontalAlign="Left"
                                                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <asp:Label CssClass="control-label" ID="GvPosition_lbPosName" runat="server" Text='<%# Eval("PosNameTH") %>' />
                                                                    <asp:Label ID="GvPosition_lb_RPosIDX_J" runat="server" Text='<%# Eval("RPosIDX_J") %>' Visible="false" />
                                                                </small>
                                                            </ItemTemplate>
                                                            <EditItemTemplate />
                                                            <FooterTemplate />
                                                        </asp:TemplateField>

                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                        <asp:CheckBox runat="server"
                                            ID="cb_pos_other"
                                            AutoPostBack="True" OnCheckedChanged="onSelectedIndexChanged"></asp:CheckBox>
                                        <asp:Label ID="Label14" CssClass="control-labelnotop text_left" runat="server" Text="ตำแหน่งอื่นๆ" />
                                        <asp:TextBox ID="txtpos_other" runat="server"
                                            CssClass="form-control" MaxLength="150"
                                            Visible="false" />
                                    </div>

                                    <asp:Label ID="Label7" CssClass="col-md-1 control-labelnotop text_right" runat="server" Text="พนักงาน : " />
                                    <div class="col-md-4">
                                        <div class="panel panel-primary">
                                            <div class="panel-body" style="height: 200px; overflow-x: scroll; overflow-y: scroll;">
                                                <asp:GridView ID="GvEmp"
                                                    runat="server"
                                                    AutoGenerateColumns="false"
                                                    CssClass="col-md-12"
                                                    HeaderStyle-CssClass="default"
                                                    ShowFooter="false"
                                                    AllowPaging="false"
                                                    ShowHeader="false"
                                                    GridLines="None"
                                                    DataKeyNames="EmpIDX">
                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">ไม่พบข้อมูล</div>
                                                    </EmptyDataTemplate>
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                                            HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <asp:CheckBox ID="GvEmp_cb_EmpIDX" runat="server"
                                                                        AutoPostBack="True" OnCheckedChanged="onSelectedIndexChanged" />
                                                                </small>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="ชื่อพนักงาน" ItemStyle-HorizontalAlign="Left"
                                                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <asp:Label CssClass="control-label" ID="GvEmp_lb_FullNameTH" runat="server" Text='<%# Eval("FullNameTH") %>' />
                                                                    <asp:Label ID="GvEmp_lb_EmpIDX" runat="server" Text='<%# Eval("EmpIDX") %>' Visible="false" />
                                                                    <asp:Label ID="GvEmp_lb_RPosIDX_J" runat="server" Text='<%# Eval("RPosIDX_J") %>' Visible="false" />
                                                                </small>
                                                            </ItemTemplate>
                                                            <EditItemTemplate />
                                                            <FooterTemplate />
                                                        </asp:TemplateField>

                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <asp:Label ID="Label10" class="col-md-2 control-labelnotop text_right" runat="server" Text="รวมทั้งสิ้นจำนวน : " />
                                    <div class="col-md-3">
                                        <asp:TextBox ID="txtQty" runat="server" CssClass="form-control" TextMode="Number" />
                                    </div>

                                    <asp:Label ID="Label11" CssClass="col-md-2 control-labelnotop text_left" runat="server" Text="คน" />
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="col-md-2">
                                    </div>
                                    <div class="col-md-7">
                                        <asp:LinkButton CssClass="btn btn-primary" runat="server"
                                            ID="btnAdd"
                                            OnCommand="btnCommand" CommandName="btnAdd"
                                            Text="<i class='fa fa-plus fa-lg'></i> Add"
                                            data-toggle="tooltip" title="Add" />
                                        <asp:LinkButton CssClass="btn btn-warning"
                                            ID="btnClear"
                                            OnCommand="btnCommand" CommandName="btnClear"
                                            data-toggle="tooltip" title="Clear" runat="server"
                                            Text="<i class='fa fa-remove fa-lg'></i> Clear" />
                                    </div>
                                </div>
                            </div>
                        </div>





                        <div class="row">

                            <div class="wrapword_p1">

                                <asp:GridView ID="GvTraningList"
                                    runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="table table-striped table-bordered table-responsiv word-wrap"
                                    HeaderStyle-CssClass="info"
                                    AllowPaging="true"
                                    PageSize="10"
                                    OnRowCommand="onRowCommand"
                                    OnRowDataBound="onRowDataBound"
                                    OnPageIndexChanging="gvPageIndexChanging"
                                    ShowFooter="false"
                                    HeaderStyle-Wrap="true"
                                    RowStyle-Wrap="true"
                                    Width="100%">
                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />

                                    <EmptyDataTemplate>
                                        <div style="text-align: center">ไม่พบข้อมูล</div>
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                                            <ItemStyle Wrap="true" />
                                            <ItemTemplate>
                                                <%# (Container.DataItemIndex + 1) %>
                                                <%-- <br>
                                                 <%# Eval("training_req_item") %>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="หลักสูตร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                                            <ItemTemplate>
                                                <div class="word-wrap">
                                                    <%# Eval("training_name") %>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ประเภทการอบรม" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                                            <ItemStyle Wrap="true" />
                                            <ItemTemplate>
                                                <div class="word-wrap">
                                                    <%# Eval("training_req_type_name") %>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ความต้องการ/ความจำเป็นในการฝึกอบรม" ItemStyle-HorizontalAlign="Left"
                                            HeaderStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="25%">
                                            <ItemTemplate>
                                                <div class="word-wrap">
                                                    <%# Eval("training_req_needs") %>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="งบประมาณ/รุ่น" ItemStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                                            <ItemTemplate>
                                                <div class="word-wrap">
                                                    <%# Eval("training_req_budget") %>
                                                </div>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ตำแหน่ง" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%"
                                            Visible="false">
                                            <ItemTemplate>
                                                <div class="word-wrap">
                                                    <%# Eval("u1_PosIDX_name") %>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="จำนวน" ItemStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                                            <ItemTemplate>
                                                <div class="word-wrap">
                                                    <%# Eval("training_req_qty") %>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText=" เดือนที่ต้องการอบรม" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                                            <ItemTemplate>
                                                <div class="word-wrap">
                                                    <%# Eval("training_req_month_study") %>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField ItemStyle-HorizontalAlign="center"
                                            HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btnRemove_GvTraningList" runat="server"
                                                    CssClass="btn btn-danger btn-xs"
                                                    CommandName="btnRemove_GvTraningList">
                                                               <i class="fa fa-times"></i>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>


                                <asp:GridView ID="Gvel_u3_traning_req"
                                    runat="server"
                                    Visible="false"
                                    AutoGenerateColumns="false"
                                    CssClass="table table-striped table-bordered table-responsive col-md-12"
                                    HeaderStyle-CssClass="info"
                                    AllowPaging="false"
                                    OnRowCommand="onRowCommand"
                                    OnRowDataBound="onRowDataBound"
                                    OnPageIndexChanging="gvPageIndexChanging"
                                    ShowFooter="false">
                                    <EmptyDataTemplate>
                                        <div style="text-align: center">ไม่พบข้อมูล</div>
                                    </EmptyDataTemplate>
                                    <Columns>

                                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <%# Eval("icount") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <%# Eval("training_req_item_ref") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="trainingID" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <%# Eval("m0_training_idx_ref") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="training_req_other" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <%# Eval("training_req_other") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="training_req_other" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <%# Eval("training_req_other") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="u3_RPosIDX_ref" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <%# Eval("u3_RPosIDX_ref") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="u3_PosIDX_name" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <%# Eval("u3_PosIDX_name") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="training_req_pos_flag" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <%# Eval("training_req_pos_flag") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="training_req_pos_other" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <%# Eval("training_req_pos_other") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>



                                <asp:GridView ID="Gvel_u2_traning_req"
                                    runat="server"
                                    Visible="false"
                                    AutoGenerateColumns="false"
                                    CssClass="table table-striped table-bordered table-responsive col-md-12"
                                    HeaderStyle-CssClass="info"
                                    AllowPaging="false"
                                    OnRowCommand="onRowCommand"
                                    OnRowDataBound="onRowDataBound"
                                    OnPageIndexChanging="gvPageIndexChanging"
                                    ShowFooter="false">
                                    <EmptyDataTemplate>
                                        <div style="text-align: center">ไม่พบข้อมูล</div>
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <%# Eval("training_req_item_ref") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="trainingID" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <%# Eval("m0_training_idx_ref") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="training_req_other" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <%# Eval("training_req_other") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="u2_PosIDX_ref" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <%# Eval("u2_RPosIDX_ref") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="u2_EmpIDX_ref" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <%# Eval("u2_EmpIDX_ref") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="u2_EmpIDX_ref_name" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <%# Eval("u2_EmpIDX_ref_name") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                    </Columns>
                                </asp:GridView>

                            </div>

                        </div>
                    </div>
                </div>



                <%-- End Training Needs Survey --%>
            </div>
            <%-- End  --%>
            <div class="form-horizontal" role="form">
                <div class="panel panel-primary">

                    <div class="panel-body">
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <asp:Label ID="Label13" class="col-md-2 control-labelnotop text_right" runat="server" Text="สถานะ : " />
                                    <div class="col-md-3">
                                        <asp:DropDownList ID="ddltraining_req_status" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="1" Text="ใช้งาน" Selected="True" />
                                            <asp:ListItem Value="0" Text="ยกเลิกใช้งาน" />
                                        </asp:DropDownList>
                                    </div>

                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="col-md-2">
                                    </div>
                                    <div class="col-md-7">
                                        <asp:LinkButton ID="btnSaveUpdate" CssClass="btn btn-success" runat="server"
                                            CommandName="btnSaveUpdate" OnCommand="btnCommand"
                                            Text="<i class='fa fa-save fa-lg'></i> บันทึกการเปลี่ยนแปลง"
                                            data-toggle="tooltip" title="บันทึกการเปลี่ยนแปลง"
                                            ValidationGroup="save" />
                                        <asp:LinkButton ID="btnSaveInsert" CssClass="btn btn-success" runat="server"
                                            CommandName="btnSaveInsert" OnCommand="btnCommand"
                                            Text="<i class='fa fa-save fa-lg'></i> บันทึก"
                                            data-toggle="tooltip" title="บันทึก"
                                            ValidationGroup="save" />
                                        <asp:LinkButton CssClass="btn btn-danger"
                                            data-toggle="tooltip" title="ยกเลิก" runat="server"
                                            Text="<i class='fa fa-close fa-lg'></i> ยกเลิก"
                                            CommandName="btnCancel" OnCommand="btnCommand" />
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </asp:Panel>



        <asp:Panel ID="pnlApp" runat="server">
            <%-- Start  --%>
            <div class="form-horizontal" role="form">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; รายละเอียดผู้ขอฝึกอบรม</strong></h3>
                    </div>
                    <div class="panel-body">

                        <asp:FormView ID="FvDetailUserApp" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                            <ItemTemplate>
                                <%-- <div class="form-horizontal" role="form">--%>

                                <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <%--<asp:Label ID="Label7" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="รหัสผู้ทำรายการ : " />
                                    <div class="col-md-3">
                                        <asp:TextBox ID="txtEmpCode" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("EmpCode") %>' />
                                        <asp:Label ID="LbEmpIDX" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("EmpIDX")%>' />

                                    </div>--%>
                                        <asp:Label ID="Label9" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="ชื่อผู้ทำรายการ : " />
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtEmpName" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("FullNameTH") %>' />
                                        </div>

                                        <asp:Label ID="lbdate" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="วันที่สร้างรายการ : " />
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txtu0_training_req_date" runat="server" CssClass="form-control"
                                                Text='<%# Eval("doc_date")%>' Enabled="false" />

                                        </div>
                                    </div>
                                </div>


                                <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <%--<asp:Label ID="Label4" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="บริษัท : " />
                                    <div class="col-md-3">
                                        <asp:TextBox ID="txtOrgNameTH" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("OrgNameTH")%>' />
                                        <asp:Label ID="LbOrgIDX" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("OrgIDX")%>' />
                                    </div>--%>
                                        <asp:Label ID="Labedl1" class="col-md-2 control-labelnotop text_right" runat="server" Text="แผนก : " />
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtSecNameTH" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("SecNameTH") %>' />
                                            <asp:Label ID="LbSecIDX" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("RSecID")%>' />

                                        </div>

                                        <asp:Label ID="Label34" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="ฝ่าย : " />
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txtDeptNameTH" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("RDeptName")%>' />
                                            <asp:Label ID="LbRDeptIDX" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("RDeptID")%>' />
                                        </div>
                                    </div>
                                </div>


                                <%-------------- แผนก,ตำแหน่ง --------------%>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <%--<asp:Label ID="Labedl1" class="col-md-2 control-labelnotop text_right" runat="server" Text="แผนก : " />
                                    <div class="col-md-3">
                                        <asp:TextBox ID="txtSecNameTH" Enabled="false" CssClass="form-control"  runat="server" Text='<%# Eval("SecNameTH") %>' />
                                        <asp:Label ID="LbSecIDX" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("RSecID")%>' />

                                    </div>--%>

                                        <asp:Label ID="Label8" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="ตำแหน่ง : " />
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtPosNameTH" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("PosNameTH")%>' />
                                            <asp:Label ID="LbPosIDX" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("RPosIDX_J")%>' />
                                        </div>

                                        <asp:Label ID="Label6" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="E-mail : " />
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txtEmail" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("Email") %>' />

                                        </div>
                                    </div>
                                </div>

                                <%-------------- เบอร์ติดต่อ,E-mail --------------%>
                                <%-- <div class="form-group">
                                <div class="col-md-12">

                                    <asp:Label ID="Label5" class="col-md-2 control-labelnotop text_right" runat="server" Text="เบอร์ติดต่อ : " />
                                    <div class="col-md-3">
                                        <asp:TextBox ID="txtMobileNo"  Enabled="false" CssClass="form-control"   runat="server" Text='<%# Eval("MobileNo") %>' />
                                    </div>

                                    
                                </div>
                            </div>--%>


                                <%-------------- Cost Center --------------%>
                                <div class="form-group">
                                    <div class="col-md-12">

                                        <asp:Label ID="Label20" class="col-md-2 control-labelnotop text_right" runat="server" Text="Cost Center : " />
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtCostNo" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("CostNo")%>' />
                                            <asp:Label ID="LoCostIDX" runat="server" Visible="false" CssClass="control-label" Text='<%# Eval("CostIDX")%>' />
                                        </div>

                                        <asp:Label ID="Label1" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="เลขที่เอกสาร : " />
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txttraining_req_no" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("doc_no")%>' />
                                            <asp:HiddenField ID="hdf_approve_status" runat="server" Value='<%# Eval("approve_status")%>' />
                                        </div>
                                    </div>
                                </div>

                                <%-- </div>--%>
                            </ItemTemplate>
                        </asp:FormView>


                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; รายการ</strong></h3>
                    </div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <asp:GridView ID="GvTraningListApp"
                                        runat="server"
                                        AutoGenerateColumns="false"
                                        CssClass="table table-striped table-bordered table-responsive col-md-12"
                                        HeaderStyle-CssClass="info"
                                        AllowPaging="false"
                                        OnRowCommand="onRowCommand"
                                        OnRowDataBound="onRowDataBound"
                                        OnPageIndexChanging="gvPageIndexChanging"
                                        ShowFooter="false">
                                        <EmptyDataTemplate>
                                            <div style="text-align: center">ไม่พบข้อมูล</div>
                                        </EmptyDataTemplate>
                                        <Columns>
                                            <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                                                <ItemTemplate>
                                                    <%# (Container.DataItemIndex + 1) %>
                                                    <%-- <%# Eval("training_req_item") %>--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="หลักสูตร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%">
                                                <ItemTemplate>
                                                    <%# getTrainingname((int)Eval("m0_training_idx_ref"),
                                                                        (string)Eval("training_name"),
                                                                        (string)Eval("training_req_other")
                                                     
                                                    ) %>  
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ประเภทการอบรม" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                                                <ItemTemplate>
                                                    <%# getValueType((int)Eval("training_req_type_flag")) %>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ความต้องการ/ความจำเป็นในการฝึกอบรม" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="25%">
                                                <ItemTemplate>
                                                    <%# Eval("training_req_needs") %>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="งบประมาณ/รุ่น" ItemStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-center"
                                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                                                <ItemTemplate>
                                                    <%# Eval("training_req_budget") %>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ตำแหน่ง" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                                                <ItemTemplate>
                                                    <%# Eval("pos_name_th") %>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="จำนวน" ItemStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-center"
                                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                                                <ItemTemplate>
                                                    <%# Eval("training_req_qty") %>
                                                </ItemTemplate>
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText=" เดือนที่ต้องการอบรม" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                                                <ItemTemplate>
                                                    <%# Eval("training_req_month_study") %>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                    </asp:GridView>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <asp:Panel ID="pnlDetailApp" runat="server">

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h2 class="panel-title">รายละเอียดการอนุมัติ
                            </h2>
                        </div>
                        <div class="panel-body">

                            <div class="row">
                                <div class="form-group">
                                    <div class="form-group col-lg-12">
                                        <asp:Label ID="Label29" CssClass="col-sm-3 control-label" runat="server" Text="ผลอนุมัติ" />
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlStatusapprove" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="" Text="ผลการอนุมัติ....." />
                                                <asp:ListItem Value="4" Text="อนุมัติ" />
                                                <asp:ListItem Value="5" Text="กลับไปแก้ไข" />
                                                <asp:ListItem Value="6" Text="ไม่อนุมัติ" />
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredddlStatusapprove"
                                                ValidationGroup="btnupdateApprove" runat="server"
                                                Display="Dynamic"
                                                SetFocusOnError="true"
                                                ControlToValidate="ddlStatusapprove"
                                                Font-Size="1em" ForeColor="Red"
                                                CssClass="pull-left"
                                                ErrorMessage="กรุณากรอกผลอนุมัติ" />
                                        </div>
                                    </div>

                                    <div class="form-group col-lg-12">
                                        <asp:Label ID="Label25" CssClass="col-sm-3 control-label" runat="server" Text="ความคิดเห็นเพิ่มเติม" />
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txtdetailApprove" CssClass="form-control" runat="server"
                                                placeholder="ความคิดเห็นเพิ่มเติม ....." MaxLength="150" TextMode="multiline" Rows="4"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group col-lg-12">
                                        <div class="col-sm-3 col-sm-offset-9">
                                            <asp:LinkButton ID="btnupdateApprove" CssClass="btn btn-success" runat="server"
                                                ValidationGroup="btnupdateApprove" OnCommand="btnCommand" CommandName="btnupdateApprove"
                                                data-toggle="tooltip" title="Save" OnClientClick="return confirm('คุณต้องการให้ผลรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                            <asp:LinkButton ID="btnupdateApproveCon" CssClass="btn btn-success" runat="server"
                                                ValidationGroup="btnupdateApprove" OnCommand="btnCommand" CommandName="btnupdateApproveCon"
                                                data-toggle="tooltip" title="Save" OnClientClick="return confirm('คุณต้องการให้ผลรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>

                                            <asp:LinkButton ID="btncancelApprove" CssClass="btn btn-default" runat="server" Text="Cancel"
                                                OnCommand="btnCommand" CommandName="btncancelApprove" data-toggle="tooltip"
                                                Visible="false"
                                                title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </asp:Panel>

                <asp:Panel ID="pnlSave" runat="server">

                    <div class="row">

                        <div class="form-group">
                            <div class="col-md-12">

                                <asp:LinkButton CssClass="btn btn-danger"
                                    data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                                    Text="<i class='fa fa-angle-left fa-lg'></i> ย้อนกลับ"
                                    ID="btnCancel"
                                    CommandName="btnBack" OnCommand="btnCommand" />

                            </div>
                        </div>
                    </div>

                </asp:Panel>
                <br />

            </div>
        </asp:Panel>

        <asp:Panel ID="pnlApproveList" runat="server">

            <asp:Panel ID="Panel1" runat="server">
                <div class="panel panel-primary">
                    <div class="panel-heading f-bold">ค้นหา</div>
                    <div class="panel-body">
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>เดือน</label>
                                <asp:DropDownList ID="ddlMonthSearch_A" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="0" Text="-" />
                                    <asp:ListItem Value="1" Text="มกราคม" />
                                    <asp:ListItem Value="2" Text="กุมภาพันธ์ " />
                                    <asp:ListItem Value="3" Text="มีนาคม" />
                                    <asp:ListItem Value="4" Text="เมษายน" />
                                    <asp:ListItem Value="5" Text="พฤษภาคม" />
                                    <asp:ListItem Value="6" Text="มิถุนายน" />
                                    <asp:ListItem Value="7" Text="กรกฎาคม" />
                                    <asp:ListItem Value="8" Text="สิงหาคม" />
                                    <asp:ListItem Value="9" Text="กันยายน" />
                                    <asp:ListItem Value="10" Text="ตุลาคม" />
                                    <asp:ListItem Value="11" Text="พฤศจิกายน" />
                                    <asp:ListItem Value="12" Text="ธันวาคม" />
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>ปี</label>
                                <asp:DropDownList ID="ddlYearSearch_A" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>เลขที่เอกสาร</label>
                                <asp:TextBox ID="txtFilterKeyword_A" runat="server"
                                    CssClass="form-control"
                                    placeholder="เลขที่เอกสาร..." />
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <label>
                                &nbsp;
                            </label>
                            <div class="form-group">
                                <asp:LinkButton ID="btnFilter_A" runat="server" CssClass="btn btn-primary" Text="<i class='fa fa-search'></i> ค้นหา"
                                    OnCommand="btnCommand" CommandName="btnFilter_A" />
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>



            <div class="form-horizontal" role="form">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp;
                            <asp:Label ID="lbTitleAppList" runat="server" Text="-"></asp:Label>
                        </strong></h3>
                    </div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <asp:GridView ID="GvApproveList"
                                        runat="server"
                                        AutoGenerateColumns="false"
                                        CssClass="table table-striped table-bordered table-responsive col-md-12"
                                        HeaderStyle-CssClass="info"
                                        AllowPaging="true"
                                        PageSize="10"
                                        OnRowCommand="onRowCommand"
                                        OnRowDataBound="onRowDataBound"
                                        OnPageIndexChanging="gvPageIndexChanging"
                                        ShowFooter="false">
                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />

                                        <EmptyDataTemplate>
                                           <div style="text-align: center">ไม่พบข้อมูล</div>
                                        </EmptyDataTemplate>
                                        <Columns>
                                            <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                                                <ItemTemplate>
                                                    <%# (Container.DataItemIndex + 1) %>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="เลขที่เอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                                                <ItemTemplate>
                                                    <%# Eval("training_req_no") %>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="วันที่สร้างรายการ" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                                                <ItemTemplate>
                                                    <%# Eval("u0_training_req_date") %>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="รหัสพนักงานผู้ขอ" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                                                <ItemTemplate>
                                                    <%# Eval("employee_code") %>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ชื่อพนักงานผู้ขอ" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%">
                                                <ItemTemplate>
                                                    <%# Eval("employee_name") %>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ตำแหน่งผู้ขอ" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                                                <ItemTemplate>
                                                    <%# Eval("pos_name_th") %>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="จำนวนหลักสูตร" ItemStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-center"
                                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                                                <ItemTemplate>
                                                    <%# Eval("training_qty") %>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="จำนวนผู้เข้าอบรม" ItemStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-center"
                                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                                                <ItemTemplate>
                                                    <%# Eval("training_emp_qty") %>
                                                </ItemTemplate>
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="สถานะของเอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                                                <ItemTemplate>

                                                    <span><%# getTextDoc((int)Eval("approve_status") , (string)Eval("decision_name")) %>
                                                    &nbsp;<%# getTextDoc((int)Eval("approve_status") ,(string)Eval("node_name")) %>
                                                    &nbsp;<%# getTextDoc((int)Eval("approve_status") ,"โดย") %>
                                                    &nbsp;<%# getTextDoc((int)Eval("approve_status") ,(string)Eval("actor_name")) %></span>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField ItemStyle-HorizontalAlign="center"
                                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                <ItemTemplate>

                                                    <asp:TextBox ID="txtu0_training_req_idx_GvAppL" runat="server"
                                                        Text='<%# Eval("u0_training_req_idx") %>'
                                                        Visible="false">
                                                    </asp:TextBox>

                                                    <asp:TextBox ID="txtapprove_status_GvAppL" runat="server"
                                                        Text='<%# Eval("approve_status") %>'
                                                        Visible="false">
                                                    </asp:TextBox>

                                                    <asp:LinkButton ID="btnApproveDetail" CssClass="btn btn-info btn-sm" runat="server" data-original-title="รายละเอียด" data-toggle="tooltip" Text="รายละเอียด"
                                                        OnCommand="btnCommand" CommandName="btnApproveDetail" CommandArgument='<%# Eval("u0_training_req_idx") %>'>
                                                        <i class="fa fa-file-alt"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="btnApproveConDetail" CssClass="btn btn-info btn-sm" runat="server" data-original-title="รายละเอียด" data-toggle="tooltip" Text="รายละเอียด"
                                                        OnCommand="btnCommand" CommandName="btnApproveConDetail" CommandArgument='<%# Eval("u0_training_req_idx") %>'>
                                                        <i class="fa fa-file-alt"></i></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                    </asp:GridView>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </asp:Panel>


        <%-- End Select --%>
    </asp:Panel>

    <asp:HiddenField ID="Hddfld_status" runat="server" />
    <asp:HiddenField ID="Hddfld_training_req_no" runat="server" />
    <asp:HiddenField ID="Hddfld_u0_training_req_idx" runat="server" />
    <asp:HiddenField ID="Hddfld_ShowdataAppList" runat="server" />
    <style type="text/css">
        .wrapword_p1 {
            word-wrap: break-word;
        }
    </style>
    <script type="text/javascript">
        function printDiv(divName) {
            var panel = document.getElementById(divName);
            var printWindow = window.open('', '', 'height=800,width=1000');
            printWindow.document.write('<html><head><title></title></head>');
            printWindow.document.write('<body style="font-size:12px;">');
            printWindow.document.write('<style>table{width:100%;color:#000000;font-size:10px;};</style>');
            printWindow.document.write(panel.innerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            setTimeout(function () {
                printWindow.print();
                //printWindow.resizeTo(100,100);
                //printWindow.focus();
                //printwindow.window.close();
            }, 500);
            return false;
        }

        function confirmWithOutValidated(message, validateGroup) {
            if (validateGroup != '') {
                var validated = Page_ClientValidate(validateGroup);
                if (validated) {
                    return confirm(message);
                }
            } else {
                return confirm(message);
            }
        }

        function dtPickerInput(inputClassFrom, inputClassTo) {
            $(inputClassFrom).datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $(inputClassTo).datetimepicker({
                format: 'DD/MM/YYYY'
            });
        }

        function dtPickerBtn(inputClassFrom, inputClassTo, btnClassFrom, btnClassTo) {
            $(btnClassFrom).click(function () {
                $(inputClassFrom).data("DateTimePicker").show();
            });
            $(btnClassTo).click(function () {
                $(inputClassTo).data("DateTimePicker").show();
            });
        }

        $(function () {
            $('.header-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-manual-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-import-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.show-from-onclick').click(function () {
                $('.cutstock-manual-date').data("DateTimePicker").show();
            });

            $('.show-order-sale-log-from-onclick').click(function () {
                $('.filter-order-from').data("DateTimePicker").show();
            });
            $('.show-order-sale-log-to-onclick').click(function () {
                $('.filter-order-to').data("DateTimePicker").show();
            });
            $('.filter-order-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });


            $('.show-order-fix-log-from-onclick').click(function () {
                $('.filter-order-fix-from').data("DateTimePicker").show();
            });
            $('.show-order-fix-log-to-onclick').click(function () {
                $('.filter-order-fix-to').data("DateTimePicker").show();
            });
            $('.filter-order-fix-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-fix-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });


            $('.show-u1-from-onclick').click(function () {
                $('.filter-u1-from').data("DateTimePicker").show();
            });
            $('.show-u1-to-onclick').click(function () {
                $('.filter-u1-to').data("DateTimePicker").show();
            });
            $('.filter-u1-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-u1-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $('.header-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-manual-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-import-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.show-from-onclick').click(function () {
                $('.cutstock-manual-date').data("DateTimePicker").show();
            });

            $('.show-order-sale-log-from-onclick').click(function () {
                $('.filter-order-from').data("DateTimePicker").show();
            });
            $('.show-order-sale-log-to-onclick').click(function () {
                $('.filter-order-to').data("DateTimePicker").show();
            });
            $('.filter-order-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });


            $('.show-order-fix-log-from-onclick').click(function () {
                $('.filter-order-fix-from').data("DateTimePicker").show();
            });
            $('.show-order-fix-log-to-onclick').click(function () {
                $('.filter-order-fix-to').data("DateTimePicker").show();
            });
            $('.filter-order-fix-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-fix-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });


            $('.show-u1-from-onclick').click(function () {
                $('.filter-u1-from').data("DateTimePicker").show();
            });
            $('.show-u1-to-onclick').click(function () {
                $('.filter-u1-to').data("DateTimePicker").show();
            });
            $('.filter-u1-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-u1-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });
    </script>
    <style type="text/css">
        .wrapword_p1 {
            word-wrap: break-word;
        }
    </style>

</asp:Content>

