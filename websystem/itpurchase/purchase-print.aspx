﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="purchase-print.aspx.cs" Inherits="websystem_itpurchase_purchase_print" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">


    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="./../Content/bootstrap.css" runat="server" rel="stylesheet" />
    <link href="./../Content/custom.css" runat="server" rel="stylesheet" />


    <title>PRINT DOCUMENT</title>

    <style type="text/css" media="print,screen">
        @page {
            size: A4 landscape;
            margin: 25px;
        }

        @media all {
            .page-break {
                display: none;
            }
        }

        @media print {
            .page-break {
                display: block;
                page-break-before: always;
            }
        }

        .formPrint {
            margin: 10pt; /*from 5 to 10*/
            padding: 10pt; /*add*/
            width: 842pt;
        }
    </style>


</head>
<body onload="window.print()">
    <%--<body>--%>

    <form id="form1" runat="server" width="100%">
        <script src='<%=ResolveUrl("~/Scripts/jquery-3.1.1.min.js") %>'></script>
        <script src='<%=ResolveUrl("~/Scripts/bootstrap.js") %>'></script>
        <script src='<%=ResolveUrl("~/Scripts/custom.js") %>'></script>


        <div class="formPrint">
            <%--  <% for(int i = 0; i <= 3; i++) %>
          <% { string cnum = i.ToString(); %>--%>
            <asp:Label ID="check_XML" runat="server"></asp:Label>
            <asp:Label ID="Literal1" runat="server"></asp:Label>
            <div style="padding: 15px;">
                <div class="col-sm-12" runat="server" id="CONTENTPRINT">
                    <div class="headOrg" style="text-align: center; padding: 10px;">
                        รายการขอซื้ออุปกรณ์
                    </div>
                    <div class="form-group">

                        <div class="col-sm-12">
                           
                            <div class="headOrg" style="text-align: center; padding: 10px;">
                                <asp:GridView ID="gvPintReport" Visible="true" runat="server" AutoGenerateColumns="false" DataKeyNames="u0_purchase_idx"
                                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12" Width="100%"
                                    HeaderStyle-CssClass="info"
                                    HeaderStyle-Height="10px"
                                    OnRowDataBound="gvRowDataBound">
                                    <Columns>
                                        <asp:TemplateField HeaderText="รหัสเอกสาร" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%"
                                            ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <asp:Label ID="lb_documentcode" runat="server" CssClass="col-sm-12">
                    <small><p> &nbsp;<%# Eval("document_code") %></p></small></asp:Label>
                                                <asp:Label ID="u0_purchase_report" runat="server" Visible="false" Text='<%# Eval("u0_purchase_idx") %>' />
                                                <asp:Label ID="idxreport" Visible="false" runat="server"><%# (Container.DataItemIndex +1) %></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="รายละเอียดผู้ทำรายการขอซื้อ" HeaderStyle-Width="30%" HeaderStyle-CssClass="text-center"
                                            ItemStyle-HorizontalAlign="left" HeaderStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <div style="padding: 15px;">
                                                    <asp:Label ID="lb_empCode_employee" runat="server" CssClass="col-sm-12">
                    <small><p><b>รหัสพนักงาน:</b> &nbsp;<%# Eval("empcode_purchase") %></p></small>
                                                    </asp:Label>
                                                    <asp:Label ID="lb_fullName_employee" runat="server" CssClass="col-sm-12">
                    <small><p><b>ชื่อ - นามสกุล:</b> &nbsp;<%# Eval("fullname_purchase") %></p></small>
                                                    </asp:Label>
                                                    <asp:Label ID="lb_orgName_probation" runat="server" CssClass="col-sm-12">
                    <small><p><b>องค์กร:</b> &nbsp;<%# Eval("org_name_purchase") %></p></small>
                                                    </asp:Label>
                                                    <asp:Label ID="lb_dpart_probation" runat="server" CssClass="col-sm-12">
                    <small><p><b>ฝ่าย:</b> &nbsp;<%# Eval("drept_name_purchase") %></p></small>
                                                    </asp:Label>
                                                    <asp:Label ID="lb_section_probation" runat="server" CssClass="col-sm-12">
                    <small><p><b>แผนก:</b> &nbsp;<%# Eval("sec_name_purchase") %></p></small>
                                                    </asp:Label>
                                                    <asp:Label ID="lb_position_probation" runat="server" CssClass="col-sm-12">
                    <small><p><b>ตำแหน่ง:</b> &nbsp;<%# Eval("pos_name_purchase") %></p></small>
                                                    </asp:Label>
                                                    <asp:Label ID="Label36" runat="server" CssClass="col-sm-12">
                    <small><p><b>cost center:</b> &nbsp;<%# Eval("costcenter_no") %></p></small>
                                                    </asp:Label>
                                                    <asp:Label ID="Label44" runat="server" CssClass="col-sm-12">
                    <small><p><b>เบอร์ติดต่อ:</b> &nbsp;<%# Eval("numberphone") %></p></small>
                                                    </asp:Label>
                                                    <asp:Label ID="Label45" runat="server" CssClass="col-sm-12">
                    <small><p><b>E-mail:</b> &nbsp;<%# Eval("email_purchase") %></p></small>
                                                    </asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="รายละเอียดการขอซื้อ" HeaderStyle-Width="50%"
                                            HeaderStyle-CssClass="text-center" HeaderStyle-Wrap="false" ItemStyle-Wrap="true"
                                            ItemStyle-HorizontalAlign="left" HeaderStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <div style="padding: 15px;">
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <%--  <asp:Label ID="lb_empCode_employedde" runat="server" CssClass="col-sm-12">
                    <small><p><b>รหัสเอกสาร:</b> &nbsp;<%# Eval("empcode_purchase") %></p></small>
                                </asp:Label>--%>
                                                            <asp:Label ID="lb_date_purchase" runat="server" CssClass="col-sm-12">
                    <small><p><b>วันที่ขอซื้อ:</b> &nbsp;<%# Eval("date_purchasequipment") %></p></small>
                                                            </asp:Label>
                                                            <%-- <asp:Label ID="lb_equipmenttype" runat="server" CssClass="col-sm-12">
                    <small><p><b>ประเภทอุปกรณ์ที่ซื้อ:</b> &nbsp;<%# Eval("name_equipment") %></p></small>
                            </asp:Label>--%>
                                                            <asp:Label ID="lb_purchasetype" runat="server" CssClass="col-sm-12">
                    <small><p><b>ประเภทการขอซื้อ:</b> &nbsp;<%# Eval("name_purchase") %></p></small>
                                                            </asp:Label>
                                                            <%--  <asp:Label ID="lb_quantity_equipment" runat="server" CssClass="col-sm-12">
                    <small><p><b>จำนวนเครื่อง:</b> &nbsp;<%# Eval("quantity_equipment") %></p></small>
                            </asp:Label>--%>
                                                            <asp:Label ID="lb_details_equipment" runat="server" CssClass="col-sm-12">
                                <small>
                                    <p><b>รายละเอียดการขอซื้อ:</b> &nbsp;<%# Eval("details_purchase") %></p>
                                </small>
                                                            </asp:Label>
                                                            <div class="col-sm-10">
                                                                <%--  <div style="width: 100px">--%>
                                                                <asp:GridView ID="gvReportitems" Visible="true" runat="server" AutoGenerateColumns="false"
                                                                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12" Width="100%" ItemStyle-Wrap="false" HeaderStyle-Wrap="false"
                                                                    HeaderStyle-CssClass="default" HeaderStyle-Height="10px" AllowPaging="true"
                                                                    OnRowDataBound="gvRowDataBound">


                                                                    <EmptyDataTemplate>
                                                                        <div style="text-align: center">ยังไม่มีรายการขอซื้อ</div>
                                                                    </EmptyDataTemplate>
                                                                    <Columns>

                                                                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                                                                            ItemStyle-HorizontalAlign="center" HeaderStyle-Width="2%" ControlStyle-Font-Size="10" HeaderStyle-Font-Size="8">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="_lbu2idxreport" runat="server" Visible="false" Text='<%# Eval("u2_purchase_idx") %>' />
                                                                                <%# (Container.DataItemIndex +1) %>
                                                                            </ItemTemplate>

                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="อุปกรณ์" HeaderStyle-CssClass="text-center" ItemStyle-Wrap="false" HeaderStyle-Wrap="false"
                                                                            ItemStyle-HorizontalAlign="center" HeaderStyle-Width="8%" ControlStyle-Font-Size="10" HeaderStyle-Font-Size="8">
                                                                            <ItemTemplate>
                                                                                <small>
                                                                                    <asp:Label ID="items_typereport" runat="server" CssClass="col-sm-12"
                                                                                        Text='<%# Eval("type_equipment") %>'></asp:Label>
                                                                                    <asp:Label ID="lbnodeidxItems" runat="server" Visible="false" Text='<%# Eval("node_idx") %>' />
                                                                                    <asp:Label ID="lbactoridxItems" runat="server" Visible="false" Text='<%# Eval("actor_idx") %>' />
                                                                                    <%--  <asp:Label ID="lbstatus_action" runat="server" Visible="false" Text='<%# Eval("status_node") %>' />--%>
                                                                                </small>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="จำนวน" HeaderStyle-Width="3%" HeaderStyle-CssClass="text-center"
                                                                            ItemStyle-HorizontalAlign="center" ControlStyle-Font-Size="10" HeaderStyle-Font-Size="8" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                                                                            <ItemTemplate>
                                                                                <small>
                                                                                    <asp:Label ID="items_quantity_report" runat="server" CssClass="col-sm-12"
                                                                                        Text='<%# Eval("items_quantity") %>'></asp:Label>
                                                                                </small>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="ความคิดเห็นจากไอที" HeaderStyle-Width="12%" HeaderStyle-CssClass="text-center"
                                                                            ItemStyle-HorizontalAlign="center" Visible="true" ControlStyle-Font-Size="10" HeaderStyle-Font-Size="8" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                                                                            <ItemTemplate>
                                                                                <small>

                                                                                    <asp:Panel ID="price_comment_complete_report" runat="server" Visible="true">
                                                                                        <asp:Label ID="commentit_report" runat="server" CssClass="col-sm-12"
                                                                                            Text='<%# Eval("commentfromITSupport") %>'></asp:Label><br />
                                                                                        <label class="bg-success">:: ราคา :: </label>
                                                                                        <br />
                                                                                        <asp:Label ID="lbprice_report" runat="server" CssClass="col-sm-12"
                                                                                            Text='<%# Eval("price_equipment") %>'></asp:Label><br />

                                                                                    </asp:Panel>

                                                                                </small>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="NO.IO" HeaderStyle-Width="15%" HeaderStyle-CssClass="text-center"
                                                                            ItemStyle-HorizontalAlign="center" Visible="true" ControlStyle-Font-Size="10" HeaderStyle-Font-Size="8"
                                                                            ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                                                                            <ItemTemplate>
                                                                                <small>
                                                                                    <div class="col-sm-12">

                                                                                        <asp:GridView ID="gvionumber_report" HeaderStyle-Width="15%" Visible="true" runat="server" AutoGenerateColumns="false"
                                                                                            CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12" ItemStyle-Wrap="false" HeaderStyle-Wrap="false"
                                                                                            HeaderStyle-CssClass="default" HeaderStyle-Height="10px" AllowPaging="true"
                                                                                            OnRowDataBound="gvRowDataBound">
                                                                                            <Columns>

                                                                                                <asp:TemplateField HeaderText="NO." HeaderStyle-CssClass="text-center" ItemStyle-Wrap="false" HeaderStyle-Wrap="false"
                                                                                                    ItemStyle-HorizontalAlign="center" HeaderStyle-Width="1%" ControlStyle-Font-Size="10" HeaderStyle-Font-Size="8">
                                                                                                    <ItemTemplate>
                                                                                                        <small>

                                                                                                            <asp:Label ID="_lbidxnumerid_report" runat="server" Visible="false" CssClass="col-sm-12"
                                                                                                                Text='<%# Eval("idx") %>'></asp:Label>
                                                                                                            <asp:Label ID="Labelz1" runat="server" CssClass="col-sm-12"></asp:Label>
                                                                                                            <%# (Container.DataItemIndex +1) %>
                                                                                                        </small>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="NO.IO" HeaderStyle-Width="9%" HeaderStyle-CssClass="text-center"
                                                                                                    ItemStyle-HorizontalAlign="center" ControlStyle-Font-Size="10" HeaderStyle-Font-Size="8" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                                                                                                    <ItemTemplate>
                                                                                                        <small>
                                                                                                            <asp:Label ID="_lbnumberio_report" runat="server" CssClass="col-sm-12"
                                                                                                                Text='<%# Eval("io") %>'></asp:Label>

                                                                                                        </small>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                            </Columns>
                                                                                        </asp:GridView>
                                                                                    </div>
                                                                                </small>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="NO.Asset" Visible="true" HeaderStyle-Width="10%" ItemStyle-Wrap="false" HeaderStyle-Wrap="false" HeaderStyle-CssClass="text-center"
                                                                            ItemStyle-HorizontalAlign="center" ControlStyle-Font-Size="10" HeaderStyle-Font-Size="8">
                                                                            <ItemTemplate>
                                                                                <small>
                                                                                    <div class="col-sm-12">
                                                                                        <asp:GridView ID="gvassetnumber_report" Visible="true" runat="server" AutoGenerateColumns="false"
                                                                                            CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12" Width="10%" ItemStyle-Wrap="false" HeaderStyle-Wrap="false"
                                                                                            HeaderStyle-CssClass="default" HeaderStyle-Height="10px" AllowPaging="true"
                                                                                            OnRowDataBound="gvRowDataBound">
                                                                                            <Columns>

                                                                                                <asp:TemplateField HeaderText="NO." HeaderStyle-Width="1%" HeaderStyle-CssClass="text-center"
                                                                                                    ItemStyle-HorizontalAlign="center" ControlStyle-Font-Size="10" HeaderStyle-Font-Size="8" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                                                                                                    <ItemTemplate>
                                                                                                        <small>

                                                                                                            <asp:Label ID="_lbidx_asset_report" runat="server" Visible="false" CssClass="col-sm-12"
                                                                                                                Text='<%# Eval("idx_asset") %>'></asp:Label>
                                                                                                            <asp:Label ID="Labelz" runat="server" CssClass="col-sm-12"></asp:Label>
                                                                                                            <%# (Container.DataItemIndex +1) %>
                                                                                                        </small>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="No.Asset" HeaderStyle-Width="9%" ItemStyle-Wrap="false" HeaderStyle-Wrap="false" HeaderStyle-CssClass="text-center"
                                                                                                    ItemStyle-HorizontalAlign="center" ControlStyle-Font-Size="10" HeaderStyle-Font-Size="8">
                                                                                                    <ItemTemplate>
                                                                                                        <small>
                                                                                                            <asp:Label ID="_lbasset_number_report" runat="server" CssClass="col-sm-12"
                                                                                                                Text='<%# Eval("asset_number") %>'></asp:Label>

                                                                                                        </small>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>

                                                                                            </Columns>

                                                                                        </asp:GridView>
                                                                                    </div>
                                                                                </small>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                                <%--</div>--%>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="สถานะ" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%"
                                            ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <small><b>
                                                    <asp:Label ID="idx_status" Visible="false" runat="server" CssClass="col-sm-12"
                                                        Text='<%# Eval("status_purchase") %>'></asp:Label>
                                                    <asp:Label ID="status_purchasereport" runat="server" CssClass="col-sm-12"
                                                        Text='<%# Eval("node_status_purchase") %>'></asp:Label></b></small>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <%--  <asp:TemplateField HeaderText="No. ASSET" HeaderStyle-Width="10%" HeaderStyle-CssClass="text-center"
                aderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="number_asssset" runat="server" CssClass="col-sm-12"
                                        Text='<%# Eval("number_asset") %>'></asp:Label></b></small>

                            </ItemTemplate>
                        </asp:TemplateField>--%>

                                        <%--  <asp:TemplateField HeaderText="IO NO." HeaderStyle-Width="20%" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="left" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="number_assssxxet" runat="server" CssClass="col-sm-12"
                                        Text='<%# Eval("number_io") %>'></asp:Label></b></small>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                                    </Columns>
                                </asp:GridView>
                             
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <br />
                <div class="headPrint">
                    PRINT DATE :
                 <asp:Label ID="lblPrintDate" runat="server" CssClass="headPrint" />
                </div>
            </div>
            <div class="page-break" id="pb2" runat="server" visible="false"></div>

        </div>

    </form>
</body>
</html>
