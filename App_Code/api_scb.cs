﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

/// <summary>
/// Summary description for api_scb
/// </summary>
public class api_scb : System.Web.Services.WebService
{

    string _xml_in = "";
    string _ret_val = "";
    string _local_xml = String.Empty;
    string webmaseter = "webmaster@taokaenoi.co.th";

    public api_scb()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    //***SCB***//
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SCBSandboxCallback(string jsonIn)
    {
        //if (jsonIn != null)
        //{
        //    // convert to xml
        //    _xml_in = _funcTool.convertJsonToXml(jsonIn);
        //    // execute and return
        //    _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 201); // return w/ json
        //}
        _ret_val = jsonIn;
        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

}