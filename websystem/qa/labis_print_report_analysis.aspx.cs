﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.IO;

public partial class websystem_qa_labis_print_report_analysis : System.Web.UI.Page
{

    function_tool _funcTool = new function_tool();
    data_qa _data_qa = new data_qa();

    string _localJson = "";
    int _tempInt = 0;
    int _tempcounter;

    int TYPE_LABRJN = 4;
    int TYPE_LABNPW = 3;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlQaGetReportAnalysis = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetReportAnalysis"];
    static string _urlQaGetTestReportAnalysis = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetTestReportAnalysis"];
    

    protected void Page_Load(object sender, EventArgs e)
    {
        string url = Request.QueryString["url"];
        if (Session["_SESSION_REPORT_ANALYSIS"].ToString() != null)
        {
            ViewState["_PRINT_REPORT_ANALYSIS"] = Session["_SESSION_REPORT_ANALYSIS"].ToString();
           
        }
        //else if (Session["_SESSION_REPORT_ANALYSIS"].ToString() == null)
        //{
        //    Response.AddHeader("REFRESH", "3;URL=" + ResolveUrl("~/login?url=" + url));
        //}
       // Literal1.Text = ViewState["_PRINT_REPORT_ANALYSIS"].ToString();
        PRINT_SAMPLECODE_Report();
        _GETPRINTDATE();
    }


    private void PRINT_SAMPLECODE_Report()
    {
        data_qa _data_sample_print = new data_qa();
        _data_sample_print.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
        qa_lab_u1_qalab _u1_doc_sampleeail = new qa_lab_u1_qalab();

        _u1_doc_sampleeail.u1_qalab_idx = int.Parse(ViewState["_PRINT_REPORT_ANALYSIS"].ToString());
        _data_sample_print.qa_lab_u1_qalab_list[0] = _u1_doc_sampleeail;

        _data_sample_print = callServicePostQA(_urlQaGetReportAnalysis, _data_sample_print);

        if (_data_sample_print.qa_lab_u1_qalab_list[0].m0_lab_idx.ToString() != null)
        {
            ViewState["m0Labidx"] = _data_sample_print.qa_lab_u1_qalab_list[0].m0_lab_idx.ToString();
        }
        if (int.Parse(ViewState["m0Labidx"].ToString()) == TYPE_LABRJN)
        {
            lbFMQALB_LabRJN.Visible = true;
            lbFMQALB_LabNPW.Visible = false;

        }
         if (int.Parse(ViewState["m0Labidx"].ToString()) == TYPE_LABNPW)
        {
            lbFMQALB_LabNPW.Visible = true;
            lbFMQALB_LabRJN.Visible = false;
        }

        if (int.Parse(ViewState["m0Labidx"].ToString()) != TYPE_LABNPW && int.Parse(ViewState["m0Labidx"].ToString()) != TYPE_LABRJN)
        {
            lbFMQALB_LabNPW.Visible = false;
            lbFMQALB_LabRJN.Visible = false;
        }

        setGridData(gvSampleReportAnalysis, _data_sample_print.qa_lab_u1_qalab_list);
        setRepeaterData(rp_test_sample_detail, _data_sample_print.qa_lab_u1_qalab_list);
        
    }

    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "gvSampleReportAnalysis":
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    Label _U1DocIDX_SampleReport = (Label)e.Row.Cells[0].FindControl("lblU1DocIDX_Sample");
                    Repeater rp_test_sample = (Repeater)e.Row.Cells[1].FindControl("rp_test_sample");
                    Repeater rp_test_sample_result = (Repeater)e.Row.Cells[2].FindControl("rp_test_sample_result");
                    Repeater rp_test_sample_method = (Repeater)e.Row.Cells[3].FindControl("rp_test_sample_method");

                    data_qa _data_test_sample_report = new data_qa();
                    _data_test_sample_report.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
                    qa_lab_u1_qalab _doc_sampledeail = new qa_lab_u1_qalab();

                    _doc_sampledeail.u1_qalab_idx = int.Parse(_U1DocIDX_SampleReport.Text);

                    _data_test_sample_report.qa_lab_u1_qalab_list[0] = _doc_sampledeail;

                    _data_test_sample_report = callServicePostQA(_urlQaGetTestReportAnalysis, _data_test_sample_report);

                    setRepeaterData(rp_test_sample, _data_test_sample_report.qa_lab_u1_qalab_list);
                    setRepeaterData(rp_test_sample_result, _data_test_sample_report.qa_lab_u1_qalab_list);
                    setRepeaterData(rp_test_sample_method, _data_test_sample_report.qa_lab_u1_qalab_list);
                    

                }
              break;
        }
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }


    protected void setRepeaterData(Repeater rptName, Object obj)
    {
        rptName.DataSource = obj;
        rptName.DataBind();
    }

    protected void _GETPRINTDATE()
    {
        ladatetimereport.Text = "Date :" + DateTime.Now.ToString();
    }

    protected data_qa callServicePostQA(string _cmdUrl, data_qa _data_qa)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_qa);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_qa = (data_qa)_funcTool.convertJsonToObject(typeof(data_qa), _localJson);

        return _data_qa;
    }


}