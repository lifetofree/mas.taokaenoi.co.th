﻿using System;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_fingermanagement")]
public class data_fingermanagement
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }
    [XmlElement("u0_finger_detail_list")]
    public u0_finger_detail[] u0_finger_detail_list { get; set; }

}


[Serializable]
public class u0_finger_detail
{
    [XmlElement("EmpCode")]
    public string EmpCode { get; set; }

    [XmlElement("fidx")]
    public int fidx { get; set; }

    [XmlElement("fm_ip")]
    public string fm_ip { get; set; }

    [XmlElement("fm_name")]
    public string fm_name { get; set; }

    [XmlElement("LocIDX")]
    public int LocIDX { get; set; }
    [XmlElement("LocName")]
    public string LocName { get; set; }

    [XmlElement("fmtidx")]
    public int fmtidx { get; set; }
    [XmlElement("fmt_name")]
    public string fmt_name { get; set; }

    [XmlElement("fmsidx")]
    public int fmsidx { get; set; }
    [XmlElement("fms_name")]
    public string fms_name { get; set; }

    [XmlElement("checkbox_row")]
    public bool checkbox_row { get; set; }

    [XmlElement("FullNameTH")]
    public string FullNameTH { get; set; }

    [XmlElement("EmpIDX")]
    public int EmpIDX { get; set; }

    [XmlElement("EmpStatus")]
    public int EmpStatus { get; set; }

    [XmlElement("OrgIDX")]
    public int OrgIDX { get; set; }

    [XmlElement("RDepIDX")]
    public int RDepIDX { get; set; }

    [XmlElement("RSecIDX")]
    public int RSecIDX { get; set; }

    [XmlElement("RPosIDX")]
    public int RPosIDX { get; set; }

    [XmlElement("EmpStatus_Succes")]
    public int EmpStatus_Sucees { get; set; }

}