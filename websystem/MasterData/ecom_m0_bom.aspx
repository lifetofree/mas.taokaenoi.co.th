﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="ecom_m0_bom.aspx.cs" Inherits="websystem_MasterData_ecom_m0_bom" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:Literal ID="litdebug" runat="server"></asp:Literal>
    <style>
        textarea {
            resize: none;
        }

        .p-controll {
            padding-right: 7px;
        }

        .first-margin {
            margin-left: calc(33.33333333% + 12px);
            display: inline-block;
        }

        .first-margin-btn {
            margin-left: calc(33.33333333% + 6px);
            display: inline-block;
        }

        .pt-7 {
            padding-top: 7px;
        }

        .overflow-text {
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }

        @media only screen and (max-width: 766px) {
            .first-margin {
                margin-left: 12px !important;
            }

            .first-margin-btn {
                margin-left: 0px !important;
            }
        }
    </style>
    <asp:MultiView ID="MvSystem" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewIndex" runat="server">
            <div class="form-group">
                <asp:LinkButton ID="btn_addplace" CssClass="btn btn-primary" Visible="true" data-original-title="เพิ่ม BOM" data-toggle="tooltip" title="เพิ่ม BOM" runat="server" CommandName="cmdAddUnit" OnCommand="btnCommand"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;เพิ่ม BOM</asp:LinkButton>

                <div class="clearfix"></div>
            </div>
            <div runat="server" id="div_insert" visible="false">
                <div class=" panel panel-info">
                    <div class=" panel-heading">
                        <h3 class="panel-title">เพิ่ม BOM
                        </h3>
                    </div>
                    <div class=" panel-body">
                        <asp:FormView ID="FvInsert" runat="server" DefaultMode="ReadOnly" Width="100%" OnDataBound="Fv_DataBound">
                            <InsertItemTemplate>


                                <div class=" form-horizontal" style="/*padding: 20px 30px; */">
                                    <div class="col-xs-12">
                                        <h5 class=""><b>เพิ่ม BOM</b></h5>

                                    </div>
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="col-xs-12 col-md-6">
                                            <div class=" form-group">
                                                <div class="col-xs-12 col-sm-4  control-label">
                                                    <asp:Label runat="server" Text="">BOM ชื่อสินค้า <span style="color:red;">*</span></asp:Label>
                                                </div>
                                                <div class="col-xs-12 col-sm-8">
                                                    <%--<asp:TextBox ID="txtmaterial" runat="server" CssClass="form-control" placeholder="กรอกรหัส Material ..." Enabled="true" />--%>
                                                    <asp:TextBox ID="txtBOM" TextMode="multiline" CssClass="form-control" Columns="50" Rows="6" runat="server" placeholder="กรอกรหัส BOM ชื่อสินค้า เช่น เถ้าแก่น้อย -   สาหร่ายเทมปุระ รสราเมน 18 กรัม (6ซอง/แพ็ค) (1302474) เป็นต้น" />
                                                    <asp:RequiredFieldValidator ID="refv1" runat="server"
                                                        ControlToValidate="txtBOM" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณากรอกรหัส BOM" ValidationGroup="Save" />
                                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="refv1" Width="220" />


                                                      <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server"
                                                        ControlToValidate="txtBOM" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณากรอกรหัส BOM" ValidationGroup="editsave" />
                                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator15" Width="220"  />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-6">

                                            <div class="form-group">
                                                <div class="col-xs-12 col-sm-4  control-label">
                                                    <asp:Label runat="server" Text="วันที่เริ่มต้น">วันที่เริ่มต้น<span style="color:red;">*</span></asp:Label>
                                                </div>
                                                <div class="col-xs-12 col-sm-8 ">
                                                    <asp:TextBox ID="tbDateStart" runat="server" CssClass="form-control date-datepicker-date start-date" MaxLength="20" autocomplete="off">
                                                    </asp:TextBox>

                                                   <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server"
                                                        ControlToValidate="tbDateStart" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณากรอก วันที่เริ่มต้น" ValidationGroup="Save" />
                                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender22"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator17" Width="220" PopupPosition="BottomRight"/>

                                                      <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server"
                                                        ControlToValidate="tbDateStart" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณากรอก วันที่เริ่มต้น" ValidationGroup="editsave" />
                                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender21"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator16" Width="220" PopupPosition="BottomRight"/>
                                                </div>
                                            </div>
                                            <div class="form-group">

                                                <div class="col-xs-12 col-sm-4 control-label">
                                                    <asp:Label runat="server" Text="วันที่สิ้นสุด">วันที่สิ้นสุด<span style="color:red;">*</span></asp:Label>
                                                </div>
                                                <div class="col-xs-12 col-sm-8 ">
                                                    <asp:TextBox ID="tbDateEnd" runat="server" CssClass="form-control date-datepicker-date end-date" MaxLength="20" autocomplete="off">
                                                    </asp:TextBox>

                                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server"
                                                        ControlToValidate="tbDateEnd" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณากรอก วันที่สิ้นสุด" ValidationGroup="Save" />

                                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender23"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator18" Width="220" PopupPosition="BottomRight"/>

                                                      <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server"
                                                        ControlToValidate="tbDateEnd" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณากรอก วันที่สิ้นสุด" ValidationGroup="editsave" />

                                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender24"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator19" Width="220" PopupPosition="BottomRight"/>
                                                </div>

                                            </div>
                                                <div class="form-group">

                                                <div class="col-xs-12 col-sm-8 col-sm-offset-4">
                                                  <asp:LinkButton runat="server" ID="Cmd_edit_bom_name" OnCommand="btnCommand" CommandName="Cmd_edit_bom_name" CssClass="btn btn-default " data-toggle="tooltip" title="แก้ไข BOM" Text="แก้ไข BOM" Visible="false" ><i class="fa fa-edit" aria-hidden="true"  style="margin-right:7px;"></i>แก้ไข BOM</asp:LinkButton>
                                                    <asp:LinkButton runat="server" ID="Cmd_save_bom_name" OnCommand="btnCommand" CommandName="Cmd_save_bom_name" CssClass="btn btn-success " data-toggle="tooltip" title="บันทึก BOM" Text="บันทึก BOM" Visible="false" ValidationGroup="editsave"><i class="fa fa-save" aria-hidden="true" style="margin-right:7px;"></i>บันทึก BOM</asp:LinkButton>
                                                     <asp:LinkButton runat="server" ID="Cmd_cc_bom_name" OnCommand="btnCommand" CommandName="Cmd_cc_bom_name" CssClass="btn btn-danger " data-toggle="tooltip" title="ยกเลิกการแก้ไข BOM" Text="ยกเลิกการแก้ไข BOM" Visible="false" >ยกเลิกการแก้ไข BOM</asp:LinkButton>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <%-- <div class=" form-group">
                                    <div class="col-xs-12 col-sm-3  control-label">
                                        <asp:Label runat="server" Text="สถานะ"></asp:Label>
                                    </div>
                                    <div class="col-xs-12 col-sm-6  col-md-6">
                                        <asp:DropDownList ID="ddstatusmain" CssClass="form-control" runat="server">
                                            <asp:ListItem Value="1" Text="Online" />
                                            <asp:ListItem Value="0" Text="Offline" />
                                        </asp:DropDownList>
                                    </div>
                                </div>--%>

                                    <%--<hr />--%>
                                    <div class="col-xs-12 col-md-12">
                                        <div class=" " style="display: grid;">
                                            <div class=" ">
                                                <h5 class=""><b>เพิ่ม รายละเอียด</b></h5>

                                            </div>
                                            <div class=" ">

                                                <div class=" form-horizontal " style="/*padding: 20px 30px; */">
                                                    <div class="col-xs-12 col-md-6">
                                                        <div class=" form-group">
                                                            <div class="col-xs-12 col-sm-4  control-label">
                                                                <asp:Label runat="server" Text="ชื่อ สินค้า">เลือกรหัส Material<span style="color:red;">*</span></asp:Label>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8 ">
                                                                <asp:DropDownList ID="ddl_material" CssClass="form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="OnSelectedIndexChanged">
                                                                </asp:DropDownList>

                                                            </div>

                                                        </div>
                                                        <div class=" form-group">
                                                            <div class="col-xs-12 col-sm-4  control-label">
                                                                <asp:Label runat="server" Text="ชื่อ สินค้า">ชื่อ สินค้า<span style="color:red;">*</span></asp:Label>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8 ">

                                                                <asp:TextBox ID="txtProductname" CssClass="form-control" runat="server" placeholder="กรอกรหัส ชื่อสินค้า ..." ReadOnly="true" />
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtProductname" Display="None" SetFocusOnError="true" ErrorMessage="*กรุณากรอก ชื่อสินค้า" ValidationGroup="Save" />
                                                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ajax01" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator5" Width="220" />
                                                            </div>
                                                        </div>

                                                        <div class=" form-group">
                                                            <div class="col-xs-12 col-sm-4  control-label">
                                                                <asp:Label runat="server" Text="รหัส material">รหัส material<span style="color:red;">*</span></asp:Label>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8 ">

                                                                <asp:TextBox ID="txtmaterial" CssClass="form-control" runat="server" placeholder="กรอกรหัส Material ..." ReadOnly="true" />

                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtmaterial" Display="None" SetFocusOnError="true" ErrorMessage="*กรุณากรอกรหัส Material" ValidationGroup="Save" />
                                                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender12" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator9" Width="220" />
                                                            </div>
                                                        </div>
                                                        <%--<hr>--%>
                                                        <div class=" form-group">
                                                            <div class="col-xs-12 col-sm-4  control-label">
                                                                <asp:Label runat="server" Text="หน่วยสินค้า">หน่วยสินค้า<span style="color:red;">*</span></asp:Label>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8 ">

                                                                <asp:TextBox ID="txtunit" CssClass="form-control" runat="server" placeholder="กรอก หน่วยของสินค้า ..." />

                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtunit" Display="None" SetFocusOnError="true" ErrorMessage="*กรุณากรอก หน่วยของสินค้า" ValidationGroup="Save" />
                                                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender13" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator10" Width="220" />
                                                            </div>
                                                        </div>
                                                        <div class=" form-group">
                                                            <div class="col-xs-12 col-sm-4  control-label">
                                                                <asp:Label runat="server" Text="จำวนวน">จำนวน<span style="color:red;">*</span></asp:Label>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8 ">

                                                                <asp:TextBox ID="txtamount" CssClass="form-control" runat="server" placeholder="กรอก จำนวนสินค้า ..." />

                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtamount" Display="None" SetFocusOnError="true" ErrorMessage="*กรุณากรอก จำนวนสินค้า" ValidationGroup="Save" />
                                                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender14" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator11" Width="220" />

                                                                <asp:CompareValidator runat="server" ID="CompareValidator1" Operator="DataTypeCheck" Type="Integer"
                                                                    ControlToValidate="txtamount" Display="None" Font-Size="10pt" ForeColor="Red" ErrorMessage="กรุณากรอก เป็นตัวเลข" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server"
                                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="CompareValidator1" Width="220" />

                                                            </div>
                                                        </div>
                                                        <div class=" form-group">
                                                            <div class="col-xs-12 col-sm-4  control-label">
                                                                <asp:Label runat="server" Text="ราคาสินค้า">ราคาสินค้า<span style="color:red;">*</span></asp:Label>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8 ">

                                                                <asp:TextBox ID="txtprice" CssClass="form-control" runat="server" placeholder="กรอก ราคาต่อหน่วยของสินค้า ..." />

                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="txtprice" Display="None" SetFocusOnError="true" ErrorMessage="*กรุณากรอก ราคาต่อหน่วยของสินค้า " ValidationGroup="Save" />
                                                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender15" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator12" Width="220" />

                                                                <asp:CompareValidator runat="server" ID="RegularExpressionValidator2" Operator="DataTypeCheck" Type="Double"
                                                                    ControlToValidate="txtprice" Display="None" Font-Size="10pt" ForeColor="Red" ErrorMessage="กรุณากรอก เป็นตัวเลข" />

                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server"
                                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator2" Width="220" />
                                                            </div>
                                                        </div>
                                                        <div class=" form-group">
                                                            <div class="col-xs-12 col-sm-4  control-label">
                                                                <asp:Label runat="server" Text="">ภาษี<span style="color:red;">*</span></asp:Label>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8 ">

                                                                <asp:TextBox ID="txttex" CssClass="form-control" runat="server" placeholder="กรอก ภาษีต่อหน่วยของสินค้า ..." />

                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="txttex" Display="None" SetFocusOnError="true" ErrorMessage="*กรุณากรอก ภาษีต่อหน่วยของสินค้า " ValidationGroup="Save" />
                                                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender17" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator13" Width="220" />

                                                                <asp:CompareValidator runat="server" ID="CompareValidator3" Operator="DataTypeCheck" Type="Double"
                                                                    ControlToValidate="txttex" Display="None" Font-Size="10pt" ForeColor="Red" ErrorMessage="กรุณากรอก เป็นตัวเลข" />

                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender18" runat="Server"
                                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="CompareValidator3" Width="220" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-6">
                                                        <div class=" form-group">
                                                            <div class="col-xs-12 col-sm-4  control-label">
                                                                <asp:Label runat="server" Text="">เลือก Promotion</asp:Label>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8 ">
                                                                <asp:DropDownList ID="ddl_promotion" CssClass="form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="OnSelectedIndexChanged">
                                                                </asp:DropDownList>

                                                            </div>

                                                        </div>
                                                        <div class=" form-group">
                                                            <div class="col-xs-12 col-sm-4  control-label">
                                                                <asp:Label runat="server" Text="ส่วนลด"></asp:Label>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8 ">

                                                                <asp:TextBox ID="txtdiscount_insert" CssClass="form-control" runat="server" placeholder="กรอก ส่วนลด ..." ReadOnly="true" />

                                                                <asp:CompareValidator runat="server" ID="CompareValidator2" Operator="DataTypeCheck" Type="Double"
                                                                    ControlToValidate="txtdiscount_insert" Display="None" Font-Size="10pt" ForeColor="Red" ErrorMessage="กรุณากรอก เป็นตัวเลข" />

                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender16" runat="Server"
                                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="CompareValidator2" Width="220" />
                                                            </div>
                                                        </div>
                                                        <div class=" form-group">
                                                            <div class="col-xs-12 col-sm-4  control-label">
                                                                <asp:Label runat="server" Text="PromotionID"></asp:Label>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8 ">
                                                                <asp:TextBox ID="txtPromotionID" CssClass="form-control" runat="server" placeholder="กรอก รหัส Promotion ..." ReadOnly="true" />
                                                                <asp:TextBox ID="txtTypepromotion" runat="server" Display="None" Visible="false" />
                                                            </div>
                                                        </div>
                                                        <div class=" form-group">
                                                            <div class="col-xs-12 col-sm-4  control-label">
                                                                <asp:Label runat="server" Text="รายละเอียด Promotion"></asp:Label>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8 ">
                                                                <asp:TextBox ID="txtPromotionDesc" CssClass="form-control" runat="server" placeholder="กรอก รายละเอียด Promotion ..." ReadOnly="true" />
                                                            </div>
                                                        </div>
                                                        <div class=" form-group">
                                                            <div class="col-xs-12 col-sm-4  control-label">
                                                                <asp:Label runat="server" Text="SAP Code"></asp:Label>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8 ">
                                                                <asp:TextBox ID="txtSAPcode" CssClass="form-control" runat="server" placeholder="กรอก รหัส SAP Code ..." ReadOnly="true" />
                                                            </div>
                                                        </div>
                                                        <%--<hr>--%>
                                                        <div class=" form-group">
                                                            <div class="col-xs-12 col-sm-4  control-label">
                                                                <asp:Label runat="server" Text="สถานะ"></asp:Label>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8">
                                                                <asp:DropDownList ID="ddSubstatus" CssClass="form-control" runat="server">
                                                                    <asp:ListItem Value="1" Text="Online" />
                                                                    <asp:ListItem Value="0" Text="Offline" />
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>

                                                        <div class=" form-group">
                                                            <div class="col-xs-12 col-sm-4  control-label">
                                                                <asp:Label runat="server" Text="ภาษีรวม :"></asp:Label>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-3  pt-7 overflow-text">
                                                                <asp:Label ID="txtVATAmount" runat="server" Text="0"></asp:Label>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-2  control-label">
                                                                <asp:Label runat="server" data-toggle="tooltip" title="ราคารวม" Text="ราคารวม :"></asp:Label>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-3  pt-7 overflow-text">
                                                                <asp:Label ID="txtNetPriceAmount" runat="server" Text="0"></asp:Label>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="col-xs-12 col-md-6">
                                            <div class="form-group">
                                                <div class="first-margin "></div>

                                                <asp:LinkButton runat="server" OnCommand="btnCommand" CommandName="Cmd_plus" CssClass="btn btn-primary " data-toggle="tooltip" title="เพิ่มข้อมูล" Text="Save" ValidationGroup="Save"><i class="fa fa-plus-square" aria-hidden="true" style="margin-right:7px;"></i>เพิ่มข้อมูล</asp:LinkButton>

                                                <asp:LinkButton runat="server" OnCommand="btnCommand" CommandName="cal_price_and_vat" CssClass="btn btn-default " data-toggle="tooltip" title="คลิกเพื่อ คำนวณราคาและภาษี รวม"><i class="fa fa-calculator" aria-hidden="true" style="margin-right:7px;"></i>คำนวณราคาและภาษี รวม</asp:LinkButton>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </InsertItemTemplate>
                        </asp:FormView>
                        <asp:GridView ID="GvSample" runat="server" RowStyle-Font-Size="Small"
                            HeaderStyle-Font-Size="Small" AutoGenerateColumns="false"
                            RowStyle-VerticalAlign="NotSet"
                            OnRowDataBound="Master_RowDataBound"
                            OnRowEditing="Master_RowEditing"
                            OnRowDeleting="gvRowDeleted"
                            OnRowUpdating="Master_RowUpdating"
                            OnRowCancelingEdit="Master_RowCancelingEdit"
                            OnPageIndexChanging="Master_PageIndexChanging"
                            RowStyle-Wrap="true" CssClass="table table-striped table-bordered table-responsive"
                            HeaderStyle-Height="25px" ShowHeaderWhenEmpty="True"
                            ShowFooter="False" BorderStyle="None" CellSpacing="2">
                            <EmptyDataTemplate>
                                <div style="text-align: center">Data Cannot Be Found</div>
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <div style="text-align: center; padding-top: 5px;">
                                                <asp:Label ID="bom_m1_idx" runat="server" Visible="false" Text='' />
                                                <%# (Container.DataItemIndex +1) %>
                                            </div>
                                        </small>

                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <div class="panel-body">
                                            <div class="form-horizontal" role="form">
                                                <%--    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txt_bom_m0_idx_edit" Visible="false" runat="server" CssClass="form-control " Text='<%# Eval("bom_m1_idx") %>'></asp:TextBox>
                                        </div>
                                    </div>--%>
                                                <div class="col-xs-12 col-md-6">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <asp:Label ID="Label1" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อ สินค้า">ชื่อ สินค้า<span style="color:red;">*</span></asp:Label>
                                                            <div class="col-sm-6">
                                                                <asp:TextBox ID="txtproduct_name" CssClass="form-control" runat="server" Text='<%# Eval("product_name") %>' placeholder="กรอกรหัส ชื่อสินค้า ..." />
                                                                <%--<asp:TextBox ID="Name_material" runat="server" CssClass="form-control " Text='<%# Eval("material") %>'></asp:TextBox>--%>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                                                                    ControlToValidate="txtproduct_name" Display="None" SetFocusOnError="true"
                                                                    ErrorMessage="*กรุณากรอก ชื่อสินค้า " ValidationGroup="Editpname" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server"
                                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="220" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <asp:Label ID="Label4" CssClass="col-sm-3 control-label" runat="server" Text="รหัส material">รหัส material<span style="color:red;">*</span></asp:Label>
                                                            <div class="col-sm-6">
                                                                <asp:TextBox ID="txtmaterial" CssClass="form-control" runat="server" Text='<%# Eval("material") %>' placeholder="กรอกรหัส Material ..." />

                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server"
                                                                    ControlToValidate="txtmaterial" Display="None" SetFocusOnError="true"
                                                                    ErrorMessage="*กรุณากรอก material " ValidationGroup="Editpname" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server"
                                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="220" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <asp:Label ID="Label5" CssClass="col-sm-3 control-label" runat="server" Text="หน่วยสินค้า">หน่วยสินค้า<span style="color:red;">*</span></asp:Label>
                                                            <div class="col-sm-6">
                                                                <asp:TextBox ID="txtunit_name" CssClass="form-control" runat="server" Text='<%# Eval("unit_name") %>' placeholder="กรอก หน่วยของสินค้า ..." />

                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server"
                                                                    ControlToValidate="txtunit_name" Display="None" SetFocusOnError="true"
                                                                    ErrorMessage="*กรุณากรอก หน่วยของสินค้า " ValidationGroup="Editpname" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server"
                                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="220" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class=" form-group">
                                                        <div class="col-xs-12 col-sm-3  control-label">
                                                            <asp:Label runat="server" Text="จำวนวน">จำวนวน<span style="color:red;">*</span></asp:Label>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-6 col-md-6 ">

                                                            <asp:TextBox ID="txtamount" CssClass="form-control" runat="server" Text='<%# Eval("amount") %>' placeholder="กรอก จำนวนสินค้า ..." />

                                                            <asp:CompareValidator runat="server" ID="CompareValidator1" Operator="DataTypeCheck" Type="Integer"
                                                                ControlToValidate="txtamount" Display="None" Font-Size="10pt" ForeColor="Red" ErrorMessage="กรุณากรอก เป็นตัวเลข" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="CompareValidator1" Width="220" />

                                                            <asp:RequiredFieldValidator ID="validate5" runat="server"
                                                                ControlToValidate="txtamount" Display="None" SetFocusOnError="true"
                                                                ErrorMessage="*กรุณากรอก จำนวนสินค้า" ValidationGroup="Save" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="validate5" Width="220" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <asp:Label ID="Label6" CssClass="col-sm-3 control-label" runat="server" data-toggle="tooltip" title="ราคาขายรวมภาษี" Text="ราคาสินค้า">ราคาสินค้า<span style="color:red;">*</span></asp:Label>
                                                            <div class="col-sm-6">
                                                                <asp:TextBox ID="txtprice" CssClass="form-control" runat="server" Text='<%# Eval("price") %>' placeholder="กรอก ราคาต่อหน่วยของสินค้า ..." />

                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server"
                                                                    ControlToValidate="txtprice" Display="None" SetFocusOnError="true"
                                                                    ErrorMessage="*กรุณากรอก ราคา " ValidationGroup="Editpname" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server"
                                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="220" />

                                                                <asp:CompareValidator runat="server" ID="RegularExpressionValidator2" Operator="DataTypeCheck" Type="Double"
                                                                    ControlToValidate="txtprice" Display="None" Font-Size="10pt" ForeColor="Red" ErrorMessage="กรุณากรอก เป็นตัวเลข" />

                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server"
                                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator2" Width="220" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-6">

                                                    <div class="form-group">
                                                        <div class="row">
                                                            <asp:Label ID="Label7" CssClass="col-sm-3 control-label" runat="server" Text="ส่วนลด" />
                                                            <div class="col-sm-6">
                                                                <asp:TextBox ID="txtdiscount" CssClass="form-control" runat="server" Text='<%# Eval("discount") %>' ErrorMessage="กรุณากรอก เป็นตัวเลข" />

                                                                <asp:CompareValidator runat="server" ID="RegularExpressionValidator1" Operator="DataTypeCheck" Type="Double"
                                                                    ControlToValidate="txtdiscount" Display="None" Font-Size="10pt" ForeColor="Red" ErrorMessage="กรุณากรอก เป็นตัวเลข" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server"
                                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="220" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class=" form-group">
                                                        <div class="col-xs-12 col-sm-3  control-label">
                                                            <asp:Label runat="server" Text="PromotionID"></asp:Label>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-6 col-md-6 ">
                                                            <asp:TextBox ID="txtPromotionID" CssClass="form-control" runat="server" Text='<%# Eval("PromotionID") %>' placeholder="กรอก รหัส Promotion ..." />
                                                            <asp:TextBox ID="txtTypepromotion" runat="server" Text='<%# Eval("type_promotion") %>' Display="None" Visible="false" />
                                                        </div>
                                                    </div>
                                                    <div class=" form-group">
                                                        <div class="col-xs-12 col-sm-3  control-label">
                                                            <asp:Label runat="server" Text="รายละเอียด Promotion"></asp:Label>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-6 col-md-6 ">
                                                            <asp:TextBox ID="txtPromotionDesc" CssClass="form-control" runat="server" Text='<%# Eval("PromotionDesc") %>' placeholder="กรอก รายละเอียด Promotion ..." />
                                                        </div>
                                                    </div>
                                                    <div class=" form-group">
                                                        <div class="col-xs-12 col-sm-3  control-label">
                                                            <asp:Label runat="server" Text="SAP Code"></asp:Label>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-6 col-md-6 ">
                                                            <asp:TextBox ID="txtSAPcode" CssClass="form-control" runat="server" Text='<%# Eval("SAP_code") %>' placeholder="กรอก รหัส SAP Code ..." />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="lbupdates_place" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                                        <div class="col-sm-6">
                                                            <asp:DropDownList ID="ddEdit_statusm1" Text='<%# Eval("bom_m1_status") %>'
                                                                CssClass="form-control" runat="server">
                                                                <asp:ListItem Value="1">Online</asp:ListItem>
                                                                <asp:ListItem Value="0">Offline</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-sm-3"></div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <%--<div class="col-sm-3"></div>--%>

                                                    <div class="col-xs-12 col-md-6">
                                                        <div class="first-margin-btn"></div>
                                                        <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" ValidationGroup="Editpname" runat="server" data-original-title="Edit" data-toggle="tooltip"
                                                            Text="Edit" CommandName="Update"></asp:LinkButton>
                                                        <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel-Edit" data-toggle="tooltip"
                                                            Text="Cancel-Edit" CommandName="Cancel"></asp:LinkButton>
                                                    </div>
                                                </div>

                                            </div>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ชื่อสินค้า" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <div style="padding-top: 5px; text-align: left;">
                                                <asp:Label runat="server"
                                                    Text='<%# Eval("product_name") %>'></asp:Label>
                                            </div>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Material" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <div style="padding-top: 5px; text-align: left;">
                                                <asp:Label runat="server"
                                                    Text='<%# Eval("material") %>'></asp:Label>
                                            </div>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="หน่วยสินค้า" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <div style="padding-top: 5px; text-align: left;">
                                                <asp:Label runat="server"
                                                    Text='<%# Eval("unit_name") %>'></asp:Label>
                                            </div>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="จำนวน" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <div style="padding-top: 5px; text-align: left;">
                                                <asp:Label runat="server"
                                                    Text='<%# Eval("amount") %>'></asp:Label>
                                            </div>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ราคา" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <div style="padding-top: 5px; text-align: left;">
                                                <asp:Label runat="server"
                                                    Text='<%# Eval("price") %>'></asp:Label>
                                            </div>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ภาษี" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <div style="padding-top: 5px; text-align: left;">
                                                <asp:Label runat="server"
                                                    Text='<%# Eval("VAT") %>'></asp:Label>
                                            </div>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ส่วนลด" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <div style="padding-top: 5px; text-align: left;">
                                                <asp:Label runat="server"
                                                    Text='<%# Eval("discount") %>'></asp:Label>
                                            </div>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="PromotionID" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <div style="padding-top: 5px; text-align: left;">
                                                <asp:Label runat="server"
                                                    Text='<%# Eval("PromotionID") %>'></asp:Label>
                                            </div>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="PromotionDesc" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <div style="padding-top: 5px; text-align: left;">
                                                <asp:Label runat="server"
                                                    Text='<%# Eval("PromotionDesc") %>'></asp:Label>
                                            </div>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="SAP Code" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <div style="padding-top: 5px; text-align: left;">
                                                <asp:Label runat="server"
                                                    Text='<%# Eval("SAP_code") %>'></asp:Label>
                                            </div>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                                    <ItemTemplate>
                                        <small>
                                            <div style="padding: 5px 10px 0px; text-align: center;">
                                                <asp:Label ID="lbpplace_status" Visible="false" runat="server"
                                                    CssClass="col-sm-12" Text='<%# Eval("bom_m1_status") %>'></asp:Label>
                                                <asp:Label ID="place_statusOnline" runat="server" Visible="false"
                                                    data-toggle="tooltip" data-placement="top" title="Online"
                                                    CssClass="col-sm-12">
                                                <div style=" color:green;">
                                                    <span><b>Online</b></span>
                                                </div>
                                                </asp:Label>
                                                <asp:Label ID="place_statusOffline" runat="server" Visible="false"
                                                    data-toggle="tooltip" data-placement="top" title="Offline" CssClass="col-sm-12">
                                                <div style=" color:red;">
                                                <span><b>Offline</b></span>
                                                     </div>
                                                </asp:Label>
                                            </div>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="จัดการข้อมูล" HeaderStyle-Width="10%" HeaderStyle-CssClass="text-center"
                                    ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <div style=" text-align: center;">

                                            <%--  <asp:LinkButton ID="Edit_m1" CssClass="text-edit p-controll" runat="server" CommandName="Edit"
                                                data-toggle="tooltip" OnCommand="btnCommand" title="Edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>--%>
                                            <%--  <asp:LinkButton ID="btnTodelete_m1" CssClass="text-trash p-controll" runat="server" CommandName="btnTodelete_m1"
                                                data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบข้อมูลนี้ใช่หรือไม่ ?')"
                                                CommandArgument='' title="Delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>--%>
                                            <asp:LinkButton ID="btn_DeleteSample" CssClass="btn btn-danger btn-sm"
                                                Visible="true" data-toggle="tooltip" title="Delete"
                                                runat="server" CommandName="Delete"
                                                OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')"><span
                                                class="glyphicon glyphicon-trash"></span></asp:LinkButton>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <div class="col-xs-12  col-sm-12">
                            <div class="form-group" runat="server">
                                <div class="col-xs-12  col-sm-6">
                                    <div class="first-margin-btn "></div>

                                    <asp:LinkButton ID="div_save_temp" Visible="false" runat="server" OnCommand="btnCommand" CommandName="CmdSave" CssClass="btn btn-success" data-toggle="tooltip" title="บันทึก" Text="Save"></asp:LinkButton>
                                    <asp:LinkButton runat="server" OnCommand="btnCommand" CommandName="CmdCancel" CssClass="btn btn-danger" data-toggle="tooltip" title="ยกเลิก" Text="Cancel"></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="div_searchmaster_page" runat="server">
                <div class=" panel panel-info">
                    <div class=" panel-heading">
                        <h3 class="panel-title">ค้นหา
                        </h3>
                    </div>
                    <div class=" panel-body">
                        <div class="form-horizontal" role="form">
                            <div class="form-group">
                                <asp:Label ID="Label130" runat="server" Text="BOM ชื่อสินค้า" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtmaster_search" runat="server" CssClass="form-control" PlaceHolder="........" />
                                </div>
                                <asp:Label ID="Label131" runat="server" Text="สถานะ" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                <div class="col-sm-3">
                                    <asp:DropDownList ID="Src_status" CssClass="form-control" runat="server">
                                        <asp:ListItem Value="999" Text="ALL" />
                                        <asp:ListItem Value="1" Text="Online" />
                                        <asp:ListItem Value="0" Text="Offline" />
                                    </asp:DropDownList>
                                </div>

                            </div>
                            <div class="form-group">
                                <div class="col-sm-2 col-sm-offset-2">
                                    <asp:LinkButton ID="LinkButton25" CssClass="btn btn-info btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="CmdSearchMaster_src" OnCommand="btnCommand"><i class="glyphicon glyphicon-search"></i>&nbsp;ค้นหา</asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton26" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnRefreshMaster_Supplier" data-toggle="tooltip" title="Refresh"><i class="glyphicon glyphicon-refresh"></i></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:GridView ID="Gv_select_place" runat="server" Visible="true" AutoGenerateColumns="false"
                CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                HeaderStyle-CssClass="success"
                AllowPaging="true"
                OnRowEditing="Master_RowEditing"
                OnRowDataBound="Master_RowDataBound"
                OnRowUpdating="Master_RowUpdating"
                OnRowCancelingEdit="Master_RowCancelingEdit"
                OnPageIndexChanging="Master_PageIndexChanging"
                PageSize="20"
                AutoPostBack="FALSE">
                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                <EmptyDataTemplate>
                    <div style="text-align: center; color: red"><b>-- ไม่พบข้อมูล --</b> </div>
                </EmptyDataTemplate>

                <Columns>
                    <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="text-align: center; padding-top: 5px;">
                                    <asp:Label ID="place_idx" runat="server" Visible="false" Text='<%# Eval("bom_m0_idx") %>' />
                                    <%# (Container.DataItemIndex +1) %>
                                </div>
                            </small>

                        </ItemTemplate>
                        <EditItemTemplate>

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txt_bom_m0_idx_edit" Visible="false" runat="server" CssClass="form-control " Text='<%# Eval("bom_m0_idx") %>'></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <asp:Label ID="Label1" CssClass="col-sm-3 control-label" runat="server">BOM ชื่อสินค้า <span style="color:red;">*</span></asp:Label>
                                                <div class="col-sm-9">
                                                    <asp:TextBox ID="Name_BOM" TextMode="multiline" CssClass="form-control" Columns="50" Rows="5" runat="server" Text='<%# Eval("bom_name") %>' placeholder="กรอกรหัส BOM ชื่อสินค้า เช่น เถ้าแก่น้อย -   สาหร่ายเทมปุระ รสราเมน 18 กรัม (6ซอง/แพ็ค) (1302474) เป็นต้น" />
                                                    <%--<asp:TextBox ID="Name_material" runat="server" CssClass="form-control " Text='<%# Eval("material") %>'></asp:TextBox>--%>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                                                        ControlToValidate="Name_BOM" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณากรอกรหัส BOM " ValidationGroup="Editpname" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="220" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="lbupdates_place" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                            <div class="col-sm-9">
                                                <asp:DropDownList ID="ddEdit_place" Text='<%# Eval("bom_status") %>'
                                                    CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="1">Online</asp:ListItem>
                                                    <asp:ListItem Value="0">Offline</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <div class="form-group">
                                            <div class="col-xs-12 col-sm-3  control-label">
                                                <asp:Label runat="server" Text="วันที่เริ่มต้น">วันที่เริ่มต้น<span style="color:red;">*</span></asp:Label>
                                            </div>
                                            <div class="col-xs-12 col-sm-6 col-md-6 ">
                                                <asp:TextBox ID="tbDateStart" runat="server" CssClass="form-control date-datepicker-date start-date" MaxLength="20" autocomplete="off" Text='<%# Eval("start_date") %>'>
                                                </asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">

                                            <div class="col-xs-12 col-sm-3 control-label">
                                                <asp:Label runat="server" Text="วันที่สิ้นสุด">วันที่สิ้นสุด<span style="color:red;">*</span></asp:Label>
                                            </div>
                                            <div class="col-xs-12 col-sm-6 col-md-6 ">
                                                <asp:TextBox ID="tbDateEnd" runat="server" CssClass="form-control date-datepicker-date end-date" MaxLength="20" autocomplete="off" Text='<%# Eval("end_date") %>'>
                                                </asp:TextBox>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-xs-12 col-md-6">
                                        <div class="form-group">
                                            <div class="col-sm-3"></div>
                                            <div class="col-sm-9">
                                                <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" ValidationGroup="Editpname" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                    Text="Save" CommandName="Update"></asp:LinkButton>
                                                <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                    Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="BOM ชื่อสินค้า" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px; text-align: left;">
                                    <asp:Label runat="server"
                                        Text='<%# Eval("bom_name") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>


                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="วันที่เริ่มต้น" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px; text-align: left;">
                                    <asp:Label runat="server"
                                        Text='<%# Eval("start_date") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>


                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="วันที่สิ้นสุด" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px; text-align: left;">
                                    <asp:Label runat="server"
                                        Text='<%# Eval("end_date") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>


                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                        <ItemTemplate>
                            <small>
                                <div style="padding: 5px 10px 0px; text-align: center;">
                                    <asp:Label ID="lbpplace_status" Visible="false" runat="server"
                                        CssClass="col-sm-12" Text='<%# Eval("bom_status") %>'></asp:Label>
                                    <asp:Label ID="place_statusOnline" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="Online"
                                        CssClass="col-sm-12">
                                                <div style=" color:green;">
                                                    <span><b>Online</b></span>
                                                </div>
                                    </asp:Label>
                                    <asp:Label ID="place_statusOffline" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="Offline" CssClass="col-sm-12">
                                                <div style=" color:red;">
                                                <span><b>Offline</b></span>
                                                     </div>
                                    </asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="จัดการข้อมูล" HeaderStyle-Width="15%" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style=" text-align: center;">
                                <asp:LinkButton ID="readdetail" CssClass="btn btn-warning btn-sm" runat="server" CommandName="readdetail"
                                    data-toggle="tooltip" OnCommand="btnCommand" CommandArgument='<%#Eval("bom_m0_idx") %>' title="Read more"><i class="fa fa-list"></i></asp:LinkButton>
                                <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit"
                                    data-toggle="tooltip" OnCommand="btnCommand" title="Edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                                <asp:LinkButton ID="btnTodelete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="btnTodelete"
                                    data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบข้อมูลนี้ใช่หรือไม่ ?')"
                                    CommandArgument='<%#Eval("bom_m0_idx") %>' title="Delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>

        </asp:View>
        <asp:View ID="ViewReadmore" runat="server">
            <div class="form-group">
                <asp:LinkButton ID="LinkButton1" CssClass="btn btn-primary" Visible="true" data-original-title="ย้อนกลับ" data-toggle="tooltip" title="ย้อนกลับ" runat="server" CommandName="cmd_backward" OnCommand="btnCommand"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;ย้อนกลับ</asp:LinkButton>
                <asp:LinkButton ID="btn_adddetail" CssClass="btn btn-primary" Visible="true" data-original-title="เพิ่ม Detail" data-toggle="tooltip" title="เพิ่ม Detail" runat="server" CommandName="cmdAdddetail" OnCommand="btnCommand"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;เพิ่ม รายละเอียด</asp:LinkButton>

                <div class="clearfix"></div>
            </div>

            <asp:FormView ID="FvsubBOM" runat="server" DefaultMode="ReadOnly" Width="100%" OnDataBound="Fv_DataBound">
                <InsertItemTemplate>
                    <div class=" panel panel-info">
                        <div class=" panel-heading">
                            <h3 class="panel-title">เพิ่ม รายละเอียด
                            </h3>

                        </div>
                        <div class=" panel-body">

                            <div class=" form-horizontal" style="padding: 20px 30px;">
                                <div class="col-xs-12 col-md-6">
                                    <div class=" form-group">
                                        <div class="col-xs-12 col-sm-4  control-label">
                                            <asp:Label runat="server" Text="ชื่อ สินค้า">เลือกรหัส Material<span style="color:red;">*</span></asp:Label>
                                        </div>
                                        <div class="col-xs-12 col-sm-8 ">
                                            <asp:DropDownList ID="ddl_material_m1" CssClass="form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="OnSelectedIndexChanged">
                                            </asp:DropDownList>

                                        </div>

                                    </div>
                                    <div class=" form-group">
                                        <div class="col-xs-12 col-sm-4  control-label">
                                            <asp:Label runat="server" Text="ชื่อ สินค้า">ชื่อ สินค้า<span style="color:red;">*</span></asp:Label>
                                        </div>
                                        <div class="col-xs-12 col-sm-8 ">
                                            <%--<asp:TextBox ID="txtmaterial" runat="server" CssClass="form-control" placeholder="กรอกรหัส Material ..." Enabled="true" />--%>
                                            <asp:TextBox ID="txtProductname" CssClass="form-control" runat="server" placeholder="กรอกรหัส ชื่อสินค้า ..." ReadOnly="true" />
                                            <asp:RequiredFieldValidator ID="Re_place_name" runat="server"
                                                ControlToValidate="txtProductname" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกชื่อสินค้า" ValidationGroup="Save" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="aj_pname" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_place_name" Width="220" />
                                        </div>
                                    </div>

                                    <div class=" form-group">
                                        <div class="col-xs-12 col-sm-4  control-label">
                                            <asp:Label runat="server" Text="รหัส material">รหัส material<span style="color:red;">*</span></asp:Label>
                                        </div>
                                        <div class="col-xs-12 col-sm-8 ">

                                            <asp:TextBox ID="txtmaterial" CssClass="form-control" runat="server" placeholder="กรอกรหัส Material ..." ReadOnly="true" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                                ControlToValidate="txtmaterial" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกรหัส material" ValidationGroup="Save" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="220" />
                                        </div>
                                    </div>

                                    <div class=" form-group">
                                        <div class="col-xs-12 col-sm-4  control-label">
                                            <asp:Label runat="server" Text="หน่วยสินค้า">หน่วยสินค้า<span style="color:red;">*</span></asp:Label>
                                        </div>
                                        <div class="col-xs-12 col-sm-8 ">

                                            <asp:TextBox ID="txtunit" CssClass="form-control" runat="server" placeholder="กรอก หน่วยของสินค้า ..." />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server"
                                                ControlToValidate="txtunit" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอก หน่วยสินค้า" ValidationGroup="Save" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator3" Width="220" />
                                        </div>
                                    </div>
                                    <div class=" form-group">
                                        <div class="col-xs-12 col-sm-4  control-label">
                                            <asp:Label runat="server" Text="จำวนวน">จำนวน<span style="color:red;">*</span></asp:Label>
                                        </div>
                                        <div class="col-xs-12 col-sm-8 ">

                                            <asp:TextBox ID="txtamount" CssClass="form-control" runat="server" placeholder="กรอก จำนวนสินค้า ..." />


                                            <asp:CompareValidator runat="server" ID="CompareValidator1" Operator="DataTypeCheck" Type="Integer"
                                                ControlToValidate="txtamount" Display="None" Font-Size="10pt" ForeColor="Red" ErrorMessage="กรุณากรอก เป็นตัวเลข" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="CompareValidator1" Width="220" />

                                            <asp:RequiredFieldValidator ID="validate5" runat="server"
                                                ControlToValidate="txtamount" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอก จำนวนสินค้า" ValidationGroup="Save" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="validate5" Width="220" />

                                        </div>
                                    </div>
                                    <div class=" form-group">
                                        <div class="col-xs-12 col-sm-4  control-label">
                                            <asp:Label runat="server" Text="ราคาสินค้า">ราคาสินค้า<span style="color:red;">*</span></asp:Label>
                                        </div>
                                        <div class="col-xs-12 col-sm-8 ">

                                            <asp:TextBox ID="txtprice" CssClass="form-control" runat="server" placeholder="กรอก ราคาต่อหน่วยของสินค้า ..." />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server"
                                                ControlToValidate="txtprice" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอก ราคาต่อหน่วยของสินค้า " ValidationGroup="Save" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator4" Width="220" />

                                            <asp:CompareValidator runat="server" ID="RegularExpressionValidator2" Operator="DataTypeCheck" Type="Double"
                                                ControlToValidate="txtprice" Display="None" Font-Size="10pt" ForeColor="Red" ErrorMessage="กรุณากรอก เป็นตัวเลข" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator2" Width="220" />
                                        </div>
                                    </div>
                                    <div class=" form-group">
                                        <div class="col-xs-12 col-sm-4  control-label">
                                            <asp:Label runat="server" Text="">ภาษี<span style="color:red;">*</span></asp:Label>
                                        </div>
                                        <div class="col-xs-12 col-sm-8 ">

                                            <asp:TextBox ID="txttex" CssClass="form-control" runat="server" placeholder="กรอก ภาษีต่อหน่วยของสินค้า ..." />

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="txttex" Display="None" SetFocusOnError="true" ErrorMessage="*กรุณากรอก ภาษีต่อหน่วยของสินค้า " ValidationGroup="Save" />
                                            <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender17" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator13" Width="220" />

                                            <asp:CompareValidator runat="server" ID="CompareValidator3" Operator="DataTypeCheck" Type="Double"
                                                ControlToValidate="txttex" Display="None" Font-Size="10pt" ForeColor="Red" ErrorMessage="กรุณากรอก เป็นตัวเลข" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender18" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="CompareValidator3" Width="220" />
                                        </div>
                                    </div>

                                </div>

                                <div class="col-xs-12 col-md-6">
                                    <div class=" form-group">
                                        <div class="col-xs-12 col-sm-4  control-label">
                                            <asp:Label runat="server" Text="">เลือก Promotion</asp:Label>
                                        </div>
                                        <div class="col-xs-12 col-sm-8 ">
                                            <asp:DropDownList ID="ddl_promotion_m1" CssClass="form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="OnSelectedIndexChanged">
                                            </asp:DropDownList>

                                        </div>

                                    </div>
                                    <div class=" form-group">
                                        <div class="col-xs-12 col-sm-4  control-label">
                                            <asp:Label runat="server" Text="ส่วนลด"></asp:Label>
                                        </div>
                                        <div class="col-xs-12 col-sm-8 ">

                                            <asp:TextBox ID="txtdiscount_insert" CssClass="form-control" runat="server" placeholder="กรอก ส่วนลด ..." ReadOnly="true" />

                                            <asp:CompareValidator runat="server" ID="RegularExpressionValidator12" Operator="DataTypeCheck" Type="Double"
                                                ControlToValidate="txtdiscount_insert" Display="None" Font-Size="10pt" ForeColor="Red" ErrorMessage="กรุณากรอก เป็นตัวเลข" />
                                            <ajaxToolkit:ValidatorCalloutExtender runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator12" Width="220" />
                                        </div>
                                    </div>
                                    <div class=" form-group">
                                        <div class="col-xs-12 col-sm-4  control-label">
                                            <asp:Label runat="server" Text="PromotionID"></asp:Label>
                                        </div>
                                        <div class="col-xs-12 col-sm-8 ">
                                            <asp:TextBox ID="txtPromotionID" CssClass="form-control" runat="server" placeholder="กรอก รหัส Promotion ..." ReadOnly="true" />
                                            <asp:TextBox ID="txtTypepromotion" runat="server" Display="None" Visible="false" />
                                        </div>
                                    </div>
                                    <div class=" form-group">
                                        <div class="col-xs-12 col-sm-4  control-label">
                                            <asp:Label runat="server" Text="รายละเอียด Promotion"></asp:Label>
                                        </div>
                                        <div class="col-xs-12 col-sm-8">
                                            <asp:TextBox ID="txtPromotionDesc" CssClass="form-control" runat="server" placeholder="กรอก รายละเอียด Promotion ..." ReadOnly="true" />
                                        </div>
                                    </div>
                                    <div class=" form-group">
                                        <div class="col-xs-12 col-sm-4  control-label">
                                            <asp:Label runat="server" Text="SAP Code"></asp:Label>
                                        </div>
                                        <div class="col-xs-12 col-sm-8">
                                            <asp:TextBox ID="txtSAPcode" CssClass="form-control" runat="server" placeholder="กรอก รหัส SAP Code ..." ReadOnly="true" />
                                        </div>
                                    </div>
                                    <div class=" form-group">
                                        <div class="col-xs-12 col-sm-4  control-label">
                                            <asp:Label runat="server" Text="สถานะ"></asp:Label>
                                        </div>
                                        <div class="col-xs-12 col-sm-8">
                                            <asp:DropDownList ID="ddPlace" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="1" Text="Online" />
                                                <asp:ListItem Value="0" Text="Offline" />
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class=" form-group">
                                        <div class="col-xs-12 col-sm-4  control-label">
                                            <asp:Label runat="server" Text="ภาษีรวม :"></asp:Label>
                                        </div>
                                        <div class="col-xs-12 col-sm-3  pt-7 overflow-text">
                                            <asp:Label ID="txtVATAmount" runat="server" Text="0"></asp:Label>
                                        </div>
                                        <div class="col-xs-12 col-sm-2  control-label">
                                            <asp:Label runat="server" data-toggle="tooltip" title="ราคารวม" Text="ราคารวม :"></asp:Label>
                                        </div>
                                        <div class="col-xs-12 col-sm-3  pt-7 overflow-text">
                                            <asp:Label ID="txtNetPriceAmount" runat="server" Text="0"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <div class="col-xs-12 col-sm-offset-2 col-sm-9">
                                            <asp:LinkButton runat="server" OnCommand="btnCommand" CommandName="CmdSaveDe" CssClass="btn btn-success" data-toggle="tooltip" title="บันทึก" Text="Save" ValidationGroup="Save"></asp:LinkButton>
                                            <asp:LinkButton runat="server" OnCommand="btnCommand" CommandName="CmdCancelDe" CssClass="btn btn-danger" data-toggle="tooltip" title="ยกเลิก" Text="Cancel"></asp:LinkButton>
                                            <asp:LinkButton runat="server" OnCommand="btnCommand" CommandName="cal_price_and_vat_sub" CssClass="btn btn-default " data-toggle="tooltip" title="คลิกเพื่อ คำนวณราคาและภาษี รวม"><i class="fa fa-calculator" aria-hidden="true" style="margin-right:7px;"></i>คำนวณราคาและภาษี รวม</asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>
            <div id="sreach_detailbox" runat="server">
                <div class=" panel panel-info">
                    <div class=" panel-heading">
                        <h3 class="panel-title">ค้นหา
                        </h3>
                    </div>
                    <div class=" panel-body">
                        <div class="form-horizontal" role="form">
                            <div class="form-group">
                                <asp:Label ID="Label2" runat="server" Text="Material" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                <div class="col-sm-3">
                                    <asp:TextBox ID="src_material" runat="server" CssClass="form-control" PlaceHolder="........" />
                                </div>
                                <asp:Label ID="Label3" runat="server" Text="สถานะ" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                <div class="col-sm-3">
                                    <asp:DropDownList ID="ddsrc_status" CssClass="form-control" runat="server">
                                        <asp:ListItem Value="999" Text="ALL" />
                                        <asp:ListItem Value="1" Text="Online" />
                                        <asp:ListItem Value="0" Text="Offline" />
                                    </asp:DropDownList>
                                </div>

                            </div>
                            <div class="form-group">
                                <div class="col-sm-2 col-sm-offset-2">
                                    <asp:LinkButton ID="LinkButton2" CssClass="btn btn-info btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="CmdSearchsub_src" OnCommand="btnCommand"><i class="glyphicon glyphicon-search"></i>&nbsp;ค้นหา</asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton3" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnRefreshsub" data-toggle="tooltip" title="Refresh"><i class="glyphicon glyphicon-refresh"></i></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class=" panel panel-success">
                <div class=" panel-heading">
                    <h3 class="panel-title" id="BOM_header_name" runat="server">BOM
                    </h3>
                </div>

            </div>
            <asp:GridView ID="Gvm1" runat="server" Visible="true" AutoGenerateColumns="false"
                CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                HeaderStyle-CssClass="success"
                AllowPaging="true"
                OnRowEditing="Master_RowEditing"
                OnRowDataBound="Master_RowDataBound"
                OnRowUpdating="Master_RowUpdating"
                OnRowCancelingEdit="Master_RowCancelingEdit"
                OnPageIndexChanging="Master_PageIndexChanging"
                PageSize="10"
                AutoPostBack="FALSE">
                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                <EmptyDataTemplate>
                    <div style="text-align: center; color: red"><b>-- ไม่พบข้อมูล --</b> </div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="text-align: center; padding-top: 5px;">
                                    <asp:Label ID="bom_m1_idx" runat="server" Visible="false" Text='<%# Eval("bom_m1_idx") %>' />
                                    <%# (Container.DataItemIndex +1) %>
                                </div>
                            </small>

                        </ItemTemplate>
                        <EditItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txt_bom_m0_idx_edit" Visible="false" runat="server" CssClass="form-control " Text='<%# Eval("bom_m1_idx") %>'></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <div class=" form-group">
                                            <div class="col-xs-12 col-sm-4  control-label">
                                                <asp:Label runat="server" Text="ชื่อ สินค้า">เลือกรหัส Material<span style="color:red;"></span></asp:Label>
                                            </div>
                                            <div class="col-xs-12 col-sm-8 ">
                                                <asp:DropDownList ID="ddl_material_m1_edit" CssClass="form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="OnSelectedIndexChanged">
                                                </asp:DropDownList>

                                            </div>

                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <asp:Label ID="Label1" CssClass="col-sm-4 control-label" runat="server" Text="ชื่อ สินค้า">ชื่อ สินค้า<span style="color:red;">*</span></asp:Label>
                                                <div class="col-sm-8">
                                                    <asp:TextBox ID="txtproduct_name" CssClass="form-control" runat="server" Text='<%# Eval("product_name") %>' placeholder="กรอกรหัส ชื่อสินค้า ..." ReadOnly="true" />
                                                    <%--<asp:TextBox ID="Name_material" runat="server" CssClass="form-control " Text='<%# Eval("material") %>'></asp:TextBox>--%>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                                                        ControlToValidate="txtproduct_name" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณากรอก ชื่อสินค้า " ValidationGroup="Editpname" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="220" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <asp:Label ID="Label4" CssClass="col-sm-4 control-label" runat="server" Text="รหัส material">รหัส material<span style="color:red;">*</span></asp:Label>
                                                <div class="col-sm-8">
                                                    <asp:TextBox ID="txtmaterial" CssClass="form-control" runat="server" Text='<%# Eval("material") %>' placeholder="กรอกรหัส Material ..." ReadOnly="true" />

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server"
                                                        ControlToValidate="txtmaterial" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณากรอก material " ValidationGroup="Editpname" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="220" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <asp:Label ID="Label5" CssClass="col-sm-4 control-label" runat="server" Text="หน่วยสินค้า">หน่วยสินค้า<span style="color:red;">*</span></asp:Label>
                                                <div class="col-sm-8">
                                                    <asp:TextBox ID="txtunit_name" CssClass="form-control" runat="server" Text='<%# Eval("unit_name") %>' placeholder="กรอก หน่วยของสินค้า ..." />

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server"
                                                        ControlToValidate="txtunit_name" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณากรอก หน่วยของสินค้า " ValidationGroup="Editpname" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="220" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class=" form-group">
                                            <div class="col-xs-12 col-sm-4  control-label">
                                                <asp:Label runat="server" Text="จำวนวน">จำวนวน<span style="color:red;">*</span></asp:Label>
                                            </div>
                                            <div class="col-xs-12 col-sm-8 ">

                                                <asp:TextBox ID="txtamount" CssClass="form-control" runat="server" Text='<%# Eval("amount") %>' placeholder="กรอก จำนวนสินค้า ..." />

                                                <asp:CompareValidator runat="server" ID="CompareValidator1" Operator="DataTypeCheck" Type="Integer"
                                                    ControlToValidate="txtamount" Display="None" Font-Size="10pt" ForeColor="Red" ErrorMessage="กรุณากรอก เป็นตัวเลข" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="CompareValidator1" Width="220" />

                                                <asp:RequiredFieldValidator ID="validate5" runat="server"
                                                    ControlToValidate="txtamount" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอก จำนวนสินค้า" ValidationGroup="Save" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="validate5" Width="220" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <asp:Label ID="Label6" CssClass="col-sm-4 control-label" runat="server" Text="ราคาสินค้า">ราคาสินค้า<span style="color:red;">*</span></asp:Label>
                                                <div class="col-sm-8">
                                                    <asp:TextBox ID="txtprice" CssClass="form-control" runat="server" Text='<%# Eval("price") %>' placeholder="กรอก ราคาต่อหน่วยของสินค้า ..." />

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server"
                                                        ControlToValidate="txtprice" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณากรอก ราคา " ValidationGroup="Editpname" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="220" />

                                                    <asp:CompareValidator runat="server" ID="RegularExpressionValidator2" Operator="DataTypeCheck" Type="Double"
                                                        ControlToValidate="txtprice" Display="None" Font-Size="10pt" ForeColor="Red" ErrorMessage="กรุณากรอก เป็นตัวเลข" />

                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator2" Width="220" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <asp:Label ID="Label8" CssClass="col-sm-4 control-label" runat="server" Text="ภาษี">ภาษี<span style="color:red;">*</span></asp:Label>
                                                <div class="col-sm-8">
                                                    <asp:TextBox ID="txttex" CssClass="form-control" runat="server" Text='<%# Eval("VAT") %>' placeholder="กรอก ราคาต่อหน่วยของสินค้า ..." />

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server"
                                                        ControlToValidate="txttex" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณากรอก ราคา " ValidationGroup="Editpname" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender19" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator14" Width="220" />

                                                    <asp:CompareValidator runat="server" ID="CompareValidator4" Operator="DataTypeCheck" Type="Double"
                                                        ControlToValidate="txtprice" Display="None" Font-Size="10pt" ForeColor="Red" ErrorMessage="กรุณากรอก เป็นตัวเลข" />

                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender20" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="CompareValidator4" Width="220" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <div class=" form-group">
                                            <div class="col-xs-12 col-sm-4  control-label">
                                                <asp:Label runat="server" Text="">เลือก Promotion</asp:Label>
                                            </div>
                                            <div class="col-xs-12 col-sm-8 ">
                                                <asp:DropDownList ID="ddl_promotion_m1_edit" CssClass="form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="OnSelectedIndexChanged">
                                                </asp:DropDownList>

                                            </div>

                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <asp:Label ID="Label7" CssClass="col-sm-4 control-label" runat="server" Text="ส่วนลด" />
                                                <div class="col-sm-8">
                                                    <asp:TextBox ID="txtdiscount" CssClass="form-control" runat="server" Text='<%# Eval("discount") %>' ErrorMessage="กรุณากรอก เป็นตัวเลข" ReadOnly="true" />

                                                    <asp:CompareValidator runat="server" ID="RegularExpressionValidator1" Operator="DataTypeCheck" Type="Double"
                                                        ControlToValidate="txtdiscount" Display="None" Font-Size="10pt" ForeColor="Red" ErrorMessage="กรุณากรอก เป็นตัวเลข" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="220" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class=" form-group">
                                            <div class="col-xs-12 col-sm-4  control-label">
                                                <asp:Label runat="server" Text="PromotionID"></asp:Label>
                                            </div>
                                            <div class="col-xs-12 col-sm-8 ">
                                                <asp:TextBox ID="txtPromotionID" CssClass="form-control" runat="server" Text='<%# Eval("PromotionID") %>' placeholder="กรอก รหัส Promotion ..." ReadOnly="true" />
                                                <asp:TextBox ID="txtTypepromotion" runat="server" Text='<%# Eval("type_promotion") %>' Display="None" Visible="false" />
                                            </div>
                                        </div>
                                        <div class=" form-group">
                                            <div class="col-xs-12 col-sm-4  control-label">
                                                <asp:Label runat="server" Text="รายละเอียด Promotion"></asp:Label>
                                            </div>
                                            <div class="col-xs-12 col-sm-8 ">
                                                <asp:TextBox ID="txtPromotionDesc" CssClass="form-control" runat="server" Text='<%# Eval("PromotionDesc") %>' placeholder="กรอก รายละเอียด Promotion ..." ReadOnly="true" />
                                            </div>
                                        </div>
                                        <div class=" form-group">
                                            <div class="col-xs-12 col-sm-4  control-label">
                                                <asp:Label runat="server" Text="SAP Code"></asp:Label>
                                            </div>
                                            <div class="col-xs-12 col-sm-8 ">
                                                <asp:TextBox ID="txtSAPcode" CssClass="form-control" runat="server" Text='<%# Eval("SAP_code") %>' placeholder="กรอก รหัส SAP Code ..." ReadOnly="true" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="lbupdates_place" CssClass="col-sm-4 control-label" runat="server" Text="สถานะ" />
                                            <div class="col-sm-8">
                                                <asp:DropDownList ID="ddEdit_statusm1" Text='<%# Eval("bom_m1_status") %>'
                                                    CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="1">Online</asp:ListItem>
                                                    <asp:ListItem Value="0">Offline</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                        </div>
                                        <div class=" form-group">
                                            <div class="col-xs-12 col-sm-4  control-label">
                                                <asp:Label runat="server" Text="ภาษีรวม :"></asp:Label>
                                            </div>
                                            <div class="col-xs-12 col-sm-3  pt-7 overflow-text">
                                                <asp:Label ID="txtVATAmount" runat="server" Text="0"></asp:Label>
                                            </div>
                                            <div class="col-xs-12 col-sm-2  control-label">
                                                <asp:Label runat="server" data-toggle="tooltip" title="ราคารวม" Text="ราคารวม :"></asp:Label>
                                            </div>
                                            <div class="col-xs-12 col-sm-3  pt-7 overflow-text">
                                                <asp:Label ID="txtNetPriceAmount" runat="server" Text="0"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                           
                                            <div class="col-xs-12 col-sm-offset-2 col-sm-9">
                                                <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" ValidationGroup="Editpname" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                    Text="Save" CommandName="Update"></asp:LinkButton>
                                                <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                    Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                                 <asp:LinkButton runat="server" OnCommand="btnCommand" CommandName="cal_price_and_vat_edit" CssClass="btn btn-default " data-toggle="tooltip" title="คลิกเพื่อ คำนวณราคาและภาษี รวม"><i class="fa fa-calculator" aria-hidden="true" style="margin-right:7px;"></i>คำนวณราคาและภาษี รวม</asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ชื่อสินค้า" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px; text-align: left;">
                                    <asp:Label runat="server"
                                        Text='<%# Eval("product_name") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Material" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px; text-align: left;">
                                    <asp:Label runat="server"
                                        Text='<%# Eval("material") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="หน่วยสินค้า" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px; text-align: left;">
                                    <asp:Label runat="server"
                                        Text='<%# Eval("unit_name") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="จำนวน" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px; text-align: left;">
                                    <asp:Label runat="server"
                                        Text='<%# Eval("amount") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ราคา" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px; text-align: left;">
                                    <asp:Label runat="server"
                                        Text='<%# Eval("price") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ภาษี" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px; text-align: left;">
                                    <asp:Label runat="server"
                                        Text='<%# Eval("VAT") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ส่วนลด" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px; text-align: left;">
                                    <asp:Label runat="server"
                                        Text='<%# Eval("discount") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="PromotionID" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px; text-align: left;">
                                    <asp:Label runat="server"
                                        Text='<%# Eval("PromotionID") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="PromotionDesc" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px; text-align: left;">
                                    <asp:Label runat="server"
                                        Text='<%# Eval("PromotionDesc") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="SAP Code" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px; text-align: left;">
                                    <asp:Label runat="server"
                                        Text='<%# Eval("SAP_code") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                        <ItemTemplate>
                            <small>
                                <div style="padding: 5px 10px 0px; text-align: center;">
                                    <asp:Label ID="lbpplace_status" Visible="false" runat="server"
                                        CssClass="col-sm-12" Text='<%# Eval("bom_m1_status") %>'></asp:Label>
                                    <asp:Label ID="place_statusOnline" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="Online"
                                        CssClass="col-sm-12">
                                                <div style=" color:green;">
                                                    <span><b>Online</b></span>
                                                </div>
                                    </asp:Label>
                                    <asp:Label ID="place_statusOffline" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="Offline" CssClass="col-sm-12">
                                                <div style=" color:red;">
                                                <span><b>Offline</b></span>
                                                     </div>
                                    </asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="จัดการข้อมูล" HeaderStyle-Width="10%" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style=" text-align: center;">

                                <asp:LinkButton ID="Edit_m1" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit"
                                    data-toggle="tooltip" OnCommand="btnCommand" title="Edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                                <asp:LinkButton ID="btnTodelete_m1" CssClass="btn btn-danger btn-sm" runat="server" CommandName="btnTodelete_m1"
                                    data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบข้อมูลนี้ใช่หรือไม่ ?')"
                                    CommandArgument='<%#Eval("bom_m1_idx") %>' title="Delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>

        </asp:View>

    </asp:MultiView>

    <script type="text/javascript">

        $(function () {
            $('.start-date').datetimepicker({
                format: 'YYYY-MM-DD',
                useCurrent: false
            });
            $('.end-date').datetimepicker({
                format: 'YYYY-MM-DD',
                useCurrent: false
            });
            $(".start-date").on("dp.change", function (e) {
                $('.end-date').data("DateTimePicker").minDate(e.date);
            });
            $(".end-date").on("dp.change", function (e) {
                $('.start-date').data("DateTimePicker").maxDate(e.date);
            });
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.start-date').datetimepicker({
                    format: 'YYYY-MM-DD',
                    useCurrent: false
                });
                $('.end-date').datetimepicker({
                    format: 'YYYY-MM-DD',
                    useCurrent: false
                });
                $(".start-date").on("dp.change", function (e) {
                    $('.end-date').data("DateTimePicker").minDate(e.date);
                });
                $(".end-date").on("dp.change", function (e) {
                    $('.start-date').data("DateTimePicker").maxDate(e.date);
                });
            });
        });
    </script>
</asp:Content>
