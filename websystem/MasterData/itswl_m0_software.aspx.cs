﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_itswl_m0_software : System.Web.UI.Page
{

    #region initial function/data
    function_tool _funcTool = new function_tool();

    data_softwarelicense _data_softwarelicense = new data_softwarelicense();
    data_permission _data_permission = new data_permission();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    //static string _urlSetm0Company = _serviceUrl + ConfigurationManager.AppSettings["urlSetm0Company"];
    static string _urlGetddlCompanySoftware = _serviceUrl + ConfigurationManager.AppSettings["urlGetddlCompanySoftware"];
    static string _urlGetddlSofware = _serviceUrl + ConfigurationManager.AppSettings["urlGetddlSofware"];
    static string _urlGetTypename = _serviceUrl + ConfigurationManager.AppSettings["urlGetTypename"];


    //Organization
    static string _urlGetddlOrganizationSW = _serviceUrl + ConfigurationManager.AppSettings["urlGetddlOrganizationSW"];
    static string _urlGetddlDeptSW = _serviceUrl + ConfigurationManager.AppSettings["urlGetddlDeptSW"];
    static string _urlSetm0Software = _serviceUrl + ConfigurationManager.AppSettings["urlSetm0Software"];
    static string _urlGetm0Software = _serviceUrl + ConfigurationManager.AppSettings["urlGetm0Software"];
    static string _urlGetm0SoftwareDetail = _serviceUrl + ConfigurationManager.AppSettings["urlGetm0SoftwareDetail"];
    static string _urlGetlogcountDetailDept = _serviceUrl + ConfigurationManager.AppSettings["urlGetlogcountDetailDept"];
    static string _urlGetddlDept = _serviceUrl + ConfigurationManager.AppSettings["urlGetddlDept"];
    static string _urlSetDelm0Software = _serviceUrl + ConfigurationManager.AppSettings["urlSetDelm0Software"];



    string _localJson = "";
    int _tempInt = 0;

    string _defaultDdlText;
    string _defaultDdlValue;

    int emp_idx = 0;
   
    #endregion initial function/data

    protected void Page_Load(object sender, EventArgs e)
    {

        emp_idx = int.Parse(Session["emp_idx"].ToString());

        if (!IsPostBack)
        {
            initPage();
            actionIndex();

            /////////////////////////////////////////////////////
            var dsEquipment = new DataSet();
            dsEquipment.Tables.Add("Equipment");
            

            dsEquipment.Tables[0].Columns.Add("Software_License_dataset", typeof(String));
            dsEquipment.Tables[0].Columns.Add("Softwareidx_License_dataset", typeof(int));
            dsEquipment.Tables[0].Columns.Add("OrgNameTH_dataset", typeof(String));
            dsEquipment.Tables[0].Columns.Add("OrgIDX_dataset", typeof(int));
            dsEquipment.Tables[0].Columns.Add("DeptNameTH_dataset", typeof(String));
            dsEquipment.Tables[0].Columns.Add("RDeptIDX_dataset", typeof(int));
            dsEquipment.Tables[0].Columns.Add("Asset_dataset", typeof(String));
            dsEquipment.Tables[0].Columns.Add("numlicense_dataset", typeof(int));
            ////dsEquipment.Tables[0].Columns.Add("status_id", typeof(int));
            //dsEquipment.Tables[0].Columns.Add("ra_name_sub", typeof(String));
            //dsEquipment.Tables[0].Columns.Add("name_sub", typeof(String));

            
            ViewState["vsBuyequipment"] = dsEquipment;

            /////////////////////////////////////////////////////

            var dsEquipment_Edit = new DataSet();
            dsEquipment_Edit.Tables.Add("Equipment_Edit");

            dsEquipment_Edit.Tables[0].Columns.Add("SoftwareEdit_License_dataset", typeof(String));
            dsEquipment_Edit.Tables[0].Columns.Add("OrgNameTHEdit_dataset", typeof(String));
            dsEquipment_Edit.Tables[0].Columns.Add("OrgIDXEdit_dataset", typeof(int));
            dsEquipment_Edit.Tables[0].Columns.Add("DeptNameTHEdit_dataset", typeof(String));
            dsEquipment_Edit.Tables[0].Columns.Add("RDeptIDXEdit_dataset", typeof(int));
            dsEquipment_Edit.Tables[0].Columns.Add("numlicenseEdit_dataset", typeof(int));
            ////dsEquipment.Tables[0].Columns.Add("status_id", typeof(int));
            //dsEquipment.Tables[0].Columns.Add("ra_name_sub", typeof(String));
            //dsEquipment.Tables[0].Columns.Add("name_sub", typeof(String));


            ViewState["vsBuyequipmentEdit"] = dsEquipment_Edit;

            /////////////////////////////////////////////////////
            ////var ds_bind = new DataSet();
            ////ds_bind.Tables.Add("Equipment_bind");

            ////ds_bind.Tables[0].Columns.Add("Software_License_dataset_bind", typeof(String));
            //////ds_bind.Tables[0].Columns.Add("OrgNameTH_dataset", typeof(String));
            ////ds_bind.Tables[0].Columns.Add("OrgIDX_dataset_bind", typeof(int));
            ////////dsEquipment.Tables[0].Columns.Add("DeptNameTH_dataset", typeof(String));
            ////////dsEquipment.Tables[0].Columns.Add("RDeptIDX_dataset", typeof(int));
            ////////dsEquipment.Tables[0].Columns.Add("numlicense_dataset", typeof(int));
            ////////dsEquipment.Tables[0].Columns.Add("status_id", typeof(int));
            //////dsEquipment.Tables[0].Columns.Add("ra_name_sub", typeof(String));
            //////dsEquipment.Tables[0].Columns.Add("name_sub", typeof(String));


            ////ViewState["data_bind_dept"] = ds_bind;


        }

    }

    #region Selected 

    protected void actionIndex()
    {

        data_softwarelicense _data_softwarelicenseindex = new data_softwarelicense();
        _data_softwarelicenseindex.bind_m0software_list = new bind_m0software_detail[1];

        bind_m0software_detail _bind_m0software_detailindex = new bind_m0software_detail();

        _bind_m0software_detailindex.software_idx = 0;

        _data_softwarelicenseindex.bind_m0software_list[0] = _bind_m0software_detailindex;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicenseindex));
        _data_softwarelicenseindex = callServiceSoftwareLicense(_urlGetm0Software, _data_softwarelicenseindex);

        setGridData(GvMaster, _data_softwarelicenseindex.bind_m0software_list);

    }

    protected void SelectViewIndex()
    {

        data_softwarelicense _data_softwarelicenseview = new data_softwarelicense();
        _data_softwarelicenseview.bind_m0software_list = new bind_m0software_detail[1];

        bind_m0software_detail _bind_m0software_detailindex = new bind_m0software_detail();

        _bind_m0software_detailindex.software_idx = int.Parse(ViewState["software_idx"].ToString());

        _data_softwarelicenseview.bind_m0software_list[0] = _bind_m0software_detailindex;

       // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicenseview));
        _data_softwarelicenseview = callServiceSoftwareLicense(_urlGetm0SoftwareDetail, _data_softwarelicenseview);

        setFormData(FvViewDetail, FormViewMode.ReadOnly, _data_softwarelicenseview.bind_m0software_list);

        //setGridData(GvMaster, _data_softwarelicenseview.bind_m0software_list);

    }

    protected void SelectEditIndex()
    {

        data_softwarelicense _data_softwarelicenseview = new data_softwarelicense();
        _data_softwarelicenseview.bind_m0software_list = new bind_m0software_detail[1];

        bind_m0software_detail _bind_m0software_detailindex = new bind_m0software_detail();

        _bind_m0software_detailindex.software_idx = int.Parse(ViewState["software_idx_edit"].ToString());

        _data_softwarelicenseview.bind_m0software_list[0] = _bind_m0software_detailindex;

         //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicenseview));
        _data_softwarelicenseview = callServiceSoftwareLicense(_urlGetm0SoftwareDetail, _data_softwarelicenseview);

        setFormData(FvViewDetail, FormViewMode.Edit, _data_softwarelicenseview.bind_m0software_list);

        //setGridData(GvMaster, _data_softwarelicenseview.bind_m0software_list);

    }

    protected void SelectLogCountDept()
    {

        Repeater rptDetailSoftware = (Repeater)ViewDetail.FindControl("rptDetailSoftware");
       
        data_softwarelicense _data_softwarelicenselog = new data_softwarelicense();
        _data_softwarelicenselog.log_m0software_list = new log_m0software_detail[1];

        log_m0software_detail _log_m0software_detail = new log_m0software_detail();

        _log_m0software_detail.software_idx = int.Parse(ViewState["software_idx"].ToString());

        _data_softwarelicenselog.log_m0software_list[0] = _log_m0software_detail;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkview));

        _data_softwarelicenselog = callServiceSoftwareLicense(_urlGetlogcountDetailDept, _data_softwarelicenselog);
    
        rptDetailSoftware.DataSource = _data_softwarelicenselog.log_m0software_list;
        rptDetailSoftware.DataBind();



    }

    protected void SelectLogEditCountDept()
    {
       // FvViewDetail.ChangeMode(FormViewMode.Edit);


        FormView FvViewDetail = (FormView)ViewDetail.FindControl("FvViewDetail");
        GridView GvShowDetailDeptEdit = (GridView)FvViewDetail.FindControl("GvShowDetailDeptEdit");

        Repeater rptDetailSoftwareEdit = (Repeater)FvViewDetail.FindControl("rptDetailSoftwareEdit");

        data_softwarelicense _data_softwarelicenselog = new data_softwarelicense();
        _data_softwarelicenselog.log_m0software_list = new log_m0software_detail[1];

        log_m0software_detail _log_m0software_detail = new log_m0software_detail();

        _log_m0software_detail.software_idx = int.Parse(ViewState["software_idx_edit"].ToString());

        _data_softwarelicenselog.log_m0software_list[0] = _log_m0software_detail;
   
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_softwarelicenselog));

        _data_softwarelicenselog = callServiceSoftwareLicense(_urlGetlogcountDetailDept, _data_softwarelicenselog);


        //////setFormData(FvViewDetail, FormViewMode.ReadOnly, _data_networklog.lognetwork_list);
        //data_softwarelicense _data_softwarelicenselog2 = new data_softwarelicense();
        //_data_softwarelicenselog2 = (data_softwarelicense)_funcTool.convertJsonToObject(typeof(data_softwarelicense), "{\"data_softwarelicense\":{\"return_code\":\"0\",\"return_msg\":\"selectedok\",\"log_m0software_list\":[{\"software_idx\":\"13\",\"software_name\":\"PDF\",\"software_version\":\"1.1\",\"type_idx\":\"1\",\"date_purchase\":\"28/04/2017\",\"date_expire\":\"26/05/2017\",\"company_idx\":\"3\",\"cemp_idx\":\"1347\",\"create_date\":\"2017-04-28T10:50:24.367\",\"update_date\":\"2017-05-12T09:27:43.577\",\"software_status\":\"1\",\"company_name\":\"Microsoft\",\"mobile_no\":\"0837842029\",\"count_license\":\"15\",\"license_org_idx\":\"1\",\"OrgNameTH\":\"เถ้าแก่น้อยฟู๊ดแอนด์มาร์เก็ตติ้งจำกัดมหาชน\",\"license_rdept_idx\":\"19\",\"num_license_rdept_idx\":\"2\",\"DeptNameTH\":\"การตลาด\"},{\"software_idx\":\"13\",\"software_name\":\"PDF\",\"software_version\":\"1.1\",\"type_idx\":\"1\",\"date_purchase\":\"28/04/2017\",\"date_expire\":\"26/05/2017\",\"company_idx\":\"3\",\"cemp_idx\":\"1347\",\"create_date\":\"2017-04-28T10:50:24.367\",\"update_date\":\"2017-05-12T09:27:43.577\",\"software_status\":\"1\",\"company_name\":\"Microsoft\",\"mobile_no\":\"0837842029\",\"count_license\":\"15\",\"license_org_idx\":\"1\",\"OrgNameTH\":\"เถ้าแก่น้อยฟู๊ดแอนด์มาร์เก็ตติ้งจำกัดมหาชน\",\"license_rdept_idx\":\"20\",\"num_license_rdept_idx\":\"3\",\"DeptNameTH\":\"การจัดการระบบสารสนเทศ\"},{\"software_idx\":\"13\",\"software_name\":\"PDF\",\"software_version\":\"1.1\",\"type_idx\":\"1\",\"date_purchase\":\"28/04/2017\",\"date_expire\":\"26/05/2017\",\"company_idx\":\"3\",\"cemp_idx\":\"1347\",\"create_date\":\"2017-04-28T10:50:24.367\",\"update_date\":\"2017-05-12T09:27:43.577\",\"software_status\":\"1\",\"company_name\":\"Microsoft\",\"mobile_no\":\"0837842029\",\"count_license\":\"15\",\"license_org_idx\":\"3\",\"OrgNameTH\":\"เถ้าแก่น้อยเรสเตอรองท์แฟรนไชส์จำกัด\",\"license_rdept_idx\":\"93\",\"num_license_rdept_idx\":\"2\",\"DeptNameTH\":\"บริหาร\"},{\"software_idx\":\"13\",\"software_name\":\"PDF\",\"software_version\":\"1.1\",\"type_idx\":\"1\",\"date_purchase\":\"28/04/2017\",\"date_expire\":\"26/05/2017\",\"company_idx\":\"3\",\"cemp_idx\":\"1347\",\"create_date\":\"2017-04-28T10:50:24.367\",\"update_date\":\"2017-05-12T09:27:43.577\",\"software_status\":\"1\",\"company_name\":\"Microsoft\",\"mobile_no\":\"0837842029\",\"count_license\":\"15\",\"license_org_idx\":\"3\",\"OrgNameTH\":\"เถ้าแก่น้อยเรสเตอรองท์แฟรนไชส์จำกัด\",\"license_rdept_idx\":\"96\",\"num_license_rdept_idx\":\"3\",\"DeptNameTH\":\"ประสานงานขาย\"},{\"software_idx\":\"13\",\"software_name\":\"PDF\",\"software_version\":\"1.1\",\"type_idx\":\"1\",\"date_purchase\":\"28/04/2017\",\"date_expire\":\"26/05/2017\",\"company_idx\":\"3\",\"cemp_idx\":\"1347\",\"create_date\":\"2017-04-28T10:50:24.367\",\"update_date\":\"2017-05-12T09:27:43.577\",\"software_status\":\"1\",\"company_name\":\"Microsoft\",\"mobile_no\":\"0837842029\",\"count_license\":\"15\",\"license_org_idx\":\"3\",\"OrgNameTH\":\"เถ้าแก่น้อยเรสเตอรองท์แฟรนไชส์จำกัด\",\"license_rdept_idx\":\"103\",\"num_license_rdept_idx\":\"2\",\"DeptNameTH\":\"สาขาแพลตินัม\"},{\"software_idx\":\"13\",\"software_name\":\"PDF\",\"software_version\":\"1.1\",\"type_idx\":\"1\",\"date_purchase\":\"28/04/2017\",\"date_expire\":\"26/05/2017\",\"company_idx\":\"3\",\"cemp_idx\":\"1347\",\"create_date\":\"2017-04-28T10:50:24.367\",\"update_date\":\"2017-05-12T09:27:43.577\",\"software_status\":\"1\",\"company_name\":\"Microsoft\",\"mobile_no\":\"0837842029\",\"count_license\":\"15\",\"license_org_idx\":\"3\",\"OrgNameTH\":\"เถ้าแก่น้อยเรสเตอรองท์แฟรนไชส์จำกัด\",\"license_rdept_idx\":\"106\",\"num_license_rdept_idx\":\"2\",\"DeptNameTH\":\"เซ็นทรัลเฟสติวัลเชียงใหม่\"},{\"software_idx\":\"13\",\"software_name\":\"PDF\",\"software_version\":\"1.1\",\"type_idx\":\"1\",\"date_purchase\":\"28/04/2017\",\"date_expire\":\"26/05/2017\",\"company_idx\":\"3\",\"cemp_idx\":\"1347\",\"create_date\":\"2017-04-28T10:50:24.367\",\"update_date\":\"2017-05-12T09:27:43.577\",\"software_status\":\"1\",\"company_name\":\"Microsoft\",\"mobile_no\":\"0837842029\",\"count_license\":\"15\",\"license_org_idx\":\"5\",\"OrgNameTH\":\"เจนซีอินสไปร์\",\"license_rdept_idx\":\"87\",\"num_license_rdept_idx\":\"1\",\"DeptNameTH\":\"เลขานุการ\"}]}}");

        //litDebug.Text = _data_softwarelicenselog2.log_m0software_list[0].software_name;

        //GridView gvTest = (GridView)FvViewDetail.FindControl("gvTest");
        //gvTest.DataSource = _data_softwarelicenselog.log_m0software_list;
        //gvTest.DataBind();

        rptDetailSoftwareEdit.DataSource = _data_softwarelicenselog.log_m0software_list;
        rptDetailSoftwareEdit.DataBind();

        //// ViewState["data_bind_dept"] = _data_softwarelicenselog.log_m0software_list;

        DataTable dt = new DataTable();
        //////DataSet ds = new DataSet();

        DataRow drow;

        dt.Columns.Add("OrgNameTH", typeof(string));
        dt.Columns.Add("DeptNameTH", typeof(string));
        dt.Columns.Add("num_license_rdept_idx", typeof(string));

        int i = 0;
        foreach (var item in _data_softwarelicenselog.log_m0software_list)
        {

            drow = dt.NewRow();
            dt.Rows.Add(drow);


            dt.Rows[i]["OrgNameTH"] = item.OrgNameTH;//item.num_license_rdept_idx;// i.ToString();
            dt.Rows[i]["DeptNameTH"] = item.DeptNameTH;//i.ToString();//item.software_idx;
            dt.Rows[i]["num_license_rdept_idx"] = item.num_license_rdept_idx;
            //dt.Rows[i].BeginEdit();
            //dt.Rows[i].Delete();


            //dt.Rows[i].Delete();


            //dt.Rows[i][col3] = String.Format("{0:C}", item.Price);
            //dt.Rows[i][col4] = String.Format("{0:.00}", item.Price);


            i++;
        }



        ViewState["data_bind_deptdetail"] = dt;


    }

    protected void ddlCompany()
    {

        DropDownList ddlcompany_idx = (DropDownList)ViewInsert.FindControl("ddlcompany_idx");

        ddlcompany_idx.Items.Clear();
        ddlcompany_idx.AppendDataBoundItems = true;
        ddlcompany_idx.Items.Add(new ListItem("กรุณาเลือกบริษัท ...", "00"));


        data_softwarelicense _data_softwarelicenseddlcom = new data_softwarelicense();
        _data_softwarelicenseddlcom.m0_itswl_company_list = new m0_itswl_company_detail[1];
        m0_itswl_company_detail _m0_itswl_company_detail = new m0_itswl_company_detail();

        _data_softwarelicenseddlcom.m0_itswl_company_list[0] = _m0_itswl_company_detail;

        ////litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense));

        _data_softwarelicenseddlcom = callServiceSoftwareLicense(_urlGetddlCompanySoftware, _data_softwarelicenseddlcom);

        ddlcompany_idx.DataSource = _data_softwarelicenseddlcom.m0_itswl_company_list;
        ddlcompany_idx.DataTextField = "company_name";
        ddlcompany_idx.DataValueField = "company_idx";
        ddlcompany_idx.DataBind();


    }

    protected void ddlSoftware()
    {

        DropDownList ddlsoftware_nameinsert = (DropDownList)ViewInsert.FindControl("ddlsoftware_nameinsert");

        ddlsoftware_nameinsert.Items.Clear();
        ddlsoftware_nameinsert.AppendDataBoundItems = true;
        ddlsoftware_nameinsert.Items.Add(new ListItem("กรุณาเลือก Software ...", "00"));

        data_softwarelicense _data_ddl_sw = new data_softwarelicense();
        _data_ddl_sw.softwarename_list = new softwarename_detail[1];
        softwarename_detail _softwarename_detailddl = new softwarename_detail();

        _data_ddl_sw.softwarename_list[0] = _softwarename_detailddl;

        ////litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense));

        _data_ddl_sw = callServiceSoftwareLicense(_urlGetddlSofware, _data_ddl_sw);

        //ViewState["type_idx"] = _data_ddl_sw.softwarename_list[0].software_type;

        ddlsoftware_nameinsert.DataSource = _data_ddl_sw.softwarename_list;
        ddlsoftware_nameinsert.DataTextField = "software_name";
        ddlsoftware_nameinsert.DataValueField = "software_name_idx";
        ddlsoftware_nameinsert.DataBind();

        
    }

    protected void ddlOrganization()
    {

        Panel PaneldetailLicense = (Panel)ViewInsert.FindControl("PaneldetailLicense");
        DropDownList ddlorg_idxinsert = (DropDownList)PaneldetailLicense.FindControl("ddlorg_idxinsert");

        ddlorg_idxinsert.Items.Clear();
        ddlorg_idxinsert.AppendDataBoundItems = true;
        ddlorg_idxinsert.Items.Add(new ListItem("กรุณาเลือกองค์กร ...", "00"));

        _data_softwarelicense.organization_list = new organization_detail[1];
        organization_detail _organization_detail = new organization_detail();

        //_permissionDetail.OrgIDX = 0;

        _data_softwarelicense.organization_list[0] = _organization_detail;


        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_permission));
        _data_softwarelicense = callServiceSoftwareLicense(_urlGetddlOrganizationSW, _data_softwarelicense);

        ddlorg_idxinsert.DataSource = _data_softwarelicense.organization_list;
        ddlorg_idxinsert.DataTextField = "OrgNameTH";
        ddlorg_idxinsert.DataValueField = "OrgIDX";
        ddlorg_idxinsert.DataBind();


    }

  
    #endregion Selected 


    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
     
        int _cemp_idx;
        int _software_idx_del;

        m0company_detail _m0company_detail = new m0company_detail();
        m0software_detail _m0software_detail = new m0software_detail();
        m0software_dataset_detail _m0software_dataset_detail = new m0software_dataset_detail();
        m0software_datasetedit_detail _m0software_datasetedit_detail = new m0software_datasetedit_detail();

        switch (cmdName)
        {

            case "btnToInsert":
                MvMaster.SetActiveView(ViewInsert);
                ddlSoftware();
                ddlCompany();
                ddlOrganization();

               


                break;

            case "btnBack":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "btnCancel":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "btnInsertDetail":

                //TextBox _txtsoftware_nameinsert = (TextBox)ViewInsert.FindControl("txtsoftware_nameinsert");

                //TextBox _Count_numin_colum = (TextBox)ViewInsert.FindControl("Count_numin_colum");
                DropDownList _ddlsoftware_nameinsert = (DropDownList)ViewInsert.FindControl("ddlsoftware_nameinsert");


                Panel PaneldetailLicense = (Panel)ViewInsert.FindControl("PaneldetailLicense");
                DropDownList _ddlorg_idxinsert = (DropDownList)PaneldetailLicense.FindControl("ddlorg_idxinsert");
                DropDownList _ddlrdept_idxinsert = (DropDownList)PaneldetailLicense.FindControl("ddlrdept_idxinsert");

                TextBox _txtassetcode_dept = (TextBox)PaneldetailLicense.FindControl("txtassetcode_dept"); // เลขที่ asset

                TextBox _txtnumlicense_dept = (TextBox)PaneldetailLicense.FindControl("txtnumlicense_dept");

                Panel gidviewdetaillicense = (Panel)PaneldetailLicense.FindControl("gidviewdetaillicense");
                GridView GVShowdetailLicense = (GridView)gidviewdetaillicense.FindControl("GVShowdetailLicense");


                //
               // _txtnumlicense_dept.Text = "1";

                //DropDownList _ddlcompanyidxma = (DropDownList)FvViewMA.FindControl("ddlcompanyidxma");

                //////  create data set show detail license  ////////

                var dsEquiment = (DataSet)ViewState["vsBuyequipment"];
                var drEquiment = dsEquiment.Tables[0].NewRow();

                int numrow = dsEquiment.Tables[0].Rows.Count; //จำนวนแถว

                ViewState["_ddlorg_idxinsert_check"] = _ddlorg_idxinsert.SelectedValue;
                ViewState["_ddlrdept_idxinsert_check"] = _ddlrdept_idxinsert.SelectedValue;
                ViewState["_asset_check"] = _txtassetcode_dept.Text;

                if (numrow > 0)
                {
                    foreach (DataRow check in dsEquiment.Tables[0].Rows)
                    {
                        ViewState["check_org"] = check["OrgIDX_dataset"];
                        ViewState["check_dept"] = check["RDeptIDX_dataset"];

                        ViewState["check_asset"] = check["Asset_dataset"];


                        //if((int.Parse(ViewState["_ddlorg_idxinsert_check"].ToString()) == int.Parse(ViewState["check_org"].ToString())) && (int.Parse(ViewState["_ddlrdept_idxinsert_check"].ToString()) == int.Parse(ViewState["check_dept"].ToString())))

                        //if (int.Parse(ViewState["_ddlorg_idxinsert_check"].ToString()) == int.Parse(ViewState["ch"].ToString()))
                        if (ViewState["_asset_check"].ToString() == ViewState["check_asset"].ToString())
                        {
                            ViewState["CheckDataset"] = "0";
                            break;

                        }
                        else
                        {
                            ViewState["CheckDataset"] = "1";

                        }
                    }

                    if (ViewState["CheckDataset"].ToString() == "1")
                    {
                        drEquiment["Software_License_dataset"] = _ddlsoftware_nameinsert.SelectedItem;
                        drEquiment["Softwareidx_License_dataset"] = _ddlsoftware_nameinsert.SelectedValue;

                        drEquiment["OrgNameTH_dataset"] = _ddlorg_idxinsert.SelectedItem;
                        drEquiment["OrgIDX_dataset"] = _ddlorg_idxinsert.SelectedValue;

                        drEquiment["DeptNameTH_dataset"] = _ddlrdept_idxinsert.SelectedItem;
                        drEquiment["RDeptIDX_dataset"] = _ddlrdept_idxinsert.SelectedValue;

                        drEquiment["Asset_dataset"] = _txtassetcode_dept.Text;

                        drEquiment["numlicense_dataset"] = int.Parse(_txtnumlicense_dept.Text);

                        dsEquiment.Tables[0].Rows.Add(drEquiment);
                        ViewState["vsBuyequipment"] = dsEquiment;

                        //////  create data set show detail license  ////////


                        gidviewdetaillicense.Visible = true;

                        GVShowdetailLicense.DataSource = dsEquiment.Tables[0];
                        GVShowdetailLicense.DataBind();

                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลอยู่แล้ว');", true);
                        //break;
                    }

                }
                else
                {
                    drEquiment["Software_License_dataset"] = _ddlsoftware_nameinsert.SelectedItem;
                    drEquiment["Softwareidx_License_dataset"] = _ddlsoftware_nameinsert.SelectedValue;

                    drEquiment["OrgNameTH_dataset"] = _ddlorg_idxinsert.SelectedItem;
                    drEquiment["OrgIDX_dataset"] = _ddlorg_idxinsert.SelectedValue;

                    drEquiment["DeptNameTH_dataset"] = _ddlrdept_idxinsert.SelectedItem;
                    drEquiment["RDeptIDX_dataset"] = _ddlrdept_idxinsert.SelectedValue;

                    drEquiment["Asset_dataset"] = _txtassetcode_dept.Text;

                    drEquiment["numlicense_dataset"] = int.Parse(_txtnumlicense_dept.Text);

                    dsEquiment.Tables[0].Rows.Add(drEquiment);
                    ViewState["vsBuyequipment"] = dsEquiment;

                    //////  create data set show detail license  ////////


                    gidviewdetaillicense.Visible = true;

                    GVShowdetailLicense.DataSource = dsEquiment.Tables[0];
                    GVShowdetailLicense.DataBind();

                }

              
                break;

            case "btnInsert":

                TextBox _txtlicense_idxall = (TextBox)ViewInsert.FindControl("txtlicense_idx"); //จำนวน License ทั้งหมด
                TextBox _Count_numin_colum_test = (TextBox)ViewInsert.FindControl("Count_numin_colum");
                CheckBox chk_license = (CheckBox)ViewInsert.FindControl("chk_license");


                //TextBox _txtasset_codeinsertadd = (TextBox)ViewInsert.FindControl("txtasset_codeinsert");
                TextBox _txtpo_codeinsertadd = (TextBox)ViewInsert.FindControl("txtpo_codeinsert");
                TextBox _lbtype_idxinsertadd = (TextBox)ViewInsert.FindControl("lbtype_idx");

                //TextBox _txtsoftware_nameadd = (TextBox)ViewInsert.FindControl("txtsoftware_nameinsert");

                DropDownList ddlsoftware_nameinsert = (DropDownList)ViewInsert.FindControl("ddlsoftware_nameinsert");
                TextBox _txtsoftware_versionadd = (TextBox)ViewInsert.FindControl("txtsoftware_versioninsert");
                TextBox _txtdatepurchaseadd = (TextBox)ViewInsert.FindControl("txtdatepurchaseinsert");
                TextBox _txtdateexpireadd = (TextBox)ViewInsert.FindControl("txtdateexpireinsert");
                DropDownList _ddlcompany_idxadd = (DropDownList)ViewInsert.FindControl("ddlcompany_idx");
                TextBox txtsoftware_price = (TextBox)ViewInsert.FindControl("txtsoftware_price");

                //DropDownList _ddlorg_idxadd = (DropDownList)ViewInsert.FindControl("ddlorg_idxinsert");               

                //CheckText.Visible = false;
                    showdetailpermissionsystem.Visible = false;

                    var dsBuyequipment = (DataSet)ViewState["vsBuyequipment"];

                    var m0_software_dataset = new m0software_dataset_detail[dsBuyequipment.Tables[0].Rows.Count];

                    var SumTotalLinse = dsBuyequipment.Tables[0].Rows.Count;

                    // Check จำนวน License ตอน insert
                    int i = 0;
                    foreach (DataRow dr in dsBuyequipment.Tables[0].Rows)
                    {

                        //AddPur1[i] = new SetADDFormList();
                        //AddPur1[i].R0_Name = dr["topic_name_id"].ToString();
                        i += int.Parse(dr["numlicense_dataset"].ToString());
                        //i += int.Parse(dr["numlicense_dataset"].ToString());
                        //AddPur1[i].R0_Status = int.Parse(dr["name_id"].ToString());

                    }

                    _Count_numin_colum_test.Text = i.ToString(); // จำนวนรวมของ License ที่เพิ่มตรง dataset

                    //Check องค์กร ฝ่าย ตอนแบ่ง License
                    int dataset = 0;
                    foreach (DataRow drw in dsBuyequipment.Tables[0].Rows)
                    {


                        m0_software_dataset[dataset] = new m0software_dataset_detail();
                        //AddPur1[i].R0_Name = dr["topic_name_id"].ToString();
                        m0_software_dataset[dataset].org_idx = int.Parse(drw["OrgIDX_dataset"].ToString());
                        m0_software_dataset[dataset].rdept_idx = int.Parse(drw["RDeptIDX_dataset"].ToString());
                        m0_software_dataset[dataset].asset_code = drw["Asset_dataset"].ToString();
                        m0_software_dataset[dataset].num_license = int.Parse(drw["numlicense_dataset"].ToString());
                        //AddPur1[i].R0_Status = int.Parse(dr["name_id"].ToString());


                        dataset++;

                    }

                if (chk_license.Checked)
                {

                    //if (dsBuyequipment.Tables[0].Rows.Count == 0)
                    //{
                    //    m0_software_dataset = null;
                    //}
                    //else
                    //{
                        ////// Check License ว่า ใส่ครบไหม  ///////
                        if (_txtlicense_idxall.Text == _Count_numin_colum_test.Text)
                        {
                            //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ok!!!');", true);


                            //_company_name = ((TextBox)ViewInsert.FindControl("txtCompanyName")).Text.Trim();
                            // DropDownList ddlCompanyStatus = (DropDownList)ViewInsert.FindControl("ddlCompanyStatus");
                            _cemp_idx = emp_idx;

                            data_softwarelicense _dataset_software = new data_softwarelicense();
                            _dataset_software.m0software_list = new m0software_detail[1];

                         
                            _m0software_detail.software_name_idx = int.Parse(ddlsoftware_nameinsert.SelectedValue);
                            //_m0software_detail.software_idx = int.Parse(ddlsoftware_nameinbtnEditindexsert.SelectedValue);
                            //_m0software_detail.asset_code = _txtasset_codeinsertadd.Text;
                            _m0software_detail.po_code = _txtpo_codeinsertadd.Text;
                            //_m0software_detail.software_name = _txtsoftware_nameadd.Text;
                            //_m0software_detail.software_version = _txtsoftware_versionadd.Text;
                            _m0software_detail.date_purchase = _txtdatepurchaseadd.Text;//

                            if(_lbtype_idxinsertadd.Text == "2") // software ผูกขาด
                            {
                                _m0software_detail.date_expire = "14/05/2100";//_txtdateexpireadd.Text;
                            }
                            else
                            {
                                _m0software_detail.date_expire = _txtdateexpireadd.Text;
                            }
                          
                            _m0software_detail.company_idx = int.Parse(_ddlcompany_idxadd.SelectedValue);
                            _m0software_detail.num_license = int.Parse(_txtlicense_idxall.Text); // จำนวน License ทั้งหมด
                            _m0software_detail.software_price = int.Parse(txtsoftware_price.Text); // ราคา Software
                            _m0software_detail.cemp_idx = _cemp_idx;

                            _dataset_software.m0software_list[0] = _m0software_detail;

                            _dataset_software.m0software_dataset_list = m0_software_dataset;

                        //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataset_software));

                        _dataset_software = callServiceSoftwareLicense(_urlSetm0Software, _dataset_software);

                        if (_dataset_software.return_code == 0)
                        {

                            ////actionIndex();
                            //MvMaster.SetActiveView(ViewIndex);
                            Page.Response.Redirect(Page.Request.Url.ToString(), true);


                        }
                        else
                        {
                            setError(_dataset_software.return_code.ToString() + " - " + _dataset_software.return_msg);
                        }

                    }
                    else if (int.Parse(_Count_numin_colum_test.Text) > int.Parse(_txtlicense_idxall.Text))
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('จำนวน License ที่แบ่งเกิน จำนวน License ทั้งหมดที่มี!!!');", true);
                        }
                        else if (int.Parse(_Count_numin_colum_test.Text) < int.Parse(_txtlicense_idxall.Text))
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('จำนวน License ที่แบ่งน้อยกว่า จำนวน License ทั้งหมดที่มี!!!');", true);
                        }
                        ////// Check License ว่า ใส่ครบไหม  ///////
                    //}
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาแบ่ง Lisense ตามฝ่าย!!!');", true);

                    showdetailpermissionsystem.Visible = true;
                    //CheckText.Visible = true;

                }

                //actionCreate();
                //Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "btnViewindex":

                string[] arg1 = new string[1];
                arg1 = e.CommandArgument.ToString().Split(';');
                int software_idx = int.Parse(arg1[0]);

                ViewState["software_idx"] = software_idx;


                MvMaster.SetActiveView(ViewDetail);

                gridviewindex.Visible = false;
                btnToInsert.Visible = false;

                panel_detail_software.Visible = true;

                SelectViewIndex();
                SelectLogCountDept();



                break;

            case "btnBackIndex":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "btnEditindex":

                string[] arg2 = new string[1];
                arg2 = e.CommandArgument.ToString().Split(';');
                int software_idx_edit = int.Parse(arg2[0]);

                ViewState["software_idx_edit"] = software_idx_edit;


                MvMaster.SetActiveView(ViewDetail);
                //panel_detail_softwareedit.Visible = true;

                gridviewindex.Visible = false;
                btnToInsert.Visible = false;

                btnBackIndex.Visible = true;
                show_btnedit.Visible = true;

                SelectEditIndex();
                SelectLogEditCountDept();


                //var data_bind = (DataSet)ViewState["data_bind_dept"];
                //var m0_software_dataset_bind = new log_m0software_detail[data_bind.Tables[0].Rows.Count];

                ////var drEquiment_ = data_bind.Tables[0].NewRow();
                //int r = 0;
                //foreach (DataRow dr_bind in data_bind.Tables[0].Rows)
                //{

                //    //AddPur1[i] = new SetADDFormList();
                //    //AddPur1[i].R0_Name = dr["topic_name_id"].ToString();
                //    //m0_software_dataset_bind[r] = new log_m0software_detail();


                //    m0_software_dataset_bind[r].software_name = dr_bind["Software_License_dataset_bind"].ToString();

                //    m0_software_dataset_bind[r].org_idx = int.Parse(dr_bind["OrgIDX_dataset_bind"].ToString());



                //    //ds_bind.Tables[0].Columns.Add("Software_License_dataset_bind", typeof(String));
                //    ////ds_bind.Tables[0].Columns.Add("OrgNameTH_dataset", typeof(String));
                //    //ds_bind.Tables[0].Columns.Add("OrgIDX_dataset_bind", typeof(int));

                //    //ds_bind.Tables[0].Columns.Add("Software_License_dataset", typeof(String));
                //    //ds_bind.Tables[0].Columns.Add("OrgNameTH_dataset", typeof(String));

                //    // i += int.Parse(data_bind["Software_License_dataset"].ToString());
                //    //i += int.Parse(dr["numlicense_dataset"].ToString());
                //    //AddPur1[i].R0_Status = int.Parse(dr["name_id"].ToString());
                //    data_bind.Tables[0].Rows.Add(m0_software_dataset_bind);
                //    ViewState["data_bind_dept0"] = data_bind;
                //    r++;
                //}

                // testbind.Text = ViewState["data_bind_dept0"].ToString();

                //DataSet data_bind = new DataSet();


                break;

            case "btn_Canceledit":

                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "btn_Saveedit":

                FormView FvViewDetail = (FormView)ViewDetail.FindControl("FvViewDetail");
                Label _hf_software_idx = (Label)FvViewDetail.FindControl("hf_software_idx");
                TextBox _txtsoftware_nameviewedit = (TextBox)FvViewDetail.FindControl("txtsoftware_nameview");
                TextBox _txtsoftware_versionviewedit = (TextBox)FvViewDetail.FindControl("txtsoftware_versionview");
                TextBox _txtdate_purchaseviewedit = (TextBox)FvViewDetail.FindControl("txtdate_purchaseview");
                TextBox _txtdate_expireviewedit = (TextBox)FvViewDetail.FindControl("txtdate_expireview");
                DropDownList _ddlcompany_nameedit = (DropDownList)FvViewDetail.FindControl("ddlcompany_nameedit");

                TextBox _txtnum_licenseviewedit = (TextBox)FvViewDetail.FindControl("txtnum_licenseview"); // จำนวน License ทั้งหมด
                //TextBox _txtasset_editviewedit = (TextBox)FvViewDetail.FindControl("txtasset_edit"); // 

                CheckBox chk_licenseedit = (CheckBox)FvViewDetail.FindControl("chk_licenseedit"); // จำนวน License ทั้งหมด
                TextBox Count_numin_columEdit = (TextBox)FvViewDetail.FindControl("Count_numin_columEdit"); // จำนวน License ทั้งหมด

                Repeater rptDetailSoftwareEdit = (Repeater)FvViewDetail.FindControl("rptDetailSoftwareEdit");
                //DropDownList _ddlorg_idxedit = (DropDownList)rptDetailSoftwareEdit.Items[1].FindControl("ddlorg_idxedit");
                //DropDownList _ddlrdept_idxedit = (DropDownList)rptDetailSoftwareEdit.Items[2].FindControl("ddlrdept_idxedit");
                //TextBox _txtnum_licenseedit = (TextBox)rptDetailSoftwareEdit.Items[3].FindControl("txtnum_licenseedit");


                var dsBuyequipment_edit = (DataSet)ViewState["vsBuyequipmentEdit"];

                var m0_software_editdataset = new m0software_datasetedit_detail[dsBuyequipment_edit.Tables[0].Rows.Count];  //datatable

                var SumTotalLinseEdit = dsBuyequipment_edit.Tables[0].Rows.Count;

                // Check จำนวน License ตอน insert
                if(chk_licenseedit.Checked) // กรณีเช็คแบ่งจำนวน License ข้างบน
                {
                    int s = 0;
                    foreach (DataRow dr in dsBuyequipment_edit.Tables[0].Rows)
                    {

                        //AddPur1[i] = new SetADDFormList();
                        //AddPur1[i].R0_Name = dr["topic_name_id"].ToString();
                        s += int.Parse(dr["numlicenseEdit_dataset"].ToString());
                        //i += int.Parse(dr["numlicense_dataset"].ToString());
                        //AddPur1[i].R0_Status = int.Parse(dr["name_id"].ToString());

                    }
                    Count_numin_columEdit.Text = s.ToString();//(int.Parse(s.ToString()) + int.Parse(_txtnum_licenseviewedit.Text)).ToString(); // จำนวนรวมของ License ที่เพิ่มตรง dataset ข้างบน

                    ViewState["Count_numin_columEdit"] = Count_numin_columEdit.Text; // ค่าจำนวน License รวม

                    //Check องค์กร ฝ่าย ตอนแบ่ง License
                    int dataset_edit = 0;
                    foreach (DataRow drw in dsBuyequipment_edit.Tables[0].Rows)
                    {


                        m0_software_editdataset[dataset_edit] = new m0software_datasetedit_detail();
                        //AddPur1[i].R0_Name = dr["topic_name_id"].ToString();
                        m0_software_editdataset[dataset_edit].org_idx = int.Parse(drw["OrgIDXEdit_dataset"].ToString());
                        m0_software_editdataset[dataset_edit].rdept_idx = int.Parse(drw["RDeptIDXEdit_dataset"].ToString());
                        m0_software_editdataset[dataset_edit].num_license = int.Parse(drw["numlicenseEdit_dataset"].ToString());
                        //AddPur1[i].R0_Status = int.Parse(dr["name_id"].ToString());


                        dataset_edit++;

                    }



                    ///////////////// Edit ข้างล่าง ///////////// repeater
                    var m0_software_dataset_edit = new m0software_dataset_detail[rptDetailSoftwareEdit.Items.Count];


                    // Check จำนวน License ทั้งหมดที่แบ่งตามฝ่าย
                    int k = 0;
                    for (int j = 0; j < rptDetailSoftwareEdit.Items.Count; j++)
                    {

                        DropDownList _ddlorg_idxedit = rptDetailSoftwareEdit.Items[j].FindControl("ddlorg_idxedit") as DropDownList;
                        DropDownList _ddlrdept_idxedit = rptDetailSoftwareEdit.Items[j].FindControl("ddlrdept_idxedit") as DropDownList;
                        TextBox _txtnum_licenseedit = rptDetailSoftwareEdit.Items[j].FindControl("txtnum_licenseedit") as TextBox;

                        k += int.Parse(_txtnum_licenseedit.Text);

                    }

                    SumLicenseEdit.Text = k.ToString();   // ผลรวมจำนวน License ทั้งหมด ตามแต่ละฝ่าย ข้างล่าง

                    for (int l = 0; l < rptDetailSoftwareEdit.Items.Count;)
                    {


                        m0_software_dataset_edit[l] = new m0software_dataset_detail();

                        DropDownList _ddlorg_idxedit = rptDetailSoftwareEdit.Items[l].FindControl("ddlorg_idxedit") as DropDownList;
                        DropDownList _ddlrdept_idxedit = rptDetailSoftwareEdit.Items[l].FindControl("ddlrdept_idxedit") as DropDownList;
                        TextBox _txtnum_licenseedit = rptDetailSoftwareEdit.Items[l].FindControl("txtnum_licenseedit") as TextBox;

                        m0_software_dataset_edit[l].software_idx = int.Parse(_hf_software_idx.Text);
                        m0_software_dataset_edit[l].org_idx = int.Parse(_ddlorg_idxedit.SelectedValue); //int.Parse(drw["OrgIDX_dataset"].ToString());
                        m0_software_dataset_edit[l].rdept_idx = int.Parse(_ddlrdept_idxedit.SelectedValue);//int.Parse(drw["RDeptIDX_dataset"].ToString());
                        m0_software_dataset_edit[l].num_license = int.Parse(_txtnum_licenseedit.Text); //int.Parse(drw["numlicense_dataset"].ToString());

                        l++;

                      

                    }

                    if (_txtnum_licenseviewedit.Text == ((int.Parse(SumLicenseEdit.Text) + int.Parse(Count_numin_columEdit.Text)).ToString())) // กรณ๊ License ทั้งหมด เท่ากับ ผลรวมจำนวน License ที่แบ่งตามฝ่าย
                    {

                        _cemp_idx = emp_idx;

                        data_softwarelicense _data_software = new data_softwarelicense();
                        _data_software.m0software_list = new m0software_detail[1];


                        _m0software_detail.software_idx = int.Parse(_hf_software_idx.Text);
                        _m0software_detail.software_name = _txtsoftware_nameviewedit.Text;
                        _m0software_detail.software_version = _txtsoftware_versionviewedit.Text;
                        _m0software_detail.date_purchase = _txtdate_purchaseviewedit.Text;//
                        _m0software_detail.date_expire = _txtdate_expireviewedit.Text;
                        _m0software_detail.company_idx = int.Parse(_ddlcompany_nameedit.SelectedValue);
                        _m0software_detail.num_license = int.Parse(_txtnum_licenseviewedit.Text);
                        _m0software_detail.cemp_idx = _cemp_idx;
                        _m0software_detail.check_checkbox = 1;

                        _data_software.m0software_list[0] = _m0software_detail;

                        //_dataset_software.m0software_dataset_list = m0_software_dataset;

                        ////litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_software));

                        //_dataset_software = callServiceSoftwareLicense(_urlSetm0Software, _dataset_software);


                        _data_software.m0software_dataset_list = m0_software_dataset_edit;

                        _data_software.m0software_datasetedit_list = m0_software_editdataset;

                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_software));

                        _data_software = callServiceSoftwareLicense(_urlSetm0Software, _data_software);

                        if (_data_software.return_code == 0)
                        {

                            ////actionIndex();
                            //MvMaster.SetActiveView(ViewIndex);
                            Page.Response.Redirect(Page.Request.Url.ToString(), true);


                        }
                        else
                        {
                            setError(_data_software.return_code.ToString() + " - " + _data_software.return_msg);
                        }


                    }
                    else if (( int.Parse(SumLicenseEdit.Text) + int.Parse(Count_numin_columEdit.Text)) > int.Parse(_txtnum_licenseviewedit.Text))
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('จำนวน License ที่แบ่งเกิน จำนวน License ทั้งหมดที่มี!!!');", true);
                    }
                    else if ((int.Parse(SumLicenseEdit.Text) + int.Parse(Count_numin_columEdit.Text)) < int.Parse(_txtnum_licenseviewedit.Text))
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('จำนวน License ที่แบ่งน้อยกว่า จำนวน License ทั้งหมดที่มี!!!');", true);
                    }




                }
                else // กรณีไม่เช็คแบ่งจำนวน License ข้างบน
                {
                    Count_numin_columEdit.Text = "No Check"; // จำนวนรวมของ License ที่เพิ่มตรง dataset
                    ViewState["Count_numin_columEdit"] = "0";  // ค่าจำนวน License รวม

                    ///////////////// Edit ข้างล่าง /////////////
                    var m0_software_dataset_edit = new m0software_dataset_detail[rptDetailSoftwareEdit.Items.Count];


                    // Check จำนวน License ทั้งหมดที่แบ่งตามฝ่าย
                    int k = 0;
                    for (int j = 0; j < rptDetailSoftwareEdit.Items.Count; j++)
                    {

                        DropDownList _ddlorg_idxedit = rptDetailSoftwareEdit.Items[j].FindControl("ddlorg_idxedit") as DropDownList;
                        DropDownList _ddlrdept_idxedit = rptDetailSoftwareEdit.Items[j].FindControl("ddlrdept_idxedit") as DropDownList;
                        TextBox _txtnum_licenseedit = rptDetailSoftwareEdit.Items[j].FindControl("txtnum_licenseedit") as TextBox;
                        TextBox _txtasset_edit = rptDetailSoftwareEdit.Items[j].FindControl("txtasset_edit") as TextBox;

                        k += int.Parse(_txtnum_licenseedit.Text);

                    }

                    SumLicenseEdit.Text = k.ToString();   // ผลรวมจำนวน License ทั้งหมด ตามแต่ละฝ่าย

                    for (int l = 0; l < rptDetailSoftwareEdit.Items.Count;)
                    {


                        m0_software_dataset_edit[l] = new m0software_dataset_detail();

                        DropDownList _ddlorg_idxedit = rptDetailSoftwareEdit.Items[l].FindControl("ddlorg_idxedit") as DropDownList;
                        DropDownList _ddlrdept_idxedit = rptDetailSoftwareEdit.Items[l].FindControl("ddlrdept_idxedit") as DropDownList;
                        TextBox _txtnum_licenseedit = rptDetailSoftwareEdit.Items[l].FindControl("txtnum_licenseedit") as TextBox;
                        TextBox _txtasset_edit = rptDetailSoftwareEdit.Items[l].FindControl("txtasset_edit") as TextBox;

                        m0_software_dataset_edit[l].software_idx = int.Parse(_hf_software_idx.Text);
                        m0_software_dataset_edit[l].org_idx = int.Parse(_ddlorg_idxedit.SelectedValue); //int.Parse(drw["OrgIDX_dataset"].ToString());
                        m0_software_dataset_edit[l].rdept_idx = int.Parse(_ddlrdept_idxedit.SelectedValue);//int.Parse(drw["RDeptIDX_dataset"].ToString());
                        m0_software_dataset_edit[l].num_license = int.Parse(_txtnum_licenseedit.Text); //int.Parse(drw["numlicense_dataset"].ToString());
                        m0_software_dataset_edit[l].asset_code = _txtasset_edit.Text; //int.Parse(drw["numlicense_dataset"].ToString());

                        l++;

                      


                    }

                    if (_txtnum_licenseviewedit.Text == SumLicenseEdit.Text) // กรณ๊ License ทั้งหมด เท่ากับ ผลรวมจำนวน License ที่แบ่งตามฝ่าย
                    {

                        _cemp_idx = emp_idx;

                        data_softwarelicense _data_software = new data_softwarelicense();
                        _data_software.m0software_list = new m0software_detail[1];


                        _m0software_detail.software_idx = int.Parse(_hf_software_idx.Text);
                        _m0software_detail.software_name = _txtsoftware_nameviewedit.Text;
                        _m0software_detail.software_version = _txtsoftware_versionviewedit.Text;
                        _m0software_detail.date_purchase = _txtdate_purchaseviewedit.Text;//
                        _m0software_detail.date_expire = _txtdate_expireviewedit.Text;
                        _m0software_detail.company_idx = int.Parse(_ddlcompany_nameedit.SelectedValue);
                        _m0software_detail.num_license = int.Parse(_txtnum_licenseviewedit.Text);
                        //_m0software_detail.asset_code = _txtasset_editviewedit.Text;
                        _m0software_detail.cemp_idx = _cemp_idx;
                        _m0software_detail.check_checkbox = 0;

                        _data_software.m0software_list[0] = _m0software_detail;

                        //_dataset_software.m0software_dataset_list = m0_software_dataset;

                        ////litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_software));

                        //_dataset_software = callServiceSoftwareLicense(_urlSetm0Software, _dataset_software);


                        _data_software.m0software_dataset_list = m0_software_dataset_edit;

                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_software));

                        _data_software = callServiceSoftwareLicense(_urlSetm0Software, _data_software);

                        if (_data_software.return_code == 0)
                        {

                            //actionIndex();
                            MvMaster.SetActiveView(ViewIndex);
                            Page.Response.Redirect(Page.Request.Url.ToString(), true);


                        }
                        else
                        {
                            setError(_data_software.return_code.ToString() + " - " + _data_software.return_msg);
                        }

                    }
                    else if (int.Parse(SumLicenseEdit.Text) > int.Parse(_txtnum_licenseviewedit.Text))
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('จำนวน License ที่แบ่งเกิน จำนวน License ทั้งหมดที่มี!!!');", true);
                    }
                    else if (int.Parse(SumLicenseEdit.Text) < int.Parse(_txtnum_licenseviewedit.Text))
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('จำนวน License ที่แบ่งน้อยกว่า จำนวน License ทั้งหมดที่มี!!!');", true);
                    }


                }
                          
              
                //Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "btnEditDetail":

                FormView FvViewDetailEdit = (FormView)ViewDetail.FindControl("FvViewDetail");               
                GridView GvShowDetailDeptEdit = (GridView)FvViewDetailEdit.FindControl("GvShowDetailDeptEdit");

                FvViewDetailEdit.ChangeMode(FormViewMode.Edit);

                //TextBox _txtsoftware_nameinsert = (TextBox)ViewInsert.FindControl("txtsoftware_nameinsert");
                //TextBox _Count_numin_colum = (TextBox)ViewInsert.FindControl("Count_numin_colum");

                Panel PaneldetailLicenseEdit = (Panel)FvViewDetailEdit.FindControl("PaneldetailLicenseEdit");
                Label hf_software_idx = (Label)FvViewDetailEdit.FindControl("hf_software_idx");

                DropDownList _ddlorg_idxeditlicense = (DropDownList)PaneldetailLicenseEdit.FindControl("ddlorg_idxeditlicense");
                DropDownList _ddlrdept_idxeditlicense = (DropDownList)PaneldetailLicenseEdit.FindControl("ddlrdept_idxeditlicense");
                TextBox _txtnumlicense_depteditlicense = (TextBox)PaneldetailLicenseEdit.FindControl("txtnumlicense_depteditlicense");

                Panel gidviewdetaileditlicense = (Panel)PaneldetailLicenseEdit.FindControl("gidviewdetaileditlicense");
                GridView GVShowEditdetailLicense = (GridView)gidviewdetaileditlicense.FindControl("GVShowEditdetailLicense");


                //DropDownList _ddlcompanyidxma = (DropDownList)FvViewMA.FindControl("ddlcompanyidxma");

                //////  create data set show detail license  ////////

                var dsEquiment_edit = (DataSet)ViewState["vsBuyequipmentEdit"];
                var drEquiment_edit = dsEquiment_edit.Tables[0].NewRow();

                int numrow_edit = dsEquiment_edit.Tables[0].Rows.Count; //จำนวนแถว

                ViewState["_ddlorg_idxinsert_checkedit"] = _ddlorg_idxeditlicense.SelectedValue;
                ViewState["_ddlrdept_idxinsert_checkedit"] = _ddlrdept_idxeditlicense.SelectedValue;

                if (numrow_edit > 0)
                {
                    foreach (DataRow check_edit in dsEquiment_edit.Tables[0].Rows)
                    {
                        ViewState["check_org_edit"] = check_edit["OrgIDXEdit_dataset"];
                        ViewState["check_dept_edit"] = check_edit["RDeptIDXEdit_dataset"];


                        if ((int.Parse(ViewState["_ddlorg_idxinsert_checkedit"].ToString()) == int.Parse(ViewState["check_org_edit"].ToString())) && (int.Parse(ViewState["_ddlrdept_idxinsert_checkedit"].ToString()) == int.Parse(ViewState["check_dept_edit"].ToString())))

                        //if (int.Parse(ViewState["_ddlorg_idxinsert_check"].ToString()) == int.Parse(ViewState["ch"].ToString()))
                        {
                            ViewState["CheckDataset_edit"] = "0";
                            break;

                        }
                        else
                        {
                            ViewState["CheckDataset_edit"] = "1";

                        }
                    }


                    if (ViewState["CheckDataset_edit"].ToString() == "1")
                    {
                        drEquiment_edit["SoftwareEdit_License_dataset"] = hf_software_idx.Text;

                        drEquiment_edit["OrgNameTHEdit_dataset"] = _ddlorg_idxeditlicense.SelectedItem;
                        drEquiment_edit["OrgIDXEdit_dataset"] = _ddlorg_idxeditlicense.SelectedValue;

                        drEquiment_edit["DeptNameTHEdit_dataset"] = _ddlrdept_idxeditlicense.SelectedItem;
                        drEquiment_edit["RDeptIDXEdit_dataset"] = _ddlrdept_idxeditlicense.SelectedValue;

                        drEquiment_edit["numlicenseEdit_dataset"] = int.Parse(_txtnumlicense_depteditlicense.Text);

                        dsEquiment_edit.Tables[0].Rows.Add(drEquiment_edit);
                        ViewState["vsBuyequipmentEdit"] = dsEquiment_edit;

                        //////  create data set show detail license  ////////


                        gidviewdetaileditlicense.Visible = true;

                        GVShowEditdetailLicense.DataSource = dsEquiment_edit.Tables[0];
                        GVShowEditdetailLicense.DataBind();

                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลอยู่แล้ว!!!');", true);
                        break;
                    }

                }
                else
                {
                    drEquiment_edit["SoftwareEdit_License_dataset"] = hf_software_idx.Text;

                    drEquiment_edit["OrgNameTHEdit_dataset"] = _ddlorg_idxeditlicense.SelectedItem;
                    drEquiment_edit["OrgIDXEdit_dataset"] = _ddlorg_idxeditlicense.SelectedValue;

                    drEquiment_edit["DeptNameTHEdit_dataset"] = _ddlrdept_idxeditlicense.SelectedItem;
                    drEquiment_edit["RDeptIDXEdit_dataset"] = _ddlrdept_idxeditlicense.SelectedValue;

                    drEquiment_edit["numlicenseEdit_dataset"] = int.Parse(_txtnumlicense_depteditlicense.Text);

                    dsEquiment_edit.Tables[0].Rows.Add(drEquiment_edit);
                    ViewState["vsBuyequipmentEdit"] = dsEquiment_edit;

                    //////  create data set show detail license  ////////


                    gidviewdetaileditlicense.Visible = true; //// ############

                    GVShowEditdetailLicense.DataSource = dsEquiment_edit.Tables[0];
                    GVShowEditdetailLicense.DataBind();

                }

                ////DataTable dtx = new DataTable();
                ////dtx = (DataTable)ViewState["data_bind_deptdetail"];
                ////DataRow drx = dtx.NewRow();
                ////drx["OrgNameTH"] = _ddlorg_idxeditlicense.SelectedItem;
                ////drx["DeptNameTH"] = _ddlrdept_idxeditlicense.SelectedItem;
                ////drx["num_license_rdept_idx"] = _txtnumlicense_depteditlicense.Text;
                ////dtx.Rows.Add(drx);
                ////ViewState["data_bind_deptdetail"] = dtx;
                //////Jon.DataSource = dtx;
                //////Jon.DataBind();

                ////GvShowDetailDeptEdit.DataSource = dtx;
                ////GvShowDetailDeptEdit.DataBind();

                //GridView1.DataSource = dtx;
                //GridView1.DataBind();

                break;

            case "btnDeletedata":

                FormView FvViewDetailEdit1 = (FormView)ViewDetail.FindControl("FvViewDetail");
                GridView GvShowDetailDeptEdit1 = (GridView)FvViewDetailEdit1.FindControl("GvShowDetailDeptEdit");

                FvViewDetailEdit1.ChangeMode(FormViewMode.Edit);


                //test_del.Text = "1111";

                ////DataTable dtx = new DataTable();
                //DataTable data_delete = new DataTable();
                //data_delete = (DataTable)ViewState["data_bind_deptdetail"];

                //for (int del = data_delete.Rows.Count - 1; del >= 0; del--)
                //{
                //    DataRow dr_del = data_delete.Rows[del];
                //    //if (dr_del["OrgNameTH"] == "Joe")
                //        dr_del.Delete();


                //    ViewState["data_bind_deptdetail"] = data_delete;

                //    GvShowDetailDeptEdit1.DataSource = dr_del;
                //    GvShowDetailDeptEdit1.DataBind();


                //}

                //var data_bind_deptdetail = (DataSet)ViewState["data_bind_deptdetail"];
                //var drdelete = data_bind_deptdetail.Tables[0].Rows;

                //drdelete.RemoveAt(e.RowIndex);

                //ViewState["data_bind_deptdetail"] = data_bind_deptdetail;
                //GvShowDetailDeptEdit.EditIndex = -1;
                //GvShowDetailDeptEdit.DataSource = ViewState["data_bind_deptdetail"];
                //GvShowDetailDeptEdit.DataBind();




                break;


            case "btnDelindex":

                _software_idx_del = int.Parse(cmdArg);
                _cemp_idx = emp_idx;

               
                _data_softwarelicense.m0software_list = new m0software_detail[1];
                _m0software_detail.software_idx = _software_idx_del;
                _m0software_detail.cemp_idx = _cemp_idx;

                _data_softwarelicense.m0software_list[0] = _m0software_detail;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

                _data_softwarelicense = callServiceSoftwareLicense(_urlSetDelm0Software, _data_softwarelicense);


                if (_data_softwarelicense.return_code == 0)
                {

                    //actionIndex();
                    //MvMaster.SetActiveView(ViewIndex);
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);


                }
                else
                {

                    //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่สามารถลบ Software ได้ เนื่องจากมีการใช้งานแล้ว');", true);

                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่สามารถลบ Software ได้ เนื่องจากมีการใช้งานแล้ว!!!');", true);
                    break;


                    //setError(_data_softwarelicense.return_code.ToString() + " - " + _data_softwarelicense.return_msg);
                }

                break;




        }
    }
    #endregion btnCommand


    #region FvDetail_DataBound

    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        var FvName = (FormView)sender;

        switch (FvName.ID)
        {

            case "FvViewDetail":

                FormView FvViewDetail = (FormView)ViewDetail.FindControl("FvViewDetail");


                if (FvViewDetail.CurrentMode == FormViewMode.ReadOnly)
                {


                }

                if (FvViewDetail.CurrentMode == FormViewMode.Edit)
                {

                    Label lbcompany_idxdit = (Label)FvViewDetail.FindControl("lbcompany_idxdit");
                    DropDownList ddlcompany_nameedit = (DropDownList)FvViewDetail.FindControl("ddlcompany_nameedit");

                    //แก้ไขบริษัท
                    ddlcompany_nameedit.AppendDataBoundItems = true;

                    _data_softwarelicense.m0_itswl_company_list = new m0_itswl_company_detail[1];
                    m0_itswl_company_detail _m0_itswl_company_detail = new m0_itswl_company_detail();

                    _data_softwarelicense.m0_itswl_company_list[0] = _m0_itswl_company_detail;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

                    _data_softwarelicense = callServiceSoftwareLicense(_urlGetddlCompanySoftware, _data_softwarelicense);

                    ddlcompany_nameedit.DataSource = _data_softwarelicense.m0_itswl_company_list;
                    ddlcompany_nameedit.DataTextField = "company_name";
                    ddlcompany_nameedit.DataValueField = "company_idx";
                    ddlcompany_nameedit.DataBind();
                    ddlcompany_nameedit.Items.Insert(0, new ListItem("กรุณาเลือกบริษัทmที่ซื้อ ....", "00"));
                    ddlcompany_nameedit.SelectedValue = lbcompany_idxdit.Text;
                 
               }

                break;

        }
    }

    #endregion

    #region bind data
    protected void getGridData(string _cmdName, int _actionType)
    {
        // setGridData(gvBookingType, ViewState["listData"]);
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected string getStatus(int status)
    {
        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    //protected string getStatusView(int statusview)
    //{
    //    if (statusview == 1)
    //    {

    //        FormView FvViewDetail = (FormView)ViewDetail.FindControl("FvViewDetail");
    //        TextBox txtsoftware_statusview = (TextBox)FvViewDetail.FindControl("txtsoftware_statusview");

    //        return txtsoftware_statusview.Text = "Online";
    //        //return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
    //    }
    //    else
    //    {
    //        FormView FvViewDetail = (FormView)ViewDetail.FindControl("FvViewDetail");
    //        TextBox txtsoftware_statusview = (TextBox)FvViewDetail.FindControl("txtsoftware_statusview");

    //        return txtsoftware_statusview.Text = "Offline";
    //        //return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
    //    }
    //}

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {

            case "GVShowdetailLicense":

                GVShowdetailLicense.PageIndex = e.NewPageIndex;
                GVShowdetailLicense.DataSource = ViewState["vsBuyequipment"];
                GVShowdetailLicense.DataBind();

                break;
            case "GvMaster":
                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();
                actionIndex();
                break;

            case "GVShowEditdetailLicense":


                //FvViewDetail.DataBind();
                FvViewDetail.ChangeMode(FormViewMode.Edit);

                Panel PaneldetailLicenseEdit = (Panel)FvViewDetail.FindControl("PaneldetailLicenseEdit");
                Panel gidviewdetaileditlicense = (Panel)PaneldetailLicenseEdit.FindControl("gidviewdetaileditlicense");
                GridView GVShowEditdetailLicense = (GridView)gidviewdetaileditlicense.FindControl("GVShowEditdetailLicense");

                GVShowEditdetailLicense.PageIndex = e.NewPageIndex;
                GVShowEditdetailLicense.DataSource = ViewState["vsBuyequipmentEdit"];
                GVShowEditdetailLicense.DataBind();

                break;


        }
    }

    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                break;

            //case "GvShowDetailDeptEdit":

            //    if (e.Row.RowType == DataControlRowType.DataRow)
            //    {
            //        //e.Row.Cells[3].Visible = false;
            //    }
            //        //if (e.Row.RowState.ToString().Contains("Edit"))
            //        //{
            //        //    GridView editGrid = sender as GridView;
            //        //    int colSpan = editGrid.Columns.Count;
            //        //    for (int i = 1; i < colSpan; i++)
            //        //    {
            //        //        e.Row.Cells[i].Visible = false;
            //        //        e.Row.Cells[i].Controls.Clear();
            //        //    }
            //        //    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
            //        //    e.Row.Cells[0].CssClass = "";

            //        //    e.Row.Cells[0].Visible = false;
            //        //}




            //        break;
        }
    }

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GVShowdetailLicense":

                GVShowdetailLicense.EditIndex = e.NewEditIndex;
                GVShowdetailLicense.DataSource = ViewState["vsBuyequipment"];
                GVShowdetailLicense.DataBind();

                break;
            case "GvMaster":
                GvMaster.EditIndex = e.NewEditIndex;
                ////actionIndex();
                break;

            case "GVShowEditdetailLicense":

                //FormView FvViewDetail = (FormView)ViewDetail.FindControl("FvViewDetail");

                //FvViewDetail.DataBind();
                FvViewDetail.ChangeMode(FormViewMode.Edit);

                Panel PaneldetailLicenseEdit = (Panel)FvViewDetail.FindControl("PaneldetailLicenseEdit");
                Panel gidviewdetaileditlicense = (Panel)PaneldetailLicenseEdit.FindControl("gidviewdetaileditlicense");
                GridView GVShowEditdetailLicense = (GridView)gidviewdetaileditlicense.FindControl("GVShowEditdetailLicense");

                GVShowEditdetailLicense.EditIndex = e.NewEditIndex;
                GVShowEditdetailLicense.DataSource = ViewState["vsBuyequipmentEdit"];
                GVShowEditdetailLicense.DataBind();

                break;


        }
    }

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.EditIndex = -1;
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
        }
    }

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GVLicenseDept":
                ////int company_idx_update = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                ////var txtCompanyNameUpdate = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtCompanyNameUpdate");
                ////var ddlCompanyStatusUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlCompanyStatusUpdate");


                ////GvMaster.EditIndex = -1;

                ////_data_networkdevices.m0company_list = new m0company_detail[1];
                ////m0company_detail _m0company_detail = new m0company_detail();
                ////_m0company_detail.company_idx = company_idx_update;
                ////_m0company_detail.company_name = txtCompanyNameUpdate.Text;
                ////_m0company_detail.company_status = int.Parse(ddlCompanyStatusUpdate.SelectedValue);
                ////_m0company_detail.cemp_idx = emp_idx;

                ////_data_networkdevices.m0company_list[0] = _m0company_detail;

                ////litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));
                ////_data_networkdevices = callServiceNetwork(_urlSetm0Company, _data_networkdevices);

                ////if (_data_networkdevices.return_code == 0)
                ////{
                ////    initPage();
                ////    setDataList(dtlMenu, _dataMenu.m0_menu_list);
                ////    actionIndex();


                ////}
                ////else
                ////{
                ////    setError(_data_networkdevices.return_code.ToString() + " - " + _data_networkdevices.return_msg);
                ////}


                break;
        }
    }

    protected void Master_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GVShowdetailLicense":

                var dsvsBuyequipmentDelete = (DataSet)ViewState["vsBuyequipment"];
                var drDriving = dsvsBuyequipmentDelete.Tables[0].Rows;

                drDriving.RemoveAt(e.RowIndex);

                ViewState["vsBuyequipment"] = dsvsBuyequipmentDelete;
                GVShowdetailLicense.EditIndex = -1;
                GVShowdetailLicense.DataSource = ViewState["vsBuyequipment"];
                GVShowdetailLicense.DataBind();

                break;

            case "GvShowDetailDeptEdit":              

                FormView FvViewDetail = (FormView)ViewDetail.FindControl("FvViewDetail");
                FvViewDetail.ChangeMode(FormViewMode.Edit);

                GridView GvShowDetailDeptEdit = (GridView)FvViewDetail.FindControl("GvShowDetailDeptEdit");

                //DataTable dtx = new DataTable();
                //dtx = (DataTable)ViewState["data_bind_deptdetail"];
                //DataRow drx = dtx.NewRow();
                //drx["OrgNameTH"] = _ddlorg_idxeditlicense.SelectedItem;
                //drx["DeptNameTH"] = _ddlrdept_idxeditlicense.SelectedItem;
                //drx["num_license_rdept_idx"] = _txtnumlicense_depteditlicense.Text;
                //dtx.Rows.Add(drx);
                //ViewState["data_bind_deptdetail"] = dtx;
                ////Jon.DataSource = dtx;
                ////Jon.DataBind();

                //GvShowDetailDeptEdit.DataSource = dtx;
                //GvShowDetailDeptEdit.DataBind();


                //DataTable dtx1 = new DataTable();
                //dtx1 = (DataTable)ViewState["data_bind_deptdetail"];

                //DataRow drx1 = dtx1.NewRow();

                //DataTable sourceData = (DataTable)GvShowDetailDeptEdit.DataSource;

                //sourceData.Rows[e.RowIndex].Delete();

                //GridVie1.DataSource = sourceData;
                //GridView1.DataBind();

                //test_del.Text = "22222";

                var data_bind_deptdetail = (DataSet)ViewState["data_bind_deptdetail"];
                var drdelete = data_bind_deptdetail.Tables[0].Rows;

                drdelete.RemoveAt(e.RowIndex);

                ViewState["data_bind_deptdetail"] = data_bind_deptdetail;
                GvShowDetailDeptEdit.EditIndex = -1;
                GvShowDetailDeptEdit.DataSource = ViewState["data_bind_deptdetail"];
                GvShowDetailDeptEdit.DataBind();

                break;

            case "GVShowEditdetailLicense":


                FormView FvViewDetail3 = (FormView)ViewDetail.FindControl("FvViewDetail");
                FvViewDetail3.ChangeMode(FormViewMode.Edit);

                Panel PaneldetailLicenseEdit = (Panel)FvViewDetail3.FindControl("PaneldetailLicenseEdit");
                Panel gidviewdetaileditlicense = (Panel)PaneldetailLicenseEdit.FindControl("gidviewdetaileditlicense");
                GridView GVShowEditdetailLicense = (GridView)gidviewdetaileditlicense.FindControl("GVShowEditdetailLicense");


                var dsvsBuyequipmentDeleteEdit = (DataSet)ViewState["vsBuyequipmentEdit"];
                var drDrivingEdit = dsvsBuyequipmentDeleteEdit.Tables[0].Rows;

                drDrivingEdit.RemoveAt(e.RowIndex);

                ViewState["vsBuyequipmentEdit"] = dsvsBuyequipmentDeleteEdit;
                GVShowEditdetailLicense.EditIndex = -1;
                GVShowEditdetailLicense.DataSource = ViewState["vsBuyequipmentEdit"];
                GVShowEditdetailLicense.DataBind();

                break;

                //case "GvShowDetailDeptEdit":

                //    //FvViewDetail.DataBind();
                //    //FvViewDetail.ChangeMode(FormViewMode.Edit);

                //    ////Panel PaneldetailLicenseEdit = (Panel)FvViewDetail.FindControl("PaneldetailLicenseEdit");
                //    ////Panel gidviewdetaileditlicense = (Panel)PaneldetailLicenseEdit.FindControl("gidviewdetaileditlicense");
                //    //GridView GvShowDetailDeptEdit = (GridView)FvViewDetail.FindControl("GvShowDetailDeptEdit");

                //    ////ViewState["data_bind_deptdetail"]
                //    //var databindDeleteEdit = (DataSet)ViewState["data_bind_deptdetail"];
                //    //var databindDrivingEdit = databindDeleteEdit.Tables[0].Rows;

                //    //databindDrivingEdit.RemoveAt(e.RowIndex);

                //    //ViewState["data_bind_deptdetail"] = databindDeleteEdit;
                //    //GvShowDetailDeptEdit.EditIndex = -1;
                //    //GvShowDetailDeptEdit.DataSource = ViewState["data_bind_deptdetail"];
                //    //GvShowDetailDeptEdit.DataBind();

                //    break;
        }
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();

        switch (fvName.ID)
        {



        }
    }

    protected void setDataList(DataList dtlName, Object obj)
    {
        dtlName.DataSource = obj;
        dtlName.DataBind();
    }

    #endregion bind data

    #region Checkbox เชคแสดง
    protected void CheckboxChanged(object sender, EventArgs e)
    {
        var cb = (CheckBox)sender;
        switch (cb.ID)
        {

            case "chk_license":

                // actionSystemall();
                if (chk_license.Checked)
                {
                    PaneldetailLicense.Visible = true;
                    showdetailpermissionsystem.Visible = false;

                    DropDownList ddlorg_idxinsert = (DropDownList)PaneldetailLicense.FindControl("ddlorg_idxinsert");
                    TextBox txtnumlicense_dept = (TextBox)PaneldetailLicense.FindControl("txtnumlicense_dept");

                    ddlorg_idxinsert.Items.Clear();
                    ddlorg_idxinsert.AppendDataBoundItems = true;
                    ddlorg_idxinsert.Items.Add(new ListItem("กรุณาเลือกองค์กร ...", "00"));

                    _data_softwarelicense.organization_list = new organization_detail[1];
                    organization_detail _organization_detail = new organization_detail();

                    //_permissionDetail.OrgIDX = 0;

                    _data_softwarelicense.organization_list[0] = _organization_detail;


                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_permission));
                    _data_softwarelicense = callServiceSoftwareLicense(_urlGetddlOrganizationSW, _data_softwarelicense);

                    ddlorg_idxinsert.DataSource = _data_softwarelicense.organization_list;
                    ddlorg_idxinsert.DataTextField = "OrgNameTH";
                    ddlorg_idxinsert.DataValueField = "OrgIDX";
                    ddlorg_idxinsert.DataBind();

                    ddlrdept_idxinsert.Items.Clear();
                    ddlrdept_idxinsert.Items.Insert(00, new ListItem("กรุณาเลือกฝ่าย ...", "00"));
                    ddlrdept_idxinsert.SelectedValue = "00";

                    txtnumlicense_dept.Text = "1";

                }
                else
                {

                    PaneldetailLicense.Visible = false;
                    showdetailpermissionsystem.Visible = true;
                    ddlorg_idxinsert.Items.Clear();

                    ddlrdept_idxinsert.Items.Clear();
                    txtnumlicense_dept.Text = "";

                   
                }
                break;

            case "chk_licenseedit":
               
                FvViewDetail.ChangeMode(FormViewMode.Edit);
               
                //FormView FvViewDetail = (FormView)ViewDetail.FindControl("FvViewDetail");
                CheckBox chk_licenseedit = (CheckBox)FvViewDetail.FindControl("chk_licenseedit");

                if(chk_licenseedit.Checked)
                {
                   // FvViewDetail.ChangeMode(FormViewMode.Edit);

                    Panel PaneldetailLicenseEdit = (Panel)FvViewDetail.FindControl("PaneldetailLicenseEdit");

                    PaneldetailLicenseEdit.Visible = true;

                    show_btnedit.Visible = true;
                    btn_Saveedit.Visible = true;

                    DropDownList ddlorg_idxeditlicense = (DropDownList)PaneldetailLicenseEdit.FindControl("ddlorg_idxeditlicense");
                    DropDownList ddlrdept_idxeditlicense = (DropDownList)PaneldetailLicenseEdit.FindControl("ddlrdept_idxeditlicense");

                    ddlorg_idxeditlicense.Items.Clear();
                    ddlorg_idxeditlicense.AppendDataBoundItems = true;
                    ddlorg_idxeditlicense.Items.Add(new ListItem("กรุณาเลือกองค์กร ...", "00"));

                    _data_softwarelicense.organization_list = new organization_detail[1];
                    organization_detail _organization_detail = new organization_detail();

                    //_permissionDetail.OrgIDX = 0;

                    _data_softwarelicense.organization_list[0] = _organization_detail;


                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_permission));
                    _data_softwarelicense = callServiceSoftwareLicense(_urlGetddlOrganizationSW, _data_softwarelicense);

                    ddlorg_idxeditlicense.DataSource = _data_softwarelicense.organization_list;
                    ddlorg_idxeditlicense.DataTextField = "OrgNameTH";
                    ddlorg_idxeditlicense.DataValueField = "OrgIDX";
                    ddlorg_idxeditlicense.DataBind();

                    ddlrdept_idxeditlicense.Items.Clear();
                    ddlrdept_idxeditlicense.Items.Insert(00, new ListItem("กรุณาเลือกฝ่าย ...", "00"));
                    ddlrdept_idxeditlicense.SelectedValue = "00";


                }
                else
                {
                    FvViewDetail.ChangeMode(FormViewMode.Edit);

                    Panel PaneldetailLicenseEdit = (Panel)FvViewDetail.FindControl("PaneldetailLicenseEdit");

                    DropDownList ddlorg_idxeditlicense = (DropDownList)PaneldetailLicenseEdit.FindControl("ddlorg_idxeditlicense");
                    DropDownList ddlrdept_idxeditlicense = (DropDownList)PaneldetailLicenseEdit.FindControl("ddlrdept_idxeditlicense");
                    TextBox txtnumlicense_depteditlicense = (TextBox)PaneldetailLicenseEdit.FindControl("txtnumlicense_depteditlicense");

                    //Panel gidviewdetaileditlicense = (Panel)PaneldetailLicenseEdit.FindControl("gidviewdetaileditlicense");
                    //GridView GVShowEditdetailLicense = (GridView)gidviewdetaileditlicense.FindControl("GVShowEditdetailLicense");


                    PaneldetailLicenseEdit.Visible = false;


                    //show_btnedit.Visible = true;
                    //btn_Saveedit.Visible = false;


                    ddlorg_idxeditlicense.Items.Clear();

                    ddlrdept_idxeditlicense.Items.Clear();
                    txtnumlicense_depteditlicense.Text = "";


                    //gidviewdetaileditlicense.Visible = false;

                    //GVShowEditdetailLicense.DataSource = null;
                    //GVShowEditdetailLicense.DataBind();


                }



                // actionSystemall();
                ////if (chk_licenseedit.Checked)
                ////{
                ////    PaneldetailLicense.Visible = true;


                ////    DropDownList ddlorg_idxinsert = (DropDownList)PaneldetailLicense.FindControl("ddlorg_idxinsert");

                ////    ddlorg_idxinsert.Items.Clear();
                ////    ddlorg_idxinsert.AppendDataBoundItems = true;
                ////    ddlorg_idxinsert.Items.Add(new ListItem("กรุณาเลือกองค์กร ...", "00"));

                ////    _data_softwarelicense.organization_list = new organization_detail[1];
                ////    organization_detail _organization_detail = new organization_detail();

                ////    //_permissionDetail.OrgIDX = 0;

                ////    _data_softwarelicense.organization_list[0] = _organization_detail;


                ////    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_permission));
                ////    _data_softwarelicense = callServiceSoftwareLicense(_urlGetddlOrganizationSW, _data_softwarelicense);

                ////    ddlorg_idxinsert.DataSource = _data_softwarelicense.organization_list;
                ////    ddlorg_idxinsert.DataTextField = "OrgNameTH";
                ////    ddlorg_idxinsert.DataValueField = "OrgIDX";
                ////    ddlorg_idxinsert.DataBind();

                ////    ddlrdept_idxinsert.Items.Clear();
                ////    ddlrdept_idxinsert.Items.Insert(00, new ListItem("กรุณาเลือกฝ่าย ...", "00"));
                ////    ddlrdept_idxinsert.SelectedValue = "00";

                ////}
                ////else
                ////{

                ////    PaneldetailLicense.Visible = false;
                ////    ddlorg_idxinsert.Items.Clear();

                ////    ddlrdept_idxinsert.Items.Clear();
                ////    txtnumlicense_dept.Text = "";


                ////}
                break;



        }
    }
    #endregion

    //protected void CheckBoxRequired_ServerValidate(object sender, ServerValidateEventArgs e)
    //{
    //    // e.IsValid = chk_license.Checked;
    //    CheckBox chk_license = (CheckBox)ViewInsert.FindControl("chk_license");

    //    if (chk_license.Checked)
    //    {
    //        e.IsValid = true;
    //    }
    //    else
    //    {
    //        e.IsValid = false;
    //    }
    //}

    //protected void MyValidator_ServerValidate(object source, ServerValidateEventArgs args)
    //{
    //    args.IsValid = MyCheckbox.Checked;
    //}

    #region ddlSelectedIndexChanged
    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {

        var ddName = (DropDownList)sender;
        DropDownList _ddlsoftware_nameinsert = (DropDownList)ViewInsert.FindControl("ddlsoftware_nameinsert");

        // RepeaterItem rpItem = ddName.NamingContainer as RepeaterItem;

        switch (ddName.ID)
        {
            case "ddlsoftware_nameinsert":

                if (ddlsoftware_nameinsert.SelectedValue == "00")
                {
                    //lbtype_idx.Text = ViewState["type_idx"].ToString();

                    lbdateexpire.Visible = false;
                    show_date.Visible = false;
                    txtdateexpireinsert.Visible = false;


                }
                else
                {
                    ViewState["value_software"] = _ddlsoftware_nameinsert.SelectedValue; // ค่า idx Software ที่เลือกใน ตอน insert

                    data_softwarelicense _data_ddl_swtype = new data_softwarelicense();
                    _data_ddl_swtype.softwarename_list = new softwarename_detail[1];
                    softwarename_detail _softwarename_detailtype = new softwarename_detail();

                    _softwarename_detailtype.software_name_idx = int.Parse(ddlsoftware_nameinsert.SelectedValue);

                    _data_ddl_swtype.softwarename_list[0] = _softwarename_detailtype;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_ddl_swtype));

                    _data_ddl_swtype = callServiceSoftwareLicense(_urlGetTypename, _data_ddl_swtype);

                    ViewState["value_type11"] = _data_ddl_swtype.softwarename_list[0].software_type;

                    ////ViewState["value_software"] == ddlsoftware_nameinsert.SelectedValue;
                    lbtype_idx.Text = ViewState["value_type11"].ToString();

                    if(ViewState["value_type11"].ToString() == "2")
                    {
                        lbdateexpire.Visible = false;
                        show_date.Visible = false;
                        txtdateexpireinsert.Visible = false;
                    }
                    else
                    {
                        lbdateexpire.Visible = true;
                        show_date.Visible = true;
                        txtdateexpireinsert.Visible = true;
                    }


                }

                break;


            case "ddlorg_idxinsert":

                Panel PaneldetailLicense = (Panel)ViewInsert.FindControl("PaneldetailLicense");
                DropDownList ddlorg_idxinsert = (DropDownList)PaneldetailLicense.FindControl("ddlorg_idxinsert");
                DropDownList ddlrdept_idxinsert = (DropDownList)PaneldetailLicense.FindControl("ddlrdept_idxinsert");

                if (ddlorg_idxinsert.SelectedValue == "00")
                {

                    ddlorg_idxinsert.Items.Clear();

                    ddlorg_idxinsert.AppendDataBoundItems = true;
                    ddlorg_idxinsert.Items.Add(new ListItem("กรุณาเลือกองค์กร ....", "00"));

                    _data_softwarelicense.organization_list = new organization_detail[1];
                    organization_detail _organization_detail = new organization_detail();

                    _data_softwarelicense.organization_list[0] = _organization_detail;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

                    _data_softwarelicense = callServiceSoftwareLicense(_urlGetddlOrganizationSW, _data_softwarelicense);



                    ddlorg_idxinsert.DataSource = _data_softwarelicense.organization_list;
                    ddlorg_idxinsert.DataTextField = "OrgNameTH";
                    ddlorg_idxinsert.DataValueField = "OrgIDX";
                    ddlorg_idxinsert.DataBind();
                    ddlorg_idxinsert.SelectedValue = "00";

                    ddlrdept_idxinsert.Items.Clear();
                    ddlrdept_idxinsert.Items.Insert(00, new ListItem("กรุณาเลือกฝ่าย ...", "00"));
                    ddlrdept_idxinsert.SelectedValue = "00";


                }
                else
                {

                    ddlrdept_idxinsert.AppendDataBoundItems = true;
                    ddlrdept_idxinsert.Items.Clear();

                    ddlrdept_idxinsert.Items.Add(new ListItem("กรุณาเลือกฝ่าย ....", "00"));


                    _data_softwarelicense.organization_list = new organization_detail[1];
                    organization_detail _organization_detail = new organization_detail();

                    _organization_detail.OrgIDX = int.Parse(ddlorg_idxinsert.SelectedValue);

                    _data_softwarelicense.organization_list[0] = _organization_detail;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

                    _data_softwarelicense = callServiceSoftwareLicense(_urlGetddlDeptSW, _data_softwarelicense);


                    ddlrdept_idxinsert.DataSource = _data_softwarelicense.organization_list;
                    ddlrdept_idxinsert.DataTextField = "DeptNameTH";
                    ddlrdept_idxinsert.DataValueField = "RDeptIDX";
                    ddlrdept_idxinsert.DataBind();


                }

                break;

            case "ddlorg_idxedit":


                // Panel PaneldetailLicense = (Panel)ViewInsert.FindControl("PaneldetailLicense");
                var ddd = (DropDownList)sender;
                RepeaterItem rptDetailSoftwareEdit = ddd.NamingContainer as RepeaterItem;


                DropDownList ddlorg_idxedit = (DropDownList)rptDetailSoftwareEdit.FindControl("ddlorg_idxedit");
                DropDownList ddlrdept_idxedit = (DropDownList)rptDetailSoftwareEdit.FindControl("ddlrdept_idxedit");

                if (ddlorg_idxedit.SelectedValue == "00")
                {

                    ddlorg_idxedit.Items.Clear();

                    ddlorg_idxedit.AppendDataBoundItems = true;
                    ddlorg_idxedit.Items.Add(new ListItem("กรุณาเลือกองค์กร ....", "00"));

                    _data_softwarelicense.organization_list = new organization_detail[1];
                    organization_detail _organization_detail = new organization_detail();

                    _data_softwarelicense.organization_list[0] = _organization_detail;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

                    _data_softwarelicense = callServiceSoftwareLicense(_urlGetddlOrganizationSW, _data_softwarelicense);



                    ddlorg_idxedit.DataSource = _data_softwarelicense.organization_list;
                    ddlorg_idxedit.DataTextField = "OrgNameTH";
                    ddlorg_idxedit.DataValueField = "OrgIDX";
                    ddlorg_idxedit.DataBind();
                    ddlorg_idxedit.SelectedValue = "00";

                    ddlrdept_idxedit.Items.Clear();
                    ddlrdept_idxedit.Items.Insert(00, new ListItem("กรุณาเลือกฝ่าย ...", "00"));
                    ddlrdept_idxedit.SelectedValue = "00";


                }
                else
                {

                    ddlrdept_idxedit.AppendDataBoundItems = true;
                    ddlrdept_idxedit.Items.Clear();

                    ddlrdept_idxedit.Items.Add(new ListItem("กรุณาเลือกฝ่าย ....", "00"));


                    _data_softwarelicense.organization_list = new organization_detail[1];
                    organization_detail _organization_detail = new organization_detail();

                    _organization_detail.OrgIDX = int.Parse(ddlorg_idxedit.SelectedValue);

                    _data_softwarelicense.organization_list[0] = _organization_detail;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

                    _data_softwarelicense = callServiceSoftwareLicense(_urlGetddlDeptSW, _data_softwarelicense);


                    ddlrdept_idxedit.DataSource = _data_softwarelicense.organization_list;
                    ddlrdept_idxedit.DataTextField = "DeptNameTH";
                    ddlrdept_idxedit.DataValueField = "RDeptIDX";
                    ddlrdept_idxedit.DataBind();


                }

                break;

            case "ddlorg_idxeditlicense":

                //Panel PaneldetailLicenseEdit = (Panel)FvViewDetail.FindControl("PaneldetailLicenseEdit");
                //Panel gidviewdetaileditlicense = (Panel)PaneldetailLicenseEdit.FindControl("gidviewdetaileditlicense");
                //GridView GVShowEditdetailLicense = (GridView)gidviewdetaileditlicense.FindControl("GVShowEditdetailLicense");


                FvViewDetail.ChangeMode(FormViewMode.Edit);

                Panel PaneldetailLicenseEdit = (Panel)FvViewDetail.FindControl("PaneldetailLicenseEdit");
                DropDownList ddlorg_idxeditlicense = (DropDownList)PaneldetailLicenseEdit.FindControl("ddlorg_idxeditlicense");
                DropDownList ddlrdept_idxeditlicense = (DropDownList)PaneldetailLicenseEdit.FindControl("ddlrdept_idxeditlicense");

                if (ddlorg_idxeditlicense.SelectedValue == "00")
                {

                    ddlorg_idxeditlicense.Items.Clear();

                    ddlorg_idxeditlicense.AppendDataBoundItems = true;
                    ddlorg_idxeditlicense.Items.Add(new ListItem("กรุณาเลือกองค์กร ....", "00"));

                    _data_softwarelicense.organization_list = new organization_detail[1];
                    organization_detail _organization_detail = new organization_detail();

                    _data_softwarelicense.organization_list[0] = _organization_detail;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

                    _data_softwarelicense = callServiceSoftwareLicense(_urlGetddlOrganizationSW, _data_softwarelicense);



                    ddlorg_idxeditlicense.DataSource = _data_softwarelicense.organization_list;
                    ddlorg_idxeditlicense.DataTextField = "OrgNameTH";
                    ddlorg_idxeditlicense.DataValueField = "OrgIDX";
                    ddlorg_idxeditlicense.DataBind();
                    ddlorg_idxeditlicense.SelectedValue = "00";

                    ddlrdept_idxeditlicense.Items.Clear();
                    ddlrdept_idxeditlicense.Items.Insert(00, new ListItem("กรุณาเลือกฝ่าย ...", "00"));
                    ddlrdept_idxeditlicense.SelectedValue = "00";


                }
                else
                {

                    ddlrdept_idxeditlicense.AppendDataBoundItems = true;
                    ddlrdept_idxeditlicense.Items.Clear();

                    ddlrdept_idxeditlicense.Items.Add(new ListItem("กรุณาเลือกฝ่าย ....", "00"));


                    _data_softwarelicense.organization_list = new organization_detail[1];
                    organization_detail _organization_detail = new organization_detail();

                    _organization_detail.OrgIDX = int.Parse(ddlorg_idxeditlicense.SelectedValue);

                    _data_softwarelicense.organization_list[0] = _organization_detail;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

                    _data_softwarelicense = callServiceSoftwareLicense(_urlGetddlDeptSW, _data_softwarelicense);


                    ddlrdept_idxeditlicense.DataSource = _data_softwarelicense.organization_list;
                    ddlrdept_idxeditlicense.DataTextField = "DeptNameTH";
                    ddlrdept_idxeditlicense.DataValueField = "RDeptIDX";
                    ddlrdept_idxeditlicense.DataBind();


                }

                break;
        }

    }

    #endregion ddlSelectedIndexChanged 

    #region reuse
    protected void initPage()
    {
        clearSession();
        clearViewState();

        // divAction.Visible = true;
        // gvBookingType.Visible = true;
        // fvBookingType.Visible = false;

        // getGridData("booking_type", 20);
        //setFormData(fvMenu, FormViewMode.Insert, null, "0", "0", "0");


    }

    protected void initPagePermission()
    {
        clearSession();
        clearViewState();

        // divAction.Visible = true;
        // gvBookingType.Visible = true;
        // fvBookingType.Visible = false;

        // getGridData("booking_type", 20);
        //setFormData(fvPermission, FormViewMode.Insert, null, "0", "0", "0");


    }

    protected void setVisible()
    {
        // divAction.Visible = !divAction.Visible;
        // gvBookingType.Visible = !gvBookingType.Visible;
        // fvBookingType.Visible = !fvBookingType.Visible;
    }

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        // ViewState["listData"] = null;
    }

    protected void setError(string _errorText)
    {
        // divShowError.Visible = !divShowError.Visible;
        // litErrorCode.Text = _dataEmployee.return_code.ToString();
        if (_errorText != "")
        {
            litDebug.Text = "error : " + _errorText;
        }
        else
        {
            litDebug.Text = String.Empty;
        }
    }

    protected data_softwarelicense callServiceSoftwareLicense(string _cmdUrl, data_softwarelicense _data_softwarelicense)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_softwarelicense);
        //litDebug.Text = _localJson;

        // call services
        //_localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //litDebug.Text = _localJson;

        // convert json to object
        _data_softwarelicense = (data_softwarelicense)_funcTool.convertJsonToObject(typeof(data_softwarelicense), _localJson);

        return _data_softwarelicense;
    }


    //protected data_softwarelicense callServicePostSoftwareLicense(string _cmdUrl, data_softwarelicense _data_softwarelicense)
    //{
    //    //// convert to json
    //    _localJson = _funcTool.convertObjectToJson(_data_softwarelicense);
    //    //text.Text =  _cmdUrl + _localJson;

    //    //// call services
    //    _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
    //    // text.Text = _localJson;

    //    ////// convert json to object
    //    _data_softwarelicense = (data_softwarelicense)_funcTool.convertJsonToObject(typeof(data_softwarelicense), _localJson);




    //    return _data_softwarelicense;
    //}




    protected data_permission callServicePermission(string _cmdUrl, data_permission _data_permission)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_permission);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);

        // convert json to object
        _data_permission = (data_permission)_funcTool.convertJsonToObject(typeof(data_permission), _localJson);

        return _data_permission;
    }

    #endregion reuse

    protected void OnEdit(object sender, EventArgs e)
    {
        //Find the reference of the Repeater Item.
        RepeaterItem item = (sender as LinkButton).Parent as RepeaterItem;
        this.ToggleElements(item, true);

        show_btnedit.Visible = true;
        btn_Saveedit.Visible = true;

        //ViewState["ClickEdit"] = 1;

    }

    private void ToggleElements(RepeaterItem item, bool isEdit)
    {
        //Toggle Buttons.
        //item.FindControl("lnkEdit").Visible = !isEdit;
        //item.FindControl("lnkUpdate").Visible = isEdit;
        //item.FindControl("lnkCancel").Visible = isEdit;
        //item.FindControl("lnkDelete").Visible = !isEdit;

        //Toggle Labels.
        item.FindControl("lblnum_licenseedit").Visible = !isEdit;
        item.FindControl("lblOrgNameTHedit").Visible = !isEdit;
        item.FindControl("lblDeptNameTHedit").Visible = !isEdit;
        item.FindControl("lbasset_edit").Visible = !isEdit;
        //item.FindControl("lblCountry").Visible = !isEdit;

        //Toggle TextBoxes.
        //item.FindControl("txtContactName").Visible = isEdit;
        item.FindControl("txtnum_licenseedit").Visible = isEdit;
        item.FindControl("ddlorg_idxedit").Visible = isEdit;
        item.FindControl("ddlrdept_idxedit").Visible = isEdit;
        item.FindControl("txtasset_edit").Visible = isEdit;
    }

    protected void rptRecords_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            DataRowView row = e.Item.DataItem as DataRowView;

            Label _lbllicense_org_idxedit = e.Item.FindControl("lbllicense_org_idxedit") as Label;
            DropDownList _ddlorg_idxedit = e.Item.FindControl("ddlorg_idxedit") as DropDownList;


            // organization
            data_softwarelicense _data_softwarelicenseorg11 = new data_softwarelicense();
            _data_softwarelicenseorg11.organization_list = new organization_detail[1];

            organization_detail _organization_detail = new organization_detail();

            _data_softwarelicenseorg11.organization_list[0] = _organization_detail;

            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_permission));
            _data_softwarelicenseorg11 = callServiceSoftwareLicense(_urlGetddlOrganizationSW, _data_softwarelicenseorg11);

            _ddlorg_idxedit.DataSource = _data_softwarelicenseorg11.organization_list;
            _ddlorg_idxedit.DataTextField = "OrgNameTH";
            _ddlorg_idxedit.DataValueField = "OrgIDX";
            //_ddlorg_idxedit.SelectedValue = row["CategoryFK"].ToString();
            _ddlorg_idxedit.DataBind();
            _ddlorg_idxedit.Items.Insert(0, new ListItem("กรุณาเลือกองค์กร ....", "00"));
            _ddlorg_idxedit.SelectedValue = _lbllicense_org_idxedit.Text;


            // dep
            Label _lblrdept_idxedit = e.Item.FindControl("lblrdept_idxedit") as Label;
            DropDownList _ddlrdept_idxedit = e.Item.FindControl("ddlrdept_idxedit") as DropDownList;

            data_softwarelicense _data_softwarelicensedept224 = new data_softwarelicense();
            _data_softwarelicensedept224.organization_list = new organization_detail[1];
            organization_detail _organization_detaildept224 = new organization_detail();

            //_organization_detail.OrgIDX = int.Parse(ddlorg_idxinsert.SelectedValue);

            _data_softwarelicensedept224.organization_list[0] = _organization_detaildept224;

            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicensedept224));

            _data_softwarelicensedept224 = callServiceSoftwareLicense(_urlGetddlDept, _data_softwarelicensedept224);


            _ddlrdept_idxedit.DataSource = _data_softwarelicensedept224.organization_list;
            _ddlrdept_idxedit.DataTextField = "DeptNameTH";
            _ddlrdept_idxedit.DataValueField = "RDeptIDX";
            _ddlrdept_idxedit.DataBind();
            _ddlrdept_idxedit.Items.Insert(0, new ListItem("กรุณาเลือกฝ่าย ....", "00"));
            _ddlrdept_idxedit.SelectedValue = _lblrdept_idxedit.Text;


        }


    }


    protected void LinkAssetDelete_OnClick(object sender, EventArgs args)
    {
        LinkButton link = (LinkButton)sender;
        GridViewRow gv = (GridViewRow)(link.Parent.Parent);
        DataSet ds = new DataSet();
        DataTable dtToGrid = (DataTable)ViewState["data_bind_deptdetail"];
        DataTable dt = ds.Tables["tToGrid"];
        DataRow dr = dt.Rows[0];
        dr.Delete();
        ds.AcceptChanges();


    }

   
}

