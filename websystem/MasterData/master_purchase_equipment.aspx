﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="master_purchase_equipment.aspx.cs" Inherits="websystem_MasterData_master_purchase_equipment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <asp:MultiView ID="mvMultiview" runat="server" ActiveViewIndex="0">
        <asp:View ID="viewHomepage" runat="server">
            <div class="col-sm-12" runat="server" id="content_Homepage">
               <%-- <label>Manage Master Data Purchase Equipment </label>--%>
                <br />
                <asp:LinkButton CssClass="btn btn-success" ID="btnshowboxinsert" data-toggle="tooltip" title="เพิ่มประเภทอุปกรณ์" runat="server"
                    CommandName="showboxinsert" OnCommand="btn_command"><i class="glyphicon glyphicon-plus-sign"></i> เพิ่มประเภทอุปกรณ์</asp:LinkButton>
                <asp:LinkButton CssClass="btn btn-danger" Visible="false" ID="btnhiddenboxinsert" data-toggle="tooltip" title="เพิ่มประเภทอุปกรณ์" runat="server"
                    CommandName="hiddenboxinsert" OnCommand="btn_command"><i class="glyphicon glyphicon-remove-sign"></i> ยกเลิกการเพิ่มอุปกรณ์</asp:LinkButton>
            </div>
            <h5>&nbsp;</h5>
            <div class="col-sm-12" runat="server" visible="false" id="content_insert_equipmentype">
             <%--   <div class="form-group">--%>
                    <div class="col-sm-9">
                        <label>ชื่อประเภทอุปกรณ์</label>
                        <asp:UpdatePanel ID="panelequipmenttype" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtequipmenttype" runat="server" CssClass="form-control"
                                    placeholder="กรุณากรอกประเภทอุปกรณ์..." />
                                <asp:RequiredFieldValidator ID="requiredequipmenttype"
                                    ValidationGroup="save" runat="server"
                                    Display="Dynamic"
                                    SetFocusOnError="true"
                                    ControlToValidate="txtequipmenttype"
                                    Font-Size="1em" ForeColor="Red"
                                    ErrorMessage="กรุณากรอกชื่อประเภทอุปกรณ์" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>

                    <div class="col-sm-3">
                        <label>ประเภทหน่วยของอุปกรณ์</label>
                        <asp:UpdatePanel ID="paneleunit_equipment" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="ddltype_unitequipment"
                                    runat="server" CssClass="form-control fa-align-left"
                                    AutoPostBack="true" Enabled="true">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                    ValidationGroup="save" runat="server"
                                    Display="Dynamic"
                                    SetFocusOnError="true"
                                    ControlToValidate="ddltype_unitequipment"
                                    Font-Size="1em" ForeColor="Red"
                                    ErrorMessage="กรุณาเลือกหน่วยอุปกรณ์" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    <%--</div>--%>
                </div>
                <h5>&nbsp;</h5>
               <%-- <div class="form-group">--%>
                    <div class="col-sm-12">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <asp:LinkButton CssClass="btn btn-success pull-right" data-toggle="tooltip" title="บันทึก" runat="server"
                                    CommandName="save_equipment" OnCommand="btn_command">บันทึก</asp:LinkButton>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <h5>&nbsp;</h5>
                    </div>
                </div>
                <h5>&nbsp;</h5>
          <%--  </div>--%>

            <!-- gridView equipmentype -->
            <div class="col-sm-12" runat="server" id="content_listequipment">
                <asp:GridView ID="gvequipment_purchasetype" runat="server" AutoGenerateColumns="false" DataKeyNames="m0_equipment_type_idx"
                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                    HeaderStyle-CssClass="info" HeaderStyle-Height="30px" AllowPaging="true" PageSize="10"
                    OnPageIndexChanging="gvPageIndexChanging" OnRowEditing="gvRowEditing" OnRowUpdating="gvRowUpdating"
                    OnRowCancelingEdit="gvRowCancelingEdit" OnRowDataBound="gvRowDataBound">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                        FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่พบข้อมูลพนักงาน</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <div style="text-align: center; padding-top: 5px;">
                                    <asp:Label ID="m0_equipment_type_idx" runat="server" Visible="false" Text='<%# Eval("m0_equipment_type_idx") %>' />
                                    <%# (Container.DataItemIndex +1) %>
                                </div>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">

                                        <div class="form-group">
                                            <div class="col-sm-2">
                                                <asp:TextBox ID="txt_equipment_idx" runat="server" CssClass="form-control" Visible="False" Text='<%# Eval("m0_equipment_type_idx")%>' />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="lb_name_equipmenttype" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อประเภทอุปกรณ์" />
                                            <div class="col-sm-5">
                                                <asp:TextBox ID="txtupdate_namequipment" runat="server" CssClass="form-control" Text='<%# Eval("name_equipment_type")%>' />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="lb_status_equipmenttype" CssClass="col-sm-3 control-label" runat="server" Text="สถานะประเภทอุปกรณ์" />
                                            <div class="col-sm-5">
                                                <asp:DropDownList ID="ddlstatus_equipmenttype" Text='<%# Eval("status_equipment") %>'
                                                    CssClass="form-control fa-align-left" runat="server">
                                                    <asp:ListItem Value="1">online</asp:ListItem>
                                                    <asp:ListItem Value="9">offline</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-4 col-sm-offset-8">
                                                <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" runat="server" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default" runat="server" CommandName="Cancel"><i class="fa fa-times"></i></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ประเภทอุปกรณ์" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <div style="padding-top: 5px;">
                                        <asp:Label ID="lb_name_equipment_type" runat="server"
                                            CssClass="col-sm-12" Text='<%# Eval("name_equipment_type") %>'></asp:Label>
                                    </div>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="สถานะอุปกรณ์" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="label_status_equipment" runat="server" Visible="false"
                                        CssClass="col-sm-12" Text='<%# Eval("status_equipment") %>'></asp:Label>
                                    <asp:Label ID="status_offline" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="offline"
                                        CssClass="col-sm-12">
                                                <div style="text-align: center; color: red;">
                                                    <span><b>offline</b></span>
                                    </asp:Label>
                                    <asp:Label ID="status_online" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="online" CssClass="col-sm-12">
                                                <div style="text-align: center; color: green;">
                                                <span><b>online</b></span>
                                    </asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <div style="padding-top: 5px;">
                                    <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                        data-toggle="tooltip" OnCommand="btn_command" title="แก้ไขประเภทอุปกรณ์">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                                    <asp:LinkButton ID="btndeletequipment" CssClass="text-trash" runat="server" CommandName="deletequipment"
                                        data-toggle="tooltip" OnCommand="btn_command" OnClientClick="return confirm('คุณต้องการลบประเภทอุปกรณ์นี้ใช่หรือไม่ ?')"
                                        CommandArgument='<%#Eval("m0_equipment_type_idx") %>' title="ลบประเภทอุปกรณ์">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>

            </div>
        </asp:View>
    </asp:MultiView>


</asp:Content>

