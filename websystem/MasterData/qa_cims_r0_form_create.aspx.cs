﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;

public partial class websystem_MasterData_qa_cims_r0_form_create : System.Web.UI.Page
{
    #region initial function/data
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();
    data_qa_cims _data_qa_cims = new data_qa_cims();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];

    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    //-- employee --//

    //-- master data setname & form detail --//
    static string _urlCimsGetEquipment_name = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetEquipment_name"];

    static string _urlCimsGetSetname = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetSetname"];

    static string _urlCimsSetFormdetail = _serviceUrl + ConfigurationManager.AppSettings["urlCimsSetFormdetail"];
    static string _urlCimsGetFormdetail = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetFormdetail"];
    static string _urlCimsDeleteFormdetail = _serviceUrl + ConfigurationManager.AppSettings["urlCimsDeleteFormdetail"];

    static string _urlCimsSetR0FormCreate = _serviceUrl + ConfigurationManager.AppSettings["urlCimsSetR0FormCreate"];
    static string _urlCimsGetR0FormCreate = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetR0FormCreate"];
    static string _urlCimsDeleteR0FormCreate = _serviceUrl + ConfigurationManager.AppSettings["urlCimsDeleteR0FormCreate"];

    static string _urlCimsSetR1FormCreate = _serviceUrl + ConfigurationManager.AppSettings["urlCimsSetR1FormCreate"];
    static string _urlCimsGetR1FormCreate = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetR1FormCreate"];
    static string _urlCimsDeleteR1FormCreate = _serviceUrl + ConfigurationManager.AppSettings["urlCimsDeleteR1FormCreate"];

    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    bool _flag_qmr = false;

    // rdept permission
    // int[] rdept_qmr = { 20 }; //QMR:26
    #endregion initial function/data


    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
        //check permission
        ////foreach (int item in rdept_qmr)
        ////{
        ////    if (_dataEmployee.employee_list[0].rdept_idx == item)
        ////    {
        ////        _flag_qmr = true;
        ////        break;
        ////    }
        ////}
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            initPage();
            selectR0FormCreate();
        }
        linkBtnTrigger(btnCreateForm);
        createForm();
    }



    #region event command


    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();


        switch (cmdName)
        {

            case "cmdCreateForm":
                switch (cmdArg)
                {
                    case "0":
                        setFormData(fvformInsert, FormViewMode.Insert, null);
                        getM0EquipmentNameList((DropDownList)fvformInsert.FindControl("ddlEquipmentName"), "0");
                        // getM0FormDetailList((DropDownList)fvformInsert.FindControl("ddlTopicForm"), "0");
                        gvMaster.EditIndex = -1;
                        selectR0FormCreate();
                        //selectR0FormCreate();
                        createForm();
                        break;

                    case "1":
                        setFormData(fvformInsertRoot, FormViewMode.Insert, null);
                        getR0FormList();
                        getM0FormDetailList((DropDownList)fvformInsertRoot.FindControl("ddlTopicForm"), "0");
                        gvR1FormCreate.EditIndex = -1;
                        getR1FormList();
                        break;

                    case "2":
                        setFormData(fvformInsertSubFormDetail, FormViewMode.Insert, null);
                        getR1FormListRoot();
                        gvMasterSubSet.EditIndex = -1;
                        selectR1RootFormCreate();
                        break;

                }

                break;

            case "cmdCancel":
                switch (cmdArg)
                {
                    case "0":
                        setActiveView("pageGenaral", 0);
                        setFormData(fvformInsert, FormViewMode.ReadOnly, null);
                        SETFOCUS.Focus();
                        break;

                    case "1":
                        setActiveView("pageCreateRoot", 0);
                        setFormData(fvformInsertRoot, FormViewMode.ReadOnly, null);
                        getR1FormList();
                        SETFOCUS.Focus();
                        break;

                    case "2":

                        setActiveView("pageRootSubSet", 0);
                        setFormData(fvformInsertSubFormDetail, FormViewMode.ReadOnly, null);
                        //getR1FormListRoot();
                        SETFOCUS.Focus();
                        break;


                }

                break;

            //case "cmdCancellvl4":
            //    switch (cmdArg)
            //    {
            //        case "1":
            //            setActiveView("pageRootSubSet", 0);
            //            setFormData(fvformInsertSubFormDetail, FormViewMode.ReadOnly, null);
            //            getR1FormListRoot();
            //            SETFOCUS.Focus();
            //            break;
            //    }

            //    break;

            case "cmdCancelSub":
                setFormData(fvformInsertSubFormDetail, FormViewMode.ReadOnly, null);
                selectR1RootFormCreate();
                SETFOCUS.Focus();
                break;

            case "cmdView":
                string[] cmdArgView = cmdArg.Split(',');
                ViewState["r0_form_idx"] = int.TryParse(cmdArgView[0].ToString(), out _default_int) ? int.Parse(cmdArgView[0].ToString()) : _default_int;
                //ViewState["r0_form_idx"] = int.TryParse(cmdArgView[0].ToString(), out _default_int) ? int.Parse(cmdArgView[0].ToString()) : _default_int;
                int conditionView = int.TryParse(cmdArgView[1].ToString(), out _default_int) ? int.Parse(cmdArgView[1].ToString()) : _default_int;

                switch (conditionView)
                {
                    case 0:
                        setActiveView("pageCreateRoot", int.Parse(ViewState["r0_form_idx"].ToString()));
                        setFormData(fvformInsertRoot, FormViewMode.ReadOnly, null);
                        //selectR1FormCreate();
                        getR1FormList();
                        break;

                    case 1:

                        litBack.Text = ViewState["r0_form_idx"].ToString();
                        setActiveView("pageRootSubSet", int.Parse(ViewState["r0_form_idx"].ToString()));
                        setFormData(fvformInsertSubFormDetail, FormViewMode.ReadOnly, null);
                        selectR1RootFormCreate();
                        break;

                }


                break;

            case "cmdViewRoot":
                string[] cmdArgViewR = cmdArg.Split(',');
                ViewState["r1_form_idx"] = int.TryParse(cmdArgViewR[0].ToString(), out _default_int) ? int.Parse(cmdArgViewR[0].ToString()) : _default_int;
                //ViewState["r0_form_idx"] = int.TryParse(cmdArgView[0].ToString(), out _default_int) ? int.Parse(cmdArgView[0].ToString()) : _default_int;
                int conditionViewR = int.TryParse(cmdArgViewR[1].ToString(), out _default_int) ? int.Parse(cmdArgViewR[1].ToString()) : _default_int;



                switch (conditionViewR)
                {
                    case 0:
                        litDebug.Text = ViewState["r1_form_idx"].ToString();
                        setActiveView("pageRootSubSet", int.Parse(ViewState["r1_form_idx"].ToString()));
                        setFormData(fvformInsertSubFormDetail, FormViewMode.ReadOnly, null);
                        selectR1RootFormCreate();

                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(int.Parse(ViewState["r1_form_idx"].ToString())));
                        break;

                    case 1:
                        //litBack.Text = (int.Parse(ViewState["r1_form_idx"].ToString()) - 1).ToString();
                        litBack.Text = litDebug.Text;
                        
                        //TextBox txtbackValue = (TextBox)pageRootSubSetLvl4.FindControl("txtbackValue");
                        //txtbackValue.Text = ViewState["r1_form_idx"].ToString();
                        setActiveView("pageRootSubSetLvl4", int.Parse(ViewState["r1_form_idx"].ToString()));
                        setFormData(fvformInsertSubFormDetaillvl4, FormViewMode.ReadOnly, null);
                        //selectR1RootFormCreate();

                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(int.Parse(ViewState["r1_form_idx"].ToString())));
                        break;

                }


                break;

            case "cmdSave":
                switch (cmdArg)
                {
                    case "0":
                        TextBox tbFormName = (TextBox)fvformInsert.FindControl("tbFormName");
                        DropDownList ddlFormStatus = (DropDownList)fvformInsert.FindControl("ddlFormStatus");
                        DropDownList ddlEquipmentName = (DropDownList)fvformInsert.FindControl("ddlEquipmentName");
                        // DropDownList ddlTopicForm = (DropDownList)fvformInsert.FindControl("ddlTopicForm");

                        _data_qa_cims.qa_cims_r0_form_create_list = new qa_cims_r0_form_create_detail[1];
                        qa_cims_r0_form_create_detail _r0form = new qa_cims_r0_form_create_detail();
                        _r0form.cemp_idx = _emp_idx;
                        _r0form.r0_form_create_name = tbFormName.Text;
                        _r0form.equipment_idx = int.Parse(ddlEquipmentName.SelectedValue);
                        _r0form.r0_form_create_status = int.Parse(ddlFormStatus.SelectedValue);

                        _data_qa_cims.qa_cims_r0_form_create_list[0] = _r0form;
                        _data_qa_cims = callServicePostMasterQACIMS(_urlCimsSetR0FormCreate, _data_qa_cims);

                        if (_data_qa_cims.return_code == 101)
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);

                        }

                        selectR0FormCreate();
                        setFormData(fvformInsert, FormViewMode.ReadOnly, null);
                        setFormData(fvformInsert, FormViewMode.Insert, null);
                        getM0EquipmentNameList((DropDownList)fvformInsert.FindControl("ddlEquipmentName"), "0");
                        //getM0FormDetailList((DropDownList)fvformInsert.FindControl("ddlTopicForm"), "0");
                        break;

                    case "1":

                        DropDownList ddlFormStatusLevel2 = (DropDownList)fvformInsertRoot.FindControl("ddlFormStatusLevel2");
                        DropDownList ddlTopicForm = (DropDownList)fvformInsertRoot.FindControl("ddlTopicForm");

                        _data_qa_cims.qa_cims_r1_form_create_list = new qa_cims_r1_form_create_detail[1];
                        qa_cims_r1_form_create_detail _r1form = new qa_cims_r1_form_create_detail();
                        _r1form.cemp_idx = _emp_idx;
                        _r1form.r0_form_create_idx = int.Parse(ViewState["r0_form_idx"].ToString());
                        _r1form.form_detail_idx = int.Parse(ddlTopicForm.SelectedValue);
                        _r1form.r1_form_create_status = int.Parse(ddlFormStatusLevel2.SelectedValue);

                        _data_qa_cims.qa_cims_r1_form_create_list[0] = _r1form;
                        _data_qa_cims = callServicePostMasterQACIMS(_urlCimsSetR1FormCreate, _data_qa_cims);

                        if (_data_qa_cims.return_code == 101)
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);

                        }

                        //selectR1FormCreate();
                        getR1FormList();
                        setFormData(fvformInsertRoot, FormViewMode.ReadOnly, null);
                        break;

                        //case "2":

                        //    TextBox txtSubSetName = (TextBox)fvformInsertSubFormDetail.FindControl("txtSubSetName");
                        //    TextBox tbSubSetName = (TextBox)fvformInsertSubFormDetail.FindControl("tbSubSetName");
                        //    DropDownList ddlSubSetStatus = (DropDownList)fvformInsertSubFormDetail.FindControl("ddlSubSetStatus");
                        //    //DropDownList ddlTopicForm = (DropDownList)fvformInsertRoot.FindControl("ddlTopicForm");

                        //    _data_qa_cims.qa_cims_r1_form_create_list = new qa_cims_r1_form_create_detail[1];
                        //    qa_cims_r1_form_create_detail _r1formroot = new qa_cims_r1_form_create_detail();

                        //    _r1formroot.text_name_setform = tbSubSetName.Text;
                        //    _r1formroot.cemp_idx = _emp_idx;
                        //    _r1formroot.r1_form_root_idx = int.Parse(ViewState["r1_form_idx"].ToString());
                        //    _r1formroot.r1_form_create_status = int.Parse(ddlSubSetStatus.SelectedValue);

                        //    _data_qa_cims.qa_cims_r1_form_create_list[0] = _r1formroot;
                        //    _data_qa_cims = callServicePostMasterQACIMS(_urlCimsSetR1FormCreate, _data_qa_cims);

                        //    if (_data_qa_cims.return_code == 101)
                        //    {
                        //        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);

                        //    }
                        //    selectR1RootFormCreate();
                        //    setFormData(fvformInsertSubFormDetail, FormViewMode.ReadOnly, null);
                        //    break;

                }

                break;

            case "cmdSaveRoot":
                switch (cmdArg)
                {
                    case "2":
                        TextBox txtSubSetName = (TextBox)fvformInsertSubFormDetail.FindControl("txtSubSetName");
                        TextBox tbSubSetName = (TextBox)fvformInsertSubFormDetail.FindControl("tbSubSetName");
                        DropDownList ddlSubSetStatus = (DropDownList)fvformInsertSubFormDetail.FindControl("ddlSubSetStatus");
                        //DropDownList ddlTopicForm = (DropDownList)fvformInsertRoot.FindControl("ddlTopicForm");

                        _data_qa_cims.qa_cims_r1_form_create_list = new qa_cims_r1_form_create_detail[1];
                        qa_cims_r1_form_create_detail _r1formroot = new qa_cims_r1_form_create_detail();

                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(int.Parse(ViewState["r1_form_idx"].ToString())));

                        _r1formroot.text_name_setform = tbSubSetName.Text;
                        _r1formroot.cemp_idx = _emp_idx;
                        _r1formroot.r1_form_root_idx = int.Parse(ViewState["r1_form_idx"].ToString());
                        _r1formroot.r1_form_create_status = int.Parse(ddlSubSetStatus.SelectedValue);

                        _data_qa_cims.qa_cims_r1_form_create_list[0] = _r1formroot;
                        _data_qa_cims = callServicePostMasterQACIMS(_urlCimsSetR1FormCreate, _data_qa_cims);

                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_qa_cims));

                        if (_data_qa_cims.return_code == 101)
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);

                        }
                        selectR1RootFormCreate();
                        setFormData(fvformInsertSubFormDetail, FormViewMode.ReadOnly, null);
                        break;
                }
                break;

            case "cmdDelete":
                int _formIDX = int.Parse(cmdArg);
                //litDebug.Text = cmdArg;
                _data_qa_cims.qa_cims_r0_form_create_list = new qa_cims_r0_form_create_detail[1];
                qa_cims_r0_form_create_detail _delform = new qa_cims_r0_form_create_detail();
                _delform.r0_form_create_idx = _formIDX;
                _data_qa_cims.qa_cims_r0_form_create_list[0] = _delform;
                _data_qa_cims = callServicePostMasterQACIMS(_urlCimsDeleteR0FormCreate, _data_qa_cims);

                if (_data_qa_cims.return_code == 0)
                {
                    // litDebug.Text = "success";
                }
                else
                {
                    // litDebug.Text = "not success";
                }

                selectR0FormCreate();

                break;

            case "cmdDeleteR1":
                int _formIDXR1 = int.Parse(cmdArg);
                //litDebug.Text = cmdArg;
                _data_qa_cims.qa_cims_r1_form_create_list = new qa_cims_r1_form_create_detail[1];
                qa_cims_r1_form_create_detail _delformR1 = new qa_cims_r1_form_create_detail();
                _delformR1.r1_form_create_idx = _formIDXR1;
                _data_qa_cims.qa_cims_r1_form_create_list[0] = _delformR1;
                _data_qa_cims = callServicePostMasterQACIMS(_urlCimsDeleteR1FormCreate, _data_qa_cims);

                if (_data_qa_cims.return_code == 0)
                {
                    // litDebug.Text = "success";
                }
                else
                {
                    // litDebug.Text = "not success";
                }

                getR1FormList();

                break;

            case "cmdDeleteR1Root":
                int _formIDXR1Root = int.Parse(cmdArg);
                //Label lbsubsetidx = (Label)gvMasterSubSet.FindControl("lbsubsetidx");
                //litDebug.Text = _formIDXR1Root;
                _data_qa_cims.qa_cims_r1_form_create_list = new qa_cims_r1_form_create_detail[1];
                qa_cims_r1_form_create_detail _delformR1Root = new qa_cims_r1_form_create_detail();
                _delformR1Root.r1_form_create_idx = _formIDXR1Root;
                _data_qa_cims.qa_cims_r1_form_create_list[0] = _delformR1Root;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_delformR1Root));

                _data_qa_cims = callServicePostMasterQACIMS(_urlCimsDeleteR1FormCreate, _data_qa_cims);

                if (_data_qa_cims.return_code == 0)
                {
                    // litDebug.Text = "success";
                }
                else
                {
                    // litDebug.Text = "not success";
                }

                //getR1FormList();
                selectR1RootFormCreate();

                break;

            case "cmdAddForm":

                DropDownList _equipmentName = (DropDownList)fvformInsert.FindControl("ddlEquipmentName");
                DropDownList _topicForm = (DropDownList)fvformInsert.FindControl("ddlTopicForm");
                GridView _gvFormList = (GridView)fvformInsert.FindControl("gvCreateFormList");
                Panel panelFormButton = (Panel)fvformInsert.FindControl("panelFormButton");

                var _dataset_createForm = (DataSet)ViewState["createFormList"];
                var _datarow_createForm = _dataset_createForm.Tables[0].NewRow();

                //ค่าที่เก็บที่จะเอามาเช็ค
                ViewState["m0Topic_check"] = (_topicForm.SelectedValue);
                int _numrow = _dataset_createForm.Tables[0].Rows.Count; //จำนวนแถว
                if (_numrow > 0)
                {
                    foreach (DataRow check in _dataset_createForm.Tables[0].Rows)
                    {
                        ViewState["_check_topicDetail"] = check["m0FormDetailIDX"];
                        //กรณิประเภทการตรวจตรงกัน
                        if (ViewState["m0Topic_check"].ToString() == ViewState["_check_topicDetail"].ToString())
                        {

                            ViewState["CheckDataset"] = "2";
                            break;

                        }
                        else
                        {
                            //กรณิประเภทการตรวจไม่ตรงกัน
                            ViewState["CheckDataset"] = "1";
                        }

                    }
                    //ถ้าข้อมูลไม่ซ้ำ จะเข้าการทำงานนี้
                    if (ViewState["CheckDataset"].ToString() == "1")
                    {
                        _datarow_createForm["m0EquipmentNameIDX"] = (_equipmentName.SelectedValue);
                        _datarow_createForm["nameEquipmentName"] = (_equipmentName.SelectedItem);
                        _datarow_createForm["m0FormDetailIDX"] = (_topicForm.SelectedValue);
                        _datarow_createForm["nameTopic"] = (_topicForm.SelectedItem);

                        _dataset_createForm.Tables[0].Rows.Add(_datarow_createForm);
                        ViewState["createFormList"] = _dataset_createForm;

                        _gvFormList.DataSource = _dataset_createForm.Tables[0];
                        _gvFormList.DataBind();

                        panelFormButton.Visible = true;
                    }
                    else if (ViewState["CheckDataset"].ToString() == "2")
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ขออภัยค่ะ ท่านมีการเพิ่มข้อมูลหัวข้อนี้แล้วค่ะ');", true);

                    }
                }
                else
                {
                    _datarow_createForm["m0EquipmentNameIDX"] = (_equipmentName.SelectedValue);
                    _datarow_createForm["nameEquipmentName"] = (_equipmentName.SelectedItem);
                    _datarow_createForm["m0FormDetailIDX"] = (_topicForm.SelectedValue);
                    _datarow_createForm["nameTopic"] = (_topicForm.SelectedItem);

                    _dataset_createForm.Tables[0].Rows.Add(_datarow_createForm);
                    ViewState["createFormList"] = _dataset_createForm;

                    _gvFormList.DataSource = _dataset_createForm.Tables[0];
                    _gvFormList.DataBind();
                    panelFormButton.Visible = true;
                }


                break;

        }
    }
    #endregion event command

    #region data set
    protected void createForm()
    {
        var _createFormList = new DataSet();
        _createFormList.Tables.Add("datacreateFromList");
        //_createFormList.Tables[0].Columns.Add("r0FormIDX", typeof(int));
        _createFormList.Tables[0].Columns.Add("m0FormDetailIDX", typeof(int));
        _createFormList.Tables[0].Columns.Add("m0EquipmentNameIDX", typeof(int));
        _createFormList.Tables[0].Columns.Add("nameFormCreate", typeof(String));
        _createFormList.Tables[0].Columns.Add("nameEquipmentName", typeof(String));
        _createFormList.Tables[0].Columns.Add("nameTopic", typeof(String));

        if (ViewState["createFormList"] == null)
        {
            ViewState["createFormList"] = _createFormList;
        }
    }

    #endregion

    #region dropdown list
    protected void getM0EquipmentNameList(DropDownList ddlName, string _equipment_idx)
    {
        data_qa_cims _dataqacims = new data_qa_cims();
        _dataqacims.qa_cims_m0_equipment_name_list = new qa_cims_m0_equipment_name_detail[1];
        qa_cims_m0_equipment_name_detail _m0equipment = new qa_cims_m0_equipment_name_detail();
        //_m0equipment.condition = 1;
        _dataqacims.qa_cims_m0_equipment_name_list[0] = _m0equipment;

        _dataqacims = callServicePostMasterQACIMS(_urlCimsGetEquipment_name, _dataqacims);
        setDdlData(ddlName, _dataqacims.qa_cims_m0_equipment_name_list, "equipment_name", "equipment_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกประเภทเครื่องมือที่สอบเทียบ ---", "0"));
        ddlName.SelectedValue = _equipment_idx;
    }

    protected void getM0FormDetailList(DropDownList ddlName, string _form_detail_idx)
    {
        data_qa_cims _dataqacims = new data_qa_cims();
        _dataqacims.qa_cims_m0_form_detail_list = new qa_cims_m0_form_detail_detail[1];
        qa_cims_m0_form_detail_detail _m0form = new qa_cims_m0_form_detail_detail();
        _m0form.condition = 1;
        _dataqacims.qa_cims_m0_form_detail_list[0] = _m0form;

        _dataqacims = callServicePostMasterQACIMS(_urlCimsGetFormdetail, _dataqacims);
        setDdlData(ddlName, _dataqacims.qa_cims_m0_form_detail_list, "form_detail_name", "form_detail_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกฟอร์มรายการที่สอบเทียบ ---", "0"));
        ddlName.SelectedValue = _form_detail_idx;
    }

    #endregion

    #region grid view
    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var gvName = (GridView)sender;

        switch (gvName.ID)
        {
            case "gvMaster":
                gvMaster.PageIndex = e.NewPageIndex;
                gvMaster.DataBind();
                selectR0FormCreate();

                break;

            case "gvR1FormCreate":

                gvR1FormCreate.PageIndex = e.NewPageIndex;
                gvR1FormCreate.DataBind();
                //selectR1FormCreate();
                getR1FormList();
                break;

            case "gvMasterSubSet":

                gvMasterSubSet.PageIndex = e.NewPageIndex;
                gvMasterSubSet.DataBind();
                //selectR1FormCreate();
                selectR1RootFormCreate();
                break;

        }

    }
    protected void gvRowDeleted(object sender, GridViewDeleteEventArgs e)
    {
        var gvName = (GridView)sender;

        switch (gvName.ID)
        {
            case "gvCreateFormList":

                GridView gvCreateFormList = (GridView)fvformInsert.FindControl("gvCreateFormList");
                var DeleteFormList = (DataSet)ViewState["createFormList"];
                var drDriving = DeleteFormList.Tables[0].Rows;

                drDriving.RemoveAt(e.RowIndex);

                ViewState["createFormList"] = DeleteFormList;
                gvCreateFormList.EditIndex = -1;

                setGridData(gvCreateFormList, ViewState["createFormList"]);



                break;
        }

    }
    protected void gvRowEditing(object sender, GridViewEditEventArgs e)
    {
        var gvName = (GridView)sender;
        switch (gvName.ID)
        {
            case "gvMaster":
                gvMaster.EditIndex = e.NewEditIndex;
                setFormData(fvformInsert, FormViewMode.ReadOnly, null);
                selectR0FormCreate();
                break;

            case "gvR1FormCreate":
                gvR1FormCreate.EditIndex = e.NewEditIndex;
                setFormData(fvformInsertRoot, FormViewMode.ReadOnly, null);
                //selectR1FormCreate();
                getR1FormList();
                break;

            case "gvMasterSubSet":
                gvMasterSubSet.EditIndex = e.NewEditIndex;
                setFormData(fvformInsertSubFormDetail, FormViewMode.ReadOnly, null);
                //selectR1FormCreate();
                selectR1RootFormCreate();
                break;
        }

    }
    protected void gvRowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var gvName = (GridView)sender;
        switch (gvName.ID)
        {
            case "gvMaster":

                var _update_r0Formidx = (TextBox)gvMaster.Rows[e.RowIndex].FindControl("_Update_R0FormIDX");
                var _update_r0FormName = (TextBox)gvMaster.Rows[e.RowIndex].FindControl("tbupdate_formname");
                var _update_equipmentidx = (DropDownList)gvMaster.Rows[e.RowIndex].FindControl("ddlEquipmentNameUpdate");
                var _update_r0status = (DropDownList)gvMaster.Rows[e.RowIndex].FindControl("ddlupdater0status");

                gvMaster.EditIndex = -1;

                _data_qa_cims.qa_cims_r0_form_create_list = new qa_cims_r0_form_create_detail[1];
                qa_cims_r0_form_create_detail _upFormR0 = new qa_cims_r0_form_create_detail();
                _upFormR0.r0_form_create_idx = int.Parse(_update_r0Formidx.Text);
                _upFormR0.r0_form_create_name = _update_r0FormName.Text;
                _upFormR0.equipment_idx = int.Parse(_update_equipmentidx.SelectedValue);
                _upFormR0.r0_form_create_status = int.Parse(_update_r0status.SelectedValue);
                _data_qa_cims.qa_cims_r0_form_create_list[0] = _upFormR0;

                _data_qa_cims = callServicePostMasterQACIMS(_urlCimsSetR0FormCreate, _data_qa_cims);
                if (_data_qa_cims.return_code == 102)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถแก้ไขข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);
                }

                else
                {

                }

                selectR0FormCreate();
                SETFOCUS.Focus();
                break;

            case "gvR1FormCreate":


                var _update_r1Formidx = (TextBox)gvR1FormCreate.Rows[e.RowIndex].FindControl("_Update_R1FormName");
                var _update_formdetail = (DropDownList)gvR1FormCreate.Rows[e.RowIndex].FindControl("ddlFormDetailUpdate");
                var _update_r1status = (DropDownList)gvR1FormCreate.Rows[e.RowIndex].FindControl("ddlupdater1status");

                gvR1FormCreate.EditIndex = -1;

                _data_qa_cims.qa_cims_r1_form_create_list = new qa_cims_r1_form_create_detail[1];
                qa_cims_r1_form_create_detail _upFormR1 = new qa_cims_r1_form_create_detail();

                _upFormR1.r1_form_create_idx = int.Parse(_update_r1Formidx.Text);
                _upFormR1.form_detail_idx = int.Parse(_update_formdetail.SelectedValue);
                _upFormR1.r1_form_create_status = int.Parse(_update_r1status.SelectedValue);
                _data_qa_cims.qa_cims_r1_form_create_list[0] = _upFormR1;

                _data_qa_cims = callServicePostMasterQACIMS(_urlCimsSetR1FormCreate, _data_qa_cims);
                if (_data_qa_cims.return_code == 102)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถแก้ไขข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);
                }

                else
                {

                }

                //selectR1FormCreate();
                getR1FormList();
                SETFOCUS.Focus();



                break;

            case "gvMasterSubSet":

                var _updateSubSetidx = (TextBox)gvMasterSubSet.Rows[e.RowIndex].FindControl("_updateSubSetidx");
                var tbupdateSubSetName = (TextBox)gvMasterSubSet.Rows[e.RowIndex].FindControl("tbupdateSubSetName");
                var ddlupdateSubSetStatus = (DropDownList)gvMasterSubSet.Rows[e.RowIndex].FindControl("ddlupdateSubSetStatus");

                gvMasterSubSet.EditIndex = -1;

                _data_qa_cims.qa_cims_r1_form_create_list = new qa_cims_r1_form_create_detail[1];
                qa_cims_r1_form_create_detail _upFormR1Root = new qa_cims_r1_form_create_detail();

                _upFormR1Root.r1_form_create_idx = int.Parse(_updateSubSetidx.Text);
                _upFormR1Root.r1_form_root_idx = int.Parse(ViewState["r1_form_idx"].ToString());
                _upFormR1Root.text_name_setform = tbupdateSubSetName.Text;
                _upFormR1Root.r1_form_create_status = int.Parse(ddlupdateSubSetStatus.SelectedValue);
                _data_qa_cims.qa_cims_r1_form_create_list[0] = _upFormR1Root;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_upFormR1Root));

                _data_qa_cims = callServicePostMasterQACIMS(_urlCimsSetR1FormCreate, _data_qa_cims);
                if (_data_qa_cims.return_code == 102)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถแก้ไขข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);
                }

                else
                {

                }

                //selectR1FormCreate();
                selectR1RootFormCreate();
                SETFOCUS.Focus();



                break;
        }
    }
    protected void gvRowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var gvName = (GridView)sender;
        switch (gvName.ID)
        {
            case "gvMaster":

                gvMaster.EditIndex = -1;
                selectR0FormCreate();
                SETFOCUS.Focus();
                break;

            case "gvR1FormCreate":

                gvR1FormCreate.EditIndex = -1;
                //selectR1FormCreate();
                getR1FormList();
                SETFOCUS.Focus();
                break;

            case "gvMasterSubSet":

                gvMasterSubSet.EditIndex = -1;
                //selectR1FormCreate();
                selectR1RootFormCreate();
                SETFOCUS.Focus();
                break;
        }
    }
    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var gvName = (GridView)sender;
        switch (gvName.ID)
        {
            case "gvCreateFormList":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                }

                break;

            case "gvMaster":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    Label lbstatus = (Label)e.Row.FindControl("lbstatus");
                    Label Formidx = (Label)e.Row.FindControl("lbFormidx");
                    Label status_offline = (Label)e.Row.FindControl("status_offline");
                    Label status_online = (Label)e.Row.FindControl("status_online");

                    ViewState["status_subForm"] = lbstatus.Text;

                    if (ViewState["status_subForm"].ToString() == "0")
                    {
                        status_offline.Visible = true;
                    }
                    else if (ViewState["status_subForm"].ToString() == "1")
                    {
                        status_online.Visible = true;
                    }


                }
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    TextBox tbtestIDXUpdate = (TextBox)e.Row.FindControl("tbtestIDXUpdate");
                    getM0EquipmentNameList((DropDownList)e.Row.FindControl("ddlEquipmentNameUpdate"), tbtestIDXUpdate.Text);

                }


                break;

            case "gvR1FormCreate":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbR1status = (Label)e.Row.FindControl("lbR1status");
                    // Label Formidx = (Label)e.Row.FindControl("lbFormidx");
                    Label R1status_offline = (Label)e.Row.FindControl("lbstatus_offline");
                    Label R1status_online = (Label)e.Row.FindControl("lbstatus_online");

                    ViewState["status_R1subForm"] = lbR1status.Text;

                    if (ViewState["status_R1subForm"].ToString() == "0")
                    {
                        R1status_offline.Visible = true;
                    }
                    else if (ViewState["status_R1subForm"].ToString() == "1")
                    {
                        R1status_online.Visible = true;
                    }
                }

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    TextBox tbFormDetailIDXUpdate = (TextBox)e.Row.FindControl("tbFormDetailIDXUpdate");
                    getM0FormDetailList((DropDownList)e.Row.FindControl("ddlFormDetailUpdate"), tbFormDetailIDXUpdate.Text);

                }




                break;

            case "gvMasterSubSet":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbRootSubStatus = (Label)e.Row.FindControl("lbRootSubStatus");
                    // Label Formidx = (Label)e.Row.FindControl("lbFormidx");
                    Label status_offline = (Label)e.Row.FindControl("status_offline");
                    Label status_online = (Label)e.Row.FindControl("status_online");

                    ViewState["status_R1subForm"] = lbRootSubStatus.Text;

                    if (ViewState["status_R1subForm"].ToString() == "0")
                    {
                        status_offline.Visible = true;
                    }
                    else if (ViewState["status_R1subForm"].ToString() == "1")
                    {
                        status_online.Visible = true;
                    }
                }

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    //TextBox tbupdateSubSetName = (TextBox)e.Row.FindControl("tbupdateSubSetName");
                    //getM0FormDetailList((DropDownList)e.Row.FindControl("ddlFormDetailUpdate"), tbFormDetailIDXUpdate.Text);

                }




                break;
        }

    }
    #endregion

    #region select 
    protected void selectR0FormCreate()
    {
        _data_qa_cims.qa_cims_r0_form_create_list = new qa_cims_r0_form_create_detail[1];
        qa_cims_r0_form_create_detail _selectForm = new qa_cims_r0_form_create_detail();
        _selectForm.condition = 1;
        _data_qa_cims.qa_cims_r0_form_create_list[0] = _selectForm;
        _data_qa_cims = callServicePostMasterQACIMS(_urlCimsGetR0FormCreate, _data_qa_cims);

        setGridData(gvMaster, _data_qa_cims.qa_cims_r0_form_create_list);

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_qa_cims.qa_cims_r0_form_create_list));

    }

    protected void selectR1RootFormCreate()
    {
        _data_qa_cims.qa_cims_r1_form_create_list = new qa_cims_r1_form_create_detail[1];
        qa_cims_r1_form_create_detail _selectForm = new qa_cims_r1_form_create_detail();
        _selectForm.condition = 1;
        _data_qa_cims.qa_cims_r1_form_create_list[0] = _selectForm;
        _data_qa_cims = callServicePostMasterQACIMS(_urlCimsGetR1FormCreate, _data_qa_cims);

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_qa_cims));

        if (litBack.Text == litDebug.Text)
        {
            var _linqSubSet = from data_qa_cims in _data_qa_cims.qa_cims_r1_form_create_list
                              where data_qa_cims.r1_form_root_idx == int.Parse(litBack.Text.ToString())
                              select data_qa_cims;
            setGridData(gvMasterSubSet, _linqSubSet.ToList()); 
        }

        else
        {
            var _linqSubSet = from data_qa_cims in _data_qa_cims.qa_cims_r1_form_create_list
                              where data_qa_cims.r1_form_root_idx == int.Parse(ViewState["r1_form_idx"].ToString())
                              select data_qa_cims;
            setGridData(gvMasterSubSet, _linqSubSet.ToList()); 
        }

        //setGridData(gvMasterSubSet, _data_qa_cims.qa_cims_r1_form_create_list);

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_linqSubSet.ToList()));

    }

    //protected void selectR1FormCreate()
    //{
    //    _data_qa_cims.qa_cims_r1_form_create_list = new qa_cims_r1_form_create_detail[1];
    //    qa_cims_r1_form_create_detail _selectForm = new qa_cims_r1_form_create_detail();
    //    _selectForm.condition = 1;
    //    _data_qa_cims.qa_cims_r1_form_create_list[0] = _selectForm;
    //    _data_qa_cims = callServicePostMasterQACIMS(_urlCimsGetR1FormCreate, _data_qa_cims);

    //    setGridData(gvR1FormCreate, _data_qa_cims.qa_cims_r1_form_create_list);

    //}

    protected void getR0FormList()
    {
        setFormData(fvformInsertRoot, FormViewMode.Insert, null);
        TextBox tbFormNameLevel1 = (TextBox)fvformInsertRoot.FindControl("tbFormNameLevel1");

        data_qa_cims _dataCimsR0Form = new data_qa_cims();
        _dataCimsR0Form.qa_cims_r0_form_create_list = new qa_cims_r0_form_create_detail[1];
        qa_cims_r0_form_create_detail _r0Formlist = new qa_cims_r0_form_create_detail();
        _r0Formlist.condition = 1;
        _dataCimsR0Form.qa_cims_r0_form_create_list[0] = _r0Formlist;
        _dataCimsR0Form = callServicePostMasterQACIMS(_urlCimsGetR0FormCreate, _dataCimsR0Form);

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataCimsR0Form.qa_cims_r0_form_create_list));

        var _linqSetFrom = (from dataqacims in _dataCimsR0Form.qa_cims_r0_form_create_list
                            where dataqacims.r0_form_create_idx == int.Parse(ViewState["r0_form_idx"].ToString())
                            select new
                            {
                                dataqacims.r0_form_create_name

                            }).ToList();

        //litDebug.Text = _linqSetFrom.ToString();
        foreach (var item1 in _linqSetFrom)
        {
            tbFormNameLevel1.Text = item1.r0_form_create_name;
        }

    }

    protected void getR1FormListRoot()
    {
        setFormData(fvformInsertSubFormDetail, FormViewMode.Insert, null);
        TextBox txtSubSetName = (TextBox)fvformInsertSubFormDetail.FindControl("txtSubSetName");




        data_qa_cims _dataCimsR1Form = new data_qa_cims();
        _dataCimsR1Form.qa_cims_r1_form_create_list = new qa_cims_r1_form_create_detail[1];
        qa_cims_r1_form_create_detail _r1Formlist = new qa_cims_r1_form_create_detail();
        _r1Formlist.condition = 1;
        _r1Formlist.r0_form_create_idx = int.Parse(ViewState["r0_form_idx"].ToString());
        _dataCimsR1Form.qa_cims_r1_form_create_list[0] = _r1Formlist;
        _dataCimsR1Form = callServicePostMasterQACIMS(_urlCimsGetR1FormCreate, _dataCimsR1Form);

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataCimsR1Form.qa_cims_r1_form_create_list) + int.Parse(ViewState["r1_form_idx"].ToString()));

        if (litBack.Text == litDebug.Text)
        {
            var _linqSetFrom = (from dataqacims in _dataCimsR1Form.qa_cims_r1_form_create_list
                                where dataqacims.r1_form_create_idx == int.Parse(litBack.Text.ToString())
                                select new
                                {
                                    dataqacims.form_detail_name

                                }).ToList();

            //litDebug.Text = _linqSetFrom.ToString();
            foreach (var item1 in _linqSetFrom)
            {
                txtSubSetName.Text = item1.form_detail_name;
            } 
        }
        else
        {
            var _linqSetFrom = (from dataqacims in _dataCimsR1Form.qa_cims_r1_form_create_list
                                where dataqacims.r1_form_create_idx == int.Parse(ViewState["r1_form_idx"].ToString())
                                select new
                                {
                                    dataqacims.form_detail_name

                                }).ToList();

            //litDebug.Text = _linqSetFrom.ToString();
            foreach (var item1 in _linqSetFrom)
            {
                txtSubSetName.Text = item1.form_detail_name;
            }
        }

    }

    protected void getR1FormList()
    {

        //data_qa_cims _dataTopic = new data_qa_cims();
        //_dataTopic.qa_cims_r0_form_create_list = new qa_cims_r0_form_create_detail[1];
        //qa_cims_r0_form_create_detail _topic = new qa_cims_r0_form_create_detail();

        //_topic.r0_form_create_idx = int.Parse(ViewState["r0_form_idx"].ToString());
        //_dataTopic.qa_cims_r0_form_create_list[0] = _topic;

        //_dataTopic = callServicePostMasterQACIMS(_urlCimsGetR1FormCreate, _dataTopic);
        //if (_dataTopic.return_code == 0)
        //{
        //    setGridData(gvR1FormCreate, _dataTopic.qa_cims_r0_form_create_list);

        //}

        data_qa_cims _dataTopic = new data_qa_cims();
        _dataTopic.qa_cims_r1_form_create_list = new qa_cims_r1_form_create_detail[1];
        qa_cims_r1_form_create_detail _topic = new qa_cims_r1_form_create_detail();

        _topic.r0_form_create_idx = int.Parse(ViewState["r0_form_idx"].ToString());
        _dataTopic.qa_cims_r1_form_create_list[0] = _topic;

        _dataTopic = callServicePostMasterQACIMS(_urlCimsGetR1FormCreate, _dataTopic);
        //if (_dataTopic.return_code == 0)
        //{
        //    setGridData(gvR1FormCreate, _dataTopic.qa_cims_r1_form_create_list);

        //}

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataTopic.qa_cims_r1_form_create_list)) + _topic.r0_form_create_idx + "   " + int.Parse(ViewState["r0_form_idx"].ToString());
        setGridData(gvR1FormCreate, _dataTopic.qa_cims_r1_form_create_list);
    }



    #endregion

    #region reuse
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void initPage()
    {

        //clearSession();
        //clearViewState();

        setActiveView("pageGenaral", 0);
        setFormData(fvformInsert, FormViewMode.ReadOnly, null);
        //  selectFormDetail();
    }

    protected void setActiveView(string activeTab, int uidx)
    {
        mvMaster.SetActiveView((View)mvMaster.FindControl(activeTab));

        switch (activeTab)
        {
            case "pageGenaral":

                break;

            case "pageCreateRoot":

                getR0FormList();
                break;

            case "pageRootSubSet":

                getR1FormListRoot();
                //selectR1RootFormCreate();

                break;

        }

    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }

    protected data_qa_cims callServicePostMasterQACIMS(string _cmdUrl, data_qa_cims _data_qa_cims)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_qa_cims);
        // litDebug.Text = _localJson;

        // call services
        // _localJson = _funcTool.callServicePost(_cmdUrl + _localJson);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_qa_cims = (data_qa_cims)_funcTool.convertJsonToObject(typeof(data_qa_cims), _localJson);

        return _data_qa_cims;
    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger1 = new PostBackTrigger();
        trigger1.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger1);
    }

    #endregion
}