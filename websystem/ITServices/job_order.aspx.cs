﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;

using System.Globalization;
using System.Text.RegularExpressions;
public partial class websystem_ITServices_Default : System.Web.UI.Page
{
    # region con
    function_tool _funcTool = new function_tool();
    data_jo _data_jo = new data_jo();
    data_employee _dataEmployee = new data_employee();

    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlSelectDatajoborder_mishen = _serviceUrl + ConfigurationManager.AppSettings["urlSelectDatajoborder_mishen"];
    static string _urlInsertDatajoborder = _serviceUrl + ConfigurationManager.AppSettings["urlInsertDatajoborder"];
    static string _urlUpdateDatajoborder = _serviceUrl + ConfigurationManager.AppSettings["urlUpdateDatajoborder"];
    static string _urlUpdateDatajobordersta = _serviceUrl + ConfigurationManager.AppSettings["urlUpdateDatajobordersta"];
    static string _urlSelectDatajoborder_headlook = _serviceUrl + ConfigurationManager.AppSettings["urlSelectDatajoborder_headlook"];
    static string _urlInsertu1 = _serviceUrl + ConfigurationManager.AppSettings["urlInsertu1"];
    static string _urlSelectU1ov = _serviceUrl + ConfigurationManager.AppSettings["urlSelectU1ov"];
    static string _urlSelectheadseehome = _serviceUrl + ConfigurationManager.AppSettings["urlSelectheadseehome"];
    static string _urlUpdateu1end = _serviceUrl + ConfigurationManager.AppSettings["urlUpdateu1end"];
    static string _urlUpdatestau0 = _serviceUrl + ConfigurationManager.AppSettings["urlUpdatestau0"];
    static string _urlUpdatestau1_ov = _serviceUrl + ConfigurationManager.AppSettings["urlUpdatestau1_ov"];
    static string _urlSelectDatajoborder_listheadsee = _serviceUrl + ConfigurationManager.AppSettings["urlSelectDatajoborder_listheadsee"];
    static string _urlSelectMIS_share = _serviceUrl + ConfigurationManager.AppSettings["urlSelectMIS_share"];
    static string _urlUpdat_statusdeleate = _serviceUrl + ConfigurationManager.AppSettings["urlUpdat_statusdeleate"];
    static string _urlUpdateu1naiend = _serviceUrl + ConfigurationManager.AppSettings["urlUpdateu1naiend"];
    static string _urlSelectU1ov_v2 = _serviceUrl + ConfigurationManager.AppSettings["urlSelectU1ov_v2"];
    static string _urlUpdateu1_userok = _serviceUrl + ConfigurationManager.AppSettings["urlUpdateu1_userok"];
    static string _urlUpdateu1_userno = _serviceUrl + ConfigurationManager.AppSettings["urlUpdateu1_userno"];
    static string _urlUpdateu1misreturn_editsucc = _serviceUrl + ConfigurationManager.AppSettings["urlUpdateu1misreturn_editsucc"];
    static string _urlUp_END = _serviceUrl + ConfigurationManager.AppSettings["urlUp_END"];
    static string _urlInsertlog_Vaddtitle = _serviceUrl + ConfigurationManager.AppSettings["urlInsertlog_Vaddtitle"];
    static string _urlUpdatelastuser = _serviceUrl + ConfigurationManager.AppSettings["urlUpdatelastuser"];
    static string _urlSelectlogu1 = _serviceUrl + ConfigurationManager.AppSettings["urlSelectlogu1"];
    static string _urlSelectWhologin = _serviceUrl + ConfigurationManager.AppSettings["urlSelectWhologin"];
    static string _urlSelectanywork = _serviceUrl + ConfigurationManager.AppSettings["urlSelectanywork"];


    static string _urlSelectDatajoborder = _serviceUrl + ConfigurationManager.AppSettings["urlSelectDatajoborder"];
    static string _urlEditDatajoborder = _serviceUrl + ConfigurationManager.AppSettings["urlEditDatajoborder"];
    static string _urlDeleteDatajoborder = _serviceUrl + ConfigurationManager.AppSettings["urlDeleteDatajoborder"];
    static string _urlSelectDatajoborder_mhead = _serviceUrl + ConfigurationManager.AppSettings["urlSelectDatajoborder_mhead"];
    static string _urlSelectDatajoborder_log = _serviceUrl + ConfigurationManager.AppSettings["urlSelectDatajoborder_log"];
    static string _urlUpdatedaystartfinal = _serviceUrl + ConfigurationManager.AppSettings["urlUpdatedaystartfinal"];
    static string _urlSelectdaystartfinal = _serviceUrl + ConfigurationManager.AppSettings["urlSelectdaystartfinal"];
    static string _urlSelect_mishead = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_mishead"];
    static string _urlInsertloggaritum = _serviceUrl + ConfigurationManager.AppSettings["urlInsertloggaritum"];
    static string _urlSearchJoborder = _serviceUrl + ConfigurationManager.AppSettings["urlSearchJoborder"];

    //employee//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];

    // string _localJson = "";

    int idx_doc_u0 = 0;
    bool _flag_qmr = false;
    int[] rdept_qmr = { 20 };

    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
        //check permission
        foreach (int item in rdept_qmr)
        {
            if (_dataEmployee.employee_list[0].rdept_idx == item)
            {
                _flag_qmr = true;
                break;
            }
        }
       

    }

    #endregion

    #region callScallServiceEmployee
    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }
    #endregion

    #region EmployeeProfile
    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;

        ViewState["Sec_idx"] = _dataEmployee.employee_list[0].rsec_idx;
        ViewState["_rdept_idx"] = _dataEmployee.employee_list[0].rdept_idx;
        ViewState["_jobgrade_level"] = _dataEmployee.employee_list[0].jobgrade_level;
        ViewState["_org_idx"] = _dataEmployee.employee_list[0].org_idx.ToString();
    }

    #endregion
  
    # region Pagelode
    protected void Page_Load(object sender, EventArgs e)
    {
        //idx_doc_u0 = int.Parse(Session["emp_id"].ToString());  --// id ของคนที่มาสร้าง

        linkBtnTrigger(about_rdep);
        linkBtnTrigger(create_story_rdep);
        linkBtnTrigger(all_page_dredp);

        linkBtnTrigger(trueadd);   
        
        var dtviewmis = new DataSet();
        dtviewmis.Tables.Add("Equipment");

        dtviewmis.Tables[0].Columns.Add("title_1", typeof(string));
        dtviewmis.Tables[0].Columns.Add("timeline", typeof(string));
        dtviewmis.Tables[0].Columns.Add("emp_okstory", typeof(string));
        dtviewmis.Tables[0].Columns.Add("dead_line", typeof(string));
        dtviewmis.Tables[0].Columns.Add("u0_jo_idx", typeof(int));

        
        if (ViewState["showdatanaja"] == null)
        {
            ViewState["showdatanaja"] = dtviewmis;
        }

        if (!IsPostBack)
        {
            //seach secinsert
         //   getSectionList_secmis(ddlinsert_sec, 1, 20);


            //seach deptjob
            getSectionList_dept(ddSec_dept, int.Parse(ViewState["_org_idx"].ToString()), int.Parse(ViewState["_rdept_idx"].ToString()));

            //seach alljob
            getOrganizationList(ddOrg_seachall);
            getDepartmentList(ddDept_seachall, int.Parse(ddOrg_seachall.SelectedValue));
            getSectionList(ddSec_seachall, int.Parse(ddOrg_seachall.SelectedValue), int.Parse(ddDept_seachall.SelectedValue));

          



            ViewState["EMP_HUMAN"] = int.Parse(Session["emp_idx"].ToString());
            ViewState["_jobgrade_level_misHead"] = 6; //กำหนดระดับของ หัวหน้าฝ่าย mis

           // test_l.Text = ViewState["Sec_idx"].ToString();

            data_jo B_id_emp_login = new data_jo();
            job_user S_id_emp_login = new job_user();
            B_id_emp_login.job_user_action = new job_user[1];

            S_id_emp_login.emp_login = int.Parse(ViewState["EMP_HUMAN"].ToString());

            B_id_emp_login.job_user_action[0] = S_id_emp_login;
            B_id_emp_login = callService(_urlSelectWhologin, B_id_emp_login);
           // ViewState["_rdept_idx"] = B_id_emp_login.job_user_action[0].rdept_idx.ToString();      //ไว้ เช็คว่าอยู๋ฝ่าย เดียวกันไหม
           // ViewState["_jobgrade_level"] = B_id_emp_login.job_user_action[0].jobgrade_level.ToString();         // เป็นหัวหน้าไหม
            //  ViewState["_org_idx"] = B_id_emp_login.job_user_action[0].org_idx.ToString();   //อยู่ไหน
         //   ViewState["_rsec_idx"] = B_id_emp_login.job_user_action[0].rsec_idx.ToString();  //แผนกอะไร

            
            //test_l.Text = ViewState["EMP_HUMAN"].ToString();
            //tangura.Text = _emp_idx.ToString();

           // gethead();
           // getmishead();
           
           // jogrid2.DataBind();

            //u1grid.DataBind();
            //create.Visible = true;

           // grid.Visible = true;

            jogrid.DataBind();
            //  searchbutton.Visible = true;
            //    btnshow_searchall.Visible = true;

            //  selectjob();
            //  pageall_work();

            if (int.Parse(ViewState["_rdept_idx"].ToString()) == 20)
            {
                setActiveTab("view1", 0);
                li_all_page_dredp.Visible = true;

            }
            else
            {
                setActiveTab("view1", 0);
                li_all_page_dredp.Visible = false;
            }
            linkBtnTrigger(trueadd);
        }
        inputshow_table();


    }
    #endregion

    #region ClearTable
    private void ClearTable(DataTable table)
    {
        try
        {
            table.Clear();
        }
        catch (DataException e)
        {
            // Process exception and return.
            Console.WriteLine("Exception of type {0} occurred.",
                e.GetType());
        }

    }
    #endregion

    #region btn_all
    protected void btnallback(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {
            case "btnbackalllist":

                all_page_page();
                viewhead_btn();
                create_Story_btn();
                view_mis_btn_no_sha();
                viewmis_btn();
                view1_btn();
                view3_btn();
                view4_btn();
                LinkButton1.Visible = false;
                LinkButton2.Visible = false;
              //  btnview3_to2.Visible = true;
                Linkback_head.Visible = true;
                LinkBu_back_missh.Visible = true;
              //  pageall_work();

                ViewState["_no_invoice"] = null;
                Gv_filemisshared.DataSource = ViewState["_no_invoice"];
                Gv_filemisshared.DataBind();

                setActiveTab("viewall_pages",0);
                multimaster.SetActiveView(viewall_pages);

                break;
            case "btnbackhead":
                plhead.Visible = true;
                divformhead.Visible = false;
                //Page.Response.Redirect(Page.Request.Url.ToString(), true);
                
                break;
            //case "btnbackmis":
            //    //getmishead();
            //    //pl_titleinsert.Visible = true;
            //    //title_indb.Text = null;
            //    //start_requiremis.Text = null;

            //    viewmis_btn();
            //    view_mis_btn_no_sha();
            //    title_indb.Text = null;
            //    start_requiremis.Text = null;
            //    getmishead();
            //    pl_titleinsert.Visible = true;

            //    ViewState["_no_invoice"] = null;
            //    Gv_filemisshared.DataSource = ViewState["_no_invoice"];
            //    Gv_filemisshared.DataBind();

            //    setActiveTab("viewmis", 0);
            //    multimaster.SetActiveView(viewmis);


            //    //Page.Response.Redirect(Page.Request.Url.ToString(), true);
            //    break;
          
            //case "back":
            //    multimaster.SetActiveView(View2);
            //  //  Page.Response.Redirect(Page.Request.Url.ToString(), true);
            //    selectmheadjob();
                //break;
            case "backall":

                view1_btn();
                //selectjob();
             //   pageall_work();
                searchbutton.Visible = true;
                searchhidden.Visible = false;
                _divsearch.Visible = false;
              //  pl_titleinsert.Visible = true;

                gvanthoer_old.DataSource = null;
                gvanthoer_old.DataBind();

                gv_logold.DataSource = null;
                gv_logold.DataBind();

                // Page.Response.Redirect(Page.Request.Url.ToString(), true);
                setActiveTab("viewall_pages", 0);
                multimaster.SetActiveView(viewall_pages);
                break;
            case "back2":

                view1_btn();
                //  selectjob();
              //  jogrid.DataBind();
                searchbutton.Visible = true;
                searchhidden.Visible = false;
                _divsearch.Visible = false;

              //  pl_titleinsert.Visible = true;

                gvanthoer_old.DataSource = null;
                gvanthoer_old.DataBind();

                gv_logold.DataSource = null;
                gv_logold.DataBind();

                // Page.Response.Redirect(Page.Request.Url.ToString(), true);
                setActiveTab("View1", 0);
                multimaster.SetActiveView(View1);

                break;
        }
    }

    protected void btncommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {
            case "submit2":
                multimaster.SetActiveView(viewhead);

                plhead.Visible = true;
                break;
            case "submit3":


                multimaster.SetActiveView(viewmis);
                plmis.Visible = true;

                break;
            //case "lbcreate":
            //    multimaster.SetActiveView(viewcreate);
            //    break;

            case "lookhead":

                string[] argument = new string[3];
                argument = e.CommandArgument.ToString().Split(';');
                int idx_jo_look = int.Parse(argument[0]);
                string no_invoice = argument[1];
                int viewclose = int.Parse(argument[2]);

                //test_l.Text = viewclose.ToString();

                ViewState["u0_jo_idx"] = idx_jo_look;
                ViewState["_no_invoice1"] = no_invoice;
                tostid.Text = idx_jo_look.ToString();

                data_jo box111 = new data_jo();
                job_order_overview box222 = new job_order_overview();
                box111.job_order_overview_action = new job_order_overview[1];

                box222.u0_jo_idx = idx_jo_look;

                box111.job_order_overview_action[0] = box222;
                // oak1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(box));

                box111 = callService(_urlSelectDatajoborder_headlook, box111);
                formviewhead.DataSource = box111.job_order_overview_action;
                formviewhead.DataBind();

                ViewState["_rdep_num"] = box111.job_order_overview_action[0].rdep_num.ToString();     // แผนก คนสร้าง
                ViewState["_job_grade_cre"] = box111.job_order_overview_action[0].job_grade_cre.ToString();   // ระดับ
                ViewState["_org_idx_cre"] = box111.job_order_overview_action[0].org_idx_cre.ToString();  // คนสร้างที่ไหน เถ้าแก่,โทบิ มั้ง
                ViewState["_require_jo"] = box111.job_order_overview_action[0].require_jo.ToString();


                try
                {
                    string getPathLotus = ConfigurationManager.AppSettings["pathfile_Job_order"];

                    string filePathLotus = Server.MapPath(getPathLotus + ViewState["_no_invoice1"].ToString());
                    DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);

                    SearchDirectories_Vhead(myDirLotus, ViewState["_no_invoice1"].ToString());
                }
                catch
                {
                }




                if (int.Parse(ViewState["_org_idx_cre"].ToString()) == int.Parse(ViewState["_org_idx"].ToString()))
                {
                    if (int.Parse(ViewState["_rdept_idx"].ToString()) == int.Parse(ViewState["_rdep_num"].ToString()))
                    {
                        if (int.Parse(ViewState["_jobgrade_level"].ToString()) > 5)
                        {
                            multimaster.SetActiveView(viewhead);

                            Panel _divformhead = (Panel)viewhead.FindControl("divformhead");
                            FormView fmbigmak = (FormView)_divformhead.FindControl("formviewhead");
                            Panel _view_dep = (Panel)fmbigmak.FindControl("view_dep");
                            Panel _viewall = (Panel)fmbigmak.FindControl("viewall");

                            if (viewclose == 1)
                            {
                                _view_dep.Visible = true;

                            }
                            else
                            {
                                _view_dep.Visible = false;

                            }
                            if (viewclose == 2)
                            {
                                _viewall.Visible = true;

                            }
                            else
                            {
                                _viewall.Visible = false;
                            }
                            //var _Vokgogo = (LinkButton)fmbigmak.FindControl("Vokgogo");
                            //var _Vnookgo = (LinkButton)fmbigmak.FindControl("Vnookgo");

                            //TextBox _testtest = (TextBox)fmbigmak.FindControl("testtest");
                            //TextBox _requiredoc = (TextBox)fmbigmak.FindControl("requiredoc");
                            Label _oak007 = (Label)fmbigmak.FindControl("oak007");
                            _oak007.Text = HttpUtility.HtmlDecode(ViewState["_require_jo"].ToString());
                            //_requiredoc.Text = _oak007.Text;

                            //_Vokgogo.Visible = true;
                            //_Vnookgo.Visible = true;

                            ////divshowselect.Visible = false;
                            plhead.Visible = false;
                            divformhead.Visible = true;
                         
                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('บุคคลทั่วไป ไม่สามารถใช้งานได้');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('บุคคลจากแผนกอื่น ไม่สามารถใช้งานได้');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('บุคคลากรบริษัทอื่น ไม่สามารถใช้งานได้');", true);
                }
                //ViewState["demeted"] = box111.job_order_overview_action[0].to_m0_actor.ToString();
                //if (ViewState["demeted"].ToString() == "2")
                //{
                //    FormView fmbigmak = (FormView)viewhead.FindControl("formviewhead");
                //    var _Vokgogo = (LinkButton)fmbigmak.FindControl("Vokgogo");
                //    var _Vnookgo = (LinkButton)fmbigmak.FindControl("Vnookgo");
                //    _Vokgogo.Visible = true;
                //    _Vnookgo.Visible = true;
                //}

                SETFOCUS_ONTOP.Focus();
                break;
            case "lookmis":

                gvanthoer_old.EditIndex = -1;

                gvinfv_showtitle.Visible = false;

                saveall.Visible = false;
                rdbt_end.Checked = false;

                save_viewdep.Visible = false;
                rdbt_end.Visible = false;
            

                //action_select_history();
                Panel _pl_titleinsert = (Panel)viewmisshare.FindControl("pl_titleinsert");
                TextBox _title_indb = (TextBox)_pl_titleinsert.FindControl("title_indb");

                _title_indb.Text = null;
                 start_requiremis.Text = null;
            
                ViewState["showdatanaja"] = null;
                gvinfv_showtitle.DataSource = ViewState["showdatanaja"];
                gvinfv_showtitle.DataBind();
                string[] arraynaja_rub = new string[4];
                arraynaja_rub = e.CommandArgument.ToString().Split(';');
                int databox_1 = int.Parse(arraynaja_rub[0]);
                int databox_2 = int.Parse(arraynaja_rub[1]);
                string no_inV = arraynaja_rub[2];
                int view_swap = int.Parse(arraynaja_rub[3]);

                if (view_swap == 1)
                {
                    save_view1.Visible = true;
                }
                else
                {
                    save_view1.Visible = false;

                }
                if (view_swap == 0)
                {
                    save_allpage.Visible = true;

                }
                else
                {
                    save_allpage.Visible = false;
                }

                TextBox main_id_to_sql = (TextBox)viewmisshare.FindControl("main_id_to_sql");
                var idx_jo_mis = databox_1;
                ViewState["u0_jo_idx"] = idx_jo_mis;
                main_id_to_sql.Text = ViewState["u0_jo_idx"].ToString();
                ViewState["_no_invoice"] = no_inV;

                    try
                    {

                        string getPathLotus = ConfigurationManager.AppSettings["pathfile_Job_order"];

                        string filePathLotus = Server.MapPath(getPathLotus + ViewState["_no_invoice"].ToString());
                        DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);

                        SearchDirectories_Vmisshare(myDirLotus, ViewState["_no_invoice"].ToString());

                       // test_l.Text = myDirLotus.ToString();
                    }
                    catch
                    {

                    } 

                

                data_jo bigbox = new data_jo();
                job_order_overview smallbox = new job_order_overview();
                bigbox.job_order_overview_action = new job_order_overview[1];

                smallbox.u0_jo_idx = idx_jo_mis;

                bigbox.job_order_overview_action[0] = smallbox;
                // test.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(bigbox));
                bigbox = callService(_urlSelectDatajoborder_headlook, bigbox);
                ViewState["rdep_num_usena"] = bigbox.job_order_overview_action[0].rdep_num.ToString();
                ViewState["_org_idx_cre_mis"] = bigbox.job_order_overview_action[0].org_idx_cre.ToString();

                ViewState["day_sta"] = bigbox.job_order_overview_action[0].day_start.ToString();
                ViewState["day_fin"] = bigbox.job_order_overview_action[0].day_finish.ToString();
                ViewState["EMP_Creator"] = bigbox.job_order_overview_action[0].emp_id_creator.ToString();
                ViewState["_rsec_creator"] = bigbox.job_order_overview_action[0].rsec_creator.ToString();
                ViewState["_rsec_mis"] = bigbox.job_order_overview_action[0].sec_mis.ToString();

                //tangura.Text = ViewState["_rsec_mis"].ToString();
                //test_l.Text = sec_mis_look.ToString();


                //   start_requiremis.Text = ViewState["day_sta"].ToString();
                //  test_l.Text = ViewState["day_sta"].ToString();

                //string[] time_start = new string[3];
                //time_start = ViewState["day_sta"].ToString().Split('/');
                //string time_sta_day = time_start[0];
                //string time_sta_month = time_start[1];
                //string time_sta_year = time_start[2];

                //string[] timenows = new string[3];
                //string timenow_reals = DateTime.Now.ToString("dd/MM/yyyy");
                //timenows = timenow_reals.Split('/');
                //string timenow_days = timenows[0];
                //string timenow_months = timenows[1];
                //string timenow_years = timenows[2];
                //ViewState["timething"] = "0";
                //if (int.Parse(timenow_years) < int.Parse(time_sta_year))
                //{
                //    ViewState["timething"] = "1";
                //}
                //else if (int.Parse(timenow_years) == int.Parse(time_sta_year))
                //{
                //    if (int.Parse(timenow_months) < int.Parse(time_sta_month))
                //    {
                //        ViewState["timething"] = "1";
                //    }
                //    else if (int.Parse(timenow_months) == int.Parse(time_sta_month))
                //    {
                //        if (int.Parse(timenow_days) < int.Parse(time_sta_day))
                //        {
                //            ViewState["timething"] = "1";
                //        }
                //    }
                //}

                string[] time_fin = new string[3];
                time_fin = ViewState["day_fin"].ToString().Split('/');
                string time_fin_day = time_fin[0];
                string time_fin_month = time_fin[1];
                string time_fin_year = time_fin[2];

                string[] timenow = new string[3];
                string timenow_real = DateTime.Now.ToString("dd/MM/yyyy");
                timenow = timenow_real.Split('/');
                string timenow_day = timenow[0];
                string timenow_month = timenow[1];
                string timenow_year = timenow[2];
                ViewState["timething"] = "0";
                if (int.Parse(timenow_year) > int.Parse(time_fin_year))
                {
                    ViewState["timething"] = "1";
                }
                else if (int.Parse(timenow_year) == int.Parse(time_fin_year))
                {
                    if (int.Parse(timenow_month) > int.Parse(time_fin_month))
                    {
                        ViewState["timething"] = "1";
                    }
                    else if (int.Parse(timenow_month) == int.Parse(time_fin_month))
                    {
                        if (int.Parse(timenow_day) > int.Parse(time_fin_day))
                        {
                            ViewState["timething"] = "1";
                        }
                    }
                }
                ViewState["id_lookmis"] = 0;
                if (ViewState["_rdept_idx"].ToString() == "20")
                {
                  
                    ViewState["id_lookmis"] = 1;

                }
                

                if (int.Parse(ViewState["_org_idx_cre_mis"].ToString()) == int.Parse(ViewState["_org_idx"].ToString()))
                {
                    ViewState["u0_jo_jo"] = bigbox.job_order_overview_action[0].u0_jo_idx.ToString();
                    ViewState["id_stad"] = bigbox.job_order_overview_action[0].id_statework.ToString();
                    ViewState["keepto_n"] = bigbox.job_order_overview_action[0].to_m0_node.ToString();
                    ViewState["keepto_a"] = bigbox.job_order_overview_action[0].to_m0_actor.ToString();

                    Label2.Text = ViewState["u0_jo_idx"].ToString();
                    Label3.Text = ViewState["keepto_n"].ToString();
                    Label4.Text = ViewState["keepto_a"].ToString();

                    if (bigbox.job_order_overview_action != null)
                    {
                        ViewState["t"] = bigbox.job_order_overview_action[0].title_jo.ToString();
                        ViewState["_id_emprequire"] = bigbox.job_order_overview_action[0].Codeemp.ToString();
                        ViewState["_name_emprequire"] = bigbox.job_order_overview_action[0].AdminMain.ToString();
                        ViewState["_company_emprequire"] = bigbox.job_order_overview_action[0].Organize.ToString();
                        ViewState["_faction_emprequire"] = bigbox.job_order_overview_action[0].Deptname.ToString();
                        ViewState["_department_emprequire"] = bigbox.job_order_overview_action[0].Secname.ToString();
                        ViewState["_position_emprequire"] = bigbox.job_order_overview_action[0].Posname.ToString();

                        if (bigbox.job_order_overview_action[0].emp_mobile_no.ToString() != null)
                        {
                            ViewState["_tel_emprequire"] = bigbox.job_order_overview_action[0].emp_mobile_no.ToString();
                        }
                      else 
                        {
                            ViewState["_tel_emprequire"] = "-";
                        }

                        if (bigbox.job_order_overview_action[0].emp_email.ToString() != null)
                        {
                            ViewState["_mail_emprequire"] = bigbox.job_order_overview_action[0].emp_email.ToString();
                        }
                        else
                        {
                            ViewState["_mail_emprequire"] = "-";
                        }

                        if (bigbox.job_order_overview_action[0].costcenter_no.ToString() != null)
                        {
                            ViewState["_cost_emprequire"] = bigbox.job_order_overview_action[0].costcenter_no.ToString();
                        }
                        else
                        {
                            ViewState["_cost_emprequire"] = "-";
                        }

                      //  ViewState["_cost_emprequire"] = bigbox.job_order_overview_action[0].costcenter_no.ToString(); //costcenter_no
                        ViewState["_lb_titleshow"] = bigbox.job_order_overview_action[0].no_invoice.ToString();
                        ViewState["_lb_daycre"] = bigbox.job_order_overview_action[0].day_created.ToString();
                        ViewState["_lb_daystart"] = bigbox.job_order_overview_action[0].day_start.ToString();
                        ViewState["_lb_dayfinish"] = bigbox.job_order_overview_action[0].day_finish.ToString();
                        //ViewState["_lb_dayamount"] = bigbox.job_order_overview_action[0].day_amount.ToString();
                        ViewState["_tb_er"] = bigbox.job_order_overview_action[0].require_jo.ToString();

                        nameisthis.Text = ViewState["t"].ToString();
                        id_emprequire.Text = ViewState["_id_emprequire"].ToString();
                        name_emprequire.Text = ViewState["_name_emprequire"].ToString();
                        company_emprequire.Text = ViewState["_company_emprequire"].ToString();
                        faction_emprequire.Text = ViewState["_faction_emprequire"].ToString();
                        department_emprequire.Text = ViewState["_department_emprequire"].ToString();
                        position_emprequire.Text = ViewState["_position_emprequire"].ToString();
                        tel_emprequire.Text = ViewState["_tel_emprequire"].ToString();
                        mail_emprequire.Text = ViewState["_mail_emprequire"].ToString();
                        cost_emprequire.Text = ViewState["_cost_emprequire"].ToString();

                        lb_titleshow.Text = ViewState["_lb_titleshow"].ToString();
                        lb_daycre.Text = ViewState["_lb_daycre"].ToString();
                        lb_daystart.Text = ViewState["_lb_daystart"].ToString();
                        lb_dayfinish.Text = ViewState["_lb_dayfinish"].ToString();
                        tb_er_Vlb.Text = HttpUtility.HtmlDecode(ViewState["_tb_er"].ToString());

                        ViewState["num_last"] = bigbox.job_order_overview_action[0].id_statework.ToString();
                        ViewState["end_job"] = bigbox.job_order_overview_action[0].end_send.ToString();
                    }
                    ViewState["Underdog"] = bigbox.job_order_overview_action[0].id_statework.ToString();
                    if (ViewState["num_last"].ToString() == "5")
                    {
                        nannasi.Visible = false;
                    }
                    else
                    {
                        nannasi.Visible = true;
                    }
                    /////////////////////// จบจ้า /////////////////////////
                    time_present_use.Text = ViewState["_lb_dayfinish"].ToString();

                    data_jo databig = new data_jo();
                    jo_add_datalist datasmall = new jo_add_datalist();
                    databig.jo_add_datalist_action = new jo_add_datalist[1];

                    datasmall.wait = int.Parse(ViewState["u0_jo_idx"].ToString());



                    databig.jo_add_datalist_action[0] = datasmall;
                    //dog1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(databig));
                      databig = callService(_urlSelectU1ov, databig);
                    setGridData(gvanthoer_old, databig.jo_add_datalist_action);

                 //   ViewState["_u1_jo_idx"] = databig.jo_add_datalist_action[0].u1_jo_idx.ToString();


                    data_jo B_logmis = new data_jo();
                    jo_log S_logmis = new jo_log();
                    B_logmis.jo_log_action = new jo_log[1];

                    S_logmis.l0_jo_idx_ref = int.Parse(ViewState["u0_jo_idx"].ToString());

                    B_logmis.jo_log_action[0] = S_logmis;
                    //despacito.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(B_logselect));
                    B_logmis = callService(_urlSelectlogu1, B_logmis);

                    ViewState["log_jobU1"] = B_logmis.jo_log_action;
                    setGridData(gv_logold, ViewState["log_jobU1"]);

                    multimaster.SetActiveView(viewmisshare);

                    int count = gvanthoer_old.Rows.Count;
                    if (count == 0)
                    {
                        nannasi.Visible = false;
                    }
                    int m = 0;
                    int sum = 0;

                    for (m = 0; m < count; m++)
                    {
                        ViewState["oaknana"] = databig.jo_add_datalist_action[m].u1_status.ToString();
                        sum += int.Parse(ViewState["oaknana"].ToString());
                    }
                    ViewState["sumallde"] = sum;    
                    ViewState["use"] = bigbox.job_order_overview_action[0].id_statework.ToString();
                    ViewState["enn_send"] = bigbox.job_order_overview_action[0].end_send.ToString();

                   // test_l.Text = ViewState["enn_send"].ToString();

                    if (ViewState["use"].ToString() == "5")  //ถ้าเป็นครั้งแรก
                    {
                        successwork.Visible = false;
                        //nannasi.Visible = false;
                    }
                    else if (ViewState["enn_send"].ToString() == "1")  //ถ้าถูกส่งไปละ ครบละนะด้วย //ViewState["use"].ToString() == "8"
                    {
                        //ViewState["id_staworkend"] = bigbox.job_order_overview_action[0].id_statework.ToString();
                    //    test_l.Text = "34444";
                        pl_titleinsert.Visible = false;

                        //  successwork.Visible = true;
                        if (sum < 2 * count)
                        {

                        }
                        else if (sum == 2 * count)
                        {
                            if (ViewState["_rdept_idx"].ToString() == "20")
                            {
                                if (ViewState["Sec_idx"].ToString() != ViewState["_rsec_creator"].ToString())
                                {

                                    //successwork.Visible = true;
                                }
                                else if (ViewState["Sec_idx"].ToString() == ViewState["_rsec_creator"].ToString())
                                {
                                    successwork.Visible = false;

                                }

                            }
                            //successwork.Visible = true;
                            if (ViewState["use"].ToString() == "4")
                            {
                                successwork.Visible = false;
                            }
                        }
                    }

                    if (ViewState["Sec_idx"].ToString() == ViewState["_rsec_creator"].ToString() || ViewState["Sec_idx"].ToString() != ViewState["_rsec_mis"].ToString()) //จำกัดสิทธิเข้าส่งมอบงาน
                    {
                        //tangura.Text = ViewState["_rsec_mis"].ToString();

                        pl_titleinsert.Visible = false;
                    }                   
                    else if (ViewState["use"].ToString() == "4" || ViewState["use"].ToString() == "8")
                    {
                        pl_titleinsert.Visible = false;
                    }
                    else if (ViewState["use"].ToString() == "5")
                    {
                        successwork.Visible = false;
                    }
                    //nameisthis

                    if (ViewState["id_lookmis"].ToString() == "0")
                    {
                        pl_titleinsert.Visible = false;
                        successwork.Visible = false;
                    }
                    if (ViewState["use"].ToString() == "4")
                    {
                        successwork.Visible = false;
                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('บุคคลากรบริษัทอื่น ไม่สามารถใช้งานได้');", true);
                }

                SETFOCUS_ONTOP.Focus();
                break;
        }
    }

    protected void btnonclick(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();


        jo_add_datalist creatnewsi = new jo_add_datalist();

        switch (cmdName)
        {
            case "cancelbu":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "deletenaja":
                int count = gvanthoer_old.Rows.Count;
                int numsum = int.Parse(ViewState["sumallde"].ToString());
                if (count == 1)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ข้อมูลสุดท้ายแล้วห้ามลบ');", true);
                }
                else
                {
                    string[] arraynaja = new string[3];
                    arraynaja = e.CommandArgument.ToString().Split(';');

                    int databox = int.Parse(arraynaja[0]);
                    int databox2 = int.Parse(arraynaja[1]);
                    string databox1 = arraynaja[2];
                    int indexdelete = int.Parse(arraynaja[3]);
                    string textcommentdelete = String.Empty;


                    foreach (GridViewRow row in gvanthoer_old.Rows)
                    {
                        if (row.RowType == DataControlRowType.DataRow)
                        {
                            int countcommentdelete = row.RowIndex;

                            TextBox txt_commentu1delete = row.Cells[6].FindControl("txt_commentu1") as TextBox;
                            if (indexdelete == countcommentdelete)
                            {

                                if (txt_commentu1delete.Text == "")
                                {
                                    textcommentdelete = "-";


                                }
                                else
                                {
                                    textcommentdelete = txt_commentu1delete.Text;
                                }

                            }
                            else
                            {
                                //test_l.Text = Convert.ToString(countcomment); ;
                            }
                        }
                    }



                    data_jo Probox = new data_jo();
                    jo_add_datalist smbox = new jo_add_datalist();
                    Probox.jo_add_datalist_action = new jo_add_datalist[1];

                    smbox.u1_jo_idx = databox;
                    //wowo.Text = databox.ToString();
                    Probox.jo_add_datalist_action[0] = smbox;

                    Probox = callService(_urlUpdat_statusdeleate, Probox);
                    // ลบแล้ว ถ้าลบหมด จะ ทำให้ไม่เห็น จึงเอาไปเช็ค ใน u1 ว่า ไอที่เรากดไป = 1 ไหม ถ้า ไม่ ก็ แสดงว่าลบหมด จะทำ update u0 id_status เป็น 5
                    //if ()
                    data_jo B_deletenaja = new data_jo();
                    jo_log S_deletenaja = new jo_log();
                    B_deletenaja.jo_log_action = new jo_log[1];

                    S_deletenaja.oakja = databox2;
                    S_deletenaja.m0_actor_idx_lo = 4;
                    S_deletenaja.m0_node_idx_lo = 5;
                    S_deletenaja.id_statework_lo = 9;
                    S_deletenaja.comment = textcommentdelete;
                    S_deletenaja.condition = 2;
                    S_deletenaja.requried_old = databox1;
                    S_deletenaja.from_table_l0 = "u1";
                    S_deletenaja.idx_create_lo = int.Parse(ViewState["EMP_HUMAN"].ToString());

                    B_deletenaja.jo_log_action[0] = S_deletenaja;
                    B_deletenaja = callService(_urlInsertloggaritum, B_deletenaja);

                    data_jo databig = new data_jo();
                    jo_add_datalist datasmall = new jo_add_datalist();
                    databig.jo_add_datalist_action = new jo_add_datalist[1];

                    datasmall.wait = int.Parse(ViewState["u0_jo_idx"].ToString());

                    databig.jo_add_datalist_action[0] = datasmall;
                    databig = callService(_urlSelectU1ov, databig);
                    setGridData(gvanthoer_old, databig.jo_add_datalist_action);
                    if (ViewState["num_last"].ToString() == "8")
                    {
                        //TextBox3.Text = counter.ToString();
                        //test1.Text = sum1.ToString();
                        if (numsum == 2 * count) // ครั้งสุดท้ายที่ user เซ็น จะลง daatabase ว่า user เซ็น
                        {
                            data_jo biguserlast = new data_jo();
                            job_order_overview smalluserlast = new job_order_overview();
                            biguserlast.job_order_overview_action = new job_order_overview[1];

                            smalluserlast.u0_jo_idx = int.Parse(main_id_to_sql.Text);
                            smalluserlast.emp_id_receive = int.Parse(ViewState["EMP_HUMAN"].ToString());

                            biguserlast.job_order_overview_action[0] = smalluserlast;
                            biguserlast = callService(_urlUpdatelastuser, biguserlast);

                        }

                    }
                    missha();
                }

                break;
            case "savework":

                string view_chack = cmdArg;


                Panel _pl_titleinsert = (Panel)viewmisshare.FindControl("pl_titleinsert");
                CheckBox rdbt_end = (CheckBox)_pl_titleinsert.FindControl("rdbt_end");
                ////////////////////////////////////////////////---------------- วันลูปเอาข้อมูล เข้า  -------------/////////////////////////////////
                var dtinternet = (DataSet)ViewState["showdatanaja"];

                int numrow_ch = dtinternet.Tables[0].Rows.Count; // จำนวนแถว
                var u1_jo_add_datalist = new jo_add_datalist[dtinternet.Tables[0].Rows.Count];
                var u1_jo_log = new jo_log[dtinternet.Tables[0].Rows.Count];

                var allpopose = dtinternet.Tables[0].Rows;

                int i = 0;
                if (7 != 0)
                {
                    foreach (DataRow drw in dtinternet.Tables[0].Rows)                         //-- เก็บค่า
                    {
                        u1_jo_add_datalist[i] = new jo_add_datalist();
                        u1_jo_log[i] = new jo_log();

                        u1_jo_add_datalist[i].jolog_m0_status = 13;
                        u1_jo_add_datalist[i].jolog_m0_node = 5;
                        u1_jo_add_datalist[i].jolog_m0_actor = 4;

                        u1_jo_add_datalist[i].u1_title = drw["title_1"].ToString();
                        u1_jo_add_datalist[i].u1_day_sent = drw["timeline"].ToString();
                        u1_jo_add_datalist[i].u0_jo_idx_ref = int.Parse(drw["u0_jo_idx"].ToString());
                        u1_jo_add_datalist[i].u1_emp_sent = int.Parse(ViewState["EMP_HUMAN"].ToString());
                        i++;
                    }
                }
                data_jo bigbox = new data_jo();

                jo_add_datalist Jo_add_u1 = new jo_add_datalist();

                bigbox.jo_add_datalist_action = new jo_add_datalist[1];

                Jo_add_u1.sec_mis = int.Parse(ViewState["_rsec_mis"].ToString());

                bigbox.jo_add_datalist_action = u1_jo_add_datalist;


                    //test_l.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(bigbox));
                 // test_l.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(bigbox));
                bigbox = callService(_urlInsertu1, bigbox);

                //////////////////////////////////////////

                Textbox5.Text = Label2.Text;
                Textbox6.Text = Label3.Text;
                Textbox7.Text = Label4.Text;
                data_jo boxbigup = new data_jo();
                job_order_overview boxsmallup = new job_order_overview();
                boxbigup.job_order_overview_action = new job_order_overview[1];

                boxsmallup.u0_jo_idx = int.Parse(main_id_to_sql.Text);
                boxsmallup.sec_mis = int.Parse(ViewState["_rsec_mis"].ToString());
                boxbigup.job_order_overview_action[0] = boxsmallup;
                boxbigup = callService(_urlSelectDatajoborder_headlook, boxbigup);


                ViewState["idwork"] = boxbigup.job_order_overview_action[0].id_statework.ToString();
               
                //Textbox8.Text = main_id_to_sql.Text;
                //Textbox9.Text = ViewState["idwork"].ToString();

                data_jo largebox = new data_jo();
                job_order_overview babybox = new job_order_overview();
                largebox.job_order_overview_action = new job_order_overview[1];
               // int numset = 0;
               // int a = 0;

                if (rdbt_end.Checked)
                {
                    pl_titleinsert.Visible = false;
                    //babybox.u0_jo_idx = int.Parse(ViewState["u0_jo_jo"].ToString());//int.Parse(main_id_to_sql.Text);
                    //babybox.end_send = "1";
                    //largebox.job_order_overview_action[0] = babybox;
                    //// test_l.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(largebox));
                    ////test_l.Text = ViewState["u0_jo_idx"].ToString();
                    //largebox = callService(_urlUpdateu1end, largebox);
                    //numset = 10;

                    data_jo B_logclick = new data_jo();
                    jo_log S_logclick = new jo_log();
                    B_logclick.jo_log_action = new jo_log[1];

                    //S_editsuccess.oakja = misedit_suc;
                    S_logclick.oakja = int.Parse(ViewState["u0_jo_jo"].ToString());
                    S_logclick.m0_actor_idx_lo = 4;
                    S_logclick.m0_node_idx_lo = 5;
                    S_logclick.id_statework_lo = 10;
                    S_logclick.condition = 1;
                    S_logclick.comment = "ส่งงานครั้งสุดท้าย";
                    S_logclick.requried_old = "-----เพิ่มหัวข้อครั้งสุดท้าย-----";
                    S_logclick.from_table_l0 = "u1";
                    S_logclick.idx_create_lo = _emp_idx;/*int.Parse(ViewState["EMP_HUMAN"].ToString());
*/
                    B_logclick.jo_log_action[0] = S_logclick;


                    B_logclick = callService(_urlInsertloggaritum, B_logclick); 
                }
                //else
                //{
                //babybox.u0_jo_idx = int.Parse(ViewState["u0_jo_jo"].ToString());
                //numset = 10;

                //}


                if (ViewState["idwork"].ToString() == "5")    //ถ้า id=5 หรือ ดป็นรอดำเนินการ จะ ทำให้อัพเดต สถานะ ไปให้ user แต่ถ้า เป็นเลขอื่นจะไม่อัพ
                {
                    data_jo jingjobig = new data_jo();
                    job_order_overview jingjosmall = new job_order_overview();
                    jingjobig.job_order_overview_action = new job_order_overview[1];

                    jingjosmall.u0_jo_idx = int.Parse(ViewState["u0_jo_jo"].ToString()); //int.Parse(Textbox5.Text);
                    jingjosmall.to_m0_node = 5;// int.Parse(Textbox6.Text);
                    jingjosmall.to_m0_actor = 4; //int.Parse(Textbox7.Text); //Textbox7.Text
                    jingjosmall.id_statework = 10; //int.Parse(usenajaei.Text);
                    jingjosmall.emp_id_creator = _emp_idx;



                    //jingjosmall.u0_jo_idx = int.Parse(Textbox5.Text);
                    //jingjosmall.to_m0_node = int.Parse(Textbox6.Text);
                    //jingjosmall.to_m0_actor = int.Parse(Textbox7.Text); //Textbox7.Text
                    //jingjosmall.id_statework = 10; //int.Parse(usenajaei.Text);
                    //jingjosmall.emp_id_creator = _emp_idx;

                    jingjobig.job_order_overview_action[0] = jingjosmall;

                    //test_l.Text = ViewState["frg"].ToString();

                    //asd.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(jingjobig));
                      jingjobig = callService(_urlUpdatestau0, jingjobig); //_urlUpdatestau0

                }


                data_jo databig2 = new data_jo();
                jo_add_datalist datasmall2 = new jo_add_datalist();
                databig2.jo_add_datalist_action = new jo_add_datalist[1];

                datasmall2.wait = int.Parse(ViewState["u0_jo_idx"].ToString());

                databig2.jo_add_datalist_action[0] = datasmall2;
                databig2 = callService(_urlSelectU1ov, databig2);
                setGridData(gvanthoer_old, databig2.jo_add_datalist_action);

                ViewState["titleimput_log"] = databig2.jo_add_datalist_action[0].u1_title.ToString();


                data_jo checklastusersent1 = new data_jo();
                job_order_overview smallusersent1 = new job_order_overview();
                checklastusersent1.job_order_overview_action = new job_order_overview[1];

                smallusersent1.u0_jo_idx = int.Parse(ViewState["u0_jo_idx"].ToString());

                checklastusersent1.job_order_overview_action[0] = smallusersent1;
                //dog.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(fat));
                checklastusersent1 = callService(_urlSelectMIS_share, checklastusersent1);
                int counter1 = gvanthoer_old.Rows.Count;
                int n1 = 0;
                int sum11 = 0;
                for (n1 = 0; n1 < counter1; n1++)
                {
                    ViewState["usersentcheck_input"] = databig2.jo_add_datalist_action[n1].u1_status.ToString();
                    sum11 += int.Parse(ViewState["usersentcheck_input"].ToString());
                }
                ViewState["usercheck"] = checklastusersent1.job_order_overview_action[0].id_statework.ToString();
                ViewState["end_sendmis"] = checklastusersent1.job_order_overview_action[0].end_send.ToString();;
                if (ViewState["end_sendmis"].ToString() == "1") //ViewState["usercheck"].ToString() == "8"
                {
                    //TextBox3.Text = counter1.ToString();
                    //test1.Text = sum11.ToString();
                    if (sum11 == 2 * counter1) // ครั้งสุดท้ายที่ user เซ็น จะลง daatabase ว่า user เซ็น
                    {
                        data_jo biguserlast = new data_jo();
                        job_order_overview smalluserlast = new job_order_overview();
                        biguserlast.job_order_overview_action = new job_order_overview[1];

                        smalluserlast.u0_jo_idx = int.Parse(main_id_to_sql.Text);
                        smalluserlast.emp_id_receive = _emp_idx;//int.Parse(ViewState["EMP_HUMAN"].ToString());

                        biguserlast.job_order_overview_action[0] = smalluserlast;
                        biguserlast = callService(_urlUpdatelastuser, biguserlast);

                    }
                }

                data_jo datalog = new data_jo();
                jo_log _datalog = new jo_log();
                datalog.jo_log_action = new jo_log[1];

                _datalog.l0_jo_idx_ref = int.Parse(ViewState["u0_jo_idx"].ToString());

                datalog.jo_log_action[0] = _datalog;
                //despacito.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(B_logselect));
                datalog = callService(_urlSelectlogu1, datalog);
                ViewState["log_jobU1"] = datalog.jo_log_action;
                setGridData(gv_logold, ViewState["log_jobU1"]);
                gvanthoer_old.DataBind();

                //if (bigbox.return_code == 0)
                //{


                gvinfv_showtitle.Visible = false;
                    ViewState["showdatanaja"] = null;

                    gvinfv_showtitle.DataSource = ViewState["showdatanaja"];
                    gvinfv_showtitle.DataBind();

                //saveall.Visible = false;
                //rdbt_end.Visible = false;

                switch (view_chack)
                {
                    case "0":

                        setActiveTab("viewall_pages", 0);
                        multimaster.SetActiveView(viewall_pages);
                        break;

                    case "1":

                        setActiveTab("view1", 0);
                        multimaster.SetActiveView(View1);
                        break;
                }


                //Page.Response.Redirect(Page.Request.Url.ToString(), true);

                //}          

                SETFOCUS_ONTOP.Focus();
                break;
            case "editsuccess":
                string[] arraynaja1 = new string[4];
                arraynaja1 = e.CommandArgument.ToString().Split(';');
                int misedit_suc = int.Parse(arraynaja1[0]);
                //   string statitle = arraynaja1[1];//.Replace('\r', ' ').Replace('\n', ' ');
                int misedit_sucok = int.Parse(arraynaja1[1]); //arraynaja1[2];//
                string statitle = arraynaja1[2];//Regex.Replace(arraynaja1[2], @"[^0-9a-zA-Z]+", "");
                int indexsend_again = int.Parse(arraynaja1[3]);
                string textcomment_send_again = String.Empty;
                //test_l.Text = Convert.ToString(misedit_sucok) ;

                foreach (GridViewRow row in gvanthoer_old.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        int countcomment_send_again = row.RowIndex;

                        TextBox txt_commentu1_sendagain = row.Cells[6].FindControl("txt_commentu1") as TextBox;
                        if (indexsend_again == countcomment_send_again)
                        {

                            if (txt_commentu1_sendagain.Text == "")
                            {
                                textcomment_send_again = "-";

                            }
                            else
                            {
                                textcomment_send_again = txt_commentu1_sendagain.Text;
                            }

                        }
                        else
                        {
                            //test_l.Text = Convert.ToString(countcomment); ;
                        }
                    }
                }


                data_jo bigedit = new data_jo();
                jo_add_datalist smalledit = new jo_add_datalist();
                bigedit.jo_add_datalist_action = new jo_add_datalist[1];

                smalledit.u1_jo_idx = misedit_suc;
                smalledit.u1_emp_sent_single = int.Parse(ViewState["EMP_HUMAN"].ToString());

                bigedit.jo_add_datalist_action[0] = smalledit;
                bigedit = callService(_urlUpdateu1misreturn_editsucc, bigedit);

                data_jo databig3 = new data_jo();
                jo_add_datalist datasmall3 = new jo_add_datalist();
                databig3.jo_add_datalist_action = new jo_add_datalist[1];

                datasmall3.wait = int.Parse(ViewState["u0_jo_idx"].ToString());

                databig3.jo_add_datalist_action[0] = datasmall3;
                //dog1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(databig));
                databig3 = callService(_urlSelectU1ov, databig3);

                setGridData(gvanthoer_old, databig3.jo_add_datalist_action);
                data_jo B_editsuccess = new data_jo();
                jo_log S_editsuccess = new jo_log();
                B_editsuccess.jo_log_action = new jo_log[1];

                //S_editsuccess.oakja = misedit_suc;
                S_editsuccess.oakja = misedit_sucok;
                S_editsuccess.m0_actor_idx_lo = 4;
                S_editsuccess.m0_node_idx_lo = 5;
                S_editsuccess.comment = textcomment_send_again;
                S_editsuccess.condition = 2;
                S_editsuccess.id_statework_lo = 12;
                S_editsuccess.requried_old = statitle;
                S_editsuccess.from_table_l0 = "u1";
                S_editsuccess.idx_create_lo = int.Parse(ViewState["EMP_HUMAN"].ToString());

               // test_l.Text = statitle.ToString();

                B_editsuccess.jo_log_action[0] = S_editsuccess;
              

                B_editsuccess = callService(_urlInsertloggaritum, B_editsuccess);

                break;
            case "MISOKSTA":
                Textbox5.Text = Label2.Text;
                Textbox6.Text = Label3.Text;
                Textbox7.Text = Label4.Text;
                data_jo biguserlast_2 = new data_jo();
                job_order_overview smalluserlast_2 = new job_order_overview();
                biguserlast_2.job_order_overview_action = new job_order_overview[1];


                smalluserlast_2.u0_jo_idx = int.Parse(Textbox5.Text);
                smalluserlast_2.to_m0_node = int.Parse(Textbox6.Text);
                smalluserlast_2.to_m0_actor = int.Parse(Textbox7.Text);
                smalluserlast_2.id_statework = 8; //int.Parse(usenajaei.Text);


                biguserlast_2.job_order_overview_action[0] = smalluserlast_2;
                //asd.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(biguserlast_2));

                biguserlast_2 = callService(_urlUpdatestau0, biguserlast_2);

                data_jo END_big = new data_jo();
                job_order_overview END_small = new job_order_overview();
                END_big.job_order_overview_action = new job_order_overview[1];

                END_small.u0_jo_idx = int.Parse(main_id_to_sql.Text);
                END_small.emp_id_sent = int.Parse(ViewState["EMP_HUMAN"].ToString());
                END_small.requried_old = ViewState["t"].ToString();


                END_big.job_order_overview_action[0] = END_small;
                //despacito.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(END_big));
                END_big = callService(_urlUp_END, END_big);

                   //   ไม่ได้ใช้
                //data_jo B_MISOKSTA = new data_jo();
                //jo_log S_MISOKSTA = new jo_log();
                //B_MISOKSTA.jo_log_action = new jo_log[1];

                //S_MISOKSTA.oakja = int.Parse(main_id_to_sql.Text);
                //S_MISOKSTA.m0_actor_idx_lo = 1;
                //S_MISOKSTA.m0_node_idx_lo = 6;
                //S_MISOKSTA.id_statework_lo = 7;
                //S_MISOKSTA.requried_old = ViewState["t"].ToString();

                //B_MISOKSTA.jo_log_action[0] = S_MISOKSTA;
                //despacito.Text= HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(B_MISOKSTA));
                //B_MISOKSTA = callService(_urlInsertloggaritum, B_MISOKSTA);


                if (END_big.return_code == 0)
                {
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                }
                break;
            case "user_okok":
                string[] arraynaja2 = new string[4];
                arraynaja2 = e.CommandArgument.ToString().Split(';');
                int id_userok = int.Parse(arraynaja2[0]);
                int id_userin_2 = int.Parse(arraynaja2[1]);
                string statitle1 = arraynaja2[2];
                int index = int.Parse(arraynaja2[3]);
                string textcomment = String.Empty;
                //string statitle1 = Regex.Replace(arraynaja2[2], @"[^0-9a-zA-Z]+", "");

                foreach (GridViewRow row in gvanthoer_old.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        int countcomment = row.RowIndex;

                            TextBox txt_commentu1 = row.Cells[6].FindControl("txt_commentu1") as TextBox;  
                            if (index == countcomment)
                            {

                                if (txt_commentu1.Text == "")
                                {
                                textcomment = "-";


                                }
                                else
                                {
                                textcomment = txt_commentu1.Text;
                                }

                            }
                            else
                            {
                              //  test_l.Text = Convert.ToString(countcomment); ;
                            }
                    }
                }

                data_jo biguserok = new data_jo();
                jo_add_datalist smalluserok = new jo_add_datalist();
                biguserok.jo_add_datalist_action = new jo_add_datalist[1];

                smalluserok.u1_jo_idx = id_userok;
                smalluserok.u1_emp_receive = int.Parse(_emp_idx.ToString());

                //wowo.Text = databox.ToString();

                biguserok.jo_add_datalist_action[0] = smalluserok;

                biguserok = callService(_urlUpdateu1_userok, biguserok);

                // test_l.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(biguserok));

                data_jo databig11 = new data_jo();
                jo_add_datalist datasmall11 = new jo_add_datalist();
                databig11.jo_add_datalist_action = new jo_add_datalist[1];

                datasmall11.wait = id_userin_2;//int.Parse(ViewState["u0_jo_idx"].ToString());
                datasmall11.u0_jo_idx_ref = id_userin_2;
                datasmall11.u1_jo_idx = id_userok;

                databig11.jo_add_datalist_action[0] = datasmall11;

                databig11 = callService(_urlSelectU1ov, databig11);

                // test_l.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(databig11));

                setGridData(gvanthoer_old, databig11.jo_add_datalist_action);

                ViewState["titleimput_log"] = databig11.jo_add_datalist_action[0].u1_title.ToString();

                data_jo biguser_okok = new data_jo();
                  jo_log smalluser_okok = new jo_log();
                  biguser_okok.jo_log_action = new jo_log[1];


                  smalluser_okok.oakja = id_userin_2;
                  smalluser_okok.jo_idx_ref = id_userok;
                  smalluser_okok.m0_actor_idx_lo = 1;
                  smalluser_okok.m0_node_idx_lo = 6;
                  smalluser_okok.id_statework_lo = 6;
                  // oaktestang.Text = ViewState["titleimput_log"].ToString();
                  smalluser_okok.requried_old = statitle1;
                  smalluser_okok.from_table_l0 = "u1";
                  smalluser_okok.condition = 2;
                  smalluser_okok.comment = textcomment;
                //  smalluser_okok.comment = "111";
                  smalluser_okok.idx_create_lo = int.Parse(_emp_idx.ToString());

                biguser_okok.jo_log_action[0] = smalluser_okok;

               // test_l.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(biguser_okok));

                 biguser_okok = callService(_urlInsertloggaritum, biguser_okok);


                /////////////////////////////////////////////////////////////////////////////

                data_jo checklastusersent = new data_jo();
                job_order_overview smallusersent = new job_order_overview();
                checklastusersent.job_order_overview_action = new job_order_overview[1];

                smallusersent.u0_jo_idx = int.Parse(ViewState["u0_jo_idx"].ToString());

                checklastusersent.job_order_overview_action[0] = smallusersent;



                checklastusersent = callService(_urlSelectMIS_share, checklastusersent);

                //   test_l.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(checklastusersent));

                int counter = gvanthoer_old.Rows.Count;
                int n = 0;
                int sum1 = 0;
                for (n = 0; n < counter; n++)
                {
                    ViewState["usersentcheck_input"] = databig11.jo_add_datalist_action[n].u1_status.ToString();
                    sum1 += int.Parse(ViewState["usersentcheck_input"].ToString());

                }
                ViewState["usercheck"] = checklastusersent.job_order_overview_action[0].id_statework.ToString();
                ViewState["node"] = checklastusersent.job_order_overview_action[0].m0_node_idx.ToString();
                ViewState["_end_send"] = checklastusersent.job_order_overview_action[0].end_send.ToString();
                //test_l.Text = ViewState["_end_send"].ToString();



                if (ViewState["_end_send"].ToString() == "1") //ViewState["usercheck"].ToString() == "8"
                {
                    //TextBox3.Text = counter.ToString();
                    //test1.Text = sum1.ToString();

                    //test_l.Text = ViewState["_end_send"].ToString();


                    if (sum1 == 2 * counter) // ครั้งสุดท้ายที่ user เซ็น จะลง daatabase ว่า user เซ็น
                    {
                        data_jo biguserlast = new data_jo();
                        job_order_overview smalluserlast = new job_order_overview();
                        biguserlast.job_order_overview_action = new job_order_overview[1];

                        smalluserlast.u0_jo_idx = int.Parse(main_id_to_sql.Text);
                        smalluserlast.emp_id_receive = _emp_idx;//int.Parse(_emp_idx.ToString());
                        smalluserlast.requried_old = ViewState["titleimput_log"].ToString();
                        smalluserlast.comment = "ปิดการดำเนินงาน";
                        biguserlast.job_order_overview_action[0] = smalluserlast;
                      
                        biguserlast = callService(_urlUpdatelastuser, biguserlast);

                        //test_l.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(biguserlast));
                        //test_l.Text = ViewState["Sec_idx"].ToString();
                        if (biguserlast.return_code == 99)
                        {
                            setActiveTab("view1", 0);
                            multimaster.SetActiveView(View1);
                            SETFOCUS_ONTOP.Focus();
                        }


                    }
                }

                if (biguserok.return_code == 1)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีการดำเนินกับหัวข้อนี้เเล้ว');", true);

                }

                break;
            case "user_nook":
                string[] arraynaja3 = new string[4];

                arraynaja3 = e.CommandArgument.ToString().Split(';');
                int id_userno = int.Parse(arraynaja3[0]);       
                int id_userin_2no = int.Parse(arraynaja3[1]);
                string statitle2 = arraynaja3[2];
                int indexno = int.Parse(arraynaja3[3]);
                string textcommentno = String.Empty;

                foreach (GridViewRow row in gvanthoer_old.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        int countcommentno = row.RowIndex;

                        TextBox txt_commentu1no = row.Cells[6].FindControl("txt_commentu1") as TextBox;
                        if (indexno == countcommentno)
                        {

                            if (txt_commentu1no.Text == "")
                            {
                                textcommentno = "-";


                            }
                            else
                            {
                                textcommentno = txt_commentu1no.Text;
                            }

                        }
                        else
                        {
                            //test_l.Text = Convert.ToString(countcomment); ;
                        }
                    }
                }

                data_jo biguserno = new data_jo();
                jo_add_datalist smalluserno = new jo_add_datalist();
                biguserno.jo_add_datalist_action = new jo_add_datalist[1];

                smalluserno.u1_jo_idx = id_userno;
                smalluserno.u1_emp_receive = int.Parse(_emp_idx.ToString());
                //wowo.Text = databox.ToString();
                biguserno.jo_add_datalist_action[0] = smalluserno;

                biguserno = callService(_urlUpdateu1_userno, biguserno);

                data_jo databig22 = new data_jo();
                jo_add_datalist datasmall22 = new jo_add_datalist();
                databig22.jo_add_datalist_action = new jo_add_datalist[1];

                datasmall22.wait = int.Parse(ViewState["u0_jo_idx"].ToString());

                databig22.jo_add_datalist_action[0] = datasmall22;
                //dog1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(databig));
                databig22 = callService(_urlSelectU1ov, databig22);
                ViewState["titleimput_log1"] = databig22.jo_add_datalist_action[0].u1_title.ToString();
                setGridData(gvanthoer_old, databig22.jo_add_datalist_action);

                data_jo biguser_nook = new data_jo();
                jo_log smalluser_nook = new jo_log();
                biguser_nook.jo_log_action = new jo_log[1];

                smalluser_nook.jo_idx_ref = id_userno;
                smalluser_nook.oakja = id_userin_2no;
                smalluser_nook.m0_actor_idx_lo = 1;
                smalluser_nook.m0_node_idx_lo = 6;
                smalluser_nook.id_statework_lo = 7;
                smalluser_nook.condition = 2;
                smalluser_nook.comment = textcommentno;
                smalluser_nook.requried_old = statitle2;
                smalluser_nook.from_table_l0 = "u1";
                smalluser_nook.idx_create_lo = int.Parse(_emp_idx.ToString());

                biguser_nook.jo_log_action[0] = smalluser_nook;
                biguser_nook = callService(_urlInsertloggaritum, biguser_nook);


       
                if (biguserno.return_code == 1)
                {

                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีการดำเนินกับหัวข้อนี้เเล้ว');", true);


                }
                break;
            case "look_log_u0":
                data_jo B_logselect = new data_jo();
                jo_log S_logselect = new jo_log();
                B_logselect.jo_log_action = new jo_log[1];

                S_logselect.l0_jo_idx_ref = int.Parse(ViewState["u0_jo_idx"].ToString());

                B_logselect.jo_log_action[0] = S_logselect;
                //despacito.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(B_logselect));
                B_logselect = callService(_urlSelectlogu1, B_logselect);

                ViewState["log_jobU1"] = B_logselect.jo_log_action;
                setGridData(gv_logold, ViewState["log_jobU1"]);
               

                pl_gvold.Visible = false;
                pl_log_gvold.Visible = true;
                look_log_u0.Visible = false;
                look_u0.Visible = true;
                title_old.Visible = false;
                title_old_log.Visible = true;
             //   successwork.Visible = false;
                pl_log_gvold.Focus();
                break;
            case "look_u0":
                data_jo B_selec = new data_jo();
                jo_add_datalist S_selec = new jo_add_datalist();
                B_selec.jo_add_datalist_action = new jo_add_datalist[1];

                S_selec.wait = int.Parse(ViewState["u0_jo_idx"].ToString());

                B_selec.jo_add_datalist_action[0] = S_selec;
                B_selec = callService(_urlSelectU1ov, B_selec);
                setGridData(gvanthoer_old, B_selec.jo_add_datalist_action);

                pl_gvold.Visible = true;
                pl_log_gvold.Visible = false;
                look_log_u0.Visible = true;
                look_u0.Visible = false;
                title_old.Visible = true;
                title_old_log.Visible = false;     
                pl_succ.Visible = true;

                //if (ViewState["_rdept_idx"].ToString() == "20")
                //{
                //    successwork.Visible = true;
                //}

                pl_gvold.Focus();
                break;
            case "name_crate":
                name_crate.Visible = false;
                Title_create.Visible = true;
                plname_cre.Visible = false;
                Pl_data_req.Visible = true;
                break;
            case "Title_create":
                name_crate.Visible = true;
                Title_create.Visible = false;
                plname_cre.Visible = true;
                Pl_data_req.Visible = false;
                break;
            case "lb_all_list":
                multimaster.SetActiveView(viewall_pages);
                break;
            case "lb_backto_list":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "lb_build_plus_1":
                multimaster.SetActiveView(create_story);
                remoteuser.Visible = true;

                break;
        }
    }

    protected void btninsert(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        var to_m0_node = (TextBox)formviewhead.FindControl("form_node");
        var to_m0_actor = (TextBox)formviewhead.FindControl("form_actor");

        switch (cmdName)
        {
            case "addtitlerequire":

                //gvinfv_showtitle.Visible = true;
                //  rdbt_end.Visible = true;

                string[] datetimeset = new string[3];
                string[] datetimeset_here = new string[3];


                data_jo year_most = new data_jo();
                job_order_overview year_log = new job_order_overview();
                year_most.job_order_overview_action = new job_order_overview[1];

                year_log.u0_jo_idx = int.Parse(ViewState["u0_jo_idx"].ToString());

                year_most.job_order_overview_action[0] = year_log;
                //oaktest.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(year_most));
                year_most = callService(_urlSelectMIS_share, year_most);

                //ViewState["_year_most"] = year_most.job_order_overview_action[0].year_check.ToString();
                ViewState["_year_start"] = year_most.job_order_overview_action[0].day_start.ToString();
                ViewState["_year_fins"] = year_most.job_order_overview_action[0].day_finish.ToString();
                TextBox title_requiremis = (TextBox)viewmisshare.FindControl("title_indb");
                TextBox start_requiremis = (TextBox)viewmisshare.FindControl("start_requiremis");
                TextBox lbu0_idx_require = (TextBox)viewmisshare.FindControl("main_id_to_sql");
                ////////////////////    เป็นส่วน ตัด ข้อมูลให้เหลือแต่ คศ


                datetimeset = ViewState["_year_start"].ToString().Split('/');
                string day_start = datetimeset[0];
                string month_start = datetimeset[1];
                string year_start = datetimeset[2];


              

                datetimeset_here = start_requiremis.Text.Split('/');
                string day_shere = datetimeset_here[0];
                string month_shere = datetimeset_here[1];
                string year_shere = datetimeset_here[2];


                //string date_s = ViewState["_year_start"].ToString();
                //DateTime newDate_s = DateTime.ParseExact(date_s, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                //test_l.Text = newDate_s.ToString();

                // string date_n = start_requiremis.Text;
                //DateTime newDate_n = DateTime.ParseExact(date_n, "dd/MM/yyyy", CultureInfo.InvariantCulture);//DateTime.ParseExact(date_n, "yyyy-mm-dd", CultureInfo.InvariantCulture);

                //// test_l.Text = newDate_n.ToString();
                // ViewState["ok_upside"] = "0";
                // if (newDate_n > newDate_s)
                // {
                //     //test_l.Text = newDate_n.ToString();
                //     //tangura.Text = newDate_s.ToString();
                //     ViewState["ok_upside"] = "1";
                // }
                // else if (newDate_n == newDate_s)
                // {
                //     ViewState["ok_upside"] = "1";
                // }
                // else if (newDate_n < newDate_s)
                // {
                //     ViewState["ok_upside"] = "0";
                // }


                if (int.Parse(year_shere) > int.Parse(year_start))
                {

                    //  test_l.Text = "111";
                    ViewState["ok_upside"] = "1";
                }
                else if (int.Parse(year_shere) == int.Parse(year_start))
                {
                    if (int.Parse(month_shere) > int.Parse(month_start))
                    {
                        //if (int.Parse(day_shere) >= int.Parse(day_start))
                        //{
                        // test_l.Text = "222";
                        ViewState["ok_upside"] = "1";
                        //}
                    }
                    else if (int.Parse(month_shere) == int.Parse(month_start))
                    {
                        if (int.Parse(day_shere) >= int.Parse(day_start))
                        {
                            // test_l.Text = "555";
                            ViewState["ok_upside"] = "1";
                        }
                        else
                        {
                            // test_l.Text = "333";
                            ViewState["ok_upside"] = "0";
                        }
                    }
                    else
                    {
                        // test_l.Text = "666";
                        ViewState["ok_upside"] = "0";
                    }
                }
                else
                {
                    //test_l.Text = "444";
                    ViewState["ok_upside"] = "0";
                }

                if (ViewState["ok_upside"].ToString() == "1")
                {
                    saveall.Visible = true;   //  ถ้าเป็นครั้งแรก ลืมปิด
                    nannasi.Visible = true;
                    rdbt_end.Visible = true;
                    save_viewdep.Visible = true;
                    gvinfv_showtitle.Visible = true;

                    var dtviewmis = (DataSet)ViewState["showdatanaja"];
                    var daviewmis = dtviewmis.Tables[0].NewRow();
                    //lbu0_idx_require.Text = ViewState["u0_jo_idx"].ToString();
                    int numrow = dtviewmis.Tables[0].Rows.Count; // จำนวนแถว
                    ViewState["dtviewmis_use"] = dtviewmis;
                    ViewState["title_re"] = title_requiremis.Text;

                    if (numrow > 0)
                    {
                        foreach (DataRow check in dtviewmis.Tables[0].Rows)
                        {
                            ViewState["check_title"] = check["title_1"];
                            if (ViewState["title_re"].ToString() == ViewState["check_title"].ToString())
                            {
                                ViewState["CheckDataset"] = "0";
                                break;
                            }
                            else
                            {
                                ViewState["CheckDataset"] = "1";
                            }
                        }
                        if (ViewState["CheckDataset"].ToString() == "1")
                        {
                            daviewmis["title_1"] = title_requiremis.Text;
                            // daviewmis["timeline"] = txtlastdate.Value;
                            daviewmis["timeline"] = start_requiremis.Text;
                            daviewmis["u0_jo_idx"] = lbu0_idx_require.Text;


                            dtviewmis.Tables[0].Rows.Add(daviewmis);
                            ViewState["showdatanaja"] = dtviewmis;

                            pl_gvdetail.Visible = true;

                            gvinfv_showtitle.DataSource = dtviewmis.Tables[0];
                            gvinfv_showtitle.DataBind();
                            //lasttdate.Text = txtlastdate.Value;
                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลอยู่แล้ว');", true);
                        }
                    }
                    else
                    {
                        daviewmis["title_1"] = title_requiremis.Text;
                        // daviewmis["timeline"] = txtlastdate.Value;
                        daviewmis["timeline"] = start_requiremis.Text;
                        daviewmis["u0_jo_idx"] = lbu0_idx_require.Text;


                        dtviewmis.Tables[0].Rows.Add(daviewmis);
                        ViewState["showdatanaja"] = dtviewmis;

                        pl_gvdetail.Visible = true;

                        gvinfv_showtitle.DataSource = dtviewmis.Tables[0];
                        gvinfv_showtitle.DataBind();
                        //lasttdate.Text = txtlastdate.Value;
                    }

                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('เวลาที่ส่งมอบงาน น้อยกว่าวันที่เริ่มงาน');", true);
                }
                title_indb.Text = null;
                start_requiremis.Text = null;
                /*SETFOCUS_ONTOP*/
                gvinfv_showtitle.Focus();
                break;
            case "submithead":
                //var str_title = cmdArg;
                string cmdArgView = cmdArg;

                Panel divformhead = ((Panel)viewhead.FindControl("divformhead"));
                FormView formviewhead1 = ((FormView)divformhead.FindControl("formviewhead"));
                Label sec_mis12 = ((Label)formviewhead1.FindControl("sec_mis1"));
                TextBox comment_userhead = ((TextBox)formviewhead1.FindControl("txt_comment"));
                data_jo bigjo = new data_jo();
                job_order_overview smalljo = new job_order_overview();
                bigjo.job_order_overview_action = new job_order_overview[1];

                smalljo.u0_jo_idx = int.Parse(tostid.Text);
                smalljo.to_m0_node = int.Parse(to_m0_node.Text);
                smalljo.to_m0_actor = int.Parse(to_m0_actor.Text);
                smalljo.sec_mis = int.Parse(sec_mis12.Text);
             
                smalljo.emp_id_creator = _emp_idx;
                smalljo.id_statework = 2;

                if (comment_userhead.Text == "")
                {
                    smalljo.comment = "-"; 
                }
                else
                {
                    smalljo.comment = comment_userhead.Text;
                }

             //   test_l.Text = comment_userhead.Text;

                bigjo.job_order_overview_action[0] = smalljo;
              //  test_l.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(bigjo));
               bigjo = callService(_urlUpdatestau0, bigjo);

                //data_jo jojojo = new data_jo();
                //jo_log jobjobjob = new jo_log();
                //jojojo.jo_log_action = new jo_log[1];

                //jobjobjob.oakja = int.Parse(tostid.Text);
                //jobjobjob.m0_actor_idx_lo = 2;
                //jobjobjob.m0_node_idx_lo = 2;
                //jobjobjob.id_statework_lo = 2;
                //jobjobjob.requried_old = str_title;
                //jobjobjob.from_table_l0 = "u0";
                //jobjobjob.idx_create_lo = int.Parse(ViewState["EMP_HUMAN"].ToString());

                //jojojo.jo_log_action[0] = jobjobjob;
                ////eiii.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(jojojo));
                //jojojo = callService(_urlInsertloggaritum, jojojo);
                //if (bigjo.return_code == 0)
                //{

                switch (cmdArgView)
                {
                    case "0":
                        setActiveTab("view1", 0);
                        multimaster.SetActiveView(View1);
                        break;
                  
                    case "1":
                        setActiveTab("viewall_pages", 0);
                        multimaster.SetActiveView(viewall_pages);
                        break;

                }                
                    //Page.Response.Redirect(Page.Request.Url.ToString(), true);
                //}
                SETFOCUS_ONTOP.Focus();
                break;
            case "cancelbu":
                string str_titlecan = cmdArg;

                Panel divformhead2 = ((Panel)viewhead.FindControl("divformhead"));
                FormView formviewhead2 = ((FormView)divformhead2.FindControl("formviewhead"));
                Label sec_mis2 = ((Label)formviewhead2.FindControl("sec_mis1"));
                TextBox commentcancel = ((TextBox)formviewhead2.FindControl("txt_comment"));

                data_jo bigjo1 = new data_jo();
                job_order_overview smalljo1 = new job_order_overview();
                bigjo1.job_order_overview_action = new job_order_overview[1];

                smalljo1.u0_jo_idx = int.Parse(tostid.Text);
                smalljo1.sec_mis = int.Parse(sec_mis2.Text);
                smalljo1.to_m0_node = /*2;*/int.Parse(to_m0_node.Text);
                smalljo1.to_m0_actor = /*2;*/int.Parse(to_m0_actor.Text);
                smalljo1.id_statework = 3;
               
               // smalljo1.condition = 2;
                smalljo1.emp_id_creator = _emp_idx;
                bigjo1.job_order_overview_action[0] = smalljo1;

                if (commentcancel.Text == "")
                {
                    smalljo1.comment = "-";
                }
                else
                {
                    smalljo1.comment = commentcancel.Text;
                }

                // test_l.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(bigjo1));

                bigjo1 = callService(_urlUpdatestau0, bigjo1);

                //test_l.Text = to_m0_node.Text;
                //tangura.Text = to_m0_actor.Text;

                //setActiveTab("view1", 0);
                //multimaster.SetActiveView(View1);


                switch (str_titlecan)
                {
                    case "0":
                        setActiveTab("view1", 0);
                        multimaster.SetActiveView(View1);
                        break;

                    case "1":
                        setActiveTab("viewall_pages", 0);
                        multimaster.SetActiveView(viewall_pages);
                        break;

                }

                //data_jo jojo = new data_jo();
                //jo_log jobjob = new jo_log();
                //jojo.jo_log_action = new jo_log[1];

                //jobjob.oakja = int.Parse(tostid.Text);
                //jobjob.m0_actor_idx_lo = 2;
                //jobjob.m0_node_idx_lo = 2;
                //jobjob.id_statework_lo = 3;
                //jobjob.requried_old = str_titlecan;
                //jobjob.from_table_l0 = "u0";
                //jobjob.idx_create_lo = int.Parse(ViewState["EMP_HUMAN"].ToString());

                //jojo.jo_log_action[0] = jobjob;
                ////eiii.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(jojojo));
                //  jojo = callService(_urlInsertloggaritum, jojo);
                //if (bigjo1.return_code == 0)
                //{
                //setActiveTab("view1", 0);
                //multimaster.SetActiveView(View1);
                //Page.Response.Redirect(Page.Request.Url.ToString(), true);
                //}
                SETFOCUS_ONTOP.Focus();
                break;

                //case "subbmittest":   --// comeback to use
                //    //var idx_jo_insert = int.Parse(cmdArg);
                //    var title_insert = ((TextBox)viewover.FindControl("titleselect")).Text.Trim();
                //    var require_insert = ((TextBox)viewover.FindControl("requireselect"));
                //    data_jo data_box111 = new data_jo();
                //    job_order_overview box222 = new job_order_overview();
                //    data_box111.job_order_overview_action = new job_order_overview[1];

                //    //box222.u0_jo_idx = idx_jo_insert;
                //    box222.title_jo = title_insert;
                //    box222.require_jo = require_insert.Text;
                //    data_box111.job_order_overview_action[0] = box222;

                //    data_box111 = callService(_urlInsertDatajoborder, data_box111);


                //    if (data_box111.return_code == "0")
                //    {

                //        Page.Response.Redirect(Page.Request.Url.ToString(), true);
                //    }
                //    break;
        }
    }

    protected void btnuser(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        int jo_idx;
        // int n;
        data_jo box = new data_jo();

        switch (cmdName)
        {

            //case "mhlook":

            //    multimaster.SetActiveView(View2);
            //    selectmheadjob();

            //    break;
            case "submithome":
                multimaster.SetActiveView(View1);
                selectjob();
                break;
            case "plusbut":
                insert.Visible = true;
                break;

            case "cmdadd":

                //  insertjobOder();

                var addtitle = (TextBox)remoteuser.FindControl("titlename");
                var addrequir = (TextBox)remoteuser.FindControl("require");
                DropDownList _ddlinsert_sec = (DropDownList)remoteuser.FindControl("ddlinsert_sec");

                ////ViewState["decode"] = addrequir.Text;//reqedit.Text;
                //HttpUtility.HtmlDecode(addrequir.Text).ToString();

                //addrequir = html

                //if (addrequir.Text == "&nbsp;&nbsp;")
                //{
                //    test_l.Text = "111";
                //}


                //HttpUtility.HtmlDecode(addrequir.Text).ToString();

                data_jo Badd_title = new data_jo();
                job_order_overview Sadd_title = new job_order_overview();
                Badd_title.job_order_overview_action = new job_order_overview[1];
                // box1.u0_jo_idx = int.Parse(ViewState["U0_idx"].ToString());
                Sadd_title.title_jo = addtitle.Text;
                Sadd_title.comment = "-";
                Sadd_title.require_jo = addrequir.Text;//.Replace("&nbsp;", "");//addrequir.Text;//HttpUtility.HtmlDecode(addrequir.Text).ToString();//add_reqi.Text;
                Sadd_title.sec_mis = int.Parse(_ddlinsert_sec.SelectedValue);
                Sadd_title.emp_id_creator = int.Parse(ViewState["EMP_HUMAN"].ToString());
                // box1.no_invoice = ViewState["IDfile"].ToString();
                //ส่วนของ User //บอกจุดเริ่มต้นของ node
                Sadd_title.m0_actor_idx = 1; //from_ac
                Sadd_title.m0_node_idx = 1; // from_M0_node
                Sadd_title.id_statework = 1;// node_decision
                Sadd_title.from_table = "u0";

                //   add_reqi.Text = HttpUtility.HtmlDecode(HttpUtility.HtmlDecode(HttpUtility.HtmlDecode(addrequir.Text).ToString()).ToString()).ToString();
                //add_reqi.Text = addrequir.Text;

                //test_l.Text = addrequir.Text;//Statement here
                                             // test_l.Text = HttpUtility.HtmlDecode(addrequir.Text).ToString();
                                             ///   test_l.Text = HttpUtility.HtmlDecode(HttpUtility.HtmlDecode(addrequir.Text).ToString()).ToString();

                //  Regex spacbar = new Regex();

                Badd_title.job_order_overview_action[0] = Sadd_title;
                  Badd_title = callService(_urlInsertDatajoborder, Badd_title);


                //////   test_l.Text = ViewState["sec_mis"].ToString();
                ////// test_l.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Badd_title));

                if (Badd_title.job_order_overview_action[0].no_invoice.ToString() != null)
                {
                    ViewState["_IDfile"] = Badd_title.job_order_overview_action[0].no_invoice.ToString();

                    if (UploadFileComment.HasFile)
                    {
                        string getPathfile = ConfigurationManager.AppSettings["pathfile_Job_order"];
                        string id_uplodefile = ViewState["_IDfile"].ToString();//_returndocumentcode;
                        string fileName_upload = id_uplodefile;
                        string filePath_upload = Server.MapPath(getPathfile + id_uplodefile);

                        if (!Directory.Exists(filePath_upload))
                        {
                            Directory.CreateDirectory(filePath_upload);
                        }
                        string extension = Path.GetExtension(UploadFileComment.FileName);

                        UploadFileComment.SaveAs(Server.MapPath(getPathfile + id_uplodefile) + "\\" + fileName_upload + extension);
                    }
                }

              //  if (Badd_title.return_code == 011)
             //   {
                   // ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาตกลง');", true);
             //   }
              //  else
              //  {
                    setActiveTab("view1", 0);
                    multimaster.SetActiveView(View1);
                    ddlinsert_sec.Items.Clear();
                    ddlinsert_sec.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "0"));
                    selectjob();
                    SETFOCUS_ONTOP.Focus();
                //}

               
                break;
            case "click":
                remoteuser.Visible = true;
                //create.Visible = false;
                //mhbtn.Visible = false;S
                searchbutton.Visible = false;
                btnshow_searchall.Visible = false;
                break;
            case "close":
                //multimaster.SetActiveView(View1);
                //remoteuser.Visible = false;
                //create.Visible = true;
                ////mhbtn.Visible = true;
                //searchbutton.Visible = true;

                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "closemishead":
                string cmdArgcloseView = cmdArg;

                Panel Panel11 = ((Panel)View3.FindControl("Panel1"));
                FormView DetailUser1 = ((FormView)Panel11.FindControl("DetailUser"));
                Label sec_misH_ = ((Label)DetailUser1.FindControl("sec_misH"));

                TextBox comment_closemisH = ((TextBox)View3.FindControl("txt_comment_misH"));


                data_jo b = new data_jo();
                job_order_overview b1 = new job_order_overview();
                b.job_order_overview_action = new job_order_overview[1];

                b1.u0_jo_idx = int.Parse(ViewState["id_idx"].ToString());
                b1.m0_actor_idx = 3;// int.Parse(ViewState["actor"].ToString()); //from_ac
                b1.m0_node_idx = 3; //int.Parse(ViewState["node"].ToString()); // from_M0_node
                b1.id_statework = 3;// node_decision
                b1.sec_mis = int.Parse(sec_misH_.Text);
                b1.emp_id_creator = _emp_idx;
                b.job_order_overview_action[0] = b1;

                if (comment_closemisH.Text == "")
                {
                    b1.comment = "-";
                }
                else
                {
                    b1.comment = comment_closemisH.Text;
                }

                //  test_l.Text = _emp_idx.ToString();
               
                b = callService(_urlUpdatestau0, b);

                //  ViewState["sec_mis"] =  b.job_order_overview_action[0].sec_mis;

                //test_l.Text = sec_misH_.Text;
                //insertlog3();

                //test_l.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(b));


                switch (cmdArgcloseView.ToString())
                {
                    case "0":
                        setActiveTab("view1", 0);
                        multimaster.SetActiveView(View1);

                        break;

                    case "1":
                        setActiveTab("viewall_pages", 0);
                        multimaster.SetActiveView(viewall_pages);
                        break;

                }


                //setActiveTab("view1", 0);
                //multimaster.SetActiveView(View1);
                //// selectmheadjob();

                // selectjob();
                SETFOCUS_ONTOP.Focus();
                break;
            case "closemishead2": //case user ไม่ยอมรับ
                string cmdArgViewclose = cmdArg;

                FormView DetailUsersent_c = ((FormView)View1.FindControl("DetailUsersent"));
                Label sec_mis_date_c = ((Label)DetailUsersent_c.FindControl("sec_mis_date"));

                TextBox txt_comment_usertime_no = ((TextBox)View1.FindControl("txt_comment_usertime"));
                TextBox txt_comment_usertimeall_no = ((TextBox)viewall_pages.FindControl("txt_comment_usertimeall"));

                data_jo bg = new data_jo();
                job_order_overview sm = new job_order_overview();
                bg.job_order_overview_action = new job_order_overview[1];
                sm.u0_jo_idx = int.Parse(ViewState["id_idx"].ToString());
                sm.m0_actor_idx = 1; //from_ac
                sm.m0_node_idx = int.Parse(ViewState["node"].ToString()); // from_M0_node
                sm.id_statework = 3;// node_decision
                sm.sec_mis = int.Parse(sec_mis_date_c.Text);
                sm.emp_id_creator = _emp_idx;
                bg.job_order_overview_action[0] = sm;

                switch (cmdArgViewclose.ToString())
                {
                    case "0":
                        if (txt_comment_usertime_no.Text == "")
                        {
                            sm.comment = "-";
                        }
                        else
                        {
                            sm.comment = txt_comment_usertime_no.Text;
                        }
                        break;
                    case "1":

                        if (txt_comment_usertimeall_no.Text == "")
                        {
                            sm.comment = "-";
                        }
                        else
                        {
                            sm.comment = txt_comment_usertimeall_no.Text;
                        }

                        break;
                }

                //  test_l.Text = sec_mis_date_c.Text;
                
                bg = callService(_urlUpdatestau0, bg);
                //  insertlog4();

                //setActiveTab("view1", 0);
                //multimaster.SetActiveView(View1);

                

               Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "del":
                // ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาตกลง');", true);
                jo_idx = int.Parse(cmdArg);
                ViewState["jo_idx"] = jo_idx;
                // p1.Text = ViewState["jo_idx"].ToString();
                deletejob();
                if (box.return_code == 0)
                {
                    selectjob();
                }
                break;
            case "reset":
                titlename.Text = null;
                require.Text = null;
                ddlinsert_sec.SelectedValue = "0";

                break;
            case "resetsearch":

                DropDownList _ddSec_dept = ((DropDownList)View1.FindControl("ddSec_dept"));

                TempCodeId.Text = null;
                txtdateId.Value = null;
                txtdateIdf.Value = null;
                empidsearch.Text = null;
                dateId.Text = null;
                dateIdf.Text = null;
                ddlSearchDate.SelectedValue = "0";
                _ddSec_dept.SelectedValue = "0";
                dateId.Enabled = false;
                dateIdf.Enabled = false;
                selectjob();
                break;
            case "resetallpage":
                txtseach_noinvoice.Text = null;
                Txtseach_names.Text = null;
                Hidd_stastdate.Value = null;
                Hidd_enddate.Value = null;
                txtseach_stastdate.Text = null;
                txtseach_enddate.Text = null;
                DDseach_dateall.SelectedValue = "0";

                ddOrg_seachall.SelectedValue = "0";
                ddDept_seachall.Items.Clear();
                ddDept_seachall.Items.Insert(0, new ListItem("--- เลือกฝ่าย ---", "0"));
                ddSec_seachall.Items.Clear();
                ddSec_seachall.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "0"));
   
                //ddDept_seachall.SelectedValue = "0";
                //ddSec_seachall.SelectedValue = "0";
                txtseach_stastdate.Enabled = false;
                txtseach_enddate.Enabled = false;
                pageall_work();
                break;

            case "detail":

                string[] arg1 = new string[8];
                arg1 = e.CommandArgument.ToString().Split(';');
                int id = int.Parse(arg1[0]);
                int nodeid = int.Parse(arg1[1]);
                int actorid = int.Parse(arg1[2]);
                int statusid = int.Parse(arg1[3]);
                string dstartid = arg1[4];
                string dfinishid = arg1[5];
                int logid = int.Parse(arg1[6]);
                string no_invoice = arg1[7];
                // int dfinishid = int.Parse(arg1[5]);


                //  var id_idx = int.Parse(cmdArg);
                multimaster.SetActiveView(View3);
                //  TextBox start = ((TextBox)View3.FindControl("startdate"));
                DetailUser.ChangeMode(FormViewMode.Insert);
                DetailUser.DataBind();

                ViewState["id_idx"] = id;
                ViewState["node"] = nodeid;
                ViewState["actor"] = actorid;
                ViewState["status"] = statusid;
                ViewState["daystart"] = dstartid;
                ViewState["dayfinish"] = dfinishid;
                ViewState["jo_idx_ref"] = logid;
                ViewState["_no_invoice1"] = no_invoice;
                SelectViewIndex();

                string a = String.Empty;

                //if (ViewState["node"].ToString() == "3" || ViewState["actor"].ToString() == "3")
                //{
                    startdate.Text = null;
                    finaldate.Text = null;
                txt_comment_misH.Text = null;

                //foreach (GridViewRow row in jogrid.Rows)
                //{
                //    if (row.RowType == DataControlRowType.DataRow)
                //    {
                //        Label _jogrid_sec = (row.Cells[1].FindControl("jogrid_sec") as Label);

                //        test_l.Text = _jogrid_sec.Text;


                //    }

                //}


                //}
                //else
                //{
                //    startdate.Text = ViewState["startdate"].ToString();//.Text;
                //    finaldate.Text = ViewState["finaldate"].ToString();
                //}

                try
                {
                    string getPathLotus = ConfigurationManager.AppSettings["pathfile_Job_order"];

                    string filePathLotus = Server.MapPath(getPathLotus + ViewState["_no_invoice1"].ToString());
                    DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);

                    SearchDirectories_Vmishead(myDirLotus, ViewState["_no_invoice1"].ToString());
                }
                catch
                {
                }

                SETFOCUS_ONTOP.Focus();
                break;

            case "accept":
               
                string cmdArgView = cmdArg;
            
                //int viewall = int.Parse(cmdArgView[1]);

                //test_l.Text = cmdArgView.ToString();
             
           
                TextBox stdate = ((TextBox)View3.FindControl("startdate"));
                TextBox fndate = ((TextBox)View3.FindControl("finaldate"));
                // DateTime dateja ;
                // dateja = DateTime.Today.ToString("dd/mm/yyyy");
                DateTime dateja = DateTime.Now;
                string format = "dd/MM/yyyy";

                data_jo bb1 = new data_jo();
                job_order_overview bb2 = new job_order_overview();
                bb1.job_order_overview_action = new job_order_overview[1];
                // ViewState["txtstartdate"] = txtstartdate.Value;
                // ViewState["txtfinaldate"] = txtfinaldate.Value;
                ViewState["startdate"] = stdate.Text;
                ViewState["finaldate"] = fndate.Text;

                string[] date_startedit = new string[3];
                date_startedit = ViewState["startdate"].ToString().Split('/');
                string day_startedit = date_startedit[0];
                string month_startedit = date_startedit[1];
                string year_startedit = date_startedit[2];

                string[] date_finaledit = new string[3];
                date_finaledit = ViewState["finaldate"].ToString().Split('/');
                string day_finaledit = date_finaledit[0];
                string month_finaledit = date_finaledit[1];
                string year_finaledit = date_finaledit[2];

                String daytoday = dateja.Day.ToString();
                String monthtoday = dateja.Month.ToString();
                String yeartoday = dateja.Year.ToString();
                int ps = 3;
                int os = 1;
                ViewState["year_futer"] = int.Parse(yeartoday) + ps;
                ViewState["year_futer_age"] = int.Parse(yeartoday) + os;
                ViewState["check"] = "0";
                ViewState["check_to_oak"] = "0";
                //  ViewState["checkdaymin"] = dateja.ToString(format);

                if (int.Parse(year_startedit.ToString()) < int.Parse(yeartoday.ToString()))
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ปีที่เริ่มต้น น้อยกว่า ปีปัจจุบัน');", true);
                }
                else if (int.Parse(year_startedit.ToString()) == int.Parse(yeartoday.ToString()))
                {
                    if (int.Parse(month_startedit.ToString()) < int.Parse(monthtoday.ToString()))
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('เดือนที่เริ่มต้น น้อยกว่า เดือนปัจจุบัน');", true);
                    }
                    else if (int.Parse(month_startedit.ToString()) == int.Parse(monthtoday.ToString()))
                    {
                        if (int.Parse(day_startedit.ToString()) < int.Parse(daytoday.ToString()))
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('วันที่เริ่มต้น น้อยกว่า วันที่ปัจจุบัน');", true);
                        }
                        else
                        {
                            ViewState["check_to_oak"] = "1";
                        }
                    }
                    else
                    {
                        ViewState["check_to_oak"] = "1";
                    }
                }
                else if (int.Parse(year_startedit.ToString()) > int.Parse(ViewState["year_futer_age"].ToString()))
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ระยะเวลาเริ่มต้น ห้ามเกิน 1 ปี');", true);
                }
                else
                {
                    ViewState["check_to_oak"] = "1";
                }

                if (ViewState["check_to_oak"].ToString() == "1")
                {
                    if (int.Parse(year_finaledit.ToString()) > int.Parse(ViewState["year_futer"].ToString()))      // ถ้า ปีสินสุดงาน มากกว่า 3 ปี ไม่ได้
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('วันที่สิ้นสุดงานเกิน 3 ปี');", true);
                    }
                    else if (int.Parse(year_finaledit.ToString()) <= int.Parse(ViewState["year_futer"].ToString()))    // ถ้า ปีสินสุดงาน เท่ากับน้อยกว่า 3 ปีล่วงหน้า จะได้
                    {
                        if (int.Parse(year_finaledit.ToString()) > int.Parse(year_startedit.ToString()))
                        {
                            ViewState["check"] = "1";
                        }
                        else if (int.Parse(year_finaledit.ToString()) == int.Parse(year_startedit.ToString()))
                        {
                            if (int.Parse(month_finaledit.ToString()) > int.Parse(month_startedit.ToString()))
                            {
                                ViewState["check"] = "1";
                            }
                            else if (int.Parse(month_finaledit.ToString()) == int.Parse(month_startedit.ToString()))
                            {
                                if (int.Parse(day_finaledit.ToString()) > int.Parse(day_startedit.ToString()))
                                {
                                    ViewState["check"] = "1";
                                }
                                else if (int.Parse(day_finaledit.ToString()) == int.Parse(day_startedit.ToString()))
                                {
                                    ViewState["check"] = "1";
                                }
                                else
                                {
                                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('วันสิ้นสุด น้อยกว่า วันเริ่มต้น');", true);
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('เดือนสิ้นสุด น้อยกว่า เดือนเริ่มต้น');", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ปีสิ้นสุด น้อยกว่า ปีเริ่มต้น');", true);
                        }
                    }
                }


                if (ViewState["check"].ToString() == "1")
                {


                   
                    bb2.u0_jo_idx = int.Parse(ViewState["id_idx"].ToString()); //box2.ตัวแปรฝั่ง sql
                    bb2.day_start = ViewState["startdate"].ToString();
                    bb2.day_finish = ViewState["finaldate"].ToString();


                    bb1.job_order_overview_action[0] = bb2;

                   
                     //test_l.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(bb1));

                    bb1 = callService(_urlUpdatedaystartfinal, bb1);

                    //test_l.Text = ViewState["node"].ToString();
                    //tangura.Text = ViewState["actor"].ToString();

                    acceptnode();

                    //setActiveTab("view1", 0);
                    //multimaster.SetActiveView(View1);



                    switch (cmdArgView.ToString())
                    {
                        case "0":
                            setActiveTab("view1", 0);
                            multimaster.SetActiveView(View1);

                            break;

                        case "1":
                            setActiveTab("viewall_pages", 0);
                            multimaster.SetActiveView(viewall_pages);
                            break;

                    }



                    // selectmheadjob();
                }

                SETFOCUS_ONTOP.Focus();
                break;
            case "acceptsent":

                string cmdArgViewtime = cmdArg;
                data_jo yaibox = new data_jo();
                job_order_overview lekbok = new job_order_overview();
                yaibox.job_order_overview_action = new job_order_overview[1];

            //    Panel userpanel11 = ((Panel)View4.FindControl("userpanel"));
                FormView DetailUsersent_ = ((FormView)View1.FindControl("DetailUsersent"));

                Label sec_mis_date_ = ((Label)DetailUsersent_.FindControl("sec_mis_date"));
                TextBox txt_comment_usertime = ((TextBox)View1.FindControl("txt_comment_usertime"));
                TextBox txt_comment_usertimeall = ((TextBox)viewall_pages.FindControl("txt_comment_usertimeall"));
                
                lekbok.u0_jo_idx =  int.Parse(ViewState["id_idx"].ToString());
                lekbok.m0_node_idx = int.Parse(ViewState["node"].ToString());
                lekbok.m0_actor_idx = int.Parse(ViewState["actor"].ToString());
                lekbok.sec_mis = int.Parse(sec_mis_date_.Text);
                lekbok.id_statework = 5;
                lekbok.emp_id_creator = _emp_idx;

                switch (cmdArgViewtime.ToString())
                {
                    case "0":
                        if (txt_comment_usertime.Text == "")
                        {
                            lekbok.comment = "-";
                        }
                        else
                        {
                            lekbok.comment = txt_comment_usertime.Text;
                        }
                        break;
                    case "1":
                        if (txt_comment_usertimeall.Text == "")
                        {
                            lekbok.comment = "-";
                        }
                        else
                        {
                            lekbok.comment = txt_comment_usertimeall.Text;
                        }

                        break;
                }
               

                yaibox.job_order_overview_action[0] = lekbok;
             //   test_l.Text = sec_mis_date_.Text;
                yaibox = callService(_urlUpdatestau0, yaibox);
                // insertlog2();

                //setActiveTab("view1", 0);
                //multimaster.SetActiveView(View1);
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "edit1": //edit หน้าแรก

               
                string[] argument = new string[6];
                argument = e.CommandArgument.ToString().Split(';');
                int u0_idx = int.Parse(argument[0]);
                int node_idx = int.Parse(argument[1]);
                int actor_idx = int.Parse(argument[2]);
                int status_idx = int.Parse(argument[3]);
                int jo_idx_ref = int.Parse(argument[4]);
                string _no_invoice = argument[5];
                int emp_create = int.Parse(argument[6]);
                //   string adminname = argument[5];
           
                //  p1.Text =  argument[2];
                //   ViewState["idx"] =  idx ;
                ViewState["idx"] = u0_idx;
                ViewState["node_idx"] = node_idx;
                ViewState["actor_idx"] = actor_idx;
                ViewState["id_statework"] = status_idx;
                ViewState["jo_idx_ref"] = jo_idx_ref;
                ViewState["_no_invoice"] = _no_invoice;
                ViewState["EMP_Creator"] = emp_create;


                multimaster.SetActiveView(View4);
               
                //selectjob();
                selecttableu1();
                SelectViewIndex2();
                // pageall_work();

                //u1grid.DataBind();

             //   test_l.Text = /*ViewState["EMP_HUMAN"].ToString();*/ ViewState["EMP_Creator"].ToString();

                LinkButton btnedit1 = (LinkButton)DetaileditUser.FindControl("btnedit");
                linkBtnTrigger(btnedit1);

                try
                {

                    string getPathLotus = ConfigurationManager.AppSettings["pathfile_Job_order"];

                    string filePathLotus = Server.MapPath(getPathLotus + ViewState["_no_invoice"].ToString());
                    DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);

                    SearchDirectories(myDirLotus, ViewState["_no_invoice"].ToString());

                     
                }
                catch
                {

                }
                //Label log_date = (Label)gv_logjob.FindControl("log_date");
                //Label log_actor = (Label)gv_logjob.FindControl("log_actor");
                //Label log_nodename = (Label)gv_logjob.FindControl("log_nodename");
                //Label log_name_sta = (Label)gv_logjob.FindControl("log_name_sta");

                data_jo bigbox = new data_jo();
                job_order_overview smallbox = new job_order_overview();

                bigbox.job_order_overview_action = new job_order_overview[1];
                smallbox.u0_jo_idx = u0_idx;
                 
              //  smallbox.day_created = log_date.Text;
                //smallbox.nameActor = log_actor.Text;
                //smallbox.node_name = log_nodename.Text;
                //smallbox.statenode_name = log_nodename.Text;
                smallbox.no_invoice = _no_invoice;
               // smallbox.emp_id_creator = _emp_idx;//int.Parse(ViewState["EMP_Creator"].ToString());
                bigbox.job_order_overview_action[0] = smallbox;
                bigbox = callService(_urlSelectDatajoborder_log, bigbox);

                // test_l.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(bigbox));


                gv_logjob.DataSource = bigbox.job_order_overview_action;
                gv_logjob.DataBind();

                // setGridData(gv_logjob, bigbox.job_order_overview_action);


                break;
            case "edit2":
                var idxchange = int.Parse(cmdArg);
                LinkButton btnedit = (LinkButton)DetaileditUser.FindControl("btnedit");
                linkBtnTrigger(btnedit);


                //   DetaileditUser.ChangeMode(FormViewMode.Edit);
                //   DetaileditUser.DataBind();
                ViewState["idxchange"] = idxchange;
                SelectViewIndex3();
                DetaileditUser.Focus();
                break;
            case "cancelbut":
                // DetaileditUser.ChangeMode(FormViewMode.ReadOnly);
                // DetaileditUser.DataBind();
                //FormView Detailedit = ((FormView)userpanel.FindControl("DetaileditUser"));
                //GridView gvFile = ((GridView)Detailedit.FindControl("gvFile"));
                //gvFile.Visible = true;         
                SelectViewIndex2();

                try
                {

                    string getPathLotus = ConfigurationManager.AppSettings["pathfile_Job_order"];

                    string filePathLotus = Server.MapPath(getPathLotus + ViewState["_no_invoice"].ToString());
                    DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);

                    SearchDirectories(myDirLotus, ViewState["_no_invoice"].ToString());


                }
                catch
                {

                }

                SETFOCUS_ONTOP.Focus();
                break;

            case "Updateedit":
                linkBtnTrigger(truebut);

                data_jo box1 = new data_jo();
                job_order_overview box2 = new job_order_overview();
                // l0_job_order box3 = new l0_job_order();
                box1.job_order_overview_action = new job_order_overview[1];

                var titledit = (TextBox)DetaileditUser.FindControl("titledit");
                var reqedit = (TextBox)DetaileditUser.FindControl("reqeditshow");
                TextBox txt_comment_edit = ((TextBox)DetaileditUser.FindControl("txt_comment_edit"));
                // ViewState["u0_jo_idx"] = edit_idx;

                box2.title_jo = titledit.Text;
                box2.require_jo = reqedit.Text;
                // box2.u0_jo_idx = edit_idx;
                box2.u0_jo_idx = int.Parse(ViewState["idx"].ToString()); //box2.ตัวแปรฝั่ง sql
                // box2.m0_node_idx = int.Parse(ViewState["node_idx"].ToString());
                // box2.m0_actor_idx = int.Parse(ViewState["actor_idx"].ToString());
               
                box2.m0_node_idx = 1;
                box2.m0_actor_idx = 1;
                box2.id_statework = 1;//int.Parse(ViewState["id_statework"].ToString());
                
                box2.jo_idx_ref = int.Parse(ViewState["jo_idx_ref"].ToString());
                box2.from_table = "u0";
                box2.emp_id_creator = _emp_idx;//int.Parse(ViewState["EMP_HUMAN"].ToString());
                box1.job_order_overview_action[0] = box2;

                if (txt_comment_edit.Text == "")
                {
                    box2.comment = "-";
                }
                else
                {
                    box2.comment = txt_comment_edit.Text;
                }

                // box1.l0_job_order_action[0] = box3;
                // p1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(box1));

               box1 = callService(_urlUpdateDatajoborder, box1);
                //

               // setFormData(DetaileditUser, FormViewMode.Edit, null);
                FileUpload UploadFileComment_edit = (FileUpload)DetaileditUser.FindControl("UploadFileComment_edit");

                if (UploadFileComment_edit.HasFile)
                {
                    string getPathfile2 = ConfigurationManager.AppSettings["pathfile_Job_order"];
                    string document_code = ViewState["_no_invoice"].ToString();
                    string filePath_old = Server.MapPath(getPathfile2 + document_code);
                    if (!Directory.Exists(filePath_old))
                    {
                        Directory.CreateDirectory(filePath_old);
                        string extension = Path.GetExtension(UploadFileComment_edit.FileName);
                        UploadFileComment_edit.SaveAs(Server.MapPath(getPathfile2 + document_code) + "\\" + document_code + extension);
                    }
                    else
                    {
                        string imagePath = string.Empty;
                        List<string> files = new List<string>();
                        DirectoryInfo dirListRoom = new DirectoryInfo(filePath_old);
                        foreach (FileInfo file in dirListRoom.GetFiles())
                        {
                            imagePath = ResolveUrl(filePath_old + "\\" + file.Name);
                        }
                        File.Delete(imagePath);
                        string extension = Path.GetExtension(UploadFileComment_edit.FileName);
                        UploadFileComment_edit.SaveAs(Server.MapPath(getPathfile2 + document_code) + "\\" + document_code + extension);
                    }

                }

                SelectViewIndex2();
                try
                {

                    string getPathLotus = ConfigurationManager.AppSettings["pathfile_Job_order"];

                    string filePathLotus = Server.MapPath(getPathLotus + ViewState["_no_invoice"].ToString());
                    DirectoryInfo myDirLotus1 = new DirectoryInfo(filePathLotus);

                    SearchDirectories(myDirLotus1, ViewState["_no_invoice"].ToString());


                }
                catch
                {

                }

                //data_jo bigbox_log = new data_jo();
                //job_order_overview smallbox_log = new job_order_overview();

                //bigbox_log.job_order_overview_action = new job_order_overview[1];
                //smallbox_log.u0_jo_idx = int.Parse(ViewState["idx"].ToString());

                ////  smallbox.day_created = log_date.Text;
                ////smallbox.nameActor = log_actor.Text;
                ////smallbox.node_name = log_nodename.Text;
                ////smallbox.statenode_name = log_nodename.Text;
                ////smallbox_log.no_invoice = _no_invoice;
                //bigbox_log.job_order_overview_action[0] = smallbox_log;
                //bigbox_log = callService(_urlSelectDatajoborder_log, bigbox_log);

                //// test_l.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(bigbox));
                //gv_logjob.DataSource = bigbox_log.job_order_overview_action;
                //gv_logjob.DataBind();


                Panel userpanel11 = ((Panel)View4.FindControl("userpanel"));
                FormView DetaileditUser1 = ((FormView)userpanel11.FindControl("DetaileditUser"));
                Label sec_mis_edit_ = ((Label)DetaileditUser1.FindControl("sec_mis_edit"));
               

                data_jo boxu0 = new data_jo();
                job_order_overview boxs0 = new job_order_overview();
                boxu0.job_order_overview_action = new job_order_overview[1];
                boxs0.u0_jo_idx = int.Parse(ViewState["idx"].ToString());
                boxs0.m0_actor_idx = 1; //from_ac
                boxs0.m0_node_idx = 1; // from_M0_node
                boxs0.id_statework = 1;// node_decision
                
                boxs0.emp_id_creator = _emp_idx;
                boxs0.sec_mis = int.Parse(sec_mis_edit_.Text);
                boxs0.condition = 1;
                boxu0.job_order_overview_action[0] = boxs0;
                boxu0 = callService(_urlUpdatestau0, boxu0);

                //test_l.Text = sec_mis_edit_.Text;

                setActiveTab("view1", 0);
                multimaster.SetActiveView(View1);


                SETFOCUS_ONTOP.Focus();
              break;
            
            case "showsent":

                // int sent = int.Parse(cmdArg);
                string[] arg2 = new string[4];
                arg2 = e.CommandArgument.ToString().Split(';');
                int ids = int.Parse(arg2[0]);
                int nodeids = int.Parse(arg2[1]);
                int actorids = int.Parse(arg2[2]);
                int statusids = int.Parse(arg2[3]);
                ViewState["id_idx"] = ids;
                ViewState["node"] = nodeids;
                ViewState["actor"] = actorids;
                ViewState["status"] = statusids;


                TextBox txt_comment_usertimeall1 = ((TextBox)viewall_pages.FindControl("txt_comment_usertimeall"));
                TextBox txt_comment_usertime1 = ((TextBox)View1.FindControl("txt_comment_usertime"));
                txt_comment_usertimeall1.Text = null;
                txt_comment_usertime1.Text = null;

                data_jo bgbox = new data_jo();
                job_order_overview smbox = new job_order_overview();
                bgbox.job_order_overview_action = new job_order_overview[1];

                smbox.u0_jo_idx = int.Parse(ViewState["id_idx"].ToString());
                smbox.m0_node_idx = int.Parse(ViewState["node"].ToString());
                smbox.m0_actor_idx = int.Parse(ViewState["actor"].ToString());
                smbox.id_statework = int.Parse(ViewState["status"].ToString());
                smbox.emp_id_creator = _emp_idx;
                bgbox.job_order_overview_action[0] = smbox;
                bgbox = callService(_urlSelectdaystartfinal, bgbox);
                // p1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(bgbox));
                setFormViewData(DetailUsersent, FormViewMode.ReadOnly, bgbox.job_order_overview_action);
                setFormViewData(FormView1, FormViewMode.ReadOnly, bgbox.job_order_overview_action);
                ScriptManager.RegisterStartupScript(this,
                    this.GetType(), "Pop", "openModal();", true);
                break;

            case "addaf":
               
                string[] arg3 = new string[4];
                arg3 = e.CommandArgument.ToString().Split(';');
                int u0_idx1 = int.Parse(arg3[0]);
                int node_idx1 = int.Parse(arg3[1]);
                int actor_idx1 = int.Parse(arg3[2]);
                int status_idx1 = int.Parse(arg3[3]);

                ViewState["idx"] = u0_idx1;
                ViewState["node_idx"] = node_idx1;
                ViewState["actor_idx"] = actor_idx1;
                ViewState["id_statework"] = status_idx1;

                Panel userpanel1 = ((Panel)View4.FindControl("userpanel"));
                FormView DetaileditUseredit = ((FormView)userpanel1.FindControl("DetaileditUser"));
                Label sec_mis_edit = ((Label)DetaileditUseredit.FindControl("sec_mis_edit"));
                TextBox comment_noedit1 = ((TextBox)DetaileditUseredit.FindControl("txt_comment_noedit"));

                data_jo box0 = new data_jo();
                job_order_overview box11 = new job_order_overview();
                box0.job_order_overview_action = new job_order_overview[1];
                box11.u0_jo_idx = int.Parse(ViewState["idx"].ToString());
                box11.m0_actor_idx = 1; //from_ac
               // box11.comment = "-";
                box11.m0_node_idx = 1; // from_M0_node
                box11.id_statework = 1;// node_decision
                box11.sec_mis = int.Parse(sec_mis_edit.Text);
                box11.emp_id_creator = _emp_idx;

                if (comment_noedit1.Text == "")
                {
                    box11.comment = "-";

                }
                else
                {
                    box11.comment = comment_noedit1.Text;
                }

               // box11.condition = 1;
                box0.job_order_overview_action[0] = box11;

              //  test_l.Text = sec_mis_edit.Text;

               box0 = callService(_urlUpdatestau0, box0);

                setActiveTab("view1", 0);
                multimaster.SetActiveView(View1);
                //Page.Response.Redirect(Page.Request.Url.ToString(), true);
                //  ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ทำการส่งเสร็จสิ้น');", true);
                SETFOCUS_ONTOP.Focus();
                break;
            case "showsearch":

                DropDownList _ddSec_dept1 = ((DropDownList)View1.FindControl("ddSec_dept"));

                searchhidden.Visible = true;
                searchbutton.Visible = false;
                _divsearch.Visible = true;
                //selectjob();
                TempCodeId.Text = String.Empty;
                //txtdateId.Value = String.Empty;
                //txtdateIdf.Value = String.Empty;
                empidsearch.Text = String.Empty;
                dateId.Enabled = false;
                dateIdf.Enabled = false;
                _ddSec_dept1.SelectedValue = "0";
                ddlSearchDate.SelectedValue = "0";
                break;

            case "showsearch1":
                searchhidden.Visible = false;
                searchbutton.Visible = true;
                _divsearch.Visible = false;
                //TempCodeId.Text = String.Empty;
                //empidsearch.Text = String.Empty;
                //ddlSearchDate.SelectedValue = "0";
                //mhbtn.Visible = true;
                 selectjob();

                break;
            case "show_searchall":

            

                btnclose_searchall.Visible = true;
                btnshow_searchall.Visible = false;
                Divseach_all.Visible = true;
                //mhbtn.Visible = false;
                txtseach_noinvoice.Text = String.Empty;
                Txtseach_names.Text = String.Empty;
                txtseach_stastdate.Enabled = false;
                txtseach_enddate.Enabled = false;
                DDseach_dateall.SelectedValue = "0";
                ddOrg_seachall.SelectedValue = "0";
                ddDept_seachall.Items.Clear();
                ddDept_seachall.Items.Insert(0, new ListItem("--- เลือกฝ่าย ---", "0"));
                ddSec_seachall.Items.Clear();
                ddSec_seachall.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "0"));





                break;

            case "close_searchall":
                btnclose_searchall.Visible = false;
                btnshow_searchall.Visible = true;
                Divseach_all.Visible = false;
                pageall_work();
                //mhbtn.Visible = true;
                break;


            case "btncommandsearch":
              //  int m0_id = int.Parse(cmdArg);

                // ViewState["noinvoiceidsearch"] = TempCodeId.Text.Trim();
                TextBox temp = (TextBox)View1.FindControl("TempCodeId");
                TextBox txt_datecreate = (TextBox)View1.FindControl("dateId");
                TextBox txt_daterecieve = (TextBox)View1.FindControl("dateIdf");
                DropDownList _ddlSearchDate = (DropDownList)View1.FindControl("ddlSearchDate");
                DropDownList ddSec_dept = (DropDownList)View1.FindControl("ddSec_dept");
                TextBox emps = (TextBox)View1.FindControl("empidsearch");
                ViewState["searchdate"] = txtdateId.Value;
                txt_datecreate.Text = ViewState["searchdate"].ToString();
                txtdateId.Value = ViewState["searchdate"].ToString();

                ViewState["searchto"] = txtdateIdf.Value;
                txt_daterecieve.Text = ViewState["searchto"].ToString();
                txtdateIdf.Value = ViewState["searchto"].ToString();



                // TextBox txt_datecreate =  (TextBox)View1.FindControl("dateId");
                data_jo searchbig = new data_jo();
                job_order_overview searchsmall = new job_order_overview();
                searchbig.job_order_overview_action = new job_order_overview[1];

                // searchsmall.u0_jo_idx = int.Parse(ViewState["id_idx"].ToString()); //box2.ตัวแปรฝั่ง sql
                searchsmall.noinvoice_search = temp.Text;
                searchsmall.ep_search = emps.Text;
               // searchsmall.m0_node_idx = m0_id;//int.Parse(m0_id.ToString());
                searchsmall.day_search = txtdateId.Value;
                searchsmall.day_searchto = txtdateIdf.Value;
                searchsmall.if_date = int.Parse(_ddlSearchDate.SelectedValue);
                searchsmall.emp_dep_job = int.Parse(ViewState["_rdept_idx"].ToString());

                //searchsmall.Org_search = int.Parse(ViewState["_org_idx"].ToString());
                ////searchsmall.Organize = ddlOrg.SelectedItem.Text;
                //searchsmall.Dept_search = int.Parse(ViewState["_rdept_idx"].ToString());
                ////searchsmall.Deptname = ddlDept.SelectedItem.Text;
                searchsmall.Sec_search = int.Parse(ddSec_dept.SelectedValue);
                searchsmall.Secname = ddSec_dept.SelectedItem.Text;

                //test_l.Text = ViewState["_rdept_idx"].ToString();

                //  p1.Text = txtstartdate.Value;
                // p1.Text = txtdateId.Value;
                // searchsmall.m0_actor_idx = int.Parse(ViewState["actor"].ToString());
                // searchsmall.id_statework = 2;

                searchbig.job_order_overview_action[0] = searchsmall;

                ViewState["_rsec_search"] = searchbig.job_order_overview_action[0].rsec_creator.ToString();
                ViewState["actor_search"] = searchbig.job_order_overview_action[0].m0_actor_idx.ToString();
                ViewState["node_search"] = searchbig.job_order_overview_action[0].m0_node_idx.ToString();

                searchbig = callService(_urlSearchJoborder, searchbig);

           
                ViewState["select_deptwork"] = searchbig.job_order_overview_action;
                setGridData(jogrid, ViewState["select_deptwork"]);


                getSectionList_dept(ddSec_dept, int.Parse(ViewState["_org_idx"].ToString()), int.Parse(ViewState["_rdept_idx"].ToString()));


                //if (searchbig.return_code == 0)
                //{


                //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('!!! ไม่พบข้อมูลเอกสารที่ท่านต้องการ');", true);

                //}



                //  p1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(searchbig));

                // jogrid.DataBind();
                // selectjob();
                // jogrid.DataBind();

                //TempCodeId.Text = null;
                //txtdateId.Value = null;
                //txtdateIdf.Value = null;
                //empidsearch.Text = null;
                //dateId.Text = null;
                //dateIdf.Text = null;
                //ddlSearchDate.SelectedValue = "0";
                //dateId.Enabled = false;
                //dateIdf.Enabled = false;
                // selectjob();



                break;
            case "seachallpage":

                // ViewState["noinvoiceidsearch"] = TempCodeId.Text.Trim();
                TextBox _txtseach_noinvoice = (TextBox)viewall_pages.FindControl("txtseach_noinvoice");
                TextBox _txtseach_stastdate = (TextBox)viewall_pages.FindControl("txtseach_stastdate");
                TextBox _txtseach_enddate = (TextBox)viewall_pages.FindControl("txtseach_enddate");
                DropDownList _DDseach_dateall = (DropDownList)viewall_pages.FindControl("DDseach_dateall");
                TextBox _Txtseach_names = (TextBox)viewall_pages.FindControl("Txtseach_names");

                DropDownList ddlOrg = (DropDownList)viewall_pages.FindControl("ddOrg_seachall");
                DropDownList ddlDept = (DropDownList)viewall_pages.FindControl("ddDept_seachall");
                DropDownList ddlSec = (DropDownList)viewall_pages.FindControl("ddSec_seachall");

                ViewState["_Hidd_stastdate"] = Hidd_stastdate.Value;
                _txtseach_stastdate.Text = ViewState["_Hidd_stastdate"].ToString();
                Hidd_stastdate.Value = ViewState["_Hidd_stastdate"].ToString();

                ViewState["_Hidd_enddate"] = Hidd_enddate.Value;
                _txtseach_enddate.Text = ViewState["_Hidd_enddate"].ToString();
                Hidd_enddate.Value = ViewState["_Hidd_enddate"].ToString();

                // TextBox txt_datecreate =  (TextBox)View1.FindControl("dateId");
                data_jo searchallpage = new data_jo();
                job_order_overview searchalldelatil = new job_order_overview();
                searchallpage.job_order_overview_action = new job_order_overview[1];

                // searchsmall.u0_jo_idx = int.Parse(ViewState["id_idx"].ToString()); //box2.ตัวแปรฝั่ง sql

                searchalldelatil.noinvoice_search = _txtseach_noinvoice.Text;
                searchalldelatil.ep_search = _Txtseach_names.Text;
                searchalldelatil.condition = 1;
                searchalldelatil.day_search = Hidd_stastdate.Value;
                searchalldelatil.day_searchto = Hidd_enddate.Value;
                searchalldelatil.if_date = int.Parse(_DDseach_dateall.SelectedValue);

                searchalldelatil.Org_search = int.Parse(ddlOrg.SelectedValue);
                searchalldelatil.Organize = ddlOrg.SelectedItem.Text;
                searchalldelatil.Dept_search = int.Parse(ddlDept.SelectedValue);
                searchalldelatil.Deptname = ddlDept.SelectedItem.Text;
                searchalldelatil.Sec_search = int.Parse(ddlSec.SelectedValue);
                searchalldelatil.Secname = ddlSec.SelectedItem.Text;

                searchalldelatil.rdep_num = int.Parse(ViewState["_rdept_idx"].ToString());

                searchallpage.job_order_overview_action[0] = searchalldelatil;

                ViewState["_rsec_searchall"] = searchallpage.job_order_overview_action[0].rsec_creator.ToString();
                ViewState["actor_searchall"] = searchallpage.job_order_overview_action[0].to_m0_actor.ToString();
                ViewState["node_searchall"] = searchallpage.job_order_overview_action[0].to_m0_node.ToString();

               // test_l.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(searchallpage));

               
                searchallpage = callService(_urlSearchJoborder, searchallpage);

                ViewState["select_allwork"] = searchallpage.job_order_overview_action;
                setGridData(gv_allwork, ViewState["select_allwork"]);

                //getOrganizationList(ddlOrg);
                //getDepartmentList(ddlDept, int.Parse(ddlOrg.SelectedValue));
                //getSectionList(ddlSec, int.Parse(ddlOrg.SelectedValue), int.Parse(ddlDept.SelectedValue));

                //if (searchallpage.return_code == 0)
                //{

                //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('!!! ไม่พบข้อมูลเอกสารที่ท่านต้องการ');", true);

                //}

                break;
        }
    }

    #endregion

    #region formview
    protected void formviewhead_DataBound(object sender, EventArgs e)
    {
        var FvName = (FormView)sender;

        switch (FvName.ID)
        {
            case "formviewhead":
                FormView FVhead = (FormView)viewhead.FindControl("formviewhead");
                break;
        }
    }

    protected void formviewmis_DataBound(object sender, EventArgs e)
    {
        var FvName = (FormView)sender;

        switch (FvName.ID)
        {
            case "formviewmis":
                FormView FVhead = (FormView)viewhead.FindControl("formviewmis");
                break;
        }
    }

    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        var FvName = (FormView)sender;

        switch (FvName.ID)
        {

            case "FvDetailUserRepair":
                FormView FvDetailUser = (FormView)create_story.FindControl("FvDetailUserRepair");

                if (FvDetailUser.CurrentMode == FormViewMode.Insert)
                {
                    var txtempcode = ((TextBox)FvDetailUser.FindControl("txtempcode"));
                    var txtrequesname = ((TextBox)FvDetailUser.FindControl("txtrequesname"));
                    var txtrequesdept = ((TextBox)FvDetailUser.FindControl("txtrequesdept"));
                    var txtsec = ((TextBox)FvDetailUser.FindControl("txtsec"));
                    var txtpos = ((TextBox)FvDetailUser.FindControl("txtpos"));
                    var txtemail = ((TextBox)FvDetailUser.FindControl("txtemail"));
                    var txttel = ((TextBox)FvDetailUser.FindControl("txttel"));
                    var txtorg = ((TextBox)FvDetailUser.FindControl("txtorg"));

                    txtempcode.Text = ViewState["EmpCode"].ToString();
                    txtrequesname.Text = ViewState["FullName"].ToString();
                    txtorg.Text = ViewState["Org_name"].ToString();
                    txtrequesdept.Text = ViewState["rdept_name"].ToString();
                    txtsec.Text = ViewState["Secname"].ToString();
                    txtpos.Text = ViewState["Positname"].ToString();
                    txttel.Text = ViewState["Tel"].ToString();
                    txtemail.Text = ViewState["Email"].ToString();



                }
                break;
        }
    }

    protected void setFormViewData(FormView foName, FormViewMode foMode, Object obj)
    {
        foName.ChangeMode(foMode);
        foName.DataSource = obj;
        foName.DataBind();
    }

    protected void Detail_DataBound(object sender, EventArgs e)
    {
        var FoName = (FormView)sender;
        switch (FoName.ID)
        {

            case "DetailUser":
                FormView DetailUser = (FormView)View3.FindControl("DetailUser");
                break;


            case "DetailUserviewH":
                FormView DetailUserviewH = (FormView)View3.FindControl("DetailUserviewH");
                break;

            case "DetailUserpv":
                FormView DetailUserpv = (FormView)View4.FindControl("DetailUserpv");

                
                break;

            case "DetaileditUser":
                Panel userpanel = ((Panel)View4.FindControl("userpanel"));
                FormView DetaileditUser = ((FormView)userpanel.FindControl("DetaileditUser"));

                // DetaileditUser.ChangeMode(FormViewMode.Edit);
                if (DetaileditUser.CurrentMode == FormViewMode.Edit)
                {
                    TextBox reqedit = ((TextBox)DetaileditUser.FindControl("reqedit"));
                    TextBox reqeditshow = ((TextBox)DetaileditUser.FindControl("reqeditshow"));
                    ViewState["decode"] = reqedit.Text;
                    reqeditshow.Text = HttpUtility.HtmlDecode(ViewState["decode"].ToString());

                    linkBtnTrigger(truebut);
                    
                }

                break;
        }

    }

    protected void DetailUsersent_DataBound(object sender, EventArgs e)
    {
        var FoName = (FormView)sender;
        switch (FoName.ID)
        {
            case "DetailUsersent":
                FormView DetailUsersent = (FormView)View1.FindControl("DetailUsersent");
                break;
        }
    }

    #endregion

    #region Masterbound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "gvinfv_showtitle":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                break;
            case "Gvmastermis":

                break;
            case "gvanthoer_old":

                //rdbt_end.Visible = false;
             //   test_l.Text = ViewState["EMP_Creator"].ToString();
                if (e.Row.RowState.ToString().Contains("Edit") || e.Row.RowType == DataControlRowType.DataRow)  //ตอน block ปุ่ม mis
                {
                    if (e.Row.RowState.ToString().Contains("Edit"))
                    {
                        GridView editGrid = sender as GridView;
                        int colSpan = editGrid.Columns.Count;
                        for (int i = 1; i < colSpan; i++)
                        {
                            e.Row.Cells[i].Visible = false;
                            e.Row.Cells[i].Controls.Clear();
                        }

                        e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                        e.Row.Cells[0].CssClass = "";

                    }
                    else
                    {
                        Literal _delete_ondelete = (Literal)FindControl("delete_ondelete");
                        Label _text_userhelp = (Label)e.Row.Cells[3].FindControl("text_userhelp");
                        Label _u1_emp_cre = (Label)e.Row.Cells[1].FindControl("u1_emp_cre");
                        TextBox _lmao = (TextBox)e.Row.Cells[3].FindControl("lmao");
                        Label _textalert_antoher = (Label)e.Row.Cells[5].FindControl("textalert_antoher");
                        Label _textalert = (Label)e.Row.Cells[5].FindControl("textalert");
                        LinkButton _user_okok = (LinkButton)e.Row.Cells[3].FindControl("user_okok");
                        LinkButton _user_nook = (LinkButton)e.Row.Cells[3].FindControl("user_nook");
                        Label _nameuse_acc = (Label)e.Row.Cells[3].FindControl("user_accept");
                        Label _day_accept = (Label)e.Row.Cells[3].FindControl("day_accept");
                        Label _time_accept = (Label)e.Row.Cells[3].FindControl("time_accept");
                        LinkButton _Edit = (LinkButton)e.Row.Cells[5].FindControl("Edit");
                        LinkButton _delettitle = (LinkButton)e.Row.Cells[5].FindControl("delettitle");
                        LinkButton _editsuccess = (LinkButton)e.Row.Cells[5].FindControl("editsuccess");
                        Label _timedateline1 = (Label)e.Row.Cells[2].FindControl("timedateline1");
                        Label _lateshow = (Label)e.Row.Cells[1].FindControl("lateshow");
                        Label _missend = (Label)e.Row.Cells[1].FindControl("missend");
                        TextBox _comment1 = (TextBox)e.Row.Cells[6].FindControl("txt_commentu1");



                        //  test_l.Text = _missend.Text;

                        ViewState["btnoof"] = _lmao.Text; //สถานะ
                        ViewState["testoak"] = delete_ondelete.Text;
                        string[] timeset_finish = new string[3];
                        string[] timeset_getma = new string[3];

                        timeset_finish = time_present_use.Text.Split('/');
                        string day_timefinish = timeset_finish[0];
                        string month_timefinish = timeset_finish[1];
                        string year_timefinish = timeset_finish[2];

                        timeset_getma = _timedateline1.Text.Split('/');
                        string day_getma = timeset_getma[0];
                        string month_getma = timeset_getma[1];
                        string year_getma = timeset_getma[2];

                        ViewState["day_late"] = "0";

                        if (int.Parse(year_getma) > int.Parse(year_timefinish))
                        {
                            ViewState["day_late"] = "1";
                        }
                        else if (int.Parse(year_getma) == int.Parse(year_timefinish))
                        {
                            if (int.Parse(month_getma) > int.Parse(month_timefinish))
                            {
                                ViewState["day_late"] = "1";
                            }
                            else if (int.Parse(month_getma) == int.Parse(month_timefinish))
                            {
                                if (int.Parse(day_getma) > int.Parse(day_timefinish))
                                {
                                    ViewState["day_late"] = "1";
                                }
                            }
                        }


                        if (ViewState["day_late"].ToString() == "1")
                        {
                            _lateshow.Visible = true;
                        }

                        if (ViewState["_rdept_idx"].ToString() == "20" && _u1_emp_cre.Text == _emp_idx.ToString())  //ถ้าเป็น mis และ เป็นคนสร้าง u1 นั้น จะทำไรไม่ได้ และเป็นคนทีไม่ได้อยู่ในแผนกเดียวกับ user
                        {
                            //   test_l.Text = _u1_emp_cre.Text;
                            _user_okok.Visible = false;
                            _user_nook.Visible = false;
                            _Edit.Visible = false;
                            _delettitle.Visible = false;
                            _editsuccess.Visible = false;
                            if (ViewState["btnoof"].ToString() == "0") //สถานะ = 0
                            {
                                _Edit.Visible = true;
                                _delettitle.Visible = true;
                                _editsuccess.Visible = true;
                                // _textalert_antoher.Visible = true;
                                if (ViewState["end_job"].ToString() == "1")
                                {
                                    _delettitle.Visible = false;
                                }
                                 if (ViewState["Sec_idx"].ToString() != _missend.Text)
                                {
                                    _Edit.Visible = false;
                                    _delettitle.Visible = false;
                                    _editsuccess.Visible = false;
                                    _textalert_antoher.Visible = true;
                                    _text_userhelp.Visible = true;
                                }
                            }
                            else if (ViewState["btnoof"].ToString() == "1") // ถ้าสถานะเป็น 1 ให้ขึ้นว่า รอดำเนินการ
                            {
                                _textalert.Visible = true;
                                _comment1.Visible = false;
                            }
                            else if (ViewState["btnoof"].ToString() == "2") // ถ้าสถานะเป็น 2 ให้ขึ้นว่า ใครรับ วันที่รับ
                            {
                                _nameuse_acc.Visible = true;
                                _day_accept.Visible = true;
                                _time_accept.Visible = true;
                                _comment1.Visible = false;
                            }

                        }

                        else if (_u1_emp_cre.Text != _emp_idx.ToString() && ViewState["Sec_idx"].ToString() != ViewState["_rsec_creator"].ToString()) //คนในฝ่าย MIS login และไม่ใช่คนที่ส่งมอบหัวข้อ
                        {
                            //  test_l.Text = _emp_idx.ToString();
                            // tangura.Text = ViewState["EMP_HUMAN"].ToString();
                            _user_okok.Visible = false;
                            _user_nook.Visible = false;
                            _Edit.Visible = false;
                            _delettitle.Visible = false;
                            _editsuccess.Visible = false;
                            if (ViewState["btnoof"].ToString() == "1") //ถ้าสถานะของหัวข้อรอให้ USER เซ็นต์รับงาน และให้คนในแผนกเดียวกันเซ็นต์รับงานแทนได้
                            {
                                _textalert.Visible = true;

                                //_user_okok.Visible = true;
                                //_user_nook.Visible = true;
                            }
                            else if (ViewState["btnoof"].ToString() == "2") // ถ้ามีการเซ็นต์รับงานเเล้ว ให้บอกว่าใครเป็นคนเซ็นต์
                            {
                                _user_okok.Visible = false;
                                _user_nook.Visible = false;
                                //   _textalert.Visible = true;
                                _nameuse_acc.Visible = true;
                                _day_accept.Visible = true;
                                _time_accept.Visible = true;
                                _comment1.Visible = false;
                            }
                            else if (ViewState["btnoof"].ToString() == "0") // ถ้า User ไม่ยอมรับเอกสาร ให้ส่งหัวข้อเพื่อกลับไปแก้ไข
                            {
                                _user_okok.Visible = false;
                                _user_nook.Visible = false;
                                _textalert.Visible = true;
                                _comment1.Visible = false;
                                if (ViewState["_rdept_idx"].ToString() == "20")
                                {
                                    _Edit.Visible = true;
                                    _delettitle.Visible = true;
                                    _editsuccess.Visible = true;
                                    _textalert.Visible = false;
                                }
                                if (ViewState["end_job"].ToString() == "1")
                                {
                                    _delettitle.Visible = false;
                                }
                                if (ViewState["Sec_idx"].ToString() != _missend.Text)
                                {
                                    _Edit.Visible = false;
                                    _delettitle.Visible = false;
                                    _editsuccess.Visible = false;
                                    _textalert_antoher.Visible = true;
                                    _text_userhelp.Visible = true;
                                    _comment1.Visible = false;
                                }

                                //if (ViewState["num_last"].ToString() == "8" || ViewState["num_last"].ToString() == "4")
                                //{
                                //    _delettitle.Visible = false;
                                //}
                            }                          
                        }
                        else if (ViewState["Sec_idx"].ToString() == ViewState["_rsec_creator"].ToString())
                        {
                            _Edit.Visible = false;
                            _delettitle.Visible = false;
                            _editsuccess.Visible = false;

                            if (ViewState["btnoof"].ToString() == "2")
                            {
                                _user_okok.Visible = false;
                                _user_nook.Visible = false;
                                _nameuse_acc.Visible = true;
                                _day_accept.Visible = true;
                                _time_accept.Visible = true;
                                _comment1.Visible = false;
                            }
                            else if (ViewState["btnoof"].ToString() == "0")
                            {
                                _user_okok.Visible = false;
                                _user_nook.Visible = false;
                                _text_userhelp.Visible = true;
                                _comment1.Visible = false;
                                if (ViewState["Sec_idx"].ToString() != _missend.Text && ViewState["_rdept_idx"].ToString() == "20")
                                {
                                    _Edit.Visible = false;
                                    _delettitle.Visible = false;
                                    _editsuccess.Visible = false;
                                  //  _textalert_antoher.Visible = true;
                                }
                            }
                            else if (ViewState["btnoof"].ToString() == "1") //ถ้าสถานะของหัวข้อรอให้ USER เซ็นต์รับงาน และให้คนในแผนกเดียวกันเซ็นต์รับงานแทนได้
                            {
                                _user_okok.Visible = true;
                                _user_nook.Visible = true;
                                // _textalert.Visible = true;
                            }
                        }
                        //if (ViewState["EMP_Creator"].ToString() == ViewState["EMP_HUMAN"].ToString())
                        //{

                        //    //test_l.Text = ViewState["EMP_HUMAN"].ToString();
                        //    //tangura.Text = ViewState["EMP_Creator"].ToString();
                        //    _Edit.Visible = false;
                        //    _delettitle.Visible = false;
                        //    _editsuccess.Visible = false;



                        //}

                    }

                }
                break;
            case "gv_allwork":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton _lb_speed_lookmis = (LinkButton)e.Row.Cells[5].FindControl("lb_speed_lookmis");
                    LinkButton _lb_speed_lestho = (LinkButton)e.Row.Cells[5].FindControl("lb_speed_lestho");
                   // LinkButton _lb_speed_addafter = (LinkButton)e.Row.Cells[5].FindControl("lb_speed_addafter");
                    LinkButton _lb_speed_sentaccept = (LinkButton)e.Row.Cells[5].FindControl("lb_speed_sentaccept");
                    LinkButton _lb_speed_sentrup = (LinkButton)e.Row.Cells[5].FindControl("lb_speed_sentrup");
                    LinkButton _lb_speed_edit1 = (LinkButton)e.Row.Cells[5].FindControl("lb_speed_edit1");
                    LinkButton _lb_speed_detail = (LinkButton)e.Row.Cells[5].FindControl("lb_speed_detail");
                    Label _u0_main = (Label)e.Row.Cells[0].FindControl("u0_main");
                    Label resc_all = (Label)e.Row.Cells[0].FindControl("lbl_rsec_checkpermission_all");

                    Label gv_all_sec = (Label)e.Row.Cells[0].FindControl("gv_all_sec");
                    Label _rdep_num_look = (Label)e.Row.Cells[1].FindControl("rdep_num_look");
                    Label _node_to_m0_db = (Label)e.Row.Cells[0].FindControl("node_to_m0_db");
                    Label _actor_to_m0_db = (Label)e.Row.Cells[0].FindControl("actor_to_m0_db");
                    Label day_start_job_all = (Label)e.Row.Cells[0].FindControl("day_start_job_all");
                    Label day_end_job_all = (Label)e.Row.Cells[0].FindControl("day_end_job_all");

                    var satatus = ((Label)e.Row.FindControl("_idstate_all"));
                    var showstatus = ((Label)e.Row.FindControl("showstatus"));
                    var _node_all = ((Label)e.Row.FindControl("_node_all"));
                    

                    switch (_node_all.Text)
                    {
                        case "1":
                        case "2":
                        case "3":
                        case "4":
                            day_start_job_all.Text = "-";
                            day_end_job_all.Text = "-";
                            break;
                    }



                    if (satatus.Text == "1")
                    {
                        showstatus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#FF8C1A");
                        showstatus.Style["font-weight"] = "bold";

                    }
                    else if (satatus.Text == "3" || satatus.Text == "7")
                    {
                        showstatus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#FF0000");
                        showstatus.Style["font-weight"] = "bold";
                    }
                    else if (satatus.Text == "10")
                    {
                        showstatus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#7c0909");
                        showstatus.Style["font-weight"] = "bold";
                    }
                    else if (satatus.Text == "2" || satatus.Text == "6" || satatus.Text == "8" || satatus.Text == "4")
                    {
                        showstatus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC66");
                        showstatus.Style["font-weight"] = "bold";

                    }
                    else if (satatus.Text == "5")
                    {
                        showstatus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC66");//#0040ff
                        showstatus.Style["font-weight"] = "bold";
                    }





                    if (ViewState["_rdept_idx"].ToString() == "20")   // ถ้าเป็ฯคนในฝ่าย MIS
                    {
                        if (int.Parse(ViewState["_jobgrade_level"].ToString()) < int.Parse(ViewState["_jobgrade_level_misHead"].ToString())) //มีตำเเหน่ง น้องชยกว่า 6
                        {
                            if (_u0_main.Text == "4" || _u0_main.Text == "8" || _u0_main.Text == "10" || _u0_main.Text == "5" || _u0_main.Text == "11" || _u0_main.Text == "12")
                            {
                                _lb_speed_lookmis.Visible = true;
                            }
                            //if (_u0_main.Text == "4")
                            //{
                            //    _lb_speed_lookmis.Visible = false;
                            //}
                            //  else if (_node_to_m0_db.Text == "1" && (_rdep_num_look.Text == ViewState["_rdept_idx"].ToString()) && (ViewState["Sec_idx"].ToString() == resc_all.Text))
                            //  {
                            ////      _lb_speed_addafter.Visible = true ;
                            //  }


                            else if (_node_to_m0_db.Text == "4" && ViewState["Sec_idx"].ToString() == resc_all.Text)
                            {
                                _lb_speed_sentaccept.Visible = true;
                            }
                        }
                        else if (int.Parse(ViewState["_jobgrade_level"].ToString()) >= int.Parse(ViewState["_jobgrade_level_misHead"].ToString()))
                        {
                            if (_actor_to_m0_db.Text == "3" || _node_to_m0_db.Text == "3")
                            {
                                _lb_speed_detail.Visible = true;
                            }
                            else if (_u0_main.Text == "4" || _u0_main.Text == "8" || _u0_main.Text == "10" || _u0_main.Text == "5" || _u0_main.Text == "11" || _u0_main.Text == "12")
                            {
                                //if(gv_all_sec.Text == ViewState["_rsec_mis"].ToString())
                                //{ 
                                _lb_speed_lookmis.Visible = true;
                                //}
                            }
                            //else if (_u0_main.Text == "4")
                            //{
                            //    _lb_speed_lookmis.Visible = false;
                            //}
                            else if (_node_to_m0_db.Text == "2" || _actor_to_m0_db.Text == "2")
                            {

                                if (ViewState["Sec_idx"].ToString() == resc_all.Text)
                                {

                                    //tangura.Text = resc_all.Text;
                                    //test_l.Text = ViewState["_rdept_idx"].ToString();
                                    _lb_speed_lestho.Visible = true;

                                }

                            }
                            else if (_node_to_m0_db.Text == "4" && ViewState["Sec_idx"].ToString() == resc_all.Text)
                            {
                                _lb_speed_sentaccept.Visible = true;
                            }
                        }
                    }
                   
                }
               
                break;
            ////////

            case "jogrid":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (jogrid.EditIndex != e.Row.RowIndex) //to overlook header row
                    {
                        // Panel grid1 = (Panel)View1.FindControl("grid");
                        Label req = ((Label)e.Row.FindControl("req"));
                        Label req1 = ((Label)e.Row.FindControl("req1"));
                        var nod = ((Label)e.Row.FindControl("_nodem0"));
                        var ac = ((Label)e.Row.FindControl("_actorm0"));
                        var st = ((Label)e.Row.FindControl("_idstate"));
                        var shac = ((Label)e.Row.FindControl("showact"));
                        var rsec = ((Label)e.Row.FindControl("lbl_rsec_checkpermission"));
                        var jogrid_sec = ((Label)e.Row.FindControl("jogrid_sec"));

                        var _day_start_job = ((Label)e.Row.FindControl("day_start_job"));
                        var _day_end_job = ((Label)e.Row.FindControl("day_end_job"));

                        var sentaccept = ((LinkButton)e.Row.FindControl("sentaccept"));
                        //    var addafter = ((LinkButton)e.Row.FindControl("addafter"));
                        var edit1 = ((LinkButton)e.Row.FindControl("edit1"));
                        var _detailj2_te = (LinkButton)e.Row.FindControl("detailj2_te");
                        var sentrup = ((LinkButton)e.Row.FindControl("sentrup"));
                        var _gohost_speed = (LinkButton)e.Row.FindControl("gohost_speed");
                        //  req.Text="1";
                        //  string a;
                        //  a = HttpUtility.HtmlDecode(reqt);
                        //  ViewState["nodesch"] = nod.Value;
                        // nod.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC00")

                       // rsec.Text = ViewState["rsec"].ToString();

                        
                        switch (nod.Text)
                        {
                            case "1":
                            case "2":
                            case "3":
                            case "4":
                                _day_start_job.Text = "-";
                                _day_end_job.Text = "-";
                                break;
                        }

                        if (st.Text == "1")
                        {
                            shac.ForeColor = System.Drawing.ColorTranslator.FromHtml("#FF8C1A");
                            shac.Style["font-weight"] = "bold";

                        }
                        else if (st.Text == "3" || st.Text == "7")
                        {
                            shac.ForeColor = System.Drawing.ColorTranslator.FromHtml("#FF0000");
                            shac.Style["font-weight"] = "bold";
                        }
                        else if (st.Text == "10")
                        {
                            shac.ForeColor = System.Drawing.ColorTranslator.FromHtml("#7c0909");
                            shac.Style["font-weight"] = "bold";
                        }
                        else if (st.Text == "2" || st.Text == "6" || st.Text == "8" || st.Text == "4")
                        {
                            shac.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC66");
                            shac.Style["font-weight"] = "bold";

                        }
                        else if (st.Text == "5")
                        {
                            shac.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC66");//#0040ff
                            shac.Style["font-weight"] = "bold";
                        }



                        //if (nod.Text == "1" && ViewState["Sec_idx"].ToString() == rsec.Text)
                        //{
                        ////    addafter.Visible = true;
                        //    // edit1.Visible = true;
                        //}
                        ////else
                        ////{
                        ////    addafter.Visible = false;
                        ////}
                        if (nod.Text == "4" && ViewState["Sec_idx"].ToString() == rsec.Text)
                        {

                            sentaccept.Visible = true;
                        }
                        //else
                        //{
                        //    sentaccept.Visible = false;
                        //}
                        if (nod.Text == "6" || nod.Text == "7" || nod.Text == "5" || nod.Text == "8" || nod.Text == "10" || nod.Text == "11" || nod.Text == "12")
                        {
                            sentrup.Visible = true;

                        }
                        //if (st.Text == "4")
                        //{
                        //    sentrup.Visible = false;
                        //}
                        //else
                        //{
                        //    sentrup.Visible = false;
                        //}

                        if (nod.Text == "2" && ac.Text == "2" && int.Parse(ViewState["_jobgrade_level"].ToString()) >= int.Parse(ViewState["_jobgrade_level_misHead"].ToString()))
                        {

                            if (ViewState["Sec_idx"].ToString() == rsec.Text)
                            {

                                //  test_l.Text = ViewState["_rsec_idx"].ToString();
                                _gohost_speed.Visible = true;
                            }

                            //else
                            //{
                            //    _gohost_speed.Visible = false;
                            //}
                        }


                        if (ViewState["_rdept_idx"].ToString() == "20" && int.Parse(ViewState["_jobgrade_level"].ToString()) >= int.Parse(ViewState["_jobgrade_level_misHead"].ToString()))
                        {
                            if (nod.Text == "3")
                            {
                                _detailj2_te.Visible = true;
                            }
                            else
                            {
                                _detailj2_te.Visible = false;
                            }


                        }

                    }



                   
                }

                break;

            case "gvFile":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HyperLink btnDL11 = (HyperLink)e.Row.Cells[0].FindControl("btnDL11");
                    HiddenField hidFile11 = (HiddenField)e.Row.Cells[0].FindControl("hidFile11");
                    // Display the company name in italics.
                    string LinkHost11 = string.Format("http://{0}", Request.Url.Host);

                    btnDL11.NavigateUrl = LinkHost11 + MapURL(hidFile11.Value);
                } 
                break;
            case "Gv_filemisH":
                //Panel userpanel = ((Panel)View4.FindControl("userpanel"));

                //GridView gvFile = ((GridView)userpanel.FindControl("gvFile"));
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HyperLink btnDL11 = (HyperLink)e.Row.Cells[0].FindControl("btnDL11");
                    HiddenField hidFile11 = (HiddenField)e.Row.Cells[0].FindControl("hidFile11");
                    // Display the company name in italics.
                    string LinkHost11 = string.Format("http://{0}", Request.Url.Host);

                    btnDL11.NavigateUrl = LinkHost11 + MapURL(hidFile11.Value);
                }

                break;
            case "Gv_filehead":
                //Panel userpanel = ((Panel)View4.FindControl("userpanel"));

                //GridView gvFile = ((GridView)userpanel.FindControl("gvFile"));
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HyperLink btnDL11 = (HyperLink)e.Row.Cells[0].FindControl("btnDL11");
                    HiddenField hidFile11 = (HiddenField)e.Row.Cells[0].FindControl("hidFile11");
                    // Display the company name in italics.
                    string LinkHost11 = string.Format("http://{0}", Request.Url.Host);

                    btnDL11.NavigateUrl = LinkHost11 + MapURL(hidFile11.Value);
                }

                break;
            case "Gv_filemisshared":
                //Panel userpanel = ((Panel)View4.FindControl("userpanel"));

                //GridView gvFile = ((GridView)userpanel.FindControl("gvFile"));
                if (e.Row.RowType == DataControlRowType.DataRow) 
                {
                    HyperLink btnDL11 = (HyperLink)e.Row.Cells[0].FindControl("btnDL11");
                    HiddenField hidFile11 = (HiddenField)e.Row.Cells[0].FindControl("hidFile11");
                    // Display the company name in italics.
                    string LinkHost11 = string.Format("http://{0}", Request.Url.Host);

                    btnDL11.NavigateUrl = LinkHost11 + MapURL(hidFile11.Value);
                }

                break;
        }

    }

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            //case "Gvmaster":
            //    Gvmaster.EditIndex = e.NewEditIndex;
            //    gethead();
            //    break;

            //case "Gvmastermis":
            //    Gvmastermis.EditIndex = e.NewEditIndex;
            //    getmishead();
               // break;
            case "gvinfv_showtitle":
                gvinfv_showtitle.EditIndex = e.NewEditIndex;

                var dtviewmis = (DataSet)ViewState["showdatanaja"];
                var daviewmis = dtviewmis.Tables[0].NewRow();

                gvinfv_showtitle.DataSource = dtviewmis.Tables[0];
                gvinfv_showtitle.DataBind();
                break;
            case "gvanthoer_old":
                gvanthoer_old.EditIndex = e.NewEditIndex;

                missha();

                break;
                //case "jogrid":
                //    jogrid.EditIndex = e.NewEditIndex;
                //    selectjob();
                //    break;
        }
    }

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            //case "Gvmaster":

            //    var titlego = (TextBox)Gvmaster.Rows[e.RowIndex].FindControl("titleedit");
            //    var requirego = (TextBox)Gvmaster.Rows[e.RowIndex].FindControl("requireedit");
            //    var id_num = (TextBox)Gvmaster.Rows[e.RowIndex].FindControl("num");

            //    Gvmaster.EditIndex = -1;

            //    data_jo data_box111 = new data_jo();
            //    job_order_overview box222 = new job_order_overview();
            //    data_box111.job_order_overview_action = new job_order_overview[1];

            //    box222.u0_jo_idx = int.Parse(id_num.Text);
            //    box222.title_jo = titlego.Text;
            //    box222.require_jo = requirego.Text;

            //    data_box111.job_order_overview_action[0] = box222;
            //    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));
            //    // oak1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(box));

            //    //tostid.Text = _funcTool.convertObjectToJson(data_box111);


            //    data_box111 = callService(_urlUpdateDatajoborder, data_box111);
            //    //uphead();

            //    if (data_box111.return_code == 0)
            //    {
            //        gethead();

            //    }
            //    else
            //    {
            //        // oak1.Text = "3333";
            //        //setError(_data_networkdevices.return_code.ToString() + " - " + _data_networkdevices.return_msg);
            //    }
            //    break;

            case "gvinfv_showtitle":

                GridView gvinfv_showtitle = (GridView)pl_gvdetail.FindControl("gvinfv_showtitle");
                var titleedit = (TextBox)gvinfv_showtitle.Rows[e.RowIndex].FindControl("titleedit");
                var titleedit_late = (TextBox)gvinfv_showtitle.Rows[e.RowIndex].FindControl("titleedit_late");
                var dateedit = (TextBox)gvinfv_showtitle.Rows[e.RowIndex].FindControl("dateedit");
                var id_looo = (TextBox)gvinfv_showtitle.Rows[e.RowIndex].FindControl("id_looo");

              
                
                string[] datetimesetedit = new string[3];
                string[] datetimeset_hereedit = new string[3];
                string[] datetimesetedit_s = new string[3];
                

                data_jo year_most1 = new data_jo();
                job_order_overview year_log1 = new job_order_overview();
                year_most1.job_order_overview_action = new job_order_overview[1];

                year_log1.u0_jo_idx = int.Parse(ViewState["u0_jo_idx"].ToString());

                year_most1.job_order_overview_action[0] = year_log1;
                //oaktest.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(year_most));
                year_most1 = callService(_urlSelectMIS_share, year_most1);

                //ViewState["_year_most"] = year_most.job_order_overview_action[0].year_check.ToString();
                ViewState["_year_finsedit"] = year_most1.job_order_overview_action[0].day_finish.ToString();
                ViewState["_year_startedit_s"] = year_most1.job_order_overview_action[0].day_start.ToString();

                ////////////////////    เป็นส่วน ตัด ข้อมูลให้เหลือแต่ คศ

                // test_l.Text = ViewState["_year_startedit_s"].ToString();

                //datetimesetedit = ViewState["_year_finsedit"].ToString().Split('/');
                //string day_finishedit = datetimesetedit[0];
                //string month_finishedit = datetimesetedit[1];
                //string year_finishedit = datetimesetedit[2];

                datetimesetedit_s = ViewState["_year_startedit_s"].ToString().Split('/');
                string day_starts = datetimesetedit_s[0];
                string month_starts = datetimesetedit_s[1];
                string year_starts = datetimesetedit_s[2];



                datetimeset_hereedit = dateedit.Text.Split('/');
                string day_hereedit = datetimeset_hereedit[0];
                string month_hereedit = datetimeset_hereedit[1];
                string year_hereedit = datetimeset_hereedit[2];

                //string date_Sedit = ViewState["_year_startedit_s"].ToString();
                //DateTime newDate_sedit = DateTime.ParseExact(date_Sedit, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                // test_l.Text = newDate_sedit.ToString();

                //string date_Nedit = dateedit.Text;
                //DateTime newDate_nedit = DateTime.ParseExact(date_Sedit, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                // test_l.Text = newDate_nedit.ToString();

                ViewState["ok_upsideedit"] = "0";

                //if (newDate_nedit >= newDate_sedit)
                //{
                //    test_l.Text = newDate_nedit.ToString();
                //    tangura.Text = newDate_sedit.ToString();
                //    ViewState["ok_upsideedit"] = "1";
                //}
                //else if(newDate_nedit < newDate_sedit)
                //{

                //    ViewState["ok_upsideedit"] = "0";

                //}

                if (int.Parse(year_hereedit) > int.Parse(year_starts))
                {

                    // test_l.Text = "111";
                    ViewState["ok_upsideedit"] = "1";
                }
                else if (int.Parse(year_hereedit) == int.Parse(year_starts))
                {
                    if (int.Parse(month_hereedit) > int.Parse(month_starts))
                    {
                        //if (int.Parse(day_shere) >= int.Parse(day_start))
                        //{
                        // test_l.Text = "222";
                        ViewState["ok_upsideedit"] = "1";
                        //}
                    }
                    else if (int.Parse(month_hereedit) == int.Parse(month_starts))
                    {
                        if (int.Parse(day_hereedit) >= int.Parse(day_starts))
                        {
                            //test_l.Text = "555";
                            ViewState["ok_upsideedit"] = "1";
                        }
                        else
                        {
                            // test_l.Text = "333";
                            ViewState["ok_upsideedit"] = "0";
                        }
                    }
                    else
                    {
                        //test_l.Text = "666";
                        ViewState["ok_upsideedit"] = "0";
                    }
                }
                else
                {
                    //test_l.Text = "444";
                    ViewState["ok_upsideedit"] = "0";
                }


                if (ViewState["timething"].ToString() == "1")
                {
                    ViewState["ok_upsideedit"] = "1";
                }

                if (ViewState["ok_upsideedit"].ToString() == "1")
                {

                    var dtviewmis = (DataSet)ViewState["showdatanaja"];
                    var daviewmis = dtviewmis.Tables[0].Rows;

                    int numrow = dtviewmis.Tables[0].Rows.Count; // จำนวนแถว

                    ViewState["id_looo"] = id_looo.Text;
                   // ViewState["dateedit"] = dateedit.Text;


                    ViewState["title_re"] = titleedit.Text;
                    int a = 0;
                    if (numrow > 0)
                    {
                        foreach (DataRow check in dtviewmis.Tables[0].Rows)
                        {

                            ViewState["check_title"] = check["title_1"];
                            if (ViewState["title_re"].ToString() == ViewState["check_title"].ToString())
                            {
                                ViewState["CheckDataset"] = "0";
                                if (e.RowIndex == a)
                                {
                                    ViewState["CheckDataset"] = "1";
                                }
                                break;
                            }
                            else
                            {
                                ViewState["CheckDataset"] = "1";
                            }
                            a++;
                        }
                        if (ViewState["CheckDataset"].ToString() == "1")
                        {
                            daviewmis[e.RowIndex]["title_1"] = titleedit.Text;
                            daviewmis[e.RowIndex]["timeline"] = dateedit.Text;
                            daviewmis[e.RowIndex]["u0_jo_idx"] = id_looo.Text;

                            gvinfv_showtitle.EditIndex = -1;
                            pl_gvdetail.Visible = true;

                            gvinfv_showtitle.DataSource = dtviewmis.Tables[0];
                            gvinfv_showtitle.DataBind();
                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลอยู่แล้ว');", true);
                        }
                    }

                    //var titleview = (TextBox)gvinfv_showtitle.FindControl("u1_title");
                    //var day_sentre = (TextBox)gvinfv_showtitle.FindControl("u1_day_sent");

                    //data_jo lastbox = new data_jo();
                    //jo_add_datalist oldbox = new jo_add_datalist();
                    //lastbox.jo_add_datalist_action = new jo_add_datalist[1];

                    //oldbox.u1_title = titleview.Text;
                    //oldbox.u1_day_sent = day_sentre.Text;
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('เวลาที่ส่งมอบงาน น้อยกว่าวันที่เริ่มงาน');", true);
                   // ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('เวลาที่ส่งมอบงาน เกินระยะเวลาสิ้นสุดของงาน');", true);
                }
                gvinfv_showtitle.Focus();
                break;
            case "gvanthoer_old":

                Panel Panelbig = (Panel)viewmisshare.FindControl("pl_gvold");
                GridView Gvbigmak = (GridView)Panelbig.FindControl("gvanthoer_old");
                var id_u1_idx = (TextBox)Gvbigmak.Rows[e.RowIndex].FindControl("wow");
                var u0_id_keep = (TextBox)Gvbigmak.Rows[e.RowIndex].FindControl("wow_id_u0");
                var titleedit1 = (TextBox)Gvbigmak.Rows[e.RowIndex].FindControl("titleedit1");
                var dateedit2 = (TextBox)Gvbigmak.Rows[e.RowIndex].FindControl("dateedit2");
                var _CopyTitle = (TextBox)Gvbigmak.Rows[e.RowIndex].FindControl("CopyTitle");

                string[] datetimesetedit_old = new string[3];
                string[] datetimeset_hereedit_old = new string[3];
                string[] datetimesetanthoer_old = new string[3];

                data_jo year_most_old = new data_jo();
                job_order_overview year_log_old = new job_order_overview();
                year_most_old.job_order_overview_action = new job_order_overview[1];

                year_log_old.u0_jo_idx = int.Parse(ViewState["u0_jo_idx"].ToString());

                year_most_old.job_order_overview_action[0] = year_log_old;
                //oaktest.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(year_most));
                year_most_old = callService(_urlSelectMIS_share, year_most_old);

                //ViewState["_year_most"] = year_most.job_order_overview_action[0].year_check.ToString();
                ViewState["_year_finsedit_old"] = year_most_old.job_order_overview_action[0].day_finish.ToString();
                ViewState["_year_startedit_old"] = year_most_old.job_order_overview_action[0].day_start.ToString();
                //////////////////////    เป็นส่วน ตัด ข้อมูลให้เหลือแต่ คศ

                string date_E = ViewState["_year_startedit_old"].ToString();

                DateTime newdate_SGv  = DateTime.ParseExact(date_E, "dd/MM/yyyy",CultureInfo.InstalledUICulture); 
        
                string date_se = dateedit2.Text;
                DateTime newdate_selon = DateTime.ParseExact(date_se, "dd/MM/yyyy", CultureInfo.InstalledUICulture);
            //    test_l.Text = newdate_selon.ToString();

                ViewState["ok_upsideedit_old"] = "0";

                if (newdate_selon >= newdate_SGv)
                {

                    ViewState["ok_upsideedit_old"] = "1";

                }
                else
                {
                    ViewState["ok_upsideedit_old"] = "0";
                }

                if (ViewState["ok_upsideedit_old"].ToString() == "1")
                {
                    gvanthoer_old.EditIndex = -1;

                    data_jo bossbox = new data_jo();
                    jo_add_datalist slavebox = new jo_add_datalist();
                    bossbox.jo_add_datalist_action = new jo_add_datalist[1];

                    slavebox.u1_ov_useonly = int.Parse(id_u1_idx.Text);
                    slavebox.u1_title = titleedit1.Text;
                    slavebox.u1_day_sent = dateedit2.Text;
                    slavebox.u1_emp_sent_single = int.Parse(ViewState["EMP_HUMAN"].ToString());

                    bossbox.jo_add_datalist_action[0] = slavebox;
                    //ngi.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(bossbox));
                    bossbox = callService(_urlUpdatestau1_ov, bossbox);

                    data_jo B_gv_old = new data_jo();
                    jo_log S_gv_old = new jo_log();
                    B_gv_old.jo_log_action = new jo_log[1];

                    S_gv_old.oakja = int.Parse(u0_id_keep.Text);
                    S_gv_old.m0_actor_idx_lo = 4;
                    S_gv_old.m0_node_idx_lo = 5;
                    S_gv_old.id_statework_lo = 11;
                    S_gv_old.requried_old = _CopyTitle.Text;
                    S_gv_old.from_table_l0 = "u1";
                    S_gv_old.idx_create_lo = int.Parse(ViewState["EMP_HUMAN"].ToString());

                    B_gv_old.jo_log_action[0] = S_gv_old;
                    //ngi.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(B_gv_old));
                    B_gv_old = callService(_urlInsertloggaritum, B_gv_old);
                    if (bossbox.return_code == 0)
                    {
                        missha();
                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('วันที่ส่งมอบงาน น้อยกว่าวันที่เริ่มงาน');", true);
                   // ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('เวลาที่ส่งมอบงาน เกินระยะเวลาสิ้นสุดของงาน');", true);
                }

                gvanthoer_old.Focus();
                break;
            case "jogrid":

                int u0_jo_idx_update = Convert.ToInt32(jogrid.DataKeys[e.RowIndex].Values[0].ToString());
                var titleUpdate = (TextBox)jogrid.Rows[e.RowIndex].FindControl("titledit");
                var reqUpdate = (TextBox)jogrid.Rows[e.RowIndex].FindControl("reqedit");
                jogrid.EditIndex = -1;
                ViewState["u0_jo_idx_update"] = u0_jo_idx_update;
                ViewState["titleUpdate"] = titleUpdate.Text;
                ViewState["reqUpdate"] = reqUpdate.Text;
                //updatejob();
                selectjob();

                break;
        }
    }

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "Gvmaster":
                Gvmaster.EditIndex = -1;
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "Gvmastermis":
                Gvmastermis.EditIndex = -1;
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "gvinfv_showtitle":
                gvinfv_showtitle.EditIndex = -1;
                //multimaster.SetActiveView(viewmisshare);
                gvinfv_showtitle.DataSource = ViewState["showdatanaja"];
                gvinfv_showtitle.DataBind();
                gvinfv_showtitle.Focus();
                break;
            case "gvanthoer_old":
                gvanthoer_old.EditIndex = -1;
                multimaster.SetActiveView(viewmisshare);
                missha();
                gvanthoer_old.Focus();
                break;
            case "jogrid":
                jogrid.EditIndex = -1;
                //Page.Response.Redirect(Page.Request.Url.ToString(), true);
                // remoteuser.Visible = true;
                selectjob();
                // grid.Visible = true;
                break;

        }
    }

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            //case "Gvmaster":
            //    Gvmaster.PageIndex = e.NewPageIndex;
            //    Gvmaster.DataBind();
            //    gethead();
            //    break;

            //case "Gvmastermis":
            //    Gvmastermis.PageIndex = e.NewPageIndex;
            //    Gvmastermis.DataBind();
            //    getmishead();
            //    break;
            case "gvinfv_showtitle":
                gvinfv_showtitle.PageIndex = e.NewPageIndex;
                gvinfv_showtitle.DataSource = ViewState["showdatanaja"];
                gvinfv_showtitle.DataBind();
                break;

            case "gv_allwork":
                setGridData(gv_allwork, ViewState["select_allwork"]);
                gv_allwork.PageIndex = e.NewPageIndex;
                gv_allwork.DataBind();
               // pageall_work();
                SETFOCUS_ONTOP.Focus();
                break;


            case "jogrid":

                setGridData(jogrid, ViewState["select_deptwork"]);
                jogrid.PageIndex = e.NewPageIndex;
                jogrid.DataBind();

               // selectjob();
                SETFOCUS_ONTOP.Focus();
                break;
            //case "jogrid2":
            //    jogrid2.PageIndex = e.NewPageIndex;
            //    jogrid2.DataBind();
            //    // jogrid2.Refresh();
            //    selectmheadjob();
                //break;

            case "u1grid":
                u1grid.PageIndex = e.NewPageIndex;
                u1grid.DataBind();
                selecttableu1();
                // selectjob();
                break;
        }
    }

    protected void Master_RowDelete(object sender, GridViewDeleteEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "gvinfv_showtitle":

                GridView gvinfv_showtitle = (GridView)pl_gvdetail.FindControl("gvinfv_showtitle");
                var deletrowone = (DataSet)ViewState["showdatanaja"];
                var controldelet = deletrowone.Tables[0].Rows;

                controldelet.RemoveAt(e.RowIndex);

              
               
                gvinfv_showtitle.EditIndex = -1;

                ViewState["showdatanaja"] = deletrowone;
                gvinfv_showtitle.DataSource = ViewState["showdatanaja"];
                gvinfv_showtitle.DataBind();

                var dtviewmis1 = (DataSet)ViewState["showdatanaja"];
                var daviewmis1 = dtviewmis1.Tables[0].NewRow();
                int rowcount = dtviewmis1.Tables[0].Rows.Count;
                
                //data_jo B_select_check = new data_jo();
                //job_order_overview S_select_check = new job_order_overview();
                //B_select_check.job_order_overview_action = new job_order_overview[1];

                //S_select_check.u0_jo_idx = int.Parse(main_id_to_sql.Text);

                //B_select_check.job_order_overview_action[0] = S_select_check;
                //B_select_check = callService(_urlSelectDatajoborder_headlook,B_select_check);
                //ViewState["dont_last"] = B_select_check.job_order_overview_action[0].id_statework.ToString();
                if (rowcount == 0)
                {
                    gvinfv_showtitle.Visible = false;
                    saveall.Visible = false;
                    save_viewdep.Visible = false;
                    nannasi.Visible = false;
                    rdbt_end.Visible = false;
                    if (ViewState["num_last"].ToString() == "8" || ViewState["num_last"].ToString() == "10")
                    {
                        nannasi.Visible = true;
                    }

                }
                else if (rowcount > 0)
                {
                    //saveall.Visible = true;
                    nannasi.Visible = true;
                }
                break;
        }
    }
    #endregion

    #region SetGrid
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region set DDL
    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }
    #endregion

    #region dropdown list

    protected void getOrganizationList(DropDownList ddlName)
    {
        _dataEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dataEmployee.organization_list[0] = _orgList;

        _dataEmployee = callServicePostEmployee(_urlGetOrganizationList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกบริษัท ---", "0"));
    }

    protected void getDepartmentList(DropDownList ddlName, int _org_idx)
    {
        data_employee _dataEmployee_rdept = new data_employee();
        _dataEmployee_rdept.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dataEmployee_rdept.department_list[0] = _deptList;

        _dataEmployee_rdept = callServicePostEmployee(_urlGetDepartmentList, _dataEmployee_rdept);
        setDdlData(ddlName, _dataEmployee_rdept.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกฝ่าย ---", "0"));
    }

    protected void getSectionList(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {
        ddlName.Items.Clear();
        _dataEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _dataEmployee.section_list[0] = _secList;

        _dataEmployee = callServicePostEmployee(_urlGetSectionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.section_list, "sec_name_th", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "0"));
    }

    protected void getSectionList_dept(DropDownList ddlName,int _org_idx, int _rdept_idx)
    {
        ddlName.Items.Clear();
        _dataEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _dataEmployee.section_list[0] = _secList;

        _dataEmployee = callServicePostEmployee(_urlGetSectionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.section_list, "sec_name_th", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "0"));
    }
    protected void getSectionList_secmis(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {
        //_org_idx = 1;
        //_rdept_idx = 20;
        ddlName.Items.Clear();
        _dataEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _dataEmployee.section_list[0] = _secList;

        ViewState["_org_idsec"] = _org_idx;
        ViewState["_rdept_idsec"] = _rdept_idx;
        _dataEmployee = callServicePostEmployee(_urlGetSectionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.section_list, "sec_name_th", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "0"));
    }
    #endregion

    #region Method

    protected void inputshow_table()
    {
        var boxshow = new DataSet();
        boxshow.Tables.Add("listtitle");

        boxshow.Tables[0].Columns.Add("title_1", typeof(String));
        boxshow.Tables[0].Columns.Add("timeline", typeof(String));

        if (ViewState["alllist"] == null)
        {
            ViewState["alllist"] = boxshow;
        }
    }

    //protected void gethead()
    //{
    //    data_jo box = new data_jo();
    //    job_order_overview box1 = new job_order_overview();
    //    box.job_order_overview_action = new job_order_overview[1];
    //    box.job_order_overview_action[0] = box1;

    //    box1.emp_dep_job = int.Parse(ViewState["_rdept_idx"].ToString());
    //    box1.emp_org_job = int.Parse(ViewState["_org_idx"].ToString());

    //    //oaklnw.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(box));ViewState["_org_idx"]
    //    box = callService(_urlSelectDatajoborder_listheadsee, box);
    //    setGridData(Gvmaster, box.job_order_overview_action);

    //}

    //protected void getmishead()
    //{

    //    data_jo box = new data_jo();
    //    job_order_overview box1 = new job_order_overview();
    //    box.job_order_overview_action = new job_order_overview[1];
    //    box.job_order_overview_action[0] = box1;
    //    // oak1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(box));


    //    box = callService(_urlSelectDatajoborder_mishen, box);
    //    //ลอง เอาค่ามาตรงนี้ come to check this value for if { }

    //    setGridData(Gvmastermis, box.job_order_overview_action);
    //    //ViewState["status"] = box.job_order_overview_action[0].id_statework.ToString();
    //    //if (ViewState["status"].ToString() != "9")
    //    //{

    //    //}

    //    //dog2.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(box));
    //}

    protected void missha()
    {
        data_jo bigbox = new data_jo();
        jo_add_datalist smallbox = new jo_add_datalist();
        bigbox.jo_add_datalist_action = new jo_add_datalist[1];

        smallbox.wait = int.Parse(Label2.Text);

        bigbox.jo_add_datalist_action[0] = smallbox;
        // dog1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(bigbox));
        bigbox = callService(_urlSelectU1ov, bigbox);

        ViewState["select_dataU1"] = bigbox.jo_add_datalist_action;

        setGridData(gvanthoer_old, ViewState["select_dataU1"]);

    }

    protected void pageall_work()
    {
        data_jo bigbox = new data_jo();
        job_order_overview smallbox = new job_order_overview();
        bigbox.job_order_overview_action = new job_order_overview[1];
        
      //  smallbox.rsec_creator =

        bigbox.job_order_overview_action[0] = smallbox;

        // dog1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(bigbox));


        bigbox = callService(_urlSelectanywork, bigbox);
        ViewState["select_allwork"] = bigbox.job_order_overview_action;

        setGridData(gv_allwork, ViewState["select_allwork"]);
    }

    #endregion met

    #region method pleach

    protected void selectjob()
    {
    
        data_jo box = new data_jo();
        job_order_overview box1 = new job_order_overview();
        box.job_order_overview_action = new job_order_overview[1];
        box.job_order_overview_action[0] = box1;
        //box1.emp_id_creator = _emp_idx;
        box1.emp_dep_job = int.Parse(ViewState["_rdept_idx"].ToString());
        //box1.costcenter_no = ViewState["costcenter_no"].ToString();
        //    cost_emprequireDe.Text = ViewState["costcenter_no"].ToString();
    
        box = callService(_urlSelectDatajoborder, box);
        //   ViewState["bind_data"] = box.job_order_overview_action;
        ViewState["select_deptwork"] = box.job_order_overview_action;
        setGridData(jogrid, ViewState["select_deptwork"]);
        
        //setFormViewData(DetailUserpv, FormViewMode.ReadOnly, box.job_order_overview_action);
    }

    protected void selecttableu1()
    {
        data_jo boxy = new data_jo();
        jo_add_datalist boxl = new jo_add_datalist();
        boxy.jo_add_datalist_action = new jo_add_datalist[1];
        boxl.wait = int.Parse(ViewState["idx"].ToString());
        //  boxl.wait  = 12;
        boxy.jo_add_datalist_action[0] = boxl;
        //    p1.Text = ViewState["idx"].ToString();
        boxy = callService(_urlSelectU1ov, boxy);
        // p1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(boxy));
        setGridData(u1grid, boxy.jo_add_datalist_action);
    }

    //protected void selectmheadjob()
    //{
    //    data_jo box12 = new data_jo();
    //    job_order_overview box22 = new job_order_overview();
    //    box12.job_order_overview_action = new job_order_overview[1];
    //    box12.job_order_overview_action[0] = box22;
    //    //p1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(box));
    //    box12 = callService(_urlSelect_mishead, box12);
    //    setGridData(jogrid2, box12.job_order_overview_action);
    //}

    protected void acceptnode()
    {
        Panel Panel11 = ((Panel)View3.FindControl("Panel1"));
        FormView DetailUser1 = ((FormView)Panel11.FindControl("DetailUser"));
        Label sec_misH = ((Label)DetailUser1.FindControl("sec_misH"));

        TextBox comment_misHead = ((TextBox)View3.FindControl("txt_comment_misH"));

        data_jo bb1 = new data_jo();
        job_order_overview bb2 = new job_order_overview();
        bb1.job_order_overview_action = new job_order_overview[1];
        bb2.u0_jo_idx = int.Parse(ViewState["id_idx"].ToString()); //box2.ตัวแปรฝั่ง sql
        bb2.m0_node_idx = int.Parse(ViewState["node"].ToString());
        bb2.m0_actor_idx = int.Parse(ViewState["actor"].ToString());
        bb2.sec_mis = int.Parse(sec_misH.Text);
        bb2.id_statework = 2;
        bb2.emp_id_creator = _emp_idx;
        bb1.job_order_overview_action[0] = bb2;

        if (comment_misHead.Text== "")
        {
            bb2.comment = "-";
        }
        else
        {
            bb2.comment = comment_misHead.Text;
        }


       // test_l.Text = sec_misH.Text;



        bb1 = callService(_urlUpdatestau0, bb1);

    }

    //protected void insertjob()
    //{
    //    var addtitle = (TextBox)remoteuser.FindControl("titlename");
    //    data_jo Badd_title = new data_jo();
    //    job_order_overview Sadd_title = new job_order_overview();
    //    Badd_title.job_order_overview_action = new job_order_overview[1];
    //    // box1.u0_jo_idx = int.Parse(ViewState["U0_idx"].ToString());
    //    Sadd_title.title_jo = titlename.Text;
    //    Sadd_title.require_jo = require.Text;
    //    Sadd_title.emp_id_creator = int.Parse(ViewState["EMP_HUMAN"].ToString());
    //    // box1.no_invoice = ViewState["IDfile"].ToString();
    //    //ส่วนของ User //บอกจุดเริ่มต้นของ node
    //    Sadd_title.m0_actor_idx = 1; //from_ac
    //    Sadd_title.m0_node_idx = 1; // from_M0_node
    //    Sadd_title.id_statework = 1;// node_decision
    //    Sadd_title.from_table = "u0";

    //    Badd_title.job_order_overview_action[0] = Sadd_title;

    //    //   p1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(box));
    //    //string localJson1 = _funcTool.convertObjectToJson(box);
    //    // p1.Text = localJson1;

    //    Badd_title = callService(_urlInsertDatajoborder, Badd_title);

    //    //test_l.Text = box.job_order_overview_action[0].no_invoice.ToString();
    //    //HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(box));

    //    if (Badd_title.job_order_overview_action[0].no_invoice.ToString() != null)
    //    {
    //        ViewState["_IDfile"] = Badd_title.job_order_overview_action[0].no_invoice.ToString();

    //        if (UploadFileComment.HasFile)
    //        {

    //            string getPathfile = ConfigurationManager.AppSettings["pathfile_Job_order"];
    //            string id_uplodefile = ViewState["_IDfile"].ToString();//_returndocumentcode;
    //            string fileName_upload = id_uplodefile;
    //            string filePath_upload = Server.MapPath(getPathfile + id_uplodefile);

    //            if (!Directory.Exists(filePath_upload))
    //            {
    //                Directory.CreateDirectory(filePath_upload);
    //            }
    //            string extension = Path.GetExtension(UploadFileComment.FileName);

    //            UploadFileComment.SaveAs(Server.MapPath(getPathfile + id_uplodefile) + "\\" + fileName_upload + extension);
    //        }
    //    }

    //    // ViewState["_IDfile"] = null;
    //    //ViewState.Clear();
    //    //ClearChildViewState();

    //}

    protected void insertlog()
    { //insert log node กรณีกดอนุมัติ
        FormView DetailUser = ((FormView)View3.FindControl("DetailUser"));
        Label reqcode = ((Label)DetailUser.FindControl("reqcodetest"));
       
        data_jo bbox = new data_jo();
        jo_log sbox = new jo_log();
        // job_order_overview sbox = new job_order_overview();
        // bbox.job_order_overview_action = new job_order_overview[1];
        bbox.jo_log_action = new jo_log[1];
        // ViewState["log_oak"] = loggy;
        sbox.oakja = int.Parse(ViewState["id_idx"].ToString());
        // sbox.oakja = int.Parse(loggy.Text);

        sbox.m0_node_idx_lo = 3;
        sbox.m0_actor_idx_lo = 3;
        sbox.id_statework_lo = 2;
        sbox.from_table_l0 = "u0";
        sbox.idx_create_lo = int.Parse(ViewState["EMP_HUMAN"].ToString());
        ViewState["reqjob"] = reqcode.Text;
        //  p1.Text = ViewState["reqjob"].ToString();
        sbox.requried_old = ViewState["reqjob"].ToString();
        bbox.jo_log_action[0] = sbox;
        // p1.Text = loggy.Text;
        // p1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(bbox));
        bbox = callService(_urlInsertloggaritum, bbox);
        //   p1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(bbox));
        //   setFormViewData(logidform, FormViewMode.ReadOnly, bbox.jo_log_action);
    }

    protected void insertlog2()
    { //insert log node กรณี user กดยอมรับ
        FormView DetailUsersent = ((FormView)View1.FindControl("DetailUsersent"));
        Label reqtest = ((Label)DetailUsersent.FindControl("reqtest"));
        data_jo bbox = new data_jo();
        jo_log sbox = new jo_log();
        // job_order_overview sbox = new job_order_overview();
        // bbox.job_order_overview_action = new job_order_overview[1];
        bbox.jo_log_action = new jo_log[1];
        // ViewState["log_oak"] = loggy;
        sbox.oakja = int.Parse(ViewState["id_idx"].ToString());
        // sbox.oakja = int.Parse(loggy.Text);

        sbox.m0_node_idx_lo = 4;
        sbox.m0_actor_idx_lo = 1;
        sbox.id_statework_lo = 5;
        sbox.from_table_l0 = "u0";
        sbox.idx_create_lo = int.Parse(ViewState["EMP_HUMAN"].ToString());
        ViewState["reqjob1"] = reqtest.Text;
        //  p1.Text =  ViewState["reqjob1"].ToString();
        sbox.requried_old = ViewState["reqjob1"].ToString();
        bbox.jo_log_action[0] = sbox;
        // p1.Text = loggy.Text;
        // p1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(bbox));
        bbox = callService(_urlInsertloggaritum, bbox);
        //   setFormViewData(logidform, FormViewMode.ReadOnly, bbox.jo_log_action);
    }

    protected void insertlog3()
    { //insert log node กรณีกดไม่อนุมัติ
        FormView DetailUser = ((FormView)View3.FindControl("DetailUser"));
        Label reqcode = ((Label)DetailUser.FindControl("reqcodetest"));
        data_jo bbox = new data_jo();
        jo_log sbox = new jo_log();
        bbox.jo_log_action = new jo_log[1];
        sbox.oakja = int.Parse(ViewState["id_idx"].ToString());
        sbox.m0_node_idx_lo = 3;
        sbox.m0_actor_idx_lo = 3;
        sbox.id_statework_lo = 3;
        sbox.from_table_l0 = "u0";
        sbox.idx_create_lo = int.Parse(ViewState["EMP_HUMAN"].ToString());
        ViewState["reqjob1"] = reqcode.Text;
        sbox.requried_old = ViewState["reqjob1"].ToString();
        bbox.jo_log_action[0] = sbox;
        bbox = callService(_urlInsertloggaritum, bbox);

    }

    protected void insertlog4()
    { //insert log node กรณีไม่กดยอมรับ
        FormView DetailUsersent = ((FormView)View1.FindControl("DetailUsersent"));
        Label reqtest = ((Label)DetailUsersent.FindControl("reqtest"));
        data_jo bbox = new data_jo();
        jo_log sbox = new jo_log();
        bbox.jo_log_action = new jo_log[1];
        sbox.oakja = int.Parse(ViewState["id_idx"].ToString());
        sbox.m0_node_idx_lo = 4;
        sbox.m0_actor_idx_lo = 1;
        sbox.id_statework_lo = 3;
        sbox.from_table_l0 = "u0";
        sbox.idx_create_lo = int.Parse(ViewState["EMP_HUMAN"].ToString());
        ViewState["reqjob1"] = reqtest.Text;
        sbox.requried_old = ViewState["reqjob1"].ToString();
        bbox.jo_log_action[0] = sbox;
        bbox = callService(_urlInsertloggaritum, bbox);

    }

    protected void editjob()
    {
        data_jo box = new data_jo();
        job_order_overview box1 = new job_order_overview();
        box.job_order_overview_action = new job_order_overview[1];
        box.job_order_overview_action[0] = box1;

        box = callService(_urlEditDatajoborder, box);
        // setGridData(jogrid,box.job_order_overview_action );
    }

    protected void updatejob()
    {
        data_jo box = new data_jo();
        job_order_overview box1 = new job_order_overview();
        box.job_order_overview_action = new job_order_overview[1]; //ประกาศกล่องย่อยฝั่ง xml
        box1.u0_jo_idx = int.Parse(ViewState["u0_jo_idx_update"].ToString());
        box1.title_jo = ViewState["titleUpdate"].ToString();
        box1.require_jo = ViewState["reqUpdate"].ToString();
        box.job_order_overview_action[0] = box1; // เอากล่องเล็กยัดใส่กล่องย่อย        
                                                 // string localJson1 = _funcTool.convertObjectToJson(box);
                                                 //  p.Text = localJson1;
        box = callService(_urlUpdateDatajoborder, box);
    }

    protected void deletejob()
    {
        data_jo box = new data_jo();
        job_order_overview box1 = new job_order_overview();
        box.job_order_overview_action = new job_order_overview[1]; //ประกาศกล่องย่อยฝั่ง xml          
        box1.u0_jo_idx = int.Parse(ViewState["jo_idx"].ToString());
        box.job_order_overview_action[0] = box1; // เอากล่องเล็กยัดใส่กล่องย่อย
                                                 // p1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(box));
        box = callService(_urlDeleteDatajoborder, box);
        // updatejob();

    }

    protected void SelectViewIndex()
    {
        // // DateTime daten;
        // string datetime = DateTimePicker1.Value.ToString("startdate");
        TextBox startdate = ((TextBox)View3.FindControl("startdate"));
        TextBox finaldate = ((TextBox)View3.FindControl("finaldate"));
        data_jo box = new data_jo();
        job_order_overview box1 = new job_order_overview();
        box.job_order_overview_action = new job_order_overview[1];

        box1.u0_jo_idx = int.Parse(ViewState["id_idx"].ToString());
       // box1.sec_mis = int.Parse(ViewState["_rsec_mis"].ToString());


       
        //p1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(box));
        box.job_order_overview_action[0] = box1;
        // startdate.Text = box.job_order_overview_action[0].day_start.ToString();
        box = callService(_urlSelectDatajoborder_mhead, box);
        ViewState["reqendbox"] = box.job_order_overview_action[0].require_jo;
        //ViewState["_rsec_mis"] = box.job_order_overview_action[0].sec_mis;
     
        setFormViewData(DetailUser, FormViewMode.ReadOnly, box.job_order_overview_action);
        setFormViewData(DetailUserviewH, FormViewMode.ReadOnly, box.job_order_overview_action);
        Label reqcode = ((Label)DetailUser.FindControl("reqcode"));
        reqcode.Text = HttpUtility.HtmlDecode(ViewState["reqendbox"].ToString());
      
        ViewState["startdate"] = startdate.Text;
        ViewState["finaldate"] = finaldate.Text;
        // startdate.Text = ViewState["startdate"].ToString();
        // finaldate.Text  = ViewState["finaldate"].ToString();
        // txtstartdate.Text = ViewState["startdate"].ToString();
        // txtfinaldate.Value = ViewState["finaldate"].ToString();
        ViewState["startdate"] = box.job_order_overview_action[0].day_start.ToString();
        ViewState["finaldate"] = box.job_order_overview_action[0].day_finish.ToString();
        //ViewState["amountdate"] = box.job_order_overview_action[0].day_amount.ToString();
    }

    protected void SelectViewIndex2()
    {
        data_jo box = new data_jo();
        job_order_overview box1 = new job_order_overview();
        box.job_order_overview_action = new job_order_overview[1];
        box.job_order_overview_action[0] = box1;

        box1.u0_jo_idx = int.Parse(ViewState["idx"].ToString());
       

        // box1.AdminMain = ViewState["admin_name"].ToString();

     //   box1.rsec_creator = int.Parse(ViewState["_rsec_creator"].ToString());
     

        box = callService(_urlSelectDatajoborder_mhead, box);
        ViewState["reqbox"] = box.job_order_overview_action[0].require_jo;
        //  p1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(box));
        
        setFormViewData(DetailUserpv, FormViewMode.ReadOnly, box.job_order_overview_action);
        setFormViewData(DetaileditUser, FormViewMode.ReadOnly, box.job_order_overview_action);
        FormView Detailedit = ((FormView)userpanel.FindControl("DetaileditUser"));
        //GridView gvFile = ((GridView)Detailedit.FindControl("gvFile"));
        LinkButton btnedit = ((LinkButton)Detailedit.FindControl("btnedit"));
        LinkButton _addafter = ((LinkButton)Detailedit.FindControl("addafter"));

        TextBox comment_user = ((TextBox)Detailedit.FindControl("txt_comment_noedit"));
        Label _labcomment = ((Label)Detailedit.FindControl("labcomment_noedit"));

        LinkButton truebtn = ((LinkButton)userpanel.FindControl("truebut"));
        LinkButton cmdcancel = ((LinkButton)userpanel.FindControl("cmdcancel"));
        Label req2 = ((Label)Detailedit.FindControl("reqcode_j"));
        if (ViewState["node_idx"].ToString() == "1" && int.Parse(ViewState["Sec_idx"].ToString()) == box.job_order_overview_action[0].rsec_creator)
        {
            //  gvFile.Visible = true;
            btnedit.Visible = true;
            _addafter.Visible = true;
            comment_user.Visible = true;
            _labcomment.Visible = true;
        }
        // gvFile.Visible = true;
        truebtn.Visible = false;
        cmdcancel.Visible = false;
        req2.Text = HttpUtility.HtmlDecode(ViewState["reqbox"].ToString());
        
    }

    protected void SelectViewIndex3()
    {

        data_jo box = new data_jo();
        job_order_overview box1 = new job_order_overview();
        box.job_order_overview_action = new job_order_overview[1];
        box.job_order_overview_action[0] = box1;

        box1.u0_jo_idx = int.Parse(ViewState["idxchange"].ToString());
        // box1.id_statework = int.Parse(ViewState["status"].ToString());
        //p1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(box));
        box = callService(_urlSelectDatajoborder_mhead, box);
        setFormViewData(DetaileditUser, FormViewMode.Edit, box.job_order_overview_action);
        LinkButton truebtn = ((LinkButton)userpanel.FindControl("truebut"));
        LinkButton cmdcancel = ((LinkButton)userpanel.FindControl("cmdcancel"));

        ViewState["reqedit1"] = box.job_order_overview_action[0].require_jo;
        
        truebtn.Visible = true;
        cmdcancel.Visible = true;


        try
        {

            string getPathLotus = ConfigurationManager.AppSettings["pathfile_Job_order"];

            string filePathLotus = Server.MapPath(getPathLotus + ViewState["_no_invoice"].ToString());
            DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);

            SearchDirectories(myDirLotus, ViewState["_no_invoice"].ToString());




        }
        catch
        {

        }





        // p2.Text = ViewState["reqedit1"].ToString();
    }

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {

        var ddName = (DropDownList)sender;

        DropDownList ddOrg_seachall = (DropDownList)viewall_pages.FindControl("ddOrg_seachall");
        DropDownList ddDept_seachall = (DropDownList)viewall_pages.FindControl("ddDept_seachall");
        DropDownList ddSec_seachall = (DropDownList)viewall_pages.FindControl("ddSec_seachall");
     //   DropDownList ddlinsert_sec = (DropDownList)create_story.FindControl("ddlinsert_sec");

        switch (ddName.ID)
        {
          
            case "ddOrg_seachall":
                getDepartmentList(ddDept_seachall, int.Parse(ddOrg_seachall.SelectedItem.Value));
                ddSec_seachall.Items.Clear();
                ddSec_seachall.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "0"));
                break;
            case "ddDept_seachall":        
                getSectionList(ddSec_seachall, int.Parse(ddOrg_seachall.SelectedItem.Value), int.Parse(ddDept_seachall.SelectedItem.Value));
                break;
            case "ddlSearchDate":

                if (int.Parse(ddlSearchDate.SelectedValue) == 0)
                {
                    dateIdf.Enabled = false;
                    dateId.Enabled = false;
                }
                else if (int.Parse(ddlSearchDate.SelectedValue) == 3)
                {
                    dateIdf.Enabled = true;
                    dateId.Enabled = true;
                }
                else
                {
                    dateId.Enabled = true;
                    dateIdf.Enabled = false;
                }
                break;
            case "DDseach_dateall":

                if (int.Parse(DDseach_dateall.SelectedValue) == 0)
                {
                    txtseach_stastdate.Enabled = false;
                    txtseach_enddate.Enabled = false;
                }
                else if (int.Parse(DDseach_dateall.SelectedValue) == 3)
                {
                    txtseach_stastdate.Enabled = true;
                    txtseach_enddate.Enabled = true;
                }
                else
                {
                    txtseach_stastdate.Enabled = true;
                    txtseach_enddate.Enabled = false;
                }

                break;

        }
    }

    #endregion

    #region setFormData
    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }
    #endregion

    #region callService
    protected data_jo callService(string _cmdUrl, data_jo _data_jo)
    {
        _localJson = _funcTool.convertObjectToJson(_data_jo);
        // litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_jo = (data_jo)_funcTool.convertJsonToObject(typeof(data_jo), _localJson);

        return _data_jo;
    }
    #endregion

    #region rdbt_end
    protected void rdbt_end_CheckedChanged(object sender, EventArgs e)
    {
        var cb = (CheckBox)sender;
        switch (cb.ID)
        {
            case "rdbt_end":

                //CheckBox oakja = (CheckBox)nannasi.FindControl("rdbt_end");
                if (rdbt_end.Checked == true)
                {
                    if (ViewState["num_last"].ToString() == "8" || ViewState["num_last"].ToString() == "10")
                    {
                        saveall.Visible = true;
                        save_viewdep.Visible = true;
                    }
                    else if (ViewState["num_last"].ToString() != "5")
                    {
                        save_viewdep.Visible = true;
                        saveall.Visible = true;
                    }
                }
                else
                {
                    if (ViewState["num_last"].ToString() == "8" || ViewState["num_last"].ToString() == "10")
                    {
                        saveall.Visible = false;
                        int count_o = gvinfv_showtitle.Rows.Count;
                        if (count_o != 0)
                        {
                            saveall.Visible = true;
                            save_viewdep.Visible = true;
                        }
                    }
                    else if (ViewState["num_last"].ToString() != "5")
                    {
                        saveall.Visible = false;
                        save_viewdep.Visible = false;
                    }
                }
              //  rdbt_end.Focus();
                //else
                //{
                //    data_jo Select_for = new data_jo();
                //    job_order_overview sel_fo = new job_order_overview();
                //    Select_for.job_order_overview_action = new job_order_overview[1];

                //    sel_fo.u0_jo_idx = int.Parse(main_id_to_sql.Text);

                //    Select_for.job_order_overview_action[0] = sel_fo;
                //    Select_for = callService(_urlSelectDatajoborder_headlook, Select_for);

                //    ViewState["lookid_sta"] = Select_for.job_order_overview_action[0].id_statework.ToString();
                //    if (ViewState["lookid_sta"].ToString() != "5") {
                //        data_jo bigbox = new data_jo();
                //        jo_add_datalist smallbox = new jo_add_datalist();
                //        bigbox.jo_add_datalist_action = new jo_add_datalist[1];

                //        smallbox.wait = int.Parse(Label2.Text);

                //        bigbox.jo_add_datalist_action[0] = smallbox;
                //        // dog1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(bigbox));
                //        bigbox = callService(_urlSelectU1ov, bigbox);
                //        setGridData(gvanthoer_old, bigbox.jo_add_datalist_action);
                //        ViewState["statesss"] = bigbox.jo_add_datalist_action[0].u0_jo_idx_ref.ToString();

                //        if (ViewState["statesss"].ToString() != "0")
                //        {
                //            LinkButton2.Visible = true;
                //        }
                //        else
                //        {
                //            LinkButton2.Visible = false;
                //        }
                //    }
                //}
                break;

        }
    }

    #endregion

    #region setActiveView
    protected void setActiveView(string activeTab, int uidx)
    {

        multimaster.SetActiveView((View)multimaster.FindControl(activeTab));

      
        //visible_base();

        //  gethead();
        // getmishead();
        jogrid.DataBind();
       // jogrid2.DataBind();
        //u1grid.DataBind();
        //create.Visible = true;
        grid.Visible = true;
        searchbutton.Visible = true;
        btnshow_searchall.Visible = true;
        selectjob();
        pageall_work();
       // lb_lookformis.Visible = false;


        Gvmaster.PageIndex = 0;
        Gvmastermis.PageIndex = 0;
        gvinfv_showtitle.PageIndex = 0;
        gvanthoer_old.PageIndex = 0;
        gv_logold.PageIndex = 0;
     //   gv_logjob.PageIndex = 0;
        jogrid.PageIndex = 0;
      //  jogrid2.PageIndex = 0;
        u1grid.PageIndex = 0;
        gv_allwork.PageIndex = 0;


        switch (activeTab)
        {

            case "create_story":
                getSectionList_secmis(ddlinsert_sec, 1, 20);
                setFormData(fvEmpDetail, FormViewMode.ReadOnly, ((data_employee)ViewState["vsEmpProfile"]).employee_list);
                titlename.Text = String.Empty ;
                require.Text = String.Empty;
                //ViewState["sec_mis"] = Sec_mis_send.Text;

                linkBtnTrigger(trueadd);
                //กด Tab เซตค่า รหัสเอกสารเป็น null แล้วสังให้ bind gridview ใหม่
                ViewState["_no_invoice"] = null;
                Gv_filemisshared.DataSource = ViewState["_no_invoice"];
                Gv_filemisshared.DataBind();

                gvanthoer_old.DataSource = null;
                gvanthoer_old.DataBind();

                gv_logold.DataSource = null;
                gv_logold.DataBind();

                break;
            case "view1":


               // ViewState["view1"] = 1;

                //test_l.Text = ViewState["view1"].ToString();

                Panel _divformhead1 = (Panel)viewhead.FindControl("divformhead");
                FormView _fmbigmak1 = (FormView)_divformhead1.FindControl("formviewhead");
                LinkButton _Vokgogo = (LinkButton)_fmbigmak1.FindControl("Vokgogo");
                LinkButton _Vnookgo = (LinkButton)_fmbigmak1.FindControl("Vnookgo");

                // jogrid.DataBind();
                pl_titleinsert.Visible = true;
                Linkback_head.Visible = false;
                LinkButton2.Visible = false;
                TempCodeId.Text = null;
                txtdateId.Value = null;
                empidsearch.Text = null;
                ddlSearchDate.SelectedValue = null;
                view1_btn();

                //_Vokgogo.Visible = true;
                //_Vnookgo.Visible = true;

                accept.Visible = true; // button
                decline.Visible = true; // button

                accept_allpage.Visible = false;
                declineall_page.Visible = false;

                LinkButton1.Visible = true;
                LinkBu_back_missh.Visible = false;
                searchbutton.Visible = true;
                searchhidden.Visible = false;
                _divsearch.Visible = false;
                lb_lookformis.Visible = true;
                lb_rdef_same_head.Visible = true;
                LinkButton1.Visible = false;
                back_rdepsamw.Visible = true;
                btnback.Visible = false;
                btnview3_to2.Visible = false;
                btnback2.Visible = true;
                btnV4back_all.Visible = false;
                //กด Tab เซตค่า รหัสเอกสารเป็น null แล้วสังให้ bind gridview ใหม่
                ViewState["_no_invoice"] = null;
                Gv_filemisshared.DataSource = ViewState["_no_invoice"];
                Gv_filemisshared.DataBind();

                gvanthoer_old.DataSource = null;
                gvanthoer_old.DataBind();

                gv_logold.DataSource = null;
                gv_logold.DataBind();

                selectjob();
                break;
            //case "viewhead":
            //    Panel _divformhead1 = (Panel)viewhead.FindControl("divformhead");
            //    FormView _fmbigmak = (FormView)_divformhead1.FindControl("formviewhead");
            //    LinkButton _Vokgogo = (LinkButton)_fmbigmak.FindControl("Vokgogo");
            //    LinkButton _Vnookgo = (LinkButton)_fmbigmak.FindControl("Vnookgo");

            //    viewhead_btn();
            //    plhead.Visible = true;
            //    lb_rdef_same_head.Visible = false;
            //    Linkback_head.Visible = false;
            //    LinkButton2.Visible = true;

            //    //กด Tab เซตค่า รหัสเอกสารเป็น null แล้วสังให้ bind gridview ใหม่
            //    ViewState["_no_invoice"] = null;
            //    Gv_filemisshared.DataSource = ViewState["_no_invoice"];
            //    Gv_filemisshared.DataBind();

            //    break;
            //case "viewmis":
            //    viewmis_btn();
            //    view_mis_btn_no_sha();
            //    title_indb.Text = null;
            //    start_requiremis.Text = null;
            //    getmishead();
            //    pl_titleinsert.Visible = true;

            //    //กด Tab เซตค่า รหัสเอกสารเป็น null แล้วสังให้ bind gridview ใหม่
            //    ViewState["_no_invoice"] = null;
            //    Gv_filemisshared.DataSource = ViewState["_no_invoice"];
            //    Gv_filemisshared.DataBind();

            //    break;
            //case "view2":
            //    selectmheadjob();
            //    btnback.Visible = true;
            //    btnview3_to2.Visible = false;
            //    back_rdepsamw.Visible = false;

            //    //กด Tab เซตค่า รหัสเอกสารเป็น null แล้วสังให้ bind gridview ใหม่
            //    ViewState["_no_invoice"] = null;
            //    Gv_filemisshared.DataSource = ViewState["_no_invoice"];
            //    Gv_filemisshared.DataBind();

            //    break;
            case "viewall_pages":


                //Panel _divformhead2 = (Panel)viewhead.FindControl("divformhead");
                //FormView _fmbigmak2 = (FormView)_divformhead2.FindControl("formviewhead");
                //LinkButton _Vokgogoall = (LinkButton)_fmbigmak2.FindControl("Vokgogo_allpage");
                //LinkButton _Vnookgoall = (LinkButton)_fmbigmak2.FindControl("Vnookgo_allpage");

                pageall_work();
                gv_allwork.DataBind();
                all_page_page();
                viewhead_btn();
                create_Story_btn();
                view_mis_btn_no_sha();
                viewmis_btn();
                view1_btn();
                //view3_btn();

                //saveall.Visible = true;
                accept.Visible = false; // button
                decline.Visible = false; // button

                accept_allpage.Visible = true;
                declineall_page.Visible = true;
                view4_btn();
                back_rdepsamw.Visible = false;
                btnshow_searchall.Visible = true;
                btnclose_searchall.Visible = false;
                Divseach_all.Visible = false;
                btnback.Visible = false;
                btnback2.Visible = false;
                btnV4back_all.Visible = true;
                btnview3_to2.Visible = true;
                LinkButton1.Visible = false;
                LinkButton2.Visible = false;
                lb_rdef_same_head.Visible = false;
                Linkback_head.Visible = true;
                LinkBu_back_missh.Visible = true;

                lb_lookformis.Visible = false;              
                //กด Tab เซตค่า รหัสเอกสารเป็น null แล้วสังให้ bind gridview ใหม่
                ViewState["_no_invoice"] = null;
                Gv_filemisshared.DataSource = ViewState["_no_invoice"];
                Gv_filemisshared.DataBind();

                gvanthoer_old.DataSource = null;
                gvanthoer_old.DataBind();

                gv_logold.DataSource = null;
                gv_logold.DataBind();

                break;
            case "View3":
                startdate.Text = null;
                finaldate.Text = null;
                txt_comment_misH.Text = null;
                break;
        }
    }
    #endregion

    #region navCommand
    protected void navCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        setActiveTab(cmdArg, 0);
    }

    #endregion

    #region btn all default
    protected void all_page_page()
    {
        Linacceptsent.Visible = true;
        Linclosemishead2.Visible = true;
        LinkButton5.Visible = true;
    }
    protected void viewhead_btn()
    {
        plhead.Visible = false;
        divformhead.Visible = false;
        LinkButton2.Visible = true;
        Linkback_head.Visible = false;
    }
    protected void create_Story_btn()
    {
        remoteuser.Visible = true;
    }
    protected void view_mis_btn_no_sha()
    {
        plmis.Visible = true;
    }
    protected void viewmis_btn()
    {
        title_old.Visible = true;
        LinkBu_back_missh.Visible = false;
        title_old_log.Visible = false;

        LinkButton1.Visible = true;
        name_crate.Visible = true;
        Title_create.Visible = false;
        plname_cre.Visible = true;
        Pl_data_req.Visible = false;
        pl_titleinsert.Visible = true;
        look_log_u0.Visible = true;
        look_u0.Visible = false;
        pl_gvold.Visible = true;
        gvanthoer_old.Visible = true;
        pl_log_gvold.Visible = false;
        gv_logold.Visible = true;
        pl_succ.Visible = true;
        successwork.Visible = false;

    }
    protected void view1_btn()
    {
        _divsearch.Visible = false; // div
        trueadd.Visible = true;//buttton
        reset.Visible = true; // button
        closejob.Visible = true; // button
        yomrup.Visible = true; // button
        
        maiyomrup.Visible = true; // button
    }
    protected void view3_btn()
    {
        accept.Visible = true; // button
        decline.Visible = true; // button
    }
    protected void view4_btn()
    {
        u0_idx.Visible = false; // TextBox
        userpanel2.Visible = true; // Panel
        userpanel.Visible = true; // Panel
        truebut.Visible = false; // button
        cmdcancel.Visible = false; // button
        gridshowu1.Visible = true; // Panel
        btnback2.Visible = true; // button
    }
    protected void visible_base()
    {
        Pl_allsta.Visible = true;
        plhead.Visible = false;
        divformhead.Visible = false;
        LinkButton2.Visible = true;
        Linkback_head.Visible = true;

        LinkButton1.Visible = true;
        LinkBu_back_missh.Visible = false;
        plname_cre.Visible = true;
        Pl_data_req.Visible = false;
        pl_titleinsert.Visible = true;
        pl_gvold.Visible = true;
        pl_log_gvold.Visible = false;
        pl_succ.Visible = true;
        successwork.Visible = false;
        remoteuser.Visible = true;
        searchbutton.Visible = false;
        searchhidden.Visible = false;
        btnshow_searchall.Visible = false;
        btnclose_searchall.Visible = false;
        _divsearch.Visible = false;
        grid.Visible = true;
        DetailUsersent.Visible = true;
        yomrup.Visible = true;
        maiyomrup.Visible = true;
       // grid2.Visible = true;
        btnback.Visible = true;
        u0_idx.Visible = false;
        userpanel2.Visible = true;
        userpanel.Visible = true;
        truebut.Visible = false;
        cmdcancel.Visible = false;
        gridshowu1.Visible = false;
        btnback2.Visible = true;
        //u0log_idx.Visible = false;

    }

    #endregion

    #region setActiveTab
    protected void setActiveTab(string activeTab, int uidx)
    {
        setActiveView(activeTab, uidx);
        switch (activeTab)
        {
            case "create_story":
               
                li_create_story_rdep.Attributes.Add("class", "active");
                li_about_rdep.Attributes.Add("class", "");
                //li_head_rdep.Attributes.Add("class", "");
                //li_mis_rdep.Attributes.Add("class", "");
                //li_head_Mis.Attributes.Add("class", "");
                li_all_page_dredp.Attributes.Add("class", "");
                break;
            case "view1":
                li_create_story_rdep.Attributes.Add("class", "");
                li_about_rdep.Attributes.Add("class", "active");
                //li_head_rdep.Attributes.Add("class", "");
                //li_mis_rdep.Attributes.Add("class", "");
                //li_head_Mis.Attributes.Add("class", "");
                li_all_page_dredp.Attributes.Add("class", "");
                break;
            case "viewhead":
                li_create_story_rdep.Attributes.Add("class", "");
                li_about_rdep.Attributes.Add("class", "active");
                //li_head_rdep.Attributes.Add("class", "active");
                //li_mis_rdep.Attributes.Add("class", "");
                //li_head_Mis.Attributes.Add("class", "");
                li_all_page_dredp.Attributes.Add("class", "");
                break;
            case "viewmis":
                li_create_story_rdep.Attributes.Add("class", "");
                li_about_rdep.Attributes.Add("class", "active");
                //li_head_rdep.Attributes.Add("class", "");
                //li_mis_rdep.Attributes.Add("class", "active");
                //li_head_Mis.Attributes.Add("class", "");
                li_all_page_dredp.Attributes.Add("class", "");
                break;
            case "view2":
                li_create_story_rdep.Attributes.Add("class", "");
                li_about_rdep.Attributes.Add("class", "active");
                //li_head_rdep.Attributes.Add("class", "");
                //li_mis_rdep.Attributes.Add("class", "");
                //li_head_Mis.Attributes.Add("class", "active");
                li_all_page_dredp.Attributes.Add("class", "");
                break;
            case "viewall_pages":
                li_create_story_rdep.Attributes.Add("class", "");
                li_about_rdep.Attributes.Add("class", "");
                //li_head_rdep.Attributes.Add("class", "");
                //li_mis_rdep.Attributes.Add("class", "");
                //li_head_Mis.Attributes.Add("class", "");
                li_all_page_dredp.Attributes.Add("class", "active");
                break;
        }
    }

    #endregion

    #region linkBtnTrigger
    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger = new PostBackTrigger();
        trigger.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger);
    }

    #endregion reuse

    #region uplode file
    protected string MapURL(string path)
    {

        string appPath = Server.MapPath("~/").ToLower();
        return string.Format("/{0}", path.ToLower().Replace(appPath, "").Replace(@"\", "/"));
    }

    public void SearchDirectories(DirectoryInfo dir, String target)
    {

        string dirfiles = dir.ToString();

        DataSet ds1 = new DataSet();
        DataTable dt1 = new DataTable();
        DataRow dr1 = dt1.NewRow();
        DataColumn dc1 = new DataColumn("FileName", typeof(string));
        dc1 = new DataColumn("Download", typeof(string));
        dt1.Columns.Add("FileName");
        dt1.Columns.Add("Download");

        FileInfo[] files = dir.GetFiles();
        int i = 0;
        if (files.Length > 0)
        {

            foreach (FileInfo file in files)
            {
                //if (file.Name.ToLower().IndexOf(target.ToLower()) > -1)
                //{
                string[] f = Directory.GetFiles(dirfiles, file.Name, SearchOption.AllDirectories);
                dt1.Rows.Add(file.Name);
                dt1.Rows[i][1] = f[0];
                i++;
                //}
            }

        }
        Panel userpanel = (Panel)View4.FindControl("userpanel");
        FormView DetaileditUser = (FormView)userpanel.FindControl("DetaileditUser");
        GridView gvFile = (GridView)DetaileditUser.FindControl("gvFile");
        gvFile.Visible = true;
        // Panel gvFileLo1 = (Panel)FvDetailUser.FindControl("gvFileLo1");

        if (dt1.Rows.Count > 0)
        {

            ds1.Tables.Add(dt1);
            // gvFileLo1.Visible = true;
            gvFile.DataSource = ds1.Tables[0]; //กรณี ที่กำหนดให้ดึง DataTable ใน DataSet 
            gvFile.DataBind();
            ds1.Dispose();
        }
        else
        {

            gvFile.DataSource = null;
            gvFile.DataBind();

        }



    }
    

    public void SearchDirectories_Vhead(DirectoryInfo dir, String target)
    {

        string dirfiles = dir.ToString();

        DataSet ds1 = new DataSet();
        DataTable dt1 = new DataTable();
        DataRow dr1 = dt1.NewRow();
        DataColumn dc1 = new DataColumn("FileName", typeof(string));
        dc1 = new DataColumn("Download", typeof(string));
        dt1.Columns.Add("FileName");
        dt1.Columns.Add("Download");

        FileInfo[] files = dir.GetFiles();
        int i = 0;
        if (files.Length > 0)
        {

            foreach (FileInfo file in files)
            {
                //if (file.Name.ToLower().IndexOf(target.ToLower()) > -1)
                //{
                string[] f = Directory.GetFiles(dirfiles, file.Name, SearchOption.AllDirectories);
                dt1.Rows.Add(file.Name);
                dt1.Rows[i][1] = f[0];
                i++;
                //}
            }

        }
        Panel _divformhead = (Panel)viewhead.FindControl("divformhead");
        FormView Formhead = (FormView)_divformhead.FindControl("formviewhead");
        GridView Gv_filehead = (GridView)Formhead.FindControl("Gv_filehead");
        // Panel gvFileLo1 = (Panel)FvDetailUser.FindControl("gvFileLo1");

        if (dt1.Rows.Count > 0)
        {

            ds1.Tables.Add(dt1);
            // gvFileLo1.Visible = true;
            Gv_filehead.DataSource = ds1.Tables[0]; //กรณี ที่กำหนดให้ดึง DataTable ใน DataSet 
            Gv_filehead.DataBind();
            ds1.Dispose();
        }
        else
        {

            Gv_filehead.DataSource = null;
            Gv_filehead.DataBind();

        }



    }
    public void SearchDirectories_Vmisshare(DirectoryInfo dir, String target)
    {

        string dirfiles = dir.ToString();

        DataSet ds1 = new DataSet();
        DataTable dt1 = new DataTable();
        DataRow dr1 = dt1.NewRow();
        DataColumn dc1 = new DataColumn("FileName", typeof(string));
        dc1 = new DataColumn("Download", typeof(string));
        dt1.Columns.Add("FileName");
        dt1.Columns.Add("Download");

        FileInfo[] files = dir.GetFiles();
        int i = 0;
        if (files.Length > 0)
        {

            foreach (FileInfo file in files)
            {
                //if (file.Name.ToLower().IndexOf(target.ToLower()) > -1)
                //{
                string[] f = Directory.GetFiles(dirfiles, file.Name, SearchOption.AllDirectories);
                dt1.Rows.Add(file.Name);
                dt1.Rows[i][1] = f[0];
                i++;
                //}
            }

        }

        GridView Gv_filemisshared = (GridView)viewmisshare.FindControl("Gv_filemisshared");
        // Panel gvFileLo1 = (Panel)FvDetailUser.FindControl("gvFileLo1");

        if (dt1.Rows.Count > 0)
        {

            ds1.Tables.Add(dt1);
            // gvFileLo1.Visible = true;
            Gv_filemisshared.DataSource = ds1.Tables[0]; //กรณี ที่กำหนดให้ดึง DataTable ใน DataSet 
            Gv_filemisshared.DataBind();
            ds1.Dispose();
            
        }
        else
        {

            Gv_filemisshared.DataSource = null;
            Gv_filemisshared.DataBind();

        }

    }
    public void SearchDirectories_Vmishead(DirectoryInfo dir, String target)
    {

        string dirfiles = dir.ToString();

        DataSet ds1 = new DataSet();
        DataTable dt1 = new DataTable();
        DataRow dr1 = dt1.NewRow();
        DataColumn dc1 = new DataColumn("FileName", typeof(string));
        dc1 = new DataColumn("Download", typeof(string));
        dt1.Columns.Add("FileName");
        dt1.Columns.Add("Download");

        FileInfo[] files = dir.GetFiles();
        int i = 0;
        if (files.Length > 0)
        {

            foreach (FileInfo file in files)
            {
                //if (file.Name.ToLower().IndexOf(target.ToLower()) > -1)
                //{
                string[] f = Directory.GetFiles(dirfiles, file.Name, SearchOption.AllDirectories);
                dt1.Rows.Add(file.Name);
                dt1.Rows[i][1] = f[0];
                i++;
                //}
            }

        }
        FormView FormMIShead = (FormView)View3.FindControl("DetailUser");
        GridView Gv_filemisH = (GridView)FormMIShead.FindControl("Gv_filemisH");
        // Panel gvFileLo1 = (Panel)FvDetailUser.FindControl("gvFileLo1");

        if (dt1.Rows.Count > 0)
        {

            ds1.Tables.Add(dt1);
            // gvFileLo1.Visible = true;
            Gv_filemisH.DataSource = ds1.Tables[0]; //กรณี ที่กำหนดให้ดึง DataTable ใน DataSet 
            Gv_filemisH.DataBind();
            ds1.Dispose();
        }
        else
        {

            Gv_filemisH.DataSource = null;
            Gv_filemisH.DataBind();

        }

    }
    #endregion
}