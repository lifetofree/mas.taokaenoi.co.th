<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="form.aspx.cs" Inherits="websystem_form" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" Runat="Server">
	<div class="col-md-12" role="main">
		<div class="bs-docs-example">
			<h4>Form with validations</h4>
			<asp:FormView ID="fvExample" runat="server" DefaultMode="Insert" CssClass="table table-borderless">
				<InsertItemTemplate>
					<div class="form-horizontal" role="form">
						<div class="form-group">test
							<label class="col-sm-2 control-label">
									<asp:Label ID="lblName" runat="server" Text="Name"></asp:Label></label>
							<div class="col-sm-3">
									<asp:TextBox ID="tbName" runat="server" CssClass="form-control" placeholder="Name" MaxLength="30" ValidationGroup="formInsert" />
							</div>
							<div class="col-sm-3 control-label textleft-red">
								<asp:RequiredFieldValidator ID="rfvName" ValidationGroup="formInsert" runat="server" Display="Dynamic" ControlToValidate="tbName" ErrorMessage="Plese enter your data" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">
									<asp:Label ID="lblDesc" runat="server" Text="Description"></asp:Label></label>
							<div class="col-sm-3">
									<asp:TextBox ID="tbDesc" runat="server" CssClass="form-control" placeholder="Description" MaxLength="500" ValidationGroup="formInsert" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<asp:LinkButton ID="lbInsert" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" ValidationGroup="formInsert"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></asp:LinkButton>
								<asp:LinkButton ID="lbCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="Cancel"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></asp:LinkButton>
							</div>
						</div>
					</div>
				</InsertItemTemplate>
			</asp:FormView>	
		</div>
	</div>	
</asp:Content>