using System;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_tpm_report")]
public class data_tpm_report
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    [XmlElement("tpm_report_list")]
    public tpm_report_detail[] tpm_report_list { get; set; }
    [XmlElement("search_tpm_report_list")]
    public search_tpm_report_detail[] search_tpm_report_list { get; set; }
    [XmlElement("tpm_permission_list")]
    public tpm_permission_detail[] tpm_permission_list { get; set; }
}

[Serializable]
public class tpm_report_detail
{
    [XmlElement("u0_docidx")]
    public int u0_docidx { get; set; }
    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }
    [XmlElement("emp_code")]
    public string emp_code { get; set; }
    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }
    [XmlElement("org_idx")]
    public int org_idx { get; set; }
    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }
    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }
    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }
    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }
    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }
    [XmlElement("rpos_idx")]
    public int rpos_idx { get; set; }
    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }
    [XmlElement("group_idx")]
    public int group_idx { get; set; }
    [XmlElement("group_name")]
    public string group_name { get; set; }

    [XmlElement("cal_1")]
    public decimal cal_1 { get; set; }
    [XmlElement("cal_2")]
    public decimal cal_2 { get; set; }
    [XmlElement("cal_3")]
    public decimal cal_3 { get; set; }
    [XmlElement("cal_4")]
    public decimal cal_4 { get; set; }
    [XmlElement("cal_5")]
    public decimal cal_5 { get; set; }

    [XmlElement("sum_cal_1")]
    public decimal sum_cal_1 { get; set; }
    [XmlElement("sum_cal_2")]
    public decimal sum_cal_2 { get; set; }
    [XmlElement("sum_cal_3")]
    public decimal sum_cal_3 { get; set; }
    [XmlElement("sum_cal_4")]
    public decimal sum_cal_4 { get; set; }
    [XmlElement("sum_cal_5")]
    public decimal sum_cal_5 { get; set; }

    [XmlElement("score_cal_1")]
    public decimal score_cal_1 { get; set; }
    [XmlElement("score_cal_2")]
    public decimal score_cal_2 { get; set; }
    [XmlElement("score_cal_3")]
    public decimal score_cal_3 { get; set; }
    [XmlElement("score_cal_4")]
    public decimal score_cal_4 { get; set; }
    [XmlElement("score_cal_5")]
    public decimal score_cal_5 { get; set; }

    [XmlElement("count_competency")]
    public int count_competency { get; set; }
}

[Serializable]
public class search_tpm_report_detail
{
    [XmlElement("s_emp_idx")]
    public string s_emp_idx { get; set; }
    [XmlElement("s_report_mode")]
    public string s_report_mode { get; set; }
}

[Serializable]
public class tpm_permission_detail
{
    [XmlElement("u0_idx")]
    public int u0_idx { get; set; }
    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }
    [XmlElement("rpos_idx")]
    public int rpos_idx { get; set; }
    [XmlElement("s_org_idx")]
    public string s_org_idx { get; set; }
    [XmlElement("permission_level")]
    public int permission_level { get; set; }
    [XmlElement("permission_status")]
    public int permission_status { get; set; }
}