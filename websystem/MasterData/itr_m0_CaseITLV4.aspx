﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="itr_m0_CaseITLV4.aspx.cs" Inherits="websystem_MasterData_itr_m0_CaseITLV4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <asp:Literal ID="text" runat="server"></asp:Literal>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

                <asp:View ID="ViewIndex" runat="server">

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-gears"></i><strong>&nbsp; Case Close Job LV.4</strong></h3>
                        </div>

                        <div class="panel-body">
                            <div class="form-group">

                                <asp:LinkButton ID="btnshow" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="AddCaseCloseJobLv3" runat="server" CommandName="CmdAdd" OnCommand="btnCommand"><i class="fa fa-plus"></i></asp:LinkButton>
                            </div>

                            <%------------------------ Div ADD  ------------------------%>
                            <asp:Panel ID="Panel_Add" runat="server" Visible="false">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><i class="glyphicon glyphicon-plus"></i><strong>&nbsp; Add Case Close Job LV.4</strong></h4>
                                    <div class="form-horizontal" role="form">
                                        <div class="panel-heading">

                                            
                                               <div class="form-group">
                                                <asp:Label ID="Label4" runat="server" Text="Case Lv1." CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="ddlITLV1" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server">
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="Save" runat="server" Display="None"
                                                        ControlToValidate="ddlITLV1" Font-Size="11"
                                                        ErrorMessage="เลือกหัวข้อ LV1"
                                                        ValidationExpression="เลือกหัวข้อ LV1" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator3" Width="160" />

                                                </div>
                                            </div>

                                               <div class="form-group">
                                                <asp:Label ID="Label3" runat="server" Text="Case Lv2." CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="ddlITLV2" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="0">เลือกอาการ..</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RqddlEquipmentEditDeviceCode1" ValidationGroup="Save" runat="server" Display="None"
                                                        ControlToValidate="ddlITLV2" Font-Size="11"
                                                        ErrorMessage="เลือกอาการ LV2"
                                                        ValidationExpression="เลือกอาการ LV2" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender16" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqddlEquipmentEditDeviceCode1" Width="160" />

                                                </div>
                                            </div>

                                               <div class="form-group">
                                                <asp:Label ID="Label5" runat="server" Text="Case Lv3." CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="ddlITLV3" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="0">เลือกสาเหตุ..</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="Save" runat="server" Display="None"
                                                        ControlToValidate="ddlITLV3" Font-Size="11"
                                                        ErrorMessage="เลือกสาเหตุ LV3"
                                                        ValidationExpression="เลือกสาเหตุ LV3" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator4" Width="160" />

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label26" runat="server" Text="Name" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="txtname" runat="server" CssClass="form-control" PlaceHolder="........" />
                                                </div>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="txtname" Font-Size="11"
                                                    ErrorMessage="กรุณาใส่สถานะรายการ"
                                                    ValidationExpression="กรุณาใส่สถานะรายการ"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                    ValidationGroup="Save" Display="None"
                                                    ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                    ControlToValidate="txtname"
                                                    ValidationExpression="^[\s\S]{0,1000}$"
                                                    SetFocusOnError="true" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />

                                            </div>
                                            <div class="form-group">
                                                <asp:Label ID="Label1" runat="server" Text="Code" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="txtcode" runat="server" CssClass="form-control" PlaceHolder="........" />
                                                </div>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="txtcode" Font-Size="11"
                                                    ErrorMessage="กรุณาใส่สถานะรายการ"
                                                    ValidationExpression="กรุณาใส่สถานะรายการ"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                                    ValidationGroup="Save" Display="None"
                                                    ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                    ControlToValidate="txtcode"
                                                    ValidationExpression="^[\s\S]{0,1000}$"
                                                    SetFocusOnError="true" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator2" Width="160" />

                                            </div>
                                            <div class="form-group">
                                                <asp:Label ID="lbl" CssClass="col-sm-3 control-label" runat="server" Text="Status" />
                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="ddStatusadd" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="1" Text="Online" />
                                                        <asp:ListItem Value="0" Text="Offline" />
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-3">
                                                    <asp:LinkButton ID="lbladd" ValidationGroup="Save" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="btnAdd" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="lblcancel" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancel" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr />
                                </div>
                            </asp:Panel>

                            <asp:GridView ID="GvMaster" runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="primary"
                                HeaderStyle-Height="40px"
                                AllowPaging="true"
                                DataKeyNames="CIT4IDX"
                                PageSize="10"
                                OnRowDataBound="Master_RowDataBound"
                                OnRowEditing="Master_RowEditing"
                                OnRowCancelingEdit="Master_RowCancelingEdit"
                                OnPageIndexChanging="Master_PageIndexChanging"
                                OnRowUpdating="Master_RowUpdating">

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>
                                <Columns>

                                    <asp:TemplateField HeaderText="#">

                                        <ItemTemplate>
                                            <asp:Label ID="lblstidx" runat="server" Visible="false" Text='<%# Eval("CIT4IDX") %>' />
                                            <%# (Container.DataItemIndex +1) %>
                                        </ItemTemplate>


                                        <EditItemTemplate>
                                            <div class="form-horizontal" role="form">
                                                <div class="panel-heading">
                                                    <div class="form-group">
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txtCIT4IDX" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("CIT4IDX")%>' />
                                                        </div>
                                                    </div>

                                                        <div class="form-group">
                                                        <asp:Label ID="Label3" runat="server" Text="Case Lv1." CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-8">
                                                             <asp:TextBox ID="txtCIT1IDX" runat="server" Visible="false" CssClass="form-control" Text='<%# Eval("CIT1IDX")%>' />
                                                            <asp:DropDownList ID="ddlITLV1_edit" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server">
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="RqddlEquipmentEditDeviceCode1" ValidationGroup="Save" runat="server" Display="None"
                                                                ControlToValidate="ddlITLV1_edit" Font-Size="11"
                                                                ErrorMessage="กรุณาเลือกหัวข้อ"
                                                                ValidationExpression="กรุณาเลือกหัวข้อ" InitialValue="0" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender16" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqddlEquipmentEditDeviceCode1" Width="160" />

                                                        </div>
                                                    </div>

                                                       <div class="form-group">
                                                        <asp:Label ID="Label5" runat="server" Text="Case Lv2." CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-8">
                                                             <asp:TextBox ID="txtCIT2IDX" runat="server" Visible="false" CssClass="form-control" Text='<%# Eval("CIT2IDX")%>' />
                                                            <asp:DropDownList ID="ddlITLV2_edit" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server">
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="Save" runat="server" Display="None"
                                                                ControlToValidate="ddlITLV2_edit" Font-Size="11"
                                                                ErrorMessage="กรุณาเลือกอาการ"
                                                                ValidationExpression="กรุณาเลือกอาการ" InitialValue="0" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator4" Width="160" />

                                                        </div>
                                                    </div>
                                                     <div class="form-group">
                                                        <asp:Label ID="Label6" runat="server" Text="Case Lv3." CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-8">
                                                             <asp:TextBox ID="txtCIT3IDX" runat="server" Visible="false" CssClass="form-control" Text='<%# Eval("CIT3IDX")%>' />
                                                            <asp:DropDownList ID="ddlITLV3_edit" CssClass="form-control" runat="server">
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ValidationGroup="Save" runat="server" Display="None"
                                                                ControlToValidate="ddlITLV3_edit" Font-Size="11"
                                                                ErrorMessage="กรุณาเลือกสาเหตุ"
                                                                ValidationExpression="กรุณาเลือกสาเหตุ" InitialValue="0" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator5" Width="160" />

                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label26" runat="server" Text="Name" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-8">
                                                            <asp:TextBox ID="txtname_edit" runat="server" CssClass="form-control" Text='<%# Eval("CIT4_Name")%>' />
                                                        </div>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="Save_edit" runat="server" Display="None" ControlToValidate="txtname_edit" Font-Size="11"
                                                            ErrorMessage="กรุณาใส่สถานะรายการ"
                                                            ValidationExpression="กรุณาใส่สถานะรายการ"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                            ValidationGroup="Save_edit" Display="None"
                                                            ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                            ControlToValidate="txtname_edit"
                                                            ValidationExpression="^[\s\S]{0,1000}$"
                                                            SetFocusOnError="true" />

                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />

                                                    </div>


                                                    <div class="form-group">
                                                        <asp:Label ID="Label2" runat="server" Text="Code" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-8">
                                                            <asp:TextBox ID="txtcode_edit" runat="server" CssClass="form-control" Text='<%# Eval("CIT4_Code")%>' />
                                                        </div>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="Save_edit" runat="server" Display="None" ControlToValidate="txtcode_edit" Font-Size="11"
                                                            ErrorMessage="กรุณาใส่สถานะรายการ"
                                                            ValidationExpression="กรุณาใส่สถานะรายการ"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                                                            ValidationGroup="Save_edit" Display="None"
                                                            ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                            ControlToValidate="txtcode_edit"
                                                            ValidationExpression="^[\s\S]{0,1000}$"
                                                            SetFocusOnError="true" />

                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator3" Width="160" />

                                                    </div>


                                                    <div class="form-group">
                                                        <asp:Label ID="lbstatate" CssClass="col-sm-3 control-label" runat="server" Text="Status" />
                                                        <div class="col-sm-8">
                                                            <asp:DropDownList ID="ddStatusUpdate" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("CIT4Status") %>'>
                                                                <asp:ListItem Value="1" Text="Online" />
                                                                <asp:ListItem Value="0" Text="Offline" />
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <div class="col-sm-2 col-sm-offset-10">
                                                            <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server" ValidationGroup="Save_edit" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>


                                        </EditItemTemplate>

                                    </asp:TemplateField>


                                        <asp:TemplateField HeaderText="Code Lv1." ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lbname1" runat="server" Text='<%# Eval("CIT1_Code") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                      <asp:TemplateField HeaderText="Code Lv2." ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lbname2" runat="server" Text='<%# Eval("CIT2_Code") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                     <asp:TemplateField HeaderText="Code Lv3" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lbname3" runat="server" Text='<%# Eval("CIT3_Code") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lbname" runat="server" Text='<%# Eval("CIT4_Name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Code" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lbcode" runat="server" Text='<%# Eval("CIT4_Code") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lbstatus" runat="server" Text='<%# Eval("CIT4StatusDetail") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                            <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDel" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("CIT4IDX") %>'><i class="fa fa-trash-o"></i></asp:LinkButton>
                                        </ItemTemplate>

                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>


                        </div>
                    </div>
                </asp:View>
            </asp:MultiView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

