using System;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_cen_employee")]
public class data_cen_employee
{
    [XmlElement("return_code")]
    public int return_code { get; set; }

    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    [XmlElement("return_idx")]
    public int return_idx { get; set; }

    [XmlElement("employee_mode")]
    public string employee_mode { get; set; }

    [XmlElement("cen_employee_list_u0")]
    public cen_employee_detail_u0[] cen_employee_list_u0 { get; set; }

    [XmlElement("search_cen_employee_list")]
    public search_cen_employee_detail[] search_cen_employee_list { get; set; }
}

[Serializable]
public class cen_employee_detail_u0
{
    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("emp_code")]
    public string emp_code { get; set; }

    [XmlElement("emp_firstname_th")]
    public string emp_firstname_th { get; set; }

    [XmlElement("emp_lastname_th")]
    public string emp_lastname_th { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("emp_firstname_en")]
    public string emp_firstname_en { get; set; }

    [XmlElement("emp_lastname_en")]
    public string emp_lastname_en { get; set; }

    [XmlElement("emp_name_en")]
    public string emp_name_en { get; set; }

    [XmlElement("aff_idx")]
    public int aff_idx { get; set; }

    [XmlElement("aff_name_th")]
    public string aff_name_th { get; set; }

    [XmlElement("aff_name_en")]
    public string aff_name_en { get; set; }

    [XmlElement("jobgrade_idx")]
    public int jobgrade_idx { get; set; }

    [XmlElement("jobgrade_name")]
    public string jobgrade_name { get; set; }

    [XmlElement("joblevel_idx")]
    public int joblevel_idx { get; set; }

    [XmlElement("joblevel_name")]
    public string joblevel_name { get; set; }

    [XmlElement("cost_idx")]
    public int cost_idx { get; set; }

    [XmlElement("cost_no")]
    public string cost_no { get; set; }

    [XmlElement("cost_name")]
    public string cost_name { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }

    [XmlElement("org_name_en")]
    public string org_name_en { get; set; }

    [XmlElement("wg_idx")]
    public int wg_idx { get; set; }

    [XmlElement("wg_name_th")]
    public string wg_name_th { get; set; }

    [XmlElement("wg_name_en")]
    public string wg_name_en { get; set; }

    [XmlElement("lw_idx")]
    public int lw_idx { get; set; }

    [XmlElement("lw_name_th")]
    public string lw_name_th { get; set; }

    [XmlElement("lw_name_en")]
    public string lw_name_en { get; set; }

    [XmlElement("dept_idx")]
    public int dept_idx { get; set; }

    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }

    [XmlElement("dept_name_en")]
    public string dept_name_en { get; set; }

    [XmlElement("sec_idx")]
    public int sec_idx { get; set; }

    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }

    [XmlElement("sec_name_en")]
    public string sec_name_en { get; set; }

    [XmlElement("pos_idx")]
    public int pos_idx { get; set; }

    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }

    [XmlElement("pos_name_en")]
    public string pos_name_en { get; set; }

    [XmlElement("empgroup_idx")]
    public int empgroup_idx { get; set; }

    [XmlElement("empgroup_name_th")]
    public string empgroup_name_th { get; set; }

    [XmlElement("empgroup_name_en")]
    public string empgroup_name_en { get; set; }

    [XmlElement("emp_start_date")]
    public string emp_start_date { get; set; }

    [XmlElement("emp_resign_date")]
    public string emp_resign_date { get; set; }

    [XmlElement("emp_org_mail")]
    public string emp_org_mail { get; set; }
    [XmlElement("emp_personal_mail")]
    public string emp_personal_mail { get; set; }

    [XmlElement("emp_status")]
    public int emp_status { get; set; }

    //-- for PMS --//
    [XmlElement("emp_name_solid")]
    public string emp_name_solid {get;set;}
    [XmlElement("emp_name_dotted")]
    public string emp_name_dotted {get;set;}
    //-- for PMS --//
}

[Serializable]
public class search_cen_employee_detail
{
    [XmlElement("s_emp_idx")]
    public string s_emp_idx { get; set; }
    [XmlElement("s_emp_code")]
    public string s_emp_code { get; set; }
    [XmlElement("s_emp_name")]
    public string s_emp_name { get; set; }
    [XmlElement("s_org_idx")]
    public string s_org_idx { get; set; }
    [XmlElement("s_wg_idx")]
    public string s_wg_idx { get; set; }
    [XmlElement("s_lw_idx")]
    public string s_lw_idx { get; set; }
    [XmlElement("s_dept_idx")]
    public string s_dept_idx { get; set; }
    [XmlElement("s_sec_idx")]
    public string s_sec_idx { get; set; }
    [XmlElement("s_pos_idx")]
    public string s_pos_idx { get; set; }
}