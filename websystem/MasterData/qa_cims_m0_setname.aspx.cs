﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_qa_cims_m0_setname : System.Web.UI.Page
{
    #region Connect
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();

    data_qa_cims _data_qa_cims = new data_qa_cims();


    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlCimsSetSetname = _serviceUrl + ConfigurationManager.AppSettings["urlCimsSetSetname"];
    static string _urlCimsGetSetname = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetSetname"];
    static string _urlCimsDeleteSetname = _serviceUrl + ConfigurationManager.AppSettings["urlCimsDeleteSetname"];

    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    //-- employee --//


    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    bool _flag_qmr = false;
    #endregion Connect

    #region Page_Init
    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
        //check permission
        //foreach (int item in rdept_qmr)
        //{
        //    if (_dataEmployee.employee_list[0].rdept_idx == item)
        //    {
        //        _flag_qmr = true;
        //        break;
        //    }
        //}
    }
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //ViewState["empIDX"] = Session["emp_idx"];
            Select_Setname();


        }
    }
    #endregion Page Load

    #region initPage
    protected void initPage()
    {
        MultiView1.SetActiveView(View1);
        Select_Setname();
    }
    #endregion

    #region getEmployeeProfile
    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }
    #endregion

    #region setformdata
    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    #endregion setformdata

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        switch (cmdName)
        {
            case "cmdAddSetname":

                Gv_select_setname.EditIndex = -1;
                Select_Setname();
                btn_addsetname.Visible = false;
                setFormData(Fv_Insert_Result, FormViewMode.Insert, null);
                Fv_Insert_Result.Visible = true;
                //Gv_select_place.Visible = false;


                break;

            case "Lbtn_submit_setname":
                Insert_Setname();
                Select_Setname();
                btn_addsetname.Visible = true;
                Fv_Insert_Result.Visible = false;
                break;

            case "Lbtn_cancel_setname":
                btn_addsetname.Visible = true;
                Fv_Insert_Result.Visible = false;
                //  setFormData(Fv_Insert_Result, FormViewMode.Insert, null);
                //Gv_select_unit.Visible = false;
                // Gv_select_place.Visible = true;
                break;

            case "btnTodelete_setname":
                // ViewState["DELETE_PLACE"] = int.Parse(cmdArg);

                int a = int.Parse(cmdArg);
                data_qa_cims Setname_de = new data_qa_cims();
                qa_cims_m0_setname_detail Setname_sde = new qa_cims_m0_setname_detail();

                Setname_de.qa_cims_m0_setname_list = new qa_cims_m0_setname_detail[1];

                Setname_sde.setname_idx = int.Parse(a.ToString());

                Setname_de.qa_cims_m0_setname_list[0] = Setname_sde;
                //  test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_de));
                Setname_de = callServicePostMasterQACIMS(_urlCimsDeleteSetname, Setname_de);

                //if (Unit_de.return_code == 0)
                //{

                //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินการสำเร็จ');", true);

                //}

                //else

                //{
                //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินการไม่สำเร็จ');", true);


                //}

                Select_Setname();
                //Gv_select_unit.Visible = false;
                btn_addsetname.Visible = true;
                Fv_Insert_Result.Visible = false;
                break;

            case "cmdview_exa":
                //Gv_selectLabM1.EditIndex = -1;
                //btnAdd.Visible = true;
                int setname_idx = int.Parse(cmdArg);

                ViewState["setname_idx"] = setname_idx;

                //test_place.Text = ViewState["setname_idx"].ToString();
                setActiveView("view_CreateRoot", setname_idx);
                select_RootSetname();
                setFormData(fvform_Insert_Root, FormViewMode.ReadOnly, null);
                //Select_labM1();
                //SETFOCUS.Focus();
                break;

            case "cmdViewSet":
                string[] cmdArgView = cmdArg.Split(',');
                ViewState["setname_idx"] = int.TryParse(cmdArgView[0].ToString(), out _default_int) ? int.Parse(cmdArgView[0].ToString()) : _default_int;
                int conditionView = int.TryParse(cmdArgView[1].ToString(), out _default_int) ? int.Parse(cmdArgView[1].ToString()) : _default_int;

                switch (conditionView)
                {
                    case 0:
                        setActiveView("pageCreateRoot", int.Parse(ViewState["setname_idx"].ToString()));
                        setFormData(fvform_Insert_Root, FormViewMode.ReadOnly, null);
                        select_RootSetname();

                        // litDebug.Text = ViewState["set_idx"].ToString();
                        break;

                    case 1:
                        setActiveView("pageRootSubSet", int.Parse(ViewState["setname_idx"].ToString()));
                        setFormData(fvformInsertSubSet, FormViewMode.ReadOnly, null);
                        select_RootSubSet();
                        break;

                }


                break;

            case "cmdback":
                //ViewState["m0_lab_idx"] = 0;
                btn_addsetname.Visible = true;
                setActiveView("View1", 0);
                setFormData(Fv_Insert_Result, FormViewMode.Insert, null);
                //SETFOCUS.Focus();
                break;

            case "cmdAddSet":
                switch (cmdArg)
                {
                    case "0":
                        setFormData(Fv_Insert_Result, FormViewMode.Insert, null);

                        Fv_Insert_Result.Visible = true;
                        break;

                    case "1":
                        setFormData(fvform_Insert_Root, FormViewMode.Insert, null);

                        fvform_Insert_Root.Visible = true;

                        getM0SetNameList();
                        break;
                    case "2":

                        setFormData(fvformInsertSubSet, FormViewMode.Insert, null);

                        fvformInsertSubSet.Visible = true;

                        getM0RootSetList();

                        break;

                }

                break;

            case "cmdSave":
                
                switch (cmdArg)
                {
                    case "0":
                        TextBox txtset_name = (TextBox)Fv_Insert_Result.FindControl("txtset_name");
                        TextBox txtset_detail = (TextBox)Fv_Insert_Result.FindControl("txtset_detail");
                        DropDownList ddSetname = (DropDownList)Fv_Insert_Result.FindControl("ddSetname");

                        _data_qa_cims.qa_cims_m0_setname_list = new qa_cims_m0_setname_detail[1];
                        qa_cims_m0_setname_detail _m0setname = new qa_cims_m0_setname_detail();
                        _m0setname.cemp_idx = _emp_idx;
                        _m0setname.set_name = txtset_name.Text;
                        _m0setname.setname_status = int.Parse(ddSetname.SelectedValue);
                        _m0setname.set_detail = txtset_detail.Text;

                        _data_qa_cims.qa_cims_m0_setname_list[0] = _m0setname;

                        _data_qa_cims = callServicePostMasterQACIMS(_urlCimsSetSetname, _data_qa_cims);
                        //-- check data duplicate in database
                        if (_data_qa_cims.return_code == 101)
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);

                        }
                        else
                        {
                            txtset_name.Text = String.Empty;
                            txtset_detail.Text = String.Empty;
                        }


                        Select_Setname();
                        setFormData(Fv_Insert_Result, FormViewMode.Insert, null);
                        break;

                    case "1":
                        TextBox tbRootSetName = (TextBox)fvform_Insert_Root.FindControl("tbRootSetName");
                        TextBox tbDetailRootSet = (TextBox)fvform_Insert_Root.FindControl("tbDetailRootSet");
                        DropDownList ddlSetName = (DropDownList)fvform_Insert_Root.FindControl("ddlSetName");
                        DropDownList ddlRootSetStatus = (DropDownList)fvform_Insert_Root.FindControl("ddlRootSetStatus");

                        _data_qa_cims.qa_cims_m0_setname_list = new qa_cims_m0_setname_detail[1];
                        qa_cims_m0_setname_detail _m0root = new qa_cims_m0_setname_detail();
                        _m0root.cemp_idx = _emp_idx;
                        _m0root.set_name = tbRootSetName.Text;
                        //test_place.Text = "ID" + ViewState["setname_idx"].ToString();
                        _m0root.setroot_idx = int.Parse(ViewState["setname_idx"].ToString());
                        _m0root.setname_status = int.Parse(ddlRootSetStatus.SelectedValue);
                        _m0root.set_detail = tbDetailRootSet.Text;

                        _data_qa_cims.qa_cims_m0_setname_list[0] = _m0root;

                        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_m0root));
                        _data_qa_cims = callServicePostMasterQACIMS(_urlCimsSetSetname, _data_qa_cims);
                        //-- check data duplicate in database
                        if (_data_qa_cims.return_code == 101)
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);

                        }
                        else
                        {
                            setFormData(fvform_Insert_Root, FormViewMode.Insert, null);
                            getM0SetNameList();
                            //tbRootSetName.Text = String.Empty;
                            //tbDetailRootSet.Text = String.Empty;
                        }

                        select_RootSetname();

                        break;

                    case "2":
                        TextBox tbSubSetName = (TextBox)fvformInsertSubSet.FindControl("tbSubSetName");
                        TextBox tbDetailSubSet = (TextBox)fvformInsertSubSet.FindControl("tbDetailSubSet");
                        DropDownList ddlSubSetStatus = (DropDownList)fvformInsertSubSet.FindControl("ddlSubSetStatus");

                        _data_qa_cims.qa_cims_m0_setname_list = new qa_cims_m0_setname_detail[1];
                        qa_cims_m0_setname_detail _m0Subset = new qa_cims_m0_setname_detail();
                        _m0Subset.cemp_idx = _emp_idx;
                        _m0Subset.set_name = tbSubSetName.Text;
                        _m0Subset.setroot_idx = int.Parse(ViewState["setname_idx"].ToString());
                        _m0Subset.setname_status = int.Parse(ddlSubSetStatus.SelectedValue);
                        _m0Subset.set_detail = tbDetailSubSet.Text;

                        _data_qa_cims.qa_cims_m0_setname_list[0] = _m0Subset;
                        _data_qa_cims = callServicePostMasterQACIMS(_urlCimsSetSetname, _data_qa_cims);
                        //-- check data duplicate in database
                        if (_data_qa_cims.return_code == 101)
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);

                        }
                        else
                        {
                            setFormData(fvformInsertSubSet, FormViewMode.Insert, null);
                            getM0RootSetList();
                            //tbDetailSubSet.Text = String.Empty;
                            //tbSubSetName.Text = String.Empty;
                        }

                        select_RootSubSet();
                        break;
                }

                break;

            case "cmdCancel":
                switch (cmdArg)
                {
                    case "0":
                        setActiveView("View1", 0);
                        setFormData(Fv_Insert_Result, FormViewMode.ReadOnly, null);
                        //SETFOCUS.Focus();
                        break;

                    case "1":
                        setActiveView("view_CreateRoot", 0);
                        //test_place.Text = ViewState["ROOTIDX"].ToString();
                        if ((ViewState["ROOTIDX"].ToString()) != null)
                        {
                            ViewState["setname_idx"] = ViewState["ROOTIDX"].ToString();
                            getM0SetNameList();
                            setFormData(fvform_Insert_Root, FormViewMode.ReadOnly, null);
                        }
                        else
                        {
                            ViewState["setname_idx"] = 0;
                            setFormData(fvform_Insert_Root, FormViewMode.Insert, null);
                        }

                        //SETFOCUS.Focus();
                        break;

                    case "2":
                        setActiveView("pageRootSubSet", 0);
                        setFormData(fvformInsertSubSet, FormViewMode.ReadOnly, null);
                        //SETFOCUS.Focus();
                        break;
                }

                break;

            case "cmdDeleteSet":

                string[] cmdArgDelete = cmdArg.Split(',');
                int set_idx = int.TryParse(cmdArgDelete[0].ToString(), out _default_int) ? int.Parse(cmdArgDelete[0].ToString()) : _default_int;
                int condition = int.TryParse(cmdArgDelete[1].ToString(), out _default_int) ? int.Parse(cmdArgDelete[1].ToString()) : _default_int;

                _data_qa_cims.qa_cims_m0_setname_list = new qa_cims_m0_setname_detail[1];
                qa_cims_m0_setname_detail _delset = new qa_cims_m0_setname_detail();
                _delset.setname_idx = set_idx;
                _data_qa_cims.qa_cims_m0_setname_list[0] = _delset;

                //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_delset));

                _data_qa_cims = callServicePostMasterQACIMS(_urlCimsDeleteSetname, _data_qa_cims);
                if (_data_qa_cims.return_code == 0)
                {
                    //test_place.Text = "success";
                }
                else
                {
                    //test_place.Text = "not success";
                }

                switch (condition)
                {
                    // select set name level 1 (DELETE)
                    case 0:
                        Select_Setname();
                        break;
                    // select set name level 2 (DELETE)
                    case 1:
                        select_RootSetname();
                        break;
                    // select set name level 3 (DELETE)
                    case 2:
                        select_RootSubSet();
                        break;
                }
                break;
        }
    }
    #endregion btnCommand

    #region Custom Functions
    protected void Insert_Setname()
    {
        FormView MvDocDetail_insert_qa_cims = (FormView)View1.FindControl("Fv_Insert_Result");
        TextBox tex_set_name = (TextBox)MvDocDetail_insert_qa_cims.FindControl("txtset_name");
        TextBox tex_set_detail = (TextBox)MvDocDetail_insert_qa_cims.FindControl("txtset_detail");
        DropDownList dropD_status_setname = (DropDownList)MvDocDetail_insert_qa_cims.FindControl("ddSetname");

        data_qa_cims Setname_b = new data_qa_cims();
        qa_cims_m0_setname_detail Setname_s = new qa_cims_m0_setname_detail();
        Setname_b.qa_cims_m0_setname_list = new qa_cims_m0_setname_detail[1];
        Setname_s.set_name = tex_set_name.Text;
        Setname_s.set_detail = tex_set_detail.Text;
        Setname_s.cemp_idx = _emp_idx;
        Setname_s.setname_status = int.Parse(dropD_status_setname.SelectedValue);
        Setname_b.qa_cims_m0_setname_list[0] = Setname_s;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        Setname_b = callServicePostMasterQACIMS(_urlCimsSetSetname, Setname_b);

        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Unit_b.return_code));
        if (Setname_b.return_code == 101)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);

        }
        else
        {
            tex_set_name.Text = String.Empty;
            tex_set_detail.Text = String.Empty;
        }
    }

    protected void Select_Setname()
    {
        _data_qa_cims.qa_cims_m0_setname_list = new qa_cims_m0_setname_detail[1];
        qa_cims_m0_setname_detail _selectset = new qa_cims_m0_setname_detail();
        _data_qa_cims.qa_cims_m0_setname_list[0] = _selectset;
        _data_qa_cims = callServicePostMasterQACIMS(_urlCimsGetSetname, _data_qa_cims);
        var _linqSet = from data_qa_cims in _data_qa_cims.qa_cims_m0_setname_list
                       where data_qa_cims.setroot_idx == 0
                       select data_qa_cims;
        setGridData(Gv_select_setname, _linqSet.ToList());
        
    }

    protected void select_RootSetname()
    {
        _data_qa_cims.qa_cims_m0_setname_list = new qa_cims_m0_setname_detail[1];
        qa_cims_m0_setname_detail _selectset = new qa_cims_m0_setname_detail();
        _data_qa_cims.qa_cims_m0_setname_list[0] = _selectset;
        _data_qa_cims = callServicePostMasterQACIMS(_urlCimsGetSetname, _data_qa_cims);
        var _linqRootSet = from data_qa_cims in _data_qa_cims.qa_cims_m0_setname_list
                           where data_qa_cims.setroot_idx == int.Parse(ViewState["setname_idx"].ToString())
                           select data_qa_cims;
        setGridData(gvMasterRoot, _linqRootSet.ToList());

    }

    protected void select_RootSubSet()
    {
        _data_qa_cims.qa_cims_m0_setname_list = new qa_cims_m0_setname_detail[1];
        qa_cims_m0_setname_detail _selectset = new qa_cims_m0_setname_detail();
        _data_qa_cims.qa_cims_m0_setname_list[0] = _selectset;
        _data_qa_cims = callServicePostMasterQACIMS(_urlCimsGetSetname, _data_qa_cims);
        var _linqSubSet = from data_qa_cims in _data_qa_cims.qa_cims_m0_setname_list
                          where data_qa_cims.setroot_idx == int.Parse(ViewState["setname_idx"].ToString())
                          select data_qa_cims;
        setGridData(gvMasterSubSet, _linqSubSet.ToList());

    }
    #endregion Custom Functions

    #region setGridData
    private void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region Masterbound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "Gv_select_setname":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbsetname_status = (Label)e.Row.Cells[3].FindControl("lbsetname_status");
                    Label setname_statusOnline = (Label)e.Row.Cells[3].FindControl("setname_statusOnline");
                    Label setname_statusOffline = (Label)e.Row.Cells[3].FindControl("setname_statusOffline");

                    ViewState["_setname_status"] = lbsetname_status.Text;


                    if (ViewState["_setname_status"].ToString() == "1")
                    {
                        setname_statusOnline.Visible = true;
                    }
                    else if (ViewState["_setname_status"].ToString() == "0")
                    {
                        setname_statusOffline.Visible = true;
                    }

                }


                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                    //btn_addplace.Visible = true;
                    //setFormData(Fv_Insert_Result, FormViewMode.ReadOnly, null);

                    //MultiView1.Visible = true;
                    //Gv_select_unit.Visible = false;
                    btn_addsetname.Visible = true;
                    Fv_Insert_Result.Visible = false;

                }

                break;

            case "gvMasterRoot":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbRootStatus = (Label)e.Row.Cells[4].FindControl("lbRootStatus");
                    Label status_online = (Label)e.Row.Cells[4].FindControl("status_online");
                    Label status_offline = (Label)e.Row.Cells[4].FindControl("status_offline");
                    TextBox tbSetRootIDX = (TextBox)e.Row.Cells[1].FindControl("tbSetRootIDX");

                    //   getM0RootNameSetList(lblRootLevelSet);
                    ViewState["ROOTIDX"] = tbSetRootIDX.Text;

                    ViewState["status_rootidx"] = lbRootStatus.Text;

                    if (ViewState["status_rootidx"].ToString() == "0")
                    {
                        status_offline.Visible = true;
                    }
                    else if (ViewState["status_rootidx"].ToString() == "1")
                    {
                        status_online.Visible = true;
                    }


                }
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    btnAddRoot.Visible = true;
                    fvform_Insert_Root.Visible = false;
                }

                break;

            case "gvMasterSubSet":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbRootSubStatus = (Label)e.Row.Cells[4].FindControl("lbRootSubStatus");
                    Label status_online = (Label)e.Row.Cells[4].FindControl("status_online");
                    Label status_offline = (Label)e.Row.Cells[4].FindControl("status_offline");

                    ViewState["status_subset"] = lbRootSubStatus.Text;

                    if (ViewState["status_subset"].ToString() == "0")
                    {
                        status_offline.Visible = true;
                    }
                    else if (ViewState["status_subset"].ToString() == "1")
                    {
                        status_online.Visible = true;
                    }


                }
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    lnkbtnRootSet.Visible = true;
                    fvformInsertSubSet.Visible = false;
                }

                break;


        }
    }
    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "Gv_select_setname":
                Gv_select_setname.EditIndex = e.NewEditIndex;
                Select_Setname();
                break;

            case "gvMasterRoot":
                gvMasterRoot.EditIndex = e.NewEditIndex;
                select_RootSetname();
                break;

            case "gvMasterSubSet":
                gvMasterSubSet.EditIndex = e.NewEditIndex;
                select_RootSubSet();
                break;

        }
    }
    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

        var GvName = (GridView)sender;
        //Label5.Text = GvName.ID;
        switch (GvName.ID)
        {
            case "Gv_select_setname":

                var setname_idx = (TextBox)Gv_select_setname.Rows[e.RowIndex].FindControl("ID_setname");
                var set_name = (TextBox)Gv_select_setname.Rows[e.RowIndex].FindControl("Name_setname");
                var set_detail = (TextBox)Gv_select_setname.Rows[e.RowIndex].FindControl("Detail_setname");
                var setname_status = (DropDownList)Gv_select_setname.Rows[e.RowIndex].FindControl("ddEdit_setname");


                Gv_select_setname.EditIndex = -1;

                data_qa_cims setname_Update = new data_qa_cims();
                setname_Update.qa_cims_m0_setname_list = new qa_cims_m0_setname_detail[1];
                qa_cims_m0_setname_detail setname_s = new qa_cims_m0_setname_detail();
                setname_s.setname_idx = int.Parse(setname_idx.Text);
                setname_s.set_name = set_name.Text;
                setname_s.set_detail = set_detail.Text;
                setname_s.setname_status = int.Parse(setname_status.SelectedValue);

                setname_Update.qa_cims_m0_setname_list[0] = setname_s;

                //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(unit_Update));

                setname_Update = callServicePostMasterQACIMS(_urlCimsSetSetname, setname_Update);

                Select_Setname();

                break;

            case "gvMasterRoot":

                var _updateRootSetidx = (TextBox)gvMasterRoot.Rows[e.RowIndex].FindControl("_updateRootSetidx");
                var _updateRootSetName = (TextBox)gvMasterRoot.Rows[e.RowIndex].FindControl("tbupdateRootSetName");
                var _updateRootSetDetail = (TextBox)gvMasterRoot.Rows[e.RowIndex].FindControl("tbupdateRootSetDetail");
                var _updateRootSetLevel1 = (DropDownList)gvMasterRoot.Rows[e.RowIndex].FindControl("ddlRootSetLevel1");
                var _updateRootSetStatus = (DropDownList)gvMasterRoot.Rows[e.RowIndex].FindControl("ddlupdateRootSetStatus");

                if (_updateRootSetName.Text != "" && _updateRootSetDetail.Text != "")
                {
                    gvMasterRoot.EditIndex = -1;

                    _data_qa_cims.qa_cims_m0_setname_list = new qa_cims_m0_setname_detail[1];
                    qa_cims_m0_setname_detail _upRootset = new qa_cims_m0_setname_detail();
                    _upRootset.setname_idx = int.Parse(_updateRootSetidx.Text);
                    _upRootset.set_name = _updateRootSetName.Text;
                    _upRootset.set_detail = _updateRootSetDetail.Text;
                    _upRootset.setname_status = int.Parse(_updateRootSetStatus.SelectedValue);
                    _data_qa_cims.qa_cims_m0_setname_list[0] = _upRootset;

                    //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_upRootset));

                    _data_qa_cims = callServicePostMasterQACIMS(_urlCimsSetSetname, _data_qa_cims);
                    if (_data_qa_cims.return_code == 102)
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถแก้ไขข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);
                    }

                    else
                    {

                    }
                    select_RootSetname();

                    //SETFOCUS.Focus();
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณากรอกข้อมูลให้ครบทุกช่อง !');", true);
                }


                break;

            case "gvMasterSubSet":

                var _updateSubSetidx = (TextBox)gvMasterSubSet.Rows[e.RowIndex].FindControl("_updateSubSetidx");
                var _updateSubSetName = (TextBox)gvMasterSubSet.Rows[e.RowIndex].FindControl("tbupdateSubSetName");
                var _updateSubSetDetail = (TextBox)gvMasterSubSet.Rows[e.RowIndex].FindControl("tbupdateSubSetDetail");
                var _updateSubSetStatus = (DropDownList)gvMasterSubSet.Rows[e.RowIndex].FindControl("ddlupdateSubSetStatus");

                if (_updateSubSetName.Text != "" && _updateSubSetDetail.Text != "")
                {
                    gvMasterSubSet.EditIndex = -1;

                    _data_qa_cims.qa_cims_m0_setname_list = new qa_cims_m0_setname_detail[1];
                    qa_cims_m0_setname_detail _upSubset = new qa_cims_m0_setname_detail();
                    _upSubset.setname_idx = int.Parse(_updateSubSetidx.Text);
                    _upSubset.set_name = _updateSubSetName.Text;
                    _upSubset.set_detail = _updateSubSetDetail.Text;
                    _upSubset.setname_status = int.Parse(_updateSubSetStatus.SelectedValue);
                    _data_qa_cims.qa_cims_m0_setname_list[0] = _upSubset;

                    _data_qa_cims = callServicePostMasterQACIMS(_urlCimsSetSetname, _data_qa_cims);
                    if (_data_qa_cims.return_code == 102)
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถแก้ไขข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);
                    }
                    else
                    {

                    }
                    select_RootSubSet();
                    //SETFOCUS.Focus();
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณากรอกข้อมูลให้ครบทุกช่อง !');", true);
                }


                break;

        }
    }
    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        //Label5.Text = GvName.ID;
        switch (GvName.ID)
        {
            case "Gv_select_setname":
                Gv_select_setname.EditIndex = -1;
                Select_Setname();
                //btn_addplace.Visible = true;
                //SETFOCUS_ONTOP.Focus();
                //Gv_select_unit.Visible = false;
                btn_addsetname.Visible = true;
                Fv_Insert_Result.Visible = false;
                break;

            case "gvMasterRoot":
                gvMasterRoot.EditIndex = -1;
                select_RootSetname();
                btnAddRoot.Visible = true;
                fvform_Insert_Root.Visible = false;
                //SETFOCUS.Focus();
                break;

            case "gvMasterSubSet":
                gvMasterSubSet.EditIndex = -1;
                select_RootSubSet();
                lnkbtnRootSet.Visible = true;
                fvformInsertSubSet.Visible = false;
                //SETFOCUS.Focus();
                break;

        }
    }
    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "Gv_select_setname":
                Gv_select_setname.PageIndex = e.NewPageIndex;
                Gv_select_setname.DataBind();
                Select_Setname();
                break;

            case "gvMasterRoot":
                gvMasterRoot.PageIndex = e.NewPageIndex;
                gvMasterRoot.DataBind();
                select_RootSetname();
                break;

            case "gvMasterSubSet":
                gvMasterSubSet.PageIndex = e.NewPageIndex;
                gvMasterSubSet.DataBind();
                select_RootSubSet();
                break;

        }
    }
    #endregion

    #region callService 
    protected data_qa_cims callServicePostMasterQACIMS(string _cmdUrl, data_qa_cims _data_qa_cims)
    {
        _localJson = _funcTool.convertObjectToJson(_data_qa_cims);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_qa_cims = (data_qa_cims)_funcTool.convertJsonToObject(typeof(data_qa_cims), _localJson);


        return _data_qa_cims;
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }
    #endregion callService Functions

    #region setActiveView

    protected void setActiveView(string activeTab, int uidx)
    {
        MultiView1.SetActiveView((View)MultiView1.FindControl(activeTab));
        Fv_Insert_Result.Visible = false;
        fvform_Insert_Root.Visible = false;

        switch (activeTab)
        {
            case "View1":

                break;

            case "view_CreateRoot":

                getM0SetNameList();
                break;

            case "pageRootSubSet":

                getM0RootSetList();

                break;
        }
    }

    #endregion

    #region dropdown list
    protected void getM0RootSetList()
    {
        setFormData(fvformInsertSubSet, FormViewMode.Insert, null);
        TextBox txtSubSetName = (TextBox)fvformInsertSubSet.FindControl("txtSubSetName");

        data_qa_cims _dataqacims = new data_qa_cims();
        _dataqacims.qa_cims_m0_setname_list = new qa_cims_m0_setname_detail[1];
        qa_cims_m0_setname_detail _m0SubSet = new qa_cims_m0_setname_detail();
        _dataqacims.qa_cims_m0_setname_list[0] = _m0SubSet;
        _dataqacims = callServicePostMasterQACIMS(_urlCimsGetSetname, _dataqacims);
        var _linqSet = (from data_qa_cims in _dataqacims.qa_cims_m0_setname_list
                        where data_qa_cims.setname_idx == int.Parse(ViewState["setname_idx"].ToString())
                        select new
                        {
                            data_qa_cims.set_name
                        }).ToList();
        foreach (var item in _linqSet)
        {
            txtSubSetName.Text = item.set_name;

        }
    }

    protected void getM0SetNameList()
    {
        setFormData(fvform_Insert_Root, FormViewMode.Insert, null);
        TextBox tbSetNameList = (TextBox)fvform_Insert_Root.FindControl("tbSetNameList");

        data_qa_cims _dataqacims = new data_qa_cims();
        _dataqacims.qa_cims_m0_setname_list = new qa_cims_m0_setname_detail[1];
        qa_cims_m0_setname_detail _m0SubSet = new qa_cims_m0_setname_detail();
        _dataqacims.qa_cims_m0_setname_list[0] = _m0SubSet;
        _dataqacims = callServicePostMasterQACIMS(_urlCimsGetSetname, _dataqacims);
        var _linqSet = (from data_qa_cims in _dataqacims.qa_cims_m0_setname_list
                        where data_qa_cims.setname_idx == int.Parse(ViewState["setname_idx"].ToString())
                        select new
                        {
                            data_qa_cims.set_name
                        }).ToList();
        foreach (var item in _linqSet)
        {
            tbSetNameList.Text = item.set_name;

        }

    }
    #endregion
}