using System;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_lunch")]
public class data_lunch
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }
    [XmlElement("return_idx")]
    public int return_idx { get; set; }

    [XmlElement("lunch_meal_list")]
    public lunch_meal_detail[] lunch_meal_list { get; set; }
    [XmlElement("search_meal_list")]
    public search_meal_detail[] search_meal_list { get; set; }

    [XmlElement("lunch_meal_template_list")]
    public lunch_meal_template_detail[] lunch_meal_template_list { get; set; }
}

[Serializable]
public class lunch_meal_detail
{
    [XmlElement("u0_idx")]
    public int u0_idx { get; set; }
    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }
    [XmlElement("emp_code")]
    public string emp_code { get; set; }
    [XmlElement("res_idx")]
    public int res_idx { get; set; }
    [XmlElement("interval_min")]
    public int interval_min { get; set; }
    [XmlElement("plant_idx")]
    public int plant_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }
    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }
    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }
    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }

    [XmlElement("count_amount")]
    public int count_amount { get; set; }
}

[Serializable]
public class search_meal_detail
{
    [XmlElement("s_emp_idx")]
    public string s_emp_idx { get; set; }
    [XmlElement("s_emp_code")]
    public string s_emp_code { get; set; }
    [XmlElement("s_res_idx")]
    public string s_res_idx { get; set; }
    [XmlElement("s_start_date")]
    public string s_start_date { get; set; }
    [XmlElement("s_end_date")]
    public string s_end_date { get; set; }
    [XmlElement("s_report_type")]
    public string s_report_type { get; set; }
    [XmlElement("s_plant_idx")]
    public string s_plant_idx { get; set; }
}

[Serializable]
public class lunch_meal_template_detail
{
    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }
    [XmlElement("emp_code")]
    public string emp_code { get; set; }
    [XmlElement("template_index")]
    public string template_index { get; set; }
    [XmlElement("plant_idx")]
    public int plant_idx { get; set; }
    [XmlElement("fid")]
    public int fid { get; set; }
}