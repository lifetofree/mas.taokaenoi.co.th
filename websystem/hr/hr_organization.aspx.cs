using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_hr_hr_organization : System.Web.UI.Page
{
    #region initial function/data
    function_tool _funcTool = new function_tool();
    data_employee _dataEmployee = new data_employee();
    service_execute _serviceExecute = new service_execute();

    string _localXml = String.Empty;
    string _localString = String.Empty;
    string _localJson = String.Empty;
    int _actionType = 0;
    int _emp_idx = 0;
    int _default_int = 0;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlSetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlSetOrganizationList"];
    #endregion initial function/data

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            initPage();
        }
    }

    #region event command
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        switch(cmdName)
        {
            case "cmdCreate":
                setFormData(fvOrganization, FormViewMode.Insert, null);
                setActiveView("viewForm", int.Parse(cmdArg));
                break;
            case "cmdEdit":
                _dataEmployee.organization_list = new organization_details[1];
                organization_details _orgEdit = new organization_details();
                _orgEdit.org_idx = int.Parse(cmdArg);

                _dataEmployee.organization_list[0] = _orgEdit;

                //--debug
                // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));

                _dataEmployee = callServicePostEmployee(_urlGetOrganizationList, _dataEmployee);
                setFormData(fvOrganization, FormViewMode.Edit, _dataEmployee.organization_list);
                setActiveView("viewForm", 0);
                break;
            case "cmdDelete":
                _dataEmployee.organization_list = new organization_details[1];
                organization_details _orgDelete = new organization_details();
                _orgDelete.org_idx = int.Parse(cmdArg);
                
                _orgDelete.org_status = 9;

                _dataEmployee.organization_list[0] = _orgDelete;

                //--debug
                // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));

                _dataEmployee = callServicePostEmployee(_urlSetOrganizationList, _dataEmployee);

                setGridData(gvOrgList, _dataEmployee.organization_list);
                setActiveView("viewList", 0);
                break;
            case "cmdSave":
                _dataEmployee.organization_list = new organization_details[1];
                organization_details _orgDetail = new organization_details();
                _orgDetail.org_idx = int.Parse(cmdArg);
                _orgDetail.org_name_th = ((TextBox)fvOrganization.FindControl("tbOrgNameTH")).Text.Trim();
                _orgDetail.org_name_en = ((TextBox)fvOrganization.FindControl("tbOrgNameEN")).Text.Trim();
                
                _orgDetail.org_status = 1;

                _dataEmployee.organization_list[0] = _orgDetail;

                //--debug
                // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));

                _dataEmployee = callServicePostEmployee(_urlSetOrganizationList, _dataEmployee);

                if(_dataEmployee.return_code == "0")
                {
                    setGridData(gvOrgList, _dataEmployee.organization_list);
                    setActiveView("viewList", 0);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('error " +  _dataEmployee.return_code + " : " + _dataEmployee.return_msg + "');", true);
                }
                break;
            case "cmdCancel":
                setFormData(fvOrganization, FormViewMode.Insert, null);
                setActiveView("viewList", 0);
                break;
            case "cmdReset":
                setFormData(fvOrganization, FormViewMode.Insert, null);
                break;
        }
    }
    #endregion event command

    #region reuse
    protected void initPage()
    {
        clearSession();
        clearViewState();

        setActiveView("viewList", 0);
    }

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        // ViewState["checkTemp"] = null;
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _data_employee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_employee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_employee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _data_employee;
    }

    protected void setActiveView(string activeTab, int uidx)
    {
        mvSystem.SetActiveView((View)mvSystem.FindControl(activeTab));

        switch (activeTab)
        {
            case "viewList":
                _dataEmployee.organization_list = new organization_details[1];
                organization_details _orgList = new organization_details();
                _orgList.org_idx = 0;
                _dataEmployee.organization_list[0] = _orgList;

                _dataEmployee = callServicePostEmployee(_urlGetOrganizationList, _dataEmployee);

                gvOrgList.DataSource = _dataEmployee.organization_list;
                gvOrgList.DataBind();
                break;
            default:
                break;
        }
    }
    #endregion reuse
}