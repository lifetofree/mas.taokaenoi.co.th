using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

public partial class websystem_hr_pms_reports : System.Web.UI.Page {
    #region initial function/data
    function_tool _funcTool = new function_tool ();

    data_cen_master _data_cen_master = new data_cen_master ();
    data_cen_employee _data_cen_employee = new data_cen_employee ();
    data_pms _data_pms = new data_pms ();

    int _emp_idx = 0;
    int _default_int = 0;
    int _serviceType = 1;
    bool _b_permission = false;
    string _local_xml = String.Empty;
    string _local_json = String.Empty;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlCenGetCenMasterList = _serviceUrl + ConfigurationManager.AppSettings["urlCenGetCenMasterList"];
    static string _urlGetCenEmployeeList = _serviceUrl + ConfigurationManager.AppSettings["urlGetCenEmployeeList"];
    static string _urlPmsGetPmsManageData = _serviceUrl + ConfigurationManager.AppSettings["urlPmsGetPmsManageData"];
    static string _urlPmsSetPmsManageData = _serviceUrl + ConfigurationManager.AppSettings["urlPmsSetPmsManageData"];

    static int[] _permission = { 172, 24047, 1413 };

    decimal dEmpSum = 0;
    decimal dEmpSumOnProcess = 0;
    decimal dEmpSumOnProcessPercent = 0;
    decimal dEmpSumNotStart = 0;
    decimal dEmpSumNotStartPercent = 0;
    decimal dEmpSumFinished = 0;
    decimal dEmpSumFinishedPercent = 0;
    decimal dEmpSumSolid = 0;
    decimal dEmpSumSolidPercent = 0;
    decimal dEmpSumDotted = 0;
    decimal dEmpSumDottedPercent = 0;
    decimal dEmpSumApprove1 = 0;
    decimal dEmpSumApprove1Percent = 0;
    decimal dEmpSumApprove2 = 0;
    decimal dEmpSumApprove2Percent = 0;
    decimal dEmpSumApprove3 = 0;
    decimal dEmpSumApprove3Percent = 0;
    #endregion initail function/data

    protected void Page_Init (object sender, EventArgs e) {
        _emp_idx = _funcTool.convertToInt (Session["emp_idx"].ToString ());

        // check permission
        // foreach (int _pass in _permission) {
        //     if (_emp_idx == _pass) {
        //         _b_permission = true;
        //         continue;
        //     }
        // }

        // if (!_b_permission) {
        //     Response.Redirect (ResolveUrl ("~/"));
        // }
    }

    protected void Page_Load (object sender, EventArgs e) {
        if (!IsPostBack) {
            initPage ();
        }

        setTrigger ();
    }

    #region event command
    protected void navCommand (object sender, CommandEventArgs e) {
        string cmdName = e.CommandName.ToString ();
        string cmdArg = e.CommandArgument.ToString ();

        switch (cmdName) {
            case "navList":
                clearViewState ();
                setActiveTab ("viewList", 0);
                break;
                //     case "navImport":
                //         clearViewState ();
                //         setActiveTab ("viewImport", 0);
                //         break;
                //     case "navReport":
                //         clearViewState ();
                //         setActiveTab ("viewReport", 0);
                //         break;
        }
    }

    protected void btnCommand (object sender, CommandEventArgs e) {
        string cmdName = e.CommandName.ToString ();
        string cmdArg = e.CommandArgument.ToString ();

        switch (cmdName) {
            case "cmdSearch":
                _data_pms = getReportList ("2");
                _funcTool.setGvData (gvReportList, _data_pms.pms_report_list);
                // display progress
                if (ViewState["ReportSearchList"] != null) {
                    litChartOverall.Text = getChartOverall ();
                    litChartOnProgress.Text = getChartOnProgress ();
                }
                break;
            case "cmdReset":
                _funcTool.setFvData (fvSearch, FormViewMode.Insert, null);
                clearViewState ();
                setActiveTab ("viewList", 0);
                break;
            case "cmdExport":
                if (ViewState["ReportSearchList"] == null) {
                    ScriptManager.RegisterClientScriptBlock (this.Page, this.Page.GetType (), "alert", "alert('ไม่พบข้อมูลที่ต้องการ Export');", true);
                    return;
                }
                
                _data_pms = (data_pms) ViewState["ReportSearchList"];
                pms_report_detail[] _reportData = (pms_report_detail[]) _data_pms.pms_report_list;
                if(_reportData == null) {
                    ScriptManager.RegisterClientScriptBlock (this.Page, this.Page.GetType (), "alert", "alert('ไม่พบข้อมูลที่ต้องการ Export');", true);
                    return;
                }

                DataTable tableReport = new DataTable ();
                int _count_row = 0;

                tableReport.Columns.Add ("#", typeof (String));
                tableReport.Columns.Add ("แผนก", typeof (String));
                tableReport.Columns.Add ("ฝ่าย", typeof (String));
                tableReport.Columns.Add ("สายงาน", typeof (String));
                tableReport.Columns.Add ("กลุ่มงาน", typeof (String));
                tableReport.Columns.Add ("จำนวนพนักงานทั้งหมด", typeof (String));
                tableReport.Columns.Add ("อยู่ในระหว่างดำเนินการ", typeof (String));
                tableReport.Columns.Add ("อยู่ในระหว่างดำเนินการ %", typeof (String));
                tableReport.Columns.Add ("ยังไม่เริ่มดำเนินการ", typeof (String));
                tableReport.Columns.Add ("ยังไม่เริ่มดำเนินการ %", typeof (String));
                tableReport.Columns.Add ("ดำเนินการแล้วเสร็จ", typeof (String));
                tableReport.Columns.Add ("ดำเนินการแล้วเสร็จ %", typeof (String));
                tableReport.Columns.Add ("ประเมินตัวเองเสร็จแล้ว", typeof (String));
                tableReport.Columns.Add ("ประเมินตัวเองเสร็จแล้ว %", typeof (String));
                tableReport.Columns.Add ("รอหัวหน้าประเมิน (Solid)", typeof (String));
                tableReport.Columns.Add ("รอหัวหน้าประเมิน (Solid) %", typeof (String));
                tableReport.Columns.Add ("รอหัวหน้าประเมิน (Dotted)", typeof (String));
                tableReport.Columns.Add ("รอหัวหน้าประเมิน (Dotted) %", typeof (String));
                tableReport.Columns.Add ("รอผู้อนุมัติการประเมิน (ลำดับ 1)", typeof (String));
                tableReport.Columns.Add ("รอผู้อนุมัติการประเมิน (ลำดับ 1) %", typeof (String));
                tableReport.Columns.Add ("รอผู้อนุมัติการประเมิน (ลำดับ 2)", typeof (String));
                tableReport.Columns.Add ("รอผู้อนุมัติการประเมิน (ลำดับ 2) %", typeof (String));
                tableReport.Columns.Add ("รอผู้อนุมัติการประเมิน (ลำดับ 3)", typeof (String));
                tableReport.Columns.Add ("รอผู้อนุมัติการประเมิน (ลำดับ 3) %", typeof (String));

                foreach (var temp_row in _reportData) {
                    DataRow add_row = tableReport.NewRow ();

                    int _empCount = temp_row.emp_count;
                    int _empOnProcessCount = (temp_row.emp_count - (temp_row.emp_count_not_start + temp_row.emp_count_finished));

                    add_row[0] = (_count_row + 1).ToString ();
                    add_row[1] = temp_row.sec_name_th.ToString (); // section
                    add_row[2] = temp_row.dept_name_th.ToString (); //department
                    add_row[3] = temp_row.lw_name_th.ToString (); // linework
                    add_row[4] = temp_row.wg_name_th.ToString (); // workgroup
                    add_row[5] = temp_row.emp_count.ToString (); // emp_count
                    add_row[6] = (temp_row.emp_count - (temp_row.emp_count_not_start + temp_row.emp_count_finished)).ToString (); // emp_count on process
                    add_row[7] = _empCount == 0 ? "0%" : (((temp_row.emp_count - (temp_row.emp_count_not_start + temp_row.emp_count_finished)) * 100) / temp_row.emp_count).ToString () + "%"; // emp__count on process percent
                    add_row[8] = temp_row.emp_count_not_start.ToString (); // emp_count_not_start
                    add_row[9] = _empCount == 0 ? "0%" : ((temp_row.emp_count_not_start * 100) / temp_row.emp_count).ToString () + "%"; // emp_count_not_start percent
                    add_row[10] = temp_row.emp_count_finished.ToString (); // emp_count_finished
                    add_row[11] = _empCount == 0 ? "0%" : ((temp_row.emp_count_finished * 100) / temp_row.emp_count).ToString () + "%"; // emp_count_finished percent
                    add_row[12] = temp_row.emp_count_myself.ToString (); // emp_count_myself
                    add_row[13] = _empCount == 0 ? "0%" : ((temp_row.emp_count_myself * 100) / temp_row.emp_count).ToString () + "%"; // emp_count_myself percent

                    add_row[14] = temp_row.emp_count_solid.ToString (); // emp_count_solid
                    add_row[15] = _empOnProcessCount == 0 ? "0%" : ((temp_row.emp_count_solid * 100) / _empOnProcessCount).ToString () + "%"; // emp_count_solid percent
                    add_row[16] = temp_row.emp_count_dotted.ToString (); // emp_count_dotted
                    add_row[17] = _empOnProcessCount == 0 ? "0%" : ((temp_row.emp_count_dotted * 100) / _empOnProcessCount).ToString () + "%"; // emp_count_dotted percent
                    add_row[18] = temp_row.emp_count_approve1.ToString (); // emp_count_approve1
                    add_row[19] = _empOnProcessCount == 0 ? "0%" : ((temp_row.emp_count_approve1 * 100) / _empOnProcessCount).ToString () + "%"; // emp_count_approve1 percent
                    add_row[20] = temp_row.emp_count_approve2.ToString (); // emp_count_approve2
                    add_row[21] = _empOnProcessCount == 0 ? "0%" : ((temp_row.emp_count_approve2 * 100) / _empOnProcessCount).ToString () + "%"; // emp_count_approve2 percent
                    add_row[22] = temp_row.emp_count_approve3.ToString (); // emp_count_approve3
                    add_row[23] = _empOnProcessCount == 0 ? "0%" : ((temp_row.emp_count_approve3 * 100) / _empOnProcessCount).ToString () + "%"; // emp_count_approve3 percent

                    tableReport.Rows.InsertAt (add_row, _count_row++);
                }

                WriteExcelWithNPOI (tableReport, "xls", "report-list");
                break;
        }
    }

    protected void ddlSelectedIndexChanged (object sender, EventArgs e) {
        DropDownList ddlName = (DropDownList) sender;

        DropDownList ddlOrganization = (DropDownList) fvSearch.FindControl ("ddlOrganization");
        DropDownList ddlWorkGroup = (DropDownList) fvSearch.FindControl ("ddlWorkGroup");
        DropDownList ddlLineWork = (DropDownList) fvSearch.FindControl ("ddlLineWork");
        DropDownList ddlDepartment = (DropDownList) fvSearch.FindControl ("ddlDepartment");
        DropDownList ddlSection = (DropDownList) fvSearch.FindControl ("ddlSection");
        DropDownList ddlEmployeeGroup = (DropDownList) fvSearch.FindControl ("ddlEmployeeGroup");

        switch (ddlName.ID) {
            case "ddlOrganization":
                if (_funcTool.convertToInt (ddlName.SelectedValue) > 0) {
                    search_cen_master_detail _orgSelected = new search_cen_master_detail ();
                    _orgSelected.s_org_idx = ddlName.SelectedValue;
                    _data_cen_master = getMasterList ("4", _orgSelected);
                    _funcTool.setDdlData (ddlWorkGroup, _data_cen_master.cen_wg_list_m0, "wg_name_th", "wg_idx");
                } else {
                    ddlWorkGroup.Items.Clear ();
                }

                ddlLineWork.Items.Clear ();
                ddlDepartment.Items.Clear ();
                ddlSection.Items.Clear ();
                ddlEmployeeGroup.Items.Clear ();
                ddlWorkGroup.Items.Insert (0, new ListItem ("--- เลือกกลุ่มงาน ---", ""));
                ddlLineWork.Items.Insert (0, new ListItem ("--- เลือกสายงาน ---", ""));
                ddlDepartment.Items.Insert (0, new ListItem ("--- เลือกฝ่าย ---", ""));
                ddlSection.Items.Insert (0, new ListItem ("--- เลือกแผนก ---", ""));
                ddlEmployeeGroup.Items.Insert (0, new ListItem ("--- เลือกกลุ่มพนักงาน ---", ""));
                break;
            case "ddlWorkGroup":
                if (_funcTool.convertToInt (ddlName.SelectedValue) > 0) {
                    search_cen_master_detail _wgSelected = new search_cen_master_detail ();
                    _wgSelected.s_org_idx = ddlOrganization.SelectedValue;
                    _wgSelected.s_wg_idx = ddlName.SelectedValue;
                    _data_cen_master = getMasterList ("5", _wgSelected);
                    _funcTool.setDdlData (ddlLineWork, _data_cen_master.cen_lw_list_m0, "lw_name_th", "lw_idx");
                } else {
                    ddlLineWork.Items.Clear ();
                }

                ddlDepartment.Items.Clear ();
                ddlSection.Items.Clear ();
                ddlEmployeeGroup.Items.Clear ();
                ddlLineWork.Items.Insert (0, new ListItem ("--- เลือกสายงาน ---", ""));
                ddlDepartment.Items.Insert (0, new ListItem ("--- เลือกฝ่าย ---", ""));
                ddlSection.Items.Insert (0, new ListItem ("--- เลือกแผนก ---", ""));
                ddlEmployeeGroup.Items.Insert (0, new ListItem ("--- เลือกกลุ่มพนักงาน ---", ""));
                break;
            case "ddlLineWork":
                if (_funcTool.convertToInt (ddlName.SelectedValue) > 0) {
                    search_cen_master_detail _lgSelected = new search_cen_master_detail ();
                    _lgSelected.s_org_idx = ddlOrganization.SelectedValue;
                    _lgSelected.s_wg_idx = ddlWorkGroup.SelectedValue;
                    _lgSelected.s_lw_idx = ddlName.SelectedValue;
                    _data_cen_master = getMasterList ("6", _lgSelected);
                    _funcTool.setDdlData (ddlDepartment, _data_cen_master.cen_dept_list_m0, "dept_name_th", "dept_idx");
                } else {
                    ddlDepartment.Items.Clear ();
                }

                ddlSection.Items.Clear ();
                ddlEmployeeGroup.Items.Clear ();
                ddlDepartment.Items.Insert (0, new ListItem ("--- เลือกฝ่าย ---", ""));
                ddlSection.Items.Insert (0, new ListItem ("--- เลือกแผนก ---", ""));
                ddlEmployeeGroup.Items.Insert (0, new ListItem ("--- เลือกกลุ่มพนักงาน ---", ""));
                break;
            case "ddlDepartment":
                if (_funcTool.convertToInt (ddlName.SelectedValue) > 0) {
                    search_cen_master_detail _deptSelected = new search_cen_master_detail ();
                    _deptSelected.s_org_idx = ddlOrganization.SelectedValue;
                    _deptSelected.s_wg_idx = ddlWorkGroup.SelectedValue;
                    _deptSelected.s_lw_idx = ddlLineWork.SelectedValue;
                    _deptSelected.s_dept_idx = ddlName.SelectedValue;
                    _data_cen_master = getMasterList ("7", _deptSelected);
                    _funcTool.setDdlData (ddlSection, _data_cen_master.cen_sec_list_m0, "sec_name_th", "sec_idx");
                } else {
                    ddlSection.Items.Clear ();
                }

                ddlEmployeeGroup.Items.Clear ();
                ddlSection.Items.Insert (0, new ListItem ("--- เลือกแผนก ---", ""));
                ddlEmployeeGroup.Items.Insert (0, new ListItem ("--- เลือกกลุ่มพนักงาน ---", ""));
                break;
        }
    }
    #endregion event command

    #region RowDatabound
    protected void gvRowDataBound (object sender, GridViewRowEventArgs e) {
        var gvName = (GridView) sender;

        switch (gvName.ID) {
            case "gvReportList":
                Label lblEmpCount = (Label) e.Row.FindControl ("lblEmpCount");
                Label lblEmpOnProcessCount = (Label) e.Row.FindControl ("lblEmpOnProcessCount");
                Label lblEmpOnProcessPercent = (Label) e.Row.FindControl ("lblEmpOnProcessPercent");
                Label lblEmpNotStartCount = (Label) e.Row.FindControl ("lblEmpNotStartCount");
                Label lblEmpNotStartPercent = (Label) e.Row.FindControl ("lblEmpNotStartPercent");
                Label lblEmpFinishedCount = (Label) e.Row.FindControl ("lblEmpFinishedCount");
                Label lblEmpFinishedPercent = (Label) e.Row.FindControl ("lblEmpFinishedPercent");

                Label lblEmpMyselfCount = (Label) e.Row.FindControl ("lblEmpMyselfCount");
                Label lblEmpMyselfPercent = (Label) e.Row.FindControl ("lblEmpMyselfPercent");

                Label lblEmpSolidCount = (Label) e.Row.FindControl ("lblEmpSolidCount");
                Label lblEmpSolidPercent = (Label) e.Row.FindControl ("lblEmpSolidPercent");
                Label lblEmpDottedCount = (Label) e.Row.FindControl ("lblEmpDottedCount");
                Label lblEmpDottedPercent = (Label) e.Row.FindControl ("lblEmpDottedPercent");

                Label lblEmpApprove1Count = (Label) e.Row.FindControl ("lblEmpApprove1Count");
                Label lblEmpApprove1Percent = (Label) e.Row.FindControl ("lblEmpApprove1Percent");

                Label lblEmpApprove2Count = (Label) e.Row.FindControl ("lblEmpApprove2Count");
                Label lblEmpApprove2Percent = (Label) e.Row.FindControl ("lblEmpApprove2Percent");

                Label lblEmpApprove3Count = (Label) e.Row.FindControl ("lblEmpApprove3Count");
                Label lblEmpApprove3Percent = (Label) e.Row.FindControl ("lblEmpApprove3Percent");

                Label lblSumEmpCount = (Label) e.Row.FindControl ("lblSumEmpCount");
                Label lblSumOnProcessCount = (Label) e.Row.FindControl ("lblSumOnProcessCount");
                Label lblSumOnProcessPercent = (Label) e.Row.FindControl ("lblSumOnProcessPercent");
                Label lblSumNotStartCount = (Label) e.Row.FindControl ("lblSumNotStartCount");
                Label lblSumNotStartPercent = (Label) e.Row.FindControl ("lblSumNotStartPercent");
                Label lblSumFinishedCount = (Label) e.Row.FindControl ("lblSumFinishedCount");
                Label lblSumFinishedPercent = (Label) e.Row.FindControl ("lblSumFinishedPercent");
                Label lblSumSolidCount = (Label) e.Row.FindControl ("lblSumSolidCount");
                Label lblSumSolidPercent = (Label) e.Row.FindControl ("lblSumSolidPercent");
                Label lblSumDottedCount = (Label) e.Row.FindControl ("lblSumDottedCount");
                Label lblSumDottedPercent = (Label) e.Row.FindControl ("lblSumDottedPercent");
                Label lblSumApprove1Count = (Label) e.Row.FindControl ("lblSumApprove1Count");
                Label lblSumApprove1Percent = (Label) e.Row.FindControl ("lblSumApprove1Percent");
                Label lblSumApprove2Count = (Label) e.Row.FindControl ("lblSumApprove2Count");
                Label lblSumApprove2Percent = (Label) e.Row.FindControl ("lblSumApprove2Percent");
                Label lblSumApprove3Count = (Label) e.Row.FindControl ("lblSumApprove3Count");
                Label lblSumApprove3Percent = (Label) e.Row.FindControl ("lblSumApprove3Percent");

                if (e.Row.RowType == DataControlRowType.DataRow) {
                    lblEmpOnProcessCount.Text = (_funcTool.convertToInt (lblEmpCount.Text) - (_funcTool.convertToInt (lblEmpNotStartCount.Text) + _funcTool.convertToInt (lblEmpFinishedCount.Text))).ToString ();
                    lblEmpOnProcessPercent.Text = ((_funcTool.convertToDecimal (lblEmpCount.Text) - _funcTool.convertToDecimal (lblEmpNotStartCount.Text) - _funcTool.convertToDecimal (lblEmpFinishedCount.Text)) * 100 / _funcTool.convertToDecimal (lblEmpCount.Text)).ToString ();
                    lblEmpNotStartPercent.Text = lblEmpCount.Text == "0" ? "0" : (_funcTool.convertToDecimal (lblEmpNotStartCount.Text) * 100 / _funcTool.convertToDecimal (lblEmpCount.Text)).ToString ();
                    lblEmpFinishedPercent.Text = lblEmpCount.Text == "0" ? "0" : (_funcTool.convertToDecimal (lblEmpFinishedCount.Text) * 100 / _funcTool.convertToDecimal (lblEmpCount.Text)).ToString ();

                    lblEmpMyselfPercent.Text = lblEmpCount.Text == "0" ? "0" : (_funcTool.convertToDecimal (lblEmpMyselfCount.Text) * 100 / _funcTool.convertToDecimal (lblEmpCount.Text)).ToString ();

                    lblEmpSolidPercent.Text = lblEmpOnProcessCount.Text == "0" ? "0" : (_funcTool.convertToDecimal (lblEmpSolidCount.Text) * 100 / _funcTool.convertToDecimal (lblEmpOnProcessCount.Text)).ToString ();
                    lblEmpDottedPercent.Text = lblEmpOnProcessCount.Text == "0" ? "0" : (_funcTool.convertToDecimal (lblEmpDottedCount.Text) * 100 / _funcTool.convertToDecimal (lblEmpOnProcessCount.Text)).ToString ();
                    lblEmpApprove1Percent.Text = lblEmpOnProcessCount.Text == "0" ? "0" : (_funcTool.convertToDecimal (lblEmpApprove1Count.Text) * 100 / _funcTool.convertToDecimal (lblEmpOnProcessCount.Text)).ToString ();
                    lblEmpApprove2Percent.Text = lblEmpOnProcessCount.Text == "0" ? "0" : (_funcTool.convertToDecimal (lblEmpApprove2Count.Text) * 100 / _funcTool.convertToDecimal (lblEmpOnProcessCount.Text)).ToString ();
                    lblEmpApprove3Percent.Text = lblEmpOnProcessCount.Text == "0" ? "0" : (_funcTool.convertToDecimal (lblEmpApprove3Count.Text) * 100 / _funcTool.convertToDecimal (lblEmpOnProcessCount.Text)).ToString ();

                    dEmpSum += _funcTool.convertToDecimal (lblEmpCount.Text);
                    dEmpSumOnProcess += _funcTool.convertToDecimal (lblEmpOnProcessCount.Text);
                    dEmpSumNotStart += _funcTool.convertToDecimal (lblEmpNotStartCount.Text);
                    dEmpSumFinished += _funcTool.convertToDecimal (lblEmpFinishedCount.Text);
                    dEmpSumSolid += _funcTool.convertToDecimal (lblEmpSolidCount.Text);
                    dEmpSumDotted += _funcTool.convertToDecimal (lblEmpDottedCount.Text);
                    dEmpSumApprove1 += _funcTool.convertToDecimal (lblEmpApprove1Count.Text);
                    dEmpSumApprove2 += _funcTool.convertToDecimal (lblEmpApprove2Count.Text);
                    dEmpSumApprove3 += _funcTool.convertToDecimal (lblEmpApprove3Count.Text);
                }

                if (e.Row.RowType == DataControlRowType.Footer) {
                    lblSumEmpCount.Text = dEmpSum.ToString ();
                    lblSumOnProcessCount.Text = dEmpSumOnProcess.ToString ();
                    lblSumOnProcessPercent.Text = dEmpSum == 0 ? "0" : _funcTool.setDecimalPlaces (((dEmpSumOnProcess * 100) / dEmpSum), 8).ToString ();
                    lblSumNotStartCount.Text = dEmpSumNotStart.ToString ();
                    lblSumNotStartPercent.Text = dEmpSum == 0 ? "0" : _funcTool.setDecimalPlaces (((dEmpSumNotStart * 100) / dEmpSum), 8).ToString ();
                    lblSumFinishedCount.Text = dEmpSumFinished.ToString ();
                    lblSumFinishedPercent.Text = dEmpSum == 0 ? "0" : _funcTool.setDecimalPlaces (((dEmpSumFinished * 100) / dEmpSum), 8).ToString ();

                    lblSumSolidCount.Text = dEmpSumSolid.ToString ();
                    lblSumSolidPercent.Text = dEmpSumOnProcess == 0 ? "0" : _funcTool.setDecimalPlaces (((dEmpSumSolid * 100) / dEmpSumOnProcess), 8).ToString ();
                    lblSumDottedCount.Text = dEmpSumDotted.ToString ();
                    lblSumDottedPercent.Text = dEmpSumOnProcess == 0 ? "0" : _funcTool.setDecimalPlaces (((dEmpSumDotted * 100) / dEmpSumOnProcess), 8).ToString ();
                    lblSumApprove1Count.Text = dEmpSumApprove1.ToString ();
                    lblSumApprove1Percent.Text = dEmpSumOnProcess == 0 ? "0" : _funcTool.setDecimalPlaces (((dEmpSumApprove1 * 100) / dEmpSumOnProcess), 8).ToString ();
                    lblSumApprove2Count.Text = dEmpSumApprove2.ToString ();
                    lblSumApprove2Percent.Text = dEmpSumOnProcess == 0 ? "0" : _funcTool.setDecimalPlaces (((dEmpSumApprove2 * 100) / dEmpSumOnProcess), 8).ToString ();
                    lblSumApprove3Count.Text = dEmpSumApprove3.ToString ();
                    lblSumApprove3Percent.Text = dEmpSumOnProcess == 0 ? "0" : _funcTool.setDecimalPlaces (((dEmpSumApprove3 * 100) / dEmpSumOnProcess), 8).ToString ();
                }
                break;
        }
    }
    #endregion RowDatabound

    #region paging
    protected void gvPageIndexChanging (object sender, GridViewPageEventArgs e) {
        GridView gvName = (GridView) sender;
        gvName.PageIndex = e.NewPageIndex;

        switch (gvName.ID) {
            case "gvReportList":
                if (ViewState["ReportSearchList"] == null) {
                    _data_pms = getReportList ("2");
                    ViewState["ReportSearchList"] = _data_pms;
                }

                _funcTool.setGvData (gvReportList, ((data_pms) ViewState["ReportSearchList"]).pms_report_list);
                break;
        }

        hlSetTotop.Focus ();
    }
    #endregion paging

    #region progress
    protected string getChartOverall () {
        string sOnProcessPercent = ((Label) gvReportList.FooterRow.FindControl ("lblSumOnProcessPercent")).Text;
        string sNotStartPercent = ((Label) gvReportList.FooterRow.FindControl ("lblSumNotStartPercent")).Text;
        string sFinishedPercent = ((Label) gvReportList.FooterRow.FindControl ("lblSumFinishedPercent")).Text;

        string s_chart = "";
        s_chart = "<div class='progress'>";
        s_chart += "    <div class='progress-bar' role='progressbar' style='width: " + sOnProcessPercent + "%' aria-valuenow='" + sOnProcessPercent + "' aria-valuemin='0' aria-valuemax='100'>" + sOnProcessPercent + "%</div>";
        s_chart += "    <div class='progress-bar progress-bar-danger' role='progressbar' style='width: " + sNotStartPercent + "%' aria-valuenow='" + sNotStartPercent + "' aria-valuemin='0' aria-valuemax='100'>" + sNotStartPercent + "%</div>";
        s_chart += "    <div class='progress-bar progress-bar-success' role='progressbar' style='width: " + sFinishedPercent + "%' aria-valuenow='" + sFinishedPercent + "' aria-valuemin='0' aria-valuemax='100'>" + sFinishedPercent + "%</div>";
        s_chart += "</div>";

        return s_chart;
    }

    protected string getChartOnProgress () {
        string sSolidPercent = ((Label) gvReportList.FooterRow.FindControl ("lblSumSolidPercent")).Text;
        string sDottedPercent = ((Label) gvReportList.FooterRow.FindControl ("lblSumDottedPercent")).Text;
        string sApprove1Percent = ((Label) gvReportList.FooterRow.FindControl ("lblSumApprove1Percent")).Text;
        string sApprove2Percent = ((Label) gvReportList.FooterRow.FindControl ("lblSumApprove2Percent")).Text;
        string sApprove3Percent = ((Label) gvReportList.FooterRow.FindControl ("lblSumApprove3Percent")).Text;

        string s_chart = "";
        s_chart = "<div class='progress'>";
        s_chart += "    <div class='progress-bar' role='progressbar' style='width: " + sSolidPercent + "%' aria-valuenow='" + sSolidPercent + "' aria-valuemin='0' aria-valuemax='100'>" + sSolidPercent + "%</div>";
        s_chart += "    <div class='progress-bar progress-bar-info' role='progressbar' style='width: " + sDottedPercent + "%' aria-valuenow='" + sDottedPercent + "' aria-valuemin='0' aria-valuemax='100'>" + sDottedPercent + "%</div>";
        s_chart += "    <div class='progress-bar progress-bar-danger' role='progressbar' style='width: " + sApprove1Percent + "%' aria-valuenow='" + sApprove1Percent + "' aria-valuemin='0' aria-valuemax='100'>" + sApprove1Percent + "%</div>";
        s_chart += "    <div class='progress-bar progress-bar-warning' role='progressbar' style='width: " + sApprove2Percent + "%' aria-valuenow='" + sApprove2Percent + "' aria-valuemin='0' aria-valuemax='100'>" + sApprove2Percent + "%</div>";
        s_chart += "    <div class='progress-bar progress-bar-success' role='progressbar' style='width: " + sApprove3Percent + "%' aria-valuenow='" + sApprove3Percent + "' aria-valuemin='0' aria-valuemax='100'>" + sApprove3Percent + "%</div>";
        s_chart += "</div>";

        return s_chart;
    }
    #endregion progress

    #region reuse
    protected void initPage () {
        clearSession ();
        clearViewState ();

        setActiveTab ("viewList", 0);

        hlSetTotop.Focus ();
    }

    protected void clearSession () {
        // Session["auth"] = null;
    }

    protected void clearViewState () {
        ViewState["ReportSearchList"] = null;
        // ViewState["EmpAllList"] = null;
        // ViewState["EmpDetail"] = null;
    }

    protected void setActiveTab (string activeTab, int doc_idx) {
        setActiveView (activeTab, doc_idx);
        setActiveTabBar (activeTab);

        switch (activeTab) {
            case "viewList":
                // get organization
                DropDownList ddlOrganization = (DropDownList) fvSearch.FindControl ("ddlOrganization");
                DropDownList ddlWorkGroup = (DropDownList) fvSearch.FindControl ("ddlWorkGroup");
                DropDownList ddlLineWork = (DropDownList) fvSearch.FindControl ("ddlLineWork");
                DropDownList ddlDepartment = (DropDownList) fvSearch.FindControl ("ddlDepartment");
                DropDownList ddlSection = (DropDownList) fvSearch.FindControl ("ddlSection");
                DropDownList ddlEmployeeGroup = (DropDownList) fvSearch.FindControl ("ddlEmployeeGroup");

                _data_cen_master = getMasterList ("3", null);
                ddlOrganization.Items.Clear ();
                ddlWorkGroup.Items.Clear ();
                ddlLineWork.Items.Clear ();
                ddlDepartment.Items.Clear ();
                ddlSection.Items.Clear ();
                // ddlPosition.Items.Clear ();
                ddlEmployeeGroup.Items.Clear ();
                _funcTool.setDdlData (ddlOrganization, _data_cen_master.cen_org_list_m0, "org_name_th", "org_idx");
                ddlOrganization.Items.Insert (0, new ListItem ("--- เลือกองค์กร ---", ""));
                ddlWorkGroup.Items.Insert (0, new ListItem ("--- เลือกกลุ่มงาน ---", ""));
                ddlLineWork.Items.Insert (0, new ListItem ("--- เลือกสายงาน ---", ""));
                ddlDepartment.Items.Insert (0, new ListItem ("--- เลือกฝ่าย ---", ""));
                ddlSection.Items.Insert (0, new ListItem ("--- เลือกแผนก ---", ""));
                ddlEmployeeGroup.Items.Insert (0, new ListItem ("--- เลือกกลุ่มพนักงาน ---", ""));

                _funcTool.setGvData (gvReportList, null);
                litChartOverall.Text = String.Empty;
                litChartOnProgress.Text = String.Empty;

                hlSetTotop.Focus ();
                break;
        }

        hlSetTotop.Focus ();
    }

    protected void setActiveView (string activeTab, int doc_idx) {
        mvSystem.SetActiveView ((View) mvSystem.FindControl (activeTab));
    }

    protected void setActiveTabBar (string activeTab) {
        switch (activeTab) {
            case "viewList":
            case "viewDetail":
                li0.Attributes.Add ("class", "active");
                // li1.Attributes.Add ("class", "");
                // li2.Attributes.Add ("class", "");
                break;
        }
    }

    protected void linkBtnTrigger (LinkButton linkBtnID) {
        UpdatePanel updatePanel = Page.Master.FindControl ("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger ();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add (triggerLinkBtn);
    }

    protected void setTrigger () {
        // nav trigger
        linkBtnTrigger (lbList);
        // linkBtnTrigger (lbImport);
        // linkBtnTrigger (lbReport);

        LinkButton lbSearchData = (LinkButton) fvSearch.FindControl ("lbSearchData");
        LinkButton lbResetData = (LinkButton) fvSearch.FindControl ("lbResetData");
        LinkButton lbExportData = (LinkButton) fvSearch.FindControl ("lbExportData");
        linkBtnTrigger (lbSearchData);
        linkBtnTrigger (lbResetData);
        linkBtnTrigger (lbExportData);
    }

    protected data_cen_master getMasterList (string _masterMode, search_cen_master_detail _searchData) {
        _data_cen_master.search_cen_master_list = new search_cen_master_detail[1];
        _data_cen_master.master_mode = _masterMode;
        _data_cen_master.search_cen_master_list[0] = _searchData;
        // litDebug.Text = _funcTool.convertObjectToJson(_data_cen_master);

        _data_cen_master = callServicePostCenMaster (_urlCenGetCenMasterList, _data_cen_master);
        return _data_cen_master;
    }

    protected data_cen_employee getEmployeeList (string _employeeMode, search_cen_employee_detail _searchData) {
        _data_cen_employee.search_cen_employee_list = new search_cen_employee_detail[1];
        _data_cen_employee.employee_mode = _employeeMode;
        _data_cen_employee.search_cen_employee_list[0] = _searchData;
        // litDebug.Text = _funcTool.convertObjectToJson(_data_cen_master);

        _data_cen_employee = callServicePostCenEmployee (_urlGetCenEmployeeList, _data_cen_employee);
        return _data_cen_employee;
    }

    protected data_pms getReportList (string _pmsMode) {
        DropDownList ddlOrganization = (DropDownList) fvSearch.FindControl ("ddlOrganization");
        DropDownList ddlWorkGroup = (DropDownList) fvSearch.FindControl ("ddlWorkGroup");
        DropDownList ddlLineWork = (DropDownList) fvSearch.FindControl ("ddlLineWork");
        DropDownList ddlDepartment = (DropDownList) fvSearch.FindControl ("ddlDepartment");
        DropDownList ddlSection = (DropDownList) fvSearch.FindControl ("ddlSection");
        DropDownList ddlEmployeeGroup = (DropDownList) fvSearch.FindControl ("ddlEmployeeGroup");

        search_pms_report_detail _searchData = new search_pms_report_detail ();
        _searchData.s_org_idx = ddlOrganization.SelectedValue;
        _searchData.s_wg_idx = ddlWorkGroup.SelectedValue;
        _searchData.s_lw_idx = ddlLineWork.SelectedValue;
        _searchData.s_dept_idx = ddlDepartment.SelectedValue;
        _searchData.s_sec_idx = ddlSection.SelectedValue;

        _data_pms.search_pms_report_list = new search_pms_report_detail[1];
        _data_pms.pms_mode = _pmsMode;
        _data_pms.search_pms_report_list[0] = _searchData;

        _data_pms = callServicePostPms (_urlPmsGetPmsManageData, _data_pms);
        ViewState["ReportSearchList"] = _data_pms;
        return _data_pms;
    }

    protected data_cen_master callServicePostCenMaster (string _cmdUrl, data_cen_master _data_cen_master) {
        // convert to json
        _local_json = _funcTool.convertObjectToJson (_data_cen_master);
        //litDebug.Text = _local_json;

        // call services
        _local_json = _funcTool.callServicePost (_cmdUrl, _local_json);

        // convert json to object
        _data_cen_master = (data_cen_master) _funcTool.convertJsonToObject (typeof (data_cen_master), _local_json);

        return _data_cen_master;
    }

    protected data_cen_employee callServicePostCenEmployee (string _cmdUrl, data_cen_employee _data_cen_employee) {
        // convert to json
        _local_json = _funcTool.convertObjectToJson (_data_cen_employee);
        //litDebug.Text = _local_json;

        // call services
        _local_json = _funcTool.callServicePost (_cmdUrl, _local_json);

        // convert json to object
        _data_cen_employee = (data_cen_employee) _funcTool.convertJsonToObject (typeof (data_cen_employee), _local_json);

        return _data_cen_employee;
    }

    protected data_pms callServicePostPms (string _cmdUrl, data_pms _data_pms) {
        // convert to json
        _local_json = _funcTool.convertObjectToJson (_data_pms);
        // litDebug.Text = _local_json;

        // call services
        _local_json = _funcTool.callServicePost (_cmdUrl, _local_json);
        // litDebug.Text = _local_json;

        // convert json to object
        _data_pms = (data_pms) _funcTool.convertJsonToObject (typeof (data_pms), _local_json);

        return _data_pms;
    }

    protected void WriteExcelWithNPOI (DataTable dt, String extension, String fileName) {
        IWorkbook workbook;
        if (extension == "xlsx") {
            workbook = new XSSFWorkbook ();
        } else if (extension == "xls") {
            workbook = new HSSFWorkbook ();
        } else {
            throw new Exception ("This format is not supported");
        }
        ISheet sheet1 = workbook.CreateSheet ("Sheet 1");
        IRow row1 = sheet1.CreateRow (0);
        for (int j = 0; j < dt.Columns.Count; j++) {
            ICell cell = row1.CreateCell (j);
            String columnName = dt.Columns[j].ToString ();
            cell.SetCellValue (columnName);
        }
        for (int i = 0; i < dt.Rows.Count; i++) {
            IRow row = sheet1.CreateRow (i + 1);
            for (int j = 0; j < dt.Columns.Count; j++) {
                ICell cell = row.CreateCell (j);
                String columnName = dt.Columns[j].ToString ();
                cell.SetCellValue (dt.Rows[i][columnName].ToString ());
            }
        }
        using (var exportData = new MemoryStream ()) {
            Response.Clear ();
            workbook.Write (exportData);
            if (extension == "xlsx") {
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader ("Content-Disposition", string.Format ("attachment;filename={0}", fileName + ".xlsx"));
                Response.BinaryWrite (exportData.ToArray ());
            } else if (extension == "xls") {
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader ("Content-Disposition", string.Format ("attachment;filename={0}", fileName + ".xls"));
                Response.BinaryWrite (exportData.GetBuffer ());
            }
            Response.End ();
        }
    }
    #endregion reuse
}