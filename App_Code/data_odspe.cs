﻿using System;
using System.Xml.Serialization;

#region data_odspe
[Serializable]
[XmlRoot("data_odspe")]
public class data_odspe
{
   [XmlElement("return_code")]
   public int return_code { get; set; }
   [XmlElement("return_msg")]
   public string return_msg { get; set; }
   [XmlElement("cen_odspe_action")]
   public cen_odspe[] cen_odspe_action { get; set; }
}
#endregion data_odspe

#region cen_odspe
[Serializable]
public class cen_odspe
{
   [XmlElement("cen_org_idx")]
   public int cen_org_idx { get; set; }
   [XmlElement("cen_rdept_idx")]
   public int cen_rdept_idx { get; set; }
   [XmlElement("cen_rsec_idx")]
   public int cen_rsec_idx { get; set; }
   [XmlElement("cen_rpos_idx")]
   public int cen_rpos_idx { get; set; }
   [XmlElement("cen_emp_idx")]
   public int cen_emp_idx { get; set; }

   [XmlElement("OrgIDX")]
   public int OrgIDX { get; set; }
   [XmlElement("OrgNameEN")]
   public string OrgNameEN { get; set; }
   [XmlElement("OrgNameTH")]
   public string OrgNameTH { get; set; }
   [XmlElement("OrgStatus")]
   public int OrgStatus { get; set; }

   [XmlElement("RDeptIDX")]
   public int RDeptIDX { get; set; }
   [XmlElement("DeptNameTH")]
   public string DeptNameTH { get; set; }
   [XmlElement("DeptNameEN")]
   public string DeptNameEN { get; set; }
   [XmlElement("RDeptStatus")]
   public int RDeptStatus { get; set; }

   [XmlElement("RSecIDX")]
   public int RSecIDX { get; set; }
   [XmlElement("SecNameTH")]
   public string SecNameTH { get; set; }
   [XmlElement("RSecStatus")]
   public int RSecStatus { get; set; }
}
#endregion cen_odspe