﻿using DocumentFormat.OpenXml.Drawing;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Data.OleDb;
using System.Web.UI.DataVisualization.Charting;

public partial class websystem_MasterData_ecom_m0_bom : System.Web.UI.Page
{

    #region Connect
    function_tool _funcTool = new function_tool();
    data_ecom_fileinterface data_ecom_fileinterface = new data_ecom_fileinterface();
    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    //--  --//
    static string _urlGetEcomM0BOM = _serviceUrl + ConfigurationManager.AppSettings["urlGetEcomM0BOM"];
    static string _urlSetEcomM0BOM = _serviceUrl + ConfigurationManager.AppSettings["urlSetEcomM0BOM"];
    static string _urlDelEcomM0BOM = _serviceUrl + ConfigurationManager.AppSettings["urlDelEcomM0BOM"];

    static string _urlGetEcomM1BOM = _serviceUrl + ConfigurationManager.AppSettings["urlGetEcomM1BOM"];
    static string _urlSetEcomM1BOM = _serviceUrl + ConfigurationManager.AppSettings["urlSetEcomM1BOM"];
    static string _urlDelEcomM1BOM = _serviceUrl + ConfigurationManager.AppSettings["urlDelEcomM1BOM"];

    static string _urlGetEcomM0MAT = _serviceUrl + ConfigurationManager.AppSettings["urlGetEcomM0MAT"];
    static string _urlGetEcomM0Promotion = _serviceUrl + ConfigurationManager.AppSettings["urlGetEcomM0Promotion"];

    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    bool _flag_qmr = false;
    #endregion Connect

    #region Page_Init
    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = _funcTool.convertToInt(Session["emp_idx"].ToString());


    }
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            Select_Place();
            clear_temp_data();

        }
    }
    #endregion Page Load

    #region initPage
    protected void initPage()
    {
        setActiveView("ViewIndex");
    }
    #endregion


    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        GvName.PageIndex = e.NewPageIndex;
        GvName.DataBind();
        switch (GvName.ID)
        {
            case "Gv_select_place":

                Select_Place();
                break;
            case "Gvm1":

                Select_BOM_m1();
                break;
            case "GvSample":

                Select_BOM_m1();
                break;
        }
    }

    protected void setActiveView(string activeTab, int doc_idx = 0)
    {
        sreach_detailbox.Visible = true;
        div_searchmaster_page.Visible = true;
        div_insert.Visible = false;
        FvsubBOM.Visible = false;
        btn_addplace.Visible = true;
        btn_adddetail.Visible = true;
        Gv_select_place.EditIndex = -1;
        Gvm1.EditIndex = -1;

        MvSystem.SetActiveView((View)MvSystem.FindControl(activeTab));

        //FormView FvAddImport = (FormView)view_ifif.FindControl("FvAddImport");
        //LinkButton btnsavedatafile2 = (LinkButton)FvAddImport.FindControl("btnsavedatafile");

        //btnsavedatafile2.Visible = false;
        //div_detail.Visible = false;
        //GV_ecom_u0.Visible = false;
        //div_detail.Visible = false;
        switch (activeTab)
        {
            case "ViewIndex":
                Select_Place();
                break;

        }
    }

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        GvName.EditIndex = e.NewEditIndex;
        GridViewRow row = GvName.Rows[GvName.EditIndex];
        switch (GvName.ID)
        {
            case "Gv_select_place":

                Select_Place();
                break;
            case "Gvm1":

                Select_BOM_m1();

                break;
            case "GvSample":
                select_data_temp();
                break;
        }

    }

    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "Gv_select_place":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbpplace_status = (Label)e.Row.FindControl("lbpplace_status");
                    Label place_statusOnline = (Label)e.Row.FindControl("place_statusOnline");
                    Label place_statusOffline = (Label)e.Row.Cells[3].FindControl("place_statusOffline");

                    ViewState["_place_status"] = lbpplace_status.Text;


                    if (ViewState["_place_status"].ToString() == "1")
                    {
                        place_statusOnline.Visible = true;
                    }
                    else if (ViewState["_place_status"].ToString() == "0")
                    {
                        place_statusOffline.Visible = true;
                    }

                }

                if (e.Row.RowState.ToString().Contains("Edit"))
                {

                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    btn_addplace.Visible = true;
                    div_insert.Visible = false;

                }

                break;
            case "Gvm1":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbpplace_status_m1 = (Label)e.Row.Cells[0].FindControl("lbpplace_status");
                    Label place_statusOnline_m1 = (Label)e.Row.Cells[0].FindControl("place_statusOnline");
                    Label place_statusOffline_m1 = (Label)e.Row.Cells[0].FindControl("place_statusOffline");

                    ViewState["_bom_m1_status"] = lbpplace_status_m1.Text;


                    if (ViewState["_bom_m1_status"].ToString() == "1")
                    {
                        place_statusOnline_m1.Visible = true;
                    }
                    else if (ViewState["_bom_m1_status"].ToString() == "0")
                    {
                        place_statusOffline_m1.Visible = true;
                    }
                }

                if (e.Row.RowState.ToString().Contains("Edit"))
                {

                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    DropDownList ddl_material_m1 = (DropDownList)e.Row.FindControl("ddl_material_m1_edit");
                    DropDownList ddl_promotion_m1 = (DropDownList)e.Row.FindControl("ddl_promotion_m1_edit");
                    ddl_material_list(ddl_material_m1);
                    ddl_promotion_list(ddl_promotion_m1);

                    btn_adddetail.Visible = true;
                    FvsubBOM.Visible = false;

                }

                break;
            case "GvSample":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbpplace_status_m1 = (Label)e.Row.Cells[0].FindControl("lbpplace_status");
                    Label place_statusOnline_m1 = (Label)e.Row.Cells[0].FindControl("place_statusOnline");
                    Label place_statusOffline_m1 = (Label)e.Row.Cells[0].FindControl("place_statusOffline");

                    ViewState["_bom_temp_status"] = lbpplace_status_m1.Text;


                    if (ViewState["_bom_temp_status"].ToString() == "1")
                    {
                        place_statusOnline_m1.Visible = true;
                    }
                    else if (ViewState["_bom_temp_status"].ToString() == "0")
                    {
                        place_statusOffline_m1.Visible = true;
                    }

                }
                if (e.Row.RowState.ToString().Contains("Edit"))
                {

                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    //btn_adddetail.Visible = true;
                    //FvsubBOM.Visible = false;



                }
                break;
        }
    }

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        GvName.EditIndex = -1;
        switch (GvName.ID)
        {
            case "Gv_select_place":
                sreach_detailbox.Visible = true;
                div_searchmaster_page.Visible = true;
                Select_Place();
                break;
            case "Gvm1":
                sreach_detailbox.Visible = true;
                div_searchmaster_page.Visible = true;
                Select_BOM_m1();

                break;
            case "GvSample":

                select_data_temp();

                break;
        }
    }
    protected void gvRowDeleted(object sender, GridViewDeleteEventArgs e)
    {
        var gridViewName = (GridView)sender;

        switch (gridViewName.ID)
        {
            case "GvSample":

                gridViewName.EditIndex = -1;

                var DeleteSampleDetail = (DataSet)ViewState["vsSample"];
                var drDriving = DeleteSampleDetail.Tables[0].Rows;

                drDriving.RemoveAt(e.RowIndex);

                ViewState["vsSample"] = DeleteSampleDetail;
                select_data_temp();

                break;

        }

    }

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "Gv_select_place":

                TextBox txt_place_idx_edit = (TextBox)GvName.Rows[e.RowIndex].FindControl("txt_bom_m0_idx_edit");
                TextBox bom_name = (TextBox)GvName.Rows[e.RowIndex].FindControl("Name_BOM");
                TextBox Name_material = (TextBox)GvName.Rows[e.RowIndex].FindControl("Name_material");

                TextBox tbDateStart = (TextBox)GvName.Rows[e.RowIndex].FindControl("tbDateStart");
                TextBox tbDateEnd = (TextBox)GvName.Rows[e.RowIndex].FindControl("tbDateEnd");

                DropDownList bom_status = (DropDownList)GvName.Rows[e.RowIndex].FindControl("ddEdit_place");
                data_ecom_fileinterface = new data_ecom_fileinterface(); ;
                ecom_m0_data_bom m0_detail = new ecom_m0_data_bom();
                data_ecom_fileinterface.ecom_m0_data_bom_list = new ecom_m0_data_bom[1];
                m0_detail.bom_m0_idx = _funcTool.convertToInt(txt_place_idx_edit.Text);
                m0_detail.bom_name = bom_name.Text.Trim();

                m0_detail.start_date = tbDateStart.Text.Trim();
                m0_detail.end_date = tbDateEnd.Text.Trim();

                m0_detail.bom_status = _funcTool.convertToInt(bom_status.Text);
                m0_detail.emp_idx_update = _emp_idx;
                data_ecom_fileinterface.ecom_m0_data_bom_list[0] = m0_detail;
                //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_ecom_fileinterface));
                data_ecom_fileinterface = callServicePostAPI(_urlSetEcomM0BOM, data_ecom_fileinterface);

                if (data_ecom_fileinterface.return_code == 1)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ข้อมูลซ้ำ กรุณาแก้ไขใหม่อีกครั้ง!!!');", true);
                }
                else
                {
                    GvName.EditIndex = -1;
                    GvName.PageIndex = 0;
                    refresh_m0();
                    set_viewstate_src_m0();
                    Select_Place();
                    btn_addplace.Visible = true;
                    div_insert.Visible = false;
                }

                break;

            case "Gvm1":
                TextBox txt_bom_m1_idx_edit = (TextBox)GvName.Rows[e.RowIndex].FindControl("txt_bom_m0_idx_edit");
                TextBox txtproduct_name = (TextBox)GvName.Rows[e.RowIndex].FindControl("txtproduct_name");
                TextBox txtmaterial = (TextBox)GvName.Rows[e.RowIndex].FindControl("txtmaterial");
                TextBox txtunit_name = (TextBox)GvName.Rows[e.RowIndex].FindControl("txtunit_name");
                TextBox txtprice = (TextBox)GvName.Rows[e.RowIndex].FindControl("txtprice");
                TextBox txtdiscount = (TextBox)GvName.Rows[e.RowIndex].FindControl("txtdiscount");

                TextBox txtdtxtPromotion = (TextBox)GvName.Rows[e.RowIndex].FindControl("txtPromotionID");
                TextBox txtPromotionDesc = (TextBox)GvName.Rows[e.RowIndex].FindControl("txtPromotionDesc");
                TextBox txtTypepromotion = (TextBox)GvName.Rows[e.RowIndex].FindControl("txtTypepromotion");

                TextBox txttex = (TextBox)GvName.Rows[e.RowIndex].FindControl("txttex");
                TextBox txtSAPcode = (TextBox)GvName.Rows[e.RowIndex].FindControl("txtSAPcode");
                TextBox txtamount = (TextBox)GvName.Rows[e.RowIndex].FindControl("txtamount");

                DropDownList bom_m1_status = (DropDownList)GvName.Rows[e.RowIndex].FindControl("ddEdit_statusm1");
                data_ecom_fileinterface = new data_ecom_fileinterface(); ;
                ecom_m1_data_bom m1_detail = new ecom_m1_data_bom();
                data_ecom_fileinterface.ecom_m1_data_bom_list = new ecom_m1_data_bom[1];

                m1_detail.bom_m1_idx = _funcTool.convertToInt(txt_bom_m1_idx_edit.Text);
                m1_detail.product_name = txtproduct_name.Text.Trim();
                m1_detail.material = txtmaterial.Text.Trim();

                m1_detail.unit_name = txtunit_name.Text.Trim();
                m1_detail.price = _funcTool.convertToDecimal(txtprice.Text.Trim());
                m1_detail.discount = _funcTool.convertToDecimal(txtdiscount.Text.Trim());
                m1_detail.VAT = _funcTool.convertToDecimal(txttex.Text.Trim());
                m1_detail.PromotionID = txtdtxtPromotion.Text.Trim();
                m1_detail.PromotionDesc = txtPromotionDesc.Text.Trim();
                m1_detail.SAP_code = txtSAPcode.Text.Trim();
                m1_detail.amount = _funcTool.convertToInt(txtamount.Text);
                m1_detail.bom_m1_status = _funcTool.convertToInt(bom_m1_status.Text);
                m1_detail.emp_idx_update = _emp_idx;
                m1_detail.type_promotion = _funcTool.convertToInt(txtTypepromotion.Text);
                data_ecom_fileinterface.ecom_m1_data_bom_list[0] = m1_detail;
                // litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_ecom_fileinterface));
                data_ecom_fileinterface = callServicePostAPI(_urlSetEcomM1BOM, data_ecom_fileinterface);

                if (data_ecom_fileinterface.return_code == 1)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ข้อมูลซ้ำ กรุณาแก้ไขใหม่อีกครั้ง!!!');", true);
                }
                else
                {
                    GvName.EditIndex = -1;
                    GvName.PageIndex = 0;
                    refresh_m1();
                    set_viewstate_src();
                    Select_BOM_m1();

                }
                break;
            case "GvSample":
                TextBox txtproduct_name_temp = (TextBox)GvName.Rows[e.RowIndex].FindControl("txtproduct_name");
                TextBox txtmaterial_temp = (TextBox)GvName.Rows[e.RowIndex].FindControl("txtmaterial");
                TextBox txtunit_name_temp = (TextBox)GvName.Rows[e.RowIndex].FindControl("txtunit_name");
                TextBox txtprice_temp = (TextBox)GvName.Rows[e.RowIndex].FindControl("txtprice");
                TextBox txtdiscount_temp = (TextBox)GvName.Rows[e.RowIndex].FindControl("txtdiscount");

                TextBox txtdtxtPromotion_temp = (TextBox)GvName.Rows[e.RowIndex].FindControl("txtPromotionID");
                TextBox txtPromotionDesc_temp = (TextBox)GvName.Rows[e.RowIndex].FindControl("txtPromotionDesc");
                TextBox txtSAPcode_temp = (TextBox)GvName.Rows[e.RowIndex].FindControl("txtSAPcode");
                TextBox txtamount_temp = (TextBox)GvName.Rows[e.RowIndex].FindControl("txtamount");

                DropDownList bom_m1_status_temp = (DropDownList)GvName.Rows[e.RowIndex].FindControl("ddEdit_statusm1");

                var TempSampleDetail = (DataSet)ViewState["vsSample"];
                var update_temp_data = TempSampleDetail.Tables[0].Rows;


                update_temp_data[e.RowIndex]["product_name"] = txtproduct_name_temp.Text.Trim();
                update_temp_data[e.RowIndex]["material"] = txtmaterial_temp.Text.Trim();

                update_temp_data[e.RowIndex]["unit_name"] = txtunit_name_temp.Text.Trim();
                update_temp_data[e.RowIndex]["price"] = _funcTool.convertToDecimal(txtprice_temp.Text.Trim());
                update_temp_data[e.RowIndex]["discount"] = _funcTool.convertToDecimal(txtdiscount_temp.Text.Trim());

                update_temp_data[e.RowIndex]["PromotionID"] = txtdtxtPromotion_temp.Text.Trim();
                update_temp_data[e.RowIndex]["PromotionDesc"] = txtPromotionDesc_temp.Text.Trim();
                update_temp_data[e.RowIndex]["SAP_code"] = txtSAPcode_temp.Text.Trim();
                update_temp_data[e.RowIndex]["amount"] = _funcTool.convertToInt(txtamount_temp.Text);
                update_temp_data[e.RowIndex]["bom_m1_status"] = _funcTool.convertToInt(bom_m1_status_temp.Text);
                ViewState["vsSample"] = TempSampleDetail;
                //e.RowIndex
                GvName.EditIndex = -1;
                GvName.PageIndex = 0;

                select_data_temp();
                break;
        }
    }

    protected void Fv_DataBound(object sender, EventArgs e)
    {
        FormView FvName = (FormView)sender;

        switch (FvName.ID)
        {
            case "FvInsert":
                DropDownList ddl_material = (DropDownList)FvName.FindControl("ddl_material");
                DropDownList ddl_promotion = (DropDownList)FvName.FindControl("ddl_promotion");
                ddl_material_list(ddl_material);
                ddl_promotion_list(ddl_promotion);
                break;
            case "FvsubBOM":
                DropDownList ddl_material_m1 = (DropDownList)FvName.FindControl("ddl_material_m1");
                DropDownList ddl_promotion_m1 = (DropDownList)FvName.FindControl("ddl_promotion_m1");
                ddl_material_list(ddl_material_m1);
                ddl_promotion_list(ddl_promotion_m1);

                break;

        }
    }
    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        //sreach_detailbox.Visible = true;
        //div_searchmaster_page.Visible = true;
        //sreach_detailbox.Visible = false;
        //div_searchmaster_page.Visible = false;

        data_ecom_fileinterface = new data_ecom_fileinterface(); ;

        ecom_m1_data_bom m1_detail = new ecom_m1_data_bom();
        data_ecom_fileinterface.ecom_m1_data_bom_list = new ecom_m1_data_bom[1];

        ecom_m0_data_bom m0_detail = new ecom_m0_data_bom();
        data_ecom_fileinterface.ecom_m0_data_bom_list = new ecom_m0_data_bom[1];

        switch (cmdName)
        {
            #region cmdAdddetail
            case "cmdAdddetail":
                btn_adddetail.Visible = false;
                setFormData(FvsubBOM, FormViewMode.Insert, null);
                FvsubBOM.Visible = true;
                sreach_detailbox.Visible = false;
                break;
            #endregion cmdAdddetail
            #region cmdAddUnit
            case "cmdAddUnit":
                btn_addplace.Visible = false;
                clear_temp_data();
                setFormData(FvInsert, FormViewMode.Insert, null);
                div_insert.Visible = true;
                div_searchmaster_page.Visible = false;
                div_save_temp.Visible = false;
                break;
            #endregion cmdAddUnit
            #region CmdCancel
            case "CmdCancel":
                btn_addplace.Visible = true;
                div_insert.Visible = false;
                sreach_detailbox.Visible = true;
                div_searchmaster_page.Visible = true;
                clear_temp_data();
                break;
            #endregion CmdCancel
            #region CmdSave
            case "CmdSave":
                insert_m0();
                //Insert_Place();
                //Select_Place();
                break;
            #endregion CmdSave
            #region CmdCancelDe
            case "CmdCancelDe":
                btn_adddetail.Visible = true;
                FvsubBOM.Visible = false;
                sreach_detailbox.Visible = true;
                break;
            #endregion CmdCancelDe
            #region CmdSaveDe
            case "CmdSaveDe":
                Insert_subBOM();
                Select_BOM_m1();
                break;
            #endregion CmdSaveDe
            #region btnRefreshMaster_Supplier
            case "btnRefreshMaster_Supplier":
                refresh_m0();
                set_viewstate_src_m0();
                Select_Place();
                break;
            #endregion btnRefreshMaster_Supplier
            #region btnRefreshsub
            case "btnRefreshsub":
                refresh_m1();
                set_viewstate_src();
                Select_BOM_m1();
                break;
            #endregion btnRefreshsub
            #region CmdSearchMaster_src
            case "CmdSearchMaster_src":
                set_viewstate_src_m0();
                Select_Place();
                break;
            #endregion CmdSearchMaster_src
            #region CmdSearchsub_src
            case "CmdSearchsub_src":
                set_viewstate_src();
                Select_BOM_m1();
                break;
            #endregion CmdSearchsub_src
            #region btnTodelete
            case "btnTodelete":
                int bom_m0_idx_del = _funcTool.convertToInt(cmdArg);
                m0_detail.bom_m0_idx = bom_m0_idx_del;
                data_ecom_fileinterface.ecom_m0_data_bom_list[0] = m0_detail;
                data_ecom_fileinterface = callServicePostAPI(_urlDelEcomM0BOM, data_ecom_fileinterface);
                //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_place_del));
                Select_Place();
                btn_addplace.Visible = true;
                div_insert.Visible = false;
                break;
            #endregion btnTodelete
            #region btnTodelete_m1
            case "btnTodelete_m1":

                int bom_m1_idx_del = _funcTool.convertToInt(cmdArg);

                m1_detail.bom_m1_idx = bom_m1_idx_del;
                data_ecom_fileinterface.ecom_m1_data_bom_list[0] = m1_detail;

                data_ecom_fileinterface = callServicePostAPI(_urlDelEcomM1BOM, data_ecom_fileinterface);
                //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_place_del));
                Select_BOM_m1();
                break;
            #endregion btnTodelete_m1
            #region readdetail
            case "readdetail":
                ecom_m0_data_bom[] data_m0 = (ecom_m0_data_bom[])ViewState["data_m0"];

                var _ViewState_m0 = (from m0_vs in data_m0
                                     where m0_vs.bom_m0_idx.ToString() == cmdArg.ToString()
                                     select new
                                     {
                                         m0_vs.bom_name
                                     }).ToList();

                string bhn = string.Empty;
                if (_ViewState_m0.Count() > 0)
                {
                    bhn = _ViewState_m0[0].bom_name.ToString();
                }
                //MvSystem.SetActiveView(ViewReadmore);
                setActiveView("ViewReadmore");
                ViewState["bom_m0_idx"] = cmdArg.ToString();
                Select_BOM_m1();
                BOM_header_name.InnerText = bhn;
                break;
            #endregion readdetail
            #region cmd_backward
            case "cmd_backward":
                setActiveView("ViewIndex");
                //MvSystem.SetActiveView(ViewIndex);
                break;
            #endregion cmd_backward
            #region Cmd_plus
            case "Cmd_plus":
                add_detail_to_viewstate();
                setFormData(FvInsert, FormViewMode.Insert, null);
                TextBox tex_name = (TextBox)FvInsert.FindControl("txtBOM");

                TextBox tbDateStart = (TextBox)FvInsert.FindControl("tbDateStart");
                TextBox tbDateEnd = (TextBox)FvInsert.FindControl("tbDateEnd");
                LinkButton Cmd_save_bom_name_plus = (LinkButton)FvInsert.FindControl("Cmd_save_bom_name");
                LinkButton Cmd_edit_bom_name_plus = (LinkButton)FvInsert.FindControl("Cmd_edit_bom_name");
                LinkButton Cmd_cc_bom_name_plus = (LinkButton)FvInsert.FindControl("Cmd_cc_bom_name");
                tex_name.Text = ViewState["bom_name"].ToString();
                tex_name.ReadOnly = true;

                DataSet temp_date = (DataSet)ViewState["dateSample"];
                var log_temp_data = temp_date.Tables[0].Rows;

                tbDateStart.Text = log_temp_data[0]["startdate"].ToString();
                tbDateEnd.Text = log_temp_data[0]["enddate"].ToString();

                //tbDateStart.Text = "2020-01-01";
                //tbDateEnd.Text = "2021-01-01";
                tbDateStart.ReadOnly = true;
                tbDateEnd.ReadOnly = true;
                set_control_bomname(string.Empty);
                div_save_temp.Visible = true;

                break;
            #endregion Cmd_plus
            #region cal_price_and_vat
            case "cal_price_and_vat":

                FormView FvName = FvInsert;
                OntxtPrice(FvName);
                break;
            #endregion cal_price_and_vat
            #region cal_price_and_vat_sub
            case "cal_price_and_vat_sub":

                FormView FvName_sub = FvsubBOM;
                OntxtPrice(FvName_sub);
                break;
            #endregion cal_price_and_vat_sub
            #region cal_price_and_vat_edit
            case "cal_price_and_vat_edit":

                FormView FvName_edit = FvsubBOM;

                LinkButton ddlName = (LinkButton)sender;
                GridViewRow gridrow = (GridViewRow)ddlName.NamingContainer;
                int rowIndex = gridrow.RowIndex;
                OntxtPrice_edit(gridrow);
                break;
            #endregion cal_price_and_vat_edit
            #region Cmd_edit_bom_name
            case "Cmd_edit_bom_name":
                set_control_bomname("edit");
                break;
            #endregion Cmd_edit_bom_name
            #region Cmd_cc_bom_name
            case "Cmd_cc_bom_name":
                set_control_bomname("cancel");
                break;
            #endregion Cmd_cc_bom_name
            #region Cmd_save_bom_name
            case "Cmd_save_bom_name":
                set_control_bomname("save");
                break;
                #endregion Cmd_save_bom_name
        }
    }
    protected void set_control_bomname(string tname)
    {
        TextBox tex_name_ed = (TextBox)FvInsert.FindControl("txtBOM");
        TextBox tbDateStart_ed = (TextBox)FvInsert.FindControl("tbDateStart");
        TextBox tbDateEnd_ed = (TextBox)FvInsert.FindControl("tbDateEnd");
        LinkButton Cmd_save_bom_name = (LinkButton)FvInsert.FindControl("Cmd_save_bom_name");
        LinkButton Cmd_edit_bom_name = (LinkButton)FvInsert.FindControl("Cmd_edit_bom_name");
        LinkButton Cmd_cc_bom_name = (LinkButton)FvInsert.FindControl("Cmd_cc_bom_name");


        tex_name_ed.ReadOnly = true;
        tbDateStart_ed.ReadOnly = true;
        tbDateEnd_ed.ReadOnly = true;
        Cmd_save_bom_name.Visible = false;
        Cmd_edit_bom_name.Visible = false;
        Cmd_cc_bom_name.Visible = false;
        DataSet temp_date = (DataSet)ViewState["dateSample"];
        var log_temp_data = temp_date.Tables[0].Rows;
        switch (tname)
        {
            case "edit":
                tex_name_ed.ReadOnly = false;
                tbDateStart_ed.ReadOnly = false;
                tbDateEnd_ed.ReadOnly = false;
                Cmd_save_bom_name.Visible = true;
                Cmd_edit_bom_name.Visible = false;
                Cmd_cc_bom_name.Visible = true;
                break;
            case "save":
              
                log_temp_data[0]["startdate"] = tbDateStart_ed.Text.Trim();
                log_temp_data[0]["enddate"] = tbDateEnd_ed.Text.Trim();
                ViewState["bom_name"] =  tex_name_ed.Text.Trim();
                Cmd_save_bom_name.Visible = false;
                Cmd_edit_bom_name.Visible = true;
                Cmd_cc_bom_name.Visible = false;
                ViewState["dateSample"] = temp_date;
                break;
            case "cancel":


                tbDateStart_ed.Text = log_temp_data[0]["startdate"].ToString();
                tbDateEnd_ed.Text = log_temp_data[0]["enddate"].ToString();
                tex_name_ed.Text = ViewState["bom_name"].ToString();
                Cmd_save_bom_name.Visible = false;
                Cmd_edit_bom_name.Visible = true;
                Cmd_cc_bom_name.Visible = false;
                break;
            default:
                Cmd_save_bom_name.Visible = false;
                Cmd_edit_bom_name.Visible = true;
                Cmd_cc_bom_name.Visible = false;
                break;
        }
    }
    #endregion btnCommand
    protected void add_detail_to_viewstate()
    {
        FormView FvName = FvInsert;
        TextBox tex_name = (TextBox)FvName.FindControl("txtBOM");

        TextBox tex_proname = (TextBox)FvName.FindControl("txtProductname");
        TextBox txtmaterial = (TextBox)FvName.FindControl("txtmaterial");
        TextBox txtunit = (TextBox)FvName.FindControl("txtunit");

        TextBox txtprice = (TextBox)FvName.FindControl("txtprice");
        TextBox txtdiscount_r = (TextBox)FvName.FindControl("txtdiscount_insert");

        TextBox txtdtxtPromotion = (TextBox)FvName.FindControl("txtPromotionID");
        TextBox txtPromotionDesc = (TextBox)FvName.FindControl("txtPromotionDesc");
        TextBox txtTypepromotion = (TextBox)FvName.FindControl("txtTypepromotion");

        TextBox txtSAPcode = (TextBox)FvName.FindControl("txtSAPcode");
        TextBox txtVAT = (TextBox)FvName.FindControl("txttex");
        TextBox txtamount = (TextBox)FvName.FindControl("txtamount");

        TextBox tbDateStart = (TextBox)FvName.FindControl("tbDateStart");
        TextBox tbDateEnd = (TextBox)FvName.FindControl("tbDateEnd");
        DropDownList ddSubstatus = (DropDownList)FvName.FindControl("ddSubstatus");

        string bom_name = ViewState["bom_name"].ToString();
        if (bom_name.ToString() == String.Empty)
        {
            ViewState["bom_name"] = tex_name.Text.Trim();

            DataSet dateSample = (DataSet)ViewState["dateSample"];
            var log_temp_data = dateSample.Tables[0].NewRow();
            log_temp_data["startdate"] = tbDateStart.Text;
            log_temp_data["enddate"] = tbDateEnd.Text;

            dateSample.Tables[0].Rows.Add(log_temp_data);
            ViewState["dateSample"] = dateSample;
        }

        var ds_Add_Sample = (DataSet)ViewState["vsSample"];
        var dr_Add_Sample = ds_Add_Sample.Tables[0].NewRow();
        int numrow = ds_Add_Sample.Tables[0].Rows.Count;

        dr_Add_Sample["product_name"] = tex_proname.Text.Trim();
        dr_Add_Sample["material"] = txtmaterial.Text.Trim();
        dr_Add_Sample["unit_name"] = txtunit.Text.Trim();
        dr_Add_Sample["price"] = _funcTool.convertToDecimal(txtprice.Text.Trim());
        dr_Add_Sample["discount"] = _funcTool.convertToDecimal(txtdiscount_r.Text.Trim());
        dr_Add_Sample["PromotionID"] = txtdtxtPromotion.Text.Trim();
        dr_Add_Sample["PromotionDesc"] = txtPromotionDesc.Text.Trim();
        dr_Add_Sample["SAP_code"] = txtSAPcode.Text.Trim();
        dr_Add_Sample["VAT"] = _funcTool.convertToDecimal(txtVAT.Text.Trim());
        dr_Add_Sample["amount"] = _funcTool.convertToInt(txtamount.Text.Trim());
        dr_Add_Sample["bom_m1_status"] = _funcTool.convertToInt(ddSubstatus.SelectedValue);

        dr_Add_Sample["type_promotion"] = _funcTool.convertToInt(txtTypepromotion.Text.Trim());
        //dr_Add_Sample["cemp_idx"] = tex_proname.Text.Trim();


        ds_Add_Sample.Tables[0].Rows.Add(dr_Add_Sample);
        ViewState["vsSample"] = ds_Add_Sample;


        //GridView GvSample = (GridView)FvInsert.FindControl("GvSample");
        select_data_temp();

    }
    protected void refresh_m1()
    {
        src_material.Text = String.Empty;
        ddsrc_status.Text = "999";
    }
    protected void refresh_m0()
    {
        txtmaster_search.Text = String.Empty;
        Src_status.Text = "999";
    }
    protected void set_viewstate_src()
    {

        data_ecom_fileinterface = new data_ecom_fileinterface(); ;

        ecom_m1_data_bom m1_detail = new ecom_m1_data_bom();
        data_ecom_fileinterface.ecom_m1_data_bom_list = new ecom_m1_data_bom[1];

        m1_detail.material = src_material.Text;
        m1_detail.bom_m1_status = _funcTool.convertToInt(ddsrc_status.SelectedValue);
        ViewState["src_m1"] = m1_detail;
    }
    protected void set_viewstate_src_m0()
    {

        data_ecom_fileinterface = new data_ecom_fileinterface();
        ecom_m0_data_bom m0_detail = new ecom_m0_data_bom();

        data_ecom_fileinterface.ecom_m0_data_bom_list = new ecom_m0_data_bom[1];

        m0_detail.bom_name = txtmaster_search.Text;
        m0_detail.bom_status = _funcTool.convertToInt(Src_status.SelectedValue);
        ViewState["src_m0"] = m0_detail;
    }
    protected void select_data_temp()
    {
        setGridData(GvSample, (DataSet)ViewState["vsSample"]);
    }
    protected void Select_Place()
    {
        //TextBox txtmaster_search = (TextBox)FvInsert.FindControl("txtmaster_search");
        //DropDownList dropD_status_place = (DropDownList)FvInsert.FindControl("Src_status");

        data_ecom_fileinterface = new data_ecom_fileinterface();
        ecom_m0_data_bom m0_detail = new ecom_m0_data_bom();

        data_ecom_fileinterface.ecom_m0_data_bom_list = new ecom_m0_data_bom[1];
        if (ViewState["src_m0"] != null)
        {
            m0_detail = (ecom_m0_data_bom)ViewState["src_m0"];
        }
        else
        {
            m0_detail.bom_status = 999;
        }
        //m0_detail.bom_name = txtmaster_search.Text;
        //m0_detail.bom_status = _funcTool.convertToInt(Src_status.SelectedValue);
        data_ecom_fileinterface.ecom_m0_data_bom_list[0] = m0_detail;


        data_ecom_fileinterface = callServicePostAPI(_urlGetEcomM0BOM, data_ecom_fileinterface);
        // litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_ecom_fileinterface));
        setGridData(Gv_select_place, data_ecom_fileinterface.ecom_m0_data_bom_list);
        ViewState["data_m0"] = data_ecom_fileinterface.ecom_m0_data_bom_list;

    }
    protected void Insert_Place()
    {

        TextBox tex_name = (TextBox)FvInsert.FindControl("txtBOM");
        //TextBox txtmaterial = (TextBox)FvInsert.FindControl("txtmaterial");
        DropDownList dropD_status_place = (DropDownList)FvInsert.FindControl("ddPlace");
        //litdebug.Text = tex_place_name.Text + dropD_status_place.SelectedValue;
        data_ecom_fileinterface = new data_ecom_fileinterface();
        ecom_m0_data_bom m0_detail = new ecom_m0_data_bom();
        data_ecom_fileinterface.ecom_m0_data_bom_list = new ecom_m0_data_bom[1];

        m0_detail.bom_name = tex_name.Text.Trim();
        //m0_detail.material = txtmaterial.Text;
        m0_detail.bom_status = _funcTool.convertToInt(dropD_status_place.SelectedValue);
        m0_detail.cemp_idx = _emp_idx;

        data_ecom_fileinterface.ecom_m0_data_bom_list[0] = m0_detail;

        data_ecom_fileinterface = callServicePostAPI(_urlSetEcomM0BOM, data_ecom_fileinterface);
        // litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_ecom_fileinterface));
        if (data_ecom_fileinterface.return_code == 1)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ข้อมูลซ้ำ กรุณาแก้ไขใหม่อีกครั้ง!!!');", true);
            div_searchmaster_page.Visible = false;
        }
        else
        {
            btn_addplace.Visible = true;
            div_insert.Visible = false;
            div_searchmaster_page.Visible = true;
            refresh_m0();
            set_viewstate_src_m0();
        }

    }
    protected void insert_m0()
    {

        //ViewState["vsSample"] = ds_sample_data;
        //ViewState["bom_name"] = String.Empty;
        //ViewState["dateSample"] = ds_sample_date;
        if (ViewState["bom_name"].ToString() == String.Empty)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณากรอกข้อมูล ให้ครบถ้วน!!!');", true);
        }
        else
        {
            data_ecom_fileinterface = new data_ecom_fileinterface();
            ecom_m0_data_bom m0_detail = new ecom_m0_data_bom();

            data_ecom_fileinterface.ecom_m0_data_bom_list = new ecom_m0_data_bom[1];

            DataSet temp_date = (DataSet)ViewState["dateSample"];
            var log_temp_data = temp_date.Tables[0].Rows;

            DataSet ds_Sample = (DataSet)ViewState["vsSample"];
            var dr_Sample = ds_Sample.Tables[0].NewRow();
            int numrow = ds_Sample.Tables[0].Rows.Count;

            m0_detail.bom_name = ViewState["bom_name"].ToString();
            m0_detail.start_date = log_temp_data[0]["startdate"].ToString();
            m0_detail.end_date = log_temp_data[0]["enddate"].ToString();
            m0_detail.bom_status = 1;
            m0_detail.cemp_idx = _emp_idx;
            data_ecom_fileinterface.ecom_m0_data_bom_list[0] = m0_detail;
            ecom_m1_data_bom[] m1_detail = new ecom_m1_data_bom[numrow];
            if (numrow > 0)
            {
                //data_ecom_fileinterface.ecom_m1_data_bom_list = new ecom_m1_data_bom[numrow];
                int i = 0;
                foreach (DataRow detail in ds_Sample.Tables[0].Rows)
                {
                    m1_detail[i] = new ecom_m1_data_bom();
                    m1_detail[i].product_name = detail["product_name"].ToString();

                    m1_detail[i].material = detail["material"].ToString();

                    m1_detail[i].unit_name = detail["unit_name"].ToString();

                    m1_detail[i].price = _funcTool.convertToDecimal(detail["price"].ToString());

                    m1_detail[i].discount = _funcTool.convertToDecimal(detail["discount"].ToString());
                    m1_detail[i].VAT = _funcTool.convertToDecimal(detail["VAT"].ToString());

                    m1_detail[i].PromotionID = detail["PromotionID"].ToString();

                    m1_detail[i].PromotionDesc = detail["PromotionDesc"].ToString();
                    m1_detail[i].type_promotion = _funcTool.convertToInt(detail["type_promotion"].ToString());

                    m1_detail[i].SAP_code = detail["SAP_code"].ToString();

                    m1_detail[i].amount = _funcTool.convertToInt(detail["amount"].ToString());
                    m1_detail[i].bom_m1_status = _funcTool.convertToInt(detail["bom_m1_status"].ToString());


                    m1_detail[i].cemp_idx = _emp_idx;
                    i++;
                }

                data_ecom_fileinterface.ecom_m1_data_bom_list = m1_detail;
            }
            // litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_ecom_fileinterface));
            //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_ecom_fileinterface));
            data_ecom_fileinterface = callServicePostAPI(_urlSetEcomM0BOM, data_ecom_fileinterface);
            if (data_ecom_fileinterface.return_code == 1)
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ข้อมูลซ้ำ หรือมีช่วงเวลาทับซ้อนกัน กรุณาแก้ไขใหม่อีกครั้ง!!!');", true);
                div_searchmaster_page.Visible = false;
            }
            else
            {
                btn_addplace.Visible = true;
                div_insert.Visible = false;
                div_searchmaster_page.Visible = true;
                refresh_m0();
                set_viewstate_src_m0();
                clear_temp_data();
            }
        }
    }

    protected void Insert_subBOM()
    {

        TextBox tex_name = (TextBox)FvsubBOM.FindControl("txtProductname");
        TextBox txtmaterial = (TextBox)FvsubBOM.FindControl("txtmaterial");
        TextBox txtunit = (TextBox)FvsubBOM.FindControl("txtunit");

        TextBox txtprice = (TextBox)FvsubBOM.FindControl("txtprice");
        TextBox txtdiscount = (TextBox)FvsubBOM.FindControl("txtdiscount_insert");

        TextBox txtdtxtPromotion = (TextBox)FvsubBOM.FindControl("txtPromotionID");
        TextBox txtTypepromotion = (TextBox)FvsubBOM.FindControl("txtTypepromotion");
        TextBox txtPromotionDesc = (TextBox)FvsubBOM.FindControl("txtPromotionDesc");
        TextBox txtSAPcode = (TextBox)FvsubBOM.FindControl("txtSAPcode");
        TextBox txtamount = (TextBox)FvsubBOM.FindControl("txtamount");
        TextBox txtVATAmount = (TextBox)FvsubBOM.FindControl("txttex");
        DropDownList dropD_status_place = (DropDownList)FvsubBOM.FindControl("ddPlace");

        data_ecom_fileinterface = new data_ecom_fileinterface();
        ecom_m1_data_bom m1_detail = new ecom_m1_data_bom();
        data_ecom_fileinterface.ecom_m1_data_bom_list = new ecom_m1_data_bom[1];

        m1_detail.bom_m0_idx = _funcTool.convertToInt(ViewState["bom_m0_idx"].ToString());
        m1_detail.product_name = tex_name.Text.Trim();
        m1_detail.material = txtmaterial.Text.Trim();
        m1_detail.unit_name = txtunit.Text.Trim();

        m1_detail.PromotionID = txtdtxtPromotion.Text.Trim();
        m1_detail.PromotionDesc = txtPromotionDesc.Text.Trim();
        m1_detail.SAP_code = txtSAPcode.Text.Trim();

        m1_detail.type_promotion = _funcTool.convertToInt(txtTypepromotion.Text.Trim());
        m1_detail.price = _funcTool.convertToDecimal(txtprice.Text.Trim());
        m1_detail.discount = _funcTool.convertToDecimal(txtdiscount.Text.Trim());
        m1_detail.amount = _funcTool.convertToInt(txtamount.Text.Trim());
        m1_detail.bom_m1_status = _funcTool.convertToInt(dropD_status_place.SelectedValue);

        m1_detail.VAT = _funcTool.convertToDecimal(txtVATAmount.Text.Trim());
        m1_detail.cemp_idx = _emp_idx;

        data_ecom_fileinterface.ecom_m1_data_bom_list[0] = m1_detail;
        // litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_ecom_fileinterface));
        data_ecom_fileinterface = callServicePostAPI(_urlSetEcomM1BOM, data_ecom_fileinterface);

        if (data_ecom_fileinterface.return_code == 1)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ข้อมูลซ้ำ กรุณาแก้ไขใหม่อีกครั้ง!!!');", true);
        }
        else
        {
            btn_adddetail.Visible = true;
            FvsubBOM.Visible = false;
            sreach_detailbox.Visible = true;
            refresh_m1();
            set_viewstate_src();
        }

    }

    protected void Select_BOM_m1()
    {
        //TextBox txtmaster_search = (TextBox)FvInsert.FindControl("txtmaster_search");
        //DropDownList dropD_status_place = (DropDownList)FvInsert.FindControl("Src_status");

        data_ecom_fileinterface = new data_ecom_fileinterface();
        ecom_m1_data_bom m1_detail = new ecom_m1_data_bom();
        data_ecom_fileinterface.ecom_m1_data_bom_list = new ecom_m1_data_bom[1];
        if (ViewState["src_m1"] != null)
        {
            //ecom_m1_data_bom m1_detail_src = (ecom_m1_data_bom)ViewState["src_m1"];
            m1_detail = (ecom_m1_data_bom)ViewState["src_m1"];
        }
        else
        {
            m1_detail.bom_m1_status = 999;
        }


        m1_detail.bom_m0_idx = _funcTool.convertToInt(ViewState["bom_m0_idx"].ToString());
        //m1_detail.material = src_material.Text;
        //m1_detail.bom_m1_status = _funcTool.convertToInt(ddsrc_status.SelectedValue);
        data_ecom_fileinterface.ecom_m1_data_bom_list[0] = m1_detail;

        //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_ecom_fileinterface));
        data_ecom_fileinterface = callServicePostAPI(_urlGetEcomM1BOM, data_ecom_fileinterface);

        // litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_ecom_fileinterface));
        setGridData(Gvm1, data_ecom_fileinterface.ecom_m1_data_bom_list);


    }

    protected void clear_temp_data()
    {
        ViewState["vsSample"] = string.Empty;
        var ds_sample_data = new DataSet();
        ds_sample_data.Tables.Add("SampleData");
        ds_sample_data.Tables[0].Columns.Add("product_name", typeof(String));
        ds_sample_data.Tables[0].Columns.Add("material", typeof(String));
        ds_sample_data.Tables[0].Columns.Add("unit_name", typeof(String));
        ds_sample_data.Tables[0].Columns.Add("price", typeof(Decimal));
        ds_sample_data.Tables[0].Columns.Add("discount", typeof(Decimal));
        ds_sample_data.Tables[0].Columns.Add("PromotionID", typeof(String));
        ds_sample_data.Tables[0].Columns.Add("PromotionDesc", typeof(String));
        ds_sample_data.Tables[0].Columns.Add("SAP_code", typeof(String));
        ds_sample_data.Tables[0].Columns.Add("amount", typeof(int));
        ds_sample_data.Tables[0].Columns.Add("VAT", typeof(Decimal));
        ds_sample_data.Tables[0].Columns.Add("bom_m1_status", typeof(int));
        ds_sample_data.Tables[0].Columns.Add("type_promotion", typeof(int));

        ViewState["vsSample"] = ds_sample_data;
        ViewState["bom_name"] = String.Empty;

        DataSet ds_sample_date = new DataSet();
        ds_sample_date.Tables.Add("dateSampledata");
        ds_sample_date.Tables[0].Columns.Add("startdate", typeof(String));
        ds_sample_date.Tables[0].Columns.Add("enddate", typeof(String));
        ViewState["dateSample"] = ds_sample_date;

        GvSample.DataSource = null;
        GvSample.DataBind();
        GvSample.Visible = true;
    }
    #region setformdata
    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }
    #endregion setformdata

    #region setGridData
    private void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
        gvName.Visible = true;
    }
    #endregion

    protected void ddl_material_list(DropDownList ddlName)
    {
        data_ecom_fileinterface = new data_ecom_fileinterface();
        //data_ecom_fileinterface.ecom_m0_material_list = new ecom_m0_material[1];
        //ecom_m0_material _data = new ecom_m0_material();

        //data_ecom_fileinterface.ecom_m0_material_list[0] = _data;

        data_ecom_fileinterface = callServicePostAPI(_urlGetEcomM0MAT, data_ecom_fileinterface);
        //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_ecom_fileinterface));
        ViewState["material_m0"] = data_ecom_fileinterface.ecom_m0_material_list;
        setDdlData(ddlName, data_ecom_fileinterface.ecom_m0_material_list, "name_and_material", "material_id");
        ddlName.Items.Insert(0, new ListItem("เลือกรหัส Material....", "0"));
    }
    protected void ddl_promotion_list(DropDownList ddlName)
    {
        data_ecom_promotion data_ecom_pro = new data_ecom_promotion();
        data_ecom_pro.ecom_m0_promotion_list = new ecom_m0_promotion[1];
        ecom_m0_promotion _data = new ecom_m0_promotion();
        _data.pro_status = 1;
        data_ecom_pro.ecom_m0_promotion_list[0] = _data;
        //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_ecom_pro));
        data_ecom_pro = callServicePostAPIpro(_urlGetEcomM0Promotion, data_ecom_pro);

        ViewState["promotion_m0"] = data_ecom_pro.ecom_m0_promotion_list;
        setDdlData(ddlName, data_ecom_pro.ecom_m0_promotion_list, "sap_and_desc", "pro_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกรหัส Promotion....", "0"));
    }
    protected void OnSelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlName = (DropDownList)sender;

        switch (ddlName.ID)
        {
            case "ddl_material":
                ecom_m0_material[] _tempe_m0_mat = (ecom_m0_material[])ViewState["material_m0"];
                var _ViewState_m0 = (from m0_temp in _tempe_m0_mat
                                     where m0_temp.material_id.ToString() == ddlName.SelectedValue.ToString()
                                     select new
                                     {
                                         m0_temp.material_name,
                                         m0_temp.material
                                     }).ToList();
                TextBox tex_name = (TextBox)FvInsert.FindControl("txtProductname");
                TextBox txtmaterial = (TextBox)FvInsert.FindControl("txtmaterial");
                if (_ViewState_m0.Count() > 0)
                {

                    tex_name.Text = _ViewState_m0[0].material_name.ToString();
                    txtmaterial.Text = _ViewState_m0[0].material.ToString();
                }
                else
                {
                    tex_name.Text = string.Empty;
                    txtmaterial.Text = string.Empty;
                }
                break;
            case "ddl_material_m1":
                ecom_m0_material[] _tempe_m1_mat = (ecom_m0_material[])ViewState["material_m0"];
                var _ViewState_m1 = (from m0_temp in _tempe_m1_mat
                                     where m0_temp.material_id.ToString() == ddlName.SelectedValue.ToString()
                                     select new
                                     {
                                         m0_temp.material_name,
                                         m0_temp.material
                                     }).ToList();
                TextBox tex_name_m1 = (TextBox)FvsubBOM.FindControl("txtProductname");
                TextBox txtmaterial_m1 = (TextBox)FvsubBOM.FindControl("txtmaterial");
                if (_ViewState_m1.Count() > 0)
                {

                    tex_name_m1.Text = _ViewState_m1[0].material_name.ToString();
                    txtmaterial_m1.Text = _ViewState_m1[0].material.ToString();
                }
                else
                {
                    tex_name_m1.Text = string.Empty;
                    txtmaterial_m1.Text = string.Empty;
                }
                break;
            case "ddl_material_m1_edit":
                ecom_m0_material[] _tempe_m1_edit = (ecom_m0_material[])ViewState["material_m0"];
                GridViewRow gridrow = (GridViewRow)ddlName.NamingContainer;
                int rowIndex = gridrow.RowIndex;

                litdebug.Text = rowIndex.ToString();
                var _ViewState_m1_edit = (from m0_temp in _tempe_m1_edit
                                          where m0_temp.material_id.ToString() == ddlName.SelectedValue.ToString()
                                          select new
                                          {
                                              m0_temp.material_name,
                                              m0_temp.material
                                          }).ToList();
                TextBox tex_name_m1_edit = (TextBox)Gvm1.Rows[rowIndex].FindControl("txtproduct_name");
                TextBox txtmaterial_m1_edit = (TextBox)Gvm1.Rows[rowIndex].FindControl("txtmaterial");
                if (_ViewState_m1_edit.Count() > 0)
                {

                    tex_name_m1_edit.Text = _ViewState_m1_edit[0].material_name.ToString();
                    txtmaterial_m1_edit.Text = _ViewState_m1_edit[0].material.ToString();
                }
                else
                {
                    tex_name_m1_edit.Text = string.Empty;
                    txtmaterial_m1_edit.Text = string.Empty;
                }
                break;
            case "ddl_promotion":
                ecom_m0_promotion[] _tempe_m0_pro = (ecom_m0_promotion[])ViewState["promotion_m0"];
                var _ViewState_m0_pro = (from m0_temp in _tempe_m0_pro
                                         where m0_temp.pro_idx.ToString() == ddlName.SelectedValue.ToString()
                                         select new
                                         {
                                             m0_temp.PromotionDesc,
                                             m0_temp.PromotionID
                                             ,
                                             m0_temp.ZAP_code
                                             ,
                                             m0_temp.amount
                                             ,
                                             m0_temp.type_promotion
                                         }).ToList();
                TextBox txtdiscount_insert = (TextBox)FvInsert.FindControl("txtdiscount_insert");
                TextBox txtPromotionID = (TextBox)FvInsert.FindControl("txtPromotionID");
                TextBox txtPromotionDesc = (TextBox)FvInsert.FindControl("txtPromotionDesc");
                TextBox txtSAPcode = (TextBox)FvInsert.FindControl("txtSAPcode");
                TextBox txtTypepromotion = (TextBox)FvInsert.FindControl("txtTypepromotion");
                if (_ViewState_m0_pro.Count() > 0)
                {

                    txtdiscount_insert.Text = _ViewState_m0_pro[0].amount.ToString();
                    txtPromotionID.Text = _ViewState_m0_pro[0].PromotionID.ToString();
                    txtPromotionDesc.Text = _ViewState_m0_pro[0].PromotionDesc.ToString();
                    txtSAPcode.Text = _ViewState_m0_pro[0].ZAP_code.ToString();

                    txtTypepromotion.Text = _ViewState_m0_pro[0].type_promotion.ToString();
                }
                else
                {
                    txtdiscount_insert.Text = string.Empty;
                    txtPromotionID.Text = string.Empty;
                    txtPromotionDesc.Text = string.Empty;
                    txtSAPcode.Text = string.Empty;
                    txtTypepromotion.Text = string.Empty;
                }

                break;

            case "ddl_promotion_m1":
                ecom_m0_promotion[] _tempe_m0_pro_m1 = (ecom_m0_promotion[])ViewState["promotion_m0"];
                var _ViewState_m1_pro = (from m0_temp in _tempe_m0_pro_m1
                                         where m0_temp.pro_idx.ToString() == ddlName.SelectedValue.ToString()
                                         select new
                                         {
                                             m0_temp.PromotionDesc
                                             ,
                                             m0_temp.PromotionID
                                             ,
                                             m0_temp.ZAP_code
                                             ,
                                             m0_temp.amount
                                             ,
                                             m0_temp.type_promotion
                                         }).ToList();
                TextBox txtdiscount_bom = (TextBox)FvsubBOM.FindControl("txtdiscount_insert");
                TextBox txtPromotionID_m1 = (TextBox)FvsubBOM.FindControl("txtPromotionID");
                TextBox txtPromotionDesc_m1 = (TextBox)FvsubBOM.FindControl("txtPromotionDesc");
                TextBox txtSAPcode_m1 = (TextBox)FvsubBOM.FindControl("txtSAPcode");
                TextBox txtTypepromotion_m1 = (TextBox)FvsubBOM.FindControl("txtTypepromotion");
                if (_ViewState_m1_pro.Count() > 0)
                {
                    txtdiscount_bom.Text = _ViewState_m1_pro[0].amount.ToString();
                    txtPromotionID_m1.Text = _ViewState_m1_pro[0].PromotionID.ToString();
                    txtPromotionDesc_m1.Text = _ViewState_m1_pro[0].PromotionDesc.ToString();
                    txtSAPcode_m1.Text = _ViewState_m1_pro[0].ZAP_code.ToString();
                    txtTypepromotion_m1.Text = _ViewState_m1_pro[0].type_promotion.ToString();
                }
                else
                {
                    txtdiscount_bom.Text = string.Empty;
                    txtPromotionID_m1.Text = string.Empty;
                    txtPromotionDesc_m1.Text = string.Empty;
                    txtSAPcode_m1.Text = string.Empty;
                    txtTypepromotion_m1.Text = string.Empty;
                }
                break;
            case "ddl_promotion_m1_edit":
                GridViewRow gridrow_edit = (GridViewRow)ddlName.NamingContainer;
                int rowIndex_e = gridrow_edit.RowIndex;
                ecom_m0_promotion[] _tempe_pro_m1_edit = (ecom_m0_promotion[])ViewState["promotion_m0"];
                var _ViewState_m1_pro_edit = (from m0_temp in _tempe_pro_m1_edit
                                              where m0_temp.pro_idx.ToString() == ddlName.SelectedValue.ToString()
                                              select new
                                              {
                                                  m0_temp.PromotionDesc,
                                                  m0_temp.PromotionID
                                                  ,
                                                  m0_temp.ZAP_code
                                                  ,
                                                  m0_temp.amount
                                                   ,
                                                  m0_temp.type_promotion
                                              }).ToList();
                TextBox txtdiscount_bom_edit = (TextBox)Gvm1.Rows[rowIndex_e].FindControl("txtdiscount");
                TextBox txtPromotionID_edit = (TextBox)Gvm1.Rows[rowIndex_e].FindControl("txtPromotionID");
                TextBox txtPromotionDesc_edit = (TextBox)Gvm1.Rows[rowIndex_e].FindControl("txtPromotionDesc");
                TextBox txtTypepromotion_edit = (TextBox)Gvm1.Rows[rowIndex_e].FindControl("txtTypepromotion");
                TextBox txtSAPcode_edit = (TextBox)Gvm1.Rows[rowIndex_e].FindControl("txtSAPcode");
                if (_ViewState_m1_pro_edit.Count() > 0)
                {

                    txtdiscount_bom_edit.Text = _ViewState_m1_pro_edit[0].amount.ToString();
                    txtPromotionID_edit.Text = _ViewState_m1_pro_edit[0].PromotionID.ToString();
                    txtPromotionDesc_edit.Text = _ViewState_m1_pro_edit[0].PromotionDesc.ToString();
                    txtSAPcode_edit.Text = _ViewState_m1_pro_edit[0].ZAP_code.ToString();
                    txtTypepromotion_edit.Text = _ViewState_m1_pro_edit[0].type_promotion.ToString();
                }
                else
                {
                    txtdiscount_bom_edit.Text = string.Empty;
                    txtPromotionID_edit.Text = string.Empty;
                    txtPromotionDesc_edit.Text = string.Empty;
                    txtSAPcode_edit.Text = string.Empty;
                    txtTypepromotion_edit.Text = string.Empty;
                }
                break;
        }


    }

    protected void OntxtPrice(FormView FvName)
    {

        TextBox txtqty = (TextBox)FvName.FindControl("txtamount");
        TextBox txttex = (TextBox)FvName.FindControl("txttex");
        TextBox txtprice = (TextBox)FvName.FindControl("txtprice");
        TextBox txtdiscount = (TextBox)FvName.FindControl("txtdiscount_insert");

        Label txtVATAmount = (Label)FvName.FindControl("txtVATAmount");
        Label txtPricemount = (Label)FvName.FindControl("txtNetPriceAmount");

        txtVATAmount.Text = txtVATAmount.ToolTip = (_funcTool.convertToDecimal(txttex.Text) * _funcTool.convertToInt(txtqty.Text)).ToString();

        Decimal price = (_funcTool.convertToDecimal(txtprice.Text) - _funcTool.convertToDecimal(txtdiscount.Text)) * _funcTool.convertToInt(txtqty.Text);
        //if (price > 0)
        //{
        //    price = price - _funcTool.convertToDecimal(txtVATAmount.Text);
        //}
        txtPricemount.Text = txtPricemount.ToolTip = price.ToString();
    }
    protected void OntxtPrice_edit(GridViewRow gridrow)
    {
        int rowIndex = gridrow.RowIndex;

        TextBox txtqty = (TextBox)Gvm1.Rows[rowIndex].FindControl("txtamount");
        TextBox txttex = (TextBox)Gvm1.Rows[rowIndex].FindControl("txttex");
        TextBox txtprice = (TextBox)Gvm1.Rows[rowIndex].FindControl("txtprice");
        TextBox txtdiscount = (TextBox)Gvm1.Rows[rowIndex].FindControl("txtdiscount");

        Label txtVATAmount = (Label)Gvm1.Rows[rowIndex].FindControl("txtVATAmount");
        Label txtPricemount = (Label)Gvm1.Rows[rowIndex].FindControl("txtNetPriceAmount");

        txtVATAmount.Text = txtVATAmount.ToolTip = (_funcTool.convertToDecimal(txttex.Text) * _funcTool.convertToInt(txtqty.Text)).ToString();

        Decimal price = (_funcTool.convertToDecimal(txtprice.Text) - _funcTool.convertToDecimal(txtdiscount.Text)) * _funcTool.convertToInt(txtqty.Text);
        //if (price > 0)
        //{
        //    price = price - _funcTool.convertToDecimal(txtVATAmount.Text);
        //}
        txtPricemount.Text = txtPricemount.ToolTip = price.ToString();
    }
    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    #region callService 
    //protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    //{
    //    // call services
    //    _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

    //    // convert json to object
    //    _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

    //    return _dataEmployee;
    //}

    protected data_ecom_promotion callServicePostAPIpro(string _cmdUrl, data_ecom_promotion _data)
    {
        _localJson = _funcTool.convertObjectToJson(_data);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data = (data_ecom_promotion)_funcTool.convertJsonToObject(typeof(data_ecom_promotion), _localJson);


        return _data;
    }
    protected data_ecom_fileinterface callServicePostAPI(string _cmdUrl, data_ecom_fileinterface _data)
    {
        _localJson = _funcTool.convertObjectToJson(_data);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data = (data_ecom_fileinterface)_funcTool.convertJsonToObject(typeof(data_ecom_fileinterface), _localJson);


        return _data;
    }
    #endregion callService Functions
}