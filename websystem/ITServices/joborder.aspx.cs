﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;


public partial class websystem_ITServices_Default : System.Web.UI.Page
{
    #region init
  

    function_tool _funcTool = new function_tool();
    data_jo _data_jo = new data_jo();
    string _local_xml = string.Empty;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlSelectDatajoborder = _serviceUrl + ConfigurationManager.AppSettings["urlSelectDatajoborder"];
    static string _urlInsertDatajoborder = _serviceUrl + ConfigurationManager.AppSettings["urlInsertDatajoborder"];
    static string _urlEditDatajoborder = _serviceUrl + ConfigurationManager.AppSettings["urlEditDatajoborder"];
    static string _urlUpdateDatajoborder = _serviceUrl + ConfigurationManager.AppSettings["urlUpdateDatajoborder"];
    static string _urlDeleteDatajoborder = _serviceUrl + ConfigurationManager.AppSettings["urlDeleteDatajoborder"];
    static string _urlSelectDatajoborder_mhead = _serviceUrl + ConfigurationManager.AppSettings["urlSelectDatajoborder_mhead"];
static string _urlSelectDatajoborder_log = _serviceUrl + ConfigurationManager.AppSettings["urlSelectDatajoborder_log"];
static string _urlUpdatestau0 = _serviceUrl + ConfigurationManager.AppSettings["urlUpdatestau0"];
static string _urlUpdatedaystartfinal = _serviceUrl + ConfigurationManager.AppSettings["urlUpdatedaystartfinal"];
static string _urlSelectdaystartfinal = _serviceUrl + ConfigurationManager.AppSettings["urlSelectdaystartfinal"];
static string _urlSelect_mishead = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_mishead"];
static string _urlInsertloggaritum = _serviceUrl + ConfigurationManager.AppSettings["urlInsertloggaritum"];
static string _urlSelectU1ov = _serviceUrl + ConfigurationManager.AppSettings["urlSelectU1ov"];

    string _localJson = "";
    int _tempInt = 0;

    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        // timeone.Text = DateTime.Today.ToLongDateString();
        // ah.Text = DateTime.Today.ToLongDateString();
        // oh.Text = DateTime.Today.ToLongDateString();
        if(!IsPostBack){
        ViewState["EmpIDX"]= int.Parse(Session["emp_idx"].ToString());
        // jogrid.DataSource = null;
        jogrid.DataBind();
        jogrid2.DataBind();
        gridhome.DataBind();
        u1grid.DataBind();
        selectjob();
        }
        


    }
    protected void btnuser(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();
        
        int jo_idx;
        // int n;
         data_jo box = new data_jo();
        
        switch (cmdName)
        {

            case "submit1":


                // if (nameuse.Text == "" || nameuse.Text == null)
                // {

                //     ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณากรอก Requirement');", true);

                // }
                // // else if (titlename.Text == "" || titlename.Text == null)
                // // {

                // //     ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณากรอก หัวข้อหลัก');", true);
                // // }
                // else if (nameuse.Text == "" || nameuse.Text == null)
                // {

                //     ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาลงชื่อ');", true);
                // }
                // else
                // {
                    create.Visible=true;
                     grid.Visible = true;
                     searchbutton.Visible = true;
                  
                // if(title =="" || title == null){
                //     delete.Visible = false;
                // }
                    // ViewState["title"] = titlename.Text;
                    // ViewState["data"] = request.Text;
                    // ViewState["name"] = nameuse.Text;
                    // FirstMultiview.SetActiveView(View2);
                    // titlehost.Text = ViewState["title"].ToString();
                    // datahost.Text = ViewState["data"].ToString();
                // }
                break;
                case "submit4":
                
                FirstMultiview.SetActiveView(View2); 
               selectmheadjob();
                // selectjob();
                break;
                case "submithome":
                 FirstMultiview.SetActiveView(ViewHome); 
                 gridhome.Visible = true;
                //  selectjob();
                break;
                case "plusbut":
                insert.Visible=true;
                break;
                case "add":
                // var insertvalue = int.Parse(cmdArg);
                insertjob();
                selectjob();
                titlename.Text = String.Empty;
                require.Text = String.Empty;
                remoteuser.Visible = false;
                
                break;
                 case "click":
                 remoteuser.Visible = true;
                  create.Visible=false;
                //grid.Visible = true;
                break;
                case "close":
                remoteuser.Visible = false;
                 create.Visible=true;
                break;
                case "closemishead":
                //  int idx2 = int.Parse(cmdArg);
               
                 data_jo b = new data_jo();
                 job_order_overview b1 = new job_order_overview();
                b.job_order_overview_action = new job_order_overview[1];
                b1.u0_jo_idx = int.Parse(ViewState["id_idx"].ToString()); 
                b1.m0_actor_idx=int.Parse(ViewState["actor"].ToString()); //from_ac
                b1.m0_node_idx= int.Parse(ViewState["node"].ToString()); // from_M0_node
                b1.id_statework = 3;// node_decision

            
                // string[] arg = new string[1];
                // arg = e.CommandArgument.ToString().Split(';');
                // int  statenode_state = int.Parse(arg[0]);
                // ViewState["state"] =  statenode_state  ;

                // int statestatus = int.Parse(cmdArg);
                
                // var statestatus = int.Parse(cmdArg);
                // ViewState["state"] = statestatus;
                // b1.statenode_state = statestatus;
                // FirstMultiview.SetActiveView(View2);
                // if(b1.statenode_state==2){

                // }
                b.job_order_overview_action[0] = b1;
                 b = callServiceJobOrder(_urlUpdatestau0, b);
                //  ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาตกลงเพื่อยืนยัน');", true);
                // Page.Response.Redirect(Page.Request.Url.ToString(), true);
                
                break;
                case "closemishead2" : //case user ไม่ยอมรับ
                data_jo bg = new data_jo();
                job_order_overview sm = new job_order_overview();
                bg.job_order_overview_action = new job_order_overview[1];
                sm.u0_jo_idx = int.Parse(ViewState["id_idx"].ToString()); 
                sm.m0_actor_idx=1; //from_ac
                sm.m0_node_idx= int.Parse(ViewState["node"].ToString()); // from_M0_node
                sm.id_statework = 3;// node_decision
                bg.job_order_overview_action[0] = sm;
                bg = callServiceJobOrder(_urlUpdatestau0, bg);
                // ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาตกลงเพื่อยืนยัน');", true);
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
                case "del":
                // ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาตกลง');", true);
                jo_idx = int.Parse(cmdArg);
                ViewState["jo_idx"] = jo_idx;
                // p1.Text = ViewState["jo_idx"].ToString();
                deletejob();
                if(box.return_code == 0){
                selectjob();
                }
                break;
                case "reset":
                titlename.Text = null;
                require.Text = null;
                break;

                case "detail":
                    string[] arg1 = new string[7];
                    arg1 = e.CommandArgument.ToString().Split(';');
                    int id = int.Parse(arg1[0]);
                    int nodeid = int.Parse(arg1[1]);
                    int actorid = int.Parse(arg1[2]);
                    int statusid = int.Parse(arg1[3]);
                    string dstartid = arg1[4];
                    string dfinishid = arg1[5];
                    int logid = int.Parse(arg1[6]);
                    // int dfinishid = int.Parse(arg1[5]);
              
              
                    //  var id_idx = int.Parse(cmdArg);
                     FirstMultiview.SetActiveView(View3);
                     DetailUser.ChangeMode(FormViewMode.Insert);
                     DetailUser.DataBind();
                     
                    
                    //  Detaildate.ChangeMode(FormViewMode.Insert);
                    //  Detaildate.DataBind();
                    //  logidform.ChangeMode(FormViewMode.ReadOnly);
                    //  logidform.DataBind();
                     ViewState["id_idx"] = id;
                     ViewState["node"] = nodeid;
                     ViewState["actor"] = actorid;
                     ViewState["status"] = statusid;
                     ViewState["daystart"] = dstartid;
                     ViewState["dayfinish"] = dfinishid;
                     ViewState["jo_idx_ref"] = logid;
                     SelectViewIndex();
                 
                     
                    //setFormViewData(DetailUser,FormViewMode.ReadOnly,"");
                break;

                case "home":
                // p1.Text = "test";
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

                case "back":
                FirstMultiview.SetActiveView(View2);
                selectmheadjob();
                sd.Visible=false;
                 
                break;
               case "back2":
                 Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

                case "accept":
                data_jo bb1 = new data_jo();
                job_order_overview bb2 = new job_order_overview();
                bb1.job_order_overview_action = new job_order_overview[1];
                
                
                if(startdate.Text == finaldate.Text){
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาระบุวันที่อีกครั้ง');", true);
                }
                else{


                acceptnode();
                // insertlog();
                bb2.u0_jo_idx = int.Parse(ViewState["id_idx"].ToString()); //box2.ตัวแปรฝั่ง sql
                // bb2.m0_node_idx = int.Parse(ViewState["node"].ToString());
                // bb2.m0_actor_idx = int.Parse(ViewState["actor"].ToString());
                // bb2.id_statework = 2;
                bb2.day_start  = startdate.Text;//ViewState["daystart"].ToString();
                bb2.day_finish = finaldate.Text;//ViewState["dayfinish"].ToString();
                 

                
                bb1.job_order_overview_action[0] = bb2;
               insertlog();
                // bb1 = callServiceJobOrder(_urlUpdatestau0, bb1);
                // p1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(bb1));

                bb1 = callServiceJobOrder(_urlUpdatedaystartfinal, bb1);
                // insertlog();
                


                    // ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ทำการอนุมัติเสร็จสิ้น');", true);
                    sd.Visible=true;
                }
                break;
                case "acceptsent":
                data_jo yaibox = new data_jo();
                job_order_overview lekbok = new job_order_overview();
                yaibox.job_order_overview_action = new job_order_overview[1];

                lekbok.u0_jo_idx = int.Parse(ViewState["id_idx"].ToString());
                lekbok.m0_node_idx = int.Parse(ViewState["node"].ToString());
                lekbok.m0_actor_idx = int.Parse(ViewState["actor"].ToString());
                lekbok.id_statework = 5;
          
            
                yaibox.job_order_overview_action[0] = lekbok;
                yaibox = callServiceJobOrder(_urlUpdatestau0, yaibox);
                insertlog2();
                    // acceptnode();
                    // ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ทำการยอมรับเสร็จสิ้น');", true);
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
                case "edit": //edit หน้าแรก
                // Label nodem0 = (Label)jogrid.FindControl("nodem0");
                // Label actorm0 = (Label)jogrid.FindControl("actorm0");
                //var idx = int.Parse(cmdArg);

                //     Panel grid1 = (Panel)View1.FindControl("grid");
                //     GridView jogrid = (GridView)grid1.FindControl("jogrid");
                //    LinkButton edit = ((LinkButton)jogrid.FindControl("edit"));
                    // Panel userpanel = (Panel)edit.FindControl("userpanel");
                    // FormView Detailedit = (FormView)userpanel.FindControl("DetaileditUser");
                    // LinkButton btnedit = (LinkButton)Detailedit.FindControl("btnedit");
                 
                  string[] argument = new string[5];
                  argument = e.CommandArgument.ToString().Split(';');
                  int u0_idx = int.Parse(argument[0]);
                  int node_idx = int.Parse(argument[1]);
                  int actor_idx = int.Parse(argument[2]);
                  int status_idx = int.Parse(argument[3]);
                  int jo_idx_ref = int.Parse(argument[4]);
                  
                //   string adminname = argument[5];

          
                     
              //  p1.Text =  argument[2];
               //   ViewState["idx"] =  idx ;
                ViewState["idx"] =  u0_idx ;
                ViewState["node_idx"] = node_idx;
                ViewState["actor_idx"] = actor_idx;
                ViewState["id_statework"] = status_idx;
                ViewState["jo_idx_ref"] = jo_idx_ref;
               
                // ViewState["admin_name"] = adminname;
                  FirstMultiview.SetActiveView(View4);
                   
                //    Panel userpanel = ((Panel)View4.FindControl("userpanel"));
                    // FormView Detailedit =((FormView)userpanel.FindControl("DetaileditUser"));
                    
                    // LinkButton btnedit = ((LinkButton)Detailedit.FindControl("btnedit"));
                    // if(btnedit != null)
                    // {
                    //     p1.Text = "asdsda";
                    // }
                //   p1.Text = ViewState["actor_idx"].ToString();
                  
                //   DetaileditUser.ChangeMode(FormViewMode.ReadOnly);
                //   DetaileditUser.DataBind();
                  
                //  DetailUserpv.ChangeMode(FormViewMode.Insert);
                //  DetailUserpv.DataBind();
                //  if(ViewState["node_idx"].ToString() == "1" || ViewState["node_idx"].ToString() == "2" ){
                      
                //      btnedit.Visible = true;
                //  }
               
                //  p1.Text = ViewState["idx"].ToString();
                selectjob();
                selecttableu1();
                 SelectViewIndex2();
                break;
                case "edit2":
                  var idxchange = int.Parse(cmdArg);
                 
                  DetaileditUser.ChangeMode(FormViewMode.Edit);
                  DetaileditUser.DataBind();
                 
                 
                 ViewState["idxchange"] = idxchange;
              
                
                
                 SelectViewIndex3();
                break;
                case "cancel":
                DetaileditUser.ChangeMode(FormViewMode.ReadOnly);
                DetaileditUser.DataBind();
                 SelectViewIndex2();
                break;

                case "Updateedit":
                 data_jo box1 = new data_jo();
                job_order_overview box2 = new job_order_overview();
                // l0_job_order box3 = new l0_job_order();
                box1.job_order_overview_action = new job_order_overview[1];
                // box1.l0_job_order_action = new l0_job_order[1];
                // int edit_idx = int.Parse(cmdArg);
                //int u0_jo_idx_update = Convert.ToInt32(jogrid.DataKeys[e.RowIndex].Values[0].ToString());
                
                // var Pane = (Panel)DetaileditUser.FindControl("userpanel");
                var titledit = (TextBox)DetaileditUser.FindControl("titledit");
                var reqedit = (TextBox)DetaileditUser.FindControl("reqedit");
                // ViewState["u0_jo_idx"] = edit_idx;
                box2.title_jo =  titledit.Text;
                box2.require_jo = reqedit.Text;
                // box2.u0_jo_idx = edit_idx;
                box2.u0_jo_idx = int.Parse(ViewState["idx"].ToString()); //box2.ตัวแปรฝั่ง sql
                // box2.m0_node_idx = int.Parse(ViewState["node_idx"].ToString());
                // box2.m0_actor_idx = int.Parse(ViewState["actor_idx"].ToString());
                box2.m0_node_idx = 2;
                box2.m0_actor_idx =2;
                box2.id_statework = int.Parse(ViewState["id_statework"].ToString());
             
                
                box2.jo_idx_ref = int.Parse(ViewState["jo_idx_ref"].ToString());
                box1.job_order_overview_action[0] = box2;
                // box1.l0_job_order_action[0] = box3;
                    // p1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(box1));
           
                box1 = callServiceJobOrder(_urlUpdateDatajoborder, box1);
                
                //  insertlog();
                  //  box1 = callServiceJobOrder(_urlSelectDatajoborder_log, box1);
                // ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ทำการแก้ไขข้อมูลเรียบร้อย');", true);
                //  DetaileditUser.ChangeMode(FormViewMode.ReadOnly);
                // DetaileditUser.DataBind();
                 SelectViewIndex2();
                // grid.Visible = true;
            //   updatejob();
                
            //     grid.Visible = true;
            //     // var Panel = (Panel)jogrid.FindControl("editpanel");
                // var titled = (TextBox)Panel.FindControl("titledit");
                // if(titled.Text == "" ||  titled.Text == null)
                // {
                //      ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณากรอกข้อมูลให้ครบถ้วน');", true);
                // }
                break;
                case "showlog":
                int log_idx = int.Parse(cmdArg);
                data_jo bigbox = new data_jo();
                job_order_overview smallbox = new job_order_overview();
                // l0_job_order smallbox1 = new l0_job_order();
                // bigbox.l0_job_order_action = new l0_job_order[1];
                bigbox.job_order_overview_action = new job_order_overview[1];
             
                // p1.Text = ViewState["u0_jo_idx"].ToString(); 
                smallbox.u0_jo_idx =  log_idx;
                // bigbox.l0_job_order_action[0] = smallbox1; 
                bigbox.job_order_overview_action[0] = smallbox;
                
               
                bigbox = callServiceJobOrder(_urlSelectDatajoborder_log, bigbox);
                // p1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(bigbox));
                rplog.DataSource = bigbox.job_order_overview_action;
                rplog.DataBind();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                break;

                case "showsent":
                // int sent = int.Parse(cmdArg);
		        string[] arg2 = new string[4];
                arg2 = e.CommandArgument.ToString().Split(';');
                int ids = int.Parse(arg2[0]);
                int nodeids = int.Parse(arg2[1]);
                int actorids = int.Parse(arg2[2]);
                int statusids = int.Parse(arg2[3]);
 		        ViewState["id_idx"] = ids;
                ViewState["node"] = nodeids;
                ViewState["actor"] = actorids;
                ViewState["status"] = statusids;

                data_jo bgbox = new data_jo();
                job_order_overview smbox = new job_order_overview();
                bgbox.job_order_overview_action = new job_order_overview[1];
                
                smbox.u0_jo_idx =  int.Parse(ViewState["id_idx"].ToString()); 
                smbox.m0_node_idx = int.Parse(ViewState["node"].ToString());
                smbox.m0_actor_idx = int.Parse(ViewState["actor"].ToString());
                smbox.id_statework = int.Parse(ViewState["status"].ToString());

                bgbox.job_order_overview_action[0] = smbox;
                bgbox = callServiceJobOrder(_urlSelectdaystartfinal,bgbox);
                // p1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(bgbox));
                setFormViewData(DetailUsersent, FormViewMode.ReadOnly, bgbox.job_order_overview_action);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                break;

                case "addaf":
                data_jo box0 = new data_jo();
    	        job_order_overview box11 = new job_order_overview();
    	        box0.job_order_overview_action = new job_order_overview[1];
                  string[] arg3 = new string[4];
                  arg3 = e.CommandArgument.ToString().Split(';');
                  int u0_idx1 = int.Parse(arg3[0]);
                  int node_idx1 = int.Parse(arg3[1]);
                  int actor_idx1 = int.Parse(arg3[2]);
                  int status_idx1 = int.Parse(arg3[3]);
                 
                ViewState["idx"] =  u0_idx1 ;
                ViewState["node_idx"] = node_idx1;
                ViewState["actor_idx"] = actor_idx1;
                ViewState["id_statework"] = status_idx1;
                box11.u0_jo_idx = int.Parse(ViewState["idx"].ToString()); 
  	            box11.m0_actor_idx=1; //from_ac
    	        box11.m0_node_idx=1; // from_M0_node
    	        box11.id_statework =1;// node_decision
	            box0.job_order_overview_action[0] = box11 ;
	            box0  = callServiceJobOrder(_urlUpdatestau0, box0 );
                 Page.Response.Redirect(Page.Request.Url.ToString(), true);
                //  ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ทำการส่งเสร็จสิ้น');", true);
                break;
                case "showsearch":
                searchhidden.Visible = true;
                searchbutton.Visible = false;
                _divsearch.Visible = true;
                break;

                case "showsearch1":
                searchhidden.Visible = false;
                searchbutton.Visible = true;
                _divsearch.Visible = false;
                break;
        
                case "btncommandsearch":
                // ViewState["noinvoiceidsearch"] = TempCodeId.Text.Trim();

                break;
        }
    }
  
protected void actionsearch(){
    data_jo bb1 = new data_jo();
    job_order_overview bb2 = new job_order_overview();
    bb1.job_order_overview_action = new job_order_overview[1];
    // bb2.noinvoice_search = ViewState["noinvoiceidsearch"].ToString();
    // bb2.id_statework = 2;
    bb1.job_order_overview_action[0] = bb2;
} 
protected void selectjob(){
    data_jo box = new data_jo();
    job_order_overview box1 = new job_order_overview();
    box.job_order_overview_action = new job_order_overview[1];
    box.job_order_overview_action[0] = box1;
    //  p1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(box));
   box = callServiceJobOrder(_urlSelectDatajoborder, box);
    setGridData(jogrid,box.job_order_overview_action );
    
    setGridData(homegrid,box.job_order_overview_action );
    setFormViewData(DetailUserpv, FormViewMode.ReadOnly, box.job_order_overview_action);
    // setGridData(jogrid2,box.job_order_overview_action );

}

protected void selecttableu1(){
    data_jo boxy = new data_jo();
    jo_add_datalist boxl = new jo_add_datalist();
    boxy.jo_add_datalist_action = new jo_add_datalist[1];
    boxl.wait  = int.Parse(ViewState["idx"].ToString());
    //  boxl.wait  = 12;
    boxy.jo_add_datalist_action[0] = boxl;
//    p1.Text = ViewState["idx"].ToString();

    boxy = callServiceJobOrder(_urlSelectU1ov, boxy);
    // p1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(boxy));
     setGridData(u1grid,boxy.jo_add_datalist_action);
   
}
protected void selectmheadjob(){
                data_jo box12 = new data_jo();
                 job_order_overview box22 = new job_order_overview();
                box12.job_order_overview_action = new job_order_overview[1];
                box12.job_order_overview_action[0] = box22;
     //p1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(box));
                 box12 = callServiceJobOrder(_urlSelect_mishead, box12);
                setGridData(jogrid2,box12.job_order_overview_action );

}
protected void acceptnode(){
        data_jo bb1 = new data_jo();
        job_order_overview bb2 = new job_order_overview();
        bb1.job_order_overview_action = new job_order_overview[1];
            
            bb2.u0_jo_idx = int.Parse(ViewState["id_idx"].ToString()); //box2.ตัวแปรฝั่ง sql
            bb2.m0_node_idx = int.Parse(ViewState["node"].ToString());
            bb2.m0_actor_idx = int.Parse(ViewState["actor"].ToString());
            bb2.id_statework = 2;
            bb1.job_order_overview_action[0] = bb2;
            bb1 = callServiceJobOrder(_urlUpdatestau0, bb1);

}
protected void insertjob(){
    var addtitle =(TextBox)remoteuser.FindControl("titlename");
    data_jo box = new data_jo();
    job_order_overview box1 = new job_order_overview();
    box.job_order_overview_action = new job_order_overview[1];
    box1.title_jo = titlename.Text;
    box1.require_jo = require.Text;
    box1.emp_id_creator = int.Parse(ViewState["EmpIDX"].ToString());
    
    
  //ส่วนของ User 
 //บอกจุดเริ่มต้นของ node 
    box1.m0_actor_idx=1; //from_ac
    box1.m0_node_idx=1; // from_M0_node
    box1.id_statework =1;// node_decision
 
  box.job_order_overview_action[0] = box1;
//   p1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(box));
  box = callServiceJobOrder(_urlInsertDatajoborder, box);
   
 //string localJson1 = _funcTool.convertObjectToJson(box);
        // p1.Text = localJson1;
    // setGridData(jogrid,box.job_order_overview_action );

}
protected void insertlog(){ //insert log node
//  FormView logidform = (FormView)View3.FindControl("logidform");
    // Label loggy = (Label)View3.FindControl("oaklog");
    data_jo bbox = new data_jo();
    jo_log sbox = new jo_log();
    // job_order_overview sbox = new job_order_overview();
    // bbox.job_order_overview_action = new job_order_overview[1];
    bbox.jo_log_action = new jo_log[1];
    // ViewState["log_oak"] = loggy;
    sbox.oakja = int.Parse(ViewState["id_idx"].ToString());
    // sbox.oakja = int.Parse(loggy.Text);
 
    sbox.m0_node_idx_lo = 3;
    sbox.m0_actor_idx_lo = 3;
    sbox.id_statework_lo = 2;
    bbox.jo_log_action[0] = sbox;
    // p1.Text = loggy.Text;
    // p1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(bbox));
     bbox = callServiceJobOrder(_urlInsertloggaritum, bbox);
    //   setFormViewData(logidform, FormViewMode.ReadOnly, bbox.jo_log_action);
}
protected void insertlog2(){ //insert log node
//  FormView logidform = (FormView)View3.FindControl("logidform");
    // Label loggy = (Label)View3.FindControl("oaklog");
    data_jo bbox = new data_jo();
    jo_log sbox = new jo_log();
    // job_order_overview sbox = new job_order_overview();
    // bbox.job_order_overview_action = new job_order_overview[1];
    bbox.jo_log_action = new jo_log[1];
    // ViewState["log_oak"] = loggy;
    sbox.oakja = int.Parse(ViewState["id_idx"].ToString());
    // sbox.oakja = int.Parse(loggy.Text);
 
    sbox.m0_node_idx_lo = 4;
    sbox.m0_actor_idx_lo = 1;
    sbox.id_statework_lo = 5;
    bbox.jo_log_action[0] = sbox;
    // p1.Text = loggy.Text;
    // p1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(bbox));
     bbox = callServiceJobOrder(_urlInsertloggaritum, bbox);
    //   setFormViewData(logidform, FormViewMode.ReadOnly, bbox.jo_log_action);
}
protected void editjob(){
    data_jo box = new data_jo();
    job_order_overview box1 = new job_order_overview();
    box.job_order_overview_action = new job_order_overview[1];
    box.job_order_overview_action[0] = box1;

    box = callServiceJobOrder(_urlEditDatajoborder, box);
    // setGridData(jogrid,box.job_order_overview_action );
   }
   protected void updatejob(){
                data_jo box = new data_jo();
                job_order_overview box1 = new job_order_overview();
                box.job_order_overview_action = new job_order_overview[1]; //ประกาศกล่องย่อยฝั่ง xml
                box1.u0_jo_idx = int.Parse(ViewState["u0_jo_idx_update"].ToString());
                box1.title_jo =  ViewState["titleUpdate"].ToString();
                box1.require_jo = ViewState["reqUpdate"].ToString();
               
                box.job_order_overview_action[0] = box1; // เอากล่องเล็กยัดใส่กล่องย่อย
               
                // string localJson1 = _funcTool.convertObjectToJson(box);
                //  p.Text = localJson1;
               box = callServiceJobOrder(_urlUpdateDatajoborder, box);
               
             

   }
 protected void deletejob(){
     data_jo box = new data_jo();
     job_order_overview box1 = new job_order_overview();
                box.job_order_overview_action = new job_order_overview[1]; //ประกาศกล่องย่อยฝั่ง xml
                
                box1.u0_jo_idx = int.Parse(ViewState["jo_idx"].ToString());
               
                box.job_order_overview_action[0] = box1; // เอากล่องเล็กยัดใส่กล่องย่อย
                // p1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(box));
                box = callServiceJobOrder(_urlDeleteDatajoborder, box);
                
                
                // updatejob();

 }

    #region SetGrid
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion
     #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        e.Row.Attributes.Add("style", "cursor:help;");
        switch (GvName.ID)
        {

            case "jogrid":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (jogrid.EditIndex != e.Row.RowIndex) //to overlook header row
                    {
                         var nod = ((Label)e.Row.FindControl("_nodem0"));
                         var ac = ((Label)e.Row.FindControl("_actorm0"));
                         var st =((Label)e.Row.FindControl("_idstate"));
                         var shac = ((Label)e.Row.FindControl("showact"));
                        
                         var sent = ((LinkButton)e.Row.FindControl("sentaccept"));
                         var addafter  = ((LinkButton)e.Row.FindControl("addafter"));
                         var editbutton = ((LinkButton)e.Row.FindControl("edit"));
                         var sentrup = ((LinkButton)e.Row.FindControl("sentrup"));
                        // nod.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC00");
                        if(st.Text== "1"){
                            shac.ForeColor = System.Drawing.ColorTranslator.FromHtml("#FF8C1A");
                            shac.Style["font-weight"] = "bold";
                           
                        }
                        else if(st.Text == "3"  || st.Text == "7"){
                            shac.ForeColor = System.Drawing.ColorTranslator.FromHtml("#FF0000");
                            shac.Style["font-weight"] = "bold";
                        }
                         else if(st.Text == "10" ){
                            shac.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ffcc00");
                            shac.Style["font-weight"] = "bold";
                        }
                        else if(st.Text == "2" || st.Text == "6"  || st.Text == "8" || st.Text == "4"){
                            shac.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC66");
                            shac.Style["font-weight"] = "bold";

                        }
                        else if(st.Text == "5"){
                            shac.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0040ff");
                            shac.Style["font-weight"] = "bold";
                        }
                        if(nod.Text == "4"){
                          
                           sent.Visible = true;
                        }
                        
                        else if(nod.Text  == "6"){
                            sentrup.Visible = true;
                            
                        }
                    }
                     e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#f0f0f5'");
                      e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='white'");
                        e.Row.BackColor = Color.FromName("white");  
                }
         

                break;
                


        }

    }

    #endregion
     #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
      
        switch (GvName.ID)
        {
            case "jogrid":

                jogrid.PageIndex = e.NewPageIndex;
                jogrid.DataBind();
                    
                selectjob();
                

                break;
            case "jogrid2":
            jogrid2.PageIndex = e.NewPageIndex;
            jogrid2.DataBind();
            // jogrid2.Refresh();
            selectmheadjob();
            break;

            case "homegrid":
            homegrid.PageIndex = e.NewPageIndex;
            homegrid.DataBind();
            selectjob();
            break;

            case "u1grid":
            u1grid.PageIndex = e.NewPageIndex;
            u1grid.DataBind();
            selecttableu1();
            // selectjob();
            break;


        }
    }

    #endregion

    //protected void action()
    //{
    //    _divMenuLiToDivIndex.Attributes.Add("class", "active");
    //    divIndex.Visible = true;
    //    ad_online objADOnline = new ad_online();
    //    dataADOnline.ad_online_action = new ad_online[1];
    //    objADOnline.on_table = Constants.KEY_GET_FROM_PERM_TEMP;
    //    objADOnline.with_where = Constants.WITH_WHERE_NO;
    //    if (txtFilterPermTempCodeId.Text.Trim() != string.Empty)
    //    {
    //        objADOnline.filter_u0_perm_temp_code_id = txtFilterPermTempCodeId.Text.Trim();
    //    }
    //    if (txtFilterPermTempCreatorName.Text.Trim() != string.Empty)
    //    {
    //        objADOnline.filter_u0_perm_temp_creator = txtFilterPermTempCreatorName.Text.Trim();
    //    }
    //    if (txtFilterPermTempCompName.Text.Trim() != string.Empty)
    //    {
    //        objADOnline.filter_u0_perm_temp_comp_name = txtFilterPermTempCompName.Text.Trim();
    //    }
    //    if (ddlFilterPermTempOrg.SelectedValue != string.Empty)
    //    {
    //        objADOnline.filter_u0_perm_temp_org = ddlFilterPermTempOrg.SelectedValue;
    //    }
    //    if (ddlFilterPermTempDept.SelectedValue != string.Empty)
    //    {
    //        objADOnline.filter_u0_perm_temp_dept = ddlFilterPermTempDept.SelectedValue;
    //    }
    //    if (ddlFilterPermTempNode.SelectedValue != string.Empty)
    //    {
    //        objADOnline.filter_u0_perm_temp_node = ddlFilterPermTempNode.SelectedValue;
    //    }
    //    dataADOnline.ad_online_action[0] = objADOnline;
    //    _local_xml = servExec.actionExec(misConn, "data_adonline", adOnlineService, dataADOnline, Constants.SELECT_ALL);
    //    dataADOnline = (data_adonline)_funcTool.convertXmlToObject(typeof(data_adonline), _local_xml);
    //    setGridData(gvIndexPermTemp, dataADOnline.ad_online_action);
    //}


 protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;
        
        switch (GvName.ID)
        {
            case "jogrid":
           
                jogrid.EditIndex = e.NewEditIndex;
               
                 
                selectjob();
               
                break;
        }
    }

        protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "jogrid":
                jogrid.EditIndex = -1;
                //Page.Response.Redirect(Page.Request.Url.ToString(), true);
                // remoteuser.Visible = true;
                selectjob();
                // grid.Visible = true;
                break;
        }
    }


    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "jogrid":
  
                int u0_jo_idx_update = Convert.ToInt32(jogrid.DataKeys[e.RowIndex].Values[0].ToString());
                var titleUpdate = (TextBox)jogrid.Rows[e.RowIndex].FindControl("titledit");
                var reqUpdate = (TextBox)jogrid.Rows[e.RowIndex].FindControl("reqedit");
                jogrid.EditIndex = -1;
               ViewState["u0_jo_idx_update"] =  u0_jo_idx_update;
               ViewState["titleUpdate"] =  titleUpdate.Text;
               ViewState["reqUpdate"] =  reqUpdate.Text;
               updatejob();
               selectjob();
                

                break;
        }
    }

 #region call service
    protected data_jo callServiceJobOrder(string _cmdUrl, data_jo _data_jo)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_jo);
        //  p1.Text = _cmdUrl+_localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl , _localJson);

        // convert json to object
         _data_jo = (data_jo)_funcTool.convertJsonToObject(typeof(data_jo), _localJson);

        return _data_jo;
    }

#endregion

 #region SetFormView
    protected void setFormViewData(FormView foName, FormViewMode foMode, Object obj)
    {
        foName.ChangeMode(foMode);
        foName.DataSource = obj;
        foName.DataBind();
    }
#endregion

#region Detail_DataBound

protected void Detail_DataBound (object sender,EventArgs e)
{
    var FoName = (FormView)sender;
    switch(FoName.ID)
    {
    
    case "DetailUser":
                FormView DetailUser = (FormView)View3.FindControl("DetailUser");
               


    break;
    //  case "DetaileditUser":
    //             FormView DetaileditUser = (FormView)View4.FindControl("DetaileditUser");
               
                
    // break;

    case "DetailUserpv":
        FormView DetailUserpv = (FormView)View4.FindControl("DetailUserpv");
    break;

    
    }


}

#endregion
#region DetailUsersent_DataBound

protected void DetailUsersent_DataBound (object sender,EventArgs e)
{
    var FoName = (FormView)sender;
    switch(FoName.ID)
    {
    
    case "DetailUsersent":
                FormView DetailUsersent = (FormView)View1.FindControl("DetailUsersent");
               


    break;
   
    
    }


}

#endregion
 protected void SelectViewIndex()
    {
    data_jo box = new data_jo();
    job_order_overview box1 = new job_order_overview();
    box.job_order_overview_action = new job_order_overview[1];
    box.job_order_overview_action[0] = box1;
   
    box1.u0_jo_idx = int.Parse(ViewState["id_idx"].ToString());
 
   
     //p1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(box));
    box = callServiceJobOrder(_urlSelectDatajoborder_mhead, box);
    
    setFormViewData(DetailUser, FormViewMode.ReadOnly, box.job_order_overview_action);
    
    // setFormViewData(Detaildate, FormViewMode.ReadOnly, box.job_order_overview_action);
   
    // setFormViewData(EditUser, FormViewMode.ReadOnly, box.job_order_overview_action);
    }

    protected void SelectViewIndex2(){
    data_jo box = new data_jo();
    job_order_overview box1 = new job_order_overview();
    box.job_order_overview_action = new job_order_overview[1];
    box.job_order_overview_action[0] = box1;
   
    box1.u0_jo_idx = int.Parse(ViewState["idx"].ToString());
    // box1.AdminMain = ViewState["admin_name"].ToString();
    
    
    box = callServiceJobOrder(_urlSelectDatajoborder_mhead, box);
    //  p1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(box));
    setFormViewData(DetaileditUser, FormViewMode.ReadOnly, box.job_order_overview_action);
    //  setFormViewData(DetailUserpv, FormViewMode.ReadOnly, box.job_order_overview_action);
     FormView Detailedit =((FormView)userpanel.FindControl("DetaileditUser"));
     LinkButton btnedit = ((LinkButton)Detailedit.FindControl("btnedit"));

       if(ViewState["node_idx"].ToString() == "1" || ViewState["node_idx"].ToString() == "2" ){
                      
                     btnedit.Visible = true;
        }
               
    
    }
    protected void SelectViewIndex3(){
    data_jo box = new data_jo();
    job_order_overview box1 = new job_order_overview();
    box.job_order_overview_action = new job_order_overview[1];
    box.job_order_overview_action[0] = box1;
   
    box1.u0_jo_idx = int.Parse(ViewState["idxchange"].ToString());
   // box1.id_statework = int.Parse(ViewState["status"].ToString());
     //p1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(box));
    box = callServiceJobOrder(_urlSelectDatajoborder_mhead, box);
    setFormViewData(DetaileditUser, FormViewMode.Edit, box.job_order_overview_action);
    
    }
}


 





