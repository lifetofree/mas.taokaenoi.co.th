﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/master_content.master" AutoEventWireup="true" CodeFile="CHR_Approve.aspx.cs" Inherits="websystem_ITServices_CHR_Approve" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">




    <script type="text/javascript"> 

        function DivClicked(u0id) {

            var btnHidden = $('.btndiv' + u0id);
            btnHidden.click();
        }


        function openModal() {
            $('#ordine').modal('show');

        }

        function openModal1() {
            $('#ordine1').modal('show');

        }

    </script>



    <style>
        #btnapprove_chr {
            margin-top: 10px;
            top: 0px;
            left: 0px;
        }

        .col-lg-12-chr {
            padding-right: 0px;
            padding-left: 0px;
        }
    </style>







   <%-- <asp:Literal ID="text" runat="server"></asp:Literal>--%>

    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewIndex" runat="server">

            <div id="PanelBody_gridviewindex">
                <div>

                    <asp:GridView ID="GvApprove"
                        runat="server"
                        AutoGenerateColumns="False"
                        DataKeyNames="u0idx"
                        CssClass="table table-hover table-responsive font_text pageCustom_chr col-lg-12-chr "
                        ShowHeader="false"
                        HeaderStyle-CssClass="success"
                        GridLines="None"
                        BorderWidth="0"
                        HeaderStyle-Height="40px"
                        AllowPaging="true"
                        PageSize="5"
                        OnRowDataBound="Master_RowDataBound"
                        OnPageIndexChanging="Master_PageIndexChanging">

                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                        <EmptyDataTemplate>
                            <div style="text-align: center">ท่านไม่มีเอกสารให้อนุมัติ</div>
                        </EmptyDataTemplate>

                        <Columns>


                            <asp:TemplateField Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblu0idx" runat="server" Visible="true" Text='<%# Eval("u0idx") %>' />
                                    <asp:Label ID="lblsysidx" runat="server" Visible="false" Text='<%# Eval("Sysidx") %>' />
                                    <asp:HiddenField ID="hfM0NodeIDXMD" runat="server" Value='<%# Eval("noidx") %>' />
                                    <asp:HiddenField ID="hfM0ActoreIDXMD" runat="server" Value='<%# Eval("acidx") %>' />

                                </ItemTemplate>
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="รหัสเอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="95%" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>

                                        <div class="col-lg-12-chr" style="padding: 0px; padding-left: 0px;">
                                            <div class="form-group">

                                                <asp:Image ID="images" ImageAlign="Left" runat="server" Width="10%" ImageUrl="~/masterpage/images/CHR/images.png" />

                                                <div class="col-lg-12-chr" style="margin-left: 20px">
                                                    <div class="col-lg-2">
                                                        &nbsp;&nbsp;&nbsp;
                                                        <strong>
                                                            <asp:Label ID="Label9" runat="server">Change Request No: </asp:Label></strong>
                                                        <asp:Literal ID="Literal11" runat="server" Text='<%# Eval("doc_code") %>' />
                                                        <asp:Literal ID="Literal3" runat="server" Text='<%# Eval("System_name") %>' />

                                                    </div>

                                                    <div class="col-lg-10">
                                                        <asp:Button runat="server" CssClass='<%# "btndiv" + Eval("u0idx") %>' CommandArgument='<%# Eval("u0idx") %>' ID="btnHidden" OnCommand="btnCommand" CommandName="btnhide" Style="display: none" Text='<%# Eval("u0idx") %>' />

                                                        <div id="div_detail" onclick="javascript:DivClicked('<%# Eval("u0idx") %>'); return true;">


                                                            <div class="col-lg-2">
                                                                <strong>
                                                                    <asp:Label ID="Label13" runat="server">ประเภทการแจ้ง: </asp:Label></strong>
                                                                <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("Typename") %>' />
                                                            </div>
                                                            <div class="col-lg-2">
                                                                <strong>
                                                                    <asp:Label ID="Label2" runat="server">ระบบใหม่: </asp:Label></strong>
                                                                <asp:Label ID="lblnew_sys" runat="server" Text='<%# Eval("new_sys") %>' />
                                                                &nbsp;
                                                        <asp:Label ID="Label8" Font-Size="X-Small" Font-Bold="true" ForeColor="White" BackColor="Blue" runat="server" Text="more..." />
                                                            </div>
                                                            <div class="col-lg-2">
                                                                <strong>
                                                                    <asp:Label ID="Label3" runat="server">สถานะดำเนินการ: </asp:Label></strong>
                                                                <asp:Label ID="Label6" runat="server" Text='<%# Eval("StatusDoc") %>' />

                                                            </div>
                                                            <div class="col-lg-3">
                                                                <strong>
                                                                    <asp:Label ID="Label25" runat="server">ชื่อ-นามสกุลผู้แจ้ง: </asp:Label></strong>
                                                                <asp:Literal ID="Literal2" runat="server" Text='<%# Eval("EmpName") %>' />


                                                                <strong>
                                                                    <asp:Literal ID="ltlnode" runat="server"> </asp:Literal></strong>

                                                                <br />

                                                                <asp:LinkButton ID="btnyes" CssClass="btn btn-xs btn-success btn_" runat="server" data-original-title="Approve" data-toggle="tooltip" Text="Approve" OnCommand="btnCommand" CommandName="btnapprove" CommandArgument='<%# Eval("u0idx") + ";" + Eval("noidx")+ ";" + Eval("acidx") + ";" + "1" %>'><i class="glyphicon glyphicon-ok-circle"></i> Approve</asp:LinkButton>
                                                                <asp:LinkButton ID="btnno" CssClass="btn btn-xs btn-danger btn_" OnClientClick="return confirm('คุณต้องการไม่อนุมัติข้อมูลนี้ใช่หรือไม่')" runat="server" data-original-title="Reject" data-toggle="tooltip" Text="DisApprove" OnCommand="btnCommand" CommandName="btnapprove" CommandArgument='<%# Eval("u0idx") + ";" + Eval("noidx")+ ";" + Eval("acidx")+ ";" + "2" %>'><i class="glyphicon glyphicon-remove-circle"></i> Reject</asp:LinkButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>




                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" HeaderStyle-Height="3%" HeaderStyle-Width="2%" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>

                                        <asp:LinkButton ID="LinkButton1" runat="server" data-original-title="ViewDetail" data-toggle="tooltip" Text="Detail" OnCommand="btnCommand" CommandName="btnhide" CommandArgument='<%# Eval("u0idx") %>'><i class="glyphicon glyphicon-list-alt"></i> </asp:LinkButton>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>

                    </asp:GridView>



                </div>
            </div>


            <div class="col-lg-12" runat="server" id="Div1">
                <div id="ordine1" class="modal open" role="dialog">
                    <div class="modal-dialog" style="width: 70%;">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">ช่องแสดงความคิดเห็น</h4>
                            </div>
                            <div class="modal-body">
                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">
                                        <div class="form-group">
                                            <asp:Label ID="Label42" runat="server" Text="ความคิดเห็น" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtremark_approve1" TextMode="multiline" Rows="5" runat="server" CssClass="form-control embed-responsive-item" PlaceHoldaer="........" />
                                            </div>


                                        </div>

                                        <br />

                                        <div class="form-group">
                                            <div class="col-sm-2 col-sm-offset-10">
                                                <asp:LinkButton ID="LinkButton2" CssClass="btn btn-xs btn-success" runat="server" data-toggle="tooltip" title="Approve" OnCommand="btnCommand" CommandName="btncomment"><i class="glyphicon glyphicon-ok"></i></asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton3" class="btn btn-xs  btn-default" data-toggle="tooltip" title="Close" runat="server" OnCommand="btnCommand" CommandName="closecomment"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </asp:View>

        <asp:View ID="ViewDetail" runat="server">

            <div class="col-xs-12" style="position: unset" id="leftside">

                <div class="panel-body">

                    <asp:FormView ID="FvDetailUser" runat="server" Width="100%" DefaultMode="ReadOnly">
                        <ItemTemplate>
                            <div class="form-horizontal" role="form">
                                <asp:HiddenField ID="hfM0NodeIDXMD" runat="server" Value='<%# Eval("noidx") %>' />
                                <asp:HiddenField ID="hfM0ActoreIDXMD" runat="server" Value='<%# Eval("acidx") %>' />

                                <div class="form-group">
                                    <div class="col-xs-1" style="text-align: center">
                                    </div>
                                    <div class="col-xs-9" style="text-align: center">
                                        <asp:Image ID="images" runat="server" Width="30%" ImageUrl="~/masterpage/images/CHR/images.png" />
                                    </div>

                                    <div class="col-xs-1" style="text-align: right">

                                        <asp:LinkButton ID="btnhome1" data-toggle="tooltip" title="Back" runat="server" CommandName="btnback" OnCommand="btnCommand"><i class="glyphicon glyphicon-list-alt"></i> </asp:LinkButton>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-xs-12">

                                        <asp:Label ID="Label23" CssClass="col-xs-6 control-labelnotop text_right " runat="server" Text="Change Request No:" />
                                        <div class="col-xs-6">
                                            <asp:Label ID="Label22" CssClass="control-labelnotop " runat="server" Text='<%# Eval("doc_code") %>' />
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <asp:Label ID="Label35" CssClass="col-xs-6 control-labelnotop text_right " runat="server" Text="วันที่สร้าง :" />
                                        <div class="col-xs-6">
                                            <asp:Label ID="Label36" CssClass="control-labelnotop " runat="server" Text='<%# Eval("datecreate") %>' />
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <asp:Label ID="Label7" CssClass="col-xs-6 control-labelnotop text_right " runat="server" Text="ระบบ :" />
                                        <div class="col-xs-6">
                                            <asp:Label ID="Label17" CssClass="control-labelnotop " runat="server" Text='<%# Eval("System_name") %>' />
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <asp:Label ID="Label30" CssClass="col-xs-6 control-labelnotop text_right" runat="server" Text="ฝ่าย :" />
                                        <div class="col-xs-6">
                                            <asp:Label ID="Label34" CssClass=" control-labelnotop" runat="server" Text='<%# Eval("DeptNameTH") %>' />

                                        </div>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <asp:Label ID="Label1" CssClass="col-xs-6 control-labelnotop text_right " runat="server" Text="ชื่อผู้แจ้ง :" />
                                        <div class="col-xs-6">
                                            <asp:Label ID="Label4" CssClass="control-labelnotop " runat="server" Text='<%# Eval("EmpName") %>' />
                                        </div>

                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="col-xs-12">

                                        <asp:Label ID="Label15" CssClass="col-xs-6 control-labelnotop text_right" runat="server" Text="ประเภทการแจ้ง :" />
                                        <div class="col-xs-6">
                                            <asp:Label ID="lbltel" CssClass="control-labelnotop" runat="server" Text='<%# Eval("Typename") %>' />
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-xs-12">

                                        <asp:Label ID="Label10" CssClass="col-xs-6 control-labelnotop text_right" runat="server" Text="UserSAP :" />
                                        <div class="col-xs-6">
                                            <asp:Label ID="Label11" CssClass="control-labelnotop" runat="server" Text='<%# Eval("Usersap") %>' />
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <asp:Label ID="Label12" CssClass="col-xs-6 control-labelnotop text_right" runat="server" Text="ผู้อนุมัติที่ 1 :" />
                                        <div class="col-xs-6">
                                            <asp:Label ID="lblappok1" Visible="false" runat="server" ForeColor="#00cc00"><i class="glyphicon glyphicon-ok-circle"></i></asp:Label>
                                            <asp:Label ID="Label14" CssClass="control-labelnotop" runat="server" Text='<%# Eval("NEmpidx1") %>' />
                                            <asp:LinkButton ID="lbtcomment1show" CommandName="lbtcomment1show" OnCommand="btnCommand" Font-Size="X-Small" Font-Bold="true" ForeColor="White" BackColor="Blue" runat="server" Text="comment" />
                                            <asp:LinkButton ID="lbtcomment1hide" Visible="false" CommandName="lbtcomment1hide" OnCommand="btnCommand" Font-Size="X-Small" Font-Bold="true" ForeColor="White" BackColor="Blue" runat="server" Text="hide..." />
                                        </div>

                                    </div>
                                </div>


                                <div class="form-group" id="div_comment1" runat="server" visible="false">
                                    <div class="col-xs-12">
                                        <asp:Label ID="Label24" CssClass="col-xs-6 control-labelnotop text_right" runat="server" Text="ความคิดเห็น :" />
                                        <div class="col-xs-6">
                                            <asp:Label ID="Label26" Visible="false" runat="server" ForeColor="#00cc00"><i class="glyphicon glyphicon-ok-circle"></i></asp:Label>
                                            <asp:Label ID="Label27" CssClass="control-labelnotop" runat="server" Text='<%# Eval("commentapp1") %>' />

                                        </div>

                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <asp:Label ID="Label16" CssClass="col-xs-6 control-labelnotop text_right" runat="server" Text="ผู้อนุมัติที่ 2 :" />
                                        <div class="col-xs-6">
                                            <asp:Label ID="lblappok2" Visible="false" runat="server" ForeColor="#00cc00"><i class="glyphicon glyphicon-ok-circle"></i></asp:Label>
                                            <asp:Label ID="Label18" CssClass="control-labelnotop" runat="server" Text='<%# Eval("NEmpidx2") %>' />
                                            <asp:LinkButton ID="lbtcomment2show" CommandName="lbtcomment2show" OnCommand="btnCommand" Font-Size="X-Small" Font-Bold="true" ForeColor="White" BackColor="Blue" runat="server" Text="comment" />
                                            <asp:LinkButton ID="lbtcomment2hide" Visible="false" CommandName="lbtcomment2hide" OnCommand="btnCommand" Font-Size="X-Small" Font-Bold="true" ForeColor="White" BackColor="Blue" runat="server" Text="hide..." />
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group" id="div_comment2" runat="server" visible="false">
                                    <div class="col-xs-12">
                                        <asp:Label ID="Label31" CssClass="col-xs-6 control-labelnotop text_right" runat="server" Text="ความคิดเห็น :" />
                                        <div class="col-xs-6">
                                            <asp:Label ID="Label33" Visible="false" runat="server" ForeColor="#00cc00"><i class="glyphicon glyphicon-ok-circle"></i></asp:Label>
                                            <asp:Label ID="Label37" CssClass="control-labelnotop" runat="server" Text='<%# Eval("commentapp2") %>' />

                                        </div>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-xs-12">

                                        <asp:Label ID="Label19" CssClass="col-xs-6 control-labelnotop text_right" runat="server" Text="ผู้อนุมัติที่ 3 :" />
                                        <div class="col-xs-6">
                                            <asp:Label ID="lblappok3" Visible="false" runat="server" ForeColor="#00cc00"><i class="glyphicon glyphicon-ok-circle"></i></asp:Label>
                                            <asp:Label ID="Label20" CssClass="control-labelnotop" runat="server" Text='<%# Eval("NEmpidx3") %>' />

                                            <asp:LinkButton ID="lbtcomment3show" CommandName="lbtcomment3show" OnCommand="btnCommand" Font-Size="X-Small" Font-Bold="true" ForeColor="White" BackColor="Blue" runat="server" Text="comment" />
                                            <asp:LinkButton ID="lbtcomment3hide" Visible="false" CommandName="lbtcomment3hide" OnCommand="btnCommand" Font-Size="X-Small" Font-Bold="true" ForeColor="White" BackColor="Blue" runat="server" Text="hide..." />
                                        </div>

                                    </div>
                                </div>


                                <div class="form-group" id="div_comment3" runat="server" visible="false">
                                    <div class="col-xs-12">
                                        <asp:Label ID="Label38" CssClass="col-xs-6 control-labelnotop text_right" runat="server" Text="ความคิดเห็น :" />
                                        <div class="col-xs-6">
                                            <asp:Label ID="Label39" Visible="false" runat="server" ForeColor="#00cc00"><i class="glyphicon glyphicon-ok-circle"></i></asp:Label>
                                            <asp:Label ID="Label40" CssClass="control-labelnotop" runat="server" Text='<%# Eval("commentapp3") %>' />

                                        </div>

                                    </div>
                                </div>


                                <div class="form-group" id="div_approveabout" runat="server" visible="false">
                                    <div class="col-xs-12">

                                        <asp:Label ID="Label41" CssClass="col-xs-6 control-labelnotop text_right" runat="server" Text="ผู้อนุมัติที่เกี่ยวข้อง :" />
                                        <div class="col-xs-6">
                                            <asp:Label ID="lblappokabout" Visible="false" runat="server" ForeColor="#00cc00"><i class="glyphicon glyphicon-ok-circle"></i></asp:Label>
                                            <asp:Label ID="Label43" CssClass="control-labelnotop" runat="server" Text='<%# Eval("NEmpidxAbout") %>' />

                                            <asp:LinkButton ID="lbtcommentaboutshow" CommandName="lbtcommentaboutshow" OnCommand="btnCommand" Font-Size="X-Small" Font-Bold="true" ForeColor="White" BackColor="Blue" runat="server" Text="comment" />
                                            <asp:LinkButton ID="lbtcommentabouthide" Visible="false" CommandName="lbtcommentabouthide" OnCommand="btnCommand" Font-Size="X-Small" Font-Bold="true" ForeColor="White" BackColor="Blue" runat="server" Text="hide..." />
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group" id="div_commentabout" runat="server" visible="false">
                                    <div class="col-xs-12">
                                        <asp:Label ID="Label44" CssClass="col-xs-6 control-labelnotop text_right" runat="server" Text="ความคิดเห็น :" />
                                        <div class="col-xs-6">
                                            <asp:Label ID="Label45" Visible="false" runat="server" ForeColor="#00cc00"><i class="glyphicon glyphicon-ok-circle"></i></asp:Label>
                                            <asp:Label ID="Label46" CssClass="control-labelnotop" runat="server" Text='<%# Eval("commentabout") %>' />

                                        </div>

                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <asp:Label ID="Label5" CssClass="col-xs-6 control-labelnotop text_right " runat="server" Text="แจ้งเรื่อง :" />
                                        <div class="col-xs-6">
                                            <asp:Label ID="Label28" CssClass="control-labelnotop " runat="server" Text='<%# Eval("topic") %>' />
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <asp:Label ID="Label29" CssClass="col-xs-6 control-labelnotop text_right " runat="server" Text="ระบบเดิม :" />
                                        <div class="col-xs-6">
                                            <asp:Label ID="lbloldsys" CssClass="control-labelnotop " runat="server" Text='<%# Eval("old_sys") %>' />
                                            <asp:LinkButton ID="lbloldmore" CommandName="lbloldmore" OnCommand="btnCommand" Font-Size="X-Small" Font-Bold="true" ForeColor="White" BackColor="Blue" runat="server" Text="more..." />
                                            <asp:LinkButton ID="lbloldhide" Visible="false" CommandName="lbloldhide" OnCommand="btnCommand" Font-Size="X-Small" Font-Bold="true" ForeColor="White" BackColor="Blue" runat="server" Text="hide..." />
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <asp:Label ID="Label32" CssClass="col-xs-6 control-labelnotop text_right " runat="server" Text="ระบบใหม่ :" />
                                        <div class="col-xs-6">
                                            <asp:Label ID="lblnewsys" CssClass="control-labelnotop " runat="server" Text='<%# Eval("new_sys") %>' />
                                            <asp:LinkButton ID="lblnewmore" Font-Size="X-Small" CommandName="lblnewmore" OnCommand="btnCommand" Font-Bold="true" ForeColor="White" BackColor="Blue" runat="server" Text="more..." />
                                            <asp:LinkButton ID="lblnewhide" Font-Size="X-Small" CommandName="lblnewhide" OnCommand="btnCommand" Font-Bold="true" ForeColor="White" BackColor="Blue" runat="server" Visible="false" Text="hide..." />
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group" id="div_comment" runat="server" visible="false">
                                    <div class="col-xs-12">
                                        <asp:Label ID="Label21" CssClass="col-xs-6 control-labelnotop text_right " runat="server" Text="ความคิดเห็นเจ้าหน้าที่ :" />
                                        <div class="col-xs-6">
                                            <asp:Label ID="lblcomment" CssClass="control-labelnotop " runat="server" Text='<%# Eval("comment") %>' />
                                            <asp:LinkButton ID="lblcommentmore" Font-Size="X-Small" CommandName="lblcommentmore" OnCommand="btnCommand" Font-Bold="true" ForeColor="White" BackColor="Blue" runat="server" Text="more..." />
                                            <asp:LinkButton ID="lblcommenthide" Font-Size="X-Small" CommandName="lblcommenthide" OnCommand="btnCommand" Font-Bold="true" ForeColor="White" BackColor="Blue" runat="server" Visible="false" Text="hide..." />
                                        </div>

                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <asp:GridView ID="gvFile" HeaderStyle-BackColor="#ffb3b3" Visible="true" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-hover table-responsive" OnRowDataBound="Master_RowDataBound">

                                            <Columns>
                                                <asp:TemplateField HeaderText="ไฟล์เอกสารเพิ่มเติม" ItemStyle-BackColor="#ffe6e6" ControlStyle-BackColor="#ffe6e6">
                                                    <ItemTemplate>
                                                        <div class="col-xs-10">
                                                            <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />

                                                        </div>
                                                        <div class="col-xs-2">
                                                            <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-xs btn-default" data-original-title="Download" Target="_blank"><i class="fa fa-download"></i></asp:HyperLink>

                                                            <asp:HiddenField runat="server" ID="hidFile11" Value='<%# Eval("Download") %>' />
                                                        </div>

                                                    </ItemTemplate>

                                                </asp:TemplateField>

                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>


                                <div class="form-group" style="text-align: center">
                                    <div class="col-xs-6">
                                        <asp:LinkButton ID="btny1" CssClass="btn btn-xs btn-success btn_" runat="server" data-toggle="tooltip" title="Approve" OnCommand="btnCommand" CommandName="btnapprove_detail" CommandArgument="1"><i class="glyphicon glyphicon-ok-circle"></i> Approve</asp:LinkButton>

                                    </div>
                                    <div class="col-xs-6">
                                        <asp:LinkButton ID="btnn1" CssClass="btn  btn-xs btn-danger btn_" runat="server" data-toggle="tooltip" title="Reject" OnClientClick="return confirm('คุณต้องการไม่อนุมัติข้อมูลนี้ใช่หรือไม่')" OnCommand="btnCommand" CommandName="btnapprove_detail" CommandArgument="2">
                                   <i class="glyphicon glyphicon-remove-circle"></i> Reject</asp:LinkButton>
                                    </div>
                                </div>
                            </div>

                        </ItemTemplate>

                    </asp:FormView>

                </div>


                <div class="col-lg-12" runat="server" id="Div_Log">
                    <div id="ordine" class="modal open" role="dialog">
                        <div class="modal-dialog" style="width: 70%;">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">ช่องแสดงความคิดเห็น</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">
                                            <div class="form-group">
                                                <asp:Label ID="Label26" runat="server" Text="ความคิดเห็น" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtremark_approve" TextMode="multiline" Rows="5" runat="server" CssClass="form-control embed-responsive-item" PlaceHoldaer="........" />
                                                </div>


                                            </div>

                                            <br />

                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-10">
                                                    <asp:LinkButton ID="btncomment" CssClass="btn btn-xs btn-success" runat="server" data-toggle="tooltip" title="Approve" OnCommand="btnCommand" CommandName="btncomment"><i class="glyphicon glyphicon-ok"></i></asp:LinkButton>

                                                    <asp:LinkButton ID="Cancel_detail" class="btn btn-xs  btn-default" data-toggle="tooltip" title="Close" runat="server" OnCommand="btnCommand" CommandName="closecomment"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>



                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>



        </asp:View>
    </asp:MultiView>
</asp:Content>

