﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="MasterPriority.aspx.cs" Inherits="websystem_MasterData_MasterPriority" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:Literal ID="text" runat="server"></asp:Literal>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

                <asp:View ID="ViewIndex" runat="server">

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-blackboard"></i><strong>&nbsp; ลำดับความสำคัญ</strong></h3>
                        </div>

                        <div class="panel-body">
                            <%-- <div class="form-horizontal" role="form">--%>
                            <div class="form-group">

                                <asp:LinkButton ID="btnaddholder" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="AddPriority" runat="server" CommandName="CmdAddHolder" OnCommand="btnCommand"><i class="fa fa-plus"></i></asp:LinkButton>
                            </div>

                            <%------------------------ Div ADD ผูกผู้ถือครอง Support and Holder ------------------------%>

                            <asp:Panel ID="Panel_Add" runat="server" Visible="false">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><i class="glyphicon glyphicon-plus"></i><strong>&nbsp; เพิ่มลำดับความสำคัญ</strong></h4>
                                    <div class="form-horizontal" role="form">
                                        <%--<div class="panel panel-default">--%>
                                        <div class="panel-heading">


                                            <div class="form-group">
                                                <asp:Label ID="Label26" runat="server" Text="ลำดับความสำคัญ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-8">
                                                    <asp:TextBox ID="txtpriority" runat="server" CssClass="form-control" PlaceHoldaer="........" />
                                                </div>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="txtpriority" Font-Size="11"
                                                    ErrorMessage="กรุณาใส่ลำดับความสำคัญ"
                                                    ValidationExpression="กรุณาใส่ลำดับความสำคัญ"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                    ValidationGroup="Save" Display="None"
                                                    ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                    ControlToValidate="txtpriority"
                                                    ValidationExpression="^[\s\S]{0,1000}$"
                                                    SetFocusOnError="true" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />

                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label1" runat="server" Text="Base Line" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-8">
                                                    <asp:TextBox ID="txtbase" runat="server" CssClass="form-control" PlaceHoldaer="........" />
                                                </div>

                                                <asp:RequiredFieldValidator ID="RqRetxtprice22" ValidationGroup="Save" runat="server" Display="None"
                                                    ControlToValidate="txtbase" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกจำนวน" />
                                                <asp:RegularExpressionValidator ID="Retxtprice22" runat="server" ValidationGroup="Save" Display="None"
                                                    ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                    ControlToValidate="txtbase"
                                                    ValidationExpression="^[0-9]{1,10}$" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqRetxtprice22" Width="160" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender223" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Retxtprice22" Width="160" />

                                            </div>


                                            <div class="form-group">
                                                <asp:Label ID="lbstatus" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                                <div class="col-sm-8">
                                                    <asp:DropDownList ID="ddStatusadd" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="1" Text="Online" />
                                                        <asp:ListItem Value="0" Text="Offline" />
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-3">
                                                    <asp:LinkButton ID="lbladd" ValidationGroup="Save" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="btnAdd" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="lblcancel" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancel" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <hr />
                                    <%--  </div>--%>
                                </div>
                            </asp:Panel>


                            <asp:GridView ID="GvMaster" runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="primary"
                                HeaderStyle-Height="40px"
                                AllowPaging="true"
                                DataKeyNames="PIDX"
                                PageSize="10"
                                OnRowDataBound="Master_RowDataBound"
                                OnRowEditing="Master_RowEditing"
                                OnRowCancelingEdit="Master_RowCancelingEdit"
                                OnPageIndexChanging="Master_PageIndexChanging"
                                OnRowUpdating="Master_RowUpdating">

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>
                                <Columns>

                                    <asp:TemplateField HeaderText="#">

                                        <ItemTemplate>
                                            <asp:Label ID="lblpidx" runat="server" Visible="false" Text='<%# Eval("PIDX") %>' />
                                            <%# (Container.DataItemIndex +1) %>
                                        </ItemTemplate>


                                        <EditItemTemplate>
                                            <div class="form-horizontal" role="form">
                                                <div class="panel-heading">
                                                    <div class="form-group">
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txtPIDX" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("PIDX")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label26" runat="server" Text="ลำดับความสำคัญ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-8">
                                                            <asp:TextBox ID="txtpriority_edit" runat="server" CssClass="form-control" Text='<%# Eval("Priority_name")%>' />
                                                        </div>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="Save_edit" runat="server" Display="None" ControlToValidate="txtpriority_edit" Font-Size="11"
                                                            ErrorMessage="กรุณาใส่ลำดับความสำคัญ"
                                                            ValidationExpression="กรุณาใส่ลำดับความสำคัญ"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                            ValidationGroup="Save_edit" Display="None"
                                                            ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                            ControlToValidate="txtpriority_edit"
                                                            ValidationExpression="^[\s\S]{0,1000}$"
                                                            SetFocusOnError="true" />

                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />

                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label2" runat="server" Text="ลำดับความสำคัญ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-8">
                                                            <asp:TextBox ID="txtbase_edit" runat="server" CssClass="form-control" Text='<%# Eval("baseline")%>' />
                                                        </div>

                                                        <asp:RequiredFieldValidator ID="RqRetxtprsice22" ValidationGroup="Save_edit" runat="server" Display="None"
                                                            ControlToValidate="txtbase_edit" Font-Size="11"
                                                            ErrorMessage="กรุณากรอกจำนวน" />
                                                        <asp:RegularExpressionValidator ID="Retxstprice22" runat="server" ValidationGroup="Save_edit" Display="None"
                                                            ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                            ControlToValidate="txtbase_edit"
                                                            ValidationExpression="^[0-9]{1,10}$" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqRetxtprsice22" Width="160" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender223" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Retxstprice22" Width="160" />
                                                    </div>


                                                    <div class="form-group">
                                                        <asp:Label ID="lbstatus" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                                        <div class="col-sm-8">
                                                            <asp:DropDownList ID="ddStatusUpdate" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("Priority_status") %>'>
                                                                <asp:ListItem Value="1" Text="Online" />
                                                                <asp:ListItem Value="0" Text="Offline" />
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <div class="col-sm-2 col-sm-offset-10">
                                                            <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server" ValidationGroup="Save" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>


                                        </EditItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ลำดับความสำคัญ" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lborg" runat="server" Text='<%# Eval("Priority_name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                      <asp:TemplateField HeaderText="Base Line" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lbbase" runat="server" Text='<%# Eval("baseline") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lbdep" runat="server" Text='<%# Eval("StatusDetail") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                            <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDel" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("PIDX") %>'><i class="fa fa-trash-o"></i></asp:LinkButton>
                                        </ItemTemplate>

                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>

                        </div>
                    </div>
                </asp:View>
            </asp:MultiView>

        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>

