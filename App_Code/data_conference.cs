using System;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_conference")]
public class data_conference
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    [XmlElement("conf_mode")]
    public string conf_mode { get; set; }

    [XmlElement("conf_user_list_u0")]
    public conf_user_detail_u0[] conf_user_list_u0 { get; set; }
    [XmlElement("search_conf_data_list")]
    public search_conf_data_detail[] search_conf_data_list { get; set; }
}

[Serializable]
public class conf_user_detail_u0
{
    [XmlElement("u0_idx")]
    public int u0_idx { get; set; }
    [XmlElement("user_name")]
    public string user_name { get; set; }
    [XmlElement("user_firstname")]
    public string user_firstname { get; set; }
    [XmlElement("user_lastname")]
    public string user_lastname { get; set; }
    [XmlElement("user_mail")]
    public string user_mail { get; set; }
    [XmlElement("user_company")]
    public string user_company { get; set; }
    [XmlElement("user_pass_real")]
    public string user_pass_real { get; set; }
    [XmlElement("user_pass")]
    public string user_pass { get; set; }
    [XmlElement("conf_type")]
    public int conf_type { get; set; }
    [XmlElement("conf_round")]
    public int conf_round { get; set; }
    [XmlElement("user_flag")]
    public int user_flag { get; set; }
    [XmlElement("user_status")]
    public int user_status { get; set; }
    [XmlElement("stamp_date")]
    public string stamp_date { get; set; }
    [XmlElement("create_date")]
    public string create_date { get; set; }
    [XmlElement("update_date")]
    public string update_date { get; set; }
}

[Serializable]
public class search_conf_data_detail
{
    [XmlElement("s_u0_idx")]
    public string s_u0_idx { get; set; }
    [XmlElement("s_user_name")]
    public string s_user_name { get; set; }
    [XmlElement("s_user_firstname")]
    public string s_user_firstname { get; set; }
    [XmlElement("s_user_lastname")]
    public string s_user_lastname { get; set; }
    [XmlElement("s_user_mail")]
    public string s_user_mail { get; set; }
    [XmlElement("s_user_company")]
    public string s_user_company { get; set; }
    [XmlElement("s_user_pass_real")]
    public string s_user_pass_real { get; set; }
    [XmlElement("s_user_pass")]
    public string s_user_pass { get; set; }
    [XmlElement("s_conf_type")]
    public string s_conf_type { get; set; }
    [XmlElement("s_conf_round")]
    public string s_conf_round { get; set; }
    [XmlElement("s_user_flag")]
    public string s_user_flag { get; set; }
    [XmlElement("s_user_status")]
    public string s_user_status { get; set; }
    [XmlElement("s_stamp_date")]
    public string s_stamp_date { get; set; }
}