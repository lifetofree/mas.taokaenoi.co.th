﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="MD-ProductList.aspx.cs" Inherits="websystem_masterdata_MD_ProductList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>

    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewIndex" runat="server">
            <div class="col-md-12">
                <asp:LinkButton ID="btnToInsert" CssClass="btn btn-primary" runat="server"
                    CommandName="btnToInsert" OnCommand="btnCommand">
                     <i class="fa fa-plus-square"></i> สร้างรายการสินค้า/บริการ</asp:LinkButton>

                <asp:GridView ID="GvMaster"
                    runat="server"
                    AutoGenerateColumns="false"
                    DataKeyNames="ProLIDX"
                    CssClass="table table-striped table-bordered table-responsive col-md-12 m-t-10"
                    HeaderStyle-CssClass="info"
                    AllowPaging="true"
                    PageSize="10"
                    OnRowEditing="Master_RowEditing"
                    OnRowUpdating="Master_RowUpdating"
                    OnRowCancelingEdit="Master_RowCancelingEdit"
                    OnPageIndexChanging="Master_PageIndexChanging"
                    OnRowDataBound="Master_RowDataBound">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="1" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">No result</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <%# (Container.DataItemIndex +1) %>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="ProLIDX" runat="server" CssClass="form-control"
                                    Visible="False" Text='<%# Eval("ProLIDX")%>' />
                                <div class="col-md-10 col-md-offset-1">
                                    <div class="row col-md-6 ">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="pull-left">ประเภทสินค้า</label>
                                                    <asp:Label ID="lbddlProTIDX" runat="server" Text='<%# Bind("ProTIDX") %>' Visible="false" />
                                                    <asp:DropDownList ID="ddlProTIDXUpdate" AutoPostBack="true" runat="server"
                                                        CssClass="form-control">
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="requiredddlProTIDXUpdate"
                                                        ValidationGroup="saveProLNameUpdate" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="ddlProTIDXUpdate"
                                                        InitialValue="0"
                                                        Font-Size="1em" ForeColor="Red"
                                                        CssClass="pull-left"
                                                        ErrorMessage="กรุณากรอกประเภทสินค้า" />
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="pull-left">รายการสินค้า</label>
                                                    <asp:UpdatePanel ID="panelProLNameUpdate" runat="server">
                                                        <ContentTemplate>

                                                            <asp:TextBox ID="txtProLNameUpdate" runat="server" CssClass="form-control"
                                                                Text='<%# Eval("ProLName")%>' />
                                                            <asp:RequiredFieldValidator ID="requiredProLNameUpdate"
                                                                ValidationGroup="saveProLNameUpdate" runat="server"
                                                                Display="Dynamic"
                                                                SetFocusOnError="true"
                                                                ControlToValidate="txtProLNameUpdate"
                                                                Font-Size="1em" ForeColor="Red"
                                                                CssClass="pull-left"
                                                                ErrorMessage="กรุณากรอกรายการสินค้า" />
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="pull-left">รสชาติ</label>
                                                    <asp:UpdatePanel ID="pnlProLFlavor" runat="server">
                                                        <ContentTemplate>
                                                            <asp:TextBox ID="txtProLFlavorUpdate" runat="server" CssClass="form-control"
                                                                Text='<%# Eval("ProLFlavor")%>'
                                                                placeholder="รสชาติ..." />
                                                            <asp:RequiredFieldValidator ID="RequiredtxtProLFlavorUpdate"
                                                                ValidationGroup="saveProLNameUpdate" runat="server"
                                                                Display="Dynamic"
                                                                SetFocusOnError="true"
                                                                ControlToValidate="txtProLFlavorUpdate"
                                                                Font-Size="1em" ForeColor="Red"
                                                                CssClass="pull-left"
                                                                ErrorMessage="กรุณากรอกรสชาติ" />
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </div>

                                            <div class="col-md-10">
                                                <div class="form-group">
                                                    <label class="pull-left">นํ้าหนัก</label>
                                                    <asp:UpdatePanel ID="pnlProLWeight" runat="server">
                                                        <ContentTemplate>
                                                            <asp:TextBox ID="txtProLWeightUpdate" runat="server" CssClass="form-control"
                                                                placeholder="นํ้าหนัก..." TextMode="Number"
                                                                Text='<%# Eval("ProLWeight")%>' />
                                                            <asp:RequiredFieldValidator ID="RequiredtxtProLWeightUpdate"
                                                                ValidationGroup="saveProLNameUpdate" runat="server"
                                                                Display="Dynamic"
                                                                SetFocusOnError="true"
                                                                ControlToValidate="txtProLWeightUpdate"
                                                                Font-Size="1em" ForeColor="Red"
                                                                CssClass="pull-left"
                                                                ErrorMessage="กรุณากรอกนํ้าหนัก" />
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <br />
                                                    <br />
                                                    <label>กรัม</label>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="pull-left">สถานะ</label>
                                                    <asp:DropDownList ID="ddlProLStatusUpdate" AutoPostBack="false" runat="server"
                                                        CssClass="form-control" SelectedValue='<%# Eval("ProLStatus") %>'>
                                                        <asp:ListItem Value="1" Text="Online"></asp:ListItem>
                                                        <asp:ListItem Value="0" Text="Offline"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="pull-left">
                                                    <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server"
                                                        ValidationGroup="saveProLNameUpdate" CommandName="Update"
                                                        OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')">
                                                         บันทึก
                                                    </asp:LinkButton>
                                                    <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server"
                                                        CommandName="Cancel">ยกเลิก</asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </EditItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="รายการสินค้า" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <asp:Label ID="ProLName" runat="server" Text='<%# "<b>"+Eval("ProLName") +"</b>"+" "+ Eval("ProLFlavor")+" "+ Eval("ProLWeight")+" กรัม" %>' />
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ประเภทสินค้า" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <asp:Label ID="ProTName" runat="server" Text='<%# Eval("ProTName") %>' />
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <asp:Label ID="ProLStatus" runat="server" Text='<%# getStatus((int)Eval("ProLStatus")) %>' />
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Manage" ItemStyle-HorizontalAlign="center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                    data-toggle="tooltip" title="แก้ไข"><i class="glyphicon glyphicon-pencil"></i></asp:LinkButton>
                                <asp:LinkButton ID="delete" CssClass="text-trash" runat="server" data-toggle="tooltip"
                                    title="ลบ" CommandName="btnDelete" OnCommand="btnCommand"
                                    CommandArgument='<%# Eval("ProLIDX") %>'
                                    OnClientClick="return confirm('คุณค้องการลบข้อมูลนี้ใช่หรือไม่')">
                                   <i class="glyphicon glyphicon-trash"></i>
                                </asp:LinkButton>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>

            </div>
        </asp:View>

        <!-- Start Insert Form -->
        <asp:View ID="ViewInsert" runat="server">
            <div class="col-md-10 col-md-offset-2">

                <div class="row col-md-6 ">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <asp:LinkButton CssClass="btn btn-danger" data-toggle="tooltip" title="Back" runat="server"
                                    CommandName="btnCancel" OnCommand="btnCommand">ย้อนกลับ</asp:LinkButton>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="pull-left">ประเภทสินค้า</label>
                                <asp:DropDownList ID="ddlProTIDX" AutoPostBack="true" runat="server"
                                    CssClass="form-control">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="requiredddlProTIDX"
                                    ValidationGroup="save" runat="server"
                                    Display="Dynamic"
                                    SetFocusOnError="true"
                                    ControlToValidate="ddlProTIDX"
                                    InitialValue="0"
                                    Font-Size="1em" ForeColor="Red"
                                    ErrorMessage="กรุณากรอกประเภทสินค้า" />
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>รายการสินค้า</label>
                                <asp:UpdatePanel ID="panelProLName" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtProLName" runat="server" CssClass="form-control"
                                            placeholder="รายการสินค้า..." />
                                        <asp:RequiredFieldValidator ID="requiredProLName"
                                            ValidationGroup="save" runat="server"
                                            Display="Dynamic"
                                            SetFocusOnError="true"
                                            ControlToValidate="txtProLName"
                                            Font-Size="1em" ForeColor="Red"
                                            CssClass="pull-left"
                                            ErrorMessage="กรุณากรอกรายการสินค้า" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>รสชาติ</label>
                                <asp:UpdatePanel ID="pnlProLFlavor" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtProLFlavor" runat="server" CssClass="form-control"
                                            placeholder="รสชาติ..." />
                                        <asp:RequiredFieldValidator ID="RequiredtxtProLFlavor"
                                            ValidationGroup="save" runat="server"
                                            Display="Dynamic"
                                            SetFocusOnError="true"
                                            ControlToValidate="txtProLFlavor"
                                            Font-Size="1em" ForeColor="Red"
                                            CssClass="pull-left"
                                            ErrorMessage="กรุณากรอกรสชาติ" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>

                        <div class="col-md-10">
                            <div class="form-group">
                                <label>นํ้าหนัก</label>
                                <asp:UpdatePanel ID="pnlProLWeight" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtProLWeight" runat="server" CssClass="form-control"
                                            placeholder="นํ้าหนัก..." TextMode="Number" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                            ValidationGroup="save" runat="server"
                                            Display="Dynamic"
                                            SetFocusOnError="true"
                                            ControlToValidate="txtProLWeight"
                                            Font-Size="1em" ForeColor="Red"
                                            CssClass="pull-left"
                                            ErrorMessage="กรุณากรอกนํ้าหนัก" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <br />
                                <br />
                                <label>กรัม</label>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>สถานะ</label>
                                <asp:DropDownList ID="ddlProLStatus" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="1" Text="Online" />
                                    <asp:ListItem Value="0" Text="Offline" />
                                </asp:DropDownList>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <asp:Button CssClass="btn btn-success" runat="server" CommandName="btnInsert" OnCommand="btnCommand" Text="บันทึก" ValidationGroup="save" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:View>
        <!-- End Insert Form -->


    </asp:MultiView>

</asp:Content>
