﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using MessagingToolkit.QRCode.Codec;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;


public partial class websystem_qa_cims : System.Web.UI.Page
{

    #region Connect
    function_tool _funcTool = new function_tool();
    data_employee _dataEmployee = new data_employee();
    data_qa_cims _data_qa_cims = new data_qa_cims();


    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];

    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    //-- employee --//

    //-- master registration device --//
    static string _urlCimsGetEquipment_name = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetEquipment_name"];
    static string _urlCimsGetBrand = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetBrand"];
    static string _urlCimsGetEquipment_type = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetEquipment_type"];
    static string _urlCimsGetUnit = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetUnit"];
    static string _urlCimsGetCal_type = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetCal_type"];
    static string _urlCimsGetM0_Frequency = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetM0_Frequency"];
    static string _urlCimsGetM0_Resolution = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetM0_Resolution"];

    //--document 
    static string _urlCimsGetSelectDecisionNode1 = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetSelectDecisionNode1"];
    static string _urlCimsGetEquipmentList = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetEquipmentList"];
    static string _urlCimsGetPlace = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetPlace"];
    static string _urlCimsGetSelectSearchTool = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetSelectSearchTool"];
    static string _urlCimsSetDocumentCalibration = _serviceUrl + ConfigurationManager.AppSettings["urlCimsSetDocumentCalibration"];
    static string _urlCimsGetDocumentCalibration = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetDocumentCalibration"];
    static string _urlCimsGetDocumentDetails = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetDocumentDetails"];
    static string _urlCimsGetCalibrationList = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetCalibrationList"];
    static string _urlCimsGetLab = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetLab"];
    static string _urlCimsSetLabCalibration = _serviceUrl + ConfigurationManager.AppSettings["urlCimsSetLabCalibration"];
    static string _urlCimsGetLabCalibrationInLab = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetLabCalibrationInLab"];
    static string _urlCimsSetAcceptCalibrationInLab = _serviceUrl + ConfigurationManager.AppSettings["urlCimsSetAcceptCalibrationInLab"];
    static string _urlCimsGetSelectSearchToolForm = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetSelectSearchToolForm"];
    static string _urlCimsGetFormRecordList = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetFormRecordList"];
    static string _urlCimsGetFormRecordListHeader = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetFormRecordListHeader"];
    static string _urlCimsGetSearchFormTopics = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetSearchFormTopics"];
    static string _urlCimsGetSelectDecisionNode = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetSelectDecisionNode"];
    static string _urlCimsSetRecordCalibration = _serviceUrl + ConfigurationManager.AppSettings["urlCimsSetRecordCalibration"];
    static string _urlCimsGetDocCalibrationSupervisor = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetDocCalibrationSupervisor"];
    static string _urlCimsGetResultCalibrationView = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetResultCalibrationView"];
    static string _urlCimsGetResultCalibrationTopicsView = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetResultCalibrationTopicsView"];
    static string _urlCimsSetApproveSupervisor = _serviceUrl + ConfigurationManager.AppSettings["urlCimsSetApproveSupervisor"];
    static string _urlCimsGetViewDetails = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetViewDetails"];
    static string _urlCimsSetCertificate = _serviceUrl + ConfigurationManager.AppSettings["urlCimsSetCertificate"];
    

    int _emp_idx = 0;
    int _default_int = 0;
    int _r1Regis = 0;
    int ExternalID = 2;
    int ExternalCondition = 6;

    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    bool _flag_qmr = false;

    //datable departmentexcel
    string calibrationpoint_dataexcel = "";
    string Temp_calibrationpoint_dataexcel = "";

    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];

        ViewState["org_permission"] = _dataEmployee.employee_list[0].org_idx;
        ViewState["rdept_permission"] = _dataEmployee.employee_list[0].rdept_idx;
        ViewState["rsec_permission"] = _dataEmployee.employee_list[0].rsec_idx;

    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            initPage();
            SetInitialRow();
            // initPageLoad();

        }

        linkBtnTrigger(lbCreate);
    }


    #region class default 
    protected void initPage()
    {

        dataSetCalibrationPoint();
        //setActiveTab("docRegistration", 0, 0, 0);
        setActiveTab("docDetailList", 0, 0, 0, 0, 0, 0, 0);
        DataTableCalEquipmentOld();
        DataTableCalEquipmentNew();
        DataTableCalibrationPoint();
        DataTableCalibrationPointInsert();

        DataTableAcceptance();
        DataTableAcceptanceInsert();

        DataTableMeasuring();
        DataTableMeasuringInsert();

        DataTableRange();
        DataTableRangeInsert();

        DataTableResolution();
        DataTableResolutionInsert();

    }

    #endregion

    #region clear data set 
    protected void ClearDataCalEquipmentOld()
    {
        //clear set equipment list
        ViewState["vsDataEquipmentCalOld"] = null;
        gvListEquipmentCalOld.DataSource = ViewState["vsDataEquipmentCalOld"];
        gvListEquipmentCalOld.DataBind();
        gvListEquipmentCalOld.Visible = false;

        DataTableCalEquipmentOld();

    }

    protected void ClearDataCalEquipmentNew()
    {
        //clear set equipment list
        ViewState["vsDataEquipmentCalNew"] = null;
        gvListEquipmentCalNew.DataSource = ViewState["vsDataEquipmentCalNew"];
        gvListEquipmentCalNew.DataBind();
        gvListEquipmentCalNew.Visible = false;

        DataTableCalEquipmentNew();

    }

    protected void ClearDataTextbox()
    {
        tbIDNo.Text = String.Empty;
        //tbResolution.Text = String.Empty;
        tbserailnumber.Text = String.Empty;
        //tbRangeOfUseMin.Text = String.Empty;
        //tbRangeOfUseMax.Text = String.Empty;
        tbdetails_equipment.Text = String.Empty;
        //tbMeasuringRangeMin.Text = String.Empty;
        //tbMeasuringRangeMax.Text = String.Empty;
        //tbAcceptanceCriteria.Text = String.Empty;
        //tbCalibrationFrequency.Text = String.Empty;

    }

    protected void ClearDataCalibrationPoint()
    {
        //clear set equipment list
        ViewState["vsDataCalibrationPoint"] = null;
        gvCalibrationPoint.DataSource = ViewState["vsDataCalibrationPoint"];
        gvCalibrationPoint.DataBind();
        // gvCalibrationPoint.Visible = false;

        DataTableCalibrationPoint();

    }

    protected void ClearDataAcceptance()
    {
        //clear set equipment list
        ViewState["vsDataAcceptance"] = null;
        gvAcceptance.DataSource = ViewState["vsDataAcceptance"];
        gvAcceptance.DataBind();
        // gvCalibrationPoint.Visible = false;

        DataTableAcceptance();

    }

    protected void ClearDataMeasuring()
    {
        //clear set equipment list
        ViewState["vsDataMeasuring"] = null;
        gvMeasuring.DataSource = ViewState["vsDataMeasuring"];
        gvMeasuring.DataBind();
        // gvCalibrationPoint.Visible = false;

        DataTableMeasuring();

    }

    protected void ClearDataRange()
    {
        //clear set equipment list
        ViewState["vsDataRange"] = null;
        gvRange.DataSource = ViewState["vsDataRange"];
        gvRange.DataBind();
        // gvCalibrationPoint.Visible = false;

        DataTableRange();

    }

    protected void ClearDataResolution()
    {
        //clear set equipment list
        ViewState["vsDataResolution"] = null;
        gvResolution.DataSource = ViewState["vsDataResolution"];
        gvResolution.DataBind();
        // gvCalibrationPoint.Visible = false;

        DataTableResolution();

    }


    #endregion

    #region data set 

    protected void DataTableCalibrationPoint()
    {

        var ds_calibration_point_data = new DataSet();
        ds_calibration_point_data.Tables.Add("CalibrationPointData");
        ds_calibration_point_data.Tables[0].Columns.Add("CalibrationPointUnitIDX", typeof(int));
        ds_calibration_point_data.Tables[0].Columns.Add("IDNUMBER", typeof(String));
        ds_calibration_point_data.Tables[0].Columns.Add("CalibrationPointUnit", typeof(String));
        ds_calibration_point_data.Tables[0].Columns.Add("CalibrationPoint", typeof(String));
        ViewState["vsDataCalibrationPoint"] = ds_calibration_point_data;
        gvCalibrationPoint.DataBind();

    }

    protected void DataTableAcceptance()
    {

        var ds_Acceptance_data = new DataSet();
        ds_Acceptance_data.Tables.Add("AcceptanceData");

        ds_Acceptance_data.Tables[0].Columns.Add("AcceptanceUnitIDX", typeof(int));
        ds_Acceptance_data.Tables[0].Columns.Add("IDNUMBERAcceptance", typeof(String));
        ds_Acceptance_data.Tables[0].Columns.Add("AcceptanceUnit", typeof(String));
        ds_Acceptance_data.Tables[0].Columns.Add("Acceptance", typeof(String));

        ViewState["vsDataAcceptance"] = ds_Acceptance_data;
        gvAcceptance.DataBind();

    }

    protected void DataTableMeasuring()
    {

        var ds_Measuring_data = new DataSet();
        ds_Measuring_data.Tables.Add("MeasuringData");

        ds_Measuring_data.Tables[0].Columns.Add("MeasuringUnitIDX", typeof(int));
        ds_Measuring_data.Tables[0].Columns.Add("IDNUMBERMeasuring", typeof(String));
        ds_Measuring_data.Tables[0].Columns.Add("MeasuringUnit", typeof(String));
        ds_Measuring_data.Tables[0].Columns.Add("MeasuringStart", typeof(String));
        ds_Measuring_data.Tables[0].Columns.Add("MeasuringEnd", typeof(String));

        ViewState["vsDataMeasuring"] = ds_Measuring_data;
        gvMeasuring.DataBind();

    }

    protected void DataTableRange()
    {

        var ds_Range_data = new DataSet();
        ds_Range_data.Tables.Add("RangeData");

        ds_Range_data.Tables[0].Columns.Add("RangeUnitIDX", typeof(int));
        ds_Range_data.Tables[0].Columns.Add("IDNUMBERRange", typeof(String));
        ds_Range_data.Tables[0].Columns.Add("RangeUnit", typeof(String));
        ds_Range_data.Tables[0].Columns.Add("RangeEnd", typeof(String));
        ds_Range_data.Tables[0].Columns.Add("RangeStart", typeof(String));

        ViewState["vsDataRange"] = ds_Range_data;
        gvRange.DataBind();

    }

    protected void DataTableResolution()
    {

        var ds_Resolution_data = new DataSet();
        ds_Resolution_data.Tables.Add("ResolutionData");
    
        ds_Resolution_data.Tables[0].Columns.Add("IDNUMBERResolution", typeof(String));
        ds_Resolution_data.Tables[0].Columns.Add("ResolutionIDX", typeof(int));
        ds_Resolution_data.Tables[0].Columns.Add("Resolution", typeof(String));
      
        ViewState["vsDataResolution"] = ds_Resolution_data;
        gvResolution.DataBind();

    }

    protected void DataTableCalibrationPointInsert()
    {

        var ds_calibration_point_data = new DataSet();
        ds_calibration_point_data.Tables.Add("CalibrationPointData_insert");
        ds_calibration_point_data.Tables[0].Columns.Add("CalibrationPointUnitIDX_insert", typeof(int));
        ds_calibration_point_data.Tables[0].Columns.Add("IDNUMBER_insert", typeof(String));
        ds_calibration_point_data.Tables[0].Columns.Add("CalibrationPointUnit_insert", typeof(String));
        ds_calibration_point_data.Tables[0].Columns.Add("CalibrationPoint_insert", typeof(String));
        ds_calibration_point_data.Tables[0].Columns.Add("Serial_no_insert", typeof(String));

        ViewState["vsDataCalibrationPoint_insert"] = ds_calibration_point_data;
        gvCalPointInsert.DataBind();

    }

    protected void DataTableAcceptanceInsert()
    {

        var ds_AcceptanceInsert_data = new DataSet();

        ds_AcceptanceInsert_data.Tables.Add("AcceptanceData_insert");

        ds_AcceptanceInsert_data.Tables[0].Columns.Add("AcceptanceUnitIDX_insert", typeof(int));
        ds_AcceptanceInsert_data.Tables[0].Columns.Add("IDNUMBERAcceptance_insert", typeof(String));
        ds_AcceptanceInsert_data.Tables[0].Columns.Add("AcceptanceUnit_insert", typeof(String));
        ds_AcceptanceInsert_data.Tables[0].Columns.Add("Acceptance_insert", typeof(String));
        ds_AcceptanceInsert_data.Tables[0].Columns.Add("Serial_no_Acceptance_insert", typeof(String));

        ViewState["vsDataAcceptance_insert"] = ds_AcceptanceInsert_data;
        gvAcceptanceInsert.DataBind();

    }

    protected void DataTableMeasuringInsert()
    {

        var ds_MeasuringInsert_data = new DataSet();

        ds_MeasuringInsert_data.Tables.Add("MeasuringData_insert");

        ds_MeasuringInsert_data.Tables[0].Columns.Add("MeasuringUnitIDX_insert", typeof(int));
        ds_MeasuringInsert_data.Tables[0].Columns.Add("IDNUMBERMeasuring_insert", typeof(String));
        ds_MeasuringInsert_data.Tables[0].Columns.Add("MeasuringUnit_insert", typeof(String));
        ds_MeasuringInsert_data.Tables[0].Columns.Add("MeasuringEnd_insert", typeof(String));
        ds_MeasuringInsert_data.Tables[0].Columns.Add("MeasuringStart_insert", typeof(String));
        ds_MeasuringInsert_data.Tables[0].Columns.Add("Serial_no_Measuring_insert", typeof(String));

        ViewState["vsDataMeasuring_insert"] = ds_MeasuringInsert_data;
        gvMeasuringInsert.DataBind();

    }

    protected void DataTableRangeInsert()
    {

        var ds_gvRangeInsert_data = new DataSet();

        ds_gvRangeInsert_data.Tables.Add("gvRangeData_insert");

        ds_gvRangeInsert_data.Tables[0].Columns.Add("RangeUnitIDX_insert", typeof(int));
        ds_gvRangeInsert_data.Tables[0].Columns.Add("IDNUMBERRange_insert", typeof(String));
        ds_gvRangeInsert_data.Tables[0].Columns.Add("RangeUnit_insert", typeof(String));
        ds_gvRangeInsert_data.Tables[0].Columns.Add("RangeEnd_insert", typeof(String));
        ds_gvRangeInsert_data.Tables[0].Columns.Add("RangeStart_insert", typeof(String));
        ds_gvRangeInsert_data.Tables[0].Columns.Add("Serial_no_Range_insert", typeof(String));

        ViewState["vsDataRange_insert"] = ds_gvRangeInsert_data;
        gvRangeInsert.DataBind();

    }

    protected void DataTableResolutionInsert()
    {

        var ds_gvResolutionInsert_data = new DataSet();

        ds_gvResolutionInsert_data.Tables.Add("gvResolutionData_insert");

        ds_gvResolutionInsert_data.Tables[0].Columns.Add("ResolutionIDX_insert", typeof(int));     
        ds_gvResolutionInsert_data.Tables[0].Columns.Add("IDNUMBERResolution_insert", typeof(String));
        ds_gvResolutionInsert_data.Tables[0].Columns.Add("Resolution_insert", typeof(String));
        ds_gvResolutionInsert_data.Tables[0].Columns.Add("Serial_no_Resolution_insert", typeof(String));

        ViewState["vsDataResolution_insert"] = ds_gvResolutionInsert_data;
        gvResolutionInsert.DataBind();

    }

    protected void DataTableCalEquipmentNew()
    {
        var ds_equipment_new_data = new DataSet();
        ds_equipment_new_data.Tables.Add("EquipmentNewData");
        ds_equipment_new_data.Tables[0].Columns.Add("M0DeviceIDX_New", typeof(int));
        ds_equipment_new_data.Tables[0].Columns.Add("M0BrandIDX_New", typeof(int));
        ds_equipment_new_data.Tables[0].Columns.Add("EquipmentTypeIDX_New", typeof(int));
        ds_equipment_new_data.Tables[0].Columns.Add("HolderRsecIDX_New", typeof(int));
        ds_equipment_new_data.Tables[0].Columns.Add("HolderOrgIDX_New", typeof(int));
        ds_equipment_new_data.Tables[0].Columns.Add("HolderRdeptIDX_New", typeof(int));
        //ds_equipment_new_data.Tables[0].Columns.Add("RangeOfUseMin", typeof(decimal));
        //ds_equipment_new_data.Tables[0].Columns.Add("RangeOfUseMax", typeof(decimal));
        //ds_equipment_new_data.Tables[0].Columns.Add("RangeOfUseUnit", typeof(int));
        ds_equipment_new_data.Tables[0].Columns.Add("CalibrationType", typeof(int));
        //ds_equipment_new_data.Tables[0].Columns.Add("Acceptance_Criteria", typeof(decimal));
        //ds_equipment_new_data.Tables[0].Columns.Add("Acceptance_Criteria_Unit", typeof(int));
        //ds_equipment_new_data.Tables[0].Columns.Add("Resolution", typeof(decimal));
        //ds_equipment_new_data.Tables[0].Columns.Add("Resolution_Unit", typeof(int));
        //ds_equipment_new_data.Tables[0].Columns.Add("Measuring_Range_Min", typeof(decimal));
       // ds_equipment_new_data.Tables[0].Columns.Add("Measuring_Range_Max", typeof(decimal));
        //ds_equipment_new_data.Tables[0].Columns.Add("Measuring_Range_Unit", typeof(int));
        //ds_equipment_new_data.Tables[0].Columns.Add("CalibrationPoint", typeof(int));
        //ds_equipment_new_data.Tables[0].Columns.Add("CalibrationPoint_Unit", typeof(int));
       // ds_equipment_new_data.Tables[0].Columns.Add("Calibration_Frequency", typeof(decimal));
        ds_equipment_new_data.Tables[0].Columns.Add("Certificate_insert", typeof(int));

        ds_equipment_new_data.Tables[0].Columns.Add("IDCodeNumber_New", typeof(String));
        ds_equipment_new_data.Tables[0].Columns.Add("EquipmentName_New", typeof(String));
        ds_equipment_new_data.Tables[0].Columns.Add("EquipmentType_New", typeof(String));
        ds_equipment_new_data.Tables[0].Columns.Add("BrandName_New", typeof(String));
        ds_equipment_new_data.Tables[0].Columns.Add("Holder_New", typeof(String));
        ds_equipment_new_data.Tables[0].Columns.Add("SerialNumber_New", typeof(String));
        //ds_equipment_new_data.Tables[0].Columns.Add("RangeOfUseUnitName", typeof(String));
        ds_equipment_new_data.Tables[0].Columns.Add("Details_Equipment", typeof(String));
        ds_equipment_new_data.Tables[0].Columns.Add("CalibrationTypeName", typeof(String));
        //ds_equipment_new_data.Tables[0].Columns.Add("Acceptance_Criteria_UnitName", typeof(String));
        //ds_equipment_new_data.Tables[0].Columns.Add("Resolution_UnitName", typeof(String));
        //ds_equipment_new_data.Tables[0].Columns.Add("CalibrationPoint_UnitName", typeof(String));
        //ds_equipment_new_data.Tables[0].Columns.Add("Measuring_Range_UnitName", typeof(String));
        ds_equipment_new_data.Tables[0].Columns.Add("CalibrationPointList", typeof(String));
        ds_equipment_new_data.Tables[0].Columns.Add("CalibrationPointList_IDX", typeof(String));


        ViewState["vsDataEquipmentCalNew"] = ds_equipment_new_data;
    }

    protected void DataTableCalEquipmentOld()
    {
        var ds_equipmentold_data = new DataSet();
        ds_equipmentold_data.Tables.Add("EquipmentOldData");
        ds_equipmentold_data.Tables[0].Columns.Add("M0DeviceIDX", typeof(int));
        ds_equipmentold_data.Tables[0].Columns.Add("IDCodeNumber", typeof(String));
        ds_equipmentold_data.Tables[0].Columns.Add("EquipmentName", typeof(String));
        ds_equipmentold_data.Tables[0].Columns.Add("EquipmentType", typeof(String));
        ds_equipmentold_data.Tables[0].Columns.Add("BrandName", typeof(String));
        ds_equipmentold_data.Tables[0].Columns.Add("Holder", typeof(String));
        ds_equipmentold_data.Tables[0].Columns.Add("calDate", typeof(String));
        ds_equipmentold_data.Tables[0].Columns.Add("DueDate", typeof(String));
        ds_equipmentold_data.Tables[0].Columns.Add("SerialNumber", typeof(String));
        ViewState["vsDataEquipmentCalOld"] = ds_equipmentold_data;

    }
    #endregion

    #region event command

    protected void setRepeaterData(Repeater rptName, Object obj)
    {
        rptName.DataSource = obj;
        rptName.DataBind();
    }

    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }


    protected void navCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        setActiveTab(cmdArg, 0, 0, 0, 0, 0, 0, 0);

    }

    protected void setActiveTab(string activeTab, int uidx, int u1idx, int staidx, int place, int type_equipment, int certificate, int nodeidx)
    {
        setActiveView(activeTab, uidx, u1idx, staidx, place, type_equipment, certificate, nodeidx);
        switch (activeTab)
        {
            case "docCreate":
                li0.Attributes.Add("class", "active");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");
                break;
            case "docDetailList":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "active");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");
                break;
            case "docLab":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "active");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");
                break;
            case "docRecordCalibration":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "active");
                li4.Attributes.Add("class", "");
                break;
            case "docSupervisor":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "active");
                break;

        }
    }

    protected void setActiveView(string activeTab, int uidx, int u1idx, int staidx, int place, int type_equipment, int certificate, int nodeidx)
    {
        mvSystem.SetActiveView((View)mvSystem.FindControl(activeTab));

        tbsearchingToolID.Text = String.Empty;

        //document cal
        setFormData(fvShowDetailsSearchTool, FormViewMode.ReadOnly, null);
        setFormData(fvDetailsDocument, FormViewMode.ReadOnly, null);
        setFormData(fvCalDocumentList, FormViewMode.ReadOnly, null);
        divActionSaveCreateDocument.Visible = false;
        pnEquipmentNew.Visible = false;
        divAlertSearchEquipmentOld.Visible = false;

        //form record result
        pnValueTopicsSalt.Visible = false;
        lbHeader.Text = String.Empty;
        lbHeaderFormName.Text = String.Empty;
        pnActionRecordResult.Visible = false;
        setGridData(GvSalt, null);
        divAlertNotfindEquipment.Visible = false;

        //record certificate
        setFormData(fvRecordCertificate, FormViewMode.ReadOnly, null);

        //supervisor approve
        setFormData(fvSupervisorApprove, FormViewMode.ReadOnly, null);

        //view result
        setFormData(fvCalibrationResult, FormViewMode.ReadOnly, null);


        switch (activeTab)
        {
            case "docCreate":

                setFormData(fvEmpDetail, FormViewMode.ReadOnly, ((data_employee)ViewState["vsEmpProfile"]).employee_list);

                if (uidx == 0)
                {
                    //select node decision node 1
                    divSelectTypeEquipment.Visible = true;
                    data_qa_cims dataCimsDecisionNode1 = new data_qa_cims();
                    dataCimsDecisionNode1.qa_cims_bindnode_decision_list = new qa_cims_bindnode_decision_detail[1];
                    qa_cims_bindnode_decision_detail _selectDecisionNode1 = new qa_cims_bindnode_decision_detail();
                    _selectDecisionNode1.condition = 0;
                    dataCimsDecisionNode1.qa_cims_bindnode_decision_list[0] = _selectDecisionNode1;

                    dataCimsDecisionNode1 = callServicePostQaDocCIMS(_urlCimsGetSelectDecisionNode1, dataCimsDecisionNode1);

                    rblTypeEquipment.DataSource = dataCimsDecisionNode1.qa_cims_bindnode_decision_list;
                    rblTypeEquipment.DataTextField = "decision_name";
                    rblTypeEquipment.DataValueField = "decision_idx";
                    rblTypeEquipment.DataBind();
                    rblTypeEquipment.SelectedValue = "2";
                    getSelectPlace(ddlPlaceCreate, "0");
                    tbsearchingToolID.Focus();
                   // linkBtnTrigger(btnSearchToolID);
                    ClearDataCalEquipmentOld();
                    ClearDataCalEquipmentNew();
                    ClearDataCalibrationPoint();
                    ClearDataTextbox();
                    setActive.Text = activeTab.ToString();

                }
                else if (uidx > 0)
                {

                    data_qa_cims _dataCims = new data_qa_cims();
                    divSelectTypeEquipment.Visible = false;
                    //details document
                    _dataCims.qa_cims_calibration_document_list = new qa_cims_calibration_document_details[1];
                    qa_cims_calibration_document_details _selectDetails = new qa_cims_calibration_document_details();
                    _selectDetails.u0_cal_idx = uidx;
                    _dataCims.qa_cims_calibration_document_list[0] = _selectDetails;

                    _dataCims = callServicePostQaDocCIMS(_urlCimsGetDocumentDetails, _dataCims);
                    setFormData(fvDetailsDocument, FormViewMode.ReadOnly, _dataCims.qa_cims_calibration_document_list);

                    //list equipment
                    data_qa_cims _dataCimslist = new data_qa_cims();
                    _dataCimslist.qa_cims_u1_calibration_document_list = new qa_cims_u1_calibration_document_details[1];
                    qa_cims_u1_calibration_document_details _selectList = new qa_cims_u1_calibration_document_details();
                    _selectList.u0_cal_idx = uidx;
                    _dataCimslist.qa_cims_u1_calibration_document_list[0] = _selectList;

                    _dataCimslist = callServicePostQaDocCIMS(_urlCimsGetCalibrationList, _dataCimslist);
                    if (_dataCimslist.qa_cims_u1_calibration_document_list != null)
                    {
                        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataCimslist));
                        setFormData(fvCalDocumentList, FormViewMode.ReadOnly, _dataCimslist.qa_cims_u1_calibration_document_list);
                        var gvCalibrationList = (GridView)fvCalDocumentList.FindControl("gvCalibrationList");
                        setGridData(gvCalibrationList, _dataCimslist.qa_cims_u1_calibration_document_list);

                    }
                    else
                    {
                        litDebug.Text = "llll";
                    }

                    if (u1idx == 0)
                    {

                    }
                    else if (u1idx > 0)
                    {
                        setFormData(fvCalDocumentList, FormViewMode.ReadOnly, null);
                        //result in form
                        data_qa_cims _data_cims_view_result = new data_qa_cims();
                        _data_cims_view_result.qa_cims_u1_calibration_document_list = new qa_cims_u1_calibration_document_details[1];
                        qa_cims_u1_calibration_document_details _select_result = new qa_cims_u1_calibration_document_details();
                        _select_result.u1_cal_idx = u1idx;
                        _select_result.place_idx = place;

                        _data_cims_view_result.qa_cims_u1_calibration_document_list[0] = _select_result;
                        _data_cims_view_result = callServicePostQaDocCIMS(_urlCimsGetResultCalibrationView, _data_cims_view_result);
 
                        //result in topics
                        data_qa_cims _data_cims_view_result_topics = new data_qa_cims();
                        _data_cims_view_result_topics.qa_cims_u1_calibration_document_list = new qa_cims_u1_calibration_document_details[1];
                        qa_cims_u1_calibration_document_details _select_result_topic = new qa_cims_u1_calibration_document_details();
                        _select_result_topic.u1_cal_idx = u1idx;
                        _select_result_topic.place_idx = place;

                        _data_cims_view_result_topics.qa_cims_u1_calibration_document_list[0] = _select_result_topic;
                        _data_cims_view_result_topics = callServicePostQaDocCIMS(_urlCimsGetResultCalibrationTopicsView, _data_cims_view_result_topics);

                        string NameGridResultView = "";
                        string NameFromResultView = "";

                        if (_data_cims_view_result.return_code == 0)
                        {
                            NameGridResultView = _data_cims_view_result.qa_cims_record_result_by_form_list[0].table_result_name.ToString();
                            NameFromResultView = _data_cims_view_result.qa_cims_record_result_by_form_list[0].m0_formcal_name.ToString();
                           
                        }

                        switch (staidx)
                        {
                            case 7:
                                switch (NameGridResultView)
                                {
                                    case "GvSalt":

                                        setFormData(fvCalibrationResult, FormViewMode.ReadOnly, _data_cims_view_result.qa_cims_record_result_by_form_list);
                                        var lbNameFormView = (Label)fvCalibrationResult.FindControl("lbNameFormView");
                                        var lbNameHeaderView = (Label)fvCalibrationResult.FindControl("lbNameHeaderView");
                                        var GvSaltView = (GridView)fvCalibrationResult.FindControl("GvSaltView");
                                        var rptTopicsSaltView = (Repeater)fvCalibrationResult.FindControl("rptTopicsSaltView");

                                        setGridData(GvSaltView, _data_cims_view_result.qa_cims_record_result_by_form_list);
                                        setRepeaterData(rptTopicsSaltView, _data_cims_view_result_topics.qa_cims_record_topics_list);

                                        lbNameHeaderView.Text = "<center>" + "บริษัท เถ้าแก่น้อย ฟู๊ดแอนด์มาร์เก็ตติ้ง จำกัด (มหาชน)" + "</center>";
                                        lbNameFormView.Text = "<center>" + NameFromResultView.ToString() + "</center>";

                                        lbNameFormView.Text = NameFromResultView;

                                        data_qa_cims dataCimsDecisionSup = new data_qa_cims();
                                        dataCimsDecisionSup.qa_cims_bindnode_decision_list = new qa_cims_bindnode_decision_detail[1];
                                        qa_cims_bindnode_decision_detail _selectDecisionidxSup = new qa_cims_bindnode_decision_detail();
                                        _selectDecisionidxSup.nodidx = nodeidx;
                                        dataCimsDecisionSup.qa_cims_bindnode_decision_list[0] = _selectDecisionidxSup;

                                        dataCimsDecisionSup = callServicePostQaDocCIMS(_urlCimsGetSelectDecisionNode, dataCimsDecisionSup);

                                        setFormData(fvSupervisorApprove, FormViewMode.ReadOnly, dataCimsDecisionSup.qa_cims_bindnode_decision_list);
                                        var rd_supervisor_approve = (RadioButtonList)fvSupervisorApprove.FindControl("rd_supervisor_approve");

                                        rd_supervisor_approve.DataSource = dataCimsDecisionSup.qa_cims_bindnode_decision_list;
                                        rd_supervisor_approve.DataTextField = "decision_name";
                                        rd_supervisor_approve.DataValueField = "decision_idx";
                                        rd_supervisor_approve.DataBind();
                                       
                                        break;
                                }

                                break;

                            case 34:

                                data_qa_cims _data_view_details = new data_qa_cims();
                                _data_view_details.qa_cims_u1_calibration_document_list = new qa_cims_u1_calibration_document_details[1];
                                qa_cims_u1_calibration_document_details _viewDetails = new qa_cims_u1_calibration_document_details();
                                _viewDetails.u1_cal_idx = u1idx;
                                _data_view_details.qa_cims_u1_calibration_document_list[0] = _viewDetails;
                                _data_view_details = callServicePostQaDocCIMS(_urlCimsGetViewDetails, _data_view_details);
                               
                                setFormData(fvRecordCertificate, FormViewMode.ReadOnly, _data_view_details.qa_cims_u1_calibration_document_list);

                                data_qa_cims dataCimsDecisionCer = new data_qa_cims();
                                dataCimsDecisionCer.qa_cims_bindnode_decision_list = new qa_cims_bindnode_decision_detail[1];
                                qa_cims_bindnode_decision_detail _selectDecisionidxCer = new qa_cims_bindnode_decision_detail();
                                _selectDecisionidxCer.nodidx = nodeidx;
                                dataCimsDecisionCer.qa_cims_bindnode_decision_list[0] = _selectDecisionidxCer;

                                dataCimsDecisionCer = callServicePostQaDocCIMS(_urlCimsGetSelectDecisionNode, dataCimsDecisionCer);
                                tbNodeDecisionIDX.Text = dataCimsDecisionCer.qa_cims_bindnode_decision_list[0].decision_idx.ToString();
                             
                                break;
                        }

                    }

                }

                break;

            case "docDetailList":

                data_qa_cims dataCimsList = new data_qa_cims();

                if (uidx == 0)
                {

                    dataCimsList.qa_cims_calibration_document_list = new qa_cims_calibration_document_details[1];
                    qa_cims_calibration_document_details _selectDocCal = new qa_cims_calibration_document_details();
                    dataCimsList.qa_cims_calibration_document_list[0] = _selectDocCal;

                    dataCimsList = callServicePostQaDocCIMS(_urlCimsGetDocumentCalibration, dataCimsList);
                    ViewState["vsGetDocumentCalibration"] = dataCimsList.qa_cims_calibration_document_list;
                    setGridData(gvCalibrationDocument, ViewState["vsGetDocumentCalibration"]);
                    setActive.Text = activeTab.ToString();
                }

                break;

            case "docLab":

                if (uidx == 0)
                {
                    data_qa_cims dataInLabList = new data_qa_cims();
                    dataInLabList.qa_cims_u1_calibration_document_list = new qa_cims_u1_calibration_document_details[1];
                    qa_cims_u1_calibration_document_details _selectListInlab = new qa_cims_u1_calibration_document_details();
                    dataInLabList.qa_cims_u1_calibration_document_list[0] = _selectListInlab;
                    dataInLabList = callServicePostQaDocCIMS(_urlCimsGetLabCalibrationInLab, dataInLabList);
                    ViewState["vsCalibrationListInLab"] = dataInLabList.qa_cims_u1_calibration_document_list;
                    setGridData(gvCalibrationInLab, ViewState["vsCalibrationListInLab"]);

                    setActive.Text = activeTab.ToString();
                }

                break;

            case "docRecordCalibration":
                if (uidx == 0)
                {
                    setFormData(fvRecordCalResults, FormViewMode.Insert, null);
                    getSelectPlace((DropDownList)fvRecordCalResults.FindControl("ddlSearchPlaceSaveResult"), "0");
                    setActive.Text = "";
                }
                break;

            case "docSupervisor":
                if (uidx == 0)
                {
                    data_qa_cims dataSupervisorList = new data_qa_cims();
                    dataSupervisorList.qa_cims_u1_calibration_document_list = new qa_cims_u1_calibration_document_details[1];
                    qa_cims_u1_calibration_document_details _selectListSupervisor = new qa_cims_u1_calibration_document_details();
                    dataSupervisorList.qa_cims_u1_calibration_document_list[0] = _selectListSupervisor;
                    dataSupervisorList = callServicePostQaDocCIMS(_urlCimsGetDocCalibrationSupervisor, dataSupervisorList);
                    ViewState["vsCalibrationListSupervisor"] = dataSupervisorList.qa_cims_u1_calibration_document_list;
                    setGridData(GvSupervisor, ViewState["vsCalibrationListSupervisor"]);
                    setActive.Text = activeTab.ToString();

                }
                break;
        }
    }

    #endregion

    #region event gridview

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView gvName = (GridView)sender;
        gvName.PageIndex = e.NewPageIndex;

        switch (gvName.ID)
        {

            case "gvCalibrationDocument":
                setGridData(gvCalibrationDocument, ViewState["vsGetDocumentCalibration"]);
                break;
        }

    }

    protected void gvRowDeleted(object sender, GridViewDeleteEventArgs e)
    {
        var gvName = (GridView)sender;

        switch (gvName.ID)
        {


            case "gvListEquipmentCalOld":

                GridView gvListEquipmentCalOld = (GridView)docCreate.FindControl("gvListEquipmentCalOld");
                var DeleteListEquipmentCalOld = (DataSet)ViewState["vsDataEquipmentCalOld"];
                var drDelete = DeleteListEquipmentCalOld.Tables[0].Rows;

                drDelete.RemoveAt(e.RowIndex);

                ViewState["vsDataEquipmentCalOld"] = DeleteListEquipmentCalOld;
                gvListEquipmentCalOld.EditIndex = -1;

                setGridData(gvListEquipmentCalOld, ViewState["vsDataEquipmentCalOld"]);

                if (DeleteListEquipmentCalOld.Tables[0].Rows.Count == 0)
                {
                    divActionSaveCreateDocument.Visible = false;
                }
                else
                {
                    divActionSaveCreateDocument.Visible = true;
                }

                break;

            case "gvCalibrationPoint":

                GridView gvCalibrationPoint = (GridView)docCreate.FindControl("gvCalibrationPoint");
                var DeleteListCalibrationPoint = (DataSet)ViewState["vsDataCalibrationPoint"];
                var drDeleteCalibrationPoint = DeleteListCalibrationPoint.Tables[0].Rows;

                drDeleteCalibrationPoint.RemoveAt(e.RowIndex);

                ViewState["vsDataCalibrationPoint"] = DeleteListCalibrationPoint;
                gvCalibrationPoint.EditIndex = -1;

                setGridData(gvCalibrationPoint, ViewState["vsDataCalibrationPoint"]);

                if (DeleteListCalibrationPoint.Tables[0].Rows.Count == 0)
                {
                    btnInsertDetail.Visible = false;
                }
                else
                {
                    btnInsertDetail.Visible = true;
                }

                break;

            case "gvAcceptance":

                GridView gvAcceptance = (GridView)docCreate.FindControl("gvAcceptance");

                var DeleteListAcceptance = (DataSet)ViewState["vsDataAcceptance"];
                var drDeleteAcceptance = DeleteListAcceptance.Tables[0].Rows;

                drDeleteAcceptance.RemoveAt(e.RowIndex);

                ViewState["vsDataAcceptance"] = DeleteListAcceptance;
                gvAcceptance.EditIndex = -1;

                setGridData(gvAcceptance, ViewState["vsDataAcceptance"]);

                //if (DeleteListAcceptance.Tables[0].Rows.Count == 0)
                //{
                //    btnInsertDetail.Visible = false;
                //}
                //else
                //{
                //    btnInsertDetail.Visible = true;
                //}

                break;
            case "gvMeasuring":

                GridView gvMeasuring = (GridView)docCreate.FindControl("gvMeasuring");

                var DeleteListvsMeasuring = (DataSet)ViewState["vsDataMeasuring"];
                var drDeleteMeasuring = DeleteListvsMeasuring.Tables[0].Rows;

                drDeleteMeasuring.RemoveAt(e.RowIndex);

                ViewState["vsDataMeasuring"] = DeleteListvsMeasuring;
                gvMeasuring.EditIndex = -1;

                setGridData(gvMeasuring, ViewState["vsDataMeasuring"]);

                //if (DeleteListAcceptance.Tables[0].Rows.Count == 0)
                //{
                //    btnInsertDetail.Visible = false;
                //}
                //else
                //{
                //    btnInsertDetail.Visible = true;
                //}

                break;

            case "gvRange":

                GridView gvRange = (GridView)docCreate.FindControl("gvRange");

                var DeleteListvsRange = (DataSet)ViewState["vsDataRange"];
                var drDeleteRange = DeleteListvsRange.Tables[0].Rows;

                drDeleteRange.RemoveAt(e.RowIndex);

                ViewState["vsDataRange"] = DeleteListvsRange;
                gvRange.EditIndex = -1;

                setGridData(gvRange, ViewState["vsDataRange"]);

                //if (DeleteListAcceptance.Tables[0].Rows.Count == 0)
                //{
                //    btnInsertDetail.Visible = false;
                //}
                //else
                //{
                //    btnInsertDetail.Visible = true;
                //}
                break;

            case "gvResolution":

                GridView gvResolution = (GridView)docCreate.FindControl("gvResolution");

                var DeleteListvsResolution = (DataSet)ViewState["vsDataResolution"];
                var drDeleteResolution = DeleteListvsResolution.Tables[0].Rows;

                drDeleteResolution.RemoveAt(e.RowIndex);

                ViewState["vsDataRange"] = DeleteListvsResolution;
                gvResolution.EditIndex = -1;

                setGridData(gvResolution, ViewState["vsDataResolution"]);

                //if (DeleteListAcceptance.Tables[0].Rows.Count == 0)
                //{
                //    btnInsertDetail.Visible = false;
                //}
                //else
                //{
                //    btnInsertDetail.Visible = true;
                //}
                break;
        }

    }

    protected void onRowCreated(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {

            case "GvSalt":
                if (e.Row.RowType == DataControlRowType.DataRow || e.Row.RowState == DataControlRowState.Insert)
                {

                }
            break;
        }
    }

    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "gvRegistrationList":

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    var _StatusDevice = (Label)e.Row.Cells[6].FindControl("lblDeviceStatus");
                    var _StatusDevice_Online = (Label)e.Row.Cells[6].FindControl("lblStatusOnLine");
                    var _StatusDevice_Offline = (Label)e.Row.Cells[6].FindControl("lblStatusOffLine");

                    if (_StatusDevice.Text == "1")
                    {
                        _StatusDevice_Online.Visible = true;
                    }
                    else
                    {
                        _StatusDevice_Offline.Visible = true;
                    }
                }

                break;

            case "gvCalibrationList":

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    var equipment_type = (TextBox)fvDetailsDocument.FindControl("tbtypeEquipment");
                    var placeidx = (TextBox)fvDetailsDocument.FindControl("tbplaceidx");
                    var gvCalibrationList = (GridView)fvCalDocumentList.FindControl("gvCalibrationList");
                    var pnlAction = (Panel)fvCalDocumentList.FindControl("pnlAction");

                    var status = (Label)e.Row.Cells[9].FindControl("lblstaidx");
                    var certificate = (Label)e.Row.Cells[8].FindControl("lblcertificate");
                    var record_certificate = (LinkButton)e.Row.Cells[10].FindControl("btnSaveCertificate");
                    var view_details = (LinkButton)e.Row.Cells[10].FindControl("btnViewDocCal");
                    var lblM0LabName = (Label)e.Row.Cells[7].FindControl("lblM0LabName");
                    var pnlChooseLocationCal = (Panel)e.Row.Cells[7].FindControl("pnlChooseLocationCal");


                    pnlAction.Visible = false;
                    pnlChooseLocationCal.Visible = false;

                    switch (equipment_type.Text)
                    {
                        case "1": // ประเภทเครื่องใหม่

                            gvCalibrationList.Columns[4].Visible = false;

                            switch (certificate.Text)
                            {
                                case "4":

                                    switch (status.Text)
                                    {
                                        case "1":
                                        case "34":
                                           // litDebug.Text = "ประเภทเครื่องใหม่ มีใบ cer";
                                            record_certificate.Visible = true;
                                            break;
                                    }


                                    break;

                                case "3":

                                    switch (status.Text)
                                    {
                                        case "1":
                                        case "2":
                                        case "3":
                                          //  litDebug.Text = "ประเภทเครื่องใหม่ ไม่มีใบ cer";
                                            view_details.Visible = true;
                                            lblM0LabName.Visible = false;
                                            pnlChooseLocationCal.Visible = true;
                                            getLocationList((DropDownList)e.Row.Cells[7].FindControl("ddlChooseLocation"), "0", placeidx.Text);
                                            getChooseLabTypeCal((DropDownList)e.Row.Cells[7].FindControl("ddlChooseLabType"), "0", placeidx.Text);
                                            pnlAction.Visible = true;
                                            break;
                                    }


                                    break;
                            }

                            break;

                        case "2": // ประเภทเครื่องเก่า
                           // litDebug.Text = "ประเภทเครื่องเก่า";
                            gvCalibrationList.Columns[8].Visible = false;
                            switch (status.Text)
                            {
                                case "3":
                                    view_details.Visible = true;
                                    lblM0LabName.Visible = false;
                                    pnlChooseLocationCal.Visible = true;
                                    getLocationList((DropDownList)e.Row.Cells[7].FindControl("ddlChooseLocation"), "0", placeidx.Text);
                                    getChooseLabTypeCal((DropDownList)e.Row.Cells[7].FindControl("ddlChooseLabType"), "0", placeidx.Text);
                                    pnlAction.Visible = true;
                                    break;
                            }
                            break;
                    }

                    if (certificate.Text == "3")
                    {
                        certificate.Text = "<i class='fa fa-times' style='color: Red; font - size: 8px; ' aria-hidden='true'></i>";
                    }
                    else
                    {
                        certificate.Text = "<i class='fa fa-check' style='color: Green; font - size: 8px; ' aria-hidden='true'></i>";
                    }

                }
                break;

            case "gvCalibrationInLab":

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    var staidxInLab = (Label)e.Row.Cells[6].FindControl("lblstaidxInLab");
                    var pnAcceptInLab = (Panel)e.Row.Cells[6].FindControl("divActionCalibrationInLab");

                    pnAcceptInLab.Visible = false;

                    switch (staidxInLab.Text)
                    {
                        case "5":
                            pnAcceptInLab.Visible = true;
                            break;
                    }
                }


                break;

            case "GvSalt":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                   

                }

                if (e.Row.RowType == DataControlRowType.Footer)
                {
                    LinkButton btnInsertRow = (LinkButton)e.Row.FindControl("btnInsertRow");
 
                    //lbl.Text = GvSalt.ToString("c");
                }
              
                break;

            case "GvSaltView":

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    var _txtvalueidx = (TextBox)e.Row.Cells[6].FindControl("tb_result_salt_val7_view");
                    var _rdiobutton = (RadioButtonList)e.Row.Cells[6].FindControl("rd_result_salt_val7_view");

                    if (_txtvalueidx.Text != "")
                    {
                        _rdiobutton.SelectedValue = _txtvalueidx.Text;
                    }
                    else
                    {
                      
                    }
                }

                break;


        }

    }

    protected void gvOnrowCommand(object sender, GridViewCommandEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvSalt":

                if (e.CommandName.Equals("InsertRowSalt"))
                {

                    TextBox insert_result_salt_val1 = (TextBox)GvSalt.FooterRow.FindControl("tb_result_salt_val1");
                    TextBox insert_result_salt_val2 = (TextBox)GvSalt.FooterRow.FindControl("tb_result_salt_val2");
                    TextBox insert_result_salt_val3 = (TextBox)GvSalt.FooterRow.FindControl("tb_result_salt_val3");
                    TextBox insert_result_salt_val4 = (TextBox)GvSalt.FooterRow.FindControl("tb_result_salt_val4");
                    TextBox insert_result_salt_val5 = (TextBox)GvSalt.FooterRow.FindControl("tb_result_salt_val5");
                    TextBox insert_result_salt_val6 = (TextBox)GvSalt.FooterRow.FindControl("tb_result_salt_val6");
                    RadioButtonList insert_result_salt_val7 = (RadioButtonList)GvSalt.FooterRow.FindControl("rd_result_salt_val7");
                    TextBox insert_result_salt_val8 = (TextBox)GvSalt.FooterRow.FindControl("tb_result_salt_val8");

                   // setGridData(GvSalt, ViewState["vsGridviewName"]);
            
                }



               break;
        }
       
    }



    #endregion

    #region event reapter
    protected void rptOnRowDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        var rptName = (Repeater)Sender;

        switch (rptName.ID)
        {
            case "rptPrintReciveDate":

                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {

                    var GvFormRecordResult = (GridView)e.Item.FindControl("GvFormRecordResult");
                    var lb1CalDoc = (Label)e.Item.FindControl("lb1CalDoc");
                    var Device_ID_no = (Label)e.Item.FindControl("Device_ID_no");
                    var lbEquipIDX = (Label)e.Item.FindControl("lbEquipIDX");


                    TextBox txtSearchEquipmentIDNo = (TextBox)fvRecordCalResults.FindControl("txtSearchEquipmentIDNo");
                    DropDownList ddlSearchPlaceSaveResult = (DropDownList)fvRecordCalResults.FindControl("ddlSearchPlaceSaveResult");


                    if (GvFormRecordResult.DataSource == null)
                    {

                        data_qa_cims _data_qa_result1 = new data_qa_cims();
                        _data_qa_result1.qa_cims_u1_calibration_document_list = new qa_cims_u1_calibration_document_details[1];
                        qa_cims_u1_calibration_document_details _deviceIDNO1 = new qa_cims_u1_calibration_document_details();
                        _deviceIDNO1.equipment_idx = int.Parse(lbEquipIDX.Text);
                        _deviceIDNO1.place_idx = int.Parse(ddlSearchPlaceSaveResult.SelectedValue);
                        //_deviceIDNO1.u1_cal_idx = 27;
                        //.r1_form_root_idx = 0;

                        _data_qa_result1.qa_cims_u1_calibration_document_list[0] = _deviceIDNO1;
                        _data_qa_result1 = callServicePostQaDocCIMS(_urlCimsGetFormRecordListHeader, _data_qa_result1);
                        if (_data_qa_result1.qa_cims_u1_calibration_document_list != null)
                        {


                            //lbPrintSampleCodeU1Doc

                            var _linqCheckMat = (from data_qa_cims in _data_qa_result1.qa_cims_u1_calibration_document_list
                                                 where data_qa_cims.equipment_idx == int.Parse(lbEquipIDX.Text)
                                                 select data_qa_cims);


                            //string getPathShowimages = ConfigurationManager.AppSettings["path_qr_code_qa"];

                            //string fileimageShowPath = Server.MapPath(getPathShowimages + lblsampleCode.Text + ".jpg");


                            GvFormRecordResult.DataSource = _data_qa_result1.qa_cims_u1_calibration_document_list;
                            GvFormRecordResult.DataBind();

                            //Label1.Text = "has";
                        }
                        else
                        {
                            //Label1.Text = "not";
                        }


                    }



                    data_qa_cims _data_qa_result_t = new data_qa_cims();
                    _data_qa_result_t.qa_cims_u1_calibration_document_list = new qa_cims_u1_calibration_document_details[1];
                    qa_cims_u1_calibration_document_details _deviceIDNO = new qa_cims_u1_calibration_document_details();
                    _deviceIDNO.device_id_no = txtSearchEquipmentIDNo.Text;
                    _deviceIDNO.place_idx = int.Parse(ddlSearchPlaceSaveResult.SelectedValue);
                    _deviceIDNO.u1_cal_idx = int.Parse(lb1CalDoc.Text);
                    //.r1_form_root_idx = 0;

                    _data_qa_result_t.qa_cims_u1_calibration_document_list[0] = _deviceIDNO;

                    _data_qa_result_t = callServicePostQaDocCIMS(_urlCimsGetFormRecordList, _data_qa_result_t);

                    for (int a = 0; a < _data_qa_result_t.qa_cims_u1_calibration_document_list.Count(); a++)
                    {

                        BoundField field = new BoundField();
                        field.HeaderText = _data_qa_result_t.qa_cims_u1_calibration_document_list[a].form_detail_name.ToString();
                        field.HeaderStyle.Width = 10;
                        DataControlField col;
                        col = field;
                        GvFormRecordResult.Columns.Add(col);
                        GvFormRecordResult.DataBind();
                        //  Label1.Text = "111";

                    }


                }

                break;

            case "rptformTopicsSalt":

                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    var textshow_value1 = (Label)e.Item.FindControl("u1_val1");
                    var textshow_value2 = (Label)e.Item.FindControl("u1_val2");

                }
                break;
        }

    }
    #endregion

    #region event formview

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    #endregion

    #region event checkbox

    protected void checkindexchange(object sender, EventArgs e)
    {
        var chkName = (CheckBox)sender;

        switch (chkName.ID)
        {

        }
    }
    #endregion

    #region event radiobutton

    protected void radioCheckChange(object sender, EventArgs e)
    {
        var rdName = (RadioButtonList)sender;

        switch (rdName.ID)
        {
            case "rblTypeEquipment":
                divAlertSearchEquipmentOld.Visible = false;
                RadioButtonList rblTypeEquipment = (RadioButtonList)docCreate.FindControl("rblTypeEquipment");

                if (rblTypeEquipment.SelectedValue == "2")
                {
                    linkBtnTrigger(btnSearchToolID);
                    pnEquipmentOld.Visible = true;
                    tbsearchingToolID.Focus();
                    pnEquipmentNew.Visible = false;
                }
                else
                {
                    pnEquipmentOld.Visible = false;
                    gvListEquipmentCalOld.Visible = false;
                    divActionSaveCreateDocument.Visible = false;
                    setFormData(fvShowDetailsSearchTool, FormViewMode.ReadOnly, null);
                    pnEquipmentNew.Visible = true;
                    ClearDataCalEquipmentOld();
                    ClearDataCalEquipmentNew();
                    ClearDataCalibrationPoint();
                    ClearDataTextbox();

                    getM0EquipmentName((DropDownList)pnEquipmentNew.FindControl("ddl_machine_name"), "0");
                    getM0Brand((DropDownList)pnEquipmentNew.FindControl("ddl_machine_brand"), "0");
                    getM0EquipmentType((DropDownList)pnEquipmentNew.FindControl("ddlmachineType"), "0");
                    ////getM0Unit((DropDownList)pnEquipmentNew.FindControl("ddl_range_of_use_unit"), "0");
                    ////getM0Unit((DropDownList)pnEquipmentNew.FindControl("ddl_calibration_point_unit"), "0");
                    ////getM0Unit((DropDownList)pnEquipmentNew.FindControl("ddl_acceptance_criteria_unit"), "0");
                    ////getM0Unit((DropDownList)pnEquipmentNew.FindControl("ddl_measuring_range_unit"), "0");
                    ////getM0Unit((DropDownList)pnEquipmentNew.FindControl("ddl_resolution_unit"), "0");
                    //getM0CalType((DropDownList)pnEquipmentNew.FindControl("ddlCalibrationType"), "0");


                    getM0CalType((DropDownList)pnEquipmentNew.FindControl("dllcaltype"), "0");
                    getOrganizationList((DropDownList)pnEquipmentNew.FindControl("ddlOrg_holder"));
                    getDepartmentList((DropDownList)pnEquipmentNew.FindControl("ddlDept_holder"), -1);
                    getSectionList((DropDownList)pnEquipmentNew.FindControl("ddlSec_holder"), -1, -1);
                    getFrequency((DropDownList)pnEquipmentNew.FindControl("ddlCalFrequency"), "0");
                    getResolution((DropDownList)pnEquipmentNew.FindControl("ddlResolution"), "0");

                    getM0Unit((DropDownList)pnEquipmentNew.FindControl("ddlUnitRange"), "0");
                    getM0Unit((DropDownList)pnEquipmentNew.FindControl("ddlUnitMeasuring"), "0");
                    getM0Unit((DropDownList)pnEquipmentNew.FindControl("ddlUnitAcceptance"), "0");
                    getM0Unit((DropDownList)pnEquipmentNew.FindControl("ddl_calibration_point_unit"), "0");

                    //getSelectPlace((DropDownList)pnEquipmentNew.FindControl("ddlPlaceMachine"), "0");

                }

                break;
        }
    }
    #endregion

    #region event dropdownlist

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }



    protected void getM0EquipmentName(DropDownList ddlName, string _equipment_idx)
    {
        data_qa_cims _dataqacims = new data_qa_cims();
        _dataqacims.qa_cims_m0_equipment_name_list = new qa_cims_m0_equipment_name_detail[1];
        qa_cims_m0_equipment_name_detail _m0Set = new qa_cims_m0_equipment_name_detail();
        _dataqacims.qa_cims_m0_equipment_name_list[0] = _m0Set;

        _dataqacims = callServicePostMasterQACIMS(_urlCimsGetEquipment_name, _dataqacims);

        setDdlData(ddlName, _dataqacims.qa_cims_m0_equipment_name_list, "equipment_name", "equipment_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือกชื่ออุปกรณ์/เครื่องมือ --", "0"));
        ddlName.SelectedValue = _equipment_idx;
    }

    protected void getM0Brand(DropDownList ddlName, string _brand_idx)
    {
        data_qa_cims _dataqacims = new data_qa_cims();
        _dataqacims.qa_cims_m0_brand_list = new qa_cims_m0_brand_detail[1];
        qa_cims_m0_brand_detail _m0Set = new qa_cims_m0_brand_detail();
        _dataqacims.qa_cims_m0_brand_list[0] = _m0Set;

        _dataqacims = callServicePostMasterQACIMS(_urlCimsGetBrand, _dataqacims);

        setDdlData(ddlName, _dataqacims.qa_cims_m0_brand_list, "brand_name", "brand_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือกยี่ห้อ/รุ่น --", "0"));
        ddlName.SelectedValue = _brand_idx;
    }

    protected void getM0EquipmentType(DropDownList ddlName, string _equipment_type_idx)
    {
        data_qa_cims _dataqacims = new data_qa_cims();
        _dataqacims.qa_cims_m0_equipment_type_list = new qa_cims_m0_equipment_type_detail[1];
        qa_cims_m0_equipment_type_detail _m0Set = new qa_cims_m0_equipment_type_detail();
        _dataqacims.qa_cims_m0_equipment_type_list[0] = _m0Set;

        _dataqacims = callServicePostMasterQACIMS(_urlCimsGetEquipment_type, _dataqacims);

        setDdlData(ddlName, _dataqacims.qa_cims_m0_equipment_type_list, "equipment_type_name", "equipment_type_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือกประเภทเครื่องมือ --", "0"));
        ddlName.SelectedValue = _equipment_type_idx;
    }

    protected void getM0Unit(DropDownList ddlName, string _unit_idx)
    {
        //data_qa_cims _dataqacims = new data_qa_cims();
        //_dataqacims.qa_cims_m0_unit_list = new qa_cims_m0_unit_detail[1];
        //qa_cims_m0_unit_detail _m0Set = new qa_cims_m0_unit_detail();
        //_dataqacims.qa_cims_m0_unit_list[0] = _m0Set;

        //_dataqacims = callServicePostMasterQACIMS(_urlCimsGetUnit, _dataqacims);

        //setDdlData(ddlName, _dataqacims.qa_cims_m0_unit_list, "unit_symbol_en", "unit_idx");
        //ddlName.Items.Insert(0, new ListItem("-- เลือกหน่วยวัด --", "0"));
        //ddlName.SelectedValue = _unit_idx;


        data_qa_cims _dataqacims = new data_qa_cims();
        _dataqacims.qa_cims_m0_unit_list = new qa_cims_m0_unit_detail[1];
        qa_cims_m0_unit_detail _m0Set = new qa_cims_m0_unit_detail();
        _dataqacims.qa_cims_m0_unit_list[0] = _m0Set;

        _dataqacims = callServicePostMasterQACIMS(_urlCimsGetUnit, _dataqacims);

        var _linqddl_unit = (from dt in _dataqacims.qa_cims_m0_unit_list

                             select new
                             {
                                 dt.unit_symbol_en,
                                 dt.unit_idx
                             }).ToList();

        setDdlData(ddlName, _linqddl_unit, "unit_symbol_en", "unit_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือกหน่วยวัด --", "0"));
        ddlName.Items.Insert(1, new ListItem("µl", "29"));
        ddlName.Items.Insert(2, new ListItem("µm", "30"));
        ddlName.Items.Insert(3, new ListItem("°C", "31"));

        ddlName.SelectedValue = _unit_idx;
    }

    protected void getM0CalType(DropDownList ddlName, string _cal_type_idx)
    {
        data_qa_cims _dataqacims = new data_qa_cims();
        _dataqacims.qa_cims_m0_cal_type_list = new qa_cims_m0_cal_type_detail[1];
        qa_cims_m0_cal_type_detail _m0Set = new qa_cims_m0_cal_type_detail();
        _dataqacims.qa_cims_m0_cal_type_list[0] = _m0Set;

        _dataqacims = callServicePostMasterQACIMS(_urlCimsGetCal_type, _dataqacims);

        setDdlData(ddlName, _dataqacims.qa_cims_m0_cal_type_list, "cal_type_name", "cal_type_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือกประเภทการสอบเทียบ --", "0"));
        ddlName.SelectedValue = _cal_type_idx;
    }

    protected void getFrequency(DropDownList ddlName, string _frequency)
    {
        data_qa_cims data_m0_frequency = new data_qa_cims();
        qa_cims_m0_frequency_detail m0_frequency = new qa_cims_m0_frequency_detail();
        data_m0_frequency.qa_cims_m0_frequency_list = new qa_cims_m0_frequency_detail[1];

        m0_frequency.condition = 1;
        data_m0_frequency.qa_cims_m0_frequency_list[0] = m0_frequency;

        data_m0_frequency = callServicePostMasterQACIMS(_urlCimsGetM0_Frequency, data_m0_frequency);

        setDdlData(ddlName, data_m0_frequency.qa_cims_m0_frequency_list, "detail_frequency", "equipment_frequency_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือกความถี่ในการสอบเทียบ --", "0"));
        ddlName.SelectedValue = _frequency;
    }

    protected void getResolution(DropDownList ddlName, string _resolution)
    {
        data_qa_cims data_m0_resulution = new data_qa_cims();
        qa_cims_m0_resulution_detail m0_resulution = new qa_cims_m0_resulution_detail();
        data_m0_resulution.qa_cims_m0_resolution_list = new qa_cims_m0_resulution_detail[1];

        m0_resulution.condition = 1;
        data_m0_resulution.qa_cims_m0_resolution_list[0] = m0_resulution;

        data_m0_resulution = callServicePostMasterQACIMS(_urlCimsGetM0_Resolution, data_m0_resulution);

        if (data_m0_resulution.qa_cims_m0_resolution_list != null)
        {
            var _linqddl_resolution = (from dt in data_m0_resulution.qa_cims_m0_resolution_list

                                       where dt.unit_idx != 29

                                       select new
                                       {
                                           dt.unit_idx,
                                           dt.unit_symbol_en,
                                           dt.resolution_name,
                                           dt.equipment_resolution_idx
                                       }).ToList();


            var _linqddl_resolution_symbol = (from dt_synbol in data_m0_resulution.qa_cims_m0_resolution_list

                                              where dt_synbol.unit_idx == 29

                                              select new
                                              {
                                                  dt_synbol.unit_idx,
                                                  dt_synbol.unit_symbol_en,
                                                  dt_synbol.resolution_name,
                                                  dt_synbol.equipment_resolution_idx
                                              }).ToList();

            string ddl_resolution = "";
            string var_ddl_resolution = "";

            for (int idset = 0; idset < _linqddl_resolution_symbol.Count(); idset++)
            {

                ddl_resolution = _linqddl_resolution_symbol[idset].unit_idx.ToString();

                if (ddl_resolution == "29")
                {
                    var_ddl_resolution = "µl";
                }
                else if (ddl_resolution == "30")
                {
                    var_ddl_resolution = "µm";
                }
                else if (ddl_resolution == "31")
                {
                    var_ddl_resolution = "°C";
                }
                else
                {
                    var_ddl_resolution = ddl_resolution;
                }

            }


            ddlName.AppendDataBoundItems = true;
            ddlName.Items.Add(new ListItem("-- เลือกค่าอ่านละเอียด --", "0"));
            if (_linqddl_resolution != null)
            {
                var productQuery1 = _linqddl_resolution.Select(p => new { ProductId = p.equipment_resolution_idx, DisplayText = p.resolution_name.ToString() + " " + (p.unit_symbol_en.ToString()) });
                ddlName.DataSource = productQuery1;
                ddlName.DataValueField = "ProductId";
                ddlName.DataTextField = "DisplayText";
                ddlName.DataBind();
                ddlName.SelectedValue = _resolution;
            }

            if (_linqddl_resolution_symbol != null)
            {
                var productQuery2 = _linqddl_resolution_symbol.Select(p => new { ProductId = p.equipment_resolution_idx, DisplayText = p.resolution_name.ToString() + " " + (var_ddl_resolution) });
                ddlName.DataSource = productQuery2;
                ddlName.DataValueField = "ProductId";
                ddlName.DataTextField = "DisplayText";
                ddlName.DataBind();
                ddlName.SelectedValue = _resolution;
            }

        }
        else
        {
            ddlName.Items.Insert(0, new ListItem("-- เลือกค่าอ่านละเอียด --", "0"));
        }
    }


    protected void getOrganizationList(DropDownList ddlName)
    {
        _dataEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dataEmployee.organization_list[0] = _orgList;

        _dataEmployee = callServicePostEmployee(_urlGetOrganizationList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือกองค์กร --", "-1"));
    }

    protected void getDepartmentList(DropDownList ddlName, int _org_idx)
    {
        data_employee _dataEmployee_rdept = new data_employee();
        _dataEmployee_rdept.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dataEmployee_rdept.department_list[0] = _deptList;

        _dataEmployee_rdept = callServicePostEmployee(_urlGetDepartmentList, _dataEmployee_rdept);
        setDdlData(ddlName, _dataEmployee_rdept.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือกฝ่าย --", "-1"));
    }

    protected void getSectionList(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {
        ddlName.Items.Clear();
        _dataEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _dataEmployee.section_list[0] = _secList;

        _dataEmployee = callServicePostEmployee(_urlGetSectionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.section_list, "sec_name_th", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือกแผนก --", "-1"));
    }

    protected void getChooseLabTypeCal(DropDownList ddlName, string _idxLabtype, string _defaultLocation)
    {

        data_qa_cims dataCimsDecisionLab = new data_qa_cims();
        dataCimsDecisionLab.qa_cims_bindnode_decision_list = new qa_cims_bindnode_decision_detail[1];
        qa_cims_bindnode_decision_detail _selectDecisionLab = new qa_cims_bindnode_decision_detail();
        _selectDecisionLab.condition = 1;
        dataCimsDecisionLab.qa_cims_bindnode_decision_list[0] = _selectDecisionLab;

        dataCimsDecisionLab = callServicePostQaDocCIMS(_urlCimsGetSelectDecisionNode1, dataCimsDecisionLab);

        qa_cims_m0_lab_detail[] _templist_default = (qa_cims_m0_lab_detail[])ViewState["vsCheckLabTypeDefault"];

        var _linqlocationDefault = (from dt in _templist_default
                                    where dt.place_idx == int.Parse(_defaultLocation)
                                    select new
                                    {
                                        dt.m0_lab_idx,
                                        dt.lab_name,
                                        dt.cal_type_idx
                                    }).ToList();

        string defaultLocation = "";

        for (int ex = 0; ex < _linqlocationDefault.Count(); ex++)
        {
            defaultLocation = _linqlocationDefault[ex].cal_type_idx.ToString();
        }

        if (defaultLocation == ExternalID.ToString())
        {
            setDdlData(ddlName, dataCimsDecisionLab.qa_cims_bindnode_decision_list, "decision_name", "decision_idx");
            ddlName.Items.Insert(0, new ListItem("- เลือกประเภทสอบเทียบ -", "0"));
            ddlName.SelectedValue = ExternalCondition.ToString();

        }
        else
        {
            var _linqlocationDefault_interanl = (from dt in dataCimsDecisionLab.qa_cims_bindnode_decision_list
                                                 where dt.decision_idx != (ExternalCondition)
                                                 select new
                                                 {
                                                     dt.decision_idx,
                                                     dt.decision_name
                                                 }).ToList();

            string defaultLocation_internal = "";

            for (int _internal = 0; _internal < _linqlocationDefault.Count(); _internal++)
            {
                defaultLocation_internal = _linqlocationDefault_interanl[_internal].decision_idx.ToString();
            }

            setDdlData(ddlName, dataCimsDecisionLab.qa_cims_bindnode_decision_list, "decision_name", "decision_idx");
            ddlName.Items.Insert(0, new ListItem("- เลือกประเภทสอบเทียบ -", "0"));
            ddlName.SelectedValue = defaultLocation_internal;

        }
    }

    protected void getLocationList(DropDownList ddlName, string _idxplace, string _defaultLocation)
    {
        data_qa_cims dataCimsDecisionLabPlace = new data_qa_cims();
        dataCimsDecisionLabPlace.qa_cims_m0_lab_list = new qa_cims_m0_lab_detail[1];
        qa_cims_m0_lab_detail _selectDecisionPlace = new qa_cims_m0_lab_detail();
        _selectDecisionPlace.condition = 1;
        dataCimsDecisionLabPlace.qa_cims_m0_lab_list[0] = _selectDecisionPlace;

        dataCimsDecisionLabPlace = callServicePostMasterQACIMS(_urlCimsGetLab, dataCimsDecisionLabPlace);

        ViewState["vsCheckLabTypeDefault"] = dataCimsDecisionLabPlace.qa_cims_m0_lab_list;
        var _linqlocationDefault = (from dt in dataCimsDecisionLabPlace.qa_cims_m0_lab_list
                                    where dt.place_idx == int.Parse(_defaultLocation)
                                    select new
                                    {
                                        dt.m0_lab_idx,
                                        dt.lab_name,
                                        dt.cal_type_idx
                                    }).ToList();

        string defaultLocation = "";
        string defaultLabtype = "";

        for (int ex = 0; ex < _linqlocationDefault.Count(); ex++)
        {
            defaultLocation = _linqlocationDefault[ex].m0_lab_idx.ToString();
            defaultLabtype = _linqlocationDefault[ex].cal_type_idx.ToString();
            ViewState["vsDefaultLocation"] = defaultLabtype;

            //litDebug.Text = _linqlocationDefault[ex].m0_lab_idx.ToString();
        }

        if (_defaultLocation != "0")
        {
            setDdlData(ddlName, dataCimsDecisionLabPlace.qa_cims_m0_lab_list, "lab_name", "m0_lab_idx");
            ddlName.Items.Insert(0, new ListItem("- เลือกสถานที่สอบเทียบ -", "0"));
            ddlName.SelectedValue = defaultLocation;

            if (defaultLabtype == ExternalID.ToString())
            {
                ddlName.Enabled = false;
            }
            else
            {

                if (_idxplace == "0")
                {

                    var linqInternal = (from dt in dataCimsDecisionLabPlace.qa_cims_m0_lab_list
                                        where dt.cal_type_idx != ExternalID
                                        select new
                                        {
                                            dt.m0_lab_idx,
                                            dt.lab_name
                                        }).ToList();


                    string extanalidx1 = "";
                    string placeDefault = "";
                    for (int ex = 0; ex < linqInternal.Count(); ex++)
                    {
                        placeDefault = linqInternal[ex].m0_lab_idx.ToString();
                        extanalidx1 = linqInternal[ex].m0_lab_idx.ToString();
                        setDdlData(ddlName, dataCimsDecisionLabPlace.qa_cims_m0_lab_list, "lab_name", "m0_lab_idx");
                        ddlName.Items.Insert(0, new ListItem("- เลือกสถานที่สอบเทียบ -", "0"));
                        ddlName.SelectedValue = defaultLocation;// "3";//placeDefault;

                    }
                }
                else
                {

                }
            }
        }
        else
        {
            if (_idxplace == ExternalCondition.ToString())
            {
                var linqExtanal = (from dt in dataCimsDecisionLabPlace.qa_cims_m0_lab_list
                                   where dt.cal_type_idx == ExternalID
                                   select new
                                   {
                                       dt.m0_lab_idx,
                                       dt.lab_name
                                   }).ToList();

                string extanalidx = "";

                for (int ex = 0; ex < linqExtanal.Count(); ex++)
                {
                    extanalidx = linqExtanal[ex].m0_lab_idx.ToString();

                    setDdlData(ddlName, dataCimsDecisionLabPlace.qa_cims_m0_lab_list, "lab_name", "m0_lab_idx");
                    ddlName.Items.Insert(0, new ListItem("- เลือกสถานที่สอบเทียบ -", "0"));
                    ddlName.SelectedValue = extanalidx;
                    ddlName.Enabled = false;
                }
            }
            else
            {
                setDdlData(ddlName, dataCimsDecisionLabPlace.qa_cims_m0_lab_list, "lab_name", "m0_lab_idx");
                ddlName.Items.Insert(0, new ListItem("- เลือกสถานที่สอบเทียบ -", "0"));
                ddlName.Enabled = true;

            }
        }
    }

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlName = (DropDownList)sender;

        DropDownList ddlOrg_excel = (DropDownList)pnEquipmentNew.FindControl("ddlOrg_holder");
        DropDownList ddlDept_excel = (DropDownList)pnEquipmentNew.FindControl("ddlDept_holder");
        DropDownList ddlSec_excel = (DropDownList)pnEquipmentNew.FindControl("ddlSec_holder");

        switch (ddlName.ID)
        {
            case "ddlChooseLabType":

                var ddlID = (DropDownList)sender;
                var rowGrid = (GridViewRow)ddlID.NamingContainer;

                DropDownList _selectLabType = (DropDownList)rowGrid.FindControl("ddlChooseLabType");
                DropDownList _selectLocation = (DropDownList)rowGrid.FindControl("ddlChooseLocation");
                TextBox _commentBeforeExtanalLab = (TextBox)rowGrid.FindControl("txtCommentBeforeExtanalLab");
                Panel pnlAction = (Panel)fvCalDocumentList.FindControl("pnlAction");

                TextBox tbplaceidx = (TextBox)fvDetailsDocument.FindControl("tbplaceidx");

                ViewState["vs_labtype"] = _selectLabType.SelectedValue.ToString();

                //litDebug.Text = ViewState["vs_labtype"].ToString();

                if (int.Parse(_selectLabType.SelectedValue) == ExternalCondition)
                {
                    _commentBeforeExtanalLab.Visible = true;
                    _selectLocation.Visible = true;
                    getLocationList((DropDownList)rowGrid.FindControl("ddlChooseLocation"), ViewState["vs_labtype"].ToString(), "0");
                }
                else
                {
                    _selectLocation.Visible = true;
                    _commentBeforeExtanalLab.Visible = false;
                    getLocationList((DropDownList)rowGrid.FindControl("ddlChooseLocation"), ViewState["vs_labtype"].ToString(), tbplaceidx.Text);

                }
                pnlAction.Visible = true;

                break;

            case "ddlOrg_holder":
                // ddlDocType.Focus();
                getDepartmentList(ddlDept_excel, int.Parse(ddlOrg_holder.SelectedItem.Value));
                ddlSec_holder.Items.Clear();
                ddlSec_holder.Items.Insert(0, new ListItem("-- เลือกแผนก --", "-1"));
                // ddlSec_create.Items.Clear();
                break;
            case "ddlDept_holder":
                //ddlDocType.Focus();
                getSectionList(ddlSec_holder, int.Parse(ddlOrg_holder.SelectedItem.Value), int.Parse(ddlDept_holder.SelectedItem.Value));

                break;

        }
    }

    protected void getSelectPlace(DropDownList ddlName, string _placeidx)
    {
        data_qa_cims _dataqacimsplace = new data_qa_cims();
        _dataqacimsplace.qa_cims_m0_place_list = new qa_cims_m0_place_detail[1];
        qa_cims_m0_place_detail _m0place = new qa_cims_m0_place_detail();
        _m0place.condition = 2;
        _dataqacimsplace.qa_cims_m0_place_list[0] = _m0place;

        _dataqacimsplace = callServicePostMasterQACIMS(_urlCimsGetPlace, _dataqacimsplace);

        setDdlData(ddlName, _dataqacimsplace.qa_cims_m0_place_list, "place_name", "place_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกสถานที่ ---", "0"));

    }

    #endregion

    #region function get data

    protected void getToolIDList(TextBox txtName, string CheckID)
    {
        divAlertSearchEquipmentOld.Visible = false;
        data_qa_cims _dataDeviceIDNo = new data_qa_cims();
        _dataDeviceIDNo.qa_cims_m0_registration_device_list = new qa_cims_m0_registration_device[1];
        qa_cims_m0_registration_device _toolSearch = new qa_cims_m0_registration_device();
        _toolSearch.device_id_no = txtName.Text;
        _toolSearch.place_idx = int.Parse(ddlPlaceCreate.SelectedValue);
        _toolSearch.rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
        _dataDeviceIDNo.qa_cims_m0_registration_device_list[0] = _toolSearch;

        _dataDeviceIDNo = callServicePostQaDocCIMS(_urlCimsGetSelectSearchTool, _dataDeviceIDNo);

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataDeviceIDNo));

        if (ddlPlaceCreate.SelectedValue == "0")
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาเลือกสถานที่ของเครื่องมือ!');", true);
            tbsearchingToolID.Text = String.Empty;
        }
        else
        {

            if (_dataDeviceIDNo.return_code == 0)
            {
                setFormData(fvShowDetailsSearchTool, FormViewMode.ReadOnly, _dataDeviceIDNo.qa_cims_m0_registration_device_list);
                divAlertSearchEquipmentOld.Visible = false;
                tbsearchingToolID.Text = String.Empty;
                //litDebug.Text = "######";
            }
            else
            {
                //litDebug.Text = "3333";
                //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่พอเครื่องมือ!!');", true);
                setFormData(fvShowDetailsSearchTool, FormViewMode.ReadOnly, null);
                divAlertSearchEquipmentOld.Visible = true;
                tbsearchingToolID.Text = String.Empty;

            }

            ////if (_dataDeviceIDNo.qa_cims_m0_registration_device_list != null)
            ////{
            ////    if (CheckID == "0")
            ////    {
            ////        setFormData(fvShowDetailsSearchTool, FormViewMode.ReadOnly, _dataDeviceIDNo.qa_cims_m0_registration_device_list);
            ////        divAlertSearchEquipmentOld.Visible = false;
            ////        tbsearchingToolID.Text = String.Empty;
            ////        //litDebug.Text = "######";
            ////    }
            ////}
            ////else if(_dataDeviceIDNo.qa_cims_m0_registration_device_list == null)
            ////{

            ////    setFormData(fvShowDetailsSearchTool, FormViewMode.ReadOnly, null);
            ////    divAlertSearchEquipmentOld.Visible = true;

            ////}
        }
    }

    protected void getToolIDSearchList(TextBox txtName, string CheckID)
    {

        //litDebug.Text = txtName.Text;


       
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataqaChecktool_index));

        divAlertSearchEquipmentOld.Visible = false;
        data_qa_cims _dataDeviceIDNo = new data_qa_cims();
        _dataDeviceIDNo.qa_cims_m0_registration_device_list = new qa_cims_m0_registration_device[1];
        qa_cims_m0_registration_device _toolSearch = new qa_cims_m0_registration_device();
        _toolSearch.device_id_no = txtName.Text;
        _toolSearch.place_idx = int.Parse(ddlPlaceCreate.SelectedValue);
        _toolSearch.rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
        _dataDeviceIDNo.qa_cims_m0_registration_device_list[0] = _toolSearch;

        _dataDeviceIDNo = callServicePostQaDocCIMS(_urlCimsGetSelectSearchTool, _dataDeviceIDNo); // detail device register


        



        if (ddlPlaceCreate.SelectedValue == "0")
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาเลือกสถานที่ของเครื่องมือ!');", true);
            tbsearchingToolID.Text = String.Empty;
        }
        else
        {

            data_qa_cims _dataqaChecktool_index = new data_qa_cims();
            _dataqaChecktool_index.qa_cims_u1_document_device_list = new qa_cims_u1_document_device_detail[1];
            qa_cims_u1_document_device_detail _selectDocument_index = new qa_cims_u1_document_device_detail();
            _selectDocument_index.condition = 9;
            _selectDocument_index.device_id_no = txtName.Text;
            _dataqaChecktool_index.qa_cims_u1_document_device_list[0] = _selectDocument_index;

            _dataqaChecktool_index = callServicePostQaDocCIMS(_urlCimsGetEquipmentList, _dataqaChecktool_index);

            litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataqaChecktool_index));

            if (_dataDeviceIDNo.return_code == 0)
            {


                ////setFormData(fvShowDetailsSearchTool, FormViewMode.ReadOnly, _dataDeviceIDNo.qa_cims_m0_registration_device_list);
                ////divAlertSearchEquipmentOld.Visible = false;
                ////tbsearchingToolID.Text = String.Empty;
                if (_dataqaChecktool_index.return_code == 0) // devices register busy
                {

                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่พบเครื่องมือ!!');", true);
                    setFormData(fvShowDetailsSearchTool, FormViewMode.ReadOnly, null);
                    //divAlertSearchEquipmentOld.Visible = true;
                    //tbsearchingToolID.Text = String.Empty;
                }
                else
                {
                    setFormData(fvShowDetailsSearchTool, FormViewMode.ReadOnly, _dataDeviceIDNo.qa_cims_m0_registration_device_list);
                    divAlertSearchEquipmentOld.Visible = false;
                    tbsearchingToolID.Text = String.Empty;
                }



                ////setFormData(fvShowDetailsSearchTool, FormViewMode.ReadOnly, _dataDeviceIDNo.qa_cims_m0_registration_device_list);
                ////divAlertSearchEquipmentOld.Visible = false;
                ////tbsearchingToolID.Text = String.Empty;
                //litDebug.Text = "######";

                //use devices in register tranfer or cutoff



                //litDebug.Text = _dataqaChecktool_index.qa_cims_u1_document_device_list[0].device_id_no.ToString();

                //if (_dataqaChecktool_index.return_code == 0)
                //{
                //    if (_dataqaChecktool_index.qa_cims_u1_document_device_list[0].device_id_no.ToString() != "")
                //    {
                //        setFormData(fvShowDetailsSearchTool, FormViewMode.ReadOnly, null);
                //        divAlertSearchEquipmentOld.Visible = true;
                //        tbsearchingToolID.Text = String.Empty;
                //    }
                //    else
                //    {
                //        setFormData(fvShowDetailsSearchTool, FormViewMode.ReadOnly, _dataDeviceIDNo.qa_cims_m0_registration_device_list);
                //        divAlertSearchEquipmentOld.Visible = false;
                //        tbsearchingToolID.Text = String.Empty;
                //    }
                //}
                //else
                //{

                //}

            }
            else
            {
                //litDebug.Text = "3333";
                //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่พอเครื่องมือ!!');", true);
                setFormData(fvShowDetailsSearchTool, FormViewMode.ReadOnly, null);
                divAlertSearchEquipmentOld.Visible = true;
                tbsearchingToolID.Text = String.Empty;
            }

        }
    }

    protected void getRecordCalResults(TextBox txtName)
    {

        TextBox txtSearchEquipmentIDNo = (TextBox)fvRecordCalResults.FindControl("txtSearchEquipmentIDNo");
        DropDownList ddlSearchPlaceSaveResult = (DropDownList)fvRecordCalResults.FindControl("ddlSearchPlaceSaveResult");
        //Repeater rptPrintReciveDate = (Repeater)fvRecordCalResults.FindControl("rptPrintReciveDate");
        TextBox txtEquipmentIDXNoResult = (TextBox)fvPassValue.FindControl("txtEquipmentIDXNoResult");

        data_qa_cims _data_search_equip = new data_qa_cims();
        _data_search_equip.qa_cims_u1_calibration_document_list = new qa_cims_u1_calibration_document_details[1];
        qa_cims_u1_calibration_document_details _select_equip = new qa_cims_u1_calibration_document_details();
        _select_equip.device_id_no = txtSearchEquipmentIDNo.Text;
        _select_equip.place_idx = int.Parse(ddlSearchPlaceSaveResult.SelectedValue);

        _data_search_equip.qa_cims_u1_calibration_document_list[0] = _select_equip;
        _data_search_equip = callServicePostQaDocCIMS(_urlCimsGetSelectSearchToolForm, _data_search_equip);
        ViewState["vsGridviewName"] = _data_search_equip.qa_cims_record_result_by_form_list;
        setFormData(fvPassValue, FormViewMode.ReadOnly, _data_search_equip.qa_cims_u1_calibration_document_list);

        if (_data_search_equip.return_code == 0) // มีรายการเครื่องมือที่จะทำการกรอกผล
        {

            data_qa_cims _data_select_topics = new data_qa_cims();
            _data_select_topics.qa_cims_bind_topics_form_list = new qa_cims_bind_topics_form_details[1];
            qa_cims_bind_topics_form_details _topics = new qa_cims_bind_topics_form_details();

            _topics.place_idx = int.Parse(ddlSearchPlaceSaveResult.SelectedValue);
            _topics.device_id_no = txtSearchEquipmentIDNo.Text;

            _data_select_topics.qa_cims_bind_topics_form_list[0] = _topics;

            _data_select_topics = callServicePostQaDocCIMS(_urlCimsGetSearchFormTopics, _data_select_topics);
            ViewState["vsTopics"] = _data_select_topics.qa_cims_bind_topics_form_list;
            // setRepeaterData(rptformTopicsSalt, ViewState["vsTopics"]);

            var linqTopics = (from data in _data_select_topics.qa_cims_bind_topics_form_list
                              select data).ToList();
            string t1 = "";

            for (int topics = 0; topics < linqTopics.Count(); topics++)
            {
                if (_data_select_topics.qa_cims_bind_topics_form_list[topics].value.ToString() == "u1_val1")
                {
                    u1_val1_salt.Text = _data_select_topics.qa_cims_bind_topics_form_list[topics].topic_form_name.ToString();
                    lb_u1_val1_idx.Text = _data_select_topics.qa_cims_bind_topics_form_list[topics].topic_form_idx.ToString();
                }
                else if (_data_select_topics.qa_cims_bind_topics_form_list[topics].value.ToString() == "u1_val2")
                {
                    u1_val2_salt.Text = _data_select_topics.qa_cims_bind_topics_form_list[topics].topic_form_name.ToString();
                    lb_u1_val2_idx.Text = _data_select_topics.qa_cims_bind_topics_form_list[topics].topic_form_idx.ToString();
                }
                else if (_data_select_topics.qa_cims_bind_topics_form_list[topics].value.ToString() == "u1_val3")
                {
                    u1_val3_salt.Text = _data_select_topics.qa_cims_bind_topics_form_list[topics].topic_form_name.ToString();
                }
                else if (_data_select_topics.qa_cims_bind_topics_form_list[topics].value.ToString() == "u1_val4")
                {
                    u1_val4_salt.Text = _data_select_topics.qa_cims_bind_topics_form_list[topics].topic_form_name.ToString();
                }

            }

            var M0Nodeidx = (TextBox)fvPassValue.FindControl("txtM0Nodeidx");

            data_qa_cims dataCimsDecision = new data_qa_cims();
            dataCimsDecision.qa_cims_bindnode_decision_list = new qa_cims_bindnode_decision_detail[1];
            qa_cims_bindnode_decision_detail _selectDecisionidx = new qa_cims_bindnode_decision_detail();
            _selectDecisionidx.nodidx = int.Parse(M0Nodeidx.Text);
            dataCimsDecision.qa_cims_bindnode_decision_list[0] = _selectDecisionidx;

            dataCimsDecision = callServicePostQaDocCIMS(_urlCimsGetSelectDecisionNode, dataCimsDecision);
            lbDecisionRecordResult.Text = dataCimsDecision.qa_cims_bindnode_decision_list[0].decision_idx.ToString();

            // check null
            if (_data_search_equip.qa_cims_u1_calibration_document_list[0].table_result_name != null 
                && _data_search_equip.qa_cims_u1_calibration_document_list[0].m0_formcal_name.ToString() != null)
            {
                var nameGrid = _data_search_equip.qa_cims_u1_calibration_document_list[0].table_result_name.ToString();
                var nameForm = _data_search_equip.qa_cims_u1_calibration_document_list[0].m0_formcal_name.ToString();

                lbHeader.Text = "<center>" + "บริษัท เถ้าแก่น้อย ฟู๊ดแอนด์มาร์เก็ตติ้ง จำกัด (มหาชน)" + "</center>";
                lbHeaderFormName.Text = "<center>" + nameForm.ToString() + "</center>";

                pnActionRecordResult.Visible = true;
                divAlertNotfindEquipment.Visible = false;

                switch (nameGrid)
                {
                    case "GvSalt":

                        SetInitialRow();

                        ////setGridData(GvSalt, ViewState["vsGridviewName"]);
                        ////pnValueTopicsSalt.Visible = true;
                        ////lbnameGridview.Text = nameGrid.ToString();
                        ////List<int> rows_salt = new List<int>();
                        ////rows_salt.Add(1);
                        //////for (int salt = 0; salt < 8; salt++)
                        //////{
                        ////// rows_salt.Add(salt);
                        //////}
                        ////////Bind the Gridview to the list of integers so we get 8 rows in the UI
                        ////setGridData(GvSalt, rows_salt);
                        ////ClearTextBoxes(pnValueTopicsSalt);
                        break;
                }
            }
        }
        else
        {
            divAlertNotfindEquipment.Visible = true;

        }
    }
    #endregion

    #region data set
    protected void dataSetCalibrationPoint()
    {
        #region View-CalibrationPoint data table excel
        var ds_calibrationpoint_dataexcel = new DataSet();
        ds_calibrationpoint_dataexcel.Tables.Add("CalibrationPointData_Excel");
        ds_calibrationpoint_dataexcel.Tables[0].Columns.Add("calibration_point", typeof(int));
        ds_calibrationpoint_dataexcel.Tables[0].Columns.Add("unit_idx", typeof(int));
        ds_calibrationpoint_dataexcel.Tables[0].Columns.Add("unit_symbol_en", typeof(string));
        ViewState["vsCalibrationPoint_Excel"] = ds_calibrationpoint_dataexcel;
        #endregion
        ViewState["CalPoint_calibration_point_Excel"] = "";
    }
    #endregion

    #region event link button
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {
            case "cmdSearchTool":

                Panel pnEquipmentOld = (Panel)docCreate.FindControl("pnEquipmentOld");
                TextBox tbsearchingToolID = (TextBox)pnEquipmentOld.FindControl("tbsearchingToolID");

                //litDebug.Text = tbsearchingToolID.Text;

                var ds_Check_id_equipment = (DataSet)ViewState["vsDataEquipmentCalOld"];
                var dr_Check_id_equipment = ds_Check_id_equipment.Tables[0].NewRow();

                int numRowId = ds_Check_id_equipment.Tables[0].Rows.Count; //จำนวนแถว


                ViewState["_id_equipment_checkDuplicate"] = tbsearchingToolID.Text;

                //litDebug.Text = ViewState["_id_equipment_checkDuplicate"].ToString();

                if (numRowId > 0)
                {
                    foreach (DataRow checkId in ds_Check_id_equipment.Tables[0].Rows)
                    {

                        ViewState["checkIdEquipment"] = checkId["IDCodeNumber"];

                        if (ViewState["_id_equipment_checkDuplicate"].ToString() == ViewState["checkIdEquipment"].ToString())
                        {
                            ViewState["CheckDataset_id_equipment"] = "1";
                            break;

                        }
                        else
                        {
                            ViewState["CheckDataset_id_equipment"] = "0";

                        }
                    }
                    if (ViewState["CheckDataset_id_equipment"].ToString() == "1")
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('คุณได้ทำการเพิ่มเครื่องมือนี้แล้ว กรุณากรอกใหม่อีกครั้ง !');", true);

                        tbsearchingToolID.Text = String.Empty;
                        setFormData(fvShowDetailsSearchTool, FormViewMode.ReadOnly, null);
                    }
                    else if (ViewState["CheckDataset_id_equipment"].ToString() == "0")
                    {
                        ViewState["CheckDataset_id_equipment"] = "0";
                        getToolIDSearchList((TextBox)pnEquipmentOld.FindControl("tbsearchingToolID"), ViewState["CheckDataset_id_equipment"].ToString());
                    }
                }

                else if (numRowId == 0)
                {
                    //litDebug.Text = "5555";
                    ViewState["CheckDataset_id_equipment"] = "0";

                    //litDebug.Text = tbsearchingToolID_txtChanged.Text;
                    getToolIDSearchList((TextBox)pnEquipmentOld.FindControl("tbsearchingToolID"), ViewState["CheckDataset_id_equipment"].ToString());
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณากรอกเลข ID เครื่องมือ.!');", true);
                }



                //TextBox tbsearchingToolID = (TextBox)pnEquipmentOld.FindControl("tbsearchingToolID");
                //tbsearchingToolID.EnableViewState = false;

                //var ds_Check_id_equipment = (DataSet)ViewState["vsDataEquipmentCalOld"];
                //var dr_Check_id_equipment = ds_Check_id_equipment.Tables[0].NewRow();

                //int numRowId = ds_Check_id_equipment.Tables[0].Rows.Count; //จำนวนแถว

                //ViewState["_id_equipment_checkDuplicate"] = tbsearchingToolID.Text;

                //if (numRowId > 0)
                //{
                //    foreach (DataRow checkId in ds_Check_id_equipment.Tables[0].Rows)
                //    {

                //        ViewState["checkIdEquipment"] = checkId["IDCodeNumber"];

                //        if (ViewState["_id_equipment_checkDuplicate"].ToString() == ViewState["checkIdEquipment"].ToString())
                //        {
                //            ViewState["CheckDataset_id_equipment"] = "1";
                //            break;

                //        }
                //        else
                //        {
                //            ViewState["CheckDataset_id_equipment"] = "0";

                //        }
                //    }
                //    if (ViewState["CheckDataset_id_equipment"].ToString() == "1")
                //    {
                //        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('คุณได้ทำการเพิ่มเลข ID เครื่องมือ นี้แล้ว กรุณเลือกใหม่อีกครั้งค่ะ !');", true);
                //        setFormData(fvShowDetailsSearchTool, FormViewMode.ReadOnly, null);
                //    }
                //    else if (ViewState["CheckDataset_id_equipment"].ToString() == "0")
                //    {
                //        ViewState["CheckDataset_id_equipment"] = "0";
                //        getToolIDList((TextBox)pnEquipmentOld.FindControl("tbsearchingToolID"), ViewState["CheckDataset_id_equipment"].ToString());



                //    }
                //}

                //else if (numRowId == 0)
                //{
                //    ViewState["CheckDataset_id_equipment"] = "0";
                //    getToolIDList((TextBox)pnEquipmentOld.FindControl("tbsearchingToolID"), ViewState["CheckDataset_id_equipment"].ToString());

                //}

                //else
                //{
                //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณากรอกเลข ID เครื่องมือ.!');", true);
                //}

                break;


            case "cmdInsertDetail":

                var _inputM0DeviceIDX = (TextBox)fvShowDetailsSearchTool.FindControl("tbM0DeviceIDX");
                var _inputIDCodeEquipment = (TextBox)fvShowDetailsSearchTool.FindControl("tbIDCodeEquipment");
                var _inputEquipmentType = (TextBox)fvShowDetailsSearchTool.FindControl("tbEquipmentType");
                var _inputEquipmentName = (TextBox)fvShowDetailsSearchTool.FindControl("tbEquipmentName");
                var _inputEquipmentBrandName = (TextBox)fvShowDetailsSearchTool.FindControl("tbEquipmentBrandName");
                var _inputSerailNumber = (TextBox)fvShowDetailsSearchTool.FindControl("tbSerailNumber");
                var _inputSectionHolder = (TextBox)fvShowDetailsSearchTool.FindControl("tbSectionHolder");
                var _inputDeviceCalDate = (TextBox)fvShowDetailsSearchTool.FindControl("tbDeviceCalDate");
                var _inputDeviceDueDate = (TextBox)fvShowDetailsSearchTool.FindControl("tbDeviceDueDate");

                var ds_Add_EquipmentList_old = (DataSet)ViewState["vsDataEquipmentCalOld"];
                var dr_Add_EquipmentList_old = ds_Add_EquipmentList_old.Tables[0].NewRow();

                int _numrow = ds_Add_EquipmentList_old.Tables[0].Rows.Count; //จำนวนแถว

                ViewState["CHECKDUPICATE_IDCODENUMBER_OLD"] = _inputIDCodeEquipment.Text;

                if (_numrow > 0)
                {
                    foreach (DataRow checkIdCodeEquipment in ds_Add_EquipmentList_old.Tables[0].Rows)
                    {
                        ViewState["_CHECKIDCODENUMBEROLD"] = checkIdCodeEquipment["IDCodeNumber"];

                        if (ViewState["CHECKDUPICATE_IDCODENUMBER_OLD"].ToString() == ViewState["_CHECKIDCODENUMBEROLD"].ToString())
                        {
                            ViewState["CheckDataset_Equipment"] = "1";
                            break;
                        }
                        else
                        {
                            ViewState["CheckDataset_Equipment"] = "0";
                        }

                    }

                    if (ViewState["CheckDataset_Equipment"].ToString() == "1")
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลรหัสเครื่อง " + _inputIDCodeEquipment.Text + " นี้แล้ว กรุณาทำการกรอกรหัสเครื่องใหม่ค่ะ!');", true);

                    }

                    else if (ViewState["CheckDataset_Equipment"].ToString() == "0")
                    {
                        dr_Add_EquipmentList_old["IDCodeNumber"] = _inputIDCodeEquipment.Text;
                        dr_Add_EquipmentList_old["EquipmentType"] = _inputEquipmentType.Text;
                        dr_Add_EquipmentList_old["EquipmentName"] = _inputEquipmentName.Text;
                        dr_Add_EquipmentList_old["BrandName"] = _inputEquipmentBrandName.Text;
                        dr_Add_EquipmentList_old["SerialNumber"] = _inputSerailNumber.Text;
                        dr_Add_EquipmentList_old["Holder"] = _inputSectionHolder.Text;
                        dr_Add_EquipmentList_old["calDate"] = _inputDeviceCalDate.Text;
                        dr_Add_EquipmentList_old["DueDate"] = _inputDeviceDueDate.Text;
                        dr_Add_EquipmentList_old["M0DeviceIDX"] = _inputM0DeviceIDX.Text;

                        ds_Add_EquipmentList_old.Tables[0].Rows.Add(dr_Add_EquipmentList_old);
                        ViewState["vsDataEquipmentCalOld"] = ds_Add_EquipmentList_old;

                        gvListEquipmentCalOld.Visible = true;
                        setGridData(gvListEquipmentCalOld, (DataSet)ViewState["vsDataEquipmentCalOld"]);

                        setFormData(fvShowDetailsSearchTool, FormViewMode.ReadOnly, null);
                    }
                }
                else
                {
                    dr_Add_EquipmentList_old["IDCodeNumber"] = _inputIDCodeEquipment.Text;
                    dr_Add_EquipmentList_old["EquipmentType"] = _inputEquipmentType.Text;
                    dr_Add_EquipmentList_old["EquipmentName"] = _inputEquipmentName.Text;
                    dr_Add_EquipmentList_old["BrandName"] = _inputEquipmentBrandName.Text;
                    dr_Add_EquipmentList_old["SerialNumber"] = _inputSerailNumber.Text;
                    dr_Add_EquipmentList_old["Holder"] = _inputSectionHolder.Text;
                    dr_Add_EquipmentList_old["calDate"] = _inputDeviceCalDate.Text;
                    dr_Add_EquipmentList_old["DueDate"] = _inputDeviceDueDate.Text;
                    dr_Add_EquipmentList_old["M0DeviceIDX"] = _inputM0DeviceIDX.Text;

                    ds_Add_EquipmentList_old.Tables[0].Rows.Add(dr_Add_EquipmentList_old);
                    ViewState["vsDataEquipmentCalOld"] = ds_Add_EquipmentList_old;

                    gvListEquipmentCalOld.Visible = true;
                    setGridData(gvListEquipmentCalOld, (DataSet)ViewState["vsDataEquipmentCalOld"]);

                    setFormData(fvShowDetailsSearchTool, FormViewMode.ReadOnly, null);


                }

                if (ViewState["vsDataEquipmentCalOld"] != null)
                {
                    divActionSaveCreateDocument.Visible = true;
                }
                else
                {
                    divActionSaveCreateDocument.Visible = false;
                }

                break;

            case "cmdSaveDocCal":

                ClearDataCalibrationPoint();


                data_qa_cims _qacalibration = new data_qa_cims();

                _qacalibration.qa_cims_u0_calibration_document_list = new qa_cims_u0_calibration_document_details[1];
                _qacalibration.qa_cims_u1_calibration_document_list = new qa_cims_u1_calibration_document_details[1];
                _qacalibration.qa_cims_u2_calibration_document_list = new qa_cims_u2_calibration_document_details[1];

                qa_cims_u0_calibration_document_details _u0calibration = new qa_cims_u0_calibration_document_details();
                qa_cims_u1_calibration_document_details _u1calibration = new qa_cims_u1_calibration_document_details();
                qa_cims_u2_calibration_document_details _u2calibration = new qa_cims_u2_calibration_document_details();

                RadioButtonList _node_decision = (RadioButtonList)docCreate.FindControl("rblTypeEquipment");

                _u0calibration.cemp_idx = _emp_idx;
                _u0calibration.m0_actor_idx = 1;
                _u0calibration.m0_node_idx = 1;
                _u0calibration.place_idx = int.Parse(ddlPlaceCreate.SelectedValue);
                _u0calibration.decision_idx = int.Parse(_node_decision.SelectedValue);

                _qacalibration.qa_cims_u0_calibration_document_list[0] = _u0calibration;

                switch (_node_decision.SelectedValue)
                {
                    case "1": // machine new

                        var ds_equipment_cal_new_insert = (DataSet)ViewState["vsDataEquipmentCalNew"];
                        var m0_doc_cal_new_insert = new qa_cims_m0_calibration_registration_details[ds_equipment_cal_new_insert.Tables[0].Rows.Count];

                        var ds_point = (DataSet)ViewState["vsDataCalibrationPoint_insert"];
                        var _point = new qa_cims_m1_registration_device[ds_point.Tables[0].Rows.Count];

                        var _checkRowCountNew = ds_equipment_cal_new_insert.Tables[0].Rows.Count;
                        var _checkpointNew = ds_point.Tables[0].Rows.Count;

                        int m = 0;
                        int p = 0;

                        foreach (DataRow dr_equipment_cal_new_insert in ds_equipment_cal_new_insert.Tables[0].Rows)
                        {
                            m0_doc_cal_new_insert[m] = new qa_cims_m0_calibration_registration_details();
                            m0_doc_cal_new_insert[m].m0_device_idx = int.Parse(dr_equipment_cal_new_insert["M0DeviceIDX_New"].ToString());
                            m0_doc_cal_new_insert[m].brand_idx = int.Parse(dr_equipment_cal_new_insert["M0BrandIDX_New"].ToString());
                            m0_doc_cal_new_insert[m].equipment_type_idx = int.Parse(dr_equipment_cal_new_insert["EquipmentTypeIDX_New"].ToString());
                            m0_doc_cal_new_insert[m].range_of_use_min = Convert.ToDecimal(dr_equipment_cal_new_insert["RangeOfUseMin"].ToString());
                            m0_doc_cal_new_insert[m].range_of_use_max = Convert.ToDecimal(dr_equipment_cal_new_insert["RangeOfUseMax"].ToString());
                            m0_doc_cal_new_insert[m].unit_idx_range_of_use = int.Parse(dr_equipment_cal_new_insert["RangeOfUseUnit"].ToString());
                            m0_doc_cal_new_insert[m].cal_type_idx = int.Parse(dr_equipment_cal_new_insert["CalibrationType"].ToString());
                            m0_doc_cal_new_insert[m].acceptance_criteria = Convert.ToDecimal(dr_equipment_cal_new_insert["Acceptance_Criteria"].ToString());
                            m0_doc_cal_new_insert[m].acceptance_criteria_unit_idx = int.Parse(dr_equipment_cal_new_insert["Acceptance_Criteria_Unit"].ToString());
                            m0_doc_cal_new_insert[m].resolution_detail = Convert.ToDecimal(dr_equipment_cal_new_insert["Resolution"].ToString());
                            m0_doc_cal_new_insert[m].unit_idx_resolution = int.Parse(dr_equipment_cal_new_insert["Resolution_Unit"].ToString());
                            m0_doc_cal_new_insert[m].measuring_range_min = Convert.ToDecimal(dr_equipment_cal_new_insert["Measuring_Range_Min"].ToString());
                            m0_doc_cal_new_insert[m].measuring_range_max = Convert.ToDecimal(dr_equipment_cal_new_insert["Measuring_Range_Max"].ToString());
                            m0_doc_cal_new_insert[m].unit_idx_measuring_range = int.Parse(dr_equipment_cal_new_insert["Measuring_Range_Unit"].ToString());
                            m0_doc_cal_new_insert[m].calibration_frequency = Convert.ToDecimal(dr_equipment_cal_new_insert["Calibration_Frequency"].ToString());
                            m0_doc_cal_new_insert[m].device_serial = (dr_equipment_cal_new_insert["SerialNumber_New"].ToString());
                            m0_doc_cal_new_insert[m].device_details = (dr_equipment_cal_new_insert["Details_Equipment"].ToString());
                            m0_doc_cal_new_insert[m].certificate = int.Parse(dr_equipment_cal_new_insert["Certificate_insert"].ToString());
                            m0_doc_cal_new_insert[m].rsec_idx_create = int.Parse(ViewState["rsec_permission"].ToString());
                            m0_doc_cal_new_insert[m].org_idx_create = int.Parse(ViewState["org_permission"].ToString());
                            m0_doc_cal_new_insert[m].rdept_idx_create = int.Parse(ViewState["rdept_permission"].ToString());
                            m0_doc_cal_new_insert[m].emp_idx_create = _emp_idx;
                            m0_doc_cal_new_insert[m].device_rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
                            m0_doc_cal_new_insert[m].device_org_idx = int.Parse(ViewState["org_permission"].ToString());
                            m0_doc_cal_new_insert[m].device_rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
                            m0_doc_cal_new_insert[m].calibration_point_list = (dr_equipment_cal_new_insert["CalibrationPointList"].ToString());
                            m0_doc_cal_new_insert[m].calibration_point_idx_list = (dr_equipment_cal_new_insert["CalibrationPointList_IDX"].ToString());
                            m0_doc_cal_new_insert[m].device_id_no = (dr_equipment_cal_new_insert["IDCodeNumber_New"].ToString());

                            m++;
                        }

                        foreach (DataRow dr_point in ds_point.Tables[0].Rows)
                        {
                            _point[p] = new qa_cims_m1_registration_device();
                            _point[p].calibration_point = (dr_point["CalibrationPoint_insert"].ToString());
                            _point[p].unit_idx = int.Parse(dr_point["CalibrationPointUnitIDX_insert"].ToString());
                            _point[p].device_id_no = (dr_point["IDNUMBER_insert"].ToString());
                            _point[p].device_serial_no = (dr_point["Serial_no_insert"].ToString());
                            _point[p].cemp_idx = _emp_idx;

                            p++;

                        }

                        _qacalibration.qa_cims_m0_calibration_registration_list = m0_doc_cal_new_insert;
                        _qacalibration.qa_cims_m1_registration_device_list = _point;
                        _qacalibration = callServicePostQaDocCIMS(_urlCimsSetDocumentCalibration, _qacalibration);

                        if (_qacalibration.return_code == 0)
                        {
                            setActiveTab("docDetailList", 0, 0, 0, 0, 0, 0, 0);
                            setOntop.Focus();
                        }

                        // litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_qacalibration));

                        break;

                    case "2": // machine old 

                        var ds_equipment_cal_old_insert = (DataSet)ViewState["vsDataEquipmentCalOld"];
                        var u1_doc_cal_insert = new qa_cims_u1_calibration_document_details[ds_equipment_cal_old_insert.Tables[0].Rows.Count];
                        var _checkRowCount = ds_equipment_cal_old_insert.Tables[0].Rows.Count;

                        int c = 0;

                        foreach (DataRow dr_equipment_cal_old_insert in ds_equipment_cal_old_insert.Tables[0].Rows)
                        {
                            u1_doc_cal_insert[c] = new qa_cims_u1_calibration_document_details();
                            u1_doc_cal_insert[c].m0_device_idx = int.Parse(dr_equipment_cal_old_insert["M0DeviceIDX"].ToString());
                            //  u1_doc_cal_insert[c].certificate = 1;

                            c++;
                        }

                        _qacalibration.qa_cims_u1_calibration_document_list = u1_doc_cal_insert;
                        _qacalibration = callServicePostQaDocCIMS(_urlCimsSetDocumentCalibration, _qacalibration);

                        if (_qacalibration.return_code == 0)
                        {
                            setActiveTab("docDetailList", 0, 0, 0, 0, 0, 0, 0);
                            setOntop.Focus();
                        }

                        //  litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_qacalibration));

                        break;

                }


                break;

            case "cmdAddCalibrationPoint":

                var _input_calibration_point = (TextBox)pnEquipmentNew.FindControl("tbcalibration_point");
                var _input_calibration_point_idx = (DropDownList)pnEquipmentNew.FindControl("ddl_calibration_point_unit");
                var _input_serailnumber_insert = (TextBox)pnEquipmentNew.FindControl("tbserailnumber");

                var ds_add_calibration_point = (DataSet)ViewState["vsDataCalibrationPoint"];
                var dr_add_calibration_point = ds_add_calibration_point.Tables[0].NewRow();

                int numrow_point = ds_add_calibration_point.Tables[0].Rows.Count; //จำนวนแถว

                ViewState["CHECK"] = _input_calibration_point.Text;

                if (numrow_point > 0)
                {
                    dr_add_calibration_point["CalibrationPoint"] = _input_calibration_point.Text;
                    dr_add_calibration_point["CalibrationPointUnitIDX"] = int.Parse(_input_calibration_point_idx.SelectedValue);
                    dr_add_calibration_point["CalibrationPointUnit"] = _input_calibration_point_idx.SelectedItem.Text;
                    dr_add_calibration_point["IDNUMBER"] = tbIDNo.Text;



                }
                else
                {
                    dr_add_calibration_point["CalibrationPoint"] = _input_calibration_point.Text;
                    dr_add_calibration_point["CalibrationPointUnitIDX"] = int.Parse(_input_calibration_point_idx.SelectedValue);
                    dr_add_calibration_point["CalibrationPointUnit"] = _input_calibration_point_idx.SelectedItem.Text;
                    dr_add_calibration_point["IDNUMBER"] = tbIDNo.Text;
                }


                var ds_add_calibration_point_insert = (DataSet)ViewState["vsDataCalibrationPoint_insert"];
                var dr_add_calibration_point_insert = ds_add_calibration_point_insert.Tables[0].NewRow();

                int numrow_point_insert = ds_add_calibration_point_insert.Tables[0].Rows.Count; //จำนวนแถว

                //  ViewState["CHECK"] = _input_calibration_point.Text;

                if (numrow_point_insert > 0)
                {
                    dr_add_calibration_point_insert["CalibrationPoint_insert"] = _input_calibration_point.Text;
                    dr_add_calibration_point_insert["CalibrationPointUnitIDX_insert"] = int.Parse(_input_calibration_point_idx.SelectedValue);
                    dr_add_calibration_point_insert["CalibrationPointUnit_insert"] = _input_calibration_point_idx.SelectedItem.Text;
                    dr_add_calibration_point_insert["IDNUMBER_insert"] = tbIDNo.Text;
                    dr_add_calibration_point_insert["Serial_no_insert"] = _input_serailnumber_insert.Text;



                }
                else
                {
                    dr_add_calibration_point_insert["CalibrationPoint_insert"] = _input_calibration_point.Text;
                    dr_add_calibration_point_insert["CalibrationPointUnitIDX_insert"] = int.Parse(_input_calibration_point_idx.SelectedValue);
                    dr_add_calibration_point_insert["CalibrationPointUnit_insert"] = _input_calibration_point_idx.SelectedItem.Text;
                    dr_add_calibration_point_insert["IDNUMBER_insert"] = tbIDNo.Text;
                    dr_add_calibration_point_insert["Serial_no_insert"] = _input_serailnumber_insert.Text;
                }

                ds_add_calibration_point_insert.Tables[0].Rows.Add(dr_add_calibration_point_insert);
                ViewState["vsDataCalibrationPoint_insert"] = ds_add_calibration_point_insert;

                ds_add_calibration_point.Tables[0].Rows.Add(dr_add_calibration_point);
                ViewState["vsDataCalibrationPoint"] = ds_add_calibration_point;


                gvCalibrationPoint.Visible = true;
                setGridData(gvCalibrationPoint, (DataSet)ViewState["vsDataCalibrationPoint"]);

                setGridData(gvCalPointInsert, (DataSet)ViewState["vsDataCalibrationPoint_insert"]);

                if (ViewState["vsDataCalibrationPoint"] != null)
                {
                    btnInsertDetail.Visible = true;
                }
                else
                {
                    btnInsertDetail.Visible = false;
                }

                break;

            case "btnAddAcceptance":

                var _input_txtAcceptance = (TextBox)pnEquipmentNew.FindControl("txtAcceptance");
                var _input_ddlUnitAcceptance = (DropDownList)pnEquipmentNew.FindControl("ddlUnitAcceptance");
                var _input_serailnumber_Acceptanct_insert = (TextBox)pnEquipmentNew.FindControl("tbserailnumber");

                var ds_add_Acceptance = (DataSet)ViewState["vsDataAcceptance"];
                var dr_add_Acceptance = ds_add_Acceptance.Tables[0].NewRow();

                int numrow_add_Acceptance = ds_add_Acceptance.Tables[0].Rows.Count; //จำนวนแถว

                ViewState["CHECK_dataAcceptance"] = _input_txtAcceptance.Text;

                if (numrow_add_Acceptance > 0)
                {
                    dr_add_Acceptance["Acceptance"] = _input_txtAcceptance.Text;
                    dr_add_Acceptance["AcceptanceUnitIDX"] = int.Parse(_input_ddlUnitAcceptance.SelectedValue);
                    dr_add_Acceptance["AcceptanceUnit"] = _input_ddlUnitAcceptance.SelectedItem.Text;
                    dr_add_Acceptance["IDNUMBERAcceptance"] = tbIDNo.Text;



                }
                else
                {
                    dr_add_Acceptance["Acceptance"] = _input_txtAcceptance.Text;
                    dr_add_Acceptance["AcceptanceUnitIDX"] = int.Parse(_input_ddlUnitAcceptance.SelectedValue);
                    dr_add_Acceptance["AcceptanceUnit"] = _input_ddlUnitAcceptance.SelectedItem.Text;
                    dr_add_Acceptance["IDNUMBERAcceptance"] = tbIDNo.Text;
                }


                //check data with delete value devices
                var ds_add_Acceptance_insert = (DataSet)ViewState["vsDataAcceptance_insert"];
                var dr_add_Acceptance_insert = ds_add_Acceptance_insert.Tables[0].NewRow();

                int numrow_Acceptance_insert = ds_add_Acceptance_insert.Tables[0].Rows.Count; //จำนวนแถว

                //  ViewState["CHECK"] = _input_calibration_point.Text;

                if (numrow_Acceptance_insert > 0)
                {
                    dr_add_Acceptance_insert["Acceptance_insert"] = _input_txtAcceptance.Text;
                    dr_add_Acceptance_insert["AcceptanceUnitIDX_insert"] = int.Parse(_input_ddlUnitAcceptance.SelectedValue);
                    dr_add_Acceptance_insert["AcceptanceUnit_insert"] = _input_ddlUnitAcceptance.SelectedItem.Text;
                    dr_add_Acceptance_insert["IDNUMBERAcceptance_insert"] = tbIDNo.Text;
                    dr_add_Acceptance_insert["Serial_no_Acceptance_insert"] = _input_serailnumber_Acceptanct_insert.Text;



                }
                else
                {
                    dr_add_Acceptance_insert["Acceptance_insert"] = _input_txtAcceptance.Text;
                    dr_add_Acceptance_insert["AcceptanceUnitIDX_insert"] = int.Parse(_input_ddlUnitAcceptance.SelectedValue);
                    dr_add_Acceptance_insert["AcceptanceUnit_insert"] = _input_ddlUnitAcceptance.SelectedItem.Text;
                    dr_add_Acceptance_insert["IDNUMBERAcceptance_insert"] = tbIDNo.Text;
                    dr_add_Acceptance_insert["Serial_no_Acceptance_insert"] = _input_serailnumber_Acceptanct_insert.Text;
                }

                ds_add_Acceptance_insert.Tables[0].Rows.Add(dr_add_Acceptance_insert);
                ViewState["vsDataAcceptance_insert"] = ds_add_Acceptance_insert;

                ds_add_Acceptance.Tables[0].Rows.Add(dr_add_Acceptance);
                ViewState["vsDataAcceptance"] = ds_add_Acceptance;


                gvAcceptance.Visible = true;
                setGridData(gvAcceptance, (DataSet)ViewState["vsDataAcceptance"]);

                setGridData(gvAcceptanceInsert, (DataSet)ViewState["vsDataAcceptance_insert"]);

                //if (ViewState["vsDataAcceptance"] != null)
                //{
                //    btnInsertDetail.Visible = true;
                //}
                //else
                //{
                //    btnInsertDetail.Visible = false;
                //}

                break;

            case "btnAddMeasuring":

                var _input_txtMeasuringstart = (TextBox)pnEquipmentNew.FindControl("txtMeasuringstart");
                var _input_txtMeasuringend = (TextBox)pnEquipmentNew.FindControl("txtMeasuringend");
                var _input_ddlUnitMeasuring = (DropDownList)pnEquipmentNew.FindControl("ddlUnitMeasuring");
                var _input_serailnumber_Measuring_insert = (TextBox)pnEquipmentNew.FindControl("tbserailnumber");

                var ds_add_Measuring = (DataSet)ViewState["vsDataMeasuring"];
                var dr_add_Measuring = ds_add_Measuring.Tables[0].NewRow();

                int numrow_add_Measuring = ds_add_Measuring.Tables[0].Rows.Count; //จำนวนแถว

                ViewState["CHECK_dataMeasuringStart"] = _input_txtMeasuringstart.Text;

                if (numrow_add_Measuring > 0)
                {
                    dr_add_Measuring["MeasuringStart"] = _input_txtMeasuringstart.Text;
                    dr_add_Measuring["MeasuringEnd"] = _input_txtMeasuringend.Text;
                    dr_add_Measuring["MeasuringUnitIDX"] = int.Parse(_input_ddlUnitMeasuring.SelectedValue);
                    dr_add_Measuring["MeasuringUnit"] = _input_ddlUnitMeasuring.SelectedItem.Text;
                    dr_add_Measuring["IDNUMBERMeasuring"] = tbIDNo.Text;

                }
                else
                {
                    dr_add_Measuring["MeasuringStart"] = _input_txtMeasuringstart.Text;
                    dr_add_Measuring["MeasuringEnd"] = _input_txtMeasuringend.Text;
                    dr_add_Measuring["MeasuringUnitIDX"] = int.Parse(_input_ddlUnitMeasuring.SelectedValue);
                    dr_add_Measuring["MeasuringUnit"] = _input_ddlUnitMeasuring.SelectedItem.Text;
                    dr_add_Measuring["IDNUMBERMeasuring"] = tbIDNo.Text;
                }


                //check data with delete value devices
                var ds_add_Measuring_insert = (DataSet)ViewState["vsDataMeasuring_insert"];
                var dr_add_Measuring_insert = ds_add_Measuring_insert.Tables[0].NewRow();

                int numrow_Measuring_insert = ds_add_Measuring_insert.Tables[0].Rows.Count; //จำนวนแถว

                //  ViewState["CHECK"] = _input_calibration_point.Text;

                if (numrow_Measuring_insert > 0)
                {
                    dr_add_Measuring_insert["MeasuringStart_insert"] = _input_txtMeasuringstart.Text;
                    dr_add_Measuring_insert["MeasuringEnd_insert"] = _input_txtMeasuringend.Text;
                    dr_add_Measuring_insert["MeasuringUnitIDX_insert"] = int.Parse(_input_ddlUnitMeasuring.SelectedValue);
                    dr_add_Measuring_insert["MeasuringUnit_insert"] = _input_ddlUnitMeasuring.SelectedItem.Text;
                    dr_add_Measuring_insert["IDNUMBERMeasuring_insert"] = tbIDNo.Text;
                    dr_add_Measuring_insert["Serial_no_Measuring_insert"] = _input_serailnumber_Measuring_insert.Text;

                }
                else
                {
                    dr_add_Measuring_insert["MeasuringStart_insert"] = _input_txtMeasuringstart.Text;
                    dr_add_Measuring_insert["MeasuringEnd_insert"] = _input_txtMeasuringend.Text;
                    dr_add_Measuring_insert["MeasuringUnitIDX_insert"] = int.Parse(_input_ddlUnitMeasuring.SelectedValue);
                    dr_add_Measuring_insert["MeasuringUnit_insert"] = _input_ddlUnitMeasuring.SelectedItem.Text;
                    dr_add_Measuring_insert["IDNUMBERMeasuring_insert"] = tbIDNo.Text;
                    dr_add_Measuring_insert["Serial_no_Measuring_insert"] = _input_serailnumber_Measuring_insert.Text;
                }

                ds_add_Measuring_insert.Tables[0].Rows.Add(dr_add_Measuring_insert);
                ViewState["vsDataMeasuring_insert"] = ds_add_Measuring_insert;
              
                ds_add_Measuring.Tables[0].Rows.Add(dr_add_Measuring);
                ViewState["vsDataMeasuring"] = ds_add_Measuring;


                gvAcceptance.Visible = true;
                setGridData(gvMeasuring, (DataSet)ViewState["vsDataMeasuring"]);

                setGridData(gvMeasuringInsert, (DataSet)ViewState["vsDataMeasuring_insert"]);

                //if (ViewState["vsDataAcceptance"] != null)
                //{
                //    btnInsertDetail.Visible = true;
                //}
                //else
                //{
                //    btnInsertDetail.Visible = false;
                //}

                break;

            case "btnAddRangeOfUse":

                var _input_txtRangestart = (TextBox)pnEquipmentNew.FindControl("txtRangestart");
                var _input_txtRangeend = (TextBox)pnEquipmentNew.FindControl("txtRangeend");
                var _input_ddlUnitRange = (DropDownList)pnEquipmentNew.FindControl("ddlUnitRange");
                var _input_serailnumber_Range_insert = (TextBox)pnEquipmentNew.FindControl("tbserailnumber");

                var ds_add_Range = (DataSet)ViewState["vsDataRange"];
                var dr_add_Range = ds_add_Range.Tables[0].NewRow();

                int numrow_add_Range = ds_add_Range.Tables[0].Rows.Count; //จำนวนแถว

                ViewState["CHECK_dataRangeStart"] = _input_txtRangestart.Text;

                if (numrow_add_Range > 0)
                {
                    dr_add_Range["RangeStart"] = _input_txtRangestart.Text;
                    dr_add_Range["RangeEnd"] = _input_txtRangeend.Text;
                    dr_add_Range["RangeUnitIDX"] = int.Parse(_input_ddlUnitRange.SelectedValue);
                    dr_add_Range["RangeUnit"] = _input_ddlUnitRange.SelectedItem.Text;
                    dr_add_Range["IDNUMBERRange"] = tbIDNo.Text;

                }
                else
                {
                    dr_add_Range["RangeStart"] = _input_txtRangestart.Text;
                    dr_add_Range["RangeEnd"] = _input_txtRangeend.Text;
                    dr_add_Range["RangeUnitIDX"] = int.Parse(_input_ddlUnitRange.SelectedValue);
                    dr_add_Range["RangeUnit"] = _input_ddlUnitRange.SelectedItem.Text;
                    dr_add_Range["IDNUMBERRange"] = tbIDNo.Text;
                }


                //check data with delete value devices
                var ds_add_Range_insert = (DataSet)ViewState["vsDataRange_insert"];
                var dr_add_Range_insert = ds_add_Range_insert.Tables[0].NewRow();

                int numrow_Range_insert = ds_add_Range_insert.Tables[0].Rows.Count; //จำนวนแถว

                //  ViewState["CHECK"] = _input_calibration_point.Text;

                if (numrow_Range_insert > 0)
                {
                    dr_add_Range_insert["RangeStart_insert"] = _input_txtRangestart.Text;
                    dr_add_Range_insert["RangeEnd_insert"] = _input_txtRangeend.Text;
                    dr_add_Range_insert["RangeUnitIDX_insert"] = int.Parse(_input_ddlUnitRange.SelectedValue);
                    dr_add_Range_insert["RangeUnit_insert"] = _input_ddlUnitRange.SelectedItem.Text;
                    dr_add_Range_insert["IDNUMBERRange_insert"] = tbIDNo.Text;
                    dr_add_Range_insert["Serial_no_Range_insert"] = _input_serailnumber_Range_insert.Text;

                }
                else
                {
                    dr_add_Range_insert["RangeStart_insert"] = _input_txtRangestart.Text;
                    dr_add_Range_insert["RangeEnd_insert"] = _input_txtRangeend.Text;
                    dr_add_Range_insert["RangeUnitIDX_insert"] = int.Parse(_input_ddlUnitRange.SelectedValue);
                    dr_add_Range_insert["RangeUnit_insert"] = _input_ddlUnitRange.SelectedItem.Text;
                    dr_add_Range_insert["IDNUMBERRange_insert"] = tbIDNo.Text;
                    dr_add_Range_insert["Serial_no_Range_insert"] = _input_serailnumber_Range_insert.Text;
                }

                ds_add_Range_insert.Tables[0].Rows.Add(dr_add_Range_insert);
                ViewState["vsDataRange_insert"] = ds_add_Range_insert;

                ds_add_Range.Tables[0].Rows.Add(dr_add_Range);
                ViewState["vsDataRange"] = ds_add_Range;
              


                gvAcceptance.Visible = true;
                setGridData(gvRange, (DataSet)ViewState["vsDataRange"]);

                setGridData(gvRangeInsert, (DataSet)ViewState["vsDataRange_insert"]);

                //if (ViewState["vsDataAcceptance"] != null)
                //{
                //    btnInsertDetail.Visible = true;
                //}
                //else
                //{
                //    btnInsertDetail.Visible = false;
                //}

                break;

            case "btnAddResolution":
              
                var _input_ddlResolution = (DropDownList)pnEquipmentNew.FindControl("ddlResolution");
                var _input_serailnumber_Resolution_insert = (TextBox)pnEquipmentNew.FindControl("tbserailnumber");

                var ds_add_Resolution = (DataSet)ViewState["vsDataResolution"];
                var dr_add_Resolution = ds_add_Resolution.Tables[0].NewRow();

                int numrow_add_Resolution = ds_add_Resolution.Tables[0].Rows.Count; //จำนวนแถว

                ViewState["CHECK_dataResolution"] = _input_ddlResolution.Text;

                if (numrow_add_Resolution > 0)
                {             
                    dr_add_Resolution["ResolutionIDX"] = int.Parse(_input_ddlResolution.SelectedValue);
                    dr_add_Resolution["Resolution"] = _input_ddlResolution.SelectedItem.Text;
                    dr_add_Resolution["IDNUMBERResolution"] = tbIDNo.Text;

                }
                else
                {
                    dr_add_Resolution["ResolutionIDX"] = int.Parse(_input_ddlResolution.SelectedValue);
                    dr_add_Resolution["Resolution"] = _input_ddlResolution.SelectedItem.Text;
                    dr_add_Resolution["IDNUMBERResolution"] = tbIDNo.Text;
                }


                //check data with delete value devices
                var ds_add_Resolution_insert = (DataSet)ViewState["vsDataResolution_insert"];
                var dr_add_Resolution_insert = ds_add_Resolution_insert.Tables[0].NewRow();

                int numrow_Resolution_insert = ds_add_Resolution_insert.Tables[0].Rows.Count; //จำนวนแถว

                //  ViewState["CHECK"] = _input_calibration_point.Text;

                if (numrow_Resolution_insert > 0)
                {
                    
                    dr_add_Resolution_insert["ResolutionIDX_insert"] = int.Parse(_input_ddlResolution.SelectedValue);
                    dr_add_Resolution_insert["Resolution_insert"] = _input_ddlResolution.SelectedItem.Text;
                    dr_add_Resolution_insert["IDNUMBERResolution_insert"] = tbIDNo.Text;
                    dr_add_Resolution_insert["Serial_no_Resolution_insert"] = _input_serailnumber_Resolution_insert.Text;

                }
                else
                {
                    dr_add_Resolution_insert["ResolutionIDX_insert"] = int.Parse(_input_ddlResolution.SelectedValue);
                    dr_add_Resolution_insert["Resolution_insert"] = _input_ddlResolution.SelectedItem.Text;
                    dr_add_Resolution_insert["IDNUMBERResolution_insert"] = tbIDNo.Text;
                    dr_add_Resolution_insert["Serial_no_Resolution_insert"] = _input_serailnumber_Resolution_insert.Text;
                }

                ds_add_Resolution_insert.Tables[0].Rows.Add(dr_add_Resolution_insert);
                ViewState["vsDataResolution_insert"] = ds_add_Resolution_insert;

                ds_add_Resolution.Tables[0].Rows.Add(dr_add_Resolution);
                ViewState["vsDataResolution"] = ds_add_Resolution;
             
                gvResolution.Visible = true;
                setGridData(gvResolution, (DataSet)ViewState["vsDataResolution"]);

                setGridData(gvResolutionInsert, (DataSet)ViewState["vsDataResolution_insert"]);

                //if (ViewState["vsDataAcceptance"] != null)
                //{
                //    btnInsertDetail.Visible = true;
                //}
                //else
                //{
                //    btnInsertDetail.Visible = false;
                //}

                break;

            case "cmdInserEquipmentNew":

                var _input_serailnumber = (TextBox)pnEquipmentNew.FindControl("tbserailnumber");
                var _input_details_equipment = (TextBox)pnEquipmentNew.FindControl("tbdetails_equipment");
                //var _input_measuring_range_min = (TextBox)pnEquipmentNew.FindControl("tbMeasuringRangeMin");
                //var _input_measuring_range_max = (TextBox)pnEquipmentNew.FindControl("tbMeasuringRangeMax");
                //var _input_resolution = (TextBox)pnEquipmentNew.FindControl("tbResolution");
                //var _input_acceptance_criteria = (TextBox)pnEquipmentNew.FindControl("tbAcceptanceCriteria");

                //var _input_calibration_frequency = (TextBox)pnEquipmentNew.FindControl("tbCalibrationFrequency");

                //var _input_range_of_use_min = (TextBox)pnEquipmentNew.FindControl("tbRangeOfUseMin");
                //var _input_range_of_use_max = (TextBox)pnEquipmentNew.FindControl("tbRangeOfUseMax");
                var _input_id_no = (TextBox)pnEquipmentNew.FindControl("tbIDNo");

                var _machine_name = (DropDownList)pnEquipmentNew.FindControl("ddl_machine_name");
                var _machine_brand = (DropDownList)pnEquipmentNew.FindControl("ddl_machine_brand");
                var _calibration_type = (DropDownList)pnEquipmentNew.FindControl("dllcaltype");
                var _machine_type = (DropDownList)pnEquipmentNew.FindControl("ddlmachineType");
                //var _range_of_use_unit = (DropDownList)pnEquipmentNew.FindControl("ddl_range_of_use_unit");
                //var _measuring_range_unit = (DropDownList)pnEquipmentNew.FindControl("ddl_measuring_range_unit");
                //var _resolution_unit = (DropDownList)pnEquipmentNew.FindControl("ddl_resolution_unit");
                //var _acceptance_criteria_unit = (DropDownList)pnEquipmentNew.FindControl("ddl_acceptance_criteria_unit");

                var _input_certificate = (RadioButtonList)pnEquipmentNew.FindControl("rd_certificate");

                var ds_add_equipment_new = (DataSet)ViewState["vsDataEquipmentCalNew"];
                var dr_add_equipment_new = ds_add_equipment_new.Tables[0].NewRow();

                int numrow_new = ds_add_equipment_new.Tables[0].Rows.Count; //จำนวนแถว

                if (numrow_new > 0)
                {
                    dr_add_equipment_new["SerialNumber_New"] = _input_serailnumber.Text;
                    dr_add_equipment_new["Details_Equipment"] = _input_details_equipment.Text;
                    dr_add_equipment_new["BrandName_New"] = _machine_brand.SelectedItem.Text;
                    dr_add_equipment_new["CalibrationTypeName"] = _calibration_type.SelectedItem.Text;
                    dr_add_equipment_new["EquipmentType_New"] = _machine_type.SelectedItem.Text;
                    //dr_add_equipment_new["RangeOfUseUnitName"] = _range_of_use_unit.SelectedItem.Text;
                    //dr_add_equipment_new["Measuring_Range_UnitName"] = _measuring_range_unit.SelectedItem.Text;
                   // dr_add_equipment_new["Resolution_UnitName"] = _resolution_unit.SelectedItem.Text;
                    //dr_add_equipment_new["Acceptance_Criteria_UnitName"] = _acceptance_criteria_unit.SelectedItem.Text;
                    dr_add_equipment_new["EquipmentName_New"] = _machine_name.SelectedItem.Text;
                    dr_add_equipment_new["IDCodeNumber_New"] = _input_id_no.Text;

                    //dr_add_equipment_new["Measuring_Range_Min"] = _input_measuring_range_min.Text;
                    //dr_add_equipment_new["Measuring_Range_Max"] = _input_measuring_range_max.Text;
                   // dr_add_equipment_new["Resolution"] = _input_resolution.Text;
                    //dr_add_equipment_new["Acceptance_Criteria"] = _input_acceptance_criteria.Text;
                    //dr_add_equipment_new["Calibration_Frequency"] = _input_calibration_frequency.Text;
                    //dr_add_equipment_new["RangeOfUseMin"] = _input_range_of_use_min.Text;
                    //dr_add_equipment_new["RangeOfUseMax"] = _input_range_of_use_max.Text;
                    dr_add_equipment_new["Certificate_insert"] = _input_certificate.SelectedValue;

                    dr_add_equipment_new["M0BrandIDX_New"] = int.Parse(_machine_brand.SelectedValue);
                    dr_add_equipment_new["CalibrationType"] = int.Parse(_calibration_type.SelectedValue);
                    dr_add_equipment_new["EquipmentTypeIDX_New"] = int.Parse(_machine_type.SelectedValue);
                    //dr_add_equipment_new["Acceptance_Criteria_Unit"] = int.Parse(_acceptance_criteria_unit.SelectedValue);
                    //dr_add_equipment_new["Resolution_Unit"] = int.Parse(_resolution_unit.SelectedValue);
                    //dr_add_equipment_new["Measuring_Range_Unit"] = int.Parse(_measuring_range_unit.SelectedValue);
                    //dr_add_equipment_new["RangeOfUseUnit"] = int.Parse(_range_of_use_unit.SelectedValue);
                    dr_add_equipment_new["M0DeviceIDX_New"] = int.Parse(_machine_name.SelectedValue);

                    var dsCalibrationPoint = (DataSet)ViewState["vsDataCalibrationPoint"];
                    string addListCalpoint = "";
                    string addListCalpointIDX = "";

                    foreach (DataRow dr in dsCalibrationPoint.Tables[0].Rows)
                    {

                        addListCalpoint += dr["CalibrationPoint"].ToString() + ",";
                        addListCalpointIDX += dr["CalibrationPointUnitIDX"].ToString() + ",";
                    }

                    dr_add_equipment_new["CalibrationPointList"] = addListCalpoint;
                    dr_add_equipment_new["CalibrationPointList_IDX"] = addListCalpointIDX;

                }
                else
                {
                    dr_add_equipment_new["SerialNumber_New"] = _input_serailnumber.Text;
                    dr_add_equipment_new["Details_Equipment"] = _input_details_equipment.Text;
                    dr_add_equipment_new["BrandName_New"] = _machine_brand.SelectedItem.Text;
                    dr_add_equipment_new["CalibrationTypeName"] = _calibration_type.SelectedItem.Text;
                    dr_add_equipment_new["EquipmentType_New"] = _machine_type.SelectedItem.Text;
                    //dr_add_equipment_new["RangeOfUseUnitName"] = _range_of_use_unit.SelectedItem.Text;
                    //dr_add_equipment_new["Measuring_Range_UnitName"] = _measuring_range_unit.SelectedItem.Text;
                    //dr_add_equipment_new["Resolution_UnitName"] = _resolution_unit.SelectedItem.Text;
                    //dr_add_equipment_new["Acceptance_Criteria_UnitName"] = _acceptance_criteria_unit.SelectedItem.Text;
                    dr_add_equipment_new["EquipmentName_New"] = _machine_name.SelectedItem.Text;
                    dr_add_equipment_new["IDCodeNumber_New"] = _input_id_no.Text;

                    //dr_add_equipment_new["Measuring_Range_Min"] = _input_measuring_range_min.Text;
                    //dr_add_equipment_new["Measuring_Range_Max"] = _input_measuring_range_max.Text;
                    //dr_add_equipment_new["Resolution"] = _input_resolution.Text;
                    //dr_add_equipment_new["Acceptance_Criteria"] = _input_acceptance_criteria.Text;
                    //dr_add_equipment_new["Calibration_Frequency"] = _input_calibration_frequency.Text;
                    //dr_add_equipment_new["RangeOfUseMin"] = _input_range_of_use_min.Text;
                    //dr_add_equipment_new["RangeOfUseMax"] = _input_range_of_use_max.Text;
                    dr_add_equipment_new["Certificate_insert"] = _input_certificate.SelectedValue;


                    dr_add_equipment_new["M0BrandIDX_New"] = int.Parse(_machine_brand.SelectedValue);
                    dr_add_equipment_new["CalibrationType"] = int.Parse(_calibration_type.SelectedValue);
                    dr_add_equipment_new["EquipmentTypeIDX_New"] = int.Parse(_machine_type.SelectedValue);
                    //dr_add_equipment_new["Acceptance_Criteria_Unit"] = int.Parse(_acceptance_criteria_unit.SelectedValue);
                    //dr_add_equipment_new["Resolution_Unit"] = int.Parse(_resolution_unit.SelectedValue);
                    //dr_add_equipment_new["Measuring_Range_Unit"] = int.Parse(_measuring_range_unit.SelectedValue);
                    //dr_add_equipment_new["RangeOfUseUnit"] = int.Parse(_range_of_use_unit.SelectedValue);
                    dr_add_equipment_new["M0DeviceIDX_New"] = int.Parse(_machine_name.SelectedValue);


                    //CalibrationPoint Start
                    var dsCalibrationPoint = (DataSet)ViewState["vsDataCalibrationPoint"];
                    string addListCalpoint = "";
                    string addListCalpointIDX = "";

                    foreach (DataRow dr in dsCalibrationPoint.Tables[0].Rows)
                    {

                        addListCalpoint += dr["CalibrationPoint"].ToString() + ",";
                        addListCalpointIDX += dr["CalibrationPointUnitIDX"].ToString() + ",";
                    }

                    dr_add_equipment_new["CalibrationPointList"] = addListCalpoint;
                    dr_add_equipment_new["CalibrationPointList_IDX"] = addListCalpointIDX;
                    //CalibrationPoint End

                    //////Resolution Start
                    ////var dsResolution = (DataSet)ViewState["vsDataResolution"];
                    ////string addListResolution = "";
                    ////string addListResolutionIDX = "";

                    ////foreach (DataRow dr_resolution in dsResolution.Tables[0].Rows)
                    ////{

                    ////    addListResolution += dr_resolution["Resolution"].ToString() + ",";
                    ////    addListResolutionIDX += dr_resolution["ResolutionIDX"].ToString() + ",";
                    ////}

                    ////dr_add_equipment_new["ResolutionList"] = addListResolution;
                    ////dr_add_equipment_new["ResolutionIDX_IDX"] = addListResolutionIDX;
                    //////Resolution END


                }
                ds_add_equipment_new.Tables[0].Rows.Add(dr_add_equipment_new);
                ViewState["vsDataEquipmentCalNew"] = ds_add_equipment_new;

                gvListEquipmentCalNew.Visible = true;
                setGridData(gvListEquipmentCalNew, (DataSet)ViewState["vsDataEquipmentCalNew"]);

                if (ViewState["vsDataEquipmentCalNew"] != null)
                {
                    divActionSaveCreateDocument.Visible = true;
                }
                else
                {
                    divActionSaveCreateDocument.Visible = false;

                }

                ClearDataCalibrationPoint();
                ClearDataAcceptance();
                ClearDataMeasuring();
                ClearDataRange();
                ClearDataResolution();

                setGridData(gvCalPointInsert, (DataSet)ViewState["vsDataCalibrationPoint_insert"]);
                setGridData(gvResolutionInsert, (DataSet)ViewState["vsDataResolution_insert"]);
                break;

            case "DeletePoint":

                string[] arg1 = new string[3];
                arg1 = e.CommandArgument.ToString().Split(',');
                string idnumber = (arg1[0]);
                int condition = int.Parse(arg1[1]);
                string calpoint = (arg1[2]);

                switch (condition)
                {
                    case 0:
                        var ds_point_delete = (DataSet)ViewState["vsDataCalibrationPoint_insert"];
                        ViewState["idnumber"] = idnumber;
                        ViewState["point_cal"] = calpoint;

                        for (int counter_point = 0; counter_point < ds_point_delete.Tables[0].Rows.Count; counter_point++)
                        {
                            if (ds_point_delete.Tables[0].Rows[counter_point]["IDNUMBER_insert"].ToString() == ViewState["idnumber"].ToString() &&
                                ds_point_delete.Tables[0].Rows[counter_point]["CalibrationPoint_insert"].ToString() == ViewState["point_cal"].ToString())
                            {
                                ds_point_delete.Tables[0].Rows[counter_point].Delete();
                                break;
                            }
                        }

                        var ds_key_point_delete = (DataSet)ViewState["vsDataCalibrationPoint"];

                        for (int counter_keypoint = 0; counter_keypoint < ds_key_point_delete.Tables[0].Rows.Count; counter_keypoint++)
                        {
                            if (ds_key_point_delete.Tables[0].Rows[counter_keypoint]["IDNUMBER"].ToString() == ViewState["idnumber"].ToString() &&
                                ds_key_point_delete.Tables[0].Rows[counter_keypoint]["CalibrationPoint"].ToString() == ViewState["point_cal"].ToString())
                            {
                                ds_key_point_delete.Tables[0].Rows[counter_keypoint].Delete();
                                break;
                            }
                        }
                        setGridData(gvCalPointInsert, (DataSet)ViewState["vsDataCalibrationPoint_insert"]);
                        setGridData(gvCalibrationPoint, (DataSet)ViewState["vsDataCalibrationPoint"]);

                        break;
                    case 1:

                        var ds_point_delete_list = (DataSet)ViewState["vsDataCalibrationPoint_insert"];
                        ViewState["idnumber_list"] = idnumber;
                        // :: for loop multiple delete cal set point in gvCalPointInsert :: 
                        for (int counter_point_in_doc = gvCalPointInsert.Rows.Count - 1; counter_point_in_doc >= 0; counter_point_in_doc--)
                            if (ds_point_delete_list.Tables[0].Rows[counter_point_in_doc]["IDNUMBER_insert"].ToString() == ViewState["idnumber_list"].ToString())
                                ds_point_delete_list.Tables[0].Rows.RemoveAt(counter_point_in_doc);

                        setGridData(gvCalPointInsert, (DataSet)ViewState["vsDataCalibrationPoint_insert"]);

                        // :: delete one only list equipment new in gvListEquipmentCalNew ::
                        var ds_list_equipment_cal = (DataSet)ViewState["vsDataEquipmentCalNew"];
                        for (int counter_cal_equipment = 0; counter_cal_equipment < ds_list_equipment_cal.Tables[0].Rows.Count; counter_cal_equipment++)
                        {
                            if (ds_list_equipment_cal.Tables[0].Rows[counter_cal_equipment]["IDCodeNumber_New"].ToString() == ViewState["idnumber_list"].ToString())
                            {
                                ds_list_equipment_cal.Tables[0].Rows[counter_cal_equipment].Delete();
                                break;
                            }
                        }
                        setGridData(gvListEquipmentCalNew, (DataSet)ViewState["vsDataEquipmentCalNew"]);

                        break;
                }


                break;

            case "cmdView":

                string[] cmdArgView = cmdArg.Split(',');
                int _u0CalDocument = int.TryParse(cmdArgView[0].ToString(), out _default_int) ? int.Parse(cmdArgView[0].ToString()) : _default_int;
                int _typeDocument = int.TryParse(cmdArgView[1].ToString(), out _default_int) ? int.Parse(cmdArgView[1].ToString()) : _default_int;
                int _typeEquipment = int.TryParse(cmdArgView[2].ToString(), out _default_int) ? int.Parse(cmdArgView[2].ToString()) : _default_int;
                int _certificate = int.TryParse(cmdArgView[3].ToString(), out _default_int) ? int.Parse(cmdArgView[3].ToString()) : _default_int;
                int _u1CalDocument = int.TryParse(cmdArgView[4].ToString(), out _default_int) ? int.Parse(cmdArgView[4].ToString()) : _default_int;
                int _statusDocument = int.TryParse(cmdArgView[5].ToString(), out _default_int) ? int.Parse(cmdArgView[5].ToString()) : _default_int;
                int _placeDocument = int.TryParse(cmdArgView[6].ToString(), out _default_int) ? int.Parse(cmdArgView[6].ToString()) : _default_int;
                int _nodeDocument = int.TryParse(cmdArgView[7].ToString(), out _default_int) ? int.Parse(cmdArgView[7].ToString()) : _default_int;

                switch (_typeDocument)
                {
                    case 1: //ประเภทเอกสารหลัก
                        setActiveTab("docCreate", _u0CalDocument, 0, 0, 0, 0, 0, 0);
                        break;
                    case 2: // ประเภทรายการของ แต่ละเครื่องมือที่สอบเทียบ
                        setActiveTab("docCreate", _u0CalDocument, _u1CalDocument, _statusDocument, _placeDocument, _typeDocument, _certificate, _nodeDocument);
                        break;
                }
                break;

            case "cmdDocCancel":
                int back_conddition = int.Parse(cmdArg);

                switch (back_conddition)
                {
                    case 0:
                        setActiveTab(setActive.Text, 0, 0, 0, 0, 0, 0, 0);
                        setOntop.Focus();
                        break;
                }



                break;

            case "cmdDocSave":

                var gvCalibrationList = (GridView)fvCalDocumentList.FindControl("gvCalibrationList");

                foreach (GridViewRow gr in gvCalibrationList.Rows)
                {
                    Label staidx = (Label)gr.FindControl("lblstaidx");
                    DropDownList ddlChooseLabType = (DropDownList)gr.FindControl("ddlChooseLabType");
                    DropDownList ddlChooseLocation = (DropDownList)gr.FindControl("ddlChooseLocation");
                    TextBox txtCommentBeforeExtanalLab = (TextBox)gr.FindControl("txtCommentBeforeExtanalLab");

                    switch (staidx.Text)
                    {
                        case "3":

                            data_qa_cims _dtqaRecordLabCal = new data_qa_cims();
                            _dtqaRecordLabCal.qa_cims_u1_calibration_document_list = new qa_cims_u1_calibration_document_details[1];

                            var _u1doc_node_select_lab = new qa_cims_u1_calibration_document_details[gvCalibrationList.Rows.Count];
                            int count_u1doc = 0;

                            Label lblU1DocCal = (Label)gr.FindControl("lblU1DocCal");
                            Label lblm0NodeIDX = (Label)gr.FindControl("lblm0NodeIDX");
                            Label lblm0DeviveIDX = (Label)gr.FindControl("lblm0DeviveIDX");

                            _u1doc_node_select_lab[count_u1doc] = new qa_cims_u1_calibration_document_details();
                            _u1doc_node_select_lab[count_u1doc].u1_cal_idx = int.Parse(lblU1DocCal.Text);
                            _u1doc_node_select_lab[count_u1doc].decision_idx = int.Parse(ddlChooseLabType.SelectedValue);
                            _u1doc_node_select_lab[count_u1doc].m0_lab_idx = int.Parse(ddlChooseLocation.SelectedValue);
                            _u1doc_node_select_lab[count_u1doc].m0_node_idx = int.Parse(lblm0NodeIDX.Text);
                            _u1doc_node_select_lab[count_u1doc].cemp_idx = _emp_idx;
                            _u1doc_node_select_lab[count_u1doc].comment = txtCommentBeforeExtanalLab.Text;
                            _u1doc_node_select_lab[count_u1doc].m0_device_idx = int.Parse(lblm0DeviveIDX.Text);

                            count_u1doc++;

                            _dtqaRecordLabCal.qa_cims_u1_calibration_document_list = _u1doc_node_select_lab;
                            _dtqaRecordLabCal = callServicePostQaDocCIMS(_urlCimsSetLabCalibration, _dtqaRecordLabCal);

                            if (_dtqaRecordLabCal.return_code == 0)
                            {
                                setActiveTab("docDetailList", 0, 0, 0, 0, 0, 0, 0);
                                setOntop.Focus();
                            }

                            break;

                        case "8":

                            break;
                    }



                }


                break;

            case "cmdDocSaveInLab":

                string[] cmdArgInLab = cmdArg.Split(',');
                int _u0CalDocInLab = int.TryParse(cmdArgInLab[0].ToString(), out _default_int) ? int.Parse(cmdArgInLab[0].ToString()) : _default_int;
                int _u1CalDocInLab = int.TryParse(cmdArgInLab[1].ToString(), out _default_int) ? int.Parse(cmdArgInLab[1].ToString()) : _default_int;
                int _DecisionCalDocInLab = int.TryParse(cmdArgInLab[2].ToString(), out _default_int) ? int.Parse(cmdArgInLab[2].ToString()) : _default_int;
                int _M0NodeInLab = int.TryParse(cmdArgInLab[3].ToString(), out _default_int) ? int.Parse(cmdArgInLab[3].ToString()) : _default_int;

                data_qa_cims _dataAcceptInLab = new data_qa_cims();

                _dataAcceptInLab.qa_cims_u1_calibration_document_list = new qa_cims_u1_calibration_document_details[1];
                qa_cims_u1_calibration_document_details _acceptInLab = new qa_cims_u1_calibration_document_details();
                ////update u1
                _acceptInLab.cemp_idx = _emp_idx;
                _acceptInLab.u0_cal_idx = _u0CalDocInLab;
                _acceptInLab.u1_cal_idx = _u1CalDocInLab;
                _acceptInLab.decision_idx = _DecisionCalDocInLab;
                _acceptInLab.m0_node_idx = _M0NodeInLab;
                _dataAcceptInLab.qa_cims_u1_calibration_document_list[0] = _acceptInLab;

                _dataAcceptInLab = callServicePostQaDocCIMS(_urlCimsSetAcceptCalibrationInLab, _dataAcceptInLab);
                litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataAcceptInLab));
                if (_dataAcceptInLab.return_code == 0)
                {
                    litDebug.Text = _dataAcceptInLab.return_msg.ToString();
                    setGridData(gvCalibrationInLab, ViewState["vsCalibrationListInLab"]);
                    setActiveTab("docLab", 0, 0, 0, 0, 0, 0, 0);
                    setOntop.Focus();

                }



                break;

            case "btnSearchResult":
                getRecordCalResults((TextBox)fvRecordCalResults.FindControl("txtSearchEquipmentIDNo"));
                break;


            case "cmdSaveResult":

                var nameGridview = (Label)docRecordCalibration.FindControl("lbnameGridview");
                var u1DocCal = (TextBox)fvPassValue.FindControl("txtu1DocCal");
                var u0DocCal = (TextBox)fvPassValue.FindControl("txtu0DocCal");
                var m0Deviceidx = (TextBox)fvPassValue.FindControl("txtM0Deviceidx");
                var m0Nodeidx = (TextBox)fvPassValue.FindControl("txtM0Nodeidx");
                var m0Actoridx = (TextBox)fvPassValue.FindControl("txtM0Actoridx");
                var Decisionidx = (Label)docRecordCalibration.FindControl("lbDecisionRecordResult");

                switch (nameGridview.Text)
                {
                    case "GvSalt":

                        data_qa_cims _data_qa_salt = new data_qa_cims();
                        _data_qa_salt.qa_cims_record_topics_list = new qa_cims_record_topics_details[1];
                        _data_qa_salt.qa_cims_u2_calibration_document_list = new qa_cims_u2_calibration_document_details[1];

                        qa_cims_record_topics_details _topics_salt = new qa_cims_record_topics_details();
                        qa_cims_u2_calibration_document_details _u2doc_log_salt = new qa_cims_u2_calibration_document_details();

                        _topics_salt.u1_val1 = tb_u1_val1_salt.Text;
                        _topics_salt.u1_val2 = tb_u1_val2_salt.Text;
                        _topics_salt.u1_val3 = tb_u1_val3_salt.Text;
                        _topics_salt.u1_val4 = tb_u1_val4_salt.Text;
                        _topics_salt.condition = 1;
                        _topics_salt.u1_cal_idx = int.Parse(u1DocCal.Text);
                        _topics_salt.u0_cal_idx = int.Parse(u0DocCal.Text);
                        _topics_salt.m0_device_idx = int.Parse(m0Deviceidx.Text);
                        _topics_salt.m0_node_idx = int.Parse(m0Nodeidx.Text);
                        _topics_salt.m0_actor_idx = int.Parse(m0Actoridx.Text);
                        _topics_salt.certificate = 1;
                        _topics_salt.decision_idx = int.Parse(Decisionidx.Text);

                        _u2doc_log_salt.u1_cal_idx = int.Parse(u1DocCal.Text);
                        _u2doc_log_salt.u0_cal_idx = int.Parse(u0DocCal.Text);
                        _u2doc_log_salt.m0_node_idx = int.Parse(m0Nodeidx.Text);
                        _u2doc_log_salt.m0_actor_idx = int.Parse(m0Actoridx.Text);
                        _u2doc_log_salt.cemp_idx = _emp_idx;
                        _u2doc_log_salt.decision_idx = int.Parse(Decisionidx.Text);

                        var _u3result_salt = new qa_cims_record_result_by_form_details[GvSalt.Rows.Count];
                        int count_u3result_salt = 0;

                        foreach (GridViewRow grSalt in GvSalt.Rows)
                        {
                            var _result_salt_val1 = (TextBox)grSalt.FindControl("tb_result_salt_val1");
                            var _result_salt_val2 = (TextBox)grSalt.FindControl("tb_result_salt_val2");
                            var _result_salt_val3 = (TextBox)grSalt.FindControl("tb_result_salt_val3");
                            var _result_salt_val4 = (TextBox)grSalt.FindControl("tb_result_salt_val4");
                            var _result_salt_val5 = (TextBox)grSalt.FindControl("tb_result_salt_val5");
                            var _result_salt_val6 = (TextBox)grSalt.FindControl("tb_result_salt_val6");
                            var _result_salt_val7 = (RadioButtonList)grSalt.FindControl("rd_result_salt_val7");
                            var _result_salt_val8 = (TextBox)grSalt.FindControl("tb_result_salt_val8");

                            _u3result_salt[count_u3result_salt] = new qa_cims_record_result_by_form_details();
                            _u3result_salt[count_u3result_salt].val1 = _result_salt_val1.Text;
                            _u3result_salt[count_u3result_salt].val2 = _result_salt_val2.Text;
                            _u3result_salt[count_u3result_salt].val3 = _result_salt_val3.Text;
                            _u3result_salt[count_u3result_salt].val4 = _result_salt_val4.Text;
                            _u3result_salt[count_u3result_salt].val5 = _result_salt_val5.Text;
                            _u3result_salt[count_u3result_salt].val6 = _result_salt_val6.Text;
                            _u3result_salt[count_u3result_salt].val7 = _result_salt_val7.SelectedValue;
                            _u3result_salt[count_u3result_salt].val8 = _result_salt_val8.Text;
                            _u3result_salt[count_u3result_salt].cemp_idx = _emp_idx;
                            _u3result_salt[count_u3result_salt].u1_cal_idx = int.Parse(u1DocCal.Text);

                            count_u3result_salt++;
                        }

                        _data_qa_salt.qa_cims_record_topics_list[0] = _topics_salt;
                        _data_qa_salt.qa_cims_u2_calibration_document_list[0] = _u2doc_log_salt;
                        _data_qa_salt.qa_cims_record_result_by_form_list = _u3result_salt;

                        _data_qa_salt = callServicePostQaDocCIMS(_urlCimsSetRecordCalibration, _data_qa_salt);

                        if (_data_qa_salt.return_code == 0)
                        {
                            setActiveTab("docRecordCalibration", 0, 0, 0, 0, 0, 0, 0);
                            setOntop.Focus();
                        }

                        setActiveTab("docRecordCalibration", 0, 0, 0, 0, 0, 0, 0);
                        setOntop.Focus();
                    

                        break;
                }

                break;

            case "cmdCancelResult":
                setActiveTab("docRecordCalibration", 0, 0, 0, 0, 0, 0, 0);
                setOntop.Focus();
               
                break;

            case "cmdCancelSupervisor":
                setActiveTab("docSupervisor", 0, 0, 0, 0, 0, 0, 0);
                setOntop.Focus();
                break;

            case "cmdApproveSupervisor":

                var _setApprove = (RadioButtonList)fvSupervisorApprove.FindControl("rd_supervisor_approve");
                var _comment = (TextBox)fvSupervisorApprove.FindControl("txtComment");
                var _nodeidxSupervisor = (Label)fvCalibrationResult.FindControl("lblM0NodeIdxView");
                var _actorSupervisor = (Label)fvCalibrationResult.FindControl("lblM0ActorIdxView");
                var _U1DocSupervisor = (Label)fvCalibrationResult.FindControl("lblU1DocIdxView");
                
                data_qa_cims _data_supervisor_approve = new data_qa_cims();
                _data_supervisor_approve.qa_cims_u1_calibration_document_list = new qa_cims_u1_calibration_document_details[1];
                _data_supervisor_approve.qa_cims_u2_calibration_document_list = new qa_cims_u2_calibration_document_details[1];

                qa_cims_u1_calibration_document_details _u1Supervisor = new qa_cims_u1_calibration_document_details();
                qa_cims_u2_calibration_document_details _u2Supervisor = new qa_cims_u2_calibration_document_details();

                _u1Supervisor.cemp_idx = _emp_idx;
                _u1Supervisor.u1_cal_idx = int.Parse(_U1DocSupervisor.Text);
                _u1Supervisor.m0_actor_idx = int.Parse(_actorSupervisor.Text);
                _u1Supervisor.m0_node_idx = int.Parse(_nodeidxSupervisor.Text);
                _u1Supervisor.decision_idx = int.Parse(_setApprove.SelectedValue);

                _u2Supervisor.cemp_idx = _emp_idx;
                _u2Supervisor.comment = _comment.Text;
                _u2Supervisor.u1_cal_idx = int.Parse(_U1DocSupervisor.Text);
                _u2Supervisor.m0_actor_idx = int.Parse(_actorSupervisor.Text);
                _u2Supervisor.m0_node_idx = int.Parse(_nodeidxSupervisor.Text);
                _u2Supervisor.decision_idx = int.Parse(_setApprove.SelectedValue);
               
                _data_supervisor_approve.qa_cims_u1_calibration_document_list[0] = _u1Supervisor;
                _data_supervisor_approve.qa_cims_u2_calibration_document_list[0] = _u2Supervisor;

                _data_supervisor_approve = callServicePostQaDocCIMS(_urlCimsSetApproveSupervisor, _data_supervisor_approve);

                if (_data_supervisor_approve.return_code == 0)
                {
                    setActiveTab("docSupervisor", 0, 0, 0, 0, 0, 0, 0);
                    setOntop.Focus();
                }

                break;

            case "cmdSaveCertificate":

                var _M0NodeIDXCertificate = (TextBox)fvRecordCertificate.FindControl("tbM0NodeIDXCer");
                var _M0ActorIDXCertificate = (TextBox)fvRecordCertificate.FindControl("tbM0ActorIDXCer");
                var _U1DocCalIDXCertificate = (TextBox)fvRecordCertificate.FindControl("tbU1CalIDXCer");
                var _U0DocCalIDXCertificate = (TextBox)fvRecordCertificate.FindControl("tbU0CalIDXCer");
                var _IDNOEquipment = (TextBox)fvRecordCertificate.FindControl("tbIDCodeEquipmentView");
                var _DecisionCertificate = (TextBox)docCreate.FindControl("tbNodeDecisionIDX");

                data_qa_cims _data_record_certificate = new data_qa_cims();
                _data_record_certificate.qa_cims_u1_calibration_document_list = new qa_cims_u1_calibration_document_details[1];
                qa_cims_u1_calibration_document_details _record_certificate = new qa_cims_u1_calibration_document_details();

                _record_certificate.cemp_idx = _emp_idx;
                _record_certificate.u1_cal_idx = int.Parse(_U1DocCalIDXCertificate.Text);
                _record_certificate.decision_idx = int.Parse(_DecisionCertificate.Text);
                _record_certificate.m0_actor_idx = int.Parse(_M0ActorIDXCertificate.Text);
                _record_certificate.m0_node_idx = int.Parse(_M0NodeIDXCertificate.Text);

                _data_record_certificate.qa_cims_u1_calibration_document_list[0] = _record_certificate;

                 //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_record_certificate));

                _data_record_certificate = callServicePostQaDocCIMS(_urlCimsSetCertificate, _data_record_certificate);

                if (_data_record_certificate.return_code == 0)
                {
                    var UploadCertificate = (FileUpload)fvRecordCertificate.FindControl("UploadCertificate");

                    if (UploadCertificate.HasFile)
                    {
                        string getPathfile = ConfigurationManager.AppSettings["path_file_certificate"];
                        string certificate = "Cer." + _IDNOEquipment.Text;
                        string fileName_upload = certificate;
                        string filePath_upload = Server.MapPath(getPathfile + certificate);

                        if (!Directory.Exists(filePath_upload))
                        {
                            Directory.CreateDirectory(filePath_upload);
                        }
                        string extension = Path.GetExtension(UploadCertificate.FileName);

                        UploadCertificate.SaveAs(Server.MapPath(getPathfile + certificate) + "\\" + fileName_upload + extension);

                    }

                    else
                    {

                    }

                    setActiveTab("docCreate", int.Parse(_U0DocCalIDXCertificate.Text), 0, 0, 0, 0, 0, 0);
                    setOntop.Focus();
                }


                break;
            case "cmdInsertRowGvSalt":

                AddNewRowToGrid();

                break;
        }

    }
    #endregion

    #region NewRow Gridview   

    private void SetInitialRow()
    {
        DataTable dt = new DataTable();
        DataRow dr = null;
        dt.Columns.Add(new DataColumn("RowNumber", typeof(string)));
        dt.Columns.Add(new DataColumn("Column1", typeof(string)));
        dt.Columns.Add(new DataColumn("Column2", typeof(string)));
        dt.Columns.Add(new DataColumn("Column3", typeof(string)));
        dr = dt.NewRow();
        dr["RowNumber"] = 1;
        dr["Column1"] = string.Empty;
        dr["Column2"] = string.Empty;
        dr["Column3"] = string.Empty;
        dt.Rows.Add(dr);

        //Store the DataTable in ViewState
        ViewState["CurrentTable"] = dt;

        GvSalt.DataSource = dt;
        GvSalt.DataBind();
    }


    private void AddNewRowToGrid()
    {
        int rowIndex = 0;

        if (ViewState["CurrentTable"] != null)
        {
            DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
            DataRow drCurrentRow = null;

            litDebug.Text ="5555";
            if (dtCurrentTable.Rows.Count > 0)
            {
                for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                {
                    //extract the TextBox values
                    TextBox box1 = (TextBox)GvSalt.Rows[rowIndex].Cells[1].FindControl("tb_result_salt_val1");
                    TextBox box2 = (TextBox)GvSalt.Rows[rowIndex].Cells[2].FindControl("tb_result_salt_val2");
                    TextBox box3 = (TextBox)GvSalt.Rows[rowIndex].Cells[3].FindControl("tb_result_salt_val3");

                    drCurrentRow = dtCurrentTable.NewRow();
                    drCurrentRow["RowNumber"] = i + 1;

                    dtCurrentTable.Rows[i - 1]["Column1"] = box1.Text;
                    dtCurrentTable.Rows[i - 1]["Column2"] = box2.Text;
                    dtCurrentTable.Rows[i - 1]["Column3"] = box3.Text;

                    rowIndex++;
                }
                dtCurrentTable.Rows.Add(drCurrentRow);
                ViewState["CurrentTable"] = dtCurrentTable;

                GvSalt.DataSource = dtCurrentTable;
                GvSalt.DataBind();
            }
        }
        else
        {
            Response.Write("ViewState is null");
        }

        //Set Previous Data on Postbacks
        SetPreviousData();
    }

    private void SetPreviousData()
    {
        int rowIndex = 0;
        if (ViewState["CurrentTable"] != null)
        {
            DataTable dt = (DataTable)ViewState["CurrentTable"];
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                   
                    TextBox box1 = (TextBox)GvSalt.Rows[rowIndex].Cells[1].FindControl("tb_result_salt_val1");
                    TextBox box2 = (TextBox)GvSalt.Rows[rowIndex].Cells[2].FindControl("tb_result_salt_val2");
                    TextBox box3 = (TextBox)GvSalt.Rows[rowIndex].Cells[3].FindControl("tb_result_salt_val3");


                    box1.Text = dt.Rows[i]["Column1"].ToString();
                    box2.Text = dt.Rows[i]["Column2"].ToString();
                    box3.Text = dt.Rows[i]["Column3"].ToString();

                    rowIndex++;
                }
            }
        }
    }


    #endregion

    #region event textbox

    private void ClearTextBoxes(Panel namepanel)
    {
  
        foreach (Control cntrlsalt in namepanel.Controls)
        {
            if (cntrlsalt is TextBox)
            {
                ((TextBox)cntrlsalt).Text = "";

            }
        }

    }

    protected void TextBoxChanged(object sender, EventArgs e)
    {
        var txtName = (TextBox)sender;

        switch (txtName.ID)
        {
            case "tbsearchingToolID":

                TextBox tbsearchingToolID_txtChanged = (TextBox)pnEquipmentOld.FindControl("tbsearchingToolID");

                var ds_Check_id_equipment = (DataSet)ViewState["vsDataEquipmentCalOld"];
                var dr_Check_id_equipment = ds_Check_id_equipment.Tables[0].NewRow();

                int numRowId = ds_Check_id_equipment.Tables[0].Rows.Count; //จำนวนแถว

                ViewState["_id_equipment_checkDuplicate"] = tbsearchingToolID_txtChanged.Text;

                if (numRowId > 0)
                {
                    foreach (DataRow checkId in ds_Check_id_equipment.Tables[0].Rows)
                    {

                        ViewState["checkIdEquipment"] = checkId["IDCodeNumber"];

                        if (ViewState["_id_equipment_checkDuplicate"].ToString() == ViewState["checkIdEquipment"].ToString())
                        {
                            ViewState["CheckDataset_id_equipment"] = "1";
                            break;

                        }
                        else
                        {
                            ViewState["CheckDataset_id_equipment"] = "0";

                        }
                    }
                    if (ViewState["CheckDataset_id_equipment"].ToString() == "1")
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('คุณได้ทำการเพิ่มเลข ID เครื่องมือ นี้แล้ว กรุณากรอกข้อมูลเลือกใหม่อีกครั้ง !');", true);
                        tbsearchingToolID.Text = string.Empty;
                        setFormData(fvShowDetailsSearchTool, FormViewMode.ReadOnly, null);
                    }
                    else if (ViewState["CheckDataset_id_equipment"].ToString() == "0")
                    {
                        ViewState["CheckDataset_id_equipment"] = "0";
                        
                        getToolIDList((TextBox)pnEquipmentOld.FindControl("tbsearchingToolID"), ViewState["CheckDataset_id_equipment"].ToString());
                    }
                }

                else if (numRowId == 0)
                {
                    ViewState["CheckDataset_id_equipment"] = "0";
                    getToolIDList((TextBox)pnEquipmentOld.FindControl("tbsearchingToolID"), ViewState["CheckDataset_id_equipment"].ToString());
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณากรอกเลข ID เครื่องมือ.!');", true);
                }
                
            break;

        }
    }
    #endregion

    #region callService 
    protected data_qa_cims callServicePostMasterQACIMS(string _cmdUrl, data_qa_cims _data_qa_cims)
    {
        _localJson = _funcTool.convertObjectToJson(_data_qa_cims);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_qa_cims = (data_qa_cims)_funcTool.convertJsonToObject(typeof(data_qa_cims), _localJson);


        return _data_qa_cims;
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_qa_cims callServicePostQaDocCIMS(string _cmdUrl, data_qa_cims _data_qa_cims)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_qa_cims);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_qa_cims = (data_qa_cims)_funcTool.convertJsonToObject(typeof(data_qa_cims), _localJson);

        return _data_qa_cims;
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger = new PostBackTrigger();
        trigger.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger);
    }

    #endregion callService Functions
}