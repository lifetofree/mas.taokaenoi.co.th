﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="qa_cims_m0_equipment_result.aspx.cs" Inherits="websystem_MasterData_qa_cims_m0_equipment_result" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:Literal ID="test_place" runat="server"></asp:Literal>
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
        <asp:View ID="View1" runat="server">

            <div class="form-group">
                <asp:LinkButton ID="btn_addequipment_result" CssClass="btn btn-primary" Visible="true" data-original-title="เพิ่มผลการตรวจสอบเครื่องมือ" data-toggle="tooltip" title="เพิ่มผลการตรวจสอบเครื่องมือ" runat="server"
                    CommandName="cmdAddEquipment_result" OnCommand="btnCommand"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;เพิ่มผลการตรวจสอบเครื่องมือ</asp:LinkButton>
            </div>

            <asp:FormView ID="Fv_Insert_Result" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">เพิ่มผลการตรวจสอบเครื่องมือ</h3>

                        </div>
                        <div class="panel-body">
                            <div class="panel-heading">

                                <div class="form-horizontal" role="form">

                                    <div class="panel-heading">

                                        <div class="form-group">
                                            <asp:Label ID="lblequipment_result" runat="server" Text="ผลการตรวจสอบเครื่องมือ" CssClass="col-sm-3 control-label"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtequipment_result" runat="server" CssClass="form-control" placeholder="กรอกผลการตรวจสอบเครื่องมือ ..." Enabled="true" />
                                                <asp:RequiredFieldValidator ID="Re_equipment_result" runat="server"
                                                    ControlToValidate="txtequipment_result" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกผลการตรวจสอบเครื่องมือ" ValidationGroup="Save" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="aj_eqpresult" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_equipment_result" Width="220" />
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="lblequipment_result_status" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="ddEquipment_result" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="1" Text="Online" />
                                                    <asp:ListItem Value="0" Text="Offline" />
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-2 col-sm-offset-3">
                                                <asp:LinkButton ID="Lbtn_submit_equipment_result" ValidationGroup="Save" CssClass="btn btn-success" Text="Save" data-toggle="tooltip" title="บันทึก" runat="server" CommandName="Lbtn_submit_equipment_result" OnCommand="btnCommand"></asp:LinkButton>
                                                <asp:LinkButton ID="Lbtn_cancel_equipment_result" CssClass="btn btn-danger" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="Lbtn_cancel_equipment_result" data-toggle="tooltip" title="ยกเลิก"></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <%--select Place--%>
            <asp:GridView ID="Gv_select_equipment_result" runat="server" Visible="true"
                AutoGenerateColumns="false"
                CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                HeaderStyle-CssClass="success"
                AllowPaging="true"
                PageSize="10"
                OnRowDataBound="Master_RowDataBound"
                OnRowUpdating="Master_RowUpdating"
                OnRowEditing="Master_RowEditing"
                OnRowCancelingEdit="Master_RowCancelingEdit"
                OnPageIndexChanging="Master_PageIndexChanging"
                AutoPostBack="false">

                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                <EmptyDataTemplate>
                    <div style="text-align: center; color: red"><b>ไม่มีข้อมูลผลการตรวจสอบเครื่องมือ</b> </div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style="text-align: center; padding-top: 5px;">
                                <asp:Label ID="equipment_result_idx" runat="server" Visible="false" Text='<%# Eval("equipment_result_idx") %>' />
                                <%# (Container.DataItemIndex +1) %>
                            </div>
                        </ItemTemplate>
                        <EditItemTemplate>

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="ID_equipment_result" Visible="false" runat="server" CssClass="form-control " Text='<%# Eval("equipment_result_idx") %>'></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="lbupdates_equipment_result" CssClass="col-sm-3 control-label" runat="server" Text="ผลการตรวจสอบเครื่องมือ" />
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="Result_equipment_result" runat="server" CssClass="form-control " Text='<%# Eval("equipment_result") %>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorequipment_result" runat="server"
                                                ControlToValidate="Result_equipment_result" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกผลการตรวจสอบเครื่องมือ" ValidationGroup="Editeqpresult" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender_eqpresult" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidatorequipment_result" Width="220" />
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <asp:Label ID="lbupdates_cal_type" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddEdit_equipment_result" Text='<%# Eval("equipment_result_status") %>'
                                                CssClass="form-control" runat="server">
                                                <asp:ListItem Value="1">Online</asp:ListItem>
                                                <asp:ListItem Value="0">Offline</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-3"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-9">
                                            <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" ValidationGroup="Editeqpresult" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                Text="Save" CommandName="Update"></asp:LinkButton>
                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                        </EditItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ผลการตรวจสอบเครื่องมือ" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lbequipment_result" runat="server"
                                        Text='<%# Eval("equipment_result") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lbpequipment_result_status" Visible="false" runat="server"
                                        CssClass="col-sm-12" Text='<%# Eval("equipment_result_status") %>'></asp:Label>
                                    <asp:Label ID="equipment_result_statusOnline" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="Online"
                                        CssClass="col-sm-12">
                                                <div style="text-align: center; color:green;">
                                                    <span><b>Online</b></span>
                                    </asp:Label>
                                    <asp:Label ID="equipment_result_statusOffline" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="Offline" CssClass="col-sm-12">
                                                <div style="text-align: center; color:red;">
                                                <span><b>Offline</b></span>
                                    </asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="จัดการข้อมูล" HeaderStyle-Width="15%" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style="padding-top: 5px">
                                <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                    data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                                <asp:LinkButton ID="btnTodelete" CssClass="text-trash" runat="server" CommandName="btnTodelete_equipment_result"
                                    data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบผลการตรวจสอบเครื่องมือนี้ใช่หรือไม่ ?')"
                                    CommandArgument='<%#Eval("equipment_result_idx") %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:View>
    </asp:MultiView>


</asp:Content>
