﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="ad-permission-policy.aspx.cs" Inherits="websystem_adonline_permission_type" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
   <asp:Literal ID="litDebug" runat="server" />
   <!-- Start Gridview & Update Form -->
   <div id="divIndex" runat="server">
      <div class="col-md-12 m-b-10 text-right">
         <asp:LinkButton ID="btnToInsert" CssClass="btn btn-success f-s-14" runat="server" CommandName="btnToInsert" OnCommand="btnCommand" Text="สร้าง" />
      </div>
      <div class="col-md-12">
         <asp:GridView ID="gvPermPol"
            runat="server"
            AutoGenerateColumns="false"
            DataKeyNames="m0_perm_pol_idx"
            CssClass="table table-striped table-bordered table-responsive col-md-12"
            HeaderStyle-CssClass="info"
            AllowPaging="true"
            PageSize="10"
            OnRowEditing="Master_RowEditing"
            OnRowUpdating="Master_RowUpdating"
            OnRowCancelingEdit="Master_RowCancelingEdit"
            OnPageIndexChanging="Master_PageIndexChanging"
            OnRowDataBound="Master_RowDataBound">
            <PagerStyle CssClass="pageCustom" />
            <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
            <EmptyDataTemplate>
               <div class="text-center">No result</div>
            </EmptyDataTemplate>
            <Columns>
               <asp:TemplateField HeaderText="#" HeaderStyle-CssClass="text-center"
                  HeaderStyle-Font-Size="Small" ItemStyle-CssClass="text-center" HeaderStyle-Width="10%">
                  <ItemTemplate>
                     <%# (Container.DataItemIndex + 1) %>
                  </ItemTemplate>
                  <EditItemTemplate>
                     <asp:TextBox ID="m0_perm_pol_idx" runat="server" CssClass="form-control" Visible="False" Text='<%# Eval("m0_perm_pol_idx")%>' />
                     <div class="col-md-12">
                        <div class="form-group">
                           <label class="f-s-13">ชื่อระดับสิทธิ์</label>
                           <asp:TextBox ID="txtPermPolNameUpdate" runat="server" CssClass="form-control" Text='<%# Eval("perm_pol_name") %>'
                              placeholder="ชื่อระดับสิทธิ์..."/>
                        </div>
                     </div>
                     <div class="col-md-12">
                        <div class="form-group">
                           <label class="f-s-13">รายละเอียดข้อจำกัด</label>
                           <asp:TextBox ID="txtPermPolDescUpdate" runat="server" CssClass="form-control multiline-no-resize" Text='<%# Eval("perm_pol_desc") %>' TextMode="MultiLine" Rows="7" placeholder="รายละเอียดข้อจำกัด..." />
                        </div>
                     </div>
                     <div class="col-md-12">
                        <div class="form-group">
                           <label class="f-s-13">สถานะ</label>
                           <asp:DropDownList ID="ddlPermPolStatusUpdate" runat="server"
                              CssClass="form-control" SelectedValue='<%# Eval("perm_pol_status") %>'>
                              <asp:ListItem Value="1" Text="Online"></asp:ListItem>
                              <asp:ListItem Value="0" Text="Offline"></asp:ListItem>
                           </asp:DropDownList>
                        </div>
                     </div>
                     <div class="col-md-12">
                        <div class="pull-left">
                           <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success f-s-14" runat="server"
                              ValidationGroup="savePermPolUpdate" CommandName="Update"
                              OnClientClick="return confirm('คุณมั่นใจหรือไม่ว่าต้องการบันทึกการเปลี่ยนแปลง ?')">
                              บันทึกการเปลี่ยนแปลง
                           </asp:LinkButton>
                           <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger f-s-14" runat="server" CommandName="Cancel">
                              ยกเลิก
                           </asp:LinkButton>
                        </div>
                     </div>
                  </EditItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="ชื่อระดับสิทธิ์" ItemStyle-HorizontalAlign="Left"
                  HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                  <ItemTemplate>
                     <small>
                        <asp:Label ID="permPolName" runat="server" Text='<%# Eval("perm_pol_name") %>' />
                     </small>
                  </ItemTemplate>
                  <EditItemTemplate />
                  <FooterTemplate />
               </asp:TemplateField>
               <asp:TemplateField HeaderText="รายละเอียดข้อจำกัด" ItemStyle-HorizontalAlign="Left"
                  HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="35%">
                  <ItemTemplate>
                     <small>
                        <asp:Label ID="permPolDesc" runat="server" Text='<%# nl2br((string)Eval("perm_pol_desc")) %>' />
                     </small>
                  </ItemTemplate>
                  <EditItemTemplate />
                  <FooterTemplate />
               </asp:TemplateField>
               <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center"
                  HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                  <ItemTemplate>
                     <small>
                        <asp:Label ID="permPolStatus" runat="server" Text='<%# getStatus((int)Eval("perm_pol_status")) %>' />
                     </small>
                  </ItemTemplate>
                  <EditItemTemplate />
                  <FooterTemplate />
               </asp:TemplateField>
               <asp:TemplateField ItemStyle-HorizontalAlign="center"
                  HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%">
                  <ItemTemplate>
                     <asp:LinkButton ID="Edit" CssClass="btn btn-warning btn-sm" runat="server" CommandName="Edit"
                        data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></asp:LinkButton>
                     <asp:LinkButton ID="delete" CssClass="btn btn-danger btn-sm" runat="server" data-toggle="tooltip"
                        title="Remove" CommandName="btnBan" OnCommand="btnCommand"
                        CommandArgument='<%# Eval("m0_perm_pol_idx") %>'
                        OnClientClick="return confirm('Do you want to delete this item?')">
                           <i class="fa fa-trash"></i>
                     </asp:LinkButton>
                  </ItemTemplate>
                  <EditItemTemplate />
                  <FooterTemplate />
               </asp:TemplateField>
            </Columns>
         </asp:GridView>
      </div>
   </div>
   <!-- End Gridview & Update Form -->

   <!-- Start Insert Form -->
   <div id="divInsert" runat="server">
      <div class="row">
         <div class="col-md-12">
            <div class="form-group">
               <asp:LinkButton CssClass="btn btn-danger f-s-14" runat="server"
                  CommandName="btnCancel" OnCommand="btnCommand"><i class="fa fa-angle-left fa-lg"></i> ย้อนกลับ</asp:LinkButton>
            </div>
         </div>
         <div class="col-md-12">
            <div class="panel panel-info">
               <div class="panel-heading">สร้างระดับสิทธิ์</div>
               <div class="panel-body">
                  <div class="col-md-12">
                     <div class="form-group">
                        <label class="f-s-13">ชื่อระดับสิทธิ์</label>
                        <asp:TextBox ID="txtPermPolName" runat="server" CssClass="form-control"
                           placeholder="ชื่อระดับสิทธิ์..." />
                        <asp:RequiredFieldValidator ID="requiredPermPolName"
                           ValidationGroup="savePermPol" runat="server"
                           Display="None"
                           SetFocusOnError="true"
                           ControlToValidate="txtPermPolName"
                           Font-Size="12px" ForeColor="Red"
                           ErrorMessage="กรุณากรอกชื่อระดับสิทธิ์" />
                        <ajaxToolkit:ValidatorCalloutExtender ID="toolkitRequiredPermPolName" runat="Server" HighlightCssClass="validatorCalloutHighlight"
                           TargetControlID="requiredPermPolName" Width="160" PopupPosition="BottomLeft" />
                     </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-group">
                        <label class="f-s-13">รายละเอียดข้อจำกัด</label>
                        <asp:TextBox ID="txtPermPolDesc" runat="server" CssClass="form-control multiline-no-resize" TextMode="MultiLine" Rows="7"
                           placeholder="รายละเอียดข้อจำกัด..." />
                        <asp:RequiredFieldValidator ID="requiredPermPolDesc"
                           ValidationGroup="savePermPol" runat="server"
                           Display="None"
                           SetFocusOnError="true"
                           ControlToValidate="txtPermPolDesc"
                           Font-Size="12px" ForeColor="Red"
                           ErrorMessage="รายละเอียดข้อจำกัด" />
                        <ajaxToolkit:ValidatorCalloutExtender ID="toolkitRequiredPermPolDesc" runat="Server" HighlightCssClass="validatorCalloutHighlight"
                           TargetControlID="requiredPermPolDesc" Width="160" PopupPosition="BottomLeft" />
                     </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-group">
                        <label class="f-s-13">สถานะ</label>
                        <asp:DropDownList ID="ddlPermPolStatus" runat="server" CssClass="form-control">
                           <asp:ListItem Value="1" Text="Online" />
                           <asp:ListItem Value="0" Text="Offline" />
                        </asp:DropDownList>
                     </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-group">
                        <asp:LinkButton ID="btnInsert" CssClass="btn btn-success f-s-14" runat="server" CommandName="btnInsert" OnCommand="btnCommand" Text="บันทึก" ValidationGroup="savePermCate" />
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- End Insert Form -->
</asp:Content>
