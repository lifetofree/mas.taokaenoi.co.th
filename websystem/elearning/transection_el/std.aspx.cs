﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.UI.WebControls;

public partial class websystem_el_std : System.Web.UI.Page
{
    #region Init
    function_tool _funcTool = new function_tool();
    data_elearning _data_elearning = new data_elearning();
    function_dmu _func_dmu = new function_dmu();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetel_m0_training = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_m0_training"];
    static string _urlSetInsel_m0_training = _serviceUrl + ConfigurationManager.AppSettings["urlSetInsel_m0_training"];
    static string _urlSetUpdel_m0_training = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdel_m0_training"];
    static string _urlDelel_m0_training = _serviceUrl + ConfigurationManager.AppSettings["urlDelel_m0_training"];
    static string _urlGetErrorel_m0_training = _serviceUrl + ConfigurationManager.AppSettings["urlGetErrorel_m0_training"];

    string _localJson = "";
    int _tempInt = 0;

    int emp_idx = 0;
    
    
    #endregion Init

    #region Constant
    public static class Constants
   {
      public const int SELECT_ALL = 20;
      public const int SELECT_WHERE = 21;
      public const int NUMBER_NULL = -999;
   }
   #endregion Constant

   #region Page Load
   protected void Page_Load(object sender, EventArgs e)
   {
        emp_idx = int.Parse(Session["emp_idx"].ToString());
      if (!IsPostBack)
      {
            actionIndex();
      }
   }
   #endregion Page Load

   #region Action
   protected void actionIndex()
   {
      
   }
    
    #endregion Action

    #region Check Session Redirect
    protected void checkSessionRedirect()
   {
      //if (Session["sesLoginIdxBackEnd"] == null)
      //{
      //   Response.Redirect(ResolveUrl("~/login"));
      //}
   }
    #endregion Check Session Redirect

    private void Page_Init(object sender, EventArgs e)
    {
        ClearApplicationCache();
    }
    #region reuse
    public void ClearApplicationCache()
    {
        List<string> keys = new List<string>();
        // retrieve application Cache enumerator
        IDictionaryEnumerator enumerator = Cache.GetEnumerator();
        // copy all keys that currently exist in Cache
        while (enumerator.MoveNext())
        {
            keys.Add(enumerator.Key.ToString());
        }

        // delete every key from cache
        for (int i = 0; i < keys.Count; i++)
        {
            Cache.Remove(keys[i]);
        }
    }
    #endregion

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        switch (cmdName)
        {
            case "btnToInsert":

                break;
        }
    }
    #endregion btnCommand

    #region zSetMode
    public void zSetMode(int AiMode)
    {
        
        switch (AiMode)
        {
            case 0:  //insert mode
                {
                   

                }
                break;
            case 1:  //update mode
                {
                    
                }
                break;
            case 2://preview mode
                {
                    

                }
                break;
            case 3://Detail mode
                {
                    

                }
                break;
            case 4:  //insert detail mode
                {
                    

                }
                break;
        }
    }
    #endregion zSetMode

    #region setActiveTab
    protected void setActiveTab(string activeTab)
    {
        //liInsert.Attributes.Add("class", "");
       // liListData.Attributes.Add("class", "");
        switch (activeTab)
        {
            case "P":
               // liListData.Attributes.Add("class", "active");

                break;

        }
    }
    #endregion setActiveTab

    protected void btnjsonIntoxml_Click(object sender, EventArgs e)
    {
        litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertJsonToXml(txt_jsonIn.Text));

    }

}