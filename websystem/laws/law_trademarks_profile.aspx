﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="law_trademarks_profile.aspx.cs" Inherits="websystem_laws_law_trademarks_profile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:HyperLink ID="setOntop" runat="server"></asp:HyperLink>

    <!--tab menu-->
    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default" id="divBtnHead" runat="server" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-bar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="sub-navbar">
                        <a class="navbar-brand" href="#"><b>Menu</b></a>
                    </div>
                </div>

                <!--Collect the nav links, forms, and other content for toggling-->
                <div class="collapse navbar-collapse" id="menu-bar">
                    <ul class="nav navbar-nav" id="uiNav" runat="server">
                        <li id="li0" runat="server">
                            <asp:LinkButton ID="lbProfileDetail" runat="server" CommandName="cmdProfileDetail" OnCommand="navCommand" CommandArgument="docProfileDetail"> Profile Detail</asp:LinkButton>
                        </li>
                        <%--<li id="li1" runat="server">
                            <asp:LinkButton ID="lbCreate" runat="server" CommandName="cmdCreate" OnCommand="navCommand" CommandArgument="docCreate"> Craete Document</asp:LinkButton>
                        </li>--%>
                    </ul>

                    <%-- <ul class="nav navbar-nav navbar-right" runat="server">

                        <li id="_divMenuLiToDocument" runat="server">
                            <asp:HyperLink ID="_divMenuBtnToDocument" runat="server"
                                NavigateUrl="https://docs.google.com/document/d/1JhPRLQm2gVCwARkpJhTF8Qrfv8zR03-qz1ZVzIGoqJo/edit?usp=sharing" Target="_blank" Text="คู่มือการใช้งาน" />
                        </li>
                    </ul>--%>
                </div>
                <!--Collect the nav links, forms, and other content for toggling-->

            </div>
        </nav>
    </div>
    <!--tab menu-->

    <div class="col-md-12">
        <asp:Literal ID="litDebug" runat="server"></asp:Literal>

    </div>

    <!--multiview-->
    <asp:MultiView ID="mvSystem" runat="server">

        <!--View Detail-->
        <asp:View ID="docProfileDetail" runat="server">
            <div class="col-md-12">



                <!-- Form view Search Detail -->
                <asp:FormView ID="fvSearchDetail" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <InsertItemTemplate>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Search Profile Detail</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Country</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlCountrySearch" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="Req_ddlCountrySearch"
                                                runat="server" ControlToValidate="ddlCountrySearch" InitialValue="0"
                                                Display="None" SetFocusOnError="true"
                                                ErrorMessage="*Select Country"
                                                ValidationGroup="SearchDetail" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlCountrySearch" Width="200" PopupPosition="BottomLeft" />
                                        </div>

                                        <label class="col-sm-2 control-label">Tademark Name</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txt_Tademarkname_Search" runat="server" CssClass="form-control" placeholder="Tademark Name ..."></asp:TextBox>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-10">
                                            <asp:LinkButton ID="btnSearchDetail" runat="server" CssClass="btn btn-primary" data-original-title="Search" data-toggle="tooltip" ValidationGroup="SearchDetail" CommandName="cmdSearchDetail" OnCommand="btnCommand"><span class="glyphicon glyphicon-search"></span> Search</asp:LinkButton>

                                            <asp:LinkButton ID="btnResetDetail" runat="server" CssClass="btn btn-default" data-original-title="Refresh" data-toggle="tooltip" CommandName="cmdResetDetail" OnCommand="btnCommand"><span class="glyphicon glyphicon-refresh"></span> Refresh</asp:LinkButton>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </InsertItemTemplate>
                </asp:FormView>
                <!-- Form view Search Detail -->


                <div id="gvrademarkProfile_scroll" style="overflow-x: auto; width: 100%" runat="server">
                    <asp:GridView ID="gvTrademarkProfile" runat="server"
                        AutoGenerateColumns="False"
                        CssClass="table table-bordered table-striped table-responsive col-md-12 footable"
                        HeaderStyle-CssClass="success"
                        OnPageIndexChanging="gvPageIndexChanging"
                        OnRowDataBound="gvRowDataBound"
                        AllowPaging="True"
                        PageSize="10">
                        <RowStyle Font-Size="Small" />
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">--- No Data ---</div>
                        </EmptyDataTemplate>
                        <Columns>

                            <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <%# (Container.DataItemIndex + 1) %>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Country" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_trademark_profile_idx" Visible="false" runat="server" Text='<%#Eval("trademark_profile_idx") %>'></asp:Label>
                                    <asp:Label ID="lbl_country_name_en" runat="server" Text='<%#Eval("country_name_en") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Tademark Detail" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="20%">
                                <ItemTemplate>

                                    <p>
                                        <b>Name :</b>
                                        <asp:Label ID="lbl_Name" runat="server" Text='<%# Eval("Name") %>' />
                                    </p>
                                    <p>
                                        <b>Doc Language :</b>
                                        <asp:Label ID="lbl_doc_language_en" runat="server" Text='<%# Eval("doc_language_en") %>' />
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Owner Detail" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%">
                                <ItemTemplate>

                                    <p>
                                        <b>Profile Owner :</b>
                                        <asp:Label ID="lbl_Profile_Owner" runat="server" Text='<%# Eval("Profile_Owner") %>' />
                                    </p>
                                    <p>
                                        <b>Language :</b>
                                        <asp:Label ID="lbl_owner_language_en" runat="server" Text='<%# Eval("owner_language_en") %>' />
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Client Detail" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%">
                                <ItemTemplate>

                                    <p>
                                        <b>Profile Client :</b>
                                        <asp:Label ID="lbl_Profile_Client" runat="server" Text='<%# Eval("Profile_Client") %>' />
                                    </p>
                                    <p>
                                        <b>Language :</b>
                                        <asp:Label ID="lbl_client_languag_en" runat="server" Text='<%# Eval("client_languag_en") %>' />
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Translation" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%">
                                <ItemTemplate>
                                    <p>
                                        <b>Translation :</b>
                                        <asp:Label ID="lbl_Translation" runat="server" Text='<%# Eval("Translation") %>' />
                                    </p>
                                    <p>
                                        <b>Transliteration :</b>
                                        <asp:Label ID="lbl_Transliteration" runat="server" Text='<%# Eval("Transliteration") %>' />
                                    </p>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="File Detail" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%">
                                <ItemTemplate>

                                    <asp:GridView ID="gvFileTrademarkDetail"
                                        runat="server"
                                        AutoGenerateColumns="false"
                                        CssClass="table table-striped table-bordered table-responsive col-md-12"
                                        HeaderStyle-CssClass="default"
                                        AllowPaging="true"
                                        OnRowDataBound="gvRowDataBound"
                                        OnPageIndexChanging="gvPageIndexChanging"
                                        PageSize="10">
                                        <PagerStyle CssClass="pageCustom" HorizontalAlign="Left" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                        <EmptyDataTemplate>
                                            <div style="text-align: center">No result</div>
                                        </EmptyDataTemplate>
                                        <Columns>

                                            <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                                HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>

                                                    <%# (Container.DataItemIndex + 1) %>
                                                </ItemTemplate>
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="File Attach" Visible="false">
                                                <ItemTemplate>
                                                    <%--<div class="col-lg-10">--%>
                                                    <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />
                                                    <%--  </div>--%>
                                                    <%--    <div class="col-lg-2">
                                                        <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-default" data-original-title="Download" Target="_blank">
                                                                    <i class="fa fa-download"></i></asp:HyperLink>
                                                        <asp:HiddenField runat="server" ID="hidFile" Value='<%# Eval("Download") %>' />
                                                    </div>--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Download">
                                                <ItemTemplate>

                                                    <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-default" data-original-title="Download" Target="_blank">
                                                                    <i class="fa fa-download"></i></asp:HyperLink>
                                                    <asp:HiddenField runat="server" ID="hidFile" Value='<%# Eval("Download") %>' />

                                                    <%--                                                     <asp:HyperLink runat="server" ID="btnViewFile" CssClass="pull-letf" data-toggle="tooltip" title="View" data-original-title="" Target="_blank">
                                                    <i class="fa fa-file-text-o"></i> ดูรายละเอียด</asp:HyperLink>--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                    </asp:GridView>

                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </asp:View>
        <!--View Detail-->

    </asp:MultiView>
    <!--multiview-->
</asp:Content>

