﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_itr_m0_CaseITLV4 : System.Web.UI.Page
{
    #region Connect

    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();

    private string ODSP_Reletion = "ODSP_Reletion";
    data_employee dtEmployee = new data_employee();

    dataODSP_Relation dtODSP = new dataODSP_Relation();
    DataReservation datareservation = new DataReservation();
    DataSupportIT _dtsupport = new DataSupportIT();

    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";
    string _localJson = "";

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string urlSelect_CaseLV4 = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_CaseLV4"];
    static string urlSelect_InsertCaseLV4 = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_InsertCaseLV4"];
    static string urlSelect_UpdateCaseLV4 = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_UpdateCaseLV4"];
    static string urlSelect_DeleteCaseLV4 = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_DeleteCaseLV4"];

    static string urlSelect_ddlLV1IT = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_ddlLV1IT"];
    static string urlSelect_CaseLV2WhereLv1 = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_CaseLV2WhereLv1"];

    static string urlSelect_CaseLV3WhereLv2 = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_CaseLV3WhereLv2"];

    
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            MvMaster.SetActiveView(ViewIndex);
            SelectMasterList();
            Select_ddlLV1IT(ddlITLV1);
        }

        ViewState["EmpIDX"] = int.Parse(Session["emp_idx"].ToString());
    }


    protected void Select_ddlLV1IT(DropDownList ddlName)
    {

        ddlName.Items.Clear();
        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Add(new ListItem("เลือกหัวข้อ....", "0"));


        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList closejobit = new UserRequestList();



        _dtsupport.BoxUserRequest[0] = closejobit;

        _dtsupport = callServicePostITRepair(urlSelect_ddlLV1IT, _dtsupport);

        ddlName.DataSource = _dtsupport.BoxUserRequest;
        ddlName.DataTextField = "Name_Code1";
        ddlName.DataValueField = "CIT1IDX";
        ddlName.DataBind();
    }

    protected void Select_ddlLV2IT(DropDownList ddlName, int CIT1IDX)
    {
        ddlName.Items.Clear();
        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Add(new ListItem("เลือกอาการ...", "0"));


        _dtsupport = new DataSupportIT();

        _dtsupport.CaseIT_Detail = new CaseITlist[1];
        CaseITlist closejobit = new CaseITlist();

        closejobit.CIT1IDX = CIT1IDX;

        _dtsupport.CaseIT_Detail[0] = closejobit;

        _dtsupport = callServicePostITRepair(urlSelect_CaseLV2WhereLv1, _dtsupport);

        ddlName.DataSource = _dtsupport.CaseIT_Detail;
        ddlName.DataTextField = "CIT2_Code";
        ddlName.DataValueField = "CIT2IDX";
        ddlName.DataBind();
    }

    protected void Select_ddlLV3IT(DropDownList ddlName,  int CIT2IDX)
    {
        ddlName.Items.Clear();
        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Add(new ListItem("เลือกอาการ...", "0"));


        _dtsupport = new DataSupportIT();

        _dtsupport.CaseIT_Detail = new CaseITlist[1];
        CaseITlist closejobit = new CaseITlist();

        closejobit.CIT2IDX = CIT2IDX;

        _dtsupport.CaseIT_Detail[0] = closejobit;
        //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtsupport));

        _dtsupport = callServicePostITRepair(urlSelect_CaseLV3WhereLv2, _dtsupport);

        ddlName.DataSource = _dtsupport.CaseIT_Detail;
        ddlName.DataTextField = "CIT3_Code";
        ddlName.DataValueField = "CIT3IDX";
        ddlName.DataBind();
    }

    protected void SelectMasterList()
    {

        _dtsupport.CaseIT_Detail = new CaseITlist[1];
        CaseITlist dtsupport = new CaseITlist();

        _dtsupport.CaseIT_Detail[0] = dtsupport;

        _dtsupport = callServicePostITRepair(urlSelect_CaseLV4, _dtsupport);
        setGridData(GvMaster, _dtsupport.CaseIT_Detail);
    }

    protected void Insert_CaseLV4()
    {
        _dtsupport.CaseIT_Detail = new CaseITlist[1];
        CaseITlist insert = new CaseITlist();

        insert.CIT4_Name = txtname.Text;
        insert.CIT4_Code = txtcode.Text;
        insert.CIT4Status = int.Parse(ddStatusadd.SelectedValue);
        insert.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        insert.CIT3IDX = int.Parse(ddlITLV3.SelectedValue);

        _dtsupport.CaseIT_Detail[0] = insert;

        _dtsupport = callServicePostITRepair(urlSelect_InsertCaseLV4, _dtsupport);

    }

    protected void Update_Master_List()
    {
        _dtsupport.CaseIT_Detail = new CaseITlist[1];
        CaseITlist update = new CaseITlist();

        update.CIT4_Name = ViewState["txtname_edit"].ToString();
        update.CIT4_Code = ViewState["txtcode_edit"].ToString();
        update.CIT4Status = int.Parse(ViewState["StatusUpdate_Update"].ToString());
        update.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        update.CIT4IDX = int.Parse(ViewState["CIT4IDX"].ToString());
        update.CIT3IDX = int.Parse(ViewState["CIT3IDX"].ToString());

        _dtsupport.CaseIT_Detail[0] = update;

        _dtsupport = callServicePostITRepair(urlSelect_UpdateCaseLV4, _dtsupport);

    }

    protected void Delete_Master_List()
    {
        _dtsupport.CaseIT_Detail = new CaseITlist[1];
        CaseITlist delete = new CaseITlist();

        delete.CIT4IDX = int.Parse(ViewState["CIT4IDX"].ToString());
        delete.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        _dtsupport.CaseIT_Detail[0] = delete;

        _dtsupport = callServicePostITRepair(urlSelect_DeleteCaseLV4, _dtsupport);
    }

    protected DataSupportIT callServicePostITRepair(string _cmdUrl, DataSupportIT _dtsupport)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtsupport);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtsupport = (DataSupportIT)_funcTool.convertJsonToObject(typeof(DataSupportIT), _localJson);

        return _dtsupport;
    }

    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (DropDownList)sender;
      

        switch (ddName.ID)
        {
            case "ddlITLV1":
                Select_ddlLV2IT(ddlITLV2, int.Parse(ddlITLV1.SelectedValue));

                break;

            case "ddlITLV2":
                Select_ddlLV3IT(ddlITLV3,  int.Parse(ddlITLV2.SelectedValue));

                break;


            case "ddlITLV1_edit":
                var ddNameDep = (DropDownList)sender;
                var row = (GridViewRow)ddNameDep.NamingContainer;

                var ddlITLV1_edit = (DropDownList)row.FindControl("ddlITLV1_edit");
                var ddlITLV2_edit = (DropDownList)row.FindControl("ddlITLV2_edit");

                Select_ddlLV2IT(ddlITLV2_edit, int.Parse(ddlITLV1_edit.SelectedValue));

                break;

            case "ddlITLV2_edit":
                var ddNameDep1 = (DropDownList)sender;
                var row1 = (GridViewRow)ddNameDep1.NamingContainer;

                var ddlITLV2_edit1 = (DropDownList)row1.FindControl("ddlITLV2_edit");
                var ddlITLV3_edit = (DropDownList)row1.FindControl("ddlITLV3_edit");

                Select_ddlLV3IT(ddlITLV3_edit, int.Parse(ddlITLV2_edit1.SelectedValue));

                break;
        }
    }
    #endregion


    #region SetGrid
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster.EditIndex == e.Row.RowIndex)
                    {
                        DropDownList ddlITLV1_edit = (DropDownList)e.Row.FindControl("ddlITLV1_edit");
                        TextBox txtCIT1IDX = (TextBox)e.Row.FindControl("txtCIT1IDX");
                        DropDownList ddlITLV2_edit = (DropDownList)e.Row.FindControl("ddlITLV2_edit");
                        TextBox txtCIT2IDX = (TextBox)e.Row.FindControl("txtCIT2IDX");
                        DropDownList ddlITLV3_edit = (DropDownList)e.Row.FindControl("ddlITLV3_edit");
                        TextBox txtCIT3IDX = (TextBox)e.Row.FindControl("txtCIT3IDX");

                        Select_ddlLV1IT(ddlITLV1_edit);
                        ddlITLV1_edit.SelectedValue = txtCIT1IDX.Text;
                       // ddlITLV1_edit.Enabled = false;

                        Select_ddlLV2IT(ddlITLV2_edit, int.Parse(ddlITLV1_edit.SelectedValue));
                        ddlITLV2_edit.SelectedValue = txtCIT2IDX.Text;
                        //ddlITLV2_edit.Enabled = false;

                        Select_ddlLV3IT(ddlITLV3_edit, int.Parse(ddlITLV2_edit.SelectedValue));
                        ddlITLV3_edit.SelectedValue = txtCIT3IDX.Text;
                    }
                }

                break;


        }

    }

    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();

                SelectMasterList();

                break;


        }
    }

    #endregion

    #region GvRowEditing

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.EditIndex = e.NewEditIndex;
                SelectMasterList();
                btnshow.Visible = false;
                break;

        }
    }

    #endregion

    #region GvRowCancelingEdit

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":
                GvMaster.EditIndex = -1;
                SelectMasterList();

                btnshow.Visible = true;
                break;

        }
    }

    #endregion

    #region GvRowUpdating

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                int CIT4IDX = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var txtname_edit = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtname_edit");
                var txtcode_edit = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtcode_edit");
                var StatusUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddStatusUpdate");
                var ddlITLV3_edit = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlITLV3_edit");

                GvMaster.EditIndex = -1;

                ViewState["CIT4IDX"] = CIT4IDX;
                ViewState["txtname_edit"] = txtname_edit.Text;
                ViewState["txtcode_edit"] = txtcode_edit.Text;
                ViewState["StatusUpdate_Update"] = StatusUpdate.SelectedValue;
                ViewState["CIT3IDX"] = ddlITLV3_edit.SelectedValue;

                Update_Master_List();
                SelectMasterList();

                break;
        }
    }

    #endregion

    protected void SetDefaultAdd()
    {
        txtname.Text = String.Empty;
        txtcode.Text = String.Empty;
        ddStatusadd.SelectedValue = "1";
        ddlITLV1.SelectedValue = "0";
        ddlITLV2.SelectedValue = "0";
        ddlITLV3.SelectedValue = "0";
    }

    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {

            case "CmdAdd":
                btnshow.Visible = false;
                Panel_Add.Visible = true;
                SetDefaultAdd();
                break;

            case "btnCancel":
                btnshow.Visible = true;
                Panel_Add.Visible = false;
                break;

            case "btnAdd":
                Insert_CaseLV4();
                Panel_Add.Visible = false;
                btnshow.Visible = true;
                SelectMasterList();
                break;
            case "CmdDel":
                int CIT4IDX = int.Parse(cmdArg);
                ViewState["CIT4IDX"] = CIT4IDX;
                Delete_Master_List();
                SelectMasterList();

                break;
        }



    }
    #endregion
}