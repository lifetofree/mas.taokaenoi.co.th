﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_hr_employee_group_shift : System.Web.UI.Page
{

    #region Connect

    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();
    service_execute _serviceExecute = new service_execute();

    data_emps _dataEmp = new data_emps();
    data_employee _dtEmployee = new data_employee();
    data_permission_set _dataPermission = new data_permission_set();

    string _localXml = String.Empty;
    string _localString = String.Empty;
    string _localJson = String.Empty;
    int _actionType = 0;
    int _emp_idx = 0;
    int _default_int = 0;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetSelectGroupDialy = _serviceUrl + ConfigurationManager.AppSettings["urlGetSelectGroupDialy"];
    static string _urlGetInsertGroupDialy = _serviceUrl + ConfigurationManager.AppSettings["urlGetInsertGroupDialy"];
    static string _urlGetUpdateGroupDialy = _serviceUrl + ConfigurationManager.AppSettings["urlGetUpdateGroupDialy"];
    static string _urlGetDeleteGroupDialy = _serviceUrl + ConfigurationManager.AppSettings["urlGetDeleteGroupDialy"];
    static string _urlGetInsertGroupDialy_Import = _serviceUrl + ConfigurationManager.AppSettings["urlGetInsertGroupDialy_Import"];

    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    static string urlGetEmployee = _serviceUrl + ConfigurationManager.AppSettings["urlGetEmployee"];
    static string _urlGetDetail_Permssion = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetail_Permssion"];
    static string urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];

    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ViewState["emp_idx"] =   int.Parse(Session["emp_idx"].ToString());
            MvMaster.SetActiveView(ViewIndex);
            select_empIdx_present();
            SelectMasterList(txtname_search.Text);
            Panel_Show_Detail.Visible = true;
            permission_check();
            Panel_shidw_detail_list.Visible = false;
        }

    }
    #endregion

    #region INSERT&SELECT&UPDATE

    protected void select_empIdx_present()
    {
        _dtEmployee = callServiceEmpProfile(urlGetMyProfile + ViewState["emp_idx"].ToString());

        ViewState["rdept_name"] = _dtEmployee.employee_list[0].dept_name_th;
        ViewState["rdept_idx"] = _dtEmployee.employee_list[0].rdept_idx;
        ViewState["FullName"] = _dtEmployee.employee_list[0].emp_name_th;
        ViewState["firsName_TH"] = _dtEmployee.employee_list[0].emp_firstname_th;
        ViewState["Org_name"] = _dtEmployee.employee_list[0].org_name_th;
        ViewState["Org_idx"] = _dtEmployee.employee_list[0].org_idx;

        ViewState["EmpCode"] = _dtEmployee.employee_list[0].emp_code;
        ViewState["Positname"] = _dtEmployee.employee_list[0].pos_name_th;
        ViewState["Pos_idx"] = _dtEmployee.employee_list[0].rpos_idx;
        ViewState["Secname"] = _dtEmployee.employee_list[0].sec_name_th;
        ViewState["Sec_idx"] = _dtEmployee.employee_list[0].rsec_idx;
    }

    protected void Insert_Status()
    {
        _dataEmp.emps_group_diary_action = new group_diary[1];
        group_diary dtemployee_ = new group_diary();

        dtemployee_.group_diary_name = txtgroupname.Text;
        dtemployee_.rsec_idx_ref = int.Parse(ddlsec.SelectedValue);
        dtemployee_.group_diary_status = 1; //int.Parse(ddl_status.SelectedValue);
        dtemployee_.group_diary_created_by = int.Parse(ViewState["emp_idx"].ToString());

        _dataEmp.emps_group_diary_action[0] = dtemployee_;
        //sfa.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmp));
        _dataEmp = callService(_urlGetInsertGroupDialy, _dataEmp);
    }

    protected void Insert_Import()
    {
        data_emps data_m0_importgroup_ot = new data_emps();

        //insert m0
        group_diary m0_importgroup_ot_detail = new group_diary();
        data_m0_importgroup_ot.emps_group_diary_action = new group_diary[1];

        m0_importgroup_ot_detail.group_diary_name = txtgroupname.Text;
        m0_importgroup_ot_detail.rsec_idx_ref = int.Parse(ddlsec.SelectedValue);
        m0_importgroup_ot_detail.group_diary_created_by = int.Parse(ViewState["emp_idx"].ToString());
        m0_importgroup_ot_detail.group_diary_status = 2;


        //insert m1
        var _m1_importgroup_ot_detail = new m1_group_diary[GvDetailExcelImport.Rows.Count];
        int sumcheck_m1 = 0;
        int count_m1 = 0;
        //int sum_total_price = 0;

        foreach (GridViewRow gvrow in GvDetailExcelImport.Rows)
        {

            data_m0_importgroup_ot.emps_group_diary_action = new group_diary[1];

            //CheckBox chk_date_ot = (CheckBox)gv_row.FindControl("chk_date_ot");
            Label lbl_emp_code_excel = (Label)gvrow.FindControl("lbl_emp_code_excel");
            Label lbl_emp_name_excel = (Label)gvrow.FindControl("lbl_emp_name_excel");


            //if (chk_date_ot.Checked == true)
            //{

            _m1_importgroup_ot_detail[count_m1] = new m1_group_diary();
            //_m1_importgroup_ot_detail[count_m1].cemp_idx = _emp_idx;
            _m1_importgroup_ot_detail[count_m1].emp_code = lbl_emp_code_excel.Text;
            _m1_importgroup_ot_detail[count_m1].group_diary_created_by = int.Parse(ViewState["emp_idx"].ToString()); //lbl_emp_code_excel.Text;

            sumcheck_m1 = sumcheck_m1 + 1;
            count_m1++;

            //}

        }

        data_m0_importgroup_ot.emps_group_diary_action[0] = m0_importgroup_ot_detail;
        data_m0_importgroup_ot.m1_emps_group_diary_action = _m1_importgroup_ot_detail;
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_importgroup_ot));

        if (int.Parse(ddl_status.SelectedValue) != 0)
        {
            _dataEmp.emps_group_diary_action = new group_diary[1];
            group_diary dtemployee_ = new group_diary();

            dtemployee_.group_diary_name = txtgroupname.Text;
            dtemployee_.rsec_idx_ref = int.Parse(ddlsec.SelectedValue);
            dtemployee_.group_diary_status = int.Parse(ddl_status.SelectedValue);
            dtemployee_.group_diary_created_by = int.Parse(ViewState["emp_idx"].ToString());

            data_m0_importgroup_ot.emps_group_diary_action[0] = dtemployee_;
            //_dataEmp = callService(_urlGetInsertGroupDialy, _dataEmp);
        }
        //sfa.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0_importgroup_ot));
        data_m0_importgroup_ot = callService(_urlGetInsertGroupDialy_Import, data_m0_importgroup_ot);

        //litDebug.Text = String.Format("{0:N2}", lit_total_hoursmonth.Text);
        //Page.Response.Redirect(Page.Request.Url.ToString(), true);
    }

    protected void SelectMasterList(string name)
    {
        _dataEmp.emps_group_diary_action = new group_diary[1];
        group_diary dtemployee_ = new group_diary();

        dtemployee_.group_diary_created_by = int.Parse(ViewState["emp_idx"].ToString());
        dtemployee_.group_diary_name = name;
        _dataEmp.emps_group_diary_action[0] = dtemployee_;

        _dataEmp = callService(_urlGetSelectGroupDialy, _dataEmp);
        //text.Text = _dataEmployee.ToString();
        setGridData(GvMaster, _dataEmp.emps_group_diary_action);
    }

    protected void Delete_Master_List()
    {
        _dataEmp.emps_group_diary_action = new group_diary[1];
        group_diary dtemployee_ = new group_diary();

        dtemployee_.m0_group_diary_idx = int.Parse(ViewState["m0_group_diary_idx"].ToString());
        dtemployee_.group_diary_status = 9;

        _dataEmp.emps_group_diary_action[0] = dtemployee_;
        _dataEmp = callService(_urlGetDeleteGroupDialy, _dataEmp);
    }

    protected void Select_Employee_DB(GridView GName)
    {
        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _dataEmployee_ = new employee_detail();

        //_dataEmployee_.org_idx = int.Parse(ddlorg.SelectedValue);
        //_dataEmployee_.rdept_idx = int.Parse(ddldep.SelectedValue);
        _dataEmployee_.rsec_idx = int.Parse(ddlsec.SelectedValue);
        _dataEmployee_.emp_type_idx = 0; //int.Parse(ddlemp_type.SelectedValue);
        _dataEmployee_.emp_status = 1;
        _dataEmployee_.type_select_emp = 16;

        _dataEmployee.employee_list[0] = _dataEmployee_;
        _dataEmployee = callServiceEmployee(urlGetEmployee, _dataEmployee);
        //ViewState["Box_dataEmployee_index_"] = _dataEmployee.employee_list;
        //sfa.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        setGridData(GName, _dataEmployee.employee_list);
    }

    protected void Select_Employee_DB_List(GridView GName, int rsec_idx)
    {
        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _dataEmployee_ = new employee_detail();

        //_dataEmployee_.org_idx = int.Parse(ddlorg.SelectedValue);
        //_dataEmployee_.rdept_idx = int.Parse(ddldep.SelectedValue);
        _dataEmployee_.rsec_idx = rsec_idx;
        _dataEmployee_.emp_type_idx = 0; //int.Parse(ddlemp_type.SelectedValue);
        _dataEmployee_.emp_status = 1;
        _dataEmployee_.type_select_emp = 16;

        _dataEmployee.employee_list[0] = _dataEmployee_;
        _dataEmployee = callServiceEmployee(urlGetEmployee, _dataEmployee);
        //ViewState["Box_dataEmployee_index_"] = _dataEmployee.employee_list;
        //sfa.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        setGridData(GName, _dataEmployee.employee_list);
    }

    protected void select_org(DropDownList ddlName)
    {
        _dataEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dataEmployee.organization_list[0] = _orgList;

        _dataEmployee = callServiceEmployee(_urlGetOrganizationList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกองค์กร....", "0"));
    }

    protected void select_dep(DropDownList ddlName, int _org_idx)
    {
        _dataEmployee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dataEmployee.department_list[0] = _deptList;

        _dataEmployee = callServiceEmployee(_urlGetDepartmentList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกฝ่าย....", "0"));
    }

    protected void select_sec(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {
        _dataEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _dataEmployee.section_list[0] = _secList;

        _dataEmployee = callServiceEmployee(_urlGetSectionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.section_list, "sec_name_th", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกหน่วยงาน....", "0"));
    }



    protected void permission_check()
    {
        _dataPermission.permission_list = new permission_set_detail[1];
        permission_set_detail _perList = new permission_set_detail();
        _perList.p_system = 2; // empshift
        _perList.p_permission_type = 1; //create
        _perList.rpos_idx_set = ViewState["Pos_idx"].ToString();
        _perList.type_action = 1;
        _dataPermission.permission_list[0] = _perList;
        //fs.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataPermission));
        _dataPermission = callServicePermission(_urlGetDetail_Permssion, _dataPermission);
        ViewState["ArrayRsec"] = "0";

        if (_dataPermission.return_code.ToString() == "0")
        {
            ViewState["permission_check"] = 1; //มีสิทธิ์
            Check_ddl_Permission(int.Parse(ViewState["Pos_idx"].ToString()), int.Parse(ViewState["Sec_idx"].ToString()));
        }
        else
        {
            ViewState["ArrayRsec"] = 0;
            ViewState["permission_check"] = 0;
        }

        //fs.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataPermission));
        //fs.Text = ViewState["permission_check"].ToString();
    }

    protected void Check_ddl_Permission(int RposIDX, int RsceIDX)
    {
        ViewState["ArrayRsec"] = null;

        _dataPermission.permission_list = new permission_set_detail[1];
        permission_set_detail _perList = new permission_set_detail();
        _perList.p_system = 2; // empshift
        _perList.p_permission_type = 1; //create
        _perList.rpos_idx_set = RposIDX.ToString();
        _perList.type_action = 2;
        _perList.p_status = 1;
        _dataPermission.permission_list[0] = _perList;
        //fds.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataPermission));
        _dataPermission = callServicePermission(_urlGetDetail_Permssion, _dataPermission);
        ViewState["ArrayRsec"] = "0";
        //aaa
        if (_dataPermission.return_code != "0")
        {
            ViewState["ArrayRsec"] = _dataPermission.return_code;
            ViewState["permission_check"] = 1; //มีสิทธิ์
        }
        else
        {
            ViewState["ArrayRsec"] = 0; ;
        }

        //fs.Text = ViewState["ArrayRsec"].ToString();
    }

    protected void ddlSec_Add_CreateAll()
    {
        ddlsec.AppendDataBoundItems = true;
        ddlsec.Items.Clear();
        ddlsec.Items.Add(new ListItem("เลือกหน่วยงาน....", "0"));
        ddlsec.SelectedValue = "0";

        _dataPermission.permission_list = new permission_set_detail[1];
        permission_set_detail _perList = new permission_set_detail();
        _perList.p_system = 2; // empshift
        _perList.p_permission_type = 1; //create
        _perList.rsec_idx_set = ViewState["ArrayRsec"].ToString();
        _perList.rpos_idx_set = ViewState["Pos_idx"].ToString();
        _perList.type_action = 3;
        _perList.p_status = 1;
        _dataPermission.permission_list[0] = _perList;
        //fs.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataPermission));
        _dataPermission = callServicePermission(_urlGetDetail_Permssion, _dataPermission);

        ddlsec.DataSource = _dataPermission.permission_list;
        ddlsec.DataTextField = "SecNameTH";
        ddlsec.DataValueField = "RSecIDX";
        ddlsec.DataBind();
        //setDdlData(ddlsec, _dataPermission.permission_list, "SecNameTH", "RSecIDX");
    }

    protected data_emps callService(string _cmdUrl, data_emps _dtmaster)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtmaster);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //text.Text = _localJson.ToString();
        _dtmaster = (data_emps)_funcTool.convertJsonToObject(typeof(data_emps), _localJson);

        return _dtmaster;
    }

    protected data_employee callServiceEmpProfile(string _cmdUrl)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }

    protected data_employee callServiceEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //fss.Text = _localJson.ToString();

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //fsa.Text = _cmdUrl + _localJson;

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);


        return _dataEmployee;
    }

    protected data_permission_set callServicePermission(string _cmdUrl, data_permission_set _dtmaster_)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtmaster_);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //text.Text = _localJson.ToString();
        _dtmaster_ = (data_permission_set)_funcTool.convertJsonToObject(typeof(data_permission_set), _localJson);

        return _dtmaster_;
    }

    #endregion

    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (DropDownList)sender;

        switch (ddName.ID)
        {
            case "ddlorg":
                select_dep(ddldep, int.Parse(ddlorg.SelectedValue));
                break;
            case "ddldep":
                select_sec(ddlsec, int.Parse(ddlorg.SelectedValue), int.Parse(ddldep.SelectedValue));
                break;
            case "ddlOrganizationEdit_Select":
                var ddNameOrg = (DropDownList)sender;
                var row = (GridViewRow)ddNameOrg.NamingContainer;

                var ddlOrg = (DropDownList)row.FindControl("ddlOrganizationEdit_Select");
                var ddlDep = (DropDownList)row.FindControl("ddlDepartmentEdit_Select");
                var ddlSec = (DropDownList)row.FindControl("ddlSectionEdit_Select");

                select_dep(ddlDep, int.Parse(ddlOrg.SelectedValue));
                select_sec(ddlSec, int.Parse(ddlOrg.SelectedValue), int.Parse(ddlDep.SelectedValue));
                break;
            case "ddlDepartmentEdit_Select":
                var ddNameOrg_1 = (DropDownList)sender;
                var row_1 = (GridViewRow)ddNameOrg_1.NamingContainer;

                var ddlOrg_1 = (DropDownList)row_1.FindControl("ddlOrganizationEdit_Select");
                var ddlDep_1 = (DropDownList)row_1.FindControl("ddlDepartmentEdit_Select");
                var ddlSec_1 = (DropDownList)row_1.FindControl("ddlSectionEdit_Select");

                select_sec(ddlSec_1, int.Parse(ddlOrg_1.SelectedValue), int.Parse(ddlDep_1.SelectedValue));
                break;

        }
    }

    #endregion

    #region Gridview

    #region SetGrid
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormData(FormView fvName, Object obj)
    {
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }
    #endregion

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    var txtEditorgIDX_Select = (TextBox)e.Row.FindControl("txtEditorgIDX_Select");
                    var txtEditDepIDX_Select = (TextBox)e.Row.FindControl("txtEditDepIDX_Select");
                    var txtEditSecIDX_Select = (TextBox)e.Row.FindControl("txtEditSecIDX_Select");

                    var ddlOrganizationEdit_Select = (DropDownList)e.Row.FindControl("ddlOrganizationEdit_Select");
                    var ddlDepartmentEdit_Select = (DropDownList)e.Row.FindControl("ddlDepartmentEdit_Select");
                    var ddlSectionEdit_Select = (DropDownList)e.Row.FindControl("ddlSectionEdit_Select");

                    select_org(ddlOrganizationEdit_Select);
                    ddlOrganizationEdit_Select.SelectedValue = txtEditorgIDX_Select.Text;
                    select_dep(ddlDepartmentEdit_Select, int.Parse(txtEditorgIDX_Select.Text));
                    ddlDepartmentEdit_Select.SelectedValue = txtEditDepIDX_Select.Text;
                    select_sec(ddlSectionEdit_Select, int.Parse(txtEditorgIDX_Select.Text), int.Parse(txtEditDepIDX_Select.Text));
                    ddlSectionEdit_Select.SelectedValue = txtEditSecIDX_Select.Text;
                }

                break;


        }

    }

    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();

                SelectMasterList(txtname_search.Text);

                break;


        }
    }

    #endregion

    #region GvRowEditing

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.EditIndex = e.NewEditIndex;
                SelectMasterList(txtname_search.Text);

                break;

        }
    }

    #endregion

    #region GvRowCancelingEdit

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":
                GvMaster.EditIndex = -1;
                SelectMasterList(txtname_search.Text);
                break;

        }
    }

    #endregion

    #region GvRowUpdating

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                int m0_group_diary_idx = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var txtName_update = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtName_update");
                var ddStatus_update = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddStatus_update");
                var ddlSectionEdit_Select = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlSectionEdit_Select");

                GvMaster.EditIndex = -1;

                _dataEmp.emps_group_diary_action = new group_diary[1];
                group_diary dtemployee_ = new group_diary();

                dtemployee_.m0_group_diary_idx = m0_group_diary_idx;
                dtemployee_.group_diary_name = txtName_update.Text;
                dtemployee_.rsec_idx_ref = int.Parse(ddlSectionEdit_Select.SelectedValue);
                dtemployee_.group_diary_status = int.Parse(ddStatus_update.SelectedValue);

                _dataEmp.emps_group_diary_action[0] = dtemployee_;
                _dataEmp = callService(_urlGetUpdateGroupDialy, _dataEmp);

                SelectMasterList(txtname_search.Text);

                break;
        }
    }

    #endregion


    #endregion

    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {

            case "CmdAdd":
                btnadd.Visible = false;
                Panel_Add.Visible = true;
                Panel_Add_Import.Visible = false;
                Panel_Show_Detail.Visible = false;
                ddlSec_Add_CreateAll();
                //select_org(ddlorg);
                break;
            case "CmdAddImport":
                //btnaddImport.Visible = false;
                Panel_Add.Visible = false;
                Panel_Add_Import.Visible = true;
                Panel_Show_Detail.Visible = false;
                break;
            case "btnCancel":
                btnadd.Visible = true;
                Panel_Add.Visible = false;
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btnCancelImport":
                //btnadd.Visible = true;
                //Panel_Add.Visible = false;
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "CmdDel":
                int m0_group_diary_idx = int.Parse(cmdArg);
                ViewState["m0_group_diary_idx"] = m0_group_diary_idx;
                Delete_Master_List();
                SelectMasterList(txtname_search.Text);

                break;
            case "cmdImportExcel":

                data_employee data_m0_import = new data_employee();

                if (UploadFileExcel.HasFile)
                {
                    //litDebug.Text = "1";
                    string datetimeNow = DateTime.Now.ToString("yyyyMMdd_HHmmss_ffff");
                    string FileName = Path.GetFileName(UploadFileExcel.PostedFile.FileName);
                    string extension = Path.GetExtension(UploadFileExcel.PostedFile.FileName);
                    string newFileName = datetimeNow + extension.ToLower();
                    string folderPath = ConfigurationManager.AppSettings["path_group_shift_import"];
                    string filePath = Server.MapPath(folderPath + newFileName);
                    if (extension.ToLower() == ".xls" || extension.ToLower() == ".xlsx")
                    {
                        UploadFileExcel.SaveAs(filePath);

                        string conStr = String.Empty;
                        conStr = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                        conStr = String.Format(conStr, filePath, "Yes");
                        OleDbConnection connExcel = new OleDbConnection(conStr);
                        OleDbCommand cmdExcel = new OleDbCommand();
                        OleDbDataAdapter oda = new OleDbDataAdapter();
                        DataTable dt = new DataTable();

                        cmdExcel.Connection = connExcel;
                        connExcel.Open();
                        DataTable dtExcelSchema;
                        dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                        string SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                        connExcel.Close();
                        connExcel.Open();
                        cmdExcel.CommandText = "SELECT * From [" + SheetName + "]";
                        oda.SelectCommand = cmdExcel;
                        oda.Fill(dt);
                        connExcel.Close();

                        // m1 group detail
                        var count_alert_u1 = dt.Rows.Count;

                        var m1_group_import = new group_diary[dt.Rows.Count];
                        //m1_group_import[count_] = new ovt_u1_document_detail();
                        for (var i = 0; i <= dt.Rows.Count - 1;)
                        {
                            //if (dt.Rows[i][0].ToString().Trim() != String.Empty || dt.Rows[i][1].ToString().Trim() != String.Empty)
                            //{
                            m1_group_import[i] = new group_diary();

                            if (dt.Rows[i][0].ToString().Trim() != String.Empty)
                            {
                                m1_group_import[i].emp_code = dt.Rows[i][0].ToString().Trim();
                            }
                            else
                            {
                                m1_group_import[i].emp_code = "-";
                            }

                            if (dt.Rows[i][1].ToString().Trim() != String.Empty)
                            {
                                m1_group_import[i].emp_name = dt.Rows[i][1].ToString().Trim();
                            }
                            else
                            {
                                m1_group_import[i].emp_name = "-";
                            }

                            if (dt.Rows[i][2].ToString().Trim() != String.Empty)
                            {
                                m1_group_import[i].DeptNameTH = dt.Rows[i][2].ToString().Trim();
                            }
                            else
                            {
                                m1_group_import[i].DeptNameTH = "-";
                            }

                            i++;
                            //}
                        }

                        ViewState["VsDetailExcelOTDay"] = m1_group_import;
                        ////data_m0_import.ovt_m1_group_list = m1_group_import;
                        ////litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_import));

                        if (dt.Rows.Count != 0)
                        {
                            //litDebug.Text = "555";
                            Div_GvDetailExcelImport.Visible = true;
                            ////Save_Excel.Visible = true;
                            setGridData(GvDetailExcelImport, ViewState["VsDetailExcelOTDay"]);

                            GvDetailExcelImport.Focus();
                        }
                        else
                        {

                            _funcTool.showAlert(this, "ไม่มีข้อมูล!!");
                            Div_GvDetailExcelImport.Visible = false;
                            //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", string.Format("alert(ไม่มีข้อมูล!!);"), true);
                            //GvExcel_Import.Visible = true;
                            //setGridData(GvExcel_Show, null);

                            //_funcTool.showAlert(this, "ไม่มีข้อมูล");

                            //txt_addnamegroup.Focus();
                            break;
                        }
                    }
                    else
                    {
                        _funcTool.showAlert(this, "Import ได้เฉพาะไฟล์ Excel (นามสกุลไฟล์ .xls หรือ .xlsx) เท่านั้น");
                    }

                    Div_GvDetailExcelImport.Visible = true;
                }
                else
                {
                    _funcTool.showAlert(this, "--- ไม่มีข้อมูล ---");
                    Div_GvDetailExcelImport.Visible = false;
                }

                break;

            case "cmdSaveImportExcel":
                Insert_Import();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "cmdSearchSection":
                Div_GvDetailExcelImport.Visible = true;
                Select_Employee_DB(GvDetailSection);
                break;

            case "btnedit_detail":
                int rsec_idx = int.Parse(cmdArg);
                Panel_Show_Detail.Visible = false;
                btnadd.Visible = false;
                Panel_shidw_detail_list.Visible = true;
                Select_Employee_DB_List(GvDetailSection_list, rsec_idx);
                break;

            case "btnedit_back":
                btnadd.Visible = true;
                Panel_Show_Detail.Visible = true;
                Panel_shidw_detail_list.Visible = false;
                break;

            case "CmdSearch":
                SelectMasterList(txtname_search.Text);
                break;

            case "btnRefresh":
                txtname_search.Text = String.Empty;
                SelectMasterList(txtname_search.Text);

                break;
        }



    }
    #endregion
}