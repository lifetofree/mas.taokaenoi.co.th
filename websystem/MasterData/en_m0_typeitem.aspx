﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="en_m0_typeitem.aspx.cs" Inherits="websystem_MasterData_en_m0_typeitem" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <asp:Literal ID="txt" runat="server"></asp:Literal>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

                <asp:View ID="ViewIndex" runat="server">

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-wrench"></i><strong>&nbsp; Point จุดตรวจ</strong></h3>
                        </div>

                        <div class="panel-body">

                            <div id="SETBoxAllSearch" runat="server">
                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">

                                        <div class="form-group">
                                            <asp:Label ID="Label4" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทเครื่องจักร :" />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddltype_search" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="0" Text="กรุณาเลือกประเภทเครื่องจักร..."></asp:ListItem>

                                                </asp:DropDownList>
                                            </div>
                                            <asp:Label ID="Label12" CssClass="col-sm-2 control-label" runat="server"  Text="รหัสกลุ่มเครื่องจักร :" />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddltypecode_search" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" runat="server" CssClass="form-control">
                                                    <asp:ListItem Text="กรุณาเลือกรหัสกลุ่มเครื่องจักร..." Value="0"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label5" CssClass="col-sm-2 control-label" runat="server" Text="รหัสกลุ่ม :" />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlgroupcode_search" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="0" Text="กรุณาเลือกรหัสกลุ่ม..."></asp:ListItem>

                                                </asp:DropDownList>
                                            </div>
                                            <asp:Label ID="Label67" CssClass="col-sm-2 control-label" runat="server" Text="Point จุดตรวจ :" />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtsearchtype" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-5 col-sm-offset-2">
                                                <asp:LinkButton ID="btnsearch" CssClass="btn btn-warning btn-sm" data-toggle="tooltip" title="Search" runat="server" CommandName="btnsearch" ValidationGroup="btnsearch" OnCommand="btnCommand"><i class="fa fa-search"></i> </asp:LinkButton>
                                                <asp:LinkButton ID="btnRefresh" CssClass="btn btn-info btn-sm" data-toggle="tooltip" title="Refresh" runat="server" CommandName="BtnBack" OnCommand="btnCommand"><i class="fa fa-refresh"></i></asp:LinkButton>
                                            </div>
                                        </div>



                                    </div>
                                </div>
                            </div>

                            <hr />


                            <div class="form-group">

                                <asp:LinkButton ID="btnshow" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Addtypeitem" runat="server" CommandName="CmdAdd" OnCommand="btnCommand"><i class="fa fa-plus"></i></asp:LinkButton>
                            </div>

                            <%------------------------ Div ADD  ------------------------%>

                            <asp:Panel ID="Panel_Add" runat="server" Visible="false">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><i class="glyphicon glyphicon-plus"></i><strong>&nbsp; Add Point จุดตรวจ</strong></h4>
                                    <div class="form-horizontal" role="form">
                                        <div class="panel-heading">

                                            <div class="form-group">

                                                <asp:Label ID="Label17" class="col-sm-3 control-label" runat="server" Text="ประเภทเครื่องจักร : " />
                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="ddltypemachine" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" class="form-control" runat="server">
                                                        <asp:ListItem Value="0" Text="กรุณาเลือกประเภทเครื่องจักร..."></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ValidationGroup="Save" runat="server" Display="None"
                                                        ControlToValidate="ddltypemachine" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกประเภทเครื่องจักร"
                                                        ValidationExpression="กรุณาเลือกประเภทเครื่องจักร" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator9" Width="160" />

                                                </div>
                                            </div>

                                            <div class="form-group">

                                                <asp:Label ID="Label18" class="col-sm-3 control-label" runat="server" Text="รหัสกลุ่มเครื่องจักร : " />
                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="ddltypecode" class="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server">
                                                        <asp:ListItem Value="0" Text="กรุณาเลือกรหัสกลุ่มเครื่องจักร..."></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ValidationGroup="Save" runat="server" Display="None"
                                                        ControlToValidate="ddltypecode" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกรหัสกลุ่มเครื่องจักร"
                                                        ValidationExpression="กรุณาเลือกรหัสกลุ่มเครื่องจักร" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender15" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator10" Width="160" />

                                                </div>
                                            </div>

                                            <div class="form-group">

                                                <asp:Label ID="Label3" class="col-sm-3 control-label" runat="server" Text="รหัสกลุ่ม : " />
                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="ddlgroupmachine" class="form-control" runat="server">
                                                        <asp:ListItem Value="0" Text="กรุณาเลือกรหัสกลุ่ม..."></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidddator8" ValidationGroup="Save" runat="server" Display="None"
                                                        ControlToValidate="ddlgroupmachine" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกรหัสกลุ่ม"
                                                        ValidationExpression="กรุณาเลือกรหัสกลุ่ม" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidddator8" Width="160" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label26" runat="server" Text="Point จุดตรวจ(th)" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="txtnameth" runat="server" CssClass="form-control" PlaceHolder="........" />
                                                </div>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="txtnameth" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกข้อมูลไทย"
                                                    ValidationExpression="กรุณากรอกข้อมูลไทย"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                    ValidationGroup="Save" Display="None"
                                                    ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                    ControlToValidate="txtnameth"
                                                    ValidationExpression="^[\s\S]{0,1000}$"
                                                    SetFocusOnError="true" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />

                                            </div>
                                            <div class="form-group">
                                                <asp:Label ID="Label1" runat="server" Text="Point จุดตรวจ(en)" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="txtnameen" runat="server" CssClass="form-control" PlaceHolder="........" />
                                                </div>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="txtnameen" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกข้อมูลอังกฤษ"
                                                    ValidationExpression="กรุณากรอกข้อมูลอังกฤษ"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                                    ValidationGroup="Save" Display="None"
                                                    ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                    ControlToValidate="txtnameen"
                                                    ValidationExpression="^[\s\S]{0,1000}$"
                                                    SetFocusOnError="true" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator2" Width="160" />

                                            </div>
                                            <div class="form-group">
                                                <asp:Label ID="lbl" CssClass="col-sm-3 control-label" runat="server" Text="Status" />
                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="ddStatusadd" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="1" Text="Online" />
                                                        <asp:ListItem Value="0" Text="Offline" />
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-3">
                                                    <asp:LinkButton ID="lbladd" ValidationGroup="Save" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="btnAdd" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="lblcancel" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancel" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr />
                                </div>
                            </asp:Panel>

                            <asp:GridView ID="GvMaster" runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="primary"
                                HeaderStyle-Height="40px"
                                AllowPaging="true"
                                DataKeyNames="m0tyidx"
                                PageSize="10"
                                OnRowDataBound="Master_RowDataBound"
                                OnRowEditing="Master_RowEditing"
                                OnRowCancelingEdit="Master_RowCancelingEdit"
                                OnPageIndexChanging="Master_PageIndexChanging"
                                OnRowUpdating="Master_RowUpdating">

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>
                                <Columns>

                                    <asp:TemplateField HeaderText="#">

                                        <ItemTemplate>
                                            <asp:Label ID="lblstidx" runat="server" Visible="false" Text='<%# Eval("m0tyidx") %>' />
                                            <%# (Container.DataItemIndex +1) %>
                                        </ItemTemplate>


                                        <EditItemTemplate>
                                            <div class="form-horizontal" role="form">
                                                <div class="panel-heading">
                                                    <div class="form-group">
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txtm0tyidx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("m0tyidx")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">

                                                        <asp:Label ID="Label17" class="col-sm-3 control-label" runat="server" Text="ประเภทเครื่องจักร : " />
                                                        <div class="col-sm-8">
                                                            <asp:TextBox ID="txtTmcIDX" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("TmcIDX")%>' />
                                                            <asp:DropDownList ID="ddltypemachine_edit" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" class="form-control" runat="server">
                                                                <asp:ListItem Value="0" Text="กรุณาเลือกประเภทเครื่องจักร..."></asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ValidationGroup="Save_edit" runat="server" Display="None"
                                                                ControlToValidate="ddltypemachine_edit" Font-Size="11"
                                                                ErrorMessage="กรุณาเลือกประเภทเครื่องจักร"
                                                                ValidationExpression="กรุณาเลือกประเภทเครื่องจักร" InitialValue="0" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator9" Width="160" />

                                                        </div>
                                                    </div>

                                                    <div class="form-group">

                                                        <asp:Label ID="Label18" class="col-sm-3 control-label" runat="server" Text="รหัสกลุ่มเครื่องจักร : " />
                                                        <div class="col-sm-8">
                                                            <asp:TextBox ID="txtTCIDX" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("TCIDX")%>' />

                                                            <asp:DropDownList ID="ddltypecode_edit" class="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server">
                                                                <asp:ListItem Value="0" Text="กรุณาเลือกรหัสกลุ่มเครื่องจักร..."></asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ValidationGroup="Save_edit" runat="server" Display="None"
                                                                ControlToValidate="ddltypecode_edit" Font-Size="11"
                                                                ErrorMessage="กรุณาเลือกรหัสกลุ่มเครื่องจักร"
                                                                ValidationExpression="กรุณาเลือกรหัสกลุ่มเครื่องจักร" InitialValue="0" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender15" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator10" Width="160" />

                                                        </div>
                                                    </div>

                                                    <div class="form-group">

                                                        <asp:Label ID="Label3" class="col-sm-3 control-label" runat="server" Text="รหัสกลุ่ม : " />
                                                        <div class="col-sm-8">
                                                            <asp:TextBox ID="txtGCIDX" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("GCIDX")%>' />

                                                            <asp:DropDownList ID="ddlgroupmachine_edit" class="form-control" runat="server">
                                                                <asp:ListItem Value="0" Text="กรุณาเลือกรหัสกลุ่ม..."></asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidddator8" ValidationGroup="Save_edit" runat="server" Display="None"
                                                                ControlToValidate="ddlgroupmachine_edit" Font-Size="11"
                                                                ErrorMessage="กรุณาเลือกรหัสกลุ่ม"
                                                                ValidationExpression="กรุณาเลือกรหัสกลุ่ม" InitialValue="0" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidddator8" Width="160" />
                                                        </div>
                                                    </div>



                                                    <div class="form-group">
                                                        <asp:Label ID="Label26" runat="server" Text="Point จุดตรวจ(th)" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-8">
                                                            <asp:TextBox ID="txtnameth_edit" runat="server" CssClass="form-control" Text='<%# Eval("type_name_th")%>' />
                                                        </div>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="Save_edit" runat="server" Display="None" ControlToValidate="txtnameth_edit" Font-Size="11"
                                                            ErrorMessage="กรุณากรอกข้อมูลภาษาไทย"
                                                            ValidationExpression="กรุณากรอกข้อมูลภาษาไทย"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                            ValidationGroup="Save_edit" Display="None"
                                                            ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                            ControlToValidate="txtnameth_edit"
                                                            ValidationExpression="^[\s\S]{0,1000}$"
                                                            SetFocusOnError="true" />

                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />

                                                    </div>


                                                    <div class="form-group">
                                                        <asp:Label ID="Label2" runat="server" Text="Point จุดตรวจ(en)" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-8">
                                                            <asp:TextBox ID="txtnameen_edit" runat="server" CssClass="form-control" Text='<%# Eval("type_name_en")%>' />
                                                        </div>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="Save_edit" runat="server" Display="None" ControlToValidate="txtnameen_edit" Font-Size="11"
                                                            ErrorMessage="กรุณากรอกข้อมูลอังกฤษ"
                                                            ValidationExpression="กรุณากรอกข้อมูลอังกฤษ"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                                                            ValidationGroup="Save_edit" Display="None"
                                                            ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                            ControlToValidate="txtnameen_edit"
                                                            ValidationExpression="^[\s\S]{0,1000}$"
                                                            SetFocusOnError="true" />

                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator3" Width="160" />

                                                    </div>


                                                    <div class="form-group">
                                                        <asp:Label ID="lbstatate" CssClass="col-sm-3 control-label" runat="server" Text="Status" />
                                                        <div class="col-sm-8">
                                                            <asp:DropDownList ID="ddStatusUpdate" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("type_status") %>'>
                                                                <asp:ListItem Value="1" Text="Online" />
                                                                <asp:ListItem Value="0" Text="Offline" />
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <div class="col-sm-2 col-sm-offset-10">
                                                            <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success btn-sm" runat="server" ValidationGroup="Save_edit" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>


                                        </EditItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ประเภทเครื่องจักร" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lbnameen" runat="server" Text='<%# Eval("NameEN") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="รหัสกลุ่มเครื่องจักร" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lbtypecode" runat="server" Text='<%# Eval("NameTypecode") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="รหัสกลุ่ม" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lbgroup" runat="server" Text='<%# Eval("NameGroupcode") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Point จุดตรวจ(th)" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lbname" runat="server" Text='<%# Eval("type_name_th") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Point จุดตรวจ(en)" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lbcode" runat="server" Text='<%# Eval("type_name_en") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lbstatus" runat="server" Text='<%# Eval("TypeStatusDetail") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                            <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDel" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("m0tyidx") %>'><i class="fa fa-trash"></i></asp:LinkButton>
                                        </ItemTemplate>

                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>



                        </div>
                    </div>
                </asp:View>
            </asp:MultiView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

