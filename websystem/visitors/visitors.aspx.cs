using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System.IO;

using System.Drawing;
using DotNet.Highcharts;
using DotNet.Highcharts.Options;
using DotNet.Highcharts.Helpers;

using System.Globalization;

public partial class websystem_visitors_visitors : System.Web.UI.Page
{
    #region initial function/data
    function_tool _funcTool = new function_tool();
    data_visitors _data_visitors = new data_visitors();
    data_employee _data_employee = new data_employee();
    data_times _data_times = new data_times();
    data_geo _data_geo = new data_geo();

    List<tm_group_detail> _data_group_selected = new List<tm_group_detail>();
    List<vm_visitors_doc_detail_u1> _data_visitor_selected = new List<vm_visitors_doc_detail_u1>();

    DateTimeFormatInfo dateTimeInfo = new DateTimeFormatInfo();

    int _emp_idx = 0;
    int _rpos_idx = 0;
    int _temp_idx = 0;
    int _temp_sum = 0;
    int _temp_value = 0;
    string _temp_data = "";
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _local_json = String.Empty;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    static string _urlGetViewEmployeeListSmall = _serviceUrl + ConfigurationManager.AppSettings["urlGetViewEmployeeListSmall"];

    static string _urlExGetGeoList = _serviceUrl + ConfigurationManager.AppSettings["urlExGetGeoList"];

    static string _urlTmGetGroup = _serviceUrl + ConfigurationManager.AppSettings["urlTmGetGroup"];
    static string _urlVmGetVisitorType = _serviceUrl + ConfigurationManager.AppSettings["urlVmGetVisitorType"];
    static string _urlVmSetTransaction = _serviceUrl + ConfigurationManager.AppSettings["urlVmSetTransaction"];
    static string _urlVmGetTransaction = _serviceUrl + ConfigurationManager.AppSettings["urlVmGetTransaction"];
    static string _urlsend_email_visitors = _serviceUrl + ConfigurationManager.AppSettings["urlsend_email_visitors"];
    static string _urlVmGetNoti = _serviceUrl + ConfigurationManager.AppSettings["urlVmGetNoti"];
    static string _urlVmGetReports = _serviceUrl + ConfigurationManager.AppSettings["urlVmGetReports"];
    static string _urlVmGetReportsPermission = _serviceUrl + ConfigurationManager.AppSettings["urlVmGetReportsPermission"];

    #endregion initial function/data

    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = _funcTool.convertToInt(Session["emp_idx"].ToString());
        search_key_employee _search_key = new search_key_employee();
        _search_key.s_emp_idx = _emp_idx.ToString();
        _data_employee.search_key_emp_list = new search_key_employee[1];
        _data_employee.search_key_emp_list[0] = _search_key;
        _data_employee = getViewEmployeeList(_data_employee);

        _rpos_idx = _data_employee.employee_list_small[0].rpos_idx;

        //-- permission list
        _data_employee = new data_employee();
        _data_employee = callServicePostEmployee(_urlVmGetReportsPermission, _data_employee);

        if(_data_employee.employee_list_small != null)
        {
            foreach(var item in _data_employee.employee_list_small)
            {
                if(item.rpos_idx == _rpos_idx)
                {
                    lbReport.Visible = true;
                    break;
                }
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // _emp_idx = _funcTool.convertToInt(Session["emp_idx"].ToString());

        if (!IsPostBack)
        {
            initPage();
        }

        //-- trigger
        linkBtnTrigger(lbReport);
        LinkButton lbReportSearch = (LinkButton)fvReportSearch.FindControl("lbReportSearch");
        LinkButton lbReportReset = (LinkButton)fvReportSearch.FindControl("lbReportReset");
        LinkButton lbReportExport = (LinkButton)fvReportSearch.FindControl("lbReportExport");
        linkBtnTrigger(lbReportSearch);
        linkBtnTrigger(lbReportReset);
        linkBtnTrigger(lbReportExport);
    }

    #region event command
    protected void navCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {
            case "navList":
                setActiveTab("viewList", 0);
                break;
            case "navCreate":
                setActiveTab("viewCreate", 0);
                break;
            case "navApprove":
                setActiveTab("viewApprove", 0);
                break;
            case "navApprove_zone":
                mvSystem.SetActiveView((View)mvSystem.FindControl("viewApprove"));
                setActiveTabBar("viewApprove_zone");
                ViewState["mode"] = "approve_zone";
                ShowDataIndexApp();
                break;
            case "navReport":
                setActiveTab("viewReport", 0);
                break;
        }
    }

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {
            case "cmdItemInsert":

                GridView gvVisitorList = (GridView)fvDocCreate.FindControl("gvVisitorList");
                DropDownList ddlIdentifyType = (DropDownList)fvDocCreate.FindControl("ddlIdentifyType");
                TextBox tbVisitCardId = (TextBox)fvDocCreate.FindControl("tbVisitCardId");
                TextBox tbVisitPhoneNo = (TextBox)fvDocCreate.FindControl("tbVisitPhoneNo");
                TextBox tbVisitFirstname = (TextBox)fvDocCreate.FindControl("tbVisitFirstname");
                TextBox tbVisitLastname = (TextBox)fvDocCreate.FindControl("tbVisitLastname");

                if (ViewState["VisitorSelected"] != null)
                {
                    _data_visitor_selected = (List<vm_visitors_doc_detail_u1>)ViewState["VisitorSelected"];
                    var v_dt = _data_visitor_selected.Where(l => l.visit_identify_no == tbVisitCardId.Text.Trim()).FirstOrDefault();
                    if (v_dt != null)
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูล "+ ddlIdentifyType.Items[_funcTool.convertToInt(ddlIdentifyType.SelectedValue)].Text + " " + tbVisitCardId.Text.Trim() + " แล้ว .!');", true);
                        return;
                    }
                    else
                    {
                        vm_visitors_doc_detail_u1 _visitor_list = new vm_visitors_doc_detail_u1();
                        _visitor_list.visit_identify_idx = _funcTool.convertToInt(ddlIdentifyType.SelectedValue);
                        _visitor_list.visit_identify_no = tbVisitCardId.Text.Trim();
                        _visitor_list.visit_phone_no = tbVisitPhoneNo.Text.Trim();
                        _visitor_list.visit_firstname = tbVisitFirstname.Text.Trim();
                        _visitor_list.visit_lastname = tbVisitLastname.Text.Trim();
                        _data_visitor_selected.Add(_visitor_list);
                    }
                }
                else
                {
                    vm_visitors_doc_detail_u1 _visitor_list = new vm_visitors_doc_detail_u1();
                    _visitor_list.visit_identify_idx = _funcTool.convertToInt(ddlIdentifyType.SelectedValue);
                    _visitor_list.visit_identify_no = tbVisitCardId.Text.Trim();
                    _visitor_list.visit_phone_no = tbVisitPhoneNo.Text.Trim();
                    _visitor_list.visit_firstname = tbVisitFirstname.Text.Trim();
                    _visitor_list.visit_lastname = tbVisitLastname.Text.Trim();
                    _data_visitor_selected.Add(_visitor_list);
                }
                // clear data
                tbVisitCardId.Text = String.Empty;
                tbVisitPhoneNo.Text = String.Empty;
                tbVisitFirstname.Text = String.Empty;
                tbVisitLastname.Text = String.Empty;

                ViewState["VisitorSelected"] = _data_visitor_selected;
                setGridData(gvVisitorList, ViewState["VisitorSelected"]);

                break;
            case "cmdItemDelete":
                GridView gvVisitorList1 = (GridView)fvDocCreate.FindControl("gvVisitorList");
                if (ViewState["VisitorSelected"] != null)
                {
                    _temp_data = cmdArg;
                    _data_visitor_selected = (List<vm_visitors_doc_detail_u1>)ViewState["VisitorSelected"];
                    _data_visitor_selected.RemoveAll(l => l.visit_identify_no == _temp_data);
                }
                ViewState["VisitorSelected"] = _data_visitor_selected;
                setGridData(gvVisitorList1, ViewState["VisitorSelected"]);
                break;
            case "cmdCancel":
                if (ViewState["mode"].ToString() == "approve_leader")
                {
                    setActiveTab("viewApprove", 0);
                    SETFOCUS.Focus();
                }
                else if (ViewState["mode"].ToString() == "approve_zone")
                {
                    mvSystem.SetActiveView((View)mvSystem.FindControl("viewApprove"));
                    setActiveTabBar("viewApprove_zone");
                    ViewState["mode"] = "approve_zone";
                    ShowDataIndexApp();
                    SETFOCUS.Focus();
                }
                else
                {
                    initPage();
                }

                break;
            case "cmdSave":
                TextBox tbVisitDate = (TextBox)fvDocCreate.FindControl("tbVisitDate");
                TextBox tbVisitTitle = (TextBox)fvDocCreate.FindControl("tbVisitTitle");
                TextBox tbVisitDetail = (TextBox)fvDocCreate.FindControl("tbVisitDetail");
                CheckBox cbFlagSafety = (CheckBox)fvDocCreate.FindControl("cbFlagSafety");
                DropDownList ddlEmpList = (DropDownList)fvDocCreate.FindControl("ddlEmpList");
                DropDownList ddlVisitorType = (DropDownList)fvDocCreate.FindControl("ddlVisitorType");
                TextBox tbVehiclePlateNo = (TextBox)fvDocCreate.FindControl("tbVehiclePlateNo");
                DropDownList ddlProvince = (DropDownList)fvDocCreate.FindControl("ddlProvince");
                TextBox tbSafetyComment = (TextBox)fvDocCreate.FindControl("tbSafetyComment");
                _data_group_selected = (List<tm_group_detail>)ViewState["GroupSelected"];
                _data_visitor_selected = (List<vm_visitors_doc_detail_u1>)ViewState["VisitorSelected"];

                HiddenField hfU0Idx = (HiddenField)fvDocCreate.FindControl("hfU0Idx");
                HiddenField hfNodeIdx = (HiddenField)fvDocCreate.FindControl("hfNodeIdx");
                HiddenField hfActorIdx = (HiddenField)fvDocCreate.FindControl("hfActorIdx");
                HiddenField hfflag_loading = (HiddenField)fvDocCreate.FindControl("hfflag_loading");
                HiddenField hfflag_walkin = (HiddenField)fvDocCreate.FindControl("hfflag_walkin");
                HiddenField hfflag_arrived = (HiddenField)fvDocCreate.FindControl("hfflag_arrived");
                GridView gvGroupList = (GridView)fvDocCreate.FindControl("gvGroupList");
                GridView gvVisitorList_save = (GridView)fvDocCreate.FindControl("gvVisitorList");

                // document : vm_visitors_doc_detail_u0
                vm_visitors_doc_detail_u0 _u0_doc = new vm_visitors_doc_detail_u0();
                _u0_doc.u0_idx = _funcTool.convertToInt(hfU0Idx.Value);
                _u0_doc.emp_idx = _emp_idx;
                _u0_doc.visitor_type_idx = _funcTool.convertToInt(ddlVisitorType.SelectedValue.ToString());
                _u0_doc.visit_date = tbVisitDate.Text.Trim();
                _u0_doc.visit_title = tbVisitTitle.Text.Trim();
                _u0_doc.visit_detail = tbVisitDetail.Text.Trim();
                _u0_doc.visit_emp_idx = _funcTool.convertToInt(ddlEmpList.SelectedValue.ToString());
                // flag_safety
                if(cbFlagSafety.Checked)
                {
                    _u0_doc.flag_safety = 1;
                }
                // safety comment
                //if (hfNodeIdx.Value == "6")
                //{
                if (tbSafetyComment != null)
                {
                    _u0_doc.safety_comment = tbSafetyComment.Text.Trim();
                }

                //}
                // flag_vehicle
                if (tbVehiclePlateNo.Text != String.Empty)
                {
                    _u0_doc.flag_vehicle = 1;
                }
                _u0_doc.node_idx = _funcTool.convertToInt(hfNodeIdx.Value);
                _u0_doc.actor_idx = _funcTool.convertToInt(hfActorIdx.Value);
                if ((_funcTool.convertToInt(hfNodeIdx.Value) == 4)
                    && (_funcTool.convertToInt(hfActorIdx.Value) == 5)
                    && (_funcTool.convertToInt(hfflag_loading.Value) == 1))
                {
                    _u0_doc.decision_idx = 19;
                }
                else
                {
                    _u0_doc.decision_idx = _funcTool.convertToInt(cmdArg);
                }

                if ((_funcTool.convertToInt(hfNodeIdx.Value) == 2)
                    && (_funcTool.convertToInt(hfActorIdx.Value) == 3)
                    && (_funcTool.convertToInt(hfflag_walkin.Value) == 1))
                {
                    _u0_doc.flag_walkin = 1;
                    _u0_doc.visitor_type_idx = int.Parse(ddlVisitorType.SelectedValue);
                }
                else
                {
                    _u0_doc.flag_walkin = 0;
                }

                if ((_funcTool.convertToInt(hfNodeIdx.Value) == 2)
                    && (_funcTool.convertToInt(hfActorIdx.Value) == 3))
                {
                    int icount = 0;
                    foreach (GridViewRow gridrow in gvGroupList.Rows)
                    {
                        CheckBox cbGroupSelected = (CheckBox)gridrow.FindControl("cbGroupSelected");
                        Label lbm0_idx = (Label)gridrow.FindControl("lbm0_idx");
                        CheckBox cbGroupSel = (CheckBox)gridrow.FindControl("cbGroupSel");
                        
                        if (cbGroupSelected.Checked == true)
                        {
                            icount++;
                        }
                    }
                    _u0_doc.decision_idx = _funcTool.convertToInt(cmdArg);
                    //
                    switch (hfflag_walkin.Value)
                    {
                        case "0":
                            if (icount == 0 && _funcTool.convertToInt(cmdArg) == 2) 
                            {
                                _u0_doc.decision_idx = 21;
                            }
                            break;
                        case "1":
                            if (icount == 0 && _funcTool.convertToInt(cmdArg) == 2)
                            {
                                _u0_doc.decision_idx = 22;
                            }
                            break;
                    }
                }
                else if (
                       (_funcTool.convertToInt(hfNodeIdx.Value) == 3)
                    && (_funcTool.convertToInt(hfActorIdx.Value) == 4)
                    && (_funcTool.convertToInt(cmdArg) == 4)
                    && (_funcTool.convertToInt(hfflag_arrived.Value) == 0)
                    && (_funcTool.convertToInt(hfflag_walkin.Value) == 0)
                    )
                {
                    _u0_doc.decision_idx = 23;
                }

                _data_visitors.vm_visitors_doc_list_u0 = new vm_visitors_doc_detail_u0[1];
                _data_visitors.vm_visitors_doc_list_u0[0] = _u0_doc;

                // visitor list : vm_visitors_doc_detail_u1
                if ((hfNodeIdx.Value == "5") && (hfActorIdx.Value == "6"))
                {
                    _data_visitors.vm_visitors_doc_list_u1 = new vm_visitors_doc_detail_u1[gvVisitorList_save.Rows.Count];
                    int i = 0;
                    foreach (GridViewRow gridrow in gvVisitorList_save.Rows)
                    {
                        Label lb_u1_idx = (Label)gridrow.FindControl("lb_u1_idx");
                        TextBox lblVistorCardNo = (TextBox)gridrow.FindControl("lblVistorCardNo");

                        vm_visitors_doc_detail_u1 _group_list = new vm_visitors_doc_detail_u1();
                        _group_list.u1_idx = int.Parse(lb_u1_idx.Text);
                        _group_list.visit_card_ex_no = lblVistorCardNo.Text.Trim();
                        _data_visitors.vm_visitors_doc_list_u1[i] = _group_list;

                        i++;
                    }
                }
                else
                {
                    if (_data_visitor_selected != null)
                    {
                        _data_visitors.vm_visitors_doc_list_u1 = new vm_visitors_doc_detail_u1[_data_visitor_selected.Count];

                        int i = 0;
                        foreach (vm_visitors_doc_detail_u1 _item in _data_visitor_selected)
                        {
                            vm_visitors_doc_detail_u1 _visitor_list = new vm_visitors_doc_detail_u1();
                            _visitor_list.visit_firstname = _item.visit_firstname;
                            _visitor_list.visit_lastname = _item.visit_lastname;
                            _visitor_list.visit_identify_no = _item.visit_identify_no;
                            _visitor_list.visit_phone_no = _item.visit_phone_no;

                            _data_visitors.vm_visitors_doc_list_u1[i] = _visitor_list;
                            i++;
                        }
                    }
                    else
                    {
                        if ((hfU0Idx.Value == "0") 
                            //||
                            //((_funcTool.convertToInt(hfNodeIdx.Value) == 2)
                            //&& (_funcTool.convertToInt(hfActorIdx.Value) == 3)
                            //&& (_funcTool.convertToInt(hfflag_walkin.Value) == 1))
                            )
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณากรอกผู้มาติดต่อ .!');", true);
                            return;
                        }
                    }
                }

                if ((_funcTool.convertToInt(hfNodeIdx.Value) == 2)
                             && (_funcTool.convertToInt(hfActorIdx.Value) == 3)
                             && (_funcTool.convertToInt(hfflag_walkin.Value) == 1))
                {
                    if(int.Parse(ddlVisitorType.SelectedValue) <= 0)
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาเลือกประเภทผู้มาติดต่อ .!');", true);
                        return;
                    }
                }

                // visitor vehicle : vm_visitors_doc_detail_u2
                vm_visitors_doc_detail_u2 _vehicle_list = new vm_visitors_doc_detail_u2();
                _vehicle_list.vehicle_plate_no = tbVehiclePlateNo.Text.Trim();
                _vehicle_list.vehicle_plate_province = _funcTool.convertToInt(ddlProvince.SelectedValue.ToString());

                _data_visitors.vm_visitors_doc_list_u2 = new vm_visitors_doc_detail_u2[1];
                _data_visitors.vm_visitors_doc_list_u2[0] = _vehicle_list;

                // area list : vm_visitors_doc_detail_u3
                if (
                    (ViewState["mode"].ToString() == "approve_zone")
                    ||
                    (cmdArg == "18")
                    )
                {
                    _data_visitors.vm_visitors_doc_list_u3 = new vm_visitors_doc_detail_u3[gvGroupList.Rows.Count];
                    int i = 0,i1=0;
                    foreach (GridViewRow gridrow in gvGroupList.Rows)
                    {
                        CheckBox cbGroupSelected = (CheckBox)gridrow.FindControl("cbGroupSelected");
                        Label lbm0_idx = (Label)gridrow.FindControl("lbm0_idx");
                        CheckBox cbGroupSel = (CheckBox)gridrow.FindControl("cbGroupSel");

                        vm_visitors_doc_detail_u3 _group_list = new vm_visitors_doc_detail_u3();
                        _group_list.group_m0_idx = 0;
                        _group_list.approve_status = 0;
                        if (cbGroupSelected.Checked == true)
                        {
                            _group_list.group_m0_idx = int.Parse(lbm0_idx.Text);
                            if (cbGroupSel.Checked == true)
                            {
                                _group_list.approve_status = 1;
                            }
                            else
                            {
                                _group_list.approve_status = 2;
                            }

                        }
                        if (cbGroupSelected.Enabled == true)
                        {
                            if (cbGroupSelected.Checked == true)
                            {
                                i1++;
                            }
                        }
                        _data_visitors.vm_visitors_doc_list_u3[i] = _group_list;
                        i++;
                    }
                    if ((i1 == 0) && (cmdArg == "18"))
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาเลือก Zone .!');", true);
                        return;
                    }
                }
                else
                {
                    if (_data_group_selected != null)
                    {
                        _data_visitors.vm_visitors_doc_list_u3 = new vm_visitors_doc_detail_u3[_data_group_selected.Count];

                        int i = 0;
                        foreach (tm_group_detail _item in _data_group_selected)
                        {
                            vm_visitors_doc_detail_u3 _group_list = new vm_visitors_doc_detail_u3();
                            _group_list.group_m0_idx = _item.m0_idx;
                            _data_visitors.vm_visitors_doc_list_u3[i] = _group_list;
                            i++;
                        }
                    }
                    else
                    {
                        if ((hfU0Idx.Value == "0") ||
                            ((_funcTool.convertToInt(hfNodeIdx.Value) == 2)
                            && (_funcTool.convertToInt(hfActorIdx.Value) == 3)
                            && (_funcTool.convertToInt(hfflag_walkin.Value) == 1))
                            )
                        {
                            // ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาเลือก Zone .!');", true);
                            // return;
                        }
                    }
                }

                // debug
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_visitors));
                _data_visitors = callServicePostVisitors(_urlVmSetTransaction, _data_visitors);
                //
                if (_data_visitors.return_code == 0)
                {
                    //start send email
                    //if(_data_visitors.vm_visitors_sendemail_list != null)
                    //{
                    //    foreach(var item in _data_visitors.vm_visitors_sendemail_list)
                    //    {
                    try
                    {
                        data_visitors dtvisitors = new data_visitors();
                        vm_visitors_sendemail_detail _sendemail = new vm_visitors_sendemail_detail();
                        dtvisitors.vm_visitors_sendemail_list = new vm_visitors_sendemail_detail[1];
                        _sendemail.u0_idx = _data_visitors.return_idx;
                        _sendemail.node_idx = _data_visitors.return_node_idx;
                        _sendemail.actor_idx = _data_visitors.return_actor_idx;
                        // _sendemail.decision_idx = _funcTool.convertToInt(cmdArg);
                        dtvisitors.vm_visitors_sendemail_list[0] = _sendemail;
                        callServicePostVisitors(_urlsend_email_visitors, dtvisitors);
                    }
                    catch { }

                    //    }
                    //}

                    //end send email

                    if (ViewState["mode"].ToString() == "approve_leader")
                    {
                        setActiveTab("viewApprove", 0);
                        SETFOCUS.Focus();
                    }
                    else if (ViewState["mode"].ToString() == "approve_zone")
                    {
                        mvSystem.SetActiveView((View)mvSystem.FindControl("viewApprove"));
                        setActiveTabBar("viewApprove_zone");
                        ShowDataIndexApp();
                        SETFOCUS.Focus();
                    }
                    else
                    {
                        initPage();
                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('error code " + _data_visitors.return_code + ": " + _data_visitors.return_msg + "')", true);
                }
                break;
            case "cmdEdit":
                setActiveTab("viewCreate", _funcTool.convertToInt(cmdArg));
                break;
            case "cmdEdit_approve":
                if (ViewState["mode"].ToString() == "approve_leader")
                {

                    if (ShowDataDetail(_funcTool.convertToInt(cmdArg)) == true)
                    {
                        mvSystem.SetActiveView((View)mvSystem.FindControl("viewCreate"));
                        setActiveTabBar("viewApprove");
                    }
                    else
                    {
                        setActiveTab("viewApprove", 0);
                    }
                }
                else if (ViewState["mode"].ToString() == "approve_zone")
                {
                    if (ShowDataDetail(_funcTool.convertToInt(cmdArg)) == true)
                    {
                        mvSystem.SetActiveView((View)mvSystem.FindControl("viewCreate"));
                        setActiveTabBar("viewApprove_zone");
                    }
                    else
                    {
                        mvSystem.SetActiveView((View)mvSystem.FindControl("viewApprove"));
                        setActiveTabBar("viewApprove_zone");
                        ShowDataIndexApp();
                    }
                }
                break;
            case "cmdSearch":
                ShowDataIndexList();
                break;
            case "cmdReset":
                setSearchDefault();
                break;
            case "cmdSearch_app":
                ShowDataIndexApp();
                break;
            case "cmdReset_app":
                setSearchDefault_app();
                break;
            case "cmdReportSearch":
                DropDownList ddlDateType = (DropDownList)fvReportSearch.FindControl("ddlDateType");
                DropDownList ddlReportType = (DropDownList)fvReportSearch.FindControl("ddlReportType");
                TextBox tbStartDate = (TextBox)fvReportSearch.FindControl("tbStartDate");
                TextBox tbEndDate = (TextBox)fvReportSearch.FindControl("tbEndDate");
                DropDownList ddlReportOrg = (DropDownList)fvReportSearch.FindControl("ddlReportOrg");
                DropDownList ddlReportDept = (DropDownList)fvReportSearch.FindControl("ddlReportDept");
                DropDownList ddlReportSec = (DropDownList)fvReportSearch.FindControl("ddlReportSec");
                DropDownList ddlReportEmpList = (DropDownList)fvReportSearch.FindControl("ddlReportEmpList");
                // DropDownList ddlReportVisitorType = (DropDownList)fvReportSearch.FindControl("ddlReportVisitorType");
                CheckBoxList cblReportVisitorType = (CheckBoxList)fvReportSearch.FindControl("cblReportVisitorType");
                // DropDownList ddlReportGroup = (DropDownList)fvReportSearch.FindControl("ddlReportGroup");
                CheckBoxList cblReportGroup = (CheckBoxList)fvReportSearch.FindControl("cblReportGroup");

                vm_visitors_report_search_detail _report_search = new vm_visitors_report_search_detail();
                _report_search.s_date_type = ddlDateType.SelectedValue; 
                _report_search.s_report_type = ddlReportType.SelectedValue; 
                _report_search.s_start_date = tbStartDate.Text.Trim();
                _report_search.s_end_date = tbEndDate.Text.Trim();
                _report_search.s_report_org = ddlReportOrg.SelectedValue;
                _report_search.s_report_dept = ddlReportDept.SelectedValue;
                _report_search.s_report_sec = ddlReportSec.SelectedValue;
                _report_search.s_report_emp_idx = ddlReportEmpList.SelectedValue;

                string _vType = "";
                for (int i = 0; i < cblReportVisitorType.Items.Count; i++)
                {
                    if (cblReportVisitorType.Items[i].Selected)
                    {
                        _vType += cblReportVisitorType.Items[i].Value + ",";
                    }
                }
                _vType = _vType.Length > 1 ? _vType.Substring(0, _vType.Length-1) : "";
                _report_search.s_visitor_type = _vType; //ddlReportVisitorType.SelectedValue;

                string _vGroup = "";
                for (int i = 0; i < cblReportGroup.Items.Count; i++)
                {
                    if (cblReportGroup.Items[i].Selected)
                    {
                        _vGroup += cblReportGroup.Items[i].Value + ",";
                    }
                }
                _vGroup = _vGroup.Length > 1 ? _vGroup.Substring(0, _vGroup.Length-1) : "";
                _report_search.s_group_idx = _vGroup; //ddlReportGroup.SelectedValue;

                _data_visitors.vm_visitors_report_search_list = new vm_visitors_report_search_detail[1];
                _data_visitors.vm_visitors_report_search_list[0] = _report_search;

                // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_visitors));
                _data_visitors = callServicePostVisitors(_urlVmGetReports, _data_visitors);
                ViewState["ReportList"] = _data_visitors;

                setGridData(gvReportSearch1, null);
                setGridData(gvReportSearch2, null);
                litReportSearch1.Visible = false;
                switch(ddlReportType.SelectedValue)
                {
                    case "1": case "3":
                        if(_data_visitors.vm_visitors_doc_list_u0 != null)
                        {
                            setGridData(gvReportSearch1, _data_visitors.vm_visitors_doc_list_u0);
                            litReportSearch1.Visible = true;

                            // set color for graph
                            string[] c_color = new string[10] { "#7cb5ec", "#aa4643", "#90ed7d", "#f7a35c", "#8085e9", "#f15c80", "#e4d354", "#2b908f", "#f45b5b", "#91e8e1" };

                            // set graph title
                            string c_title = "รายงานวันที่ " + tbStartDate.Text.Trim();
                            c_title += (tbEndDate.Text.Trim() != "") ? " ถึงวันที่ " + tbEndDate.Text.Trim() : "";

                            // prepare type list
                            data_visitors _data_type = (data_visitors)ViewState["visitor_type"];
                            string[] v_type = new string[_data_type.vm_visitors_type_list_m0.Length];
                            // check number of type. not over than 10(c_color)
                            if(v_type.Length > c_color.Length + 1){ return; }
                            int i = 0;
                            foreach(var item in _data_type.vm_visitors_type_list_m0)
                            {
                                v_type[i] = item.visitor_type_name;
                                i++;
                            }

                            // prepare date list
                            var select_date = (from x in _data_visitors.vm_visitors_doc_list_u0
                                                .GroupBy(x => x.visit_date)
                                                select x);
                            int count_date = select_date.Count();
                            string[] v_date = new string[count_date];
                            
                            i = 0;
                            foreach(var item in select_date.ToList())
                            {
                                try { v_date[i] = item.ElementAt(0).visit_date; }
                                catch { v_date[i] = "0"; };
                                i++;
                            }

                            // prepare data list
                            object[,] v_zcount = new object[v_type.Length,v_date.Length];
                            for(int j = 0; j < v_type.Length; j++)
                            {
                                for(int k = 0; k < v_date.Length; k++)
                                {
                                    var select_value = (from x in _data_visitors.vm_visitors_doc_list_u0
                                                        .Where(x => x.visit_date == v_date[k] &&
                                                        x.visitor_type_name == v_type[j])
                                                        select x).ToList();

                                    try { v_zcount[j,k] = select_value.ElementAt(0).zcount; }
                                    catch { v_zcount[j,k] = "0"; };
                                }
                            }

                            // convert month to text
                            if(ddlReportType.SelectedValue == "3")
                            {
                                i = 0;
                                foreach(var item in v_date)
                                {
                                    string[] _temp_split = item.Split('-');
                                    v_date[i] = dateTimeInfo.GetAbbreviatedMonthName(_funcTool.convertToInt(_temp_split[0]));
                                    i++;
                                }
                            }

                            // create graph
                            Highcharts chart = new Highcharts("chart");

                            Series[] s_list = new Series[v_type.Length];
                            for(int m = 0; m < v_type.Length; m++)
                            {
                                object[] v_display = new object[v_date.Length];
                                for(int n = 0; n < v_date.Length; n++)
                                {
                                    v_display[n] = v_zcount[m,n];
                                }

                                s_list[m] = new Series {
                                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                                    Name = v_type[m],
                                    Data = new Data(v_display),
                                    Color = System.Drawing.ColorTranslator.FromHtml(c_color[m])};
                            }

                            chart.SetTitle(new Title { Text = c_title });
                            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Count" } } });
                            chart.SetXAxis(new XAxis { Categories = v_date });
                            chart.SetPlotOptions(new PlotOptions { Series = new PlotOptionsSeries { DataLabels = new PlotOptionsSeriesDataLabels { Enabled = true } } } );
                            chart.SetSeries(
                                s_list
                            );
                            litReportSearch1.Text = chart.ToHtmlString();
                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่พบข้อมูลที่ต้องการค้นหาค่ะ');", true);
                        }
                        break;
                    case "2":
                        if(_data_visitors.vm_visitors_doc_list_u0 != null)
                        {
                            setGridData(gvReportSearch2, _data_visitors.vm_visitors_doc_list_u0);
                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่พบข้อมูลที่ต้องการค้นหาค่ะ');", true);
                        }
                        break;
                }
                break;
            case "cmdReportReset":
                setFormData(fvReportSearch, FormViewMode.Insert, null);
                setGridData(gvReportSearch1, null);
                setGridData(gvReportSearch2, null);
                litReportSearch1.Visible = false;

                setActiveView("viewReport", 0);
                break;
            case "cmdReportExport":
                // ViewState["ReportList"]
                DropDownList ddlExportReportType = (DropDownList)fvReportSearch.FindControl("ddlReportType");
                data_visitors report_data = (data_visitors)ViewState["ReportList"];
                DataTable tableReport = new DataTable();
                int _count_row = 0;
                int _temp_sum = 0;

                if(report_data == null)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่พบข้อมูลที่ต้องการ Export ค่ะ');", true);
                    return;
                }

                switch(ddlExportReportType.SelectedValue)
                {
                    case "1": case "3":
                        if(report_data.vm_visitors_doc_list_u0 != null)
                        {
                            tableReport.Columns.Add("วันที่เข้าพบ", typeof(String));
                            tableReport.Columns.Add("ประเภทผู้มาติดต่อ", typeof(String));
                            tableReport.Columns.Add("จำนวน (รายการ)", typeof(String));

                            foreach (var temp_row in report_data.vm_visitors_doc_list_u0)
                            {
                                DataRow add_row = tableReport.NewRow();

                                // convert month
                                string _temp_visit_date = temp_row.visit_date.ToString();
                                if(ddlExportReportType.SelectedValue == "3")
                                {
                                        string[] _temp_split = _temp_visit_date.Split('-');
                                        _temp_visit_date = dateTimeInfo.GetAbbreviatedMonthName(_funcTool.convertToInt(_temp_split[0])) + "-" + _temp_split[1];
                                }

                                add_row[0] = _temp_visit_date;
                                add_row[1] = temp_row.visitor_type_name.ToString();
                                add_row[2] = temp_row.zcount.ToString();

                                _temp_sum = _temp_sum + temp_row.zcount;

                                tableReport.Rows.InsertAt(add_row, _count_row++);
                            }

                            DataRow sum_row = tableReport.NewRow();
                            sum_row[2] = _temp_sum.ToString("#,###");
                            tableReport.Rows.InsertAt(sum_row, _count_row + 1);

                            WriteExcelWithNPOI(tableReport, "xls", "report-visitor");
                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่พบข้อมูลที่ต้องการ Export ค่ะ');", true);
                        }
                        break;
                    case "2":
                        if(report_data.vm_visitors_doc_list_u0 != null)
                        {
                            tableReport.Columns.Add("วันที่ต้องการเข้าพบ", typeof(String));
                            tableReport.Columns.Add("ผู้ที่ต้องการเข้าพบ", typeof(String));
                            tableReport.Columns.Add("ประเภทผู้มาติดต่อ", typeof(String));
                            tableReport.Columns.Add("วันที่เข้าพบ", typeof(String));
                            tableReport.Columns.Add("หัวข้อ", typeof(String));
                            tableReport.Columns.Add("ชื่อผู้มาติดต่อ", typeof(String));
                            tableReport.Columns.Add("หมายเลขบัตรประชาชน", typeof(String));
                            tableReport.Columns.Add("หมายเลขทะเบียนรถ", typeof(String));

                            foreach (var temp_row in report_data.vm_visitors_doc_list_u0)
                            {
                                DataRow add_row = tableReport.NewRow();

                                add_row[0] = temp_row.visit_date.ToString();
                                add_row[1] = temp_row.visit_emp_name_th.ToString();
                                add_row[2] = temp_row.visitor_type_name.ToString();
                                add_row[3] = temp_row.update_date.ToString();
                                add_row[4] = temp_row.visit_title.ToString();
                                add_row[5] = temp_row.visit_prefix.ToString() + "" + temp_row.visit_firstname.ToString() + " " + temp_row.visit_lastname.ToString();
                                add_row[6] = temp_row.visit_identify_no.ToString();
                                add_row[7] = temp_row.vehicle_plate_no.ToString() + " " + temp_row.vehicle_plate_province_name.ToString();

                                tableReport.Rows.InsertAt(add_row, _count_row++);
                            }

                            WriteExcelWithNPOI(tableReport, "xls", "report-visitor");
                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่พบข้อมูลที่ต้องการ Export ค่ะ');", true);
                        }
                        break;
                }
                break;
        }
    }

    protected void cbCheckedChanged(object sender, EventArgs e)
    {
        var cb = (CheckBox)sender;
        switch (cb.ID)
        {
            case "cbGroupSelected":
                string[] _val_in = cb.Text.Split('|');
                _temp_idx = _funcTool.convertToInt(_val_in[0]);
                if (ViewState["GroupSelected"] != null)
                {
                    _data_group_selected = (List<tm_group_detail>)ViewState["GroupSelected"];
                    if (cb.Checked)
                    {
                        var check_selected = _data_group_selected.Where(d =>
                        d.m0_idx == _temp_idx).FirstOrDefault();
                        if (check_selected == null)
                        {
                            tm_group_detail _group_list = new tm_group_detail();
                            _group_list.m0_idx = _temp_idx;
                            _group_list.group_name = _val_in[1];
                            _data_group_selected.Add(_group_list);
                        }
                    }
                    else
                    {
                        _data_group_selected.RemoveAll(l => l.m0_idx == _temp_idx);
                    }
                }
                else
                {
                    if (cb.Checked)
                    {
                        tm_group_detail _group_list = new tm_group_detail();
                        _group_list.m0_idx = _temp_idx;
                        _group_list.group_name = _val_in[1];
                        _data_group_selected.Add(_group_list);
                    }
                }

                ViewState["GroupSelected"] = _data_group_selected;
                break;
        }
    }
    #endregion event command

    #region RowDatabound
    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var gvName = (GridView)sender;

        switch (gvName.ID)
        {
            case "gvDocList":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HiddenField hfFlagSafety = (HiddenField)e.Row.FindControl("hfFlagSafety");
                    Literal litFlagSafety = (Literal)e.Row.FindControl("litFlagSafety");
                    if (hfFlagSafety.Value == "1")
                    {
                        litFlagSafety.Visible = true;
                    }
                }
                break;
            case "gvGroupList":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox cbGroupSelected = (CheckBox)e.Row.FindControl("cbGroupSelected");
                    Label lbapprove_status = (Label)e.Row.FindControl("lbapprove_status");
                    Label lbapprove_status1 = (Label)e.Row.FindControl("lbapprove_status1");
                    HiddenField hfM0Idx = (HiddenField)e.Row.FindControl("hfM0Idx");
                    CheckBox cbGroupSel = (CheckBox)e.Row.FindControl("cbGroupSel");
                    HiddenField hfNodeIdx = (HiddenField)fvDocCreate.FindControl("hfNodeIdx");
                    HiddenField hfActorIdx = (HiddenField)fvDocCreate.FindControl("hfActorIdx");
                    HiddenField hfflag_walkin = (HiddenField)fvDocCreate.FindControl("hfflag_walkin");

                    _temp_idx = _funcTool.convertToInt(hfM0Idx.Value);
                    cbGroupSel.Visible = false;
                    if ((hfNodeIdx.Value == "2") && (hfActorIdx.Value == "3") && hfflag_walkin.Value == "1")
                    {
                        cbGroupSelected.Enabled = true;
                    }
                    else
                    {
                        if (ViewState["document_detail"] != null)
                        {
                            _data_visitors = (data_visitors)ViewState["document_detail"];
                            if (_data_visitors.vm_visitors_doc_list_u3 != null)
                            {
                                var check_selected = _data_visitors.vm_visitors_doc_list_u3.Where(d =>
                                d.group_m0_idx == _temp_idx).FirstOrDefault();
                                if (check_selected != null)
                                {
                                    cbGroupSelected.Checked = true;
                                    lbapprove_status.Text = check_selected.approve_status.ToString();
                                    //cbGroupSel.Visible = false;
                                    if (check_selected.approve_status == 1)
                                    {
                                        lbapprove_status1.Text = "<i class='fa fa-smile-o' style='color: green' title='อนุมัติ'></i>";
                                        cbGroupSel.Checked = true;
                                    }
                                    else if (check_selected.approve_status == 2)
                                    {
                                        lbapprove_status1.Text = "<i class='fa fa-smile-o' style='color: red' title='ไม่อนุมัติ'></i>";
                                        cbGroupSel.Checked = false;
                                    }
                                }
                                else
                                {
                                    if ((hfNodeIdx.Value == "5") && (hfActorIdx.Value == "6"))
                                    {
                                        cbGroupSelected.Enabled = getPermission(int.Parse(hfNodeIdx.Value), int.Parse(hfActorIdx.Value));
                                    }
                                }
                            }
                            else
                            {
                                if ((hfNodeIdx.Value == "5") && (hfActorIdx.Value == "6"))
                                {
                                    cbGroupSelected.Enabled = getPermission(int.Parse(hfNodeIdx.Value), int.Parse(hfActorIdx.Value));
                                }
                            }

                            if ((cbGroupSelected.Checked == true) && (lbapprove_status.Text != "1") && (lbapprove_status.Text != "2"))
                            {
                                if (_data_visitors.view_vm_visitors_doc_list_u3 != null)
                                {
                                    var check_selected = _data_visitors.view_vm_visitors_doc_list_u3.Where(d =>
                                    d.group_m0_idx == _temp_idx).FirstOrDefault();
                                    cbGroupSel.Visible = false;
                                    if (check_selected != null)
                                    {
                                        if (check_selected.approve_status == 1)
                                        {
                                            lbapprove_status1.Text = "<i class='fa fa-smile-o' style='color: green' title='อนุมัติ'></i>";
                                        }
                                        else
                                        {
                                            lbapprove_status1.Text = "<i class='fa fa-smile-o' style='color: coral' title='ยังไม่อนุมัติ'></i>";
                                            cbGroupSel.Checked = true;
                                            if (ViewState["mode"] != null)
                                            {
                                                if (ViewState["mode"].ToString() == "approve_zone")
                                                {
                                                    cbGroupSel.Visible = true;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                break;
            case "gvVisitorList":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HiddenField hfNodeIdx = (HiddenField)fvDocCreate.FindControl("hfNodeIdx");
                    HiddenField hfActorIdx = (HiddenField)fvDocCreate.FindControl("hfActorIdx");
                    TextBox lblVistorCardNo = (TextBox)e.Row.FindControl("lblVistorCardNo");

                    if (lblVistorCardNo != null)
                    {
                        if ((hfNodeIdx.Value == "5") && (hfActorIdx.Value == "6"))
                        {
                            lblVistorCardNo.Enabled = true;
                        }
                        else
                        {
                            lblVistorCardNo.Enabled = false;
                        }
                    }
                }
                break;
            case "gvReportSearch1":
                // merge cell
                for (int rowIndex = gvReportSearch1.Rows.Count - 2; rowIndex >= 0; rowIndex--)
                {
                    GridViewRow gvRow = gvReportSearch1.Rows[rowIndex];
                    GridViewRow gvPreviousRow = gvReportSearch1.Rows[rowIndex + 1];
                    string dateRow = ((HiddenField)gvRow.FindControl("hfVisitDate")).Value;
                    string datePreviousRow = ((HiddenField)gvPreviousRow.FindControl("hfVisitDate")).Value;

                    if (dateRow == datePreviousRow)
                    {
                        gvRow.Cells[0].RowSpan = gvPreviousRow.Cells[0].RowSpan < 2 ? 2 : 
                                                    gvPreviousRow.Cells[0].RowSpan + 1;
                        gvPreviousRow.Cells[0].Visible = false;
                    }
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    // sum result
                    HiddenField hfCount = (HiddenField)e.Row.FindControl("hfCount");
                    _temp_sum = _temp_sum + _funcTool.convertToInt(hfCount.Value.ToString());

                    // convert month
                    DropDownList ddlReportType = (DropDownList)fvReportSearch.FindControl("ddlReportType");
                    HiddenField hfVisitDate = (HiddenField)e.Row.FindControl("hfVisitDate");
                    Label lblVisitDate = (Label)e.Row.FindControl("lblVisitDate");
                    if(ddlReportType.SelectedValue == "3")
                    {
                        string[] _temp_split = hfVisitDate.Value.Split('-');
                        lblVisitDate.Text = dateTimeInfo.GetAbbreviatedMonthName(_funcTool.convertToInt(_temp_split[0])) + "-" + _temp_split[1];
                    }
                }

                if (e.Row.RowType == DataControlRowType.Footer)
                {
                    Label lblSum = (Label)e.Row.FindControl("lblSum");
                    lblSum.Text = _temp_sum.ToString("#,###");
                }
                break;
            case "gvReportSearch2":
                // merge cell
                for (int rowIndex = gvReportSearch2.Rows.Count - 2; rowIndex >= 0; rowIndex--)
                {
                    GridViewRow gvRow = gvReportSearch2.Rows[rowIndex];
                    GridViewRow gvPreviousRow = gvReportSearch2.Rows[rowIndex + 1];
                    string dateRow = ((HiddenField)gvRow.FindControl("hfU0Idx")).Value;
                    string datePreviousRow = ((HiddenField)gvPreviousRow.FindControl("hfU0Idx")).Value;

                    if (dateRow == datePreviousRow)
                    {
                        // visit_date
                        gvRow.Cells[0].RowSpan = gvPreviousRow.Cells[0].RowSpan < 2 ? 2 : 
                                                    gvPreviousRow.Cells[0].RowSpan + 1;
                        gvPreviousRow.Cells[0].Visible = false;

                        // visit_emp_name_th
                        gvRow.Cells[1].RowSpan = gvPreviousRow.Cells[1].RowSpan < 2 ? 2 : 
                                                    gvPreviousRow.Cells[1].RowSpan + 1;
                        gvPreviousRow.Cells[1].Visible = false;

                        // visitor_type_name
                        gvRow.Cells[2].RowSpan = gvPreviousRow.Cells[2].RowSpan < 2 ? 2 : 
                                                    gvPreviousRow.Cells[2].RowSpan + 1;
                        gvPreviousRow.Cells[2].Visible = false;

                        // update_date
                        gvRow.Cells[3].RowSpan = gvPreviousRow.Cells[3].RowSpan < 2 ? 2 : 
                                                    gvPreviousRow.Cells[3].RowSpan + 1;
                        gvPreviousRow.Cells[3].Visible = false;

                        // visit_title
                        gvRow.Cells[4].RowSpan = gvPreviousRow.Cells[4].RowSpan < 2 ? 2 : 
                                                    gvPreviousRow.Cells[4].RowSpan + 1;
                        gvPreviousRow.Cells[4].Visible = false;
                        
                        // vehicle_plate_no
                        gvRow.Cells[7].RowSpan = gvPreviousRow.Cells[7].RowSpan < 2 ? 2 : 
                                                    gvPreviousRow.Cells[7].RowSpan + 1;
                        gvPreviousRow.Cells[7].Visible = false;
                    }
                }
                break;
        }
    }
    #endregion RowDatabound

    #region databound
    protected void fvDataBound(object sender, EventArgs e)
    {
        var fvName = (FormView)sender;
        switch (fvName.ID)
        {
            case "fvEmpProfile":
                break;
            case "fvDocCreate":
                DropDownList ddlEmpList = (DropDownList)fvName.FindControl("ddlEmpList");
                DropDownList ddlVisitorType = (DropDownList)fvName.FindControl("ddlVisitorType");
                DropDownList ddlProvince = (DropDownList)fvName.FindControl("ddlProvince");
                GridView gvGroupList = (GridView)fvName.FindControl("gvGroupList");
                GridView gvVisitorList = (GridView)fvName.FindControl("gvVisitorList");
                HiddenField hfNodeIdx = (HiddenField)fvName.FindControl("hfNodeIdx");
                HiddenField hfActorIdx = (HiddenField)fvName.FindControl("hfActorIdx");
                HiddenField hfflag_walkin = (HiddenField)fvDocCreate.FindControl("hfflag_walkin");

                CheckBox cbFlagSafety = (CheckBox)fvDocCreate.FindControl("cbFlagSafety");

                // check formview mode
                if (fvName.CurrentMode == FormViewMode.Insert)
                {
                    // ddlEmpList
                    HiddenField hfRdeptIdx = (HiddenField)fvEmpProfile.FindControl("hfRdeptIdx");
                    search_key_employee _search_key = new search_key_employee();
                    // _search_key.s_rdept_idx = hfRdeptIdx.Value.ToString();
                    _search_key.s_org_idx = "1";
					_search_key.s_emp_type = "2";
                    _search_key.s_emp_status = "1";
                    _data_employee.search_key_emp_list = new search_key_employee[1];
                    _data_employee.search_key_emp_list[0] = _search_key;
                    _data_employee = getViewEmployeeList(_data_employee);

                    setDdlData(ddlEmpList, _data_employee.employee_list_small, "emp_name_th", "emp_idx");
                    ddlEmpList.Items.Insert(0, new ListItem("--- ชื่อผู้ที่ต้องการเข้าพบ ---", "-1"));
                }

                if (fvName.CurrentMode == FormViewMode.Insert || fvName.CurrentMode == FormViewMode.Edit)
                {
                    // ddlVisitorType
                    search_visitor_type_detail _search_type = new search_visitor_type_detail();
                    _search_type.s_type_status = "1";
                    _data_visitors.search_visitor_type_list = new search_visitor_type_detail[1];
                    _data_visitors.search_visitor_type_list[0] = _search_type;
                    _data_visitors = getVisitorTypeList(_data_visitors);

                    // debug
                    // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_visitors));
                    setDdlData(ddlVisitorType, _data_visitors.vm_visitors_type_list_m0, "visitor_type_name", "m0_idx");
                    ddlVisitorType.Items.Insert(0, new ListItem("--- ประเภทผู้มาติดต่อ ---", "-1"));

                    // ddlProvince
                    geo_detail _province_select = new geo_detail();
                    _province_select.geography_idx = -1;
                    _data_geo.geo_list = new geo_detail[1];
                    _data_geo.geo_list[0] = _province_select;
                    _data_geo = getGeoList(_data_geo);

                    setDdlData(ddlProvince, _data_geo.geo_list, "name_th", "idx");
                    ddlProvince.Items.Insert(0, new ListItem("--- จังหวัด ---", "-1"));

                    // gvGroupList
                    _data_times.search_group_list = new search_group_detail[1];
                    search_group_detail _groupList = new search_group_detail();
                    _groupList.s_group_status = "1";
                    _data_times.search_group_list[0] = _groupList;
                    _data_times = getGroupList(_data_times);
                    setGridData(gvGroupList, _data_times.tm_group_list);
                }
                if (fvName.CurrentMode == FormViewMode.Insert)
                {
                    div_display_log.Visible = false;
                }
                if (fvName.CurrentMode == FormViewMode.Edit)
                {
                    if (ViewState["document_detail"] != null)
                    {
                        _data_visitors = (data_visitors)ViewState["document_detail"];
                        ddlProvince.Items.Insert(0, new ListItem("--- จังหวัด ---", "-1"));
                        ddlEmpList.Items.Insert(0, new ListItem(_data_visitors.vm_visitors_doc_list_u0[0].visit_emp_name_th, _data_visitors.vm_visitors_doc_list_u0[0].visit_emp_idx.ToString()));

                        // visitor type
                        HiddenField hfVisitorTypeIdx = (HiddenField)fvName.FindControl("hfVisitorTypeIdx");
                        ddlVisitorType.SelectedValue = hfVisitorTypeIdx.Value.ToString();

                        // visitor list
                        setGridData(gvVisitorList, _data_visitors.vm_visitors_doc_list_u1);

                        if (_data_visitors.vm_visitors_doc_list_u2 != null)
                        {
                            TextBox tbVehiclePlateNo = (TextBox)fvName.FindControl("tbVehiclePlateNo");
                            tbVehiclePlateNo.Text = _data_visitors.vm_visitors_doc_list_u2[0].vehicle_plate_no;
                            ddlProvince.SelectedValue = _data_visitors.vm_visitors_doc_list_u2[0].vehicle_plate_province.ToString();
                        }

                        // flag safety
                        HiddenField hfFlagSafety = (HiddenField)fvName.FindControl("hfFlagSafety");
                        int _flag_safety = _funcTool.convertToInt(hfFlagSafety.Value);

                        Panel pnNode2 = (Panel)fvName.FindControl("pnNode2");
                        Panel pnNode3 = (Panel)fvName.FindControl("pnNode3");
                        Panel pnNode4 = (Panel)fvName.FindControl("pnNode4");
                        Panel pnNode5 = (Panel)fvName.FindControl("pnNode5");
                        Panel pnNode6 = (Panel)fvName.FindControl("pnNode6");
                        Panel pnNode6Comment = (Panel)fvName.FindControl("pnNode6Comment");
                        Panel pnNode7 = (Panel)fvName.FindControl("pnNode7");
                        Panel pnNode8 = (Panel)fvName.FindControl("pnNode8");
                        Panel pnNode9 = (Panel)fvName.FindControl("pnNode9");

                        LinkButton lbN5Safety = (LinkButton)fvName.FindControl("lbN5Safety");
                        TextBox tbSafetyComment = (TextBox)fvName.FindControl("tbSafetyComment");
                        LinkButton lbN5ApproveZone = (LinkButton)fvName.FindControl("lbN5ApproveZone");
                        LinkButton lbN3Approve = (LinkButton)fvName.FindControl("lbN3Approve");
                        LinkButton lbN5NoSafety = (LinkButton)fvName.FindControl("lbN5NoSafety");
                        LinkButton lbN5RejectSafety = (LinkButton)fvName.FindControl("lbN5RejectSafety");

                        HiddenField hfzcount = (HiddenField)fvName.FindControl("hfzcount");
                        HiddenField hfvisit_emp_idx = (HiddenField)fvName.FindControl("hfvisit_emp_idx");

                        switch (hfNodeIdx.Value)
                        {
                            case "2":

                                if (ViewState["mode"] != null)
                                {
                                    if (ViewState["mode"].ToString() == "approve_leader")
                                    {
                                        pnNode2.Visible = true;
                                        pnNode6Comment.Visible = true;
                                        tbSafetyComment.Enabled = true;
                                    }
                                }
                                break;
                            case "12": // กรณีขอเพิ่ม zone

                                if (ViewState["mode"] != null)
                                {
                                    if (ViewState["mode"].ToString() == "approve_zone")
                                    {
                                        pnNode3.Visible = true;
                                        pnNode6Comment.Visible = true;
                                        tbSafetyComment.Enabled = true;
                                    }
                                }
                                break;
                            case "3":

                                if (ViewState["mode"] != null)
                                {
                                    if (ViewState["mode"].ToString() == "approve_zone")
                                    {
                                        pnNode3.Visible = true;
                                        pnNode6Comment.Visible = true;
                                        tbSafetyComment.Enabled = true;
                                    }
                                }
                                break;
                            case "4":
                                pnNode4.Visible = getPermission(int.Parse(hfNodeIdx.Value), int.Parse(hfActorIdx.Value));
                                break;
                            case "5":
                                pnNode5.Visible = getPermission(int.Parse(hfNodeIdx.Value), int.Parse(hfActorIdx.Value));
                                lbN5Safety.Visible = false;
                                pnNode6Comment.Visible = pnNode5.Visible;
                                tbSafetyComment.Enabled = pnNode5.Visible;
                                if ((int.Parse(hfNodeIdx.Value) == 5) && (int.Parse(hfActorIdx.Value) == 6))
                                {
                                    lbN5ApproveZone.Visible = true;
                                }
                                else
                                {
                                    lbN5ApproveZone.Visible = false;
                                }

                                switch (_flag_safety)
                                {
                                    case 0:
                                        lbN5Safety.Visible = false;
                                        lbN5RejectSafety.Visible = false;
                                        break;
                                    case 1:
                                        //   pnNode6Comment.Visible = true;
                                        lbN5RejectSafety.Visible = true;
                                        break;
                                }

                                if (int.Parse(hfzcount.Value) == 0)
                                {
                                    lbN5ApproveZone.Visible = false;
                                }

                                // lbN5RejectSafety.Visible = lbN5NoSafety.Visible;
                                break;
                            case "6":
                                pnNode6.Visible = true;
                                pnNode6Comment.Visible = true;
                                tbSafetyComment.Enabled = true;
                                break;
                            case "7":
                                pnNode7.Visible = false;
                                if(hfvisit_emp_idx != null)
                                {
                                    if(_funcTool.convertToInt(hfvisit_emp_idx.Value) == _emp_idx)
                                    {
                                        pnNode7.Visible = true;
                                    }
                                }
                                
                                pnNode6Comment.Visible = true;
                                tbSafetyComment.Enabled = true;
                                break;
                            case "8":
                                pnNode8.Visible = false;
                                if(hfvisit_emp_idx != null)
                                {
                                    if(_funcTool.convertToInt(hfvisit_emp_idx.Value) == _emp_idx)
                                    {
                                        pnNode8.Visible = true;
                                    }
                                }
                                
                                pnNode6Comment.Visible = true;
                                tbSafetyComment.Enabled = true;
                                break;
                        }

                        // log history
                        div_display_log.Visible = true;
                        setRepeaterData(rptLog, _data_visitors.vm_visitors_doc_list_l0);

                        // flag_safety
                        if(hfFlagSafety.Value == "1")
                        {
                            cbFlagSafety.Checked = true;
                        }
                    }

                    if ((hfNodeIdx.Value == "2") && (hfActorIdx.Value == "3") && hfflag_walkin.Value == "1")
                    {
                        ddlVisitorType.Enabled = true;
                    }
                    else
                    {
                        ddlVisitorType.Enabled = false;
                    }
                }
                break;
        }
    }
    #endregion databound

    #region drorpdown list
    protected void getOrganizationList(DropDownList ddlName)
    {
        _data_employee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _data_employee.organization_list[0] = _orgList;

        _data_employee = callServicePostEmployee(_urlGetOrganizationList, _data_employee);
        setDdlData(ddlName, _data_employee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("--- องค์กร ---", "0"));
    }

    protected void getDepartmentList(DropDownList ddlName, int _org_idx)
    {
        _data_employee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _data_employee.department_list[0] = _deptList;

        _data_employee = callServicePostEmployee(_urlGetDepartmentList, _data_employee);
        setDdlData(ddlName, _data_employee.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("--- ฝ่าย ---", "0"));
    }

    protected void getSectionList(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {
        _data_employee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _data_employee.section_list[0] = _secList;

        _data_employee = callServicePostEmployee(_urlGetSectionList, _data_employee);
        setDdlData(ddlName, _data_employee.section_list, "sec_name_th", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("--- แผนก ---", "0"));
    }

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlName = (DropDownList)sender;

        DropDownList ddlReportOrg = (DropDownList)fvReportSearch.FindControl("ddlReportOrg");
        DropDownList ddlReportDept = (DropDownList)fvReportSearch.FindControl("ddlReportDept");
        DropDownList ddlReportSec = (DropDownList)fvReportSearch.FindControl("ddlReportSec");
        DropDownList ddlReportEmpList = (DropDownList)fvReportSearch.FindControl("ddlReportEmpList");

        switch (ddlName.ID)
        {
            case "ddlReportOrg":
                getDepartmentList(ddlReportDept, _funcTool.convertToInt(ddlReportOrg.SelectedValue));
                ddlReportSec.Items.Clear();
                ddlReportSec.Items.Insert(0, new ListItem("--- แผนก ---", "0"));
                break;
            case "ddlReportDept":
                getSectionList(ddlReportSec, _funcTool.convertToInt(ddlReportOrg.SelectedValue), _funcTool.convertToInt(ddlReportDept.SelectedValue));
                break;
            case "ddlReportSec":
                search_key_employee _search_report_key = new search_key_employee();
                _search_report_key.s_org_idx = ddlReportOrg.SelectedValue;
                _search_report_key.s_rdept_idx = ddlReportDept.SelectedValue;
                _search_report_key.s_rsec_idx = ddlReportSec.SelectedValue;
                _search_report_key.s_emp_type = "2";
                _search_report_key.s_emp_status = "1";
                _data_employee.search_key_emp_list = new search_key_employee[1];
                _data_employee.search_key_emp_list[0] = _search_report_key;
                _data_employee = getViewEmployeeList(_data_employee);

                setDdlData(ddlReportEmpList, _data_employee.employee_list_small, "emp_name_th", "emp_idx");
                ddlReportEmpList.Items.Insert(0, new ListItem("--- ชื่อผู้ที่ต้องการเข้าพบ ---", "0"));
                break;
        }
    }
    #endregion drorpdown list

    #region reuse
    protected void initPage()
    {
        clearSession();
        clearViewState();

        setActiveTab("viewList", 0);

        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "scrollToTop", "window.scrollTo(0, 0)", true);
    }

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        ViewState["GroupSelected"] = null;
        ViewState["VisitorSelected"] = null;
        ViewState["ReportList"] = null;
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();

        switch (fvName.ID)
        {
            case "fvDocCreate":
                if (fvMode == FormViewMode.Edit)
                {

                }
                break;
        }
    }

    protected void setRepeaterData(Repeater rptName, Object obj)
    {
        rptName.DataSource = obj;
        rptName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setCblData(CheckBoxList cblName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        cblName.Items.Clear();
        // bind items
        cblName.DataSource = obj;
        cblName.DataTextField = _data_text;
        cblName.DataValueField = _data_value;
        cblName.DataBind();
    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    protected void setActiveView(string activeTab, int doc_idx)
    {

        mvSystem.SetActiveView((View)mvSystem.FindControl(activeTab));

        switch (activeTab)
        {
            case "viewList": //  รายการทั่วไป
                ViewState["mode"] = "peview";
                ShowDataIndexList();
                // clear document detail
                // ViewState["document_detail"] = null;
                break;
            case "viewCreate": //   สร้างรายการ
                if (doc_idx == 0)
                {
                    ViewState["mode"] = "insert";
                    div_create_heading.Visible = true;
                    search_key_employee _search_key = new search_key_employee();
                    _search_key.s_emp_idx = _emp_idx.ToString();
                    _data_employee.search_key_emp_list = new search_key_employee[1];
                    _data_employee.search_key_emp_list[0] = _search_key;
                    _data_employee = getViewEmployeeList(_data_employee);
                    setFormData(fvEmpProfile, FormViewMode.ReadOnly, _data_employee.employee_list_small);

                    div_create_form.Attributes.Add("class", "panel panel-success");
                    litCreateFormTitle.Text = "สร้างรายการ";
                    setFormData(fvDocCreate, FormViewMode.Insert, null);
                }
                else
                {
                    ViewState["mode"] = "update";
                    div_create_heading.Visible = false;
                    setFormData(fvEmpProfile, FormViewMode.ReadOnly, null);

                    div_create_form.Attributes.Add("class", "panel panel-warning");
                    litCreateFormTitle.Text = "รายละเอียดรายการ";

                    vm_visitors_doc_detail_u0 _u0_edit = new vm_visitors_doc_detail_u0();
                    _u0_edit.emp_idx = _emp_idx;
                    _u0_edit.u0_idx = doc_idx;
                    _data_visitors.vm_visitors_doc_list_u0 = new vm_visitors_doc_detail_u0[1];
                    _data_visitors.vm_visitors_doc_list_u0[0] = _u0_edit;
                    _data_visitors = callServicePostVisitors(_urlVmGetTransaction, _data_visitors);
                    ViewState["document_detail"] = _data_visitors;
                    setFormData(fvDocCreate, FormViewMode.Edit, ((data_visitors)ViewState["document_detail"]).vm_visitors_doc_list_u0);
                }
                break;
            case "viewApprove": // รายการที่รอหัวหน้างานอนุมัติ
                ViewState["mode"] = "approve_leader";
                ShowDataIndexApp();
                break;
            case "viewReport":
                DropDownList ddlReportOrg = (DropDownList)fvReportSearch.FindControl("ddlReportOrg");
                DropDownList ddlReportEmpList = (DropDownList)fvReportSearch.FindControl("ddlReportEmpList");
                // DropDownList ddlReportVisitorType = (DropDownList)fvReportSearch.FindControl("ddlReportVisitorType");
                CheckBoxList cblReportVisitorType = (CheckBoxList)fvReportSearch.FindControl("cblReportVisitorType");
                DropDownList ddlReportGroup = (DropDownList)fvReportSearch.FindControl("ddlReportGroup");
                CheckBoxList cblReportGroup = (CheckBoxList)fvReportSearch.FindControl("cblReportGroup");

                getOrganizationList(ddlReportOrg);

                // ddlReportEmpList
                search_key_employee _search_report_key = new search_key_employee();
                _search_report_key.s_org_idx = "1";
                _search_report_key.s_emp_type = "2";
                _search_report_key.s_emp_status = "1";
                _data_employee.search_key_emp_list = new search_key_employee[1];
                _data_employee.search_key_emp_list[0] = _search_report_key;
                _data_employee = getViewEmployeeList(_data_employee);

                setDdlData(ddlReportEmpList, _data_employee.employee_list_small, "emp_name_th", "emp_idx");
                ddlReportEmpList.Items.Insert(0, new ListItem("--- ชื่อผู้ที่ต้องการเข้าพบ ---", "0"));

                // ddlVisitorType
                search_visitor_type_detail _search_type = new search_visitor_type_detail();
                _search_type.s_type_status = "1";
                _data_visitors.search_visitor_type_list = new search_visitor_type_detail[1];
                _data_visitors.search_visitor_type_list[0] = _search_type;
                _data_visitors = getVisitorTypeList(_data_visitors);

                // debug
                // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_visitors));
                // setDdlData(ddlReportVisitorType, _data_visitors.vm_visitors_type_list_m0, "visitor_type_name", "m0_idx");
                // ddlReportVisitorType.Items.Insert(0, new ListItem("--- ประเภทผู้มาติดต่อ ---", "0"));
                setCblData(cblReportVisitorType, _data_visitors.vm_visitors_type_list_m0, "visitor_type_name", "m0_idx");
                ViewState["visitor_type"] = _data_visitors;

                // ddlReportGroup
                _data_times.search_group_list = new search_group_detail[1];
                search_group_detail _groupList = new search_group_detail();
                _groupList.s_group_status = "1";
                _data_times.search_group_list[0] = _groupList;
                _data_times = getGroupList(_data_times);

                // setDdlData(ddlReportGroup, _data_times.tm_group_list, "group_name", "m0_idx");
                // ddlReportGroup.Items.Insert(0, new ListItem("--- Zone ---", "0"));
                setCblData(cblReportGroup, _data_times.tm_group_list, "group_name", "m0_idx");
                ListItem li = new ListItem();
                li.Text = "ไม่เลือก Zone";
                li.Value = "0";
                cblReportGroup.Items.Add(li);
                break;
        }
    }

    protected void setActiveTab(string activeTab, int doc_idx)
    {
        setActiveView(activeTab, doc_idx);
        setActiveTabBar(activeTab);
    }

    protected data_visitors callServicePostVisitors(string _cmdUrl, data_visitors _data_visitors)
    {
        // convert to json
        _local_json = _funcTool.convertObjectToJson(_data_visitors);
        // litDebug.Text = _local_json;

        // call services
        _local_json = _funcTool.callServicePost(_cmdUrl, _local_json);

        // convert json to object
        _data_visitors = (data_visitors)_funcTool.convertJsonToObject(typeof(data_visitors), _local_json);

        return _data_visitors;
    }

    protected data_times callServicePostTimes(string _cmdUrl, data_times _data_times)
    {
        // convert to json
        _local_json = _funcTool.convertObjectToJson(_data_times);
        // litDebug.Text = _local_json;

        // call services
        _local_json = _funcTool.callServicePost(_cmdUrl, _local_json);

        // convert json to object
        _data_times = (data_times)_funcTool.convertJsonToObject(typeof(data_times), _local_json);

        return _data_times;
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _data_employee)
    {
        // convert to json
        _local_json = _funcTool.convertObjectToJson(_data_employee);
        //litDebug.Text = _local_json;

        // call services
        _local_json = _funcTool.callServicePost(_cmdUrl, _local_json);

        // convert json to object
        _data_employee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _local_json);

        return _data_employee;
    }

    protected data_geo callServicePostGeo(string _cmdUrl, data_geo _data_geo)
    {
        // convert to json
        _local_json = _funcTool.convertObjectToJson(_data_geo);
        // litDebug.Text = _local_json;

        // call services
        _local_json = _funcTool.callServicePost(_cmdUrl, _local_json);

        // convert json to object
        _data_geo = (data_geo)_funcTool.convertJsonToObject(typeof(data_geo), _local_json);

        return _data_geo;
    }

    protected data_times getGroupList(data_times _data_times)
    {
        _data_times = callServicePostTimes(_urlTmGetGroup, _data_times);
        return _data_times;
    }

    protected data_employee getViewEmployeeList(data_employee _data_employee)
    {
        _data_employee = callServicePostEmployee(_urlGetViewEmployeeListSmall, _data_employee);
        return _data_employee;
    }

    protected data_geo getGeoList(data_geo _data_geo)
    {
        _data_geo = callServicePostGeo(_urlExGetGeoList, _data_geo);
        return _data_geo;
    }

    protected data_visitors getVisitorTypeList(data_visitors _data_visitors)
    {
        _data_visitors = callServicePostVisitors(_urlVmGetVisitorType, _data_visitors);
        return _data_visitors;
    }

    protected void WriteExcelWithNPOI(DataTable dt, String extension, String fileName)
    {
        IWorkbook workbook;
        if (extension == "xlsx")
        {
            workbook = new XSSFWorkbook();
        }
        else if (extension == "xls")
        {
            workbook = new HSSFWorkbook();
        }
        else
        {
            throw new Exception("This format is not supported");
        }
        ISheet sheet1 = workbook.CreateSheet("Sheet 1");
        IRow row1 = sheet1.CreateRow(0);
        for (int j = 0; j < dt.Columns.Count; j++)
        {
            ICell cell = row1.CreateCell(j);
            String columnName = dt.Columns[j].ToString();
            cell.SetCellValue(columnName);
        }
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            IRow row = sheet1.CreateRow(i + 1);
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                ICell cell = row.CreateCell(j);
                String columnName = dt.Columns[j].ToString();
                cell.SetCellValue(dt.Rows[i][columnName].ToString());
            }
        }
        using (var exportData = new MemoryStream())
        {
            Response.Clear();
            workbook.Write(exportData);
            if (extension == "xlsx")
            {
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xlsx"));
                Response.BinaryWrite(exportData.ToArray());
            }
            else if (extension == "xls")
            {
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xls"));
                Response.BinaryWrite(exportData.GetBuffer());
            }
            Response.End();
        }
    }
    #endregion reuse

    #region teppanop
    private Boolean getPermission(int _node_idx, int _actor_idx)
    {
        if (_data_visitors.vm_visitors_setperms_admin_list_m0 != null)
        {
            var v_data = _data_visitors.vm_visitors_setperms_admin_list_m0.Where(d =>
                        d.node_idx == _node_idx
                        &&
                        d.actor_idx == _actor_idx
                        ).FirstOrDefault();
            if (v_data != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }

    }

    private void ShowDataIndexList()
    {
        vm_visitors_doc_detail_u0 _u0_doc = new vm_visitors_doc_detail_u0();
        _u0_doc.u0_idx = 0;
        _u0_doc.emp_idx = _emp_idx;
        _u0_doc.s_idcard = tbSearchIdCard.Text.Trim();
        _u0_doc.s_filter_keyword = tbSearchName.Text.Trim();
        _u0_doc.s_visit_name_th = tbSearchVisitName.Text.Trim();
        _data_visitors.vm_visitors_doc_list_u0 = new vm_visitors_doc_detail_u0[1];
        _data_visitors.vm_visitors_doc_list_u0[0] = _u0_doc;
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_visitors));
        _data_visitors = callServicePostVisitors(_urlVmGetTransaction, _data_visitors);
        ViewState["document_detail"] = _data_visitors.vm_visitors_doc_list_u0;
        setGridData(gvDocList, _data_visitors.vm_visitors_doc_list_u0);
        Notification();
    }

    private void setSearchDefault()
    {
        tbSearchIdCard.Text = "";
        tbSearchName.Text = "";
        tbSearchVisitName.Text = String.Empty;
    }

    private void setSearchDefault_app()
    {
        tbSearchIdCard_app.Text = "";
        tbSearchName_app.Text = "";
        tbSearchVisitName_app.Text = String.Empty;
    }

    private void ShowDataIndexApp()
    {
        vm_visitors_doc_detail_u0 _u0_doc = new vm_visitors_doc_detail_u0();
        _u0_doc.u0_idx = 0;
        _u0_doc.emp_idx = 0;
        //litDebug.Text = ViewState["mode"].ToString();
        if (ViewState["mode"].ToString() == "approve_leader")
        {
            _u0_doc.node_idx = 2;
            _u0_doc.actor_idx = 3;
            _u0_doc.emp_idx_approve1 = _emp_idx;
        }
        else if (ViewState["mode"].ToString() == "approve_zone")
        {
            _u0_doc.node_idx = 3;
            _u0_doc.actor_idx = 4;
            _u0_doc.emp_idx_approve1 = _emp_idx;
        }
        else
        {
            _u0_doc.emp_idx_approve1 = _emp_idx;
        }
        _u0_doc.s_idcard = tbSearchIdCard_app.Text.Trim();
        _u0_doc.s_filter_keyword = tbSearchName_app.Text.Trim();
        _u0_doc.s_filter_keyword = tbSearchVisitName_app.Text.Trim();
        _data_visitors.vm_visitors_doc_list_u0 = new vm_visitors_doc_detail_u0[1];
        _data_visitors.vm_visitors_doc_list_u0[0] = _u0_doc;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_visitors));
        _data_visitors = callServicePostVisitors(_urlVmGetTransaction, _data_visitors);
        ViewState["document_detail"] = _data_visitors.vm_visitors_doc_list_u0;
        setGridData(gvDocList_app, _data_visitors.vm_visitors_doc_list_u0);
        Notification();

    }

    protected void setActiveTabBar(string activeTab)
    {
        switch (activeTab)
        {
            case "viewList":
                li0.Attributes.Add("class", "active");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");
                break;
            case "viewCreate":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "active");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");
                break;
            case "viewApprove":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "active");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");
                break;
            case "viewApprove_zone":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "active");
                li4.Attributes.Add("class", "");
                break;
            case "viewReport":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "active");
                break;
        }
    }

    private Boolean ShowDataDetail(int id)
    {
        Boolean _boolean = false;
        div_create_heading.Visible = false;
        setFormData(fvEmpProfile, FormViewMode.ReadOnly, null);

        div_create_form.Attributes.Add("class", "panel panel-warning");
        litCreateFormTitle.Text = "รายละเอียดรายการ";

        vm_visitors_doc_detail_u0 _u0_edit = new vm_visitors_doc_detail_u0();
        _u0_edit.u0_idx = id;
        _u0_edit.emp_idx = _emp_idx;
        _data_visitors.vm_visitors_doc_list_u0 = new vm_visitors_doc_detail_u0[1];
        _data_visitors.vm_visitors_doc_list_u0[0] = _u0_edit;
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_visitors));
        _data_visitors = callServicePostVisitors(_urlVmGetTransaction, _data_visitors);
        if (_data_visitors.vm_visitors_doc_list_u0 != null)
        {
            _boolean = true;
        }
        ViewState["document_detail"] = _data_visitors;
        setFormData(fvDocCreate, FormViewMode.Edit, ((data_visitors)ViewState["document_detail"]).vm_visitors_doc_list_u0);

        return _boolean;
    }

    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var gridViewName = (GridView)sender;

        switch(gridViewName.ID)
        {
            case "gvDocList_app":
                gridViewName.PageIndex = e.NewPageIndex;
                gridViewName.DataBind();
                setGridData(gridViewName, ViewState["document_detail"]);
                SETFOCUS.Focus();
                break;
            case "gvDocList":
                gridViewName.PageIndex = e.NewPageIndex;
                gridViewName.DataBind();
                setGridData(gridViewName, ViewState["document_detail"]);
                SETFOCUS.Focus();
                break;
        }
    }
    
    private void Notification()
    {
        data_visitors dt_visitors = new data_visitors();
        vm_visitors_doc_detail_u0 _u0 = new vm_visitors_doc_detail_u0();
        _u0.emp_idx = _emp_idx;
        dt_visitors.vm_visitors_doc_list_u0 = new vm_visitors_doc_detail_u0[1];
        dt_visitors.vm_visitors_doc_list_u0[0] = _u0;

        dt_visitors = callServicePostVisitors(_urlVmGetNoti, dt_visitors);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dt_visitors));
        ViewState["noti_detail"] = dt_visitors;
        getNotification(lbApprove);
        getNotification(lbApprove_zone);
    }

    private void getNotification(LinkButton _linkbtn)
    {
        if (ViewState["noti_detail"] != null)
        {
            _data_visitors = (data_visitors)ViewState["noti_detail"];
            var v_data = _data_visitors.vm_visitors_menu_list_m0.Where(d =>
                  d.lb_menu == _linkbtn.ID.Trim()
                  ).FirstOrDefault();
            if (v_data != null)
            {
                setNotification(_linkbtn, v_data.menu, v_data.zcount);
                //  litDebug.Text = v_data.zcount.ToString();
            }
        }
        else
        {
            setNotification(lbApprove, "รายการที่รออนุมัติ", 0);
            setNotification(lbApprove_zone, "รายการที่รออนุมัติโซน", 0);
        }

    }

    private void setNotification(LinkButton _linkbtn, string _label, int _value)
    {
        _linkbtn.Text = _label + " <span class='badge progress-bar-danger' >" + Convert.ToString(_value) + "</span>";
    }
    #endregion teppanop
}