﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="U0_MasterCasePOS.aspx.cs" Inherits="websystem_MasterData_U0_MasterCasePOS" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

                <asp:View ID="ViewIndex" runat="server">

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-blackboard"></i><strong>&nbsp; Case Close Job POS</strong></h3>
                        </div>

                        <div class="panel-body">
                            <%-- <div class="form-horizontal" role="form">--%>
                            <div class="form-group">

                                <asp:LinkButton ID="btnadd" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Add" runat="server" CommandName="CmdAdd" OnCommand="btnCommand"><i class="fa fa-plus"></i></asp:LinkButton>
                            </div>

                            <%------------------------ Div ADD ผูกผู้ถือครอง Support and Holder ------------------------%>

                            <asp:Panel ID="Panel_Add" runat="server" Visible="false">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><i class="glyphicon glyphicon-plus"></i><strong>&nbsp;Add Case Close Job POS</strong></h4>
                                    <div class="form-horizontal" role="form">
                                        <%--<div class="panel panel-default">--%>
                                        <div class="panel-heading">


                                            <div class="form-group">
                                                <asp:Label ID="Label26" runat="server" Text="Case LV1." CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-8">
                                                    <asp:DropDownList ID="ddlLV1" AutoPostBack="true" CssClass="form-control" runat="server">
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidddator8" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                        ControlToValidate="ddlLV1" Font-Size="11"
                                                        ErrorMessage="กรุณากรอกเลือกเคสปิดงาน"
                                                        ValidationExpression="กรุณากรอกเลือกเคสปิดงาน" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidddator8" Width="160" />

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label1" runat="server" Text="Case LV2." CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-8">
                                                    <asp:DropDownList ID="ddlLV2" AutoPostBack="true" CssClass="form-control" runat="server">
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                        ControlToValidate="ddlLV2" Font-Size="11"
                                                        ErrorMessage="กรุณากรอกเลือกเคสปิดงาน"
                                                        ValidationExpression="กรุณากรอกเลือกเคสปิดงาน" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label3" runat="server" Text="Case LV3." CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-8">
                                                    <asp:DropDownList ID="ddlLV3" AutoPostBack="true" CssClass="form-control" runat="server">
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                        ControlToValidate="ddlLV3" Font-Size="11"
                                                        ErrorMessage="กรุณากรอกเลือกเคสปิดงาน"
                                                        ValidationExpression="กรุณากรอกเลือกเคสปิดงาน" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label4" runat="server" Text="Case LV4." CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-8">
                                                    <asp:DropDownList ID="ddlLV4" AutoPostBack="true" CssClass="form-control" runat="server">
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                        ControlToValidate="ddlLV4" Font-Size="11"
                                                        ErrorMessage="กรุณากรอกเลือกเคสปิดงาน"
                                                        ValidationExpression="กรุณากรอกเลือกเคสปิดงาน" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator3" Width="160" />
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <asp:Label ID="lbstatus" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                                <div class="col-sm-8">
                                                    <asp:DropDownList ID="ddStatusadd" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="1" Text="Online" />
                                                        <asp:ListItem Value="0" Text="Offline" />
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-3">
                                                    <asp:LinkButton ID="lbladd" ValidationGroup="SaveAdd" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="btnAdd" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="lblcancel" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancel" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <hr />
                                    <%--  </div>--%>
                                </div>
                            </asp:Panel>


                            <asp:GridView ID="GvMaster" runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="primary"
                                HeaderStyle-Height="40px"
                                AllowPaging="true"
                                DataKeyNames="POSIDX"
                                PageSize="10"
                                OnRowDataBound="Master_RowDataBound"
                                OnRowEditing="Master_RowEditing"
                                OnRowCancelingEdit="Master_RowCancelingEdit"
                                OnPageIndexChanging="Master_PageIndexChanging"
                                OnRowUpdating="Master_RowUpdating">

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>
                                <Columns>

                                    <asp:TemplateField HeaderText="#">

                                        <ItemTemplate>
                                            <asp:Label ID="lblpidx" runat="server" Visible="false" Text='<%# Eval("POSIDX") %>' />
                                            <%# (Container.DataItemIndex +1) %>
                                        </ItemTemplate>


                                        <EditItemTemplate>
                                            <div class="form-horizontal" role="form">
                                                <div class="panel-heading">

                                                    <div class="form-group">
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txtPIDX" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("POSIDX")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label25" CssClass="col-sm-3 control-label" runat="server" Text="Case LV1." />
                                                        <div class="col-sm-8">
                                                            <asp:Label ID="lbLV1" runat="server" Text='<%# Bind("POS1IDX") %>' Visible="false" />
                                                            <asp:DropDownList ID="ddlPOS1IDX_edit" AutoPostBack="true" CssClass="form-control" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ValidationGroup="Save_edit" runat="server" Display="None"
                                                            ControlToValidate="ddlPOS1IDX_edit" Font-Size="11"
                                                            ErrorMessage="กรุณากรอกเลือกเคสปิดงาน"
                                                            ValidationExpression="กรุณากรอกเลือกเคสปิดงาน" InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator6" Width="160" />
                                                    </div>


                                                    <div class="form-group">
                                                        <asp:Label ID="Label2" CssClass="col-sm-3 control-label" runat="server" Text="Case LV2." />
                                                        <div class="col-sm-8">
                                                            <asp:Label ID="lbLV2" runat="server" Text='<%# Bind("POS2IDX") %>' Visible="false" />
                                                            <asp:DropDownList ID="ddlPOS2IDX_edit" AutoPostBack="true" CssClass="form-control" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="Save_edit" runat="server" Display="None"
                                                            ControlToValidate="ddlPOS2IDX_edit" Font-Size="11"
                                                            ErrorMessage="กรุณากรอกเลือกเคสปิดงาน"
                                                            ValidationExpression="กรุณากรอกเลือกเคสปิดงาน" InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator4" Width="160" />
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label6" CssClass="col-sm-3 control-label" runat="server" Text="Case LV3." />
                                                        <div class="col-sm-8">
                                                            <asp:Label ID="lbLV3" runat="server" Text='<%# Bind("POS3IDX") %>' Visible="false" />
                                                            <asp:DropDownList ID="ddlPOS3IDX_edit" AutoPostBack="true" CssClass="form-control" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ValidationGroup="Save_edit" runat="server" Display="None"
                                                            ControlToValidate="ddlPOS3IDX_edit" Font-Size="11"
                                                            ErrorMessage="กรุณากรอกเลือกเคสปิดงาน"
                                                            ValidationExpression="กรุณากรอกเลือกเคสปิดงาน" InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator5" Width="160" />
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label8" CssClass="col-sm-3 control-label" runat="server" Text="Case LV4." />
                                                        <div class="col-sm-8">
                                                            <asp:Label ID="lbLV4" runat="server" Text='<%# Bind("POS4IDX") %>' Visible="false" />
                                                            <asp:DropDownList ID="ddlPOS4IDX_edit" AutoPostBack="true" CssClass="form-control" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ValidationGroup="Save_edit" runat="server" Display="None"
                                                            ControlToValidate="ddlPOS4IDX_edit" Font-Size="11"
                                                            ErrorMessage="กรุณากรอกเลือกเคสปิดงาน"
                                                            ValidationExpression="กรุณากรอกเลือกเคสปิดงาน" InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator7" Width="160" />
                                                    </div>




                                                    <div class="form-group">
                                                        <asp:Label ID="lbstatus" CssClass="col-sm-3 control-label" runat="server" Text="Status" />
                                                        <div class="col-sm-8">
                                                            <asp:DropDownList ID="ddStatusUpdate" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("POSStatus") %>'>
                                                                <asp:ListItem Value="1" Text="Online" />
                                                                <asp:ListItem Value="0" Text="Offline" />
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <div class="col-sm-2 col-sm-offset-10">
                                                            <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server" ValidationGroup="Save_edit" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>


                                        </EditItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Case LV1." ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lborg" runat="server" Text='<%# Eval("Name_Code1") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Case LV2." ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lboarg2" runat="server" Text='<%# Eval("Name_Code2") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Case LV3." ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lboarg3" runat="server" Text='<%# Eval("Name_Code3") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Case LV4." ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lboarg4" runat="server" Text='<%# Eval("Name_Code4") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lbdep" runat="server" Text='<%# Eval("POSStatusDetail") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                            <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDel" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("POSIDX") %>'><i class="fa fa-trash-o"></i></asp:LinkButton>
                                        </ItemTemplate>

                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>

                        </div>
                    </div>
                </asp:View>
            </asp:MultiView>

        </ContentTemplate>
    </asp:UpdatePanel>


</asp:Content>

