﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_masterdata_TFeedBack : System.Web.UI.Page
{
    #region initial function/data
    function_tool _funcTool = new function_tool();

    data_FeedBack _data_FeedBack = new data_FeedBack();
    

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];//urlGetddlplace
    static string _urlGetM0_System = _serviceUrl + ConfigurationManager.AppSettings["urlGetM0_SystemTMMS"];
    static string _urlGetFeedBackU = _serviceUrl + ConfigurationManager.AppSettings["urlGetFeedBackUTMMS"];
    static string _urlSetFeedBackU = _serviceUrl + ConfigurationManager.AppSettings["urlSetInsFeedBackU"];
    static string _urlSetUpdFeedBackU = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdFeedBackU"];
    static string _urlDeleteFeedBackU = _serviceUrl + ConfigurationManager.AppSettings["urlDeleteFeedBackU"];

    static string _urlGetm0FeedBack1 = _serviceUrl + ConfigurationManager.AppSettings["urlGetm0FeedBack1TMMS"];
    static string _urlGetm0FeedBack2 = _serviceUrl + ConfigurationManager.AppSettings["urlGetm0FeedBack2"];
    static string _urlGetm0FeedBack3 = _serviceUrl + ConfigurationManager.AppSettings["urlGetm0FeedBack3"];

    string _localJson = "";
    int _tempInt = 0;

    string _defaultDdlText;
    string _defaultDdlValue;

    int emp_idx = 0;
    #endregion initial function/data


    protected void Page_Load(object sender, EventArgs e)
    {
        emp_idx = int.Parse(Session["emp_idx"].ToString());

        if (!IsPostBack)
        {
            initPage();
            actionIndex();

        }
    }

    #region selected   
    protected void actionIndex()
    {

        data_FeedBack data_FeedBack = new data_FeedBack();
        data_FeedBack.U_FeedBack_list = new U_FeedBack_detail[1];

        U_FeedBack_detail _U_FeedBack_detailindex = new U_FeedBack_detail();

        _U_FeedBack_detailindex.MFBIDX1 = 0;

        data_FeedBack.U_FeedBack_list[0] = _U_FeedBack_detailindex;
        //litDebug.Text = _funcTool.convertObjectToXml(data_FeedBack);
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_FeedBack));
        data_FeedBack = callServiceNetwork(_urlGetFeedBackU, data_FeedBack);

         setGridData(GvMaster, data_FeedBack.U_FeedBack_list);

    }

    protected void actionddlplace()
    {

        //MFBIDX1
        // var lbddlMFBIDX1 = (Label)ViewInsert.FindControl("lbddlMFBIDX1");
        DropDownList ddlMFBIDX1 = (DropDownList)ViewInsert.FindControl("ddlMFBIDX1");
        ddlMFBIDX1.AppendDataBoundItems = true;
        ddlMFBIDX1.Items.Add( new ListItem("กรุณาเลือกชุดคำถาม....", "0"));
        _data_FeedBack.M0_FeedBack_1_list = new M0_FeedBack_1_detail[1];
        M0_FeedBack_1_detail ojbmFB1 = new M0_FeedBack_1_detail();
        _data_FeedBack.M0_FeedBack_1_list[0] = ojbmFB1;
        _data_FeedBack = callServiceNetwork(_urlGetm0FeedBack1, _data_FeedBack);
        ddlMFBIDX1.DataSource = _data_FeedBack.M0_FeedBack_1_list;
        ddlMFBIDX1.DataTextField = "QuestionSet";
        ddlMFBIDX1.DataValueField = "MFBIDX1";
        ddlMFBIDX1.DataBind();

        //MFBIDX2
        // var lbddlMFBIDX2 = (Label)ViewInsert.FindControl("lbddlMFBIDX2");
        DropDownList ddlMFBIDX2 = (DropDownList)ViewInsert.FindControl("ddlMFBIDX2");
        ddlMFBIDX2.AppendDataBoundItems = true;
        ddlMFBIDX2.Items.Add( new ListItem("กรุณาเลือกหัวข้อคำถาม....", "0"));
        _data_FeedBack.M0_FeedBack_2_list = new M0_FeedBack_2_detail[1];
        M0_FeedBack_2_detail ojbmFB2 = new M0_FeedBack_2_detail();
        _data_FeedBack.M0_FeedBack_2_list[0] = ojbmFB2;
        _data_FeedBack = callServiceNetwork(_urlGetm0FeedBack2, _data_FeedBack);
        ddlMFBIDX2.DataSource = _data_FeedBack.M0_FeedBack_2_list;
        ddlMFBIDX2.DataTextField = "Topic";
        ddlMFBIDX2.DataValueField = "MFBIDX2";
        ddlMFBIDX2.DataBind();

        //MFBIDX3
        //var lbddlMFBIDX3 = (Label)ViewInsert.FindControl("lbddlMFBIDX3");
        DropDownList ddlMFBIDX3 = (DropDownList)ViewInsert.FindControl("ddlMFBIDX3");
        ddlMFBIDX3.AppendDataBoundItems = true;
        ddlMFBIDX3.Items.Add( new ListItem("กรุณาเลือกคำถามย่อย....", "0"));
        _data_FeedBack.M0_FeedBack_3_list = new M0_FeedBack_3_detail[1];
        M0_FeedBack_3_detail ojbmFB3 = new M0_FeedBack_3_detail();
        _data_FeedBack.M0_FeedBack_3_list[0] = ojbmFB3;
        _data_FeedBack = callServiceNetwork(_urlGetm0FeedBack3, _data_FeedBack);
        ddlMFBIDX3.DataSource = _data_FeedBack.M0_FeedBack_3_list;
        ddlMFBIDX3.DataTextField = "Question";
        ddlMFBIDX3.DataValueField = "MFBIDX3";
        ddlMFBIDX3.DataBind();


    }


    #endregion selected 

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        int _MFBIDX;
        string _txtQuestionSet;
        int _cemp_idx;

        U_FeedBack_detail objU_FeedBack = new U_FeedBack_detail();

        switch (cmdName)
        {

            case "btnToInsert":
                MvMaster.SetActiveView(ViewInsert);
                actionddlplace();
                break;
            case "btnCancel":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btnInsert":

                
                DropDownList _ddlMFBIDX1 = (DropDownList)ViewInsert.FindControl("ddlMFBIDX1");
                DropDownList _ddlMFBIDX2 = (DropDownList)ViewInsert.FindControl("ddlMFBIDX2");
                DropDownList _ddlMFBIDX3 = (DropDownList)ViewInsert.FindControl("ddlMFBIDX3");
                DropDownList _ddlMFBStatus = (DropDownList)ViewInsert.FindControl("ddlMFBStatus");
                _cemp_idx = emp_idx;
                U_FeedBack_detail obj = new U_FeedBack_detail();
                _data_FeedBack.U_FeedBack_list = new U_FeedBack_detail[1];
                obj.MFBIDX = 0;//_type_idx; 
                obj.MFBIDX1 = int.Parse(_ddlMFBIDX1.SelectedValue);
                obj.MFBIDX2 = int.Parse(_ddlMFBIDX2.SelectedValue);
                obj.MFBIDX3 = int.Parse(_ddlMFBIDX3.SelectedValue);
                obj.MFBStatus = int.Parse(_ddlMFBStatus.SelectedValue);
                obj.CEmpIDX = _cemp_idx;

                _data_FeedBack.U_FeedBack_list[0] = obj;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_FeedBack));
               
                _data_FeedBack = callServiceNetwork(_urlSetFeedBackU, _data_FeedBack);

                if (_data_FeedBack.return_code == 0)
                {
                    actionIndex();
                    //MvMaster.SetActiveView(ViewIndex);
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                }
                else
                {
                    setError(_data_FeedBack.return_code.ToString() + " - " + _data_FeedBack.return_msg);
                }

                //actionCreate();
                //Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btnDelete":

                _MFBIDX = int.Parse(cmdArg);
                _cemp_idx = emp_idx;


                _data_FeedBack.U_FeedBack_list = new U_FeedBack_detail[1];
                objU_FeedBack.MFBIDX = _MFBIDX;
                objU_FeedBack.CEmpIDX = _cemp_idx;

                _data_FeedBack.U_FeedBack_list[0] = objU_FeedBack;

               // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_FeedBack));

                _data_FeedBack = callServiceNetwork(_urlDeleteFeedBackU, _data_FeedBack);


                if (_data_FeedBack.return_code == 0)
                {

                    //actionIndex();
                    //MvMaster.SetActiveView(ViewIndex);
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);


                }
                else
                {
                    setError(_data_FeedBack.return_code.ToString() + " - " + _data_FeedBack.return_msg);
                }

                break;




        }
    }
    #endregion btnCommand

    #region bind data
    protected void getGridData(string _cmdName, int _actionType)
    {
        // setGridData(gvBookingType, ViewState["listData"]);
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected string getStatus(int status)
    {
        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();
                actionIndex();
                break;
        }
    }

    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster.EditIndex == e.Row.RowIndex) //to overlook header row
                    {
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_FeedBack));
                        //MFBIDX1
                        var lbddlMFBIDX1Update = (Label)e.Row.FindControl("lbddlMFBIDX1Update");
                        var ddlMFBIDX1Update = (DropDownList)e.Row.FindControl("ddlMFBIDX1Update");
                        ddlMFBIDX1Update.AppendDataBoundItems = true;
                        _data_FeedBack.M0_FeedBack_1_list = new M0_FeedBack_1_detail[1];
                        M0_FeedBack_1_detail ojbmFB1 = new M0_FeedBack_1_detail();
                        _data_FeedBack.M0_FeedBack_1_list[0] = ojbmFB1;
                        _data_FeedBack = callServiceNetwork(_urlGetm0FeedBack1, _data_FeedBack);
                        ddlMFBIDX1Update.DataSource = _data_FeedBack.M0_FeedBack_1_list;
                        ddlMFBIDX1Update.DataTextField = "QuestionSet";
                        ddlMFBIDX1Update.DataValueField = "MFBIDX1";
                        ddlMFBIDX1Update.DataBind();
                        ddlMFBIDX1Update.Items.Insert(0, new ListItem("กรุณาเลือกชุดคำถาม....", "0"));
                        ddlMFBIDX1Update.SelectedValue = lbddlMFBIDX1Update.Text;
                      
                        //MFBIDX2
                        var lbddlMFBIDX2Update = (Label)e.Row.FindControl("lbddlMFBIDX2Update");
                        var ddlMFBIDX2Update = (DropDownList)e.Row.FindControl("ddlMFBIDX2Update");
                        ddlMFBIDX2Update.AppendDataBoundItems = true;
                        _data_FeedBack.M0_FeedBack_2_list = new M0_FeedBack_2_detail[1];
                        M0_FeedBack_2_detail ojbmFB2 = new M0_FeedBack_2_detail();
                        _data_FeedBack.M0_FeedBack_2_list[0] = ojbmFB2;
                        _data_FeedBack = callServiceNetwork(_urlGetm0FeedBack2, _data_FeedBack);
                        ddlMFBIDX2Update.DataSource = _data_FeedBack.M0_FeedBack_2_list;
                        ddlMFBIDX2Update.DataTextField = "Topic";
                        ddlMFBIDX2Update.DataValueField = "MFBIDX2";
                        ddlMFBIDX2Update.DataBind();
                        ddlMFBIDX2Update.Items.Insert(0, new ListItem("กรุณาเลือกหัวข้อคำถาม....", "0"));
                        ddlMFBIDX2Update.SelectedValue = lbddlMFBIDX2Update.Text;

                        //MFBIDX2
                        var lbddlMFBIDX3Update = (Label)e.Row.FindControl("lbddlMFBIDX3Update");
                        var ddlMFBIDX3Update = (DropDownList)e.Row.FindControl("ddlMFBIDX3Update");
                        ddlMFBIDX3Update.AppendDataBoundItems = true;
                        _data_FeedBack.M0_FeedBack_3_list = new M0_FeedBack_3_detail[1];
                        M0_FeedBack_3_detail ojbmFB3 = new M0_FeedBack_3_detail();
                        _data_FeedBack.M0_FeedBack_3_list[0] = ojbmFB3;
                        _data_FeedBack = callServiceNetwork(_urlGetm0FeedBack3, _data_FeedBack);
                        ddlMFBIDX3Update.DataSource = _data_FeedBack.M0_FeedBack_3_list;
                        ddlMFBIDX3Update.DataTextField = "Question";
                        ddlMFBIDX3Update.DataValueField = "MFBIDX3";
                        ddlMFBIDX3Update.DataBind();
                        ddlMFBIDX3Update.Items.Insert(0, new ListItem("กรุณาเลือกคำถามย่อย....", "0"));
                        ddlMFBIDX3Update.SelectedValue = lbddlMFBIDX3Update.Text;
                    }
                }

                break;
        }
    }

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.EditIndex = e.NewEditIndex;
                actionIndex();
                break;
        }
    }

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.EditIndex = -1;
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
        }
    }

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":


                int MFBIDX1_update = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var ddlMFBIDX1Update = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlMFBIDX1Update");
                var ddlMFBIDX2Update = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlMFBIDX2Update");
                var ddlMFBIDX3Update = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlMFBIDX3Update");
                var ddlMFBStatusUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlMFBStatusUpdate");

                GvMaster.EditIndex = -1;

                _data_FeedBack.U_FeedBack_list = new U_FeedBack_detail[1];
                U_FeedBack_detail _U_FeedBack_detail = new U_FeedBack_detail();

                _U_FeedBack_detail.MFBIDX = MFBIDX1_update;
                _U_FeedBack_detail.MFBIDX1 = int.Parse(ddlMFBIDX1Update.SelectedValue);
                _U_FeedBack_detail.MFBIDX2 = int.Parse(ddlMFBIDX2Update.SelectedValue);
                _U_FeedBack_detail.MFBIDX3 = int.Parse(ddlMFBIDX2Update.SelectedValue);
                _U_FeedBack_detail.MFBStatus = int.Parse(ddlMFBStatusUpdate.SelectedValue);
                _U_FeedBack_detail.CEmpIDX = emp_idx;

                _data_FeedBack.U_FeedBack_list[0] = _U_FeedBack_detail;

                // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_FeedBack));
                _data_FeedBack = callServiceNetwork(_urlSetUpdFeedBackU, _data_FeedBack);

                if (_data_FeedBack.return_code == 0)
                {
                    //initPage();
                    //setDataList(dtlMenu, _dataMenu.m0_menu_list);  
                    actionIndex();


                }
                else
                {
                    setError(_data_FeedBack.return_code.ToString() + " - " + _data_FeedBack.return_msg);
                }


                break;
        }
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj, string sysidx_menu, string menuidx_per, string role_per)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();

        //switch (fvName.ID)
        //{



        //}
    }

    protected void setDataList(DataList dtlName, Object obj)
    {
        dtlName.DataSource = obj;
        dtlName.DataBind();
    }

    #endregion bind data

    #region reuse
    protected void initPage()
    {
        clearSession();
        clearViewState();

        // divAction.Visible = true;
        // gvBookingType.Visible = true;
        // fvBookingType.Visible = false;

        // getGridData("booking_type", 20);
        //setFormData(fvMenu, FormViewMode.Insert, null, "0", "0", "0");


    }

    protected void initPagePermission()
    {
        clearSession();
        clearViewState();

        // divAction.Visible = true;
        // gvBookingType.Visible = true;
        // fvBookingType.Visible = false;

        // getGridData("booking_type", 20);
        //setFormData(fvPermission, FormViewMode.Insert, null, "0", "0", "0");


    }

    protected void setVisible()
    {
        // divAction.Visible = !divAction.Visible;
        // gvBookingType.Visible = !gvBookingType.Visible;
        // fvBookingType.Visible = !fvBookingType.Visible;
    }

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        // ViewState["listData"] = null;
    }

    protected void setError(string _errorText)
    {
        // divShowError.Visible = !divShowError.Visible;
        // litErrorCode.Text = _dataEmployee.return_code.ToString();
        if (_errorText != "")
        {
            litDebug.Text = "error : " + _errorText;
        }
        else
        {
            litDebug.Text = String.Empty;
        }
    }

    protected data_FeedBack callServiceNetwork(string _cmdUrl, data_FeedBack _data_FeedBack)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_FeedBack);
        // litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);

        // convert json to object
        _data_FeedBack = (data_FeedBack)_funcTool.convertJsonToObject(typeof(data_FeedBack), _localJson);

        return _data_FeedBack;
    }


    #endregion reuse

}