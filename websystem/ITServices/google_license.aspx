﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="google_license.aspx.cs" Inherits="websystem_ITServices_google_license" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:HyperLink ID="setOntop" runat="server"></asp:HyperLink>

    <script>
        $(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });


        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            });
        });

    </script>

    <script>
        $(document).ready(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        })
    </script>

    <script>
        $(function () {

            $('.from-date-datepickerexpire').datetimepicker({
                format: 'DD/MM/YYYY',
                minDate: new Date()

            });
        });


        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepickerexpire').datetimepicker({
                    format: 'DD/MM/YYYY',
                    minDate: new Date()

                });
            });
        });

    </script>

    <script>
        $(document).ready(function () {
            $('.from-date-datepickerexpire').datetimepicker({
                format: 'DD/MM/YYYY',
                minDate: new Date()

            });
        })
    </script>

    <asp:Literal ID="litDebug" runat="server"></asp:Literal>

    <!--*** START Menu ***-->
    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand">Menu</a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav" runat="server">
                    <li id="liToIndexList" runat="server">
                        <asp:LinkButton ID="btnToIndexList" runat="server"
                            CommandName="btnToIndexList"
                            OnCommand="btnCommand" Text="ข้อมูล Google-License" />
                    </li>

                    <%-- <% if(Session["emp_idx"].ToString() == "172" || Session["emp_idx"].ToString() == "1413" || Session["emp_idx"].ToString() == "178" || Session["emp_idx"].ToString() == "3657" || Session["emp_idx"].ToString() == "3593" || Session["emp_idx"].ToString() == "3834" || Session["emp_idx"].ToString() == "3752") { %>--%>
                    <li id="li1" runat="server">
                        <asp:LinkButton ID="btnToGoogleLicense" Visible="false" runat="server"
                            CommandName="btnToGoogleLicense"
                            OnCommand="btnCommand" Text="เพิ่มรายการ Google-License" />
                    </li>
                    <%--<% } %>--%>

                    <li id="li2" runat="server">
                        <asp:LinkButton ID="btnDetailGoogleLicense" Visible="false" runat="server"
                            CommandName="btnDetailGoogleLicense"
                            OnCommand="btnCommand" Text="รายการ Holder Google-License" />
                    </li>


                    <%-- <% if(Session["emp_idx"].ToString() == "172" || Session["emp_idx"].ToString() == "1413" || Session["emp_idx"].ToString() == "178" || Session["emp_idx"].ToString() == "3657" || Session["emp_idx"].ToString() == "3593" || Session["emp_idx"].ToString() == "3834" || Session["emp_idx"].ToString() == "3752") { %>--%>
                    <li id="li4" runat="server">
                        <asp:LinkButton ID="btnToReport" Visible="false" runat="server"
                            CommandName="btnToReport"
                            OnCommand="btnCommand" Text="รายงาน" />
                    </li>
                    <%-- <% } %>--%>
                </ul>

                <ul class="nav navbar-nav navbar-right" runat="server">

                    <li id="_divMenuLiToDocument" runat="server">
                        <asp:HyperLink ID="_divMenuBtnToDocument" runat="server"
                            NavigateUrl="https://docs.google.com/document/d/1dF-UFvy2YemJ7hgBVbS69c5zdu3y99Q5jxQVjo3tHrg" Target="_blank" Text="คู่มือการใช้งาน" />
                    </li>
                </ul>


            </div>
        </nav>
    </div>
    <!--*** END Menu ***-->

    <%----------- MultiView Start ---------------%>
    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <%----------- ViewIndex Google License Start ---------------%>
        <asp:View ID="ViewIndex" runat="server">

            <div class="form-group">
                <div class="col-md-12">
                    <asp:LinkButton ID="btnSearch" Visible="false" CssClass="btn btn-primary" runat="server" data-original-title="Search" data-toggle="tooltip" CommandName="btnSearch" OnCommand="btnCommand"><span class="glyphicon glyphicon-search"></span> ค้นหา</asp:LinkButton>

                    <asp:LinkButton ID="btnResetSearchPage" CssClass="btn btn-default" runat="server" data-original-title="Refresh" data-toggle="tooltip" CommandName="btnCancelIndex_First" OnCommand="btnCommand"><span class="glyphicon glyphicon-refresh"></span></asp:LinkButton>
                </div>
            </div>

            <!--*** START Back To Index ***-->
            <div class="form-group">
                <div class="col-md-12">
                    <div id="fvBacktoIndex" class="row" runat="server" visible="false">
                        <%--<div class="col-md-12">--%>
                        <asp:LinkButton ID="btnSearchBack" CssClass="btn btn-danger" runat="server" data-original-title="Back" data-toggle="tooltip" OnCommand="btnCommand" CommandName="btnSearchBack"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> กลับ</asp:LinkButton>




                        <%--</div>--%>
                    </div>
                </div>

            </div>
            <!--*** END Back To Index ***-->

            <!--*** START Search Index ***-->
            <div class="form-group">
                <div class="col-md-12">
                    <div id="showsearch" runat="server" visible="false">
                        <br />
                        <div class="panel panel-primary">
                            <div class="panel-heading f-bold">ค้นหาข้อมูล google license</div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <%-------------- องค์กร,ฝ่าย --------------%>
                                    <div class="form-group">
                                        <asp:Label ID="lborg_idxsearch" CssClass="col-sm-2 control-label" runat="server" Text="องค์กร : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlorg_idxsearch" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            </asp:DropDownList>

                                        </div>

                                        <asp:Label ID="lbrdept_idxsearch" CssClass="col-sm-2 control-label" runat="server" Text="ฝ่าย : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlrdept_idxsearch" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                <asp:ListItem Text="เลือกฝ่าย ...." Value="00"></asp:ListItem>
                                            </asp:DropDownList>

                                        </div>

                                    </div>
                                    <%-------------- องค์กร,ฝ่าย --------------%>

                                    <%-------------- ตำแหน่ง,ชื่อพนักงาน --------------%>
                                    <div class="form-group">
                                        <asp:Label ID="lbrsec_idxsearch" CssClass="col-sm-2 control-label" runat="server" Text="แผนก : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlrsec_idxsearch" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                <asp:ListItem Text="เลือกแผนก ...." Value="00"></asp:ListItem>
                                            </asp:DropDownList>

                                        </div>


                                        <%--<asp:Label ID="lbddlposition_idxsearch" CssClass="col-sm-2 control-label" runat="server" Text="ตำแหน่ง : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlposition_idxsearch" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                <asp:ListItem Text="กรุณาเลือกตำแหน่ง ...." Value="00"></asp:ListItem>
                                            </asp:DropDownList>

                                        </div>--%>

                                        <asp:Label ID="lbemp_idxsearch" CssClass="col-sm-2 control-label" Visible="true" runat="server" Text="ชื่อพนักงาน : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlemp_idxsearch" runat="server" CssClass="form-control" Visible="true">
                                                <asp:ListItem Text="เลือกพนักงาน ...." Value="00"></asp:ListItem>
                                            </asp:DropDownList>

                                        </div>

                                    </div>
                                    <%-------------- ตำแหน่ง,ชื่อพนักงาน --------------%>

                                    <%-------------- Cost center,  ชื่อ e-mail --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label1" CssClass="col-sm-2 control-label" runat="server" Text="Cost center : " />
                                        <div class="col-sm-3">

                                            <asp:TextBox ID="txtcostcenter_no" runat="server" CssClass="form-control" MaxLength="6" placeholder="Cost center ...">
                                            </asp:TextBox>
                                        </div>

                                        <asp:Label ID="lbname_email" CssClass="col-sm-2 control-label" runat="server" Text="ชื่อ e-mail : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtname_email" runat="server" ValidationGroup="SearchIndex" CssClass="form-control" placeholder="ชื่อ e-mail ...">                                                           
                                            </asp:TextBox>

                                            <asp:RegularExpressionValidator ID="Re_txtname_email" runat="server" ValidationGroup="SearchIndex" Display="None"
                                                ErrorMessage="กรุณากรอกอีเมล์ให้ถูกต้อง" Font-Size="11"
                                                ControlToValidate="txtname_email"
                                                ValidationExpression="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?"
                                                 />
                                            <%-- <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender81" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtname_license_gmail" Width="160" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*\.\w+([-.]\w+)*" />--%>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender91" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txtname_email" Width="160" />

                                        </div>

                                    </div>
                                    <%-------------- Cost center, ชื่อ e-mail  --------------%>

                                    <%-------------- btnsearch --------------%>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <%--<div class="col-sm-12"></div>--%>
                                            <div class="pull-left">
                                                <asp:LinkButton ID="btnSearchIndex_First" CssClass="btn btn-primary" runat="server" ValidationGroup="SearchIndex" OnCommand="btnCommand" CommandName="btnSearchIndex_First" data-toggle="tooltip" title="Search" OnClientClick="return confirm('คุณต้องการค้นหารายการนี้ใช่หรือไม่ ?')"><span class="glyphicon glyphicon-search"></span></asp:LinkButton>


                                                <asp:LinkButton ID="btnCancelIndex_First" CssClass="btn btn-default" runat="server" OnCommand="btnCommand" CommandName="btnCancelIndex_First" data-toggle="tooltip" title="Cancel"><i class="fa fa-times"></i></asp:LinkButton>
                                            </div>

                                        </div>
                                    </div>
                                    <%-------------- btnsearch --------------%>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!--*** END Search Index ***-->

            <!--*** START GRIDVIEW Index *** AllowPaging="true" PageSize="10"-->
            <%-- col-md-12 m-t-10--%>
            <div class="col-md-12">
                <div id="gridviewindex" style="overflow-x: scroll; width: 100%" runat="server" class="row">
                    <%--<div id="gridviewindex" class="row" runat="server">--%>
                    <br />
                    <asp:GridView ID="GvMaster"
                        runat="server"
                        AutoGenerateColumns="false"
                        DataKeyNames="m0_idx"
                        CssClass="table table-striped table-bordered table-responsive"
                        HeaderStyle-CssClass="info"
                        ShowHeaderWhenEmpty="True"
                        OnRowEditing="Master_RowEditing"
                        OnRowUpdating="Master_RowUpdating"
                        OnRowCancelingEdit="Master_RowCancelingEdit"
                        OnPageIndexChanging="Master_PageIndexChanging"
                        OnRowDataBound="Master_RowDataBound"
                        ShowFooter="false">

                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">--- No result ---</div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center" Visible="true">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbm0_idx" runat="server" Visible="false" Text='<%# Eval("m0_idx") %>' />
                                        <%-- <asp:Label ID="lbunidx" runat="server" Visible="false" Text='<%# Eval("m0_node_idx") %>' />--%>
                                        <%# (Container.DataItemIndex +1) %>
                                    </small>
                                </ItemTemplate>



                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="รายละเอียดการสร้าง" ItemStyle-HorizontalAlign="Left"
                                HeaderStyle-CssClass="text-center" Visible="false" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <%-- <asp:Label ID="lbemp_idx" runat="server" Visible="false" Text='<%# Eval("emp_idx") %>' />--%>
                                        <asp:Label ID="DetailGooglelicenseCreate" runat="server">


                                            <p><b>&nbsp;&nbsp;&nbsp;ชื่อผู้สร้าง :</b> <%# Eval("name_create") %></p>   

                                            <p><b>&nbsp;&nbsp;&nbsp;องค์กร :</b> <%# Eval("org_name_th") %></p>                                                                             
                                            <p><b>&nbsp;&nbsp;&nbsp;ฝ่าย :</b> <%# Eval("dept_name_th") %></p>
                                       
                                            <p><b>&nbsp;&nbsp;&nbsp;แผนก :</b> <%# Eval("sec_name_th") %></p>

                                            

                                            <%--<p><b>&nbsp;&nbsp;&nbsp;เลขที่ Asset :</b> <%# Eval("u0_acc") %></p>
                                        
                                            <p><b>&nbsp;&nbsp;&nbsp;เลขที่ PO :</b> <%# Eval("u0_po") %></p>

                                            <p><b>&nbsp;&nbsp;&nbsp;เลขที่ Serial :</b> <%# Eval("u0_serial") %></p>--%>
                                                                                  
                                     
                                        </asp:Label>


                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="รายชื่อ e-mail" ItemStyle-HorizontalAlign="Left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="Name_License" runat="server" Text='<%# Eval("name_license_gmail") %>' />
                                        <asp:Label ID="Count_EmpGoogleLicnese" Visible="false" runat="server" Text='<%# Eval("Count_EmpGoogleLicnese") %>' />
                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="รายละเอียด Google License" Visible="false" ItemStyle-HorizontalAlign="Left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>

                                        <asp:Label ID="DetailLicense" runat="server">

                                        <p><b>&nbsp;&nbsp;&nbsp;เลขที่ po :</b> <%# Eval("po_code") %></p>                           
                                        <p><b>&nbsp;&nbsp;&nbsp;เลขที่ lot :</b> <%# Eval("lot_codebuy") %></p> 
                                        
                                        <p><b>&nbsp;&nbsp;&nbsp;องค์กร :</b> <%# Eval("OrgNameTH_inlicense") %></p> 
                                        <p><b>&nbsp;&nbsp;&nbsp;ฝ่าย :</b> <%# Eval("DeptNameTH_inlicense") %></p>
                                        <p><b>&nbsp;&nbsp;&nbsp;เลขที่ Costcenter :</b> <%# Eval("costcenter_no") %></p>
                                        <p><b>&nbsp;&nbsp;&nbsp;วันที่ซื้อ :</b> <%# Eval("date_purchase") %></p>
                                        <p><b>&nbsp;&nbsp;&nbsp;ราคา :</b> <%# Eval("price") %></p>
                                        <%--<p><b>&nbsp;&nbsp;&nbsp;ชื่อ software :</b> <%# Eval("software_name") %></p> --%>
                                        <p><b>&nbsp;&nbsp;&nbsp;บริษัที่ซื้อ :</b> <%# Eval("company_name") %></p> 
                                     
                                        </asp:Label>

                                        <div class="panel-body">
                                            <table class="table table-striped f-s-12">
                                                <asp:Repeater ID="rpcountogdept" runat="server">
                                                    <HeaderTemplate>
                                                        <tr>
                                                            <%--<th>#</th>--%>
                                                            <th>ชื่อ software</th>
                                                            <th>จำนวน License</th>

                                                        </tr>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>

                                                            <%--<td data-th="#"><%# (Container.ItemIndex + 1) %></td>--%>
                                                            <%--<td data-th="ชื่อ software"><%# Eval("software_name") %></td>
                                                            <td data-th="จำนวน License"><%# Eval("count_indept") %></td>--%>
                                                        </tr>

                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                            </table>
                                        </div>

                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ผู้สร้างรายการ" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-CssClass="text-center" Visible="false" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="NameCreate" runat="server" Text='<%# Eval("name_create") %>' />
                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Costcenter" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbcostcenter_no" runat="server" Text='<%# Eval("costcenter_no") %>' />
                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ประเภท e-mail" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-CssClass="text-left" HeaderStyle-Font-Size="Small">

                                <HeaderTemplate>
                                    <div class="col-md-10 col-md-offset-1">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>ประเภท e-mail</label>
                                                <asp:UpdatePanel ID="UpdatePanel_Header" runat="server">
                                                    <ContentTemplate>
                                                        <small>
                                                            <asp:DropDownList ID="ddlTypeEmailFilter" runat="server" CssClass="form-control" Font-Size="Small"
                                                                OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                                <asp:ListItem Text="--- เลือกประเภทอีเมล ---" Value="0" />
                                                                <asp:ListItem Text="Mail กลาง" Value="1" />
                                                                <asp:ListItem Text="Mail ส่วนตัว" Value="2" />
                                                            </asp:DropDownList>
                                                        </small>

                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>
                                    </div>

                                </HeaderTemplate>


                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="Type_Email" runat="server" Text='<%# Eval("m0type_email_name") %>' />
                                    </small>
                                </ItemTemplate>

                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="วันที่หมดอายุ" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="Date_expire" runat="server" Text='<%# Eval("date_expire") %>' />
                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="StatusActive" Visible="false" runat="server" Text='<%#Eval("status_active") %>' />
                                        <asp:Label ID="lbl_Count_EmpGoogleLicnese" Visible="false" runat="server" Text='<%#Eval("Count_EmpGoogleLicnese") %>' />

                                        <asp:Label ID="lbstatus_active" runat="server" Text='<%# getStatus((int)Eval("Count_EmpGoogleLicnese")) %>' />

                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ดำเนินการ" ItemStyle-HorizontalAlign="center"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>

                                    <asp:LinkButton ID="btnInsertemp" CssClass="btn btn-success" Font-Size="11px" runat="server" CommandName="btnInsertemp" OnCommand="btnCommand" data-toggle="tooltip" title="เพิ่มผู้ถือครอง" CommandArgument='<%# Eval("m0_idx") + ";" + Eval("org_idx")+ ";" + Eval("rdept_idx")+ ";" + Eval("u0_buy_idx") %>'><i class="glyphicon glyphicon-user"></i></asp:LinkButton>

                                    <asp:LinkButton ID="btnEditGoogle" CssClass="btn btn-warning" Font-Size="11px" runat="server" CommandName="btnEditGoogle" OnCommand="btnCommand" data-toggle="tooltip" title="แก้ไข" CommandArgument='<%# Eval("m0_idx") + ";" + Eval("org_idx")+ ";" + Eval("rdept_idx")+ ";" + Eval("u0_buy_idx")+ ";" + Eval("Count_EmpGoogleLicnese")+ ";" + Eval("name_license_gmail") %>'><i class="glyphicon glyphicon-pencil"></i></asp:LinkButton>

                                    <asp:LinkButton ID="btnEditCost" CssClass="btn btn-default" Visible="false" Font-Size="11px" runat="server" CommandName="btnEditCost" OnCommand="btnCommand" data-toggle="tooltip" title="โอนย้าย" CommandArgument='<%# Eval("m0_idx") + ";" + Eval("org_idx")+ ";" + Eval("rdept_idx")+ ";" + Eval("u0_buy_idx") %>'><i class="glyphicon glyphicon-floppy-open"></i></asp:LinkButton>

                                    <asp:LinkButton ID="btnDeleteemp" Visible="false" CssClass="btn btn-danger" Font-Size="11px" runat="server" CommandName="btnDeleteemp" OnCommand="btnCommand" data-toggle="tooltip" title="ลบผู้ถือครอง" CommandArgument='<%# Eval("m0_idx") + ";" + Eval("org_idx")+ ";" + Eval("rdept_idx")+ ";" + Eval("u0_buy_idx") %>'><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>

                                    <br />

                                    <%--<i class='glyphicon glyphicon-user'></i>--%>
                                    <%--<asp:Label ID="status_emp_computer" Visible="true" Text='<%# getStatusemp((int)Eval("emp_idx")) %>' runat="server"/>--%>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>


                        </Columns>
                    </asp:GridView>
                </div>
            </div>
            <!--*** END GRIDVIEW Index ***-->

            <!--*** START Insert Holder  Index ***-->
            <div class="form-group">
                <div class="col-md-12">
                    <div id="show_insert_empholderggl" class="row" runat="server" visible="false">
                        <br />
                        <div class="panel panel-default">
                            <div class="panel-heading f-bold">รายละเอียดรายการ Google - License </div>
                            <div class="panel-body ">

                                <div class="form-horizontal" role="form">

                                    <asp:FormView ID="FvViewDetail" runat="server" OnDataBound="FvDetail_DataBound" class="col-sm-12 font_text">
                                        <ItemTemplate>

                                            <div class="col-md-12">
                                                <div class="form-horizontal" role="form">

                                                    <asp:TextBox ID="txt_m0_idx_email" Visible="false" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("m0_idx") %>'>
                                                    </asp:TextBox>
                                                    <asp:TextBox ID="txt_u0_buy_idx_email" runat="server" Enabled="false" Visible="false" CssClass="form-control" Text='<%# Eval("u0_buy_idx") %>'>
                                                    </asp:TextBox>

                                                    <div class="form-group">


                                                        <asp:Label ID="lbname_create" CssClass="col-sm-2 control-label" runat="server" Text="ชื่อผู้สร้าง : " />
                                                        <div class="col-sm-3">


                                                            <%-- <asp:TextBox ID="txtemp_idxview" runat="server" Visible="false" Enabled="false" CssClass="form-control" Text='<%# Eval("emp_idx") %>'>
                                                    </asp:TextBox>--%>
                                                            <asp:TextBox ID="txtname_createview" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("name_create") %>'>
                                                            </asp:TextBox>
                                                        </div>

                                                        <asp:Label ID="lborg_name_thview" CssClass="col-sm-2 control-label" runat="server" Text="องค์กรผู้สร้าง : " />
                                                        <div class="col-sm-3">
                                                            <%-- <asp:TextBox ID="txtorg_idxview" Enabled="false" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("org_idx") %>'>
                                                    </asp:TextBox>--%>
                                                            <asp:TextBox ID="txtorg_name_thview" Enabled="false" runat="server" CssClass="form-control" Text='<%# Eval("org_name_th") %>'>
                                                            </asp:TextBox>

                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="lbdept_name_thview" CssClass="col-sm-2 control-label" runat="server" Text="ฝ่ายผู้สร้าง : " />
                                                        <div class="col-sm-3">
                                                            <%-- <asp:TextBox ID="txtemp_idxview" runat="server" Visible="false" Enabled="false" CssClass="form-control" Text='<%# Eval("emp_idx") %>'>
                                                    </asp:TextBox>--%>
                                                            <asp:TextBox ID="txtdept_name_thview" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("dept_name_th") %>'>
                                                            </asp:TextBox>
                                                        </div>

                                                        <asp:Label ID="lbsec_name_thview" CssClass="col-sm-2 control-label" runat="server" Text="แผนกผู้สร้าง : " />
                                                        <div class="col-sm-3">
                                                            <%-- <asp:TextBox ID="txtorg_idxview" Enabled="false" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("org_idx") %>'>
                                                    </asp:TextBox>--%>
                                                            <asp:TextBox ID="txtsec_name_thview" Enabled="false" runat="server" CssClass="form-control" Text='<%# Eval("sec_name_th") %>'>
                                                            </asp:TextBox>

                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="lbpo_codeview" CssClass="col-sm-2 control-label" runat="server" Text="เลขที่ PO : " />
                                                        <div class="col-sm-3">
                                                            <%-- <asp:TextBox ID="txtemp_idxview" runat="server" Visible="false" Enabled="false" CssClass="form-control" Text='<%# Eval("emp_idx") %>'>
                                                    </asp:TextBox>--%>
                                                            <asp:TextBox ID="txtpo_code_view" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("po_code") %>'>
                                                            </asp:TextBox>
                                                        </div>

                                                        <asp:Label ID="lblot_codebuyview" CssClass="col-sm-2 control-label" runat="server" Text="เลขที่ lot : " />
                                                        <div class="col-sm-3">
                                                            <%-- <asp:TextBox ID="txtorg_idxview" Enabled="false" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("org_idx") %>'>
                                                    </asp:TextBox>--%>
                                                            <asp:TextBox ID="txtlot_codebuyview" Enabled="false" runat="server" CssClass="form-control" Text='<%# Eval("lot_codebuy") %>'>
                                                            </asp:TextBox>

                                                        </div>

                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="lbdate_purchaseview" CssClass="col-sm-2 control-label" runat="server" Text="วันที่ซื้อ : " />
                                                        <div class="col-sm-3">
                                                            <%-- <asp:TextBox ID="txtemp_idxview" runat="server" Visible="false" Enabled="false" CssClass="form-control" Text='<%# Eval("emp_idx") %>'>
                                                    </asp:TextBox>--%>
                                                            <asp:TextBox ID="txtdate_purchaseview" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("date_purchase") %>'>
                                                            </asp:TextBox>
                                                        </div>

                                                        <asp:Label ID="lbdate_expireview" CssClass="col-sm-2 control-label" runat="server" Text="วันที่หมดอายุ : " />
                                                        <div class="col-sm-3">
                                                            <%-- <asp:TextBox ID="txtorg_idxview" Enabled="false" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("org_idx") %>'>
                                                    </asp:TextBox>--%>
                                                            <asp:TextBox ID="txtdate_expireview" Enabled="false" runat="server" CssClass="form-control" Text='<%# Eval("date_expire") %>'>
                                                            </asp:TextBox>

                                                        </div>

                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="lbcompany_nameview" CssClass="col-sm-2 control-label" runat="server" Text="บริษัทที่ซื้อ : " />
                                                        <div class="col-sm-3">
                                                            <%-- <asp:TextBox ID="txtemp_idxview" runat="server" Visible="false" Enabled="false" CssClass="form-control" Text='<%# Eval("emp_idx") %>'>
                                                    </asp:TextBox>--%>
                                                            <asp:TextBox ID="txtcompany_nameview" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("company_name") %>'>
                                                            </asp:TextBox>
                                                        </div>

                                                        <asp:Label ID="lbpriceview" CssClass="col-sm-2 control-label" runat="server" Text="ราคา : " />
                                                        <div class="col-sm-3">
                                                            <%-- <asp:TextBox ID="txtorg_idxview" Enabled="false" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("org_idx") %>'>
                                                    </asp:TextBox>--%>
                                                            <asp:TextBox ID="txtpriceview" Enabled="false" runat="server" CssClass="form-control" Text='<%# Eval("price") %>'>
                                                            </asp:TextBox>

                                                        </div>

                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="lbstorageview" CssClass="col-sm-2 control-label" runat="server" Text="Storage : " />
                                                        <div class="col-sm-3">
                                                            <%-- <asp:TextBox ID="txtemp_idxview" runat="server" Visible="false" Enabled="false" CssClass="form-control" Text='<%# Eval("emp_idx") %>'>
                                                    </asp:TextBox>--%>
                                                            <asp:TextBox ID="txtstorageview" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("storage") %>'>
                                                            </asp:TextBox>
                                                        </div>

                                                        <asp:Label ID="lbl_typeemail_view" CssClass="col-sm-2 control-label" runat="server" Text="ประเภท email : " />
                                                        <div class="col-sm-3">
                                                            <%-- <asp:TextBox ID="txtorg_idxview" Enabled="false" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("org_idx") %>'>
                                                    </asp:TextBox>--%>
                                                            <asp:Label ID="lbl_m0type_email_idxview" Enabled="false" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("m0type_email_idx") %>'>
                                                            </asp:Label>
                                                            <asp:TextBox ID="txt_m0type_email_nameview" Enabled="false" runat="server" CssClass="form-control" Text='<%# Eval("m0type_email_name") %>'>
                                                            </asp:TextBox>

                                                        </div>

                                                    </div>


                                                </div>

                                            </div>

                                        </ItemTemplate>

                                        <EditItemTemplate>

                                            <div class="col-md-12">
                                                <div class="form-horizontal" role="form">

                                                    <asp:TextBox ID="txt_m0_idx_email" Visible="false" runat="server" Enabled="true" CssClass="form-control" Text='<%# Eval("m0_idx") %>'>
                                                    </asp:TextBox>
                                                    <asp:TextBox ID="txt_u0_buy_idx_email" runat="server" Enabled="false" Visible="false" CssClass="form-control" Text='<%# Eval("u0_buy_idx") %>'>
                                                    </asp:TextBox>

                                                    <div class="form-group">
                                                        <asp:Label ID="lbname_create" CssClass="col-sm-2 control-label" runat="server" Text="ชื่อผู้สร้าง : " />
                                                        <div class="col-sm-3">


                                                            <%-- <asp:TextBox ID="txtemp_idxview" runat="server" Visible="false" Enabled="false" CssClass="form-control" Text='<%# Eval("emp_idx") %>'>
                                                    </asp:TextBox>--%>
                                                            <asp:TextBox ID="txtname_createview" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("name_create") %>'>
                                                            </asp:TextBox>
                                                        </div>

                                                        <asp:Label ID="lborg_name_thview" CssClass="col-sm-2 control-label" runat="server" Text="องค์กรผู้สร้าง : " />
                                                        <div class="col-sm-3">
                                                            <%-- <asp:TextBox ID="txtorg_idxview" Enabled="false" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("org_idx") %>'>
                                                    </asp:TextBox>--%>
                                                            <asp:TextBox ID="txtorg_name_thview" Enabled="false" runat="server" CssClass="form-control" Text='<%# Eval("org_name_th") %>'>
                                                            </asp:TextBox>

                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="lbdept_name_thview" CssClass="col-sm-2 control-label" runat="server" Text="ฝ่ายผู้สร้าง : " />
                                                        <div class="col-sm-3">
                                                            <%-- <asp:TextBox ID="txtemp_idxview" runat="server" Visible="false" Enabled="false" CssClass="form-control" Text='<%# Eval("emp_idx") %>'>
                                                    </asp:TextBox>--%>
                                                            <asp:TextBox ID="txtdept_name_thview" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("dept_name_th") %>'>
                                                            </asp:TextBox>
                                                        </div>

                                                        <asp:Label ID="lbsec_name_thview" CssClass="col-sm-2 control-label" runat="server" Text="แผนกผู้สร้าง : " />
                                                        <div class="col-sm-3">
                                                            <%-- <asp:TextBox ID="txtorg_idxview" Enabled="false" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("org_idx") %>'>
                                                    </asp:TextBox>--%>
                                                            <asp:TextBox ID="txtsec_name_thview" Enabled="false" runat="server" CssClass="form-control" Text='<%# Eval("sec_name_th") %>'>
                                                            </asp:TextBox>

                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="lbpo_codeview" CssClass="col-sm-2 control-label" runat="server" Text="เลขที่ PO : " />
                                                        <div class="col-sm-3">
                                                            <%-- <asp:TextBox ID="txtemp_idxview" runat="server" Visible="false" Enabled="false" CssClass="form-control" Text='<%# Eval("emp_idx") %>'>
                                                    </asp:TextBox>--%>
                                                            <asp:TextBox ID="txtpo_code_view" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("po_code") %>'>
                                                            </asp:TextBox>
                                                        </div>

                                                        <asp:Label ID="lblot_codebuyview" CssClass="col-sm-2 control-label" runat="server" Text="เลขที่ lot : " />
                                                        <div class="col-sm-3">
                                                            <%-- <asp:TextBox ID="txtorg_idxview" Enabled="false" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("org_idx") %>'>
                                                    </asp:TextBox>--%>
                                                            <asp:TextBox ID="txtlot_codebuyview" Enabled="false" runat="server" CssClass="form-control" Text='<%# Eval("lot_codebuy") %>'>
                                                            </asp:TextBox>

                                                        </div>

                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="lbdate_purchaseview" CssClass="col-sm-2 control-label" runat="server" Text="วันที่ซื้อ : " />
                                                        <div class="col-sm-3">
                                                            <%-- <asp:TextBox ID="txtemp_idxview" runat="server" Visible="false" Enabled="false" CssClass="form-control" Text='<%# Eval("emp_idx") %>'>
                                                    </asp:TextBox>--%>
                                                            <asp:TextBox ID="txtdate_purchaseview" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("date_purchase") %>'>
                                                            </asp:TextBox>
                                                        </div>

                                                        <asp:Label ID="lbdate_expireview" CssClass="col-sm-2 control-label" runat="server" Text="วันที่หมดอายุ : " />
                                                        <div class="col-sm-3">
                                                            <%-- <asp:TextBox ID="txtorg_idxview" Enabled="false" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("org_idx") %>'>
                                                    </asp:TextBox>--%>
                                                            <asp:TextBox ID="txtdate_expireview" Enabled="false" runat="server" CssClass="form-control" Text='<%# Eval("date_expire") %>'>
                                                            </asp:TextBox>

                                                        </div>

                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="lbcompany_nameview" CssClass="col-sm-2 control-label" runat="server" Text="บริษัทที่ซื้อ : " />
                                                        <div class="col-sm-3">
                                                            <%-- <asp:TextBox ID="txtemp_idxview" runat="server" Visible="false" Enabled="false" CssClass="form-control" Text='<%# Eval("emp_idx") %>'>
                                                    </asp:TextBox>--%>
                                                            <asp:TextBox ID="txtcompany_nameview" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("company_name") %>'>
                                                            </asp:TextBox>
                                                        </div>

                                                        <asp:Label ID="lbpriceview" CssClass="col-sm-2 control-label" runat="server" Text="ราคา : " />
                                                        <div class="col-sm-3">
                                                            <%-- <asp:TextBox ID="txtorg_idxview" Enabled="false" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("org_idx") %>'>
                                                    </asp:TextBox>--%>
                                                            <asp:TextBox ID="txtpriceview" Enabled="false" runat="server" CssClass="form-control" Text='<%# Eval("price") %>'>
                                                            </asp:TextBox>

                                                        </div>

                                                    </div>


                                                    <div class="panel panel-default">
                                                        <div class="panel-heading f-bold">แก้ไข Google - License </div>
                                                        <div class="panel-body ">

                                                            <div class="form-horizontal" role="form">

                                                                <div class="form-group">
                                                                    <asp:Label ID="lbname_license_gmail" CssClass="col-sm-2 control-label" runat="server" Text="รายชื่อ e-mail : " />
                                                                    <div class="col-sm-3">
                                                                        <%-- <asp:TextBox ID="txtemp_idxview" runat="server" Visible="false" Enabled="false" CssClass="form-control" Text='<%# Eval("emp_idx") %>'>
                                                    </asp:TextBox>--%>
                                                                        <asp:TextBox ID="txtname_license_gmail_old" runat="server" Enabled="false" Visible="false" CssClass="form-control" Text='<%# Eval("name_license_gmail") %>'>
                                                                        </asp:TextBox>

                                                                        <asp:TextBox ID="txtname_license_gmail" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("name_license_gmail") %>'>
                                                                        </asp:TextBox>

                                                                        <%-- <asp:RequiredFieldValidator ID="Requiredtxtname_license_gmail" ValidationGroup="SaveEditEmail" runat="server" Display="None"
                                                                            ControlToValidate="txtname_license_gmail" Font-Size="11"
                                                                            ErrorMessage="กรุณากรอกอีเมล์"
                                                                            ValidationExpression="กรุณากรอกอีเมล์" />--%>

                                                                        <asp:RegularExpressionValidator ID="Requiredtxtname_license_gmail1" runat="server" ValidationGroup="SaveEditEmail" Display="None"
                                                                            ErrorMessage="กรุณากรอกอีเมล์ให้ถูกต้อง" Font-Size="11"
                                                                            ControlToValidate="txtname_license_gmail"
                                                                            ValidationExpression="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?" />
                                                                        <%-- <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender81" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtname_license_gmail" Width="160" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*\.\w+([-.]\w+)*" />--%>
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender91" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtname_license_gmail1" Width="160" />

                                                                    </div>

                                                                    <asp:Label ID="lbStoragegmail_old" CssClass="col-sm-2 control-label" runat="server" Text="Storage : " />
                                                                    <div class="col-sm-3">
                                                                        <%-- <asp:TextBox ID="txtemp_idxview" runat="server" Visible="false" Enabled="false" CssClass="form-control" Text='<%# Eval("emp_idx") %>'>
                                                    </asp:TextBox>--%>
                                                                        <asp:TextBox ID="txtStoragegmail_old" runat="server" Enabled="false" Visible="false" CssClass="form-control" Text='<%# Eval("storage") %>'>
                                                                        </asp:TextBox>

                                                                        <asp:TextBox ID="txtstorage_gmail" runat="server" MaxLength="3" Enabled="false" CssClass="form-control" Text='<%# Eval("storage") %>'>
                                                                        </asp:TextBox>

                                                                        <asp:RequiredFieldValidator ID="Requiredtxtname_license_gmail" ValidationGroup="SaveEditEmail" runat="server" Display="None"
                                                                            ControlToValidate="txtstorage_gmail" Font-Size="11"
                                                                            ErrorMessage="กรุณากรอก storage ที่ใช้งาน"
                                                                            ValidationExpression="กรุณากรอก storage ที่ใช้งาน" />

                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationGroup="SaveEditEmail" Display="None"
                                                                            ErrorMessage="กรุณากรอกเฉพาะตัวเลข" Font-Size="11"
                                                                            ControlToValidate="txtstorage_gmail"
                                                                            ValidationExpression="^\d+" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender81" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtname_license_gmail" Width="160" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender15" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtname_license_gmail1" Width="160" />

                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <%--   <asp:Label ID="lbstorageview" CssClass="col-sm-2 control-label" runat="server" Text="Storage : " />
                                                                    <div class="col-sm-3">
                                                         
                                                                        <asp:TextBox ID="txtstorageview" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("storage") %>'>
                                                                        </asp:TextBox>
                                                                    </div>--%>

                                                                    <asp:Label ID="lbl_typeemail_view" CssClass="col-sm-2 control-label" runat="server" Text="ประเภท email : " />
                                                                    <div class="col-sm-3">

                                                                        <asp:Label ID="lbl_m0type_email_idxview" Enabled="false" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("m0type_email_idx") %>'>
                                                                        </asp:Label>
                                                                        <asp:TextBox ID="txt_m0type_email_nameview" Enabled="false" runat="server" CssClass="form-control" Text='<%# Eval("m0type_email_name") %>'>
                                                                        </asp:TextBox>

                                                                    </div>
                                                                    <div class="col-sm-7"></div>

                                                                </div>


                                                                <div class="form-group">

                                                                    <div class="col-md-12">
                                                                        <%-- <div class="col-lg-offset-6 col-md-6">--%>
                                                                        <div class="pull-right">
                                                                            <asp:LinkButton ID="btnSaveEditEmail" CssClass="btn btn-success" runat="server" CommandName="btnSaveEditEmail" OnCommand="btnCommand" ValidationGroup="SaveEditEmail" OnClientClick="return confirm('คุณต้องการเพิ่มรายการนี้ใช่หรือไม่ ?')">บันทึก</asp:LinkButton>

                                                                            <asp:LinkButton ID="btnCancelEditEmail" CssClass="btn btn-danger" runat="server" OnCommand="btnCommand" CommandName="btnCancelEditEmail">ยกเลิก</asp:LinkButton>
                                                                            <%--<br />--%>
                                                                        </div>
                                                                    </div>



                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        </EditItemTemplate>

                                    </asp:FormView>


                                </div>


                            </div>
                        </div>

                    </div>

                </div>
            </div>
            <!--*** END Insert Holder  Index ***-->

            <!--*** START Gv Show Detail Holder Gmail  Index ***-->
            <div class="form-group" visible="false" runat="server" id="gridview_showdetail_holder">

                <div class="col-md-12" id="div_showholdergmail" runat="server" visible="false">
                    <div class="panel panel-default">


                        <div id="div_ggl_licenseholder" runat="server" visible="false" class="panel-heading f-bold">รายละเอียดผู้ถือครอง Google - License</div>
                        <div class="panel-body ">

                            <div class="form-horizontal" role="form">

                                <asp:GridView ID="GvShowDetailHolder"
                                    runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="table table-striped table-bordered table-responsive"
                                    DataKeyNames="m1_idx"
                                    HeaderStyle-CssClass="primary"
                                    AllowPaging="true"
                                    PageSize="10"
                                    OnRowEditing="Master_RowEditing"
                                    OnRowUpdating="Master_RowUpdating"
                                    OnRowCancelingEdit="Master_RowCancelingEdit"
                                    OnPageIndexChanging="Master_PageIndexChanging"
                                    OnRowDataBound="Master_RowDataBound">
                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                    <EmptyDataTemplate>
                                        <div style="text-align: center">No result</div>
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center" Visible="true">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lbu0_idx" runat="server" Visible="false" Text='<%# Eval("m0_idx") %>' />
                                                    <asp:Label ID="lbm1_idx" runat="server" Visible="false" Text='<%# Eval("m1_idx") %>' />
                                                    <%# (Container.DataItemIndex +1) %>
                                                </small>
                                            </ItemTemplate>

                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="รายชื่อผู้ถือครอง" ItemStyle-HorizontalAlign="left"
                                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <small>

                                                    <asp:Label ID="lbname_holder_emaildetail" runat="server" Text='<%# Eval("name_holder_email") %>' />
                                                    <asp:Label ID="lbemp_idxdetail" Visible="false" runat="server" Text='<%# Eval("emp_idx") %>' />

                                                </small>
                                            </ItemTemplate>
                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="แผนก" ItemStyle-HorizontalAlign="left"
                                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lbsec_name_thdetail" runat="server" Text='<%# Eval("sec_name_th") %>' />
                                                    <asp:Label ID="lbrsec_idxdetail" Visible="false" runat="server" Text='<%# Eval("rsec_idx") %>' />

                                                </small>
                                            </ItemTemplate>
                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ชื่อ e-mail" ItemStyle-HorizontalAlign="left"
                                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <small>

                                                    <asp:Label ID="lbname_license_gmaildetail" runat="server" Text='<%# Eval("name_license_gmail") %>' />
                                                </small>
                                            </ItemTemplate>
                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>

                            </div>

                        </div>

                    </div>

                </div>
            </div>
            <!--*** END Gv Show Detail Holder Gmail  Index ***-->

            <!--*** START Insert Holder  Index ***-->
            <div class="form-group">
                <div class="col-md-12">
                    <div id="Show_InsertHolder" class="row" runat="server" visible="false">
                        <%-- <br />--%>
                        <div class="panel panel-info">
                            <div class="panel-heading f-bold">เพิ่มผู้ถือครอง Google - License</div>
                            <div class="panel-body ">

                                <div class="form-horizontal" role="form">

                                    <asp:FormView ID="FvInsert_HolderLicense" runat="server" OnDataBound="FvDetail_DataBound" class="col-sm-12 font_text">
                                        <ItemTemplate>

                                            <div class="col-md-12">
                                                <div class="form-horizontal" role="form">
                                                    <%--<div class="panel panel-default">
                                                    <div class="panel-heading">--%>


                                                    <div class="form-group">
                                                        <asp:Label ID="lbl_type_email" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทอีเมล : " />
                                                        <div class="col-sm-8">
                                                            <asp:DropDownList ID="ddl_type_email" runat="server" Enabled="true" CssClass="form-control"
                                                                OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                                <%--<asp:ListItem Text="--- เลือกประเภทอีเมล ---" Value="0" />--%>
                                                                <%--<asp:ListItem Text="Mail กลาง" Value="1" />
                                                                <asp:ListItem Text="Mail ส่วนตัว" Value="2" />--%>
                                                            </asp:DropDownList>
                                                        </div>

                                                        <asp:Label ID="Label2" CssClass="col-sm-2 control-label" runat="server" />

                                                    </div>

                                                    <asp:Panel ID="Panel_SelectDetailInsert" runat="server" Visible="false">
                                                        <%-------------- องค์กร,  ฝ่าย --------------%>
                                                        <div class="form-group">
                                                            <asp:Label ID="lbOrgNameTH_inlicenseinsert" CssClass="col-sm-2 control-label" runat="server" Text="องค์กร : " />
                                                            <div class="col-sm-3">
                                                                <%-- <asp:TextBox ID="txtemp_idxview" runat="server" Visible="false" Enabled="false" CssClass="form-control" Text='<%# Eval("emp_idx") %>'>
                                                    </asp:TextBox>--%>
                                                                <asp:TextBox ID="txtorg_idxemail" runat="server" Enabled="false" Visible="false" CssClass="form-control" Text='<%# Eval("org_idx") %>'>
                                                                </asp:TextBox>
                                                                <asp:TextBox ID="txtOrgNameTH_inlicenseinsert" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("OrgNameTH_inlicense") %>'>
                                                                </asp:TextBox>
                                                            </div>

                                                            <asp:Label ID="lbDeptNameTH_inlicense" CssClass="col-sm-2 control-label" runat="server" Text="ฝ่าย : " />
                                                            <div class="col-sm-3">
                                                                <%-- <asp:TextBox ID="txtorg_idxview" Enabled="false" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("org_idx") %>'>
                                                    </asp:TextBox>--%>
                                                                <asp:TextBox ID="txtrdept_idxemail" Enabled="false" runat="server" Visible="false" CssClass="form-control" Text='<%# Eval("rdept_idx") %>'>
                                                                </asp:TextBox>
                                                                <asp:TextBox ID="txtDeptNameTH_inlicense" Enabled="false" runat="server" CssClass="form-control" Text='<%# Eval("DeptNameTH_inlicense") %>'>
                                                                </asp:TextBox>

                                                            </div>
                                                        </div>
                                                        <%-------------- องค์กร,  ฝ่าย --------------%>

                                                        <%-------------- แผนก,  ชื่อผู้ถือครอง --------------%>
                                                        <div class="form-group">
                                                            <asp:Label ID="lbrsec_idxinsert" CssClass="col-sm-2 control-label" runat="server" Text="แผนก : " />
                                                            <div class="col-sm-3">
                                                                <asp:DropDownList ID="ddlrsec_idxinsert" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                    <asp:ListItem Text="กรุณาเลือกแผนก ...." Value="00"></asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="Requireddlrsec_idxinsert" ValidationGroup="Saveinsertholder" runat="server" Display="None"
                                                                    ControlToValidate="ddlrsec_idxinsert" Font-Size="11"
                                                                    ErrorMessage="กรุณาเลือกแผนก"
                                                                    ValidationExpression="กรุณาเลือกแผนก" InitialValue="00" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requireddlrsec_idxinsert" Width="160" />
                                                            </div>

                                                            <asp:Label ID="lbemp_idxinsert" CssClass="col-sm-2 control-label" runat="server" Text="ชื่อผู้ถือครอง : " />
                                                            <div class="col-sm-3">
                                                                <asp:DropDownList ID="ddlemp_idxinsert" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                    <asp:ListItem Text="กรุณาเลือกชื่อผู้ถือครอง ...." Value="00"></asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="Requiredddlemp_idxinsert" ValidationGroup="Saveinsertholder" runat="server" Display="None"
                                                                    ControlToValidate="ddlemp_idxinsert" Font-Size="11"
                                                                    ErrorMessage="กรุณาเลือกชื่อพนักงาน"
                                                                    ValidationExpression="กรุณาเลือกชื่อพนักงาน" InitialValue="00" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredddlemp_idxinsert" Width="160" />
                                                            </div>
                                                        </div>
                                                        <%-------------- แผนก,  ชื่อผู้ถือครอง --------------%>

                                                        <%-------------- Cost center,  ชื่อ e-mail --------------%>
                                                        <div class="form-group">

                                                            <asp:Label ID="Label1" CssClass="col-sm-2 control-label" runat="server" Text="Cost center : " />
                                                            <div class="col-sm-3">
                                                                <%-- <asp:TextBox ID="txtemp_idxview" runat="server" Visible="false" Enabled="false" CssClass="form-control" Text='<%# Eval("emp_idx") %>'>
                                                    </asp:TextBox>--%>
                                                                <asp:TextBox ID="lbcostcenter_idx" runat="server" Enabled="false" Visible="false" CssClass="form-control" Text='<%# Eval("costcenter_idx") %>'>
                                                                </asp:TextBox>
                                                                <asp:TextBox ID="txtcostcenter_no" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("costcenter_no") %>'>
                                                                </asp:TextBox>
                                                            </div>

                                                            <asp:Label ID="lbname_email" CssClass="col-sm-2 control-label" runat="server" Text="ชื่อ e-mail : " />
                                                            <div class="col-sm-3">
                                                                <asp:TextBox ID="txtname_email" runat="server" ValidationGroup="Saveinsertholder" CssClass="form-control" placeholder="ชื่อ e-mail ..." Text='<%# Eval("name_license_gmail") %>'>                                                           
                                                                </asp:TextBox>

                                                                <asp:RequiredFieldValidator ID="Requiredtxtname_email" ValidationGroup="Saveinsertholder" runat="server" Display="None"
                                                                    ControlToValidate="txtname_email" Font-Size="11"
                                                                    ErrorMessage="กรุณากรอกอีเมล์"
                                                                    ValidationExpression="กรุณากรอกอีเมล์" />
                                                                <asp:RegularExpressionValidator ID="Requiredtxtname_email1" runat="server" ValidationGroup="Saveinsertholder" Display="None"
                                                                    ErrorMessage="กรุณากรอกอีเมล์ให้ถูกต้อง" Font-Size="11"
                                                                    ControlToValidate="txtname_email"
                                                                    ValidationExpression="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender81" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtname_email" Width="160" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender91" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtname_email1" Width="160" />

                                                            </div>



                                                        </div>
                                                        <%-------------- Cost center, ชื่อ e-mail  --------------%>

                                                        <%-------------- Storage --------------%>
                                                        <div class="form-group" id="show_storage" visible="false" runat="server">

                                                            <asp:Label ID="lbStorage" Visible="false" CssClass="col-sm-2 control-label" runat="server" Text="Storage(GB) : " />
                                                            <div class="col-sm-3">
                                                                <%-- <asp:TextBox ID="txtemp_idxview" runat="server" Visible="false" Enabled="false" CssClass="form-control" Text='<%# Eval("emp_idx") %>'>
                                                    </asp:TextBox>--%>
                                                                <%--<asp:TextBox ID="TextBox1" runat="server" Enabled="false" Visible="false" CssClass="form-control" Text='<%# Eval("costcenter_idx") %>'>
                                                            </asp:TextBox>--%>
                                                                <asp:TextBox ID="txtstorage_insert" Visible="false" runat="server" Enabled="false" CssClass="form-control">
                                                                </asp:TextBox>
                                                            </div>

                                                        </div>
                                                        <%-------------- Storage --------------%>
                                                    </asp:Panel>

                                                    <asp:Panel ID="Panel_AddbtnInsert" runat="server" Visible="false">
                                                        <%-------------- เพิ่มรายการ --------------%>
                                                        <div class="form-group">
                                                            <%--<div class="col-sm-2"></div>--%>
                                                            <div class="col-lg-offset-2 col-sm-10">
                                                                <asp:LinkButton ID="btnInsertHolder" CssClass="btn btn-primary" runat="server" CommandName="btnInsertHolder"
                                                                    OnCommand="btnCommand" ValidationGroup="Saveinsertholder" title="เพิ่มผู้ถือครอง">
                                                                    <i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่ม</asp:LinkButton>

                                                                <%--<asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" OnCommand="btnCommand" CommandName="btnCancel">ยกเลิก</asp:LinkButton>--%>
                                                            </div>

                                                        </div>
                                                    </asp:Panel>
                                                    <%-------------- เพิ่มรายการ --------------%>

                                                    <%-------------- GridView แสดงรายละเอียด  จำนวน License แต่ละฝ่าย  --------------%>
                                                    <asp:Panel ID="gvmail_holder" runat="server" Visible="false" AutoPostBack="true">
                                                        <div class="col-md-12">
                                                            <asp:GridView ID="GVShowdetailmailholder"
                                                                runat="server"
                                                                AutoGenerateColumns="false"
                                                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-5"
                                                                HeaderStyle-CssClass="info"
                                                                HeaderStyle-Height="40px"
                                                                AllowPaging="true"
                                                                PageSize="10"
                                                                OnRowEditing="Master_RowEditing"
                                                                OnRowDeleting="Master_RowDeleting"
                                                                OnPageIndexChanging="Master_PageIndexChanging">
                                                                <PagerStyle CssClass="pageCustom" />
                                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />

                                                                <EmptyDataTemplate>
                                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                                </EmptyDataTemplate>

                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                                        <ItemTemplate>
                                                                            <small>
                                                                                <%# (Container.DataItemIndex +1) %>

                                                                            </small>
                                                                            <%-- <%# (Container.DataItemIndex +1) %>--%>
                                                                        </ItemTemplate>

                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="องค์กร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                        <ItemTemplate>
                                                                            <small>

                                                                                <asp:Label ID="lborgname_th_emailholder" runat="server" CssClass="col-sm-12" Text='<%# Eval("orgname_th_emailholder") %>'></asp:Label>
                                                                                <asp:Label ID="lborg_idx_emailholger" runat="server" Visible="false" CssClass="col-sm-12" Text='<%# Eval("org_idx_emailholger") %>'></asp:Label>
                                                                            </small>
                                                                        </ItemTemplate>

                                                                        <EditItemTemplate />
                                                                        <FooterTemplate />
                                                                    </asp:TemplateField>


                                                                    <asp:TemplateField HeaderText="ฝ่าย" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                        <ItemTemplate>
                                                                            <small>
                                                                                <asp:Label ID="lbrdeptname_th_emailholder" runat="server" CssClass="col-sm-12" Text='<%# Eval("rdeptname_th_emailholder") %>'></asp:Label>
                                                                                <asp:Label ID="lbRDeptIDX_dataset" runat="server" Visible="false" CssClass="col-sm-12" Text='<%# Eval("rdept_idx_emailholder") %>'></asp:Label>
                                                                            </small>

                                                                        </ItemTemplate>

                                                                        <EditItemTemplate />
                                                                        <FooterTemplate />
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="แผนก" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                        <ItemTemplate>
                                                                            <small>
                                                                                <asp:Label ID="lbrsecname_th_emailholder" runat="server" CssClass="col-sm-12" Text='<%# Eval("rsecname_th_emailholder") %>'></asp:Label>
                                                                                <asp:Label ID="lbrsec_idx_emailholder" runat="server" Visible="false" CssClass="col-sm-12" Text='<%# Eval("rsec_idx_emailholder") %>'></asp:Label>
                                                                            </small>

                                                                        </ItemTemplate>

                                                                        <EditItemTemplate />
                                                                        <FooterTemplate />
                                                                    </asp:TemplateField>


                                                                    <asp:TemplateField HeaderText="ชื่อผู้ถือครอง" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                        <ItemTemplate>
                                                                            <small>
                                                                                <asp:Label ID="lbempnameth_emailholder" runat="server" CssClass="col-sm-12" Text='<%# Eval("empnameth_emailholder") %>'></asp:Label>
                                                                                <asp:Label ID="lbemp_idx_emailholder" Visible="false" runat="server" CssClass="col-sm-12" Text='<%# Eval("emp_idx_emailholder") %>'></asp:Label>
                                                                            </small>
                                                                        </ItemTemplate>

                                                                        <EditItemTemplate />
                                                                        <FooterTemplate />
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="ชื่อ e-mail" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                        <ItemTemplate>
                                                                            <small>
                                                                                <asp:Label ID="lbemailname_license" runat="server" CssClass="col-sm-12" Text='<%# Eval("emailname_license") %>'></asp:Label>
                                                                            </small>
                                                                        </ItemTemplate>

                                                                        <EditItemTemplate />
                                                                        <FooterTemplate />
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="การจัดการ" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                        <ItemTemplate>
                                                                            <small>
                                                                                <asp:LinkButton ID="btnDrivingDelete" runat="server" Font-Size="Small" Text="ลบ" CssClass="btn btn-danger" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใชหรือไม่?')" CommandName="Delete" />
                                                                            </small>
                                                                        </ItemTemplate>

                                                                        <EditItemTemplate />
                                                                        <FooterTemplate />
                                                                    </asp:TemplateField>

                                                                </Columns>

                                                            </asp:GridView>
                                                        </div>

                                                    </asp:Panel>
                                                    <%-------------- GridView แสดงรายละเอียด  จำนวน License แต่ละฝ่าย --------------%>


                                                    <asp:TextBox ID="Count_numin_colum" runat="server" Visible="false"></asp:TextBox>
                                                </div>
                                            </div>


                                            <%-------------- Give จำนวน License to Dept --------------%>
                                        </ItemTemplate>

                                    </asp:FormView>


                                </div>


                            </div>
                        </div>

                    </div>

                </div>
            </div>
            <!--*** END Insert Holder  Index ***-->

            <%-------------- บันทึก / ยกเลิก --------------%>
            <div id="div_saveInsertHolder" runat="server" visible="false" class="form-group">
                <div class="col-md-12">
                    <div class="pull-right">
                        <asp:LinkButton ID="btnSaveHolderMail" CssClass="btn btn-success" runat="server" CommandName="btnSaveHolderMail" OnCommand="btnCommand" ValidationGroup="SaveinsertHolder" OnClientClick="return confirm('คุณต้องการเพิ่มรายการนี้ใช่หรือไม่ ?')">บันทึก</asp:LinkButton>

                        <asp:LinkButton ID="btnCancelHolder" CssClass="btn btn-danger" runat="server" OnCommand="btnCommand" CommandName="btnCancelHolder">ยกเลิก</asp:LinkButton>
                        <%--<br />--%>
                    </div>
                </div>

            </div>
            <%-------------- บันทึก / ยกเลิก --------------%>

            <!--*** START ข้อมูลก่อนการโอนย้าย Index ***-->
            <div class="form-group">
                <div class="col-md-12">
                    <div id="show_data_beforetranfer" class="row" runat="server" visible="false">
                        <br />
                        <div class="panel panel-default">
                            <div class="panel-heading f-bold">ข้อมูลก่อนการโอนย้าย Google - License</div>
                            <div class="panel-body ">

                                <div class="form-horizontal" role="form">

                                    <asp:FormView ID="FvBeforeTranfer" runat="server" OnDataBound="FvDetail_DataBound" class="col-sm-12 font_text">
                                        <ItemTemplate>

                                            <div class="col-md-12">
                                                <div class="form-horizontal" role="form">
                                                    <%--<div class="panel panel-default">
                                                    <div class="panel-heading">--%>

                                                    <asp:TextBox ID="txtm0_idx_tranfer" runat="server" Enabled="false" Visible="false" CssClass="form-control" Text='<%# Eval("m0_idx") %>'></asp:TextBox>

                                                    <%-------------- องค์กร,  ฝ่าย --------------%>
                                                    <div class="form-group">
                                                        <asp:Label ID="lbOrgNameTH_inlicenseinsert" CssClass="col-sm-2 control-label" runat="server" Text="องค์กร : " />
                                                        <div class="col-sm-3">
                                                            <%-- <asp:TextBox ID="txtemp_idxview" runat="server" Visible="false" Enabled="false" CssClass="form-control" Text='<%# Eval("emp_idx") %>'>
                                                    </asp:TextBox>--%>
                                                            <asp:TextBox ID="txtorg_idxemail" runat="server" Enabled="false" Visible="false" CssClass="form-control" Text='<%# Eval("org_idx") %>'>
                                                            </asp:TextBox>
                                                            <asp:TextBox ID="txtOrgNameTH_inlicenseinsert" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("OrgNameTH_inlicense") %>'>
                                                            </asp:TextBox>
                                                        </div>

                                                        <asp:Label ID="lbDeptNameTH_inlicense" CssClass="col-sm-2 control-label" runat="server" Text="ฝ่าย : " />
                                                        <div class="col-sm-3">
                                                            <%-- <asp:TextBox ID="txtorg_idxview" Enabled="false" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("org_idx") %>'>
                                                    </asp:TextBox>--%>
                                                            <asp:TextBox ID="txtrdept_idxemail" Enabled="false" runat="server" Visible="false" CssClass="form-control" Text='<%# Eval("rdept_idx") %>'>
                                                            </asp:TextBox>
                                                            <asp:TextBox ID="txtDeptNameTH_inlicense" Enabled="false" runat="server" CssClass="form-control" Text='<%# Eval("DeptNameTH_inlicense") %>'>
                                                            </asp:TextBox>

                                                        </div>
                                                    </div>
                                                    <%-------------- องค์กร,  ฝ่าย --------------%>

                                                    <%-------------- Cost center,  ชื่อ e-mail --------------%>
                                                    <div class="form-group">

                                                        <asp:Label ID="Label1" CssClass="col-sm-2 control-label" runat="server" Text="Cost center : " />
                                                        <div class="col-sm-3">
                                                            <%-- <asp:TextBox ID="txtemp_idxview" runat="server" Visible="false" Enabled="false" CssClass="form-control" Text='<%# Eval("emp_idx") %>'>
                                                    </asp:TextBox>--%>
                                                            <asp:TextBox ID="lbcostcenter_idx" runat="server" Enabled="false" Visible="false" CssClass="form-control" Text='<%# Eval("costcenter_idx") %>'>
                                                            </asp:TextBox>
                                                            <asp:TextBox ID="txtcostcenter_no" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("costcenter_no") %>'>
                                                            </asp:TextBox>
                                                        </div>

                                                    </div>
                                                    <%-------------- Cost center, ชื่อ e-mail  --------------%>
                                                </div>
                                            </div>

                                        </ItemTemplate>

                                    </asp:FormView>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
            <!--*** END ข้อมูลก่อนการโอนย้าย Index ***-->

            <!--*** START โอนย้าย ***-->
            <div class="form-group">
                <div class="col-md-12">
                    <div id="div_tranfer_googlelicense" class="row" runat="server" visible="false">
                        <%-- <br />--%>
                        <div class="panel panel-warning">
                            <div class="panel-heading f-bold">โอนย้าย Google - License </div>
                            <div class="panel-body ">

                                <div class="form-horizontal" role="form">

                                    <%-------------- องค์กร, ฝ่าย--------------%>
                                    <div class="form-group">
                                        <asp:Label ID="lborg_idxtranfer" CssClass="col-sm-2 control-label" runat="server" Text="องค์กร : " />
                                        <div class="col-sm-3">
                                            <%-- <asp:TextBox ID="txtnamecreate" CssClass="form-control" runat="server"></asp:TextBox>--%>
                                            <asp:DropDownList ID="ddlorg_idxtranfer" runat="server" AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="Requiredddlorg_idxtranfer" ValidationGroup="SaveTranfer" runat="server" Display="None"
                                                ControlToValidate="ddlorg_idxtranfer" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกองค์กร"
                                                ValidationExpression="กรุณาเลือกองค์กร" InitialValue="00" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredddlorg_idxtranfer" Width="160" />
                                        </div>

                                        <asp:Label ID="lbrdept_idxtranfer" CssClass="col-sm-2 control-label" runat="server" Text="ฝ่าย : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlrdept_idxtranfer" runat="server" CssClass="form-control">
                                                <asp:ListItem Text="กรุณาเลือกฝ่าย ...." Value="00"></asp:ListItem>
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="Requiredddlrdept_idxtranfer" ValidationGroup="SaveTranfer" runat="server" Display="None"
                                                ControlToValidate="ddlrdept_idxtranfer" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกฝ่าย"
                                                ValidationExpression="กรุณาเลือกฝ่าย" InitialValue="00" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredddlrdept_idxtranfer" Width="160" />
                                        </div>

                                    </div>
                                    <%-------------- องค์กร / ฝ่าย --------------%>

                                    <%-------------- เลขที่ Costcenter --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="lbasset_codetranfer" CssClass="col-sm-2 control-label" runat="server" Text="เลขที่ Costcenter : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtcostcenter_costtranfer" runat="server" CssClass="form-control" MaxLength="6" placeholder="เลขที่ Costcenter ..."></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="Requiredtxtcostcenter_costtranfer" ValidationGroup="SaveTranfer" runat="server" Display="None"
                                                ControlToValidate="txtcostcenter_costtranfer" Font-Size="11"
                                                ErrorMessage="กรุณากรอกเลขที่ Costcenter"
                                                ValidationExpression="กรุณากรอกเลขที่ Costcenter" />
                                            <%-- <asp:RegularExpressionValidator ID="Requiredtxtnumlicense_dept1" runat="server" ValidationGroup="saveCompany" Display="None"
                                                                    ErrorMessage="กรุณากรอกเฉพาะตัวเลข" Font-Size="11"
                                                                    ControlToValidate="txtnumlicense_dept"
                                                                    ValidationExpression="^\d+" />--%>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtcostcenter_costtranfer" Width="160" />
                                            <%-- <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtnumlicense_dept1" Width="160" />--%>
                                        </div>


                                    </div>
                                    <%-------------- เลขที่ Costcenter --------------%>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <!--*** END โอนย้าย ***-->

            <%-------------- บันทึก / ยกเลิก โอนย้าย --------------%>
            <div id="div_save_tranfer" runat="server" visible="false" class="form-group">
                <div class="col-md-12">
                    <div class="pull-right">
                        <asp:LinkButton ID="btnSaveTranfer" CssClass="btn btn-success" runat="server" CommandName="btnSaveTranfer" OnCommand="btnCommand" ValidationGroup="SaveTranfer" OnClientClick="return confirm('คุณต้องการเพิ่มรายการนี้ใช่หรือไม่ ?')">บันทึก</asp:LinkButton>

                        <asp:LinkButton ID="btnCancelTranfer" CssClass="btn btn-danger" runat="server" OnCommand="btnCommand" CommandName="btnCancelTranfer">ยกเลิก</asp:LinkButton>
                        <%--<br />--%>
                    </div>
                </div>

            </div>
            <%-------------- บันทึก / ยกเลิก โอนย้าย --------------%>

            <!--*** START  ลบผู้ถือครอง License  Index ***-->
            <div class="form-group" visible="false" runat="server" id="div_showdelete_holder">
                <div class="col-md-12" id="show_delete_emp" runat="server" visible="false">
                    <br />
                    <div class="panel panel-danger">

                        <div id="show_detail_empdelete" runat="server" visible="false" class="panel-heading f-bold">ลบผู้ถือครอง Google - License</div>
                        <div class="panel-body ">

                            <div class="form-horizontal" role="form">

                                <asp:GridView ID="GvDeleteEmpHolder"
                                    runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="table table-striped table-bordered table-responsive"
                                    DataKeyNames="m1_idx"
                                    HeaderStyle-CssClass="primary"
                                    AllowPaging="true"
                                    PageSize="10"
                                    OnRowEditing="Master_RowEditing"
                                    OnRowUpdating="Master_RowUpdating"
                                    OnRowCancelingEdit="Master_RowCancelingEdit"
                                    OnPageIndexChanging="Master_PageIndexChanging"
                                    OnRowDataBound="Master_RowDataBound">
                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                    <EmptyDataTemplate>
                                        <div style="text-align: center">No result</div>
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center" Visible="true">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lbu0_idx" runat="server" Visible="false" Text='<%# Eval("m0_idx") %>' />
                                                    <asp:Label ID="lbm1_idx" runat="server" Visible="false" Text='<%# Eval("m1_idx") %>' />
                                                    <%# (Container.DataItemIndex +1) %>
                                                </small>
                                            </ItemTemplate>

                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="รายชื่อผู้ถือครอง" ItemStyle-HorizontalAlign="left"
                                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <small>

                                                    <asp:Label ID="lbname_holder_emaildetail" runat="server" Text='<%# Eval("name_holder_email") %>' />
                                                    <asp:Label ID="lbemp_idxdetail" Visible="false" runat="server" Text='<%# Eval("emp_idx") %>' />

                                                </small>
                                            </ItemTemplate>
                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ตำแหน่ง" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lbsec_name_thdetail" runat="server" Text='<%# Eval("sec_name_th") %>' />
                                                    <asp:Label ID="lbrsec_idxdetail" Visible="false" runat="server" Text='<%# Eval("rsec_idx") %>' />

                                                </small>
                                            </ItemTemplate>
                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ชื่อ e-mail" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <small>

                                                    <asp:Label ID="lbname_license_gmaildetail" runat="server" Text='<%# Eval("name_license_gmail") %>' />
                                                </small>
                                            </ItemTemplate>
                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ประเภท e-mail" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <small>

                                                    <asp:Label ID="lbm0type_email_namedetail" runat="server" Text='<%# Eval("m0type_email_name") %>' />
                                                </small>
                                            </ItemTemplate>
                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="การจัดาร" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <small>

                                                    <asp:CheckBox ID="check_delete_emp" runat="server" AutoPostBack="true" OnCheckedChanged="CheckboxChanged" Text='<%# Container.DataItemIndex %>' Style="color: transparent;"></asp:CheckBox>

                                                    <asp:HiddenField ID="hfSelected" runat="server" Value='<%# Eval("selected") %>' />

                                                </small>
                                            </ItemTemplate>
                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>

                            </div>

                        </div>

                    </div>

                </div>
            </div>
            <!--*** END ลบผู้ถือครอง License  Index ***-->

            <%-------------- บันทึก / ยกเลิก ลบผู้ถือครอง --------------%>
            <div id="div_show_managedel_emp" runat="server" visible="false" class="form-group">
                <div class="col-md-12">
                    <div class="pull-right">
                        <asp:LinkButton ID="btnDeleteEmpLicense" CssClass="btn btn-success" runat="server" CommandName="btnDeleteEmpLicense" OnCommand="btnCommand" ValidationGroup="SaveTranfer" OnClientClick="return confirm('คุณต้องการเพิ่มรายการนี้ใช่หรือไม่ ?')">บันทึก</asp:LinkButton>

                        <asp:LinkButton ID="btnCancelEmpLicense" CssClass="btn btn-danger" runat="server" OnCommand="btnCommand" CommandName="btnCancelEmpLicense">ยกเลิก</asp:LinkButton>
                        <%--<br />--%>
                    </div>
                </div>

            </div>
            <%-------------- บันทึก / ยกเลิก ลบผู้ถือครอง --------------%>

            <!-- START Log การลบ ผู้ถือครอง License -->
            <div class="form-group">
                <div class="col-md-12">
                    <br />
                    <div class="row panel panel-default font_text" visible="false" runat="server" id="panel_delete_googlelicense">
                        <div class="panel panel-default">
                            <%-- <h3 class="panel-heading f-bold"><strong>ประวัติข้อมูล</strong></h3>--%>
                            <div class="panel-heading f-bold">ประวัติการลบผู้ถือครอง Google - License</div>
                        </div>


                        <%-- <asp:TextBox ID="testbind" runat="server"></asp:TextBox>--%>
                        <div class="panel-body">
                            <table class="table table-striped f-s-12">
                                <%--<asp:GridView ID="gvTest" runat="server"></asp:GridView>--%>
                                <asp:Repeater ID="rptLogDeleteHolderGooglelicense" runat="server">
                                    <HeaderTemplate>
                                        <tr>
                                            <th>วัน/ เวลา</th>
                                            <th>ผู้ดำเนินการ</th>
                                            <th>รายชื่อผู้ที่ถูกลบข้อมูล</th>
                                            <%-- <th>ผลการดำเนินการ</th>
                                            <th>ความคิดเห็น</th>--%>
                                        </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <%--<td data-th="ลำดับ">
                                                <%# (Container.ItemIndex + 1) %>

                                            </td>--%>

                                            <td data-th="วัน/ เวลา">
                                                <%--<%# Eval("OrgNameTH") %>--%>
                                                <asp:Label ID="lbcreate_datelog" runat="server" Text='<%# Eval("create_date") %>' />
                                                <%-- <asp:Label ID="lbsoftware_name_idxview" Visible="false" runat="server" Text='<%# Eval("software_name_idx") %>' />--%>

                                            </td>

                                            <td data-th="ผู้ดำเนินการ">
                                                <%--<%# Eval("OrgNameTH") %>--%>
                                                <asp:Label ID="lbname_createlog" runat="server" Text='<%# Eval("name_create") %>' /><%--&nbsp;<asp:Label ID="lbactor_deslog" runat="server" Text='<%# Eval("actor_des") %>' />--%>
                                                <%-- <asp:Label ID="lbsoftware_typeview" Visible="false" runat="server" Text='<%# Eval("software_type") %>' />--%>

                                            </td>

                                            <td data-th="รายชื่อผู้ที่ถูกลบข้อมูล">
                                                <%--<%# Eval("OrgNameTH") %>--%>
                                                <asp:Label ID="lbnode_namelog" runat="server" Text='<%# Eval("name_holder_delete") %>' />
                                                <%--<asp:Label ID="Label8" Visible="false" runat="server" Text='<%# Eval("software_name_idx") %>' />--%>
                                           
                                            </td>

                                            <%--<td data-th="ผลการดำเนินการ">--%>
                                            <%--<%# Eval("OrgNameTH") %>--%>
                                            <%-- <asp:Label ID="lbstatus_namelog" runat="server" Text='<%# Eval("status_name") %>' />--%>
                                            <%--<asp:Label ID="Label8" Visible="false" runat="server" Text='<%# Eval("software_name_idx") %>' />--%>

                                            <%--</td>--%>

                                            <%--<td data-th="ความคิดเห็น">--%>
                                            <%--<%# Eval("OrgNameTH") %>--%>
                                            <%--<asp:Label ID="lbcommentlog" runat="server" Text='<%# Eval("comment") %>' />--%>
                                            <%--<asp:Label ID="Label8" Visible="false" runat="server" Text='<%# Eval("software_name_idx") %>' />--%>

                                            <%-- </td>--%>
                                        </tr>

                                    </ItemTemplate>

                                </asp:Repeater>
                            </table>
                        </div>
                    </div>

                    <%--  <h5>&nbsp;</h5>--%>
                    <br />
                </div>
            </div>
            <!-- END Log การลบ ผู้ถือครอง License -->


            <h2>&nbsp;&nbsp;</h2>

        </asp:View>
        <%----------- ViewIndex Google License END ---------------%>

        <%----------- View Google Deviecs Insert Start ---------------%>
        <asp:View ID="ViewGoogleInsert" runat="server">

            <asp:FormView ID="FvInsert" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                <InsertItemTemplate>
                    <div class="col-sm-12">
                        <div class="panel panel-info">
                            <div class="panel-heading f-bold">เพิ่มรายการ Google-License</div>
                            <div class="panel-body">

                                <div class="form-horizontal" role="form">

                                    <%-------------- เลือกประเภทการซื้อ --------------%>
                                    <div class="form-group">
                                        <asp:Label ID="lbtype_idxinsert" CssClass="col-sm-2 control-label" runat="server" Text="เลือกประเภทการซื้อ : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddltype_idxinsert" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" CssClass="form-control">
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="Requiredddltype_idxinsert" ValidationGroup="Saveinsert" runat="server" Display="None"
                                                ControlToValidate="ddltype_idxinsert" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกประเภทการซื้อ"
                                                ValidationExpression="กรุณาเลือกประเภทการซื้อ" InitialValue="00" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1222" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredddltype_idxinsert" Width="160" />


                                        </div>

                                        <asp:Label ID="lbpo_code" CssClass="col-sm-2 control-label" runat="server" Text="เลขที่ PO : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtpo_code" CssClass="form-control" MaxLength="10" placeholder="เลขที่ PO ..." runat="server">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="Requiredtxtpo_code" ValidationGroup="Saveinsert" runat="server" Display="None"
                                                ControlToValidate="txtpo_code" Font-Size="11"
                                                ErrorMessage="กรุณากรอกเลขที่ PO"
                                                ValidationExpression="กรุณากรอกเลขที่ PO" />
                                            <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationGroup="Saveinsert" Display="None"
                                                ErrorMessage="กรุณากรอกเฉพาะตัวเลข" Font-Size="11"
                                                ControlToValidate="txtcount_licenseinsert"
                                                ValidationExpression="^\d+" />--%>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtpo_code" Width="160" />
                                            <%-- <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender13" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtcount_licenseinsert1" Width="160" />--%>
                                        </div>

                                    </div>
                                    <%-------------- เลือกประเภทการซื้อ --------------%>

                                    <%-------------- เลือก lot ที่ซื้อ --------------%>
                                    <asp:Panel ID="lot_codebuy" runat="server" Visible="false">
                                        <div class="form-group">
                                            <%--<asp:Label ID="Label1" CssClass="col-sm-2 control-label" runat="server" Text="เลือก lot ที่ต่ออายุ : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="DropDownList1" runat="server" CssClass="form-control">
                                            </asp:DropDownList>--%>

                                            <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="Saveinsert" runat="server" Display="None"
                                                ControlToValidate="ddltype_idxinsert" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกประเภทการซื้อ"
                                                ValidationExpression="กรุณาเลือกประเภทการซื้อ" InitialValue="00" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender13" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredddltype_idxinsert" Width="160" />--%>


                                            <%--  </div>--%>

                                            <asp:Label ID="lblot_codebuy" CssClass="col-sm-2 control-label" runat="server" Text="เลขที่ lot ที่ซื้อ : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtlblot_codebuy" CssClass="form-control" MaxLength="10" placeholder="เลขที่ lot ที่ซื้อ ..." runat="server">
                                                </asp:TextBox>
                                                <asp:RequiredFieldValidator ID="Requiredtxtlblot_codebuy" ValidationGroup="Saveinsert" runat="server" Display="None"
                                                    ControlToValidate="txtlblot_codebuy" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกเลขที่ lot ที่ซื้อ"
                                                    ValidationExpression="กรุณากรอกเลขที่ lot ที่ซื้อ" />
                                                <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationGroup="Saveinsert" Display="None"
                                                ErrorMessage="กรุณากรอกเฉพาะตัวเลข" Font-Size="11"
                                                ControlToValidate="txtcount_licenseinsert"
                                                ValidationExpression="^\d+" />--%>
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender14" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtlblot_codebuy" Width="160" />
                                                <%-- <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender13" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtcount_licenseinsert1" Width="160" />--%>
                                            </div>

                                        </div>
                                    </asp:Panel>
                                    <%-------------- เลือก lot ที่ซื้อ --------------%>

                                    <%-------------- เลือก lot ที่ต่ออายุ --------------%>
                                    <asp:Panel ID="lot_dateexpire" runat="server" Visible="false">
                                        <div class="form-group">
                                            <asp:Label ID="lblot_code" CssClass="col-sm-2 control-label" runat="server" Text="เลือก lot ที่ต่ออายุ : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddllblot_code" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" runat="server" CssClass="form-control">
                                                    <asp:ListItem Text="กรุณาเลือก lot ...." Value="00"></asp:ListItem>
                                                </asp:DropDownList>

                                                <asp:RequiredFieldValidator ID="Requiredddllblot_code" ValidationGroup="Saveinsert" runat="server" Display="None"
                                                    ControlToValidate="ddllblot_code" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือก lot ที่ต่ออายุ"
                                                    ValidationExpression="กรุณาเลือก lot ที่ต่ออายุ" InitialValue="00" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender13" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredddllblot_code" Width="160" />


                                            </div>

                                            <%--<asp:Label ID="Label2" CssClass="col-sm-2 control-label" runat="server" Text="เลขที่ PO : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="TextBox1" CssClass="form-control" placeholder="เลขที่ PO ..." runat="server">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="Saveinsert" runat="server" Display="None"
                                                ControlToValidate="txtpo_code" Font-Size="11"
                                                ErrorMessage="กรุณากรอกเลขที่ PO"
                                                ValidationExpression="กรุณากรอกเลขที่ PO" />--%>
                                            <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationGroup="Saveinsert" Display="None"
                                                ErrorMessage="กรุณากรอกเฉพาะตัวเลข" Font-Size="11"
                                                ControlToValidate="txtcount_licenseinsert"
                                                ValidationExpression="^\d+" />--%>
                                            <%-- <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender14" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtpo_code" Width="160" />--%>
                                            <%-- <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender13" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtcount_licenseinsert1" Width="160" />--%>

                                            <%--</div>--%>
                                        </div>
                                    </asp:Panel>
                                    <%-------------- เลือก lot ที่ต่ออายุ --------------%>

                                    <%-------------- ชื่อ Software , บริษัทที่ซื้อ --------------%>
                                    <div class="form-group">
                                        <%--<asp:Label ID="lbsoftware_name_idxinsert" CssClass="col-sm-2 control-label" runat="server" Text="ชื่อ Software : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlsoftware_name_idxinsert" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="Requiredddlsoftware_name_idxinsert" ValidationGroup="Saveinsert" runat="server" Display="None"
                                                ControlToValidate="ddlsoftware_name_idxinsert" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกชื่อ Software"
                                                ValidationExpression="กรุณาเลือกชื่อ Software" InitialValue="00" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredddlsoftware_name_idxinsert" Width="160" />
                                        </div>--%>

                                        <asp:Label ID="lbcompany_idxinsert" CssClass="col-sm-2 control-label" runat="server" Text="บริษัทที่ซื้อ : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlcompany_idxinsert" runat="server" CssClass="form-control">
                                                <%--<asp:ListItem Text="กรุณาเลือกฝ่าย ...." Value="00"></asp:ListItem>--%>
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="Requiredddlcompany_idxinsert" ValidationGroup="Saveinsert" runat="server" Display="None"
                                                ControlToValidate="ddlcompany_idxinsert" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกบริษัทที่ซื้อ"
                                                ValidationExpression="กรุณาเลือกบริษัทที่ซื้อ" InitialValue="00" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredddlcompany_idxinsert" Width="160" />

                                        </div>

                                    </div>
                                    <%-------------- ชื่อ Software , บริษัทที่ซื้อ --------------%>

                                    <%-------------- วันที่ซื้อ , วันที่หมดประกัน --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="lbdatepurchase" CssClass="col-sm-2 control-label" runat="server" Text="วันที่ซื้อ : " />
                                        <div class="col-sm-3">
                                            <div class='input-group date from-date-datepicker'>
                                                <asp:TextBox ID="txtdatepurchaseinsert" CssClass="form-control" runat="server" placeholder="วันที่ซื้อ ..."></asp:TextBox><span class="input-group-addon"><span data-icon-element="" class="glyphicon glyphicon-calendar"></span></span>
                                                <asp:RequiredFieldValidator ID="Requiredtxtdatepurchaseadd" ValidationGroup="Saveinsert" runat="server" Display="None"
                                                    ControlToValidate="txtdatepurchaseinsert" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกวันที่ซื้อ"
                                                    ValidationExpression="กรุณากรอกวันที่ซื้อ" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtdatepurchaseadd" Width="160" />
                                            </div>

                                        </div>

                                        <asp:Label ID="lbdateexpire" CssClass="col-sm-2 control-label" runat="server" Text="วันที่หมดอายุ : " />
                                        <div class="col-sm-3">
                                            <div id="show_date" class='input-group date from-date-datepickerexpire' runat="server">
                                                <asp:TextBox ID="txtdateexpireinsert" CssClass="form-control" placeholder="วันที่หมดอายุ ..." runat="server"></asp:TextBox><span class="input-group-addon"><span data-icon-element="" class="glyphicon glyphicon-calendar"></span></span>
                                                <asp:RequiredFieldValidator ID="Requiredtxtdateexpireadd" ValidationGroup="Saveinsert" runat="server" Display="None"
                                                    ControlToValidate="txtdateexpireinsert" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกวันที่หมดอายุ"
                                                    ValidationExpression="กรุณากรอกวันที่หมดอายุ" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtdateexpireadd" Width="160" />
                                            </div>
                                        </div>

                                    </div>
                                    <%-------------- วันที่ซื้อ , วันที่หมดประกัน --------------%>

                                    <%-------------- ราคาที่ซื้อ / จำนวนที่ซื้อ --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="lbprice_insert" CssClass="col-sm-2 control-label" runat="server" Text="ราคา : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtprice_insert" CssClass="form-control" placeholder="ราคา ..." runat="server">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="Requiredtxtprice_insert" ValidationGroup="Saveinsert" runat="server" Display="None"
                                                ControlToValidate="txtprice_insert" Font-Size="11"
                                                ErrorMessage="กรุณากรอกราคา"
                                                ValidationExpression="กรุณากรอกราคา" />
                                            <asp:RegularExpressionValidator ID="Requiredtxtprice_insert1" runat="server" ValidationGroup="Saveinsert" Display="None"
                                                ErrorMessage="กรุณากรอกเฉพาะตัวเลข" Font-Size="11"
                                                ControlToValidate="txtprice_insert"
                                                ValidationExpression="^\d+" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtprice_insert" Width="160" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtprice_insert1" Width="160" />

                                        </div>

                                        <asp:Label ID="lbcountlicense_insert" CssClass="col-sm-2 control-label" runat="server" Text="จำนวน License ที่ซื้อ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtcount_licenseinsert" CssClass="form-control" placeholder="จำนวน License ที่ซื้อ ..." runat="server">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="Requiredtxtcount_licenseinsert" ValidationGroup="Saveinsert" runat="server" Display="None"
                                                ControlToValidate="txtcount_licenseinsert" Font-Size="11"
                                                ErrorMessage="กรุณากรอกจำนวน License"
                                                ValidationExpression="กรุณากรอกจำนวน License" />
                                            <asp:RegularExpressionValidator ID="Requiredtxtcount_licenseinsert1" runat="server" ValidationGroup="Saveinsert" Display="None"
                                                ErrorMessage="กรุณากรอกเฉพาะตัวเลข" Font-Size="11"
                                                ControlToValidate="txtcount_licenseinsert"
                                                ValidationExpression="^\d+" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtcount_licenseinsert" Width="160" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtcount_licenseinsert1" Width="160" />

                                        </div>


                                    </div>
                                    <%-------------- ราคาที่ซื้อ / จำนวนที่ซื้อ --------------%>

                                    <%-------------- CheckBox --------------%>
                                    <div class="form-group">
                                        <%--<div class="col-sm-offset-3 col-sm-9">--%>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-4">
                                            <div class="checkbox">
                                                <asp:CheckBox ID="chk_license" runat="server" Font-Bold="true" Text="แบ่งจำนวน Lisense ตามเลขที่ Costcenter" OnCheckedChanged="CheckboxChanged" AutoPostBack="true" RepeatDirection="Vertical" />
                                                <%-- <span style="color: red; font-size: 16px; font-weight: bold; margin-left: 20px;">
                                                        <asp:Literal ID="litShowDetail" runat="server"></asp:Literal></span>--%>

                                                <asp:Panel ID="showdetailpermissionsystem" class="form-group" runat="server" Visible="false">
                                                    <div class="col-sm-12">
                                                        <asp:Label ID="detailshow" runat="server" ForeColor="Red" Font-Bold="true" Font-Size="14px" Text="กรุณาแบ่ง License ตามเลขที่ Costcenter"></asp:Label>
                                                        <%--<h4>คุณไม่มีสิทธิ์เข้าดูรายละเอียด Network Devices </h4>--%>
                                                    </div>
                                                </asp:Panel>


                                            </div>
                                        </div>


                                    </div>
                                    <%-------------- CheckBox --------------%>

                                    <%-------------- Give จำนวน License to Dept --------------%>
                                    <asp:Panel ID="PaneldetailLicense" runat="server" Visible="false">
                                        <div class="panel panel-info">
                                            <div class="panel-heading f-bold">รายละเอียดการแบ่งจำนวน License ตามเลขที่ Costcenter</div>
                                            <div class="panel-body">
                                                <div class="form-horizontal" role="form">

                                                    <%-------------- องค์กร, ฝ่าย--------------%>
                                                    <div class="form-group">
                                                        <asp:Label ID="lborg_idxinsert" CssClass="col-sm-2 control-label" runat="server" Text="องค์กร : " />
                                                        <div class="col-sm-3">
                                                            <%-- <asp:TextBox ID="txtnamecreate" CssClass="form-control" runat="server"></asp:TextBox>--%>
                                                            <asp:DropDownList ID="ddlorg_idxinsert" runat="server" AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                            </asp:DropDownList>

                                                            <asp:RequiredFieldValidator ID="Requiredddlorg_idxinsert" ValidationGroup="Saveinsertdetail" runat="server" Display="None"
                                                                ControlToValidate="ddlorg_idxinsert" Font-Size="11"
                                                                ErrorMessage="กรุณาเลือกองค์กร"
                                                                ValidationExpression="กรุณาเลือกองค์กร" InitialValue="00" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredddlorg_idxinsert" Width="160" />
                                                        </div>

                                                        <asp:Label ID="lbrdept_idxinsert" CssClass="col-sm-2 control-label" runat="server" Text="ฝ่าย : " />
                                                        <div class="col-sm-3">
                                                            <asp:DropDownList ID="ddlrdept_idxinsert" runat="server" CssClass="form-control">
                                                                <asp:ListItem Text="กรุณาเลือกฝ่าย ...." Value="00"></asp:ListItem>
                                                            </asp:DropDownList>

                                                            <asp:RequiredFieldValidator ID="Requiredddlrdept_idxinsert" ValidationGroup="Saveinsertdetail" runat="server" Display="None"
                                                                ControlToValidate="ddlrdept_idxinsert" Font-Size="11"
                                                                ErrorMessage="กรุณาเลือกฝ่าย"
                                                                ValidationExpression="กรุณาเลือกฝ่าย" InitialValue="00" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredddlrdept_idxinsert" Width="160" />
                                                        </div>

                                                    </div>
                                                    <%-------------- องค์กร / ฝ่าย --------------%>

                                                    <%-------------- เลขที่ Costcenter / จำนวน License--------------%>
                                                    <div class="form-group">

                                                        <asp:Label ID="lbasset_code" CssClass="col-sm-2 control-label" runat="server" Text="เลขที่ Costcenter : " />
                                                        <div class="col-sm-3">
                                                            <asp:TextBox ID="txtcostcenter_cost" runat="server" CssClass="form-control" MaxLength="6" placeholder="เลขที่ Costcenter ..."></asp:TextBox>

                                                            <asp:RequiredFieldValidator ID="Requiredtxtcostcenter_cost" ValidationGroup="Saveinsertdetail" runat="server" Display="None"
                                                                ControlToValidate="txtcostcenter_cost" Font-Size="11"
                                                                ErrorMessage="กรุณากรอกเลขที่ Costcenter"
                                                                ValidationExpression="กรุณากรอกเลขที่ Costcenter" />
                                                            <%-- <asp:RegularExpressionValidator ID="Requiredtxtnumlicense_dept1" runat="server" ValidationGroup="saveCompany" Display="None"
                                                                    ErrorMessage="กรุณากรอกเฉพาะตัวเลข" Font-Size="11"
                                                                    ControlToValidate="txtnumlicense_dept"
                                                                    ValidationExpression="^\d+" />--%>
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtcostcenter_cost" Width="160" />
                                                            <%-- <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtnumlicense_dept1" Width="160" />--%>
                                                        </div>

                                                        <asp:Label ID="lbnumlicense_cost" CssClass="col-sm-2 control-label" runat="server" Text="จำนวน License : " />
                                                        <div class="col-sm-3">
                                                            <asp:TextBox ID="txtnumlicense_cost" placeholder="จำนวน License ..." runat="server" CssClass="form-control"></asp:TextBox>

                                                            <asp:RequiredFieldValidator ID="Requiredtxtnumlicense_cost" ValidationGroup="Saveinsertdetail" runat="server" Display="None"
                                                                ControlToValidate="txtnumlicense_cost" Font-Size="11"
                                                                ErrorMessage="กรุณากรอกจำนวน License"
                                                                ValidationExpression="กรุณากรอกจำนวน License" />
                                                            <asp:RegularExpressionValidator ID="Requiredtxtnumlicense_cost1" runat="server" ValidationGroup="Saveinsertdetail" Display="None"
                                                                ErrorMessage="กรุณากรอกเฉพาะตัวเลข" Font-Size="11"
                                                                ControlToValidate="txtnumlicense_cost"
                                                                ValidationExpression="^\d+" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender81" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtnumlicense_cost" Width="160" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender91" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtnumlicense_cost1" Width="160" />
                                                        </div>

                                                    </div>
                                                    <%-------------- Asset No/ จำนวน License --------------%>

                                                    <%-------------- เพิ่มรายการ --------------%>
                                                    <div class="form-group">
                                                        <%--<div class="col-sm-2"></div>--%>
                                                        <div class="col-lg-offset-2 col-sm-10">
                                                            <asp:LinkButton ID="btnInsertDetail" CssClass="btn btn-default" runat="server" CommandName="btnInsertDetail" OnCommand="btnCommand" ValidationGroup="Saveinsertdetail" title="เพิ่มรายการ"><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่ม</asp:LinkButton>

                                                            <%--<asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" OnCommand="btnCommand" CommandName="btnCancel">ยกเลิก</asp:LinkButton>--%>
                                                        </div>

                                                    </div>
                                                    <%-------------- เพิ่มรายการ --------------%>

                                                    <%-------------- GridView แสดงรายละเอียด  จำนวน License แต่ละฝ่าย  --------------%>
                                                    <asp:Panel ID="gidviewdetaillicense" runat="server" Visible="false" AutoPostBack="true">
                                                        <div class="col-sm-12">
                                                            <asp:GridView ID="GVShowdetailLicense"
                                                                runat="server"
                                                                AutoGenerateColumns="false"
                                                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-5"
                                                                HeaderStyle-CssClass="info"
                                                                HeaderStyle-Height="40px"
                                                                AllowPaging="true"
                                                                PageSize="10"
                                                                OnRowEditing="Master_RowEditing"
                                                                OnRowDeleting="Master_RowDeleting"
                                                                OnPageIndexChanging="Master_PageIndexChanging">
                                                                <PagerStyle CssClass="pageCustom" />
                                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />

                                                                <EmptyDataTemplate>
                                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                                </EmptyDataTemplate>

                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                                        <ItemTemplate>
                                                                            <small>
                                                                                <%# (Container.DataItemIndex +1) %>

                                                                            </small>
                                                                            <%-- <%# (Container.DataItemIndex +1) %>--%>
                                                                        </ItemTemplate>

                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="องค์กร" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                        <ItemTemplate>
                                                                            <small>
                                                                                <%-- <asp:Label ID="lbSoftware_License_dataset" runat="server" Visible="false" CssClass="col-sm-12" Text='<%# Eval("Software_License_dataset") %>'></asp:Label>--%>

                                                                                <asp:Label ID="lbOrgNameTH_dataset" runat="server" CssClass="col-sm-12" Text='<%# Eval("OrgNameTH_dataset") %>'></asp:Label>
                                                                                <asp:Label ID="lbOrgIDX_dataset" runat="server" Visible="false" CssClass="col-sm-12" Text='<%# Eval("OrgIDX_dataset") %>'></asp:Label>
                                                                            </small>
                                                                        </ItemTemplate>

                                                                        <EditItemTemplate />
                                                                        <FooterTemplate />
                                                                    </asp:TemplateField>


                                                                    <asp:TemplateField HeaderText="ฝ่าย" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                        <ItemTemplate>
                                                                            <small>
                                                                                <asp:Label ID="lbDeptNameTH_dataset" runat="server" CssClass="col-sm-12" Text='<%# Eval("DeptNameTH_dataset") %>'></asp:Label>
                                                                                <asp:Label ID="lbRDeptIDX_dataset" runat="server" Visible="false" CssClass="col-sm-12" Text='<%# Eval("RDeptIDX_dataset") %>'></asp:Label>
                                                                            </small>

                                                                        </ItemTemplate>

                                                                        <EditItemTemplate />
                                                                        <FooterTemplate />
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="เลขที่ Costcenter" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                        <ItemTemplate>
                                                                            <small>
                                                                                <asp:Label ID="lbCostcenter_dataset" runat="server" CssClass="col-sm-12" Text='<%# Eval("Costcenter_dataset") %>'></asp:Label>
                                                                                <%-- <asp:Label ID="lbRDeptIDX_dataset" runat="server" Visible="false" CssClass="col-sm-12" Text='<%# Eval("RDeptIDX_dataset") %>'></asp:Label>--%>
                                                                            </small>

                                                                        </ItemTemplate>

                                                                        <EditItemTemplate />
                                                                        <FooterTemplate />
                                                                    </asp:TemplateField>


                                                                    <asp:TemplateField HeaderText="จำนวน License" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                        <ItemTemplate>
                                                                            <small>
                                                                                <asp:Label ID="lbnumlicense_dataset" runat="server" CssClass="col-sm-12" Text='<%# Eval("numlicense_dataset") %>'></asp:Label>
                                                                            </small>
                                                                        </ItemTemplate>

                                                                        <EditItemTemplate />
                                                                        <FooterTemplate />
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="การจัดการ" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="btnDrivingDelete" runat="server" Text="ลบ" CssClass="btn btn-danger" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใชหรือไม่?')" CommandName="Delete" />
                                                                        </ItemTemplate>

                                                                        <EditItemTemplate />
                                                                        <FooterTemplate />
                                                                    </asp:TemplateField>

                                                                </Columns>

                                                            </asp:GridView>
                                                        </div>

                                                    </asp:Panel>
                                                    <%-------------- GridView แสดงรายละเอียด  จำนวน License แต่ละฝ่าย --------------%>

                                                    <asp:TextBox ID="Count_numin_colum" runat="server" Visible="false"></asp:TextBox>



                                                </div>

                                            </div>

                                        </div>
                                    </asp:Panel>


                                </div>

                            </div>



                        </div>
                    </div>

                </InsertItemTemplate>
            </asp:FormView>

            <!--*** START Gv Show Detail Holder Gmail  Index ***-->
            <div class="form-group" visible="false" runat="server" id="show_grid_renew">

                <div class="col-md-12" id="div_showdetail_expire" runat="server" visible="false">
                    <div class="panel panel-default">


                        <div id="div_show_detailinsert_expire" runat="server" visible="false" class="panel-heading f-bold">ข้อมูล Google - License</div>
                        <div class="panel-body ">

                            <div class="form-horizontal" role="form">

                                <asp:GridView ID="GvShowDetailInsertExpire"
                                    runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="table table-striped table-bordered table-responsive" HeaderStyle-CssClass="primary"
                                    AllowPaging="true"
                                    PageSize="10"
                                    OnRowEditing="Master_RowEditing"
                                    OnRowUpdating="Master_RowUpdating"
                                    OnRowCancelingEdit="Master_RowCancelingEdit"
                                    OnPageIndexChanging="Master_PageIndexChanging"
                                    OnRowDataBound="Master_RowDataBound">
                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                    <EmptyDataTemplate>
                                        <div style="text-align: center">No result</div>
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center" Visible="true">
                                            <ItemTemplate>
                                                <small>
                                                    <%--<asp:Label ID="lbu0_idx" runat="server" Visible="false" Text='<%# Eval("m0_idx") %>' />
                                                    <asp:Label ID="lbm1_idx" runat="server" Visible="false" Text='<%# Eval("m1_idx") %>' />--%>
                                                    <%# (Container.DataItemIndex +1) %>
                                                </small>
                                            </ItemTemplate>

                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="องค์กร" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <small>

                                                    <asp:Label ID="lbOrgNameTH_inlicenseexpire" runat="server" Text='<%# Eval("OrgNameTH_inlicense") %>' />
                                                    <asp:Label ID="lborg_idxexpire" Visible="false" runat="server" Text='<%# Eval("org_idx") %>' />

                                                </small>
                                            </ItemTemplate>
                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ฝ่าย" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lbDeptNameTH_inlicenseexpire" runat="server" Text='<%# Eval("DeptNameTH_inlicense") %>' />
                                                    <asp:Label ID="lbrdept_idxexpire" Visible="false" runat="server" Text='<%# Eval("rdept_idx") %>' />

                                                </small>
                                            </ItemTemplate>
                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Cost center" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <small>

                                                    <asp:Label ID="lbcostcenter_noexpire" runat="server" Text='<%# Eval("costcenter_no") %>' />
                                                </small>
                                            </ItemTemplate>
                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="จำนวน License" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <small>

                                                    <asp:Label ID="lbnum_licenseexpire" runat="server" Text='<%# Eval("num_license") %>' />
                                                </small>
                                            </ItemTemplate>
                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>

                            </div>

                        </div>

                    </div>

                </div>
            </div>
            <!--*** END Gv Show Detail Holder Gmail  Index ***-->

            <%-------------- บันทึก / ยกเลิก --------------%>
            <div class="form-group">
                <div class="col-sm-12">
                    <div class="pull-right">
                        <asp:LinkButton ID="btnInsert" CssClass="btn btn-success" runat="server" CommandName="btnInsert" OnCommand="btnCommand" ValidationGroup="Saveinsert" OnClientClick="return confirm('คุณต้องการเพิ่มรายการนี้ใช่หรือไม่ ?')">บันทึก</asp:LinkButton>

                        <asp:LinkButton ID="btnCancel" CssClass="btn btn-danger" runat="server" OnCommand="btnCommand" CommandName="btnCancel">ยกเลิก</asp:LinkButton>
                    </div>
                </div>

            </div>
            <%-------------- บันทึก / ยกเลิก --------------%>

            <h2>&nbsp;&nbsp;</h2>
        </asp:View>
        <%----------- View Google Deviecs Insert END ---------------%>

        <%----------- View Detail Holder Google License Start ---------------%>
        <asp:View ID="ViewDetailHolderGmail" runat="server">

            <div class="col-sm-12">
                <div class="panel panel-info">
                    <div class="panel-heading f-bold">รายการผู้ถือครอง Google - License</div>
                    <%-- <div class="panel-body">--%>
                    <%--</div>--%>
                </div>
            </div>

        </asp:View>
        <%----------- View Detail Holder Google License End ---------------%>


        <%----------- View Report Google Deviecs Insert Start ---------------%>
        <asp:View ID="ViewGoogleReport" runat="server">

            <!--*** START Search Report  ***-->
            <div class="col-sm-12">
                <div class="panel panel-success">
                    <div class="panel-heading f-bold">รายงาน Google - License</div>
                    <div class="panel-body">
                        <div class="form-horizontal" role="form">
                            <%-------------- องค์กร,ฝ่าย --------------%>
                            <div class="form-group">
                                <asp:Label ID="lbddlorg_idx_report" CssClass="col-sm-2 control-label" runat="server" Text="องค์กร : " />
                                <div class="col-sm-3">
                                    <asp:DropDownList ID="ddlorg_idx_report" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                    </asp:DropDownList>

                                </div>

                                <asp:Label ID="lbddlrdept_idx_report" CssClass="col-sm-2 control-label" runat="server" Text="ฝ่าย : " />
                                <div class="col-sm-3">
                                    <asp:DropDownList ID="ddlrdept_idx_report" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                        <asp:ListItem Text="เลือกฝ่าย ...." Value="00"></asp:ListItem>
                                    </asp:DropDownList>

                                </div>

                            </div>
                            <%-------------- องค์กร,ฝ่าย --------------%>

                            <%-------------- เลือกแผนก,ชื่อพนักงาน --------------%>
                            <div class="form-group">
                                <asp:Label ID="lbddlrsec_idx_report" CssClass="col-sm-2 control-label" runat="server" Text="แผนก : " />
                                <div class="col-sm-3">
                                    <asp:DropDownList ID="ddlrsec_idx_report" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                        <asp:ListItem Text="เลือกแผนก ...." Value="00"></asp:ListItem>
                                    </asp:DropDownList>

                                </div>

                                <asp:Label ID="lbddlemp_idx_report" CssClass="col-sm-2 control-label" Visible="true" runat="server" Text="ชื่อพนักงาน : " />
                                <div class="col-sm-3">
                                    <asp:DropDownList ID="ddlemp_idx_report" runat="server" CssClass="form-control" Visible="true">
                                        <asp:ListItem Text="เลือกพนักงาน ...." Value="00"></asp:ListItem>
                                    </asp:DropDownList>

                                </div>

                            </div>
                            <%-------------- เลือกแผนก,ชื่อพนักงาน --------------%>

                            <%-------------- Cost center,  ชื่อ e-mail --------------%>
                            <div class="form-group">

                                <asp:Label ID="lbcostcenter_no_report" CssClass="col-sm-2 control-label" runat="server" Text="Cost center : " />
                                <div class="col-sm-3">

                                    <asp:TextBox ID="txtcostcenter_no_report" runat="server" CssClass="form-control" MaxLength="6" placeholder="Cost center ...">
                                    </asp:TextBox>
                                </div>

                                <asp:Label ID="lbemail_name_report" CssClass="col-sm-2 control-label" runat="server" Text="ชื่อ e-mail : " />
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtemail_name_report" runat="server" CssClass="form-control" placeholder="ชื่อ e-mail ...">                                                           
                                    </asp:TextBox>

                                </div>

                            </div>
                            <%-------------- Cost center, ชื่อ e-mail  --------------%>

                            <%-------------- btnreport --------------%>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <%--<div class="col-sm-12"></div>--%>
                                    <div class="pull-left">
                                        <asp:LinkButton ID="btnSearchReport" CssClass="btn btn-primary" runat="server" ValidationGroup="SearchReport" OnCommand="btnCommand" CommandName="btnSearchReport" data-toggle="tooltip" title="Search" OnClientClick="return confirm('คุณต้องการค้นหารายการนี้ใช่หรือไม่ ?')"><span class="glyphicon glyphicon-search"></span></asp:LinkButton>


                                        <asp:LinkButton ID="btnCancelReport" CssClass="btn btn-default" runat="server" OnCommand="btnCommand" CommandName="btnCancelReport" data-toggle="tooltip" title="Cancel"><i class="fa fa-times"></i></asp:LinkButton>
                                    </div>

                                </div>
                            </div>
                            <%-------------- btnreport --------------%>
                        </div>
                    </div>
                </div>
            </div>
            <!--*** START Search Report  ***-->

            <!--*** START GRIDVIEW Index ***-->
            <%-- col-md-12 m-t-10--%>
            <div class="col-md-12">
                <div id="gridviewreport" style="overflow-x: scroll; width: 100%" runat="server" visible="false" class="row">
                    <%--<div id="gridviewreport" runat="server" visible="false">--%>
                    <%--<br />--%>
                    <asp:GridView ID="GvReport"
                        runat="server"
                        AutoGenerateColumns="false"
                        DataKeyNames="m0_idx"
                        CssClass="table table-striped table-bordered table-responsive"
                        HeaderStyle-CssClass="success"
                        AllowPaging="true"
                        PageSize="5"
                        OnRowEditing="Master_RowEditing"
                        OnRowUpdating="Master_RowUpdating"
                        OnRowCancelingEdit="Master_RowCancelingEdit"
                        OnPageIndexChanging="Master_PageIndexChanging"
                        OnRowDataBound="Master_RowDataBound">
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">-- ไม่พบข้อมูล --</div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center" Visible="true">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbm0_idx_report" runat="server" Visible="false" Text='<%# Eval("m0_idx") %>' />
                                        <%--<asp:Label ID="lbm1_idx_report" runat="server" Visible="true" Text='<%# Eval("m1_idx") %>' />--%>
                                        <%# (Container.DataItemIndex +1) %>
                                    </small>
                                </ItemTemplate>

                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="รายชื่อ e-mail" ItemStyle-HorizontalAlign="Left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <%--<asp:Label ID="lbemp_idxReport" Visible="true" runat="server" Text='<%# Eval("emp_idx") %>' />--%>
                                        <asp:Label ID="name_license_gmail_report" runat="server" Text='<%# Eval("name_license_gmail") %>' />
                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ประเภท e-mail" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <%--<asp:Label ID="lbemp_idxReport" Visible="true" runat="server" Text='<%# Eval("emp_idx") %>' />--%>
                                        <asp:Label ID="lbl_m0type_email_idx_report" runat="server" Visible="false" Text='<%# Eval("m0type_email_idx") %>' />
                                        <asp:Label ID="lbl_m0type_email_name_report" runat="server" Text='<%# Eval("m0type_email_name") %>' />
                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="รายละเอียดการสร้าง Google - License" ItemStyle-HorizontalAlign="Left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" Visible="false">
                                <ItemTemplate>
                                    <small>
                                        <%-- <asp:Label ID="lbemp_idx" runat="server" Visible="false" Text='<%# Eval("emp_idx") %>' />--%>
                                        <asp:Label ID="DetailGooglelicenseCreate" runat="server">


                                            <p><b>&nbsp;&nbsp;&nbsp;ชื่อผู้สร้าง :</b> <%# Eval("name_create") %></p>   

                                            <p><b>&nbsp;&nbsp;&nbsp;องค์กร :</b> <%# Eval("org_name_th") %></p>                                                                             
                                            <p><b>&nbsp;&nbsp;&nbsp;ฝ่าย :</b> <%# Eval("dept_name_th") %></p>
                                       
                                            <p><b>&nbsp;&nbsp;&nbsp;แผนก :</b> <%# Eval("sec_name_th") %></p>

                                           
                                        </asp:Label>


                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="รายละเอียด Google - License" ItemStyle-HorizontalAlign="Left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>

                                        <asp:Label ID="DetailLicense" runat="server">

                                        <p><b>&nbsp;&nbsp;&nbsp;เลขที่ po :</b> <%# Eval("po_code") %></p>                           
                                        <p><b>&nbsp;&nbsp;&nbsp;เลขที่ lot :</b> <%# Eval("lot_codebuy") %></p> 
                                        
                                        <p><b>&nbsp;&nbsp;&nbsp;องค์กร :</b> <%# Eval("OrgNameTH_inlicense") %></p> 
                                        <p><b>&nbsp;&nbsp;&nbsp;ฝ่าย :</b> <%# Eval("DeptNameTH_inlicense") %></p>
                                        <p><b>&nbsp;&nbsp;&nbsp;เลขที่ Costcenter :</b> <%# Eval("costcenter_no") %></p>
                                        <p><b>&nbsp;&nbsp;&nbsp;วันที่ซื้อ :</b> <%# Eval("date_purchase") %></p>
                                        <p><b>&nbsp;&nbsp;&nbsp;ราคา :</b> <%# Eval("price") %></p>
                                        <%--<p><b>&nbsp;&nbsp;&nbsp;ชื่อ software :</b> <%# Eval("software_name") %></p> --%>
                                        <p><b>&nbsp;&nbsp;&nbsp;บริษัที่ซื้อ :</b> <%# Eval("company_name") %></p> 
                                     
                                        </asp:Label>

                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="รายชื่อผู้ถือครอง" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>

                                        <div class="panel-body">
                                            <table class="table table-striped f-s-12">
                                                <asp:Repeater ID="rp_detail_holder_gmail" runat="server">
                                                    <HeaderTemplate>
                                                        <tr>
                                                            <th>ลำดับ</th>
                                                            <th id="idx_show_hd" runat="server" visible="false">รหัสผู้ถือครอง Google - License</th>
                                                            <th>รายชื่อผู้ถือครอง</th>
                                                            <%--<th>จำนวน License</th>--%>
                                                        </tr>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>

                                                            <td data-th="#"><%# (Container.ItemIndex + 1) %></td>
                                                            <td id="emp_idx_report" visible="false" runat="server" data-th="รหัสผู้ถือครอง Google - License"><%# Eval("emp_idx") %></td>
                                                            <td data-th="รายชื่อผู้ถือครอง"><%# Eval("name_holder_email") %></td>
                                                        </tr>

                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                            </table>
                                        </div>

                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>


                        </Columns>
                    </asp:GridView>
                </div>
            </div>
            <!--*** END GRIDVIEW Index ***-->

        </asp:View>
        <%----------- View Report Google Deviecs Insert End ---------------%>
    </asp:MultiView>
    <%-----------END MultiView END ---------------%>
</asp:Content>

