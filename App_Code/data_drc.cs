﻿using System;
using System.Xml.Serialization;
/// <summary>
/// Summary description for data_drc
/// </summary>
[Serializable]
[XmlRoot("data_drc")]
public class data_drc
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    // master data //
    [XmlElement("BoxTypeTopicList")]
    public TypeTopicDetail[] BoxTypeTopicList { get; set; }

    [XmlElement("BoxTypeWorkList")]
    public TypeWorkDetail[] BoxTypeWorkList { get; set; }

    [XmlElement("BoxM0SetNameList")]
    public SetNameDetail[] BoxM0SetNameList { get; set; }

    [XmlElement("BoxTopicNameList")]
    public SetTopicNameDetail[] BoxTopicNameList { get; set; }

    [XmlElement("BoxOptionList")]
    public SetOptionDetail[] BoxOptionList { get; set; }

    [XmlElement("BoxFormR0MasterList")] 
    public SetFormR0MasterDetail[] BoxFormR0MasterList { get; set; }

    [XmlElement("BoxFormR1MasterList")]
    public SetFormR1MasterDetail[] BoxFormR1MasterList { get; set; }

}


[Serializable]
public class TypeTopicDetail
{
    [XmlElement("TYIDX")]
    public int TYIDX { get; set; }

    [XmlElement("Typename")]
    public string Typename { get; set; }

    [XmlElement("Type_status")]
    public int Type_status { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("Type_statusDetail")]
    public string Type_statusDetail { get; set; }

    [XmlElement("EmpIDX")]
    public int EmpIDX { get; set; }

    [XmlElement("FullNameTH")]
    public string FullNameTH { get; set; }

    [XmlElement("EmpStatus")]
    public int EmpStatus { get; set; }

    [XmlElement("topic_name")]
    public string topic_name { get; set; }

    [XmlElement("tidx")]
    public int tidx { get; set; }

}

[Serializable]
public class TypeWorkDetail
{
    [XmlElement("TWIDX")]
    public int TWIDX { get; set; }

    [XmlElement("Type_Work")]
    public string Type_Work { get; set; }

    [XmlElement("Work_Status")]
    public int Work_Status { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("Work_StatusDetail")]
    public string Work_StatusDetail { get; set; }

}

[Serializable]
public class SetNameDetail
{
    [XmlElement("sidx")]
    public int sidx { get; set; }

    [XmlElement("setroot_idx")]
    public int setroot_idx { get; set; }

    [XmlElement("set_name")]
    public string set_name { get; set; }

    [XmlElement("set_detail")]
    public string set_detail { get; set; }

    [XmlElement("set_status")]
    public int set_status { get; set; }

    [XmlElement("cempidx")]
    public int cempidx { get; set; }

    [XmlElement("set_statusDetail")]
    public string set_statusDetail { get; set; }

    [XmlElement("tidx")]
    public int tidx { get; set; }

    [XmlElement("u0idx")]
    public int u0idx { get; set; }

    [XmlElement("R0IDX")]
    public int R0IDX { get; set; }

    [XmlElement("value2")]
    public int value2 { get; set; }

    [XmlElement("root_tidx")]
    public int root_tidx { get; set; }

    [XmlElement("value1")]
    public string value1 { get; set; }

    [XmlElement("test1")]
    public string test1 { get; set; }

}

[Serializable]
public class SetTopicNameDetail
{

    [XmlElement("tidx")]
    public int tidx { get; set; }

    [XmlElement("sidx")]
    public int sidx { get; set; }

    [XmlElement("set_name")]
    public string set_name { get; set; }

    [XmlElement("OPIDX")]
    public int OPIDX { get; set; }

    [XmlElement("Option_name")]
    public string Option_name { get; set; }

    [XmlElement("root_idx")]
    public int root_idx { get; set; }

    [XmlElement("topic_name")]
    public string topic_name { get; set; }

    [XmlElement("topic_detail")]
    public string topic_detail { get; set; }

    [XmlElement("topic_status")]
    public int topic_status { get; set; }

    [XmlElement("cempidx")]
    public int cempidx { get; set; }

    [XmlElement("topic_statusDetail")]
    public string topic_statusDetail { get; set; }

    [XmlElement("R0IDX")]
    public int R0IDX { get; set; }

    [XmlElement("u0idx")]
    public int u0idx { get; set; }

}

[Serializable]
public class SetOptionDetail
{

    [XmlElement("OPIDX")]
    public int OPIDX { get; set; }

    [XmlElement("Option_name")]
    public string Option_name { get; set; }

    [XmlElement("Option_Status")]
    public int Option_Status { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

}

[Serializable]
public class SetFormR0MasterDetail
{

    [XmlElement("R0IDX")]
    public int R0IDX { get; set; }

    [XmlElement("R0_Name")]
    public string R0_Name { get; set; }

    [XmlElement("R0_Status")]
    public int R0_Status { get; set; }

    [XmlElement("cempidx")]
    public int cempidx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("TWIDX")]
    public int TWIDX { get; set; }

    [XmlElement("R1IDX")]
    public int R1IDX { get; set; }

    [XmlElement("R1_Status")]
    public int R1_Status { get; set; }

    [XmlElement("tidx")]
    public int tidx { get; set; }

    [XmlElement("topic_name")]
    public string topic_name { get; set; }

    [XmlElement("topicnameroot")]
    public string topicnameroot { get; set; }

    [XmlElement("root_idx")]
    public int root_idx { get; set; }

    [XmlElement("sidx")]
    public int sidx { get; set; }

    [XmlElement("OPIDX")]
    public int OPIDX { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

}

[Serializable]
public class SetFormR1MasterDetail
{

    [XmlElement("R0IDX")]
    public int R0IDX { get; set; }

    [XmlElement("R0_Name")]
    public string R0_Name { get; set; }

    [XmlElement("R0_Status")]
    public int R0_Status { get; set; }

    [XmlElement("cempidx_add")]
    public int cempidx_add { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("TWIDX")]
    public int TWIDX { get; set; }

    [XmlElement("R1IDX")]
    public int R1IDX { get; set; }

    [XmlElement("R1_Status")]
    public int R1_Status { get; set; }

    [XmlElement("tidx_add")]
    public int tidx_add { get; set; }

    [XmlElement("tidx")]
    public int tidx { get; set; }

    [XmlElement("topic_name")]
    public string topic_name { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

}

