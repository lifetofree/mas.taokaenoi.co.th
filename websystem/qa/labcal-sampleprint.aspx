﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="labcal-sampleprint.aspx.cs" Inherits="websystem_qa_labcal_sampleprint" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="./../Content/bootstrap.css" runat="server" rel="stylesheet" />
    <link href="./../Content/custom.css" runat="server" rel="stylesheet" />

    <title>Sample Code Print</title>

    <style type="text/css" media="print,screen">
        @page {
            size: A4 landscape;
            margin: 25px;
        }

        @media all {
            .page-break {
                display: none;
            }
        }

        @media print {
            .page-break {
                display: block;
                page-break-before: always;
            }
        }

        .formPrint {
            margin: 10pt; /*from 5 to 10*/
            padding: 10pt; /*add*/
            width: 842pt;
        }
    </style>
</head>
<body onload="window.print()">
    <form id="form1" runat="server" width="100%">
        <script src='<%=ResolveUrl("~/Scripts/jquery-3.1.1.min.js") %>'></script>
        <script src='<%=ResolveUrl("~/Scripts/bootstrap.js") %>'></script>
        <script src='<%=ResolveUrl("~/Scripts/custom.js") %>'></script>

        <div class="formPrint">


            <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                <tbody>
                    <tr class="info" style="font-size: Small; height: 40px;">
                        <th scope="col" style="width: 1%;">No.</th>
                        <th scope="col" style="width: 10%;">Machine Name</th>
                        <th scope="col" style="width: 10%;">ID No.</th>
                        <th scope="col" style="width: 10%;">Serial No.</th>
                        <th scope="col" style="width: 5%;">certificate</th>
                     

                    </tr>
                    <tr style="font-size: Small;">
                        <td>1.</td>
                        <td>เครื่องชั่ง 1.5 Kg</td>
                        <td>LAB 1001 E</td>
                        <td>10092883317</td>
                        <td style="text-align: center">/</td>
                    

                    </tr>
                    <tr style="font-size: Small;">
                        <td>2.</td>
                        <td>เครื่องชั่ง 1.5 Kg</td>
                        <td>LAB 1002 E</td>
                        <td>10092883337</td>
                        <td style="text-align: center">/</td>
                     

                    </tr>
                    <tr style="font-size: Small;">
                        <td>3.</td>
                        <td>เครื่องชั่ง 1.5 Kg</td>
                        <td>LAB 1003 E</td>
                        <td>10092883307</td>
                        <td style="text-align:center">-</td>
                       

                    </tr>
                </tbody>
            </table>

        </div>
    </form>
</body>
</html>
