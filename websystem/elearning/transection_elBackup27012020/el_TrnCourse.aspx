﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="el_TrnCourse.aspx.cs" Inherits="websystem_el_TrnCourse" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">


    <asp:HyperLink ID="SETFOCUS" runat="server"></asp:HyperLink>
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    <asp:Panel ID="Panel3" runat="server" CssClass="m-t-10">
        <asp:Panel ID="panelMenu" runat="server">
            <nav class="navbar navbar-default">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand">Menu</a>
                </div>
                <div class="collapse navbar-collapse p-l-10" style="overflow-x: hidden;" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-left" runat="server">
                        <li id="liListData" runat="server">
                            <asp:LinkButton ID="btnListData" runat="server"
                                OnCommand="btnCommand" CommandName="btnListData" Text="ข้อมูลทั่วไป" />

                        </li>
                        <li id="liInsert" runat="server">
                            <asp:LinkButton ID="btnInsert" runat="server"
                                OnCommand="btnCommand" CommandName="btnInsert" Text="สร้างหลักสูตร" />

                        </li>
                        <li id="liSearchTrnNeedsSurvey" runat="server">
                            <asp:LinkButton ID="btnSearchTrnNeedsSurvey" runat="server"
                                OnCommand="btnCommand" CommandName="btnSearchTrnNeedsSurvey" Text="รายงานสรุปหลักสูตรที่ขอ" />

                        </li>
                        <li id="liSearchTrnNeedsSurveyDEPT" runat="server">
                            <asp:LinkButton ID="btnSearchTrnNeedsSurveyDEPT" runat="server"
                                OnCommand="btnCommand" CommandName="btnSearchTrnNeedsSurveyDEPT" Text="รายงานสรุปหลักสูตรที่ขอของแต่ละแผนก" />

                        </li>
                    </ul>
                </div>
            </nav>

            <div class="clearfix"></div>

        </asp:Panel>

        <asp:Panel ID="pnlmenu_detail" runat="server">

            <nav class="navbar navbar-default">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <%--<a class="navbar-brand">Menu</a>--%>
                </div>
                <div class="collapse navbar-collapse p-l-10" style="overflow-x: hidden;" id="bs-example-navbar-collapse-2">
                    <ul class="nav navbar-nav navbar-left" runat="server">
                        <li id="litraining" runat="server">
                            <asp:LinkButton ID="btncourse" runat="server"
                                OnCommand="btnCommand" CommandName="btncourse" Text="หลักสูตร" />
                        </li>
                        <li id="litest" runat="server">
                            <asp:LinkButton ID="btntest" runat="server"
                                OnCommand="btnCommand" CommandName="btntest" Text="แบบทดสอบ" />
                        </li>
                        <li id="lievaluationform" runat="server">
                            <asp:LinkButton ID="btnevaluationform" runat="server"
                                OnCommand="btnCommand" CommandName="btnevaluationform" Text="แบบประเมิน" />

                        </li>
                    </ul>
                </div>
            </nav>

            <div class="clearfix"></div>

        </asp:Panel>
    </asp:Panel>

    <asp:MultiView ID="MultiViewBody" runat="server" ActiveViewIndex="0">

        <asp:View ID="View_ListDataPag" runat="server">

            <asp:Panel ID="pnlListData" runat="server">

                <asp:Panel ID="pnlsearch" runat="server">
                    <div class="panel panel-primary m-t-10">
                        <div class="panel-heading f-bold">ค้นหา</div>
                        <div class="panel-body">
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label>เดือน</label>
                                    <asp:DropDownList ID="ddlMonthSearch_L" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label>ปี</label>
                                    <asp:DropDownList ID="ddlYearSearch_L" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>เลขที่เอกสาร / ชื่อหลักสูตร</label>
                                    <asp:TextBox ID="txtFilterKeyword_L" runat="server"
                                        CssClass="form-control"
                                        placeholder="เลขที่เอกสาร / ชื่อหลักสูตร..." />
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <label>
                                    &nbsp;
                                </label>
                                <div class="form-group">
                                    <asp:LinkButton ID="btnFilter" runat="server" CssClass="btn btn-primary" Text="<i class='fa fa-search'></i> ค้นหา"
                                        OnCommand="btnCommand" CommandName="btnFilter" />
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>


                <div class="row">

                    <asp:GridView ID="GvListData"
                        runat="server"
                        AutoGenerateColumns="false"
                        CssClass="table table-striped table-bordered table-responsive col-md-12 word-wrap"
                        HeaderStyle-CssClass="info"
                        AllowPaging="true"
                        PageSize="10"
                        OnRowCommand="onRowCommand"
                        OnRowDataBound="onRowDataBound"
                        OnPageIndexChanging="gvPageIndexChanging"
                        DataKeyNames="u0_course_idx"
                        ShowFooter="false">
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />

                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่พบข้อมูล</div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                                <ItemTemplate>
                                    <div class="word-wrap">
                                        <%# (Container.DataItemIndex + 1) %>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="เลขที่เอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="12%">
                                <ItemTemplate>
                                    <div class="word-wrap">
                                        <%# Eval("course_no") %>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="วันที่เปิดหลักสูตร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                                <ItemTemplate>
                                    <div class="word-wrap">
                                        <%# Eval("course_date") %>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ชื่อหลักสูตร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="13%">
                                <ItemTemplate>
                                    <div class="word-wrap">
                                        <%# Eval("course_name") %>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="กลุ่มวิชา" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                                <ItemTemplate>
                                    <div class="word-wrap">
                                        <%# Eval("training_group_name") %>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="สาขาวิชา" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                                <ItemTemplate>
                                    <div class="word-wrap">
                                        <%# Eval("training_branch_name") %>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="ประเภท" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                                <ItemTemplate>
                                    <div class="word-wrap">
                                        <%# getCourseType((int)Eval("course_type_etraining"),
                                                        (int)Eval("course_type_elearning")
                                                        ) %>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ระดับความสำคัญ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                                <ItemTemplate>
                                    <div class="word-wrap">
                                        <%# getpriority((int)Eval("course_priority")) %>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="แบบทดสอบ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                                <ItemTemplate>
                                    <div class="word-wrap">
                                        <%#  getStatustest((int)Eval("u2_course_test_item")) %>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="แบบประเมิน" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                                <ItemTemplate>
                                    <div class="word-wrap">
                                        <%# getStatusEvalue((int)Eval("u3_course_evaluation_item")) %>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                                <ItemTemplate>
                                    <div class="word-wrap">
                                        <%# getStatus( (int)Eval("course_status")) %>
                                        <br />
                                        <%# getStatusPlan( (int)Eval("trn_plan_flag")) %>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-HorizontalAlign="center"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%">
                                <ItemTemplate>

                                    <asp:TextBox ID="txtapprove_status_GvListData" runat="server" Visible="false" Text='<%# Eval("approve_status") %>' />
                                    <asp:TextBox ID="txtcourse_created_by_GvListData" runat="server"
                                        Visible="false" Text='<%# Eval("course_created_by") %>' />
                                    <asp:TextBox ID="txttrn_plan_flag_GvListData" runat="server"
                                        Visible="false" Text='<%# Eval("trn_plan_flag") %>' />

                                    <asp:LinkButton ID="btnDetail"
                                        CssClass="btn btn-info btn-sm" runat="server"
                                        data-original-title="รายละเอียด" data-toggle="tooltip" Text="รายละเอียด"
                                        OnCommand="btnCommand" CommandName="btnDetail" CommandArgument='<%# Eval("u0_course_idx") %>'>
                                    <i class="fa fa-file-alt"></i></asp:LinkButton>

                                    <asp:LinkButton CssClass="btn btn-warning btn-sm" runat="server"
                                        ID="btnUpdate_GvListData"
                                        CommandName="btnUpdate_GvListData" OnCommand="btnCommand"
                                        data-original-title="แก้ไข" data-toggle="tooltip" Text="แก้ไข"
                                        CommandArgument='<%# Eval("u0_course_idx") %>'>
                           <i class="fa fa-pencil-alt"></i>
                                    </asp:LinkButton>

                                    <asp:LinkButton CssClass="btn btn-danger btn-sm" runat="server"
                                        data-original-title="ลบ" data-toggle="tooltip" Text="ลบ"
                                        ID="btnDelete_GvListData"
                                        CommandName="btnDelete" OnCommand="btnCommand" CommandArgument='<%# Eval("u0_course_idx") %>'
                                        OnClientClick="return confirm('คุณต้องการลบข้อมูลนี้ใช่หรือไม่')">
                                    <i class="fa fa-trash"></i>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                </div>


            </asp:Panel>
            <%-- Start Select --%>
        </asp:View>

        <asp:View ID="View_trainingPage" runat="server">

            <%--<asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
                <ContentTemplate>--%>

            <asp:FormView ID="fvCRUD" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                <InsertItemTemplate>

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-pencil"></i><strong>&nbsp; ข้อมูลหลักสูตร</strong></h3>
                        </div>
                        <div class="panel-body">

                            <div class="form-horizontal" role="form">
                                <asp:Panel ID="Panel5" runat="server" Visible="false">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:Label ID="Label20" class="col-md-2 control-labelnotop text_right" runat="server" Text="รหัสหลักสูตร :" />
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="txtcourse_no" runat="server" CssClass="form-control" Enabled="false"
                                                        Text='<%# Eval("course_no") %>' />
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:Label ID="Label12" class="col-md-2 control-labelnotop text_right" runat="server" Text="ชื่อหลักสูตร :" />
                                            <div class="col-md-4">
                                                <asp:TextBox ID="txtcourse_name" runat="server"
                                                    CssClass="form-control" MaxLength="250"
                                                    Text='<%# Eval("course_name") %>' />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                                                    ValidationGroup="btnSaveInsert" runat="server"
                                                    Display="Dynamic"
                                                    SetFocusOnError="true"
                                                    ControlToValidate="txtcourse_name"
                                                    Font-Size="1em" ForeColor="Red"
                                                    CssClass="pull-left"
                                                    ErrorMessage="กรุณากรอกชื่อหลักสูตร" />
                                            </div>

                                            <div class="col-md-6">
                                                <asp:Label ID="Label1" CssClass="col-md-4 control-labelnotop text_right" runat="server" Text="กลุ่มวิชา : " />
                                                <div class="col-md-6">
                                                    <asp:DropDownList ID="ddlm0_training_group_idx_ref" runat="server"
                                                        AutoPostBack="true"
                                                        OnSelectedIndexChanged="FvDetail_DataBound"
                                                        CssClass="form-control">
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                        ValidationGroup="btnSaveInsert" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="ddlm0_training_group_idx_ref"
                                                        Font-Size="1em" ForeColor="Red"
                                                        CssClass="pull-left"
                                                        InitialValue="0"
                                                        ErrorMessage="กรุณากรอกกลุ่มวิชา" />
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">

                                            <asp:Label ID="Label2" class="col-md-2 control-labelnotop text_right" runat="server" Text="รายละเอียด :" />
                                            <div class="col-md-4">
                                                <asp:TextBox ID="txtcourse_remark" runat="server" CssClass="form-control"
                                                    TextMode="MultiLine" MaxLength="250"
                                                    Rows="9"
                                                    Text='<%# Eval("course_remark") %>' />
                                            </div>
                                            <div class="col-md-6">

                                                <div class="row">
                                                    <asp:Label ID="Label6" CssClass="col-md-4 control-labelnotop text_right" runat="server" Text="สาขาวิชา :" />
                                                    <div class="col-md-6">
                                                        <asp:DropDownList ID="ddm0_training_branch_idx" runat="server"
                                                            CssClass="form-control"
                                                            AutoPostBack="true"
                                                            OnSelectedIndexChanged="FvDetail_DataBound">
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14"
                                                            ValidationGroup="btnSaveInsert" runat="server"
                                                            Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="ddm0_training_branch_idx"
                                                            Font-Size="1em" ForeColor="Red"
                                                            CssClass="pull-left"
                                                            InitialValue="0"
                                                            ErrorMessage="กรุณากรอกสาขาวิชา" />
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="form-group" style="padding: 0px"></div>
                                                </div>

                                                <div class="row">
                                                    <asp:Label ID="Label3" CssClass="col-md-4 control-labelnotop text_right" runat="server" Text="วันที่เปิดหลักสูตร :" />
                                                    <div class="col-md-6">
                                                        <div class='input-group date'>
                                                            <asp:TextBox ID="txtcourse_date" runat="server"
                                                                CssClass="form-control filter-order-from"
                                                                Text='<%# Eval("course_date") %>' />
                                                            <span class="input-group-addon show-order-sale-log-from-onclick">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3"
                                                            ValidationGroup="btnSaveInsert" runat="server"
                                                            Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="txtcourse_date"
                                                            Font-Size="1em" ForeColor="Red"
                                                            CssClass="pull-left"
                                                            ErrorMessage="กรุณากรอกวันที่เปิดหลักสูตร" />
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group" style="padding: 0px"></div>
                                                </div>

                                                <div class="row">
                                                    <asp:Label ID="Label8" CssClass="col-md-4 control-labelnotop text_right" runat="server" Text="ระดับความยากของหลักสูตร :" />
                                                    <div class="col-md-6">
                                                        <asp:DropDownList ID="ddllevel_code" runat="server"
                                                            CssClass="form-control">
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4"
                                                            ValidationGroup="btnSaveInsert" runat="server"
                                                            Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="ddllevel_code"
                                                            Font-Size="1em" ForeColor="Red"
                                                            CssClass="pull-left"
                                                            InitialValue="0"
                                                            ErrorMessage="กรุณากรอกระดับความยากของหลักสูตร" />
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group" style="padding: 0px"></div>
                                                </div>

                                                <div class="row">
                                                    <asp:Label ID="Label9" CssClass="col-md-4 control-labelnotop text_right" runat="server" Text="คะแนนเต็ม :" />
                                                    <div class="col-md-6">
                                                        <asp:TextBox ID="txtcourse_score" runat="server"
                                                            CssClass="form-control"
                                                            TextMode="Number"
                                                            Text='<%# Eval("course_score") %>'>
                                                        </asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5"
                                                            ValidationGroup="btnSaveInsert" runat="server"
                                                            Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="txtcourse_score"
                                                            Font-Size="1em" ForeColor="Red"
                                                            CssClass="pull-left"
                                                            ErrorMessage="กรุณากรอกคะแนนเต็ม" />
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group" style="padding: 0px"></div>
                                                </div>
                                                <div class="row">
                                                    <asp:Label ID="Label11" CssClass="col-md-4 control-labelnotop text_right" runat="server" Text="คะแนนผ่าน % :" />
                                                    <div class="col-md-6">
                                                        <asp:TextBox ID="txtscore_through_per" runat="server"
                                                            CssClass="form-control"
                                                            TextMode="Number"
                                                            Text='<%# Eval("score_through_per") %>'>
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group" style="padding: 0px"></div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">

                                            <asp:Label ID="Label4" class="col-md-2 control-labelnotop text_right" runat="server" Text="ต้องผ่านการเรียนหลักสูตร:" />

                                            <div class="col-md-4">
                                                <div>

                                                    <div class="input-group col-md-12 pull-left">
                                                        <asp:TextBox ID="txtsearch_GvCourse" runat="server"
                                                            placeholder="ค้นหา..." CssClass="form-control" />
                                                        <div class="input-group-btn">
                                                            <asp:LinkButton ID="btnsearch_GvCourse" CssClass="btn btn-primary" runat="server"
                                                                data-original-title="ค้นหา..." data-toggle="tooltip"
                                                                OnCommand="btnCommand" CommandArgument="1"
                                                                CommandName="btnsearch_GvCourse"><i class="glyphicon glyphicon-search"></i></asp:LinkButton>
                                                        </div>
                                                    </div>

                                                </div>
                                                <br>
                                                <div class="col-md-12"></div>
                                                <br />
                                                <div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-body" style="height: 200px; overflow-x: scroll; overflow-y: scroll;">
                                                            <div>

                                                                <asp:GridView ID="GvCourse"
                                                                    runat="server"
                                                                    AutoGenerateColumns="false"
                                                                    CssClass="col-md-12"
                                                                    HeaderStyle-CssClass="default"
                                                                    ShowFooter="false"
                                                                    AllowPaging="false"
                                                                    ShowHeader="false"
                                                                    GridLines="None"
                                                                    DataKeyNames="zId">
                                                                    <EmptyDataTemplate>
                                                                        <div style="text-align: center">ไม่พบข้อมูล</div>
                                                                    </EmptyDataTemplate>
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                                                            HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
                                                                            <ItemTemplate>
                                                                                <small>
                                                                                    <asp:CheckBox ID="GvCourse_cb_zId" runat="server" />
                                                                                </small>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="ตำแหน่ง" ItemStyle-HorizontalAlign="Left"
                                                                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                            <ItemTemplate>
                                                                                <small>
                                                                                    <asp:Label CssClass="control-label" ID="GvCourse_zName" runat="server" Text='<%# Eval("zName") %>' />
                                                                                    <asp:Label ID="GvCourse_zId" runat="server" Text='<%# Eval("zId") %>' Visible="false" />
                                                                                </small>
                                                                            </ItemTemplate>
                                                                            <EditItemTemplate />
                                                                            <FooterTemplate />
                                                                        </asp:TemplateField>

                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>


                                            <div class="col-md-6">
                                                <asp:Label ID="Label5" CssClass="col-md-4 control-labelnotop text_right" runat="server" Text="ระดับพนักงาน :" />
                                                <div class="col-md-6">
                                                    <div class="panel panel-default">
                                                        <div class="panel-body" style="height: 200px; overflow-x: scroll; overflow-y: scroll;">
                                                            <asp:GridView ID="GvM0_EmpTarget"
                                                                runat="server"
                                                                AutoGenerateColumns="false"
                                                                CssClass="col-md-12"
                                                                HeaderStyle-CssClass="default"
                                                                ShowFooter="false"
                                                                AllowPaging="false"
                                                                ShowHeader="false"
                                                                GridLines="None"
                                                                DataKeyNames="zId">
                                                                <EmptyDataTemplate>
                                                                    <div style="text-align: center">ไม่พบข้อมูล</div>
                                                                </EmptyDataTemplate>
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                                                        HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
                                                                        <ItemTemplate>
                                                                            <small>
                                                                                <asp:CheckBox ID="GvM0_EmpTarget_cb_zId" runat="server" />
                                                                            </small>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="ตำแหน่ง" ItemStyle-HorizontalAlign="Left"
                                                                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                        <ItemTemplate>
                                                                            <small>
                                                                                <asp:Label CssClass="control-label" ID="GvM0_EmpTarget_zName" runat="server" Text='<%# Eval("zName") %>' />
                                                                                <asp:Label ID="GvM0_EmpTarget_zId" runat="server" Text='<%# Eval("zId") %>' Visible="false" />
                                                                            </small>
                                                                        </ItemTemplate>
                                                                        <EditItemTemplate />
                                                                        <FooterTemplate />
                                                                    </asp:TemplateField>

                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:Label ID="Label16" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="ประเภท :" />
                                            <div class="col-md-4">
                                                <asp:CheckBox ID="cbcourse_type_etraining" runat="server" Text=""
                                                    Checked='<%# getDataCheckbox((string)Eval("course_type_etraining")) %>' />
                                                <asp:Label ID="Label18" runat="server" Text="ClassRoom"></asp:Label>
                                                &nbsp;
                                            <asp:CheckBox ID="cbcourse_type_elearning" runat="server" Text=""
                                                Checked='<%# getDataCheckbox((string)Eval("course_type_elearning")) %>' />
                                                <asp:Label ID="Label19" runat="server" Text="E-Learning"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:Label ID="Label21" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="ระดับความสำคัญ :" />
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlcourse_priority" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="1" Text="Must - บังคับตามกฎหมาย / ข้อกำหนด / นโยบายและกลยุทธ์บริษัทในแต่ละปี " Selected="True" />
                                                    <asp:ListItem Value="2" Text="Need - ตามความต้องการ และความจำเป็นของแต่ละสายงาน" />
                                                    <asp:ListItem Value="3" Text="Want - ควรจะมีเพื่อสร้างบุคลากร และความจำเป็นในอนาคต" />
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:Label ID="Label28" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="การประเมิน :" />
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlcourse_assessment" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="1" Text="ประเมิน" Selected="True" />
                                                    <asp:ListItem Value="0" Text="ไม่ประเมิน" />
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:Label ID="Label17" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="สถานะการใช้งาน :" />
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlcourse_status" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="1" Text="ใช้งาน" />
                                                    <asp:ListItem Value="0" Text="ไม่ใช้งาน" />
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <%-- End  --%>
                            </div>
                        </div>
                    </div>

                </InsertItemTemplate>


            </asp:FormView>

            <%-- Edit  --%>
            <asp:FormView ID="fv_Update" runat="server" DefaultMode="Edit" Width="100%" OnDataBound="FvDetail_DataBound">
                <EditItemTemplate>

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-pencil"></i><strong>&nbsp; ข้อมูลหลักสูตร</strong></h3>
                        </div>
                        <div class="panel-body">

                            <div class="form-horizontal" role="form">
                                <asp:Panel ID="Panel5" runat="server" Visible="true">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:Label ID="Label20" class="col-md-2 control-labelnotop text_right" runat="server" Text="รหัสหลักสูตร :" />
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="txtcourse_no" runat="server" CssClass="form-control" Enabled="false"
                                                        Text='<%# Eval("course_no") %>' />
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:Label ID="Label12" class="col-md-2 control-labelnotop text_right" runat="server" Text="ชื่อหลักสูตร :" />
                                            <div class="col-md-4">
                                                <asp:TextBox ID="txtcourse_name" runat="server"
                                                    CssClass="form-control" MaxLength="250"
                                                    Text='<%# Eval("course_name") %>' />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                                                    ValidationGroup="btnSaveUpdate" runat="server"
                                                    Display="Dynamic"
                                                    SetFocusOnError="true"
                                                    ControlToValidate="txtcourse_name"
                                                    Font-Size="1em" ForeColor="Red"
                                                    CssClass="pull-left"
                                                    ErrorMessage="กรุณากรอกชื่อหลักสูตร" />
                                            </div>

                                            <div class="col-md-6">
                                                <asp:Label ID="Label1" CssClass="col-md-4 control-labelnotop text_right" runat="server" Text="กลุ่มวิชา : " />
                                                <div class="col-md-6">
                                                    <asp:DropDownList ID="ddlm0_training_group_idx_ref" runat="server"
                                                        AutoPostBack="true"
                                                        OnSelectedIndexChanged="FvDetail_DataBound"
                                                        CssClass="form-control">
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                        ValidationGroup="btnSaveUpdate" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="ddlm0_training_group_idx_ref"
                                                        Font-Size="1em" ForeColor="Red"
                                                        CssClass="pull-left"
                                                        InitialValue="0"
                                                        ErrorMessage="กรุณากรอกกลุ่มวิชา" />
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">

                                            <asp:Label ID="Label2" class="col-md-2 control-labelnotop text_right" runat="server" Text="รายละเอียด :" />
                                            <div class="col-md-4">
                                                <asp:TextBox ID="txtcourse_remark" runat="server" CssClass="form-control"
                                                    TextMode="MultiLine" MaxLength="250"
                                                    Rows="9"
                                                    Text='<%# Eval("course_remark") %>' />
                                            </div>
                                            <div class="col-md-6">

                                                <div class="row">
                                                    <asp:Label ID="Label6" CssClass="col-md-4 control-labelnotop text_right" runat="server" Text="สาขาวิชา :" />
                                                    <div class="col-md-6">
                                                        <asp:DropDownList ID="ddm0_training_branch_idx" runat="server"
                                                            CssClass="form-control"
                                                            AutoPostBack="true"
                                                            OnSelectedIndexChanged="FvDetail_DataBound">
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14"
                                                            ValidationGroup="btnSaveUpdate" runat="server"
                                                            Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="ddm0_training_branch_idx"
                                                            Font-Size="1em" ForeColor="Red"
                                                            CssClass="pull-left"
                                                            InitialValue="0"
                                                            ErrorMessage="กรุณากรอกสาขาวิชา" />
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="form-group" style="padding: 0px"></div>
                                                </div>

                                                <div class="row">
                                                    <asp:Label ID="Label3" CssClass="col-md-4 control-labelnotop text_right" runat="server" Text="วันที่เปิดหลักสูตร :" />
                                                    <div class="col-md-6">
                                                        <div class='input-group date'>
                                                            <asp:TextBox ID="txtcourse_date" runat="server"
                                                                CssClass="form-control filter-order-from"
                                                                Text='<%# Eval("course_date") %>' />
                                                            <span class="input-group-addon show-order-sale-log-from-onclick">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3"
                                                            ValidationGroup="btnSaveUpdate" runat="server"
                                                            Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="txtcourse_date"
                                                            Font-Size="1em" ForeColor="Red"
                                                            CssClass="pull-left"
                                                            ErrorMessage="กรุณากรอกวันที่เปิดหลักสูตร" />
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group" style="padding: 0px"></div>
                                                </div>

                                                <div class="row">
                                                    <asp:Label ID="Label8" CssClass="col-md-4 control-labelnotop text_right" runat="server" Text="ระดับความยากของหลักสูตร :" />
                                                    <div class="col-md-6">
                                                        <asp:DropDownList ID="ddllevel_code" runat="server"
                                                            CssClass="form-control">
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4"
                                                            ValidationGroup="btnSaveUpdate" runat="server"
                                                            Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="ddllevel_code"
                                                            Font-Size="1em" ForeColor="Red"
                                                            CssClass="pull-left"
                                                            InitialValue="0"
                                                            ErrorMessage="กรุณากรอกระดับความยากของหลักสูตร" />
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group" style="padding: 0px"></div>
                                                </div>

                                                <div class="row">
                                                    <asp:Label ID="Label9" CssClass="col-md-4 control-labelnotop text_right" runat="server" Text="คะแนนเต็ม :" />
                                                    <div class="col-md-6">
                                                        <asp:TextBox ID="txtcourse_score" runat="server"
                                                            CssClass="form-control"
                                                            TextMode="Number"
                                                            Text='<%# Eval("course_score") %>'>
                                                        </asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5"
                                                            ValidationGroup="btnSaveUpdate" runat="server"
                                                            Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="txtcourse_score"
                                                            Font-Size="1em" ForeColor="Red"
                                                            CssClass="pull-left"
                                                            ErrorMessage="กรุณากรอกคะแนนเต็ม" />
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group" style="padding: 0px"></div>
                                                </div>
                                                <div class="row">
                                                    <asp:Label ID="Label11" CssClass="col-md-4 control-labelnotop text_right" runat="server" Text="คะแนนผ่าน % :" />
                                                    <div class="col-md-6">
                                                        <asp:TextBox ID="txtscore_through_per" runat="server"
                                                            CssClass="form-control"
                                                            TextMode="Number"
                                                            Text='<%# Eval("score_through_per") %>'>
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group" style="padding: 0px"></div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:Label ID="Label4" class="col-md-2 control-labelnotop text_right" runat="server" Text="ต้องผ่านการเรียนหลักสูตร:" />
                                            <div class="col-md-4">
                                                <div>

                                                    <div class="input-group col-md-12 pull-left">
                                                        <asp:TextBox ID="txtsearch_GvCourse" runat="server"
                                                            placeholder="ค้นหา..." CssClass="form-control" />
                                                        <div class="input-group-btn">
                                                            <asp:LinkButton ID="btnsearch_GvCourse" CssClass="btn btn-primary" runat="server"
                                                                data-original-title="ค้นหา..." data-toggle="tooltip"
                                                                OnCommand="btnCommand" CommandArgument="1"
                                                                CommandName="btnsearch_GvCourse"><i class="glyphicon glyphicon-search"></i></asp:LinkButton>
                                                        </div>
                                                    </div>

                                                </div>
                                                <br>
                                                <div class="col-md-12"></div>
                                                <br />
                                                <div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-body" style="height: 200px; overflow-x: scroll; overflow-y: scroll;">
                                                            <div>

                                                                <asp:GridView ID="GvCourse"
                                                                    runat="server"
                                                                    AutoGenerateColumns="false"
                                                                    CssClass="col-md-12"
                                                                    HeaderStyle-CssClass="default"
                                                                    ShowFooter="false"
                                                                    AllowPaging="false"
                                                                    ShowHeader="false"
                                                                    GridLines="None">
                                                                    <EmptyDataTemplate>
                                                                        <div style="text-align: center">ไม่พบข้อมูล</div>
                                                                    </EmptyDataTemplate>
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                                                            HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
                                                                            <ItemTemplate>
                                                                                <small>
                                                                                    <asp:CheckBox ID="GvCourse_cb_zId" runat="server"
                                                                                        Checked='<%# getValue((string)Eval("id")) %>' />
                                                                                </small>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="ตำแหน่ง" ItemStyle-HorizontalAlign="Left"
                                                                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                            <ItemTemplate>
                                                                                <small>
                                                                                    <asp:Label CssClass="control-label" ID="GvCourse_zName" runat="server" Text='<%# Eval("zName") %>' />
                                                                                    <asp:Label ID="GvCourse_zId" runat="server" Text='<%# Eval("zId") %>' Visible="false" />
                                                                                </small>
                                                                            </ItemTemplate>
                                                                            <EditItemTemplate />
                                                                            <FooterTemplate />
                                                                        </asp:TemplateField>

                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>


                                            <div class="col-md-6">

                                                <asp:Label ID="Label5" CssClass="col-md-4 control-labelnotop text_right" runat="server" Text="ระดับพนักงาน :" />
                                                <div class="col-md-6">
                                                    <div class="panel panel-default">
                                                        <div class="panel-body" style="height: 200px; overflow-x: scroll; overflow-y: scroll;">
                                                            <asp:GridView ID="GvM0_EmpTarget"
                                                                runat="server"
                                                                AutoGenerateColumns="false"
                                                                CssClass="col-md-12"
                                                                HeaderStyle-CssClass="default"
                                                                ShowFooter="false"
                                                                AllowPaging="false"
                                                                ShowHeader="false"
                                                                GridLines="None"
                                                                DataKeyNames="zId">
                                                                <EmptyDataTemplate>
                                                                    <div style="text-align: center">ไม่พบข้อมูล</div>
                                                                </EmptyDataTemplate>
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                                                        HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
                                                                        <ItemTemplate>
                                                                            <small>

                                                                                <asp:CheckBox ID="GvM0_EmpTarget_cb_zId" runat="server"
                                                                                    Checked='<%# getDataCheckbox((string)Eval("id")) %>' />
                                                                            </small>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="ตำแหน่ง" ItemStyle-HorizontalAlign="Left"
                                                                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                        <ItemTemplate>
                                                                            <small>
                                                                                <asp:Label CssClass="control-label" ID="GvM0_EmpTarget_zName" runat="server" Text='<%# Eval("zName") %>' />
                                                                                <asp:Label ID="GvM0_EmpTarget_zId" runat="server" Text='<%# Eval("zId") %>' Visible="false" />
                                                                            </small>
                                                                        </ItemTemplate>
                                                                        <EditItemTemplate />
                                                                        <FooterTemplate />
                                                                    </asp:TemplateField>

                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>


                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:Label ID="Label16" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="ประเภท :" />
                                            <div class="col-md-4">
                                                <asp:CheckBox ID="cbcourse_type_etraining" runat="server" Text="" /><%----%>
                                                <asp:Label ID="Label18" runat="server" Text="ClassRoom"></asp:Label>
                                                &nbsp;
                                            <asp:CheckBox ID="cbcourse_type_elearning" runat="server" Text="" />
                                                <asp:Label ID="Label19" runat="server" Text="E-Learning"></asp:Label><%--Checked='<%# getDataCheckbox((string)Eval("course_type_elearning")) %>' --%>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:Label ID="Label21" CssClass="col-md-2 control-labelnotop text_right"
                                                runat="server" Text="ระดับความสำคัญ :" />
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlcourse_priority"
                                                    runat="server" CssClass="form-control"
                                                    SelectedValue='<%# Eval("course_priority") %>'>
                                                    <asp:ListItem Value="1" Text="Must - บังคับตามกฎหมาย / ข้อกำหนด / นโยบายและกลยุทธ์บริษัทในแต่ละปี " />
                                                    <asp:ListItem Value="2" Text="Need - ตามความต้องการ และความจำเป็นของแต่ละสายงาน" />
                                                    <asp:ListItem Value="3" Text="Want - ควรจะมีเพื่อสร้างบุคลากร และความจำเป็นในอนาคต" />
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:Label ID="Label28" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="การประเมิน :" />
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlcourse_assessment" runat="server" CssClass="form-control"
                                                    SelectedValue='<%# Eval("course_assessment") %>'>
                                                    <asp:ListItem Value="1" Text="ประเมิน" />
                                                    <asp:ListItem Value="0" Text="ไม่ประเมิน" />
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:Label ID="Label17" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="สถานะการใช้งาน :" />
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlcourse_status" runat="server"
                                                    CssClass="form-control"
                                                    SelectedValue='<%# Eval("course_status") %>'>
                                                    <asp:ListItem Value="1" Text="ใช้งาน" />
                                                    <asp:ListItem Value="0" Text="ไม่ใช้งาน" />
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <%-- End  --%>
                            </div>
                        </div>
                    </div>

                </EditItemTemplate>


            </asp:FormView>


            <%-- End Select --%>

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="glyphicon glyphicon-pencil"></i><strong>&nbsp; แผนกที่สามารถเข้าอบรมและเรียนรู้หลักสูตร</strong></h3>
                </div>
                <div class="panel-body">
                    <div class="form-horizontal" role="form">

                        <asp:Panel ID="pnlAddDeptTraningList" runat="server">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <asp:Label ID="Label14" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="องค์กร :" />
                                        <div class="col-md-4">
                                            <asp:DropDownList ID="ddlOrg_search" runat="server"
                                                AutoPostBack="true"
                                                OnSelectedIndexChanged="FvDetail_DataBound"
                                                CssClass="form-control" />
                                        </div>
                                        <div class="col-md-6">
                                            <asp:Label ID="Label27" CssClass="col-md-4 control-labelnotop text_right" runat="server" Text="ฝ่าย :" />
                                            <div class="col-md-6">
                                                <asp:DropDownList ID="ddlDept_search" runat="server"
                                                    CssClass="form-control"
                                                    AutoPostBack="true"
                                                    OnSelectedIndexChanged="FvDetail_DataBound">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <asp:Label ID="Label7" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="แผนก :" />
                                        <div class="col-md-4">
                                            <asp:DropDownList ID="ddlSec_search" runat="server"
                                                CssClass="form-control"
                                                AutoPostBack="true"
                                                OnSelectedIndexChanged="FvDetail_DataBound">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-md-6">
                                            <asp:Label ID="Label30" CssClass="col-md-4 control-labelnotop text_right" runat="server" Text=" " />
                                            <div class="col-md-6">
                                                <asp:LinkButton CssClass="btn btn-primary" runat="server"
                                                    ID="btnAdd"
                                                    OnCommand="btnCommand" CommandName="btnAdd"
                                                    Text="<i class='fa fa-plus fa-lg'></i> เพิ่ม"
                                                    data-toggle="tooltip" title="เพิ่ม" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12">

                                        <asp:Label ID="Label5" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="ระดับพนักงาน :" />
                                        <div class="col-md-4">
                                            <div class="panel panel-default">
                                                <div class="panel-body" style="height: 200px; overflow-x: scroll; overflow-y: scroll;">
                                                    <asp:GridView ID="GvM0_EmpTarget_search"
                                                        runat="server"
                                                        AutoGenerateColumns="false"
                                                        CssClass="col-md-12"
                                                        HeaderStyle-CssClass="default"
                                                        ShowFooter="false"
                                                        AllowPaging="false"
                                                        ShowHeader="false"
                                                        GridLines="None"
                                                        DataKeyNames="zId">
                                                        <EmptyDataTemplate>
                                                            <div style="text-align: center">ไม่พบข้อมูล</div>
                                                        </EmptyDataTemplate>
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                                                HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <small>

                                                                        <asp:CheckBox ID="GvM0_EmpTarget_cb_zId" runat="server"
                                                                            Checked='<%# getDataCheckbox((string)Eval("id")) %>' />
                                                                    </small>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="ตำแหน่ง" ItemStyle-HorizontalAlign="Left"
                                                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                <ItemTemplate>
                                                                    <small>
                                                                        <asp:Label CssClass="control-label" ID="GvM0_EmpTarget_zName" runat="server" Text='<%# Eval("zName") %>' />
                                                                        <asp:Label ID="GvM0_EmpTarget_zId" runat="server" Text='<%# Eval("zId") %>' Visible="false" />
                                                                    </small>
                                                                </ItemTemplate>
                                                                <EditItemTemplate />
                                                                <FooterTemplate />
                                                            </asp:TemplateField>

                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </asp:Panel>

                        <div class="row">

                            <div class="wrapword_p1">

                                <asp:GridView ID="GvDeptTraningList"
                                    runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="table table-striped table-bordered table-responsiv word-wrap"
                                    HeaderStyle-CssClass="info"
                                    AllowPaging="false"
                                    OnRowCommand="onRowCommand"
                                    OnRowDataBound="onRowDataBound"
                                    OnPageIndexChanging="gvPageIndexChanging"
                                    ShowFooter="false"
                                    HeaderStyle-Wrap="true"
                                    RowStyle-Wrap="true"
                                    Width="100%">
                                    <EmptyDataTemplate>
                                        <div style="text-align: center">ไม่พบข้อมูล</div>
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                                            <ItemStyle Wrap="true" />
                                            <ItemTemplate>
                                                <%# (Container.DataItemIndex + 1) %>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="องค์กร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="25%">
                                            <ItemTemplate>
                                                <div class="word-wrap"></div>
                                                <%# Eval("org_name_th") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ฝ่าย" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="25%">
                                            <ItemStyle Wrap="true" />
                                            <ItemTemplate>
                                                <div class="word-wrap"></div>
                                                <%# Eval("dept_name_th") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="แผนก" ItemStyle-HorizontalAlign="Left"
                                            HeaderStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="25%">
                                            <ItemTemplate>
                                                <div class="word-wrap">
                                                    <%# Eval("sec_name_th") %>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="กลุ่มงาน" ItemStyle-HorizontalAlign="Left"
                                            HeaderStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%">
                                            <ItemTemplate>
                                                <div class="word-wrap">
                                                    <%# Eval("target_name") %>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField ItemStyle-HorizontalAlign="center"
                                            HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btnRemove_GvDeptTraningList" runat="server"
                                                    CssClass="btn btn-danger btn-xs"
                                                    CommandName="btnRemove_GvDeptTraningList">
                                                               <i class="fa fa-times"></i>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>



                            </div>

                        </div>

                    </div>
                </div>
            </div>
            <asp:Panel ID="pnlSave" runat="server">

                <div class="row">

                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="col-md-2"></div>
                            <div class="col-md-7">
                                <asp:LinkButton ID="btnSaveUpdate" CssClass="btn btn-success" runat="server"
                                    CommandName="btnSaveUpdate" OnCommand="btnCommand"
                                    Text="<i class='fa fa-save fa-lg'></i> บันทึกการเปลี่ยนแปลง"
                                    data-toggle="tooltip" title="บันทึกการเปลี่ยนแปลง"
                                    ValidationGroup="btnSaveUpdate" />
                                <asp:LinkButton CssClass="btn btn-success" runat="server"
                                    CommandName="btnSaveInsert" OnCommand="btnCommand"
                                    Text="<i class='fa fa-save fa-lg'></i> บันทึก"
                                    data-toggle="tooltip" title="บันทึก"
                                    ID="btnSaveInsert"
                                    ValidationGroup="btnSaveInsert" />
                                <asp:LinkButton CssClass="btn btn-warning"
                                    ID="btnClear" Visible="false"
                                    OnCommand="btnCommand" CommandName="btnInsert"
                                    data-toggle="tooltip" title="ยกเลิก" runat="server"
                                    Text="<i class='fa fa-close fa-lg'></i> ยกเลิก" />
                                <asp:LinkButton CssClass="btn btn-danger"
                                    data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                                    Text="<i class='fa fa-angle-left fa-lg'></i> ย้อนกลับ"
                                    ID="btnCancel"
                                    CommandName="btnCancel" OnCommand="btnCommand" />
                            </div>
                        </div>
                    </div>
                </div>

            </asp:Panel>
            <br />

            <%--                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnSaveUpdate" />
                    <asp:PostBackTrigger ControlID="btnSaveInsert" />
                    <asp:PostBackTrigger ControlID="btnClear" />
                    <asp:PostBackTrigger ControlID="btnCancel" />
                    <asp:PostBackTrigger ControlID="btnAdd" />
                    <asp:PostBackTrigger ControlID="btnAdd" />
                   
                </Triggers>
            </asp:UpdatePanel>--%>
        </asp:View>


        <asp:View ID="View_testform" runat="server">

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="glyphicon glyphicon-pencil"></i><strong>&nbsp;ข้อมูลแบบทดสอบ</strong></h3>
                </div>
                <div class="panel-body">
                    <asp:Panel ID="pnlAddtest" runat="server">

                        <div class="form-horizontal" role="form">
                            <%--  <asp:UpdatePanel ID="UpdatePnlImages_proposition" UpdateMode="Conditional" runat="server">
                                <ContentTemplate>--%>
                            <%--<asp:FormView ID="fvDetail" runat="server" DefaultMode="Insert" Width="100%">
                                        <InsertItemTemplate>--%>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <asp:Label ID="Label13" class="col-md-2 control-labelnotop text_right" runat="server" Text="ลำดับ  :" />
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txtcourse_item" runat="server" CssClass="form-control"
                                                TextMode="Number" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator11"
                                                ValidationGroup="btntestAdd" runat="server"
                                                Display="Dynamic"
                                                SetFocusOnError="true"
                                                ControlToValidate="txtcourse_item"
                                                Font-Size="1em" ForeColor="Red"
                                                CssClass="pull-left"
                                                ErrorMessage="กรุณากรอกลำดับ"></asp:RequiredFieldValidator>
                                        </div>

                                        <div class="col-md-6">
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <asp:Label ID="Label15" class="col-md-2 control-labelnotop text_right" runat="server" Text="โจทย์  :" />
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txtcourse_proposition" runat="server"
                                                CssClass="form-control"
                                                MaxLength="250" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6"
                                                ValidationGroup="btntestAdd" runat="server"
                                                Display="Dynamic"
                                                SetFocusOnError="true"
                                                ControlToValidate="txtcourse_proposition"
                                                Font-Size="1em" ForeColor="Red"
                                                CssClass="pull-left"
                                                ErrorMessage="กรุณากรอกโจทย์"></asp:RequiredFieldValidator>
                                        </div>

                                        <div class="col-md-3">

                                            <asp:FileUpload ID="UploadImages_proposition"
                                                ViewStateMode="Enabled" Font-Size="small"
                                                AutoPostBack="true"
                                                Visible="false"
                                                runat="server" CssClass="control-label multi max-1" accept="gif|jpg|png" />

                                            <input id="UploadImages_proposition_html" type="file" onchange="imageproposUpload(this)"
                                                accept="gif|jpg|png" />

                                            <%--<p class="help-block"><font color="red">**เฉพาะนามสกุล gif , jpg , png</font></p>--%>
                                        </div>
                                        <div class="col-md-1">
                                            <img alt="" id="img_1" class="pull-left"
                                                style="height: 50px; width: 50px;" />
                                        </div>
                                        <div class="col-md-1">
                                            <input type="button" value="ลบ"
                                                class="btn btn-danger pull-left"
                                                onclick="imageUpload_del(1)" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <asp:Label ID="Label22" class="col-md-2 control-labelnotop text_right" runat="server" Text=" ก :" />
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txtcourse_propos_a" runat="server" CssClass="form-control"
                                                MaxLength="250" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7"
                                                ValidationGroup="btntestAdd" runat="server"
                                                Display="Dynamic"
                                                SetFocusOnError="true"
                                                ControlToValidate="txtcourse_propos_a"
                                                Font-Size="1em" ForeColor="Red"
                                                CssClass="pull-left"
                                                ErrorMessage="กรุณากรอกคำตอบข้อ ก"></asp:RequiredFieldValidator>
                                        </div>

                                        <div class="col-md-3">

                                            <asp:FileUpload ID="UploadImages_a"
                                                ViewStateMode="Enabled" Font-Size="small"
                                                runat="server" CssClass="control-label multi max-1"
                                                accept="gif|jpg|png"
                                                Visible="false" />

                                            <input id="UploadImages_a_html" type="file" onchange="image_aUpload(this)"
                                                accept="gif|jpg|png" />
                                            <%--<br />
                                            <img alt="" id="img_a"
                                                style="height: 100px; width: 100px;" />--%>

                                            <%--<p class="help-block"><font color="red">**เฉพาะนามสกุล gif , jpg , png</font></p>--%>
                                        </div>
                                        <div class="col-md-1">
                                            <img alt="" id="img_2" class="pull-left"
                                                style="height: 50px; width: 50px;" />
                                        </div>
                                        <div class="col-md-1">
                                            <input type="button" value="ลบ"
                                                class="btn btn-danger pull-left"
                                                onclick="imageUpload_del(2)" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <asp:Label ID="Label23" class="col-md-2 control-labelnotop text_right" runat="server" Text=" ข :" />
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txtcourse_propos_b" runat="server" CssClass="form-control"
                                                MaxLength="250" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8"
                                                ValidationGroup="btntestAdd" runat="server"
                                                Display="Dynamic"
                                                SetFocusOnError="true"
                                                ControlToValidate="txtcourse_propos_b"
                                                Font-Size="1em" ForeColor="Red"
                                                CssClass="pull-left"
                                                ErrorMessage="กรุณากรอกคำตอบข้อ ข"></asp:RequiredFieldValidator>
                                        </div>

                                        <div class="col-md-3">

                                            <asp:FileUpload ID="UploadImages_b"
                                                ViewStateMode="Enabled" Font-Size="small"
                                                runat="server" CssClass="control-label multi max-1"
                                                accept="gif|jpg|png"
                                                Visible="false" />

                                            <input id="UploadImages_b_html" type="file" onchange="image_bUpload(this)"
                                                accept="gif|jpg|png" />


                                            <%--<p class="help-block"><font color="red">**เฉพาะนามสกุล gif , jpg , png</font></p>--%>
                                        </div>
                                        <div class="col-md-1">
                                            <img alt="" id="img_3" class="pull-left"
                                                style="height: 50px; width: 50px;" />
                                        </div>
                                        <div class="col-md-1">
                                            <input type="button" value="ลบ"
                                                class="btn btn-danger pull-left"
                                                onclick="imageUpload_del(3)" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <asp:Label ID="Label24" class="col-md-2 control-labelnotop text_right" runat="server" Text=" ค :" />
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txtcourse_propos_c" runat="server" CssClass="form-control"
                                                MaxLength="250" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9"
                                                ValidationGroup="btntestAdd" runat="server"
                                                Display="Dynamic"
                                                SetFocusOnError="true"
                                                ControlToValidate="txtcourse_propos_c"
                                                Font-Size="1em" ForeColor="Red"
                                                CssClass="pull-left"
                                                ErrorMessage="กรุณากรอกคำตอบข้อ ค"></asp:RequiredFieldValidator>
                                        </div>

                                        <div class="col-md-3">

                                            <asp:FileUpload ID="UploadImages_c"
                                                ViewStateMode="Enabled" Font-Size="small"
                                                runat="server" CssClass="control-label multi max-1"
                                                accept="gif|jpg|png"
                                                Visible="false" />

                                            <input id="UploadImages_c_html" type="file" onchange="image_cUpload(this)"
                                                accept="gif|jpg|png" />
                                            <%--<br />
                                            <img alt="" id="img_c"
                                                style="height: 100px; width: 100px;" />--%>

                                            <%--<p class="help-block"><font color="red">**เฉพาะนามสกุล gif , jpg , png</font></p>--%>
                                        </div>
                                        <div class="col-md-1">
                                            <img alt="" id="img_4" class="pull-left"
                                                style="height: 50px; width: 50px;" />
                                        </div>
                                        <div class="col-md-1">
                                            <input type="button" value="ลบ"
                                                class="btn btn-danger pull-left"
                                                onclick="imageUpload_del(4)" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <asp:Label ID="Label25" class="col-md-2 control-labelnotop text_right" runat="server" Text=" ง :" />
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txtcourse_propos_d" runat="server" CssClass="form-control"
                                                MaxLength="250" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10"
                                                ValidationGroup="btntestAdd" runat="server"
                                                Display="Dynamic"
                                                SetFocusOnError="true"
                                                ControlToValidate="txtcourse_propos_d"
                                                Font-Size="1em" ForeColor="Red"
                                                CssClass="pull-left"
                                                ErrorMessage="กรุณากรอกคำตอบข้อ ง"></asp:RequiredFieldValidator>
                                        </div>

                                        <div class="col-md-3">

                                            <asp:FileUpload ID="UploadImages_d"
                                                ViewStateMode="Enabled" Font-Size="small"
                                                runat="server" CssClass="control-label multi max-1"
                                                accept="gif|jpg|png"
                                                Visible="false" />

                                            <input id="UploadImages_d_html" type="file" onchange="image_dUpload(this)"
                                                accept="gif|jpg|png" />
                                            <%-- <br />
                                            <img alt="" id="img_d" 
                                                style="height: 100px; width: 100px;" />--%>

                                            <%--<p class="help-block"><font color="red">**เฉพาะนามสกุล gif , jpg , png</font></p>--%>
                                        </div>
                                        <div class="col-md-1">
                                            <img alt="" id="img_5" class="pull-left"
                                                style="height: 50px; width: 50px;" />
                                        </div>
                                        <div class="col-md-1">
                                            <input type="button" value="ลบ"
                                                class="btn btn-danger pull-left"
                                                onclick="imageUpload_del(5)" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <asp:Label ID="Label26" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="เฉลย :" />
                                        <div class="col-md-4">
                                            <asp:DropDownList ID="ddlcourse_propos_answer" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="1" Text="ก" Selected="True" />
                                                <asp:ListItem Value="2" Text="ข" />
                                                <asp:ListItem Value="3" Text="ค" />
                                                <asp:ListItem Value="4" Text="ง" />
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <%--</InsertItemTemplate>
                                    </asp:FormView>--%>


                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-4">

                                            <asp:LinkButton CssClass="btn btn-primary" runat="server"
                                                ID="btntestAdd"
                                                OnCommand="btnCommand" CommandName="btntestAdd"
                                                ValidationGroup="btntestAdd"
                                                Text="<i class='fa fa-plus fa-lg'></i> Add"
                                                data-toggle="tooltip" title="Add" />


                                            <asp:LinkButton CssClass="btn btn-warning"
                                                ID="btntestClear"
                                                OnCommand="btnCommand" CommandName="btntestClear"
                                                data-toggle="tooltip" title="Clear" runat="server"
                                                Text="<i class='fa fa-remove fa-lg'></i> Clear" />
                                        </div>

                                        <div class="col-md-6">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <%--  </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btntestAdd" />
                                </Triggers>
                            </asp:UpdatePanel>--%>
                        </div>

                    </asp:Panel>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-12">
                                <asp:GridView ID="GvTest"
                                    runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="table table-striped table-bordered table-responsive col-md-12"
                                    HeaderStyle-CssClass="info"
                                    AllowPaging="false"
                                    OnRowCommand="onRowCommand"
                                    OnRowDataBound="onRowDataBound"
                                    OnPageIndexChanging="gvPageIndexChanging"
                                    ShowFooter="false">
                                    <EmptyDataTemplate>
                                        <div style="text-align: center">ไม่พบข้อมูล</div>
                                    </EmptyDataTemplate>
                                    <Columns>

                                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-Width="5%">
                                            <ItemTemplate>
                                                <div class="word-wrap">
                                                    <small>
                                                        <asp:Label ID="lbcourse_item" Visible="false" runat="server" Text='<%# Eval("course_item") %>' />
                                                        <%# Eval("course_item") %>
                                                        <asp:Label ID="lbdocno_bin_GvTest" runat="server"
                                                            Text='<%# Eval("docno_bin") %>'
                                                            Visible="false"></asp:Label>

                                                    </small>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="โจทย์" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="25%">
                                            <ItemTemplate>
                                                <div class="word-wrap">
                                                    <%# Eval("course_proposition") %>
                                                    <asp:Label ID="lbcourse_proposition_img_GvTest" runat="server"
                                                        Text='<%# Eval("course_proposition_img") %>'
                                                        Visible="false"></asp:Label>
                                                    <br />
                                                    <asp:Label ID="btncourse_proposition_img_GvTest"
                                                        class="btn btn-primary" runat="server"
                                                        Text="มีรูป" />
                                                    <img alt="" id="img_3" class="pull-left"
                                                        src='<%# getImgUrl((string)Eval("docno_bin"),
                                                                           (string)Eval("course_item"),
                                                                           (string)Eval("course_proposition_img")) %>'
                                                        style="height: 40px; width: 40px;" />
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ก" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                                            <ItemTemplate>
                                                <div class="word-wrap">
                                                    <%# Eval("course_propos_a") %>
                                                    <asp:Label ID="lbcourse_propos_a_img_GvTest" runat="server"
                                                        Text='<%# Eval("course_propos_a_img") %>'
                                                        Visible="false"></asp:Label>
                                                    <br />
                                                    <asp:Label ID="btncourse_propos_a_img_GvTest"
                                                        class="btn btn-primary" runat="server"
                                                        Text="มีรูป" />
                                                    <img alt="" id="img_3" class="pull-left"
                                                        src='<%# getImgUrl((string)Eval("docno_bin"),
                                                                           (string)Eval("course_item"),
                                                                           (string)Eval("course_propos_a_img")) %>'
                                                        style="height: 40px; width: 40px;" />
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="ข" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                                            <ItemTemplate>
                                                <div class="word-wrap">
                                                    <%# Eval("course_propos_b") %>
                                                    <asp:Label ID="lbcourse_propos_b_img_GvTest" runat="server"
                                                        Text='<%# Eval("course_propos_b_img") %>'
                                                        Visible="false"></asp:Label>
                                                    <br />
                                                    <asp:Label ID="btncourse_propos_b_img_GvTest"
                                                        class="btn btn-primary" runat="server"
                                                        Text="มีรูป" />
                                                    <img alt="" id="img_3" class="pull-left"
                                                        src='<%# getImgUrl((string)Eval("docno_bin"),
                                                                           (string)Eval("course_item"),
                                                                           (string)Eval("course_propos_b_img")) %>'
                                                        style="height: 40px; width: 40px;" />
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="ค" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                                            <ItemTemplate>
                                                <div class="word-wrap">
                                                    <%# Eval("course_propos_c") %>
                                                    <asp:Label ID="lbcourse_propos_c_img_GvTest" runat="server"
                                                        Text='<%# Eval("course_propos_c_img") %>'
                                                        Visible="false"></asp:Label>
                                                    <br />
                                                    <asp:Label ID="btncourse_propos_c_img_GvTest"
                                                        class="btn btn-primary" runat="server"
                                                        Text="มีรูป" />
                                                    <img alt="" id="img_3" class="pull-left"
                                                        src='<%# getImgUrl((string)Eval("docno_bin"),
                                                                           (string)Eval("course_item"),
                                                                           (string)Eval("course_propos_c_img")) %>'
                                                        style="height: 40px; width: 40px;" />
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="ง" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                                            <ItemTemplate>
                                                <div class="word-wrap">
                                                    <%# Eval("course_propos_d") %>
                                                    <asp:Label ID="lbcourse_propos_d_img_GvTest" runat="server"
                                                        Text='<%# Eval("course_propos_d_img") %>'
                                                        Visible="false"></asp:Label>
                                                    <br />
                                                    <asp:Label ID="btncourse_propos_d_img_GvTest"
                                                        class="btn btn-primary" runat="server"
                                                        Text="มีรูป" />
                                                    <img alt="" id="img_3" class="pull-left"
                                                        src='<%# getImgUrl((string)Eval("docno_bin"),
                                                                           (string)Eval("course_item"),
                                                                           (string)Eval("course_propos_d_img")) %>'
                                                        style="height: 40px; width: 40px;" />
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="เฉลย" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                                            <ItemTemplate>
                                                <div class="word-wrap">
                                                    <%# gettest((string)Eval("course_propos_answer")) %>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="รูป" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%"
                                            Visible="false">
                                            <ItemTemplate>
                                                <table class="table table-striped f-s-12 table-empshift-responsive">

                                                    <tr>
                                                        <th>ชื่อ</th>
                                                        <th>ไฟล์</th>
                                                    </tr>
                                                    <tr>
                                                        <td>โจทย์</td>
                                                        <td></td>
                                                    </tr>
                                                    <td>ก</td>
                                                    <td></td>
                                                    </tr>
                                                        <td>ข</td>
                                                    <td></td>
                                                    </tr>
                                                        <td>ค</td>
                                                    <td></td>
                                                    </tr>
                                                        <td>ง</td>
                                                    <td></td>
                                                    </tr>
                                                </table>


                                            </ItemTemplate>
                                        </asp:TemplateField>



                                        <asp:TemplateField ItemStyle-HorizontalAlign="center"
                                            HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btnRemove_GvTest" runat="server"
                                                    CssClass="btn btn-danger btn-xs"
                                                    CommandName="btnRemove_GvTest">
                                                               <i class="fa fa-times"></i>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>



                </div>
            </div>



        </asp:View>


        <asp:View ID="View_evaluationform" runat="server">

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="glyphicon glyphicon-pencil"></i><strong>&nbsp; ข้อมูลแบบประเมิน</strong></h3>
                </div>
                <div class="panel-body">
                    <div class="form-horizontal" role="form">

                        <asp:Panel ID="pnlAddEvaluation" runat="server" Visible="true">

                            <div class="row">

                                <div class="form-group">
                                    <div class="col-md-12">
                                        <asp:Label ID="Label10" class="col-md-1 control-labelnotop text_right" runat="server" Text="กลุ่ม" />
                                        <div class="col-md-3">
                                            <asp:DropDownList ID="ddlm0_evaluation_group_idx_refsel" runat="server"
                                                CssClass="form-control">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator12"
                                                ValidationGroup="btnAddevaluationform" runat="server"
                                                Display="Dynamic"
                                                SetFocusOnError="true"
                                                InitialValue="0"
                                                ControlToValidate="ddlm0_evaluation_group_idx_refsel"
                                                Font-Size="1em" ForeColor="Red"
                                                CssClass="pull-left"
                                                ErrorMessage="กรุณาเลือกกลุ่ม"></asp:RequiredFieldValidator>
                                        </div>


                                        <asp:Label ID="Label11" CssClass="col-md-1 control-labelnotop text_right" runat="server" Text="หัวข้อ" />
                                        <div class="col-md-5">
                                            <asp:TextBox ID="txtevaluation_f_name_sel" runat="server"
                                                CssClass="form-control">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator13"
                                                ValidationGroup="btnAddevaluationform" runat="server"
                                                Display="Dynamic"
                                                SetFocusOnError="true"
                                                ControlToValidate="txtevaluation_f_name_sel"
                                                Font-Size="1em" ForeColor="Red"
                                                CssClass="pull-left"
                                                ErrorMessage="กรุณากรอกหัวข้อ"></asp:RequiredFieldValidator>
                                        </div>

                                        <div class="col-md-2">
                                            <asp:LinkButton CssClass="btn btn-primary" runat="server"
                                                ID="btnVideoAdd"
                                                ValidationGroup="btnAddevaluationform"
                                                OnCommand="btnCommand" CommandName="btnVideoAdd"
                                                Text="<i class='fa fa-plus fa-lg'></i> Add"
                                                data-toggle="tooltip" title="Add" />
                                        </div>
                                    </div>
                                </div>


                            </div>

                        </asp:Panel>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <asp:GridView ID="GvEvaluation"
                                        runat="server"
                                        AutoGenerateColumns="false"
                                        CssClass="table table-striped table-bordered table-responsive col-md-12"
                                        HeaderStyle-CssClass="info"
                                        AllowPaging="false"
                                        OnRowCommand="onRowCommand"
                                        OnRowDataBound="onRowDataBound"
                                        OnPageIndexChanging="gvPageIndexChanging"
                                        ShowFooter="false">
                                        <EmptyDataTemplate>
                                            <div style="text-align: center">ไม่พบข้อมูล</div>
                                        </EmptyDataTemplate>
                                        <Columns>
                                            <asp:TemplateField HeaderText="ลำดับ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbevaluation_group_idx_GvEvalu" runat="server" Text='<%# Eval("evaluation_group_idx") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lbevaluation_group_name_GvEvalu" runat="server" Text='<%# Eval("evaluation_group_name") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lbevaluation_f_idx_GvEvalu" runat="server" Text='<%# Eval("evaluation_f_idx") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lbevaluation_f_name_GvEvalu" runat="server" Text='<%# Eval("evaluation_f_name") %>' Visible="false"></asp:Label>

                                                    <%# (Container.DataItemIndex +1) %>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="เลือก" HeaderStyle-CssClass="text-center"
                                                HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-Width="5%">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:CheckBox ID="GvEvaluation_cb_sel" runat="server"
                                                            Checked='<%# getValue((string)Eval("id")) %>' />
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="กลุ่ม" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                                                <ItemTemplate>
                                                    <%# Eval("evaluation_group_name") %>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="หัวข้อ" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="25%">
                                                <ItemTemplate>
                                                    <%# Eval("evaluation_f_name") %>
                                                </ItemTemplate>
                                            </asp:TemplateField>


                                        </Columns>
                                    </asp:GridView>

                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>

        </asp:View>

        <asp:View ID="View_RptTraining" runat="server">

            <asp:Panel ID="Panel1" runat="server" Visible="true">
                <div class="panel panel-primary m-t-10">
                    <div class="panel-heading f-bold">ค้นหา</div>
                    <div class="panel-body">
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>เดือน</label>
                                <asp:DropDownList ID="ddlMonthSearch_TrnNSur" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>ปี</label>
                                <asp:DropDownList ID="ddlYearSearch_TrnNSur" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>รหัส / ชื่อ</label>
                                <asp:TextBox ID="txtFilterKeyword_TrnNSur" runat="server"
                                    CssClass="form-control"
                                    placeholder="รหัส / ชื่อ..." />
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <label>
                                &nbsp;
                            </label>
                            <div class="form-group">
                                <asp:LinkButton ID="btnsearch_TrnNSur" runat="server" CssClass="btn btn-primary" Text="<i class='fa fa-search'></i> ค้นหา"
                                    OnCommand="btnCommand" CommandName="btnsearch_TrnNSur" />
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <div class="row">

                <div class="pull-right">

                    <asp:LinkButton ID="btnexport1" CssClass="btn btn-primary"
                        runat="server" data-toggle="tooltip" Text="<i class='fa fa-file-excel'></i> Export Excel"
                        CommandName="btnexport1"
                        OnCommand="btnCommand"
                        title="Export"></asp:LinkButton>

                </div>

            </div>
            <br />
            <div class="row">

                <asp:GridView ID="GvListData_TrnNSur"
                    runat="server"
                    AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered table-responsive col-md-12"
                    HeaderStyle-CssClass="info"
                    AllowPaging="true"
                    OnRowCommand="onRowCommand"
                    OnRowDataBound="onRowDataBound"
                    OnPageIndexChanging="gvPageIndexChanging"
                    PageSize="15"
                    DataKeyNames="m0_training_idx_ref"
                    ShowFooter="false">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />

                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่พบข้อมูล</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                            <ItemTemplate>
                                <%# (Container.DataItemIndex + 1) %>
                                <asp:Label ID="lbDataItemIndex" runat="server" Text='<%# (Container.DataItemIndex + 1) %>' Visible="false"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="รหัสหัวข้อ/หลักสูตร" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="training_code" runat="server" Text='<%# Eval("training_code") %>' />
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ชื่อหัวข้อ/หลักสูตร" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="training_name" runat="server" Text='<%# Eval("training_name") %>' />
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="จำนวนครั้งที่ขอ" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="13%">
                            <ItemTemplate>
                                <asp:Label ID="training_qty" runat="server" Text='<%# Eval("training_qty") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>
                <asp:Panel ID="Panel4" runat="server" Visible="false">
                    <asp:GridView ID="GvListData_TrnNSur_Excel"
                        runat="server"
                        AutoGenerateColumns="false"
                        CssClass="table table-striped table-bordered table-responsive col-md-12"
                        HeaderStyle-CssClass="info"
                        AllowPaging="false"
                        OnRowCommand="onRowCommand"
                        OnRowDataBound="onRowDataBound"
                        OnPageIndexChanging="gvPageIndexChanging"
                        ShowFooter="false">
                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่พบข้อมูล</div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                                <ItemTemplate>
                                    <%# (Container.DataItemIndex + 1) %>
                                    <asp:Label ID="lbDataItemIndex" runat="server" Text='<%# (Container.DataItemIndex + 1) %>' Visible="false"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="รหัสหัวข้อ/หลักสูตร" ItemStyle-HorizontalAlign="Left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="training_code" runat="server" Text='<%# Eval("training_code") %>' />
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ชื่อหัวข้อ/หลักสูตร" ItemStyle-HorizontalAlign="Left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="training_name" runat="server" Text='<%# Eval("training_name") %>' />
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวนครั้งที่ขอ" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="13%">
                                <ItemTemplate>
                                    <asp:Label ID="training_qty" runat="server" Text='<%# Eval("training_qty") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>
                </asp:Panel>

            </div>



        </asp:View>

        <asp:View ID="View_RptTrainingByDept" runat="server">

            <asp:Panel ID="Panel2" runat="server" Visible="true">
                <div class="panel panel-primary m-t-10">
                    <div class="panel-heading f-bold">ค้นหา</div>
                    <div class="panel-body">
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>เดือน</label>
                                <asp:DropDownList ID="ddlMonthSearch_TrnNSurDept" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>ปี</label>
                                <asp:DropDownList ID="ddlYearSearch_TrnNSurDept" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>รหัส / ชื่อ</label>
                                <asp:TextBox ID="txtFilterKeyword_TrnNSurDept" runat="server"
                                    CssClass="form-control"
                                    placeholder="รหัส / ชื่อ..." />
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <label>
                                &nbsp;
                            </label>
                            <div class="form-group">
                                <asp:LinkButton ID="btnsearch_TrnNSurDept" runat="server" CssClass="btn btn-primary" Text="<i class='fa fa-search'></i> ค้นหา"
                                    OnCommand="btnCommand" CommandName="btnsearch_TrnNSurDept" />
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <div class="row">

                <div class="pull-right">

                    <asp:LinkButton ID="btnexport2" CssClass="btn btn-primary"
                        runat="server" data-toggle="tooltip" Text="<i class='fa fa-file-excel'></i> Export Excel"
                        CommandName="btnexport2"
                        OnCommand="btnCommand"
                        title="Export"></asp:LinkButton>

                </div>

            </div>
            <br />
            <div class="row">

                <asp:GridView ID="GvListData_TrnNSurDept"
                    runat="server"
                    AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered table-responsive col-md-12"
                    HeaderStyle-CssClass="info"
                    AllowPaging="true"
                    OnRowCommand="onRowCommand"
                    OnRowDataBound="onRowDataBound"
                    OnPageIndexChanging="gvPageIndexChanging"
                    PageSize="15"
                    DataKeyNames="u1_training_req_idx"
                    ShowFooter="false">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />

                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่พบข้อมูล</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                            <ItemTemplate>
                                <asp:Label ID="lbDataItemIndex" runat="server" Text='<%# (Container.DataItemIndex + 1) %>' />
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="องค์กร" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="lborg_name_th" runat="server" Text='<%# Eval("org_name_th") %>' />
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ฝ่าย" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="lbdept_name_th" runat="server" Text='<%# Eval("dept_name_th") %>' />
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="รหัสหัวข้อ/หลักสูตร" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="lbtraining_code" runat="server" Text='<%# Eval("training_code") %>' />
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ชื่อหัวข้อ/หลักสูตร" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="lbtraining_name" runat="server" Text='<%# Eval("training_name") %>' />
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ประเภทการ" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                            <ItemTemplate>

                                <asp:Label ID="lbtraining_req_type_flag" runat="server" Text='<%# gettraining_req_type((int)Eval("training_req_type_flag")) %>' />

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ความต้องการ/ความจำเป็นในการฝึก" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                            <ItemTemplate>

                                <asp:Label ID="lbtraining_req_needs" runat="server" Text='<%# Eval("training_req_needs") %>' />

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="งบประมาณต่อรุ่น" ItemStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="13%">
                            <ItemTemplate>

                                <asp:Label ID="lbtraining_req_budget" runat="server" Text='<%# Eval("training_req_budget") %>' />

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="เดือนที่ต้องการเรียน" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="13%">
                            <ItemTemplate>

                                <asp:Label ID="lbtraining_req_month_study" runat="server" Text='<%# Eval("training_req_month_study") %>' />

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="จำนวนผู้เรียน" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="13%">
                            <ItemTemplate>

                                <asp:Label ID="lbtraining_req_qty" runat="server" Text='<%# Eval("training_req_qty") %>' />

                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>

                <asp:Panel ID="Panel6" runat="server" Visible="false">
                    <asp:GridView ID="GvListData_TrnNSurDept_Excel"
                        runat="server"
                        AutoGenerateColumns="false"
                        CssClass="table table-striped table-bordered table-responsive col-md-12"
                        HeaderStyle-CssClass="info"
                        AllowPaging="false"
                        OnRowCommand="onRowCommand"
                        OnRowDataBound="onRowDataBound"
                        OnPageIndexChanging="gvPageIndexChanging"
                        ShowFooter="false">
                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่พบข้อมูล</div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                                <ItemTemplate>
                                    <asp:Label ID="lbDataItemIndex" runat="server" Text='<%# (Container.DataItemIndex + 1) %>' />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="องค์กร" ItemStyle-HorizontalAlign="Left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lborg_name_th" runat="server" Text='<%# Eval("org_name_th") %>' />
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ฝ่าย" ItemStyle-HorizontalAlign="Left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbdept_name_th" runat="server" Text='<%# Eval("dept_name_th") %>' />
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="รหัสหัวข้อ/หลักสูตร" ItemStyle-HorizontalAlign="Left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbtraining_code" runat="server" Text='<%# Eval("training_code") %>' />
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ชื่อหัวข้อ/หลักสูตร" ItemStyle-HorizontalAlign="Left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbtraining_name" runat="server" Text='<%# Eval("training_name") %>' />
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ประเภทการ" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                                <ItemTemplate>

                                    <asp:Label ID="lbtraining_req_type_flag" runat="server" Text='<%# gettraining_req_type((int)Eval("training_req_type_flag")) %>' />

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ความต้องการ/ความจำเป็นในการฝึก" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                                <ItemTemplate>

                                    <asp:Label ID="lbtraining_req_needs" runat="server" Text='<%# Eval("training_req_needs") %>' />

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="งบประมาณต่อรุ่น" ItemStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="13%">
                                <ItemTemplate>

                                    <asp:Label ID="lbtraining_req_budget" runat="server" Text='<%# Eval("training_req_budget") %>' />

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="เดือนที่ต้องการเรียน" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="13%">
                                <ItemTemplate>

                                    <asp:Label ID="lbtraining_req_month_study" runat="server" Text='<%# Eval("training_req_month_study") %>' />

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวนผู้เรียน" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="13%">
                                <ItemTemplate>

                                    <asp:Label ID="lbtraining_req_qty" runat="server" Text='<%# Eval("training_req_qty") %>' />

                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>
                </asp:Panel>

            </div>

        </asp:View>

    </asp:MultiView>

    <br />

    <%-- end search needs --%>

    <asp:HiddenField ID="Hddfld_status" runat="server" />
    <asp:HiddenField ID="Hddfld_course_no" runat="server" />
    <asp:HiddenField ID="Hddfld_u0_course_idx" runat="server" />
    <asp:HiddenField ID="Hddfld_folderImage" runat="server" />
    <asp:Label ID="lbfolderImage" runat="server" Text=""></asp:Label>
    <br />



    <script type="text/javascript">
        function imageproposUpload(input) {
            if (input.files && input.files[0]) {

                var filerdr = new FileReader();
                filerdr.onload = function (e) {
                    $('#img_1').attr('src', e.target.result);
                }
                filerdr.readAsDataURL(input.files[0]);

                var data_proposition = new FormData();

                var files = $("#UploadImages_proposition_html").get(0).files;

                if (files.length > 0) {
                    data_proposition.append("UploadedImage_proposition", files[0]);
                }

                var ajaxRequest = $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("el_TrnCourse.aspx") %>',
                    contentType: false,
                    processData: false,
                    data: data_proposition,
                    success: function (response) {
                       // alert('succes!!');
                       <%-- $('#UploadImages_proposition_html').val('');
                        $("#<%=lbfolderImage.ClientID%>").val('');--%>
                    },
                    error: function (error) {
                        alert("errror");
                        $('#img_1').attr('src', '');
                    }
                });

                ajaxRequest.done(function (xhr, textStatus) {
                    // Do other operation
                });
            }

        }

    </script>

    <script type="text/javascript">
        function imageUpload_del(value) {
            var data_proposition = new FormData();
            // data_proposition.append("delImage_proposition", value);
            //alert(data_proposition);

            if (value == 1) {
                var files = $("#UploadImages_proposition_html").get(0).files;
                if (files.length > 0) {
                    data_proposition.append("delImage_1", files[0]);
                }
            }
            else if (value == 2) {
                var files = $("#UploadImages_a_html").get(0).files;
                if (files.length > 0) {
                    data_proposition.append("delImage_2", files[0]);
                }
            }
            else if (value == 3) {
                var files = $("#UploadImages_b_html").get(0).files;
                if (files.length > 0) {
                    data_proposition.append("delImage_3", files[0]);
                }
            }
            else if (value == 4) {
                var files = $("#UploadImages_c_html").get(0).files;
                if (files.length > 0) {
                    data_proposition.append("delImage_4", files[0]);
                }
            }
            else if (value == 5) {
                var files = $("#UploadImages_d_html").get(0).files;
                if (files.length > 0) {
                    data_proposition.append("delImage_5", files[0]);
                }
            }

            //for (i = 1; i <= value; i++) {
            //    data_proposition.append("delImage_proposition", files[0]);
            //}
            var ajaxRequest = $.ajax({
                type: "POST",
                url: '<%=ResolveUrl("el_TrnCourse.aspx") %>',
                contentType: false,
                processData: false,
                data: data_proposition,
                success: function (response) {

                    if (value == 1) {
                        $('#UploadImages_proposition_html').val('');
                        $('#img_1').attr('src', '');
                    }
                    else if (value == 2) {
                        $('#UploadImages_a_html').val('');
                        $('#img_2').attr('src', '');
                    }
                    else if (value == 3) {
                        $('#UploadImages_b_html').val('');
                        $('#img_3').attr('src', '');
                    }
                    else if (value == 4) {
                        $('#UploadImages_c_html').val('');
                        $('#img_4').attr('src', '');
                    }
                    else if (value == 5) {
                        $('#UploadImages_d_html').val('');
                        $('#img_5').attr('src', '');
                    }
                },
                error: function (error) {
                    alert("delete errror");
                }
            });
            ajaxRequest.done(function (xhr, textStatus) {
                // Do other operation
            });

        }
    </script>

    <script type="text/javascript">
        function image_aUpload(input) {
            if (input.files && input.files[0]) {

                var filerdr = new FileReader();
                filerdr.onload = function (e) {
                    $('#img_2').attr('src', e.target.result);
                }
                filerdr.readAsDataURL(input.files[0]);

                var data_proposition = new FormData();

                var files = $("#UploadImages_a_html").get(0).files;
                if (files.length > 0) {
                    data_proposition.append("UploadedImage_a", files[0]);
                }

                var ajaxRequest = $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("el_TrnCourse.aspx") %>',
                    contentType: false,
                    processData: false,
                    data: data_proposition,
                    success: function (response) {
                        //alert('succes!!');
                        //document.getElementById('lbfolderImage').value = "1";
                        //alert(document.getElementById('lbfolderImage').value);
                       // $("#<%=lbfolderImage.ClientID%>")[0].value = "1";
                    },
                    error: function (error) {
                        alert("errror");
                        $('#img_2').attr('src', '');
                    }
                });

                ajaxRequest.done(function (xhr, textStatus) {
                    // Do other operation
                });
            }
        }
    </script>

    <script type="text/javascript">
        function image_bUpload(input) {
            if (input.files && input.files[0]) {

                var filerdr = new FileReader();
                filerdr.onload = function (e) {
                    $('#img_3').attr('src', e.target.result);
                }
                filerdr.readAsDataURL(input.files[0]);

                var data_proposition = new FormData();

                var files = $("#UploadImages_b_html").get(0).files;

                if (files.length > 0) {
                    data_proposition.append("UploadedImage_b", files[0]);
                }

                var ajaxRequest = $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("el_TrnCourse.aspx") %>',
                    contentType: false,
                    processData: false,
                    data: data_proposition,
                    success: function (response) {
                        // alert('succes!!');
                    },
                    error: function (error) {
                        alert("errror");
                        $('#img_3').attr('src', '');
                    }
                });

                ajaxRequest.done(function (xhr, textStatus) {
                    // Do other operation
                });
            }
        }
    </script>


    <script type="text/javascript">
        function image_cUpload(input) {
            if (input.files && input.files[0]) {

                var filerdr = new FileReader();
                filerdr.onload = function (e) {
                    $('#img_4').attr('src', e.target.result);
                }
                filerdr.readAsDataURL(input.files[0]);

                var data_proposition = new FormData();

                var files = $("#UploadImages_c_html").get(0).files;

                if (files.length > 0) {
                    data_proposition.append("UploadedImage_c", files[0]);
                }

                var ajaxRequest = $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("el_TrnCourse.aspx") %>',
                    contentType: false,
                    processData: false,
                    data: data_proposition,
                    success: function (response) {
                        // alert('succes!!');
                    },
                    error: function (error) {
                        alert("errror");
                        $('#img_4').attr('src', '');
                    }
                });

                ajaxRequest.done(function (xhr, textStatus) {
                    // Do other operation
                });
            }
        }
    </script>


    <script type="text/javascript">
        function image_dUpload(input) {
            if (input.files && input.files[0]) {

                var filerdr = new FileReader();
                filerdr.onload = function (e) {
                    $('#img_5').attr('src', e.target.result);
                }
                filerdr.readAsDataURL(input.files[0]);

                var data_proposition = new FormData();

                var files = $("#UploadImages_d_html").get(0).files;

                if (files.length > 0) {
                    data_proposition.append("UploadedImage_d", files[0]);
                }

                var ajaxRequest = $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("el_TrnCourse.aspx") %>',
                    contentType: false,
                    processData: false,
                    data: data_proposition,
                    success: function (response) {
                        // alert('succes!!');
                    },
                    error: function (error) {
                        alert("errror");
                        $('#img_5').attr('src', '');
                    }
                });

                ajaxRequest.done(function (xhr, textStatus) {
                    // Do other operation
                });
            }
        }
    </script>

    <script type="text/javascript">
        function imageUpload(input) {
            if (input.files && input.files[0]) {

                var filerdr = new FileReader();
                filerdr.onload = function (e) {
                    $('#img_proposition').attr('src', e.target.result);
                    alert(e.target.result);
                }

                filerdr.readAsDataURL(input.files[0]);

                var data = new FormData();

                var files = $("#fileUpload").get(0).files;

                if (files.length > 0) {
                    data.append("UploadedImage", files[0]);
                }

                var ajaxRequest = $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("el_TrnCourse.aspx") %>',
                    contentType: false,
                    processData: false,
                    data: data,
                    success: function (response) {
                        //alert('succes!!');
                    },
                    error: function (error) {
                        // alert("errror");
                        $('#img_proposition').attr('src', "");
                    }
                });

                ajaxRequest.done(function (xhr, textStatus) {
                    // Do other operation
                });



                <%--
                 var uploadfiles = $("#fileUpload").get(0);
                 var uploadedfiles = uploadfiles.files;

                var fromdata = new FormData();

                for (var i = 0; i < uploadedfiles.length; i++) {

                    fromdata.append(uploadedfiles[i].name, uploadedfiles[i]);

                }
        
                var choice = {};
                choice.url = '<%=ResolveUrl("el_TrnCourse.aspx") %>/imagePropositionProcessRequest';
                choice.type = "POST";
                choice.data = fromdata;
                choice.contentType = false;
                choice.processData = false;
                choice.success = function (result) { alert(result); };
                choice.error = function (err) { alert(err.statusText); };
                $.ajax(choice);
            event.preventDefault();--%>

            }
        }
    </script>

    <script type="text/javascript">

        $(document).ready(function () {

            $("#BtnUpload").click(function (event) {

                var uploadfiles = $("#MultipleFilesUpload").get(0);

                var uploadedfiles = uploadfiles.files;

                var fromdata = new FormData();

                for (var i = 0; i < uploadedfiles.length; i++) {

                    fromdata.append(uploadedfiles[i].name, uploadedfiles[i]);

                }

                var choice = {};

                choice.url = '<%=ResolveUrl("el_TrnCourse.aspx") %>/imagePropositionProcessRequest';
                choice.type = "POST";
                choice.data = fromdata;
                choice.contentType = false;
                choice.processData = false;
                choice.success = function (result) { alert(result); };
                choice.error = function (err) { alert(err.statusText); };
                $.ajax(choice);
                event.preventDefault();

            });

        });

    </script>

    <style type="text/css">
        .wrapword_p1 {
            word-wrap: break-word;
        }
    </style>
    <script type="text/javascript">
        function printDiv(divName) {
            var panel = document.getElementById(divName);
            var printWindow = window.open('', '', 'height=800,width=1000');
            printWindow.document.write('<html><head><title></title></head>');
            printWindow.document.write('<body style="font-size:12px;">');
            printWindow.document.write('<style>table{width:100%;color:#000000;font-size:10px;};</style>');
            printWindow.document.write(panel.innerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            setTimeout(function () {
                printWindow.print();
                //printWindow.resizeTo(100,100);
                //printWindow.focus();
                //printwindow.window.close();
            }, 500);
            return false;
        }

        function confirmWithOutValidated(message, validateGroup) {
            if (validateGroup != '') {
                var validated = Page_ClientValidate(validateGroup);
                if (validated) {
                    return confirm(message);
                }
            } else {
                return confirm(message);
            }
        }

        function dtPickerInput(inputClassFrom, inputClassTo) {
            $(inputClassFrom).datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $(inputClassTo).datetimepicker({
                format: 'DD/MM/YYYY'
            });
        }

        function dtPickerBtn(inputClassFrom, inputClassTo, btnClassFrom, btnClassTo) {
            $(btnClassFrom).click(function () {
                $(inputClassFrom).data("DateTimePicker").show();
            });
            $(btnClassTo).click(function () {
                $(inputClassTo).data("DateTimePicker").show();
            });
        }

        $(function () {
            $('.header-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-manual-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-import-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.show-from-onclick').click(function () {
                $('.cutstock-manual-date').data("DateTimePicker").show();
            });

            $('.show-order-sale-log-from-onclick').click(function () {
                $('.filter-order-from').data("DateTimePicker").show();
            });
            $('.show-order-sale-log-to-onclick').click(function () {
                $('.filter-order-to').data("DateTimePicker").show();
            });
            $('.filter-order-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });


            $('.show-order-fix-log-from-onclick').click(function () {
                $('.filter-order-fix-from').data("DateTimePicker").show();
            });
            $('.show-order-fix-log-to-onclick').click(function () {
                $('.filter-order-fix-to').data("DateTimePicker").show();
            });
            $('.filter-order-fix-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-fix-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });


            $('.show-u1-from-onclick').click(function () {
                $('.filter-u1-from').data("DateTimePicker").show();
            });
            $('.show-u1-to-onclick').click(function () {
                $('.filter-u1-to').data("DateTimePicker").show();
            });
            $('.filter-u1-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-u1-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $('.header-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-manual-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-import-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.show-from-onclick').click(function () {
                $('.cutstock-manual-date').data("DateTimePicker").show();
            });

            $('.show-order-sale-log-from-onclick').click(function () {
                $('.filter-order-from').data("DateTimePicker").show();
            });
            $('.show-order-sale-log-to-onclick').click(function () {
                $('.filter-order-to').data("DateTimePicker").show();
            });
            $('.filter-order-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });


            $('.show-order-fix-log-from-onclick').click(function () {
                $('.filter-order-fix-from').data("DateTimePicker").show();
            });
            $('.show-order-fix-log-to-onclick').click(function () {
                $('.filter-order-fix-to').data("DateTimePicker").show();
            });
            $('.filter-order-fix-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-fix-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });


            $('.show-u1-from-onclick').click(function () {
                $('.filter-u1-from').data("DateTimePicker").show();
            });
            $('.show-u1-to-onclick').click(function () {
                $('.filter-u1-to').data("DateTimePicker").show();
            });
            $('.filter-u1-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-u1-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });
    </script>
    <script type="text/javascript">
        tinymce.init({
            mode: "specific_textareas",
            editor_selector: "tinymce",
            encoding: "xml",
            theme: "modern",
            menubar: false,
            resize: false,
            statusbar: false,
            plugins: ["advlist autolink lists charmap preview hr anchor",
                "pagebreak code nonbreaking table contextmenu directionality paste"],
            toolbar1: "styleselect | bold italic underline | undo redo",
            toolbar2: "alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
            setup: function (editor) {
                editor.on('change', function () { tinymce.triggerSave(); });
            }
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            tinymce.remove(".tinymce");

            tinymce.init({
                mode: "specific_textareas",
                editor_selector: "tinymce",
                encoding: "xml",
                theme: "modern",
                menubar: false,
                resize: false,
                statusbar: false,
                plugins: ["advlist autolink lists charmap preview hr anchor",
                    "pagebreak code nonbreaking table contextmenu directionality paste"],
                toolbar1: "styleselect | bold italic underline | undo redo",
                toolbar2: "alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
                setup: function (editor) {
                    editor.on('change', function () { tinymce.triggerSave(); });
                }
            });

            $(".multi").MultiFile();
        })
    </script>
    <div>
    </div>

</asp:Content>

