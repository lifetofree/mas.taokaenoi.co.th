﻿using DotNet.Highcharts;
using DotNet.Highcharts.Helpers;
using DotNet.Highcharts.Options;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_hr_perf_evalu_emp_monthly : System.Web.UI.Page
{
    //2018-12-28 08:52:39.950//
    #region Connect

    service_execute serviceexcute = new service_execute();
    service_mail servicemail = new service_mail();
    function_tool _funcTool = new function_tool();
    data_employee _dtEmployee = new data_employee();
    data_hr_evaluation _data_hr_evaluation = new data_hr_evaluation();
    function_dmu _func_dmu = new function_dmu();

    string _localJson = String.Empty;
    int _tempInt = 0;
    string _mail_subject = "";
    string _mail_body = "";
    string _link = "";
    int emp_idx = 0;


    // set object
    FormView _FormView;

    GridView
        _GridView
        ;

    TextBox _txtremark,
        _txt,
         _txt_hidden_u0idx
        ;

    DropDownList
        _ddl
        ;
    Panel
        _Panel_body_approve,
        _Panel_approve,
        _Panel_ImportAdd
        ;
    Label
        _lb_title_approve,
        _ddlu0_status
        ;
    Repeater
        _Repeater
        ;
    LinkButton
        _lbtn,
        _btnsave,
        _btncancel,
        _btncalculator,
        _btnConfirm
        ;
    CheckBox
        _CheckBox
        ;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];

    // start it asset 
    // static string _urlGetits_lookup = _serviceUrl + ConfigurationManager.AppSettings["urlGetits_lookup"];
    //its_u_hr_evaluation
    static string _urlGetits_u_hr_evaluation_monthly = _serviceUrl + ConfigurationManager.AppSettings["urlGetits_u_hr_evaluation_monthly"];
    static string _urlSetInsits_u_hr_evaluation_monthly = _serviceUrl + ConfigurationManager.AppSettings["urlSetInsits_u_hr_evaluation_monthly"];
    static string _urlSetUpdits_u_hr_evaluation_monthly = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdits_u_hr_evaluation_monthly"];
    static string _urlDelits_u_hr_evaluation_monthly = _serviceUrl + ConfigurationManager.AppSettings["urlDelits_u_hr_evaluation_monthly"];

    //static string _urlGetits_masterit = _serviceUrl + ConfigurationManager.AppSettings["urlGetits_masterit"];
    //static string _urlsendEmailits_u_hr_evaluation = _serviceUrl + ConfigurationManager.AppSettings["urlsendEmailits_u_hr_evaluation"];

    static string _urlsendEmailhr_evaluation = _serviceUrl + ConfigurationManager.AppSettings["urlsendEmailhr_evaluation"];

    // end it asset

    static string _urlGetddlDeptSW = _serviceUrl + ConfigurationManager.AppSettings["urlGetddlDeptSW"];
    static string _urlGetddlOrganizationSW = _serviceUrl + ConfigurationManager.AppSettings["urlGetddlOrganizationSW"];
    static string _urlGetlocation = _serviceUrl + ConfigurationManager.AppSettings["urlGetlocation"];



    #endregion Connect

    private void Page_Init(object sender, EventArgs e)
    {
        ClearApplicationCache();
    }
    public void ClearApplicationCache()
    {
        List<string> keys = new List<string>();
        // retrieve application Cache enumerator
        IDictionaryEnumerator enumerator = Cache.GetEnumerator();
        // copy all keys that currently exist in Cache
        while (enumerator.MoveNext())
        {
            keys.Add(enumerator.Key.ToString());
        }

        // delete every key from cache
        for (int i = 0; i < keys.Count; i++)
        {
            Cache.Remove(keys[i]);
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        emp_idx = int.Parse(Session["emp_idx"].ToString());
        if (!Page.IsPostBack)
        {
            ViewState["EmpIDX"] = int.Parse(Session["emp_idx"].ToString());

            ViewState["mode"] = "p";
            select_empIdx_present();
            setActiveTab("p");
            if (_func_dmu.zStringToInt(ViewState["status_u0idx"].ToString()) == 0)
            {
                ShowDataIndex();
            }
            else
            {
                int _u0idx = _func_dmu.zStringToInt(ViewState["status_u0idx"].ToString());
                setActiveTab("insert");
                ViewState["mode"] = "update";
                MvMaster.SetActiveView(ViewMain);
                FvInsert.ChangeMode(FormViewMode.Insert);
                FvInsert.DataBind();
                FvInsertH.ChangeMode(FormViewMode.Edit);
                FvInsertH.DataBind();
                VMain_Showdata(_u0idx);

            }
            SETFOCUS.Focus();

        }
        setBtnTrigger();
    }
    private void setBtnTrigger()
    {
        linkBtnTrigger(_divMenuBtnToIndex);
        linkBtnTrigger(_divMenuBtnToInsert);
        linkBtnTrigger(_divMenuBtnToGarp);
        linkBtnTrigger(btnSearchGraph);
        linkBtnTrigger(btnexport_report);
        GridViewTrigger(GvIndexlist);

    }
    protected void GridViewTrigger(GridView gridview)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = gridview.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }
    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }
    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var gridViewName = (GridView)sender;

        if (gridViewName.ID == "GvIndexlist")
        {
            gridViewName.PageIndex = e.NewPageIndex;
            gridViewName.DataBind();
            _func_dmu.zSetGridData(gridViewName, ViewState["V_GvIndexlist"]);
            SETFOCUS.Focus();
        }
    }

    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var gridViewName = (GridView)sender;

        if (gridViewName.ID == "gvItemsu1PerfEvaluEmpDaily")
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                Label lb_u0_emp_idx = (Label)e.Row.FindControl("lb_u0_emp_idx");
                Label lb_group_code = (Label)e.Row.FindControl("lb_group_code");
                Label lb_u0_status = (Label)e.Row.FindControl("lb_u0_status");

                TextBox txt_leader_ownership_qty = (TextBox)e.Row.FindControl("txt_leader_ownership_qty");
                TextBox txt_leader_improvement_continuously_qty = (TextBox)e.Row.FindControl("txt_leader_improvement_continuously_qty");
                TextBox txt_leader_commitment_qty = (TextBox)e.Row.FindControl("txt_leader_commitment_qty");

                TextBox txt_leader_innovation = (TextBox)e.Row.FindControl("txt_leader_innovation");
                TextBox txt_leader_achieveing_result = (TextBox)e.Row.FindControl("txt_leader_achieveing_result");
                TextBox txt_leader_relation = (TextBox)e.Row.FindControl("txt_leader_relation");
                TextBox txt_leader_job_knowledge = (TextBox)e.Row.FindControl("txt_leader_job_knowledge");
                TextBox txt_leader_art_communication = (TextBox)e.Row.FindControl("txt_leader_art_communication");
                TextBox txt_leader_leadership = (TextBox)e.Row.FindControl("txt_leader_leadership");
                TextBox txt_leader_decision_making_planning = (TextBox)e.Row.FindControl("txt_leader_decision_making_planning");

                Boolean _BMode = false;
                txt_leader_ownership_qty.Enabled = _BMode;
                txt_leader_improvement_continuously_qty.Enabled = _BMode;
                txt_leader_commitment_qty.Enabled = _BMode;

                txt_leader_innovation.Enabled = _BMode;
                txt_leader_achieveing_result.Enabled = _BMode;
                txt_leader_relation.Enabled = _BMode;
                txt_leader_job_knowledge.Enabled = _BMode;
                txt_leader_art_communication.Enabled = _BMode;
                txt_leader_leadership.Enabled = _BMode;
                txt_leader_decision_making_planning.Enabled = _BMode;

                _BMode = true;

                if (
                    (
                    (_func_dmu.zStringToInt(lb_u0_emp_idx.Text) == emp_idx)
                    ||
                    (ViewState["mode"].ToString() == "insert")
                    )
                    &&
                    (_func_dmu.zStringToInt(lb_u0_status.Text) != 2)
                    )
                {
                    if (lb_group_code.Text == "1")
                    {

                        txt_leader_innovation.Enabled = _BMode;
                        txt_leader_achieveing_result.Enabled = _BMode;
                        txt_leader_relation.Enabled = _BMode;
                        txt_leader_job_knowledge.Enabled = _BMode;
                        txt_leader_art_communication.Enabled = _BMode;
                        txt_leader_leadership.Enabled = _BMode;
                        txt_leader_decision_making_planning.Enabled = _BMode;
                    }
                    else if (lb_group_code.Text == "2")
                    {

                        txt_leader_achieveing_result.Enabled = _BMode;
                        txt_leader_relation.Enabled = _BMode;
                        txt_leader_job_knowledge.Enabled = _BMode;
                    }
                    else
                    {
                        txt_leader_innovation.Enabled = _BMode;
                        txt_leader_achieveing_result.Enabled = _BMode;
                        txt_leader_relation.Enabled = _BMode;
                        txt_leader_job_knowledge.Enabled = _BMode;
                        txt_leader_art_communication.Enabled = _BMode;
                        txt_leader_leadership.Enabled = _BMode;
                        txt_leader_decision_making_planning.Enabled = _BMode;
                    }
                }

                if (
                    (
                    (_func_dmu.zStringToInt(lb_u0_emp_idx.Text) == emp_idx)
                    ||
                    (ViewState["mode"].ToString() == "insert")
                    )
                    &&
                    (_func_dmu.zStringToInt(lb_u0_status.Text) != 2)
                    )
                {
                    _BMode = true;
                }
                else
                {
                    _BMode = false;

                }
                txt_leader_ownership_qty.Enabled = _BMode;
                txt_leader_improvement_continuously_qty.Enabled = _BMode;
                txt_leader_commitment_qty.Enabled = _BMode;


            }
        }
        else if (gridViewName.ID == "GvIndexlist")
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton btnDelete = (LinkButton)e.Row.FindControl("btnDelete");
                LinkButton btnunconfirm = (LinkButton)e.Row.FindControl("btnunconfirm");
                Label lb_u0_emp_idx = (Label)e.Row.FindControl("lb_u0_emp_idx");
                Label lb_u0_status = (Label)e.Row.FindControl("lb_u0_status");
                if (
                    (_func_dmu.zStringToInt(lb_u0_emp_idx.Text) == emp_idx)
                    &&
                    (_func_dmu.zStringToInt(lb_u0_status.Text) == 1)
                    )
                {
                    btnDelete.Visible = true;
                }
                else
                {
                    btnDelete.Visible = false;
                }

                if ((ViewState["admin_idx"].ToString() == "1") && (lb_u0_status.Text == "2"))
                {
                    btnunconfirm.Visible = true;
                }
                else
                {
                    btnunconfirm.Visible = false;
                }
            }
        }
        else if (gridViewName.ID == "GVreport")
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton btnperf_evalu_print = (LinkButton)e.Row.FindControl("btnperf_evalu_print");
                Label lb_u0_status = (Label)e.Row.FindControl("lb_u0_status");
                if (lb_u0_status.Text == "2")
                {
                    btnperf_evalu_print.Visible = true;
                }
                else
                {
                    btnperf_evalu_print.Visible = false;
                }
            }
        }
    }

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        if (cmdName == "_divMenuBtnToIndex")
        {
            setActiveTab("p");
            ViewState["mode"] = "p";
            MvMaster.SetActiveView(ViewIndex);
            ShowDataIndex();
        }
        else if (cmdName == "_divMenuLiToInsert")
        {
            setActiveTab("insert");
            ViewState["mode"] = "insert";
            MvMaster.SetActiveView(ViewMain);
            FvInsert.ChangeMode(FormViewMode.Insert);
            FvInsert.DataBind();
            FvInsertH.ChangeMode(FormViewMode.Insert);
            FvInsertH.DataBind();
            VMain_Showdata(0);

        }
        else if (cmdName == "btncalculator")
        {
            if (setCheckError(2) == false)
            {
                setCalculator();
            }

        }
        else if (cmdName == "btnSave")
        {

            if (setCheckError(2) == false)
            {
                setCalculator();
                setObject_Main();
                int _u0idx = _func_dmu.zStringToInt(_txt_hidden_u0idx.Text);
                _u0idx = zSave(_u0idx, 1);
                
                //if (_u0idx > 0)
                //{
                //    setActiveTab("insert");
                //    ViewState["mode"] = "update";
                //    MvMaster.SetActiveView(ViewMain);
                //    FvInsert.ChangeMode(FormViewMode.Insert);
                //    FvInsert.DataBind();
                //    FvInsertH.ChangeMode(FormViewMode.Edit);
                //    FvInsertH.DataBind();
                //    VMain_Showdata(_u0idx);
                //}
            }

        }
        else if (cmdName == "btnCancel")
        {
            // Page.Response.Redirect(Page.Request.Url.ToString(), true);
            setActiveTab("p");
            ViewState["mode"] = "p";
            MvMaster.SetActiveView(ViewIndex);
            ShowDataIndex();
            SETFOCUS.Focus();
        }
        else if (cmdName == "btnmanage_its")
        {
            int _u0idx = _func_dmu.zStringToInt(cmdArg);
            setActiveTab("insert");
            ViewState["mode"] = "update";
            MvMaster.SetActiveView(ViewMain);
            FvInsert.ChangeMode(FormViewMode.Insert);
            FvInsert.DataBind();
            FvInsertH.ChangeMode(FormViewMode.Edit);
            FvInsertH.DataBind();
            VMain_Showdata(_u0idx);
        }
        else if (cmdName == "btnDelete")
        {
            int _u0idx = _func_dmu.zStringToInt(cmdArg);
            zDelete(_u0idx);
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
        }
        else if (cmdName == "btnexport")
        {
            int _u0idx = _func_dmu.zStringToInt(cmdArg);
            zExportExcel(_u0idx);
        }
        else if (cmdName == "_divMenuBtnToGarp")
        {
            int _u0idx = _func_dmu.zStringToInt(cmdArg);
            setActiveTab("report");
            ViewState["mode"] = "report";
            MvMaster.SetActiveView(View_report);
            Update_PanelGraph.Visible = false;
            Update_Panelreport.Visible = false;
            CreateDsits_u1_PerfEvaluEmpDaily();
            select_hr_year(ddlyear);
            ddlTypeReport.SelectedIndex = 0;
            txt_emp_code.Text = "";

            if (
                (_func_dmu.zStringToInt(ViewState["admin_idx"].ToString()) == 2)
                ||
                (_func_dmu.zStringToInt(ViewState["admin_idx"].ToString()) == 3)
                ) //k. แก้ม
            {
                GVreport.Columns[53].Visible = true;
            }
            else
            {
                GVreport.Columns[53].Visible = false;
            }

        }

        else if (cmdName == "btnConfirm")
        {

            if (setCheckError(1) == false)
            {
                setCalculator();
                setObject_Main();
                int _u0idx = _func_dmu.zStringToInt(_txt_hidden_u0idx.Text);
                zSave(_u0idx, 2);
                Page.Response.Redirect(Page.Request.Url.ToString(), true);

            }

        }
        else if (cmdName == "btnexport_report")
        {
            zExportExcel_report();
        }
        else if (cmdName == "cmdSearchReportGraph")
        {
            if (_func_dmu.zStringToInt(ddlTypeReport.SelectedValue) == 1)
            {
                ReportGraph();
            }
            else
            {
                getCountBookingReportGraph();
            }

        }
        else if (cmdName == "btnunconfirm")
        {
            int _u0idx = _func_dmu.zStringToInt(cmdArg);
            zUnconfirm(_u0idx);
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
        }
        else if (cmdName == "btnImport")
        {
            ImportData();
        }
        else if (cmdName == "btnperf_evalu_print")
        {
            //  _U0IDX = int.Parse(cmdArg);
            string[] _argument = new string[2];
            _argument = cmdArg.ToString().Split('|');
            Session["_sesion_emp_idx"] = _argument[0];
            Session["_sesion_u1idx"] = _argument[1];
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "", "window.open('hr-perf-evalu-emp-profile-print', '', '');", true);

        }

    }



    #region setActiveTab
    protected void setActiveTab(string activeTab)
    {
        _divMenuLiToIndex.Attributes.Remove("class");
        _divMenuLiToDivInsert.Attributes.Remove("class");
        _divMenuLiToDivGarp.Attributes.Remove("class");

        if (activeTab == "p")
        {
            _divMenuLiToIndex.Attributes.Add("class", "active");
        }
        else if (activeTab == "insert")
        {
            _divMenuLiToDivInsert.Attributes.Add("class", "active");
        }
        else if (activeTab == "report")
        {
            _divMenuLiToDivGarp.Attributes.Add("class", "active");
        }

    }
    #endregion setActiveTab

    #region Select
    protected void select_empIdx_present()
    {

        _dtEmployee = callServiceEmpProfile(_urlGetMyProfile + ViewState["EmpIDX"].ToString());

        if (_dtEmployee.employee_list != null)
        {
            ViewState["rdept_name"] = _dtEmployee.employee_list[0].dept_name_th;
            ViewState["rdept_idx"] = _dtEmployee.employee_list[0].rdept_idx;
            ViewState["FullName"] = _dtEmployee.employee_list[0].emp_name_th;
            ViewState["Org_name"] = _dtEmployee.employee_list[0].org_name_th;
            ViewState["Org_idx"] = _dtEmployee.employee_list[0].org_idx;

            ViewState["EmpCode"] = _dtEmployee.employee_list[0].emp_code;
            ViewState["Positname"] = _dtEmployee.employee_list[0].pos_name_th;
            ViewState["Pos_idx"] = _dtEmployee.employee_list[0].rpos_idx;
            ViewState["Email"] = _dtEmployee.employee_list[0].emp_email;
            ViewState["Tel"] = _dtEmployee.employee_list[0].emp_mobile_no;
            ViewState["Secname"] = _dtEmployee.employee_list[0].sec_name_th;
            ViewState["Sec_idx"] = _dtEmployee.employee_list[0].rsec_idx;
            ViewState["CostIDX"] = _dtEmployee.employee_list[0].costcenter_idx;

            ViewState["rsec_idx"] = _dtEmployee.employee_list[0].rsec_idx;
            ViewState["rdept_idx"] = _dtEmployee.employee_list[0].rdept_idx;
            ViewState["rpos_idx"] = _dtEmployee.employee_list[0].rpos_idx;
            ViewState["org_idx"] = _dtEmployee.employee_list[0].org_idx;
            ViewState["jobgrade_level"] = _dtEmployee.employee_list[0].jobgrade_level;
            ViewState["locidx"] = _dtEmployee.employee_list[0].LocIDX;

            if (emp_idx == 33455)
            {
                ViewState["Pos_idx"] = 5900;
                ViewState["Sec_idx"] = 431;

                ViewState["rsec_idx"] = 431;
                ViewState["rdept_idx"] = 13;
                ViewState["rpos_idx"] = 5900;
                ViewState["jobgrade_level"] = 9;
            }

        }
        else
        {
            ViewState["rdept_name"] = "";
            ViewState["rdept_idx"] = "";
            ViewState["FullName"] = "";
            ViewState["Org_name"] = "";
            ViewState["Org_idx"] = "";

            ViewState["EmpCode"] = "";
            ViewState["Positname"] = "";
            ViewState["Pos_idx"] = "";
            ViewState["Email"] = "";
            ViewState["Tel"] = "";
            ViewState["Secname"] = "";
            ViewState["Sec_idx"] = "";
            ViewState["CostIDX"] = "";

            ViewState["rsec_idx"] = "";
            ViewState["rdept_idx"] = "";
            ViewState["rpos_idx"] = "";
            ViewState["org_idx"] = "";
            ViewState["jobgrade_level"] = "";
            ViewState["locidx"] = "";
        }

        ViewState["emp_idx"] = ViewState["EmpIDX"].ToString();


        _data_hr_evaluation.hr_perf_evalu_emp_monthly_action = new hr_perf_evalu_emp_monthly[1];
        hr_perf_evalu_emp_monthly select_obj = new hr_perf_evalu_emp_monthly();
        select_obj.emp_idx = _func_dmu.zStringToInt(ViewState["emp_idx"].ToString());
        select_obj.rpos_idx = _func_dmu.zStringToInt(ViewState["rpos_idx"].ToString());
        select_obj.rdept_idx = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());
        select_obj.operation_status_id = "hr_setpermission_admin_dept_m0";
        _data_hr_evaluation.hr_perf_evalu_emp_monthly_action[0] = select_obj;
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_hr_evaluation));
        _data_hr_evaluation = callServicePostPerfEvaluEmp(_urlGetits_u_hr_evaluation_monthly, _data_hr_evaluation);
        if (_data_hr_evaluation.hr_perf_evalu_emp_monthly_action != null)
        {
            foreach (var item in _data_hr_evaluation.hr_perf_evalu_emp_monthly_action)
            {
                ViewState["admin_idx"] = item.admin_idx;
            }
        }
        else
        {
            ViewState["admin_idx"] = 0;
        }


        _data_hr_evaluation.hr_perf_evalu_emp_monthly_action = new hr_perf_evalu_emp_monthly[1];
        select_obj = new hr_perf_evalu_emp_monthly();
        select_obj.emp_idx = _func_dmu.zStringToInt(ViewState["emp_idx"].ToString());
        select_obj.operation_status_id = "list_dataconfirm";
        _data_hr_evaluation.hr_perf_evalu_emp_monthly_action[0] = select_obj;
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_hr_evaluation));
        _data_hr_evaluation = callServicePostPerfEvaluEmp(_urlGetits_u_hr_evaluation_monthly, _data_hr_evaluation);
        if (_data_hr_evaluation.hr_perf_evalu_emp_monthly_action != null)
        {
            ViewState["status_u0idx"] = _data_hr_evaluation.hr_perf_evalu_emp_monthly_action[0].u0idx;
        }
        else
        {
            ViewState["status_u0idx"] = 0;
        }



    }
    #endregion Select

    protected void select_hr_year(DropDownList ddlName)
    {
        ddlName.Items.Clear();
        data_hr_evaluation datahrevaluation = new data_hr_evaluation();
        datahrevaluation.hr_perf_evalu_emp_monthly_action = new hr_perf_evalu_emp_monthly[1];
        hr_perf_evalu_emp_monthly select_obj = new hr_perf_evalu_emp_monthly();
        select_obj.operation_status_id = "list_th_year";
        datahrevaluation.hr_perf_evalu_emp_monthly_action[0] = select_obj;
        datahrevaluation = callServicePostPerfEvaluEmp(_urlGetits_u_hr_evaluation_monthly, datahrevaluation);
        ddlName.DataSource = datahrevaluation.hr_perf_evalu_emp_monthly_action;
        ddlName.DataTextField = "zyear";
        ddlName.DataValueField = "zyear";
        ddlName.DataBind();
        ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("ปี.....", "0"));

    }

    protected data_employee callServiceEmpProfile(string _cmdUrl)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }


    protected void ShowDataIndex()
    {


        _data_hr_evaluation.hr_perf_evalu_emp_monthly_action = new hr_perf_evalu_emp_monthly[1];
        hr_perf_evalu_emp_monthly select_obj = new hr_perf_evalu_emp_monthly();
        select_obj.emp_idx = emp_idx;
        select_obj.admin_idx = _func_dmu.zStringToInt(ViewState["admin_idx"].ToString());
        //select_obj.rpos_idx = _func_dmu.zStringToInt(ViewState["rpos_idx"].ToString());
        //select_obj.rdept_idx = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());
        select_obj.operation_status_id = "list_index";
        _data_hr_evaluation.hr_perf_evalu_emp_monthly_action[0] = select_obj;
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_hr_evaluation));
        _data_hr_evaluation = callServicePostPerfEvaluEmp(_urlGetits_u_hr_evaluation_monthly, _data_hr_evaluation);

        ViewState["V_GvIndexlist"] = _data_hr_evaluation.hr_perf_evalu_emp_monthly_action;

        _func_dmu.zSetGridData(GvIndexlist, ViewState["V_GvIndexlist"]);

    }
    protected data_hr_evaluation callServicePostPerfEvaluEmp(string _cmdUrl, data_hr_evaluation _dt)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dt);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dt = (data_hr_evaluation)_funcTool.convertJsonToObject(typeof(data_hr_evaluation), _localJson);

        return _dt;
    }
    public string getformatfloat(string Str, int i)
    {
        return _func_dmu.zFormatfloat(Str, i);
    }

    private void setObject_Main()
    {
        _GridView = (GridView)FvInsert.FindControl("gvItemsu1PerfEvaluEmpDaily");
        _txtremark = (TextBox)FvInsertH.FindControl("txtremark");
        _ddlu0_status = (Label)FvInsertH.FindControl("ddlu0_status");
        _txtremark = (TextBox)FvInsertH.FindControl("txtremark");
        _txt_hidden_u0idx = (TextBox)FvInsertH.FindControl("txt_hidden_u0idx");
        _btnsave = (LinkButton)FvInsertH.FindControl("btnSave");
        _btncalculator = (LinkButton)FvInsertH.FindControl("btncalculator");
        _btnConfirm = (LinkButton)FvInsertH.FindControl("btnConfirm");
        _Panel_ImportAdd = (Panel)FvInsertH.FindControl("Panel_ImportAdd");

    }
    private void setUserMain(int _emp_idx)
    {

        _dtEmployee = callServiceEmpProfile(_urlGetMyProfile + _emp_idx.ToString());
        //litDebug.Text = _dtEmployee.employee_list[0].emp_name_th;
        FvDetailUser_Main.DataSource = _dtEmployee.employee_list;
        FvDetailUser_Main.DataBind();
    }
    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        var FvName = (FormView)sender;
        if (FvName.ID == "FvInsertH")
        {
            if (
                       (FvName.CurrentMode == FormViewMode.Insert) ||
                       (FvName.CurrentMode == FormViewMode.Edit)
                       )
            {
                LinkButton btncalculator = (LinkButton)FvInsertH.FindControl("btncalculator");
                LinkButton btnSave = (LinkButton)FvInsertH.FindControl("btnSave");
                LinkButton btnConfirm = (LinkButton)FvInsertH.FindControl("btnConfirm");
                LinkButton btnCancel = (LinkButton)FvInsertH.FindControl("btnCancel");
                LinkButton btnImport = (LinkButton)FvInsertH.FindControl("btnImport");
                if (btncalculator != null)
                {
                    linkBtnTrigger(btncalculator);
                }
                if (btnSave != null)
                {
                    linkBtnTrigger(btnSave);
                }
                if (btnConfirm != null)
                {
                    linkBtnTrigger(btnConfirm);
                }
                if (btnCancel != null)
                {
                    linkBtnTrigger(btnCancel);
                }
                if (btnImport != null)
                {
                    linkBtnTrigger(btnImport);
                }

            }

        }
    }
    protected void CreateDsits_u1_PerfEvaluEmpDaily()
    {
        string sDs = "dshr_u1_perf_evalu_employee_daily";
        string sVs = "vshr_u1_perf_evalu_employee_daily";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);

        ds.Tables[sDs].Columns.Add("id", typeof(String));

        ds.Tables[sDs].Columns.Add("doccode", typeof(String));
        ds.Tables[sDs].Columns.Add("zdocdate", typeof(String));
        ds.Tables[sDs].Columns.Add("docdate", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_status", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_emp_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_org_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_rdept_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_rpos_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_rsec_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("remark", typeof(String));
        ds.Tables[sDs].Columns.Add("CEmpIDX", typeof(String));
        ds.Tables[sDs].Columns.Add("CreateDate", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_emp_name_th", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_pos_name_th", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_sec_name_th", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_org_name_th", typeof(String));
        ds.Tables[sDs].Columns.Add("u1idx", typeof(String));
        ds.Tables[sDs].Columns.Add("u0idx", typeof(String));
        ds.Tables[sDs].Columns.Add("emp_idx", typeof(String));

        ds.Tables[sDs].Columns.Add("work_experience", typeof(String));
        ds.Tables[sDs].Columns.Add("group_code", typeof(String));
        ds.Tables[sDs].Columns.Add("level_code", typeof(String));
        ds.Tables[sDs].Columns.Add("group_work", typeof(String));
        ds.Tables[sDs].Columns.Add("late_mm", typeof(String));
        ds.Tables[sDs].Columns.Add("Back_before_mm", typeof(String));
        ds.Tables[sDs].Columns.Add("missing_qty", typeof(String));
        ds.Tables[sDs].Columns.Add("sick_Leave_qty_day", typeof(String));
        ds.Tables[sDs].Columns.Add("Errandleave_day", typeof(String));
        ds.Tables[sDs].Columns.Add("vacation_day", typeof(String));
        ds.Tables[sDs].Columns.Add("workbreak_day", typeof(String));
        ds.Tables[sDs].Columns.Add("Maternityleave_day", typeof(String));
        ds.Tables[sDs].Columns.Add("corp_kpi", typeof(String));
        ds.Tables[sDs].Columns.Add("tkn_value", typeof(String));
        ds.Tables[sDs].Columns.Add("taokae_d", typeof(String));
        ds.Tables[sDs].Columns.Add("absence", typeof(String));
        ds.Tables[sDs].Columns.Add("punishment1", typeof(String));
        ds.Tables[sDs].Columns.Add("punishment2", typeof(String));
        ds.Tables[sDs].Columns.Add("ass_self_ownership_qty", typeof(String));
        ds.Tables[sDs].Columns.Add("ass_self_improvement_continuously_qty", typeof(String));
        ds.Tables[sDs].Columns.Add("ass_self_commitment_qty", typeof(String));
        ds.Tables[sDs].Columns.Add("ass_self_innovation", typeof(String));
        ds.Tables[sDs].Columns.Add("ass_self_achieveing_result", typeof(String));
        ds.Tables[sDs].Columns.Add("ass_self_relation", typeof(String));
        ds.Tables[sDs].Columns.Add("ass_self_job_knowledge", typeof(String));
        ds.Tables[sDs].Columns.Add("ass_self_art_communication", typeof(String));
        ds.Tables[sDs].Columns.Add("ass_self_leadership", typeof(String));
        ds.Tables[sDs].Columns.Add("ass_self_decision_making_planning", typeof(String));
        ds.Tables[sDs].Columns.Add("leader_ownership_qty", typeof(String));
        ds.Tables[sDs].Columns.Add("leader_improvement_continuously_qty", typeof(String));
        ds.Tables[sDs].Columns.Add("leader_commitment_qty", typeof(String));
        ds.Tables[sDs].Columns.Add("leader_innovation", typeof(String));
        ds.Tables[sDs].Columns.Add("leader_achieveing_result", typeof(String));
        ds.Tables[sDs].Columns.Add("leader_relation", typeof(String));
        ds.Tables[sDs].Columns.Add("leader_job_knowledge", typeof(String));
        ds.Tables[sDs].Columns.Add("leader_art_communication", typeof(String));
        ds.Tables[sDs].Columns.Add("leader_leadership", typeof(String));
        ds.Tables[sDs].Columns.Add("leader_decision_making_planning", typeof(String));
        ds.Tables[sDs].Columns.Add("kpi", typeof(String));
        ds.Tables[sDs].Columns.Add("grade", typeof(String));
        ds.Tables[sDs].Columns.Add("grade_criteria", typeof(String));


        ds.Tables[sDs].Columns.Add("UEmpIDX", typeof(String));
        ds.Tables[sDs].Columns.Add("UpdateDate", typeof(String));
        ds.Tables[sDs].Columns.Add("u1_status", typeof(String));
        ds.Tables[sDs].Columns.Add("emp_code", typeof(String));
        ds.Tables[sDs].Columns.Add("rpos_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("pos_name_th", typeof(String));
        ds.Tables[sDs].Columns.Add("rsec_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("sec_name_th", typeof(String));
        ds.Tables[sDs].Columns.Add("rdept_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("dept_name_th", typeof(String));
        ds.Tables[sDs].Columns.Add("org_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("org_name_th", typeof(String));
        ds.Tables[sDs].Columns.Add("emp_name_th", typeof(String));
        ds.Tables[sDs].Columns.Add("costcenter_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("costcenter_no", typeof(String));
        ds.Tables[sDs].Columns.Add("costcenter_name", typeof(String));
        ds.Tables[sDs].Columns.Add("emp_probation_date", typeof(String));
        //ds.Tables[sDs].Columns.Add("tax_mounth", typeof(String));
        ds.Tables[sDs].Columns.Add("supervisor1_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("supervisor2_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("description", typeof(String));
        ds.Tables[sDs].Columns.Add("total_leader_tkn_value", typeof(String));
        ds.Tables[sDs].Columns.Add("total_leader_taokae_d", typeof(String));
        ds.Tables[sDs].Columns.Add("total_leader_ass_qty", typeof(String));
        ds.Tables[sDs].Columns.Add("total_deduction", typeof(String));
        ds.Tables[sDs].Columns.Add("score_balance", typeof(String));

        ds.Tables[sDs].Columns.Add("total_deduction_before", typeof(String));
        ds.Tables[sDs].Columns.Add("total_leader_ass_qty_before", typeof(String));
        ds.Tables[sDs].Columns.Add("total_leader_taokae_d_before", typeof(String));
        ds.Tables[sDs].Columns.Add("total_leader_tkn_value_before", typeof(String));
        ds.Tables[sDs].Columns.Add("score_balance_before", typeof(String));

        ds.Tables[sDs].Columns.Add("kpi_before", typeof(String));

        ViewState[sVs] = ds;

    }

    protected void VMain_Showdata(int id)
    {
        
        _data_hr_evaluation.hr_perf_evalu_emp_monthly_action = new hr_perf_evalu_emp_monthly[1];
        hr_perf_evalu_emp_monthly select_obj = new hr_perf_evalu_emp_monthly();
        select_obj.u0idx = id;
        select_obj.operation_status_id = "list_th";
        _data_hr_evaluation.hr_perf_evalu_emp_monthly_action[0] = select_obj;
        _data_hr_evaluation = callServicePostPerfEvaluEmp(_urlGetits_u_hr_evaluation_monthly, _data_hr_evaluation);
        FvInsertH.DataSource = _data_hr_evaluation.hr_perf_evalu_emp_monthly_action;
        FvInsertH.DataBind();

        Label lb_u0_status = (Label)FvInsertH.FindControl("lb_u0_status");
        Label ddlu0_status = (Label)FvInsertH.FindControl("ddlu0_status");

        lb_u0_status.Text = getStatus(_func_dmu.zStringToInt(ddlu0_status.Text));

        int _emp_idx = 0;
        if (_data_hr_evaluation.hr_perf_evalu_emp_action != null)
        {
            _emp_idx = _data_hr_evaluation.hr_perf_evalu_emp_action[0].u0_emp_idx;
        }
        else
        {
            _emp_idx = emp_idx;
        }

        FV_docno.DataSource = _data_hr_evaluation.hr_perf_evalu_emp_monthly_action;
        FV_docno.DataBind();

        setUserMain(_emp_idx);

        setObject_Main();
        _txtremark.Enabled = true;
        _btnsave.Visible = true;
        _btncalculator.Visible = true;
        _btnConfirm.Visible = true;
        _Panel_ImportAdd.Visible = true;

        if (id == 0)
        {
            _data_hr_evaluation.hr_perf_evalu_emp_monthly_action = new hr_perf_evalu_emp_monthly[1];
            select_obj = new hr_perf_evalu_emp_monthly();
            select_obj.emp_idx = emp_idx;
            select_obj.operation_status_id = "list_employee_monthly"; 
            _data_hr_evaluation.hr_perf_evalu_emp_monthly_action[0] = select_obj;
            // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_hr_evaluation));
            _data_hr_evaluation = callServicePostPerfEvaluEmp(_urlGetits_u_hr_evaluation_monthly, _data_hr_evaluation);
        }
        else
        {
            if (_data_hr_evaluation.hr_perf_evalu_emp_monthly_action != null)
            {
                foreach (var item in _data_hr_evaluation.hr_perf_evalu_emp_monthly_action)
                {
                    if ((item.u0_emp_idx == emp_idx) && (_func_dmu.zStringToInt(ddlu0_status.Text) != 2))
                    {
                        // 
                    }
                    else
                    {
                        _txtremark.Enabled = false;
                        _ddlu0_status.Enabled = false;
                        _btnsave.Visible = false;
                        _btncalculator.Visible = false;
                        _btnConfirm.Visible = false;
                        _Panel_ImportAdd.Visible = false;
                    }
                }
            }
            _data_hr_evaluation.hr_perf_evalu_emp_monthly_action = new hr_perf_evalu_emp_monthly[1];
            select_obj = new hr_perf_evalu_emp_monthly();
            select_obj.u0idx = id;
           
            select_obj.operation_status_id = "list_td"; // "list_td";
            _data_hr_evaluation.hr_perf_evalu_emp_monthly_action[0] = select_obj;
            // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_hr_evaluation));
            _data_hr_evaluation = callServicePostPerfEvaluEmp(_urlGetits_u_hr_evaluation_monthly, _data_hr_evaluation);
        }


        CreateDsits_u1_PerfEvaluEmpDaily();
        
        DataSet dsContacts = (DataSet)ViewState["vshr_u1_perf_evalu_employee_daily"];
        if (_data_hr_evaluation.hr_perf_evalu_emp_monthly_action != null)
        {
            int ic = 0;
            int igroupVIP = 0;
            int igroup1 = 0;
            int igroup2 = 0;
            foreach (var vdr in _data_hr_evaluation.hr_perf_evalu_emp_monthly_action)
            {
                ic++;

                if (vdr.group_code == "1")
                {
                    igroup1++;
                }
                else if (vdr.group_code == "2")
                {
                    igroup2++;
                }
                else
                {
                    igroupVIP++;
                }

                DataRow drContacts = dsContacts.Tables["dshr_u1_perf_evalu_employee_daily"].NewRow();

                drContacts["doccode"] = vdr.doccode;
                drContacts["zdocdate"] = vdr.zdocdate;
                drContacts["docdate"] = vdr.docdate;
                drContacts["u0_status"] = vdr.u0_status;
                drContacts["u0_emp_idx"] = vdr.u0_emp_idx;
                drContacts["u0_org_idx"] = vdr.u0_org_idx;
                drContacts["u0_rdept_idx"] = vdr.u0_rdept_idx;
                drContacts["u0_rpos_idx"] = vdr.u0_rpos_idx;
                drContacts["u0_rsec_idx"] = vdr.u0_rsec_idx;
                drContacts["remark"] = vdr.remark;
                drContacts["CEmpIDX"] = vdr.CEmpIDX;
                drContacts["CreateDate"] = vdr.CreateDate;
                drContacts["u0_emp_name_th"] = vdr.u0_emp_name_th;
                drContacts["u0_pos_name_th"] = vdr.u0_pos_name_th;
                drContacts["u0_sec_name_th"] = vdr.u0_sec_name_th;
                drContacts["u0_org_name_th"] = vdr.u0_org_name_th;
                drContacts["u1idx"] = vdr.u1idx;
                drContacts["u0idx"] = vdr.u0idx;
                drContacts["emp_idx"] = vdr.emp_idx;


                drContacts["work_experience"] = vdr.work_experience;
                drContacts["group_code"] = vdr.group_code;
                drContacts["level_code"] = vdr.level_code;
                drContacts["group_work"] = vdr.group_work;
                drContacts["late_mm"] = vdr.late_mm;
                drContacts["Back_before_mm"] = vdr.Back_before_mm;
                drContacts["missing_qty"] = vdr.missing_qty;
                drContacts["sick_Leave_qty_day"] = vdr.sick_Leave_qty_day;
                drContacts["Errandleave_day"] = vdr.Errandleave_day;
                drContacts["vacation_day"] = vdr.vacation_day;
                drContacts["workbreak_day"] = vdr.workbreak_day;
                drContacts["Maternityleave_day"] = vdr.Maternityleave_day;
                if (id == 0)
                {
                    drContacts["corp_kpi"] = vdr.m0corp_kpi;
                    drContacts["tkn_value"] = vdr.m0tkn_value;
                    drContacts["taokae_d"] = vdr.m0taokae_d;
                    drContacts["absence"] = vdr.m0absence;
                    // litDebug.Text = vdr.m0taokae_d.ToString();
                }
                else
                {
                    drContacts["corp_kpi"] = vdr.corp_kpi;
                    drContacts["tkn_value"] = vdr.tkn_value;
                    drContacts["taokae_d"] = vdr.taokae_d;
                    drContacts["absence"] = vdr.absence;
                }

                drContacts["punishment1"] = vdr.punishment1;
                drContacts["punishment2"] = vdr.punishment2;
                drContacts["ass_self_ownership_qty"] = vdr.ass_self_ownership_qty;
                drContacts["ass_self_improvement_continuously_qty"] = vdr.ass_self_improvement_continuously_qty;

                drContacts["UEmpIDX"] = vdr.UEmpIDX;
                drContacts["UpdateDate"] = vdr.UpdateDate;
                drContacts["u1_status"] = vdr.u1_status;
                drContacts["emp_code"] = vdr.emp_code;
                drContacts["rpos_idx"] = vdr.rpos_idx;
                drContacts["pos_name_th"] = vdr.pos_name_th;
                drContacts["rsec_idx"] = vdr.rsec_idx;
                drContacts["sec_name_th"] = vdr.sec_name_th;
                drContacts["rdept_idx"] = vdr.rdept_idx;
                drContacts["dept_name_th"] = vdr.dept_name_th;
                drContacts["org_idx"] = vdr.org_idx;
                drContacts["org_name_th"] = vdr.org_name_th;
                drContacts["emp_name_th"] = vdr.emp_name_th;

                drContacts["costcenter_idx"] = vdr.costcenter_idx;
                drContacts["costcenter_no"] = vdr.costcenter_no;
                drContacts["costcenter_name"] = vdr.costcenter_name;
                drContacts["emp_probation_date"] = vdr.emp_probation_date;
                drContacts["supervisor1_idx"] = vdr.supervisor1_idx;
                drContacts["supervisor2_idx"] = vdr.supervisor2_idx;


                drContacts["group_code"] = vdr.group_code;
                drContacts["level_code"] = vdr.level_code;
                drContacts["group_work"] = vdr.group_work;
                drContacts["late_mm"] = vdr.late_mm;
                drContacts["Back_before_mm"] = vdr.Back_before_mm;
                drContacts["missing_qty"] = vdr.missing_qty;
                drContacts["sick_Leave_qty_day"] = vdr.sick_Leave_qty_day;
                drContacts["Errandleave_day"] = vdr.Errandleave_day;
                drContacts["vacation_day"] = vdr.vacation_day;
                drContacts["workbreak_day"] = vdr.workbreak_day;
                drContacts["Maternityleave_day"] = vdr.Maternityleave_day;


                drContacts["ass_self_ownership_qty"] = vdr.ass_self_ownership_qty;
                drContacts["ass_self_improvement_continuously_qty"] = vdr.ass_self_improvement_continuously_qty;
                drContacts["ass_self_commitment_qty"] = vdr.ass_self_commitment_qty;
                drContacts["ass_self_innovation"] = vdr.ass_self_innovation;
                drContacts["ass_self_achieveing_result"] = vdr.ass_self_achieveing_result;
                drContacts["ass_self_relation"] = vdr.ass_self_relation;
                drContacts["ass_self_job_knowledge"] = vdr.ass_self_job_knowledge;
                drContacts["ass_self_art_communication"] = vdr.ass_self_art_communication;
                drContacts["ass_self_leadership"] = vdr.ass_self_leadership;
                drContacts["ass_self_decision_making_planning"] = vdr.ass_self_decision_making_planning;

                drContacts["leader_ownership_qty"] = vdr.leader_ownership_qty;
                drContacts["leader_improvement_continuously_qty"] = vdr.leader_improvement_continuously_qty;
                drContacts["leader_commitment_qty"] = vdr.leader_commitment_qty;
                drContacts["leader_innovation"] = vdr.leader_innovation;
                drContacts["leader_achieveing_result"] = vdr.leader_achieveing_result;
                drContacts["leader_relation"] = vdr.leader_relation;
                drContacts["leader_job_knowledge"] = vdr.leader_job_knowledge;
                drContacts["leader_art_communication"] = vdr.leader_art_communication;
                drContacts["leader_leadership"] = vdr.leader_leadership;
                drContacts["leader_decision_making_planning"] = vdr.leader_decision_making_planning;
                drContacts["description"] = vdr.description;
                drContacts["total_leader_tkn_value"] = vdr.total_leader_tkn_value;
                drContacts["total_leader_taokae_d"] = vdr.total_leader_taokae_d;
                drContacts["total_leader_ass_qty"] = vdr.total_leader_ass_qty;
                drContacts["total_deduction"] = vdr.total_deduction;
                drContacts["score_balance"] = vdr.score_balance;

                drContacts["total_leader_tkn_value_before"] = vdr.total_leader_tkn_value_before;
                drContacts["total_leader_taokae_d_before"] = vdr.total_leader_taokae_d_before;
                drContacts["total_leader_ass_qty_before"] = vdr.total_leader_ass_qty_before;
                drContacts["total_deduction_before"] = vdr.total_deduction_before;
                drContacts["score_balance_before"] = vdr.score_balance_before;

                drContacts["grade"] = vdr.grade;
                drContacts["grade_criteria"] = vdr.grade_criteria;
                drContacts["kpi"] = vdr.kpi;
                drContacts["kpi_before"] = vdr.kpi_before;

                dsContacts.Tables["dshr_u1_perf_evalu_employee_daily"].Rows.Add(drContacts);
            }
            _GridView.Columns[42].Visible = false;
            _GridView.Columns[43].Visible = false;
            _GridView.Columns[44].Visible = false;
            _GridView.Columns[45].Visible = false;
            _GridView.Columns[46].Visible = false;
            _GridView.Columns[47].Visible = false;
            _GridView.Columns[48].Visible = false;
            //42
            // litDebug.Text = igroupVIP.ToString();
            if ((igroupVIP > 0) || (igroup1 > 0))
            {
                _GridView.Columns[42].Visible = true;
                _GridView.Columns[43].Visible = true;
                _GridView.Columns[44].Visible = true;
                _GridView.Columns[45].Visible = true;
                _GridView.Columns[46].Visible = true;
                _GridView.Columns[47].Visible = true;
                _GridView.Columns[48].Visible = true;

            }
            else if (igroup2 > 0)
            {
                _GridView.Columns[43].Visible = true;
                _GridView.Columns[44].Visible = true;
                _GridView.Columns[45].Visible = true;
            }

        }


        ViewState["vshr_u1_perf_evalu_employee_daily"] = dsContacts;
        _func_dmu.zSetGridData(_GridView, dsContacts.Tables["dshr_u1_perf_evalu_employee_daily"]);


    }


    private void setCalculator()
    {
        setObject_Main();

        foreach (GridViewRow gridrow in _GridView.Rows)
        {

            TextBox txt_leader_ownership_qty = (TextBox)gridrow.FindControl("txt_leader_ownership_qty");
            TextBox txt_leader_improvement_continuously_qty = (TextBox)gridrow.FindControl("txt_leader_improvement_continuously_qty");
            TextBox txt_leader_commitment_qty = (TextBox)gridrow.FindControl("txt_leader_commitment_qty");

            TextBox txt_leader_innovation = (TextBox)gridrow.FindControl("txt_leader_innovation");
            TextBox txt_leader_achieveing_result = (TextBox)gridrow.FindControl("txt_leader_achieveing_result");
            TextBox txt_leader_relation = (TextBox)gridrow.FindControl("txt_leader_relation");
            TextBox txt_leader_job_knowledge = (TextBox)gridrow.FindControl("txt_leader_job_knowledge");
            TextBox txt_leader_art_communication = (TextBox)gridrow.FindControl("txt_leader_art_communication");
            TextBox txt_leader_leadership = (TextBox)gridrow.FindControl("txt_leader_leadership");
            TextBox txt_leader_decision_making_planning = (TextBox)gridrow.FindControl("txt_leader_decision_making_planning");

            Label lb_total_leader_tkn_value = (Label)gridrow.FindControl("lb_total_leader_tkn_value");
            Label lb_total_leader_taokae_d = (Label)gridrow.FindControl("lb_total_leader_taokae_d");

            Label lb_tkn_value = (Label)gridrow.FindControl("lb_tkn_value");
            Label lb_taokae_d = (Label)gridrow.FindControl("lb_taokae_d");
            Label lb_group_code = (Label)gridrow.FindControl("lb_group_code");
            Label lb_total_leader_ass_qty = (Label)gridrow.FindControl("lb_total_leader_ass_qty");
            Label lb_sick_Leave_qty_day = (Label)gridrow.FindControl("lb_sick_Leave_qty_day");
            Label lb_Errandleave_day = (Label)gridrow.FindControl("lb_Errandleave_day");
            Label lb_missing_qty = (Label)gridrow.FindControl("lb_missing_qty");
            Label lb_punishment1 = (Label)gridrow.FindControl("lb_punishment1");
            Label lb_punishment2 = (Label)gridrow.FindControl("lb_punishment2");
            Label lb_late_mm = (Label)gridrow.FindControl("lb_late_mm");
            Label lb_Back_before_mm = (Label)gridrow.FindControl("lb_Back_before_mm");
            Label lb_total_deduction = (Label)gridrow.FindControl("lb_total_deduction");
            Label lb_absence = (Label)gridrow.FindControl("lb_absence");
            Label lb_grade = (Label)gridrow.FindControl("lb_grade");
            Label lb_description = (Label)gridrow.FindControl("lb_description");
            Label lb_corp_kpi = (Label)gridrow.FindControl("lb_corp_kpi");
            Label lb_kpi = (Label)gridrow.FindControl("lb_kpi");
            Label lb_vacation_day = (Label)gridrow.FindControl("lb_vacation_day");
            Label lb_score_balance = (Label)gridrow.FindControl("lb_score_balance");

            Label lb_score_balance_before = (Label)gridrow.FindControl("lb_score_balance_before");
            Label lb_total_leader_tkn_value_before = (Label)gridrow.FindControl("lb_total_leader_tkn_value_before");
            Label lb_total_leader_taokae_d_before = (Label)gridrow.FindControl("lb_total_leader_taokae_d_before");
            Label lb_total_leader_ass_qty_before = (Label)gridrow.FindControl("lb_total_leader_ass_qty_before");


            int itotal = 0;
            itotal = _func_dmu.zStringToInt(txt_leader_ownership_qty.Text) +
                    _func_dmu.zStringToInt(txt_leader_improvement_continuously_qty.Text) +
                    _func_dmu.zStringToInt(txt_leader_commitment_qty.Text);
            decimal dtotal = 0;
            decimal dtotal1 = itotal;
            decimal dc2 = 0;
            int itkn_value = _func_dmu.zStringToInt(lb_tkn_value.Text);
            if (itkn_value == 15)
            {
                dtotal = itotal;
            }
            else if (itkn_value == 20)
            {
                dc2 = 3;
                dtotal = (itotal * 4) / dc2;
            }
            else if (itkn_value == 40)
            {
                dc2 = 3;
                dtotal = (itotal * 8) / dc2;
            }

            lb_total_leader_tkn_value_before.Text = getformatfloat(dtotal.ToString(), 4);

            dtotal = Convert.ToInt32(dtotal);
            lb_total_leader_tkn_value.Text = getformatfloat(dtotal.ToString(), 0);
            


            int itaokae_d = _func_dmu.zStringToInt(lb_taokae_d.Text);
            dtotal = 0;
            itotal = 0;
            if (lb_group_code.Text == "2")
            {
                itotal = _func_dmu.zStringToInt(txt_leader_achieveing_result.Text) +
                         _func_dmu.zStringToInt(txt_leader_relation.Text) +
                         _func_dmu.zStringToInt(txt_leader_job_knowledge.Text);

            }
            else
            {

                itotal = _func_dmu.zStringToInt(txt_leader_innovation.Text) +
                         _func_dmu.zStringToInt(txt_leader_achieveing_result.Text) +
                         _func_dmu.zStringToInt(txt_leader_relation.Text) +
                         _func_dmu.zStringToInt(txt_leader_job_knowledge.Text) +
                         _func_dmu.zStringToInt(txt_leader_art_communication.Text) +
                         _func_dmu.zStringToInt(txt_leader_leadership.Text) +
                         _func_dmu.zStringToInt(txt_leader_decision_making_planning.Text);
            }
            if (itaokae_d == 20)
            {

                if (lb_group_code.Text == "2") // 3 ช่อง
                {
                    dtotal = (itotal * 4) / 3;

                }
                else // 7 ช่อง
                {
                    dtotal = (itotal * 4) / 7;
                }
            }
            else if (itaokae_d == 30)
            {
                if (lb_group_code.Text == "2") // 3 ช่อง
                {
                    dtotal = (itotal * 6) / 3;

                }
                else // 7 ช่อง
                {
                    dtotal = (itotal * 6) / 7;
                }
            }
            else if (itaokae_d == 40)
            {
                if (lb_group_code.Text == "2") // 3 ช่อง
                {
                    dtotal = (itotal * 8) / 3;

                }
                else // 7 ช่อง
                {
                    dtotal = (itotal * 8) / 7;
                }
            }
            else if (itaokae_d == 55)
            {
                if (lb_group_code.Text == "2") // 3 ช่อง
                {
                    dtotal = (itotal * 11) / 3;

                }
                else // 7 ช่อง
                {
                    dtotal = (itotal * 11) / 7;
                }
            }
            else if (itaokae_d == 60)
            {
                if (lb_group_code.Text == "2") // 3 ช่อง
                {
                    // dtotal = (itotal * 12) / 3;
                    dtotal = (itotal * 4);
                }
                else // 7 ช่อง
                {
                    dtotal = (itotal * 12) / 7;
                }
            }
            else if (itaokae_d == 65)
            {
                if (lb_group_code.Text == "2") // 3 ช่อง
                {
                    dtotal = (itotal * 13) / 3;

                }
                else // 7 ช่อง
                {
                    dtotal = (itotal * 13) / 7;
                }
            }
            else if (itaokae_d == 70)
            {
                if (lb_group_code.Text == "2") // 3 ช่อง
                {
                    dtotal = (itotal * 14) / 3;

                }
                else // 7 ช่อง
                {
                    dtotal = (itotal * 14) / 7;
                }
            }
            else if (itaokae_d == 80)
            {
                if (lb_group_code.Text == "2") // 3 ช่อง
                {
                    dtotal = (itotal * 16) / 3;

                }
                else // 7 ช่อง
                {
                    dtotal = (itotal * 16) / 7;
                }
            }
            else
            {
                dtotal = 0;
                itotal = 0;
            }

            lb_total_leader_taokae_d_before.Text = getformatfloat(dtotal.ToString(), 4);
            dtotal = Convert.ToInt32(dtotal);
            lb_total_leader_taokae_d.Text = getformatfloat(dtotal.ToString(), 0);


            if (_func_dmu.zStringToInt(lb_absence.Text) > 0)
            {
                int itotal_deduction = 0;
                itotal = 0;
                //ลาป่วยเกิน 10 วัน หักวันละ 1 คะแนน
                if (Convert.ToInt32(_func_dmu.zStringToDecimal(lb_sick_Leave_qty_day.Text)) > 10)
                {
                    itotal = Convert.ToInt32(_func_dmu.zStringToDecimal(lb_sick_Leave_qty_day.Text)) - 10;
                    itotal_deduction = itotal_deduction + itotal;
                }

                //ลากิจเกิน 3 วัน หักวันละ 1 คะแนน
                if (Convert.ToInt32(_func_dmu.zStringToDecimal(lb_Errandleave_day.Text)) > 3)
                {
                    itotal = Convert.ToInt32(_func_dmu.zStringToDecimal(lb_Errandleave_day.Text)) - 3;
                    itotal_deduction = itotal_deduction + itotal;
                }
                //	ลากิจ ทดลองงาน(วัน) 3 วัน หักวันละ 1 คะแนน
                if (Convert.ToInt32(_func_dmu.zStringToDecimal(lb_vacation_day.Text)) > 3)
                {
                    itotal = Convert.ToInt32(_func_dmu.zStringToDecimal(lb_vacation_day.Text)) - 3;
                    itotal_deduction = itotal_deduction + itotal;
                }
                //ขาดงาน หักวันละ 5 คะแนน
                if (Convert.ToInt32(_func_dmu.zStringToDecimal(lb_missing_qty.Text)) > 0)
                {
                    itotal = Convert.ToInt32(_func_dmu.zStringToDecimal(lb_missing_qty.Text)) * 5;
                    itotal_deduction = itotal_deduction + itotal;
                }
                //เตือนลายลักษณ์อักษร หักวันละ 10 คะแนน
                if (Convert.ToInt32(_func_dmu.zStringToDecimal(lb_punishment1.Text)) > 0)
                {
                    itotal = Convert.ToInt32(_func_dmu.zStringToDecimal(lb_punishment1.Text)) * 10;
                    itotal_deduction = itotal_deduction + itotal;
                }
                //พักงาน หักวันละ 10 คะแนน
                if (Convert.ToInt32(_func_dmu.zStringToDecimal(lb_punishment2.Text)) > 0)
                {
                    itotal = Convert.ToInt32(_func_dmu.zStringToDecimal(lb_punishment2.Text)) * 10;
                    itotal_deduction = itotal_deduction + itotal;
                }
                //สาย(นาที)+กลับก่อน(นาที) 30 นาที หักว 1 คะแนน
                itotal = 0;
                itotal = Convert.ToInt32(_func_dmu.zStringToDecimal(lb_late_mm.Text)) +
                         Convert.ToInt32(_func_dmu.zStringToDecimal(lb_Back_before_mm.Text));
                
                if (itotal >= 30)
                {
                    itotal = itotal / 30;
                    itotal_deduction = itotal_deduction + itotal;
                }


                lb_total_deduction.Text = getformatfloat(itotal_deduction.ToString(), 0);
                //หาแบบตัดเศษ
                itotal = itotal_deduction;
                int iabsence = _func_dmu.zStringToInt(lb_absence.Text);
                if (iabsence == 10)
                {
                    //สูตรคำนวนหา 10 %
                    itotal = Convert.ToInt32(100 - itotal);
                    itotal = itotal / 10;
                    if (itotal < 0)
                    {
                        itotal = 0;
                    }

                }
                else if (iabsence == 20)
                {

                    //สูตรคำนวนหา 20 %
                    itotal = Convert.ToInt32(100 - itotal);
                    itotal = (itotal * 2) / 10;
                    if (itotal < 0)
                    {
                        itotal = 0;
                    }

                }
                else if (iabsence == 40)
                {

                    //สูตรคำนวนหา 40 %
                    itotal = Convert.ToInt32(100 - itotal);
                    itotal = (itotal * 2) / 5;
                    if (itotal < 0)
                    {
                        itotal = 0;
                    }

                }
                else
                {
                    itotal = 0;
                }
                lb_score_balance.Text = getformatfloat(itotal.ToString(), 0);


                //หาแบบเอาเศษ
                itotal = itotal_deduction;
                iabsence = _func_dmu.zStringToInt(lb_absence.Text);
                decimal dctotal = 0, dc1 = 0;
                if (iabsence == 10)
                {
                    //สูตรคำนวนหา 10 %
                    itotal = Convert.ToInt32(100 - itotal);
                    dc1 = 10;
                    dctotal = itotal / dc1;
                    if (dctotal < 0)
                    {
                        dctotal = 0;
                    }

                }
                else if (iabsence == 20)
                {

                    //สูตรคำนวนหา 20 %
                    itotal = Convert.ToInt32(100 - itotal);
                    dc1 = 10;
                    dctotal = (itotal * 2) / dc1;
                    if (dctotal < 0)
                    {
                        dctotal = 0;
                    }

                }
                else if (iabsence == 40)
                {

                    //สูตรคำนวนหา 40 %
                    itotal = Convert.ToInt32(100 - itotal);
                    dc1 = 5;
                    dctotal = (itotal * 2) / dc1;
                    if (dctotal < 0)
                    {
                        dctotal = 0;
                    }

                }
                else
                {
                    dctotal = 0;
                }
                lb_score_balance_before.Text = getformatfloat(dctotal.ToString(), 4);


            }
            else
            {
                lb_score_balance.Text = "0";
            }
            //หารตัดเศษ
            lb_total_leader_ass_qty.Text = getformatfloat((_func_dmu.zStringToInt(lb_total_leader_tkn_value.Text) +
                                           _func_dmu.zStringToInt(lb_total_leader_taokae_d.Text)).ToString(), 0);
            //หารเอาเศษ
            lb_total_leader_ass_qty_before.Text = getformatfloat((_func_dmu.zStringToDecimal(lb_total_leader_tkn_value_before.Text) +
                                           _func_dmu.zStringToDecimal(lb_total_leader_taokae_d_before.Text)).ToString(), 4);

            if (_func_dmu.zStringToDecimal(lb_corp_kpi.Text) > 0)
            {
                //หารเอาเศษ
                lb_total_leader_ass_qty_before.Text = getformatfloat((_func_dmu.zStringToDecimal(lb_total_leader_ass_qty_before.Text) +
                                                       _func_dmu.zStringToDecimal(lb_kpi.Text)).ToString(), 4);
                //หารตัดเศษ

                itotal = _func_dmu.zStringToInt(lb_total_leader_ass_qty.Text) +
                         _func_dmu.zStringToInt(lb_kpi.Text);
                lb_total_leader_ass_qty.Text = itotal.ToString();
            }

            if (_func_dmu.zStringToInt(lb_absence.Text) > 0)
            {

                //หารเอาเศษ
                lb_total_leader_ass_qty_before.Text = getformatfloat((_func_dmu.zStringToDecimal(lb_total_leader_ass_qty_before.Text) +
                                                       _func_dmu.zStringToDecimal(lb_score_balance_before.Text)).ToString(), 4);
                //หารตัดเศษ
                itotal = _func_dmu.zStringToInt(lb_total_leader_ass_qty.Text) +
                         _func_dmu.zStringToInt(lb_score_balance.Text);
                lb_total_leader_ass_qty.Text = itotal.ToString();
            }
            itotal = _func_dmu.zStringToInt(lb_total_leader_ass_qty.Text);


            string _sadmonish_speech = "", _swork_break_qty = "";
            int ickeck = 0;
            if (lb_group_code.Text == "2")
            {
                if (
                   (_func_dmu.zStringToInt(txt_leader_ownership_qty.Text) > 0) &&
                   (_func_dmu.zStringToInt(txt_leader_improvement_continuously_qty.Text) > 0) &&
                   (_func_dmu.zStringToInt(txt_leader_commitment_qty.Text) > 0) &&

                   (_func_dmu.zStringToInt(txt_leader_achieveing_result.Text) > 0) &&
                   (_func_dmu.zStringToInt(txt_leader_relation.Text) > 0) &&
                   (_func_dmu.zStringToInt(txt_leader_job_knowledge.Text) > 0)

               )
                {
                    ickeck = 1;


                }

            }
            else
            {
                if (
                   (_func_dmu.zStringToInt(txt_leader_ownership_qty.Text) > 0) &&
                   (_func_dmu.zStringToInt(txt_leader_improvement_continuously_qty.Text) > 0) &&
                   (_func_dmu.zStringToInt(txt_leader_commitment_qty.Text) > 0) &&

                   (_func_dmu.zStringToInt(txt_leader_innovation.Text) > 0) &&
                   (_func_dmu.zStringToInt(txt_leader_achieveing_result.Text) > 0) &&
                   (_func_dmu.zStringToInt(txt_leader_relation.Text) > 0) &&
                   (_func_dmu.zStringToInt(txt_leader_job_knowledge.Text) > 0) &&
                   (_func_dmu.zStringToInt(txt_leader_art_communication.Text) > 0) &&
                   (_func_dmu.zStringToInt(txt_leader_leadership.Text) > 0) &&
                   (_func_dmu.zStringToInt(txt_leader_decision_making_planning.Text) > 0)
               )
                {
                    ickeck = 1;


                }
            }

            if (
                   ickeck == 1
               )
            {
                lb_grade.Text = zGradeCalculat(itotal);


            }
            else
            {
                lb_grade.Text = "";
            }

            if (Convert.ToInt32(_func_dmu.zStringToDecimal(lb_punishment1.Text)) > 0)
            {
                _sadmonish_speech = "*ตักเตือนด้วยวาจา";
            }

            if (Convert.ToInt32(_func_dmu.zStringToDecimal(lb_punishment2.Text)) > 0)
            {
                _swork_break_qty = "*ไม่พิจารณาปรับเงินเดือน";
            }
            else
            {

                if ((lb_grade.Text == "D") || (lb_grade.Text == "E"))
                {
                    _swork_break_qty = "*ไม่พิจารณาปรับเงินเดือน";
                }
            }
            lb_description.Text = _sadmonish_speech + _swork_break_qty;
            

        }

        _GridView.SelectedIndex = 0;

    }
    private Boolean setCheckError(int _iError)
    {
        setObject_Main();
        Boolean _Boolean = false;

        if (_GridView.Rows.Count == 0)
        {
            showMsAlert("ไม่มีข้อมูลพนักงาน !");
            _GridView.Focus();
            return true;
        }
        else
        {

            foreach (GridViewRow gridrow in _GridView.Rows)
            {
                Label lb_emp_code = (Label)gridrow.FindControl("lb_emp_code");
                Label lb_emp_name_th = (Label)gridrow.FindControl("lb_emp_name_th");
                Label lb_group_code = (Label)gridrow.FindControl("lb_group_code");

                TextBox txt_leader_ownership_qty = (TextBox)gridrow.FindControl("txt_leader_ownership_qty");
                TextBox txt_leader_improvement_continuously_qty = (TextBox)gridrow.FindControl("txt_leader_improvement_continuously_qty");
                TextBox txt_leader_commitment_qty = (TextBox)gridrow.FindControl("txt_leader_commitment_qty");

                TextBox txt_leader_innovation = (TextBox)gridrow.FindControl("txt_leader_innovation");
                TextBox txt_leader_achieveing_result = (TextBox)gridrow.FindControl("txt_leader_achieveing_result");
                TextBox txt_leader_relation = (TextBox)gridrow.FindControl("txt_leader_relation");
                TextBox txt_leader_job_knowledge = (TextBox)gridrow.FindControl("txt_leader_job_knowledge");
                TextBox txt_leader_art_communication = (TextBox)gridrow.FindControl("txt_leader_art_communication");
                TextBox txt_leader_leadership = (TextBox)gridrow.FindControl("txt_leader_leadership");
                TextBox txt_leader_decision_making_planning = (TextBox)gridrow.FindControl("txt_leader_decision_making_planning");
                Label lb_total_leader_ass_qty = (Label)gridrow.FindControl("lb_total_leader_ass_qty");

                string _sEmp_Name = "";
                _sEmp_Name = lb_emp_code.Text + " : " + lb_emp_name_th.Text;
                string _sMs1 = "เฉพาะจำนวนเต็มเท่านั้น !";
                string _sMs2 = "ตั้งแต่ 1 - 5 เท่านั้น !";
                string _sMs2_cal = "ตั้งแต่ 0 - 5 เท่านั้น !";

                int _bl = 0;
                _bl = TKNCoreValue(txt_leader_ownership_qty.Text, _iError);
                if (_bl == 1)
                {
                    showMsAlert("กรุณากรอกคะแนน มุ่งมั่น บรรลุผล จนสำเร็จ เฉพาะจำนวนเต็มเท่านั้น !");
                    txt_leader_ownership_qty.Focus();
                    return true;
                }
                else if (_bl == 2)
                {
                    if (_iError == 1) // 1 save
                    {
                        showMsAlert("กรุณากรอกคะแนน มุ่งมั่น บรรลุผล จนสำเร็จ " + _sMs2);
                    }
                    else // 2 cal
                    {
                        showMsAlert("กรุณากรอกคะแนน มุ่งมั่น บรรลุผล จนสำเร็จ " + _sMs2_cal);
                    }
                    txt_leader_ownership_qty.Focus();
                    return true;
                }
                _bl = TKNCoreValue(txt_leader_improvement_continuously_qty.Text, _iError);
                if (_bl == 1)
                {
                    showMsAlert("กรุณากรอกคะแนน เชี่ยวชาญ ต่อยอด ส่งมอบความพึงพอใจ เฉพาะจำนวนเต็มเท่านั้น !");
                    txt_leader_improvement_continuously_qty.Focus();
                    return true;
                }
                else if (_bl == 2)
                {
                    if (_iError == 1) // 1 save
                    {
                        showMsAlert("กรุณากรอกคะแนน เชี่ยวชาญ ต่อยอด ส่งมอบความพึงพอใจ " + _sMs2);
                    }
                    else // 2 cal
                    {
                        showMsAlert("กรุณากรอกคะแนน เชี่ยวชาญ ต่อยอด ส่งมอบความพึงพอใจ " + _sMs2_cal);
                    }
                    txt_leader_improvement_continuously_qty.Focus();
                    return true;
                }
                _bl = TKNCoreValue(txt_leader_commitment_qty.Text, _iError);
                if (_bl == 1)
                {
                    showMsAlert("กรุณากรอกคะแนน ตั้งใจ รับผิดชอบ ทุ่มเท เฉพาะจำนวนเต็มเท่านั้น !");
                    txt_leader_commitment_qty.Focus();
                    return true;
                }
                else if (_bl == 2)
                {
                    if (_iError == 1) // 1 save
                    {
                        showMsAlert("กรุณากรอกคะแนน ตั้งใจ รับผิดชอบ ทุ่มเท " + _sMs2);
                    }
                    else // 2 cal
                    {
                        showMsAlert("กรุณากรอกคะแนน ตั้งใจ รับผิดชอบ ทุ่มเท " + _sMs2_cal);
                    }
                    txt_leader_commitment_qty.Focus();
                    return true;
                }
                string _leader_innovation1 = "กรุณากรอกคะแนน INNOVATION เฉพาะจำนวนเต็มเท่านั้น !";
                string _leader_achieveing_result1 = "กรุณากรอกคะแนน ACHIEVEING RESULT เฉพาะจำนวนเต็มเท่านั้น !";
                string _leader_relation1 = "กรุณากรอกคะแนน RELATION เฉพาะจำนวนเต็มเท่านั้น !";
                string _leader_job_knowledge1 = "กรุณากรอกคะแนน JOB KNOWLEDGE เฉพาะจำนวนเต็มเท่านั้น !";
                string _leader_art_communication1 = "กรุณากรอกคะแนน ART OF COMMUNICATION เฉพาะจำนวนเต็มเท่านั้น !";
                string _leader_leadership1 = "กรุณากรอกคะแนน LEADERSHIP เฉพาะจำนวนเต็มเท่านั้น !";
                string _leader_decision_making_planning1 = "กรุณากรอกคะแนน DECISION MAKING & PLANNING เฉพาะจำนวนเต็มเท่านั้น !";

                string _leader_innovation2 = "กรุณากรอกคะแนน INNOVATION ";
                string _leader_achieveing_result2 = "กรุณากรอกคะแนน ACHIEVEING RESULT ";
                string _leader_relation2 = "กรุณากรอกคะแนน RELATION ";
                string _leader_job_knowledge2 = "กรุณากรอกคะแนน JOB KNOWLEDGE ";
                string _leader_art_communication2 = "กรุณากรอกคะแนน ART OF COMMUNICATION ";
                string _leader_leadership2 = "กรุณากรอกคะแนน LEADERSHIP ";
                string _leader_decision_making_planning2 = "กรุณากรอกคะแนน DECISION MAKING & PLANNING ";

                if (lb_group_code.Text == "2")
                {

                    _bl = TKNCoreValue(txt_leader_achieveing_result.Text, _iError);
                    if (_bl == 1)
                    {
                        showMsAlert(_leader_achieveing_result1);
                        txt_leader_achieveing_result.Focus();
                        return true;
                    }
                    else if (_bl == 2)
                    {

                        if (_iError == 1) // 1 save
                        {
                            showMsAlert(_leader_achieveing_result2 + _sMs2);
                        }
                        else // 2 cal
                        {
                            showMsAlert(_leader_achieveing_result2 + _sMs2_cal);
                        }
                        txt_leader_achieveing_result.Focus();
                        return true;
                    }

                    _bl = TKNCoreValue(txt_leader_relation.Text, _iError);
                    if (_bl == 1)
                    {
                        showMsAlert(_leader_relation1);
                        txt_leader_relation.Focus();
                        return true;
                    }
                    else if (_bl == 2)
                    {

                        if (_iError == 1) // 1 save
                        {
                            showMsAlert(_leader_relation2 + _sMs2);
                        }
                        else // 2 cal
                        {
                            showMsAlert(_leader_relation2 + _sMs2_cal);
                        }
                        txt_leader_relation.Focus();
                        return true;
                    }

                    _bl = TKNCoreValue(txt_leader_job_knowledge.Text, _iError);
                    if (_bl == 1)
                    {
                        showMsAlert(_leader_job_knowledge1);
                        txt_leader_job_knowledge.Focus();
                        return true;
                    }
                    else if (_bl == 2)
                    {

                        if (_iError == 1) // 1 save
                        {
                            showMsAlert(_leader_job_knowledge2 + _sMs2);
                        }
                        else // 2 cal
                        {
                            showMsAlert(_leader_job_knowledge2 + _sMs2_cal);
                        }
                        txt_leader_job_knowledge.Focus();
                        return true;
                    }

                }
                else
                {

                    _bl = TKNCoreValue(txt_leader_innovation.Text, _iError);
                    if (_bl == 1)
                    {
                        showMsAlert(_leader_innovation1);
                        txt_leader_innovation.Focus();
                        return true;
                    }
                    else if (_bl == 2)
                    {

                        if (_iError == 1) // 1 save
                        {
                            showMsAlert(_leader_innovation2 + _sMs2);
                        }
                        else // 2 cal
                        {
                            showMsAlert(_leader_innovation2 + _sMs2_cal);
                        }
                        txt_leader_innovation.Focus();
                        return true;
                    }

                    _bl = TKNCoreValue(txt_leader_achieveing_result.Text, _iError);
                    if (_bl == 1)
                    {
                        showMsAlert(_leader_achieveing_result1);
                        txt_leader_achieveing_result.Focus();
                        return true;
                    }
                    else if (_bl == 2)
                    {

                        if (_iError == 1) // 1 save
                        {
                            showMsAlert(_leader_achieveing_result2 + _sMs2);
                        }
                        else // 2 cal
                        {
                            showMsAlert(_leader_achieveing_result2 + _sMs2_cal);
                        }
                        txt_leader_achieveing_result.Focus();
                        return true;
                    }

                    _bl = TKNCoreValue(txt_leader_relation.Text, _iError);
                    if (_bl == 1)
                    {
                        showMsAlert(_leader_relation1);
                        txt_leader_relation.Focus();
                        return true;
                    }
                    else if (_bl == 2)
                    {

                        if (_iError == 1) // 1 save
                        {
                            showMsAlert(_leader_relation2 + _sMs2);
                        }
                        else // 2 cal
                        {
                            showMsAlert(_leader_relation2 + _sMs2_cal);
                        }
                        txt_leader_relation.Focus();
                        return true;
                    }

                    _bl = TKNCoreValue(txt_leader_job_knowledge.Text, _iError);
                    if (_bl == 1)
                    {
                        showMsAlert(_leader_job_knowledge1);
                        txt_leader_job_knowledge.Focus();
                        return true;
                    }
                    else if (_bl == 2)
                    {

                        if (_iError == 1) // 1 save
                        {
                            showMsAlert(_leader_job_knowledge2 + _sMs2);
                        }
                        else // 2 cal
                        {
                            showMsAlert(_leader_job_knowledge2 + _sMs2_cal);
                        }
                        txt_leader_job_knowledge.Focus();
                        return true;
                    }

                    _bl = TKNCoreValue(txt_leader_art_communication.Text, _iError);
                    if (_bl == 1)
                    {
                        showMsAlert(_leader_art_communication1);
                        txt_leader_art_communication.Focus();
                        return true;
                    }
                    else if (_bl == 2)
                    {

                        if (_iError == 1) // 1 save
                        {
                            showMsAlert(_leader_art_communication2 + _sMs2);
                        }
                        else // 2 cal
                        {
                            showMsAlert(_leader_art_communication2 + _sMs2_cal);
                        }
                        txt_leader_art_communication.Focus();
                        return true;
                    }

                    _bl = TKNCoreValue(txt_leader_leadership.Text, _iError);
                    if (_bl == 1)
                    {
                        showMsAlert(_leader_leadership1);
                        txt_leader_leadership.Focus();
                        return true;
                    }
                    else if (_bl == 2)
                    {

                        if (_iError == 1) // 1 save
                        {
                            showMsAlert(_leader_leadership2 + _sMs2);
                        }
                        else // 2 cal
                        {
                            showMsAlert(_leader_leadership2 + _sMs2_cal);
                        }
                        txt_leader_leadership.Focus();
                        return true;
                    }

                    _bl = TKNCoreValue(txt_leader_decision_making_planning.Text, _iError);
                    if (_bl == 1)
                    {
                        showMsAlert(_leader_decision_making_planning1);
                        txt_leader_decision_making_planning.Focus();
                        return true;
                    }
                    else if (_bl == 2)
                    {

                        if (_iError == 1) // 1 save
                        {
                            showMsAlert(_leader_decision_making_planning2 + _sMs2);
                        }
                        else // 2 cal
                        {
                            showMsAlert(_leader_decision_making_planning2 + _sMs2_cal);
                        }
                        txt_leader_decision_making_planning.Focus();
                        return true;
                    }
                }

            }

        }
        _GridView.SelectedIndex = 0;

        return _Boolean;

    }

    private int TKNCoreValue(string _qty, int _iError)
    {
        int ierror = 0;

        ierror = 0;
        try
        {
            _func_dmu.zStringToInt(_qty);
        }
        catch
        {
            ierror = 1;
        }
        if (ierror == 1)
        {

        }
        else
        {

            if (_iError == 1)
            {
                if (_func_dmu.zStringToInt(_qty) > 5)
                {
                    ierror = 2;

                }
                else if (_func_dmu.zStringToInt(_qty) <= 0)
                {
                    ierror = 2;

                }
            }
            else
            {
                if (_func_dmu.zStringToInt(_qty) > 5)
                {
                    ierror = 2;

                }
                else if (_func_dmu.zStringToInt(_qty) < 0)
                {
                    ierror = 2;

                }
            }

        }

        return ierror;

    }
    public void showMsAlert(string Str)
    {
        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('" + Str + "');", true);
    }


    private string zGradeCriteriaCalculat(int _ownership, int _improvement_continuously, int _commitment
        , int _total_deduction, int _score_balance, int _score_assessment
        , int _missing, int _sick_Leave, int _call_back, int _warning_notice, int _work_break)
    {
        string _rtGrade = "";

        //พนักงานที่จะได้เกรด A  คะแนนทุกช่องจะต้องไม่ต่ำกว่า 9  และถูกหักคะแนน ไม่เกิน 5 คะแนน
        if ((_ownership >= 9) &&
            (_improvement_continuously >= 9) &&
            (_commitment >= 9) &&
            (_total_deduction <= 5))
        {
            _rtGrade = "A";
        }
        //พนักงานที่จะได้เกรด B  คะแนนทุกช่องจะต้องไม่ต่ำกว่า 8  และถูกหักคะแนน ไม่เกิน 20 คะแนน
        else if ((_ownership >= 8) &&
            (_improvement_continuously >= 8) &&
            (_commitment >= 8) &&
            (_total_deduction <= 20))
        {
            _rtGrade = "B";
        }
        //พนักงานที่มีคะแนนประเมินในแต่ละช่องต่ำกว่า 7 คะแนน
        else if ((_ownership < 7) ||
            (_improvement_continuously < 7) ||
            (_commitment < 7))
        {
            _rtGrade = "C";
        }
        //พนักงานที่ลาป่วยเกิน 10 วัน
        else if (_sick_Leave > 10)
        {
            _rtGrade = "C";
        }
        //พนักงานที่มาสายเกิน 100 นาที
        else if (_call_back > 100)
        {
            _rtGrade = "C";
        }
        //พนักงานที่มีคะแนนประเมินในแต่ละช่องต่ำกว่า 6 คะแนน
        else if ((_ownership < 6) ||
            (_improvement_continuously < 6) ||
            (_commitment < 6))
        {
            _rtGrade = "D";
        }
        //พนักงานที่มียอดหักรวมเกิน 50 คะแนนขึ้นไป  
        else if ((_score_assessment >= 95) && (_score_assessment < 50))
        {
            _rtGrade = "D";
        }
        else
        {
            _rtGrade = "E";
        }


        return _rtGrade;
    }

    private int zSave(int id, int _iu0_status)
    {
        setObject_Main();
        int itemObj = 0;

        hr_perf_evalu_emp_monthly[] objcourse1 = new hr_perf_evalu_emp_monthly[_GridView.Rows.Count];

        foreach (GridViewRow gridrow in _GridView.Rows)
        {

            Label lb_emp_idx = (Label)gridrow.FindControl("lb_emp_idx");
            Label lb_work_experience = (Label)gridrow.FindControl("lb_work_experience");

            Label lb_u0idx = (Label)gridrow.FindControl("lb_u0idx");
            Label lb_u1idx = (Label)gridrow.FindControl("lb_u1idx");

            Label lb_supervisor1_idx = (Label)gridrow.FindControl("lb_supervisor1_idx");
            Label lb_supervisor2_idx = (Label)gridrow.FindControl("lb_supervisor2_idx");

            Label lb_leader_achieveing_result = (Label)gridrow.FindControl("lb_leader_achieveing_result");
            Label lb_total_leader_tkn_value = (Label)gridrow.FindControl("lb_total_leader_tkn_value");
            Label lb_total_leader_taokae_d = (Label)gridrow.FindControl("lb_total_leader_taokae_d");

            // Label lb_grade_criteria = (Label)gridrow.FindControl("lb_grade_criteria");

            Label lb_group_code = (Label)gridrow.FindControl("lb_group_code");
            Label lb_level_code = (Label)gridrow.FindControl("lb_level_code");
            Label lb_group_work = (Label)gridrow.FindControl("lb_group_work");
            Label lb_late_mm = (Label)gridrow.FindControl("lb_late_mm");
            Label lb_Back_before_mm = (Label)gridrow.FindControl("lb_Back_before_mm");

            Label lb_missing_qty = (Label)gridrow.FindControl("lb_missing_qty");
            Label lb_sick_Leave_qty_day = (Label)gridrow.FindControl("lb_sick_Leave_qty_day");
            Label lb_Errandleave_day = (Label)gridrow.FindControl("lb_Errandleave_day");
            Label lb_vacation_day = (Label)gridrow.FindControl("lb_vacation_day");
            Label lb_workbreak_day = (Label)gridrow.FindControl("lb_workbreak_day");
            Label lb_Maternityleave_day = (Label)gridrow.FindControl("lb_Maternityleave_day");

            Label lb_corp_kpi = (Label)gridrow.FindControl("lb_corp_kpi");
            Label lb_tkn_value = (Label)gridrow.FindControl("lb_tkn_value");
            Label lb_taokae_d = (Label)gridrow.FindControl("lb_taokae_d");
            Label lb_absence = (Label)gridrow.FindControl("lb_absence");
            Label lb_punishment1 = (Label)gridrow.FindControl("lb_punishment1");
            Label lb_punishment2 = (Label)gridrow.FindControl("lb_punishment2");

            Label txt_ass_self_ownership_qty = (Label)gridrow.FindControl("txt_ass_self_ownership_qty");
            Label txt_ass_self_improvement_continuously_qty = (Label)gridrow.FindControl("txt_ass_self_improvement_continuously_qty");
            Label txt_ass_self_commitment_qty = (Label)gridrow.FindControl("txt_ass_self_commitment_qty");
            Label txt_ass_self_innovation = (Label)gridrow.FindControl("txt_ass_self_innovation");
            Label txt_ass_self_achieveing_result = (Label)gridrow.FindControl("txt_ass_self_achieveing_result");
            Label txt_ass_self_relation = (Label)gridrow.FindControl("txt_ass_self_relation");
            Label txt_ass_self_job_knowledge = (Label)gridrow.FindControl("txt_ass_self_job_knowledge");
            Label txt_ass_self_art_communication = (Label)gridrow.FindControl("txt_ass_self_art_communication");
            Label txt_ass_self_leadership = (Label)gridrow.FindControl("txt_ass_self_leadership");
            Label txt_ass_self_decision_making_planning = (Label)gridrow.FindControl("txt_ass_self_decision_making_planning");


            TextBox txt_leader_ownership_qty = (TextBox)gridrow.FindControl("txt_leader_ownership_qty");
            TextBox txt_leader_improvement_continuously_qty = (TextBox)gridrow.FindControl("txt_leader_improvement_continuously_qty");
            TextBox txt_leader_commitment_qty = (TextBox)gridrow.FindControl("txt_leader_commitment_qty");
            TextBox txt_leader_innovation = (TextBox)gridrow.FindControl("txt_leader_innovation");
            TextBox txt_leader_achieveing_result = (TextBox)gridrow.FindControl("txt_leader_achieveing_result");
            TextBox txt_leader_relation = (TextBox)gridrow.FindControl("txt_leader_relation");
            TextBox txt_leader_job_knowledge = (TextBox)gridrow.FindControl("txt_leader_job_knowledge");
            TextBox txt_leader_art_communication = (TextBox)gridrow.FindControl("txt_leader_art_communication");
            TextBox txt_leader_leadership = (TextBox)gridrow.FindControl("txt_leader_leadership");
            TextBox txt_leader_decision_making_planning = (TextBox)gridrow.FindControl("txt_leader_decision_making_planning");
            Label lb_total_leader_ass_qty = (Label)gridrow.FindControl("lb_total_leader_ass_qty");
            Label lb_total_deduction = (Label)gridrow.FindControl("lb_total_deduction");

            Label lb_kpi = (Label)gridrow.FindControl("lb_kpi");
            Label lb_grade = (Label)gridrow.FindControl("lb_grade");
            Label lb_description = (Label)gridrow.FindControl("lb_description");
            Label lb_score_balance = (Label)gridrow.FindControl("lb_score_balance");

            Label lb_total_leader_tkn_value_before = (Label)gridrow.FindControl("lb_total_leader_tkn_value_before");
            Label lb_total_leader_taokae_d_before = (Label)gridrow.FindControl("lb_total_leader_taokae_d_before");
            Label lb_total_leader_ass_qty_before = (Label)gridrow.FindControl("lb_total_leader_ass_qty_before");
            Label lb_total_deduction_before = (Label)gridrow.FindControl("lb_total_deduction_before");
            Label lb_score_balance_before = (Label)gridrow.FindControl("lb_score_balance_before");
            Label lb_kpi_before = (Label)gridrow.FindControl("lb_kpi_before");

            objcourse1[itemObj] = new hr_perf_evalu_emp_monthly();

            objcourse1[itemObj].total_leader_tkn_value_before = _func_dmu.zStringToDecimal(lb_total_leader_tkn_value_before.Text);
            objcourse1[itemObj].total_leader_taokae_d_before = _func_dmu.zStringToDecimal(lb_total_leader_taokae_d_before.Text);
            objcourse1[itemObj].total_leader_ass_qty_before = _func_dmu.zStringToDecimal(lb_total_leader_ass_qty_before.Text);
            objcourse1[itemObj].total_deduction_before = _func_dmu.zStringToDecimal(lb_total_deduction_before.Text);
            objcourse1[itemObj].score_balance_before = _func_dmu.zStringToDecimal(lb_score_balance_before.Text);
            objcourse1[itemObj].kpi_before = _func_dmu.zStringToDecimal(lb_kpi_before.Text);

            objcourse1[itemObj].u0idx = _func_dmu.zStringToInt(lb_u0idx.Text);
            objcourse1[itemObj].u1idx = _func_dmu.zStringToInt(lb_u1idx.Text);
            objcourse1[itemObj].supervisor1_idx = _func_dmu.zStringToInt(lb_supervisor1_idx.Text);
            objcourse1[itemObj].supervisor2_idx = _func_dmu.zStringToInt(lb_supervisor2_idx.Text);
            objcourse1[itemObj].score_balance = _func_dmu.zStringToInt(lb_score_balance.Text);
            objcourse1[itemObj].emp_idx = _func_dmu.zStringToInt(lb_emp_idx.Text);
            objcourse1[itemObj].work_experience = lb_work_experience.Text;
            objcourse1[itemObj].group_code = lb_group_code.Text;
            objcourse1[itemObj].level_code = _func_dmu.zStringToInt(lb_level_code.Text);
            objcourse1[itemObj].group_work = _func_dmu.zStringToInt(lb_group_work.Text);
            objcourse1[itemObj].late_mm = _func_dmu.zStringToInt(lb_late_mm.Text);
            objcourse1[itemObj].Back_before_mm = _func_dmu.zStringToInt(lb_Back_before_mm.Text);
            objcourse1[itemObj].missing_qty = _func_dmu.zStringToDecimal(lb_missing_qty.Text);
            objcourse1[itemObj].sick_Leave_qty_day = _func_dmu.zStringToDecimal(lb_sick_Leave_qty_day.Text);
            objcourse1[itemObj].Errandleave_day = _func_dmu.zStringToDecimal(lb_Errandleave_day.Text);
            objcourse1[itemObj].vacation_day = _func_dmu.zStringToDecimal(lb_vacation_day.Text);
            objcourse1[itemObj].workbreak_day = _func_dmu.zStringToDecimal(lb_workbreak_day.Text);
            objcourse1[itemObj].Maternityleave_day = _func_dmu.zStringToDecimal(lb_Maternityleave_day.Text);
            objcourse1[itemObj].corp_kpi = _func_dmu.zStringToDecimal(lb_corp_kpi.Text);
            objcourse1[itemObj].tkn_value = _func_dmu.zStringToInt(lb_tkn_value.Text);
            objcourse1[itemObj].taokae_d = _func_dmu.zStringToInt(lb_taokae_d.Text);
            objcourse1[itemObj].absence = _func_dmu.zStringToInt(lb_absence.Text);
            objcourse1[itemObj].punishment1 = _func_dmu.zStringToInt(lb_punishment1.Text);
            objcourse1[itemObj].punishment2 = _func_dmu.zStringToInt(lb_punishment2.Text);
            objcourse1[itemObj].ass_self_ownership_qty = _func_dmu.zStringToInt(txt_ass_self_ownership_qty.Text);
            objcourse1[itemObj].ass_self_improvement_continuously_qty = _func_dmu.zStringToInt(txt_ass_self_improvement_continuously_qty.Text);
            objcourse1[itemObj].ass_self_commitment_qty = _func_dmu.zStringToInt(txt_ass_self_commitment_qty.Text);
            objcourse1[itemObj].ass_self_innovation = _func_dmu.zStringToInt(txt_ass_self_innovation.Text);
            objcourse1[itemObj].ass_self_achieveing_result = _func_dmu.zStringToInt(txt_ass_self_achieveing_result.Text);
            objcourse1[itemObj].ass_self_relation = _func_dmu.zStringToInt(txt_ass_self_relation.Text);
            objcourse1[itemObj].ass_self_job_knowledge = _func_dmu.zStringToInt(txt_ass_self_job_knowledge.Text);
            objcourse1[itemObj].ass_self_art_communication = _func_dmu.zStringToInt(txt_ass_self_art_communication.Text);
            objcourse1[itemObj].ass_self_leadership = _func_dmu.zStringToInt(txt_ass_self_leadership.Text);
            objcourse1[itemObj].ass_self_decision_making_planning = _func_dmu.zStringToInt(txt_ass_self_decision_making_planning.Text);

            objcourse1[itemObj].leader_ownership_qty = _func_dmu.zStringToInt(txt_leader_ownership_qty.Text);
            objcourse1[itemObj].leader_improvement_continuously_qty = _func_dmu.zStringToInt(txt_leader_improvement_continuously_qty.Text);
            objcourse1[itemObj].leader_commitment_qty = _func_dmu.zStringToInt(txt_leader_commitment_qty.Text);
            objcourse1[itemObj].leader_innovation = _func_dmu.zStringToInt(txt_leader_innovation.Text);
            objcourse1[itemObj].leader_achieveing_result = _func_dmu.zStringToInt(txt_leader_achieveing_result.Text);
            objcourse1[itemObj].leader_relation = _func_dmu.zStringToInt(txt_leader_relation.Text);
            objcourse1[itemObj].leader_job_knowledge = _func_dmu.zStringToInt(txt_leader_job_knowledge.Text);
            objcourse1[itemObj].leader_art_communication = _func_dmu.zStringToInt(txt_leader_art_communication.Text);
            objcourse1[itemObj].leader_leadership = _func_dmu.zStringToInt(txt_leader_leadership.Text);
            objcourse1[itemObj].leader_decision_making_planning = _func_dmu.zStringToInt(txt_leader_decision_making_planning.Text);

            objcourse1[itemObj].total_leader_tkn_value = _func_dmu.zStringToInt(lb_total_leader_tkn_value.Text);
            objcourse1[itemObj].total_leader_taokae_d = _func_dmu.zStringToInt(lb_total_leader_taokae_d.Text);
            objcourse1[itemObj].total_leader_ass_qty = _func_dmu.zStringToInt(lb_total_leader_ass_qty.Text);
            objcourse1[itemObj].total_deduction = _func_dmu.zStringToInt(lb_total_deduction.Text);
            objcourse1[itemObj].kpi = _func_dmu.zStringToInt(lb_kpi.Text);
            objcourse1[itemObj].grade = lb_grade.Text;
            objcourse1[itemObj].description = lb_description.Text;

            objcourse1[itemObj].CEmpIDX = emp_idx;
            objcourse1[itemObj].UEmpIDX = emp_idx;

            objcourse1[itemObj].u0_emp_idx = emp_idx;
            objcourse1[itemObj].u0_org_idx = _func_dmu.zStringToInt(ViewState["org_idx"].ToString());
            objcourse1[itemObj].u0_rdept_idx = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());
            objcourse1[itemObj].u0_rpos_idx = _func_dmu.zStringToInt(ViewState["rpos_idx"].ToString());
            objcourse1[itemObj].u0_rsec_idx = _func_dmu.zStringToInt(ViewState["rsec_idx"].ToString());
            objcourse1[itemObj].remark = _txtremark.Text;
            objcourse1[itemObj].u0_status = _iu0_status; //_func_dmu.zStringToInt(_ddlu0_status.SelectedValue);
           
            objcourse1[itemObj].operation_status_id = "perf_evalu_employee_monthly";

            itemObj++;

        }
        _data_hr_evaluation.hr_perf_evalu_emp_monthly_action = objcourse1;
       // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_hr_evaluation));

        if (id == 0)
        {
            _data_hr_evaluation = callServicePostPerfEvaluEmp(_urlSetInsits_u_hr_evaluation_monthly, _data_hr_evaluation);
            id = _data_hr_evaluation.return_code;
        }
        else
        {
            callServicePostPerfEvaluEmp(_urlSetUpdits_u_hr_evaluation_monthly, _data_hr_evaluation);
        }

        if (_iu0_status == 2)
        {
            _data_hr_evaluation.hr_perf_evalu_emp_action = new hr_perf_evalu_emp[1];
            hr_perf_evalu_emp select_obj = new hr_perf_evalu_emp();
            select_obj.u0idx = id;
            select_obj.operation_status_id = "monthly";
            _data_hr_evaluation.hr_perf_evalu_emp_action[0] = select_obj;
            callServicePostPerfEvaluEmp(_urlsendEmailhr_evaluation, _data_hr_evaluation);
        }


        return id;

    }

    private void zDelete(int id)
    {
        _data_hr_evaluation.hr_perf_evalu_emp_monthly_action = new hr_perf_evalu_emp_monthly[1];
        hr_perf_evalu_emp_monthly select_obj = new hr_perf_evalu_emp_monthly();
        select_obj.u0idx = id;
        select_obj.operation_status_id = "perf_evalu_employee_monthly";
        _data_hr_evaluation.hr_perf_evalu_emp_monthly_action[0] = select_obj;
        callServicePostPerfEvaluEmp(_urlDelits_u_hr_evaluation_monthly, _data_hr_evaluation);
    }

    protected string getStatus(int status)
    {

        if (status == 2)
        {
            // return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
            return "จบการดำเนินการและส่งข้อมูลให้ HR";
        }
        {

            // return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
            return "ดำเนินการสร้าง / แก้ไข";
        }

    }
    private string zGradeCalculat(int total)
    {
        string _Grade = "";
        // A Outstanding ผลการปฏิบัติงานโดดเด่นอย่างชัดเจน 96 - 100 %
        if ((total >= 96) && (total <= 100))
        {
            _Grade = "A";
        }
        //B Exceeds Expectations ผลการปฏิบัติงานเกินกว่าความคาดหวัง 81-95%
        else if ((total >= 81) && (total <= 95))
        {
            _Grade = "B";
        }
        //C Meet Expectations ผลการปฏิบัติงานเป็นไปตามความคาดหวัง 61-80%
        else if ((total >= 61) && (total <= 80))
        {
            _Grade = "C";
        }
        //D Improvement Needed ผลการปฏิบัติงานยังต้องปรับปรุง 50-60% (ไม่พิจารณาปรับเงินเดือน) 
        else if ((total >= 50) && (total <= 60))
        {
            _Grade = "D";
        }
        else
        {
            _Grade = "E";
        }

        return _Grade;
    }

    private void zExportExcel(int id)
    {
       
        _data_hr_evaluation.hr_perf_evalu_emp_monthly_action = new hr_perf_evalu_emp_monthly[1];
        hr_perf_evalu_emp_monthly select_obj = new hr_perf_evalu_emp_monthly();
        select_obj.u0idx = id;
        select_obj.admin_idx = _func_dmu.zStringToInt(ViewState["admin_idx"].ToString());
        select_obj.zstatus = "exportdata";
        select_obj.operation_status_id = "list_td_export";
        _data_hr_evaluation.hr_perf_evalu_emp_monthly_action[0] = select_obj;
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_hr_evaluation));
        _data_hr_evaluation = callServicePostPerfEvaluEmp(_urlGetits_u_hr_evaluation_monthly, _data_hr_evaluation);

        _func_dmu.zSetGridData(GvExportExcel, _data_hr_evaluation.hr_perf_evalu_emp_monthly_action);

        ExportGridToExcel(GvExportExcel, "แบบประเมินผลการปฏิบัติงานระดับพนักงานรายเดือน", "แบบประเมินผลการปฏิบัติงานระดับพนักงานรายเดือน");

    }


    private void ExportGridToExcel(GridView _gv, string sfilename = "ExportToExcel", string ATitle = "Export To Excel")
    {
        if (_gv.Rows.Count == 0)
        {
            return;
        }
        //_gv.AllowPaging = false;
        //_gv.DataBind();
        var totalCols = _gv.Rows[0].Cells.Count;
        var totalRows = _gv.Rows.Count;
        var headerRow = _gv.HeaderRow;

        DataTable tableMaterialLog = new DataTable();

        for (var i = 1; i <= totalCols; i++)
        {
            tableMaterialLog.Columns.Add(headerRow.Cells[i - 1].Text, typeof(String));

        }
        for (var j = 1; j <= totalRows; j++)
        {
            DataRow addMaterialLogRow = tableMaterialLog.NewRow();
            for (var i = 1; i <= totalCols; i++)
            {
                string sdata = ((Label)_gv.Rows[j - 1].Cells[i - 1].Controls[1]).Text;
                addMaterialLogRow[i - 1] = sdata;

            }
            tableMaterialLog.Rows.Add(addMaterialLogRow);
        }
        GridView gv = new GridView();
        gv.DataSource = tableMaterialLog;
        gv.DataBind();
        WriteExcelWithNPOI(tableMaterialLog, "xls", sfilename, ATitle);

    }
    public void WriteExcelWithNPOI(DataTable dt, String extension, String fileName, string ATitle)
    {
        IWorkbook workbook;
        if (extension == "xlsx")
        {
            workbook = new XSSFWorkbook();
        }
        else if (extension == "xls")
        {
            workbook = new HSSFWorkbook();
        }
        else
        {
            throw new Exception("This format is not supported");
        }
        ISheet sheet1 = workbook.CreateSheet("Sheet 1");

        IRow row1 = sheet1.CreateRow(0);

        ICell cellTitle = row1.CreateCell(0);
        String columnNameTitle = ATitle;
        cellTitle.SetCellValue(columnNameTitle);


        row1 = sheet1.CreateRow(1);
        cellTitle = row1.CreateCell(0);
        columnNameTitle = "Print Date : " + DateTime.Now.ToString("dd/MM/yyyy", CultureInfo.InstalledUICulture);
        cellTitle.SetCellValue(columnNameTitle);

        row1 = sheet1.CreateRow(3);

        for (int j = 0; j < dt.Columns.Count; j++)
        {
            ICell cell = row1.CreateCell(j);
            String columnName = dt.Columns[j].ToString();
            cell.SetCellValue(columnName);
        }
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            IRow row = sheet1.CreateRow(i + 4);
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                ICell cell = row.CreateCell(j);
                String columnName = dt.Columns[j].ToString();
                cell.SetCellValue(dt.Rows[i][columnName].ToString());
            }
        }

        using (var exportData = new MemoryStream())
        {
            Response.Clear();
            workbook.Write(exportData);
            if (extension == "xlsx")
            {
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xlsx"));
                Response.BinaryWrite(exportData.ToArray());
            }
            else if (extension == "xls")
            {
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xls"));
                Response.BinaryWrite(exportData.GetBuffer());
            }
            Response.End();
        }
    }

    private int getGrade(int count_grade, int count_grade_criteria)
    {
        int _grade = 0;
        if (_func_dmu.zStringToInt(ViewState["admin_idx"].ToString()) == 0)
        {
            _grade = count_grade;
        }
        else if (_func_dmu.zStringToInt(ViewState["admin_idx"].ToString()) == 1) //  k.นุช
        {
            _grade = count_grade;
        }
        else if (_func_dmu.zStringToInt(ViewState["admin_idx"].ToString()) == 2) //  Dev.
        {
            _grade = count_grade;
        }
        else if (_func_dmu.zStringToInt(ViewState["admin_idx"].ToString()) == 3) //  k.แก้ม
        {
            _grade = count_grade_criteria;
        }
        else
        {
            _grade = count_grade;
        }

        return _grade;

    }

    public void getCountBookingReportGraph()
    {

        _data_hr_evaluation.hr_perf_evalu_emp_monthly_action = new hr_perf_evalu_emp_monthly[1];
        hr_perf_evalu_emp_monthly select_obj = new hr_perf_evalu_emp_monthly();
        select_obj.emp_idx = emp_idx;
        select_obj.admin_idx = _func_dmu.zStringToInt(ViewState["admin_idx"].ToString());
        //select_obj.rpos_idx = _func_dmu.zStringToInt(ViewState["rpos_idx"].ToString());
        //select_obj.rdept_idx = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());
        select_obj.zyear = _func_dmu.zStringToInt(ddlyear.SelectedValue);
        select_obj.emp_code = txt_emp_code.Text.Trim();
        select_obj.operation_status_id = "garp_grade";
        _data_hr_evaluation.hr_perf_evalu_emp_monthly_action[0] = select_obj;
        //litDebug.Text = ViewState["admin_idx"].ToString();
        _data_hr_evaluation = callServicePostPerfEvaluEmp(_urlGetits_u_hr_evaluation_monthly, _data_hr_evaluation);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_hr_evaluation));
        ViewState["Vs_ReportCountGarp"] = _data_hr_evaluation.hr_perf_evalu_emp_monthly_action;
        if (_data_hr_evaluation.hr_perf_evalu_emp_monthly_action != null)
        {

            //litDebug.Text = "3333";
            int count = _data_hr_evaluation.hr_perf_evalu_emp_monthly_action.Length;
            string[] Dept_name = new string[count];
            object[] Count_booking = new object[count];
            int i = 0;
            int iTotal = 0;
            foreach (var data in _data_hr_evaluation.hr_perf_evalu_emp_monthly_action)
            {
                int g = getGrade(data.count_grade, data.count_grade_criteria);
                Dept_name[i] = data.grade_code;
                Count_booking[i] = g.ToString();
                iTotal += g;

                i++;
            }
            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "จำนวนพนักงาน" } } });
            chart.SetXAxis(new XAxis { Categories = Dept_name });
            chart.SetSeries(
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "จำนวนพนักงาน",
                    Data = new Data(Count_booking)
                }
            );
            Update_PanelGraph.Visible = true;
            litReportChart.Text = chart.ToHtmlString();

            // -- -- //

            // -- pie chart --//
            var caseclose = new List<object>();

            foreach (var data in _data_hr_evaluation.hr_perf_evalu_emp_monthly_action)
            {
                int g = getGrade(data.count_grade, data.count_grade_criteria);
                caseclose.Add(new object[] {

                    data.grade_code.ToString(), g.ToString() });
            }

            Highcharts chart1 = new Highcharts("chart1");
            chart1.SetTitle(new DotNet.Highcharts.Options.Title { Text = "จำนวนพนักงาน / เกรด" });
            chart1.InitChart(new DotNet.Highcharts.Options.Chart { DefaultSeriesType = DotNet.Highcharts.Enums.ChartTypes.Pie });

            chart1.SetPlotOptions(new PlotOptions
            {
                Pie = new PlotOptionsPie
                {
                    ShowInLegend = true,
                    AllowPointSelect = true,
                    DataLabels = new PlotOptionsPieDataLabels
                    {
                        Enabled = true,
                        // Formatter = "function() { return '" + this. + ' : '" + lv1count + "';}" //  this.percentage.toFixed(2)+' % '; }"
                        //Color = ColorTranslator.FromHtml("#000000"),
                        //ConnectorColor = ColorTranslator.FromHtml("#000000"),
                        Formatter = "function() { return this.point.name +' : '+  this.y.toFixed(0)+' '; }"
                    }
                }
            });

            chart1.SetTooltip(new Tooltip
            {
                Enabled = true,
                Formatter = "function() { return this.point.name +' : '+  this.percentage.toFixed(2)+' % '; }"

            });
            chart1.SetSeries(new DotNet.Highcharts.Options.Series[]
                 {
                        new DotNet.Highcharts.Options.Series()
                        {
                            Data = new Data (caseclose.ToArray())
                        }
                 });


            litReportChart1.Text = chart1.ToHtmlString();
            if (iTotal == 0)
            {
                //  Update_PanelGraph.Visible = false;
            }
            // -- pie chart --//
        }
        else
        {
            Update_PanelGraph.Visible = false;

        }
        Update_PanelGraph.Visible = true;
        Update_Panelreport.Visible = false;

    }

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlName = (DropDownList)sender;

        switch (ddlName.ID)
        {

            case "ddlTypeReport":

                Update_PanelGraph.Visible = false;
                Update_Panelreport.Visible = false;
                btnexport_report.Visible = false;
                if (ddlName.SelectedValue == "1")
                {
                    CreateDsits_u1_PerfEvaluEmpDaily();
                    btnexport_report.Visible = true;
                }

                break;
        }
    }
    public void ReportGraph()
    {

        _data_hr_evaluation.hr_perf_evalu_emp_monthly_action = new hr_perf_evalu_emp_monthly[1];
        hr_perf_evalu_emp_monthly select_obj = new hr_perf_evalu_emp_monthly();
        select_obj.emp_idx = emp_idx;
        select_obj.admin_idx = _func_dmu.zStringToInt(ViewState["admin_idx"].ToString());
        select_obj.zyear = _func_dmu.zStringToInt(ddlyear.SelectedValue);
        select_obj.emp_code = txt_emp_code.Text.Trim();
        select_obj.operation_status_id = "garp_report";
        _data_hr_evaluation.hr_perf_evalu_emp_monthly_action[0] = select_obj;
        _data_hr_evaluation = callServicePostPerfEvaluEmp(_urlGetits_u_hr_evaluation_monthly, _data_hr_evaluation);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_hr_evaluation));

        CreateDsits_u1_PerfEvaluEmpDaily();
        DataSet dsContacts = (DataSet)ViewState["vshr_u1_perf_evalu_employee_daily"];
        if (_data_hr_evaluation.hr_perf_evalu_emp_monthly_action != null)
        {
            int ic = 0;
            foreach (var vdr in _data_hr_evaluation.hr_perf_evalu_emp_monthly_action)
            {
                ic++;

                DataRow drContacts = dsContacts.Tables["dshr_u1_perf_evalu_employee_daily"].NewRow();


                drContacts["doccode"] = vdr.doccode;
                drContacts["zdocdate"] = vdr.zdocdate;
                drContacts["docdate"] = vdr.docdate;
                drContacts["u0_status"] = vdr.u0_status;
                drContacts["u0_emp_idx"] = vdr.u0_emp_idx;
                drContacts["u0_org_idx"] = vdr.u0_org_idx;
                drContacts["u0_rdept_idx"] = vdr.u0_rdept_idx;
                drContacts["u0_rpos_idx"] = vdr.u0_rpos_idx;
                drContacts["u0_rsec_idx"] = vdr.u0_rsec_idx;
                drContacts["remark"] = vdr.remark;
                drContacts["CEmpIDX"] = vdr.CEmpIDX;
                drContacts["CreateDate"] = vdr.CreateDate;
                drContacts["u0_emp_name_th"] = vdr.u0_emp_name_th;
                drContacts["u0_pos_name_th"] = vdr.u0_pos_name_th;
                drContacts["u0_sec_name_th"] = vdr.u0_sec_name_th;
                drContacts["u0_org_name_th"] = vdr.u0_org_name_th;
                drContacts["u1idx"] = vdr.u1idx;
                drContacts["u0idx"] = vdr.u0idx;
                drContacts["emp_idx"] = vdr.emp_idx;


                drContacts["work_experience"] = vdr.work_experience;
                drContacts["group_code"] = vdr.group_code;
                drContacts["level_code"] = vdr.level_code;
                drContacts["group_work"] = vdr.group_work;
                drContacts["late_mm"] = vdr.late_mm;
                drContacts["Back_before_mm"] = vdr.Back_before_mm;
                drContacts["missing_qty"] = vdr.missing_qty;
                drContacts["sick_Leave_qty_day"] = vdr.sick_Leave_qty_day;
                drContacts["Errandleave_day"] = vdr.Errandleave_day;
                drContacts["vacation_day"] = vdr.vacation_day;
                drContacts["workbreak_day"] = vdr.workbreak_day;
                drContacts["Maternityleave_day"] = vdr.Maternityleave_day;
                drContacts["corp_kpi"] = vdr.corp_kpi;
                drContacts["tkn_value"] = vdr.tkn_value;
                drContacts["taokae_d"] = vdr.taokae_d;
                drContacts["absence"] = vdr.absence;
                drContacts["punishment1"] = vdr.punishment1;
                drContacts["punishment2"] = vdr.punishment2;
                drContacts["ass_self_ownership_qty"] = vdr.ass_self_ownership_qty;
                drContacts["ass_self_improvement_continuously_qty"] = vdr.ass_self_improvement_continuously_qty;

                drContacts["UEmpIDX"] = vdr.UEmpIDX;
                drContacts["UpdateDate"] = vdr.UpdateDate;
                drContacts["u1_status"] = vdr.u1_status;
                drContacts["emp_code"] = vdr.emp_code;
                drContacts["rpos_idx"] = vdr.rpos_idx;
                drContacts["pos_name_th"] = vdr.pos_name_th;
                drContacts["rsec_idx"] = vdr.rsec_idx;
                drContacts["sec_name_th"] = vdr.sec_name_th;
                drContacts["rdept_idx"] = vdr.rdept_idx;
                drContacts["dept_name_th"] = vdr.dept_name_th;
                drContacts["org_idx"] = vdr.org_idx;
                drContacts["org_name_th"] = vdr.org_name_th;
                drContacts["emp_name_th"] = vdr.emp_name_th;

                drContacts["costcenter_idx"] = vdr.costcenter_idx;
                drContacts["costcenter_no"] = vdr.costcenter_no;
                drContacts["costcenter_name"] = vdr.costcenter_name;
                drContacts["emp_probation_date"] = vdr.emp_probation_date;
                drContacts["supervisor1_idx"] = vdr.supervisor1_idx;
                drContacts["supervisor2_idx"] = vdr.supervisor2_idx;


                drContacts["group_code"] = vdr.group_code;
                drContacts["level_code"] = vdr.level_code;
                drContacts["group_work"] = vdr.group_work;
                drContacts["late_mm"] = vdr.late_mm;
                drContacts["Back_before_mm"] = vdr.Back_before_mm;
                drContacts["missing_qty"] = vdr.missing_qty;
                drContacts["sick_Leave_qty_day"] = vdr.sick_Leave_qty_day;
                drContacts["Errandleave_day"] = vdr.Errandleave_day;
                drContacts["vacation_day"] = vdr.vacation_day;
                drContacts["workbreak_day"] = vdr.workbreak_day;
                drContacts["Maternityleave_day"] = vdr.Maternityleave_day;


                drContacts["ass_self_ownership_qty"] = vdr.ass_self_ownership_qty;
                drContacts["ass_self_improvement_continuously_qty"] = vdr.ass_self_improvement_continuously_qty;
                drContacts["ass_self_commitment_qty"] = vdr.ass_self_commitment_qty;
                drContacts["ass_self_innovation"] = vdr.ass_self_innovation;
                drContacts["ass_self_achieveing_result"] = vdr.ass_self_achieveing_result;
                drContacts["ass_self_relation"] = vdr.ass_self_relation;
                drContacts["ass_self_job_knowledge"] = vdr.ass_self_job_knowledge;
                drContacts["ass_self_art_communication"] = vdr.ass_self_art_communication;
                drContacts["ass_self_leadership"] = vdr.ass_self_leadership;
                drContacts["ass_self_decision_making_planning"] = vdr.ass_self_decision_making_planning;

                drContacts["leader_ownership_qty"] = vdr.leader_ownership_qty;
                drContacts["leader_improvement_continuously_qty"] = vdr.leader_improvement_continuously_qty;
                drContacts["leader_commitment_qty"] = vdr.leader_commitment_qty;
                drContacts["leader_innovation"] = vdr.leader_innovation;
                drContacts["leader_achieveing_result"] = vdr.leader_achieveing_result;
                drContacts["leader_relation"] = vdr.leader_relation;
                drContacts["leader_job_knowledge"] = vdr.leader_job_knowledge;
                drContacts["leader_art_communication"] = vdr.leader_art_communication;
                drContacts["leader_leadership"] = vdr.leader_leadership;
                drContacts["leader_decision_making_planning"] = vdr.leader_decision_making_planning;
                drContacts["description"] = vdr.description;
                drContacts["total_leader_tkn_value"] = vdr.total_leader_tkn_value;
                drContacts["total_leader_taokae_d"] = vdr.total_leader_taokae_d;
                drContacts["total_leader_ass_qty"] = vdr.total_leader_ass_qty;
                drContacts["total_deduction"] = vdr.total_deduction;

                drContacts["total_leader_tkn_value_before"] = vdr.total_leader_tkn_value_before;
                drContacts["score_balance_before"] = vdr.score_balance_before;
                drContacts["total_leader_taokae_d_before"] = vdr.total_leader_taokae_d_before;
                drContacts["total_leader_ass_qty_before"] = vdr.total_leader_ass_qty_before;
                drContacts["total_deduction_before"] = vdr.total_deduction_before;


                drContacts["grade"] = vdr.grade;
                drContacts["grade_criteria"] = vdr.grade_criteria;
                drContacts["kpi"] = vdr.kpi;
                drContacts["kpi_before"] = vdr.kpi_before;

                dsContacts.Tables["dshr_u1_perf_evalu_employee_daily"].Rows.Add(drContacts);
            }
        }


        ViewState["vshr_u1_perf_evalu_employee_daily"] = dsContacts;
        _func_dmu.zSetGridData(GVreport, dsContacts.Tables["dshr_u1_perf_evalu_employee_daily"]);

        Update_PanelGraph.Visible = false;
        Update_Panelreport.Visible = true;


    }
    private void zExportExcel_report()
    {

        _func_dmu.zSetGridData(GvExportExcel, ViewState["vshr_u1_perf_evalu_employee_daily"]);
        ExportGridToExcel(GvExportExcel, "แบบประเมินผลการปฏิบัติงานระดับพนักงานรายเดือน", "แบบประเมินผลการปฏิบัติงานระดับพนักงานรายเดือน");

    }


    private void zUnconfirm(int id)
    {
        if (_func_dmu.zStringToInt(ViewState["admin_idx"].ToString()) == 1)
        {
            _data_hr_evaluation.hr_perf_evalu_emp_monthly_action = new hr_perf_evalu_emp_monthly[1];
            hr_perf_evalu_emp_monthly select_obj = new hr_perf_evalu_emp_monthly();
            select_obj.u0idx = id;
            select_obj.operation_status_id = "unconfirm";
            _data_hr_evaluation.hr_perf_evalu_emp_monthly_action[0] = select_obj;
            callServicePostPerfEvaluEmp(_urlSetUpdits_u_hr_evaluation_monthly, _data_hr_evaluation);

            data_hr_evaluation _data = new data_hr_evaluation();
            _data.hr_perf_evalu_emp_action = new hr_perf_evalu_emp[1];
            hr_perf_evalu_emp select_obj1 = new hr_perf_evalu_emp();
            select_obj1.u0idx = id;
            select_obj1.operation_status_id = "monthly_unconfirm";
            _data.hr_perf_evalu_emp_action[0] = select_obj1;
            // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data));
            callServicePostPerfEvaluEmp(_urlsendEmailhr_evaluation, _data);

        }

    }

    //protected void grvMergeHeader_RowCreated(object sender, GridViewRowEventArgs e)
    //{
    //    var gridViewName = (GridView)sender;
    //    if (e.Row.RowType == DataControlRowType.Header)
    //    {
    //        GridView HeaderGrid = (GridView)sender;
    //        GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
    //        TableCell HeaderCell = new TableCell();
    //        HeaderCell.Text = "";
    //        HeaderCell.ColumnSpan = 11;
    //        HeaderCell.HorizontalAlign = HorizontalAlign.Center;
    //        HeaderGridRow.Cells.Add(HeaderCell);

    //        HeaderCell = new TableCell();
    //        HeaderCell.Text = "สัดส่วนการคำนวณคะแนน";
    //        HeaderCell.ColumnSpan = 4;
    //        HeaderCell.HorizontalAlign = HorizontalAlign.Center;
    //        HeaderGridRow.Cells.Add(HeaderCell);

    //        HeaderCell = new TableCell();
    //        HeaderCell.Text = "การลงโทษ";
    //        HeaderCell.ColumnSpan = 2;
    //        HeaderCell.HorizontalAlign = HorizontalAlign.Center;
    //        HeaderGridRow.Cells.Add(HeaderCell);

    //        HeaderCell = new TableCell();
    //        HeaderCell.Text = "";
    //        HeaderCell.ColumnSpan = 6;
    //        HeaderCell.HorizontalAlign = HorizontalAlign.Center;
    //        HeaderGridRow.Cells.Add(HeaderCell);

    //        HeaderCell = new TableCell();
    //        HeaderCell.Text = "การลงโทษ";
    //        HeaderCell.ColumnSpan = 2;
    //        HeaderCell.HorizontalAlign = HorizontalAlign.Center;
    //        HeaderGridRow.Cells.Add(HeaderCell);



    //        gridViewName.Controls[0].Controls.AddAt(0, HeaderGridRow);

    //    }
    //}

    public void ImportData()
    {
        FileUpload uploadio = (FileUpload)FvInsertH.FindControl("upload");
        if (uploadio.HasFile)
        {
            string datetimeNow = DateTime.Now.ToString("yyyyMMdd_HHmmss_ffff");
            string FileName = Path.GetFileName(uploadio.PostedFile.FileName);
            string extension = Path.GetExtension(uploadio.PostedFile.FileName);
            string newFileName = datetimeNow + extension.ToLower();
            string folderPath = ConfigurationManager.AppSettings["path_flie_elearning"];
            string filePath = Server.MapPath(folderPath + newFileName);
            if (extension.ToLower() == ".xls" || extension.ToLower() == ".xlsx")
            {
                uploadio.SaveAs(filePath);
                GridView gvItemsu1PerfEvaluEmpDaily = (GridView)FvInsert.FindControl("gvItemsu1PerfEvaluEmpDaily");
                // filePath = Server.MapPath(filePath);

                string conStr = String.Empty;
                if (extension.ToLower() == ".xls")
                {
                    conStr = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;

                }
                else
                {
                    conStr = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;

                }

                conStr = String.Format(conStr, filePath, "Yes");
                OleDbConnection connExcel = new OleDbConnection(conStr);
                OleDbCommand cmdExcel = new OleDbCommand();
                OleDbDataAdapter oda = new OleDbDataAdapter();
                DataTable dt = new DataTable();

                cmdExcel.Connection = connExcel;
                connExcel.Open();
                DataTable dtExcelSchema;
                dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                string SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                connExcel.Close();
                connExcel.Open();
                cmdExcel.CommandText = "SELECT * From [" + SheetName + "]";
                oda.SelectCommand = cmdExcel;
                oda.Fill(dt);
                connExcel.Close();

                // setObject_Main();
                int itemObj = 0;
                var count_alert_u1 = dt.Rows.Count;
                for (int i = 0; i <= dt.Rows.Count - 1; i++)
                {
                    if (dt.Rows[i][0].ToString().Trim() != String.Empty)
                    {
                        string sEmp_Code = "";
                        int iEmp_Idx = 0;
                        sEmp_Code = dt.Rows[i][0].ToString().Trim();
                        //if(i == 2)
                        //{
                        //    litDebug.Text = dt.Rows[i][0].ToString().Trim() + "-" + dt.Rows[i][38].ToString().Trim();
                        //}
                        //*****************************
                        GridView gvList = (GridView)FvInsert.FindControl("gvItemsu1PerfEvaluEmpDaily");
                        foreach (GridViewRow gridrow in gvList.Rows)
                        {

                            Label lb_emp_idx = (Label)gridrow.FindControl("lb_emp_idx");
                            Label lb_emp_code = (Label)gridrow.FindControl("lb_emp_code");

                            if (lb_emp_code.Text.Trim() == sEmp_Code)
                            {

                                Label lb_group_code = (Label)gridrow.FindControl("lb_group_code");
                                Label lb_level_code = (Label)gridrow.FindControl("lb_level_code");
                                Label lb_group_work = (Label)gridrow.FindControl("lb_group_work");

                                //if (
                                //    (lb_group_code.Text.Trim() == dt.Rows[i][4].ToString().Trim()) &&
                                //    (lb_level_code.Text.Trim() == dt.Rows[i][5].ToString().Trim()) &&
                                //    (lb_group_work.Text.Trim() == dt.Rows[i][6].ToString().Trim()) 
                                //    )
                                //{
                                Label lb_u0idx = (Label)gridrow.FindControl("lb_u0idx");
                                Label lb_u1idx = (Label)gridrow.FindControl("lb_u1idx");

                                Label lb_supervisor1_idx = (Label)gridrow.FindControl("lb_supervisor1_idx");
                                Label lb_supervisor2_idx = (Label)gridrow.FindControl("lb_supervisor2_idx");

                                TextBox txt_leader_ownership_qty = (TextBox)gridrow.FindControl("txt_leader_ownership_qty");
                                TextBox txt_leader_improvement_continuously_qty = (TextBox)gridrow.FindControl("txt_leader_improvement_continuously_qty");
                                TextBox txt_leader_commitment_qty = (TextBox)gridrow.FindControl("txt_leader_commitment_qty");
                                TextBox txt_leader_innovation = (TextBox)gridrow.FindControl("txt_leader_innovation");
                                TextBox txt_leader_achieveing_result = (TextBox)gridrow.FindControl("txt_leader_achieveing_result");
                                TextBox txt_leader_relation = (TextBox)gridrow.FindControl("txt_leader_relation");
                                TextBox txt_leader_job_knowledge = (TextBox)gridrow.FindControl("txt_leader_job_knowledge");
                                TextBox txt_leader_art_communication = (TextBox)gridrow.FindControl("txt_leader_art_communication");
                                TextBox txt_leader_leadership = (TextBox)gridrow.FindControl("txt_leader_leadership");
                                TextBox txt_leader_decision_making_planning = (TextBox)gridrow.FindControl("txt_leader_decision_making_planning");
                                Label lb_kpi = (Label)gridrow.FindControl("lb_kpi");

                                txt_leader_ownership_qty.Text = TKNCoreValue_import(dt.Rows[i][1].ToString().Trim());
                                txt_leader_improvement_continuously_qty.Text = TKNCoreValue_import(dt.Rows[i][2].ToString().Trim());
                                txt_leader_commitment_qty.Text = TKNCoreValue_import(dt.Rows[i][3].ToString().Trim());

                                txt_leader_innovation.Text = TKNCoreValue_import(dt.Rows[i][4].ToString().Trim());
                                txt_leader_achieveing_result.Text = TKNCoreValue_import(dt.Rows[i][5].ToString().Trim());
                                txt_leader_relation.Text = TKNCoreValue_import(dt.Rows[i][6].ToString().Trim());
                                txt_leader_job_knowledge.Text = TKNCoreValue_import(dt.Rows[i][7].ToString().Trim());
                                txt_leader_art_communication.Text = TKNCoreValue_import(dt.Rows[i][8].ToString().Trim());
                                txt_leader_leadership.Text = TKNCoreValue_import(dt.Rows[i][9].ToString().Trim());
                                txt_leader_decision_making_planning.Text = TKNCoreValue_import(dt.Rows[i][10].ToString().Trim());

                                int igroupVIP = 0;
                                int igroup1 = 0;
                                int igroup2 = 0;

                                if (lb_group_code.Text.Trim() == "1")
                                {
                                    igroup1++;
                                }
                                else if (lb_group_code.Text.Trim() == "2")
                                {
                                    igroup2++;
                                }
                                else
                                {
                                    igroupVIP++;
                                }

                                if ((igroupVIP > 0) || (igroup1 > 0))
                                {


                                }
                                else if (igroup2 > 0)
                                {
                                    txt_leader_innovation.Text = "0";
                                    // txt_leader_achieveing_result.Text = TKNCoreValue_import(dt.Rows[i][42].ToString().Trim());
                                    //txt_leader_relation.Text = TKNCoreValue_import(dt.Rows[i][43].ToString().Trim());
                                    //txt_leader_job_knowledge.Text = TKNCoreValue_import(dt.Rows[i][44].ToString().Trim());
                                    txt_leader_art_communication.Text = "0";
                                    txt_leader_leadership.Text = "0";
                                    txt_leader_decision_making_planning.Text = "0";

                                }


                                itemObj++;
                                break;

                                //}
                                //else
                                //{
                                //    break;
                                //}

                            }



                        }

                        //*****************************

                    }
                }


                File.Delete(filePath);

                if (itemObj > 0)
                {
                    if (setCheckError(2) == false)
                    {
                        setCalculator();
                        showMsAlert("Import ข้อมูลเสร็จแล้ว");
                    }

                }
                else
                {
                    showMsAlert("ไฟล์ข้อมูลไม่ถูกต้อง !");
                }

            }
            else
            {
                showMsAlert("Import ได้เฉพาะไฟล์ Excel (นามสกุลไฟล์ .xls หรือ .xlsx) เท่านั้น");
            }
        }

    }

    private string TKNCoreValue_import(string _qty)
    {
        int ierror = 0, qty_return = 0;

        ierror = 0;
        try
        {
            qty_return = _func_dmu.zStringToInt(_qty);
        }
        catch
        {
            ierror = 1;
        }
        if (ierror == 1)
        {
            qty_return = 0;
        }
        else
        {

            if (_func_dmu.zStringToInt(_qty) > 5)
            {
                ierror = 2;
                qty_return = 0;

            }
            else if (_func_dmu.zStringToInt(_qty) < 0)
            {
                ierror = 2;
                qty_return = 0;

            }

        }

        return qty_return.ToString();

    }

}