﻿<%@ WebHandler Language="C#" Class="Handler" %>

using System;
using System.Web;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Globalization;
using System.Threading;

public class Handler : IHttpHandler
{

   public void ProcessRequest(HttpContext context)
   {
      string callback = context.Request.QueryString["callback"];
      int announceIdx = 0;
      bool subString = false;
      int.TryParse(context.Request.QueryString["announceIdx"], out announceIdx);
      bool.TryParse(context.Request.QueryString["subString"], out subString);
      string json = this.getEvents(announceIdx, subString);
      if (!string.IsNullOrEmpty(callback))
      {
         json = string.Format("{0}({1});", callback, json);
      }
      context.Response.ContentType = "text/json";
      context.Response.Write(json);
   }

   private string getEvents(int announceIdx, bool subString = false)
   {
      List<object> emps = new List<object>();
      using (SqlConnection conn = new SqlConnection())
      {
         conn.ConnectionString = ConfigurationManager.ConnectionStrings["conn_mas"].ConnectionString;
         using (SqlCommand cmd = new SqlCommand())
         {
            cmd.CommandText = "SELECT * FROM view_emps_u0_announce_parttime WHERE u0_announce_idx = @announceIdx";
            cmd.Parameters.AddWithValue("@announceIdx", announceIdx);
            cmd.Connection = conn;
            conn.Open();
            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;
            using (SqlDataReader sdr = cmd.ExecuteReader())
            {
               var textHexColor = String.Empty;
               var parttimeCode = String.Empty;
               while (sdr.Read())
               {
                  System.Drawing.Color col = System.Drawing.ColorTranslator.FromHtml(sdr["parttime_bg_color"].ToString());
                  if (((col.R * 0.2126) + (col.G * 0.7152) + (col.B * 0.0722)) > (255 / 2))
                  {
                     textHexColor = "#000000";
                  }
                  else
                  {
                     textHexColor = "#ffffff";
                  }
                  if (sdr["parttime_code"].ToString().Length > 7 && subString == true)
                  {
                     parttimeCode = sdr["parttime_code"].ToString().Substring(0, 7) + "..";
                  }
                  else
                  {
                     parttimeCode = sdr["parttime_code"].ToString();
                  }
                  emps.Add(new
                  {
                     parttimeIdx = sdr["m0_parttime_idx"],
                     title = parttimeCode,
                     start = String.Format("{0:yyyy-MM-dd}", sdr["empshift_date"]),
                     color = sdr["parttime_bg_color"],
                     textColor = textHexColor,
                     allDay = true,
                     announceStartDate = String.Format("{0:yyyy-MM-dd}", sdr["announce_start_date"]),
                     announceEndDate = String.Format("{0:yyyy-MM-dd}", sdr["announce_end_date"]),
                     newAnnounceStartDate = String.Format("{0:dd/MM/yyyy}", sdr["announce_start_date"]),
                     newAnnounceEndDate = String.Format("{0:dd/MM/yyyy}", sdr["announce_end_date"])
                  });
               }
            }
            conn.Close();
         }
         return (new JavaScriptSerializer().Serialize(emps));
      }
   }

   public bool IsReusable
   {
      get
      {
         return false;
      }
   }
}


