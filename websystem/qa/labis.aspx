﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="labis.aspx.cs" Inherits="websystem_qa_labis" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <asp:HyperLink ID="setOntop" runat="server"></asp:HyperLink>
    <asp:TextBox ID="tbValuePlace" Visible="false" runat="server" CssClass="form-control"></asp:TextBox>
    <asp:TextBox ID="tb_PlaceName" Visible="false" runat="server" CssClass="form-control"></asp:TextBox>

    <asp:TextBox ID="tbValuePlaceSup" Visible="false" runat="server" CssClass="form-control"></asp:TextBox>
    <asp:TextBox ID="tbValuePlaceNameSup" Visible="false" runat="server" CssClass="form-control"></asp:TextBox>

    <div class="modal fade bs-example-modal-sm success" id="success_alert" tabindex="-1" hidden="hidden" role="dialog"
        aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content bg-success text-success" style="height: 100px">
                <div class="alert text-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong class="text-success text-center"><i class="fa fa-check-circle"
                            aria-hidden="true"></i>ดำเนินรายการสำเร็จ ! </strong>

                </div>
            </div>
        </div>
    </div>
    <!--tab menu-->
    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default" id="divBtnHead" runat="server" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-bar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="sub-navbar">
                        <a class="navbar-brand" href="#"><b>Menu</b></a>
                    </div>
                </div>

                <!--Collect the nav links, forms, and other content for toggling-->
                <div class="collapse navbar-collapse" id="menu-bar">
                    <ul class="nav navbar-nav" id="uiNav" runat="server">
                        <li id="li0" runat="server">
                            <asp:LinkButton ID="lbCreate" runat="server" CommandName="cmdCreate" OnCommand="navCommand"
                                CommandArgument="docCreate"> สร้างรายการ</asp:LinkButton>
                        </li>
                        <li id="li1" runat="server">
                            <asp:LinkButton ID="lbDetail" runat="server" CommandName="cmdDetail" OnCommand="navCommand"
                                CommandArgument="docDetail"> รายการทั่วไป</asp:LinkButton>
                        </li>
                        <li id="li5" runat="server">
                            <asp:LinkButton ID="lbPrint" runat="server" CommandName="cmdMenuPrint"
                                OnCommand="navCommand" CommandArgument="docPrint"> ปริ้นตัวอย่าง</asp:LinkButton>
                        </li>
                        <% if (ViewState["rdept_permission"].ToString() == "27" || Session["emp_idx"].ToString().ToString() == "172" || Session["emp_idx"].ToString().ToString() == "1413")
                            { %>
                        <li id="li2" runat="server">
                            <asp:LinkButton ID="lbLab" runat="server" CommandName="cmdLab" OnCommand="navCommand"
                                CommandArgument="docLab"> Lab</asp:LinkButton>
                        </li>

                        <li id="li3" runat="server">
                            <asp:LinkButton ID="lbLabResult" runat="server" CommandName="cmdLabResult"
                                OnCommand="navCommand" CommandArgument="docLabResult"> กรอกผลการวิเคราะห์ตัวอย่าง
                            </asp:LinkButton>
                        </li>
                        <% } %>

                        <%--<% if (ViewState["rdept_permission"].ToString() == "27" && ViewState["joblevel_permission"].ToString() == "6")--%>
                        <% if (ViewState["rdept_permission"].ToString() == "27" && Session["emp_idx"].ToString() == "297" || ViewState["rdept_permission"].ToString() == "27" && Session["emp_idx"].ToString() == "295"
                                || Session["emp_idx"].ToString().ToString() == "172" || Session["emp_idx"].ToString().ToString() == "1413")
                            { %>
                        <li id="li4" runat="server">
                            <asp:LinkButton ID="lbSupervisor" runat="server" CssClass="supervisor"
                                CommandName="cmdLabResult" OnCommand="navCommand" CommandArgument="docSupervisor">
                                Supervisor</asp:LinkButton>
                        </li>
                        <% } %>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
    <!--tab menu-->
    <div class="col-md-12">
        <asp:Literal ID="litDebug" runat="server"></asp:Literal>
        <asp:Literal ID="litDebug1" runat="server"></asp:Literal>
        <asp:Literal ID="litDebug2" runat="server"></asp:Literal>
        <asp:Literal ID="litDebugcheck" runat="server"></asp:Literal>
        <asp:Literal ID="li_count" runat="server"></asp:Literal>
        <asp:Literal ID="litDebug_show" runat="server"></asp:Literal>
        <asp:PlaceHolder ID="plBarCode" runat="server" />
    </div>

    <!--multiview-->
    <asp:MultiView ID="mvSystem" runat="server">

        <!--View Create-->
        <asp:View ID="docCreate" runat="server">


            <div id="divCreate" runat="server" class="col-md-12">
                <asp:FormView ID="fvEmpDetail" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <ItemTemplate>
                        <asp:HiddenField ID="hfEmpOrgIDX" runat="server" Value='<%# Eval("org_idx") %>' />
                        <asp:HiddenField ID="hfEmpRdeptIDX" runat="server" Value='<%# Eval("rdept_idx") %>' />
                        <asp:HiddenField ID="hfEmpRsecIDX" runat="server" Value='<%# Eval("rsec_idx") %>' />
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายละเอียดผู้ใช้งาน</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">รหัสพนักงาน</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpCode" runat="server" CssClass="form-control"
                                                Text='<%# Eval("emp_code") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ชื่อ - นามสกุล</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpName" runat="server" CssClass="form-control"
                                                Text='<%# Eval("emp_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">องค์กร</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpOrg" runat="server" CssClass="form-control"
                                                Text='<%# Eval("org_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ฝ่าย</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpDept" runat="server" CssClass="form-control"
                                                Text='<%# Eval("dept_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">แผนก</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpSec" runat="server" CssClass="form-control"
                                                Text='<%# Eval("sec_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ตำแหน่ง</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpPos" runat="server" CssClass="form-control"
                                                Text='<%# Eval("pos_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:FormView>

                <div class="panel panel-info" id="div_create" runat="server">
                    <div class="panel-heading">
                        <h3 class="panel-title">รายละเอียดรายการ</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal" role="form">
                            <asp:FormView ID="fvActor1Node1" runat="server" DefaultMode="Insert" Width="100%"
                                OnDataBound="fvDetail_DataBound">
                                <InsertItemTemplate>
                                    <!--requestor / fill form-->
                                    <asp:HiddenField ID="hfNodeIDX" runat="server" Value="1" />
                                    <asp:HiddenField ID="hfActorIDX" runat="server" Value="1" />
                                    <asp:HiddenField ID="hfU0IDX" runat="server" Value="0" />

                                    <div class="form-group">
                                        <div class="panel-heading f-bold">รายละเอียดรายการตรวจวิเคราะห์</div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">สถานที่ตรวจสอบ</label>
                                        <div class="col-sm-5">
                                            <asp:DropDownList ID="ddlPlace" runat="server" CssClass="form-control"
                                                OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                ValidationGroup="Create_Actor1" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValid4ator1"
                                                ValidationGroup="Create_Actor1" runat="server" Display="None"
                                                ControlToValidate="ddlPlace" Font-Size="11" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกสถานที่ตรวจสอบ"
                                                ValidationExpression="*กรุณาเลือกสถานที่ตรวจสอบ" InitialValue="-1" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3"
                                                runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                TargetControlID="RequiredFieldValid4ator1" Width="160" />
                                        </div>

                                        <label class="col-sm-2 control-label"></label>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-10">
                                            <asp:CheckBox ID="Check_LabOut" runat="server"
                                                Text=" มีความประสงค์ส่งตรวจที่ Lab ภายนอก" CellPadding="5"
                                                CellSpacing="5" RepeatColumns="3" Width="100%" Font-Bold="false"
                                                RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right" />

                                            <%-- <asp:DropDownList ID="DropDownList1" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" ValidationGroup="Create_Actor1" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="Create_Actor1" runat="server" Display="None"
                                                ControlToValidate="ddlPlace" Font-Size="11" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกสถานที่ตรวจสอบ"
                                                ValidationExpression="*กรุณาเลือกสถานที่ตรวจสอบ" InitialValue="-1" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValid4ator1" Width="160" />--%>
                                        </div>

                                        <label class="col-sm-2 control-label"></label>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">ตรวจแบบรายการ</label>
                                        <div class="col-sm-10 control-label textleft">
                                            <asp:Label ID="lbl_test_detail_idx" runat="server" Visible="false"
                                                Text='<%# Eval("test_detail_idx") %>' />
                                            <%--<div class="repeaterStyle">--%>
                                            <asp:Label ID="lbl_test_detail_name" runat="server" CssClass="control-label"
                                                Font-Bold="false" Text='<%# Eval("test_detail_name") %>' />

                                            <asp:CheckBoxList ID="chk_test" runat="server" CellPadding="5"
                                                CellSpacing="5" ValidationGroup="Create_Actor1" RepeatColumns="3"
                                                Width="100%" Font-Bold="false" RepeatDirection="Vertical"
                                                RepeatLayout="Table" TextAlign="Right" />

                                            <asp:TextBox ID="chk_selectdetail" runat="server" Visible="false">
                                            </asp:TextBox>
                                        </div>
                                    </div>

                                    <asp:UpdatePanel ID="update_test" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">ตรวจแบบชุด</label>
                                                <div class="col-sm-10 control-label textleft">
                                                    <asp:Label ID="lbl_set_test_detail_idx" runat="server"
                                                        Visible="true" Text='<%# Eval("set_test_detail_idx") %>' />

                                                    <asp:Label ID="lbl_set_test_detail_name" runat="server"
                                                        CssClass="control-label" Font-Bold="false"
                                                        Text='<%# Eval("set_test_detail_name") %>' />
                                                    <asp:CheckBoxList ID="chk_settest" runat="server"
                                                        onclick="CheckOnlyOne(event)" ValidationGroup="Create_Actor1"
                                                        CellPadding="5" CellSpacing="5" RepeatColumns="2" Width="50%"
                                                        Font-Bold="false" AutoPostBack="true"
                                                        OnSelectedIndexChanged="CheckBox_Clicked"
                                                        RepeatDirection="Vertical" RepeatLayout="Table"
                                                        TextAlign="Right" />

                                                    <asp:TextBox ID="chk_select" runat="server" Visible="false">
                                                    </asp:TextBox>
                                                    <asp:TextBox ID="chk_select_un" runat="server" Visible="false">
                                                    </asp:TextBox>
                                                    <asp:TextBox ID="chk_select_count" runat="server" Visible="false">
                                                    </asp:TextBox>
                                                </div>
                                            </div>

                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="chk_settest"
                                                EventName="SelectedIndexChanged" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                    <script type="text/javascript">
                                        function CheckOnlyOne(e) {
                                            if (!e) e = window.event;
                                            var sender = e.target || e.srcElement;

                                            if (sender.nodeName != 'INPUT') return;
                                            var checker = sender;
                                            var chkBox = document.getElementById('<%= chk_settest.ClientID %>'); // give checkboxlist name
                                            var chks = chkBox.getElementsByTagName('INPUT');
                                            for (i = 0; i < chks.length; i++) {
                                                if (chks[i] != checker)
                                                    chks[i].checked = false;
                                            }
                                        }


                                    </script>

                                    <div class="form-group" id="div_form" runat="server" visible="false">
                                        <label class="col-sm-2 control-label">แนบฟอร์มอย่างง่าย</label>
                                        <div class="col-sm-10">
                                            <asp:FileUpload ID="fuAttachFile" ViewStateMode="Enabled"
                                                AutoPostBack="true" Font-Size="small" ClientIDMode="Static"
                                                runat="server" CssClass="btn btn-sm multi"
                                                accept="gif|jpg|pdf|png|pdf" />
                                            <p class="help-block">
                                                <font color="red">**แนบเอกสารเพิ่มเติม เฉพาะนามสกุล gif|jpg|pdf|png
                                                </font>
                                            </p>
                                        </div>
                                    </div>
                                    <hr />
                                    <!--START Detail Sample create-->
                                    <div class="form-group" id="show_detailinsert">
                                        <div class="panel-heading f-bold">รายละเอียดรายการตัวอย่าง</div>
                                    </div>

                                    <asp:Panel class="form-group" runat="server" ID="div_searchcreate">
                                        <label class="col-sm-2 control-label">ค้นหาเลข Mat.</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txt_material_search" runat="server" MaxLength="7"
                                                CssClass="form-control" OnTextChanged="TextBoxChanged"
                                                AutoPostBack="true" placeholder="Search Mat ...">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="Requiredtxt_material_search"
                                                ValidationGroup="SearchMat" runat="server" Display="None"
                                                ControlToValidate="txt_material_search" Font-Size="11"
                                                ErrorMessage="*กรุณากรอกเลข Mat. ที่ต้องการค้นหา!!!" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender105555"
                                                runat="Server" PopupPosition="TopLeft"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                TargetControlID="Requiredtxt_material_search" Width="160" />
                                        </div>
                                        <div class="col-sm-2 ">
                                            <div class="pull-left">
                                                <asp:LinkButton ID="btnSearchMat" CssClass="btn btn-primary"
                                                    runat="server" ValidationGroup="SearchMat" OnCommand="btnCommand"
                                                    CommandName="btnSearchMat" Font-Size="Small" data-toggle="tooltip"
                                                    title="Search"><span class="glyphicon glyphicon-search"></span>
                                                </asp:LinkButton>
                                            </div>

                                            <%-- </label>--%>
                                        </div>
                                        <label class="col-sm-4 control-label"></label>

                                    </asp:Panel>

                                </InsertItemTemplate>
                            </asp:FormView>

                            <asp:FormView ID="FvAddImport" runat="server" Width="100%" DefaultMode="Insert"
                                Visible="true">
                                <InsertItemTemplate>


                                    <asp:UpdatePanel ID="upActor1Node1Files11" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">แนบไฟล์ Excel</label>
                                                <div class="col-sm-10">
                                                    <asp:FileUpload ID="upload" runat="server" AutoPostBack="false"
                                                        Enabled="false" />

                                                    <asp:RequiredFieldValidator ID="requiredFileUpload" runat="server"
                                                        ValidationGroup="Create_Actor1file" Display="None"
                                                        SetFocusOnError="true" ControlToValidate="upload"
                                                        Font-Size="13px" ForeColor="Red"
                                                        ErrorMessage="กรุณาเลือกไฟล์" />
                                                    <ajaxToolkit:ValidatorCalloutExtender
                                                        ID="toolkitRequiredTxtEmpVisitorReason" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight"
                                                        TargetControlID="requiredFileUpload" Width="160"
                                                        PopupPosition="BottomRight" />
                                                </div>
                                            </div>

                                        </ContentTemplate>

                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnImport" />
                                        </Triggers>
                                    </asp:UpdatePanel>

                                    <asp:UpdatePanel ID="upActor1Node1Files1" runat="server">
                                        <ContentTemplate>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label"></label>
                                                <div class="col-sm-10">
                                                    <asp:LinkButton ID="btnImport" runat="server" Text="Import"
                                                        ValidationGroup="Create_Actor1file" OnCommand="btnCommand"
                                                        CommandName="btnImport" Visible="false"
                                                        CssClass="btn btn-success" />
                                                    <asp:LinkButton ID="lbDocCancelExcel" runat="server" Text="Cancel"
                                                        OnCommand="btnCommand" CommandName="cmdDocCancel"
                                                        CssClass="btn btn-danger" />
                                                </div>
                                            </div>
                                        </ContentTemplate>

                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnImport" />
                                        </Triggers>
                                    </asp:UpdatePanel>

                                    <asp:Panel ID="GvExcel_Import" runat="server" Visible="false">
                                        <asp:GridView ID="GvExcel_Show" runat="server" RowStyle-Font-Size="Small"
                                            HeaderStyle-Font-Size="Small" AutoGenerateColumns="false"
                                            RowStyle-VerticalAlign="NotSet" OnRowDataBound="gvRowDataBound"
                                            RowStyle-Wrap="true"
                                            CssClass="table table-striped table-bordered table-responsive"
                                            HeaderStyle-Height="25px" ShowHeaderWhenEmpty="True" ShowFooter="False"
                                            BorderStyle="None" CellSpacing="2">
                                            <EmptyDataTemplate>
                                                <div style="text-align: center">Data Cannot Be Found</div>
                                            </EmptyDataTemplate>
                                            <Columns>
                                                <asp:TemplateField HeaderText="#" HeaderStyle-CssClass="text-center"
                                                    HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <small>
                                                            <%# (Container.DataItemIndex +1) %>
                                                            <%--<asp:Label ID="lbl_material_code" runat="server" Visible="false" Text='<%# Eval("material_code")%>'>
                                                            DataKeyNames="material_code" </asp:Label>--%>
                                                        </small>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="ชื่อตัวอย่าง"
                                                    HeaderStyle-CssClass="text-left" HeaderStyle-Font-Size="Small"
                                                    ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <small>
                                                            <asp:Label ID="lbl_customer_name_excel" runat="server"
                                                                Text='<%# Eval("customer_name")%>'></asp:Label>
                                                        </small>
                                                    </ItemTemplate>
                                                    <EditItemTemplate />
                                                    <FooterTemplate />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="จุดปฏิบัติงาน"
                                                    HeaderStyle-CssClass="text-left" HeaderStyle-Font-Size="Small"
                                                    ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <small>
                                                            <asp:Label ID="lbl_other_details_excel" runat="server"
                                                                Text='<%# Eval("other_details")%>'></asp:Label>
                                                        </small>
                                                    </ItemTemplate>
                                                    <EditItemTemplate />
                                                    <FooterTemplate />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="อาคารผลิต"
                                                    HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small"
                                                    ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <small>
                                                            <asp:Label ID="lbl_rdept_idx_production_name_excel"
                                                                runat="server"
                                                                Text='<%# Eval("rdept_idx_production_name")%>'>
                                                            </asp:Label>
                                                        </small>
                                                    </ItemTemplate>
                                                    <EditItemTemplate />
                                                    <FooterTemplate />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="สายการผลิต"
                                                    HeaderStyle-CssClass="text-left" HeaderStyle-Font-Size="Small"
                                                    ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <small>
                                                            <asp:Label ID="lbl_rsec_idx_production_name_excel"
                                                                runat="server"
                                                                Text='<%# Eval("rsec_idx_production_name")%>'>
                                                            </asp:Label>
                                                        </small>
                                                    </ItemTemplate>
                                                    <EditItemTemplate />
                                                    <FooterTemplate />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="กะ" HeaderStyle-CssClass="text-left"
                                                    HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <small>
                                                            <asp:Label ID="lbl_shift_time_excel" runat="server"
                                                                Text='<%# Eval("shift_time")%>'></asp:Label>
                                                        </small>
                                                    </ItemTemplate>
                                                    <EditItemTemplate />
                                                    <FooterTemplate />
                                                </asp:TemplateField>

                                                <%-- <asp:TemplateField HeaderText="ลบ" HeaderStyle-CssClass="text-left" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>--%>
                                                <%--  <div class="panel-heading">--%>
                                                <%--<small>
                                                        <asp:LinkButton ID="btn_DeleteSample" runat="server" Text="" Font-Size="9px" CssClass="btn btn-danger btn-sm" OnCommand="btnCommand" CommandName="btn_DeleteSample" CommandArgument='<%# Eval("material_code") %>'
                                                OnClientClick="return confirm('Do you want delete this item?')"><span
                                                    class="glyphicon glyphicon-trash"></span>
                                                </asp:LinkButton>
                                                </small>--%>

                                                <%-- </div>--%>
                                                <%-- </ItemTemplate>
                                                <EditItemTemplate />
                                                <FooterTemplate />
                                            </asp:TemplateField>--%>
                                            </Columns>
                                        </asp:GridView>
                                    </asp:Panel>

                                    <asp:Panel ID="Dept_Excel" runat="server" Visible="false">

                                        <div class="form-group">
                                            <div class="panel-heading f-bold">แผนกที่เห็นผลการตรวจสอบ</div>
                                        </div>

                                        <div class="form-group">

                                            <%-- <asp:UpdatePanel ID="update_test11" runat="server" UpdateMode="Always">
                                        <ContentTemplate>--%>
                                            <label class="col-sm-2 control-label">องค์กร</label>
                                            <div class="col-sm-4">
                                                <asp:DropDownList ID="ddlOrg_excel" runat="server"
                                                    CssClass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true"
                                                    ValidationGroup="saveInsertDeptexcel" />
                                                <asp:RequiredFieldValidator ID="RequiredddlOrg_excel"
                                                    ValidationGroup="saveInsertDeptexcel" runat="server" Display="None"
                                                    ControlToValidate="ddlOrg_excel" Font-Size="11"
                                                    SetFocusOnError="true" ErrorMessage="*กรุณาเลือกองค์กร"
                                                    ValidationExpression="*กรุณาเลือกองค์กร" InitialValue="-1" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11"
                                                    runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                    TargetControlID="RequiredddlOrg_excel" Width="160" />
                                            </div>
                                            <%--    </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ddlOrg_excel" EventName="SelectedIndexChanged" />
                                        </Triggers>
                                    </asp:UpdatePanel>--%>
                                            <label class="col-sm-2 control-label">ฝ่าย</label>
                                            <div class="col-sm-4">
                                                <asp:DropDownList ID="ddlDept_excel" runat="server"
                                                    CssClass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true"
                                                    ValidationGroup="saveInsertDeptexcel" />
                                                <asp:RequiredFieldValidator ID="RequiredddlDept_excel"
                                                    ValidationGroup="saveInsertDeptexcel" runat="server" Display="None"
                                                    ControlToValidate="ddlDept_excel" Font-Size="11" ErrorMessage="ฝ่าย"
                                                    ValidationExpression="ฝ่าย" InitialValue="-1" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12"
                                                    runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                    TargetControlID="RequiredddlDept_excel" Width="160" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">แผนก</label>
                                            <div class="col-sm-4">
                                                <asp:DropDownList ID="ddlSec_excel" runat="server"
                                                    CssClass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true"
                                                    ValidationGroup="saveInsertDeptexcel" />
                                                <asp:RequiredFieldValidator ID="RequiredddlSec_excel"
                                                    ValidationGroup="saveInsertDeptexcel" runat="server" Display="None"
                                                    ControlToValidate="ddlSec_excel" Font-Size="11" ErrorMessage="แผนก"
                                                    ValidationExpression="แผนก" InitialValue="-1" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3"
                                                    runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                    TargetControlID="RequiredddlSec_excel" Width="160" />
                                            </div>

                                            <div class="col-sm-2">
                                                <asp:LinkButton ID="btnInsertDeptExcel" CssClass="btn btn-default"
                                                    runat="server" CommandName="btnInsertDeptExcel"
                                                    OnCommand="btnCommand" ValidationGroup="saveInsertDeptexcel"
                                                    title="เพิ่มแผนก"><i class="fa fa-plus-square"
                                                        aria-hidden="true"></i> เพิ่ม</asp:LinkButton>
                                            </div>

                                            <label class="col-sm-4 control-label"></label>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"></label>
                                            <div class="col-sm-10">
                                                <asp:GridView ID="GvDeptExcel" runat="server" RowStyle-Font-Size="Small"
                                                    HeaderStyle-Font-Size="Small" RowStyle-Height="10%"
                                                    AutoGenerateColumns="false" RowStyle-VerticalAlign="NotSet"
                                                    CssClass="table table-striped table-bordered table-hover"
                                                    HeaderStyle-Height="25px" ShowFooter="False"
                                                    DataKeyNames="RSecIDX_Excel" BorderStyle="None" CellSpacing="2">
                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">Data Cannot Be Found</div>
                                                    </EmptyDataTemplate>
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="#" Visible="false">
                                                            <ItemTemplate>
                                                                <div class="panel-heading">
                                                                    <%# (Container.DataItemIndex +1) %>
                                                                    <asp:Label ID="lbl_RSecIDXExcel" runat="server"
                                                                        Visible="false"
                                                                        Text='<%# Eval("RSecIDX_Excel")%>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="ชื่อองค์กร">
                                                            <ItemTemplate>
                                                                <div class="panel-heading">
                                                                    <asp:Label ID="lbl_orgnameexcel" runat="server"
                                                                        Text='<%# Eval("OrgNameEN_Excel")%>'>
                                                                    </asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                            <EditItemTemplate />
                                                            <FooterTemplate />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="ชื่อฝ่าย">
                                                            <ItemTemplate>
                                                                <div class="panel-heading">
                                                                    <asp:Label ID="lbl_deptnameexcel" runat="server"
                                                                        Text='<%# Eval("DeptNameEN_Excel")%>'>
                                                                    </asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                            <EditItemTemplate />
                                                            <FooterTemplate />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="ชื่อแผนก">
                                                            <ItemTemplate>
                                                                <div class="panel-heading">
                                                                    <asp:Label ID="lbl_secnameexcel" runat="server"
                                                                        Text='<%# Eval("SecNameEN_Excel")%>'>
                                                                    </asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                            <EditItemTemplate />
                                                            <FooterTemplate />
                                                        </asp:TemplateField> <%--ControlStyle-Font-Size="1px"--%>
                                                        <asp:TemplateField HeaderText="ลบ">
                                                            <ItemTemplate>
                                                                <div class="panel-heading">
                                                                    <asp:LinkButton ID="btn_delete_departmentexcel"
                                                                        runat="server" Text=""
                                                                        CssClass="btn btn-sm btn-danger"
                                                                        OnCommand="btnCommand"
                                                                        CommandName="btn_delete_departmentexcel"
                                                                        CommandArgument='<%# Eval("RSecIDX_Excel") %>'
                                                                        OnClientClick="return confirm('Do you want delete this item?')">
                                                                        <span class="glyphicon glyphicon-trash"></span>
                                                                    </asp:LinkButton>

                                                                </div>
                                                            </ItemTemplate>
                                                            <EditItemTemplate />
                                                            <FooterTemplate />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>

                                    </asp:Panel>

                                    <asp:Panel ID="Save_Excel" runat="server" Visible="false">
                                        <div class="form-group" id="div_saveinsert" runat="server">
                                            <label class="col-sm-2 control-label"></label>
                                            <div class="col-sm-10">

                                                <asp:LinkButton ID="btnSaveFileExcel" runat="server" Text="Sagve"
                                                    data-original-title="Save" data-toggle="tooltip"
                                                    OnCommand="btnCommand" CommandName="cmdSaveFileExcel"
                                                    CssClass="btn btn-success" />
                                                <asp:LinkButton ID="lbDocCancelFileExcel" runat="server" Text="Cancel"
                                                    OnCommand="btnCommand" data-original-title="Cancel"
                                                    data-toggle="tooltip" CommandName="cmdDocCancel"
                                                    CssClass="btn btn-danger" />
                                            </div>
                                        </div>
                                    </asp:Panel>


                                </InsertItemTemplate>
                            </asp:FormView>

                            <asp:Panel class="form-group" ID="div_other_detail" runat="server" Visible="false">
                                <label class="col-sm-2 control-label">อื่นๆ</label>
                                <div class="col-sm-4 control-label textleft">
                                    <asp:RadioButton ID="rd_other_detail" Width="100%" runat="server"
                                        AutoPostBack="true" OnCheckedChanged="RadioButton_CheckedChanged"
                                        Text=" กรอกข้อมูลตัวอย่าง " GroupName="other_detail"
                                        ValidationGroup="SaveinsertSample" />
                                    <%--  <asp:RequiredFieldValidator ID="Requiredrd_other_detail" ValidationGroup="SaveinsertSample" runat="server" Display="None"
                                    ControlToValidate="rd_other_detail" Font-Size="11"
                                    ErrorMessage="*กรุณาเลือก อื่นๆ เพื่อกรอกข้อมูลของตัวอย่าง" />
                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="Server" PopupPosition="TopLeft" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredrd_other_detail" Width="160" />--%>
                                </div>

                                <label class="col-sm-6 control-label"></label>
                            </asp:Panel>

                            <asp:FormView ID="fvDetailSearchCreate" runat="server" Width="100%">
                                <InsertItemTemplate>
                                    <div class="form-group">
                                        <%--<div style="text-align: center; color: red"><b>-- ไม่พบข้อมูล Material -- </b></div>--%>
                                        <div class="col-sm-12">
                                            <label class="form-control">
                                                <div style="text-align: center; color: red"><b>-- ไม่พบข้อมูล Material
                                                        --</b></div>
                                            </label>
                                        </div>
                                    </div>
                                </InsertItemTemplate>
                            </asp:FormView>

                            <asp:FormView ID="fvDetailMat" runat="server" Width="100%">
                                <InsertItemTemplate>
                                    <div class="form-horizontal" role="form">

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Mat.</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="txt_material_code_create" runat="server" MaxLength="7"
                                                    CssClass="form-control" Text='<%# Eval("material_code") %>'
                                                    placeholder="Mat ..." />
                                                <asp:RequiredFieldValidator ID="Requiredtxt_material_code_create"
                                                    ValidationGroup="Saveinsert_Sample" runat="server" Display="None"
                                                    ControlToValidate="txt_material_code_create" Font-Size="11"
                                                    SetFocusOnError="true" ErrorMessage="*กรุณากรอกเลข Mat" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4"
                                                    runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                    PopupPosition="TopLeft"
                                                    TargetControlID="Requiredtxt_material_code_create" Width="160" />
                                            </div>

                                            <label class="col-sm-2 control-label">ชื่อตัวอย่าง</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="txt_samplename_create" MaxLength="100" runat="server"
                                                    CssClass="form-control" Text='<%# Eval("material_name") %>'
                                                    placeholder="ชื่อตัวอย่าง ..." />
                                                <asp:RequiredFieldValidator ID="Requiredtxt_samplename_create"
                                                    ValidationGroup="Saveinsert_Sample" runat="server" Display="None"
                                                    ControlToValidate="txt_samplename_create" Font-Size="11"
                                                    SetFocusOnError="true" ErrorMessage="*กรุณากรอกชื่อตัวอย่าง" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5"
                                                    runat="Server" PopupPosition="TopLeft"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    TargetControlID="Requiredtxt_samplename_create" Width="160" />
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">ประเภทตัวอย่าง</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="txt_typesample" runat="server" CssClass="form-control"
                                                    placeholder="ประเภทตัวอย่าง ..." />
                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="Saveinsert_Sample" runat="server" Display="None"
                                                    ControlToValidate="txt_material_code_create" Font-Size="11" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกเลข Mat" />--%>
                                                <%-- <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" TargetControlID="Requiredtxt_material_code_create" Width="160" />--%>
                                            </div>

                                            <label class="col-sm-6 control-label"></label>

                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-2">
                                                <asp:DropDownList ID="ddldate_sample1_idx_create" runat="server"
                                                    CssClass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                    AutoPostBack="true" />
                                                <%-- <asp:RequiredFieldValidator ID="Requiredddldate_sample1_idx_create" ValidationGroup="SaveinsertSample" runat="server" Display="None"
                                                    ControlToValidate="ddldate_sample1_idx_create" Font-Size="11"
                                                    ErrorMessage="*เลือกวันที่บนตัวอย่าง" SetFocusOnError="true"
                                                    ValidationExpression="*เลือกวันที่บนตัวอย่าง" InitialValue="-1" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" PopupPosition="TopLeft" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredddldate_sample1_idx_create" Width="160" />--%>
                                            </div>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="txt_data_sample1_create" runat="server" MaxLength="40"
                                                    placeholder="วันที่บนตัวอย่าง ..." CssClass="form-control" />
                                                <%--<asp:RequiredFieldValidator ID="Rqtxt_data_sample1_create" ValidationGroup="SaveinsertSample" runat="server" Display="None"
                                                    ControlToValidate="txt_data_sample1_create" Font-Size="11" SetFocusOnError="true"
                                                    ErrorMessage="*กรอกวันที่บนตัวอย่าง" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" PopupPosition="TopLeft" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Rqtxt_data_sample1_create" Width="160" />--%>
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:DropDownList ID="ddldate_sample2_idx_create" runat="server"
                                                    CssClass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                    AutoPostBack="true" />
                                                <%--<asp:RequiredFieldValidator ID="Requireddldate_sample2_idx_create" ValidationGroup="SaveinsertSample" runat="server" Display="None"
                                                    ControlToValidate="ddldate_sample2_idx_create" Font-Size="11" SetFocusOnError="true"
                                                    ErrorMessage="*เลือกวันที่บนตัวอย่าง"
                                                    ValidationExpression="*เลือกวันที่บนตัวอย่าง" InitialValue="-1" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender166" runat="Server" PopupPosition="TopLeft" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requireddldate_sample2_idx_create" Width="160" />--%>
                                            </div>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="txt_data_sample2_create" MaxLength="20"
                                                    placeholder="วันที่บนตัวอย่าง ..." runat="server"
                                                    CssClass="form-control" />
                                                <%--<asp:RequiredFieldValidator ID="Requiredtxt_data_sample2_create" ValidationGroup="SaveinsertSample" runat="server" Display="None"
                                                    ControlToValidate="txt_data_sample2_create" Font-Size="11" SetFocusOnError="true"
                                                    ErrorMessage="*กรอกวันที่บนตัวอย่าง" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" PopupPosition="TopLeft" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxt_data_sample2_create" Width="160" />--%>
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Batch</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="txt_batch_create" runat="server" MaxLength="10"
                                                    placeholder="Batch ..." CssClass="form-control" />
                                                <%--<asp:RequiredFieldValidator ID="Requiredtxt_batch_create" ValidationGroup="SaveinsertSample" runat="server" Display="None"
                                                    ControlToValidate="txt_batch_create" Font-Size="11" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอก Batch" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" PopupPosition="TopLeft" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxt_batch_create" Width="160" />--%>
                                            </div>

                                            <label class="col-sm-2 control-label">ชื่อลูกค้า</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="txt_customer_name_create" MaxLength="50"
                                                    placeholder="ชื่อลูกค้า ..." runat="server"
                                                    CssClass="form-control" />
                                                <%--<asp:RequiredFieldValidator ID="Requiredtxt_customer_name_create" ValidationGroup="SaveinsertSample" runat="server" Display="None"
                                                    ControlToValidate="txt_customer_name_create" Font-Size="11" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกชื่อลูกค้า" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" PopupPosition="TopLeft" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxt_customer_name_create" Width="160" />--%>
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">เลขตู้</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="txt_cabinet_create" runat="server" MaxLength="10"
                                                    placeholder="เลขตู้ ..." CssClass="form-control" />
                                                <%--<asp:RequiredFieldValidator ID="Requiredtxt_cabinet_create" ValidationGroup="SaveinsertSample" runat="server" Display="None"
                                                    ControlToValidate="txt_cabinet_create" Font-Size="11" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกเลขตู้" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server" PopupPosition="TopLeft" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxt_cabinet_create" Width="160" />--%>
                                            </div>

                                            <label class="col-sm-2 control-label">กะ</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="txt_shift_time_create" runat="server" MaxLength="5"
                                                    placeholder="กะ ..." CssClass="form-control" />
                                                <%--<asp:RequiredFieldValidator ID="Requiredtxt_shift_time_create" ValidationGroup="SaveinsertSample" runat="server" Display="None"
                                                    ControlToValidate="txt_shift_time_create" Font-Size="11" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกกะ" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server" PopupPosition="TopLeft" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxt_shift_time_create" Width="160" />--%>
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">เวลา</label>
                                            <div class="col-sm-4">
                                                <div class="input-group clockpicker">
                                                    <asp:HiddenField ID="HiddenTimeCreate" runat="server" />
                                                    <asp:TextBox ID="txt_time_create" placeholder="เวลา ..."
                                                        runat="server" CssClass="form-control" value="" /><span
                                                        class="input-group-addon"><span
                                                            class="glyphicon glyphicon-time"></span></span>

                                                </div>
                                            </div>

                                            <label class="col-sm-2 control-label">รายละเอียดอื่นๆ</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="txt_other_details_create"
                                                    placeholder="รายละเอียดอื่นๆ ..." runat="server"
                                                    CssClass="form-control" />

                                            </div>

                                        </div>

                                        <%--                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Certificate</label>
                                        <div class="col-sm-4 control-label textleft">
                                            <asp:RadioButtonList ID="rd_certificate" runat="server" CssClass="radioButtonList">
                                                <asp:ListItem Text="มี certificate" Value="1" />
                                                <asp:ListItem Text="ไม่มี certificate" Value="0" />
                                            </asp:RadioButtonList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldrd_certificate" runat="server"
                                                ControlToValidate="rd_certificate" Display="None" SetFocusOnError="true"
                                                ErrorMessage="กรุณาเลือก certificate" ValidationGroup="SaveinsertSample" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender222" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldrd_certificate" PopupPosition="TopLeft" Width="200" />

                                        </div>

                                        <label class="col-sm-6 control-label"></label>

                                    </div>--%>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">องค์กรสายการผลิต</label>
                                            <div class="col-sm-4">
                                                <asp:DropDownList ID="ddlOrg_samplecreate" runat="server"
                                                    CssClass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                    AutoPostBack="true" />
                                                <%--<asp:RequiredFieldValidator ID="RequiredddlOrg_create" ValidationGroup="Create_Dept" runat="server" Display="None"
                                                    ControlToValidate="ddlOrg_create" Font-Size="11"
                                                    ErrorMessage="*กรุณาเลือกองค์กร"
                                                    ValidationExpression="*กรุณาเลือกองค์กร" InitialValue="-1" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" PopupPosition="TopLeft" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredddlOrg_create" Width="160" />--%>
                                            </div>

                                            <label class="col-sm-2 control-label">ฝ่ายสายการผลิต</label>
                                            <div class="col-sm-4">
                                                <asp:DropDownList ID="ddlDept_samplecreate" runat="server"
                                                    CssClass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                    AutoPostBack="true" />
                                                <%--<asp:RequiredFieldValidator ID="RequiredddlDept_create" ValidationGroup="Create_Dept" runat="server" Display="None"
                                                    ControlToValidate="ddlDept_create" Font-Size="11"
                                                    ErrorMessage="*กรุณาเลือกฝ่าย"
                                                    ValidationExpression="*กรุณาเลือกฝ่าย" InitialValue="-1" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" PopupPosition="TopLeft" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredddlDept_create" Width="160" />--%>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">แผนกสายการผลิต</label>
                                            <div class="col-sm-4">
                                                <asp:DropDownList ID="ddlSec_samplecreate" runat="server"
                                                    CssClass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                    AutoPostBack="true" />
                                                <%--<asp:RequiredFieldValidator ID="RequiredddlSec_create" ValidationGroup="Create_Dept" runat="server" Display="None"
                                                    ControlToValidate="ddlSec_create" Font-Size="11"
                                                    ErrorMessage="*กรุณาเลือกแผนก"
                                                    ValidationExpression="*กรุณาเลือกแผนก" InitialValue="-1" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" PopupPosition="TopLeft" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredddlSec_create" Width="160" />--%>
                                            </div>

                                            <label class="col-sm-6 control-label"></label>


                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"></label>
                                            <div class="col-sm-4">
                                                <asp:LinkButton ID="btnInsertSample" CssClass="btn btn-default"
                                                    runat="server" CommandName="btnInsertSample" OnCommand="btnCommand"
                                                    ValidationGroup="Saveinsert_Sample" title="เพิ่มตัวอย่าง"><i
                                                        class="fa fa-plus-square" aria-hidden="true"></i> เพิ่ม
                                                </asp:LinkButton>
                                            </div>

                                            <label class="col-sm-6 control-label"></label>

                                        </div>


                                    </div>
                                </InsertItemTemplate>
                                <ItemTemplate>
                                    <div class="form-horizontal" role="form">

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Mat.</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="txt_material_code_create" runat="server" MaxLength="7"
                                                    Enabled="false" CssClass="form-control"
                                                    Text='<%# Eval("material_code") %>' placeholder="Mat ..." />
                                                <asp:RequiredFieldValidator ID="Requiredtxt_material_code_create"
                                                    ValidationGroup="Saveinsert_Sample" runat="server" Display="None"
                                                    ControlToValidate="txt_material_code_create" Font-Size="11"
                                                    SetFocusOnError="true" ErrorMessage="*กรุณากรอกเลข Mat" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4"
                                                    runat="Server" PopupPosition="TopLeft"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    TargetControlID="Requiredtxt_material_code_create" Width="160" />
                                            </div>

                                            <label class="col-sm-2 control-label">ชื่อตัวอย่าง</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="txt_samplename_create" runat="server"
                                                    CssClass="form-control" Enabled="false"
                                                    Text='<%# Eval("material_name") %>'
                                                    placeholder="ชื่อตัวอย่าง ..." />
                                                <asp:RequiredFieldValidator ID="Requiredtxt_samplename_create"
                                                    ValidationGroup="Saveinsert_Sample" runat="server" Display="None"
                                                    ControlToValidate="txt_samplename_create" Font-Size="11"
                                                    SetFocusOnError="true" ErrorMessage="*กรุณากรอกชื่อตัวอย่าง" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5"
                                                    runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                    PopupPosition="TopLeft"
                                                    TargetControlID="Requiredtxt_samplename_create" Width="160" />
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">ประเภทตัวอย่าง</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="txt_typesample" runat="server" CssClass="form-control"
                                                    Enabled="false" placeholder="ประเภทตัวอย่าง ..." />
                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="Saveinsert_Sample" runat="server" Display="None"
                                                    ControlToValidate="txt_material_code_create" Font-Size="11" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกเลข Mat" />--%>
                                                <%-- <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" TargetControlID="Requiredtxt_material_code_create" Width="160" />--%>
                                            </div>

                                            <label class="col-sm-6 control-label"></label>

                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-2">
                                                <asp:DropDownList ID="ddldate_sample1_idx_create" runat="server"
                                                    CssClass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                    AutoPostBack="true" />
                                                <%--   <asp:RequiredFieldValidator ID="Requiredddldate_sample1_idx_create" ValidationGroup="SaveinsertSample" runat="server" Display="None"
                                                    ControlToValidate="ddldate_sample1_idx_create" Font-Size="11"
                                                    ErrorMessage="*กรุณาเลือกวันที่บนตัวอย่าง" SetFocusOnError="true"
                                                    ValidationExpression="*กรุณาเลือกวันที่บนตัวอย่าง" InitialValue="-1" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" TargetControlID="Requiredddldate_sample1_idx_create" Width="160" />--%>
                                            </div>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="txt_data_sample1_create" runat="server" MaxLength="40"
                                                    placeholder="วันที่บนตัวอย่าง ..." CssClass="form-control" />
                                                <%-- <asp:RequiredFieldValidator ID="Rqtxt_data_sample1_create" ValidationGroup="SaveinsertSample" runat="server" Display="None"
                                                    ControlToValidate="txt_data_sample1_create" Font-Size="11" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกวันที่บนตัวอย่าง" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" PopupPosition="TopLeft" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Rqtxt_data_sample1_create" Width="160" />--%>
                                            </div>

                                            <div class="col-sm-2">
                                                <asp:DropDownList ID="ddldate_sample2_idx_create" runat="server"
                                                    CssClass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                    AutoPostBack="true" />
                                                <%--<asp:RequiredFieldValidator ID="Requireddldate_sample2_idx_create" ValidationGroup="SaveinsertSample" runat="server" Display="None"
                                                    ControlToValidate="ddldate_sample2_idx_create" Font-Size="11" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกเลือกวันที่บนตัวอย่าง"
                                                    ValidationExpression="*กรุณาเลือกวันที่บนตัวอย่าง" InitialValue="-1" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender166" runat="Server" PopupPosition="TopLeft" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requireddldate_sample2_idx_create" Width="160" />--%>
                                            </div>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="txt_data_sample2_create" MaxLength="20"
                                                    placeholder="วันที่บนตัวอย่าง ..." runat="server"
                                                    CssClass="form-control" />
                                                <%--<asp:RequiredFieldValidator ID="Requiredtxt_data_sample2_create" ValidationGroup="SaveinsertSample" runat="server" Display="None"
                                                    ControlToValidate="txt_data_sample2_create" Font-Size="11" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกวันที่บนตัวอย่าง" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" PopupPosition="TopLeft" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxt_data_sample2_create" Width="160" />--%>
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Batch</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="txt_batch_create" runat="server" MaxLength="10"
                                                    placeholder="Batch ..." CssClass="form-control" />
                                                <asp:RequiredFieldValidator ID="Requiredtxt_batch_create"
                                                    ValidationGroup="SaveinsertSample" runat="server" Display="None"
                                                    ControlToValidate="txt_batch_create" Font-Size="11"
                                                    SetFocusOnError="true" ErrorMessage="*กรุณากรอก Batch" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1"
                                                    runat="Server" PopupPosition="TopLeft"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    TargetControlID="Requiredtxt_batch_create" Width="160" />
                                            </div>

                                            <label class="col-sm-2 control-label">ชื่อลูกค้า</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="txt_customer_name_create" MaxLength="50"
                                                    placeholder="ชื่อลูกค้า ..." runat="server"
                                                    CssClass="form-control" />
                                                <asp:RequiredFieldValidator ID="Requiredtxt_customer_name_create"
                                                    ValidationGroup="SaveinsertSample" runat="server" Display="None"
                                                    ControlToValidate="txt_customer_name_create" Font-Size="11"
                                                    SetFocusOnError="true" ErrorMessage="*กรุณากรอกชื่อลูกค้า" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6"
                                                    runat="Server" PopupPosition="TopLeft"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    TargetControlID="Requiredtxt_customer_name_create" Width="160" />
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">เลขตู้</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="txt_cabinet_create" runat="server" MaxLength="10"
                                                    placeholder="เลขตู้ ..." CssClass="form-control" />
                                                <asp:RequiredFieldValidator ID="Requiredtxt_cabinet_create"
                                                    ValidationGroup="SaveinsertSample" runat="server" Display="None"
                                                    ControlToValidate="txt_cabinet_create" Font-Size="11"
                                                    ErrorMessage="*กรุณากรอกเลขตู้" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8"
                                                    runat="Server" PopupPosition="TopLeft"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    TargetControlID="Requiredtxt_cabinet_create" Width="160" />
                                            </div>

                                            <label class="col-sm-2 control-label">กะ</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="txt_shift_time_create" runat="server" MaxLength="5"
                                                    placeholder="กะ ..." CssClass="form-control" />
                                                <asp:RequiredFieldValidator ID="Requiredtxt_shift_time_create"
                                                    ValidationGroup="SaveinsertSample" runat="server" Display="None"
                                                    ControlToValidate="txt_shift_time_create" Font-Size="11"
                                                    ErrorMessage="*กรุณากรอกกะ" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9"
                                                    runat="Server" PopupPosition="TopLeft"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    TargetControlID="Requiredtxt_shift_time_create" Width="160" />
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">เวลา</label>
                                            <div class="col-sm-4">
                                                <div class="input-group clockpicker">
                                                    <asp:HiddenField ID="HiddenTimeCreate" runat="server" />
                                                    <asp:TextBox ID="txt_time_create" placeholder="เวลา ..."
                                                        MaxLength="5" runat="server" CssClass="form-control" value="" />
                                                    <span class="input-group-addon"><span
                                                            class="glyphicon glyphicon-time"></span></span>
                                                    <%--<asp:RequiredFieldValidator ID="Requiredtxt_time_create" ValidationGroup="SaveinsertSample" runat="server" Display="None"
                                                        ControlToValidate="txt_time_create" Font-Size="11"
                                                        ErrorMessage="*กรุณากรอกเวลา" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="Server" PopupPosition="TopLeft" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxt_time_create" Width="160" />--%>
                                                </div>
                                            </div>

                                            <label class="col-sm-2 control-label">รายละเอียดอื่นๆ</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="txt_other_details_create" MaxLength="50"
                                                    placeholder="รายละเอียดอื่นๆ ..." runat="server"
                                                    CssClass="form-control" />

                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">องค์กรสายการผลิต</label>
                                            <div class="col-sm-4">
                                                <asp:DropDownList ID="ddlOrg_samplecreate" runat="server"
                                                    CssClass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                    AutoPostBack="true" />
                                                <%--<asp:RequiredFieldValidator ID="RequiredddlOrg_create" ValidationGroup="Create_Dept" runat="server" Display="None"
                                                    ControlToValidate="ddlOrg_create" Font-Size="11"
                                                    ErrorMessage="*กรุณาเลือกองค์กร"
                                                    ValidationExpression="*กรุณาเลือกองค์กร" InitialValue="-1" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" PopupPosition="TopLeft" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredddlOrg_create" Width="160" />--%>
                                            </div>

                                            <label class="col-sm-2 control-label">ฝ่ายสายการผลิต</label>
                                            <div class="col-sm-4">
                                                <asp:DropDownList ID="ddlDept_samplecreate" runat="server"
                                                    CssClass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                    AutoPostBack="true" />
                                                <%--<asp:RequiredFieldValidator ID="RequiredddlDept_create" ValidationGroup="Create_Dept" runat="server" Display="None"
                                                    ControlToValidate="ddlDept_create" Font-Size="11"
                                                    ErrorMessage="*กรุณาเลือกฝ่าย"
                                                    ValidationExpression="*กรุณาเลือกฝ่าย" InitialValue="-1" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" PopupPosition="TopLeft" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredddlDept_create" Width="160" />--%>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">แผนกสายการผลิต</label>
                                            <div class="col-sm-4">
                                                <asp:DropDownList ID="ddlSec_samplecreate" runat="server"
                                                    CssClass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                    AutoPostBack="true" />
                                                <%--<asp:RequiredFieldValidator ID="RequiredddlSec_create" ValidationGroup="Create_Dept" runat="server" Display="None"
                                                    ControlToValidate="ddlSec_create" Font-Size="11"
                                                    ErrorMessage="*กรุณาเลือกแผนก"
                                                    ValidationExpression="*กรุณาเลือกแผนก" InitialValue="-1" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" PopupPosition="TopLeft" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredddlSec_create" Width="160" />--%>
                                            </div>

                                            <label class="col-sm-6 control-label"></label>


                                        </div>

                                        <%--                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Certificate</label>
                                        <div class="col-sm-4 control-label textleft">
                                            <asp:RadioButtonList ID="rd_certificate" runat="server" CssClass="radioButtonList">
                                                <asp:ListItem Text="มี certificate" Value="1" />
                                                <asp:ListItem Text="ไม่มี certificate" Value="0" />
                                            </asp:RadioButtonList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldrd_certificate" runat="server"
                                                ControlToValidate="rd_certificate" Display="None" SetFocusOnError="true"
                                                ErrorMessage="กรุณาเลือก certificate" ValidationGroup="SaveinsertSample" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender222" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldrd_certificate" Width="200" />
                                        </div>

                                        <label class="col-sm-6 control-label"></label>

                                    </div>--%>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"></label>
                                            <div class="col-sm-4">
                                                <asp:LinkButton ID="btnInsertSample" CssClass="btn btn-default"
                                                    runat="server" CommandName="btnInsertSample" OnCommand="btnCommand"
                                                    ValidationGroup="Saveinsert_Sample" title="เพิ่มตัวอย่าง"><i
                                                        class="fa fa-plus-square" aria-hidden="true"></i> เพิ่ม
                                                </asp:LinkButton>
                                            </div>

                                            <label class="col-sm-6 control-label"></label>

                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:FormView>

                            <%--<div class="form-group col-md-12" id="gvsample_show" runat="server">--%>
                            <div id="divGvSample_scroll" style="overflow-x: scroll; width: 100%" visible="false"
                                runat="server">
                                <asp:GridView ID="GvSample" runat="server" RowStyle-Font-Size="Small"
                                    HeaderStyle-Font-Size="Small" AutoGenerateColumns="false"
                                    RowStyle-VerticalAlign="NotSet" OnRowDataBound="gvRowDataBound"
                                    OnRowDeleting="gvRowDeleted" OnRowEditing="gvRowEditing"
                                    OnRowUpdating="gvRowUpdating" OnRowCancelingEdit="gvRowCancelingEdit"
                                    RowStyle-Wrap="true" CssClass="table table-striped table-bordered table-responsive"
                                    HeaderStyle-Height="25px" DataKeyNames="material_code" ShowHeaderWhenEmpty="True"
                                    ShowFooter="False" BorderStyle="None" CellSpacing="2">
                                    <EmptyDataTemplate>
                                        <div style="text-align: center">Data Cannot Be Found</div>
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:TemplateField HeaderText="#" HeaderStyle-CssClass="text_left"
                                            HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="left">
                                            <ItemTemplate>
                                                <small>

                                                    <%# (Container.DataItemIndex +1) %>

                                                    <asp:Label ID="lbl_material_code" runat="server" Visible="false"
                                                        Text='<%# Eval("material_code")%>'></asp:Label>

                                                </small>
                                            </ItemTemplate>

                                            <EditItemTemplate>
                                                <div class="panel-body">
                                                    <div class="form-horizontal" role="form">

                                                        <div class="form-group">
                                                            <asp:Label ID="lbl_material_codeedit"
                                                                CssClass="col-sm-2 control-label" runat="server"
                                                                Text="Mat." />
                                                            <div class="col-sm-4">
                                                                <asp:TextBox ID="txt_material_code_create_edit"
                                                                    runat="server" Visible="true" Enabled="false"
                                                                    CssClass="form-control"
                                                                    Text='<%# Eval("material_code")%>' />

                                                            </div>
                                                            <asp:Label ID="lbl_samplename_create_edit"
                                                                CssClass="col-sm-2 control-label" runat="server"
                                                                Text="ชื่อตัวอย่าง" />
                                                            <div class="col-sm-4">
                                                                <asp:TextBox ID="txt_samplename_create_edit"
                                                                    MaxLength="100" runat="server" Visible="true"
                                                                    Enabled="false" CssClass="form-control"
                                                                    Text='<%# Eval("samplename")%>' />

                                                            </div>

                                                        </div>

                                                        <div class="form-group">
                                                            <asp:Label ID="lbl_txt_typesample_edit"
                                                                CssClass="col-sm-2 control-label" runat="server"
                                                                Text="ประเภทตัวอย่าง" />
                                                            <div class="col-sm-4">
                                                                <asp:TextBox ID="txt_txt_typesample_edit" runat="server"
                                                                    Visible="true" Enabled="false"
                                                                    CssClass="form-control"
                                                                    Text='<%# Eval("txt_typesample")%>' />
                                                            </div>

                                                            <asp:Label ID="Label8" CssClass="col-sm-6 control-label"
                                                                runat="server" />

                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txt_sample1_idx_edit" MaxLength="10"
                                                                    Visible="false" Enabled="false"
                                                                    Text='<%# Eval("sample1_idx") %>' runat="server"
                                                                    CssClass="form-control" />
                                                                <asp:DropDownList ID="ddldate_sample1_idx_create_edit"
                                                                    runat="server" CssClass="form-control"
                                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                                    AutoPostBack="true" />

                                                            </div>
                                                            <div class="col-sm-4">
                                                                <asp:TextBox ID="txt_data_sample1_edit" runat="server"
                                                                    MaxLength="40" placeholder="วันที่บนตัวอย่าง ..."
                                                                    CssClass="form-control"
                                                                    Text='<%# Eval("data_sample1")%>' />

                                                            </div>

                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txt_sample2_idx_edit" MaxLength="10"
                                                                    placeholder="วันที่บนตัวอย่าง ..." Visible="false"
                                                                    Enabled="false" Text='<%# Eval("sample2_idx") %>'
                                                                    runat="server" CssClass="form-control" />
                                                                <asp:DropDownList ID="ddldate_sample2_idx_create_edit"
                                                                    runat="server" CssClass="form-control"
                                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                                    AutoPostBack="true" />

                                                            </div>
                                                            <div class="col-sm-4">
                                                                <asp:TextBox ID="txt_data_sample2_create_edit"
                                                                    MaxLength="20" placeholder="วันที่บนตัวอย่าง ..."
                                                                    runat="server" CssClass="form-control"
                                                                    Text='<%# Eval("data_sample2")%>' />

                                                            </div>

                                                        </div>

                                                        <div class="form-group">
                                                            <asp:Label ID="lbl_batch_edit"
                                                                CssClass="col-sm-2 control-label" runat="server"
                                                                Text="Batch" />
                                                            <div class="col-sm-4">
                                                                <asp:TextBox ID="txt_batch_edit" runat="server"
                                                                    Visible="true" CssClass="form-control"
                                                                    Text='<%# Eval("batch")%>' />
                                                            </div>
                                                            <asp:Label ID="lbl_customer_name_edit"
                                                                CssClass="col-sm-2 control-label" runat="server"
                                                                Text="ชื่อลูกค้า" />
                                                            <div class="col-sm-4">
                                                                <asp:TextBox ID="txt_customer_name_edit" runat="server"
                                                                    Visible="true" CssClass="form-control"
                                                                    Text='<%# Eval("customer_name")%>' />

                                                            </div>

                                                        </div>

                                                        <div class="form-group">
                                                            <asp:Label ID="lbl_cabinet_edit"
                                                                CssClass="col-sm-2 control-label" runat="server"
                                                                Text="เลขตู้" />
                                                            <div class="col-sm-4">
                                                                <asp:TextBox ID="txt_cabinet_edit" runat="server"
                                                                    Visible="true" CssClass="form-control"
                                                                    Text='<%# Eval("cabinet")%>' />

                                                            </div>
                                                            <asp:Label ID="lbl_shift_time_edit"
                                                                CssClass="col-sm-2 control-label" runat="server"
                                                                Text="กะ" />
                                                            <div class="col-sm-4">
                                                                <asp:TextBox ID="txt_shift_time_edit" runat="server"
                                                                    Visible="true" CssClass="form-control"
                                                                    Text='<%# Eval("shift_time")%>' />

                                                            </div>

                                                        </div>

                                                        <div class="form-group">
                                                            <asp:Label ID="lbl_time_edit"
                                                                CssClass="col-sm-2 control-label" runat="server"
                                                                Text="เวลา" />
                                                            <div class="col-sm-4">
                                                                <div class="input-group clockpicker">
                                                                    <asp:TextBox ID="txt_time_edit_create"
                                                                        placeholder="เวลา ..." runat="server"
                                                                        Text='<%# Eval("time") %>' ReadOnly="false"
                                                                        Enabled="true" CssClass="form-control"
                                                                        value="" /><span class="input-group-addon"><span
                                                                            class="glyphicon glyphicon-time"></span></span>

                                                                </div>
                                                            </div>

                                                            <asp:Label ID="lbl_other_details_edit"
                                                                CssClass="col-sm-2 control-label" runat="server"
                                                                Text="รายละเอียดอื่นๆ" />
                                                            <div class="col-sm-4">
                                                                <asp:TextBox ID="txt_other_details_edit_create"
                                                                    runat="server" Visible="true"
                                                                    CssClass="form-control"
                                                                    Text='<%# Eval("other_details")%>' />

                                                            </div>

                                                        </div>

                                                        <div class="form-group">
                                                            <label
                                                                class="col-sm-2 control-label">องค์กรสายการผลิต</label>
                                                            <div class="col-sm-4">
                                                                <asp:TextBox ID="txt_org_idx_production_edit_create"
                                                                    MaxLength="10" placeholder="องค์กรสายการผลิต ..."
                                                                    Visible="false"
                                                                    Text='<%# Eval("ddlOrg_idxsamplecreate") %>'
                                                                    runat="server" CssClass="form-control" />
                                                                <asp:DropDownList ID="ddlorg_idx_production_edit_create"
                                                                    runat="server" CssClass="form-control"
                                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                                    AutoPostBack="true" />

                                                            </div>

                                                            <label class="col-sm-2 control-label">ฝ่ายสายการผลิต</label>
                                                            <div class="col-sm-4">
                                                                <asp:TextBox ID="txt_rdept_idx_production_edit_create"
                                                                    MaxLength="10" placeholder="ฝ่ายสายการผลิต ..."
                                                                    Visible="false"
                                                                    Text='<%# Eval("ddlDept_idxsamplecreate") %>'
                                                                    runat="server" CssClass="form-control" />
                                                                <asp:DropDownList
                                                                    ID="ddlrdept_idx_production_edit_create"
                                                                    runat="server" CssClass="form-control"
                                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                                    AutoPostBack="true" />

                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">แผนกสายการผลิต</label>
                                                            <div class="col-sm-4">
                                                                <asp:TextBox ID="txt_rsec_idx_production_edit_create"
                                                                    MaxLength="10" placeholder="ฝ่ายสายการผลิต ..."
                                                                    Visible="false"
                                                                    Text='<%# Eval("ddlSec_idxsamplecreate") %>'
                                                                    runat="server" CssClass="form-control" />
                                                                <asp:DropDownList
                                                                    ID="ddlrsec_idx_production_edit_create"
                                                                    runat="server" CssClass="form-control"
                                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                                    AutoPostBack="true" />
                                                            </div>

                                                            <div class="col-sm-6"></div>

                                                        </div>
                                                        <%-- <i class="fa fa-check"></i>--%>
                                                        <%--<i class="fa fa-times"></i>--%>
                                                        <div class="form-group">
                                                            <div class="col-sm-2"></div>
                                                            <div class="col-sm-10">
                                                                <asp:LinkButton ID="btnupdate"
                                                                    CssClass="btn btn-success" runat="server"
                                                                    CommandName="Update"
                                                                    OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')">
                                                                    แก้ไข</asp:LinkButton>

                                                                <asp:LinkButton ID="lbCmdCancel"
                                                                    CssClass="btn btn-danger" runat="server"
                                                                    CommandName="Cancel"> ยกเลิก
                                                                </asp:LinkButton>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </EditItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ชื่อตัวอย่าง" HeaderStyle-CssClass="text-left"
                                            HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lbl_samplename" runat="server"
                                                        Text='<%# Eval("samplename")%>'></asp:Label>
                                                </small>
                                            </ItemTemplate>
                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Mat." HeaderStyle-CssClass="text-left"
                                            HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lbl_material_code_idx" runat="server"
                                                        Text='<%# Eval("material_code")%>'></asp:Label>
                                                </small>
                                            </ItemTemplate>
                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ประเภทตัวอย่าง" HeaderStyle-CssClass="text-left"
                                            HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lbl_txt_typesample" runat="server"
                                                        Text='<%# Eval("txt_typesample")%>'></asp:Label>
                                                </small>
                                            </ItemTemplate>
                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ชุดการตรวจ" HeaderStyle-CssClass="text-left"
                                            HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <small>
                                                    <%--<asp:Label ID="lbl_material_code_idx" runat="server" Text='<%# Eval("material_code")%>'>
                                                    </asp:Label>--%>
                                                    <asp:Label ID="lbl_chk_settestidx_create" runat="server"
                                                        Visible="false" Text='<%# Eval("chk_settestidx_create")%>'>
                                                    </asp:Label>
                                                    <asp:Label ID="lbl_chk_settest_create" runat="server"
                                                        Text='<%# Eval("chk_settest_create")%>'></asp:Label>
                                                </small>
                                            </ItemTemplate>
                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="รายการที่ตรวจ" HeaderStyle-CssClass="text-left"
                                            HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <small>
                                                    <%--<asp:Label ID="lbl_material_code_idx" runat="server" Text='<%# Eval("material_code")%>'>
                                                    </asp:Label>--%>
                                                    <asp:Label ID="lbl_chk_testidx_create" runat="server"
                                                        Visible="false" Text='<%# Eval("chk_testidx_create")%>'>
                                                    </asp:Label>
                                                    <asp:Label ID="lbl_chk_test_create" runat="server"
                                                        Text='<%# Eval("chk_test_create")%>'></asp:Label>
                                                </small>
                                            </ItemTemplate>
                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Date of sample(1)"
                                            HeaderStyle-CssClass="text-left" HeaderStyle-Font-Size="Small"
                                            ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lbl_sample1_idx" runat="server" Visible="false"
                                                        Text='<%# Eval("sample1_idx")%>'></asp:Label>
                                                    <asp:Label ID="lbl_sample1_idx_name" runat="server"
                                                        Text='<%# Eval("sample1_idx_name")%>'></asp:Label>
                                                    <asp:Label ID="lbl_data_sample1" runat="server"
                                                        Text='<%# Eval("data_sample1")%>'></asp:Label>
                                                </small>
                                            </ItemTemplate>
                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Date of sample(2)"
                                            HeaderStyle-CssClass="text-left" HeaderStyle-Font-Size="Small"
                                            ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lbl_sample2_idx" runat="server" Visible="false"
                                                        Text='<%# Eval("sample2_idx")%>'></asp:Label>
                                                    <asp:Label ID="lbl_sample2_idx_name" runat="server"
                                                        Text='<%# Eval("sample2_idx_name")%>'></asp:Label>
                                                    <asp:Label ID="lbl_data_sample2" runat="server"
                                                        Text='<%# Eval("data_sample2")%>'></asp:Label>
                                                </small>
                                            </ItemTemplate>
                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Batch" HeaderStyle-CssClass="text-left"
                                            HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lbl_batch" runat="server" Text='<%# Eval("batch")%>'>
                                                    </asp:Label>
                                                </small>
                                            </ItemTemplate>
                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ชื่อลูกค้า" HeaderStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lbl_customer_name" runat="server"
                                                        Text='<%# Eval("customer_name")%>'></asp:Label>
                                                </small>
                                            </ItemTemplate>
                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="เลขตู้" HeaderStyle-CssClass="text-left"
                                            HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lbl_cabinet" runat="server"
                                                        Text='<%# Eval("cabinet")%>'></asp:Label>
                                                </small>
                                            </ItemTemplate>
                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="กะ" HeaderStyle-CssClass="text-left"
                                            HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lbl_shift_time" runat="server"
                                                        Text='<%# Eval("shift_time")%>'></asp:Label>
                                                </small>
                                            </ItemTemplate>
                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="เวลา" HeaderStyle-CssClass="text-left"
                                            HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <%-- <div class="panel-heading">--%>
                                                <small>
                                                    <asp:Label ID="lbl_time" runat="server" Text='<%# Eval("time")%>'>
                                                    </asp:Label>
                                                </small>
                                                <%-- </div>--%>
                                            </ItemTemplate>
                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="อื่นๆ" HeaderStyle-CssClass="text-center"
                                            HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <%--<div class="panel-heading">--%>
                                                <small>
                                                    <asp:Label ID="lbl_other_details" runat="server"
                                                        Text='<%# Eval("other_details")%>'></asp:Label>
                                                </small>
                                                <%--</div>--%>
                                            </ItemTemplate>
                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="สายการผลิต" HeaderStyle-CssClass="text-left"
                                            HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <small>
                                                    <%-- <div class="panel-heading">--%>
                                                    <asp:Label ID="lbl_ddlOrg_idxsamplecreate" Visible="false"
                                                        runat="server" Text='<%# Eval("ddlOrg_idxsamplecreate")%>'>
                                                    </asp:Label>
                                                    <asp:Label ID="lbl_ddlDept_samplecreate" Visible="false"
                                                        runat="server" Text='<%# Eval("ddlDept_idxsamplecreate")%>'>
                                                    </asp:Label>
                                                    <asp:Label ID="lbl_ddlSec_idxsamplecreate" Visible="false"
                                                        runat="server" Text='<%# Eval("ddlSec_idxsamplecreate")%>'>
                                                    </asp:Label>

                                                    <asp:Label ID="DetailProduction" runat="server">

                                                        <p><b>องค์กร :</b> <%# Eval("ddlOrg_samplecreate") %></p>
                                                        <p><b>ฝ่าย :</b> <%# Eval("ddlDept_samplecreate") %></p>
                                                        <p><b>แผนก :</b> <%# Eval("ddlSec_samplecreate") %></p>

                                                    </asp:Label>
                                                </small>
                                                <%--</div>--%>
                                            </ItemTemplate>
                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <%--  <asp:TemplateField HeaderText="Certificate" &nbsp; HeaderStyle-Width="5%">
                                        <ItemTemplate>
                                            <div class="panel-heading">
                                                <asp:Label ID="lbl_certificate" runat="server" Visible="false" Text='<%# Eval("certificate")%>'>
                                        </asp:Label>
                                        <asp:Label ID="lbl_certificateshow" runat="server"
                                            Text='<%#getStatus((int)Eval("certificate")) %>'></asp:Label>

                            </div>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                            </asp:TemplateField>--%>

                            <asp:TemplateField HeaderText="จัดการ" HeaderStyle-CssClass="text-left"
                                HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <%--  <div class="panel-heading">--%>
                                    <small>
                                        <%--<asp:LinkButton ID="btn_DeleteSample" runat="server" Text="" Font-Size="9px" CssClass="btn btn-danger btn-sm" OnCommand="btnCommand" CommandName="btn_DeleteSample" CommandArgument='<%# Eval("material_code") %>'
                                        OnClientClick="return confirm('Do you want delete this item?')"><span
                                            class="glyphicon glyphicon-trash"></span>
                                        </asp:LinkButton>--%>
                                        <asp:LinkButton ID="btn_EditSample" CssClass="btn btn-warning btn-sm"
                                            Font-Size="9px" Visible="true" data-toggle="tooltip" title="Edit"
                                            runat="server" CommandName="Edit" OnCommand="btnCommand"
                                            ValidationGroup="insertpurchase"
                                            OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><span
                                                class="glyphicon glyphicon-pencil"></asp:LinkButton>

                                        <asp:LinkButton ID="btn_DeleteSample" CssClass="btn btn-danger btn-sm"
                                            Font-Size="9px" Visible="true" data-toggle="tooltip" title="Delete"
                                            runat="server" CommandName="Delete" OnCommand="btnCommand"
                                            ValidationGroup="insertpurchase"
                                            OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')"><span
                                                class="glyphicon glyphicon-trash"></span></asp:LinkButton>

                                    </small>

                                    <%-- </div>--%>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>
                            </Columns>
                            </asp:GridView>
                        </div>
                        <hr />
                        <%-- </div>--%>

                        <!--END Detail Sample create-->

                        <!--START Detail Department -->
                        <asp:FormView ID="fvDeptSelect" runat="server" Width="100%">
                            <InsertItemTemplate>

                                <div class="form-group">
                                    <div class="panel-heading f-bold">แผนกที่เห็นผลการตรวจสอบ</div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">องค์กร</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="ddlOrg_create" runat="server" CssClass="form-control"
                                            OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true"
                                            ValidationGroup="Create_Dept" />
                                        <asp:RequiredFieldValidator ID="RequiredddlOrg_create"
                                            ValidationGroup="Create_Dept" runat="server" Display="None"
                                            ControlToValidate="ddlOrg_create" Font-Size="11"
                                            ErrorMessage="*กรุณาเลือกองค์กร" ValidationExpression="*กรุณาเลือกองค์กร"
                                            InitialValue="-1" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11"
                                            runat="Server" PopupPosition="TopLeft"
                                            HighlightCssClass="validatorCalloutHighlight"
                                            TargetControlID="RequiredddlOrg_create" Width="160" />
                                    </div>

                                    <label class="col-sm-2 control-label">ฝ่าย</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="ddlDept_create" runat="server" CssClass="form-control"
                                            OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true"
                                            ValidationGroup="Create_Dept" />
                                        <asp:RequiredFieldValidator ID="RequiredddlDept_create"
                                            ValidationGroup="Create_Dept" runat="server" Display="None"
                                            ControlToValidate="ddlDept_create" Font-Size="11"
                                            ErrorMessage="*กรุณาเลือกฝ่าย" ValidationExpression="*กรุณาเลือกฝ่าย"
                                            InitialValue="-1" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12"
                                            runat="Server" PopupPosition="TopLeft"
                                            HighlightCssClass="validatorCalloutHighlight"
                                            TargetControlID="RequiredddlDept_create" Width="160" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">แผนก</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="ddlSec_create" runat="server" CssClass="form-control"
                                            OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true"
                                            ValidationGroup="Create_Dept" />
                                        <asp:RequiredFieldValidator ID="RequiredddlSec_create"
                                            ValidationGroup="Create_Dept" runat="server" Display="None"
                                            ControlToValidate="ddlSec_create" Font-Size="11"
                                            ErrorMessage="*กรุณาเลือกแผนก" ValidationExpression="*กรุณาเลือกแผนก"
                                            InitialValue="-1" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3"
                                            runat="Server" PopupPosition="TopLeft"
                                            HighlightCssClass="validatorCalloutHighlight"
                                            TargetControlID="RequiredddlSec_create" Width="160" />
                                    </div>

                                    <div class="col-sm-2">
                                        <asp:LinkButton ID="btnInsertDept" CssClass="btn btn-default" runat="server"
                                            CommandName="btnInsertDept" OnCommand="btnCommand"
                                            ValidationGroup="Create_Dept" title="เพิ่มแผนก"><i class="fa fa-plus-square"
                                                aria-hidden="true"></i> เพิ่ม</asp:LinkButton>
                                    </div>

                                    <label class="col-sm-4 control-label"></label>
                                </div>

                                <%--GvDeptCreate ShowHeaderWhenEmpty="True"  --%>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:GridView ID="GvDeptCreate" runat="server" AutoGenerateColumns="false"
                                            CssClass="table table-striped table-bordered table-hover"
                                            HeaderStyle-Height="25px" ShowFooter="False" DataKeyNames="RSecIDX"
                                            BorderStyle="None" RowStyle-Font-Size="Small" HeaderStyle-Font-Size="Small"
                                            CellSpacing="2">
                                            <EmptyDataTemplate>
                                                <div style="text-align: center">Data Cannot Be Found</div>
                                            </EmptyDataTemplate>
                                            <Columns>
                                                <asp:TemplateField HeaderText="#" Visible="false">
                                                    <ItemTemplate>
                                                        <div class="panel-heading">
                                                            <%# (Container.DataItemIndex +1) %>
                                                            <asp:Label ID="lbl_RSecIDX" runat="server" Visible="false"
                                                                Text='<%# Eval("RSecIDX")%>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ชื่อองค์กร">
                                                    <ItemTemplate>
                                                        <div class="panel-heading">
                                                            <asp:Label ID="lbl_orgname" runat="server"
                                                                Text='<%# Eval("OrgNameEN")%>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                    <EditItemTemplate />
                                                    <FooterTemplate />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="ชื่อฝ่าย">
                                                    <ItemTemplate>
                                                        <div class="panel-heading">
                                                            <asp:Label ID="lbl_deptname" runat="server"
                                                                Text='<%# Eval("DeptNameEN")%>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                    <EditItemTemplate />
                                                    <FooterTemplate />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ชื่อแผนก">
                                                    <ItemTemplate>
                                                        <div class="panel-heading">
                                                            <asp:Label ID="lbl_secname" runat="server"
                                                                Text='<%# Eval("SecNameEN")%>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                    <EditItemTemplate />
                                                    <FooterTemplate />
                                                </asp:TemplateField>
                                                <asp:TemplateField ControlStyle-Font-Size="1px" HeaderText="ลบ">
                                                    <ItemTemplate>
                                                        <%--<div class="panel-heading">--%>
                                                        <asp:LinkButton ID="btn_delete_department" runat="server"
                                                            Text="" CssClass="btn btn-sm btn-danger"
                                                            OnCommand="btnCommand" CommandName="btn_delete_department"
                                                            CommandArgument='<%# Eval("RSecIDX") %>'
                                                            OnClientClick="return confirm('Do you want delete this item?')">
                                                            <span class="glyphicon glyphicon-trash"></span>
                                                        </asp:LinkButton>

                                                        <%-- </div>--%>
                                                    </ItemTemplate>
                                                    <EditItemTemplate />
                                                    <FooterTemplate />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>

                                <div class="form-group" id="div_saveinsert" runat="server">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="lbDocSave" CssClass="btn btn-success" runat="server"
                                            data-original-title="Save" data-toggle="tooltip" Text="Save"
                                            ValidationGroup="Create_Actor1" OnCommand="btnCommand"
                                            CommandName="cmdDocSave"></asp:LinkButton>
                                        <asp:LinkButton ID="lbDocCancel" CssClass="btn btn-danger" runat="server"
                                            data-original-title="Cancel" data-toggle="tooltip" Text="Cancel"
                                            OnCommand="btnCommand" CommandName="cmdDocCancel"></asp:LinkButton>
                                    </div>
                                </div>

                            </InsertItemTemplate>
                        </asp:FormView>
                        <!--END Detail Department -->

                    </div>
                </div>
            </div>

            <asp:FormView ID="fvDetail" runat="server" Width="100%">
                <ItemTemplate>
                    <%--<asp:HiddenField ID="hfActorIDX" runat="server" Value='<%# Eval("current_actor") %>' />
                    <asp:HiddenField ID="hfNodeIDX" runat="server" Value='<%# Eval("current_node") %>' />--%>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">รายละเอียดรายการ</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Document No</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="tbU0IDX" runat="server" Visible="false" CssClass="form-control"
                                            Text='<%# Eval("u0_qalab_idx") %>' Enabled="false" />
                                        <asp:TextBox ID="tbEmpCode" runat="server" CssClass="form-control"
                                            Text='<%# Eval("document_code") %>' Enabled="false" />
                                        <asp:TextBox ID="tbstaidx" runat="server" CssClass="form-control"
                                            Visible="false" Text='<%# Eval("staidx") %>' Enabled="false" />
                                        <asp:TextBox ID="tbplaceidx" runat="server" CssClass="form-control"
                                            Visible="false" Text='<%# Eval("place_idx") %>' Enabled="false" />

                                    </div>
                                    <label class="col-sm-2 control-label">ผู้ทำรายการ</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="tbEmpName" runat="server" CssClass="form-control"
                                            Text='<%# Eval("emp_name_th") %>' Enabled="false" />
                                        <asp:TextBox ID="tbActorCempIDX" runat="server" Visible="false"
                                            Text='<%# Eval("cemp_idx") %>' CssClass="form-control" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">องค์กร</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="tbEmpOrg" runat="server" CssClass="form-control"
                                            Text='<%# Eval("org_name_th") %>' Enabled="false" />
                                    </div>
                                    <label class="col-sm-2 control-label">ฝ่าย</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="tbEmpDept" runat="server" CssClass="form-control"
                                            Text='<%# Eval("dept_name_th") %>' Enabled="false" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">แผนก</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txtcemp_rsec_idx" runat="server" Visible="false"
                                            CssClass="form-control" Text='<%# Eval("cemp_rsec_idx") %>'
                                            Enabled="false" />
                                        <asp:TextBox ID="tbEmpSec" runat="server" CssClass="form-control"
                                            Text='<%# Eval("sec_name_th") %>' Enabled="false" />
                                    </div>
                                    <label class="col-sm-2 control-label">ตำแหน่ง</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="tbEmpPos" runat="server" CssClass="form-control"
                                            Text='<%# Eval("pos_name_th") %>' Enabled="false" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ส่งตรวจ Lab นอก</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="tblab_out" runat="server" CssClass="form-control"
                                            Text='<%# Eval("lab_out") %>' Enabled="false" />
                                    </div>
                                    <label class="col-sm-6 control-label"></label>

                                </div>

                                <div class="form-group" id="testdetail" runat="server" visible="true">
                                    <label class="col-sm-2 control-label hidden">รายการที่สั่งตรวจ</label>
                                    <div class="col-sm-4">
                                        <%-- <div class="panel-body">--%>
                                        <table class="table">
                                            <asp:Repeater ID="rp_testdetail" runat="server">

                                                <ItemTemplate>
                                                    <tr style="border: hidden;">

                                                        <td><%# (Container.ItemIndex + 1) %></td>
                                                        <%-- <td id="emp_idx_report" visible="false" runat="server" data-th="รหัสผู้ถือครอง Google - License"><%# Eval("emp_idx") %>
                                                        </td>--%>
                                                        <td><%# Eval("test_detail_name") %></td>
                                                    </tr>

                                                </ItemTemplate>
                                                <%--  <FooterTemplate>
                                                        </FooterTemplate>--%>
                                            </asp:Repeater>
                                        </table>
                                        <%--</div>--%>
                                        <%--<asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" Text='<%# Eval("sec_name_th") %>'
                                        Enabled="false" />--%>
                                    </div>
                                    <label class="col-sm-6 control-label"></label>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="lbDocCancelDoc" CssClass="btn btn-default" runat="server"
                                            data-original-title="Back" data-toggle="tooltip" OnCommand="btnCommand"
                                            CommandName="cmdDocCancel"><i class="fa fa-reply" aria-hidden="true"></i>
                                            Back</asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:FormView>

            <asp:FormView ID="fvUserShowDetailEditDocument" runat="server" Width="100%">
                <ItemTemplate>
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">Sample list</h3>
                        </div>
                        <div class="panel-body">

                            <div class="form-group">
                                <asp:GridView ID="gvDocumentEditList" runat="server" AutoGenerateColumns="false"
                                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                    AllowPaging="false" PageSize="10" OnPageIndexChanging="gvPageIndexChanging"
                                    OnRowDataBound="gvRowDataBound">
                                    <RowStyle Font-Size="Small" />
                                    <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                                    <EmptyDataTemplate>
                                        <div style="text-align: center">Data Cannot Be Found</div>
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:TemplateField HeaderText="Sample Code" HeaderStyle-Width="10%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblu0IDX_Sample_edit" runat="server" Visible="false"
                                                    Text='<%# Eval("u0_qalab_idx") %>'></asp:Label>
                                                <asp:Label ID="lblDocIDX_Sample_edit" runat="server" Visible="false"
                                                    Text='<%# Eval("u1_qalab_idx") %>'></asp:Label>
                                                <asp:Label ID="lblSampleCode_edit" runat="server"
                                                    Text='<%# Eval("sample_code") %>'></asp:Label>
                                                <asp:Label ID="lblCount_All_edit" runat="server" Visible="false"
                                                    Text='<%# Eval("Count_All") %>'></asp:Label>
                                                <asp:Label ID="lblCount_Success_edit" runat="server" Visible="false"
                                                    Text='<%# Eval("Count_Success") %>'></asp:Label>
                                                <asp:Label ID="lblCount_Place_edit" runat="server" Visible="false"
                                                    Text='<%# Eval("Count_Place") %>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Tested Detail" HeaderStyle-Width="25%">
                                            <ItemTemplate>
                                                <table class="table table-striped f-s-12"
                                                    style="background-color: transparent; margin-top: 0;">
                                                    <asp:Repeater ID="rp_test_sample_edit" runat="server">

                                                        <ItemTemplate>
                                                            <tr style="background-color: transparent; border: hidden;">
                                                                <td style="background-color: transparent;">
                                                                    <asp:Label ID="lbltestdetailname_edit"
                                                                        runat="server"
                                                                        Text='<%# " - " + Eval("test_detail_name") %>'>
                                                                    </asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr id="u2_idx_n3_edit" runat="server" visible="false">
                                                                <td>
                                                                    <asp:Label ID="lblu2qalab_idx_edit" runat="server"
                                                                        Text='<%# Eval("u2_qalab_idx") %>'></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr id="test_detail_idx_n11_edit" runat="server"
                                                                visible="false">
                                                                <td>
                                                                    <asp:Label ID="lbltest_detail_idx_n11_edit"
                                                                        runat="server"
                                                                        Text='<%# Eval("test_detail_idx") %>'>
                                                                    </asp:Label>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Received Date" HeaderStyle-Width="10%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblreceived_sample_date_edit" runat="server"
                                                    Text='<%# Eval("received_sample_date") %>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Tested Date" HeaderStyle-Width="10%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTest_date_edit" runat="server"
                                                    Text='<%# Eval("test_date") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Lab" HeaderStyle-Width="20%">
                                            <ItemTemplate>

                                                <asp:Label ID="lblm0_lab_idx_admin_edit" runat="server" Visible="false"
                                                    Text='<%# Eval("m0_lab_idx") %>'></asp:Label>
                                                <asp:Label ID="lbllab_nameadmin_edit" runat="server"
                                                    Text='<%# Eval("lab_name") %>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="	Status" HeaderStyle-Width="15%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblStaidx_edit" runat="server" Visible="false"
                                                    Text='<%# Eval("staidx") %>'></asp:Label>
                                                <asp:Label ID="lbstatus_name_edit" runat="server"
                                                    Text='<%# Eval("status_name") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Action" HeaderStyle-Width="10%">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btnViewSamplelist_edit"
                                                    CssClass="btn-sm btn-warning" runat="server"
                                                    CommandName="cmdUserEditDocument" OnCommand="btnCommand"
                                                    CommandArgument='<%# Eval("u0_qalab_idx")+ ";" + Eval("u1_qalab_idx")+ ";" + Eval("staidx") %>'
                                                    data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i>
                                                </asp:LinkButton>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>

                                </asp:GridView>

                            </div>

                            <div class="form-group pull-right">
                                <asp:LinkButton ID="btnEditDocument" CssClass="btn btn-success" runat="server"
                                    data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand"
                                    CommandName="cmdSaveEditDocument" CommandArgument="0"></asp:LinkButton>
                                <asp:LinkButton ID="btnCancelEditDocument" CssClass="btn btn-danger" runat="server"
                                    data-original-title="Cancel" data-toggle="tooltip" Text="Cancel"
                                    OnCommand="btnCommand" CommandName="cmdCancelEditDocument"></asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:FormView>


            <asp:FormView ID="fvSampleTestDetail" runat="server" Width="100%">
                <ItemTemplate>

                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">Sample list</h3>
                        </div>

                        <div class="panel-body">

                            <div class="form-group">
                                <asp:UpdatePanel ID="updatepanelbutton" runat="server">
                                    <ContentTemplate>
                                        <%--  <p style="font-size: small;"><b>คุณต้องการปริ้น QR Code หรือไม่ <i class="fa fa-question-circle" aria-hidden="true"></i></b></p>    target="_blank"--%>
                                        <asp:LinkButton CssClass="btn btn-success" ID="btnExportExcel"
                                            data-toggle="tooltip" title="Export Excel" runat="server"
                                            CommandName="CmdExport" Visible="true" OnCommand="btnCommand"
                                            CommandArgument='<%# Eval("u0_qalab_idx") + ";" + Eval("u1_qalab_idx") %>'>
                                            <i class="glyphicon glyphicon-list-alt"></i> Export</asp:LinkButton>
                                        <asp:LinkButton CssClass="btn btn-default tar" ID="btnPrintSampleCode"
                                            data-toggle="tooltip" title="Print" runat="server"
                                            CommandName="printSampleCode" Visible="true" OnCommand="btnCommand"
                                            target="" CommandArgument='<%# Eval("u0_qalab_idx") %>'><i
                                                class="glyphicon glyphicon-print"></i> Print</asp:LinkButton>

                                        <asp:RadioButton ID="rdbuttonNeedPrint" runat="server" Font-Size="9"
                                            Visible="false" GroupName="radioButtonListPrint" Checked="true"
                                            Text="ต้องการ QR Code" />
                                        <asp:RadioButton ID="rdbuttonNotNeedPrint" runat="server" Font-Size="9"
                                            Visible="false" GroupName="radioButtonListPrint"
                                            Text="ไม่ต้องการ QR Code" />


                                        <asp:CheckBox ID="ChkPrintQrCode" runat="server" Checked="true"
                                            Text="ต้องการ QR Code" />
                                        <asp:CheckBox ID="ChkPrintTestedItems" Checked="true" runat="server"
                                            Text="ต้องการ Tested Items" />


                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="btnPrintSampleCode" />
                                        <asp:PostBackTrigger ControlID="btnExportExcel" />
                                    </Triggers>

                                </asp:UpdatePanel>
                            </div>

                            <asp:GridView ID="gvSampleTestDetal" runat="server" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-striped table-responsive col-md-12 footable"
                                HeaderStyle-CssClass="info" OnPageIndexChanging="gvPageIndexChanging"
                                OnRowDataBound="gvRowDataBound" AllowPaging="false" PageSize="10"
                                DataKeyNames="u1_qalab_idx">

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First"
                                    LastPageText="Last" />
                                <EmptyDataTemplate>
                                    <div style="text-align: center">ไม่พบข้อมูล</div>
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="Sample Code" ItemStyle-CssClass="f-s-13 text-left"
                                        HeaderStyle-CssClass="f-s-13 text-center" HeaderStyle-Width="7%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblu0IDX_Sample" runat="server" Visible="false"
                                                Text='<%# Eval("u0_qalab_idx") %>'></asp:Label>
                                            <asp:Label ID="lblDocIDX_Sample" runat="server" Visible="true"
                                                Text='<%# Eval("u1_qalab_idx") %>'></asp:Label>
                                            <asp:Label ID="lblSampleCode" runat="server"
                                                Text='<%# Eval("sample_code") %>'></asp:Label>
                                            <asp:Label ID="lblCount_All" runat="server" Visible="false"
                                                Text='<%# Eval("Count_All") %>'></asp:Label>
                                            <asp:Label ID="lblCount_Success" runat="server" Visible="false"
                                                Text='<%# Eval("Count_Success") %>'></asp:Label>
                                            <asp:Label ID="lblCount_Place" runat="server" Visible="false"
                                                Text='<%# Eval("Count_Place") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Tested Detail" ItemStyle-CssClass="f-s-13 text-left"
                                        HeaderStyle-CssClass="f-s-13 text-center" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <%--<asp:Label ID="lblCreateDate" runat="server" Text='<%# getOnlyDate((string)Eval("create_date")) %>'>
                                            </asp:Label>--%>
                                            <%--<asp:Label ID="lblCreateDate" runat="server" Text='<%#Eval("create_date") %>'>
                                            </asp:Label> table-striped f-s-12--%>
                                            <table class="table table-striped f-s-12"
                                                style="background-color: transparent; margin-top: 0;">
                                                <asp:Repeater ID="rp_test_sample" runat="server">

                                                    <ItemTemplate>
                                                        <tr
                                                            style="background-color: transparent; border-style: none; border: hidden;">
                                                            <td style="background-color: transparent;">
                                                                <asp:Label ID="lbltestdetailname" runat="server"
                                                                    Text='<%# " - " + Eval("test_detail_name") %>'>
                                                                </asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr id="u2_idx_n3" runat="server" visible="false">
                                                            <td>
                                                                <asp:Label ID="lblu2qalab_idx" runat="server"
                                                                    Text='<%# Eval("u2_qalab_idx") %>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr id="test_detail_idx_n11" runat="server" visible="false">
                                                            <td>
                                                                <asp:Label ID="lbltest_detail_idx_n11" runat="server"
                                                                    Text='<%# Eval("test_detail_idx") %>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </table>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Received Date" ItemStyle-CssClass="f-s-13 text-left"
                                        HeaderStyle-CssClass="f-s-13 text-center" HeaderStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblreceived_sample_date" runat="server"
                                                Text='<%# Eval("received_sample_date") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Tested Date" ItemStyle-CssClass="f-s-13 text-left"
                                        HeaderStyle-CssClass="f-s-13 text-center" HeaderStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTest_date" runat="server" Text='<%# Eval("test_date") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Lab" ItemStyle-CssClass="f-s-13 text-left"
                                        HeaderStyle-CssClass="f-s-13 text-center" HeaderStyle-Width="18%">
                                        <ItemTemplate>

                                            <asp:Label ID="lblm0_lab_idx_admin" runat="server" Visible="false"
                                                Text='<%# Eval("m0_lab_idx") %>'></asp:Label>
                                            <asp:Label ID="lbllab_nameadmin" runat="server"
                                                Text='<%# Eval("lab_name") %>'></asp:Label>

                                            <asp:Panel ID="pnlChooseLocation" runat="server" Visible="true"
                                                Width="100%">
                                                <div class="panel-body">
                                                    <div class="form-horizontal" role="form">
                                                        <div class="form-group">
                                                            <div class="col-sm-12">
                                                                <asp:DropDownList ID="ddlChooseLabType" runat="server"
                                                                    CssClass="form-control"
                                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                                    AutoPostBack="true" />
                                                                <asp:RequiredFieldValidator
                                                                    ID="RequiredddlChooseLabType" runat="server"
                                                                    InitialValue="0"
                                                                    ControlToValidate="ddlChooseLabType" Display="None"
                                                                    SetFocusOnError="true"
                                                                    ErrorMessage="*กรุณาเลือกประเภทการตรวจ"
                                                                    ValidationGroup="SelectTypegroup" />
                                                                <ajaxToolkit:ValidatorCalloutExtender
                                                                    ID="ValidatorddlChooseLabType" runat="Server"
                                                                    HighlightCssClass="validatorCalloutHighlight"
                                                                    TargetControlID="RequiredddlChooseLabType"
                                                                    Width="250" />
                                                            </div>
                                                        </div>
                                                        <div class="form-group">

                                                            <div class="col-sm-12">
                                                                <asp:DropDownList ID="ddlChooseLocation" Visible="true"
                                                                    Enabled="false" CssClass="form-control"
                                                                    runat="server" />
                                                                <asp:RequiredFieldValidator
                                                                    ID="RequiredddlChooseLocation" runat="server"
                                                                    InitialValue="0"
                                                                    ControlToValidate="ddlChooseLocation" Display="None"
                                                                    SetFocusOnError="true"
                                                                    ErrorMessage="*กรุณาเลือกประเภทแลปตรวจ"
                                                                    ValidationGroup="SelectTypegroup" />
                                                                <ajaxToolkit:ValidatorCalloutExtender
                                                                    ID="ValidatorddlChooseLocation" runat="Server"
                                                                    HighlightCssClass="validatorCalloutHighlight"
                                                                    TargetControlID="RequiredddlChooseLocation"
                                                                    Width="250" />
                                                            </div>
                                                        </div>
                                                        <div class="form-group">

                                                            <div class="col-sm-12">
                                                                <asp:TextBox ID="txtCommentBeforeExtanalLab"
                                                                    Visible="false" CssClass="col-sm-12 form-control"
                                                                    placeholder="กรอกความคิดเห็น ..." runat="server" />
                                                                <asp:TextBox ID="tet" Visible="false"
                                                                    CssClass="col-sm-12 form-control"
                                                                    placeholder="กรอกความคิดเห็น ..." runat="server" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                            <%--<asp:Label ID="lblDocStatus" runat="server" Text='<%# Eval("status_name") %>'>
                                            </asp:Label>--%>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Status" ItemStyle-CssClass="f-s-13 text-left"
                                        HeaderStyle-CssClass="f-s-13 text-center" HeaderStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStaidx" runat="server" Visible="false"
                                                Text='<%# Eval("staidx") %>'></asp:Label>
                                            <asp:Label ID="lbstatus_name" runat="server"
                                                Text='<%# Eval("status_name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Action" ItemStyle-CssClass="f-s-13 text-left"
                                        HeaderStyle-CssClass="f-s-13 text-center" HeaderStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnViewSamplelist" CssClass="btn-sm btn-info"
                                                runat="server" CommandName="cmdDocDetailAdminUser"
                                                OnCommand="btnCommand"
                                                CommandArgument='<%# Eval("u0_qalab_idx")+ ";" + Eval("u1_qalab_idx")+ ";" + Eval("staidx") %>'
                                                data-toggle="tooltip" title="View"><i class="fa fa-file-text-o"></i>
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="btnPrintReport" CssClass="btn-sm btn-success" target=""
                                                runat="server" CommandName="cmdPrintReport" OnCommand="btnCommand"
                                                CommandArgument='<%# Eval("u1_qalab_idx") %>' data-toggle="tooltip"
                                                title="Report"><i class="fa fa-file-code-o"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>

                            <asp:Panel ID="pnlAction" runat="server" CssClass="pull-right" Visible="true">
                                <asp:LinkButton ID="lnkbtnSave" CssClass="btn btn-success" runat="server"
                                    data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand"
                                    ValidationGroup="SelectTypegroup" CommandName="cmdSaveNode3"></asp:LinkButton>
                                <asp:LinkButton ID="lnkbtnCancel" CssClass="btn btn-danger" runat="server"
                                    data-original-title="Cancel" data-toggle="tooltip" Text="Cancel"
                                    OnCommand="btnCommand" CommandName="cmdDocCancel"></asp:LinkButton>
                            </asp:Panel>

                            <asp:Panel ID="pnlActionNode11" runat="server" CssClass="pull-right" Visible="true">
                                <asp:LinkButton ID="btnApproveNode11" CssClass="btn btn-success" runat="server"
                                    data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand"
                                    CommandName="cmdSaveNode11" CommandArgument="16"></asp:LinkButton>
                                <asp:LinkButton ID="btnCancelApproveN11" CssClass="btn btn-danger" runat="server"
                                    data-original-title="Cancel" data-toggle="tooltip" Text="Cancel"
                                    OnCommand="btnCommand" CommandName="cmdDocCancel"></asp:LinkButton>
                            </asp:Panel>

                        </div>
                    </div>
                </ItemTemplate>
            </asp:FormView>

            <asp:GridView ID="GvShowExportExcel" runat="server" HeaderStyle-CssClass="info" AllowPaging="True"
                AllowSorting="True" BackColor="White" BorderColor="#DEDFDE " BorderStyle="None" BorderWidth="1px"
                CssClass="table table-striped table-bordered" Width="728px" CellPadding="4" ForeColor="Black"
                GridLines="Vertical" PageSize="14" OnRowDataBound="gvRowDataBound">
                <Columns>
                    <%-- <asp:TemplateField HeaderText="No." HeaderStyle-Width="5%" ControlStyle-CssClass="text-left">
                        <ItemTemplate>
                            <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
                    </ItemTemplate>
                    </asp:TemplateField>--%>

                    <%--  <asp:TemplateField HeaderText="ลำดับที่" HeaderStyle-Width="5%" ControlStyle-CssClass="text-left">
                        <ItemTemplate>
                            <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
                    </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Document Code" HeaderStyle-Width="15%" ItemStyle-Width="100%">
                        <ItemTemplate>
                            <asp:Label ID="lbExportDocumentCode" runat="server" Text='<%# Eval("DocumentCode")%>'>
                            </asp:Label>
                            <asp:Label ID="lbExportU1idx" runat="server" Visible="false"
                                Text='<%# Eval("u1_qalab_idx")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Sample Code" HeaderStyle-Width="15%" ItemStyle-Width="100%">
                        <ItemTemplate>
                            <asp:Label ID="lbExportSampleCode" runat="server" Text='<%# Eval("SampleCode")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ชื่อตัวอย่าง" HeaderStyle-Width="30%" ItemStyle-Width="100">
                        <ItemTemplate>
                            <asp:Label ID="lbExportSampleCodeName" runat="server" Text='<%# Eval("ชื่อตัวอย่าง")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Test Item" HeaderStyle-Width="20%" ItemStyle-Width="100%">
                        <ItemTemplate>
                            <table class="table table-striped f-s-12"
                                style="background-color: transparent; margin-top: 0;">
                                <asp:Repeater ID="rp_test_sample_export" runat="server">

                                    <ItemTemplate>
                                        <tr style="background-color: transparent;">
                                            <td style="background-color: transparent;">
                                                <asp:Label ID="lbltestdetailname_export" runat="server"
                                                    Text='<%# Eval("test_detail_name") %>'></asp:Label>
                                            </td>
                                        </tr>
                                        <tr id="u2_idx_n3" runat="server" visible="false">
                                            <td>
                                                <asp:Label ID="lblu2qalab_idx_export" runat="server"
                                                    Text='<%# Eval("u2_qalab_idx") %>'></asp:Label>
                                            </td>
                                        </tr>
                                        <tr id="test_detail_idx_n11" runat="server" visible="false">
                                            <td>
                                                <asp:Label ID="lbltest_detail_idx_export" runat="server"
                                                    Text='<%# Eval("test_detail_idx") %>'></asp:Label>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Result" HeaderStyle-Width="15%" ItemStyle-Width="100%">
                        <ItemTemplate>
                            <table class="table table-striped f-s-12" style="width: 100%">
                                <asp:Repeater ID="rp_test_sample_result_export" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lbltestdetailname_export" runat="server"
                                                    Text='<%# Eval("detail_tested") %>'></asp:Label>
                                            </td>
                                        </tr>
                                        <tr id="u2_idx_n3" runat="server" visible="false">
                                            <td>
                                                <asp:Label ID="lblu2qalab_idx_export" runat="server"
                                                    Text='<%# Eval("u2_qalab_idx") %>'></asp:Label>
                                            </td>
                                        </tr>
                                        <tr id="test_detail_idx_n11" runat="server" visible="false">
                                            <td>
                                                <asp:Label ID="lbltest_detail_idx_export" runat="server"
                                                    Text='<%# Eval("test_detail_idx") %>'></asp:Label>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                </Columns>
            </asp:GridView>
            <asp:HyperLink ID="setOnSample" runat="server"></asp:HyperLink>
            <asp:FormView ID="fvSampleCode" runat="server" Width="100%">
                <ItemTemplate>
                    <%--<asp:HiddenField ID="hfActorIDX" runat="server" Value='<%# Eval("current_actor") %>' />
                    <asp:HiddenField ID="hfNodeIDX" runat="server" Value='<%# Eval("current_node") %>' />--%>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">รายละเอียดรายการที่ตรวจ</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Sample Name</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="tbmaterial_name_view" runat="server" Visible="true"
                                            CssClass="form-control" Text='<%# Eval("material_name") %>'
                                            Enabled="false" />
                                        <asp:TextBox ID="tbu0_qalab_idx_view" runat="server" CssClass="form-control"
                                            Visible="false" Text='<%# Eval("u0_qalab_idx") %>' Enabled="false" />
                                        <asp:TextBox ID="tbu1_qalab_idx_view" runat="server" CssClass="form-control"
                                            Visible="false" Text='<%# Eval("u1_qalab_idx") %>' Enabled="false" />
                                        <asp:TextBox ID="tbm0labidx" runat="server" CssClass="form-control"
                                            Visible="false" Text='<%# Eval("m0_lab_idx") %>' Enabled="true" />
                                    </div>
                                    <label class="col-sm-2 control-label">Sample Code</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="tbsample_code" runat="server" CssClass="form-control"
                                            Text='<%# Eval("sample_code") %>' Enabled="false" />

                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">รายการที่สั่งตรวจ</label>
                                    <div class="col-sm-4">
                                        <%--  <div class="panel-body">--%>
                                        <table class="table">
                                            <asp:Repeater ID="rp_testdetail_lab_view" runat="server">

                                                <ItemTemplate>
                                                    <tr style="border: hidden;">

                                                        <td><%# (Container.ItemIndex + 1) %></td>
                                                        <%-- <td id="emp_idx_report" visible="false" runat="server" data-th="รหัสผู้ถือครอง Google - License"><%# Eval("emp_idx") %>
                                                        </td>--%>
                                                        <td><%# Eval("test_detail_name") %></td>
                                                        <asp:Label ID="u2_labtest_detailidx_view" runat="server"
                                                            Text='<%# Eval("test_detail_idx") %>' Visible="false">
                                                        </asp:Label>

                                                        <asp:Label ID="u2_lab_detail_view" runat="server"
                                                            Text='<%# Eval("u2_qalab_idx") %>' Visible="false">
                                                        </asp:Label>
                                                    </tr>

                                                </ItemTemplate>
                                                <FooterTemplate>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </table>
                                        <%-- </div>--%>
                                        <%--<asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" Text='<%# Eval("sec_name_th") %>'
                                        Enabled="false" />--%>
                                    </div>
                                    <label class="col-sm-6 control-label"></label>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">

                                        <asp:LinkButton ID="lbDocCancelUserAdmin" CssClass="btn btn-default"
                                            runat="server" data-original-title="Back" data-toggle="tooltip"
                                            OnCommand="btnCommand" CommandName="cmdDocCancelUserAdmin"
                                            CommandArgument='<%# Eval("u0_qalab_idx")%>'><i class="fa fa-reply"
                                                aria-hidden="true"></i> Back</asp:LinkButton>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </ItemTemplate>
            </asp:FormView>

            <asp:FormView ID="fvDetailSampleCode" runat="server" Width="100%">
                <ItemTemplate>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">รายละเอียดรายการตัวอย่าง</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Mat.</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_material_code_view" runat="server" Enabled="false"
                                            MaxLength="7" CssClass="form-control" Text='<%# Eval("material_code") %>'
                                            placeholder="Mat ..." />
                                        <asp:TextBox ID="txtUpdate_U1QaLabIDX" runat="server" Enabled="false"
                                            Visible="false" CssClass="form-control"
                                            Text='<%# Eval("u1_qalab_idx") %>' />
                                        <asp:TextBox ID="txtUpdate_U0QaLabIDX" runat="server" Enabled="false"
                                            Visible="false" CssClass="form-control"
                                            Text='<%# Eval("u0_qalab_idx") %>' />

                                    </div>

                                    <label class="col-sm-2 control-label">ชื่อตัวอย่าง</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_samplename_edit" MaxLength="100" runat="server"
                                            Enabled="false" CssClass="form-control" Text='<%# Eval("sample_name") %>'
                                            placeholder="ชื่อตัวอย่าง ..." />

                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ประเภทตัวอย่าง</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_material_type_view" runat="server" Enabled="false"
                                            CssClass="form-control" Text='<%# Eval("materail_type") %>'
                                            placeholder="ประเภทตัวอย่าง ..." />


                                    </div>

                                    <label class="col-sm-6 control-label"></label>


                                </div>

                                <div class="form-group">
                                    <div class="col-sm-2">
                                        <asp:TextBox ID="tbSample1IDX_view" MaxLength="10"
                                            placeholder="วันที่บนตัวอย่าง ..." Visible="false"
                                            Text='<%# Eval("date_sample1_idx") %>' runat="server"
                                            CssClass="form-control" />
                                        <asp:DropDownList ID="ddldate_sample1_idx_view" runat="server" Enabled="false"
                                            CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                            AutoPostBack="true" />

                                    </div>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_data_sample1_view" runat="server" MaxLength="40"
                                            Enabled="false" placeholder="วันที่บนตัวอย่าง ..."
                                            Text='<%# Eval("data_sample1") %>' CssClass="form-control" />

                                    </div>

                                    <div class="col-sm-2">
                                        <asp:TextBox ID="tbSample2IDX_view" MaxLength="10"
                                            placeholder="วันที่บนตัวอย่าง ..." Visible="false" Enabled="false"
                                            Text='<%# Eval("date_sample2_idx") %>' runat="server"
                                            CssClass="form-control" />
                                        <asp:DropDownList ID="ddldate_sample2_idx_view" runat="server" Enabled="false"
                                            CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                            AutoPostBack="true" />

                                    </div>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_data_sample2_view" MaxLength="20"
                                            placeholder="วันที่บนตัวอย่าง ..." Enabled="false"
                                            Text='<%# Eval("data_sample2") %>' runat="server" CssClass="form-control" />
                                        <asp:RequiredFieldValidator ID="Requiredtxt_data_sample2_edit"
                                            ValidationGroup="SaveinsertSampleEdit" runat="server" Display="None"
                                            ControlToValidate="txt_data_sample2_view" Font-Size="11"
                                            SetFocusOnError="true" ErrorMessage="*กรอกวันที่บนตัวอย่าง" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="Validatortxt_data_sample2_edit"
                                            runat="Server" PopupPosition="TopLeft"
                                            HighlightCssClass="validatorCalloutHighlight"
                                            TargetControlID="Requiredtxt_data_sample2_edit" Width="160" />
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Batch</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_batch_view" runat="server" MaxLength="10"
                                            placeholder="Batch ..." Text='<%# Eval("batch") %>' Enabled="false"
                                            CssClass="form-control" />

                                    </div>

                                    <label class="col-sm-2 control-label">ชื่อลูกค้า</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_customer_name_view" MaxLength="25"
                                            placeholder="ชื่อลูกค้า ..." runat="server"
                                            Text='<%# Eval("customer_name") %>' Enabled="false"
                                            CssClass="form-control" />

                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">เลขตู้</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_cabinet_view" runat="server" MaxLength="10"
                                            placeholder="เลขตู้ ..." Enabled="false" Text='<%# Eval("cabinet") %>'
                                            CssClass="form-control" />

                                    </div>

                                    <label class="col-sm-2 control-label">กะ</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_shift_time_view" runat="server" MaxLength="5"
                                            placeholder="กะ ..." Enabled="false" Text='<%# Eval("shift_time") %>'
                                            CssClass="form-control" />

                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">เวลา</label>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <asp:HiddenField ID="HiddenTimeEdit" runat="server" />
                                            <asp:TextBox ID="txt_time_view" placeholder="เวลา ..." runat="server"
                                                Text='<%# Eval("time") %>' ReadOnly="true" Enabled="false"
                                                CssClass="form-control" value="" /><span class="input-group-addon"><span
                                                    class="glyphicon glyphicon-time"></span></span>

                                        </div>
                                    </div>
                                    <label class="col-sm-2 control-label">รายละเอียดอื่นๆ</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_other_details_view" placeholder="รายละเอียดอื่นๆ ..."
                                            Text='<%# Eval("other_details") %>' Enabled="false" runat="server"
                                            CssClass="form-control" />

                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">องค์กรสายการผลิต</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_org_name_th" runat="server" MaxLength="10"
                                            placeholder="เลขตู้ ..." Enabled="false" Text='<%# Eval("org_name_th") %>'
                                            CssClass="form-control" />

                                    </div>

                                    <label class="col-sm-2 control-label">ฝ่ายสายการผลิต</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_dept_name_th" runat="server" MaxLength="5"
                                            placeholder="กะ ..." Enabled="false" Text='<%# Eval("dept_name_th") %>'
                                            CssClass="form-control" />

                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">แผนกสายการผลิต</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_sec_name_th" runat="server" MaxLength="10"
                                            placeholder="เลขตู้ ..." Enabled="false" Text='<%# Eval("sec_name_th") %>'
                                            CssClass="form-control" />

                                    </div>

                                    <label class="col-sm-6 control-label"></label>


                                </div>


                            </div>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:FormView>

            <asp:FormView ID="fvUserEditDocument" runat="server" Width="100%">
                <EditItemTemplate>
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">รายละเอียดรายการตัวอย่างที่ดำเนินการแก้ไข</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <%--                
                                <asp:Panel class="form-group" runat="server" ID="div_search_edit">--%>
                                    <label class="col-sm-2 control-label">ค้นหาเลข Mat.</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_material_search_edit" runat="server" MaxLength="7"
                                            CssClass="form-control" OnTextChanged="TextBoxChanged" AutoPostBack="true"
                                            placeholder="Search Mat ...">
                                        </asp:TextBox>
                                        <asp:RequiredFieldValidator ID="Requiredtxt_material_search_edit"
                                            ValidationGroup="SearchMatEdit" runat="server" Display="None"
                                            ControlToValidate="txt_material_search_edit" Font-Size="11"
                                            ErrorMessage="*กรุณากรอกเลข Mat. ที่ต้องการค้นหา!!!" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="Validatortxt_material_search_edit"
                                            runat="Server" PopupPosition="TopLeft"
                                            HighlightCssClass="validatorCalloutHighlight"
                                            TargetControlID="Requiredtxt_material_search_edit" Width="160" />
                                    </div>
                                    <div class="col-sm-2 ">
                                        <div class="pull-left">
                                            <asp:LinkButton ID="btnSearchMatForEdit" CssClass="btn btn-primary"
                                                runat="server" ValidationGroup="SearchMatEdit" OnCommand="btnCommand"
                                                CommandName="btnSearchMatEdit" Font-Size="Small" data-toggle="tooltip"
                                                title="Search"><span class="glyphicon glyphicon-search"></span>
                                            </asp:LinkButton>
                                        </div>

                                        </label>
                                    </div>
                                    <label class="col-sm-4 control-label"></label>
                                </div>
                                <%--                                </asp:Panel>--%>
                                <%--      <asp:Panel class="form-group" runat="server" ID="div_detail_edit">--%>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Mat.</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_material_code_edit" runat="server" Enabled="false"
                                            MaxLength="7" CssClass="form-control" Text='<%# Eval("material_code") %>'
                                            placeholder="Mat ..." />
                                        <asp:TextBox ID="txtUpdate_U1QaLabIDX" runat="server" Enabled="false"
                                            Visible="false" CssClass="form-control"
                                            Text='<%# Eval("u1_qalab_idx") %>' />
                                        <asp:TextBox ID="txtUpdate_U0QaLabIDX" runat="server" Enabled="false"
                                            Visible="false" CssClass="form-control"
                                            Text='<%# Eval("u0_qalab_idx") %>' />
                                        <asp:RequiredFieldValidator ID="Requiredtxt_material_code_edit"
                                            ValidationGroup="Saveinsert_SampleEdit" runat="server" Display="None"
                                            ControlToValidate="txt_material_code_edit" Font-Size="11"
                                            SetFocusOnError="true" ErrorMessage="*กรุณากรอกเลข Mat" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="Validatortxt_material_code_edit"
                                            runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                            PopupPosition="TopLeft" TargetControlID="Requiredtxt_material_code_edit"
                                            Width="160" />
                                    </div>

                                    <label class="col-sm-2 control-label">ชื่อตัวอย่าง</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_samplename_edit" MaxLength="100" runat="server"
                                            Enabled="false" CssClass="form-control" Text='<%# Eval("sample_name") %>'
                                            placeholder="ชื่อตัวอย่าง ..." />
                                        <%-- <asp:RequiredFieldValidator ID="Requiredtxt_samplename_edit" ValidationGroup="Saveinsert_SampleEdit" runat="server" Display="None"
                                                ControlToValidate="txt_samplename_edit" Font-Size="11" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกชื่อตัวอย่าง" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="Validatortxt_samplename_edit" runat="Server" PopupPosition="TopLeft" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxt_samplename_edit" Width="160" />--%>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ประเภทตัวอย่าง</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_typesample_edit" Enabled="false" runat="server"
                                            CssClass="form-control" Text='<%# Eval("materail_type") %>'
                                            placeholder="ประเภทตัวอย่าง ..." />

                                    </div>

                                    <label class="col-sm-6 control-label"></label>

                                </div>

                                <div class="form-group">
                                    <div class="col-sm-2">
                                        <asp:TextBox ID="tbSample1IDX_Edit" MaxLength="10"
                                            placeholder="วันที่บนตัวอย่าง ..." Visible="false"
                                            Text='<%# Eval("date_sample1_idx") %>' runat="server"
                                            CssClass="form-control" />
                                        <asp:DropDownList ID="ddldate_sample1_idx_edit" runat="server" Enabled="true"
                                            CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                            AutoPostBack="true" />
                                        <%--<asp:RequiredFieldValidator ID="Requireddldate_sample1_idx_edit" ValidationGroup="SaveinsertSampleEdit" runat="server" Display="None"
                                                ControlToValidate="ddldate_sample1_idx_edit" Font-Size="11"
                                                ErrorMessage="*เลือกวันที่บนตัวอย่าง" SetFocusOnError="true"
                                                ValidationExpression="*เลือกวันที่บนตัวอย่าง" InitialValue="-1" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="Validatorddldate_sample1_idx_edit" runat="Server" PopupPosition="TopLeft" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requireddldate_sample1_idx_edit" Width="160" />--%>
                                    </div>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_data_sample1_edit" runat="server" MaxLength="40"
                                            Enabled="true" placeholder="วันที่บนตัวอย่าง ..."
                                            Text='<%# Eval("data_sample1") %>' CssClass="form-control" />
                                        <%--<asp:RequiredFieldValidator ID="Rqtxt_data_sample1_edit" ValidationGroup="SaveinsertSampleEdit" runat="server" Display="None"
                                                ControlToValidate="txt_data_sample1_edit" Font-Size="11" SetFocusOnError="true"
                                                ErrorMessage="*กรอกวันที่บนตัวอย่าง" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="Validatortxt_data_sample1_edit" runat="Server" PopupPosition="TopLeft" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Rqtxt_data_sample1_edit" Width="160" />--%>
                                    </div>

                                    <div class="col-sm-2">
                                        <asp:TextBox ID="tbSample2IDX_Edit" MaxLength="10"
                                            placeholder="วันที่บนตัวอย่าง ..." Visible="false" Enabled="false"
                                            Text='<%# Eval("date_sample2_idx") %>' runat="server"
                                            CssClass="form-control" />
                                        <asp:DropDownList ID="ddldate_sample2_idx_edit" runat="server" Enabled="true"
                                            CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                            AutoPostBack="true" />
                                        <%--<asp:RequiredFieldValidator ID="Requireddldate_sample2_idx_edit" ValidationGroup="SaveinsertSampleEdit" runat="server" Display="None"
                                                ControlToValidate="ddldate_sample2_idx_edit" Font-Size="11" SetFocusOnError="true"
                                                ErrorMessage="*เลือกวันที่บนตัวอย่าง"
                                                ValidationExpression="*เลือกวันที่บนตัวอย่าง" InitialValue="-1" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender166" runat="Server" PopupPosition="TopLeft" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requireddldate_sample2_idx_edit" Width="160" />--%>
                                    </div>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_data_sample2_edit" MaxLength="20"
                                            placeholder="วันที่บนตัวอย่าง ..." Enabled="true"
                                            Text='<%# Eval("data_sample2") %>' runat="server" CssClass="form-control" />
                                        <%--<asp:RequiredFieldValidator ID="Requiredtxt_data_sample2_edit" ValidationGroup="SaveinsertSampleEdit" runat="server" Display="None"
                                                ControlToValidate="txt_data_sample2_edit" Font-Size="11" SetFocusOnError="true"
                                                ErrorMessage="*กรอกวันที่บนตัวอย่าง" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="Validatortxt_data_sample2_edit" runat="Server" PopupPosition="TopLeft" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxt_data_sample2_edit" Width="160" />--%>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Batch</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_batch_edit" runat="server" MaxLength="10"
                                            placeholder="Batch ..." Text='<%# Eval("batch") %>' Enabled="true"
                                            CssClass="form-control" />
                                        <%--<asp:RequiredFieldValidator ID="Requitxt_batch_edit" ValidationGroup="SaveinsertSampleEdit" runat="server" Display="None"
                                                ControlToValidate="txt_batch_edit" Font-Size="11" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอก Batch" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorRequitxt_batch_edit" runat="Server" PopupPosition="TopLeft" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requitxt_batch_edit" Width="160" />--%>
                                    </div>

                                    <label class="col-sm-2 control-label">ชื่อลูกค้า</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_customer_name_edit" MaxLength="50"
                                            placeholder="ชื่อลูกค้า ..." runat="server"
                                            Text='<%# Eval("customer_name") %>' Enabled="true"
                                            CssClass="form-control" />
                                        <%-- <asp:RequiredFieldValidator ID="Requiredtxt_customer_name_edit" ValidationGroup="SaveinsertSampleEdit" runat="server" Display="None"
                                                ControlToValidate="txt_customer_name_edit" Font-Size="11" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกชื่อลูกค้า" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="Validatortxt_customer_name_edit" runat="Server" PopupPosition="TopLeft" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxt_customer_name_edit" Width="160" />--%>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">เลขตู้</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_cabinet_edit" runat="server" MaxLength="10"
                                            placeholder="เลขตู้ ..." Enabled="true" Text='<%# Eval("cabinet") %>'
                                            CssClass="form-control" />
                                        <%-- <asp:RequiredFieldValidator ID="Requiredtxt_cabinet_edit" ValidationGroup="SaveinsertSampleEdit" runat="server" Display="None"
                                                ControlToValidate="txt_cabinet_edit" Font-Size="11" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกเลขตู้" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="Validatortxt_cabinet_edit" runat="Server" PopupPosition="TopLeft" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxt_cabinet_edit" Width="160" />--%>
                                    </div>

                                    <label class="col-sm-2 control-label">กะ</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_shift_time_edit" runat="server" MaxLength="5"
                                            placeholder="กะ ..." Enabled="true" Text='<%# Eval("shift_time") %>'
                                            CssClass="form-control" />
                                        <%--<asp:RequiredFieldValidator ID="Requiredtxt_shift_time_edit" ValidationGroup="SaveinsertSampleEdit" runat="server" Display="None"
                                                ControlToValidate="txt_shift_time_edit" Font-Size="11" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกกะ" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="Validatortxt_shift_time_edit" runat="Server" PopupPosition="TopLeft" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxt_shift_time_edit" Width="160" />--%>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">เวลา</label>
                                    <div class="col-sm-4">
                                        <div class="input-group clockpicker">
                                            <asp:HiddenField ID="HiddenTimeEdit" runat="server" />
                                            <asp:TextBox ID="txt_time_edit" placeholder="เวลา ..." runat="server"
                                                Text='<%# Eval("time") %>' ReadOnly="false" Enabled="true"
                                                CssClass="form-control" value="" /><span class="input-group-addon"><span
                                                    class="glyphicon glyphicon-time"></span></span>
                                            <%--  <asp:RequiredFieldValidator ID="Requiredtxt_time_edit" ValidationGroup="SaveinsertSampleEdit" runat="server" Display="None"
                                                    ControlToValidate="txt_time_edit" Font-Size="11" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกเวลา" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="Validatortxt_time_edit" runat="Server" PopupPosition="TopLeft" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxt_time_edit" Width="160" />--%>
                                        </div>
                                    </div>

                                    <label class="col-sm-2 control-label">รายละเอียดอื่นๆ</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_other_details_edit" placeholder="รายละเอียดอื่นๆ ..."
                                            Text='<%# Eval("other_details") %>' Enabled="true" runat="server"
                                            CssClass="form-control" />

                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">องค์กรสายการผลิต</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_org_idx_production_edit" MaxLength="10"
                                            placeholder="องค์กรสายการผลิต ..." Visible="false"
                                            Text='<%# Eval("org_idx_production") %>' runat="server"
                                            CssClass="form-control" />
                                        <asp:DropDownList ID="ddlorg_idx_production_edit" runat="server"
                                            CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                            AutoPostBack="true" />

                                        <%--<asp:RequiredFieldValidator ID="Requiredddlorg_idx_production_edit" ValidationGroup="SaveinsertSampleEdit" runat="server" Display="None"
                                                    ControlToValidate="ddlorg_idx_production_edit" Font-Size="11"
                                                    ErrorMessage="*กรุณาเลือกองค์กร"
                                                    ValidationExpression="*กรุณาเลือกองค์กร" InitialValue="-1" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" PopupPosition="TopLeft" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredddlorg_idx_production_edit" Width="160" />--%>
                                    </div>

                                    <label class="col-sm-2 control-label">ฝ่ายสายการผลิต</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_rdept_idx_production_edit" MaxLength="10"
                                            placeholder="ฝ่ายสายการผลิต ..." Visible="false"
                                            Text='<%# Eval("rdept_idx_production") %>' runat="server"
                                            CssClass="form-control" />
                                        <asp:DropDownList ID="ddlrdept_idx_production_edit" runat="server"
                                            CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                            AutoPostBack="true" />
                                        <%--<asp:RequiredFieldValidator ID="Requireddlrdept_idx_production_edit" ValidationGroup="SaveinsertSampleEdit" runat="server" Display="None"
                                                    ControlToValidate="ddlrdept_idx_production_edit" Font-Size="11"
                                                    ErrorMessage="*กรุณาเลือกฝ่าย"
                                                    ValidationExpression="*กรุณาเลือกฝ่าย" InitialValue="-1" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" PopupPosition="TopLeft" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requireddlrdept_idx_production_edit" Width="160" />--%>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">แผนกสายการผลิต</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_rsec_idx_production_edit" MaxLength="10"
                                            placeholder="ฝ่ายสายการผลิต ..." Visible="false"
                                            Text='<%# Eval("rsec_idx_production") %>' runat="server"
                                            CssClass="form-control" />
                                        <asp:DropDownList ID="ddlrsec_idx_production_edit" runat="server"
                                            CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                            AutoPostBack="true" />

                                        <%--<asp:RequiredFieldValidator ID="Requireddlrsec_idx_production_edit" ValidationGroup="SaveinsertSampleEdit" runat="server" Display="None"
                                                    ControlToValidate="ddlrsec_idx_production_edit" Font-Size="11"
                                                    ErrorMessage="*กรุณาเลือกแผนก"
                                                    ValidationExpression="*กรุณาเลือกแผนก" InitialValue="-1" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" PopupPosition="TopLeft" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requireddlrsec_idx_production_edit" Width="160" />--%>
                                    </div>

                                    <label class="col-sm-6 control-label"></label>


                                </div>




                                <div class="form-group">
                                    <%--   <label class="col-sm-2 control-label">Certificate</label>--%>
                                    <div class="col-sm-4 control-label textleft">
                                        <asp:RadioButtonList ID="rd_certificate_edit" Visible="false"
                                            Text='<%# Eval("certificate") %>' Enabled="false" runat="server"
                                            CssClass="radioButtonList">
                                            <asp:ListItem Text="มี certificate" Value="1" />
                                            <asp:ListItem Text="ไม่มี certificate" Value="0" />
                                        </asp:RadioButtonList>
                                        <asp:RequiredFieldValidator ID="Requiredrd_certificate_edit" runat="server"
                                            ControlToValidate="rd_certificate_edit" Display="None"
                                            SetFocusOnError="true" ErrorMessage="กรุณาเลือก certificate"
                                            ValidationGroup="SaveinsertSampleEdit" />

                                        <ajaxToolkit:ValidatorCalloutExtender ID="Validatord_certificate_edit"
                                            runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                            TargetControlID="Requiredrd_certificate_edit" PopupPosition="TopLeft"
                                            Width="200" />
                                    </div>

                                    <label class="col-sm-6 control-label"></label>

                                </div>

                                <div class="form-group">
                                    <asp:Panel ID="pnUserEditDocAction" Visible="true" runat="server">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-10">
                                            <asp:LinkButton ID="btnSaveDocUserEdit" CssClass="btn btn-success"
                                                runat="server" data-original-title="Save" data-toggle="tooltip"
                                                OnCommand="btnCommand" Text="Save"
                                                ValidationGroup="Saveinsert_SampleEdit"
                                                CommandArgument='<%# Eval("u0_qalab_idx") + ";" + Eval("u1_qalab_idx") %>'
                                                CommandName="cmdUserSaveEdit"></asp:LinkButton>
                                            <asp:LinkButton ID="btnCancelDocUserEdit" CssClass="btn btn-danger"
                                                runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                OnCommand="btnCommand" Text="Cancel" CommandName="cmdCancelDocUserEdit">
                                            </asp:LinkButton>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                    </div>
                </EditItemTemplate>
            </asp:FormView>

            <asp:FormView ID="fvActor2_Receive" runat="server" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">พิจารณาผล โดย เจ้าหน้าที่ห้องปฏิบัติการ</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">พิจารณาผล</label>
                                    <div class="col-sm-5">
                                        <asp:DropDownList ID="ddlActor2Approve" runat="server" CssClass="form-control"
                                            ValidationGroup="Create_Actor2" />
                                        <asp:RequiredFieldValidator ID="RequiredddlActor2Approve"
                                            ValidationGroup="Create_Actor2" runat="server" Display="None"
                                            ControlToValidate="ddlActor2Approve" Font-Size="11"
                                            ErrorMessage="เลือกผลการพิจารณา" ValidationExpression="เลือกผลการพิจารณา"
                                            InitialValue="-1" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1"
                                            runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                            TargetControlID="RequiredddlActor2Approve" Width="160" />
                                    </div>
                                    <label class="col-sm-5"></label>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Comment</label>
                                    <div class="col-sm-5">
                                        <asp:TextBox ID="tbActor2Comment" runat="server" placeHolder="ความคิดเห็น"
                                            CssClass="form-control" />
                                    </div>
                                    <label class="col-sm-5"></label>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="lbDocSaveReceive" CssClass="btn btn-success" runat="server"
                                            data-original-title="Save" data-toggle="tooltip" Text="Save"
                                            OnCommand="btnCommand" CommandName="cmdDocSaveReceive"
                                            ValidationGroup="Create_Actor2"></asp:LinkButton>
                                        <asp:LinkButton ID="lbDocCancel" CssClass="btn btn-danger" runat="server"
                                            data-original-title="Cancel" data-toggle="tooltip" Text="Cancel"
                                            OnCommand="btnCommand" CommandName="cmdDocCancel"></asp:LinkButton>
                                    </div>
                                </div>

                            </div>
                        </div>


                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:FormView ID="fvActor1_Approve" runat="server" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">พิจารณาผล โดย User</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">พิจารณาผล</label>
                                    <div class="col-sm-5">
                                        <asp:DropDownList ID="ddlActor1Approve" runat="server" CssClass="form-control"
                                            ValidationGroup="Create_Actor1" />
                                        <asp:RequiredFieldValidator ID="Required4ddlActor1Approve"
                                            ValidationGroup="Approve_Actor1" runat="server" Display="None"
                                            ControlToValidate="ddlActor1Approve" Font-Size="11"
                                            ErrorMessage="เลือกผลการพิจารณา" ValidationExpression="เลือกผลการพิจารณา"
                                            InitialValue="-1" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1333"
                                            runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                            TargetControlID="Required4ddlActor1Approve" Width="160" />
                                    </div>
                                    <label class="col-sm-5"></label>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Comment</label>
                                    <div class="col-sm-5">
                                        <asp:TextBox ID="tbActor1Comment" runat="server" CssClass="form-control" />
                                    </div>
                                    <label class="col-sm-5"></label>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="lbSaveApproveActor1" CssClass="btn btn-success"
                                            runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save"
                                            OnCommand="btnCommand" CommandName="cmdSaveApproveActor1"
                                            ValidationGroup="Approve_Actor1"></asp:LinkButton>
                                        <asp:LinkButton ID="lbDocCancel" CssClass="btn btn-danger" runat="server"
                                            data-original-title="Cancel" data-toggle="tooltip" Text="Cancel"
                                            OnCommand="btnCommand" CommandName="cmdDocCancel"></asp:LinkButton>
                                    </div>
                                </div>

                            </div>
                        </div>


                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:FormView ID="fvActor2_Approve" runat="server" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">พิจารณาผล โดยเจ้าหน้าที่ห้องปฏิบัติการ(ส่งต่อ Lab ภายนอก)</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">พิจารณาผล</label>
                                    <div class="col-sm-5">
                                        <asp:DropDownList ID="ddlActor2ApproveN8" runat="server" CssClass="form-control"
                                            ValidationGroup="Create_Actor2N8" />
                                        <asp:RequiredFieldValidator ID="RequiredddlActor2ApproveN8"
                                            ValidationGroup="Create_Actor2N8" runat="server" Display="None"
                                            ControlToValidate="ddlActor2ApproveN8" Font-Size="11"
                                            ErrorMessage="เลือกผลการพิจารณา" ValidationExpression="เลือกผลการพิจารณา"
                                            InitialValue="-1" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1"
                                            runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                            TargetControlID="RequiredddlActor2ApproveN8" Width="160" />
                                    </div>
                                    <label class="col-sm-5"></label>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Comment</label>
                                    <div class="col-sm-5">
                                        <asp:TextBox ID="tbActor2CommentN8" runat="server" CssClass="form-control"
                                            placeHolder="ความคิดเห็น" />
                                    </div>
                                    <label class="col-sm-5"></label>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="lbSaveApproveActor2N8" CssClass="btn btn-success"
                                            runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save"
                                            OnCommand="btnCommand" CommandName="cmdSaveApproveActor2N8"
                                            ValidationGroup="Create_Actor2N8"></asp:LinkButton>
                                        <asp:LinkButton ID="lbDocCancel" CssClass="btn btn-danger" runat="server"
                                            data-original-title="Cancel" data-toggle="tooltip" Text="Cancel"
                                            OnCommand="btnCommand" CommandName="cmdDocCancelUserAdmin"></asp:LinkButton>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:FormView ID="fvResultUser" runat="server" Width="100%">
                <ItemTemplate>
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">ผลการวิเคราะห์ตัวอย่าง</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <%--<label class="col-sm-2 control-label">บันทึกผล</label>--%>
                                    <div class="col-sm-12">
                                        <!-- gridview form record test result -->
                                        <asp:GridView ID="GvResultDetailUser" runat="server" AutoGenerateColumns="false"
                                            CssClass="table table-striped table-bordered table-hover"
                                            HeaderStyle-CssClass="info" OnRowDataBound="gvRowDataBound"
                                            HeaderStyle-Height="30px" ShowHeaderWhenEmpty="True" ShowFooter="False"
                                            AllowPaging="True" BorderStyle="None" CellSpacing="2">
                                            <Columns>
                                                <asp:TemplateField HeaderText="แบบฟอร์มบันทึกการตรวจสอบ"
                                                    HeaderStyle-CssClass="text-center">
                                                    <ItemTemplate>
                                                        <div id="Form1" cssclass="col-sm-12" runat="server">
                                                            <div class="form-horizontal" role="form">
                                                                <asp:Label ID="Lbstaidx_result" runat="server"
                                                                    Visible="false" CssClass="col-sm-12"
                                                                    Text='<%# Eval("staidx") %>'></asp:Label>
                                                                <asp:Label ID="Lbr1form_create_idx" runat="server"
                                                                    Visible="false" CssClass="col-sm-12"
                                                                    Text='<%# Eval("r1_form_create_idx") %>'>
                                                                </asp:Label>
                                                                <%--<asp:Label ID="Lbform_detail_root_idx" runat="server" Visible="false" CssClass="col-sm-12" Text='<%# Eval("form_detail_root_idx") %>'>
                                                                </asp:Label>--%>
                                                                <asp:Label ID="Lbu2_qalab_idx" runat="server"
                                                                    Visible="false" CssClass="col-sm-12"
                                                                    Text='<%# Eval("u2_qalab_idx") %>'></asp:Label>
                                                                <asp:Label ID="Lbu1_qalab_idx" runat="server"
                                                                    Visible="false" CssClass="col-sm-12"
                                                                    Text='<%# Eval("u1_qalab_idx") %>'></asp:Label>
                                                                <asp:Label ID="Lbu0_qalab_idx" runat="server"
                                                                    Visible="false" CssClass="col-sm-12"
                                                                    Text='<%# Eval("u0_qalab_idx") %>'></asp:Label>
                                                                <%--     <asp:Label ID="lbtext_detail_name" runat="server" Font-Bold="true" Visible="false" Text='<%# Eval("text_name_setform") %>'
                                                                CssClass="col-sm-12"></asp:Label>
                                                                <asp:Label ID="Lboption_idx" runat="server"
                                                                    Visible="false" CssClass="col-sm-12"
                                                                    Text='<%# Eval("option_idx") %>'></asp:Label>--%>
                                                                <asp:Label ID="lbform_detail_name" runat="server"
                                                                    Font-Bold="false" Visible="true" CssClass="col-sm-8"
                                                                    Text='<%# Eval("r0_form_create_name") %>'>
                                                                </asp:Label>
                                                                <div class="col-sm-4">
                                                                    <asp:TextBox ID="txtRecordResultTest" runat="server"
                                                                        Visible="true" CssClass="form-control small"
                                                                        Text='<%# Eval("detail_tested") %>'
                                                                        Enabled="false"></asp:TextBox>
                                                                    <%--<asp:DropDownList ID="ddlChooseResult" Visible="true" Text='<%# Eval("detail_tested") %>'
                                                                    runat="server"
                                                                    CssClass="form-control" placeholder=""
                                                                    Enabled="true">
                                                                    </asp:DropDownList>--%>
                                                                </div>
                                                                <%--   <div class="col-sm-4">
                                                                        <asp:Label ID="lbRecordResult" runat="server" Visible="false" 
                                                                            CssClass="lable-control" placeholder="" Enabled="true"></asp:Label>
                                                                    </div>--%>
                                                            </div>
                                                        </div>
                                                        <!-- gridview form record test result level 2 -->
                                                        <%--<asp:GridView ID="GvResult" GridLines="None"
                                                            runat="server"
                                                            ShowHeader="false" AutoGenerateColumns="false"
                                                            CssClass="table table-striped table-bordered"
                                                            HeaderStyle-CssClass="info"
                                                            HeaderStyle-Height="30px"
                                                            ShowHeaderWhenEmpty="True"
                                                            OnRowDataBound="gvRowDataBound"
                                                            ShowFooter="False"
                                                            AllowPaging="True"
                                                            BorderStyle="None"
                                                            CellSpacing="2">
                                                            <Columns>--%>
                                                        <%-- <asp:TemplateField ControlStyle-Font-Size="Small">
                                                                    <ItemTemplate>
                                                                        <div class="form-horizontal" role="form">
                                                                            <asp:Label ID="Lbform_detail_idxlv2" runat="server" Visible="false" CssClass="col-sm-12" Text='<%# Eval("form_detail_idx") %>'>
                                                        </asp:Label>
                                                        <asp:Label ID="Lbu2_qalab_idxlv2" runat="server" Visible="false"
                                                            CssClass="col-sm-12" Text='<%# Eval("u2_qalab_idx") %>'>
                                                        </asp:Label>
                                                        <asp:Label ID="Lbr1_form_create_idxlv2" runat="server"
                                                            Visible="false" CssClass="col-sm-12"
                                                            Text='<%# Eval("r1_form_create_idx") %>'></asp:Label>
                                                        <asp:Label ID="Lbform_detail_root_idx" runat="server"
                                                            Visible="false" CssClass="col-sm-12"
                                                            Text='<%# Eval("r1_form_detail_root_idx") %>'></asp:Label>
                                                        <asp:Label ID="Lboption_idxlv2" runat="server" Visible="false"
                                                            CssClass="col-sm-12" Text='<%# Eval("option_idx") %>'>
                                                        </asp:Label>

                                                        <asp:Label ID="lbform_detail_name1" runat="server"
                                                            CssClass="col-sm-8">
                                                            <p><i class="fa fa-circle" style="font-size:1%;"
                                                                    aria-hidden="true"></i>
                                                                <%# Eval("text_name_setform") +" 2 " %></p>
                                                        </asp:Label>
                                                        <div class="col-sm-4">
                                                            <asp:TextBox ID="txtRecordResult" runat="server"
                                                                Visible="false" CssClass="form-control" placeholder=""
                                                                Enabled="true"></asp:TextBox>

                                                        </div>
                                    </div>--%>

                                    <%--<asp:GridView ID="GvResultRoot"
                                                                            runat="server" GridLines="None"
                                                                            ShowHeader="false" AutoGenerateColumns="false"
                                                                            CssClass="table table-striped table-bordered table-hover"
                                                                            HeaderStyle-CssClass="info"
                                                                            HeaderStyle-Height="30px"
                                                                            ShowHeaderWhenEmpty="True"
                                                                            ShowFooter="False"
                                                                            AllowPaging="True"
                                                                            BorderStyle="None"
                                                                            CellSpacing="2">
                                                                            <Columns>
                                                                                <asp:TemplateField>
                                                                                    <ItemTemplate>
                                                                                        <div class="form-horizontal" role="form">
                                                                                            <asp:Label ID="lbform_detail_name1" runat="server" Visible="false" CssClass="col-sm-12" Text='<%# Eval("text_name_setform") +" :level 3 " %>'>
                                    </asp:Label>
                                    <asp:Label ID="Lbform_detail_idx" runat="server" Visible="false"
                                        CssClass="col-sm-12" Text='<%# Eval("form_detail_idx") %>'></asp:Label>
                                    <asp:Label ID="Lbr1_form_create_idx2" runat="server" Visible="false"
                                        CssClass="col-sm-12" Text='<%# Eval("r1_form_create_idx") %>'></asp:Label>
                                    <asp:Label ID="Lboption_idxlv3" runat="server" Visible="false" CssClass="col-sm-12"
                                        Text='<%# Eval("option_idx") %>'></asp:Label>

                                    <asp:Label ID="Label2" runat="server" CssClass="col-sm-8">
                                        <p><%# Eval("text_name_setform") +" 3 " %></p>
                                    </asp:Label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txtRecordResultRoot" runat="server" Visible="true"
                                            CssClass="form-control small" placeholder="" Enabled="true"></asp:TextBox>
                                    </div>
                                </div>
                </ItemTemplate>
                </asp:TemplateField>

                </Columns>
                </asp:GridView>--%>
                <%--</ItemTemplate>
                                                                </asp:TemplateField>--%>

                <%--                                                            </Columns>
                                                        </asp:GridView>--%>
                </ItemTemplate>
                </asp:TemplateField>
                </Columns>
                </asp:GridView>

                <div class="form-group">
                    <asp:Panel ID="divfileViewUser" class="form-group" runat="server" Visible="true">
                        <%--  <asp:Label ID="lbfileupload" runat="server" CSSClass="col-sm-2 control-label">เอกสารอ้างอิง</asp:Label>--%>
                        <div class="col-sm-9">
                            <asp:GridView ID="gvFileResultUser" Visible="true" runat="server"
                                OnRowDataBound="gvRowDataBound" AutoGenerateColumns="false" BackColor="White"
                                BorderColor="White" ShowFooter="false" ShowHeader="false" CssClass="table">
                                <Columns>
                                    <asp:TemplateField ItemStyle-BorderColor="White" ControlStyle-BackColor="White">
                                        <ItemTemplate>
                                            <div class="col-lg-9">
                                                <asp:Label ID="ltFileName112" runat="server"
                                                    Text='<%# "ไฟล์บันทึกผลรายการตรวจวิเคราะห์ : " + Eval("FileName") %>' />
                                                &nbsp;
                                                <asp:HyperLink runat="server" ID="btnDL112" CssClass="pull-letf"
                                                    data-toggle="tooltip" title="view" data-original-title=""
                                                    Target="_blank"><i class="fa fa-file-text-o"></i> ดูรายละเอียด
                                                </asp:HyperLink>
                                                <asp:HiddenField runat="server" ID="hidFile112"
                                                    Value='<%# Eval("Download") %>' />
                                            </div>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </asp:Panel>
                </div>



                </div>
                </div>


                </div>
                </div>

                </div>
                </ItemTemplate>
            </asp:FormView>

            <div class="form-group" id="div_showhistory" runat="server">
                <asp:LinkButton ID="btnHistoryDocument" Visible="true" CssClass="btn btn-primary" runat="server"
                    data-original-title="History" data-toggle="tooltip" CommandName="btnHistoryDocument"
                    OnCommand="btnCommand"><span class="glyphicon glyphicon-search"></span> ประวัติการดำเนินการ
                </asp:LinkButton>
                <asp:LinkButton ID="btnHistoryBack" CssClass="btn btn-primary" runat="server" Visible="false"
                    data-original-title="Back" data-toggle="tooltip" OnCommand="btnCommand"
                    CommandName="btnHistoryBack"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>
                    กลับ</asp:LinkButton>
            </div>

            <%--<div class="form-group" id="div_showhistoryback" runat="server" visible="false">
                <asp:LinkButton ID="btnHistoryBack" CssClass="btn btn-info" runat="server" data-original-title="Back" data-toggle="tooltip" OnCommand="btnCommand" CommandName="btnHistoryBack"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> กลับ</asp:LinkButton>
            </div>--%>

            <asp:Repeater ID="rptHistoryDocument" runat="server">
                <HeaderTemplate>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">รายละเอียดการดำเนินการ</h3>
                        </div>
                        <div class="panel-body">
                </HeaderTemplate>
                <ItemTemplate>
                    <%--<asp:HiddenField ID="hfProcessActorIDX" OnItemDataBound="rptItemDataBound" runat="server" Value='<%# Eval("actor_idx") %>'
                    />
                    <asp:HiddenField ID="hfProcessFromNode" runat="server" Value='<%# Eval("from_node") %>' />--%>
                    <div class="form-horizontal">
                        <div class="form-group">

                            <div class="col-sm-2 control-label">
                                <span>ดำเนินการโดย</span>
                            </div>
                            <div class="col-sm-4 control-label textleft">
                                <span><%# Eval("current_artor") %></span>
                            </div>
                            <div class="col-sm-2 control-label">
                                <span>ผลการดำเนินการ</span>
                            </div>
                            <div class="col-sm-4 control-label textleft">
                                <span><%# Eval("current_decision") %></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2 control-label">
                                <span>โดย</span>
                            </div>
                            <div class="col-sm-4 control-label textleft">
                                <span><%# Eval("emp_name_th") %></span>

                            </div>
                            <div class="col-sm-2 control-label">
                                <span>วันที่</span>
                            </div>
                            <div class="col-sm-4 control-label textleft">
                                <span><%# Eval("create_date") %></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2 control-label">
                                <span>Sample Code</span>
                            </div>
                            <div class="col-sm-4 control-label textleft">
                                <asp:Label ID="lblsample_code" runat="server" Text='<%# Eval("sample_code") %>' />
                            </div>
                            <div class="col-sm-2 control-label">
                                <span>comment</span>
                            </div>
                            <div class="col-sm-4 control-label textleft">
                                <asp:Label ID="lblComment" runat="server" Text='<%# Eval("comment") %>' />
                            </div>
                            <asp:HyperLink ID="setOnSamplelist" runat="server"></asp:HyperLink>
                        </div>
                    </div>
                    <hr />
                </ItemTemplate>
                <FooterTemplate>
                    </div>
                    </div>

                </FooterTemplate>
            </asp:Repeater>

            <asp:Repeater ID="rptHistorySampleCode" runat="server">
                <HeaderTemplate>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">รายละเอียดการดำเนินการ</h3>
                        </div>
                        <div class="panel-body">
                </HeaderTemplate>
                <ItemTemplate>
                    <%--<asp:HiddenField ID="hfProcessActorIDX" OnItemDataBound="rptItemDataBound" runat="server" Value='<%# Eval("actor_idx") %>'
                    />
                    <asp:HiddenField ID="hfProcessFromNode" runat="server" Value='<%# Eval("from_node") %>' />--%>
                    <div class="form-horizontal">
                        <div class="form-group">
                            <div class="col-sm-2 control-label">
                                <span>ดำเนินการโดย</span>
                            </div>
                            <div class="col-sm-4 control-label textleft">
                                <span><%# Eval("current_artor") %></span>
                            </div>
                            <div class="col-sm-2 control-label">
                                <span>ผลการดำเนินการ</span>
                            </div>
                            <div class="col-sm-4 control-label textleft">
                                <span><%# Eval("current_decision") %></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2 control-label">
                                <span>โดย</span>
                            </div>
                            <div class="col-sm-4 control-label textleft">
                                <span><%# Eval("emp_name_th") %></span>
                            </div>
                            <div class="col-sm-2 control-label">
                                <span>วันที่</span>
                            </div>
                            <div class="col-sm-4 control-label textleft">
                                <span><%# Eval("create_date") %></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2 control-label">
                                <span>comment</span>
                            </div>
                            <div class="col-sm-10 control-label textleft">
                                <asp:Label ID="lblComment" runat="server" Text='<%# Eval("comment") %>' />
                            </div>
                        </div>
                    </div>
                    <hr />
                </ItemTemplate>
                <FooterTemplate>
                    </div>
                    </div>

                </FooterTemplate>
            </asp:Repeater>
            </div>
        </asp:View>
        <!--View Create-->

        <!--View Detail-->
        <asp:View ID="docDetail" runat="server">

            <div id="divDetail" runat="server" class="col-md-12">
                <div class="form-group">
                    <asp:LinkButton ID="btnSearchIndex" CssClass="btn btn-info" runat="server"
                        data-original-title="แสดงแถบเครื่องมือค้นหา" data-toggle="tooltip" CommandName="btnSearchIndex"
                        OnCommand="btnCommand"><span class="glyphicon glyphicon-search"></span> ค้นหา</asp:LinkButton>

                    <asp:LinkButton ID="btnResetSearchPage" CssClass="btn btn-default" runat="server"
                        data-original-title="ล้างค่า" data-toggle="tooltip" CommandName="cmdResetSearchPage"
                        OnCommand="btnCommand"><span class="glyphicon glyphicon-refresh"></span></asp:LinkButton>
                </div>

                <div class="form-group">
                    <div id="fvBacktoIndex" class="row" runat="server" visible="false">
                        <asp:LinkButton ID="btnSearchBack" CssClass="btn btn-danger" runat="server"
                            data-original-title="ซ่อนแถบเครื่องมือการค้นหา" data-toggle="tooltip" OnCommand="btnCommand"
                            CommandName="btnSearchBack"><span class="fa fa-times" aria-hidden="true"></span>
                            &nbsp;ซ่อนแถบเครื่องมือการค้นหา</asp:LinkButton>
                    </div>
                </div>

                <!--*** START Search Index ***-->
                <div id="showsearch" runat="server" visible="false">
                    <div class="panel panel-info">
                        <div class="panel-heading f-bold">ค้นหารายการส่งตรวจสอบ</div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">

                                <%-------------- Document No, Sample Code --------------%>
                                <div class="form-group">
                                    <asp:Label ID="lborg_idxsearch" CssClass="col-sm-2 control-label" runat="server"
                                        Text="Document No : " />
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_document_nosearch" placeholder="Document No ..."
                                            MaxLength="8" runat="server" CssClass="form-control">
                                        </asp:TextBox>

                                    </div>

                                    <asp:Label ID="lbrdept_idxsearch" CssClass="col-sm-2 control-label" runat="server"
                                        Text="Sample Code : " />
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_sample_codesearch" AutoPostBack="true" MaxLength="7"
                                            placeholder="Sample Code ..." runat="server" CssClass="form-control">

                                        </asp:TextBox>

                                    </div>

                                </div>
                                <%-------------- Document No, Sample Code --------------%>

                                <%-------------- Mat., เลขตู้  --------------%>
                                <div class="form-group">
                                    <asp:Label ID="Label1" CssClass="col-sm-2 control-label" runat="server"
                                        Text="เลข Material : " />
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_mat_search" AutoPostBack="true" MaxLength="7"
                                            placeholder="เลข Material ..." runat="server" CssClass="form-control">
                                        </asp:TextBox>

                                    </div>

                                    <asp:Label ID="Label2" CssClass="col-sm-2 control-label" runat="server"
                                        Text="เลขตู้ : " />
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_too_search" AutoPostBack="true" MaxLength="5"
                                            placeholder="เลขตู้ ..." runat="server" CssClass="form-control">

                                        </asp:TextBox>

                                    </div>

                                </div>

                                <%-------------- ชื่อลุกค้า, วันที่ส่ง --------------%>
                                <div class="form-group">
                                    <asp:HyperLink ID="setTopOnSearch" runat="server"></asp:HyperLink>
                                    <asp:Label ID="Label4" CssClass="col-sm-2 control-label" runat="server"
                                        Text="ชื่อลูกค้า : " />
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_customer_search" MaxLength="50"
                                            placeholder="ชื่อลูกค้า ..." runat="server" CssClass="form-control">
                                        </asp:TextBox>
                                    </div>
                                    <asp:Label ID="Label3" CssClass="col-sm-2 control-label" runat="server"
                                        Text="วันที่ส่งตรวจ : " />
                                    <div class="col-sm-4">
                                        <div class='input-group date from-date-datepicker'>
                                            <asp:HiddenField ID="HiddenDateSearch" runat="server" />
                                            <asp:TextBox ID="txt_date_search" placeholder="Date ..." runat="server"
                                                CssClass="form-control" ReadOnly="true">
                                            </asp:TextBox><span class="input-group-addon"><span data-icon-element=""
                                                    class="glyphicon glyphicon-calendar"></span></span>
                                        </div>

                                    </div>
                                </div>
                                <%-------------- เวลา,กะ --------------%>
                                <div class="form-group">
                                    <asp:Label ID="Label5" CssClass="col-sm-2 control-label" runat="server"
                                        Text="กะ : " />
                                    <div class="col-sm-4">

                                        <asp:TextBox ID="txt_shift_search" AutoPostBack="true" MaxLength="5"
                                            CssClass="form-control" placeholder="กะ ..." runat="server" />
                                    </div>


                                    <asp:Label ID="Label6" CssClass="col-sm-2 control-label" runat="server"
                                        Text="เวลา : " />
                                    <div class="col-sm-4">
                                        <div class="input-group clockpicker">
                                            <asp:HiddenField ID="HiddenTimeSearch" runat="server" />
                                            <%--<asp:TextBox ID="txt_time_search" AutoPostBack="true" placeholder="Time ..." value="" runat="server" CssClass="form-control clockpicker"/><span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>    --%>
                                            <asp:TextBox ID="txt_time_search" placeholder="เวลา ..." runat="server"
                                                Enabled="true" value="" CssClass="form-control" /><span
                                                class="input-group-addon"><span
                                                    class="glyphicon glyphicon-time"></span></span>

                                        </div>
                                    </div>
                                </div>
                                <%-------------- btnsearch --------------%>
                                <div class="form-group">
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-4">
                                        <div class="pull-left">
                                            <asp:LinkButton ID="btnSearchIndexDetail" CssClass="btn btn-primary"
                                                runat="server" ValidationGroup="SearchIndexDetail"
                                                OnCommand="btnCommand" CommandName="btnSearchIndexDetail"
                                                data-toggle="tooltip" title="ค้นหา"
                                                OnClientClick="return confirm('คุณต้องการค้นหารายการนี้ใช่หรือไม่ ?')">
                                                <span class="glyphicon glyphicon-search"></span>&nbsp;ค้นหา
                                            </asp:LinkButton>

                                            <asp:LinkButton ID="btnResetSearch" CssClass="btn btn-default"
                                                runat="server" OnCommand="btnCommand" CommandName="resetSearch"
                                                data-toggle="tooltip" title="ล้างค่า"><i
                                                    class="fa fa-refresh"></i>&nbsp;ล้างค่า</asp:LinkButton>
                                        </div>

                                    </div>
                                </div>
                                <%-------------- btnsearch --------------%>
                            </div>
                        </div>

                    </div>
                </div>
                <!--*** END Search Index *** table table-bordered table-striped table-responsive col-md-12 footable    table table-striped table-bordered table-hover table-responsive -->
                <div class="form-group">
                    <asp:Repeater ID="rptBindPlaceForSelectDoc" OnItemDataBound="rptOnRowDataBound" runat="server">
                        <ItemTemplate>
                            <asp:Label ID="lbcheck_coler_place" runat="server" Visible="false"
                                Text='<%# Eval("place_idx") %>'></asp:Label>
                            <asp:LinkButton ID="btnDocumentPlaceLab" CssClass="btn btn-primary" runat="server"
                                data-original-title='<%# Eval("place_name") %>' data-toggle="tooltip"
                                CommandArgument='<%# Eval("place_idx")+ ";" + Eval("place_name") %>'
                                OnCommand="btnCommand" Text='<%# Eval("place_name") %>'
                                CommandName="cmdDocumentPlaceLab"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:Repeater>
                    <asp:Label ID="lblplaceName" runat="server" Visible="true"></asp:Label>
                </div>

                <div class="form-group">
                    <h5>
                        <asp:Label ID="lbl_detail_placeindex" Font-Bold="true" runat="server" Visible="false">
                        </asp:Label>
                    </h5>

                </div>

                <%--<div id="divgvDoingList_scroll" style="overflow-x: scroll; width: 100%" visible="true" runat="server">--%>
                <asp:GridView ID="gvDoingList" runat="server" AutoGenerateColumns="False"
                    CssClass="table table-bordered table-striped table-responsive col-md-12 footable"
                    HeaderStyle-CssClass="info" OnPageIndexChanging="gvPageIndexChanging"
                    OnRowDataBound="gvRowDataBound" AllowPaging="True" PageSize="10" DataKeyNames="u0_qalab_idx">
                    <RowStyle Font-Size="Small" />
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First"
                        LastPageText="Last" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่พบข้อมูล</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="Document No." ItemStyle-CssClass="f-s-13 text-left"
                            HeaderStyle-CssClass="f-s-13 text-center" HeaderStyle-Width="15%">
                            <ItemTemplate>
                                <asp:Label ID="lblDocIDX" runat="server" Visible="false"
                                    Text='<%# Eval("u0_qalab_idx") %>'></asp:Label>
                                <asp:Label ID="lblCemp_idx_per" runat="server" Visible="false"
                                    Text='<%# Eval("cemp_idx") %>'></asp:Label>
                                <asp:Label ID="lblplaceIDX" runat="server" Visible="false"
                                    Text='<%# Eval("place_idx") %>'></asp:Label>
                                <%--                            <asp:Label ID="lblRsecidxView" runat="server" Visible="true" Text='<%# Eval("cemp_rsec") %>'>
                                </asp:Label>--%>
                                <asp:Label ID="lblDocCode" runat="server" Text='<%# Eval("document_code") %>'>
                                </asp:Label>
                                <%--  <asp:Label ID="lblRsecidxView" runat="server" Text='<%#Eval("rsec_idx") %>'>
                                </asp:Label>--%>
                                <%--  <asp:GridView ID="gvRsecView"
                                runat="server" ShowFooter="false" ShowHeader="false"
                                AutoGenerateColumns="False"
                                CssClass="table table-striped table-bordered table-hover table-responsive">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Label ID="lblRsecidxU0Doc" runat="server" Text='<%#Eval("u0_qalab_idx") %>'>
                                </asp:Label>
                                <asp:Label ID="lblRsecidxView" runat="server" Text='<%#Eval("rsec_idx") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>--%>
                </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Create Date" ItemStyle-CssClass="f-s-13 text-left"
                    HeaderStyle-CssClass="f-s-13 text-center" HeaderStyle-Width="20%">
                    <ItemTemplate>
                        <%--<asp:Label ID="lblCreateDate" runat="server" Text='<%# getOnlyDate((string)Eval("create_date")) %>'>
                        </asp:Label>--%>
                        <asp:Label ID="lblCreateDate" runat="server" Text='<%#Eval("create_date") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Received Date" ItemStyle-CssClass="f-s-13 text-left"
                    HeaderStyle-CssClass="f-s-13 text-center" HeaderStyle-Width="20%">
                    <ItemTemplate>

                        <asp:Label ID="lblReceived_date" runat="server" Text='<%# Eval("received_date") %>'></asp:Label>

                        <%--<strong>เรื่อง :</strong>&nbsp;<asp:Label ID="lblDocTitle" runat="server" Text='<%# Eval("doc_title") %>'>
                        </asp:Label><br />
                        <small>
                            <strong>ประเภทเอกสาร :</strong>&nbsp;<asp:Label ID="lblDocType" runat="server"
                                Text='<%# Eval("doc_type_name") + " - " + Eval("doc_sub_type_name") %>'></asp:Label>
                        </small>--%>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Document Status" ItemStyle-CssClass="f-s-13 text-left"
                    HeaderStyle-CssClass="f-s-13 text-center" HeaderStyle-Width="15%">
                    <ItemTemplate>
                        <asp:Label ID="lblDocStatus" runat="server" Text='<%# Eval("status_name") %>'></asp:Label>
                        <asp:Label ID="lstaidx" runat="server" Visible="false" Text='<%# Eval("staidx") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Action" ItemStyle-CssClass="f-s-13 text-left"
                    HeaderStyle-CssClass="f-s-13 text-center" HeaderStyle-Width="10%">
                    <ItemTemplate>
                        <%--<asp:Label ID="lblDocStatus" runat="server" Text='<%# Eval("doc_status_name") + " โดย " + Eval("current_actor_name") %>'>
                        </asp:Label>--%>
                        <asp:LinkButton ID="btnView" CssClass="btn-sm btn-info" runat="server"
                            CommandName="cmdDocDetail" OnCommand="btnCommand"
                            CommandArgument='<%# Eval("u0_qalab_idx") %>' data-toggle="tooltip" title="View"><i
                                class="fa fa-file-text-o"></i></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>

                </Columns>
                </asp:GridView>
                <%--</div>--%>
            </div>
        </asp:View>
        <!--View Detail-->

        <!--View Lab-->
        <asp:View ID="docLab" runat="server">
            <%--tab3--%>
            <div id="divLab" runat="server" class="col-md-12">
                <div class="form-horizontal" role="form" id="show_search_lab" runat="server">
                    <div class="col-sm-12">
                        <div class="form-group">

                            <%--<asp:LinkButton ID="LinkButton1" CssClass="btn btn-primary" runat="server" data-original-title="Search.." data-toggle="tooltip" OnCommand="btnCommand" CommandName="btnSearchSampleLab">นพวงศ์</asp:LinkButton>
                                <asp:LinkButton ID="LinkButton2" CssClass="btn btn-primary" runat="server" data-original-title="Reset.." data-toggle="tooltip" OnCommand="btnCommand" CommandArgument='1' CommandName="btnResetInLab">โรจนะ</asp:LinkButton>--%>


                            <div class="input-group col-sm-4 pull-right">
                                <asp:TextBox ID="txtseach_samplecode_lab" runat="server"
                                    placeholder="Sample Name/ Sample Code" CssClass="form-control" />
                                <div class="input-group-btn">
                                    <asp:LinkButton ID="btnSearchSampleLab" CssClass="btn btn-primary" runat="server"
                                        data-original-title="Search.." data-toggle="tooltip" OnCommand="btnCommand"
                                        CommandName="btnSearchSampleLab"><i class="glyphicon glyphicon-search"></i>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnResetInLab" CssClass="btn btn-default" runat="server"
                                        data-original-title="Reset.." data-toggle="tooltip" OnCommand="btnCommand"
                                        CommandArgument='1' CommandName="btnResetInLab"><i class="fa fa-refresh"
                                            aria-hidden="true"></i></asp:LinkButton>
                                </div>
                            </div>

                            <label class="col-sm-8 control-label"></label>

                        </div>
                        <div class="form-group">

                            <asp:Repeater ID="rp_place" OnItemDataBound="rptOnRowDataBound" runat="server">
                                <ItemTemplate>
                                    <asp:Label ID="lbcheck_coler" runat="server" Visible="false"
                                        Text='<%# Eval("m0_lab_idx") %>'></asp:Label>
                                    <asp:LinkButton ID="btnPlaceLab" CssClass="btn btn-primary" runat="server"
                                        data-original-title='<%# Eval("lab_name") %>' data-toggle="tooltip"
                                        CommandArgument='<%# Eval("m0_lab_idx") %>' OnCommand="btnCommand"
                                        Text='<%# Eval("lab_name") %>' CommandName="cmdPlaceLab"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <h4>
                        <asp:Label ID="lbdetailsShow" Font-Bold="true" runat="server" Visible="false">
                        </asp:Label>
                    </h4>

                </div>
                <%--<div id="divgvLab_scroll" style="overflow-x: scroll; width: 100%" visible="true" runat="server">--%>

                <asp:GridView ID="gvLab" runat="server" AutoGenerateColumns="False"
                    CssClass="table table-striped table-bordered table-responsive col-md-12" HeaderStyle-CssClass="info"
                    OnPageIndexChanging="gvPageIndexChanging" OnRowDataBound="gvRowDataBound" AllowPaging="True"
                    PageSize="10" DataKeyNames="u1_qalab_idx">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First"
                        LastPageText="Last" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่พบข้อมูล</div>
                    </EmptyDataTemplate>
                    <Columns>

                        <asp:TemplateField HeaderText="Sample Name" ItemStyle-CssClass="f-s-13 text-left"
                            HeaderStyle-CssClass="f-s-13 text-center" HeaderStyle-Width="12%">
                            <ItemTemplate>
                                <asp:Label ID="lblSampleName" runat="server" Text='<%# Eval("material_name") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="Sample Code" ItemStyle-CssClass="f-s-13 text-left"
                            HeaderStyle-CssClass="f-s-13 text-center" HeaderStyle-Width="10%">
                            <ItemTemplate>
                                <asp:Label ID="lblU0IDX_lab" runat="server" Visible="false"
                                    Text='<%# Eval("u0_qalab_idx") %>'></asp:Label>
                                <asp:Label ID="lblU1IDX_Lab" runat="server" Visible="false"
                                    Text='<%# Eval("u1_qalab_idx") %>'></asp:Label>
                                <asp:Label ID="lblSampleCode" runat="server" Text='<%# Eval("sample_code") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Tested Detail" ItemStyle-CssClass="f-s-13 text-left"
                            HeaderStyle-CssClass="f-s-13 text-center" HeaderStyle-Width="15%">
                            <ItemTemplate>

                                <%-- <div class="panel-body">--%>
                                <table class="table table-striped f-s-12"
                                    style="background-color: transparent; border: hidden; margin-top: 0;">
                                    <asp:Repeater ID="rp_testdetail_lab" runat="server">

                                        <ItemTemplate>
                                            <tr style="background-color: transparent; border: hidden;">
                                                <td style="background-color: transparent;">
                                                    <%# " - " +  Eval("test_detail_name") %></td>
                                                <asp:Label ID="test_detail_idxlbl" runat="server"
                                                    Text='<%# Eval("test_detail_idx") %>' Visible="false"></asp:Label>
                                                <%--<asp:Label ID="lblu2test_detail_name" runat="server" Text='<%# Eval("test_detail_name") %>'
                                                Visible="true"></asp:Label>--%>
                                            </tr>


                                            <tr>
                                                <asp:Label ID="lblU2IDX_Lab_r4" runat="server"
                                                    Text='<%# Eval("u2_qalab_idx") %>' Visible="false"></asp:Label>
                                            </tr>
                                        </ItemTemplate>

                                    </asp:Repeater>
                                </table>
                                <%-- </div>--%>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Lab" ItemStyle-CssClass="f-s-13 text-left"
                            HeaderStyle-CssClass="f-s-13 text-center" HeaderStyle-Width="8%">
                            <ItemTemplate>
                                <asp:Label ID="lblm0_lab_idx" runat="server" Visible="false"
                                    Text='<%# Eval("m0_lab_idx") %>'></asp:Label>

                                <asp:Label ID="lbllab_name" runat="server" Text='<%# Eval("lab_name") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Status" ItemStyle-CssClass="f-s-13 text-left"
                            HeaderStyle-CssClass="f-s-13 text-center" HeaderStyle-Width="10%">
                            <ItemTemplate>
                                <asp:Label ID="lblStaidx_Lab" runat="server" Visible="false"
                                    Text='<%# Eval("staidx") %>'></asp:Label>
                                <asp:Label ID="lbstatus_name_Lab" runat="server" Text='<%# Eval("status_name") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ดำเนินการ" ItemStyle-CssClass="f-s-13 text-center"
                            HeaderStyle-CssClass="f-s-13 text-center" HeaderStyle-Width="15%">
                            <ItemTemplate>


                                <asp:LinkButton ID="lnkbtnSaveN4Accept" CssClass="btn btn-success" runat="server"
                                    data-original-title="Accept" data-toggle="tooltip" Text="Accept"
                                    OnCommand="btnCommand" CommandName="cmdSaveReceiveN4"
                                    CommandArgument='<%# Container.DataItemIndex + ";" + Eval("u0_qalab_idx")+ ";" + Eval("u1_qalab_idx")+ ";" + "6"%>'>
                                </asp:LinkButton>
                                <asp:LinkButton ID="lnkbtnSaveN4Reject" CssClass="btn btn-danger" runat="server"
                                    data-original-title="Reject" data-toggle="tooltip" Text="Reject"
                                    OnCommand="btnCommand" CommandName="cmdRejectN4e"
                                    CommandArgument='<%# Container.DataItemIndex + ";" + Eval("u0_qalab_idx") + ";" + Eval("u1_qalab_idx") + ";" + "5" %>'>
                                </asp:LinkButton>


                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Action" ItemStyle-CssClass="f-s-13 text-left"
                            HeaderStyle-CssClass="f-s-13 text-center" HeaderStyle-Width="10%">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnViewSucess" CssClass="btn-sm btn-info" runat="server"
                                    CommandName="cmdDocDetailLab" OnCommand="btnCommand"
                                    CommandArgument='<%# Eval("u1_qalab_idx")+ ";" + Eval("staidx") %>'
                                    data-toggle="tooltip" title="View"><i class="fa fa-file-text-o"></i>
                                </asp:LinkButton>

                                <asp:LinkButton ID="btnReportSucess" CssClass="btn-sm btn-success" runat="server"
                                    CommandName="cmdReportSucess" OnCommand="btnCommand"
                                    CommandArgument='<%# Eval("u1_qalab_idx")%>' data-toggle="tooltip" title="Report"><i
                                        class="fa fa-file-code-o"></i></asp:LinkButton>

                            </ItemTemplate>
                        </asp:TemplateField>


                    </Columns>
                </asp:GridView>


                <div id="save_node4" class="form-group" runat="server" visible="false">
                    <div class="pull-right">
                        <asp:LinkButton ID="lnkbtnSaveN4" CssClass="btn btn-success" runat="server"
                            data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand"
                            CommandName="cmdSaveNode4"></asp:LinkButton>
                        <asp:LinkButton ID="lnkbtnCancel" CssClass="btn btn-danger" runat="server"
                            data-original-title="Cancel" data-toggle="tooltip" Text="Cancel" OnCommand="btnCommand"
                            CommandName="cmdCancel" CommandArgument="0"></asp:LinkButton>
                    </div>
                    <br />
                </div>

                <asp:FormView ID="fvEmpDetailLab" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <ItemTemplate>
                        <asp:HiddenField ID="hfEmpOrgIDX_Lab" runat="server" Value='<%# Eval("org_idx") %>' />
                        <asp:HiddenField ID="hfEmpRdeptIDX" runat="server" Value='<%# Eval("rdept_idx") %>' />
                        <asp:HiddenField ID="hfEmpRsecIDX" runat="server" Value='<%# Eval("rsec_idx") %>' />
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายละเอียดผู้ใช้งาน</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">รหัสพนักงาน</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpCode" runat="server" CssClass="form-control"
                                                Text='<%# Eval("emp_code") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ชื่อ - นามสกุล</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpName" runat="server" CssClass="form-control"
                                                Text='<%# Eval("emp_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">องค์กร</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpOrg" runat="server" CssClass="form-control"
                                                Text='<%# Eval("org_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ฝ่าย</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpDept" runat="server" CssClass="form-control"
                                                Text='<%# Eval("dept_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">แผนก</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpSec" runat="server" CssClass="form-control"
                                                Text='<%# Eval("sec_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ตำแหน่ง</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpPos" runat="server" CssClass="form-control"
                                                Text='<%# Eval("pos_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </ItemTemplate>
                </asp:FormView>

                <asp:FormView ID="fvDetailLab" runat="server" Width="100%">
                    <ItemTemplate>
                        <%--<asp:HiddenField ID="hfActorIDX" runat="server" Value='<%# Eval("current_actor") %>' />
                        <asp:HiddenField ID="hfNodeIDX" runat="server" Value='<%# Eval("current_node") %>' />--%>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายละเอียดรายการที่ตรวจ</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Sample Name</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbmaterial_name" runat="server" Visible="true"
                                                CssClass="form-control" Text='<%# Eval("material_name") %>'
                                                Enabled="false" />
                                            <asp:TextBox ID="tbu1_qalab_idx" runat="server" CssClass="form-control"
                                                Visible="false" Text='<%# Eval("u1_qalab_idx") %>' Enabled="false" />
                                            <asp:TextBox ID="tbu0_qalab_idx" runat="server" CssClass="form-control"
                                                Visible="false" Text='<%# Eval("u0_qalab_idx") %>' Enabled="false" />
                                            <asp:TextBox ID="tbm0_labidx" runat="server" CssClass="form-control"
                                                Visible="false" Text='<%# Eval("m0_lab_idx") %>' Enabled="false" />

                                            <asp:TextBox ID="check_m0_lab" runat="server" CssClass="form-control"
                                                Visible="false" Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">Sample Code</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbsample_code" runat="server" CssClass="form-control"
                                                Text='<%# Eval("sample_code") %>' Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">รายการที่สั่งตรวจ</label>
                                        <div class="col-sm-4">
                                            <%--<div class="panel-body">--%>
                                            <table class="table">
                                                <asp:Repeater ID="rp_testdetail_lab" runat="server">

                                                    <ItemTemplate>
                                                        <tr style="border: hidden;">
                                                            <td><%# (Container.ItemIndex + 1) %></td>
                                                            <%-- <td id="emp_idx_report" visible="false" runat="server" data-th="รหัสผู้ถือครอง Google - License"><%# Eval("emp_idx") %>
                                                            </td>--%>
                                                            <td><%# Eval("test_detail_name") %></td>
                                                            <asp:Label ID="u2_labtest_detail_idx" runat="server"
                                                                Text='<%# Eval("test_detail_idx") %>' Visible="false">
                                                            </asp:Label>

                                                            <asp:Label ID="u2_lab_detail" runat="server"
                                                                Text='<%# Eval("u2_qalab_idx") %>' Visible="false">
                                                            </asp:Label>
                                                        </tr>

                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                            </table>
                                            <%-- </div>--%>
                                            <%--<asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" Text='<%# Eval("sec_name_th") %>'
                                            Enabled="false" />--%>
                                        </div>
                                        <label class="col-sm-6 control-label"></label>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-10">
                                            <asp:LinkButton ID="lbDocCancelLab" CssClass="btn btn-default"
                                                runat="server" data-original-title="Back" data-toggle="tooltip"
                                                OnCommand="btnCommand" CommandName="cmdDocCancelLab"><i
                                                    class="fa fa-reply" aria-hidden="true"></i> Back</asp:LinkButton>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:FormView>

                <asp:FormView ID="fvActor3_Receive" runat="server" Width="100%">
                    <InsertItemTemplate>
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title">พิจารณาผล โดย Lab(ภายใน)</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">พิจารณาผล</label>
                                        <div class="col-sm-5">
                                            <asp:DropDownList ID="ddlActor3Approve" runat="server"
                                                CssClass="form-control" ValidationGroup="Create_Actor3" />
                                            <asp:RequiredFieldValidator ID="RequiredddlActor3Approve"
                                                ValidationGroup="Create_Actor3" runat="server" Display="None"
                                                ControlToValidate="ddlActor3Approve" Font-Size="11"
                                                ErrorMessage="เลือกผลการพิจารณา"
                                                ValidationExpression="เลือกผลการพิจารณา" InitialValue="-1" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1"
                                                runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                TargetControlID="RequiredddlActor3Approve" Width="160" />
                                        </div>
                                        <label class="col-sm-5"></label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Comment</label>
                                        <div class="col-sm-5">
                                            <asp:TextBox ID="tbActor3Comment" runat="server" CssClass="form-control"
                                                placeHolder="ความคิดเห็น" />
                                        </div>
                                        <label class="col-sm-5"></label>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-10">
                                            <asp:LinkButton ID="lbDocSaveReceiveN4" CssClass="btn btn-success"
                                                runat="server" data-original-title="Save" data-toggle="tooltip"
                                                Text="Save" OnCommand="btnCommand" CommandName="cmdDocSaveReceiveN4"
                                                ValidationGroup="Create_Actor3"></asp:LinkButton>
                                            <asp:LinkButton ID="lbDocCancelN4" CssClass="btn btn-danger" runat="server"
                                                data-original-title="Cancel" data-toggle="tooltip" Text="Cancel"
                                                OnCommand="btnCommand" CommandName="cmdDocCancelN4"></asp:LinkButton>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </InsertItemTemplate>
                </asp:FormView>

                <asp:FormView ID="fvDetailResult" runat="server" Width="100%">
                    <ItemTemplate>
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title">ผลการวิเคราะห์ตัวอย่าง</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <%--<label class="col-sm-2 control-label">บันทึกผล</label>--%>
                                        <div class="col-sm-12">
                                            <!-- gridview form record test result -->
                                            <asp:GridView ID="GvResultDetail" runat="server" AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered table-hover"
                                                HeaderStyle-CssClass="info" OnRowDataBound="gvRowDataBound"
                                                HeaderStyle-Height="30px" ShowHeaderWhenEmpty="True" ShowFooter="False"
                                                AllowPaging="True" BorderStyle="None" CellSpacing="2">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="แบบฟอร์มบันทึกการตรวจสอบ"
                                                        HeaderStyle-CssClass="text-center">
                                                        <ItemTemplate>
                                                            <div id="Form1" cssclass="col-sm-12" runat="server">
                                                                <div class="form-horizontal" role="form">
                                                                    <asp:Label ID="Lbstaidx_result" runat="server"
                                                                        Visible="false" CssClass="col-sm-12"
                                                                        Text='<%# Eval("staidx") %>'></asp:Label>
                                                                    <asp:Label ID="Lbr1form_create_idx" runat="server"
                                                                        Visible="false" CssClass="col-sm-12"
                                                                        Text='<%# Eval("r1_form_create_idx") %>'>
                                                                    </asp:Label>
                                                                    <%--<asp:Label ID="Lbform_detail_root_idx" runat="server" Visible="false" CssClass="col-sm-12" Text='<%# Eval("form_detail_root_idx") %>'>
                                                                    </asp:Label>--%>
                                                                    <asp:Label ID="Lbu2_qalab_idx" runat="server"
                                                                        Visible="false" CssClass="col-sm-12"
                                                                        Text='<%# Eval("u2_qalab_idx") %>'></asp:Label>
                                                                    <asp:Label ID="Lbu1_qalab_idx" runat="server"
                                                                        Visible="false" CssClass="col-sm-12"
                                                                        Text='<%# Eval("u1_qalab_idx") %>'></asp:Label>
                                                                    <asp:Label ID="Lbu0_qalab_idx" runat="server"
                                                                        Visible="false" CssClass="col-sm-12"
                                                                        Text='<%# Eval("u0_qalab_idx") %>'></asp:Label>
                                                                    <%--     <asp:Label ID="lbtext_detail_name" runat="server" Font-Bold="true" Visible="false" Text='<%# Eval("text_name_setform") %>'
                                                                    CssClass="col-sm-12"></asp:Label>
                                                                    <asp:Label ID="Lboption_idx" runat="server"
                                                                        Visible="false" CssClass="col-sm-12"
                                                                        Text='<%# Eval("option_idx") %>'></asp:Label>
                                                                    --%>
                                                                    <asp:Label ID="lbform_detail_name" runat="server"
                                                                        Font-Bold="false" Visible="true"
                                                                        CssClass="col-sm-8"
                                                                        Text='<%# Eval("r0_form_create_name") %>'>
                                                                    </asp:Label>
                                                                    <div class="col-sm-4">
                                                                        <asp:TextBox ID="txtRecordResultTest"
                                                                            runat="server" Visible="true"
                                                                            CssClass="form-control small"
                                                                            Text='<%# Eval("detail_tested") %>'
                                                                            Enabled="false"></asp:TextBox>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <!-- gridview form record test result level 2 -->
                                                            <%--<asp:GridView ID="GvResult" GridLines="None"
                                                            runat="server"
                                                            ShowHeader="false" AutoGenerateColumns="false"
                                                            CssClass="table table-striped table-bordered"
                                                            HeaderStyle-CssClass="info"
                                                            HeaderStyle-Height="30px"
                                                            ShowHeaderWhenEmpty="True"
                                                            OnRowDataBound="gvRowDataBound"
                                                            ShowFooter="False"
                                                            AllowPaging="True"
                                                            BorderStyle="None"
                                                            CellSpacing="2">
                                                            <Columns>--%>
                                                            <%-- <asp:TemplateField ControlStyle-Font-Size="Small">
                                                                    <ItemTemplate>
                                                                        <div class="form-horizontal" role="form">
                                                                            <asp:Label ID="Lbform_detail_idxlv2" runat="server" Visible="false" CssClass="col-sm-12" Text='<%# Eval("form_detail_idx") %>'>
                                                            </asp:Label>
                                                            <asp:Label ID="Lbu2_qalab_idxlv2" runat="server"
                                                                Visible="false" CssClass="col-sm-12"
                                                                Text='<%# Eval("u2_qalab_idx") %>'></asp:Label>
                                                            <asp:Label ID="Lbr1_form_create_idxlv2" runat="server"
                                                                Visible="false" CssClass="col-sm-12"
                                                                Text='<%# Eval("r1_form_create_idx") %>'></asp:Label>
                                                            <asp:Label ID="Lbform_detail_root_idx" runat="server"
                                                                Visible="false" CssClass="col-sm-12"
                                                                Text='<%# Eval("r1_form_detail_root_idx") %>'>
                                                            </asp:Label>
                                                            <asp:Label ID="Lboption_idxlv2" runat="server"
                                                                Visible="false" CssClass="col-sm-12"
                                                                Text='<%# Eval("option_idx") %>'></asp:Label>

                                                            <asp:Label ID="lbform_detail_name1" runat="server"
                                                                CssClass="col-sm-8">
                                                                <p><i class="fa fa-circle" style="font-size:1%;"
                                                                        aria-hidden="true"></i>
                                                                    <%# Eval("text_name_setform") +" 2 " %></p>
                                                            </asp:Label>
                                                            <div class="col-sm-4">
                                                                <asp:TextBox ID="txtRecordResult" runat="server"
                                                                    Visible="false" CssClass="form-control"
                                                                    placeholder="" Enabled="true"></asp:TextBox>

                                                            </div>
                                        </div>--%>

                                        <%--<asp:GridView ID="GvResultRoot"
                                                                            runat="server" GridLines="None"
                                                                            ShowHeader="false" AutoGenerateColumns="false"
                                                                            CssClass="table table-striped table-bordered table-hover"
                                                                            HeaderStyle-CssClass="info"
                                                                            HeaderStyle-Height="30px"
                                                                            ShowHeaderWhenEmpty="True"
                                                                            ShowFooter="False"
                                                                            AllowPaging="True"
                                                                            BorderStyle="None"
                                                                            CellSpacing="2">
                                                                            <Columns>
                                                                                <asp:TemplateField>
                                                                                    <ItemTemplate>
                                                                                        <div class="form-horizontal" role="form">
                                                                                            <asp:Label ID="lbform_detail_name1" runat="server" Visible="false" CssClass="col-sm-12" Text='<%# Eval("text_name_setform") +" :level 3 " %>'>
                                        </asp:Label>
                                        <asp:Label ID="Lbform_detail_idx" runat="server" Visible="false"
                                            CssClass="col-sm-12" Text='<%# Eval("form_detail_idx") %>'></asp:Label>
                                        <asp:Label ID="Lbr1_form_create_idx2" runat="server" Visible="false"
                                            CssClass="col-sm-12" Text='<%# Eval("r1_form_create_idx") %>'></asp:Label>
                                        <asp:Label ID="Lboption_idxlv3" runat="server" Visible="false"
                                            CssClass="col-sm-12" Text='<%# Eval("option_idx") %>'></asp:Label>

                                        <asp:Label ID="Label2" runat="server" CssClass="col-sm-8">
                                            <p><%# Eval("text_name_setform") +" 3 " %></p>
                                        </asp:Label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txtRecordResultRoot" runat="server" Visible="true"
                                                CssClass="form-control small" placeholder="" Enabled="true">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                    </ItemTemplate>
                    </asp:TemplateField>

                    </Columns>
                    </asp:GridView>--%>
                    <%--</ItemTemplate>
                                                                </asp:TemplateField>--%>

                    <%--                                                            </Columns>
                                                        </asp:GridView>--%>
                    </ItemTemplate>
                    </asp:TemplateField>
                    </Columns>
                    </asp:GridView>
                    <div class="form-group">
                        <asp:Panel ID="divfileView" class="form-group" runat="server" Visible="true">
                            <%--  <asp:Label ID="lbfileupload" runat="server" CSSClass="col-sm-2 control-label">เอกสารอ้างอิง</asp:Label>--%>
                            <div class="col-sm-9">
                                <asp:GridView ID="gvFileResult" Visible="true" runat="server"
                                    OnRowDataBound="gvRowDataBound" AutoGenerateColumns="false" BackColor="White"
                                    BorderColor="White" ShowFooter="false" ShowHeader="false" CssClass="table">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-BorderColor="White" ControlStyle-BackColor="White">
                                            <ItemTemplate>
                                                <div class="col-lg-9">
                                                    <asp:Label ID="ltFileName11" runat="server"
                                                        Text='<%# "ไฟล์บันทึกผลรายการตรวจวิเคราะห์ : " + Eval("FileName") %>' />
                                                    &nbsp;
                                                    <asp:HyperLink runat="server" ID="btnDL11" CssClass="pull-letf"
                                                        data-toggle="tooltip" title="view" data-original-title=""
                                                        Target="_blank"><i class="fa fa-file-text-o"></i> ดูรายละเอียด
                                                    </asp:HyperLink>
                                                    <asp:HiddenField runat="server" ID="hidFile11"
                                                        Value='<%# Eval("Download") %>' />
                                                </div>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </asp:Panel>
                    </div>
            </div>
            </div>
            </div>
            </div>

            </div>
            </ItemTemplate>
            </asp:FormView>

            <asp:Repeater ID="rptHistorySampleInLab" runat="server">
                <HeaderTemplate>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">รายละเอียดการดำเนินการ</h3>
                        </div>
                        <div class="panel-body">
                </HeaderTemplate>
                <ItemTemplate>
                    <%--<asp:HiddenField ID="hfProcessActorIDX" OnItemDataBound="rptItemDataBound" runat="server" Value='<%# Eval("actor_idx") %>'
                    />
                    <asp:HiddenField ID="hfProcessFromNode" runat="server" Value='<%# Eval("from_node") %>' />--%>
                    <div class="form-horizontal">
                        <div class="form-group">
                            <div class="col-sm-2 control-label">
                                <span>ดำเนินการโดย</span>
                            </div>
                            <div class="col-sm-4 control-label textleft">
                                <span><%# Eval("current_artor") %></span>
                            </div>
                            <div class="col-sm-2 control-label">
                                <span>ผลการดำเนินการ</span>
                            </div>
                            <div class="col-sm-4 control-label textleft">
                                <span><%# Eval("current_decision") %></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2 control-label">
                                <span>โดย</span>
                            </div>
                            <div class="col-sm-4 control-label textleft">
                                <span><%# Eval("emp_name_th") %></span>
                            </div>
                            <div class="col-sm-2 control-label">
                                <span>วันที่</span>
                            </div>
                            <div class="col-sm-4 control-label textleft">
                                <span><%# Eval("create_date") %></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2 control-label">
                                <span>comment</span>
                            </div>
                            <div class="col-sm-10 control-label textleft">
                                <asp:Label ID="lblComment" runat="server" Text='<%# Eval("comment") %>' />
                            </div>
                        </div>
                    </div>
                    <hr />
                </ItemTemplate>
                <FooterTemplate>
                    </div>
                    </div>

                </FooterTemplate>
            </asp:Repeater>
            </div>
        </asp:View>
        <!--View Lab-->

        <!--View Lab Result-->
        <asp:View ID="docLabResult" runat="server">
            <div id="divLabResult" runat="server" class="col-md-12">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">กรอกผลการวิเคราะห์ตัวอย่าง</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal" role="form">

                            <asp:FormView ID="fvAnalyticalResults" runat="server" Width="100%">
                                <InsertItemTemplate>
                                    <asp:UpdatePanel ID="updatebtnsaveResult" runat="server">
                                        <ContentTemplate>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Sample Code</label>
                                                <div class="col-sm-4">

                                                    <asp:TextBox ID="txtSearchSampleCode" runat="server" MaxLength="7"
                                                        CssClass="form-control" placeholder="ex.7G07001" Enabled="true">
                                                    </asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredtxtSearchSampleCode"
                                                        runat="server" ValidationGroup="SearchResult" Display="None"
                                                        SetFocusOnError="true" ControlToValidate="txtSearchSampleCode"
                                                        Font-Size="13px" ForeColor="Red"
                                                        ErrorMessage="*กรุณากรอกเลข Sample Code" />
                                                    <ajaxToolkit:ValidatorCalloutExtender
                                                        ID="ValidatortxtSearchSampleCode" runat="Server"
                                                        HighlightCssClass="validatortxtSearchSampleCode"
                                                        TargetControlID="RequiredtxtSearchSampleCode" Width="220" />
                                                </div>

                                                <label class="col-sm-2 control-label">เลือกสถานที่ :</label>
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddlSearchPlaceSaveResult"
                                                        CssClass="form-control" runat="server"></asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="requireddlSearchPlaceSaveResult"
                                                        runat="server" ValidationGroup="SearchResult" Display="None"
                                                        SetFocusOnError="true" InitialValue="0"
                                                        ControlToValidate="ddlSearchPlaceSaveResult" Font-Size="13px"
                                                        ForeColor="Red" ErrorMessage="*กรุณาเลือกสถานที่" />
                                                    <ajaxToolkit:ValidatorCalloutExtender
                                                        ID="toolkitddlSearchPlaceSaveResult" runat="Server"
                                                        PopupPosition="BottomLeft"
                                                        HighlightCssClass="validatorddlSearchPlaceSaveResult"
                                                        TargetControlID="requireddlSearchPlaceSaveResult" Width="220" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label"></label>
                                                <div class="col-sm-10">
                                                    <div class="pull-left">
                                                        <asp:LinkButton ID="btnSearchResult" CssClass="btn btn-primary"
                                                            runat="server" ValidationGroup="SearchResult"
                                                            OnCommand="btnCommand" Font-Size="Small"
                                                            CommandName="btnSearchResult" data-toggle="tooltip"
                                                            title="Search"><span
                                                                class="glyphicon glyphicon-search"></span>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="btnCancelResult" CssClass="btn btn-default"
                                                            runat="server" OnCommand="btnCommand"
                                                            CommandName="cmdDocCancelResult" data-toggle="tooltip"
                                                            Font-Size="Small" title="Cancel"><i class="fa fa-times"></i>
                                                        </asp:LinkButton>
                                                    </div>
                                                    </label>
                                                </div>

                                            </div>
                                            <hr />
                                            <asp:Panel ID="pnRecordResult" runat="server" Visible="false">
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Sample Code</label>
                                                    <div class="col-sm-4">
                                                        <asp:TextBox ID="txtSampleCodeResult" runat="server"
                                                            Text='<%# Eval("sample_code") %>' CssClass="form-control"
                                                            placeholder="" Enabled="false"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">รายการที่สั่งตรวจ</label>
                                                    <div class="col-sm-4">
                                                        <%-- <div class="panel-body">--%>
                                                        <table class="table">
                                                            <asp:Repeater ID="rpt_testdetail_list" runat="server">

                                                                <ItemTemplate>
                                                                    <tr
                                                                        style="background-color: transparent; border: hidden;">

                                                                        <td><%# (Container.ItemIndex + 1) %></td>
                                                                        <%-- <td id="emp_idx_report" visible="false" runat="server" data-th="รหัสผู้ถือครอง Google - License"><%# Eval("emp_idx") %>
                                                                        </td>--%>
                                                                        <td><%# Eval("test_detail_name") %></td>
                                                                        <asp:Label ID="u2_labtest_detailidx_viewresult"
                                                                            runat="server"
                                                                            Text='<%# Eval("test_detail_idx") %>'
                                                                            Visible="false"></asp:Label>

                                                                        <asp:Label ID="u2_lab_detail_viewresult"
                                                                            runat="server"
                                                                            Text='<%# Eval("u2_qalab_idx") %>'
                                                                            Visible="false"></asp:Label>

                                                                    </tr>

                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                </FooterTemplate>
                                                            </asp:Repeater>
                                                        </table>
                                                        <%--  </div>--%>
                                                        <%--<asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" Text='<%# Eval("sec_name_th") %>'
                                                        Enabled="false" />--%>
                                                    </div>
                                                    <label class="col-sm-6 control-label"></label>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">บันทึกผล</label>
                                                    <div class="col-sm-10">
                                                        <!-- gridview form record test result -->
                                                        <asp:GridView ID="GvFormRecordResult" runat="server"
                                                            AutoGenerateColumns="false"
                                                            CssClass="table table-striped table-bordered table-hover"
                                                            HeaderStyle-CssClass="info" OnRowDataBound="gvRowDataBound"
                                                            HeaderStyle-Height="30px" ShowHeaderWhenEmpty="True"
                                                            ShowFooter="False" BorderStyle="None" CellSpacing="2">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="แบบฟอร์มบันทึกการตรวจสอบ"
                                                                    HeaderStyle-CssClass="text-center">
                                                                    <ItemTemplate>
                                                                        <div id="Form1" cssclass="col-sm-12"
                                                                            runat="server">
                                                                            <div class="form-horizontal" role="form">
                                                                                <asp:Label ID="lbl_lab_type_idx"
                                                                                    runat="server" Visible="false"
                                                                                    CssClass="col-sm-12"
                                                                                    Text='<%# Eval("lab_type_idx") %>'>
                                                                                </asp:Label>
                                                                                <asp:Label ID="Lbstaidx_result"
                                                                                    runat="server" Visible="false"
                                                                                    CssClass="col-sm-12"
                                                                                    Text='<%# Eval("staidx") %>'>
                                                                                </asp:Label>
                                                                                <asp:Label ID="Lbr1form_create_idx"
                                                                                    runat="server" Visible="false"
                                                                                    CssClass="col-sm-12"
                                                                                    Text='<%# Eval("r1_form_create_idx") %>'>
                                                                                </asp:Label>
                                                                                <asp:Label ID="Lbform_detail_root_idx"
                                                                                    runat="server" Visible="false"
                                                                                    CssClass="col-sm-12"
                                                                                    Text='<%# Eval("form_detail_root_idx") %>'>
                                                                                </asp:Label>
                                                                                <asp:Label ID="Lbu2_qalab_idx"
                                                                                    runat="server" Visible="false"
                                                                                    CssClass="col-sm-12"
                                                                                    Text='<%# Eval("u2_qalab_idx") %>'>
                                                                                </asp:Label>
                                                                                <asp:Label ID="Lbu1_qalab_idx"
                                                                                    runat="server" Visible="false"
                                                                                    CssClass="col-sm-12"
                                                                                    Text='<%# Eval("u1_qalab_idx") %>'>
                                                                                </asp:Label>
                                                                                <asp:Label ID="Lbu0_qalab_idx"
                                                                                    runat="server" Visible="false"
                                                                                    CssClass="col-sm-12"
                                                                                    Text='<%# Eval("u0_qalab_idx") %>'>
                                                                                </asp:Label>
                                                                                <asp:Label ID="lbtext_detail_name"
                                                                                    runat="server" Font-Bold="true"
                                                                                    Visible="false"
                                                                                    Text='<%# Eval("text_name_setform") %>'
                                                                                    CssClass="col-sm-12"></asp:Label>
                                                                                <asp:Label ID="Lboption_idx"
                                                                                    runat="server" Visible="false"
                                                                                    CssClass="col-sm-12"
                                                                                    Text='<%# Eval("option_idx") %>'>
                                                                                </asp:Label>
                                                                                <asp:Label ID="lbform_detail_name"
                                                                                    runat="server" Font-Bold="false"
                                                                                    Visible="true" CssClass="col-sm-8"
                                                                                    Text='<%# Eval("form_detail_name") %>'>
                                                                                </asp:Label>
                                                                                <div class="col-sm-4">
                                                                                    <asp:TextBox
                                                                                        ID="txtRecordResultTest"
                                                                                        runat="server" Visible="false"
                                                                                        CssClass="form-control small"
                                                                                        placeholder="" Enabled="true">
                                                                                    </asp:TextBox>
                                                                                    <asp:RequiredFieldValidator
                                                                                        ID="RequiredtxtRecordResultTest"
                                                                                        runat="server"
                                                                                        ControlToValidate="txtRecordResultTest"
                                                                                        Display="None"
                                                                                        SetFocusOnError="true"
                                                                                        ErrorMessage="*กรุณากรอกผลแลปปฏิบัติการ"
                                                                                        ValidationGroup="saveDocResult" />
                                                                                    <ajaxToolkit:ValidatorCalloutExtender
                                                                                        ID="ValtxtRecordResultTest"
                                                                                        runat="Server"
                                                                                        PopupPosition="BottomRight"
                                                                                        HighlightCssClass="validatorCalloutHighlight"
                                                                                        TargetControlID="RequiredtxtRecordResultTest"
                                                                                        Width="250" />

                                                                                    <asp:DropDownList
                                                                                        ID="ddlChooseResult"
                                                                                        Visible="false" runat="server"
                                                                                        CssClass="form-control"
                                                                                        placeholder="" Enabled="true">
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                                <%--   <div class="col-sm-4">
                                                                        <asp:Label ID="lbRecordResult" runat="server" Visible="false" 
                                                                            CssClass="lable-control" placeholder="" Enabled="true"></asp:Label>
                                                                    </div>--%>
                                                                            </div>
                                                                        </div>
                                                                        <!-- gridview form record test result level 2 -->
                                                                        <asp:GridView ID="GvRecordResult"
                                                                            GridLines="None" runat="server"
                                                                            ShowHeader="false"
                                                                            AutoGenerateColumns="false"
                                                                            CssClass="table table-striped table-bordered"
                                                                            HeaderStyle-CssClass="info"
                                                                            HeaderStyle-Height="30px"
                                                                            ShowHeaderWhenEmpty="True"
                                                                            OnRowDataBound="gvRowDataBound"
                                                                            ShowFooter="False" BorderStyle="None"
                                                                            CellSpacing="2">
                                                                            <Columns>
                                                                                <asp:TemplateField
                                                                                    ControlStyle-Font-Size="Small">
                                                                                    <ItemTemplate>
                                                                                        <div class="form-horizontal"
                                                                                            role="form">
                                                                                            <asp:Label
                                                                                                ID="Lbform_detail_idxlv2"
                                                                                                runat="server"
                                                                                                Visible="false"
                                                                                                CssClass="col-sm-12"
                                                                                                Text='<%# Eval("form_detail_idx") %>'>
                                                                                            </asp:Label>
                                                                                            <asp:Label
                                                                                                ID="Lbu2_qalab_idxlv2"
                                                                                                runat="server"
                                                                                                Visible="false"
                                                                                                CssClass="col-sm-12"
                                                                                                Text='<%# Eval("u2_qalab_idx") %>'>
                                                                                            </asp:Label>
                                                                                            <asp:Label
                                                                                                ID="Lbr1_form_create_idxlv2"
                                                                                                runat="server"
                                                                                                Visible="false"
                                                                                                CssClass="col-sm-12"
                                                                                                Text='<%# Eval("r1_form_create_idx") %>'>
                                                                                            </asp:Label>
                                                                                            <asp:Label
                                                                                                ID="Lbform_detail_root_idx"
                                                                                                runat="server"
                                                                                                Visible="false"
                                                                                                CssClass="col-sm-12"
                                                                                                Text='<%# Eval("r1_form_detail_root_idx") %>'>
                                                                                            </asp:Label>
                                                                                            <asp:Label
                                                                                                ID="Lboption_idxlv2"
                                                                                                runat="server"
                                                                                                Visible="false"
                                                                                                CssClass="col-sm-12"
                                                                                                Text='<%# Eval("option_idx") %>'>
                                                                                            </asp:Label>

                                                                                            <asp:Label
                                                                                                ID="lbform_detail_name1"
                                                                                                runat="server"
                                                                                                CssClass="col-sm-8">
                                                                                                <p><i class="fa fa-circle"
                                                                                                        style="font-size:1%;"
                                                                                                        aria-hidden="true"></i>
                                                                                                    <%# Eval("text_name_setform") +" 2 " %>
                                                                                                </p>
                                                                                            </asp:Label>
                                                                                            <div class="col-sm-4">
                                                                                                <asp:TextBox
                                                                                                    ID="txtRecordResult"
                                                                                                    runat="server"
                                                                                                    Visible="false"
                                                                                                    CssClass="form-control"
                                                                                                    placeholder=""
                                                                                                    Enabled="true">
                                                                                                </asp:TextBox>

                                                                                            </div>
                                                                                        </div>

                                                                                        <asp:GridView
                                                                                            ID="GvRecordResultRoot"
                                                                                            runat="server"
                                                                                            GridLines="None"
                                                                                            ShowHeader="false"
                                                                                            AutoGenerateColumns="false"
                                                                                            CssClass="table table-striped table-bordered table-hover"
                                                                                            HeaderStyle-CssClass="info"
                                                                                            HeaderStyle-Height="30px"
                                                                                            ShowHeaderWhenEmpty="True"
                                                                                            ShowFooter="False"
                                                                                            BorderStyle="None"
                                                                                            CellSpacing="2">
                                                                                            <Columns>
                                                                                                <asp:TemplateField>
                                                                                                    <ItemTemplate>
                                                                                                        <div class="form-horizontal"
                                                                                                            role="form">
                                                                                                            <asp:Label
                                                                                                                ID="lbform_detail_name1"
                                                                                                                runat="server"
                                                                                                                Visible="false"
                                                                                                                CssClass="col-sm-12"
                                                                                                                Text='<%# Eval("text_name_setform") +" :level 3 " %>'>
                                                                                                            </asp:Label>
                                                                                                            <asp:Label
                                                                                                                ID="Lbform_detail_idx"
                                                                                                                runat="server"
                                                                                                                Visible="false"
                                                                                                                CssClass="col-sm-12"
                                                                                                                Text='<%# Eval("form_detail_idx") %>'>
                                                                                                            </asp:Label>
                                                                                                            <asp:Label
                                                                                                                ID="Lbr1_form_create_idx2"
                                                                                                                runat="server"
                                                                                                                Visible="false"
                                                                                                                CssClass="col-sm-12"
                                                                                                                Text='<%# Eval("r1_form_create_idx") %>'>
                                                                                                            </asp:Label>
                                                                                                            <asp:Label
                                                                                                                ID="Lboption_idxlv3"
                                                                                                                runat="server"
                                                                                                                Visible="false"
                                                                                                                CssClass="col-sm-12"
                                                                                                                Text='<%# Eval("option_idx") %>'>
                                                                                                            </asp:Label>

                                                                                                            <asp:Label
                                                                                                                ID="Label2"
                                                                                                                runat="server"
                                                                                                                CssClass="col-sm-8">
                                                                                                                <p><%--<i class="fa fa-circle" style="font-size:1%;" aria-hidden="true"></i>--%>
                                                                                                                    -
                                                                                                                    <%# Eval("text_name_setform") +" 3 " %>
                                                                                                                </p>
                                                                                                            </asp:Label>
                                                                                                            <div
                                                                                                                class="col-sm-4">
                                                                                                                <asp:TextBox
                                                                                                                    ID="txtRecordResultRoot"
                                                                                                                    runat="server"
                                                                                                                    Visible="true"
                                                                                                                    CssClass="form-control small"
                                                                                                                    placeholder=""
                                                                                                                    Enabled="true">
                                                                                                                </asp:TextBox>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>

                                                                                            </Columns>
                                                                                        </asp:GridView>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                            </Columns>
                                                                        </asp:GridView>

                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                                <asp:Panel ID="pnUploadfileResult" runat="server" Visible="true">
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">แนบไฟล์</label>
                                                        <div class="col-sm-10">
                                                            <asp:FileUpload ID="UploadFileResult" Font-Size="small"
                                                                ViewStateMode="Enabled" runat="server"
                                                                CssClass="control-label" accept="jpg|pdf|png" />
                                                            <p class="help-block">
                                                                <font color="red">**แนบเอกสารเพิ่มเติม เฉพาะนามสกุล
                                                                    jpg|pdf|png</font>
                                                            </p>
                                                            <asp:RequiredFieldValidator ID="requiredUploadFileResult"
                                                                runat="server" ValidationGroup="saveDocResult"
                                                                Display="None" SetFocusOnError="true"
                                                                ControlToValidate="UploadFileResult" Font-Size="13px"
                                                                ForeColor="Red" ErrorMessage="*กรุณาเลือกไฟล์" />
                                                            <ajaxToolkit:ValidatorCalloutExtender
                                                                ID="toolkitRequiredUploadFileResult" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight"
                                                                TargetControlID="requiredUploadFileResult"
                                                                Width="220" />

                                                            <asp:RegularExpressionValidator
                                                                ID="RegularExpreUploadFileResult" runat="server"
                                                                ErrorMessage="*กรุณาเลือกเฉพาะไฟล์ jpg, png หรือ pdf"
                                                                SetFocusOnError="true" Display="None"
                                                                ControlToValidate="UploadFileResult"
                                                                ValidationExpression="[a-zA-Z\\].*(.jpg|.JPG|.pdf|.PDF|.png|.PNG)$"
                                                                ValidationGroup="saveDocResult" />
                                                            <ajaxToolkit:ValidatorCalloutExtender
                                                                ID="ValidatorUploadFileResult" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight"
                                                                TargetControlID="RegularExpreUploadFileResult"
                                                                Width="220" />
                                                        </div>
                                                    </div>
                                                </asp:Panel>

                                                <div class="form-group pull-right">
                                                    <div class="col-sm-12">

                                                        <asp:LinkButton ID="lbDocSaveReceiveN5"
                                                            CssClass="btn btn-success" runat="server"
                                                            ValidationGroup="saveDocResult" data-original-title="Save"
                                                            data-toggle="tooltip" Text="Save" OnCommand="btnCommand"
                                                            CommandName="cmdSaveNode5" CommandArgument="7,17">
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="lbDocCancelN4" CssClass="btn btn-danger"
                                                            runat="server" data-original-title="Cancel"
                                                            data-toggle="tooltip" Text="Cancel" OnCommand="btnCommand"
                                                            CommandName="cmdDocCancelResult"></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </asp:Panel>

                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="lbDocSaveReceiveN5" />
                                            <asp:PostBackTrigger ControlID="btnSearchResult" />

                                        </Triggers>
                                    </asp:UpdatePanel>
                                </InsertItemTemplate>
                            </asp:FormView>

                        </div>
                    </div>
                </div>
            </div>

        </asp:View>
        <!--View Lab Result-->

        <!--View Lab Supervisor-->
        <asp:View ID="docSupervisor" runat="server">
            <div id="divSupervisor" runat="server" class="col-md-12">
                <asp:PlaceHolder ID="myPlaceHolder" runat="server" />


                <div class="form-group">
                    <asp:Repeater ID="rpt_gvsupervisor" OnItemDataBound="rptOnRowDataBound" runat="server">
                        <ItemTemplate>
                            <asp:Label ID="lbcheck_colersup" runat="server" Visible="false"
                                Text='<%# Eval("m0_lab_idx") %>'></asp:Label>
                            <asp:LinkButton ID="btnPlaceLabSup" CssClass="btn btn-primary" runat="server"
                                data-original-title='<%# Eval("lab_name") %>' data-toggle="tooltip"
                                CommandArgument='<%# Eval("m0_lab_idx")+ ";" + Eval("lab_name") %>'
                                OnCommand="btnCommand" Text='<%# Eval("lab_name") %>' CommandName="cmdPlaceLabSup">
                            </asp:LinkButton>

                            <%-- <asp:LinkButton ID="btnDocumentPlaceLab" CssClass="btn btn-primary" runat="server" data-original-title='<%# Eval("place_name") %>'
                            data-toggle="tooltip" CommandArgument='<%# Eval("place_idx")+ ";" + Eval("place_name") %>'
                            OnCommand="btnCommand" Text='<%# Eval("place_name") %>' CommandName="cmdDocumentPlaceLab">
                            </asp:LinkButton>--%>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>

                <div class="form-group">
                    <h4>
                        <asp:Label ID="lbl_showlabsup" Font-Bold="true" runat="server" Visible="false">
                        </asp:Label>
                    </h4>

                </div>

                <div class="alert alert-warning" id="EmptyData" runat="server" visible="false">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>ไม่พบข้อมูล!! </strong>
                </div>

                <asp:UpdatePanel ID="UpdatePanelSup" runat="server">
                    <ContentTemplate>

                        <div id="GvSupervisor_scroll" style="overflow-x: scroll; width: 100%" visible="false"
                            runat="server">

                            <asp:GridView ID="GvSupervisor" runat="server" HeaderStyle-CssClass="info"
                                AllowPaging="false" AllowSorting="True" BackColor="White" BorderColor="#DEDFDE "
                                BorderStyle="None" BorderWidth="1px" CssClass="table table-striped table-bordered"
                                Width="728px" CellPadding="4" ForeColor="Black" GridLines="Vertical"
                                OnRowDataBound="gvRowDataBound">
                                <%--<PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />--%>
                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>
                                <%--<HeaderStyle CssClass="GridviewScrollHeader" /> 
                            <RowStyle CssClass="GridviewScrollItem" /> 
                            <PagerStyle CssClass="GridviewScrollPager" />--%>
                                <%--   <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>

                             <asp:LinkButton ID="lbDocSaveReceiveN4" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="cmdSaveNode5" CommandArgument="7,17" ValidationGroup="Save_ResultLab"></asp:LinkButton>--%>
                                <%--              <asp:GridView ID="GridView1" runat="server" HeaderStyle-CssClass="info" AllowPaging="True" AllowSorting="True"
                BackColor="White" BorderColor="#DEDFDE " BorderStyle="None" BorderWidth="1px" CssClass="table table-striped table-bordered"
                Width="728px" CellPadding="4" ForeColor="Black" GridLines="Vertical"
                PageSize="14" OnRowDataBound="gvRowDataBound">
                          </asp:GridView>--%>
                                <%--  </ItemTemplate>
                    </asp:TemplateField>
                     
                </Columns>--%>
                            </asp:GridView>
                        </div>


                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </asp:View>
        <!--View Lab Supervisor-->

        <!--View  Print-->
        <asp:View ID="docPrint" runat="server">
            <div id="divDocumentPrint" runat="server" class="col-md-12">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">ค้นหารายการตรวจที่ต้องการปริ้น</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal" role="form">

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Recive Date :</label>
                                <div class="col-sm-4">
                                    <div class='input-group date from-date-datepicker'>
                                        <asp:HiddenField ID="HiddenSearchDateRecive" runat="server" />
                                        <asp:TextBox ID="tbSearchDateRecive" placeholder="Recive Date ..."
                                            AutoPostBack="true" runat="server" CssClass="form-control" ReadOnly="false">
                                        </asp:TextBox><span class="input-group-addon"><span data-icon-element=""
                                                class="glyphicon glyphicon-calendar"></span></span>
                                        <asp:RequiredFieldValidator ID="RequiredtbSearchDateRecive" runat="server"
                                            ValidationGroup="searchprint" Display="None" SetFocusOnError="true"
                                            ControlToValidate="tbSearchDateRecive" Font-Size="13px" ForeColor="Red"
                                            ErrorMessage="*กรุณาเลือกวันที่รับตัวอย่าง" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2"
                                            runat="Server" HighlightCssClass="validatortbSearchDateRecive"
                                            TargetControlID="RequiredtbSearchDateRecive" Width="220" />
                                    </div>
                                </div>
                                <label class="col-sm-2 control-label">เลือกสถานที่ :</label>
                                <div class="col-sm-4">
                                    <asp:DropDownList ID="ddlSearchPlace" CssClass="form-control" runat="server">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="requiredddlSearchPlace" runat="server"
                                        ValidationGroup="searchprint" Display="None" SetFocusOnError="true"
                                        InitialValue="0" ControlToValidate="ddlSearchPlace" Font-Size="13px"
                                        ForeColor="Red" ErrorMessage="*กรุณาเลือกสถานที่" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="toolkitRequiredUploadFileResult1"
                                        runat="Server" PopupPosition="BottomLeft"
                                        HighlightCssClass="validatorddlSearchPlace"
                                        TargetControlID="requiredddlSearchPlace" Width="220" />
                                </div>
                                <%--<div class="col-sm-2">
                                    <div class="pull-left">
                                        <asp:LinkButton ID="btnSearchDatePrint" CssClass="btn btn-primary" runat="server"
                                            OnCommand="btnCommand" CommandName="cmdSearchDatePrint" Font-Size="Small" data-toggle="tooltip" title="Search"><span class="glyphicon glyphicon-search"></span></asp:LinkButton>
                                        <asp:LinkButton ID="btnSearchDateReset" CssClass="btn btn-default" runat="server"
                                            OnCommand="btnCommand" CommandName="cmdSearchDateReset" Font-Size="Small" data-toggle="tooltip" title="Reset"><span class="glyphicon glyphicon-refresh"></span></asp:LinkButton>
                                    </div>
                                </div>--%>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label"></label>
                                <div class="col-sm-4">
                                    <asp:LinkButton ID="btnSearchDatePrint" CssClass="btn btn-primary" runat="server"
                                        ValidationGroup="searchprint" OnCommand="btnCommand"
                                        CommandName="cmdSearchDatePrint" Font-Size="Small" data-toggle="tooltip"
                                        title="Search"><span class="glyphicon glyphicon-search"></span></asp:LinkButton>
                                    <asp:LinkButton ID="btnSearchDateReset" CssClass="btn btn-default" runat="server"
                                        OnCommand="btnCommand" CommandName="cmdSearchDateReset" Font-Size="Small"
                                        data-toggle="tooltip" title="Reset"><span
                                            class="glyphicon glyphicon-refresh"></span></asp:LinkButton>
                                </div>
                                <label class="col-sm-6 control-label"></label>

                            </div>


                            <hr />
                            <div class="alert alert-warning" id="divAlertSearchnoneDate" runat="server" visible="false">
                                <button type="button" class="close" data-dismiss="alert">x</button>
                                <strong>ไม่มีพบรายการที่ค้นหา ! </strong>
                            </div>

                        </div>
                    </div>
                </div>

                <div id="divShowToolPrint" runat="server" visible="false">
                    <div class="form-group">
                        <asp:UpdatePanel ID="updatepanelbuttonSearch" runat="server">
                            <ContentTemplate>
                                <%--  <p style="font-size: small;"><b>คุณต้องการปริ้น QR Code หรือไม่ <i class="fa fa-question-circle" aria-hidden="true"></i></b></p>    target="_blank"--%>
                                <asp:LinkButton CssClass="btn btn-success" ID="btnExportSearchDate"
                                    data-toggle="tooltip" title="Export Excel" runat="server"
                                    CommandName="CmdExportSearchDate" Visible="true" OnCommand="btnCommand"><i
                                        class="glyphicon glyphicon-list-alt"></i> Export</asp:LinkButton>
                                <asp:LinkButton CssClass="btn btn-default tar" ID="btnprintSampleCodeSearchDate"
                                    data-toggle="tooltip" title="Print" runat="server"
                                    CommandName="printSampleCodeSearchDate" Visible="true" OnCommand="btnCommand"
                                    target=""><i class="glyphicon glyphicon-print"></i> Print</asp:LinkButton>

                                <asp:RadioButton ID="rdbuttonNeedPrint1" runat="server" Font-Size="9" Visible="false"
                                    GroupName="radioButtonListPrint" Checked="true" Text="ต้องการ QR Code" />
                                <asp:RadioButton ID="rdbuttonNotNeedPrint1" runat="server" Font-Size="9" Visible="false"
                                    GroupName="radioButtonListPrint" Text="ไม่ต้องการ QR Code" />


                                <asp:CheckBox ID="ChkPrintQrCodeSearch" runat="server" Checked="true"
                                    Text="ต้องการ QR Code" />
                                <asp:CheckBox ID="ChkPrintTestedItemsSearch" Checked="true" runat="server"
                                    Text="ต้องการ Tested Items" />


                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnprintSampleCodeSearchDate" />
                                <asp:PostBackTrigger ControlID="btnExportSearchDate" />
                            </Triggers>

                        </asp:UpdatePanel>
                    </div>
                    <asp:GridView ID="gvExportSearchDate" runat="server" HeaderStyle-CssClass="info" AllowPaging="True"
                        AllowSorting="True" BackColor="White" BorderColor="#DEDFDE " BorderStyle="None"
                        BorderWidth="1px" CssClass="table table-striped table-bordered" Width="728px" CellPadding="4"
                        ForeColor="Black" GridLines="Vertical" PageSize="14" OnRowDataBound="gvRowDataBound">
                        <Columns>
                        </Columns>
                    </asp:GridView>
                    <div class="form-group">
                        <asp:GridView ID="gvSampleCodeSearchPrint" runat="server" AutoGenerateColumns="false"
                            CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                            AllowPaging="false" PageSize="10" OnPageIndexChanging="gvPageIndexChanging"
                            OnRowDataBound="gvRowDataBound">
                            <RowStyle Font-Size="Small" />
                            <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                            <EmptyDataTemplate>
                                <div style="text-align: center">Data Cannot Be Found</div>
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="Sample Code" HeaderStyle-Width="10%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblu0IDX_Sample_search_print" runat="server" Visible="false"
                                            Text='<%# Eval("u0_qalab_idx") %>'></asp:Label>
                                        <asp:Label ID="lblDocIDX_Sample_search_print" runat="server" Visible="false"
                                            Text='<%# Eval("u1_qalab_idx") %>'></asp:Label>
                                        <asp:Label ID="lblSampleCode_search_print" runat="server"
                                            Text='<%# Eval("sample_code") %>'></asp:Label>
                                        <asp:Label ID="lblCount_All_search_print" runat="server" Visible="false"
                                            Text='<%# Eval("Count_All") %>'></asp:Label>
                                        <asp:Label ID="lblCount_Success_search_print" runat="server" Visible="false"
                                            Text='<%# Eval("Count_Success") %>'></asp:Label>
                                        <asp:Label ID="lblCount_Place_search_print" runat="server" Visible="false"
                                            Text='<%# Eval("Count_Place") %>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Tested Detail" HeaderStyle-Width="25%">
                                    <ItemTemplate>
                                        <table class="table table-striped f-s-12"
                                            style="background-color: transparent;">
                                            <asp:Repeater ID="rp_test_sample_search_print" runat="server">

                                                <ItemTemplate>
                                                    <tr style="background-color: transparent; border: hidden;">
                                                        <td style="background-color: transparent;">
                                                            <asp:Label ID="lbltestdetailname_search_print"
                                                                runat="server"
                                                                Text='<%# " - " + Eval("test_detail_name") %>'>
                                                            </asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr id="u2_idx_n3_edit" runat="server" visible="false">
                                                        <td>
                                                            <asp:Label ID="lblu2qalab_idx_search_print" runat="server"
                                                                Text='<%# Eval("u2_qalab_idx") %>'></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr id="test_detail_idx_n11_edit" runat="server" visible="false">
                                                        <td>
                                                            <asp:Label ID="lbltest_detail_idx_n11_search_print"
                                                                runat="server" Text='<%# Eval("test_detail_idx") %>'>
                                                            </asp:Label>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </table>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Received Date" HeaderStyle-Width="10%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblreceived_sample_date_search_print" runat="server"
                                            Text='<%# Eval("received_sample_date") %>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Tested Date" HeaderStyle-Width="10%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTest_date_search_print" runat="server"
                                            Text='<%# Eval("test_date") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Lab" HeaderStyle-Width="15%">
                                    <ItemTemplate>

                                        <asp:Label ID="lblm0_lab_idx_admin_search_print" runat="server" Visible="false"
                                            Text='<%# Eval("m0_lab_idx") %>'></asp:Label>
                                        <asp:Label ID="lbllab_nameadmin_search_print" runat="server"
                                            Text='<%# Eval("lab_name") %>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="	Status" HeaderStyle-Width="20%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblStaidx_search_print" runat="server" Visible="false"
                                            Text='<%# Eval("staidx") %>'></asp:Label>
                                        <asp:Label ID="lbstatus_name_search_print" runat="server"
                                            Text='<%# Eval("status_name") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Action" Visible="false" HeaderStyle-Width="10%">
                                    <ItemTemplate>
                                        <%--    <asp:LinkButton ID="btnViewSamplelist_search_print" CssClass="btn-sm btn-warning" runat="server" CommandName="cmdUserEditDocument" OnCommand="btnCommand" CommandArgument='<%# Eval("u0_qalab_idx")+ ";" + Eval("u1_qalab_idx")+ ";" + Eval("staidx") %>'
                                        data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                        --%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>

                        </asp:GridView>

                    </div>



                </div>
            </div>
        </asp:View>
        <!--View Print-->
    </asp:MultiView>
    <!--multiview-->

    <!--time picker-->
    <%--autoclose: true,  format: 'HH:mm',--%>
    <script type="text/javascript">
        //is
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {

            $('.loading-icon-approve').hide();

            $('.clockpicker').datetimepicker({
                format: 'HH:mm',
                ignoreReadonly: true
            });
        });
        //no post back
        $('.clockpicker').datetimepicker({
            format: 'HH:mm',
            ignoreReadonly: true
        });

        function getAlertSuccess() {
            $("#success_alert").show();
            $("#success_alert").fadeTo(500, 500).slideUp(500, function () {
                $("#success_alert").slideUp(500);
            });
        }
        function ValidatePage(toHide, toShow) {

            $(toHide).hide();
            $(toShow).show();

        }
        function getSupervisorApprove(samplecode, empidx, decision, place) {

            var answerConfirm = confirm("คุณต้องการดำเนินรายการนี้ต่อหรือไม่?")
            // var answerConfirm = confirm(place)
            if (answerConfirm) {

                ValidatePage('.loading-icon-button-approve', '.loading-icon-approve');

                $.ajax({
                    method: "POST",
                    url: 'http://services.taokaenoi.co.th/webapi/api_qa_lab.asmx/getSupervisorApprove',
                    dataType: 'json',
                    data: {
                        id: samplecode,
                        emp: empidx,
                        dec: decision,
                        place: place

                    },
                    success: function (data) {
                        if (data.data_qa.return_code == 0) {
                            console.log(data);
                            // getAlertSuccess();
                        }
                        __doPostBack('ctl00$ContentMain$lbSupervisor', '');
                    }
                });
            }
            else {
                return false;
            }
        }

    </script>

    <script>
        $(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });
        });


        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY',
                    ignoreReadonly: true
                });
            });
        });

    </script>

    <script>
        $(document).ready(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });
        })
    </script>

    <script>
        $(function () {

            $('.from-date-datepickerexpire').datetimepicker({
                format: 'DD/MM/YYYY',
                minDate: new Date()

            });
        });


        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepickerexpire').datetimepicker({
                    format: 'DD/MM/YYYY',
                    minDate: new Date()

                });
            });
        });

    </script>

    <script>
        $(document).ready(function () {
            $('.from-date-datepickerexpire').datetimepicker({
                format: 'DD/MM/YYYY',
                minDate: new Date()

            });
        })
    </script>



</asp:Content>