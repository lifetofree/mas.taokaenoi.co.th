﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_hc_doctor_history : System.Web.UI.Page
{
    #region Connect
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();
    data_hr_healthcheck _data_hr_healthcheck = new data_hr_healthcheck();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    //-- employee --//

    // master doctor //
    static string _urlGetProvinceDoctor = _serviceUrl + ConfigurationManager.AppSettings["urlGetProvinceDoctor"];
    static string _urlGetAmphurDoctor = _serviceUrl + ConfigurationManager.AppSettings["urlGetAmphurDoctor"];
    static string _urlGetDistrictDoctor = _serviceUrl + ConfigurationManager.AppSettings["urlGetDistrictDoctor"];
    static string _urlSetDoctorHistory = _serviceUrl + ConfigurationManager.AppSettings["urlSetDoctorHistory"];
    static string _urlGetDetailDoctor = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailDoctor"];
    static string _urlSetDelDoctor = _serviceUrl + ConfigurationManager.AppSettings["urlSetDelDoctor"];

    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    bool _flag_qmr = false;
    #endregion Connect

    #region Page_Init
    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        ////getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
        //check permission
        //foreach (int item in rdept_qmr)
        //{
        //    if (_dataEmployee.employee_list[0].rdept_idx == item)
        //    {
        //        _flag_qmr = true;
        //        break;
        //    }
        //}
    }
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Select_DetailDoctor();
            
        }
    }
    #endregion Page Load 

    protected void Select_DetailDoctor()
    {
        data_hr_healthcheck data_m0_doctor_detail = new data_hr_healthcheck();
        hr_healthcheck_m0_doctor_detail m0_doctor_detail = new hr_healthcheck_m0_doctor_detail();

        data_m0_doctor_detail.healthcheck_m0_doctor_list = new hr_healthcheck_m0_doctor_detail[1];

        data_m0_doctor_detail.healthcheck_m0_doctor_list[0] = m0_doctor_detail;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));
        data_m0_doctor_detail = callServicePostMasterHealthCheck(_urlGetDetailDoctor, data_m0_doctor_detail);

        setGridData(gvDoctor, data_m0_doctor_detail.healthcheck_m0_doctor_list);

    }

    protected void actionReadDetailDoctor(int m0_doctor_idx)
    {

        data_hr_healthcheck data_m0_doctor_update = new data_hr_healthcheck();
        hr_healthcheck_m0_doctor_detail m0_doctor_detail_update = new hr_healthcheck_m0_doctor_detail();

        data_m0_doctor_update.healthcheck_m0_doctor_list = new hr_healthcheck_m0_doctor_detail[1];
        m0_doctor_detail_update.m0_doctor_idx = m0_doctor_idx;

        data_m0_doctor_update.healthcheck_m0_doctor_list[0] = m0_doctor_detail_update;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));
        data_m0_doctor_update = callServicePostMasterHealthCheck(_urlGetDetailDoctor, data_m0_doctor_update);

        setFormData(fvInsertDataDoctor, FormViewMode.Edit, data_m0_doctor_update.healthcheck_m0_doctor_list);

    }

    protected void getSelect_Province(DropDownList ddlName, int _province_id)
    {
        data_hr_healthcheck _data_m0_province = new data_hr_healthcheck();
        hr_healthcheck_m0_province_detail m0_province = new hr_healthcheck_m0_province_detail();

        _data_m0_province.healthcheck_m0_province_list = new hr_healthcheck_m0_province_detail[1];

        _data_m0_province.healthcheck_m0_province_list[0] = m0_province;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));
        _data_m0_province = callServicePostMasterHealthCheck(_urlGetProvinceDoctor, _data_m0_province);
        
        setDdlData(ddlName, _data_m0_province.healthcheck_m0_province_list, "ProvName", "ProvIDX");
        ddlName.Items.Insert(0, new ListItem("--- เลือกจังหวัด ---", "0"));
        ddlName.SelectedValue = _province_id.ToString();
        //setGridData(GvResolution, data_m0_resulution.qa_cims_m0_resolution_list);

    }

    protected void getSelect_Amphoe(DropDownList ddlName, int _province_idx, int _amphur_id)
    {
        data_hr_healthcheck _data_m0_amphoe = new data_hr_healthcheck();
        hr_healthcheck_m0_amphur_detail _m0_amphoe = new hr_healthcheck_m0_amphur_detail();
        _m0_amphoe.ProvIDX = _province_idx;

        _data_m0_amphoe.healthcheck_m0_amphur_list = new hr_healthcheck_m0_amphur_detail[1];

        _data_m0_amphoe.healthcheck_m0_amphur_list[0] = _m0_amphoe;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));
        _data_m0_amphoe = callServicePostMasterHealthCheck(_urlGetAmphurDoctor, _data_m0_amphoe);

        setDdlData(ddlName, _data_m0_amphoe.healthcheck_m0_amphur_list, "AmpName", "AmpIDX");
        ddlName.Items.Insert(0, new ListItem("--- เลือกอำเภอ ---", "0"));
        ddlName.SelectedValue = _amphur_id.ToString();
        //setGridData(GvResolution, data_m0_resulution.qa_cims_m0_resolution_list);

    }
    //ddl_District_insert

    protected void getSelect_district(DropDownList ddlName, int _amphur, int district_id)
    {
        data_hr_healthcheck _data_m0_district = new data_hr_healthcheck();
        hr_healthcheck_m0_district_detail _m0_district = new hr_healthcheck_m0_district_detail();
        //_m0_amphoe.ProvIDX = _province_idx;
        _m0_district.AmpIDX = _amphur;

        _data_m0_district.healthcheck_m0_district_list = new hr_healthcheck_m0_district_detail[1];

        _data_m0_district.healthcheck_m0_district_list[0] = _m0_district;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));
        _data_m0_district = callServicePostMasterHealthCheck(_urlGetDistrictDoctor, _data_m0_district);

        setDdlData(ddlName, _data_m0_district.healthcheck_m0_district_list, "DistName", "DistIDX");
        ddlName.Items.Insert(0, new ListItem("--- เลือกตำบล ---", "0"));
        ddlName.SelectedValue = district_id.ToString();
        //setGridData(GvResolution, data_m0_resulution.qa_cims_m0_resolution_list);

    }

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        switch (cmdName)
        {

            case "cmdInsertDoctor":

                mvSystem.SetActiveView(docDetailInsert);
                fvInsertDataDoctor.ChangeMode(FormViewMode.Insert);

                //getSelect_Province(ddl_Province_insert);
                //getSelect_Amphoe(ddl_Amphoe_insert, int.Parse(ddl_Province_insert.SelectedValue));
                //getSelect_district(ddl_District_insert, int.Parse(ddl_Amphoe_insert.SelectedValue));


                break;

            case "cmdSave":

                TextBox txt_doctorname_insert = (TextBox)fvInsertDataDoctor.FindControl("txt_doctorname_insert");
                TextBox txt_card_number_insert = (TextBox)fvInsertDataDoctor.FindControl("txt_card_number_insert");
                TextBox txt_health_authority_name_insert = (TextBox)fvInsertDataDoctor.FindControl("txt_health_authority_name_insert");
                TextBox txt_location_name_insert = (TextBox)fvInsertDataDoctor.FindControl("txt_location_name_insert");
                TextBox txt_village_no_insert = (TextBox)fvInsertDataDoctor.FindControl("txt_village_no_insert");
                TextBox txt_road_name_insert = (TextBox)fvInsertDataDoctor.FindControl("txt_road_name_insert");
                DropDownList ddl_Province_insert = (DropDownList)fvInsertDataDoctor.FindControl("ddl_Province_insert");
                DropDownList ddl_Amphoe_insert = (DropDownList)fvInsertDataDoctor.FindControl("ddl_Amphoe_insert");
                DropDownList ddl_District_insert = (DropDownList)fvInsertDataDoctor.FindControl("ddl_District_insert");
                TextBox txt_phone_number_insert = (TextBox)fvInsertDataDoctor.FindControl("txt_phone_number_insert");

                data_hr_healthcheck data_m0_doctor_insert = new data_hr_healthcheck();
                hr_healthcheck_m0_doctor_detail m0_doctor_insert= new hr_healthcheck_m0_doctor_detail();

                data_m0_doctor_insert.healthcheck_m0_doctor_list = new hr_healthcheck_m0_doctor_detail[1];
                m0_doctor_insert.m0_doctor_idx = 0;
                m0_doctor_insert.doctor_name = txt_doctorname_insert.Text;
                m0_doctor_insert.card_number = txt_card_number_insert.Text;
                m0_doctor_insert.health_authority_name = txt_health_authority_name_insert.Text;
                m0_doctor_insert.location_name = txt_location_name_insert.Text;
                m0_doctor_insert.village_no = txt_village_no_insert.Text;
                m0_doctor_insert.road_name = txt_road_name_insert.Text;
                m0_doctor_insert.ProvIDX = int.Parse(ddl_Province_insert.SelectedValue);
                m0_doctor_insert.AmpIDX = int.Parse(ddl_Amphoe_insert.SelectedValue);
                m0_doctor_insert.DistIDX = int.Parse(ddl_District_insert.SelectedValue);
                m0_doctor_insert.cemp_idx = _emp_idx;
                m0_doctor_insert.phone_number = txt_phone_number_insert.Text;


                data_m0_doctor_insert.healthcheck_m0_doctor_list[0] = m0_doctor_insert;
                //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        

                data_m0_doctor_insert = callServicePostMasterHealthCheck(_urlSetDoctorHistory, data_m0_doctor_insert);

                //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_doctor_insert));
                if (data_m0_doctor_insert.return_code == 1)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);

                }
                else
                {
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                    //tex_equipment_result.Text = String.Empty;
                }

                break;

            case "cmdSaveUpdate":

                int m0_doctor_idx_update = int.Parse(cmdArg);

                TextBox txt_doctorname_update = (TextBox)fvInsertDataDoctor.FindControl("txt_doctorname_insert");
                TextBox txt_card_number_update = (TextBox)fvInsertDataDoctor.FindControl("txt_card_number_insert");
                TextBox txt_health_authority_name_update = (TextBox)fvInsertDataDoctor.FindControl("txt_health_authority_name_insert");
                TextBox txt_location_name_update = (TextBox)fvInsertDataDoctor.FindControl("txt_location_name_insert");
                TextBox txt_village_no_update = (TextBox)fvInsertDataDoctor.FindControl("txt_village_no_insert");
                TextBox txt_road_name_update = (TextBox)fvInsertDataDoctor.FindControl("txt_road_name_insert");
                DropDownList ddl_Province_update = (DropDownList)fvInsertDataDoctor.FindControl("ddl_Province_insert");
                DropDownList ddl_Amphoe_update = (DropDownList)fvInsertDataDoctor.FindControl("ddl_Amphoe_insert");
                DropDownList ddl_District_update = (DropDownList)fvInsertDataDoctor.FindControl("ddl_District_insert");
                TextBox txt_phone_number_update = (TextBox)fvInsertDataDoctor.FindControl("txt_phone_number_insert");
                DropDownList ddl_DoctorStatus_insert = (DropDownList)fvInsertDataDoctor.FindControl("ddl_DoctorStatus_insert");


                data_hr_healthcheck data_m0_doctor_update = new data_hr_healthcheck();
                hr_healthcheck_m0_doctor_detail m0_doctor_update = new hr_healthcheck_m0_doctor_detail();

                data_m0_doctor_update.healthcheck_m0_doctor_list = new hr_healthcheck_m0_doctor_detail[1];
                m0_doctor_update.m0_doctor_idx = m0_doctor_idx_update;
                m0_doctor_update.doctor_name = txt_doctorname_update.Text;
                m0_doctor_update.card_number = txt_card_number_update.Text;
                m0_doctor_update.health_authority_name = txt_health_authority_name_update.Text;
                m0_doctor_update.location_name = txt_location_name_update.Text;
                m0_doctor_update.village_no = txt_village_no_update.Text;
                m0_doctor_update.road_name = txt_road_name_update.Text;
                m0_doctor_update.ProvIDX = int.Parse(ddl_Province_update.SelectedValue);
                m0_doctor_update.AmpIDX = int.Parse(ddl_Amphoe_update.SelectedValue);
                m0_doctor_update.DistIDX = int.Parse(ddl_District_update.SelectedValue);
                m0_doctor_update.cemp_idx = _emp_idx;
                m0_doctor_update.phone_number = txt_phone_number_update.Text;
                m0_doctor_update.doctor_status = int.Parse(ddl_DoctorStatus_insert.SelectedValue);


                data_m0_doctor_update.healthcheck_m0_doctor_list[0] = m0_doctor_update;
                
                data_m0_doctor_update = callServicePostMasterHealthCheck(_urlSetDoctorHistory, data_m0_doctor_update);

                //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_doctor_update));
                if (data_m0_doctor_update.return_code == 1)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);

                }
                else
                {
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                    //tex_equipment_result.Text = String.Empty;
                }

                break;

            case "cmdDelDoctor":

                int m0_doctor_idx_del = int.Parse(cmdArg);

                data_hr_healthcheck data_doctor_del = new data_hr_healthcheck();
                hr_healthcheck_m0_doctor_detail m0_doctor_detail_del = new hr_healthcheck_m0_doctor_detail();
                data_doctor_del.healthcheck_m0_doctor_list = new hr_healthcheck_m0_doctor_detail[1];

                m0_doctor_detail_del.cemp_idx = _emp_idx;
                m0_doctor_detail_del.m0_doctor_idx = m0_doctor_idx_del;

                data_doctor_del.healthcheck_m0_doctor_list[0] = m0_doctor_detail_del;
                //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        

                data_doctor_del = callServicePostMasterHealthCheck(_urlSetDelDoctor, data_doctor_del);

                Select_DetailDoctor();

                break;

            case "cmdUpdateDoctor":

                mvSystem.SetActiveView(docDetailInsert);
                fvInsertDataDoctor.ChangeMode(FormViewMode.Edit);

                actionReadDetailDoctor(int.Parse(cmdArg));

                break;

            case "cmdCancel":

                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;




        }
    }
    #endregion btnCommand

    #region bind data
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected string getStatus(int status)
    {
        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }
    #endregion bind data

    #region Gridview
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvResolution":


                if (e.Row.RowType == DataControlRowType.DataRow)
                {


                    Label lbl_resolution_status = (Label)e.Row.FindControl("lbl_resolution_status");
                    Label lbl_resolution_statusOnline = (Label)e.Row.FindControl("lbl_resolution_statusOnline");
                    Label lbl_resolution_statusOffline = (Label)e.Row.FindControl("lbl_resolution_statusOffline");

                    ViewState["vs_status_resolution"] = lbl_resolution_status.Text;


                    if (ViewState["vs_status_resolution"].ToString() == "1")
                    {
                        lbl_resolution_statusOnline.Visible = true;
                    }
                    else if (ViewState["vs_status_resolution"].ToString() == "0")
                    {
                        lbl_resolution_statusOffline.Visible = true;
                    }
                    else
                    {

                    }



                    var _unit_idx_Resolution = (Label)e.Row.FindControl("lbequipment_unit_idx");
                    var _unit_name_Resolution = (Label)e.Row.FindControl("lbl_unit_symbol_en");

                    switch (_unit_idx_Resolution.Text)
                    {
                        case "29":
                            _unit_name_Resolution.Text = "µl";
                            break;

                        case "30":
                            _unit_name_Resolution.Text = "µm";
                            break;

                        case "31":
                            _unit_name_Resolution.Text = "°C";
                            break;
                    }


                }

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";


                    Label _lbl_unit_idx_edit = (Label)e.Row.FindControl("lbl_unit_idx_edit");
                    //getM0CaltypeList((DropDownList)e.Row.Cells[0].FindControl("DD_caltype"), txt_caltype.Text);

                   // getM0Unit((DropDownList)e.Row.FindControl("ddlunit_idx_edit"), _lbl_unit_idx_edit.Text);

                }

                break;
        }
    }

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvResolution":
                //GvResolution.EditIndex = e.NewEditIndex;
                //Select_Resolution();

                break;
        }
    }

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvResolution":

                ////var txt_equipment_resolution_idx_edit = (TextBox)GvResolution.Rows[e.RowIndex].FindControl("txt_equipment_resolution_idx_edit");
                ////var txt_resolution_name_edit = (TextBox)GvResolution.Rows[e.RowIndex].FindControl("txt_resolution_name_edit");
                ////var ddlunit_idx_edit = (DropDownList)GvResolution.Rows[e.RowIndex].FindControl("ddlunit_idx_edit");
                ////var ddl_resolution_status_edit = (DropDownList)GvResolution.Rows[e.RowIndex].FindControl("ddl_resolution_status_edit");


                ////GvResolution.EditIndex = -1;

                ////data_qa_cims data_resolution_edit = new data_qa_cims();
                ////data_resolution_edit.qa_cims_m0_resolution_list = new qa_cims_m0_resulution_detail[1];
                ////qa_cims_m0_resulution_detail m0_resolution_edit = new qa_cims_m0_resulution_detail();

                ////m0_resolution_edit.equipment_resolution_idx = int.Parse(txt_equipment_resolution_idx_edit.Text);
                ////m0_resolution_edit.resolution_name = txt_resolution_name_edit.Text;
                ////m0_resolution_edit.unit_idx = int.Parse(ddlunit_idx_edit.SelectedValue);
                ////m0_resolution_edit.resolution_status = int.Parse(ddl_resolution_status_edit.SelectedValue);

                ////data_resolution_edit.qa_cims_m0_resolution_list[0] = m0_resolution_edit;

                //////litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_resolution_edit));

                ////data_resolution_edit = callServicePostMasterQACIMS(_urlSetM0_Resolution, data_resolution_edit);

                ////if (data_resolution_edit.return_code == 1)
                ////{
                ////    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ข้อมูลซ้ำ กรุณาแก้ไขใหม่อีกครั้ง!!!');", true);
                ////    Select_Resolution();
                ////}
                ////else
                ////{
                ////    Select_Resolution();
                ////}
                break;
        }
    }

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvResolution":
                ////GvResolution.EditIndex = -1;
                ////Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
        }
    }

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvResolution":
                ////GvResolution.PageIndex = e.NewPageIndex;
                ////GvResolution.DataBind();
                ////Select_Resolution();
                break;
        }
    }

    #endregion Gridview

    #region Formview
    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        if (sender is FormView)
        {
            FormView FvName = (FormView)sender;
            switch (FvName.ID)
            {
                case "fvInsertDataDoctor":
                    if (FvName.CurrentMode == FormViewMode.ReadOnly) { }
                    if (FvName.CurrentMode == FormViewMode.Edit)
                    {

                        Label lbl_ddl_Province_update = (Label)fvInsertDataDoctor.FindControl("lbl_ddl_Province_update");
                        Label lbl_ddl_Amphoe_update = (Label)fvInsertDataDoctor.FindControl("lbl_ddl_Amphoe_update");
                        Label lbl_ddl_District_update = (Label)fvInsertDataDoctor.FindControl("lbl_ddl_District_update");

                        DropDownList ddl_Province_insert = (DropDownList)fvInsertDataDoctor.FindControl("ddl_Province_insert");
                        DropDownList ddl_Amphoe_insert = (DropDownList)fvInsertDataDoctor.FindControl("ddl_Amphoe_insert");
                        DropDownList ddl_District_insert = (DropDownList)fvInsertDataDoctor.FindControl("ddl_District_insert");

                        getSelect_Province(ddl_Province_insert, int.Parse(lbl_ddl_Province_update.Text));
                        getSelect_Amphoe(ddl_Amphoe_insert, int.Parse(ddl_Province_insert.SelectedValue), int.Parse(lbl_ddl_Amphoe_update.Text));
                        getSelect_district(ddl_District_insert, int.Parse(ddl_Amphoe_insert.SelectedValue), int.Parse(lbl_ddl_District_update.Text));



                    }
                    if (FvName.CurrentMode == FormViewMode.Insert)
                    {
                        DropDownList ddl_Province_insert = (DropDownList)fvInsertDataDoctor.FindControl("ddl_Province_insert");
                        DropDownList ddl_Amphoe_insert = (DropDownList)fvInsertDataDoctor.FindControl("ddl_Amphoe_insert");
                        DropDownList ddl_District_insert = (DropDownList)fvInsertDataDoctor.FindControl("ddl_District_insert");
                     
                        getSelect_Province(ddl_Province_insert, 0);
                        getSelect_Amphoe(ddl_Amphoe_insert, int.Parse(ddl_Province_insert.SelectedValue), 0);
                        getSelect_district(ddl_District_insert, int.Parse(ddl_Amphoe_insert.SelectedValue), 0);
                
                    }                    
                    break;
            }
        }
    }

    #endregion Formview


   #region event dropdownlist

        protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (DropDownList)sender;


        switch (ddName.ID)
        {
            case "ddl_Province_insert":

                DropDownList ddl_Province_insert = (DropDownList)fvInsertDataDoctor.FindControl("ddl_Province_insert");
                DropDownList ddl_Amphoe_insert = (DropDownList)fvInsertDataDoctor.FindControl("ddl_Amphoe_insert");
                DropDownList ddl_District_insert = (DropDownList)fvInsertDataDoctor.FindControl("ddl_District_insert");


                getSelect_Amphoe(ddl_Amphoe_insert, int.Parse(ddl_Province_insert.SelectedItem.Value), 0);
                ddl_District_insert.Items.Clear();
                ddl_District_insert.Items.Insert(0, new ListItem("--- เลือกตำบล ---", "0"));

                break;
            case "ddl_Amphoe_insert":

                DropDownList _ddl_Province_insert = (DropDownList)fvInsertDataDoctor.FindControl("ddl_Province_insert");
                DropDownList _ddl_Amphoe_insert = (DropDownList)fvInsertDataDoctor.FindControl("ddl_Amphoe_insert");
                DropDownList _ddl_District_insert = (DropDownList)fvInsertDataDoctor.FindControl("ddl_District_insert");

                getSelect_district(_ddl_District_insert, int.Parse(_ddl_Amphoe_insert.SelectedItem.Value), 0);

                break;
        }
    }
    #endregion dropdownlist

   #region callService 
   protected data_hr_healthcheck callServicePostMasterHealthCheck(string _cmdUrl, data_hr_healthcheck _data_hr_healthcheck)
   {
        _localJson = _funcTool.convertObjectToJson(_data_hr_healthcheck);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_hr_healthcheck = (data_hr_healthcheck)_funcTool.convertJsonToObject(typeof(data_hr_healthcheck), _localJson);


        return _data_hr_healthcheck;
   }
   #endregion callService
}