﻿using System;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_log")]
public class data_log
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }
    [XmlElement("m0_event_list")]
    public m0_event_type[] m0_event_list { get; set; }
    [XmlElement("u0_log_list")]
    public u0_log_detail[] u0_log_list { get; set; }
    [XmlElement("search_key_list")]
    public search_key[] search_key_list { get; set; }
}

[Serializable]
public class m0_event_type
{
    [XmlElement("midx")]
    public int midx { get; set; }
    [XmlElement("event_type")]
    public string event_type { get; set; }
}

[Serializable]
public class u0_log_detail
{
    [XmlElement("uidx")]
    public Int64 uidx { get; set; }
    [XmlElement("log_detail")]
    public string log_detail { get; set; }
    [XmlElement("m0_event_type")]
    public int m0_event_type { get; set; }
    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }
    [XmlElement("ip_address")]
    public string ip_address { get; set; }
    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("event_type")]
    public string event_type { get; set; }
    [XmlElement("emp_code")]
    public string emp_code { get; set; }
    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }
}

[Serializable]
public class search_key
{
    [XmlElement("s_uidx")]
    public string s_uidx { get; set; }
    [XmlElement("s_log_detail")]
    public string s_log_detail { get; set; }
    [XmlElement("s_m0_event_type")]
    public string s_m0_event_type { get; set; }
    [XmlElement("s_emp_idx")]
    public string s_emp_idx { get; set; }
    [XmlElement("s_ip_address")]
    public string s_ip_address { get; set; }
    [XmlElement("s_create_date")]
    public string s_create_date { get; set; }

    [XmlElement("s_emp_code")]
    public string s_emp_code { get; set; }
    [XmlElement("s_emp_name_th")]
    public string s_emp_name_th { get; set; }
}
