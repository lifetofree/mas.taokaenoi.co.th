﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Drawing;

public partial class websystem_reference_price_reference_price : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        setActiveTab("tab1", 0, 0);
    }


    #region event command
    protected void navCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        setActiveTab(cmdArg, 0, 0);
    }

    #endregion

    protected void setActiveView(string activeTab, int uidx, int decision)
    {
        mvReferencePrice.SetActiveView((View)mvReferencePrice.FindControl(activeTab));

        switch (activeTab)
        {
            case "tab1":
                
                break;
            case "tab2":

                break;
            case "tab3":

                break;
            case "tab4":

                break;
        }
    }

    protected void setActiveTab(string activeTab, int uidx, int decision)
    {
        setActiveView(activeTab, uidx, decision);
        switch (activeTab)
        {
            case "tab1":
                li0.Attributes.Add("class", "active");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                break;
            case "tab2":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "active");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                break;
            case "tab3":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "active");
                li3.Attributes.Add("class", "");
                break;
            case "tab4":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "active");
                break;
        }
    }
}