﻿using System;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_permission_set")]
public class data_permission_set
{
    [XmlElement("return_code")]
    public string return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }
    [XmlElement("return_idx")]
    public string return_idx { get; set; }

    [XmlElement("permission_list")]
    public permission_set_detail[] permission_list { get; set; }

    [XmlElement("permission_use_list")]
    public permission_use_detail[] permission_use_list { get; set; }


}

[Serializable]
public class permission_set_detail
{
    [XmlElement("peridx")]
    public int peridx { get; set; }
    [XmlElement("emp_idx_set")]
    public string emp_idx_set { get; set; }
    [XmlElement("rdept_idx_set")]
    public string rdept_idx_set { get; set; }
    [XmlElement("rsec_idx_set")]
    public string rsec_idx_set { get; set; }
    [XmlElement("rpos_idx_set")]
    public string rpos_idx_set { get; set; }
    [XmlElement("p_system")]
    public int p_system { get; set; }
    [XmlElement("p_permission_type")]
    public int p_permission_type { get; set; }
    [XmlElement("p_status")]
    public int p_status { get; set; }
    [XmlElement("p_emp_create")]
    public int p_emp_create { get; set; }
    [XmlElement("org_idx_use")]
    public string org_idx_use { get; set; }
    [XmlElement("rdept_idx_use")]
    public string rdept_idx_use { get; set; }
    [XmlElement("rsec_idx_use")]
    public string rsec_idx_use { get; set; }
    [XmlElement("rpos_idx_use")]
    public string rpos_idx_use { get; set; }
    [XmlElement("type_action")]
    public int type_action { get; set; }

	[XmlElement("org_name_set")]
    public string org_name_set { get; set; }
    [XmlElement("rdep_name_set")]
    public string rdep_name_set { get; set; }
    [XmlElement("rsec_name_set")]
    public string rsec_name_set { get; set; }
    [XmlElement("rpos_name_set")]
    public string rpos_name_set { get; set; }

	[XmlElement("org_name_use")]
    public string org_name_use { get; set; }
    [XmlElement("rdep_name_use")]
    public string rdep_name_use { get; set; }
    [XmlElement("rsec_name_use")]
    public string rsec_name_use { get; set; }
    [XmlElement("rpos_name_use")]
    public string rpos_name_use { get; set; }

    [XmlElement("p_system_name")]
    public string p_system_name { get; set; }
    [XmlElement("p_permission_type_name")]
    public string p_permission_type_name { get; set; }
	[XmlElement("SecNameTH")]
    public string SecNameTH { get; set; }
    [XmlElement("RSecIDX")]
    public int RSecIDX { get; set; }
    [XmlElement("PosNameTH")]
    public string PosNameTH { get; set; }
    [XmlElement("RPosIDX")]
    public int RPosIDX { get; set; }


    [XmlElement("orgidx_search")]
    public int orgidx_search { get; set; }

    [XmlElement("rdeptidx_search")]
    public int rdeptidx_search { get; set; }

    [XmlElement("rsecidx_search")]
    public int rsecidx_search { get; set; }

    [XmlElement("rposidx_search")]
    public int rposidx_search { get; set; }

    [XmlElement("system_search")]
    public int system_search { get; set; }


}

[Serializable]
public class permission_use_detail
{
    [XmlElement("peridx")]
    public int peridx { get; set; }
    [XmlElement("emp_idx_set")]
    public string emp_idx_set { get; set; }
    [XmlElement("rdept_idx_set")]
    public string rdept_idx_set { get; set; }
    [XmlElement("rsec_idx_set")]
    public string rsec_idx_set { get; set; }
    [XmlElement("rpos_idx_set")]
    public string rpos_idx_set { get; set; }
    [XmlElement("p_system")]
    public int p_system { get; set; }
    [XmlElement("p_permission_type")]
    public int p_permission_type { get; set; }
    [XmlElement("p_status")]
    public int p_status { get; set; }
    [XmlElement("p_emp_create")]
    public int p_emp_create { get; set; }
    [XmlElement("org_idx_use")]
    public string org_idx_use { get; set; }
    [XmlElement("rdept_idx_use")]
    public string rdept_idx_use { get; set; }
    [XmlElement("rsec_idx_use")]
    public string rsec_idx_use { get; set; }
    [XmlElement("rpos_idx_use")]
    public string rpos_idx_use { get; set; }
    [XmlElement("type_action")]
    public int type_action { get; set; }

    [XmlElement("rdep_name_set")]
    public string rdep_name_set { get; set; }
    [XmlElement("rsec_name_set")]
    public string rsec_name_set { get; set; }
    [XmlElement("rpos_name_set")]
    public string rpos_name_set { get; set; }

    [XmlElement("rdep_name_use")]
    public string rdep_name_use { get; set; }
    [XmlElement("rsec_name_use")]
    public string rsec_name_use { get; set; }
    [XmlElement("rpos_name_use")]
    public string rpos_name_use { get; set; }

    [XmlElement("p_system_name")]
    public string p_system_name { get; set; }
    [XmlElement("p_permission_type_name")]
    public string p_permission_type_name { get; set; }
}

