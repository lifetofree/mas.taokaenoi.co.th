﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class websystem_MasterData_qa_lab_form_detail : System.Web.UI.Page
{

    #region initial function/data
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();
    data_qa _data_qa = new data_qa();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];

    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    //-- employee --//

    //-- master data setname & form detail --//
    static string _urlQaGetOption = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetOption"];
    static string _urlQaGetNameList = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetNameList"];
    static string _urlQaSetFormDetail = _serviceUrl + ConfigurationManager.AppSettings["urlQaSetFormDetail"];
    static string _urlQaGetFormDetail = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetFormDetail"];
    static string _urlQaDelFormDetail = _serviceUrl + ConfigurationManager.AppSettings["urlQaDelFormDetail"];
   

    
    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    bool _flag_qmr = false;

    // rdept permission
    // int[] rdept_qmr = { 20 }; //QMR:26
    #endregion initial function/data

    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
        //check permission
        ////foreach (int item in rdept_qmr)
        ////{
        ////    if (_dataEmployee.employee_list[0].rdept_idx == item)
        ////    {
        ////        _flag_qmr = true;
        ////        break;
        ////    }
        ////}
    }



    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            initPage();
        }
       linkBtnTrigger(btnAddFormDetail);
    }


    protected void initPage()
    {

        clearSession();
        clearViewState();

        setActiveView("pageGenaral", 0);
        setFormData(fvformInsert, FormViewMode.ReadOnly, null);
        selectFormDetail();
    }


    #region class select
    protected void selectFormDetail()
    {
        _data_qa.qa_m0_form_detail_list = new qa_m0_form_detail[1];
        qa_m0_form_detail _selectForm = new qa_m0_form_detail();
        _data_qa.qa_m0_form_detail_list[0] = _selectForm;
        _data_qa = callServicePostMasterQA(_urlQaGetFormDetail, _data_qa);
        //if (_data_qa.qa_m0_form_detail_list != null)
        //{
        //    var _linqSet = from data_qa in _data_qa.qa_m0_form_detail_list
        //                   where data_qa.form_detail_root_idx == (0)
        //                   select data_qa;
        //    setGridData(gvMaster, _linqSet.ToList());
        //}

        //else
        //{
            setGridData(gvMaster, _data_qa.qa_m0_form_detail_list);
      //  }
        

    }

    protected void selectFormDetailLevel2()
    {
        _data_qa.qa_m0_form_detail_list = new qa_m0_form_detail[1];
        qa_m0_form_detail _selectFormRoot = new qa_m0_form_detail();
        _data_qa.qa_m0_form_detail_list[0] = _selectFormRoot;
        _data_qa = callServicePostMasterQA(_urlQaGetFormDetail, _data_qa);
        var _linqRootSet = from data_qa in _data_qa.qa_m0_form_detail_list
                           where data_qa.form_detail_root_idx == int.Parse(ViewState["form_idx"].ToString())
                           select data_qa;
        setGridData(gvMasterRoot, _linqRootSet.ToList());

    }

    protected void selectRootSubFormDetail()
    {
        _data_qa.qa_m0_form_detail_list = new qa_m0_form_detail[1];
        qa_m0_form_detail _selectFormRoot = new qa_m0_form_detail();
        _data_qa.qa_m0_form_detail_list[0] = _selectFormRoot;
        _data_qa = callServicePostMasterQA(_urlQaGetFormDetail, _data_qa);
        var _linqSubSet = from data_qa in _data_qa.qa_m0_form_detail_list
                          where data_qa.form_detail_root_idx == int.Parse(ViewState["form_idx"].ToString())
                          select data_qa;
       setGridData(gvMasterSubForm, _linqSubSet.ToList());

    }

    protected void getM0RootFormList()
    {
        setFormData(fvformInsertRoot, FormViewMode.Insert, null);
        TextBox tbFormNameLevel1 = (TextBox)fvformInsertRoot.FindControl("tbFormNameLevel1");

        data_qa _dataqaForm = new data_qa();
        _dataqaForm.qa_m0_form_detail_list = new qa_m0_form_detail[1];
        qa_m0_form_detail _m0SubSet = new qa_m0_form_detail();
        _dataqaForm.qa_m0_form_detail_list[0] = _m0SubSet;
        _dataqaForm = callServicePostMasterQA(_urlQaGetFormDetail, _dataqaForm);
        var _linqSetFrom = (from data_qa in _dataqaForm.qa_m0_form_detail_list
                        where data_qa.form_detail_idx == int.Parse(ViewState["form_idx"].ToString())
                        select new
                        {
                            data_qa.form_detail_name
                        }).ToList();
        foreach (var item in _linqSetFrom)
        {
            tbFormNameLevel1.Text = item.form_detail_name;

        }

    }

    protected void getM0RootFormSubList()
    {
        TextBox tbFormNameRootLevel2 = (TextBox)fvformInsertSubFormDetail.FindControl("tbFormNameRootLevel2");

        data_qa _dataqa = new data_qa();
        _dataqa.qa_m0_setname_list = new qa_m0_setname_detail[1];
        qa_m0_setname_detail _m0SubSet = new qa_m0_setname_detail();
        _dataqa.qa_m0_setname_list[0] = _m0SubSet;
        _dataqa = callServicePostMasterQA(_urlQaGetFormDetail, _dataqa);
        var _linqSet = (from data_qa in _dataqa.qa_m0_form_detail_list
                        where data_qa.form_detail_idx == int.Parse(ViewState["form_idx"].ToString())
                        select new
                        {
                            data_qa.form_detail_name
                        }).ToList();
        foreach (var item in _linqSet)
        {
            tbFormNameRootLevel2.Text = item.form_detail_name;

        }
    }

    #region dropdown list
    protected void getM0SetNameList(DropDownList ddlName, string _set_idx)
    {
         data_qa _dataqa = new data_qa();
        _dataqa.qa_m0_setname_list = new qa_m0_setname_detail[1];
        qa_m0_setname_detail _m0Set = new qa_m0_setname_detail();
        _dataqa.qa_m0_setname_list[0] = _m0Set;
        
        _dataqa = callServicePostMasterQA(_urlQaGetNameList, _dataqa);
       // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataqa));

        var _linqSetName = from data in _dataqa.qa_m0_setname_list
                           where data.setroot_idx == (0)
                           && data.set_name_status == (1)
                           select data;
     //  ddlName.SelectedValue = _set_idx;
       setDdlData(ddlName, _linqSetName.ToList(), "set_name", "set_idx");
       ddlName.Items.Insert(0, new ListItem("--- เลือกชุดข้อมูล ---", "0"));
       ddlName.SelectedValue = _set_idx;
    }

    protected void getM0OptionList(DropDownList ddlName, string _option_idx)
    {
        data_qa _dataOption = new data_qa();
        _dataOption.qa_m0_option_list = new qa_m0_option_detail[1];
        qa_m0_option_detail _m0Option = new qa_m0_option_detail();
        _dataOption.qa_m0_option_list[0] = _m0Option;
        _dataOption = callServicePostMasterQA(_urlQaGetOption, _dataOption);

        setDdlData(ddlName, _dataOption.qa_m0_option_list, "option_name", "option_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกการกรอกข้อมูล ---", "0"));
        ddlName.SelectedValue = _option_idx;
    }

    #endregion

    #endregion


    #region event command


    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

   
        switch (cmdName)
        {

            case "cmdAddFromDetail":
                switch (cmdArg)
                {
                    case "0":
                        setFormData(fvformInsert, FormViewMode.Insert, null);
                        getM0SetNameList((DropDownList)fvformInsert.FindControl("ddlSetNameCreate"), "0");
                        getM0OptionList((DropDownList)fvformInsert.FindControl("ddlOptionCreate"), "0");
                        break;

                    case "1":
                        setFormData(fvformInsertRoot, FormViewMode.Insert, null);
                        getM0RootFormList();
                        getM0SetNameList((DropDownList)fvformInsertRoot.FindControl("ddlSetName"), "0");
                        getM0OptionList((DropDownList)fvformInsertRoot.FindControl("ddlOption"), "0");
                        break;
                    case "2":

                        setFormData(fvformInsertSubFormDetail, FormViewMode.Insert, null);
                        getM0RootFormSubList();
                        getM0SetNameList((DropDownList)fvformInsertSubFormDetail.FindControl("ddlSubSetName"), "0");
                        getM0OptionList((DropDownList)fvformInsertSubFormDetail.FindControl("ddlSubOption"), "0");
                        break;

                }

                break;

            case "cmdCancel":
                switch (cmdArg)
                {
                    case "0":
                        setActiveView("pageGenaral", 0);
                        setFormData(fvformInsert, FormViewMode.ReadOnly, null);
                        SETFOCUS.Focus();
                        break;

                    case "1":
                        setActiveView("pageCreateRoot", 0);
                     
                        if ((ViewState["ROOTIDX"].ToString()) != null)
                        {
                            ViewState["form_idx"] = ViewState["ROOTIDX"].ToString();
                            getM0RootFormList();
                            setFormData(fvformInsertRoot, FormViewMode.ReadOnly, null);
                        }
                        else
                        {
                            ViewState["form_idx"] = 0;
                            setFormData(fvformInsertRoot, FormViewMode.Insert, null);
                            //DropDownList ddlSetName = (DropDownList)fvformInsertRoot.FindControl("ddlSetName");s
                            //getM0SetNameList(ddlSetName);
                        }

                        SETFOCUS.Focus();
                        break;

                    case "2":
                        setActiveView("pageRootSubSet", 0);
                        setFormData(fvformInsertSubFormDetail, FormViewMode.ReadOnly, null);
                        SETFOCUS.Focus();
                        break;
                }

                break;

            case "cmdSave":
                switch (cmdArg)
                {
                    case "0":
                        TextBox tbFormName = (TextBox)fvformInsert.FindControl("tbFormName");
                        TextBox tbFormDetail = (TextBox)fvformInsert.FindControl("tbFormDetail");
                        DropDownList ddlOptionCreate = (DropDownList)fvformInsert.FindControl("ddlOptionCreate");
                        DropDownList ddlSetNameCreate = (DropDownList)fvformInsert.FindControl("ddlSetNameCreate");
                        DropDownList ddlFormStatus = (DropDownList)fvformInsert.FindControl("ddlFormStatus");


                        _data_qa.qa_m0_form_detail_list = new qa_m0_form_detail[1];
                        qa_m0_form_detail _m0form = new qa_m0_form_detail();
                        _m0form.cemp_idx = _emp_idx;
                        _m0form.form_detail_name = tbFormName.Text;
                        _m0form.form_detail = tbFormDetail.Text;
                        _m0form.option_idx = int.Parse(ddlOptionCreate.SelectedValue);
                        _m0form.set_idx = int.Parse(ddlSetNameCreate.SelectedValue);
                        _m0form.form_datail_status = int.Parse(ddlFormStatus.SelectedValue);
                        _data_qa.qa_m0_form_detail_list[0] = _m0form;

                        _data_qa = callServicePostMasterQA(_urlQaSetFormDetail, _data_qa);
                        //-- check data duplicate in database
                        if (_data_qa.return_code == 101)
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);

                        }
                        else
                        {
                        }
                        selectFormDetail();
                        setFormData(fvformInsert, FormViewMode.Insert, null);
                        getM0SetNameList((DropDownList)fvformInsert.FindControl("ddlSetNameCreate"), "0");
                        getM0OptionList((DropDownList)fvformInsert.FindControl("ddlOptionCreate"), "0");
                        break;

                    case "1":
                        
                        TextBox tbFormNameLevel2 = (TextBox)fvformInsertRoot.FindControl("tbFormNameLevel2");
                        TextBox tbFormDetailLevel2 = (TextBox)fvformInsertRoot.FindControl("tbFormDetailLevel2");
                        DropDownList ddlFormStatusLevel2 = (DropDownList)fvformInsertRoot.FindControl("ddlFormStatusLevel2");
                        DropDownList ddlOption = (DropDownList)fvformInsertRoot.FindControl("ddlOption");
                        DropDownList ddlSetName = (DropDownList)fvformInsertRoot.FindControl("ddlSetName");

                        data_qa _dataSave = new data_qa();
                        _dataSave.qa_m0_form_detail_list = new qa_m0_form_detail[1];
                        qa_m0_form_detail _m0root = new qa_m0_form_detail();
                        _m0root.cemp_idx = _emp_idx;
                        _m0root.form_detail_name = tbFormNameLevel2.Text;
                        _m0root.form_detail = tbFormDetailLevel2.Text;
                        _m0root.form_detail_root_idx = int.Parse(ViewState["form_idx"].ToString());
                        _m0root.form_datail_status = int.Parse(ddlFormStatusLevel2.SelectedValue);
                        _m0root.set_idx = int.Parse(ddlSetName.SelectedValue);
                        _m0root.option_idx = int.Parse(ddlOption.SelectedValue);

                        _dataSave.qa_m0_form_detail_list[0] = _m0root;
                      //  litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataSave));
                        _dataSave = callServicePostMasterQA(_urlQaSetFormDetail, _dataSave);
                        ///--check data duplicate in database
                        if (_dataSave.return_code == 101)
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);

                        }
                        else
                        {
                       
                        }

                        selectFormDetailLevel2();
                        setFormData(fvformInsertRoot, FormViewMode.Insert, null);
                        getM0RootFormList();
                     //   getM0SetNameList((DropDownList)fvformInsertRoot.FindControl("ddlSetName"),"0");
                    //    getM0OptionList((DropDownList)fvformInsertRoot.FindControl("ddlOption"),"0");
                        break;

                    case "2":
                        TextBox tbFormNameLevel3 = (TextBox)fvformInsertSubFormDetail.FindControl("tbFormNameLevel3");
                        TextBox tbFormDetailLevel3 = (TextBox)fvformInsertSubFormDetail.FindControl("tbFormDetailLevel3");
                        DropDownList ddlFormStatusLevel3 = (DropDownList)fvformInsertSubFormDetail.FindControl("ddlFormStatusLevel3");
                        DropDownList ddlSubOption = (DropDownList)fvformInsertSubFormDetail.FindControl("ddlSubOption");
                        DropDownList ddlSubSetName = (DropDownList)fvformInsertSubFormDetail.FindControl("ddlSubSetName");

                        _data_qa.qa_m0_form_detail_list = new qa_m0_form_detail[1];
                        qa_m0_form_detail _m0FormSub = new qa_m0_form_detail();
                        _m0FormSub.cemp_idx = _emp_idx;
                        _m0FormSub.form_detail_name = tbFormNameLevel3.Text;
                        _m0FormSub.form_detail_root_idx = int.Parse(ViewState["form_idx"].ToString());
                        _m0FormSub.form_datail_status = int.Parse(ddlFormStatusLevel3.SelectedValue);
                        _m0FormSub.form_detail = tbFormDetailLevel3.Text;
                        _m0FormSub.set_idx = int.Parse(ddlSubSetName.SelectedValue);
                        _m0FormSub.option_idx = int.Parse(ddlSubOption.SelectedValue);

                        _data_qa.qa_m0_form_detail_list[0] = _m0FormSub;
                        _data_qa = callServicePostMasterQA(_urlQaSetFormDetail, _data_qa);
                        //-- check data duplicate in database
                        if (_data_qa.return_code == 101)
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);

                        }
                        else
                        {

                        }
                        selectRootSubFormDetail();
                        setFormData(fvformInsertSubFormDetail, FormViewMode.Insert, null);
                        getM0RootFormSubList();
                        break;
                }

                break;

            case "cmdDelete":

                string[] cmdArgDelete = cmdArg.Split(',');
                int formIDX = int.TryParse(cmdArgDelete[0].ToString(), out _default_int) ? int.Parse(cmdArgDelete[0].ToString()) : _default_int;
                int condition = int.TryParse(cmdArgDelete[1].ToString(), out _default_int) ? int.Parse(cmdArgDelete[1].ToString()) : _default_int;

                _data_qa.qa_m0_form_detail_list = new qa_m0_form_detail[1];
                qa_m0_form_detail _delform = new qa_m0_form_detail();
                _delform.form_detail_idx = formIDX;
                _data_qa.qa_m0_form_detail_list[0] = _delform;
                _data_qa = callServicePostMasterQA(_urlQaDelFormDetail, _data_qa);

                if (_data_qa.return_code == 0)
                {
                   // litDebug.Text = "success";
                }
                else
                {
                   // litDebug.Text = "not success";
                }

                switch (condition)
                {
                    // select form details level 1 (DELETE)
                    case 0:
                        selectFormDetail();
                        break;
                      //  select form details level 2(DELETE)
                    case 1:
                        selectFormDetailLevel2();
                        break;
                    //// select set name level 3 (DELETE)
                    case 2:
                        selectRootSubFormDetail();
                        break;
                }
                break;

            case "cmdView":
                string[] cmdArgView = cmdArg.Split(',');
                ViewState["form_idx"] = int.TryParse(cmdArgView[0].ToString(), out _default_int) ? int.Parse(cmdArgView[0].ToString()) : _default_int;
                int conditionView = int.TryParse(cmdArgView[1].ToString(), out _default_int) ? int.Parse(cmdArgView[1].ToString()) : _default_int;

                switch (conditionView)
                {
                    case 0:
                        setActiveView("pageCreateRoot", int.Parse(ViewState["form_idx"].ToString()));
                        setFormData(fvformInsertRoot, FormViewMode.ReadOnly, null);
                        selectFormDetailLevel2();
                        break;

                    case 1:
                        setActiveView("pageRootSubSet", int.Parse(ViewState["form_idx"].ToString()));
                        setFormData(fvformInsertSubFormDetail, FormViewMode.ReadOnly, null);
                        selectRootSubFormDetail();
                        break;

                }


                break;

        }
    }
    #endregion event command

    #region aaaa

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        // ViewState["listData"] = null;
    }


    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var gvName = (GridView)sender;

        switch (gvName.ID)
        {
            case "gvMaster":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbstatusForm = (Label)e.Row.Cells[3].FindControl("lbstatusForm");
                    Label status_online = (Label)e.Row.Cells[3].FindControl("status_online");
                    Label status_offline = (Label)e.Row.Cells[3].FindControl("status_offline");

                    ViewState["status_formDetail"] = lbstatusForm.Text;

                    if (ViewState["status_formDetail"].ToString() == "0")
                    {
                        status_offline.Visible = true;
                    }
                    else if (ViewState["status_formDetail"].ToString() == "1")
                    {
                        status_online.Visible = true;
                    }
                }

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    TextBox _UpdateSetNameIDX = (TextBox)e.Row.Cells[0].FindControl("tbUpdateSetNameIDX");
                    TextBox _UpdateOptionIDX = (TextBox)e.Row.Cells[0].FindControl("tbUpdateOptionIDX");
                    getM0SetNameList((DropDownList)e.Row.Cells[0].FindControl("ddlUpdateSetName"), _UpdateSetNameIDX.Text);
                    getM0OptionList((DropDownList)e.Row.Cells[0].FindControl("ddlUpdateOption"), _UpdateOptionIDX.Text);


                }



                break;

            case "gvMasterRoot":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbstatusFormLevel2 = (Label)e.Row.Cells[6].FindControl("lbstatusFormLevel2");
                    Label status_online = (Label)e.Row.Cells[6].FindControl("status_online");
                    Label status_offline = (Label)e.Row.Cells[6].FindControl("status_offline");
                    TextBox tbFormIDXLevel2 = (TextBox)e.Row.Cells[1].FindControl("tbFormIDXLevel2");
                  
                    //   getM0RootNameSetList(lblRootLevelSet);
                    ViewState["ROOTIDX"] = tbFormIDXLevel2.Text;

                    ViewState["status_rootidx"] = lbstatusFormLevel2.Text;

                    if (ViewState["status_rootidx"].ToString() == "0")
                    {
                        status_offline.Visible = true;
                    }
                    else if (ViewState["status_rootidx"].ToString() == "1")
                    {
                        status_online.Visible = true;
                    }


                }
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    TextBox _SetNameIDX = (TextBox)e.Row.Cells[0].FindControl("tbSetNameIDX");
                    TextBox _OptionIDX = (TextBox)e.Row.Cells[0].FindControl("tbOptionIDX");
                    getM0SetNameList((DropDownList)e.Row.Cells[0].FindControl("ddlSetNameUpdate"), _SetNameIDX.Text);
                    getM0OptionList((DropDownList)e.Row.Cells[0].FindControl("ddlOptionUpdate"), _OptionIDX.Text);

                }

                break;

            case "gvMasterSubForm":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbstatusFormSub = (Label)e.Row.Cells[4].FindControl("lbstatusFormSub");
                    Label status_online = (Label)e.Row.Cells[4].FindControl("status_online");
                    Label status_offline = (Label)e.Row.Cells[4].FindControl("status_offline");

                    ViewState["status_subForm"] = lbstatusFormSub.Text;

                    if (ViewState["status_subForm"].ToString() == "0")
                    {
                        status_offline.Visible = true;
                    }
                    else if (ViewState["status_subForm"].ToString() == "1")
                    {
                        status_online.Visible = true;
                    }


                }
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                }

                break;
        }
    }


    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var gvName = (GridView)sender;

        switch (gvName.ID)
        {
            case "gvMaster":
                gvMaster.PageIndex = e.NewPageIndex;
                gvMaster.DataBind();
                selectFormDetail();
                break;

            case "gvMasterRoot":
                gvMasterRoot.PageIndex = e.NewPageIndex;
                gvMasterRoot.DataBind();
                selectFormDetailLevel2();
                break;

            case "gvMasterSubForm":
                gvMasterSubForm.PageIndex = e.NewPageIndex;
                gvMasterSubForm.DataBind();
                selectRootSubFormDetail();
                break;
        }
    }

    protected void gvRowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var gvName = (GridView)sender;

        switch (gvName.ID)
        {
            case "gvMaster":

                var _updateFormidx = (TextBox)gvMaster.Rows[e.RowIndex].FindControl("_updateFormidx");
                var _updateFormName = (TextBox)gvMaster.Rows[e.RowIndex].FindControl("tbupdateFormName");
                var _updateFormDetail = (TextBox)gvMaster.Rows[e.RowIndex].FindControl("tbupdateFormDetail");
                var _updateFormStatus = (DropDownList)gvMaster.Rows[e.RowIndex].FindControl("ddlupdateFormStatus");
                var _updateSetnameIDX = (DropDownList)gvMaster.Rows[e.RowIndex].FindControl("ddlUpdateSetName");
                var _updateOptionIDX = (DropDownList)gvMaster.Rows[e.RowIndex].FindControl("ddlUpdateOption");

                gvMaster.EditIndex = -1;

                _data_qa.qa_m0_form_detail_list = new qa_m0_form_detail[1];
                qa_m0_form_detail _upForm = new qa_m0_form_detail();
                _upForm.form_detail_idx = int.Parse(_updateFormidx.Text);
                _upForm.form_detail_name = _updateFormName.Text;
                _upForm.form_detail = _updateFormDetail.Text;
                _upForm.form_datail_status = int.Parse(_updateFormStatus.SelectedValue);
                _upForm.set_idx = int.Parse(_updateSetnameIDX.SelectedValue);
                _upForm.option_idx = int.Parse(_updateOptionIDX.SelectedValue);
                _data_qa.qa_m0_form_detail_list[0] = _upForm;

                _data_qa = callServicePostMasterQA(_urlQaSetFormDetail, _data_qa);
                if (_data_qa.return_code == 102)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถแก้ไขข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);
                }

                else
                {

                }

                selectFormDetail();
                SETFOCUS.Focus();
                break;

            case "gvMasterRoot":

                var _updateRootFormidx = (TextBox)gvMasterRoot.Rows[e.RowIndex].FindControl("_updateFormRootidx");
                var _updateRootFormName = (TextBox)gvMasterRoot.Rows[e.RowIndex].FindControl("tbupdateFormRootName");
                var _updateRootFormDetail = (TextBox)gvMasterRoot.Rows[e.RowIndex].FindControl("tbupdateFormRootDetail");
                var _updateFormRootStatus = (DropDownList)gvMasterRoot.Rows[e.RowIndex].FindControl("ddlupdateFormRootStatus");
                var _updateSetNameidx = (DropDownList)gvMasterRoot.Rows[e.RowIndex].FindControl("ddlSetNameUpdate");
                var _updateOptionidx = (DropDownList)gvMasterRoot.Rows[e.RowIndex].FindControl("ddlOptionUpdate");

                gvMasterRoot.EditIndex = -1;

                _data_qa.qa_m0_form_detail_list = new qa_m0_form_detail[1];
                qa_m0_form_detail _upRootset = new qa_m0_form_detail();
                _upRootset.form_detail_idx = int.Parse(_updateRootFormidx.Text);
                _upRootset.form_detail_name = _updateRootFormName.Text;
                _upRootset.form_detail = _updateRootFormDetail.Text;
                _upRootset.form_datail_status = int.Parse(_updateFormRootStatus.SelectedValue);
                _upRootset.set_idx = int.Parse(_updateSetNameidx.SelectedValue);
                _upRootset.option_idx = int.Parse(_updateOptionidx.SelectedValue);
                _data_qa.qa_m0_form_detail_list[0] = _upRootset;


                _data_qa = callServicePostMasterQA(_urlQaSetFormDetail, _data_qa);
                if (_data_qa.return_code == 102)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถแก้ไขข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);
                }

                else
                {

                }
                selectFormDetailLevel2();
                SETFOCUS.Focus();
                break;

            case "gvMasterSubForm":

                var _updateRootSubFormidx = (TextBox)gvMasterSubForm.Rows[e.RowIndex].FindControl("_updateFormSubidx");
                var _updateRootSubFormName = (TextBox)gvMasterSubForm.Rows[e.RowIndex].FindControl("tbupdateFormSubName");
                var _updateRootSubFormDetail = (TextBox)gvMasterSubForm.Rows[e.RowIndex].FindControl("tbupdateFormSubDetail");
                var _updateRootSubFormStatus = (DropDownList)gvMasterSubForm.Rows[e.RowIndex].FindControl("ddlupdateFormSubStatus");

                gvMasterSubForm.EditIndex = -1;

                _data_qa.qa_m0_form_detail_list = new qa_m0_form_detail[1];
                qa_m0_form_detail _upRootSub = new qa_m0_form_detail();
                _upRootSub.form_detail_idx = int.Parse(_updateRootSubFormidx.Text);
                _upRootSub.form_detail_name = _updateRootSubFormName.Text;
                _upRootSub.form_detail = _updateRootSubFormDetail.Text;
                _upRootSub.form_datail_status = int.Parse(_updateRootSubFormStatus.SelectedValue);
                _data_qa.qa_m0_form_detail_list[0] = _upRootSub;

                _data_qa = callServicePostMasterQA(_urlQaSetFormDetail, _data_qa);
                if (_data_qa.return_code == 102)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถแก้ไขข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);
                }
                else
                {

                }
                selectRootSubFormDetail();
                SETFOCUS.Focus();
                break;
        }



    }

    protected void gvRowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {

        var gvName = (GridView)sender;

        switch (gvName.ID)
        {
            case "gvMaster":

                gvMaster.EditIndex = -1;
                selectFormDetail();
                SETFOCUS.Focus();
                break;

            case "gvMasterRoot":
                gvMasterRoot.EditIndex = -1;
                selectFormDetailLevel2();
                SETFOCUS.Focus();
                break;

            case "gvMasterSubForm":
                gvMasterSubForm.EditIndex = -1;
                selectRootSubFormDetail();
                SETFOCUS.Focus();
                break;

        }
    }

    protected void gvRowEditing(object sender, GridViewEditEventArgs e)
    {
        var gvName = (GridView)sender;

        switch (gvName.ID)
        {
            case "gvMaster":
                gvMaster.EditIndex = e.NewEditIndex;
                selectFormDetail();
                break;

            case "gvMasterRoot":
                gvMasterRoot.EditIndex = e.NewEditIndex;
                selectFormDetailLevel2();
                break;

            case "gvMasterSubForm":
                gvMasterSubForm.EditIndex = e.NewEditIndex;
                selectRootSubFormDetail();
                break;
        }
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void setActiveView(string activeTab, int uidx)
    {
        mvMaster.SetActiveView((View)mvMaster.FindControl(activeTab));

        switch (activeTab)
        {
            case "pageGenaral":
                break;

            case "pageCreateRoot":
                getM0RootFormList();

                break;

            case "pageRootSubSet":
                getM0RootFormSubList();

                break;
        }
    }




    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }

    protected data_qa callServicePostMasterQA(string _cmdUrl, data_qa _data_qa)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_qa);
        // litDebug.Text = _localJson;

        // call services
        // _localJson = _funcTool.callServicePost(_cmdUrl + _localJson);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_qa = (data_qa)_funcTool.convertJsonToObject(typeof(data_qa), _localJson);

        return _data_qa;
    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger1 = new PostBackTrigger();
        trigger1.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger1);
    }
    #endregion


}