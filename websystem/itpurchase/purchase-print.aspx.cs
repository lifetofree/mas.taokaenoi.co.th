﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;

public partial class websystem_itpurchase_purchase_print : System.Web.UI.Page
{

    function_tool _funcTool = new function_tool();
    data_purchase _data_purchase = new data_purchase();

    string _localJson = "";
    int _tempInt = 0;
    string _mail_subject = "";
    string _mail_body = "";
    int _tempcounter;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetitems_purchase = _serviceUrl + ConfigurationManager.AppSettings["urlGetitems_purchase"];
    static string _urlGetreportpurchaselist = _serviceUrl + ConfigurationManager.AppSettings["urlGetreportpurchaselist"];
    static string _urlgetionumber = _serviceUrl + ConfigurationManager.AppSettings["urlgetionumber"];
    static string _urlgetassetnumber = _serviceUrl + ConfigurationManager.AppSettings["urlgetassetnumber"];


    protected void Page_Load(object sender, EventArgs e)
    {


        //if (!IsPostBack)
        //{


            // -------------- รับค่าจาก ไฟล์ it_purchase.aspx -------------//

            if (Session["_REPROTSESION_EMPCODE"].ToString() != null || Session["_REPROTSESION_DOCUMENTCODE"].ToString() != null
                || Session["_REPROTSESION_FULLNAME"].ToString() != null || Session["_REPROTSESION_ORGIDX"].ToString() != null
                || Session["_REPROTSESION_RDEPTIDX"].ToString() != null || Session["_REPROTSESION_LOCATION"].ToString() != null
                || Session["_REPROTSESION_COSTCENTER"].ToString() != null || Session["_REPROTSESION_PURCHASEIDX"].ToString() != null
                || Session["_REPROTSESION_EQUIPMENTIDX"].ToString() != null || Session["_REPROTSESION_STATUSPURCHASE"].ToString() != null
                || Session["_REPROTSESION_CONDITION"].ToString() != null || Session["_REPROTSESION_STARTDATE"].ToString() != null
                || Session["_REPROTSESION_ENDDATE"].ToString() != null)
            {

                ViewState["_PRINTPURCHASE_EMPCODE"] = Session["_REPROTSESION_EMPCODE"].ToString();
                ViewState["_PRINTPURCHASE_DOCUMENTCODE"] = Session["_REPROTSESION_DOCUMENTCODE"].ToString();
                ViewState["_PRINTPURCHASE_FULLNAME"] = Session["_REPROTSESION_FULLNAME"].ToString();
                ViewState["_PRINTPURCHASE_ORGANIZATION"] = Session["_REPROTSESION_ORGIDX"].ToString();
                ViewState["_PRINTPURCHASE_DEPARTMENT"] = Session["_REPROTSESION_RDEPTIDX"].ToString();
                ViewState["_PRINTPURCHASE_LOCATION"] = Session["_REPROTSESION_LOCATION"].ToString();
                ViewState["_PRINTPURCHASE_COSTCENTER"] = Session["_REPROTSESION_COSTCENTER"].ToString();
                ViewState["_PRINTPURCHASE_PURCHASETYPE"] = Session["_REPROTSESION_PURCHASEIDX"].ToString();
                ViewState["_PRINTPURCHASE_EQUIPMENTTYPE"] = Session["_REPROTSESION_EQUIPMENTIDX"].ToString();
                ViewState["_PRINTPURCHASE_STATUSPURCHAE"] = Session["_REPROTSESION_STATUSPURCHASE"].ToString();
                ViewState["_PRINTPURCHASE_CONDITION"] = Session["_REPROTSESION_CONDITION"].ToString();
                ViewState["_PRINTPURCHASE_STARTDATE"] = Session["_REPROTSESION_STARTDATE"].ToString();
                ViewState["_PRINTPURCHASE_ENDDATE"] = Session["_REPROTSESION_ENDDATE"].ToString();


                PRINT_REPORTPURCHASE();
                _GETPRINTDATE();


            }

            else

            {



            }


        //}


        gvPintReport.UseAccessibleHeader = true;
        gvPintReport.HeaderRow.TableSection = TableRowSection.TableHeader;


    }


    private void PRINT_REPORTPURCHASE()
    {

        data_purchase _print_purchase = new data_purchase();

        _print_purchase.report_purchase_list = new report_purchase_details[1];


        report_purchase_details _PRINTPURCHASE = new report_purchase_details();

        _PRINTPURCHASE.condition_date_type = int.Parse(ViewState["_PRINTPURCHASE_CONDITION"].ToString());
        _PRINTPURCHASE.start_date = ViewState["_PRINTPURCHASE_STARTDATE"].ToString();
        _PRINTPURCHASE.end_date = ViewState["_PRINTPURCHASE_ENDDATE"].ToString();

        _PRINTPURCHASE.document_code = ViewState["_PRINTPURCHASE_DOCUMENTCODE"].ToString();
        _PRINTPURCHASE.fullname_purchase = ViewState["_PRINTPURCHASE_FULLNAME"].ToString();
        _PRINTPURCHASE.empcode_purchase = ViewState["_PRINTPURCHASE_EMPCODE"].ToString();
        _PRINTPURCHASE.org_idx_purchase = int.Parse(ViewState["_PRINTPURCHASE_ORGANIZATION"].ToString());
        _PRINTPURCHASE.dept_idx_purchase = int.Parse(ViewState["_PRINTPURCHASE_DEPARTMENT"].ToString());
        _PRINTPURCHASE.m0_location_idx = int.Parse(ViewState["_PRINTPURCHASE_LOCATION"].ToString());
        _PRINTPURCHASE.costcenter_idx = int.Parse(ViewState["_PRINTPURCHASE_COSTCENTER"].ToString());
        _PRINTPURCHASE.m0_purchase_idx = int.Parse(ViewState["_PRINTPURCHASE_PURCHASETYPE"].ToString());
        _PRINTPURCHASE.m0_equipment_idx = int.Parse(ViewState["_PRINTPURCHASE_EQUIPMENTTYPE"].ToString());
        _PRINTPURCHASE.status_purchase = int.Parse(ViewState["_PRINTPURCHASE_STATUSPURCHAE"].ToString());


        _print_purchase.report_purchase_list[0] = _PRINTPURCHASE;

        // check_XML.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_report_purchase));


        _print_purchase = callServicePurchase(_urlGetreportpurchaselist, _print_purchase);



        gvPintReport.DataSource = _print_purchase.report_purchase_list;
        gvPintReport.DataBind();



    }


    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var gridViewName = (GridView)sender;
      
        switch (gridViewName.ID)
        {
            case "gvPintReport":
               
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                   

                    _tempcounter = _tempcounter + 1;

                    if (_tempcounter == 2)
                    {
                        e.Row.Attributes.Add("style", "page-break-after: always;");
                       // e.Row.Attributes.Add("style", "height:50px;");
                        _tempcounter = 0;
                        

                    }


                    Label u0_purchase_report = (Label)e.Row.Cells[0].FindControl("u0_purchase_report");
                    Label status_purchasereport = (Label)e.Row.Cells[3].FindControl("status_purchasereport");
                    Label idx_status = (Label)e.Row.Cells[3].FindControl("idx_status");

                    GridView gvReportitems = (GridView)e.Row.Cells[2].FindControl("gvReportitems");


                    data_purchase _reportItemspurchase = new data_purchase();
                    _reportItemspurchase.items_purchase_list = new items_purchase_details[1];


                    items_purchase_details _reportingItems = new items_purchase_details();

                    _reportingItems.u0_purchase_idx = int.Parse(u0_purchase_report.Text);



                    _reportItemspurchase.items_purchase_list[0] = _reportingItems;

                    _reportItemspurchase = callServicePurchase(_urlGetitems_purchase, _reportItemspurchase);

                    ViewState["_SESSION_PRINTREPORT"] = _reportItemspurchase.items_purchase_list;

                    gvReportitems.DataSource = ViewState["_SESSION_PRINTREPORT"];//_reportItemspurchase.items_purchase_list;
                    gvReportitems.DataBind();


                    foreach (GridViewRow row_report in gvReportitems.Rows)
                    {
                        var _lbu2idxreport = (Label)row_report.FindControl("_lbu2idxreport");
                        GridView _gvionumber_report = (GridView)row_report.FindControl("gvionumber_report");
                        GridView _gvassetnumber_report = (GridView)row_report.FindControl("gvassetnumber_report");

                        //:::: start select number io::://
                        data_purchase _data_ioreport = new data_purchase();
                        _data_ioreport.items_purchase_list = new items_purchase_details[1];
                        items_purchase_details _selectnumberio = new items_purchase_details();
                        _selectnumberio.u2_purchase_idx = int.Parse(_lbu2idxreport.Text);
                        _data_ioreport.items_purchase_list[0] = _selectnumberio;
                        _data_ioreport = callServicePurchase(_urlgetionumber, _data_ioreport);

                        _gvionumber_report.DataSource = _data_ioreport.numberio_list;
                        _gvionumber_report.DataBind();
                        //:::: end select number io::://


                        // :::: start select number asset ::://
                        data_purchase _data_assetreport = new data_purchase();
                        _data_assetreport.items_purchase_list = new items_purchase_details[1];
                        items_purchase_details _selectnumberasset = new items_purchase_details();
                        _selectnumberasset.u2_purchase_idx = int.Parse(_lbu2idxreport.Text);
                        _data_assetreport.items_purchase_list[0] = _selectnumberasset;
                        _data_assetreport = callServicePurchase(_urlgetassetnumber, _data_assetreport);

                        _gvassetnumber_report.DataSource = _data_assetreport.numberAsset_list;
                        _gvassetnumber_report.DataBind();

                        // :::: end select number asset::://



                    }

                    }

             
                break;

        }
    }


    #region call data
    protected void _GETPRINTDATE()
    {

       // grdUserList.DataBind();

        lblPrintDate.Text = DateTime.Now.ToString();

    }

    #endregion


    #region call service
    protected data_purchase callServicePurchase(string _cmdUrl, data_purchase _data_purchase)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_purchase);
        // litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);

        // convert json to object
        _data_purchase = (data_purchase)_funcTool.convertJsonToObject(typeof(data_purchase), _localJson);

        return _data_purchase;
    }

    #endregion


    public override void VerifyRenderingInServerForm(Control control)
    {
        /*Verifies that the control is rendered */
    }


}