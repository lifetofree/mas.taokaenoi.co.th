﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="employee_holidays.aspx.cs" Inherits="websystem_hr_employee_holidays" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:HyperLink ID="setOntop" runat="server"></asp:HyperLink>

    <!--tab menu-->
    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default" id="divBtnHead" runat="server" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-bar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="sub-navbar">
                        <a class="navbar-brand" href="#"><b>Menu</b></a>
                    </div>
                </div>

                <!--Collect the nav links, forms, and other content for toggling-->
                <div class="collapse navbar-collapse" id="menu-bar">
                    <ul class="nav navbar-nav" id="uiNav" runat="server">
                        <li id="li0" runat="server">
                            <asp:LinkButton ID="lbDetailHoliday" runat="server" CommandName="cmdDetailHoliday" OnCommand="navCommand" CommandArgument="docDetailHoliday"> ปฏิทินวันหยุด</asp:LinkButton>
                        </li>

                        <li id="li1" runat="server">
                            <asp:LinkButton ID="lbDetail" runat="server" CommandName="cmdDetail" OnCommand="navCommand" CommandArgument="docDetail"> รายการวันหยุด</asp:LinkButton>
                        </li>
                        <li id="li2" runat="server">
                            <asp:LinkButton ID="lbCreate" runat="server" CommandName="cmdCreate" OnCommand="navCommand" CommandArgument="docCreate"> สร้างรายการวันหยุด</asp:LinkButton>
                        </li>

                        <li id="li4" runat="server">
                            <asp:LinkButton ID="lblOrg" runat="server" CommandName="cmdDetail" OnCommand="navCommand" CommandArgument="docDetailOrg"> วันหยุดของแต่ละบริษัท</asp:LinkButton>
                        </li>

                        <li id="li3" runat="server">
                            <asp:LinkButton ID="lbManageHoliday" runat="server" CommandName="cmdCreate" OnCommand="navCommand" CommandArgument="docManageHoliday"> จัดการวันหยุดของบริษัท</asp:LinkButton>
                        </li>

                    </ul>

                </div>
                <!--Collect the nav links, forms, and other content for toggling-->

            </div>
        </nav>
    </div>
    <!--tab menu-->


    <div class="col-md-12">
        <asp:Literal ID="litDebug" runat="server"></asp:Literal>
        <asp:Literal ID="litDebug1" runat="server"></asp:Literal>
    </div>

    <!--multiview-->
    <asp:MultiView ID="mvSystem" runat="server">

        <!--View Detail Holiday-->
        <asp:View ID="docDetailHoliday" runat="server">
            <div class="col-md-12">

                <asp:UpdatePanel ID="upMain_HeadHoliday" runat="server" Visible="true">
                    <ContentTemplate>

                        <div class="row">
                            <%--<div class="col-md-10 col-md-offset-1">--%>
                            <div class="col-md-12">
                                <%--<div class="row col-md-12">--%>
                                <%--<div class="col-md-12" style="margin-top: -10px; padding: 0px;">--%>
                                <asp:Literal ID="litPanelTitleCalendar" runat="server" Text="Title"></asp:Literal>

                            </div>

                            <%-- </div>--%>
                            <%-- </div>--%>
                        </div>

                    </ContentTemplate>
                </asp:UpdatePanel>
                <%-- <br />--%>

                <div class="panel panel-primary" id="CalendarHoliday" runat="server" visible="false">
                    <div class="panel-heading" id="div_header" runat="server" style="height: 50px">
                        <h1 class="panel-title" style="font-size: 24px; text-align: center;">ปฏิทินวันหยุดประจำปี</h1>
                    </div>
                </div>
                <%-- <br />--%> <%--src='<%= ResolveUrl("~/images/carousel/reset-password-03.jpg") %>'--%>
                <div class="col-md-12">
                    <div class="panel-body" style="background-color: transparent; background-image: url('masterpage/images/holiday-header/background-calendar-1.jpg'); background-size: 100% 100%" id="pn_bgCalendar" runat="server">
                        <%-- <div class="panel-body">--%>
                        <br />
                        <div class="col-md-10 col-md-offset-1">

                            <div class="col-md-12">
                                <div class="form-group">
                                    <div id="calendar">
                                        <div id="fullCalModal" class="modal fade">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">close</span></button>
                                                        <h4 id="modalTitle" class="modal-title"></h4>
                                                    </div>
                                                    <div id="modalBody" class="modal-body">uteueuetuteu</div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                        <%-- </div>--%>
                    </div>
                </div>
            </div>
        </asp:View>
        <!--View Detail Holiday -->

        <!--View Detail Holiday Org-->
        <asp:View ID="docDetailOrg" runat="server">
            <div class="col-md-12">

                <!-- Back To Detail To Index -->
                <asp:UpdatePanel ID="Div_BackToDetail" runat="server">
                    <ContentTemplate>
                        <div class="form-group">
                            <asp:LinkButton ID="btnBackToDetail" CssClass="btn btn-danger" data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                                CommandName="cmdBackToDetail" OnCommand="btnCommand">< ย้อนกลับ</asp:LinkButton>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- Back To Detail To Index -->

                <asp:GridView ID="GvDetailHolidayOrg" runat="server"
                    AutoGenerateColumns="False"
                    CssClass="table table-striped table-bordered table-hover table-responsive"
                    OnPageIndexChanging="gvPageIndexChanging"
                    OnRowDataBound="gvRowDataBound"
                    OnRowEditing="gvRowEditing"
                    OnRowCancelingEdit="gvRowCancelingEdit"
                    OnRowUpdating="gvRowUpdating"
                    ShowHeaderWhenEmpty="True"
                    AllowPaging="True" PageSize="10">
                    <HeaderStyle CssClass="success" Height="40px" Font-Size="Small" />
                    <RowStyle Font-Size="Small" />
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">--- ไม่พบข้อมูล ---</div>
                    </EmptyDataTemplate>
                    <Columns>

                        <asp:TemplateField HeaderText="#" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="lbl_u0_manage_idx_detailorg" Visible="false" runat="server" CssClass="col-sm-12" Text='<%# Eval("u0_manage_idx") %>'></asp:Label>
                                    <%# (Container.DataItemIndex + 1) %>
                                </small>

                            </ItemTemplate>

                            <EditItemTemplate>

                                <%--<div class="panel-body">

                                    <div class="col-md-8 col-md-offset-2">

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>ชื่อวันหยุด</label>
                                                <asp:TextBox ID="txt_holiday_idx_edit" Visible="false" placeholder="ชื่อวันหยุด ..." runat="server" Text='<%# Eval("holiday_idx") %>' CssClass="form-control" />
                                                <asp:TextBox ID="txt_name_holidat" placeholder="ชื่อวันหยุด ..." runat="server" Text='<%# Eval("holiday_name") %>' CssClass="form-control" />

                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>เลือกวันหยุด</label>
                                                <div class="input-group date">
                                                    <asp:TextBox ID="txt_date_holiday" placeholder="เลือกวันหยุด ..." Text='<%# Eval("holiday_date") %>' runat="server" CssClass="form-control from-date-datepicker" />

                                                    <span class="input-group-addon show-from-onclick">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="pull-right">
                                                    <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" ValidationGroup="Editpname" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                        Text="แก้ไข" CommandName="Update"></asp:LinkButton>
                                                    <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                        Text="ยกเลิก" CommandName="Cancel"></asp:LinkButton>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>--%>
                            </EditItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="บริษัท" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="Left">
                            <ItemTemplate>

                                <asp:Label ID="lbl_org_name_th_detailorg" Visible="true" runat="server" CssClass="col-sm-12" Text='<%# Eval("org_name_th") %>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>



                        <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center" ItemStyle-Width="10%">
                            <ItemTemplate>

                                <%--<asp:LinkButton ID="Edit" CssClass="btn btn-warning btn-xs" runat="server" CommandName="Edit" data-toggle="tooltip" OnCommand="btnCommand" title="Edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>--%>

                                <asp:LinkButton ID="btnViewHolidayDetailOrg" CssClass="btn btn-info btn-xs" target="" runat="server"
                                    CommandName="cmdViewHolidayDetailOrg" OnCommand="btnCommand"
                                    CommandArgument='<%# Eval("u0_manage_idx")+ ";" + Eval("org_idx")%>' data-toggle="tooltip" title="View">
                                    <i class="glyphicon glyphicon-file"></i></asp:LinkButton>

                                <%--<asp:LinkButton ID="btnDeleteHolidayDetailOrg" CssClass="btn btn-danger btn-xs" target="" runat="server"
                                    CommandName="cmdDeleteHolidayDetailOrg" OnCommand="btnCommand"
                                    CommandArgument='<%# Eval("u0_manage_idx")%>' OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')"
                                    data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>--%>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>


                 <asp:UpdatePanel ID="Update_DetailHolidayOrgName" runat="server">
                    <ContentTemplate>
                        <div class="panel panel-info" id="_DetailHolidayOrgName" runat="server">
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                     <asp:Label ID="lbl_DetailHolidayOrgName" runat="server"></asp:Label>
                                </h3>
                               
                              <%--  <h3 class="panel-title">รายการขอ OT รายวัน</h3>--%>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>


                <asp:UpdatePanel ID="Panel_ViewDetailHolidayOrg" runat="server">
                    <ContentTemplate>
                        <div id="Div_ViewDetailHolidayOrg" class="panel panel-primary" runat="server">
                            <div class="panel-body">
                                <div class="col-md-10 col-md-offset-1">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label>เลือกปี</label>
                                            <asp:Label ID="lbl_ViewDetailHolidayOrg_u0idx" Visible="false" runat="server"></asp:Label>
                                            <asp:Label ID="lbl_ViewDetailHolidayOrg_orgidx" Visible="false" runat="server"></asp:Label>
                                            <asp:DropDownList ID="ddlYearViewDetailHolidayOrg" runat="server" CssClass="form-control" 
                                                OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" />
                                            <%-- <asp:RequiredFieldValidator ID="RequiredFieldDdl_Permanagement"
                                                runat="server"
                                                InitialValue="0"
                                                ControlToValidate="ddl_Permanagement" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกสิทธิ์"
                                                ValidationGroup="addPermanageList" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender22" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldDdl_Permanagement" Width="250" />--%>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>&nbsp;</label>
                                            <div class="clearfix"></div>
                                            <asp:LinkButton ID="btnRefreshViewDetailHolidayOrg" runat="server" CssClass="btn btn-default col-md-12" data-original-title="Refresh" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdRefreshViewDetailHolidayOrg"><span class="glyphicon glyphicon-refresh"></span> ล้างค่า</asp:LinkButton>
                                        </div>
                                    </div>

                                    <div class="col-md-2" id="Div_ExportViewDetailHolidayOrg" runat="server">
                                        <div class="form-group">
                                            <label>&nbsp;</label>
                                            <div class="clearfix"></div>
                                            <asp:LinkButton ID="btnExportViewDetailHolidayOrg" runat="server" CssClass="btn btn-success col-md-12" data-original-title="Export" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdExportViewDetailHolidayOrg"><span class="glyphicon glyphicon-export"></span> Export</asp:LinkButton>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </ContentTemplate>

                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnExportViewDetailHolidayOrg" />
                    </Triggers>
                </asp:UpdatePanel>

                <asp:GridView ID="GvViewDetailHolidayOrg" runat="server"
                    AutoGenerateColumns="False"
                    CssClass="table table-striped table-bordered table-hover table-responsive"
                    OnPageIndexChanging="gvPageIndexChanging"
                    OnRowDataBound="gvRowDataBound"
                    OnRowEditing="gvRowEditing"
                    OnRowCancelingEdit="gvRowCancelingEdit"
                    OnRowUpdating="gvRowUpdating"
                    ShowHeaderWhenEmpty="True"
                    AllowPaging="True" PageSize="10">
                    <HeaderStyle CssClass="success" Height="40px" Font-Size="Small" />
                    <RowStyle Font-Size="Small" />
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">--- ไม่พบข้อมูล ---</div>
                    </EmptyDataTemplate>
                    <Columns>

                        <asp:TemplateField HeaderText="#" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="lbl_u0_manage_idx_viewdetailorg" Visible="false" runat="server" CssClass="col-sm-12" Text='<%# Eval("u0_manage_idx") %>'></asp:Label>
                                    <asp:Label ID="lbl_org_idx_viewdetailorg" Visible="false" runat="server" CssClass="col-sm-12" Text='<%# Eval("org_idx") %>'></asp:Label>
                                    <asp:Label ID="lbl_u1_manage_idx_viewdetailorg" Visible="false" runat="server" CssClass="col-sm-12" Text='<%# Eval("u1_manage_idx") %>'></asp:Label>
                                    <%# (Container.DataItemIndex + 1) %>
                                </small>

                            </ItemTemplate>

                            <EditItemTemplate>

                                <%--<div class="panel-body">

                                    <div class="col-md-8 col-md-offset-2">

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>ชื่อวันหยุด</label>
                                                <asp:TextBox ID="txt_holiday_idx_edit" Visible="false" placeholder="ชื่อวันหยุด ..." runat="server" Text='<%# Eval("holiday_idx") %>' CssClass="form-control" />
                                                <asp:TextBox ID="txt_name_holidat" placeholder="ชื่อวันหยุด ..." runat="server" Text='<%# Eval("holiday_name") %>' CssClass="form-control" />

                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>เลือกวันหยุด</label>
                                                <div class="input-group date">
                                                    <asp:TextBox ID="txt_date_holiday" placeholder="เลือกวันหยุด ..." Text='<%# Eval("holiday_date") %>' runat="server" CssClass="form-control from-date-datepicker" />

                                                    <span class="input-group-addon show-from-onclick">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="pull-right">
                                                    <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" ValidationGroup="Editpname" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                        Text="แก้ไข" CommandName="Update"></asp:LinkButton>
                                                    <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                        Text="ยกเลิก" CommandName="Cancel"></asp:LinkButton>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>--%>
                            </EditItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="วันที่หยุด" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="Left">
                            <ItemTemplate>
                                <asp:Label ID="lbl_holiday_date_viewdetailorg" Visible="true" runat="server" CssClass="col-sm-12" Text='<%# Eval("holiday_date") %>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ชื่อวันหยุด" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="Left">
                            <ItemTemplate>
                                <asp:Label ID="lbl_holiday_name_viewdetailorg" Visible="true" runat="server" CssClass="col-sm-12" Text='<%# Eval("holiday_name") %>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>



                        <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center" ItemStyle-Width="10%">
                            <ItemTemplate>

                                <%--<asp:LinkButton ID="Edit" CssClass="btn btn-warning btn-xs" runat="server" CommandName="Edit" data-toggle="tooltip" OnCommand="btnCommand" title="Edit"><i class="glyphicon glyphicon-edit"></i></asp:LinkButton>--%>

                                <asp:LinkButton ID="btnDeleteViewHolidayDetailOrg" CssClass="btn btn-danger btn-xs" target="" runat="server"
                                    CommandName="cmdDeleteViewHolidayDetailOrg" OnCommand="btnCommand"
                                    CommandArgument='<%# Eval("u1_manage_idx")+ ";" + Eval("org_idx")+ ";" + Eval("u0_manage_idx")%>' OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')"
                                    data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>

                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>



            </div>
        </asp:View>
        <!--View Detail Holiday Org-->

        <!--View Detail -->
        <asp:View ID="docDetail" runat="server">
            <div class="col-md-12">

                <asp:UpdatePanel ID="updatePanelInsertPer" runat="server">
                    <ContentTemplate>
                        <div id="panelSearchHoliday" class="panel panel-primary" runat="server">
                            <div class="panel-body">
                                <div class="col-md-10 col-md-offset-1">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label>เลือกปี</label>
                                            <asp:DropDownList ID="ddlyear" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" />
                                            <%-- <asp:RequiredFieldValidator ID="RequiredFieldDdl_Permanagement"
                                                runat="server"
                                                InitialValue="0"
                                                ControlToValidate="ddl_Permanagement" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกสิทธิ์"
                                                ValidationGroup="addPermanageList" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender22" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldDdl_Permanagement" Width="250" />--%>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>&nbsp;</label>
                                            <div class="clearfix"></div>
                                            <asp:LinkButton ID="btnRefresh" runat="server" CssClass="btn btn-default col-md-12" data-original-title="Refresh" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdRefresh"><span class="glyphicon glyphicon-refresh"></span> ล้างค่า</asp:LinkButton>
                                        </div>
                                    </div>

                                    <div class="col-md-2" id="div_Export" runat="server" visible="false">
                                        <div class="form-group">
                                            <label>&nbsp;</label>
                                            <div class="clearfix"></div>
                                            <asp:LinkButton ID="btnExport" runat="server" CssClass="btn btn-success col-md-12" data-original-title="Export" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdExport"><span class="glyphicon glyphicon-export"></span> Export</asp:LinkButton>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </ContentTemplate>

                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnExport" />
                    </Triggers>
                </asp:UpdatePanel>

                <%--  <div id="div_GvCarDetail" style="overflow-x: scroll; width: 100%" runat="server">--%>
                <asp:GridView ID="GvDetailHoliday" runat="server"
                    AutoGenerateColumns="False"
                    CssClass="table table-striped table-bordered table-hover table-responsive"
                    OnPageIndexChanging="gvPageIndexChanging"
                    OnRowDataBound="gvRowDataBound"
                    OnRowEditing="gvRowEditing"
                    OnRowCancelingEdit="gvRowCancelingEdit"
                    OnRowUpdating="gvRowUpdating"
                    ShowHeaderWhenEmpty="True"
                    AllowPaging="True" PageSize="10">
                    <HeaderStyle CssClass="success" Height="40px" Font-Size="Small" />
                    <RowStyle Font-Size="Small" />
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">--- ไม่พบข้อมูล ---</div>
                    </EmptyDataTemplate>
                    <Columns>

                        <asp:TemplateField HeaderText="#" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center" ItemStyle-Width="10%">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="lbl_holiday_idx_detail" Visible="false" runat="server" CssClass="col-sm-12" Text='<%# Eval("holiday_idx") %>'></asp:Label>
                                    <%# (Container.DataItemIndex + 1) %>
                                </small>

                            </ItemTemplate>

                            <EditItemTemplate>

                                <div class="panel-body">
                                    <%--<div class="form-horizontal" role="form">--%>

                                    <div class="col-md-8 col-md-offset-2">

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>ชื่อวันหยุด</label>
                                                <asp:TextBox ID="txt_holiday_idx_edit" Visible="false" placeholder="ชื่อวันหยุด ..." runat="server" Text='<%# Eval("holiday_idx") %>' CssClass="form-control" />
                                                <asp:TextBox ID="txt_name_holidat" placeholder="ชื่อวันหยุด ..." runat="server" Text='<%# Eval("holiday_name") %>' CssClass="form-control" />

                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>เลือกวันหยุด</label>
                                                <div class="input-group date">
                                                    <asp:TextBox ID="txt_date_holiday" placeholder="เลือกวันหยุด ..." Text='<%# Eval("holiday_date") %>' runat="server" CssClass="form-control from-date-datepicker" />

                                                    <span class="input-group-addon show-from-onclick">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="pull-right">
                                                    <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" ValidationGroup="Editpname" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                        Text="แก้ไข" CommandName="Update"></asp:LinkButton>
                                                    <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                        Text="ยกเลิก" CommandName="Cancel"></asp:LinkButton>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>

                            </EditItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="วันที่หยุด" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center" ItemStyle-Width="10%">
                            <ItemTemplate>
                                <asp:Label ID="lbl_holiday_date_detail" Visible="true" runat="server" CssClass="col-sm-12" Text='<%# Eval("holiday_date") %>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ชื่อวันหยุด" HeaderStyle-CssClass="text-center" ItemStyle-Width="35%" ItemStyle-CssClass="text-left">

                            <ItemTemplate>
                                <asp:Label ID="lbl_holiday_name_detail" Visible="true" runat="server" CssClass="col-sm-12" Text='<%# Eval("holiday_name") %>'></asp:Label>
                                <%-- <p>
                                    <b>ประเภทรถ:</b>
                                    <asp:Label ID="lbl_m0_car_idx_detail" Visible="false" runat="server" Text='<%# Eval("m0_car_idx") %>' />
                                    <asp:Label ID="lbl_type_car_idx_detail" Visible="false" runat="server" Text='<%# Eval("type_car_idx") %>' />
                                    <asp:Label ID="lbl_type_car_name_detail" runat="server" Text='<%# Eval("type_car_name") %>' />
                                </p>
                                <p>
                                    <b>รายละเอียดประเภทรถ:</b>
                                    <asp:Label ID="lbl_detailtype_car_idx_detail" Visible="false" runat="server" Text='<%# Eval("detailtype_car_idx") %>' />
                                    <asp:Label ID="lbl_detailtype_car_name_detail" runat="server" Text='<%# Eval("detailtype_car_name") %>' />
                                </p>

                                <p>
                                    <b>ทะเบียนรถ:</b>
                                    <asp:Label ID="lbl_car_register_detail" runat="server" Text='<%# Eval("car_register") %>' />
                                </p>
                                <p>
                                    <b>จังหวัด:</b>
                                    <asp:Label ID="lbl_car_province_idx_detail" Visible="false" runat="server" Text='<%# Eval("car_province_idx") %>' />
                                    <asp:Label ID="lbl_ProvName_detail" runat="server" Text='<%# Eval("ProvName") %>' />
                                </p>

                                <p>
                                    <b>วันครบกำหนดเสียภาษี:</b>
                                    <asp:Label ID="lbl_m1_car_idx_detail" Visible="false" runat="server" Text='<%# Eval("m1_car_idx") %>' />
                                    <asp:Label ID="lbl_tax_deadline_detail" runat="server" Text='<%# Eval("tax_deadline") %>' />
                                </p>
                                <p>
                                    <b>ค่าภาษี บาท/สต.:</b>
                                    <asp:Label ID="lbl_price_tax_detail" runat="server" Text='<%# Eval("price_tax") %>' />
                                </p>

                                <p>
                                    <b>วันครบกำหนดประกันภัยคุ้มครองผู้ประสบภัยจากรถ:</b>
                                    <asp:Label ID="lbl_date_insurance_term_detail" runat="server" Text='<%# Eval("date_insurance_term") %>' />
                                </p>
                                <p>
                                    <b>วันครบกำหนดประกันภัยรถยนต์:</b>
                                    <asp:Label ID="lbl_date_car_insurance_deadline_detail" runat="server" Text='<%# Eval("date_car_insurance_deadline") %>' />
                                </p>--%>
                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center" ItemStyle-Width="10%">
                            <ItemTemplate>

                                <asp:LinkButton ID="Edit" CssClass="btn btn-warning btn-xs" runat="server" CommandName="Edit" data-toggle="tooltip" OnCommand="btnCommand" title="Edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>

                                <asp:LinkButton ID="btnDeleteHolidayDetail" CssClass="btn btn-danger btn-xs" target="" runat="server" CommandName="cmdDeleteHolidayDetail" OnCommand="btnCommand"
                                    CommandArgument='<%# Eval("holiday_idx")%>' OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')"
                                    data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>

                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>
                <%--</div>--%>
            </div>
        </asp:View>
        <!--View Detail -->

        <!--View Create -->
        <asp:View ID="docCreate" runat="server">
            <div class="col-md-12">

                <!-- Form Detail Employee Create -->
                <asp:FormView ID="fvEmpDetail" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <ItemTemplate>

                        <asp:HiddenField ID="hfEmpOrgIDX" runat="server" Value='<%# Eval("org_idx") %>' />
                        <asp:HiddenField ID="hfEmpRdeptIDX" runat="server" Value='<%# Eval("rdept_idx") %>' />
                        <asp:HiddenField ID="hfEmpRsecIDX" runat="server" Value='<%# Eval("rsec_idx") %>' />
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายละเอียดผู้ใช้งาน</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">รหัสพนักงาน</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpCode" runat="server" CssClass="form-control" Text='<%# Eval("emp_code") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ชื่อ - นามสกุล</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpName" runat="server" CssClass="form-control" Text='<%# Eval("emp_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">องค์กร</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpOrg" runat="server" CssClass="form-control" Text='<%# Eval("org_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ฝ่าย</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpDept" runat="server" CssClass="form-control" Text='<%# Eval("dept_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">แผนก</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpSec" runat="server" CssClass="form-control" Text='<%# Eval("sec_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ตำแหน่ง</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpPos" runat="server" CssClass="form-control" Text='<%# Eval("pos_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </ItemTemplate>
                </asp:FormView>
                <!-- Form Detail Employee Create -->

                <!-- Form Create Holiday -->
                <asp:UpdatePanel ID="Panel_FormCreateHoliday" runat="server" UpdateMode="Always">
                    <ContentTemplate>
                        <asp:FormView ID="fvCreateHoliday" runat="server" DefaultMode="ReadOnly" Width="100%">
                            <InsertItemTemplate>

                                <div class="row">

                                    <div class="panel panel-info">
                                        <div class="panel-heading f-bold">สร้างรายการวันหยุด</div>

                                        <div class="panel-body">

                                            <div class="col-md-8 col-md-offset-2">

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>ชื่อวันหยุด</label>
                                                        <asp:TextBox ID="txt_name_holidat" placeholder="ชื่อวันหยุด ..." runat="server" CssClass="form-control" />

                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>เลือกวันหยุด</label>
                                                        <div class="input-group date">
                                                            <asp:TextBox ID="txt_date_holiday" placeholder="เลือกวันหยุด ..." runat="server" CssClass="form-control from-date-datepicker" />

                                                            <span class="input-group-addon show-from-onclick">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="pull-right">
                                                            <asp:LinkButton ID="btnSaveHoliday" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" OnCommand="btnCommand"
                                                                CommandName="CmdSaveHoliday" Text="บันทึก" ValidationGroup="SaveTax" />
                                                            <%-- <asp:LinkButton ID="btnSave" CssClass="btn btn-success" runat="server" OnCommand="btnCommand"
                                                                CommandName="CmdSave" Text="บันทึก" ValidationGroup="SaveDetailCar"
                                                                OnClientClick="return confirm('ยืนยันการบันทึก')" />--%>
                                                            <asp:LinkButton CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" OnCommand="btnCommand"
                                                                CommandName="CmdCancel" Text="ยกเลิก" />
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </InsertItemTemplate>

                        </asp:FormView>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- Form Create Holiday -->

            </div>
        </asp:View>
        <!--View Create -->

        <!--View Manage Holiday Org -->
        <asp:View ID="docManageHoliday" runat="server">
            <div class="col-md-12">
                <!-- Form Detail Employee Create -->
                <asp:FormView ID="fvEmpDetailManage" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <ItemTemplate>

                        <asp:HiddenField ID="hfEmpOrgIDX" runat="server" Value='<%# Eval("org_idx") %>' />
                        <asp:HiddenField ID="hfEmpRdeptIDX" runat="server" Value='<%# Eval("rdept_idx") %>' />
                        <asp:HiddenField ID="hfEmpRsecIDX" runat="server" Value='<%# Eval("rsec_idx") %>' />
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายละเอียดผู้ใช้งาน</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">รหัสพนักงาน</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpCode" runat="server" CssClass="form-control" Text='<%# Eval("emp_code") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ชื่อ - นามสกุล</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpName" runat="server" CssClass="form-control" Text='<%# Eval("emp_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">องค์กร</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpOrg" runat="server" CssClass="form-control" Text='<%# Eval("org_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ฝ่าย</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpDept" runat="server" CssClass="form-control" Text='<%# Eval("dept_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">แผนก</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpSec" runat="server" CssClass="form-control" Text='<%# Eval("sec_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ตำแหน่ง</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpPos" runat="server" CssClass="form-control" Text='<%# Eval("pos_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </ItemTemplate>
                </asp:FormView>
                <!-- Form Detail Employee Create -->

                <!-- Form Create Holiday -->
                <asp:UpdatePanel ID="Panel_FormManageHoliday" runat="server" UpdateMode="Always">
                    <ContentTemplate>

                        <asp:FormView ID="fvManageHoliday" runat="server" DefaultMode="ReadOnly" Width="100%">
                            <InsertItemTemplate>

                                <div class="row">
                                    <div class="panel panel-info">
                                        <div class="panel-heading f-bold">จัดการวันหยุดสำหรับเจ้าหน้าที่</div>
                                        <div class="panel-body">
                                            <div class="col-md-10 col-md-offset-1">

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>เลือกองค์กร</label>
                                                        <asp:DropDownList ID="ddlorg" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" />
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>เลือกปี</label>
                                                        <asp:DropDownList ID="ddlyear_manage" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" />
                                                    </div>
                                                </div>

                                                <%-- <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>เลือกองค์กร</label>
                                                        <asp:GridView ID="GvCreateDetailOrg" runat="server"
                                                            AutoGenerateColumns="False"
                                                            CssClass="table table-striped table-bordered table-hover table-responsive"
                                                            OnPageIndexChanging="gvPageIndexChanging"
                                                            OnRowDataBound="gvRowDataBound"
                                                            AllowPaging="False" PageSize="10"
                                                            ShowFooter="False">
                                                            <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                                                            <RowStyle Font-Size="Small" />
                                                            <PagerStyle CssClass="pageCustom" />
                                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                                            <EmptyDataTemplate>
                                                                <div style="text-align: center">ไม่พบข้อมูล</div>
                                                            </EmptyDataTemplate>
                                                            <Columns>

                                                                <asp:TemplateField HeaderText="ลำดับ" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                    <ItemTemplate>

                                                                        <asp:CheckBox ID="chk_org" runat="server" OnCheckedChanged="onSelectedCheckChanged" AutoPostBack="true"></asp:CheckBox>

                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="ชื่อองค์กร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                    <ItemTemplate>

                                                                        <asp:Label ID="lbl_org_idx_detail" runat="server" Visible="false" Text='<%# Eval("org_idx") %>'></asp:Label>
                                                                        <asp:Label ID="lbl_org_name_th_detail" runat="server" Text='<%# Eval("org_name_th") %>'></asp:Label>


                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>

                                                </div>--%>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </InsertItemTemplate>

                        </asp:FormView>

                        <asp:UpdatePanel ID="Panel_DetailHolidaySearchCreateOrg" runat="server" UpdateMode="Always">
                            <ContentTemplate>

                                <div class="form-group">
                                    <label>เลือกวันหยุด</label>
                                    <asp:GridView ID="GvCreateDetailHoliday" runat="server"
                                        AutoGenerateColumns="False"
                                        CssClass="table table-striped table-bordered table-hover table-responsive"
                                        OnPageIndexChanging="gvPageIndexChanging"
                                        OnRowDataBound="gvRowDataBound"
                                        AllowPaging="False" PageSize="10"
                                        ShowFooter="False">
                                        <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                                        <RowStyle Font-Size="Small" />
                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                        <EmptyDataTemplate>
                                            <div style="text-align: center">ไม่พบข้อมูล</div>
                                        </EmptyDataTemplate>
                                        <Columns>

                                            <asp:TemplateField HeaderText="ลำดับ" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_u0_manage_idx_create" Visible="false" runat="server" CssClass="col-sm-12" Text='<%# Eval("u0_manage_idx") %>'></asp:Label>
                                                    <asp:Label ID="lbl_holiday_idx_create" Visible="false" runat="server" CssClass="col-sm-12" Text='<%# Eval("holiday_idx") %>'></asp:Label>
                                                    <asp:CheckBox ID="chk_holiday" runat="server" OnCheckedChanged="onSelectedCheckChanged" AutoPostBack="true"></asp:CheckBox>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="วันที่หยุด" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>


                                                    <asp:Label ID="lbl_holiday_date_create" runat="server" Text='<%# Eval("holiday_date") %>'></asp:Label>


                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ชื่อวันหยุด" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>


                                                    <asp:Label ID="lbl_holiday_name_create" runat="server" Text='<%# Eval("holiday_name") %>'></asp:Label>


                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                    </asp:GridView>
                                </div>

                                <div class="form-group">
                                    <div class="pull-right">
                                        <asp:LinkButton ID="btnSaveHolidayManage" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" OnCommand="btnCommand"
                                            CommandName="CmdSaveHolidayManage" Text="บันทึก" ValidationGroup="SaveHolidayManage" />
                                        <%-- <asp:LinkButton ID="btnSave" CssClass="btn btn-success" runat="server" OnCommand="btnCommand"
                                                                CommandName="CmdSave" Text="บันทึก" ValidationGroup="SaveDetailCar"
                                                                OnClientClick="return confirm('ยืนยันการบันทึก')" />--%>
                                        <asp:LinkButton CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" OnCommand="btnCommand"
                                            CommandName="CmdCancel" Text="ยกเลิก" />
                                    </div>
                                </div>


                            </ContentTemplate>
                        </asp:UpdatePanel>



                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- Form Create Holiday -->

            </div>
        </asp:View>
        <!--View Manage Holiday Org -->
    </asp:MultiView>
    <!--multiview-->

    <script src='<%= ResolveUrl("~/Scripts/script-dar-holiday.js") %>'></script>

    <script type="text/javascript">

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('#calendar').fullCalendar({
                    theme: true,

                    header: {
                        //left: 'prev,next today',
                        //center: 'title',
                        //right: 'month'
                        left: 'title',
                        center: '',
                        right: 'prev,next, today, month'
                    },

                    displayEventTime: false, //hide time event
                    eventColor: '#E3025B',
                    nowIndicator: true,


                    //views: {
                    //    listDay: { buttonText: 'list day' },
                    //    listWeek: { buttonText: 'list week' }
                    //},

                    defaultView: 'month',

                    dayCount: 7,
                    timeFormat: 'h:mm a',
                    allDayText: 'all-day', // set replace all-day
                    selectable: false,
                    selectHelper: false,

                    //select: selectDate, //allDay: true,// this decides if the all day slot should be showed at the top
                    editable: false,
                    events: '<%= ResolveUrl("~/websystem/hr/JsonResponseHoliday.ashx") %>',
                    eventDrop: eventDropped,
                    eventResize: eventResized,
                    eventLimit: true,

                    eventClick: function (event, jsEvent, view) {
                        $('#modalTitle').html(
                            '<strong>' + event.holiday_name + '</strong>'
                        );
                        $('#modalBody').html(
                            //event.description
                            '<strong>' + 'วันที่ : ' + '</strong>' + event.start.format("DD/MM/YYYY") + '<br>'
                            + '<strong>' + ' ( ' + event.holiday_name + ' ) ' + '</strong>' + '<br>'

                        );
                        $('#eventUrl').attr('href', event.url);
                        $('#fullCalModal').modal();
                    },

                    eventRender: function (event, element) {

                        //alert(moment.duration(given.diff(today)).asDays());

                        if (event != null) {
                            element.qtip({
                                content: {
                                    text: qTipText(event.holiday_name, event.start, event.end, event.descrition),
                                    //text: qTipText(event.topic_booking, event.place_name, event.room_name_th, event.start, event.end, event.description),//qTipText(event.start, event.end, event.description, event.topic_booking),
                                    //title: '<strong>' + 'ทะเบียน' + ' : ' + event.car_register + '</strong>'
                                    title: '<strong>' + "(" + event.holiday_name + ")" + '</strong>'
                                    //title: '<strong>' + 'หัวข้อ' + ' : ' + event.title + '</strong>'
                                },
                                position: {
                                    my: 'top left',
                                    at: 'center left'
                                    //my: 'bottom left',
                                    //at: 'left center'
                                },
                                hide: {
                                    event: 'mouseout'
                                },
                                show: {
                                    event: 'mouseover'
                                },
                                style: {
                                    //classes: 'qtip-shadow qtip-rounded qtip-youtube'
                                    classes: 'qtip-tipsy'
                                }

                            });


                        }

                    }
                    //defaultView: 'listWeek'



                });
            });
        });

        $(function () {
            $('#calendar').fullCalendar({
                theme: true,

                header: {
                    //left: 'prev,next today',
                    //center: 'title',
                    //right: 'month'

                    left: 'title',
                    center: '',
                    right: 'prev,next, today, month'




                    // right: 'month'//,agendaWeek,agendaDay,listWeek,listDay'
                },
                displayEventTime: false, //hide time event
                eventColor: '#E3025B',



                nowIndicator: true,

                //views: {
                //    listDay: { buttonText: 'list day' },
                //    listWeek: { buttonText: 'list week' }
                //},


                defaultView: 'month',
                dayCount: 7,

                timeFormat: 'h:mm a',
                allDayText: 'all-day', // set replace all-day
                //eventClick: updateEvent,
                selectable: false,
                selectHelper: false,

                //allDay: true,// this decides if the all day slot should be showed at the top
                //select: selectDate,
                editable: false,
                events: '<%= ResolveUrl("~/websystem/hr/JsonResponseHoliday.ashx") %>',
                eventDrop: eventDropped,
                eventResize: eventResized,
                eventLimit: true,

                eventClick: function (event, jsEvent, view) {
                    $('#modalTitle').html(
                        '<strong>' + event.holiday_name + '</strong>'
                    );
                    $('#modalBody').html(
                        //event.description
                        '<strong>' + 'วันที่ : ' + '</strong>' + event.start.format("DD/MM/YYYY") + '<br>'
                        + '<strong>' + ' ( ' + event.holiday_name + ' ) ' + '</strong>' + '<br>'

                    );
                    $('#eventUrl').attr('href', event.url);
                    $('#fullCalModal').modal();
                },

                eventRender: function (event, element) {
                    var today = moment(new Date()).format("YYYY-MM-DD");
                    //var start = moment(event.start).format("YYYY-MM-DD");
                    var date_end = moment(event.end, "YYYY-MM-DD");
                    var date_start = moment(event.start, "YYYY-MM-DD");

                    //var current = moment().startOf('day');

                    //alert(moment.duration(given.diff(today)).asDays());

                    if (event != null) {
                        element.qtip({
                            content: {
                                text: qTipText(event.holiday_name, event.start, event.end, event.descrition),
                                //text: qTipText(event.topic_booking, event.place_name, event.room_name_th, event.start, event.end, event.description),//qTipText(event.start, event.end, event.description, event.topic_booking),
                                //title: '<strong>' + 'ทะเบียน' + ' : ' + event.car_register + '</strong>'
                                title: '<strong>' + "(" + event.holiday_name + ")" + '</strong>'
                                //title: '<strong>' + 'หัวข้อ' + ' : ' + event.title + '</strong>'
                            },
                            position: {
                                my: 'top left',
                                at: 'center left'
                                //my: 'bottom left',
                                //at: 'left center'
                            },
                            hide: {
                                event: 'mouseout'
                            },
                            show: {
                                event: 'mouseover'
                            },
                            style: {
                                //classes: 'qtip-shadow qtip-rounded qtip-youtube'
                                classes: 'qtip-tipsy'
                            }

                        });


                    }

                    //element.find(".fc ui-widget fc-ltr").css('background-color', '#1E90FF');
                    //$('.fc-toolbar.fc-header-toolbar').css('font-color', '#00CC00');
                    //element.css('background-color', '#00CC00');
                    //element.find(".fc ui-widget fc-ltr").css('background-color', '#00CC00');

                }
                //defaultView: 'listWeek'

            });
        });

    </script>

    <script type="text/javascript">
        $(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY'
                //minDate: moment().add(-1, 'day'),
                //maxDate: moment().add(30, 'day'),

            });

        });

        $('.show-from-onclick').click(function () {
            $('.from-date-datepicker').data("DateTimePicker").show();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY'
                });

            });

            $('.show-from-onclick').click(function () {
                $('.from-date-datepicker').data("DateTimePicker").show();
            });

        });

    </script>

</asp:Content>
