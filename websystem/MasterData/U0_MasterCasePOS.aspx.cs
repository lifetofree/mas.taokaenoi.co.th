﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_U0_MasterCasePOS : System.Web.UI.Page
{
    #region Connect

    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();

    private string ODSP_Reletion = "ODSP_Reletion";
    data_employee dtEmployee = new data_employee();

    dataODSP_Relation dtODSP = new dataODSP_Relation();
    DataReservation datareservation = new DataReservation();
    DataSupportIT _dtsupport = new DataSupportIT();

    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";
    string _localJson = "";

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];

    static string urlSelectCaseLV1POSU0 = _serviceUrl + ConfigurationManager.AppSettings["urlSelectCaseLV1POSU0"];
    static string urlSelectCaseLV2POSU0 = _serviceUrl + ConfigurationManager.AppSettings["urlSelectCaseLV2POSU0"];
    static string urlSelectCaseLV3POSU0 = _serviceUrl + ConfigurationManager.AppSettings["urlSelectCaseLV3POSU0"];
    static string urlSelectCaseLV4POSU0 = _serviceUrl + ConfigurationManager.AppSettings["urlSelectCaseLV4POSU0"];

    static string urlInsertCaseU0POS = _serviceUrl + ConfigurationManager.AppSettings["urlInsertCaseU0POS"];
    static string urlSelectCaseU0POS = _serviceUrl + ConfigurationManager.AppSettings["urlSelectCaseU0POS"];
    static string urlUpdaeteCaseU0POS = _serviceUrl + ConfigurationManager.AppSettings["urlUpdaeteCaseU0POS"];
    static string urlDeleteCaseU0POS = _serviceUrl + ConfigurationManager.AppSettings["urlDeleteCaseU0POS"];



    #endregion

    #region PageLoad

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            MvMaster.SetActiveView(ViewIndex);
            SelectddlLV1();
            SelectddlLV2();
            SelectddlLV3();
            SelectddlLV4();
            SelectMasterList();

        }

        ViewState["EmpIDX"] = int.Parse(Session["emp_idx"].ToString());

    }

    #endregion

    #region Select DDL

    protected void SelectddlLV1()
    {
        ddlLV1.Items.Clear();
        ddlLV1.AppendDataBoundItems = true;
        ddlLV1.Items.Add(new ListItem("กรุณาเลือกเคสปิดงานLV1....", "0"));


        _dtsupport.BoxPOSList = new POSList[1];
        POSList dtsupport = new POSList();

        _dtsupport.BoxPOSList[0] = dtsupport;

        _dtsupport = callServicePostITRepair(urlSelectCaseLV1POSU0, _dtsupport);

        ddlLV1.DataSource = _dtsupport.BoxPOSList;
        ddlLV1.DataTextField = "Name_Code1";
        ddlLV1.DataValueField = "POS1IDX";
        ddlLV1.DataBind();
    }

    protected void SelectddlLV2()
    {
        ddlLV2.Items.Clear();
        ddlLV2.AppendDataBoundItems = true;
        ddlLV2.Items.Add(new ListItem("กรุณาเลือกเคสปิดงานLV2....", "0"));


        _dtsupport.BoxPOSList = new POSList[1];
        POSList dtsupport = new POSList();

        _dtsupport.BoxPOSList[0] = dtsupport;

        _dtsupport = callServicePostITRepair(urlSelectCaseLV2POSU0, _dtsupport);

        ddlLV2.DataSource = _dtsupport.BoxPOSList;
        ddlLV2.DataTextField = "Name_Code2";
        ddlLV2.DataValueField = "POS2IDX";
        ddlLV2.DataBind();
    }

    protected void SelectddlLV3()
    {
        ddlLV3.Items.Clear();
        ddlLV3.AppendDataBoundItems = true;
        ddlLV3.Items.Add(new ListItem("กรุณาเลือกเคสปิดงานLV3....", "0"));


        _dtsupport.BoxPOSList = new POSList[1];
        POSList dtsupport = new POSList();

        _dtsupport.BoxPOSList[0] = dtsupport;

        _dtsupport = callServicePostITRepair(urlSelectCaseLV3POSU0, _dtsupport);

        ddlLV3.DataSource = _dtsupport.BoxPOSList;
        ddlLV3.DataTextField = "Name_Code3";
        ddlLV3.DataValueField = "POS3IDX";
        ddlLV3.DataBind();
    }

    protected void SelectddlLV4()
    {
        ddlLV4.Items.Clear();
        ddlLV4.AppendDataBoundItems = true;
        ddlLV4.Items.Add(new ListItem("กรุณาเลือกเคสปิดงานLV4....", "0"));


        _dtsupport.BoxPOSList = new POSList[1];
        POSList dtsupport = new POSList();

        _dtsupport.BoxPOSList[0] = dtsupport;

        _dtsupport = callServicePostITRepair(urlSelectCaseLV4POSU0, _dtsupport);

        ddlLV4.DataSource = _dtsupport.BoxPOSList;
        ddlLV4.DataTextField = "Name_Code4";
        ddlLV4.DataValueField = "POS4IDX";
        ddlLV4.DataBind();
    }

    #endregion

    #region INSERT&SELECT&UPDATE

    protected void Insert_CaseLV1()
    {
        _dtsupport.BoxPOSList = new POSList[1];
        POSList insert = new POSList();

        insert.POS1IDX = int.Parse(ddlLV1.SelectedValue);
        insert.POS2IDX = int.Parse(ddlLV2.SelectedValue);
        insert.POS3IDX = int.Parse(ddlLV3.SelectedValue);
        insert.POS4IDX = int.Parse(ddlLV4.SelectedValue);
        insert.POSStatus = int.Parse(ddStatusadd.SelectedValue);
        insert.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

        _dtsupport.BoxPOSList[0] = insert;

        _dtsupport = callServicePostITRepair(urlInsertCaseU0POS, _dtsupport);

    }

    protected void SelectMasterList()
    {
        _dtsupport.BoxPOSList = new POSList[1];
        POSList dtsupport = new POSList();

        _dtsupport.BoxPOSList[0] = dtsupport;

        _dtsupport = callServicePostITRepair(urlSelectCaseU0POS, _dtsupport);
        setGridData(GvMaster, _dtsupport.BoxPOSList);
    }

    protected void Update_Master_List()
    {
        _dtsupport.BoxPOSList = new POSList[1];
        POSList dtsupport = new POSList();


        dtsupport.POS1IDX = int.Parse(ViewState["POS1IDX"].ToString());
        dtsupport.POS2IDX = int.Parse(ViewState["POS2IDX"].ToString());
        dtsupport.POS3IDX = int.Parse(ViewState["POS3IDX"].ToString());
        dtsupport.POS4IDX = int.Parse(ViewState["POS4IDX"].ToString());


        dtsupport.POSStatus = int.Parse(ViewState["StatusUpdate_Update"].ToString());
        dtsupport.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

        _dtsupport.BoxPOSList[0] = dtsupport;

        _dtsupport = callServicePostITRepair(urlUpdaeteCaseU0POS, _dtsupport);

    }

    protected void Delete_Master_List()
    {
        _dtsupport.BoxPOSList = new POSList[1];
        POSList dtsupport = new POSList();

        dtsupport.POSIDX = int.Parse(ViewState["POSIDX"].ToString());
        dtsupport.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        _dtsupport.BoxPOSList[0] = dtsupport;

        _dtsupport = callServicePostITRepair(urlDeleteCaseU0POS, _dtsupport);
    }

    protected DataSupportIT callServicePostITRepair(string _cmdUrl, DataSupportIT _dtsupport)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtsupport);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtsupport = (DataSupportIT)_funcTool.convertJsonToObject(typeof(DataSupportIT), _localJson);

        return _dtsupport;
    }


    #endregion

    #region Gridview

    #region SetGrid
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster.EditIndex != e.Row.RowIndex) 
                    {

                        var ddlPOS1IDX_edit = (DropDownList)e.Row.FindControl("ddlPOS1IDX_edit");
                        var ddlPOS2IDX_edit = (DropDownList)e.Row.FindControl("ddlPOS2IDX_edit");
                        var ddlPOS32IDX_edit = (DropDownList)e.Row.FindControl("ddlPOS3IDX_edit");
                        var ddlPOS4IDX_edit = (DropDownList)e.Row.FindControl("ddlPOS4IDX_edit");
                        var lbLV1 = (Label)e.Row.FindControl("lbLV1");
                        var lbLV2 = (Label)e.Row.FindControl("lbLV2");
                        var lbLV3 = (Label)e.Row.FindControl("lbLV3");
                        var lbLV4 = (Label)e.Row.FindControl("lbLV4");


                        ddlPOS1IDX_edit.AppendDataBoundItems = true;

                        _dtsupport.BoxPOSList = new POSList[1];
                        POSList dtsupport = new POSList();

                        _dtsupport.BoxPOSList[0] = dtsupport;

                        _dtsupport = callServicePostITRepair(urlSelectCaseLV1POSU0, _dtsupport);

                        ddlLV1.DataSource = _dtsupport.BoxPOSList;
                        ddlLV1.DataTextField = "Name_Code1";
                        ddlLV1.DataValueField = "POS1IDX";
                        ddlLV1.DataBind();
                        ddlLV1.SelectedValue = lbLV1.Text;

                        DataSupportIT _dtsupport2 = new DataSupportIT();
                        _dtsupport2.BoxPOSList = new POSList[1];
                        POSList dtsupport2 = new POSList();

                        _dtsupport2.BoxPOSList[0] = dtsupport2;

                        _dtsupport2 = callServicePostITRepair(urlSelectCaseLV2POSU0, _dtsupport2);

                        ddlLV2.DataSource = _dtsupport2.BoxPOSList;
                        ddlLV2.DataTextField = "Name_Code2";
                        ddlLV2.DataValueField = "POS2IDX";
                        ddlLV2.DataBind();
                        ddlLV2.SelectedValue = lbLV2.Text;


                        DataSupportIT _dtsupport3 = new DataSupportIT();
                        _dtsupport3.BoxPOSList = new POSList[1];
                        POSList dtsupport3 = new POSList();

                        _dtsupport3.BoxPOSList[0] = dtsupport3;

                        _dtsupport3 = callServicePostITRepair(urlSelectCaseLV3POSU0, _dtsupport3);

                        ddlLV3.DataSource = _dtsupport3.BoxPOSList;
                        ddlLV3.DataTextField = "Name_Code3";
                        ddlLV3.DataValueField = "POS3IDX";
                        ddlLV3.DataBind();
                        ddlLV3.SelectedValue = lbLV3.Text;

                        DataSupportIT _dtsupport4 = new DataSupportIT();
                        _dtsupport4.BoxPOSList = new POSList[1];
                        POSList dtsupport4 = new POSList();

                        _dtsupport4.BoxPOSList[0] = dtsupport4;

                        _dtsupport4 = callServicePostITRepair(urlSelectCaseLV4POSU0, _dtsupport4);

                        ddlLV4.DataSource = _dtsupport4.BoxPOSList;
                        ddlLV4.DataTextField = "Name_Code4";
                        ddlLV4.DataValueField = "POS4IDX";
                        ddlLV4.DataBind();
                        ddlLV4.SelectedValue = lbLV4.Text;

                    }

                }


                break;


        }

    }

    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();

                SelectMasterList();

                break;


        }
    }

    #endregion

    #region GvRowEditing

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.EditIndex = e.NewEditIndex;
                SelectMasterList();

                break;

        }
    }

    #endregion

    #region GvRowCancelingEdit

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":
                GvMaster.EditIndex = -1;
                SelectMasterList();
                break;

        }
    }

    #endregion

    #region GvRowUpdating

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                int POSIDX = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var ddlPOS1IDX_edit = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlPOS1IDX_edit");
                var ddlPOS2IDX_edit = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlPOS2IDX_edit");
                var ddlPOS3IDX_edit = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlPOS3IDX_edit");
                var ddlPOS4IDX_edit = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlPOS4IDX_edit");

                var StatusUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddStatusUpdate");

                GvMaster.EditIndex = -1;

                ViewState["POSIDX"] = POSIDX;
                ViewState["POS1IDX"] = ddlPOS1IDX_edit.SelectedValue;
                ViewState["POS2IDX"] = ddlPOS2IDX_edit.SelectedValue;
                ViewState["POS3IDX"] = ddlPOS3IDX_edit.SelectedValue;
                ViewState["POS4IDX"] = ddlPOS4IDX_edit.SelectedValue;
                ViewState["StatusUpdate_Update"] = StatusUpdate.SelectedValue;

                Update_Master_List();
                SelectMasterList();

                break;
        }
    }

    #endregion


    #endregion

    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {

            case "CmdAdd":

                btnadd.Visible = false;
                Panel_Add.Visible = true;
                break;

            case "btnCancel":
                btnadd.Visible = true;
                Panel_Add.Visible = false;

                ddlLV1.SelectedValue = "0";
                ddlLV2.SelectedValue = "0";
                ddlLV3.SelectedValue = "0";
                ddlLV4.SelectedValue = "0";

                break;

            case "btnAdd":
                Insert_CaseLV1();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "CmdDel":

                int POSIDX = int.Parse(cmdArg);
                ViewState["POSIDX"] = POSIDX;
                Delete_Master_List();
                SelectMasterList();


                break;
        }



    }
    #endregion
}