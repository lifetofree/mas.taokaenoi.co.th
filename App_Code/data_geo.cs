using System;
using System.Xml.Serialization;

/// <summary>
/// Summary description for data_geo
/// </summary>
[Serializable]
[XmlRoot("data_geo")]
public class data_geo
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }
    [XmlElement("return_idx")]
    public int return_idx { get; set; }

    [XmlElement("geo_list")]
    public geo_detail[] geo_list { get; set; }
}

[Serializable]
public class geo_detail
{
    [XmlElement("idx")]
    public int idx { get; set; }
    [XmlElement("code")]
    public string code { get; set; }
    [XmlElement("name_th")]
    public string name_th { get; set; }
    [XmlElement("name_en")]
    public string name_en { get; set; }

    [XmlElement("geography_idx")]
    public int geography_idx { get; set; }
    [XmlElement("province_idx")]
    public int province_idx { get; set; }
    [XmlElement("amphure_idx")]
    public int amphure_idx { get; set; }
    [XmlElement("district_idx")]
    public int district_idx { get; set; }

    [XmlElement("zip_code")]
    public int zip_code { get; set; }
}