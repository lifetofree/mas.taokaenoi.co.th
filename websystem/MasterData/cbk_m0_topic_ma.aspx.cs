﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_cbk_m0_topic_ma : System.Web.UI.Page
{

    #region Connect
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();
    data_carbooking _data_carbooking = new data_carbooking();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    //-- employee --//

    //-- carbooking --//
    static string _urlSetCbkm0Place = _serviceUrl + ConfigurationManager.AppSettings["urlSetCbkm0Place"];
    static string _urlGetCbkm0Place = _serviceUrl + ConfigurationManager.AppSettings["urlGetCbkm0Place"];
    static string _urlSetCbkm0PlaceDel = _serviceUrl + ConfigurationManager.AppSettings["urlSetCbkm0PlaceDel"];

    static string _urlSetCbkm0TopicMa = _serviceUrl + ConfigurationManager.AppSettings["urlSetCbkm0TopicMa"];
    static string _urlGetCbkTopicMA = _serviceUrl + ConfigurationManager.AppSettings["urlGetCbkTopicMA"];
    static string _urlSetCbkm0TopicMADel = _serviceUrl + ConfigurationManager.AppSettings["urlSetCbkm0TopicMADel"];


    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    bool _flag_qmr = false;
    #endregion Connect

    #region Page_Init
    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
        //check permission
        //foreach (int item in rdept_qmr)
        //{
        //    if (_dataEmployee.employee_list[0].rdept_idx == item)
        //    {
        //        _flag_qmr = true;
        //        break;
        //    }
        //}
    }
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //ViewState["empIDX"] = Session["emp_idx"];
            Select_TopicMA();


        }
    }
    #endregion Page Load

    #region initPage
    protected void initPage()
    {
        MvSystem.SetActiveView(ViewIndex);
        Select_TopicMA();
    }
    #endregion

    #region getEmployeeProfile
    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }
    #endregion

    #region setformdata
    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    #endregion setformdata

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        switch (cmdName)
        {
            case "cmdAddTopicMA":

                GvDetail.EditIndex = -1;
                Select_TopicMA();

                btn_AddTopicMA.Visible = false;
                setFormData(FvInsert, FormViewMode.Insert, null);
                FvInsert.Visible = true;
                //Gv_select_place.Visible = false;


                break;

            case "CmdSave":
                Insert_TopicMA();
                Select_TopicMA();
                btn_AddTopicMA.Visible = true;
                FvInsert.Visible = false;

                setOntop.Focus();
                break;

            case "cmdCancel":
                btn_AddTopicMA.Visible = true;
                FvInsert.Visible = false;
                setOntop.Focus();
                break;

            case "cmdDelete":

                int topic_ma_idx_del = int.Parse(cmdArg);

                data_carbooking data_m0_topicma_del = new data_carbooking();
                cbk_m0_topic_ma_detail m0_topicma_del = new cbk_m0_topic_ma_detail();
                data_m0_topicma_del.cbk_m0_topic_ma_list = new cbk_m0_topic_ma_detail[1];

                m0_topicma_del.topic_ma_idx = topic_ma_idx_del;
                m0_topicma_del.cemp_idx = _emp_idx;

                data_m0_topicma_del.cbk_m0_topic_ma_list[0] = m0_topicma_del;

                //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_expenses_del));
                data_m0_topicma_del = callServicePostCarBooking(_urlSetCbkm0TopicMADel, data_m0_topicma_del);

                Select_TopicMA();
                //Gv_select_unit.Visible = false;
                btn_AddTopicMA.Visible = true;
                FvInsert.Visible = false;
                break;
        }
    }
    #endregion btnCommand

    #region Custom Functions
    protected void Insert_TopicMA()
    {

        TextBox txt_topic_name = (TextBox)FvInsert.FindControl("txt_topic_name");
        //TextBox txt_detailtopic_name = (TextBox)FvInsert.FindControl("txt_detailtopic_name");
        DropDownList ddlStatusTopic = (DropDownList)FvInsert.FindControl("ddlStatusTopic");

        data_carbooking data_m0_topic_ma = new data_carbooking();
        cbk_m0_topic_ma_detail m0_topic_ma_insert = new cbk_m0_topic_ma_detail();
        data_m0_topic_ma.cbk_m0_topic_ma_list = new cbk_m0_topic_ma_detail[1];

        m0_topic_ma_insert.topic_ma_idx = 0;
        m0_topic_ma_insert.topic_ma_name = txt_topic_name.Text;
        //m0_topic_ma_insert.detail_ma_name = txt_detailtopic_name.Text;
        m0_topic_ma_insert.cemp_idx = _emp_idx;
        m0_topic_ma_insert.topic_ma_status = int.Parse(ddlStatusTopic.SelectedValue);

        data_m0_topic_ma.cbk_m0_topic_ma_list[0] = m0_topic_ma_insert;

        data_m0_topic_ma = callServicePostCarBooking(_urlSetCbkm0TopicMa, data_m0_topic_ma);

        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Unit_b.return_code));
        if (data_m0_topic_ma.return_code == 1)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);

        }
        else
        {
            txt_topic_name.Text = String.Empty;
            //txt_detailtopic_name.Text = String.Empty;
        }
    }

    protected void Select_TopicMA()
    {

        data_carbooking data_m0_topic_detail = new data_carbooking();
        cbk_m0_topic_ma_detail m0_topic_detail = new cbk_m0_topic_ma_detail();
        data_m0_topic_detail.cbk_m0_topic_ma_list = new cbk_m0_topic_ma_detail[1];

        m0_topic_detail.condition = 0;

        data_m0_topic_detail.cbk_m0_topic_ma_list[0] = m0_topic_detail;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_m0_topic_detail = callServicePostCarBooking(_urlGetCbkTopicMA, data_m0_topic_detail);

        setGridData(GvDetail, data_m0_topic_detail.cbk_m0_topic_ma_list);
        //Gv_select_place.DataSource = m0_place_detail.qa_cims_m0_place_list;
        //Gv_select_place.DataBind();
    }
    #endregion Custom Functions

    #region setGridData
    private void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region Masterbound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvDetail":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbl_topic_ma_status = (Label)e.Row.Cells[3].FindControl("lbl_topic_ma_status");
                    Label topic_ma_statusOnline = (Label)e.Row.Cells[3].FindControl("topic_ma_statusOnline");
                    Label topic_ma_statusOffline = (Label)e.Row.Cells[3].FindControl("topic_ma_statusOffline");

                    ViewState["vs_topic_status"] = lbl_topic_ma_status.Text;


                    if (ViewState["vs_topic_status"].ToString() == "1")
                    {
                        topic_ma_statusOnline.Visible = true;
                    }
                    else if (ViewState["vs_topic_status"].ToString() == "0")
                    {
                        topic_ma_statusOffline.Visible = true;
                    }

                }


                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                    //btn_addplace.Visible = true;
                    //setFormData(Fv_Insert_Result, FormViewMode.ReadOnly, null);

                    //MultiView1.Visible = true;
                    //Gv_select_unit.Visible = false;
                    btn_AddTopicMA.Visible = true;
                    FvInsert.Visible = false;

                }

                break;


        }
    }
    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvDetail":
                GvDetail.EditIndex = e.NewEditIndex;
                Select_TopicMA();
                break;

        }
    }
    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

        var GvName = (GridView)sender;
        //Label5.Text = GvName.ID;
        switch (GvName.ID)
        {
            case "GvDetail":

                var txt_topic_ma_idx_edit = (TextBox)GvDetail.Rows[e.RowIndex].FindControl("txt_topic_ma_idx_edit");
                var txt_topic_ma_name_edit = (TextBox)GvDetail.Rows[e.RowIndex].FindControl("txt_topic_ma_name_edit");
                //var txt_detail_ma_name_edit = (TextBox)GvDetail.Rows[e.RowIndex].FindControl("txt_detail_ma_name_edit");
                var ddltopic_ma_status_edit = (DropDownList)GvDetail.Rows[e.RowIndex].FindControl("ddltopic_ma_status_edit");


                GvDetail.EditIndex = -1;


                data_carbooking data_m0_topicma_edit = new data_carbooking();
                cbk_m0_topic_ma_detail m0_topicma_edit = new cbk_m0_topic_ma_detail();
                data_m0_topicma_edit.cbk_m0_topic_ma_list = new cbk_m0_topic_ma_detail[1];

                m0_topicma_edit.topic_ma_idx = int.Parse(txt_topic_ma_idx_edit.Text);
                m0_topicma_edit.topic_ma_name = txt_topic_ma_name_edit.Text;
                //m0_topicma_edit.detail_ma_name = txt_detail_ma_name_edit.Text;
                m0_topicma_edit.cemp_idx = _emp_idx;
                m0_topicma_edit.topic_ma_status = int.Parse(ddltopic_ma_status_edit.SelectedValue);

                data_m0_topicma_edit.cbk_m0_topic_ma_list[0] = m0_topicma_edit;
                data_m0_topicma_edit = callServicePostCarBooking(_urlSetCbkm0TopicMa, data_m0_topicma_edit);


                if (data_m0_topicma_edit.return_code == 1)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ข้อมูลซ้ำ กรุณาแก้ไขใหม่อีกครั้ง!!!');", true);
                    Select_TopicMA();
                }
                else
                {
                    Select_TopicMA();
                }
                break;

        }
    }
    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        //Label5.Text = GvName.ID;
        switch (GvName.ID)
        {
            case "GvDetail":
                GvDetail.EditIndex = -1;
                Select_TopicMA();
                //btn_addplace.Visible = true;
                //SETFOCUS_ONTOP.Focus();
                //Gv_select_unit.Visible = false;
                btn_AddTopicMA.Visible = true;
                FvInsert.Visible = false;
                break;

        }
    }
    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvDetail":
                GvDetail.PageIndex = e.NewPageIndex;
                GvDetail.DataBind();
                Select_TopicMA();

                setOntop.Focus();
                break;

        }
    }
    #endregion

    #region selected Changed
    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlName = (DropDownList)sender;

        switch (ddlName.ID)
        {
            
            case "ddlStatusFilter":
                //int _set_statusFilter = 0;
                //setGridData(GvDetail, ViewState["Va_DetailCarbooking"]);
                ////if (ddlStatusFilter.SelectedValue != "0")
                ////{
                ////    //setGridData(GvDetail, ViewState["Va_DetailCarbooking"]);
                ////    //litDebug.Text = "1";

                ////    data_carbooking data_u0_document_detail = new data_carbooking();
                ////    cbk_u0_document_detail u0_document_detail = new cbk_u0_document_detail();
                ////    data_u0_document_detail.cbk_u0_document_list = new cbk_u0_document_detail[1];
                ////    u0_document_detail.cemp_idx = _emp_idx;
                ////    u0_document_detail.cemp_rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
                ////    u0_document_detail.cemp_rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
                ////    u0_document_detail.cemp_rpos_idx = int.Parse(ViewState["rpos_permission"].ToString());
                ////    u0_document_detail.cemp_joblevel = int.Parse(ViewState["joblevel_permission"].ToString());

                ////    data_u0_document_detail.cbk_u0_document_list[0] = u0_document_detail;


                ////    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_detail));
                ////    data_u0_document_detail = callServicePostCarBooking(_urlGetCbkDetailCarbooking, data_u0_document_detail);
                ////    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_type_travel_detail));

                ////    //ViewState["Va_DetailCarbooking"] = data_u0_document_detail.cbk_u0_document_list;



                ////    cbk_u0_document_detail[] _item_FilterStatus = (cbk_u0_document_detail[])data_u0_document_detail.cbk_u0_document_list;

                ////    var _linq_FilterStatusIndex = (from data in _item_FilterStatus
                ////                                   where
                ////                                   data.staidx == int.Parse(ddlStatusFilter.SelectedValue)

                ////                                   select data
                ////                            ).ToList();

                ////    _set_statusFilter = int.Parse(ddlStatusFilter.SelectedValue);

                ////    //

                ////    ViewState["Va_DetailCarbooking"] = _linq_FilterStatusIndex;
                ////    setGridData(GvDetail, ViewState["Va_DetailCarbooking"]);
                ////    //ViewState["Va_DetailCarbooking"] = null;

                ////}
                ////else
                ////{
                ////    //ViewState["Va_DetailCarbooking_linq"] = null;
                ////    getDetailCarBooking();

                ////    //setGridData(GvDetail, ViewState["Va_DetailCarbooking"]);
                ////}

                break;

        }
    }
    #endregion selected Changed

    #region callService 

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_carbooking callServicePostCarBooking(string _cmdUrl, data_carbooking _data_carbooking)
    {
        _localJson = _funcTool.convertObjectToJson(_data_carbooking);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_carbooking = (data_carbooking)_funcTool.convertJsonToObject(typeof(data_carbooking), _localJson);


        return _data_carbooking;
    }
    #endregion callService Functions
}