﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_el_Trn_plan_course_evaluform : System.Web.UI.Page
{
    #region Init
    function_tool _funcTool = new function_tool();
    data_elearning _data_elearning = new data_elearning();
    function_dmu _func_dmu = new function_dmu();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetel_employee = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_employee"];

    static string _urlGetel_u_plan_course = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_u_plan_course"];
    static string _urlSetInsel_u_plan_course = _serviceUrl + ConfigurationManager.AppSettings["urlSetInsel_u_plan_course"];
    static string _urlDelel_u_plan_course = _serviceUrl + ConfigurationManager.AppSettings["urlDelel_u_plan_course"];
    static string _urlSetUpdel_u_plan_course = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdel_u_plan_course"];

    static string _urlsendEmail_plan_course_hrtohrd = _serviceUrl + ConfigurationManager.AppSettings["urlsendEmail_plan_course_hrtohrd"];
    static string _urlsendEmail_plan_course_hrdtomd = _serviceUrl + ConfigurationManager.AppSettings["urlsendEmail_plan_course_hrdtomd"];
    static string _urlsendEmail_plan_course_hrdtohr_all = _serviceUrl + ConfigurationManager.AppSettings["urlsendEmail_plan_course_hrdtohr_all"];
    static string _urlsendEmail_plan_course_mdtohrd_all = _serviceUrl + ConfigurationManager.AppSettings["urlsendEmail_plan_course_mdtohrd_all"];
    static string _urlsendEmail_outplan_course_usertoleader = _serviceUrl + ConfigurationManager.AppSettings["urlsendEmail_outplan_course_usertoleader"];
    static string _urlsendemail_outplan_leadertohr = _serviceUrl + ConfigurationManager.AppSettings["urlsendemail_outplan_leadertohr"];
    static string _urlsendemail_outplan_leadertouser_all = _serviceUrl + ConfigurationManager.AppSettings["urlsendemail_outplan_leadertouser_all"];


    static string _urlGetel_Report_plan_course = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_Report_plan_course"];

    //หลักสูตร 
    static string _urlSetInsel_u_course = _serviceUrl + ConfigurationManager.AppSettings["urlSetInsel_u_course"];

    string _localJson = "";
    int _tempInt = 0;
    int emp_idx = 0;
    #endregion Init

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            int id = 0;
            ViewState["_id"] = "0";
            try
            {
                if (Page.RouteData.Values["id"] != null)
                {
                    string sid = Page.RouteData.Values["id"].ToString().ToLower();
                    string[] urldecry = (_funcTool.getDecryptRC4(sid, "id")).Split('|');
                    sid = urldecry[0];
                    id = _func_dmu.zStringToInt(sid);
                    ViewState["_id"] = sid;
                }
                //string sid = Request.QueryString["id"].ToString().Trim();

            }
            catch
            {

            }
            setTitle(id);
        }

    }
    private void setTitle(int id)
    {
        lbltopic.Text = "แบบฟอร์มประเมินผลความพึงพอใจหลังการฝึกอบรม";
        litcourse.Text = "";

        Panel1.Attributes.Add("style", "height: 100%; background-position: top; background-repeat: no-repeat; background-size: 100% 100%");
        Panel1.BackImageUrl = "~/images/elearning/evaluationform/background_evaluationform_1.png";

        dv_header.Attributes.Add("style", "background-image: url(" + ResolveUrl("~/images/elearning/evaluationform/banner_evaluationform_1old.png") + "); height: 250px; background-position: top; background-repeat: no-repeat; background-size: 100% 100%;");

        //Panel5.Attributes.Add("style", "background-size: 100% 100%");
        //Panel5.BackImageUrl = "~/masterpage/images/hr/background_register_2.png";

        //pnl_background.Attributes.Add("style", "background-size: 100% 100%");
        //pnl_background.BackImageUrl = "~/images/elearning/evaluationform/background_evaluationform_1.png";

        data_elearning dataelearning_detail;
        training_course obj_detail;
        //el_u1_training_course_lecturer
        dataelearning_detail = new data_elearning();
        dataelearning_detail.el_training_course_action = new training_course[1];
        obj_detail = new training_course();
        obj_detail.u0_training_course_idx = id;
        obj_detail.operation_status_id = "U1-FULL";
        dataelearning_detail.el_training_course_action[0] = obj_detail;
        dataelearning_detail = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning_detail);
        CreateDs_el_u1_training_course_lecturer();
        CreateDs_el_tc_evaluationform();
        pnldata.Visible = false;
        pnl_alert.Visible = false;
        if (dataelearning_detail.el_training_course_action != null)
        {
            pnldata.Visible = true;
            DataSet ds = (DataSet)ViewState["vsel_u1_training_course_lecturer"];
            DataRow dr;
            foreach (var v_item in dataelearning_detail.el_training_course_action)
            {
                dr = ds.Tables["dsel_u1_training_course_lecturer"].NewRow();
                dr["lecturer_type"] = v_item.lecturer_type.ToString();
                dr["m0_institution_idx_ref"] = v_item.m0_institution_idx_ref.ToString();
                dr["zName"] = v_item.institution_name;
                ds.Tables["dsel_u1_training_course_lecturer"].Rows.Add(dr);
            }
            ViewState["vsel_u1_training_course_lecturer"] = ds;
        }
        _func_dmu.zSetGridData(Gvinstitution, ViewState["vsel_u1_training_course_lecturer"]);


        dataelearning_detail = new data_elearning();
        dataelearning_detail.el_training_course_action = new training_course[1];
        obj_detail = new training_course();
        obj_detail.u0_training_course_idx = id;
        obj_detail.operation_status_id = "evaluationform";
        dataelearning_detail.el_training_course_action[0] = obj_detail;
        dataelearning_detail = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning_detail);
        if (dataelearning_detail.el_training_course_action != null)
        {
            obj_detail = dataelearning_detail.el_training_course_action[0];
            litcourse.Text = obj_detail.training_course_no + " - " + obj_detail.zcourse_name;

            DataSet ds = (DataSet)ViewState["vsel_tc_evaluationform"];
            DataRow dr;
            int i = 0;
            string _evaluation_group_name_th = "";
            foreach (var v_item in dataelearning_detail.el_training_course_action)
            {
                i++;
                dr = ds.Tables["dsel_tc_evaluationform"].NewRow();
                dr["id"] = i.ToString();
                dr["u0_training_course_idx"] = v_item.u0_training_course_idx.ToString();
                dr["m0_evaluation_f_idx_ref"] = v_item.m0_evaluation_f_idx_ref.ToString();
                dr["m0_evaluation_group_idx_ref"] = v_item.m0_evaluation_group_idx_ref.ToString();
                dr["course_item"] = v_item.course_item.ToString();
                dr["zName"] = v_item.zName;
                if (_evaluation_group_name_th != v_item.evaluation_group_name_th)
                {
                    dr["evaluation_group_name_th"] = v_item.evaluation_group_name_th;
                }
                else
                {
                    dr["evaluation_group_name_th"] = "";
                }
                _evaluation_group_name_th = v_item.evaluation_group_name_th;
                ds.Tables["dsel_tc_evaluationform"].Rows.Add(dr);
            }
            ViewState["vsel_tc_evaluationform"] = ds;

        }
        else
        {
            pnl_alert.Visible = true;
        }
        _func_dmu.zSetGridData(GvListData, ViewState["vsel_tc_evaluationform"]);
        SETFOCUS.Focus();

    }
    private void CreateDs_el_tc_evaluationform()
    {
        string sDs = "dsel_tc_evaluationform";
        string sVs = "vsel_tc_evaluationform";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);
        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_training_course_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("m0_evaluation_f_idx_ref", typeof(String));
        ds.Tables[sDs].Columns.Add("m0_evaluation_group_idx_ref", typeof(String));
        ds.Tables[sDs].Columns.Add("course_item", typeof(String));
        ds.Tables[sDs].Columns.Add("evaluation_group_name_th", typeof(String));
        ds.Tables[sDs].Columns.Add("zName", typeof(String));
        ViewState[sVs] = ds;

    }
    private void CreateDs_el_u1_training_course_lecturer()
    {
        string sDs = "dsel_u1_training_course_lecturer";
        string sVs = "vsel_u1_training_course_lecturer";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);
        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("u1_training_course_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_training_course_idx_ref", typeof(String));
        ds.Tables[sDs].Columns.Add("lecturer_type", typeof(String));
        ds.Tables[sDs].Columns.Add("m0_institution_idx_ref", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_status", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_updated_by", typeof(String));
        ds.Tables[sDs].Columns.Add("zName", typeof(String));
        ViewState[sVs] = ds;

    }

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        int _idx = 0;
        string[] _calc = new string[4];
        switch (cmdName)
        {
            case "btnSaveInsert":
                if (checkError() == false)
                {
                    zSave();
                }
                break;
        }

    }
    #endregion btnCommand
    private Boolean checkError()
    {
        Boolean _Boolean = false;
        foreach (GridViewRow gr_item in GvListData.Rows)
        {
            RadioButtonList rdoItem = (RadioButtonList)gr_item.FindControl("rdoItem");
            Label lbzName = (Label)gr_item.FindControl("lbzName");
            if (_func_dmu.zStringToInt(rdoItem.SelectedValue) == 0)
            {
                showAlert("กรุณาเลือกคำตอบข้อ : " + lbzName.Text);
                _Boolean = true;
                break;
            }

        }
        return _Boolean;
    }
    private void showAlert(string Str)
    {
        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('" + Str + "');", true);
    }
    private void zSave()
    {
        string _fromrunno = "trn_p_c_evaluform";
        string sDocno;
        string m0_prefix = "TRQEF";
        sDocno = _func_dmu.zRun_Number(_fromrunno, m0_prefix, "YYMM", "N", "0000");
        int itemObj = 0;
        data_elearning dataelearning = new data_elearning();
        itemObj = 0;
        training_course[] objcourse1 = new training_course[GvListData.Rows.Count];
        foreach (GridViewRow gr_item in GvListData.Rows)
        {
            RadioButtonList rdoItem = (RadioButtonList)gr_item.FindControl("rdoItem");
            Label lbu0_training_course_idx = (Label)gr_item.FindControl("lbu0_training_course_idx");
            Label lbm0_evaluation_f_idx_ref = (Label)gr_item.FindControl("lbm0_evaluation_f_idx_ref");
            Label lbm0_evaluation_group_idx_ref = (Label)gr_item.FindControl("lbm0_evaluation_group_idx_ref");
            Label lbcourse_item = (Label)gr_item.FindControl("lbcourse_item");
            Label lbcourse_evaluation_f_name_th = (Label)gr_item.FindControl("lbcourse_evaluation_f_name_th");
            //if (_func_dmu.zStringToInt(lbcourse_item.Text) > 0)
            //{
            objcourse1[itemObj] = new training_course();
            objcourse1[itemObj].u0_training_course_idx_ref = _func_dmu.zStringToInt(lbu0_training_course_idx.Text);
            objcourse1[itemObj].doc_no = sDocno;
            objcourse1[itemObj].u0_training_course_idx = _func_dmu.zStringToInt(lbu0_training_course_idx.Text);
            objcourse1[itemObj].m0_evaluation_f_idx_ref = _func_dmu.zStringToInt(lbm0_evaluation_f_idx_ref.Text);
            objcourse1[itemObj].m0_evaluation_group_idx_ref = _func_dmu.zStringToInt(lbm0_evaluation_group_idx_ref.Text);
            objcourse1[itemObj].course_item = _func_dmu.zStringToInt(lbcourse_item.Text);
            objcourse1[itemObj].zName = lbcourse_evaluation_f_name_th.Text;
            objcourse1[itemObj].score_qty = _func_dmu.zStringToInt(rdoItem.SelectedValue);
            objcourse1[itemObj].training_course_status = 1;
            objcourse1[itemObj].training_course_updated_by = 0;
            objcourse1[itemObj].operation_status_id = "U10";
            itemObj++;
            //}

        }
        dataelearning.el_training_course_action = objcourse1;
        _func_dmu.zCallServicePostNetwork(_urlSetInsel_u_plan_course, dataelearning);
        pnl_alert.Visible = false;
        pnldata.Visible = false;
        pnl_complete.Visible = true;

    }

}