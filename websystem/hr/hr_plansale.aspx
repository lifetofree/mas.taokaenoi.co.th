﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="hr_plansale.aspx.cs" EnableEventValidation="false" Inherits="websystem_hr_hr_plansale" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:Literal ID="text" runat="server"></asp:Literal>

    <style type="text/css">
        .statusmaster-waiting {
            color: #FFA500;
            font-size: 2em;
        }

        .statusmaster-online {
            color: #2ECC71;
            font-size: 2em;
        }

        .statusmaster-offline {
            color: #CF000F;
            font-size: 2em;
        }

        .statusmaster-edit {
            color: #289AFF;
            font-size: 2em;
        }

        .AlgRgh {
            text-align: center;
            font-family: Verdana, Arial, Helvetica, sans-serif;
        }
    </style>

    <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>--%>
    <%----------- Menu Tab Start---------------%>
    <div id="BoxTabMenuIndex" runat="server">
        <div class="form-group">
            <nav class="navbar navbar-default" id="divBtnHead" runat="server" role="navigation">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#Menu1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div class="sub-navbar">
                            <a class="navbar-brand " href="#"><b>Menu</b></a>
                        </div>
                    </div>

                    <div class="collapse navbar-collapse" id="Menu1">
                        <asp:LinkButton ID="lbindex" CssClass="btn_menulist" runat="server" CommandName="btnIndex" OnCommand="btnCommand" CommandArgument="1">สร้างรายการ</asp:LinkButton>
                        <asp:LinkButton ID="lbdetail" CssClass="btn_menulist" runat="server" CommandName="btndetail" OnCommand="btnCommand" CommandArgument="2">รายการทั่วไป</asp:LinkButton>
                        <asp:LinkButton ID="lbfileupload" CssClass="btn_menulist" runat="server" CommandName="btnfile_uplad" OnCommand="btnCommand" CommandArgument="5">จัดการไฟล์เอกสาร</asp:LinkButton>
                        <asp:LinkButton ID="lbsetapprove" CssClass="btn_menulist" runat="server" CommandName="btnsetapprove" OnCommand="btnCommand" CommandArgument="3">รออนุมัติ</asp:LinkButton>
                        <asp:LinkButton ID="lbreport" CssClass="btn_menulist" runat="server" CommandName="btnreport" OnCommand="btnCommand" CommandArgument="4">รายงาน</asp:LinkButton>
                        <asp:LinkButton ID="lbguid" CssClass="btn_menulist col-sm-offset-5" runat="server" CommandName="btnguid" OnCommand="btnCommand">คู่มือการใช้งาน</asp:LinkButton>
                    </div>
                </div>
                <!-- /.container-fluid -->
            </nav>
        </div>
    </div>

    <asp:Label ID="sdfa" runat="server"></asp:Label>
    <%-------------- Menu Tab End--------------%>

    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewIndex" runat="server">

            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-blackboard"></i><strong>&nbsp; Plan Sale</strong></h3>
                    </div>

                    <%--<div class="form-group">
                            <asp:LinkButton ID="btnaddholder" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="AddStatus" runat="server" CommandName="CmdAddHolder" OnCommand="btnCommand"><i class="fa fa-plus"> </i> Upload File</asp:LinkButton>
                        </div>--%>

                    <div class="panel-body">

                        <div id="calendar" class="col-md-8">
                            <div id="fullCalModal" class="modal fade">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">close</span></button>
                                            <h4 id="modalTitle" class="modal-title"></h4>
                                        </div>
                                        <div id="modalBody" class="modal-body">uteueuetuteu</div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <br />
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <div id="ca" class="col-md-4">
                                    <div class="form-group">
                                        <div class="panel panel-info">
                                            <div class="panel-heading">ช่วงวันที่ต้องทำรายการ</div>
                                            <div class="panel-body">

                                                <asp:DropDownList ID="ddltypelist" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                    <asp:ListItem Value="0">ประเภทรายการ....</asp:ListItem>
                                                    <asp:ListItem Value="1">ปฏิบัติงาน</asp:ListItem>
                                                    <asp:ListItem Value="2">รายการลา</asp:ListItem>
                                                </asp:DropDownList>

                                                <asp:RequiredFieldValidator ID="Requtor1" ValidationGroup="InsertData" runat="server" Display="None"
                                                    ControlToValidate="ddltypelist" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกกะทำงาน ...."
                                                    ValidationExpression="กรุณาเลือกกะทำงาน ...." InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requtor1" Width="160" />
                                                <br />

                                                <asp:Panel ID="box_leavetime" runat="server" Visible="false">
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="ddlleavetime" runat="server" CssClass="form-control">
                                                            <asp:ListItem Value="0">ประเภทลา....</asp:ListItem>
                                                        </asp:DropDownList>

                                                        <asp:RequiredFieldValidator ID="Rewetor1" ValidationGroup="InsertData" runat="server" Display="None"
                                                            ControlToValidate="ddlleavetime" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกประเภทลา ...."
                                                            ValidationExpression="กรุณาเลือกประเภทลา ...." InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="Validaaender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Rewetor1" Width="160" />
                                                    </div>
                                                </asp:Panel>

                                                <asp:Panel ID="Panel1" runat="server" Visible="true">
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="ddl_shift_time" runat="server" CssClass="form-control">
                                                            <asp:ListItem Value="0">เลือกกะทำงาน....</asp:ListItem>
                                                        </asp:DropDownList>

                                                        <asp:RequiredFieldValidator ID="Reqadtor1" ValidationGroup="InsertData" runat="server" Display="None"
                                                            ControlToValidate="ddl_shift_time" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกประเภทกะทำงาน ...."
                                                            ValidationExpression="กรุณาเลือกประเภทกะทำงาน ...." InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Reqadtor1" Width="160" />
                                                    </div>
                                                </asp:Panel>

                                                <div class='input-group date'>
                                                    <asp:TextBox ID="txtDate_Start" runat="server" placeholder="ตั้งแต่วันที่..."
                                                        CssClass="form-control from-date-datepicker" />
                                                    <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>

                                                    <asp:RequiredFieldValidator ID="Reqator1" ValidationGroup="InsertData" runat="server" Display="None"
                                                        ControlToValidate="txtDate_Start" Font-Size="11"
                                                        ErrorMessage="ช่วงวันที่เริ่ม ...." />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValdsidatorasCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Reqator1" Width="160" />
                                                </div>
                                                <br />
                                                <div class='input-group date'>
                                                    <asp:TextBox ID="txtDate_End" runat="server" placeholder="ถึงวันที่..."
                                                        CssClass="form-control from-date-datepicker" />
                                                    <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>

                                                    <asp:RequiredFieldValidator ID="Requirwidator1" ValidationGroup="InsertData" runat="server" Display="None"
                                                        ControlToValidate="txtDate_End" Font-Size="11"
                                                        ErrorMessage="ช่วงวันที่จบ ...." />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requirwidator1" Width="160" />
                                                </div>
                                                <br />
                                                <asp:Panel ID="box_comment" runat="server" Visible="false">
                                                    <div class="form-group">
                                                        <asp:TextBox ID="txt_comment" runat="server" TextMode="MultiLine" CssClass="form-control" placeholder="เหตุผล/หมายเหตุ" />
                                                        <asp:RequiredFieldValidator ID="Requto2r1" ValidationGroup="InsertData" runat="server" Display="None"
                                                            ControlToValidate="txt_comment" Font-Size="11"
                                                            ErrorMessage="กรุณากรอกข้อมูล ...." />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="Vader3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requto2r1" Width="160" />
                                                    </div>
                                                </asp:Panel>

                                                <asp:Panel ID="box_workdetail" runat="server" Visible="false">
                                                    <div class="form-group">
                                                        <asp:TextBox ID="txt_work_detail" runat="server" TextMode="MultiLine" CssClass="form-control" placeholder="ร้านค้าที่เข้าเยี่ยม/รายละเอียดงาน" />
                                                        <asp:RequiredFieldValidator ID="Requir4r1" ValidationGroup="InsertData" runat="server" Display="None"
                                                            ControlToValidate="txt_work_detail" Font-Size="11"
                                                            ErrorMessage="กรุณากรอกข้อมูล ...." />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="Valnder3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requir4r1" Width="160" />
                                                    </div>
                                                </asp:Panel>
                                            </div>
                                        </div>

                                        <asp:LinkButton ID="LinkButton2" runat="server" CssClass="btn btn-primary" OnCommand="btnCommand" CommandName="AddDetail" ValidationGroup="InsertData">ADD + &nbsp;</asp:LinkButton>

                                        <br />
                                        <br />
                                        <div class="form-group">
                                            <asp:GridView ID="GvPlansaleAdd"
                                                runat="server"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered table-hover"
                                                HeaderStyle-CssClass="table_headCenter"
                                                HeaderStyle-Height="40px"
                                                ShowFooter="False"
                                                ShowHeaderWhenEmpty="True"
                                                AllowPaging="True"
                                                PageSize="100"
                                                BorderStyle="None"
                                                CellSpacing="2"
                                                OnRowDeleting="gvRowDeleting"
                                                OnRowDataBound="Master_RowDataBound">
                                                <HeaderStyle CssClass="info" />
                                                <PagerStyle CssClass="PageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                </EmptyDataTemplate>

                                                <Columns>

                                                    <asp:TemplateField HeaderText="#">
                                                        <ItemTemplate>
                                                            <div class="panel-heading">
                                                                <%# (Container.DataItemIndex +1) %>
                                                            </div>
                                                            </p> 
                                                        <asp:Image ID="img_profile" runat="server" HorizontalAlign="Center"></asp:Image>


                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ประเภทรายการ">
                                                        <ItemTemplate>
                                                            <small>
                                                                <b>
                                                                    <asp:Label ID="lbl_m0_typelist" runat="server" Text="ประเภท : " /><asp:Label ID="lbl_m0_typelist_1" runat="server" Text='<%# Eval("m0_typelist_name") %>'></asp:Label></b>
                                                                <asp:Label ID="lbl_m0_typelist_idx" runat="server" Text='<%# Eval("m0_typelist_idx") %>' Visible="false" />
                                                                </p>   
                                                        <b>
                                                            <asp:Label ID="lbl_m0_leavetype" runat="server" Text="ประเภทลา : " /></b><asp:Label ID="lbl_m0_leavetype_1" runat="server" Text='<%# Eval("m0_leavetype_name") %>'></asp:Label>
                                                                </p>
                                    <b>
                                        <asp:Label ID="lbl_ps_start" runat="server" Text="วันที่เริ่ม : " /></b><asp:Label ID="lbl_ps_start_1" runat="server" Text='<%# Eval("ps_startdate") %>'></asp:Label>
                                                                </p>
                                        <b>
                                            <asp:Label ID="lbl_ps_enddate" runat="server" Text="วันที่จบ : " /></b><asp:Label ID="lbl_ps_enddate_1" runat="server" Text='<%# Eval("ps_enddate") %>'></asp:Label>

                                                                </p>
                                    
                                                        <b>
                                                            <asp:Label ID="lbl_comment" runat="server" Text="หมายเหตุ : " /></b><asp:Label ID="lbl_ps_comment" runat="server" Text='<%# Eval("ps_comment") %>'></asp:Label>
                                                                </p>
                                                        <b>
                                                            <asp:Label ID="lbl_work_detail" runat="server" Text="รายละเอียดงาน : " /></b><asp:Label ID="lbl_ps_work_detail" runat="server" Text='<%# Eval("ps_work_detail") %>'></asp:Label>
                                                                </p>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="จัดการ">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnDeleteGV" runat="server" ValidationGroup="fromgroup" CommandName="Delete" CssClass="btn btn-danger" OnClientClick="return confirm('Do you want delete this item?')"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                </Columns>

                                            </asp:GridView>
                                        </div>
                                        <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" Visible="false" CommandName="btnSave" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')">Save &nbsp;</asp:LinkButton>
                                        <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-default" OnCommand="btnCommand" CommandName="btnCancel_edit">Cancel &nbsp;</asp:LinkButton>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="ddltypelist" />
                                <asp:PostBackTrigger ControlID="LinkButton2" />
                                <asp:PostBackTrigger ControlID="btnSave" />
                                <asp:PostBackTrigger ControlID="LinkButton1" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </asp:View>

        <asp:View ID="ViewDetail" runat="server">
            <asp:Panel ID="boxsearch_shiftime" runat="server">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i><strong>&nbsp; ค้นหาข้อมูล</strong></h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal" id="BoxSearch" runat="server">
                            <asp:FormView ID="Fv_Search_Shift_Index" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                <InsertItemTemplate>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label105" runat="server" Text="Year" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label106" runat="server" Text="ปี" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:DropDownList ID="ddlyear_search_detail" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="2020">2020</asp:ListItem>
                                                <asp:ListItem Value="2021">2021</asp:ListItem>
                                                <asp:ListItem Value="2022">2022</asp:ListItem>
                                                <asp:ListItem Value="2023">2023</asp:ListItem>
                                                <asp:ListItem Value="2024">2024</asp:ListItem>
                                                <asp:ListItem Value="2025">2025</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label220" runat="server" Text="Month" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label225" runat="server" Text="เดือน" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:DropDownList ID="ddlmonth_search_detail" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือกเดือน....</asp:ListItem>
                                                <asp:ListItem Value="01">มกราคม</asp:ListItem>
                                                <asp:ListItem Value="02">กุมภาพันธ์</asp:ListItem>
                                                <asp:ListItem Value="03">มีนาคม</asp:ListItem>
                                                <asp:ListItem Value="04">เมษายน</asp:ListItem>
                                                <asp:ListItem Value="05">พฤษภาคม</asp:ListItem>
                                                <asp:ListItem Value="06">มิถุนายน</asp:ListItem>
                                                <asp:ListItem Value="07">กรกฏาคม</asp:ListItem>
                                                <asp:ListItem Value="08">สิงหาคม</asp:ListItem>
                                                <asp:ListItem Value="09">กันยายน</asp:ListItem>
                                                <asp:ListItem Value="10">ตุลาคม</asp:ListItem>
                                                <asp:ListItem Value="11">พฤศจิกายน</asp:ListItem>
                                                <asp:ListItem Value="12">ธันวาคม</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-2">
                                            <asp:Button ID="btnsearch_index" class="btn btn-primary" ValidationGroup="Search" runat="server" Text="ค้นหา" data-original-title="search" data-toggle="tooltip" CommandName="btn_search_index" OnCommand="btnCommand" />
                                            <asp:Button ID="btnclearsearch_index" class="btn btn-primary" runat="server" Text="Clear" data-original-title="search" data-toggle="tooltip" CommandName="Cmdsearch_clear" OnCommand="btnCommand" />
                                        </div>
                                    </div>

                                </InsertItemTemplate>
                            </asp:FormView>
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <asp:Panel ID="box_detail_back" runat="server" Visible="false">
                <div class="row">
                    <div class="form-group">
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <asp:LinkButton ID="btnBackToDetailOTDay" CssClass="btn btn-success" data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                                    CommandName="btnedit_back" OnCommand="btnCommand" CommandArgument="2">< ย้อนกลับ</asp:LinkButton>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnBackToDetailOTDay" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </asp:Panel>
            <!-- Back To Detail To Day -->


            <asp:Panel ID="boxgv_index_list" runat="server">
                <asp:GridView ID="GvIndex"
                    DataKeyNames="ps_u0_idx"
                    runat="server"
                    AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered table-hover table-responsive"
                    HeaderStyle-CssClass="table_headCenter"
                    HeaderStyle-BackColor="#DCF0DA"
                    HeaderStyle-Height="40px"
                    AllowPaging="True"
                    PageSize="10"
                    OnPageIndexChanging="Master_PageIndexChanging"
                    OnRowDataBound="Master_RowDataBound">

                    <HeaderStyle CssClass="info" />
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                    </EmptyDataTemplate>

                    <Columns>
                        <asp:TemplateField HeaderText="วันที่">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="lbl_node_u0_unidx_detail" Visible="false" runat="server" CssClass="col-sm-10" Text='<%# Eval("u0_unidx") %>'></asp:Label>
                                    <asp:Label ID="lbl_node_u0_acidx_detail" Visible="false" runat="server" CssClass="col-sm-10" Text='<%# Eval("u0_acidx") %>'></asp:Label>
                                    <asp:Label ID="lbl_node_u0_doc_decision" Visible="false" runat="server" CssClass="col-sm-10" Text='<%# Eval("doc_decision") %>'></asp:Label>
                                    <b>
                                        <asp:Label ID="Label1" runat="server" Text="วันที่สร้างรายการ : " /></b><span><%# formatDateTime((String)Eval("create_date")) %></span>
                                    </p>                                     
                                        <b>
                                            <asp:Label ID="lblemp_name_th_" runat="server" Text="วันที่ปฏิบัติงาน : " /></b><span><%# formatDateTime((String)Eval("ps_startdate")) %> - <span><%# formatDateTime((String)Eval("ps_enddate")) %></span>
                                            </p>                                     
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="รายละเอียด">
                            <ItemTemplate>
                                <small>
                                    <span><%# Eval("ps_comment") %></span>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สถานะ">
                            <ItemTemplate>
                                <asp:Label ID="lblstatus" runat="server" Text='<%# getStatus((int)Eval("u0_unidx"),(int)Eval("u0_acidx"),(int)Eval("doc_decision")) %>'></asp:Label>
                                <%--<span><%# Eval("status_name") %>&nbsp;<%# Eval("node_name") %>&nbsp;โดย&nbsp;<%# Eval("actor_name") %></span>--%>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="จัดการ" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="1%">
                            <ItemTemplate>
                                <small>
                                    <asp:LinkButton ID="btnedit" CssClass="btn btn-primary" runat="server" CommandName="btnedit_detail" OnCommand="btnCommand" data-toggle="tooltip" title="แก้ไขข้อมูล" CommandArgument='<%#Eval("ps_u0_idx") + ";" + Eval("u0_unidx") + ";" + Eval("u0_acidx") + ";" + Eval("doc_decision") %>'><i class="fa fa-cog"></i></asp:LinkButton>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </asp:Panel>

            <asp:Panel ID="boxfv_detail_list" runat="server" Visible="false">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; รายละเอียดข้อมูล</strong></h3>
                    </div>
                    <!-- Back To Detail To Day -->

                    <asp:FormView ID="fvPlanList" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <EditItemTemplate>
                            <div class="form-horizontal" role="form">
                                <div class="form-group"></div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">วันที่ทำรายการ</label>

                                    <div class="col-sm-3">
                                        <asp:TextBox ID="tbDocCreateE" runat="server" CssClass="form-control" Enabled="false" Text='<%# formatDateTime((String)Eval("create_date")) %>' ValidationGroup="formEdit"></asp:TextBox>
                                        <asp:Label ID="lbl_ps_u0_idx" runat="server" Visible="false" Text='<%# Eval("ps_u0_idx") %>' />
                                    </div>
                                    <label class="col-sm-2 control-label">เวลาเข้าทำงาน</label>
                                    <div class="col-sm-3">
                                        <asp:Label ID="lbl_emp_shift" runat="server" Visible="false" Text='<%# Eval("emp_shift") %>' />
                                        <asp:DropDownList ID="ddlShiftType_" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="0">ประเภทรายการ....</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="ReShiftTypeE" ValidationGroup="formEdit" runat="server" Display="None"
                                            ControlToValidate="ddlShiftType_" Font-Size="11"
                                            ErrorMessage="Select ShiftType"
                                            ValidationExpression="Select ShiftType" InitialValue="-1" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCddalldoutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReShiftTypeE" Width="160" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ลาตั้งแต่วันที่</label>
                                    <div class="col-sm-3">
                                        <div class='input-group date'>
                                            <asp:TextBox ID="tbDocStartE" runat="server" CssClass="form-control from-date-datepicker" Text='<%# formatDateTime((String)Eval("ps_startdate")) %>' ValidationGroup="formEdit"></asp:TextBox><span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                            <asp:RequiredFieldValidator ID="ReDocStartE" ValidationGroup="formEdit" runat="server" Display="None"
                                                ControlToValidate="tbDocStartE" Font-Size="11"
                                                ErrorMessage="Plese DocStartE" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCffalfloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReDocStartE" Width="160" />
                                        </div>
                                    </div>
                                    <asp:Label class="col-sm-2 control-label" ID="lblDocEndE" runat="server">ถึงวันที่</asp:Label>
                                    <div class="col-sm-3">
                                        <div class='input-group date'>
                                            <asp:TextBox ID="tbDocEndE" runat="server" CssClass="form-control from-date-datepicker" Text='<%# formatDateTime((String)Eval("ps_enddate")) %>' ValidationGroup="formEdit"></asp:TextBox><span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                            <asp:RequiredFieldValidator ID="ReDocEndE" ValidationGroup="formEdit" runat="server" Display="None"
                                                ControlToValidate="tbDocEndE" Font-Size="11"
                                                ErrorMessage="Plese DocEndE" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCfdalloutdfExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReDocEndE" Width="160" />
                                        </div>
                                    </div>
                                    <asp:CompareValidator runat="server" ID="cmpNumbersE" ControlToValidate="tbDocEndE" ControlToCompare="tbDocStartE" Operator="GreaterThan" Type="String" ValidationGroup="formEdit" Display="None"
                                        ErrorMessage="กรุณาเลือกวันและเวลาที่ถูกต้อง" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCallodutddExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="cmpNumbersE" Width="160" />
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ระบุเหตุผลการลา</label>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="tbDocRemarkE" runat="server" CssClass="form-control" Enabled="true" MaxLength="250" Text='<%# Eval("ps_comment") %>' ValidationGroup="formEdit"></asp:TextBox>
                                        <%--<asp:RequiredFieldValidator ID="ReDocRemarkE" ValidationGroup="formEdit" runat="server" Display="None"
                                                ControlToValidate="tbDocRemarkE" Font-Size="11"
                                                ErrorMessage="Plese DocRemarkE" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValifdatdorCalloutfgExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReDocRemarkE" Width="160" />--%>
                                    </div>
                                </div>

                                <%--<div class="form-group">
                                            <div class="col-lg-12">
                                                <asp:Label ID="lbSelectFile" CssClass="col-sm-2 control-label" runat="server" Text="เลือกไฟล์รูปภาพ : " />

                                                <div class="col-sm-5">
                                                    <asp:FileUpload ID="UploadImages" ClientIDMode="Static" runat="server" CssClass="control-label multi" accept="gif|jpg|png" />
                                                    <p class="help-block"><font color="red">**แนบเอกสารเพิ่มเติม เฉพาะนามสกุล gif , jpg , png</font></p>
                                                </div>
                                            </div>

                                        </div>--%>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-5">
                                        <asp:LinkButton ID="lbUpdate" CssClass="btn btn-success" runat="server" data-original-title="บันทึก" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdUpdate" Text="บันทึก" ValidationGroup="formEdit" OnClientClick="return confirm('คุณต้องการบันทึกรายการใช่หรือไม่ ?')"><span class="fa fa-save" aria-hidden="true" > บันทึก</span></asp:LinkButton>
                                        <asp:LinkButton ID="btncancel_user_update" CssClass="btn btn-warning" runat="server" data-original-title="ยกเลิกรายการลา" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdcancel_user" Text="ยกเลิกรายการลา" ValidationGroup="formItem" OnClientClick="return confirm('คุณต้องการยกเลิกรายการใช่หรือไม่ ?')"><span class="fa fa-save" aria-hidden="true"> ยกเลิกรายการลา</span></asp:LinkButton>
                                        <asp:LinkButton ID="lbCancel" CssClass="btn btn-default" runat="server" data-original-title="ยกเลิก" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="1" Text="ยกเลิก" ValidationGroup="formCancel"><span class="fa fa-times" aria-hidden="true"> กลับหน้าหลัก</span></asp:LinkButton>
                                    </div>

                                    <%--<script type="text/javascript">
                                            function btnSubmit_Edit_Click() {

                                                var T1 = "วันที่เขียนใบลา";
                                                var D1 = document.getElementById("<%=fvLONDoc.FindControl("tbDocCreateE").ClientID %>").value;
                                                var T2 = "ประเภทการลา";
                                                var D2 = document.getElementById("<%=fvLONDoc.FindControl("ddlLeaveTypeE").ClientID %>");
                                                var D22 = D2.options[D2.selectedIndex].text;
                                                var T3 = "เวลาเข้าทำงาน";
                                                var D3 = document.getElementById("<%=fvLONDoc.FindControl("ddlShiftTypeE").ClientID %>");
                                                var D33 = D3.options[D3.selectedIndex].text;
                                                var T4 = "ลาตั้งแต่วันที่";
                                                var D4 = document.getElementById("<%=fvLONDoc.FindControl("tbDocStartE").ClientID %>").value;
                                                var T5 = "ถึงวันที่";
                                                var D5 = document.getElementById("<%=fvLONDoc.FindControl("tbDocEndE").ClientID %>").value;
                                                var T6 = "ระบุเหตุผลการลา";
                                                var D6 = document.getElementById("<%=fvLONDoc.FindControl("tbDocRemarkE").ClientID %>").value;

                                                var report = T1 + " : " + D1 + "\n" + T2 + " : " + D22 + "\n" + T3 + " : " + D33 + "\n" + T4 + " : " + D4 + " - " + D5 + "\n" + T6 + " : " + D6;
                                                return confirm(report);

                                            }
                                        </script>--%>
                                    <%--OnClientClick="return btnSubmit_Edit_Click();"--%>
                                    <!-- <label class="col-sm-2 control-label"></label>
					                <div class="col-sm-3">
					                </div> -->
                                </div>
                            </div>
                        </EditItemTemplate>
                    </asp:FormView>
                </div>

                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i><strong>&nbsp; Log / ประวัติการดำเนินการ</strong></h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal" role="form">
                            <asp:Repeater ID="rptLog" runat="server">
                                <HeaderTemplate>
                                    <div class="form-group">
                                        <label class="col-sm-2" style="text-align: center;"><small>วัน / เวลา</small></label>
                                        <label class="col-sm-3" style="text-align: left;"><small>ผู้ดำเนินการ</small></label>
                                        <label class="col-sm-2" style="text-align: left;"><small>ดำเนินการ</small></label>
                                        <label class="col-sm-2" style="text-align: left;"><small>ผลการดำเนินการ</small></label>
                                        <label class="col-sm-2" style="text-align: left;"><small>ความคิดเห็น</small></label>
                                    </div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <span><small><%#Eval("create_date")%></small></span>
                                        </div>
                                        <div class="col-sm-3">
                                            <span><small><%# Eval("emp_name_th") %></small></span>
                                        </div>
                                        <div class="col-sm-2">
                                            <span><small><%# Eval("node_name") %></small></span>
                                        </div>
                                        <div class="col-sm-2">
                                            <span><small><%# Eval("status_name") %></small></span>
                                        </div>
                                        <div class="col-sm-2">
                                            <span><small><%# Eval("comment_approve") %></small></span>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </asp:Panel>


        </asp:View>

        <asp:View ID="ViewFileUpload" runat="server">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i><strong>&nbsp; Upload File</strong></h3>
                </div>
                <div class="panel-body">
                    <div class="form-horizontal" id="Div1" runat="server">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">
                                <h2 class="panel-title">
                                    <asp:Label ID="Label1" runat="server" Text="Month" /><br />
                                    <small><b>
                                        <asp:Label ID="Label2" runat="server" Text="เดือน" /></b></small>
                                </h2>
                            </label>

                            <div class="col-sm-2">
                                <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="ddlmonth" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="0">เลือกเดือน....</asp:ListItem>
                                            <asp:ListItem Value="01">มกราคม</asp:ListItem>
                                            <asp:ListItem Value="02">กุมภาพันธ์</asp:ListItem>
                                            <asp:ListItem Value="03">มีนาคม</asp:ListItem>
                                            <asp:ListItem Value="04">เมษายน</asp:ListItem>
                                            <asp:ListItem Value="05">พฤษภาคม</asp:ListItem>
                                            <asp:ListItem Value="06">มิถุนายน</asp:ListItem>
                                            <asp:ListItem Value="07">กรกฏาคม</asp:ListItem>
                                            <asp:ListItem Value="08">สิงหาคม</asp:ListItem>
                                            <asp:ListItem Value="09">กันยายน</asp:ListItem>
                                            <asp:ListItem Value="10">ตุลาคม</asp:ListItem>
                                            <asp:ListItem Value="11">พฤศจิกายน</asp:ListItem>
                                            <asp:ListItem Value="12">ธันวาคม</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="Rewwq21" ValidationGroup="savefile_picture" runat="server" Display="None"
                                            ControlToValidate="ddlmonth" Font-Size="11"
                                            ErrorMessage="เลือกเดือน ...."
                                            ValidationExpression="เลือกเดือน ...." InitialValue="0" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="Va2li3der4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Rewwq21" Width="160" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <div class="col-sm-1">
                                <asp:Label ID="Label437" runat="server" ForeColor="Red" Text="***" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">
                                <h2 class="panel-title">
                                    <asp:Label ID="Label5" runat="server" Text="Year" /><br />
                                    <small><b>
                                        <asp:Label ID="Label6" runat="server" Text="ปี" /></b></small>
                                </h2>
                            </label>

                            <div class="col-sm-2">
                                <asp:DropDownList ID="ddlyear" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="2020">2020</asp:ListItem>
                                    <asp:ListItem Value="2021">2021</asp:ListItem>
                                    <asp:ListItem Value="2022">2022</asp:ListItem>
                                    <asp:ListItem Value="2023">2023</asp:ListItem>
                                    <asp:ListItem Value="2024">2024</asp:ListItem>
                                    <asp:ListItem Value="2025">2025</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="Requiredaadator1" ValidationGroup="savefile_picture" runat="server" Display="None"
                                    ControlToValidate="ddlyear" Font-Size="11"
                                    ErrorMessage="เลือกปี ...."
                                    ValidationExpression="เลือกปี ...." InitialValue="0" />
                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredaadator1" Width="160" />
                            </div>
                            <div class="col-sm-1">
                                <asp:Label ID="Label9" runat="server" ForeColor="Red" Text="***" />
                            </div>
                        </div>
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                            <ContentTemplate>
                                <div class="form-group">

                                    <label class="col-sm-2 control-label">
                                        <h2 class="panel-title">
                                            <asp:Label ID="Label3" runat="server" Text="File" /><br />
                                            <small><b>
                                                <asp:Label ID="Label4" runat="server" Text="เอกสารแนบ" /></b></small>
                                        </h2>
                                    </label>

                                    <label class="col-sm-2">
                                        <%--<asp:FileUpload ID="UploadImages" ClientIDMode="Static" runat="server" CssClass="control-label" accept="gif|jpg|png|pdf" />--%>
                                        <asp:FileUpload ID="UploadImages" ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small" ClientIDMode="Static" runat="server" CssClass="btn btn-sm multi" accept="gif|jpg|png|JPEG|pdf" />
                                        <p class="help-block"><font color="red">**เฉพาะนามสกุล gif , jpg , png, pdf</font></p>
                                    </label>

                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-2">
                                        <asp:LinkButton ID="btn_save_file1" runat="server" CssClass="btn btn-warning btn-sm" data-toggle="tooltip" title="อัพโหลดภาพ" CommandName="savefile_picture" OnCommand="btnCommand"><i class="glyphicon glyphicon-picture"></i> อัพโหลดภาพ</asp:LinkButton>
                                        <asp:Button ID="btnclearsearch_index" class="btn btn-primary" runat="server" Visible="false" Text="Clear" data-original-title="search" data-toggle="tooltip" CommandName="Cmdsearch_clear" OnCommand="btnCommand" />
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btn_save_file1" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <hr />
                        <label class="col-sm-2 control-label">
                            <h2 class="panel-title">
                                <asp:Label ID="Labfel223" runat="server" Text="Document List" /><br />
                                <small><b>
                                    <asp:Label ID="Lasb3el30" runat="server" Text="เอกสารแนบทั้งหมด" /></b></small>
                            </h2>
                        </label>
                        <div class="col-sm-3">
                            <asp:GridView ID="gvFileEmp_edit" Visible="true" runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive"
                                HeaderStyle-CssClass="warning"
                                OnRowDataBound="Master_RowDataBound"
                                BorderStyle="None"
                                CellSpacing="2"
                                Font-Size="Small">
                                <HeaderStyle CssClass="info" />
                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>

                                <Columns>
                                    <asp:TemplateField HeaderText="ไฟล์เอกสารเพิ่มเติม">
                                        <ItemTemplate>
                                            <div class="col-lg-8">
                                                <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />

                                            </div>
                                            <div class="col-lg-2">
                                                <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-default" data-original-title="Download" Target="_blank"><i class="fa fa-download"></i></asp:HyperLink>

                                                <asp:HiddenField runat="server" ID="hidFile11" Value='<%# Eval("Download") %>' />
                                            </div>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </div>

                    </div>
                </div>
            </div>
        </asp:View>

        <asp:View ID="ViewApprove" runat="server">
            <asp:Label ID="ffsa" runat="server"></asp:Label>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i><strong>&nbsp; ค้นหาข้อมูล</strong></h3>
                </div>
                <div class="panel-body">
                    <div class="form-horizontal" id="Div3" runat="server">
                        <asp:FormView ID="Fv_Search_approve" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                            <InsertItemTemplate>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">
                                        <h2 class="panel-title">
                                            <asp:Label ID="Label38" runat="server" Text="Employee Code" /><br />
                                            <small><b>
                                                <asp:Label ID="Label39" runat="server" Text="รหัสพนักงาน" /></b></small>
                                        </h2>
                                    </label>

                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txtempcode_search_approve" CssClass="form-control" TextMode="MultiLine" placeholder="รหัสพนักงาน Ex. 57000001,57000002" runat="server" ValidationGroup="Search"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-2">
                                        <asp:Button ID="btnsearch_approve" class="btn btn-primary" ValidationGroup="Search" runat="server" Text="ค้นหา" data-original-title="search" data-toggle="tooltip" CommandName="btn_search_approve" OnCommand="btnCommand" />
                                        <asp:Button ID="btnclearsearch_index" class="btn btn-primary" runat="server" Text="Clear" data-original-title="search" data-toggle="tooltip" CommandName="Cmdsearch_clear" OnCommand="btnCommand" />
                                    </div>
                                </div>

                            </InsertItemTemplate>
                        </asp:FormView>
                    </div>
                </div>
            </div>



            <asp:Panel ID="boxGvApprove" runat="server">
                <asp:GridView ID="GvApproveAll"
                    runat="server"
                    CssClass="table table-striped table-bordered table-hover table-responsive"
                    HeaderStyle-CssClass="table_headCenter"
                    AutoGenerateColumns="false"
                    AllowPaging="true"
                    PageSize="300"
                    OnRowDataBound="Master_RowDataBound">

                    <HeaderStyle CssClass="info" />
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Position="Bottom" Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                    <EmptyDataTemplate>
                        ไม่พบข้อมูล
                    </EmptyDataTemplate>
                    <Columns>

                        <asp:TemplateField HeaderText="#" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <%# (Container.DataItemIndex +1) %>
                                    <asp:Label ID="approve_empidx" Visible="false" runat="server" CssClass="col-sm-10" Text='<%# Eval("emp_idx") %>'></asp:Label>
                                    <asp:Label ID="approve_uidx" Visible="false" runat="server" CssClass="col-sm-10" Text='<%# Eval("ps_u0_idx") %>'></asp:Label>
                                    <asp:Label ID="approve_empcode" Visible="false" runat="server" CssClass="col-sm-10" Text='<%# Eval("emp_code") %>'></asp:Label>
                                    <asp:Label ID="lbl_node_u0_unidx" Visible="false" runat="server" CssClass="col-sm-10" Text='<%# Eval("u0_unidx") %>'></asp:Label>
                                    <asp:Label ID="lbl_node_u0_acidx" Visible="false" runat="server" CssClass="col-sm-10" Text='<%# Eval("u0_acidx") %>'></asp:Label>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ข้อมูลทั่วไป" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <b>
                                        <asp:Label ID="Label2" runat="server" Text="รหัสพนักงาน : " /></b><span><%# Eval("emp_code") %></span>
                                    </p> 
                                    <b>
                                        <asp:Label ID="Label1" runat="server" Text="ชื่อ-สกุล : " /></b><span><%# Eval("emp_name_th") %></span>
                                    </p>                           
                                    <b>
                                        <asp:Label ID="Labsel1" runat="server" Text="เริ่มวันที่ : " /></b><span><%# formatDateTime((String)Eval("ps_startdate")) %></span>
                                    </p>                                     
                                        <b>
                                            <asp:Label ID="lblempd" runat="server" Text="ถึงวันที่ : " /></b><span><%# formatDateTime((String)Eval("ps_enddate")) %></span>
                                    </p>
                                        <b>
                                            <asp:Label ID="lblemp_name_th_" runat="server" Text="กะการทำงาน : " /></b><span><%# Eval("TypeWork") %></span>
                                    </p>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="รายละเอียดงาน" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <span><%# Eval("ps_comment") %></span>
                                </small>
                                <br />

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="อนุมัติ" HeaderStyle-Font-Size="Small">
                            <HeaderTemplate>
                                <small>
                                    <asp:CheckBox ID="chkApproveAll" runat="server" Text="&nbspAll (อนุมัติ)" AutoPostBack="true" OnCheckedChanged="chkSelectedIndexChanged"></asp:CheckBox>
                                </small>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <small>
                                    <asp:CheckBox ID="YrChkBox_Approve" runat="server"></asp:CheckBox>
                                    <asp:Image ID="img_profile1" runat="server" HorizontalAlign="Center"></asp:Image>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ไม่อนุมัติ(แก้ไข)" HeaderStyle-Font-Size="Small">
                            <HeaderTemplate>
                                <small>
                                    <asp:CheckBox ID="chkApproveAll_edit" runat="server" Text="&nbspAll (แก้ไข)" AutoPostBack="true" OnCheckedChanged="chkSelectedIndexChanged" />
                                </small>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <small>
                                    <asp:CheckBox ID="YrChkBox_edit" runat="server" />
                                    <asp:Image ID="img_profile2" runat="server" HorizontalAlign="Center"></asp:Image>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ไม่อนุมัติ(ปิดเอกสาร)" HeaderStyle-Font-Size="Small">
                            <HeaderTemplate>
                                <small>
                                    <asp:CheckBox ID="chkApproveAll_no" runat="server" Text="&nbspAll (ปิดเอกสาร)" AutoPostBack="true" OnCheckedChanged="chkSelectedIndexChanged" />
                                </small>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <small>
                                    <asp:CheckBox ID="YrChkBox_no" runat="server" />
                                    <asp:Image ID="img_profile3" runat="server" HorizontalAlign="Center"></asp:Image>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="หมายเหตุ" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:TextBox ID="txtcommentapproveall" CssClass="form-control" runat="server"></asp:TextBox>
                                </small>
                                </p>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>

                <div class="form-group">
                    <div class="col-sm-3">
                        <asp:LinkButton ID="LinkButton3" CssClass="btn btn-success" runat="server" data-original-title="บันทึก" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdApprove_All" Text="บันทึก" OnClientClick="return confirm('คุณต้องการบันทึกใช่หรือไม่ ?')"><span class="fa fa-save" aria-hidden="true"></span> บันทึก</asp:LinkButton>
                        <asp:LinkButton ID="LinkButton4" CssClass="btn btn-default" runat="server" data-original-title="ยกเลิก" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdCancel_approveall" CommandArgument="7" Text="ยกเลิก"><span class="fa fa-times" aria-hidden="true"></span> ยกเลิก</asp:LinkButton>
                    </div>
                </div>

            </asp:Panel>
        </asp:View>

        <asp:View ID="ViewReport" runat="server">
            <asp:Label ID="ffd" runat="server"></asp:Label>
            <asp:Label ID="fsa" runat="server"></asp:Label>
            <asp:Panel ID="Panel2" runat="server">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i><strong>&nbsp; ค้นหาข้อมูล</strong></h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal" id="Div2" runat="server">
                            <asp:FormView ID="Fv_Search_Emp_Report" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                <InsertItemTemplate>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label105" runat="server" Text="Year" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label106" runat="server" Text="ปี" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:DropDownList ID="ddlyear_search" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="2020">2020</asp:ListItem>
                                                <asp:ListItem Value="2021">2021</asp:ListItem>
                                                <asp:ListItem Value="2022">2022</asp:ListItem>
                                                <asp:ListItem Value="2023">2023</asp:ListItem>
                                                <asp:ListItem Value="2024">2024</asp:ListItem>
                                                <asp:ListItem Value="2025">2025</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="Rew1" ValidationGroup="Search" runat="server" Display="None"
                                                ControlToValidate="ddlyear_search" Font-Size="11"
                                                ErrorMessage="เลือกปี ...."
                                                ValidationExpression="เลือกปี ...." InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Rew1" Width="160" />
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:Label ID="Label437" runat="server" ForeColor="Red" Text="***" />
                                        </div>

                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label220" runat="server" Text="Month" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label225" runat="server" Text="เดือน" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:DropDownList ID="ddlmonth_search" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือกเดือน....</asp:ListItem>
                                                <asp:ListItem Value="01">มกราคม</asp:ListItem>
                                                <asp:ListItem Value="02">กุมภาพันธ์</asp:ListItem>
                                                <asp:ListItem Value="03">มีนาคม</asp:ListItem>
                                                <asp:ListItem Value="04">เมษายน</asp:ListItem>
                                                <asp:ListItem Value="05">พฤษภาคม</asp:ListItem>
                                                <asp:ListItem Value="06">มิถุนายน</asp:ListItem>
                                                <asp:ListItem Value="07">กรกฏาคม</asp:ListItem>
                                                <asp:ListItem Value="08">สิงหาคม</asp:ListItem>
                                                <asp:ListItem Value="09">กันยายน</asp:ListItem>
                                                <asp:ListItem Value="10">ตุลาคม</asp:ListItem>
                                                <asp:ListItem Value="11">พฤศจิกายน</asp:ListItem>
                                                <asp:ListItem Value="12">ธันวาคม</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="Rewwqd21" ValidationGroup="Search" runat="server" Display="None"
                                                ControlToValidate="ddlmonth_search" Font-Size="11"
                                                ErrorMessage="เลือกเดือน ...."
                                                ValidationExpression="เลือกเดือน ...." InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="Va2li3der4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Rewwqd21" Width="160" />
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:Label ID="Label7" runat="server" ForeColor="Red" Text="***" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label38" runat="server" Text="Employee Code" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label39" runat="server" Text="รหัสพนักงาน" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txtempcode_search" CssClass="form-control" runat="server" ValidationGroup="Search"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="Reqlidator1" ValidationGroup="Search" runat="server" Display="None"
                                                ControlToValidate="txtempcode_search" Font-Size="11"
                                                ErrorMessage="รหัสพนักงาน ...."
                                                ValidationExpression="รหัสพนักงาน ...." />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Reqlidator1" Width="160" />
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:Label ID="Label8" runat="server" ForeColor="Red" Text="***" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-4">
                                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                <ContentTemplate>
                                                    <asp:LinkButton ID="btnsearch_report" class="btn btn-primary" ValidationGroup="Search" runat="server" Text="Search" data-original-title="search" data-toggle="tooltip" CommandName="btn_search_report" OnCommand="btnCommand" Visible="true" />
                                                    <asp:Button ID="btnclearsearch_index1" class="btn btn-primary" runat="server" Text="Clear" data-original-title="search" data-toggle="tooltip" CommandName="Cmdsearch_clear" OnCommand="btnCommand" />
                                                    <asp:Button ID="btnexport_general" CssClass="btn btn-success" runat="server" data-toggle="tooltip" Text="Export Excel (ข้อมูลทั่วไป)" CommandName="btnexport_general" OnCommand="btnCommand" title="Export"></asp:Button>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="btnsearch_report" />
                                                    <asp:PostBackTrigger ControlID="btnexport_general" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <asp:Label ID="Label10" runat="server" ForeColor="Red" Text="*** ตัวอย่างการตัดรอบเฉพาะพนักงาน PC ต่างจังหวัด ของรายเดือน(TKN) เช่น 16/01/2563 - 15/02/2563 ถือว่าเป็นรอบของเดือนกุมภาพันธ์ " />
                                    </div>

                                </InsertItemTemplate>
                            </asp:FormView>
                        </div>

                    </div>
                </div>
            </asp:Panel>

            <div class="form-group">
                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="Gv_report"
                            runat="server"
                            AutoGenerateColumns="false"
                            CssClass="table table-striped table-bordered table-hover"
                            HeaderStyle-CssClass="table_headCenter"
                            HeaderStyle-Height="40px"
                            ShowFooter="true"
                            ShowHeaderWhenEmpty="True"
                            AllowPaging="True"
                            PageSize="100"
                            BorderStyle="None"
                            CellSpacing="2"
                            OnRowDataBound="Master_RowDataBound">

                            <PagerStyle CssClass="PageCustom" />
                            <HeaderStyle CssClass="info" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                            <EmptyDataTemplate>
                                <div style="text-align: center">Data Cannot Be Found</div>
                            </EmptyDataTemplate>

                            <Columns>

                                <asp:TemplateField HeaderText="Approve" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <asp:CheckBox ID="YrChkBox_Approve1" runat="server" Visible="false"></asp:CheckBox>
                                            <center><b><asp:Label ID="status_approve" runat="server" ItemStyle-HorizontalAlign="Center" CssClass="AlgRgh" ></asp:Label></b></center>
                                            <asp:Label ID="lblu0_unidx" runat="server" CssClass="col-sm-10" Visible="false" Text='<%# Eval("u0_unidx") %>'></asp:Label>
                                            <asp:Label ID="lblu0_acidx" runat="server" CssClass="col-sm-10" Visible="false" Text='<%# Eval("u0_acidx") %>'></asp:Label>
                                            <asp:Label ID="lbldoc_status" runat="server" CssClass="col-sm-10" Visible="false" Text='<%# Eval("doc_status") %>'></asp:Label>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="วันที่" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <asp:Label ID="lbldate_run" runat="server" CssClass="col-sm-10" Text='<%# Eval("ps_u1_idx") %>'></asp:Label>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="เวลาเข้างาน" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <asp:Label ID="lbldate_start" runat="server" CssClass="col-sm-10" Text='<%# Eval("ps_startdate") %>'></asp:Label>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="เวลาออก" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <asp:Label ID="lbldate_end" runat="server" CssClass="col-sm-10" Text='<%# Eval("ps_enddate") %>'></asp:Label>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ร้านค้าที่เข้าเยี่ยม" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <asp:Label ID="lbletc_comment" runat="server" CssClass="col-sm-10" Text='<%# Eval("ps_comment") %>'></asp:Label>
                                        </small>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <div style="text-align: right;">
                                            <%--<asp:Label ID="lit_sum" runat="server" Text="รวมทั้งหมด :"></asp:Label>--%>
                                        </div>
                                    </FooterTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="โอที 1 เท่า (ทำวันหยุด)" HeaderStyle-Font-Size="Small" Visible="false">
                                    <ItemTemplate>
                                        <center><small>
                                            <asp:Label ID="lbl_ot_x1_reportmonth" Visible="true" runat="server" Text='<%# Eval("ot_x1") %>' />
                                        </small></center>
                                    </ItemTemplate>

                                    <FooterTemplate>
                                        <div style="text-align: right; background-color: chartreuse;">
                                            <asp:Label ID="lit_ot_x1_all_sum" runat="server" Font-Bold="true"></asp:Label>
                                        </div>
                                    </FooterTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="โอที 1.5 เท่า (ทำเกินเวลา)" HeaderStyle-Font-Size="Small" Visible="false">
                                    <ItemTemplate>
                                        <center><small><asp:Label ID="lbl_ot_x15_reportmonth" Visible="true" runat="server" Text='<%# Eval("ot_x15") %>' /></small></center>
                                    </ItemTemplate>

                                    <FooterTemplate>
                                        <div style="text-align: right; background-color: chartreuse;">
                                            <asp:Label ID="lit_ot_x15_all_sum" runat="server" Font-Bold="true"></asp:Label>
                                        </div>
                                    </FooterTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="โอที 3 เท่า (ทำเกินเวลาในวันหยุด)" HeaderStyle-Font-Size="Small" Visible="false">
                                    <ItemTemplate>
                                        <center><small><asp:Label ID="lbl_ot_x3_reportmonth" Visible="true" runat="server" Text='<%# Eval("ot_x3") %>' /></small></center>
                                    </ItemTemplate>

                                    <FooterTemplate>
                                        <div style="text-align: right; background-color: chartreuse;">
                                            <asp:Label ID="lit_ot_x3_all_sum" runat="server" Font-Bold="true"></asp:Label>
                                        </div>
                                    </FooterTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="X (หากเป็นวันหยุดประจำสัปดาห์)" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <asp:Label ID="lblday_off" runat="server" CssClass="col-sm-10" Text='<%# Eval("day_off") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblday_off_shift" runat="server" CssClass="col-sm-10" Text='<%# Eval("day_off_shift") %>' Visible="false"></asp:Label>
                                            <center><b><asp:Label ID="lblstatus_holiday" runat="server" CssClass="col-sm-10"></asp:Label></b></center>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ลาหยุด (ระบุสาเหตุ)" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <asp:Label ID="lblleave_type" runat="server" CssClass="col-sm-10" Text='<%# Eval("M0LeaveTypeName") %>'></asp:Label>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="หมายเหตุ" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small></small>
                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>

            <div class="form-group">
                <asp:Panel ID="box_gvfile_report" Visible="false" runat="server">
                    <asp:GridView ID="Gv_file_report" runat="server"
                        AutoGenerateColumns="false"
                        CssClass="table table-striped table-bordered table-hover table-responsive"
                        HeaderStyle-CssClass="warning"
                        OnRowDataBound="Master_RowDataBound"
                        BorderStyle="None"
                        CellSpacing="2"
                        Font-Size="Small">
                        <HeaderStyle CssClass="info" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">Data Cannot Be Found</div>
                        </EmptyDataTemplate>

                        <Columns>
                            <asp:TemplateField HeaderText="ไฟล์เอกสารเพิ่มเติม">
                                <ItemTemplate>
                                    <div class="col-lg-8">
                                        <asp:Literal ID="ltFileName112" runat="server" Text='<%# Eval("FileName") %>' />

                                    </div>
                                    <div class="col-lg-2">
                                        <asp:HyperLink runat="server" ID="btnDL112" CssClass="btn btn-default" data-original-title="Download" Target="_blank"><i class="fa fa-download"></i></asp:HyperLink>

                                        <asp:HiddenField runat="server" ID="hidFile112" Value='<%# Eval("Download") %>' />
                                    </div>

                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>
                </asp:Panel>
            </div>
        </asp:View>

    </asp:MultiView>

    <script src='<%= ResolveUrl("~/Scripts/script-dar-plansale.js") %>'></script>

    <script>
        $(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY HH:mm'
            });
        });
        //});

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY HH:mm'
                });
            });
        });

    </script>

    <script type="text/javascript">
        function confirmWithOutValidated(message, validateGroup) {
            if (validateGroup != '') {
                var validated = Page_ClientValidate(validateGroup);
                if (validated) {
                    return confirm(message);
                }
            } else {
                return confirm(message);
            }
        }
        $(function () {
            $('.datetimepicker-from').datetimepicker({

                format: 'DD/MM/YYYY',
                minDate: moment().add(-1, 'day'),
                maxDate: moment().add(30, 'day'),
                //ignoreReadonly: true

            });
            $('.datetimepicker-from').on('dp.change', function (e) {
                var dateTo = $('.datetimepicker-to').data("DateTimePicker").date();
                if (e.date > dateTo) {
                    $('.datetimepicker-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                //else {
                //    $('.datetimepicker-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                //}

            });
            $('.show-from-onclick').click(function () {
                $('.datetimepicker-from').data("DateTimePicker").show();
            });
            $('.datetimepicker-to').datetimepicker({
                format: 'DD/MM/YYYY',
                minDate: moment().add(-1, 'day'),
                maxDate: moment().add(30, 'day'),
                //ignoreReadonly: true
            });
            $('.show-to-onclick').click(function () {
                $('.datetimepicker-to').data("DateTimePicker").show();
            });
            $('.datetimepicker-to').on('dp.change', function (e) {
                var dateFrom = $('.datetimepicker-from').data("DateTimePicker").date();
                if (e.date < dateFrom) {
                    $('.datetimepicker-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                //else {
                //    $('.datetimepicker-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));

                //}
                //$('.txtSearchToHidden').val($('.datetimepicker-to').val());
                <%--$('#<%= txtSearchToHidden.ClientID %>').val($('.datetimepicker-to').val());--%>
            });
        });
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            //var dateToday = new Date();
            //currentTime = new Date();
            $('.datetimepicker-from').datetimepicker({

                format: 'DD/MM/YYYY',
                minDate: moment().add(-1, 'day'),
                maxDate: moment().add(30, 'day'),
                //ignoreReadonly: true

            });
            $('.datetimepicker-from').on('dp.change', function (e) {
                var dateTo = $('.datetimepicker-to').data("DateTimePicker").date();
                if (e.date > dateTo) {
                    $('.datetimepicker-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                //else {
                //    $('.datetimepicker-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                //}

            });
            $('.show-from-onclick').click(function () {
                $('.datetimepicker-from').data("DateTimePicker").show();
            });
            $('.datetimepicker-to').datetimepicker({
                format: 'DD/MM/YYYY',
                minDate: moment().add(-1, 'day'),
                maxDate: moment().add(30, 'day'),
                //ignoreReadonly: true
            });
            $('.show-to-onclick').click(function () {
                $('.datetimepicker-to').data("DateTimePicker").show();
            });
            $('.datetimepicker-to').on('dp.change', function (e) {
                var dateFrom = $('.datetimepicker-from').data("DateTimePicker").date();
                if (e.date < dateFrom) {
                    $('.datetimepicker-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                //else {
                //    $('.datetimepicker-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));

                //}
                //$('.txtSearchToHidden').val($('.datetimepicker-to').val());
                <%--$('#<%= txtSearchToHidden.ClientID %>').val($('.datetimepicker-to').val());--%>
            });

            /* START Filter DatetimePicker Permission Temporary */
            $('.datetimepicker-filter-from').datetimepicker({
                format: 'DD/MM/YYYY',
                //ignoreReadonly: true
            });
            $('.datetimepicker-filter-from').on('dp.change', function (e) {
                var dateTo = $('.datetimepicker-filter-to').data("DateTimePicker").date();
                if (e.date > dateTo) {
                    $('.datetimepicker-filter-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
               <%-- $('#<%= txtFilterIndexPermTempFromHidden.ClientID %>').val($('.datetimepicker-filter-from').val());--%>
            });
            $('.show-filter-from-onclick').click(function () {
                $('.datetimepicker-filter-from').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-to').datetimepicker({
                format: 'DD/MM/YYYY',
                //ignoreReadonly: true
            });
            $('.show-filter-to-onclick').click(function () {
                $('.datetimepicker-filter-to').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-to').on('dp.change', function (e) {
                var dateFrom = $('.datetimepicker-filter-from').data("DateTimePicker").date();
                if (e.date < dateFrom) {
                    $('.datetimepicker-filter-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                <%--$('#<%= txtFilterIndexPermTempToHidden.ClientID %>').val($('.datetimepicker-filter-to').val());--%>
            });
            $('.datetimepicker-filter-only').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });
            $('.show-filter-only-onclick').click(function () {
                $('.datetimepicker-filter-only').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-only').on('dp.change', function (e) {
                <%--$('#<%= txtFilterIndexPermTempOnlyHidden.ClientID %>').val($('.datetimepicker-filter-only').val());--%>
            });
            /* END Filter Datetimepicker Permission Temporary */
            /* START Filter Datetimepicker Permission Permanent */
            $('.datetimepicker-filter-perm-from').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });
            $('.datetimepicker-filter-perm-from').on('dp.change', function (e) {
                var dateTo = $('.datetimepicker-filter-perm-to').data("DateTimePicker").date();
                if (e.date > dateTo) {
                    $('.datetimepicker-filter-perm-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                <%--$('#<%= txtFilterIndexPermPermFromHidden.ClientID %>').val($('.datetimepicker-filter-perm-from').val());--%>
            });
            $('.show-filter-perm-from-onclick').click(function () {
                $('.datetimepicker-filter-perm-from').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-perm-to').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });
            $('.show-filter-perm-to-onclick').click(function () {
                $('.datetimepicker-filter-perm-to').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-perm-to').on('dp.change', function (e) {
                var dateFrom = $('.datetimepicker-filter-perm-from').data("DateTimePicker").date();
                if (e.date < dateFrom) {
                    $('.datetimepicker-filter-perm-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                <%--$('#<%= txtFilterIndexPermPermToHidden.ClientID %>').val($('.datetimepicker-filter-perm-to').val());--%>
            });
            $('.datetimepicker-filter-perm-only').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });
            $('.show-filter-perm-only-onclick').click(function () {
                $('.datetimepicker-filter-perm-only').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-perm-only').on('dp.change', function (e) {
                <%--$('#<%= txtFilterIndexPermPermOnlyHidden.ClientID %>').val($('.datetimepicker-filter-perm-only').val());--%>
            });
            /* END Filter Datetimepicker Permission Permanent */
        });
    </script>

    <script type="text/javascript">

        //no post back
        $('.clockpicker').datetimepicker({
            format: 'HH:mm',
            stepping: 30,
            ignoreReadonly: true
        });
        $('.show-time-onclick').click(function () {
            $('.clockpicker').data("DateTimePicker").show();
        });

        $('.clockpickerto').datetimepicker({
            format: 'HH:mm',
            stepping: 30,
            ignoreReadonly: true
        });
        $('.show-timeto-onclick').click(function () {
            $('.clockpickerto').data("DateTimePicker").show();
        });

        //is
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {

            $('.loading-icon-approve').hide();

            $('.clockpicker').datetimepicker({
                format: 'HH:mm',
                stepping: 30,
                ignoreReadonly: true
            });
            $('.show-time-onclick').click(function () {
                $('.clockpicker').data("DateTimePicker").show();
            });

            $('.clockpickerto').datetimepicker({
                format: 'HH:mm',
                stepping: 30,
                ignoreReadonly: true
            });
            $('.show-timeto-onclick').click(function () {
                $('.clockpickerto').data("DateTimePicker").show();
            });
        });


        function getAlertSuccess() {
            $("#success_alert").show();
            $("#success_alert").fadeTo(500, 500).slideUp(500, function () {
                $("#success_alert").slideUp(500);
            });
        }

        function ValidatePage(toHide, toShow) {

            $(toHide).hide();
            $(toShow).show();

        }
    </script>

    <script type="text/javascript">
        $(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY HH:mm'
            });
        });

        //});

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY HH:mm'
                });
            });
        });

    </script>

    <script type="text/javascript">

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('#calendar').fullCalendar({
                    theme: true,

                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'month,agendaWeek'
                    },
                    displayEventTime: false, //hide time event
                    //eventColor: '#E3025B',
                    nowIndicator: true,

                    defaultView: 'month',
                    dayCount: 7,
                    timeFormat: 'h:mm a',
                    allDayText: 'all-day', // set replace all-day
                    selectable: false,
                    selectHelper: false,

                    //select: selectDate, //allDay: true,// this decides if the all day slot should be showed at the top
                    editable: false,
                    events: '<%= ResolveUrl("~/websystem/hr/JsonResponsePlanSale.ashx") %>',
                    eventDrop: eventDropped,
                    eventResize: eventResized,
                    eventLimit: true,

                    eventClick: function (event, jsEvent, view) {
                        $('#modalTitle').html(
                            '<strong>' + event.ty_name + '</strong>'
                        );
                        $('#modalBody').html(
                            //event.description
                            '<strong>' + 'ตั้งแต่วันที่ : ' + '</strong>' + event.start.format("DD/MM/YYYY h:mm a") + '<br>'
                            + '<strong>' + 'ถึงวันที่ : ' + '</strong>' + event.end.format("DD/MM/YYYY h:mm a") + '<br>'
                            + '<strong>' + 'รายละเอียดเพิ่มเติม : ' + '</strong>' + event.description + '<br>'
                        );
                        $('#eventUrl').attr('href', event.url);
                        $('#fullCalModal').modal();
                    },

                    eventRender: function (event, element) {
                        var today = moment(new Date()).format("YYYY-MM-DD");
                        //var start = moment(event.start).format("YYYY-MM-DD");
                        var date_end = moment(event.end, "YYYY-MM-DD");
                        var date_start = moment(event.start, "YYYY-MM-DD");

                        //var current = moment().startOf('day');

                        //alert(event.ty_name);

                        if (event != null) {
                            element.qtip({
                                content: {
                                    text: qTipText(event.parttime_name_th, event.start, event.end, event.description, event.ty_name, event.u0_unidx, event.u0_acidx, event.doc_decision),
                                    //text: qTipText(event.topic_booking, event.place_name, event.room_name_th, event.start, event.end, event.description),//qTipText(event.start, event.end, event.description, event.topic_booking),
                                    //title: '<strong>' + 'ทะเบียน' + ' : ' + event.car_register + '</strong>'
                                    title: '<strong>' + event.ty_name + '</strong>'
                                    //title: '<strong>' + 'หัวข้อ' + ' : ' + event.title + '</strong>'
                                    //alert(event.ty_name);
                                },
                                position: {
                                    my: 'top left',
                                    at: 'center left'
                                    //my: 'bottom left',
                                    //at: 'left center'
                                },
                                hide: {
                                    event: 'mouseout'
                                },
                                show: {
                                    event: 'mouseover'
                                },
                                style: {
                                    //classes: 'qtip-shadow qtip-rounded qtip-youtube'
                                    classes: 'qtip-tipsy'
                                }

                            });
                        }

                    },
                    //defaultView: 'listWeek'

                });
            });
        });
        $(function () {
            $('#calendar').fullCalendar({
                theme: true,

                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek'
                },
                displayEventTime: false, //hide time event
                //eventColor: '#E3025B',
                nowIndicator: true,
                defaultView: 'month',
                dayCount: 7,

                timeFormat: 'h:mm a',
                allDayText: 'all-day', // set replace all-day
                //eventClick: updateEvent,
                selectable: false,
                selectHelper: false,

                //allDay: true,// this decides if the all day slot should be showed at the top
                //select: selectDate,
                editable: false,
                events: '<%= ResolveUrl("~/websystem/hr/JsonResponsePlanSale.ashx") %>',
                eventDrop: eventDropped,
                eventResize: eventResized,
                eventLimit: true,
                backgroundColor: '#ccc',
                border: '0px solid #7BD148',

                eventClick: function (event, jsEvent, view) {
                    $('#modalTitle').html(
                        '<strong>' + event.ty_name + '</strong>'
                    );
                    $('#modalBody').html(
                        //event.description
                        '<strong>' + 'ตั้งแต่วันที่ : ' + '</strong>' + event.start.format("DD/MM/YYYY h:mm a") + '<br>'
                        + '<strong>' + 'ถึงวันที่ : ' + '</strong>' + event.end.format("DD/MM/YYYY h:mm a") + '<br>'
                        + '<strong>' + 'รายละเอียดเพิ่มเติม : ' + '</strong>' + event.description + '<br>'
                    );
                    $('#eventUrl').attr('href', event.url);
                    $('#fullCalModal').modal();
                },

                eventRender: function (event, element) {
                    var today = moment(new Date()).format("YYYY-MM-DD")
                    //var start = moment(event.start).format("YYYY-MM-DD");

                    var date_start = moment(event.start, "YYYY-MM-DD");
                    var date_end = moment(event.end, "YYYY-MM-DD");
                    //var current = moment().startOf('day');

                    //alert("3333" + event.u0);

                    if (event != null) {
                        element.qtip({
                            content: {
                                text: qTipText(event.parttime_name_th, event.start, event.end, event.description, event.ty_name, event.u0_unidx, event.u0_acidx, event.doc_decision),
                                //text: qTipText(event.topic_booking, event.place_name, event.start, event.end, event.description),
                                //text: qTipText(event.topic_booking, event.place_name, event.room_name_th, event.start, event.end, event.description),//qTipText(event.start, event.end, event.description, event.topic_booking),
                                title: '<strong>' + event.ty_name + '</strong>'
                                //title: '<strong>' + 'หัวข้อ' + ' : ' + event.title + '</strong>'
                            },
                            position: {
                                my: 'top left',
                                at: 'center left'
                                //target: 'mouse',
                                //adjust: {
                                //    mouse: false
                                //}
                                //my: 'bottom left',Panel_AddCarUseHr
                                //at: 'left center'
                            },
                            hide: {
                                event: 'mouseout'
                            },
                            show: {
                                event: 'mouseover'
                                //solo: true 
                            },
                            style: {
                                //classes: 'qtip-shadow  qtip-youtube'
                                classes: 'qtip-tipsy'
                            }

                        });

                        //alert(event.u0_unidx + " - " + event.u0_acidx);
                        var ty_name = event.ty_name;
                        var u0_unidx = event.u0_unidx;
                        var u0_acidx = event.u0_acidx;
                        var doc_decision = event.doc_decision;

                        if (ty_name == "ใบลา") {
                            if (u0_unidx == 2 && u0_acidx == 2) { //wait / edit
                                element.css('background-color', '#FF0000');
                                element.find(".fc-event-dot").css('background-color', '#FF0000')
                            }
                            else if ((u0_unidx == 5 && u0_acidx == 2 && doc_decision == 1) || (u0_unidx == 5 && u0_acidx == 3 && doc_decision == 1)) { // approve
                                element.css('background-color', '#3CB371');
                                element.find(".fc-event-dot").css('background-color', '#3CB371')
                            }
                            /*else { // cancel
                                element.css('background-color', '#969696');
                                element.find(".fc-event-dot").css('background-color', '#969696')
                            }*/
                        }
                        else if (ty_name == "ปฏิบัติงาน") {
                            if (u0_unidx == 2 && u0_acidx == 2 || u0_unidx == 1 && u0_acidx == 1) { //wait / edit
                                element.css('background-color', '#FF0000');
                                element.find(".fc-event-dot").css('background-color', '#FF0000')
                            }
                            else if (u0_unidx == 3 && u0_acidx == 2 && doc_decision == 2) { // approve
                                element.css('background-color', '#3CB371');
                                element.find(".fc-event-dot").css('background-color', '#3CB371')
                            }
                            else { // cancel
                                //element.css('background-color', '#969696');
                                //element.find(".fc-event-dot").css('background-color', '#969696')
                                element.css("color", "#FFFFFF");
                                element.css('background-color', '#FFFFFF');
                                element.css("border-style", "none");
                            }
                        }
                        else {
                            element.css("color", "#000000");
                            element.css('background-color', '#FFFFFF');
                            element.css("border-style", "none");
                        }

                    }
                }
                //defaultView: 'listWeek'


            });
        });

    </script>

    <script type="text/javascript">

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('#calendar').fullCalendar({
                    theme: true,

                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'month,agendaWeek'
                    },
                    displayEventTime: false, //hide time event
                    //eventColor: '#E3025B',
                    nowIndicator: true,
                    defaultView: 'month',

                    dayCount: 7,
                    timeFormat: 'h:mm a',
                    allDayText: 'all-day', // set replace all-day
                    selectable: false,
                    selectHelper: false,

                    //select: selectDate, //allDay: true,// this decides if the all day slot should be showed at the top
                    editable: false,
                    events: '<%= ResolveUrl("~/websystem/hr/JsonResponsePlanSale.ashx") %>',
                    eventDrop: eventDropped,
                    eventResize: eventResized,
                    eventLimit: true,

                    eventClick: function (event, jsEvent, view) {
                        $('#modalTitle').html(
                            '<strong>' + event.ty_name + "dddf" + '</strong>'
                        );
                        $('#modalBody').html(
                            //event.description
                            '<strong>' + 'ตั้งแต่วันที่ : ' + '</strong>' + event.start.format("DD/MM/YYYY h:mm a") + '<br>'
                            + '<strong>' + 'ถึงวันที่ : ' + '</strong>' + event.end.format("DD/MM/YYYY h:mm a") + '<br>'
                            + '<strong>' + 'รายละเอียดเพิ่มเติม : ' + '</strong>' + event.description + '<br>'
                        );
                        $('#eventUrl').attr('href', event.url);
                        $('#fullCalModal').modal();
                    },

                    eventRender: function (event, element) {
                        var today = moment(new Date()).format("YYYY-MM-DD");
                        //var start = moment(event.start).format("YYYY-MM-DD");
                        var date_end = moment(event.end, "YYYY-MM-DD");
                        var date_start = moment(event.start, "YYYY-MM-DD");

                        //var current = moment().startOf('day');

                        //alert(moment.duration(given.diff(today)).asDays());

                        if (event != null) {
                            element.qtip({
                                content: {
                                    text: qTipText(event.parttime_name_th, event.start, event.end, event.description, event.ty_name, event.u0_unidx, event.u0_acidx),
                                    //text: qTipText(event.topic_booking, event.place_name, event.room_name_th, event.start, event.end, event.description),//qTipText(event.start, event.end, event.description, event.topic_booking),
                                    //title: '<strong>' + 'ทะเบียน' + ' : ' + event.car_register + '</strong>'
                                    title: '<strong>' + event.ty_name + '</strong>'
                                    //title: '<strong>' + 'หัวข้อ' + ' : ' + event.title + '</strong>'
                                },
                                position: {
                                    my: 'top left',
                                    at: 'center left'
                                    //my: 'bottom left',
                                    //at: 'left center'
                                },
                                hide: {
                                    event: 'mouseout'
                                },
                                show: {
                                    event: 'mouseover'
                                },
                                style: {
                                    //classes: 'qtip-shadow qtip-rounded qtip-youtube'
                                    classes: 'qtip-tipsy'
                                }

                            });

                            var ty_name = event.ty_name;
                            var u0_unidx = event.u0_unidx;
                            var u0_acidx = event.u0_acidx;
                            var doc_decision = event.doc_decision;

                            if (ty_name == "ใบลา") {
                                if (u0_unidx == 2 && u0_acidx == 2) { //wait / edit
                                    element.css('background-color', '#FF0000');
                                    element.find(".fc-event-dot").css('background-color', '#FF0000')
                                }
                                else if ((u0_unidx == 5 && u0_acidx == 2 && doc_decision == 1) || (u0_unidx == 5 && u0_acidx == 3 && doc_decision == 1)) { // approve
                                    element.css('background-color', '#3CB371');
                                    element.find(".fc-event-dot").css('background-color', '#3CB371')
                                }
                                /*else { // cancel
                                    element.css('background-color', '#969696');
                                    element.find(".fc-event-dot").css('background-color', '#969696')
                                }*/
                            }
                            else if (ty_name == "ปฏิบัติงาน") {
                                if (u0_unidx == 2 && u0_acidx == 2 || u0_unidx == 1 && u0_acidx == 1) { //wait / edit
                                    element.css('background-color', '#FF0000');
                                    element.find(".fc-event-dot").css('background-color', '#FF0000')
                                }
                                else if (u0_unidx == 3 && u0_acidx == 2 && doc_decision == 2) { // approve
                                    element.css('background-color', '#3CB371');
                                    element.find(".fc-event-dot").css('background-color', '#3CB371')
                                }
                                else { // cancel
                                    //element.css('background-color', '#969696');
                                    //element.find(".fc-event-dot").css('background-color', '#969696')
                                    element.css("color", "#FFFFFF");
                                    element.css('background-color', '#FFFFFF');
                                    element.css("border-style", "none");
                                }
                            }
                            else {
                                element.css("color", "#000000");
                                element.css('background-color', '#FFFFFF');
                                element.css("border-style", "none");
                            }

                        }

                    },
                    //defaultView: 'listWeek'

                });
            });
        });
        $(function () {
            $('#calendar').fullCalendar({
                theme: true,

                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek'
                },
                displayEventTime: false, //hide time event
                //eventColor: '#E3025B',
                nowIndicator: true,


                defaultView: 'month',
                dayCount: 7,

                timeFormat: 'h:mm a',
                allDayText: 'all-day', // set replace all-day
                //eventClick: updateEvent,
                selectable: false,
                selectHelper: false,

                //allDay: true,// this decides if the all day slot should be showed at the top
                //select: selectDate,
                editable: false,
                events: '<%= ResolveUrl("~/websystem/hr/JsonResponsePlanSale.ashx") %>',
                eventDrop: eventDropped,
                eventResize: eventResized,
                eventLimit: true,

                eventClick: function (event, jsEvent, view) {
                    $('#modalTitle').html(
                        '<strong>' + event.ty_name + '</strong>'
                    );
                    $('#modalBody').html(
                        //event.description
                        '<strong>' + 'ตั้งแต่วันที่ : ' + '</strong>' + event.start.format("DD/MM/YYYY h:mm a") + '<br>'
                        + '<strong>' + 'ถึงวันที่ : ' + '</strong>' + event.end.format("DD/MM/YYYY h:mm a") + '<br>'
                        + '<strong>' + 'รายละเอียดเพิ่มเติม : ' + '</strong>' + event.description + '<br>'
                    );
                    $('#eventUrl').attr('href', event.url);
                    $('#fullCalModal').modal();
                },

                eventRender: function (event, element) {
                    var today = moment(new Date()).format("YYYY-MM-DD")
                    //var start = moment(event.start).format("YYYY-MM-DD");

                    var date_start = moment(event.start, "YYYY-MM-DD");
                    var date_end = moment(event.end, "YYYY-MM-DD");
                    //var current = moment().startOf('day');

                    //alert("5555" + event.ty_name);

                    if (event != null) {
                        element.qtip({
                            content: {
                                text: qTipText(event.parttime_name_th, event.start, event.end, event.description, event.ty_name, event.u0_unidx, event.u0_acidx),
                                //text: qTipText(event.topic_booking, event.place_name, event.start, event.end, event.description),
                                //text: qTipText(event.topic_booking, event.place_name, event.room_name_th, event.start, event.end, event.description),//qTipText(event.start, event.end, event.description, event.topic_booking),
                                title: '<strong>' + event.ty_name + '</strong>'
                                //title: '<strong>' + 'หัวข้อ' + ' : ' + event.title + '</strong>'
                            },
                            position: {
                                my: 'top left',
                                at: 'center left'
                                //target: 'mouse',
                                //adjust: {
                                //    mouse: false
                                //}
                                //my: 'bottom left',Panel_AddCarUseHr
                                //at: 'left center'
                            },
                            hide: {
                                event: 'mouseout'
                            },
                            show: {
                                event: 'mouseover'
                                //solo: true 
                            },
                            style: {
                                //classes: 'qtip-shadow  qtip-youtube'
                                classes: 'qtip-tipsy'
                            }

                        });

                    }
                }
                //defaultView: 'listWeek'


            });
        });

    </script>

    <script>
        $(document).ready(function () {
            window.Sys.WebForms.PageRequestManager.getInstance().add_endRequest(pageLoad);

            function pageLoad() {
                $("#UploadImages").MultiFile();
            }
        })
    </script>


    <%--</ContentTemplate>
    </asp:UpdatePanel>--%>
</asp:Content>

