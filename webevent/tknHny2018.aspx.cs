using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class webevent_tknHny2018 : System.Web.UI.Page
{
    #region initial function/data
    function_tool _funcTool = new function_tool();
    data_hny dataHny = new data_hny();
    service_execute serviceExecute = new service_execute();

    string localXml = String.Empty;
    string localString = String.Empty;
    int actionType = 0;
	int _type = 0;
    #endregion initial function/data

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            initPage();
        }
    }

    #region event command
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {
            case "cmdSelectPlant":
                setActiveView("viewPlant", 0);
                switch (cmdArg)
                {
                    case "npw":
                        ViewState["plant"] = "1";
                        setPrizeList();
                        break;
                    case "rjn":
                        ViewState["plant"] = "2";
                        setPrizeList();
                        break;
                    default:
                        break;
                }
                break;
            case "cmdSelectPrize":
                setActiveView("viewRandom", 0);
                randomBox.Visible = true;
                getPrizeList(int.Parse(cmdArg));
                setGridData(gvResult, null);
                // getHistoryList(int.Parse(cmdArg));
                setGridData(gvHistory, null);
                break;
            case "cmdViewPrize":
                setActiveView("viewRandom", 0);
                randomBox.Visible = false;
                setGridData(gvResult, null);
                getPrizeList(int.Parse(cmdArg));
                getHistoryList(int.Parse(cmdArg));
                break;
            case "cmdBackToPrize":
                setActiveView("viewPlant", 0);
                // data_hny dLocal = new data_hny();
                // detail_prize _detail = new detail_prize();
                // _detail.midx = int.Parse(cmdArg);
                // _detail.prize_type = int.Parse(ViewState["plant"].ToString());
                // dLocal.prize_list = new detail_prize[1];
                // dLocal.prize_list[0] = _detail;
                // dLocal = (data_hny)_funcTool.convertXmlToObject(typeof(data_hny), serviceExecute.actionExec("conn_mas", "data_hny", "service_hny_random", dLocal, 13));
                setPrizeList();
                break;
            case "cmdRandomPrize":
                detail_prize_emp prize_emp = new detail_prize_emp();
                prize_emp.emp_prize_idx = int.Parse(cmdArg);
                prize_emp.emp_prize_rand = int.Parse(tbRandPrize.Text.Trim());
                dataHny.prize_emp_list = new detail_prize_emp[1];
                dataHny.prize_emp_list[0] = prize_emp;

                dataHny = (data_hny)_funcTool.convertXmlToObject(typeof(data_hny), serviceExecute.actionExec("conn_mas", "data_hny", "service_hny_random", dataHny, 20));
                setGridData(gvResult, dataHny.prize_emp_list);

                getPrizeList(int.Parse(cmdArg));
                // getHistoryList(int.Parse(cmdArg));
                setGridData(gvHistory, null);
                break;
            case "cmdConfirm":
                string[] cmdArg2 = cmdArg.Split(','); //empcode, prize_idx
                detail_prize_emp prize_emp_confirm = new detail_prize_emp();
                prize_emp_confirm.emp_code = cmdArg2[0];
                prize_emp_confirm.emp_prize_idx = int.Parse(cmdArg2[1]);
                dataHny.prize_emp_list = new detail_prize_emp[1];
                dataHny.prize_emp_list[0] = prize_emp_confirm;

                dataHny = (data_hny)_funcTool.convertXmlToObject(typeof(data_hny), serviceExecute.actionExec("conn_mas", "data_hny", "service_hny_random", dataHny, 11));
                // setGridData(gvResult, dataHny.prize_emp_list);
                getHistoryList(int.Parse(cmdArg2[1]));
                break;
            case "cmdCancel":
                string[] cmdArg3 = cmdArg.Split(','); //empcode, prize_idx
                detail_prize_emp prize_emp_cancel = new detail_prize_emp();
                prize_emp_cancel.emp_code = cmdArg3[0];
                prize_emp_cancel.emp_prize_idx = int.Parse(cmdArg3[1]);
                dataHny.prize_emp_list = new detail_prize_emp[1];
                dataHny.prize_emp_list[0] = prize_emp_cancel;

                dataHny = (data_hny)_funcTool.convertXmlToObject(typeof(data_hny), serviceExecute.actionExec("conn_mas", "data_hny", "service_hny_random", dataHny, 14));
                // setGridData(gvResult, dataHny.prize_emp_list);
                getPrizeList(int.Parse(cmdArg3[1]));
                getHistoryList(int.Parse(cmdArg3[1]));
                break;
            default:
                break;
        }
    }
    #endregion event command

    #region rowdatabound
    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "gvPrize":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Literal litPrizeRemain = (Literal)e.Row.Cells[2].FindControl("litPrizeRemain");
                    LinkButton lbSelectPrize = (LinkButton)e.Row.Cells[3].FindControl("lbSelectPrize");
                    if(litPrizeRemain.Text == "0")
                    {
                        lbSelectPrize.Visible = false;
                    }
                }
                break;
            case "gvHistory":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HiddenField hfEmpPrizeStatus = (HiddenField)e.Row.Cells[0].FindControl("hfEmpPrizeStatus");
                    LinkButton lbConfirm = (LinkButton)e.Row.Cells[4].FindControl("lbConfirm");
                    LinkButton lbCancel = (LinkButton)e.Row.Cells[4].FindControl("lbCancel");
                    if(hfEmpPrizeStatus.Value == "1")
                    {
                        lbConfirm.Visible = false;
                        lbCancel.Visible = false;
                    }
                }
                break;
        }
    }
    #endregion rowdatabound

    #region reuse
    protected void initPage()
    {
        clearSession();
        clearViewState();

        setActiveView("viewSelectPlant", 0);
    }

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        ViewState["plant"] = null;
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setActiveView(string activeTab, int uidx)
    {
        mvSystem.SetActiveView((View)mvSystem.FindControl(activeTab));
    }

    protected void setPrizeList()
    {
        data_hny dLocal = new data_hny();
        detail_prize _detail = new detail_prize();
        _detail.prize_type = int.Parse(ViewState["plant"].ToString());
        dLocal.prize_list = new detail_prize[1];
        dLocal.prize_list[0] = _detail;
        dLocal = (data_hny)_funcTool.convertXmlToObject(typeof(data_hny), serviceExecute.actionExec("conn_mas", "data_hny", "service_hny_random", dLocal, 30));
		try 
	    {
            // _type = int.Parse(ViewState["plant"].ToString());
			// var _linqPrizeList = from d in dLocal.prize_list
			// 					where d.prize_type == _type
			// 					select d;

			// setGridData(gvPrize, _linqPrizeList.ToList());
            setGridData(gvPrize, dLocal.prize_list);
		} 
	    catch (Exception ex) 
	    {
	    	Response.Write(ex.Message);
	    } finally {}
    }

    protected void getPrizeList(int midx)
    {
        data_hny dLocal = new data_hny();
        detail_prize _detail = new detail_prize();
        _detail.midx = midx;
        dLocal.prize_list = new detail_prize[1];
        dLocal.prize_list[0] = _detail;
        dLocal = (data_hny)_funcTool.convertXmlToObject(typeof(data_hny), serviceExecute.actionExec("conn_mas", "data_hny", "service_hny_random", dLocal, 31));
		try 
	    {
            litPrizeName.Text = dLocal.prize_list[0].prize_name;
            litPrizeReward.Text = dLocal.prize_list[0].prize_reward.ToString();
            litPrizeRemain.Text = dLocal.prize_list[0].prize_remain.ToString();
            tbRandPrize.Text = dLocal.prize_list[0].prize_remain.ToString();
            lbRandomPrize.CommandArgument = midx.ToString();
            lbBackToPrizeTop.CommandArgument = midx.ToString();
            // lbBackToPrizeBottom.CommandArgument = midx.ToString();
		} 
	    catch (Exception ex) 
	    {
	    	Response.Write(ex.Message);
	    } finally {}
    }

    protected void getHistoryList(int midx)
    {
        data_hny dLocal = new data_hny();
        detail_prize _detail = new detail_prize();
        _detail.midx = midx;
        dLocal.prize_list = new detail_prize[1];
        dLocal.prize_list[0] = _detail;
        dLocal = (data_hny)_funcTool.convertXmlToObject(typeof(data_hny), serviceExecute.actionExec("conn_mas", "data_hny", "service_hny_random", dLocal, 21));
		setGridData(gvHistory, dLocal.prize_emp_list);
    }
    #endregion reuse
}